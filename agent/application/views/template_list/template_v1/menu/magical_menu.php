<?php
$active_domain_modules = $this->active_domain_modules;
/**
 * Need to make privilege based system
 * Privilege only for loading menu and access of the web page
 * 
 * Data loading will not be based on privilege.
 * Data loading logic will be different.
 * It depends on many parameters
 */
$menu_list = array();
if (count($active_domain_modules) > 0) {
	$any_domain_module = true;
} else {
	$any_domain_module = false;
}
$airline_module = is_active_airline_module();
$accomodation_module = is_active_hotel_module();
$bus_module = is_active_bus_module();
$package_module = is_active_package_module();
$sightseeing_module = is_active_sightseeing_module();
$car_module = is_active_car_module();
$transfer_module =is_active_transferv1_module();
// debug($car_module);exit;
?>
<ul class="sidebar-menu">
	<li class="header">MAIN NAVIGATION</li>
	<li class="active treeview">
		<a href="<?php echo base_url()?>">
			<i class="fa fa-tachometer-alt light_green"></i> <span>Dashboard</span>
		</a>
	</li>
	<!-- USER ACCOUNT MANAGEMENT -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-search dark_pink"></i> 
			<span>Search </span><i class="fa fa-angle-left pull-right"></i></a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<?php if ($airline_module) { ?>
			<li><a href="<?=base_url().'menu/index/flight/?default_view='.META_AIRLINE_COURSE?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?> text-blue"></i> <span class="hidden-xs">Flight</span></a></li>
			<?php } ?>
			<?php if ($accomodation_module) { ?>
			<li><a href="<?=base_url().'menu/index/hotel/?default_view='.META_ACCOMODATION_COURSE?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?> text-green"></i> <span class="hidden-xs">Hotel</span></a></li>
			<!-- <li><a href="<?=base_url().'menu/index/villa/?default_view='.'villa001'?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?> text-green"></i> <span class="hidden-xs">Villa</span></a></li> -->
			<?php } ?>
			<?php if ($bus_module) { ?>
			<li><a href="<?=base_url().'menu/index/bus/?default_view='.META_BUS_COURSE?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?> text-red"></i> <span class="hidden-xs">Bus</span></a></li>
			<?php } ?>
			<?php if($transfer_module){?>
				<li><a href="<?=base_url().'menu/index/transfers/?default_view='.META_TRANSFERV1_COURSE?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?> text-yellow"></i> <span class="hidden-xs">Car Rental</span></a></li>
			<?php }?>
			<?php if($sightseeing_module){?>
				<li><a href="<?=base_url().'menu/index/sightseeing/?default_view='.META_SIGHTSEEING_COURSE?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?> text-aqua"></i> <span class="hidden-xs">Activities</span></a></li>
			<?php }?>
			<?php if($car_module){?>
				<li><a href="<?=base_url().'menu/index/car/?default_view='.META_CAR_COURSE?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?> text-teal"></i> <span class="hidden-xs">Car</span></a></li>
			<?php }?>
			<?php if ($package_module) { ?>
			<li><a href="<?=base_url().'menu/index/package/?default_view='.META_PACKAGE_COURSE?>"><i class="<?=get_arrangement_icon(META_PACKAGE_COURSE)?> text-olive"></i> <span class="hidden-xs">Holiday</span></a></li>
			<?php } ?>
		</ul>
	</li>
	<?php if ($any_domain_module) {?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-chart-bar dark_blue"></i> 
			<span> Reports </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<li><a href="#"><i class="fa fa-book blue"></i> Booking Details</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'report/flight/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'report/hotel/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
				<!-- <li><a href="<?php echo base_url().'report/hotel_crs/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel CRS</a></li> -->
				<!-- <li><a href="<?php echo base_url().'report/villa/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Villa</a></li> -->
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'report/bus/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>
				<?php if($transfer_module){?>
					<li><a href="<?php echo base_url().'report/transfers/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Car Rental</a></li>
				<?php }?>
				<?php if($sightseeing_module):?>
					<li><a href="<?php echo base_url().'report/activities/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i>Activities</a></li>
				<?php endif;?>
				<?php if($package_module):?>
					<li><a href="<?php echo base_url().'report/holiday/';?>"><i class="<?=get_arrangement_icon(META_PACKAGE_COURSE)?>"></i>Holiday</a></li>
				<?php endif;?>
				<?php if($car_module):?>
					<li><a href="<?php echo base_url().'report/car/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i>Car</a></li>
				<?php endif;?>
				</ul>
			</li>
			<li><a href="<?php echo base_url().'management/pnr_search'?>"><i class="fa fa-search sandal"></i> <span>PNR Search</span></a></li>
			<li><a href="<?php echo base_url().'report/flight?filter_booking_status=BOOKING_PENDING'?>"><i class="fa fa-ticket deep_blue"></i> <span>Pending Ticket</span></a></li>
			<li><a href="<?php echo base_url().'report/flight?daily_sales_report='.ACTIVE?>"><i class="far fa-chart-bar violet"></i> <span>Daily Sales Report</span></a></li>
			<li><a href="<?php echo base_url().'management/account_ledger'?>"><i class="fa fa-calculator greenish_blue"></i> <span>Account Ledger</span></a></li>
			<li class="treeview"><a href="<?php echo base_url().'index.php/transaction/logs'?>"><i class="far fa-list-alt yellowish_orange"></i> <span> Transaction Logs </span></a></li>

			<li class="treeview"><a href="<?php echo base_url().'index.php/transaction/loyality'?>"><i class="far fa-list-alt yellowish_orange"></i> <span> Loyality Points </span></a></li>


		</ul>
	</li>
	<?php /*
	if($airline_module || $bus_module || $sightseeing_module) {
	?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-briefcase light_pink"></i> 
			<span> My Commission </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<?php if ($airline_module) { ?>
			<li><a href="<?=base_url().'management/flight_commission';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?> text-blue"></i> Flight</a></li>
			<?php } ?>
			<?php if ($bus_module) { ?>
			<li><a href="<?=base_url().'management/bus_commission';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?> text-green"></i> Bus</a></li>
			<?php } ?>
			<?php if($transfer_module):?>
				<li><a href="<?=base_url().'management/transfer_commission';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?> text-red"></i>Transfers</a></li>

			<?php endif;?>
			<?php if($sightseeing_module){?>
				<li><a href="<?=base_url().'management/sightseeing_commission';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?> text-yellow"></i>Activities</a></li>
			<?php }?>
		</ul>
	</li>
	<?php } ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-plus-square dark_green"></i> 
			<span> My Markup </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<?php if ($airline_module) { ?>
			<li><a href="<?php echo base_url().'management/b2b_airline_markup/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?> text-blue"></i> Flight</a></li>
			<?php } ?>
			<?php if ($accomodation_module) { ?>
			<li><a href="<?php echo base_url().'management/b2b_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?> text-green"></i> Hotel</a></li>
			<?php } ?>
			<?php if ($bus_module) { ?>
			<li><a href="<?php echo base_url().'management/b2b_bus_markup/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?> text-red"></i> Bus</a></li>
			<?php } ?>

			<?php if ($transfer_module) { ?>
			<li><a href="<?php echo base_url().'management/b2b_transfer_markup/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?> text-yellow"></i>Transfers</a></li>
			<?php } ?>

			<?php if ($sightseeing_module) { ?>
			<li><a href="<?php echo base_url().'management/b2b_sightseeing_markup/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?> text-aqua"></i>Activities</a></li>
			<?php } ?>
			<?php if($car_module){?>
			<li><a href="<?=base_url().'management/b2b_car_markup';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?> text-teal"></i>Car</a></li>
			<?php }?>
		</ul>
	</li>
	<?php */ } /*?>
	
	<li class="treeview">
		<a href="#">
			<i class="fab fa-google-wallet light_brown"></i> 
			<span> Payment </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<li><a href="<?php echo base_url().'management/b2b_balance_manager'?>"><i class="fas fa-rupee-sign orange"></i> Update Balance</a></li>
			<li><a href="<?php echo base_url().'index.php/management/bank_account_details'?>"><i class="fas fa-university dark_grey"></i> Bank Account Details</a></li>
		</ul>
	</li>
	<li><a href="<?php echo base_url().'management/set_balance_alert'?>"><i class="fa fa-bell light_orange"></i> <span>Set Balance Alert</span></a></li>
	<?php */ ?>
	<li><a href="<?php echo base_url().'user/domain_logo'?>"><i class="fa fa-image light_yellow"></i> <span>Logo</span></a></li>
	</ul>