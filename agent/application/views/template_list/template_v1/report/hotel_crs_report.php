<link href="<?php echo $GLOBALS['CI']->template->template_css_dir('bootstrap-toastr/toastr.min.css');?>" rel="stylesheet" defer>
<script src="<?php echo $GLOBALS['CI']->template->template_js_dir('bootstrap-toastr/toastr.min.js'); ?>"></script>
<div class="bodyContent">
	<div class="table_outer_wrper"><!-- PANEL WRAP START -->
		<div class="panel_custom_heading"><!-- PANEL HEAD START -->
			<div class="panel_title">
				<?php echo $GLOBALS['CI']->template->isolated_view('share/report_navigator_tab')?>
                <div class="clearfix"></div>
                <div class="search_fltr_section">
                <form method="GET" role="search" class="navbar-form" id="auto_suggest_booking_id_form">
				<div class="form-group">
				<input type="hidden" id="module" value="<?=CRS_HOTEL_BOOKING_SOURCE?>">
				<input type="text" autocomplete="off" data-search_category="search_query" placeholder="AppReference/ConfNumber" class="form-control auto_suggest_booking_id ui-autocomplete-input" id="auto_suggest_booking_id" name="filter_report_data" value="<?=@$_GET['filter_report_data']?>">
				</div>
				<button title="Search" class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
				<a title="Clear Search" class="btn btn-default" href="<?=base_url().'index.php/report/hotel'?>"><i class="fa fa-history"></i></a>
			</form>
           	 </div>
			</div>
		</div><!-- PANEL HEAD START -->
		<div class="panel_bdy"><!-- PANEL BODY START -->
			
			<div class="tab-content">
			<?php echo get_table($table_data, $total_rows);?>
			</div>
		</div>
	</div>
</div>
<?=$GLOBALS['CI']->template->isolated_view('report/email_popup')?>
<?php
function get_table($table_data, $total_rows)
{
	$pagination = '<div class="pull-right">'.$GLOBALS['CI']->pagination->create_links().'<span class="">Total '.$total_rows.' Bookings</span></div>';
	$report_data = '';
	$report_data .= '<div id="tableList" class="">';
	$report_data .= $pagination;
	
	$report_data .= '<table class="table table-condensed table-bordered" id="b2b_report_hotel_table">
		<tr>
			<th>Sno</th>
			<th>Action</th>
			<th>Application<br/>Reference</th>
			<th>Confirmation<br/>Number</th>
			<th>Customer<br/>Name</th>
            <th>Area</th>
			<th>Fare</th>
			<th>Markup</th>
			<th>TotalFare</th>
			<th>CheckIn/<br/>CheckOut</th>
			<th>BookedOn</th>
            <th>Status</th>
		</tr>';
		
		if (isset($table_data) == true and valid_array($table_data['booking_details']) == true) {
			$segment_3 = $GLOBALS['CI']->uri->segment(3);
			$current_record = (empty($segment_3) ? 1 : $segment_3);
			$booking_details = $table_data['booking_details'];
		    foreach($booking_details as $parent_k => $parent_v) {
		    	//debug($parent_v); 
		    	extract($parent_v);
		    	$app_reference = $parent_pnr;
				$action = '';
				$email=$email_id;
				$tdy_date = date ( 'Y-m-d' );
				$hotel_check_in = date('Y-m-d',strtotime($actual_check_in_date));
				$booking_source = CRS_HOTEL_BOOKING_SOURCE;
				$status = $booking_status;
				$confirmation_reference = $ref_id;
				$app_reference = $parent_pnr;

				$customer_title = 'Mr';
				$customer_f_name = $contact_fname;
				$customer_m_name = $contact_mname;
				$customer_l_name = $contact_sur_name;

				$fare = $total_room_price;
				$agent_markup = $agent_markup_only;
				$grand_total = $total_room_price;
				$hotel_check_out = $actual_check_out_date;

				$diff = get_date_difference($tdy_date,$hotel_check_in);
				//$action .= hotel_voucher($app_reference, $booking_source, $status);
				$action .= crs_hotel_voucher($app_reference, $booking_source, $booking_status);
				$action .='<br/>';
				/*$action .= hotel_pdf($app_reference, $booking_source, $status);*/
				// $action .= crs_hotel_pdf($ref_id, $booking_source, $booking_status);
				$action .='<br/>';
				$action .= hotel_voucher_email($app_reference, $booking_source,$status,$email);
				$action.='<br/>';
				if(($status == 'CONFIRM') && ($diff > 0)) {
					$action .= cancel_crs_hotel_booking($parent_pnr, $booking_source, $booking_status);
				}
				$action.= get_cancellation_details_button($app_reference, $booking_source, $status);
				$email = hotel_email_voucher($app_reference, $booking_source, $status);
			
		$report_data .= '<tr>
					<td>'.($current_record++).'</td>
					<td><div class="btn-group dropdown_hover" role="group">
							<button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="glyphicon glyphicon-chevron-right"></span>Actions
							</button>
							<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
							' . $action . '
							</div>
						</div>
					</td>
					<td>'.$app_reference.'</td>
					<td class="">'.$confirmation_reference.'</span></td>
					<td>'.$customer_title.' '.$customer_f_name.' '.$customer_l_name.'</td>
					<td>'.$hotel_location.'</td>
					<td>'.($fare+$admin_markup).'</td>
					<td>'.$agent_markup.'</td>
					<td>'.$grand_total.'</td>
					<td>'.app_friendly_absolute_date($hotel_check_in).'/<br/>'.app_friendly_absolute_date($hotel_check_out).'</td>
					<td>'.$voucher_date.'</td>
					<td><span class="'.booking_status_label($status).'">'.$status.'</span></td>
				</tr>';
			}
		} else {
			$report_data .= '<tr><td>No Data Found</td></tr>';
		}
	$report_data .= '</table>
			</div>';
	return $report_data;
}
function get_accomodation_cancellation($courseType, $refId)
{
	return '<a href="'.base_url().'index.php/booking/accomodation_cancellation?courseType='.$courseType.'&refId='.$refId.'" class="col-md-12 btn btn-sm btn-danger "><i class="fa fa-exclamation-triangle"></i> Cancel</a>';
}
function hotel_voucher_email($app_reference, $booking_source,$status,$recipient_email)
{

	return '<a class=" send_email_voucher fa fa-envelope-o" data-app-status="'.$status.'"   data-app-reference="'.$app_reference.'" data-booking-source="'.$booking_source.'"data-recipient_email="'.$recipient_email.'">Email Voucher</a>';
}
function get_cancellation_details_button($app_reference, $booking_source, $status)
{
	if($status == 'BOOKING_CANCELLED'){
		return '<a target="_blank" href="'.base_url().'hotel/cancellation_refund_details?app_reference='.$app_reference.'&booking_source='.$booking_source.'&status='.$status.'" class="col-md-12 btn btn-sm btn-info "><i class="fa fa-info"></i> Cancellation Details</a>';
	}
}
function crs_hotel_voucher($app_reference, $booking_source='', $status='')
{
	return '<a href="'.crs_hotel_voucher_url($app_reference, $booking_source, $status).'/show_voucher" target="_blank" class=""><i class="fa fa-file-o"></i> Voucher</a>';
}
function crs_hotel_voucher_url($app_reference, $booking_source='', $status='')
{
	return base_url().'index.php/voucher/hotels/'.$app_reference.'/'.$status;
}
function crs_hotel_pdf($app_reference, $booking_source='', $status='')
{
	return '<a href="'.crs_hotel_voucher_pdf_url($app_reference, $booking_source, $status).'/show_pdf" target="_blank" class=""><i class="fa fa-file-o"></i> PDF</a>';
}
function crs_hotel_voucher_pdf_url($app_reference, $booking_source='', $status='')
{
	return base_url().'index.php/voucher/hotel_crs/'.$app_reference.'/'.$status;
}
function cancel_crs_hotel_booking($app_reference, $booking_source = '', $status = '') {
	return '<a href="' . cancel_crs_hotel_booking_url ( $app_reference, $booking_source, $status ) . '"  class="" target="_blank"><i class="fa fa-file-o"></i>Cancel</a>';
}
function cancel_crs_hotel_booking_url($app_reference, $booking_source = '', $status = '') {
	return base_url () . 'index.php/hotels/pre_cancellation/' . $app_reference . '/' . $booking_source . '/' . $status;
}
?>
<script>

$(document).ready(function() {

	 //send the email voucher
		$('.send_email_voucher').on('click', function(e) {
			$("#mail_voucher_modal").modal('show');
			$('#mail_voucher_error_message').empty();
	        email = $(this).data('recipient_email');
			$("#voucher_recipient_email").val(email);
	        app_reference = $(this).data('app-reference');
	        book_reference = $(this).data('booking-source');
	        app_status = $(this).data('app-status');
		  $("#send_mail_btn").off('click').on('click',function(e){
			  email = $("#voucher_recipient_email").val();
			  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			 if(email != ''){ 
				  if(!emailReg.test(email)){
					  $('#mail_voucher_error_message').empty().text('Please Enter Correct Email Id');
	                     return false;    
					      }
				 
						var _opp_url = app_base_url+'index.php/voucher/hotels/';
						_opp_url = _opp_url+app_reference+'/'+'/'+app_status+'/email_voucher/'+email;
						toastr.info('Please Wait!!!');
						$.get(_opp_url, function() {
							
							toastr.info('Email sent  Successfully!!!');
							$("#mail_voucher_modal").modal('hide');
						});
			    }else{
			 	  	 $('#mail_voucher_error_message').empty().text('Please Enter Email ID');
				 }
			 
		  });

	});

	$('#b2b_report_hotel_table a.btn-warning').on('click', function(e) {

		var _opp_url = $(this).attr('href');
		$('#hotel_cancel_modal').modal('show');
		$('#cancel_hotel_btn').on('click',function(e){
				$.ajax({
				  url: _opp_url,
				  cache: false,
				  dataType:"json",
				  success: function(html){
				    alert(html.msg);
				    $('#hotel_cancel_modal').modal('hide');
				     location.reload();
				  }
				});
		});
		e.preventDefault();
	});	

	});
</script>
