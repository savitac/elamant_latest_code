<?php
$template_images = $GLOBALS ['CI']->template->template_images ();
$mini_loading_image = '<div class="text-center loader-image">Please Wait</div>';
$trace_id = $raw_hotel_list[0]['unique_id'];
//debug($raw_hotel_list);exit;
foreach ( $raw_hotel_list as $hd_key=> $hd ) { 
	$current_hotel_rate = $hd ['star_rating'];
	$image_found=1;
	?>
<div class="rowresult r-r-i item">
	<div class="madgrid forhtlpopover shapcs" id="result_<?=$hd_key?>" data-key="<?=$hd_key?>" 
	data-hotel-code="<?=$hd['hotel_code'].$hd['HotelCode']?>" data-access-key="<?=@$hd['latitude'].'_'.@$hd['longitude']?>">
	<div class="col-xs-4 nopad listimage full_mobile">
			<div class="imagehtldis">
				<?php 
                    $search_id = intval($attr['search_id']);	
                    if($hd['pic']&&$image_found==true):?>
					<img src="<?=$hd['pic']?>" alt="Hotel Image" data-src="<?=$hd['pic']?>" class="lazy h-img load-image hide">
					<img src="<?=$GLOBALS['CI']->template->template_images('image_loader.gif')?>" class='loader-image'>
				<?php else:?>
					<img src="<?=$hd['pic']?>" alt="Hotel Image" data-src="<?=$GLOBALS['CI']->template->template_images('default_hotel_img.jpg') ?>" class="lazy h-img">
				<?php endif;?>

				<!-- <?php if($hd['pic']&&$image_found==true):?>
				<a data-target="map-box-modal" data-result-token="<?=urlencode($hd['unique_id'])?>" data-booking-source="<?=urlencode($hd['api'])?>" data-price="<?=$hd['total_price']?>" data-star-rating="<?=$current_hotel_rate?>"  data-hotel-name="<?php echo $hd['hotel_name']?>" id="map_id_<?=$hd_key?>" data-trip-url="<?=$hd['pic']?>" data-trip-rating="<?=$current_hotel_rate?>" data-id="<?=$hd_key?>" class="hotel-image-gal mapviewhtlhotl fa fa-picture-o view-photo-btn" data-hotel-code="<?=$hd['hotel_code'].$hd['HotelCode']?>"></a>  
				<?php endif;?> -->
				<a class="hotel_location" data-lat="<?=@$hd['latitude']?>" data-lon="<?=@$hd['longitude']?>"></a>
				
			</div>
		</div>

<div class="col-xs-8 nopad listfull full_mobile">
			<div class="sidenamedesc">
				<div class="celhtl width70">
					<div class="innd">
					   <div class="imptpldz">
						<div class="property-type" data-property-type="hotel"></div>
						<div class="shtlnamehotl">
							 <span class="h-name"><?php echo $hd['hotel_name']?></span> 
						</div>
						<div class="starrtinghotl rating-no">
							<span class="h-sr hide"><?php echo $current_hotel_rate?></span>
							<?php echo print_star_rating($current_hotel_rate);?>
						</div>
						<div class="adreshotle h-adr" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $hd['hotel_address']?>"><?php echo $hd['hotel_address']?>
						</div>
						<div class="preclsdv">
							<?php if(isset($hd['Free_cancel_date'])):?>
								<?php if($hd['Free_cancel_date']):?>
						 		 <span class="canplyto"><i class="fa fa-check" aria-hidden="true"></i> Free Cancellation till:<b><?=local_month_date($hd['Free_cancel_date']);?></b></span>
						 		 <input type="hidden" class="free_cancel" type="text" value="1" data-free-cancel="1">
						 		<?php else:?>
						 			<input type="hidden" class="free_cancel" type="text" value="0" data-free-cancel="0">
						 		<?php endif;?>
						 	<?php else:?>
						 		<input type="hidden" data-free-cancel="0" class="free_cancel" type="text" value="0">
							<?php endif;?>
						</div> 
						<div class="bothicntri">
						<div class="mwifdiv">
                           <ul class="htl_spr">                         
				         	<?php if(isset($hd['hotel_amenities'])):?>
				         		<?php if($hd['hotel_amenities']):?>
				         			<?php
				         				$in_search_params = "".strtolower('wireless')."";
										$in_input = preg_quote(@$in_search_params, '~'); // don't forget to quote input string!
										$internet_result = preg_grep('~' . $in_input . '~', $hd['hotel_amenities_name']);
										$inn_search_params = "Wi-Fi";
										$inn_input = preg_quote(@$inn_search_params, '~'); 
										$innternet_result = preg_grep('~' . $inn_input . '~', $hd['hotel_amenities_name']);

										//checking free wifi
										
										$wf_search_params = "Wi";
										$wf_input = preg_quote(@$wf_search_params, '~'); 
										$wf_result = preg_grep('~' . $wf_input . '~', $hd['hotel_amenities_name']);

										$b_search_params = "".strtolower('breakfast')."";
										$b_input = preg_quote(@$b_search_params, '~'); 
										$b_result = preg_grep('~' . $b_input . '~', $hd['hotel_amenities_name']);
										//checking breakfast 
										$bf_search_params = "Breakfast";
										$bf_input = preg_quote(@$bf_search_params, '~'); 
										$bf_result = preg_grep('~' . $bf_input . '~', $hd['hotel_amenities_name']);
										$p_search_params = "".strtolower('parking')."";
										$p_input = preg_quote(@$p_search_params, '~');										$p_result = preg_grep('~' . $p_input . '~', $hd['hotel_amenities_name']);
										//car parking
										$cp_search_params = "".strtolower('park')."";
										$cp_input = preg_quote(@$cp_search_params, '~'); 
										$cp_result = preg_grep('~' . $cp_input . '~', $hd['hotel_amenities_name']);

										$s_search_params = "pool";
										$s_input = preg_quote(@$s_search_params, '~'); 
										$s_result = preg_grep('~' . $s_input . '~', $hd['hotel_amenities_name']);
										$swim = "Swim";
								
										$sw_input = preg_quote(@$swim, '~'); 
										$sw_result = preg_grep('~' . $sw_input . '~', $hd['hotel_amenities_name']);
				         			?>
				         				<?php if($internet_result||$innternet_result|| $wf_result):?>
				         					<li class="wf" data-toggle="tooltip" data-placement="top" title="Wifi"><span>Wifi</span></li>
				         					<input type="hidden" value="filter" id="wifi" class="wifi" data-wifi="1">
				         				<?php else:?>
				         					<input type="hidden" value="filter" id="wifi" class="wifi" data-wifi="0">
				         				<?php endif;?>
				         				<?php if($b_result||$bf_result):?>
				         					<li class="bf" data-toggle="tooltip" data-placement="top" title="Breakfast"><span>Breakfast</span></li>
				         					<input type="hidden" value="filter" id="breakfast" class="breakfast" data-breakfast="1">
				         				<?php else:?>
				         					<input type="hidden" value="filter" id="breakfast" class="breakfast" data-breakfast="0">
				         				<?php endif;?>
				         				<?php if($p_result || $cp_result):?>
				         						 <li class="pr" data-toggle="tooltip" data-placement="top" title="Parking"><span>Parking</span></li>
		         						 		<input type="hidden" value="filter" id="parking" data-parking ="1" class="parking">
		         						<?php else:?>
		         								<input type="hidden" value="filter" id="parking" class="parking" data-parking="0">
				         				<?php endif;?>
				         				<?php if($s_result||$sw_result):?>
				         						 <li class="sf" data-toggle="tooltip" data-placement="top" title="Swimming pool"><span>Swimming pool</span></li>
				         						 <input type="hidden" value="filter" id="pool" class="pool" data-pool="1">
				         				<?php else:?>
				         					 <input type="hidden" value="filter" id="pool" class="pool" data-pool="0">
				         				<?php endif;?>			         			 
				         			<?php else:?>
				         				<input type="hidden" value="filter" id="wifi" class="wifi" data-wifi="0">
						         		<input type="hidden" value="filter" id="breakfast" class="breakfast" data-breakfast="0">
						         		<input type="hidden" value="filter" id="parking" class="parking" data-parking="0">
						         		<input type="hidden" value="filter" id="pool" class="pool" data-pool="0">
				         		<?php endif;?>
				         	<?php else:?>
				         		<input type="hidden" value="filter" id="wifi" class="wifi" data-wifi="0">
				         		<input type="hidden" value="filter" id="breakfast" class="breakfast" data-breakfast="0">
				         		<input type="hidden" value="filter" id="parking" class="parking" data-parking="0">
				         		<input type="hidden" value="filter" id="pool" class="pool" data-pool="0">
				         	<?php endif;?>
                           
                           </ul>
						</div>

						<?php if(isset($hd['trip_adv_url'])&&empty($hd['trip_adv_url'])==false):?>
						  <div class="tripad">
						    <a href="#"><img src="<?=$hd['trip_adv_url']?>"></a>
						    <span>Rating <?=$hd['trip_rating']?></span>
						  </div>
						<?php endif;?>
						 </div>
						</div>
						
						<div class="maprew">
						  <div class="hoteloctnf">
						  <a href="<?php echo base_url().'index.php/hotel/map?lat='.$hd['latitude'].'&lon='.$hd['longitude'].'&hn='.urlencode($hd['hotel_name']).'&sr='.intval($hd['star_rating']).'&c='.urlencode($hd['place']).'&price='.$hd['total_price'].'&img='.urlencode($hd['pic'])?>" class="location-map  fa fa-map-marker" target="map_box_frame" data-key="<?=$hd_key?>" data-hotel-code="
						  <?=$hd['hotel_code'].$hd['HotelCode']?>" data-star-rating="<?=$hd['star_rating']?>" data-hotel-name="<?=$hd['hotel_name']?>" id="location_<?=$hd['hotel_code'].$hd['HotelCode']?>_<?=$hd_key?>" data-toggle="tooltip" data-placement="top" data-original-title="View Map"></a>
						   </div>
						  
						</div>
					</div>
				</div>
			<?php
	/**
	 * HOTEL PRICE SECTION With Markup price will be returned
	 */
	$search_id = intval ( $attr ['search_id'] ); 
	// $RoomPrice = $hd ['total_price'];
	$RoomPrice = $hd ['total_price'] + $hd['admin_markup'] + $hd['agent_markup'];
	// debug($RoomPrice);exit;
	$no_of_nights  = $hd['request']['data']['no_of_nights'];
	$per_night_price = ($RoomPrice / $no_of_nights);
	?>
		<div class="celhtl width30">
			<div class="sidepricewrp">
				<div class="priceflights">
					<strong> <?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?> </strong>
					<span class="h-p"><?php echo roundoff_number($per_night_price); ?></span>
				<div class="prcstrtingt">Avg / Night</div>
				</div>
				<form action="<?php echo base_url().'index.php/hotels/hotel_details/'.($search_id)?>">
					<input type="hidden" id="mangrid_id_<?=$hd_key?>_<?=$hd['hotel_code'].$hd['HotelCode']?>" value="<?=urlencode($hd['unique_id'])?>" name="ResultIndex"  data-key="<?=$hd_key?>" data-hotel-code="<?=$hd['hotel_code'].$hd['HotelCode']?>" class="result-index">
					<input type="hidden" id="booking_source_<?=$hd_key?>_<?=$hd['hotel_code'].$hd['HotelCode']?>" value="<?=urlencode($hd['api'])?>" name="booking_source"  data-key="<?=$hd_key?>" data-hotel-code="<?=$hd['hotel_code'].$hd['HotelCode']?> class="booking_source">
					
					<input type="hidden" value="<?=$hd['HotelCode']?>" name="HotelCode" class="operation">
					<input type="hidden" value="<?=$hd['HotelCode']?>" name="hotel_id" class="operation">
					<input type="hidden" value="<?=$hd['TraceId']?>" name="TraceId" class="operation">

					<input type="hidden" value="get_details" name="op" class="operation">
					<button class="confirmBTN b-btn bookallbtn splhotltoy" type="submit">Book</button>
				</form>
				<div class="viewhotlrmtgle hide">
					<button class="vwrums room-btn" type="button">View Rooms</button>
				</div>
			</div>
		</div>
	</div>
	</div>
	
	</div>
</div>
<?php
}
?>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
});
</script>
<strong class="currency_symbol hide" > <?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?> </strong>
<script type="text/javascript">
	$(document).ready(function(){
		var default_loader = "<?=$GLOBALS['CI']->template->template_images('image_loader.gif')?>";
		//console.log("default_loader"+default_loader);
		//$(".load-image").attr('src',default_loader);

		setTimeout(function(){
			$(".load-image").removeClass('hide');
			$(".loader-image").addClass('hide');
			//loader-image
		},3000);

	});
</script>
