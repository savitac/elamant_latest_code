<script>

function showMore(obj) {
	var id = obj.id;
	var className = '.room-details-facilities-more' + id;
	var morelink = '.more-room-facilities' + id;
	if($(className).hasClass('hide')) {
		console.log(morelink);
		$(className).removeClass('hide');
		$(morelink).html('Show Less');
	}else {
		console.log(morelink);
		$(className).addClass('hide');
		$(morelink).html('Show More');
	}
}
</script>

<link href="<?php echo $GLOBALS['CI']->template->template_css_dir('custom_hotel.css') ?>" rel="stylesheet">
<!--<link href="--><?php //echo $GLOBALS['CI']->template->template_css_dir('custom_sky.css') ?><!--" rel="stylesheet">-->
<?php

$package_datepicker = array(array('date_of_travel', FUTURE_DATE_DISABLED_MONTH));
$GLOBALS['CI']->current_page->set_datepicker($package_datepicker);
?>
<?php
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('page_resource/hotel_result.css'), 'media' => 'screen');
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('page_resource/hotel_detail.css'), 'media' => 'screen');
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('page_resource/animation.css'), 'media' => 'screen');
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('owl.carousel.min.css'), 'media' => 'screen');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/flight_session_expiry_script.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('owl.carousel.min.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('custom_1.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => '//code.jquery.com/ui/1.11.4/jquery-ui.js', 'defer' => 'defer');

$sanitized_data ['Images'] = $hotel_details ['hotel_images'];

$pscheckin = date('d M Y',strtotime($hotel_search_params['from_date']));
$pscheckout = date('d M Y',strtotime($hotel_search_params['to_date']));
$template_images	= $GLOBALS['CI']->template->template_images();
// debug($template_images);exit;
// debug($country_code);exit;
//******* Search Params *******//
$search_id			= $hotel_search_params['search_id'];
$number_of_nigths	= $hotel_search_params['no_of_nights'];
$number_of_rooms	= $hotel_search_params['room_count'];
$number_of_adults	= array_sum($hotel_search_params['adult_config']);
$number_of_childs	= array_sum($hotel_search_params['child_config']);
//******* Dynamic Data *******//

// debug($hotel_details); exit;
$hotel_id                  = $hotel_details['hotel_details_id'];
$hotel_code				= $hotel_details['hotel_code'];
$hotel_name				= $hotel_details['hotel_name'];
$star_rating			= $hotel_details['star_rating'];
$destination			= @$hotel_details['destination'];
$check_in				= $hotel_details['from_date'];
$check_out				= $hotel_details['to_date'];
$latitude				= $hotel_details['latitude'];
$longitude				= $hotel_details['longitude'];
// $min_rate				= $rooms_lists[0]['total_price'];
$min_rate				= ($rooms_lists[0]['total_price'] + $rooms_lists[0]['admin_markup']);
$minRate 				= @$hotel_details['minRate'];
$currency				= $currency_obj->get_currency_symbol($currency_obj->to_currency);
$room_list				= $rooms_lists;
$print_format_room_list	= @$rooms_lists;
$terminal				= @$hotel_details['hotel_distance'];
//******* Static Data *******//FIXME: Get Hotel Static Data from Local Database
$hotel_image			= explode(",",$hotel_details['hotel_images']);

$img_url 				= 'http://' . $_SERVER ['HTTP_HOST'].APP_ROOT_DIR.'/supervision/';
$mediumImage_baseUrl	= 'uploads/hotel_images/';
$image_url =$img_url.$mediumImage_baseUrl;


$hotelContact			= explode(",",$hotel_details['phone_number']);
$hotel_address			= @$hotel_details['hotel_address'];
$acc_type				= @$hotel_details['accomodation_type_code'];
$postal					= @$hotel_detail['zone_code'];
$hotel_ameneties		= '';
$contact_details		= @$hotel_details['hotel_static_contact_num'];
$hotel_description		= @$hotel_details['hotel_info'].@$hotel_details['description'];
$hotel_facilities		= @$hotel_details['hotel_static_facilities_arr'];
$hotel_policies			= '';
$hotelEmail				= @$hotel_details['email'];
$medium_image_baseUrl = $hotel_details['medium_image_baseUrl'];
//******* cached Token Details *******//
$Token					= @$hotel_details['Token'];
$TokenKey				= @$hotel_details['TokenKey'];


$trip_rating = 0;
$trip_message = 'Terrible';

if($acc_type == 'APTHOTEL'){ 
		$acc_type = "HOTEL APARTMENT";
	}else if($acc_type == 'APART'){
		$acc_type = "APARTMENT";
	}else{
		$acc_type = $acc_type;
	}

// trip Advisor
$trip_result = $this->hotels_model->get_trip_adv_not_exists();

if(($trip_result['status'] == 1)){
	$trip = json_decode($trip_result['result']['tri_adv_hotel'],true);
	$trip_location_id = $trip['location_id'];
	$trip_rate = $trip['rating_image_url'];
	$trip_web_url = $trip['web_url'];
	$trip_rating = $trip['rating'];
	$trip_num_reviews = $trip['num_reviews'];
}

switch (round($trip_rating)) {
	case 1:
		$trip_message = 'Terrible';
		break;
	case 2:
		$trip_message = 'Poor';
		break;
	case 3:
		$trip_message = 'Average';
		break;
	case 4:
		$trip_message = 'Very Good';
		break;
	case 5:
		$trip_message = 'Excellent';
		break;
	default:
		$trip_message = 'Terrible';
		break;
}

?>

<script type="text/javascript">
	/*
		session time out variables defined
	*/
	var  search_session_expiry = "<?php echo $GLOBALS ['CI']->config->item ( 'flight_search_session_expiry_period' ); ?>";
	var search_session_alert_expiry = "<?php echo $GLOBALS ['CI']->config->item ( 'flight_search_session_expiry_alert_period' ); ?>";
	var search_hash = "<?php echo $session_expiry_details['search_hash']; ?>";
	var start_time = "<?php echo $session_expiry_details['session_start_time']; ?>";
	var session_time_out_function_call = 1; 
</script>

<style type="text/css">
	.hotel_fac li { margin:0px; }
</style>
<style type="text/css">

    #alertp{background-color: transparent !important;color: #3c763d !important;padding: 10px 10px;font-size: 13px;}
    .caption {
        background-color: rgba(0, 0, 0, 0.75);
        height: auto;
        position: absolute;
        bottom: 0;
        left: 0;
        padding: 10px;
        color: white;
        width: 100%;
    }

    #or {
        position: relative;
        width: 270px;
        height: 50px;
        line-height: 50px;
        text-align: center;
    }

    #or::before,
    #or::after {
        position: absolute;
        width: 130px;
        height: 1px;

        top: 24px;

        background-color: #aaa;

        content: '';
    }

    #or::before {
        left: 0;
    }

    #or::after {
        right: 0;
    }

.outerfullfuture{ background: #fff none repeat scroll 0 0;display: block;margin: 0px;box-shadow: 0 0 0 0;}

.ourdest{display: block;min-height: 298px;max-height: 298px;max-width: 100%;overflow: hidden;position: relative;}
.ourdest img{width: 100%;min-height: 298px;max-height: 298px;}
.destplace{bottom:0px;color:#fff;font-size:14px;left:0;padding:5px 15px;position:absolute;right:0;z-index:1;background:rgba(0, 0, 0, 0.7) none repeat scroll 0 0;}
.cntdes1{color: #fff;font-weight: 200;padding: 1px 0;font-size: 15px;}
.price_ht strong{color: #06b5f1;font-weight: 400;}
.star{color:#ff9800;}
.surbtm1{float:left;}
.destbtm2{float: right;width: 30%;margin: 15px 10px;}
.bk_btn{
	background: #fdb912 none repeat scroll 0 0;
    border: 1px solid #fdb912;
    color: #000;
    display: block;
    font-size: 15px;
    overflow: hidden;
    padding: 7px 0;
    text-align: center;
    width: 100%;
    border-radius: 10px;text-transform: uppercase;}
.tooltip { 
	width: auto !important;
 float: left; background: none !important; 
 border-radius: 3px;
} 
.tooltip.left {
 padding: 0px !important;
  }
.tooltip-inner{padding: 2px 7px !important;background: #333 !important;max-width: 100% !important;}
.tooltip-inner .table {margin-bottom: 0px !important;background: #333 !important;}
.tooltip.left .tooltip-arrow {right: -5px !important;border-left-color: #333;}
.tooltip.in {opacity: 1 !important;}
.str_ratng, .detail_htlname {float: left;}		
.str_ratng {line-height: 31px;}
    .fulldetab.mart20 .nav-tabs.trul li a span { display: block; margin:0 0 10px; height: 20px; }
    .fulldetab.mart20 .nav-tabs.trul li a span { font-size: 20px; color: #000; }

    .caption p{ margin: 0px; }
</style>

<!-- Header Carousel -->
<div class="fldealsec">
  <div class="container">
    <div class="tabcontnue">
      <div class="breadli nopadding">
        <div class="rondsts success">
          <a class="taba core_review_tab">
            <!-- <div class="iconstatus fa fa-list-alt"></div> -->
            <div class="iconstatus"><i class="fal fa-list"></i></div>
            <div class="stausline"><?php echo $this->lang->line('hotel_search_results');?></div>
            
          </a>
        </div>
      </div>

      <div class="breadli nopadding">
        <div class="rondsts active">
          <a class="taba core_review_tab">
            <!-- <div class="iconstatus fa fa-list-alt"></div> -->
            <div class="iconstatus"><i class="fal fa-bed"></i></div>
            <div class="stausline"><?php echo $this->lang->line('hotel_select_rooms');?></div>
            
          </a>
        </div>
      </div>

      <div class="breadli nopadding">
        <div class="rondsts">
          <a class="taba core_review_tab">
            <!-- <div class="iconstatus fa fa-list-alt"></div> -->
            <div class="iconstatus"><i class="fal fa-users"></i></div>
            <div class="stausline"><?php echo $this->lang->line('hotel_guest_details');?></div>
            
          </a>
        </div>
      </div>

      <div class="breadli nopadding">
        <div class="rondsts">
          <a class="taba core_travellers_tab">
            <!-- <div class="iconstatus fa fa-money"></div> -->
            <div class="iconstatus"><i class="fal fa-credit-card-front"></i></div>
            <div class="stausline"><?php echo $this->lang->line('hotel_payment_method');?></div>
            
          </a>
        </div>
      </div>
      <div class="breadli nopadding">
        <div class="rondsts">
          <a class="taba">
            <!-- <div class="iconstatus fa fa-check"></div> -->
            <div class="iconstatus"><i class="fal fa-check"></i></div>
            <div class="stausline"><?php echo $this->lang->line('hotel_confirmation');?></div>
            
          </a>
        </div>
      </div>
    </div>
  </div>
</div> 

<div class="allpagewrp top80">
	<div class="newmodify">
		<div class="container">
			<div class="contentsdw">
				<div class="col-md-5 col-sm-8 col-xs-9 nopad">
					<div class="detail_htlname"><?=$hotel_name?> <?php echo ucfirst(strtolower($acc_type)); ?></div>
					<?php //if(!empty($acc_type == 'HOTEL')){?>
					<div class="star_detail">
						<div class="stra_hotel" data-star="<?=intval($star_rating)?>">
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div>
					</div>
					<?php //} else{ ?>
						<!-- <div class="str_ratng"><?php echo $star_rating; ?></div> -->
					<?php// } ?>
					<div class="clearfix"></div>
					<div class="detal_htladrs"><?=strtolower($hotel_address)?></div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-3 nopad">
					<!--
						<a class="view_map_dets">
						   	<span class="fa fa-map-marker"></span>
						       View map
						   </a>-->
					<a class="view_map_dets location-map " href="<?php echo base_url().'index.php/hotels/map?lat='.$latitude.'&lon='.$longitude.'&hn='.urlencode($hotel_name).'&sr='.intval($star_rating).'&c='.urlencode($destination).'&img='.$image_url.urlencode($hotel_details['thumb_image'])	?>" target="map_box_frame">
					<strong><?php echo $this->lang->line('hotel_view_map');?></strong>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="dets_section">
	<div class="container">
		<div class="col-md-8 col-xs-12 nopad">
			<div class="leftslider">
				<div id="sync1" class="owl-carousel detowl">
					<?php

						if(valid_array($sanitized_data) == true) {
							$hotel_imgs_count = 0;
							$hotel_image =  explode(',', $sanitized_data ['Images']);
							foreach($hotel_image as $hmKey => $img) { ?>
									<div class="bighotl bigimg">
			                        <img src="<?= $image_url . $img;?>" onerror="javascript:this.src='<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>'" alt="" />
			                        </div>

						<?php 	}
						}else { ?>

							?><img src="<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>"/>
						<?php 
						}
						?>
				</div>
				<div id="sync2" class="owl-carousel syncslide" style="margin-top: 10px;">
					<?php 
						if(valid_array($sanitized_data) == true) {
							$hotel_imgs_count = 0;
							$hotel_image =  explode(',', $sanitized_data ['Images']);
							foreach($hotel_image as $hmKey => $image) {
								?>
								<div class="item" style="padding-right:5px;">
									<div class="bighotl"><img src="<?=$image_url . $image;?>" height="60" onerror="javascript:this.src='<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>'"  /></div>
								</div>
							<?php }
						}else{ ?>	
							<img src="<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>"/>
						<?php }
					?>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 nopad">
			<div class="inside_detsy">
				<div class="dets_hotel">
					<div class="col-md-12 col-sm-12 col-xs-12 nopad">
					<span class="hotel_address elipsetool"><?php echo $this->lang->line('check_in');?>: <?= $pscheckin;?></span>
			    </div>
			    
			    <div class="col-md-12 col-sm-12 col-xs-12 nopad">		
					<span class="hotel_address elipsetool"><?php echo $this->lang->line('check_out');?>: <?= $pscheckout;?></span>
			    </div>	

			    <div class="col-md-12 col-sm-12 col-xs-12 nopadding_left">		
					<span class="hotel_address elipsetool"><?php echo $this->lang->line('hotel_adults');?>: <?= array_sum($hotel_search_params['adult_config']);?></span>
					<?php if(array_sum($hotel_search_params['child_config']) > 0){?>
					<span class="hotel_address elipsetool">No. of Children : <?= array_sum($hotel_search_params['child_config']);?></span>
					<?php 
					$i = 1; 

					foreach($hotel_search_params['child_age'] as $age){ ?>
					<?php if($age > 0){ ?>
					<span class="hotel_address elipsetool"><?php echo $this->lang->line('hotel_child');?> <?php echo $i; ?> : <?php echo $age; ?> Yrs </span>
					<?php $i++; } ?>
			    	<?php  } } ?>
			    </div>	
			   <div class="clearfix"></div> 
					<div class="side_amnties">
							<ul class="hotel_fac">
					<?php if(isset($hotel_details['hotel_amenities'])):?> 
			   <div class="side_amnties">
                 	<ul class="hotel_fac">
                 		<?php
                 			//$hotel_details['hotel_amenities_name'] = explode(',',$hotel_details['hotel_amenities']);
                 			//debug($hotel_details['hotel_amenities_name']);exit; 
                 			foreach($hotel_details['hotel_amenities_name'] as $h_akey=>$h_avalue):?>
                 			<li class="<?= $h_avalue['icon'].' tooltipv h-f' ?>" data-toggle="tooltip" title="<?=$h_avalue['amenities_name']?>"></li>
                 		<?php endforeach;?>
                 	</ul>
                </div>
            <?php endif;?>
					<a style="line-height: 30px;" href="#facility" data-toggle="tab" onclick="facilitypsw()"><?php echo $this->lang->line('hotel_see_more');?></a>
							</ul>
					</div>
				</div>
				<div class="clearfix"></div>

				<!-- <div class="trp_adv">
				<h3>Exceptional 5/5</h3>
				<img class="blk" style="margin-right: 10px; margin-top: 10px;" src="<?=$GLOBALS['CI']->template->template_images('trp_adnisor.png'); ?>">
				<p class="rvw">12 Reviews</p>
				</div> -->

				<?php 
				// debug($trip_result);exit;
				if($trip_result['status']){ 

					// echo "fdgggggggggggg";
					?>
				<!-- <div class="superb col-xs-4"> -->
				<!-- <div class="reviews-box resp-module"> -->
				   <!-- <div class="cont"> -->
				      <div class="guest-reviews " style="display: block;">
				         <div class="guest-reviews-badge guest-rating-excellent"><?php echo $trip_message; ?> <span class="guest-rating-value"><strong><?= $trip_rating;?> / 5</strong></span></div>
				        <div class="guest-reviews-link"><a href="<?php echo $trip_web_url; ?>" target="_blank"><span class="small-view"><?= $trip_num_reviews;?> <?php echo $this->lang->line('hotel_reviews');?></span><span class="full-view" hidden="hidden"><?= $trip_num_reviews;?> www.tripadvisor.com guest reviews</span></a></div> 
				      </div>
				      <div class="ta-reviews" style="display: block;">
				         <div class="logo-wrap">
				         <img class="blk" style="margin-right: 10px; margin-top: 10px;" src="<?=base_url()?>extras/system/template_list/template_v1/images/trip_advisor/<?php echo $trip_rating?>-37331-5.svg"></div>
				         <a href="<?php echo $trip_web_url; ?>" target="_blank">
				         <div class="text-wrap"><span class="ta-total-reviews"><?= $trip_num_reviews;?> <?php echo $this->lang->line('hotel_reviews');?></span></div>
				         </a>
				         <!-- <img class="ta-tracking-pixel" src="<?=$trip_rate; ?>" alt="">  -->
				      </div>
				   <!-- </div> -->
				<!-- </div> -->
				<!-- </div> -->
				<?php 
					}
				?>

				<div class="clearfix"></div>
				<div class="price_strts">
					<div class="price_froms"><?php echo $this->lang->line('hotel_start_from');?><strong><?=$currency?> <?=$min_rate?></strong></div>
				</div>
				<div class="clearfix"></div>
				<a class="room_select" href="#"><?php echo $this->lang->line('hotel_select_rooms');?></a>
			</div>
		</div>
		
	</div>
</div>
<div class="clearfix"></div>
<div class="down_hotel">
	<div class="container">
		<div class="col-md-12 col-xs-12 nopad">
			<div class="shdoww">
				<div class="hotel_detailtab">
					<ul class="nav nav-tabs htl_bigtab">
						<li class="" id='facilitypsh'>
							<a href="#htldets" data-toggle="tab"><span class="fal fa-info-circle"></span><?php echo $this->lang->line('hotel_details');?></a>
						</li>
						<li class="active" id='facilitypsr'>
							<a href="#rooms" data-toggle="tab" id="room-list"><span class="fal fa-building"></span><?php echo $this->lang->line('hotel_rooms');?></a>
						</li>
						<li id='facilitypsw'>
							<a href="#facility" data-toggle="tab"><span class="fal fa-check-square"></span><?php echo $this->lang->line('hotel_amenities');?></a>
						</li>

						<!-- <li id='facilitypost'>
							<a href="#post" data-toggle="tab"><span class="fal fa-comment"></span><?php echo $this->lang->line('hotel_review');?></a>
						</li> -->
<!--						<li>-->
<!--							<a href="#policy" data-toggle="tab"><span class="fa fa-list-alt"></span>Hotel Policy</a>-->
<!--						</li>-->
					</ul>
					<div class="clearfix"></div>
					<div class="tab-content clearfix">

						<!-- Hotel Detail-->
						<div class="tab-pane" id="htldets">
							<div class="innertabs">
                            <div class="dets_hotels">
                            <strong><?=$hotel_name?></strong>
								<div class="comenhtlsum">  <?php echo $hotel_description;?></div>
							</div>
                            </div>
						</div>
						<!-- Hotel Detail End--> 
						<div class="clearfix"></div>
						<!-- Rooms Summry-->
						<div class="tab-pane active" id="rooms">
							<div class="innertabs">
								<?php
								$rm_cnt = 0;
								//debug($print_format_room_list);
								$room_info = $print_format_room_list;
								//$room_info_count = count($room_list);
								$room_rate = $base_fare = 0;
								foreach($room_info as $rl => $room_list) {
									//debug($room_list['cancel_policy']);
									/*$total_adult = $rl_v['max_adults'];
									$total_child = $rl_v['max_child'];*/
									$room_desc = $room_list['room_description'];
									$room_code = $room_list['hotel_room_type_id'];

									$base_fare = $room_list['total_price'];
									$admin_markup = $room_list['admin_markup'];
									// echo $admin_markup;exit;
									$tax_fare = @$room_list['tax_fare'];
									$dis_fare = @$room_list['discount_fare'];								
									$room_rate = $room_list['total_price'];
									$total_price = (($base_fare + $tax_fare + $admin_markup) - $dis_fare);
									$booking_code = @$room_list['room_rate']['booking_code'];

									//debug($room_list);exit;
								?>

								<div class="htl_rumrow">
								   <div class="hotel_list">
								      <div class="col-md-9 col-sm-9 col-xs-8 fullfives nopad">
								         <div class="col-md-12 col-xs-12 nopad">
								            <!--<div class="col-md-2 nopad">--><!--<div class="imagehotel hotel_image"><img src="" alt="" /></div>--><!--</div>-->
								            <div class="col-md-9 col-sm-9 col-xs-7 nopad">
								               <div class="in_center_htl">
								                  <div class="hotel_hed"><?php echo $room_list['room_type_name']; ?></div>
								                  <div class="hotel_sub_hed"><?php $room_includes = @$room_list['room_rate']['inclusions']; 
								                  //debug($room_includes);
								                 
								                  if(@$room_list['room_amenity']['amenities']['name'][$rl]) {
								                  	echo '<span>Inclusion In the Room</span>';	
								                  }
								                  
								                  /*echo '<ul>';
								                  if((count($room_includes) > 0) && (valid_array($room_includes)))
								                  {
								                  	for($inc=0; $inc < count($room_includes); $inc++)
								                  	{
								                  		echo '<li><span class="fa fa-check-square-o"></span>'.$room_includes[$inc].'</li>';		
								                  	}									                  	
								                  }
								                  else
								                  {
								                  	if($room_includes != '')
								                  	{
								                  		echo '<li><span class="fa fa-check-square-o"></span>'.$room_includes.'</li>';
								                  	}
								                  	else
								                  	{
								                  		echo '<li>Not Available</li>';	
								                  	}								                  	
								                  }
								                  echo '</ul>';*/

								                   ?></div>
								                  <!--<div class="mensionspl"> Extra Bed Available : <span class="menlbl">Crib</span> </div> 
								                     -->
								                     <!-- <?php if($room_list['room_amenity']): ?>

								                     <div class="side_amnties">
									                 	<ul class="hotel_fac">
									                 		<?php foreach($room_list['room_amenity'] as $a_key=>$a_value):?>

									                 		<li class="<?=$a_value['icon']?>" data-toggle="tooltip" title="<?=$a_value['amenities_name']?>"></li>

									                 	<?php endforeach;?>
									                 	</ul>
									                </div>
									            <?php endif;?> -->
									            	<?php if(@$room_list['room_amenity']):?>
								                     <div class="hotel_sub_hed">
								                     	<a class="all_amints" data-toggle="collapse" data-target="#demo_<?=$rl?>">Other Amenities</a>
								                     	<div id="demo_<?=$rl?>" class="col-md-12 nopad shw_aminities collapse">
								                     		<ul>
								                     		<?php foreach($room_list['room_amenity'] as $o_key=>$o_value):?>
							                     			
									                     		<li><span class="fa fa-check-square-o"></span><?=$o_value['amenities_name']?></li>               
									                     	<?php endforeach; ?>
									                     	</ul> 	
								                     	</div>
								                     	
								                     </div>
								                    <?php endif;?>
								                  <div class="mensionspl"> </div>
								                  <div class="clearfix"></div>
								                  <div class="morerumdesc"><a class="morerombtn" data-toggle="modal" data-target="#cancellation_policy<?=$rl?>"><span class="fa fa-ban"></span>Cancellation Policy</a>
								                  </div>
								                  <div class="morerumdesc prom"></div>
								                  <div class="morerumdesc"></div>
								                  <div class="ht_refnd hide"><span class="fa fa-ticket fldetail"></span> </div>
								               </div>
								            </div>
								            <div class="col-md-3 col-sm-3 col-xs-3 nopad hide">
								               <div class="pers hide">
								                  <h5 class="htl_rm">Rooms  : <?php echo $number_of_rooms; ?></h5>
								                  <?php 
								                  //debug($hotel_details['formated_room_list']);
								                  $adult_cnt = $child_cnt = 0;
								                  for($frl=0; $frl < count($room_list); $frl++)
								                  {
								                  	if($hotel_details['formated_room_list'][$frl]['room_rate']['max_adults'] != '')
								                  	{
								                  		$adult_cnt = $hotel_details['formated_room_list'][$frl]['room_rate']['max_adults'];	
								                  	}
								                  	if($hotel_details['formated_room_list'][$frl]['room_rate']['max_child'] != '')
								                  	{
								                  		$child_cnt = $hotel_details['formated_room_list'][$frl]['room_rate']['max_child'];	
								                  	}
								                  }
								                  
								                 ?>
								                 <i class="fa fa-male"></i>
								                 <?php echo $adult_cnt;  ?>
								                 <i class="fa fa-child"></i>
								                 <?php echo $child_cnt; ?>
								               </div>
								            </div>
								            <div class="col-md-3 col-sm-3 col-xs-3 nopad">
								               <div class="sideprice"><?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?> <?php echo number_format($total_price,2); ?> <span class="avgper">Total</span> </div>
								            </div>
								         </div>
								         <!--Cancellation details Modal Starts-->
								         <div id="cancellation_policy<?=$rl?>" class="modal fade" role="dialog">
								            <div class="modal-dialog">
								               <div class="modal-content">
								                  <div class="modal-header">
								                     <button type="button" class="close" data-dismiss="modal">×</button>
								                     <h4 class="modal-title">Cancellation Policy</h4>
								                  </div>
								                  <div class="modal-body">
								                     <ul class="list_popup">
								                        <li class="listcancel">
								                        	<?php 
								                        		$cancel_policy = '';
								                        		if(isset($cancellation_details) && !empty($cancellation_details[0]->cancellation_description)){
																	$cancel_policy = $cancellation_details[0]->cancellation_description;
																}else{
																	$cancel_policy = 'This booking is non-refundable.';
																}
								                        		
								                        	?>
								                        	<?php echo $cancel_policy;?>
								                        </li>
								                     </ul>
								                  </div>
								                  <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
								               </div>
								            </div>
								         </div>
								         <!--Cancellation details Modal Ends-->
								      </div>
								      <div class="col-md-3 col-sm-3 col-xs-4 fullfives">
                                          <div class="col-md-12 col-xs-12 nopad bordrit hide">
                                              <button type="button" data-toggle="modal" data-target="#myModal" id="enquiry_pop_button" class="btn btn-default booknow " >Send Enquiry</button>
                                          </div>
                                          <div class="col-md-12 col-xs-12 nopad bordrit hide">
                                          <div class="text-center nopad bordrit" id="or">OR</div>
                                          </div>
								         <div class="col-md-12 col-xs-12 nopad bordrit">
								            <div class="pricesec">
								               <div class="bookbtn">
								               <?php
								               	$booking_url = $GLOBALS['CI']->hotel_lib->booking_url($hotel_search_params['search_id']);
								               	//debug($hotel_search_params);
								               ?>
								                  <form action="<?php echo 	$booking_url; ?>"   id="room_details_form" method="post">
								                     <div class="">
								                     <input type="hidden" name="HotelCode"      value="<?= $room_list['hotel_details_id'] ?>">
								                     <input type="hidden" name="ResultIndex"    value="<?= @$params['ResultIndex'] ?>">
								                     <input type="hidden" name="booking_source" value="<?= $active_booking_source; ?>">
								                     <input type="hidden" name="search_id"		value="<?= $search_id ?>">
								                     <input type="hidden" name="TraceId"        value="<?= @$params['TraceId'] ?>">
								                     <input type="hidden" name="op" value="block_room">
								                     <input type="hidden" name="HotelName"		value="<?= $hotel_details['hotel_name'] ?>" >
								                     <input type="hidden" name="StarRating"		value="<?= $hotel_details['star_rating'] ?>">
								                     <input type="hidden" name="HotelImage"		value="<?= $hotel_details['thumb_image'] ?>">
								                     <input type="hidden" name="HotelAddress"	value="<?= $hotel_details['hotel_address'] ?>">
								                     <input type="hidden" name="room_id"	    value ="<?= $room_list['hotel_room_type_id'] ?>">
								                     <input type="hidden" name="season_id"	    value ="<?= $room_list['seasons_details_id'] ?>">
								                     <input type="hidden" name="room_type_name"	value ="<?= $room_list['room_type_name'] ?>">
								                     <input type="hidden" name="price"	        value ="<?= get_converted_currency_value ( $currency_obj->force_currency_conversion ( $room_list['total_price']) ) ?>">
								                     <input type="hidden" name="admin_markup"	        value ="<?= $room_list['admin_markup'] ?>">
								                     <input type="hidden" name="CancellationPolicy[]"	value ="<?= @$room_list['price_data']['room_rate_cancellation_policy'] ?>">

								                     <input type="hidden" name="token" value="<?php echo @$hotel_details['Token']; ?>">
								                     <!-- <input type="hidden" name="token_key" value="<?php echo $hotel_details['TokenKey']; ?>"> -->
								                    <!--  <input type="hidden" name="rateKey[]" value="<?php echo $booking_code; ?>"> -->
								                    <!--  <input type="hidden" name="rooms[]" value="<?php echo $hotel_search_params['room_count']; ?>"> -->
								                     <input type="hidden" name="adults[]" value="<?php echo implode(',',$hotel_search_params['adult_config']); ?>">
								                     <input type="hidden" name="childs[]" value="<?php echo implode(',',$hotel_search_params['child_config']); ?>"></div>

								                     <?php if($room_list['cancel_policy']):?>
								                     	<input type="hidden" name="cancel_policy[]" value="<?php echo $room_list['cancel_policy']; ?>">
								                     <?php else:?>
														<input type="hidden" name="cancel_policy[]" value="This booking is non-refundable">
								                     <?php endif;?>
								                     <input type="hidden" name="hotel_policy" value="<?php echo $hotel_details['hotel_notice']?>">
								                     <input type="submit" class="booknow" value="Book">
								                  </form>
								               </div>
								            </div>
								         </div>
								         <div class="col-md-12 col-xs-12 nopad bordrit hide">
								            <div class="col-md-12 col-xs-12 nopad">
								               <div class="col-md-12 col-xs-12 sideprice"> <?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?> <?php echo number_format($room_rate,2); ?> <span class="avgper">Total</span> </div>
								            </div>
								         </div>
								      </div>
								   </div>
								</div>
								<?php } ?>
							</div>
						</div>

						<!-- Rooms End--> 
						<!-- Facilities-->
						<div class="tab-pane" id="facility">
							<div class="innertabs">
                            <div class="dets_hotels">
								<div class="col-xs-12 nopad">
									<ul class="checklist">
										<?php 
											//debug($room_list['room_amenity']);exit;
											if(valid_array($room_list['room_amenity']) == true) {
												$amen_cat = count($room_list['room_amenity']);
												for($ame=0; $ame < $amen_cat; $ame++) {
													if($room_list['room_amenity'][$ame] != '')
													{
													?>				
													<li> <span class="fa fa-check-square-o"></span>
													<strong><?php echo $room_list['room_amenity'][$ame]['amenities_name'];?></strong>
													<!-- <ul>
														<?php 
														if($hotel_details['hotel_amenities'][$ame] !=''){

															if(valid_array($hotel_details['amenities']['value'][$ame])){
															$amen_val = count($hotel_details['amenities']['value'][$ame]);
															for($s_ame=0; $s_ame < $amen_val; $s_ame++) {  ?>
															<li> <span class=""></span><?php echo $hotel_details['amenities']['value'][$ame][$s_ame];?></li>
													<?php } }else{ echo "<li><span class=''></span>".$hotel_details['amenities']['value'][$ame]."</li>";} } ?>
													</ul> -->
													</li>
										<?php	
													}													
												}
											}
											?>
										<?php 
											//debug($hotel_amenities_name);exit;
											if(isset($hotel_amenities_name[0]) == true) {
												$amen_cat = count($hotel_amenities_name);
												for($ame=0; $ame < $amen_cat; $ame++) {
													//debug($hotel_amenities_name[$ame]['amenities_name']);
													if($hotel_amenities_name[$ame]['amenities_name'] != '')
													{
													?>				
													<li> <span class="fa fa-check-square-o"></span>
													<strong><?php echo $hotel_amenities_name[$ame]['amenities_name'];?></strong>
													<!-- <ul>
														<?php 
														if($hotel_details['hotel_amenities'][$ame] !=''){

															if(valid_array($hotel_details['amenities']['value'][$ame])){
															$amen_val = count($hotel_details['amenities']['value'][$ame]);
															for($s_ame=0; $s_ame < $amen_val; $s_ame++) {  ?>
															<li> <span class=""></span><?php echo $hotel_details['amenities']['value'][$ame][$s_ame];?></li>
													<?php } }else{ echo "<li><span class=''></span>".$hotel_details['amenities']['value'][$ame]."</li>";} } ?>
													</ul> -->
													</li>
										<?php	
													}													
												}
											}
											?>
									</ul>
								</div>
								<p class="mar_t20"><?php echo $this->lang->line('hotel_facility_message1');?> <strong class="text-danger">*</strong> <?php echo $this->lang->line('hotel_facility_message2');?></p>
							</div>
                            </div>
						</div>

						<div class="tab-pane hide" id="post">
							<div class="innertabs">
	                            <div class="dets_hotels">
									<?php echo $this->template->isolated_view('hotel/post_review'); ?>
								</div>
                            </div>
						</div>

						<!-- Facilities End--> 
						<!-- Reviews-->
<!--						<div class="tab-pane" id="policy">-->
<!--							<div class="innertabs">-->
<!--								<ul class="list_detail_tab">-->
<!--									<li class="listcancel">-->
<!--										Lorem Ipsum is simply dummy text of the printing and typesetting industry.-->
<!--									</li>-->
<!--									<li class="listcancel">-->
<!--										Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.-->
<!--									</li>-->
<!--									<li class="listcancel">-->
<!--										It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.-->
<!--									</li>-->
<!--								</ul>-->
<!--							</div>-->
<!--						</div>-->
						<!-- Reviews End--> 
					</div>
				</div>
			</div>
		</div>



		
	</div>
</div>



<!-- Page Content -->
<!-- Hotel Location Map Starts -->
<div class="modal fade bs-example-modal-lg" id="map-box-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('hotel_location_map');?></h4>
			</div>
			<div class="modal-body">
				<iframe src="" id="map-box-frame" name="map_box_frame" style="height: 500px;width: 850px;">
				</iframe>
			</div>
		</div>
	</div>
</div>
<!-- Hotel Location Map Ends -->

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background: #16acdf !important;
    border: 1px solid #1397c4;
    color: #ffffff !important;
    line-height: 20px;
    font-size: 15px;
    text-align: center;">

                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color: #fff;">Hotel Enquiry</h4>

            </div>
            <div id="alertp" class="hide text-success"><img style="text-align: center; margin: 10px auto; display: block;" src="<?=$GLOBALS['CI']->template->template_images('tick.png'); ?>"><div class="confirm-span" style="text-align: center;">Your request has been submitted successfully!</div></div>

            <form id="enquiry_form" action="javascript:void(0);">
                <div class="modal-body">
                    <h3 class="gateway" style="text-align: center;"><?=$hotel_name?></h3>
                    <div class="form-group modl">
                        <label class="control-label col-md-4 col-xs-4" for="user_name">Title : <strong class="text-danger"></strong></label>
                        <div class="col-md-7 col-xs-8">
                            <i style="visibility: hidden;" class="fa fa-user" aria-hidden="true"></i>
                            <select class="form-control mntxt" name="title" id="title" aria-required="true" required="required">
                                <?=generate_options(get_enum_list('title'),(array)$this->entity_title)?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group modl"><label class="control-label col-md-4 col-xs-4" for="user_name">First Name : <strong class="text-danger"></strong></label><div class="col-md-7 col-xs-8"><i style="visibility: hidden;" class="fa fa-user" aria-hidden="true"></i><input type="text" class="form-control mntxt" name="ename" id="ename" placeholder="" aria-required="true" required="required" value="<?=$this->entity_first_name?>" style="text-transform: capitalize !important;"></div></div>
                    <div class="form-group modl"><label class="control-label col-md-4 col-xs-4" for="user_name">Last Name : <strong class="text-danger"></strong></label><div class="col-md-7 col-xs-8"><i style="visibility: hidden;" class="fa fa-user" aria-hidden="true"></i><input type="text" class="form-control mntxt" name="elname" id="elname" placeholder="" aria-required="true" required="required" value="<?=$this->entity_last_name?>" style="text-transform: capitalize !important;"></div></div>

                    <div class="form-group modl contct">
                        <label class="control-label col-md-4 col-xs-4" for="user_mobile"> Country Code  :</label>
                        <div class="col-md-7 col-xs-8">
                            <?php
                            if(isset($country_code) && valid_array($country_code))
                            {
                                ?>
                                <div class="selectedwrap"><select name="pn_country_code" id="pn_country_code" required="" aria-required="true" class="mySelectBoxClass flyinputsnor">
                                     
                                        <?php foreach($country_code as $c__k => $__country) {
                                            ?><option value="<?=trim(@$c__k."_".$__country)?>" <?php 
                                            if($c__k == $this->entity_phone_code) {echo 'selected';}

                                           else if($c__k == '+61') {echo 'selected';} ?>><?=@$__country?></option><?php
                                        }?>

                                           <option value="+1_Canada +1">Canada +1</option>
                                       <!--  <option value="+1_United States +1">United States +1</option> -->


                                    </select></div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group modl contct">
                        <label class="control-label col-md-4 col-xs-4" for="user_mobile"> Phone Number  :</label>
                        <div class="col-md-7 col-xs-8">
                            <i style="font-size: 25px;" class="fa fa-mobile" aria-hidden="true"></i>
                            <input type="text" class="form-control mntxt mobile" name="emobile" id="emobile" placeholder="" value="<?=$this->entity_phone?>">
                        </div>
                    </div>

                    <div class="form-group modl"><label class="control-label col-md-4 col-xs-4" for="user_email"> Email : <strong class="text-danger"></strong></label><div class="col-md-7 col-xs-8 eml"><i class="fa fa-envelope" aria-hidden="true"></i><input type="email" class="form-control mntxt" name="eemail" id="eemail" placeholder="" aria-required="true" required="required" value="<?=$this->entity_email?>"></div></div>
                    <div class="form-group modl"><label class="control-label col-md-4 col-xs-4" for="comment"> Additional Details  : <strong class="text-danger"></strong></label><div class="col-md-7 col-xs-8"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><textarea rows="3" cols="40" class="form-control mntxt" name="ecomment" id="ecomment" placeholder="" ></textarea></div></div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hotel_id" value="<?=$hotel_id?>">
                    <input type="hidden" id="adult_count" value="<?=$number_of_adults?>">
                    <?php if($number_of_childs > 0){?>
                        <input type="hidden" id="child_count" value="<?=$number_of_childs?>">
                    <?php } ?>
                    <input type="hidden" id="no_of_nights" value="<?=$hotel_search_params['no_of_nights']?>">
                    <input type="hidden" id="room_count" value="<?=$hotel_search_params['room_count']?>">

                    <input type="submit" class="btn btn-default" id="send_enquiry_button" value="Send" />
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo $GLOBALS['CI']->template->isolated_view('share/flight_session_expiry_popup');?>

<script type="text/javascript">
$(document).ready(function(){

$("#owl_demo_1").owlCarousel({
    items : 1, 
    itemsDesktop : [1000,1],
    itemsDesktopSmall : [900,1], 
    itemsTablet: [768,1], 
    itemsMobile : [479,1], 
    navigation : true,
    pagination : false
});		
$("#owl_demo_2").owlCarousel({
    items : 1, 
    itemsDesktop : [1000,1],
    itemsDesktopSmall : [900,1], 
    itemsTablet: [768,2], 
    itemsMobile : [479,1], 
    navigation : true,
    pagination : false
});
  
	
	$(".tooltipv").tooltip();
	
	$(".provabslideshow").provabslideshow({
            mobile: true
        });
	
	
	$(document).on('click', '.location-map', function() {
		$('#map-box-modal').modal();
	});
	$('.room_select').click(function(){
		$('#room-list').trigger('click');
		var topmove = $('.room_select').offset().top;
		$('html, body').animate({scrollTop: topmove}, 500);
	});
	var sync1 = $("#sync1");
	var sync2 = $("#sync2");
	sync1.owlCarousel({
		singleItem : true,
		slideSpeed : 1000,
		navigation: true,
		pagination:false,
		afterAction : syncPosition,
		responsiveRefreshRate : 200,
	});

	sync2.owlCarousel({
		items : 4,
		itemsDesktop	: [1199,4],
		itemsDesktopSmall	 : [979,4],
		itemsTablet	 : [768,4],
		itemsMobile	 : [479,2],
		pagination:false,
		responsiveRefreshRate : 100,
		afterInit : function(el){
		el.find(".owl-item").eq(0).addClass("synced");
		}
	});

	function syncPosition(el){
		var current = this.currentItem;
		$("#sync2")
		.find(".owl-item")
		.removeClass("synced")
		.eq(current)
		.addClass("synced")
		if($("#sync2").data("owlCarousel") !== undefined){
		center(current)
		}

	}

	$("#sync2").on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).data("owlItem");
		sync1.trigger("owl.goTo",number);
	});

	function center(number){
		var sync2visible = sync2.data("owlCarousel").owl.visibleItems;

		var num = number;
		var found = false;
		for(var i in sync2visible){
		if(num === sync2visible[i]){
			var found = true;
		}
		}

		if(found===false){
		if(num>sync2visible[sync2visible.length-1]){
			sync2.trigger("owl.goTo", num - sync2visible.length+2)
		}else{
			if(num - 1 === -1){
			num = 0;
			}
			sync2.trigger("owl.goTo", num);
		}
		} else if(num === sync2visible[sync2visible.length-1]){
		sync2.trigger("owl.goTo", sync2visible[1])
		} else if(num === sync2visible[0]){
		sync2.trigger("owl.goTo", num-1)
		}
	}
});

/*function facilitypsw() {
	window.setTimeout( function () { top(); }, 2000 );
	$('#facilitypsw').addClass('active');
	$('#facilitypsr').removeClass('active');
	$('#facilitypsh').removeClass('active');
	$('#facilitypost').removeClass('active');
	// body...
}*/


function facilitypsw() {
    $('html,body').animate({
        scrollTop: $(".shdoww").offset().top},
        'slow');
	$('#facilitypsw').addClass('active');
	$('#facilitypsr').removeClass('active');
	$('#facilitypsh').removeClass('active');
	$('#facilitypost').removeClass('active');
}
</script>

<script type="text/javascript">
	$('.stra_hotel .fa').click(function() {
    $(this).toggleClass("active");
});
</script>

<script type="text/javascript">
    $(document).ready(function(){
        var sync1 = $("#sync1");
        var sync2 = $("#sync2");
        sync1.owlCarousel({
            singleItem : true,
            slideSpeed : 1000,
            navigation: true,
            pagination:false,
            afterAction : syncPosition,
            responsiveRefreshRate : 200,
        });

        sync2.owlCarousel({
            items : 4,
            itemsDesktop  : [1199,4],
            itemsDesktopSmall  : [979,4],
            itemsTablet  : [768,4],
            itemsMobile  : [479,2],
            pagination:false,
            responsiveRefreshRate : 100,
            afterInit : function(el){
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });

        function syncPosition(el){
            var current = this.currentItem;
            $("#sync2")
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced")
            if($("#sync2").data("owlCarousel") !== undefined){
                center(current)
            }

        }

        $("#sync2").on("click", ".owl-item", function(e){
            e.preventDefault();
            var number = $(this).data("owlItem");
            sync1.trigger("owl.goTo",number);
        });

        function center(number){
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;

            var num = number;
            var found = false;
            for(var i in sync2visible){
                if(num === sync2visible[i]){
                    var found = true;
                }
            }

            if(found===false){
                if(num>sync2visible[sync2visible.length-1]){
                    sync2.trigger("owl.goTo", num - sync2visible.length+2)
                }else{
                    if(num - 1 === -1){
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if(num === sync2visible[sync2visible.length-1]){
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if(num === sync2visible[0]){
                sync2.trigger("owl.goTo", num-1)
            }
        }
        $('#send_enquiry_button').click(function() {
            $title    = $('#title').val();
            $ename    = $('#ename').val();
            $elname   = $('#elname').val();
            $emobile  = $('#emobile').val();
            $eemail   = $('#eemail').val();
            $ecomment = $('#ecomment').val();
            $hotel_id  = $('#hotel_id').val();
            $pn_country_code  = $('#pn_country_code').val();
            $adult_count  = $('#adult_count').val();
            $child_count  = $('#child_count').val();
            $no_of_nights  = $('#no_of_nights').val();
            $room_count  = $('#room_count').val();
            if($ename==''  || $eemail=='' || $ecomment=='' ) { return false; }
            $.post('<?=base_url();?>index.php/hotels/ajax_enquiry', {'title': $title,'ename': $ename,'elname': $elname,'pn_country_code':$pn_country_code,'emobile': $emobile,'eemail': $eemail,'ecomment': $ecomment,'hotel_id': $hotel_id,'adult_count': $adult_count,'child_count':$child_count,'no_of_nights':$no_of_nights,'room_count':$room_count}, function(data) {
                $('#enquiry_form').trigger("reset");
                $('#alertp').removeClass('hide');
                $('#enquiry_form').addClass('hide');
            });
        });
        $('#enquiry_pop_button').click(function() {
            $('#alertp').addClass('hide');
            $('#enquiry_form').removeClass('hide');
        });
    });
</script>

<script>
    $(document).ready(function() {
      
            $( "#departure_date" ).datepicker({
                minDate : 0,
            });

        $(".n_psngr").click(function(){

            $('#departure_date').datepicker('show');
        });
        if ($('#departure_date_list').find('tr').length == 0) {
            $('#departure_date_table').hide();
        }
       /* $('#vertical').lightSlider({
            gallery:true,
            item:1,
            vertical:false,
            verticalHeight:370,
            vThumbWidth:120,
            thumbItem:4,
            thumbMargin:8,
            slideMargin:0,
            loop:true
        });*/



        $('.nav-tabs a').click(function(){
            $(this).tab('show');
        });



    });

    $(document).on('change', '.dynamicPriceBreakdown', function(){
        dynamicPriceBreakdown();
    });

    function dynamicPriceBreakdown(){
        $('.dynamicPriceBreakdown:checked').each(function(){
            // console.log($(this).data('type'));
            // console.log($(this).attr('data-priceTitle'));
            if($(this).data('price')!='undefined' && $(this).data('price')>0)
            {

                $('.trs-'+$(this).data('type')).find('.price').text($(this).data('price'));
                $('.type_'+$(this).data('type')).val($(this).attr('data-priceTitle'));
            }
            var total_price =parseInt($('.trs-hotel').find('.price').text()) + parseInt($('.trs-car').find('.price').text()) + parseInt($('.package_origional_price').val());
            $('.trs-total').find('.price').text(total_price);
        });
    }

</script>

