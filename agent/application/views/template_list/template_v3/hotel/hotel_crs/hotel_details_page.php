<?php
     // debug($currency_obj);exit;
$mini_loading_image = '<div class="text-center loader-image"><img src="' . $GLOBALS['CI']->template->template_images('loader_v3.gif') . '" alt="Loading........"/></div>';
$loading_image = '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div><div class="bounce4"></div></div>';

Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/hotel_details_slider.js'), 'defer' => 'defer');
$booking_url = $GLOBALS['CI']->hotel_lib->booking_url($hotel_search_params['search_id']);
//debug($hotel_details);
$TraceId = $params['TraceId'];
$_HotelDetails = $hotel_details; //debug(); exit();
$sanitized_data ['HotelCode'] = $_HotelDetails['hotel_details_id'];
$sanitized_data ['HotelName'] = $_HotelDetails ['hotel_name'];
$sanitized_data ['StarRating'] = $_HotelDetails ['star_rating'];
$sanitized_data ['Description'] = $_HotelDetails ['hotel_info'];
$sanitized_data ['Attractions'] = (isset ( $_HotelDetails ['Attractions'] ) ? $_HotelDetails ['Attractions'] : false);
$sanitized_data ['HotelFacilities'] = (isset ( $_HotelDetails ['HotelFacilities'] ) ? $_HotelDetails ['HotelFacilities'] : false);
$sanitized_data ['HotelPolicy'] = (isset ( $_HotelDetails ['HotelPolicy'] ) ? $_HotelDetails ['HotelPolicy'] : false);
$sanitized_data ['SpecialInstructions'] = (isset ( $_HotelDetails ['SpecialInstructions'] ) ? $_HotelDetails ['SpecialInstructions'] : false);
$sanitized_data ['Address'] = (isset ( $_HotelDetails ['hotel_address'] ) ? $_HotelDetails ['hotel_address'] : false);
$sanitized_data ['PinCode'] = (isset ( $_HotelDetails ['postal_code'] ) ? $_HotelDetails ['postal_code'] : false);
$sanitized_data ['HotelContactNo'] = (isset ( $_HotelDetails ['phone_number'] ) ? $_HotelDetails ['phone_number'] : false);
$sanitized_data ['Latitude'] = (isset ( $_HotelDetails ['latitude'] ) ? $_HotelDetails ['latitude'] : 0);
$sanitized_data ['Longitude'] = (isset ( $_HotelDetails ['longitude'] ) ? $_HotelDetails ['longitude'] : 0);
$sanitized_data ['RoomFacilities'] = (isset ( $_HotelDetails ['RoomFacilities'] ) ? $_HotelDetails ['RoomFacilities'] : false);
$sanitized_data ['Images'] = $_HotelDetails ['hotel_images'];



if ($sanitized_data['Images']) {
    $sanitized_data['Images'] = $sanitized_data['Images'];
} else {
    $sanitized_data['Images'] = $GLOBALS['CI']->template->template_images('default_hotel_img.jpg');
}

Js_Loader::$css [] = array (
		'href' => $GLOBALS ['CI']->template->template_css_dir ( 'owl.carousel.min.css' ),
		'media' => 'screen' 
);
Js_Loader::$js [] = array (
		'src' => $GLOBALS ['CI']->template->template_js_dir ( 'owl.carousel.min.js' ),
		'defer' => 'defer' 
);

$base_url_image=base_url().'index.php/hotel/image_details_cdn';
?>

<?php
/**
 * Application VIEW
 */
echo $GLOBALS ['CI']->template->isolated_view ( 'hotel/search_panel_summary' );
?>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJfvWH36KY3rrRfopWstNfduF5-OzoywY&sensor=false"></script>
<script type="text/javascript">
	/** Google Maps **/
	var myCenter=new google.maps.LatLng(<?=floatval($sanitized_data['Latitude'])?>,<?=floatval($sanitized_data['Longitude']); ?>);
	function initialize()
	{
		var mapProp = {
			center:myCenter,
			zoom:12,
			mapTypeId:google.maps.MapTypeId.ROADMAP
		};

		var map = new google.maps.Map(document.getElementById("Map"), mapProp);
	
		var marker = new google.maps.Marker({
			position:myCenter,
		});
	
		marker.setMap(map);
	
		var infowindow = new google.maps.InfoWindow({
			content:"Hotel Location"
		});
	
		google.maps.event.addListener(marker, "click", function() {
			infowindow.open(map, marker);
		});
	}
	google.maps.event.addDomListener(window, "load", initialize);
</script>
<div class="clearfix"></div>
<input type="hidden" id="latitude" value="<?= $sanitized_data['Latitude'] ?>">
<input type="hidden" id="longitude" value="<?= $sanitized_data['Longitude'] ?>">
<input type="hidden" id="api_base_url" value="<?= $GLOBALS['CI']->template->template_images('marker/green_hotel_map_marker.png') ?>">
<input type="hidden" id="hotel_name" value="<?php echo $sanitized_data['HotelName'] ?>">
<div class="search-result">
    <div class="container mobilepad">
        <div class="htl_dtls_cont htldetailspage">
            <div class="rowfstep">
 				 <div class="col-md-8 col-sm-12 col-xs-12 nopad">
                   <div class="col-md-12 nopad">
                        <div class="htladdet">
                            <span><?php echo strtoupper($sanitized_data['HotelName']) ?></span>
                            <div class="marhtldet">
                                <span class="locadres"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;<?php echo $sanitized_data['Address'] ?></span>
                                <ul class="htlratpz">
                                    <div class="stardetshtl"><span class="rating-no"><span class="hide" id="h-sr"><?= $sanitized_data['StarRating'] ?></span><?php echo print_star_rating($sanitized_data['StarRating']); ?></span>
                                    </div>
                                </ul>
                                <!-- <?php if ($_HotelDetails['trip_adv_url']): ?>
                                    <div class="triexcimg mobile_advisor">
                                        <a href="#"><img src="<?= $_HotelDetails['trip_adv_url'] ?>"></a>
                                    </div>
                                <?php endif; ?> -->
                            </div>
                        </div>
                    </div>


                <div class="col-md-12 nopad">
                    <div class="htldtdv">
                         <div id="hotel_top" class="owl-carousel owl-theme">
                        <?php if (valid_array($sanitized_data) == true) { ?>    
							<?php
							//debug($sanitized_data['Images']); exit;
                            if($_HotelDetails['added_by_type']=='Admin'){
                                $pathh ='supervision';
                            }else{
                                $pathh ='extranet';
                            }
							$pic =  explode(',', $sanitized_data ['Images']);
							$visible = 'active';
							if(count($pic) > 0){ 
								foreach ( $pic as $i_k => $i_v ) {
								?>
								<div class="item">
								<img src=<?php echo get_host().APP_ROOT_DIR.'/'.$pathh.'/uploads/hotel_images'.'/'.$i_v?> alt="<?php echo $i_k?>" class="img-responsive" style="width: 100%; height: 400px">
								<div class="carousel-caption">
								<p><?php echo $sanitized_data['HotelName']?></p>
								</div>
								</div>
								<?php		
								}
							}
						}else { ?>
							<?= '<img src="' . $GLOBALS['CI']->template->template_images('default_hotel_img.jpg') . '" alt="' . $sanitized_data['HotelName'] . '"/>' ?>
						<?php } ?>
						
				</div>
 							<div class="item hide" id="map_viewsld">
                                <div class="map_mobile_dets">
                                    <div id="Map" class="col-md-12" style="height:363px; width:100%;max-height: 363px;">Map</div>
                                </div> 
                            </div>
                            <div id="hotel_bottom" class="owl-carousel owl-theme">
								<?php if (valid_array($sanitized_data) == true) { ?>
                                    <?php foreach ($pic as $i_k => $i_v) { ?>
                                        <?php
                                        $image_found = 1;
                                        ?>
                                        <?php if ($image_found): ?>
                                            <div class="item">
                                                 <img src=<?php echo get_host().APP_ROOT_DIR.'/'.$pathh.'/uploads/hotel_images'.'/'.$i_v?> alt="<?php echo $i_k?>" class="img-responsive" style="width: 100%; height: 200px">
                                            </div>
                                            <?php endif; ?>
                                        <?php } ?>
                                <?php }else { ?>
                                    <?= '<img src="' . $GLOBALS['CI']->template->template_images('default_hotel_img.jpg') . '" alt="' . $sanitized_data['HotelName'] . '"/>' ?>
                                <?php } ?>
                            </div>
                            <div class="htlmapdtls" id="maphtlmapdtls">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </div>
                            <div class="htlmapdtls hide" id="maphtlmapimages">
                                <i class="fa fa-picture-o" aria-hidden="true"></i>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- slider end -->
                <div class="col-md-4 col-sm-12 col-xs-12 resmagfix">
                 <?php if (isset($_HotelDetails['trip_rating'])): ?>
                    <div class="tridtls">                       
                            <div class="trirat">
                                <span class="trpratclr"><?= $_HotelDetails['trip_rating'] ?></span>
                            <?php
                            $star_rating = $_HotelDetails['trip_rating'];
                            $trip_text = '';
                            if ($star_rating == 3 || $star_rating == 3.5) {
                                $trip_text = 'Average';
                            } elseif ($star_rating == 4) {
                                $trip_text = 'Good';
                            } elseif ($star_rating == 4.5 || $star_rating == 5) {
                                $trip_text = 'Excellent';
                            } elseif ($star_rating == 2 || $star_rating == 2.5) {
                                $trip_text = 'Bad';
                            } elseif ($star_rating < 2) {
                                $trip_text = 'Very Bad';
                            }
                            ?>
                                <span class="triexcer"><?= $trip_text ?></span>
                            </div>
                           
                            <?php if ($_HotelDetails['trip_adv_url']): ?>
                            <div class="triexcimg">
                                <span class="trptrvrat">TripAdvisor Traveler Rating</span>
                                <a href="#"><img src="<?= $_HotelDetails['trip_adv_url'] ?>"></a>
                            </div>
                             <?php endif; ?>
                    </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                        <?php 
                       // debug($rooms_lists[0]);
                        if (isset($rooms_lists) && empty($rooms_lists[0]) == false): ?>
                        <div class="htlfull_dtls">
                            <div class="htlamtnyt">
                        <?php
                        $RoomPrice = $rooms_lists[0]['total_price'];
                        $admin_markup = $rooms_lists[0]['admin_markup'];
                        $agent_markup = $rooms_lists[0]['agent_markup'];
                        $total_price = ($RoomPrice + $admin_markup +$agent_markup);
                        $no_of_nights = $hotel_search_params['no_of_nights'];
                        $per_night_price = ceil($total_price / $no_of_nights);
                        $night_str = 'Night';
                        if ($no_of_nights > 1) {
                            $night_str = 'Nights';
                        }
                        ?>
                        <h2 class="amthtlrs"><strong> <?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?> </strong></i>&nbsp;<?= roundoff_number(get_converted_currency_value ( $currency_obj->force_currency_conversion 
                            ($per_night_price))) ?><span class="pernyt">/ Per Night</span></h2> 
							<div class="stdrmac">
                                    <span class="stdnonaclt"><?=$rooms_lists[0]['room_type_name'] ?></span>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="htlamtnytstd">
                                <h4 class="amthtlrsstd"><i class="" aria-hidden="true"></i><?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?>&nbsp;<?= get_converted_currency_value ( $currency_obj->force_currency_conversion 
                                  ($per_night_price)) ?><span class="pernytdet">( <?= $no_of_nights ?> <?= $night_str ?> )</span></h4> 

                    <?php endif; 
                    ?>
                    <div class="clearfix"></div>
                    <div class="cheoutdv">
                        <div class="chkdatetacell">
                            <span class="chkin">Check-in</span>
                            <span class="chkdate"><?= date('d M Y', strtotime($hotel_search_params['from_date'])); ?></span>
                        </div>
                        <div class="chkdatetacell">
                            <span class="chkin">Check-out</span>
                            <span class="chkdate"><?= date('d M Y', strtotime($hotel_search_params['to_date'])); ?></span>
                        </div>
                        <div class="chkdatetacell">
                            <span class="chkin">Room Guests</span>
<?php
$adult_count = 0;
$child_count = 0;
foreach ($hotel_search_params['adult_config'] as $a_value) {
    $adult_count += $a_value;
}
foreach ($hotel_search_params['child_config'] as $c_value) {
    $child_count += $c_value;
}
$adult_str = 'adult';
if ($adult_count > 1) {
    $adult_str = 'adults';
}
$child_str = 'child';
if ($child_count > 1) {
    $child_str = 'childrens';
}
?>
<span class="chkdate"><?= $hotel_search_params['room_count'] ?> room & <?= $adult_count ?> <?= $adult_str ?> <?php if ($child_count): ?> & <?= $child_count ?> <?= $child_str ?> <?php endif; ?></span>
</div>
</div>
<div class="clearfix"></div>
<div class="htlbkbtn">
<?php

$common_params_url = '';
$common_params_url .= '<input type="hidden" name="CancellationPolicy[]"	value="Cancellation">'; //Jaganath
$common_params_url .= '<input type="hidden" name="booking_source"	value="' . $params['booking_source'] . '">';
$common_params_url .= '<input type="hidden" name="search_id"		value="' . $hotel_search_params['search_id'] . '">';
$common_params_url .= '<input type="hidden" name="ResultIndex"		value="' . $params['ResultIndex'] . '">';
$common_params_url .= '<input type="hidden" name="op"				value="block_room">';
$common_params_url .= '<input type="hidden" name="GuestNationality"	value="' . ISO_INDIA . '" >';
$common_params_url .= '<input type="hidden" name="HotelName"		value="" >';
$common_params_url .= '<input type="hidden" name="StarRating"		value="">';
$common_params_url .= '<input type="hidden" name="HotelImage"		value="">'; //Jaganath
$common_params_url .= '<input type="hidden" name="HotelAddress"		value="">'; //

$dynamic_params_url[] = $rooms_lists;

$dynamic_params_url = serialized_data($dynamic_params_url);

$temp_dynamic_params_url = '';
$temp_dynamic_params_url .= '<input type="hidden" name="token" value="' . $dynamic_params_url . '">';
$temp_dynamic_params_url .= '<input type="hidden" name="token_key" value="' . md5($dynamic_params_url) . '">';
?>
                        <form method="POST" action="<?= $booking_url ?>">
                        <?php
                        echo $common_params_url . $temp_dynamic_params_url;
                        ?>
                        <a href="#rooms" class="bookallbtn htlbkftsz" id="selectroom">Book Now</a>
                          <!--   <button class="bookallbtn htlbkftsz" type="submit" id="selectroom">Book Now</button> -->
                        </form>

                    </div>
                </div>
            </div> 
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="fulldowny"> 
        <div class="container mobilepad">	
            <div class="fuldownsct">
                <div class="col-xs-12 nopad tab_htl_detail">
                    <div class="detailtab fulldetab shdoww">
                        <ul class="nav nav-tabs responsive-tabs">
                            <li class=""><a href="#htldets" data-toggle="tab">Hotel Details</a></li>
                            <li class="active roomstab"><a href="#rooms" data-toggle="tab">Rooms</a></li>
                            <li><a href="#facility" data-toggle="tab">Facilities</a></li>
                            <!-- <li><a href="#htlpolicy" data-toggle="tab">Hotel Policy</a></li> -->
                        </ul>
                        <div class="tab-content">
                            <!-- Hotel Detail-->
                            <div class="tab-pane" id="htldets">

                                <div class="innertabs">
                                    <h3 class="mobile_view_header">Description</h3>
                                    <!-- <div class="htldesdv">Hotel Description</div> -->
                                    <div id="hotel-additional-info" class="padinnerntb">
                                        <div class="lettrfty short-text"><?php echo $sanitized_data['Description'] ?></div>
                                        <div class="show-more">
                                            <a href="#">Show More +</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Hotel Detail End-->
                            <!-- Rooms-->
                            <div class="tab-pane active" id="rooms">

                                <div class="innertabs">
                                    <h3 class="mobile_view_header">Rooms</h3>

                                    <div class="">
                                        <div id="room-list" class="room-list romlistnh short-text1">
										<?php echo $loading_image; ?>
                                        </div>

                                        <div class="show-rooms">
                                            <a href="#" id="show-more-link" class="hide">Show More Rooms +</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Rooms End-->
                            <!-- Facilities--> 
                          <div class="tab-pane" id="facility">
								<div class="innertabs">
									<div class="padinnerntb htlfac_lity">
									<div class="innertabs">
                                    <h3 class="mobile_view_header">Facilities</h3>
                                     <div class="padinnerntb htlfac_lity">
                                   
							<?php
							//debug($rooms_lists);
							if (isset($rooms_lists)) {
								$color_code = '#00a0e0';
							for ($i=0; $i < count($rooms_lists); $i++) { 
								for ($j=0; $j < count($rooms_lists[$i]['room_amenity']); $j++) {
								//foreach ( $sanitized_data ['HotelFacilities'] as $ak => $av ) {
									?>
									 <div class="col-xs-4 col-md-3 nopad">
                                            <div class="facltyid">
												<span class="glyphicon glyphicon-check" style="color:<?php echo $color_code?>"></span> <?php echo $rooms_lists[$i]['room_amenity'][$j]['amenities_name']; ?></div>
										</div>
								<?php
								} }
								?>

								
							<?php
							}
							?>
							</div>
							</div>
							</div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
<?php
/**
 * This is used only for sending hotel room request - AJAX
 */
$hotel_room_params ['HotelCode'] = $params ['HotelCode'];
$hotel_room_params ['ResultIndex'] = $params ['ResultIndex'];
$hotel_room_params ['booking_source'] = $params ['booking_source'];
$hotel_room_params ['TraceId'] = $params ['TraceId'];
$hotel_room_params ['search_id'] = $hotel_search_params ['search_id'];
$hotel_room_params ['op'] = 'get_room_details';
$hotel_room_params['search_data'] = $requests;
?>
<script>
$(document).ready(function() {
	//Load hotel Room Details
	var ResultIndex = '';
	var HotelCode = '';
	var TraceId = '';
	var booking_source = '';
	var op = 'get_room_details';
	function load_hotel_room_details()
	{
		var _q_params = <?php echo json_encode($hotel_room_params)?>;
		if (booking_source) { _q_params.booking_source = booking_source; }
		if (ResultIndex) { _q_params.ResultIndex = ResultIndex; }
		if (HotelCode) { _q_params.HotelCode = HotelCode; }
		if (TraceId) { _q_params.TraceId = TraceId; }
		$.post(app_base_url+"index.php/ajax/get_room_details", _q_params, function(response) {
			if (response.hasOwnProperty('status') == true && response.status == true) {
				$('#room-list').html(response.data);
				var _hotel_name = "<?php echo preg_replace('/^\s+|\n|\r|\s+$/m', '', $sanitized_data['HotelName']);//Hotel Name comes from hotel info response ?>";
				var _hotel_star_rating = <?php echo abs($sanitized_data['StarRating'])?>;
				var _hotel_image = "<?php echo $sanitized_data['Images'][0];?>";
				var _hotel_address = "<?php echo preg_replace('/^\s+|\n|\r|\s+$/m', '', $sanitized_data['Address']);?>";
				$('[name="HotelName"]').val(_hotel_name);
				$('[name="StarRating"]').val(_hotel_star_rating);
				$('[name="HotelImage"]').val(_hotel_image);//Jaganath
				$('[name="HotelAddress"]').val(_hotel_address);//Jaganath
			}
		});
	}
	load_hotel_room_details();
	$('.hotel_search_form').on('click', function(e) {
		e.preventDefault();
		$('#hotel_search_form').slideToggle(500);
	});
	
	
	$('.movetop').click(function(){
		$('html, body').animate({scrollTop: $('.fulldowny').offset().top - 60 }, 'slow');
	});
	
});

/*  For responsive tab  */



! function($) {
	"use strict";
	var a = {
		accordionOn: ["xs"]
	};
	$.fn.responsiveTabs = function(e) {
		var t = $.extend({}, a, e),
			s = "";
		return $.each(t.accordionOn, function(a, e) {
			s += " accordion-" + e
		}), this.each(function() {
			var a = $(this),
				e = a.find("> li > a"),
				t = $(e.first().attr("href")).parent(".tab-content"),
				i = t.children(".tab-pane");
			a.add(t).wrapAll('<div class="responsive-tabs-container" />');
			var n = a.parent(".responsive-tabs-container");
			n.addClass(s), e.each(function(a) {
				var t = $(this),
					s = t.attr("href"),
					i = "",
					n = "",
					r = "";
				t.parent("li").hasClass("active") && (i = " active"), 0 === a && (n = " first"), a === e.length - 1 && (r = " last"), t.clone(!1).addClass("accordion-link" + i + n + r).insertBefore(s)
			});
			var r = t.children(".accordion-link");
			e.on("click", function(a) {
				a.preventDefault();
				var e = $(this),
					s = e.parent("li"),
					n = s.siblings("li"),
					c = e.attr("href"),
					l = t.children('a[href="' + c + '"]');
				s.hasClass("active") || (s.addClass("active"), n.removeClass("active"), i.removeClass("active"), $(c).addClass("active"), r.removeClass("active"), l.addClass("active"))
			}), r.on("click", function(t) {
				t.preventDefault();
				var s = $(this),
					n = s.attr("href"),
					c = a.find('li > a[href="' + n + '"]').parent("li");
				s.hasClass("active") || (r.removeClass("active"), s.addClass("active"), i.removeClass("active"), $(n).addClass("active"), e.parent("li").removeClass("active"), c.addClass("active"))
			})
		})
	}
}(jQuery);

$('.responsive-tabs').responsiveTabs({
	accordionOn: ['xs']
});
</script>
<?php
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/pax_count.js'), 'defer' => 'defer');
?>