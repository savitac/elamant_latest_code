<?php
if (isset($login) == false || is_object($login) == false) {
	$login = new Provab_Page_Loader('login');
}
?>




<!--<div class="background_login">
    
    	<div class="loadcity"></div>
    
    	<div class="clodnsun"></div>
        
        <div class="reltivefligtgo">
        	<div class="flitfly"></div>
        </div>

        <div class="clearfix"></div>
        <div class="busrunning">
            <div class="runbus"></div>
            <div class="runbus2"></div>
            <div class="roadd"></div>
        </div>
    </div>-->

    <style>


.login_logo{
    display: none
}
.login_body{
    padding: 0
}
.login-box, .register-box {
    width: 329px;
    margin: 7% auto;
    width: 300px;
    padding: 20px;
    background: white;
    -webkit-box-shadow: 0 2px 4px 0 rgba(0,0,0,0.5);
    -moz-box-shadow: 0 2px 4px 0 rgba(0,0,0,0.5);
    -ms-box-shadow: 0 2px 4px 0 rgba(0,0,0,0.5);
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.5);
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    -ms-border-radius: 4px;
    border-radius: 4px;
}
.panel_footer{
        margin: 0px 0 0;
    padding: 10px 0 0;
}
</style>


<header id="main-header" class="new-header">  
      <div class="container">            
      <div class="col-md-3 col-xs-3 sme-logo">               
      <a href="#" style=""><img src="<?php echo $GLOBALS['CI']->template->domain_images($GLOBALS['CI']->template->get_domain_logo())?>" alt="Elamant Travel"></a>            
      </div>            
            <div class="col-md-6 col-xs-9 col-md-offset-3 ">                
            <p class="col-md-6 login-signup-text">Have a Elamant Travel <br>
            <span>for Business account? </span>                
            </p>                
            <div class="col-md-6 sme-flr sme-login-signup">                    
            <div class="login-signup">                        
            <button type="button" id="login-btn" class="login-btn" login-scroll-to="form-login">Login</button>                        
            <span class="top-signup-or">OR</span> 

            <a href="<?=base_url().'index.php/user/agentRegister' ?>" class="sign-btn">Sign up</a>               
            <!-- <button type="button" class="sign-btn" scroll-to="signup-block">Sign up</button>  -->                   
            </div>                
            </div>            
            </div>        
            </div>  
</header>
<div class="row main-bg">
<div class="container">
<div class="col-md-8">
    <div class="left-panel">
  <h2 class="title-new">  Welcome to Elamant Travel for Business</h2>

<h4 class="title-new1">The LARGEST platform to book & manage all your Business Travels!</h4>

<p class="title-new2">Some of our esteemed clients:</p>

<img class ="logo-all" src="<?= $GLOBALS['CI']->template->template_images('logo-all.png');?>">
    </div>

</div>
<div class="col-md-4">
<div class="login-box log_inner">
	<figure class="login_logo">
		<img src="<?php echo $GLOBALS['CI']->template->domain_images($GLOBALS['CI']->template->get_domain_logo())?>" alt="logo"	class="img-responsive center-block">
	</figure>
	<div class="login_body">
		<div class="login_box_msg">Already have a Elamant Travel for Business account? <strong>
            
         Login Now </strong></div>
         <div class="text-center"><i class="fa fa-user-circle" aria-hidden="true" style="font-size: 45px;"></i></div>
		<?php echo $login->generate_form('login', array('email' => '@gmail.com', 'password' => '')); ?>
	</div>
    <div class="text-center" style="margin:10px 0; "><a href="<?=base_url().'index.php/user/agentRegister' ?>">Create an Account</a></div>
	<div class="panel_footer">
		<?php echo $GLOBALS['CI']->template->isolated_view('general/forgot-password');?>
	</div>
</div>
</div>
</div>
</div>
<div class="row">
        <div class="container">
          <h4 class="nw-tit">  Let numbers do the talking!</h4>

            <p class="nw-p">And let India’s largest brand in Business travel take care of your organisations travel needs!</p>

            <div class="col-md-12">
            <div class="circles-desktop">
                    <div class="inner-achievements">
                      <div class="circle-container ">
                          <div class="cir-orange cir-new"></div>
                          <p class="img-block"></p>
                          <p class="content-new  ic-text">700+ 
                            <span>Large Corporate</span> 
                            <span>Clients</span> 
                         </p>
                      </div>
                    <div class="circle-container ">
                          <div class="cir-new cir-green">
                          </div>
                          <p class="img-block"></p>
                          <p class="content-new  ic-text">10,000+ 
                            <span>Registered SME</span>
                            <span>Customers</span></p>
                      </div>
                     <div class="circle-container">
                          <div class="cir-new cir-pink">
                          </div>
                          <p class="img-block"></p>
                          <p class="content-new  ic-text">No. 1 <span>Leader in 
                             </span>  <span>Business Travel*</span></p>
                      </div>
                     <div class="circle-container">
                          <div class="cir-new cir-blue">
                          </div>
                          <p class="img-block"></p>
                          <p class="content-new  ic-text">2.4m+ <span>Bookings</span></p>
                      </div>
                     <div class="circle-container">
                          <div class="cir-new cir-brown">
                          </div>
                          <p class="img-block"></p>
                          <p class="content-new ic-text">4.2m+ <span>Corporate</span><span>Employees</span></p>
                      </div>
                    
                    </div>
                   
                   <div class="text-center">
                   <button class="sign-up-btn">Sign Up For Elamant Travel for Business</button>
                   </div>
                </div>
            </div>
        </div>
</div>
<script type="text/javascript">
    
    $('#login-btn').click(function(){
    $('#email').focus();
});
</script>