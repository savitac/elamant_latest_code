<?php

/**
 * author: Arjun J Gowda
 * FORM START
 */
$form_configuration['inputs'] = array(
	'origin' => array('type' => 'hidden', 'label_line_code' => -1,'mandatory' => false),
	'transaction_type' => array('type' => 'hidden', 'label_line_code' => -1),
	'amount' => array('type' => 'text', 'label_line_code' => 208),
    'date_of_transaction' => array('type' => 'text', 'label_line_code' => 205,'readonly' => true, 'enable_dp' => true),
	'remarks' => array('type' => 'textarea', 'label_line_code' => 211),
	'card-number' => array('type' => 'text', 'label_line_code' => 900),
	'month' => array('type' => 'text', 'label_line_code' => 901),
	'year' => array('type' => 'text', 'label_line_code' => 902)
);

/**
 * Add FORM
 */
$form_configuration['form']['request_form'] = array(
'form_header' => '',
		'sections' => array(
			array('elements' => array(
					'origin','transaction_type','amount','date_of_transaction', 'remarks','card-number','month','year'
				),
				'fieldset' => 'FFL0048'
			)
		),
		'form_footer' => array('submit', 'reset')
	);
/*** Form End ***/
/**
 * FORM VALIDATION SETTINGS
 */
$auto_validator['amount'] = 'trim|required|numeric';
$auto_validator['date_of_transaction'] = 'trim|required';
$auto_validator['remarks'] = 'trim';