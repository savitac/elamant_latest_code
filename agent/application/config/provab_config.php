<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['master_module_list']	= array(
META_AIRLINE_COURSE => 'flight',
META_TRANSFERS_COURSE => 'transferHotel',
META_ACCOMODATION_COURSE => 'hotel',
META_BUS_COURSE => 'bus',
// META_TRANSFERV1_COURSE=>'transfers',
META_TRANSFERV1_COURSE=>'Car Rental',
META_SIGHTSEEING_COURSE=>'activities',
META_PACKAGE_COURSE => 'holiday',

META_CAR_COURSE=>'car'

);
/******** Current Module ********/
$config['current_module'] = 'b2b';

$config['verify_domain_balance'] = true;

/******** PAYMENT GATEWAY START ********/
//To enable/disable PG
$config['enable_payment_gateway'] = true;
$config['active_payment_gateway'] = 'AUTHORIZE';
$config['active_payment_system'] = 'test';//test/live
$config['payment_gateway_currency'] = 'USD';//INR
/******** PAYMENT GATEWAY END ********/


/******** BOOKING ENGINE START ********/
$config['flight_engine_system'] = 'test'; //test/live
$config['hotel_engine_system'] = 'test'; //test/live
$config['bus_engine_system'] = 'test'; //test/live
$config['sightseeing_engine_system'] = 'test';
$config['transfer_engine_system'] = 'test';
$config['car_engine_system'] = 'test'; //test/live
$config['external_service_system'] = 'test'; //test/live

$config['domain_key'] = CURRENT_DOMAIN_KEY;
$config['test_username'] = 'test229267';
$config['test_password'] = 'test@229';

//$config['flight_url'] = 'http://192.168.0.87/travelomatix_services/webservices/index.php/flight/service/';
//$config['hotel_url'] = 'http://192.168.0.87/travelomatix_services/webservices/index.php/hotel/service/';
//$config['bus_url'] = 'http://test.services.travelomatix.com/webservices/index.php/bus/service/';


$config['sightseeing_url'] =  'http://test.services.travelomatix.com/webservices/index.php/sightseeing/service/';

$config['transferv1_url'] = 'http://test.services.travelomatix.com/webservices/index.php/transferv1/service/';

$config['car_url'] = 'http://test.services.travelomatix.com/webservices/index.php/car/service/';




$config['flight_url'] = 'http://test.services.travelomatix.com/webservices/index.php/flight/service/';
// $config['hotel_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/hotel/service/';
$config['hotel_url'] = 'http://test.services.travelomatix.com/webservices/index.php/hotel_v3/service/';
$config['bus_url'] = 'http://test.services.travelomatix.com/webservices/index.php/bus/service/';




if ($config['external_service_system'] == 'live') {
	$config['external_service'] = 'http://prod.services.travelomatix.com/webservices/index.php/rest/';
} else {
	$config['external_service'] = 'http://test.services.travelomatix.com/webservices/index.php/rest/';
}



// $config['live_username'] = 'TMX241790';
// $config['live_password'] = 'TMX@550241';
/******** BOOKING ENGINE END ********/

/**
 * 
 * Enable/Disable caching for search result
 */
$config['cache_hotel_search'] = true;
$config['cache_sightseeing_search'] = true;
$config['cache_flight_search'] = false;
$config['cache_bus_search'] = true;
$config['cache_car_search'] = false;

/**
 * Number of seconds results should be cached in the system
 */
$config['cache_hotel_search_ttl'] = 300;
$config['cache_flight_search_ttl'] = 1900;
$config['cache_bus_search_ttl'] = 300;
$config['cache_car_search_ttl'] = 300;
$config['cache_sightseeing_search_ttl'] = 300;



/*$config['lazy_load_hotel_search'] = true;*/
$config['hotel_per_page_limit'] = 20;
$config['car_per_page_limit'] = 200;

/*
	search session expiry period in seconds
*/
$config['flight_search_session_expiry_period'] = 600;
$config['flight_search_session_expiry_alert_period'] = 300;
