<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// ------------------------------------------------------------------------
/**
 * Controller for all ajax activities
 *
 * @package    Provab
 * @subpackage ajax loaders
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */
// ------------------------------------------------------------------------

class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	/**
	 * index page of application will be loaded here
	 */
	function index()
	{

	}

	/**
	 * Arjun J Gowda
	 */
	function forgot_password()
	{

		$post_data = $this->input->post();
		extract($post_data);
		//email, phone
		$condition['email'] = $email;
		$condition['phone'] = $phone;
		$condition['status'] = ACTIVE;
		$condition['user_type'] = B2B_USER;
		$user_record = $this->custom_db->single_table_records('user', 'email, password, user_id, first_name, last_name', $condition);
		if ($user_record['status'] == true and valid_array($user_record['data']) == true) {

			//Sms config & Checkpoint
			/* if(active_sms_checkpoint('forget_password'))
			{
			$msg = "Dear ".$user_record['data'][0]['first_name']." Your Password details has been sent to your email id";
			//print($msg); exit;
			$msg = urlencode($msg);
			$this->provab_sms->send_msg($phone,$msg);
			} */
			//sms will be sent

			$user_record['data'][0]['password'] = time();
			//send email
			$mail_template = $this->template->isolated_view('user/forgot_password_template', $user_record['data'][0]);
			$user_record['data'][0]['password'] = md5($user_record['data'][0]['password']);
			$this->custom_db->update_record('user', $user_record['data'][0], array('user_id' => intval($user_record['data'][0]['user_id'])));
			$this->load->library('provab_mailer');
			$this->provab_mailer->send_mail($email, 'Password Reset', $mail_template);
			$data = 'Password Has Been Reset Successfully and Sent To Your Email ID';
			$status = true;
		} else {
			$data = 'Password Provide Correct Data To Identify Your Account';
			$status = false;
		}
		header('content-type:application/json');
		echo json_encode(array('status' => $status, 'data' => $data));
		exit;
	}

	/**
	 * Arjun J Gowda
	 */
	function login()
	{
		$post_data = $this->input->post();
		extract($post_data);
		$user_type = B2C_USER;
		//email, phone
		$condition['email'] = $username;
		$condition['password'] = md5($password);
		$condition['status'] = ACTIVE;
		$condition['domain_list_fk'] = get_domain_auth_id();
		$condition['user_type'] = $user_type;
		$user_record = $this->custom_db->single_table_records('user', '*', $condition);
		if ($user_record['status'] == true and valid_array($user_record['data']) == true) {
			//send email
			$data = 'Login Successful';
			$status = true;
			//create login pointer
			$user_type = $user_record['data'][0]['user_type'];
			$auth_user_pointer = $user_record['data'][0]['uuid'];
			$login_pointer = $this->user_model->create_login_auth_record($auth_user_pointer, $user_type);
			$this->session->set_userdata(array(AUTH_USER_POINTER => $auth_user_pointer, LOGIN_POINTER => $login_pointer));
		} else {
			$data = 'Username And Password Does Not Match!!!';
			$status = false;
		}
		header('content-type:application/json');
		echo json_encode(array('status' => $status, 'data' => $data));
		exit;
	}

	function change_password()
	{
		$data=array();
		$entity_user_id = $this->entity_user_id;
		if(intval($entity_user_id) < 1) {
			redirect("general/initilize_logout");
		}
		$page_data['form_data'] = $this->input->post();
		if(valid_array($page_data['form_data'])==TRUE) {
			$this->current_page->set_auto_validator();
			if ($this->form_validation->run()) {
				$table_name="user";
				/** Checking New Password and Old Password Are Same OR Not **/
				$condition['password'] = md5($this->input->post('new_password'));
				$condition['user_id'] = $user_id;
				$check_pwd = $this->custom_db->single_table_records($table_name,'password',$condition);
				if(!$check_pwd['status']) {
					$condition['password'] = md5($this->input->post('current_password'));
					$condition['user_id'] = $user_id;
					$data['password'] = md5($this->input->post('new_password'));
					$update_res=$this->custom_db->update_record($table_name, $data, $condition);
					if($update_res)	{
						$this->session->set_flashdata(array('message' => 'UL0010', 'type' => SUCCESS_MESSAGE));
						refresh();
					} else {
						$this->session->set_flashdata(array('message' => 'UL0011', 'type' => ERROR_MESSAGE));
						refresh();
						/*$data['msg'] = 'UL0011';
						 $data['type'] = ERROR_MESSAGE;*/
					}
				} else {
					$this->session->set_flashdata(array('message' => 'UL0012', 'type'=>WARNING_MESSAGE));
					refresh();
					//redirect('general/change_password?uid='.urlencode($get_data['uid']));
				}
			}
		}
		$this->template->view('user/change_password', $data);
	}

	/**
	 * Logout function for logout from account and unset all the session variables
	 */
	function initilize_logout() {
		if (is_logged_in_user()) {
			$user_id = $this->session->userdata(AUTH_USER_POINTER);
			$login_id = $this->session->userdata(LOGIN_POINTER);
			$this->user_model->update_login_manager($user_id, $login_id);
			$this->session->unset_userdata(array(AUTH_USER_POINTER => '',LOGIN_POINTER => ''));
			redirect(base_url());
		}
	}
}
