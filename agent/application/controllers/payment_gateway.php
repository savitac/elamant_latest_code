<?php
if (! defined ( 'BASEPATH' ))
exit ( 'No direct script access allowed' );
/**
 *
 * @package Provab
 * @subpackage Transaction
 * @author Pravinkuamr P <pravinkumar.provab@gmail.com>
 * @version V1
 */
class Payment_Gateway extends CI_Controller {
	/**
	 *
	 */
	public function __construct() {
		parent::__construct ();
		// $this->output->enable_profiler(TRUE);
		$this->load->model ( 'module_model' );
	}

	function demo_booking_blocked()
	{
		echo '<h1>Booking Not Allowed, This Is Demo Site. Go To <a href="'.base_url().'">Travelomatix</a></h1>';
	}

	/**
	 * Redirection to payment gateway
	 * @param string $book_id		Unique string to identify every booking - app_reference
	 * @param number $book_origin	Unique origin of booking
	 */
	public function payment($book_id,$book_origin, $card='', $month='', $year='')
	{
		//redirect('payment_gateway/demo_booking_blocked');//Blocked the payment gateway temporarly

		$this->load->model('transaction');
		$PG = $this->config->item('active_payment_gateway');
		load_pg_lib ( $PG );

		$pg_record = $this->transaction->read_payment_record($book_id);
		
		//Converting Application Payment Amount to Pyment Gateway Currency
		$pg_record['amount'] = roundoff_number($pg_record['amount']*$pg_record['currency_conversion_rate']);
		if (empty($pg_record) == false and valid_array($pg_record) == true) {
			$params = json_decode($pg_record['request_params'], true);
			$pg_initialize_data = array (
				'txnid' => $params['txnid'],
				'pgi_amount' => ceil($pg_record['amount']),
				'firstname' => $params['firstname'],
				'email'=>$params['email'],
				'phone'=>$params['phone'],
				'productinfo'=> $params['productinfo'],
				'card-number'=> $card,
				'month'=> $month,
				'year'=> $year,
			);
			// print_r($pg_initialize_data);
		} else {
			echo 'Under Construction :p';
			exit;
		}
		
		//defined in provab_config.php
		$payment_gateway_status = $this->config->item('enable_payment_gateway');
		if ($payment_gateway_status == true) {
			$this->pg->initialize ( $pg_initialize_data );
			$page_data['pay_data'] = $this->pg->process_payment ();
			// debug($page_data['pay_data']);
			// debug(array(
			// 	'description' => $page_data['pay_data']['description'],
			// 	'boking_id' => $book_id,
			// 	'status' => $page_data['pay_data']['status'],
			// ));
			$this->auth_success($page_data['pay_data']['description'],$book_id,$page_data['pay_data']['status']);
			/*debug($page_data['pay_data']); exit();
			//Not to show cache data in browser
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			echo $this->template->isolated_view('payment/'.$PG.'/pay', $page_data);*/
		} else {
			echo "directly going to process booking";
			exit();
			// $temp_booking = $this->custom_db->single_table_records('temp_booking','',array('book_id' => $book_id));
			// $book_origin = $temp_booking ['data'] ['0'] ['id'];
			// $this->redirect_booking($params['productinfo'], $params['txnid'], $book_origin);
		}
	}

	/**
	 * Redirection to payment gateway
	 * @param string $book_id		Unique string to identify every booking - app_reference
	 * @param number $book_origin	Unique origin of booking
	 */
	/*public function payment($book_id,$book_origin)
	{
		$this->load->model('transaction');
		$PG = $this->config->item('active_payment_gateway');
		load_pg_lib ( $PG );

		$pg_record = $this->transaction->read_payment_record($book_id);
		// debug($pg_record);exit;
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
				'book_id' => $book_id 
		) );
		$book_origin = $temp_booking ['data'] ['0'] ['id'];

		if (empty($pg_record) == false and valid_array($pg_record) == true) {
			$params = json_decode($pg_record['request_params'], true);
			
			$pg_initialize_data = array (
				'txnid' => $params['txnid'],
				'pgi_amount' => ceil($pg_record['amount']),
				'firstname' => $params['firstname'],
				'email'=>$params['email'],
				'phone'=>$params['phone'],
				'productinfo'=> $params['productinfo']
			);
		} else {
			echo 'Under Construction :p';
			exit;
		}
		//defined in provab_config.php
		$payment_gateway_status = $this->config->item('enable_payment_gateway');
		if ($payment_gateway_status == true) {
			// $this->pg->initialize ( $pg_initialize_data );
			// $page_data['pay_data'] = $this->pg->process_payment ();
			// //Not to show cache data in browser
			// header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			// header("Cache-Control: post-check=0, pre-check=0", false);
			// header("Pragma: no-cache");
			// echo $this->template->isolated_view('payment/'.$PG.'/pay', $page_data);
		} else {
			//directly going to process booking
			$this->redirect_booking($params['productinfo'], $params['txnid'], $book_origin);
		}
	}*/

	/**
	 *
	 */

	public function auth_success($info, $txn_id, $status)
	{
		$this->load->model('transaction');
		$product = $info; // $_REQUEST ['productinfo'];
		$book_id = $txn_id; //$_REQUEST ['txnid'];
		
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
				'book_id' => $book_id 
		) );
		$pg_status = lcfirst($status); //$_REQUEST['status'];
		$pg_record = $this->transaction->read_payment_record($book_id);
		$book_respd = substr($book_id,0,1);
		switch ($book_respd[0]) {
			case 'F':
				$product = META_AIRLINE_COURSE;
				break;
			case 'H':
				$product = META_ACCOMODATION_COURSE;
				break;
			case 'T':
				$product = META_CAR_COURSE;
				break;
			
			default:
				echo 'error in type of travel'; exit();
				break;
		}

		/*if ($pg_status == 'Success' and empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {*/
		if ($pg_status == 'success') {
			echo 'asd';
			//update payment gateway status
			$response_params = $_REQUEST;
			$this->transaction->update_payment_record_status($book_id, ACCEPTED, $response_params);
			$book_origin = $temp_booking ['data'] ['0'] ['id'];
			$this->redirect_booking($product, $book_id, $book_origin);

		}else{
			$this->auth_cancel($product,$book_id );
		}
	}
	function success() {
		$this->load->model('transaction');
		$product = $_REQUEST ['productinfo'];
		$book_id = $_REQUEST ['txnid'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
				'book_id' => $book_id 
		) );
		$pg_status = $_REQUEST['status'];
		$pg_record = $this->transaction->read_payment_record($book_id);
		if ($pg_status == 'success' and empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {
			//update payment gateway status
			$response_params = $_REQUEST;
			$this->transaction->update_payment_record_status($book_id, ACCEPTED, $response_params);
			$book_origin = $temp_booking ['data'] ['0'] ['id'];
			$this->redirect_booking($product, $book_id, $book_origin);

		}
	}

	private function redirect_booking($product, $book_id, $book_origin)
	{
		switch ($product) {
			case META_AIRLINE_COURSE :
				redirect ( base_url () . 'index.php/flight/process_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_BUS_COURSE :
				redirect ( base_url () . 'index.php/bus/process_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'index.php/hotel/process_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_CAR_COURSE :
				redirect ( base_url () . 'index.php/car/process_booking/' . $book_id . '/' . $book_origin );
				break;
			default :
				redirect ( base_url().'index.php/transaction/cancel' );
				break;
		}
	}

	/**
	 *
	 */
	
	function auth_cancel($prod, $txn_id ) {
		$this->load->model('transaction');
		$product = $prod;//$_REQUEST ['productinfo'];
		$book_id = $txn_id; //$_REQUEST ['txnid'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
				'book_id' => $book_id 
		) );
		$pg_record = $this->transaction->read_payment_record($book_id);
		if (empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {
			$response_params = array($product,$book_id);
			$this->transaction->update_payment_record_status($book_id, DECLINED, $response_params);
			$msg = "Payment Unsuccessful, Please try again.";
			switch ($product) {
				case META_AIRLINE_COURSE :
					redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_BUS_COURSE :
					redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_ACCOMODATION_COURSE :
					redirect ( base_url () . 'index.php/hotel/exception?op=booking_exception&notification=' . $msg );
					break;
			}
		}
	}


	function cancel() {
		$this->load->model('transaction');
		$product = $_REQUEST ['productinfo'];
		$book_id = $_REQUEST ['txnid'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
				'book_id' => $book_id 
		) );
		$pg_record = $this->transaction->read_payment_record($book_id);
		if (empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {
			$response_params = $_REQUEST;
			$this->transaction->update_payment_record_status($book_id, DECLINED, $response_params);
			$msg = "Payment Unsuccessful, Please try again.";
			switch ($product) {
				case META_AIRLINE_COURSE :
					redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_BUS_COURSE :
					redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_ACCOMODATION_COURSE :
					redirect ( base_url () . 'index.php/hotel/exception?op=booking_exception&notification=' . $msg );
					break;
			}
		}
	}


	function transaction_log(){
		load_pg_lib('PAYU');
		echo $this->template->isolated_view('payment/PAYU/pay');
	}

	/// B2B Instant Recharge
	function ssl_success() {
		error_reporting(E_ALL);
		
		$this->load->model('transaction');
		$validate_data = $_POST;
		$PG = $this->config->item('active_payment_gateway');
		load_pg_lib ($PG);
		// debug($_REQUEST);exit();
		// $validation = $this->pg->orderValidate($_POST['tran_id'], $_POST['amount'], $_POST['currency_type'], $_POST);
		// print_r($validation);
		if($_REQUEST['status'] == 'success')
		{
			$user_id = $this->entity_user_id;
			$cond = array('user_oid' => intval($user_id));
			$b2b_user_details = $this->custom_db->single_table_records ( 'b2b_user_details','balance', array (
				'user_oid' => $user_id 
			));
			$prev_balance = $b2b_user_details['data'][0]['balance'];
			$new_balance = $prev_balance + $_REQUEST['amount'];
			$this->custom_db->update_record('b2b_user_details',array("balance"=>$new_balance), $cond);
// exit();
			/*test{}*/
			$array = array(
				'system_transaction_id'=>$_REQUEST['txnid'],
				'amount'=>$_REQUEST['amount'],
				'currency'=>'INR',
				'currency_conversion_rate'=>1,
				'date_of_transaction'=>date('Y-m-d', strtotime($_REQUEST['addedon'])),
				'transaction_number'=>$_REQUEST['mihpayid'],
			);
			$insert_id = $this->domain_management_model->save_master_transaction_details_pg($array);
			
			redirect ( base_url () . 'index.php/management/b2b_balance_manager');

				// $product = $_REQUEST ['firstname'];
				// $book_id = $_REQUEST ['txnid'];
				// $pg_status = $_REQUEST['status'];
				
				// if ($pg_status == 'success') {
				// 	//update payment gateway status
				// 	$insert_id = $this->domain_management_model->save_master_transaction_details_pg($_REQUEST);

				// 	if(!empty($insert_id)){
						
				// 		$trns_details = $this->custom_db->single_table_records ( 'master_transaction_details', '', array (
				// 				'origin' => $insert_id 
				// 			));
						

				// 		$prev_balance = $b2b_user_details['data'][0]['balance'];
				// 		$pg_amount = $trns_details['data'][0]['amount'];
				// 		$conv_rate = $trns_details['data'][0]['currency_conversion_rate'];
				// 		$pg_total_amount = $pg_amount * $conv_rate;
				// 		$new_balance = $prev_balance + $pg_total_amount;
					
						
				// 		$this->custom_db->update_record('b2b_user_details',array("balance"=>$new_balance), $cond);
						
				// 		redirect ( base_url () . 'index.php/management/b2b_balance_manager');
				// 	}
				// }else{
				// 	echo 'Invalid';
				// }
				
		}else {
			echo 'not validate';
		}
		
	}
	function ssl_success_old() {
		
		$this->load->model('transaction');
		$validate_data = $_POST;
		$PG = $this->config->item('active_payment_gateway');
		load_pg_lib ($PG);
		$validation = $this->pg->orderValidate($_POST['tran_id'], $_POST['amount'], $_POST['currency_type'], $_POST);
		
		if($validation == SUCCESS_STATUS)
		{
				$product = $_POST ['value_a'];
				$book_id = $_POST ['tran_id'];
				$pg_status = $_POST['status'];
				
				if ($pg_status == 'VALID') {
					//update payment gateway status
					$insert_id = $this->domain_management_model->save_master_transaction_details_pg($_POST);

					if(!empty($insert_id)){
						$user_id = $this->entity_user_id;
						$trns_details = $this->custom_db->single_table_records ( 'master_transaction_details', '', array (
								'origin' => $insert_id 
							));
						$b2b_user_details = $this->custom_db->single_table_records ( 'b2b_user_details','balance', array (
								'user_oid' => $user_id 
							));

						$prev_balance = $b2b_user_details['data'][0]['balance'];
						$pg_amount = $trns_details['data'][0]['amount'];
						$conv_rate = $trns_details['data'][0]['currency_conversion_rate'];
						$pg_total_amount = $pg_amount * $conv_rate;
						$new_balance = $prev_balance + $pg_total_amount;
					
						$cond = array('user_oid' => intval($user_id));
						$this->custom_db->update_record('b2b_user_details',array("balance"=>$new_balance), $cond);
						
						redirect ( base_url () . 'index.php/management/b2b_balance_manager');
					}
				}else{
					echo 'Invalid';
				}
				
		}else {
			echo 'not validate';
		}
		
	}

	function ssl_fail() {
		//echo 'fail';die();
		redirect ( base_url () . 'index.php/management/b2b_balance_manager');
		
	}
	function ssl_cancel() {
		//echo 'cancel';die();
		redirect ( base_url () . 'index.php/management/b2b_balance_manager');
	}
}