<!-- HTML BEGIN -->
<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/provab-forms.css">

<style type="text/css">
	.panel-default > a {
    background-color: #f58830;
    border-color: #f58830;
    border-radius: 3px;
    color: #fff;
    font-size: 14px;
    height: 34px;
    margin: 10px 5px 0 0;
    padding: 6px 12px;
    float: right;
    vertical-align: middle;
}
</style>

<div class="bodyContent">
	<div class="panel panel-default clearfix">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<i class="fa fa-credit-card"></i> Room Types List
			</div>
		</div>
		<a href="<?php echo site_url()."/hotels/add_room_types"; ?>" class="btn btn-primary addnwhotl pull-right">Add Room Type</a>
		<div class="clearfix"></div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<div class="table-responsive">
				<form action="" method="POST" autocomplete="off">
					<table class="table table-striped">
						<tr>
							<th>Sl No</th>
							<th>Room Type Name</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
				<tbody>
					<?php if($room_types_list!=''){ for($a=0;$a<count($room_types_list);$a++){ ?>
						<tr>
							<td><?php echo ($a+1); ?></td>
							<td><?php echo $room_types_list[$a]->room_type_name; ?></td>
							<td>
								<?php if($room_types_list[$a]->status == "ACTIVE"){ ?>
									<a type="button" class="btn btn-green btn-icon icon-left">Active<i class="entypo-check"></i></a>
								<?php }else{ ?>
										<a type="button" class="btn btn-orange btn-icon icon-left">InActive<i class="entypo-cancel"></i></a>
								<?php } ?>
							</td>
							<td class="center">
								<?php if($room_types_list[$a]->status == "ACTIVE"){ ?>
									<a href="<?php echo site_url()."/hotels/inactive_room_types/".base64_encode(json_encode($room_types_list[$a]->room_type_id)); ?>" class="btn btn-orange btn-sm btn-icon icon-left margin_right_3"><i class="entypo-eye"></i>InActive</a>
								<?php }else{ ?>
									<a href="<?php echo site_url()."/hotels/active_room_types/".base64_encode(json_encode($room_types_list[$a]->room_type_id)); ?>" class="btn btn-green btn-sm btn-icon icon-left padding_right_19 margin_right_3"><i class="entypo-check"></i>Active</a>
								<?php } ?>
								<a href="<?php echo site_url()."/hotels/edit_room_types/".base64_encode(json_encode($room_types_list[$a]->room_type_id)); ?>" class="btn btn-default btn-sm btn-icon icon-left margin_right_3"><i class="entypo-pencil"></i>Edit</a>				
								<a href="<?php echo site_url()."/hotels/delete_room_types/".base64_encode(json_encode($room_types_list[$a]->room_type_id)); ?>" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
							</td>
						</tr>
					<?php }} ?>												
					</tbody>
				</table>
				</form>
			</div>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL END -->
</div>

<!-- Page Ends Here -->
