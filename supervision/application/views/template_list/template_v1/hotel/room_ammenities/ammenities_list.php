<!-- HTML BEGIN -->
<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/provab-forms.css">
<?php
//debug($ammenities_list);die;
?>
<style type="text/css">
	.panel-default > a {
    background-color: #f58830;
    border-color: #f58830;
    border-radius: 3px;
    color: #fff;
    font-size: 14px;
    height: 34px;
    margin: 10px 5px 0 0;
    padding: 6px 12px;
    float: right;
    vertical-align: middle;
}
</style>

<div class="bodyContent">
	<div class="panel panel-default clearfix">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<i class="fa fa-credit-card"></i> Amenities List
			</div>
		</div>
		<a href="<?php echo site_url()?>/hotels/add_room_ammenties" class="btn btn-primary addnwhotl pull-right">Add Room Amenity</a>
		<div class="clearfix"></div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<div class="table-responsive">
				<form action="" method="POST" autocomplete="off">
					<table class="table table-striped">
						<tr>
							<th>Sl No</th>
							<th>Room Amenity Name</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
				<tbody>
					<?php if($ammenities_list!=''){ for($a=0;$a<count($ammenities_list);$a++){ ?>
						<tr>
							<td><?php echo ($a+1); ?></td>
							<td><?php echo $ammenities_list[$a]->amenities_name; ?></td>
							<td>
								<?php if($ammenities_list[$a]->status == "ACTIVE"){ ?>
									<button type="button" class="btn btn-green btn-icon icon-left">Active<i class="entypo-check"></i></button>
								<?php }else{ ?>
										<button type="button" class="btn btn-orange btn-icon icon-left">InActive<i class="entypo-cancel"></i></button>
								<?php } ?>
							</td>
							<td class="center">
								<?php if($ammenities_list[$a]->status == "ACTIVE"){ ?>
									<a href="<?php echo site_url()."/hotels/inactive_room_ammenity/".base64_encode(json_encode($ammenities_list[$a]->hotel_amenities_id)); ?>" class="btn btn-orange btn-sm btn-icon icon-left margin_right_3"><i class="entypo-eye"></i>InActive</a>
								<?php }else{ ?>
									<a href="<?php echo site_url()."/hotels/active_room_ammenity/".base64_encode(json_encode($ammenities_list[$a]->hotel_amenities_id)); ?>" class="btn btn-green btn-sm btn-icon icon-left margin_right_3 padding_right_19"><i class="entypo-check"></i>Active</a>
								<?php } ?>
								<a href="<?php echo site_url()."/hotels/edit_room_ammenity/".base64_encode(json_encode($ammenities_list[$a]->hotel_amenities_id)); ?>" class="btn btn-default btn-sm btn-icon icon-left margin_right_3"><i class="entypo-pencil"></i>Edit</a>				
								<a href="<?php echo site_url()."/hotels/delete_room_ammenity/".base64_encode(json_encode($ammenities_list[$a]->hotel_amenities_id)); ?>" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
							</td>
						</tr>
					<?php }} ?>												
					</tbody>
				</table>
				</form>
			</div>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL END -->
</div>

<!-- Page Ends Here -->
