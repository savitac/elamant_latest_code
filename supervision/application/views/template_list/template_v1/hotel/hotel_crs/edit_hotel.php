<!-- HTML BEGIN -->
<head>
<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/provab-core.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/provab-theme.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/provab-forms.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/custom.css">
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/assets/js/daterangepicker/daterangepicker-bs3.css"> 
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/js/daterangepicker/daterangepicker-bs3.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/js/select2/select2.css">

</head>
<style> 
 .tab_error_color {
 	color: red !important;
 } 
 .tab_msg_color {
 	background: blue !important;
 }
</style>	

<div class="bodyContent">
	<div class="panel panel-default clearfix">
		<!-- PANEL WRAP START -->
		 <?=$GLOBALS['CI']->template->isolated_view('hotel/hotel_widgets',['hotel_id'=>$hotel_id,'hotel_name'=>$hotel_name, 'active'=>$wizard_status, 'current'=>'step1'])?> 
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<i class="fa fa-credit-card"></i> Hotel Management
			</div>
		</div>
		<!-- PANEL HEAD START -->
		
			<!-- PANEL BODY START -->
			<div class="panel-body">
				<form method="post" id="hotel11" name="hotel11" action="<?php echo site_url()."/hotels/update_hotel/".base64_encode(json_encode($hotels_list[0]->hotel_details_id)); ?>" class="form-horizontal form-groups-bordered validate" enctype= "multipart/form-data">				
					<fieldset form="user_edit">
				<legend class="form_legend">Edit Hotel</legend>
				
								<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Hotel Type</label>									
									<div class="col-sm-5">
										<select id="hotel_type" name="hotel_type" class="form-control">
											 <option value="0">Select Hotel Type</option>
											<?php foreach ($hotel_types_list as $type){ ?>
												<option value="<?php echo $type->hotel_type_id; ?>" <?php if($hotels_list[0]->hotel_type_id==$type->hotel_type_id) echo "Selected"; ?> data-iconurl=""><?php echo $type->hotel_type_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Hotel Name</label>									
									<div class="col-sm-5">
										<input type="text" class="form-control" name="hotel_name" placeholder="Hotel Name" data-validate="required" data-message-required="Please enter the Hotel Name" value="<?php echo $hotels_list[0]->hotel_name; ?>">
									</div>
								</div>
								<?php  $contract_expire_date = explode(",", $hotels_list[0]->contract_expire_date);  ?>
									<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Contract Expire Date</label>									
									<div class="col-sm-5">
										<input type="text" readonly class="form-control logpadding dateFrom"  id="datepickerform" name="exclude_checkout_date" value="<?php echo date('d/m/Y',strtotime($hotels_list[0]->contract_expire_date)); ?>" id="exclude_checkout_date" data-start-date="d" data-min-date="<?php echo date('m-d-Y');?>" data-validate="required" data-message-required="Please Select the Date Range" />
									</div>
								</div>
						<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Star Rating</label>									
									<div class="col-sm-5">
										<?php //debug($hotels_list[0]->star_rating); ?>
										<select id="star_rating" name="star_rating" class="form-control">
											 <option value="">Select Rating</option>
											
											<option value="0" <?php if($hotels_list[0]->star_rating == 1){echo "selected";} ?>>1</option>
											<option value="2" <?php if($hotels_list[0]->star_rating == 2){echo "selected";} ?>>2</option>
											<option value="3" <?php if($hotels_list[0]->star_rating == 3){echo "selected";} ?>>3</option>
											<option value="4" <?php if($hotels_list[0]->star_rating == 4){echo "selected";} ?>>4</option>
											<option value="5" <?php if($hotels_list[0]->star_rating == 5){echo "selected";} ?>>5</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Hotel Info</label>									
									<div class="col-sm-5">

										<textarea class="form-control" name="hotel_info" placeholder="Hotel Info" data-message-required="Please enter the Hotel Info"><?php echo $hotels_list[0]->hotel_info; ?></textarea>
									</div>
								</div>
								<div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Country</label>									
                                        <div class="col-sm-5">                                            
                                            <select name="country" id="country" onchange="select_city(this.value)" class="form-control" required>
                                                <option value="0">Select Country</option>                                                    
                                                <?php for ($c = 0; $c < count($country); $c++) { ?>
                                                    <option value="<?php echo $country[$c]->country_name; ?>" <?php if($hotels_list[0]->country_id == $country[$c]->country_name){ echo "selected"; }  ?> data-iconurl=""><?php echo $country[$c]->country_name; ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php echo form_error('country_id', '<span for="field-1" class="validate-has-error">', '</span>'); ?>
                                        </div>                                        
                                </div>  

                                <div class="form-group" id="api">
									<label for="field-1"  class="col-sm-3 control-label">City</label>									
									<div class="col-sm-5">
									<input type="text" id="city_name" class="form-control" name="city_name" value="<?php echo $hotels_list[0]->city_details_id; ?>">                                
										<!-- <select name="city_name"  class="form-control" id="city_name" onchange="select_location(this.value)">											
										<option value="0">Select</option>
										</select> -->
                                        <?php echo form_error('city_name',  '<span for="field-1" class="validate-has-error">', '</span>'); ?>
									</div>
								 </div> 
								
								  <?php  if(isset($hotels_list[0]->hotel_amenities)){ $sel_ammenities = explode(",",$hotels_list[0]->hotel_amenities); } ?>
								<div class="form-group">							  	   	
									<label for="field-1" class="col-sm-3 control-label">Ammenities</label>									
									<div class="col-sm-5">
										 <select id="ammenities" name="ammenities[]" class="qweselect2" multiple style="width:500px">										 
											<?php 
											$ammenities = $this->hotels_model->get_ammenities_list();

											for($a=0; $a<count($ammenities); $a++){ ?>
												<option value="<?php echo $ammenities[$a]->hotel_amenities_id; ?>" data-iconurl="" 
												<?php if(isset($sel_ammenities)){ if(count($sel_ammenities) > 0){
													if(in_array($ammenities[$a]->hotel_amenities_id,$sel_ammenities)){
														echo "selected";
													}
												} }
												?>
												>
												<?php echo $ammenities[$a]->amenities_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>	
								<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Hotel Map</label>									
									<div class="col-sm-5">
                                       <div id="map_canvas" style="height:300px;width:700px;margin: 0.6em;"></div>
                                      </div>
                                    </div>
                                
                                <div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Hotel Address</label>									
									<div class="col-sm-5">
										<textarea class="form-control"  name="hotel_address" placeholder="Hotel Address" data-validate="required" data-message-required="Please enter the Hotel Address" id="hotel_address" rows="7"><?php echo $hotels_list[0]->hotel_address; ?></textarea>
									</div>                                     
								</div>
								<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Latitude</label>									
									<div class="col-sm-5">
										<input type="text" class="form-control" name="latitude" onblur="getmap()" placeholder="Latitude" data-validate="" data-message-required="Please enter the Latitude of Hotel" value="<?php echo $hotels_list[0]->latitude; ?>" id="lat">
									</div>
								</div>
								<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Longitude</label>									
									<div class="col-sm-5">
										<input type="text" class="form-control" name="longitude" onblur="getmap()" placeholder="Longitude" data-validate="" data-message-required="Please enter the Longitude of Hotel" value="<?php echo $hotels_list[0]->longitude; ?>" id="lng">
									</div>
								</div>

								<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Postal Code</label>									
									<div class="col-sm-5">										
										<input type="text" class="form-control" readonly name="postal_code" placeholder="Postal Code"  data-message-required="Please enter Postal Code" value="<?php echo $hotels_list[0]->postal_code; ?>"  id="postal_code">
									</div>
								</div>
								<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Phone Number</label>									
									<div class="col-sm-5">
										<input type="text" class="form-control" data-rule-number="true" id="phone_number" name="phone_number" maxlength="15" placeholder="Phone Number" data-validate="required" data-message-required="Please enter the Phone Number" value="<?php echo $hotels_list[0]->phone_number; ?>" >
									</div>
								</div>
								<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Email Address</label>									
									<div class="col-sm-5">
										<input type="text" class="form-control" id="email" name="email" maxlength="50" onBlur="checkUniqueEmail(this.value)" placeholder="Email Address" data-validate="email,required" data-message-required="Please enter the Valid Email Id" value="<?php echo $hotels_list[0]->email; ?>" >
									</div>
								</div>
								<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label">Hotel Cancellation</label>									
									<div class="col-sm-5">
										 <select id="hotel_cancellation" name="hotel_cancellation" class="form-control">										 
											 <option value="0" <?php if($hotels_list[0]->cancellation_status == 0){echo "selected";} ?>>Not Applicaple</option>
											 <option value="1" <?php if($hotels_list[0]->cancellation_status == 1){echo "selected";} ?>>Applicable</option>
											
										</select>
									</div>
								</div>

								<div id="cancellation">
									<div class="form-group">
										<label for="field-1" class="col-sm-3 control-label">Cancellation Description</label>									
										<div class="col-sm-5">
											<textarea class="form-control" id="cancellation_dexcription" name="cancellation_dexcription" placeholder="Cancellation Description" ><?php echo @$cancellation_details[0]->cancellation_description; ?></textarea>
										</div>
									</div>
									<input type="hidden" name="hotel_cancellation_id" value="<?php echo @$cancellation_details[0]->hotel_cancellation_id; ?>">
									
									
								</div>
								<div class="form-group">
									<label for="field-3" class="col-sm-3 control-label">Upload Thumb Image</label>									
									<div class="col-sm-5">
										<div class="fileinput fileinput-new" data-provides="fileinput">
											<div class="fileinput-new thumbnail" data-trigger="fileinput">
												<?php if( $hotels_list[0]->thumb_image == ''){ ?>
													<img src="<?php echo base_url(); ?>assets/images/logo.png" alt="Hotel Logo">
												<?php }else{ ?>
													<img src="<?php echo base_url(); ?>uploads/hotel_images/<?php echo $hotels_list[0]->thumb_image; ?>" alt="Hotel Image">
												<?php } ?>
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail"></div>
											<div>
												<span class="btn btn-white btn-file">
													<span class="fileinput-new">Select image</span>
													<span class="fileinput-exists">Change</span>
													<input type="file" value="<?php echo $hotels_list[0]->thumb_image; ?>" name="thumb_image" accept="image/*">
													<input type="hidden" value="<?php echo $hotels_list[0]->thumb_image; ?>" name="thumb_image_old">
												</span>
												<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
											</div>
										</div>										
									</div>
								</div>


								<?php $hotel_images = explode(',', $hotels_list[0]->hotel_images);  ?>

							<div class="form-group">
									<label for="field-3" class="col-sm-3 control-label">Upload Hotel Images</label>									
									<div class="col-sm-5">
										<div class="fileinput fileinput-new" data-provides="">
											<?php foreach($hotel_images as $hotel_image){ ?>
											<div class="fileinput-new thumbnail col-xs-12" data-trigger="">
												<?php if( $hotel_image == ''){ ?>
													<img src="<?php echo base_url(); ?>assets/images/logo.png" alt="Airline Logo">
												<?php }else{ ?>
													<img src="<?php echo base_url(); ?>uploads/hotel_images/<?php echo $hotel_image; ?>" alt="Hotel Image">
													<a href="<?php echo base_url()."hotels/delete_image/".$hotel_id.'/'.$hotel_image; ?>" class="btn btn-danger btn-sm btn-icon icon-left action-details1" onclick="return deletechecked();"><i class="entypo-cancel"></i>Delete</a>
												<?php } ?>
											</div> <?php //echo 'sanjay'; print_r($hotel_id); ?>
											
											<?php } ?>
											<div class="fileinput-preview fileinput-exists thumbnail"></div>
											<div class="col-xs-12 nopad">
												<span class="btn btn-white btn-file">
													<span class="fileinput-new">Select image</span>
													<span class="fileinput-exists">Change</span>
													<input name="hotel_image[]" type="file" multiple />
													<input type="hidden" value="<?php echo $hotels_list[0]->hotel_images; ?>" name="hotel_image_old">
												</span>
												<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
											</div>
										</div>										
									</div>
								</div>
								<?php if ($this->session->userdata('lgm_supplier_admin_logged_in') == "Logged_In") {
									
								} else{ ?>
								<div class="form-group">
									<label class="col-sm-3 control-label">Status</label>									
									<div class="col-sm-5">
										<div id="label-switch" class="make-switch" data-on-label="Active" data-off-label="InActive">
											<input type="checkbox" name="status" value="<?php echo $hotels_list[0]->status; ?>" id="status" <?php if($hotels_list[0]->status == "ACTIVE"){ ?> checked <?php } ?> >
										</div>
									</div>
								</div>
								<?php } ?>
						<!-- 	<div class="form-group">                                    
                                    <label for="field-1" class="col-sm-3 control-label">Hotel Complimentary</label>									
									<div class="col-sm-5">
										<textarea class="form-control" id="hotel_complimentary" name="hotel_complimentary" placeholder="Hotel Complimentary" rows="7"></textarea>
									</div> 								
							</div> -->
							<?php /*if ($this->session->userdata('lgm_supplier_admin_logged_in')) {
							
							}else { ?>
							<div class="form-group">
									<label class="col-sm-3 control-label">Top Deals</label>									
									<div class="col-sm-5">
										<div id="label-switch" class="make-switch top_deals" data-on-label="Enable" data-off-label="Disabe">
											<input type="checkbox" name="top_deals" value="1" id="top_deals" checked>
										</div>
									</div>
								</div>
								<?php }*/ ?>

								<div class="form-group">
									<label class="col-sm-3 control-label">&nbsp;</label>									
									<div class="col-sm-5">
										<button type="submit" class="btn btn-success">Update Hotel</button>
									</div>
								</div>
				</form>
			</div>
		
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL END -->
</div>

<!-- Page Ends Here -->
<!--Load Js--> 
	<script src="<?php echo base_url(); ?>hotel_assets/js/gsap/main-gsap.js"></script>
	<script src="<?php echo base_url(); ?>hotel_assets/js/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>hotel_assets/js/store.min.js"></script>
	<script src="<?php echo base_url(); ?>hotel_assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?php echo base_url(); ?>hotel_assets/js/bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>hotel_assets/js/joinable.js"></script>
	<script src="<?php echo base_url(); ?>hotel_assets/js/resizeable.js"></script>
	<script src="<?php echo base_url(); ?>hotel_assets/js/jquery.validate.min.js"></script>
	<script src="<?php echo base_url(); ?>hotel_assets/js/provab-login.js"></script>
	<script src="<?php echo base_url(); ?>hotel_assets/js/provab-api.js"></script>
	<script src="<?php echo base_url(); ?>hotel_assets/js/jquery-idleTimeout.js"></script>
	
	<script src="<?php echo base_url(); ?>hotel_assets/js/provab-custom.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-switch.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>	
	<script src="<?php echo base_url(); ?>assets/js/fileinput.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/ckeditor/adapters/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>

	<script src="<?php echo base_url(); ?>assets/js/select2/select2.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>

  <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/datatables/TableTools.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/datatables/lodash.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/datatables/responsive/js/datatables.responsive.js"></script>   
 <!--    <script src="<?= base_url(); ?>assets/js/plugins/datatables/dataTables.overrides.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/js/plugins/lightbox/lightbox.min.js" type="text/javascript"></script>
 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css" type="text/css">-->
  
  
  
  
<script type="text/javascript">
	$(document).ready(function () {
			$('#datepickerform').datepicker({
				dateFormat: 'dd/mm/yy',
				minDate: 0,
				//firstDay: 1,
				//maxDate: "+1Y",
			});
			var search_city  = $('#city_name').val();
			var country = $('#country').val();
		 	if(search_city!=''){
		 		geocodeAddress(search_city+','+country);	
		 	}
		});
	$("#ammenities").select2();
		$(document).ready(function(){
			var canc = "<?php echo $hotels_list[0]->cancellation_status; ?>";
			if(canc == 1){
				$("#cancellation").show();
			}else{
				$("#cancellation").show();
			}
			
		})

		function addMoreRooms1() {
			$("#cancellation_clone").css({'display':'inherit'});
			var id = $('#rows_cnt').val();
			
	    	$("#cancellation_clone").append( '<div class="form-group" style="widht:80%;" ><div class="col-sm-2">'+								
								'<input type="text" class="form-control" name="cancellation_from[]" id="cancellation_from'+id+'" value="">'+
								'</div>'+
								
								'<div class="col-sm-3">'+							
								'<input type="text" class="form-control" name="cancellation_nightcharge[]" id="cancellation_nightcharge'+id+'" value="">'+
								'</div>'+
								'<div class="col-sm-3 ">'+							
								' <input type="text" class="form-control" value="" name="cancellation_percentage[]" id="cancellation_percentage'+id+'" value=""> </div></div>');																				
			id = parseInt(id)+1;
			$('#rows_cnt').val(id);																

		}

		function removeLastRoom1(v){
			var id = $('#rows_cnt').val();
			$('#cancellation_clone .form-group').last().remove();
			if(id <= 1) {
				$("#cancellation_clone").css({'display':'none'});
			}
			id = parseInt(id)-1;
			$('#rows_cnt').val(id);
		}

		$("#hotel_cancellation").on("change",function(){
			var hotel_cancellation = $("#hotel_cancellation").val();
			if(hotel_cancellation == 0){
				$("#cancellation").hide();
			}else{
				$("#cancellation").show();
			}
		});


	</script>
	<script>
		 jQuery(document).ready(function($){
			
                    var country_id = $( "#country" ).val();
                    var from_selected_city_id = "<?php if(isset($hotels_list[0]->city_details_id)) { echo $hotels_list[0]->city_details_id; } ?>";                                        
                    //alert(from_selected_city_id)
                    if (country_id != '') {
                        var select1 = $('#city_name');
                        $.ajax({
                            url: '<?php echo site_url(); ?>/hotel/get_city_name/' + country_id + '/' + from_selected_city_id,
                            success: function (data, textStatus, jqXHR) {                            
                                select1.html('');
                                //alert(data)
                                select1.html(data);
                                select1.trigger("chosen:updated");                                                                             		   			
                        	}
                         });
                    } 
                    var city_id = "<?php if(isset($hotels_list[0]->city_details_id)) { echo $hotels_list[0]->city_details_id; } ?>";
                    var selected_location_id = "<?php if(isset($hotels_list[0]->location_info)) { echo $hotels_list[0]->location_info; } ?>"; 
                    if (city_id != '') {
                        var location_select = $('#location_info');
                        $.ajax({
                            url: '<?php echo site_url(); ?>/hotel/get_location_name/' + city_id + '/' + selected_location_id,
                            success: function (data, textStatus, jqXHR) {                            
                                location_select.html('');
                                location_select.html(data);
                                location_select.trigger("chosen:updated");                                                                             		   			
                        	}
                         });
                    }      
      });
		 $(function(){			 
		 	$('#exclude_checkin_time').timepicker({
      			pickDate: false,
      			showMeridian: false
    		});
			$('#exclude_checkout_time').timepicker({
      			pickDate: false,
      			showMeridian: false
    		}); 

			$('#status').change(function(){
				var current_status = $('#status').val();
				if(current_status == "ACTIVE")
					$('#status').val('INACTIVE');
				else
					$('#status').val('ACTIVE');
			});	

			$('#hotel11').submit(function(){		 			
	 			var number_filter = /^[0-9]*$/;

				if(postal_code.value != '')
				{
					if(!(postal_code.value.match(number_filter)))
					{
						postal_code.style.border = "1px solid #f52c2c";   
						postal_code.focus(); 
						return false; 
					}
				}
				else
				{
					postal_code.style.border = "1px solid #f52c2c";   
					postal_code.focus(); 
					return false; 
				}
				
				if(postal_code.value == '') {
					    postal_code.style.border = "1px solid #f52c2c";   
						postal_code.focus(); 
						return false; 
				}

				if(postal_code.value.length > 8 ) {
					    postal_code.style.border = "1px solid #f52c2c";   
						postal_code.focus(); 
						return false; 
				}	

				if(location_info.value == "select" && location_name.value == ""){
					location_info.style.border = "1px solid #f52c2c";   
					location_name.style.border = "1px solid #f52c2c";   										
					return false;
				}

				var hotel_type_id = $('#hotel_type').val();				
				if(hotel_type_id == "0"){					
					hotel_type.style.border = "1px solid #f52c2c";   
					hotel_type.focus(); 
					return false; 			
				}				

				var country_id = $('#country').val();      				
				if(country_id == "0"){										
					country.style.border = "1px solid #f52c2c";   
					country.focus(); 
					return false; 		
				}	

				var city_id = $('#city_name').val();
				if(city_id == "0"){					
					city_name.style.border = "1px solid #f52c2c";   
					city_name.focus(); 
					return false; 		
				}	

				if(phone_number.value.length > 50 ) {
					    phone_number.style.border = "1px solid #f52c2c";   
						phone_number.focus(); 
						return false; 
				}				
						
				

			});	

		}); 	

		function select_city(country_id){				 
		 if (country_id != '') {         	  
          var select1 = $('#city_name'); 
          $.ajax({
            url: '<?php echo site_url(); ?>/hotel/get_city_name/' + country_id,
            success: function (data, textStatus, jqXHR) {                                    
              select1.html('');
              select1.html(data);
              select1.trigger("chosen:updated");  
          	}
           });         
         }		
		}
		
		function select_location(city_id){				 
		 if (city_id != '') {         	  
          var location_select = $('#location_info');          
          $.ajax({
            url: '<?php echo site_url(); ?>/hotel/get_location_name/' + city_id,
            success: function (data, textStatus, jqXHR) {                                    
              location_select.html('');
              location_select.html(data);
              location_select.trigger("chosen:updated");  
          	}
           });         
          }		
		 }
		
		$("#add_location").click(function(){
				$("#add_location").hide();
				$("#location_info").hide();
				$("#location_name").slideToggle("slow");
		});
		 
		
	</script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyCJfvWH36KY3rrRfopWstNfduF5-OzoywY"></script>
    <script>

    	$(function(){
    	
    	});
		var map;
		var geocoder;
		var mapOptions = { center: new google.maps.LatLng(0.0, 0.0), zoom: 2,
        mapTypeId: google.maps.MapTypeId.ROADMAP };
	
		function initialize() {				
			var latitude =  <?php echo $hotels_list[0]->latitude; ?>; 
			var longitude = <?php echo $hotels_list[0]->longitude; ?>;
			var myOptions = {
                center: new google.maps.LatLng(latitude,longitude),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };                     

            var newPosition = new google.maps.LatLng(latitude,longitude);                        
            geocoder = new google.maps.Geocoder();
            var map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);            
            marker = new google.maps.Marker({ //on créé le marqueur
                        position: newPosition, 
                        map: map
            });            

            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(event.latLng);
            });

            var marker;
            function placeMarker(location) {
            	alert("You are changing the Address of the hotel");
                if(marker){ //on vérifie si le marqueur existe
                    marker.setPosition(location); //on change sa position
                }else{
                    marker = new google.maps.Marker({ //on créé le marqueur
                        position: location, 
                        map: map
                    });
                }
                 document.getElementById('lat').value=location.lat();
                 document.getElementById('lng').value=location.lng();
                getAddress(location);
            }

			function getAddress(latLng) {
				geocoder.geocode( {'latLng': latLng},
				function(results, status) {
					if(status == google.maps.GeocoderStatus.OK) {
					  if(results[0]) {					 
						document.getElementById("hotel_address").value 	= results[0].formatted_address;
						var address = results[0].address_components;
						var zipcode = address[address.length - 1].long_name;
						//document.getElementById("city").value 		= results[0].address_components[1]['long_name'];
						document.getElementById("postal_code").value 	= zipcode;						
					  }
					  else {
						//document.getElementById("city").value = "No results";
					  }
					}
					else {
					  //document.getElementById("city").value = status;
					}
				});
			}
		}

      google.maps.event.addDomListener(window, 'load', initialize);

      function getmap(){	 		 	
	 	var edValue = document.getElementById("lat");
        lat = edValue.value;
      	var edValue = document.getElementById("lng");
        lng = edValue.value;        
        var newPosition = new google.maps.LatLng(lat,lng);
        if(lat > 0 && lng > 0){
           myOptions = {                
                center: new google.maps.LatLng(lat,lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            geocoder = new google.maps.Geocoder();
            map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);                        
            marker = new google.maps.Marker({ //on créé le marqueur
                        position: newPosition, 
                        map: map
            });            
            getAddress2(newPosition);        
       }        
	 }

	function getAddress2(latLng) {				
				geocoder.geocode( {'latLng': latLng},
				function(results, status) {
					if(status == google.maps.GeocoderStatus.OK) {
					  if(results[0]) {					 
						document.getElementById("hotel_address").value 	= results[0].formatted_address;
						var address = results[0].address_components;
						var zipcode = address[address.length - 1].long_name;
						//document.getElementById("city").value 		= results[0].address_components[1]['long_name'];
						document.getElementById("postal_code").value 	= zipcode;						
					  }
					  else {
						//document.getElementById("city").value = "No results";
					  }
					}
					else {
					  //document.getElementById("city").value = status;
					}
				});
			}
      function geocodeAddress(address) {
	 		geocoder.geocode({address:address}, function (results,status)
		      { 
		         if (status == google.maps.GeocoderStatus.OK) {
		          var p = results[0].geometry.location;
		          var lat=p.lat();
		          var lng=p.lng();
		          //createMarker(address,lat,lng);
		          ///alert(lng);
		          var myOptions = {
	                center: new google.maps.LatLng(lat, lng ),
			                //center: new google.maps.LatLng(-1.9501,30.0588),
			                zoom: 10,
			                mapTypeId: google.maps.MapTypeId.ROADMAP
			            };
              	 var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
              	 google.maps.event.addListener(map, 'click', function(event) {
	              	 	placeMarker(event.latLng);
		            });	

	              	 var marker;
		            function placeMarker(location) {            	
		                if(marker){ //on vérifie si le marqueur existe
		                    marker.setPosition(location); //on change sa position
		                }else{
		                    marker = new google.maps.Marker({ //on créé le marqueur
		                        position: location, 
		                        map: map
		                    });
		                }
		                 document.getElementById('lat').value=location.lat();
		                 document.getElementById('lng').value=location.lng();
		                getAddress(location);
		            }

					function getAddress(latLng) {				
						geocoder.geocode( {'latLng': latLng},
						function(results, status) {
							if(status == google.maps.GeocoderStatus.OK) {
							  if(results[0]) {					 
								document.getElementById("hotel_address").value 	= results[0].formatted_address;
								var address = results[0].address_components;
								var zipcode = address[address.length - 1].long_name;
								//document.getElementById("city").value 		= results[0].address_components[1]['long_name'];
								document.getElementById("postal_code").value 	= zipcode;						
							  }
							  else {
								//document.getElementById("city").value = "No results";
							  }
							}
							else {
							  //document.getElementById("city").value = status;
							}
						});
					}

		        }
		        
		      }
		    );
		  }

		
		 

      function addMoreRooms(c) {
			var id = $('#rows_cnt').val();
			$("#rooms").css({'display':'inherit'});
			$("#rooms").append('<div class="form-group"><label for="field-1" class="col-sm-3 control-label">Exclude Checkout Date</label><div class="col-md-5"><input type="text" class="form-control datepicker" name="exclude_checkout_date[]" id="exclude_checkout_date'+id+'" data-min-date="<?php echo date('m-d-Y');?>" data-validate="required" data-message-required="Please Select the Date Range" /></div></div>');
			$('.datepicker').datepicker();
			id = id+1;
			$('#rows_cnt').val(id);
		}

		function removeLastRoom(v){
			var id = $('#rows_cnt').val();
			$('#rooms .form-group').last().remove();
			id = id-1;
			$('#rows_cnt').val(id);
		}
		 
	
		 
		 <?php if(isset($hotels_list[0]->exclude_checkout_date) && $hotels_list[0]->exclude_checkout_date != '') {
		  $exclude_checkout_date = explode(",", $hotels_list[0]->exclude_checkout_date);
			  
		  for($min = 1; $min < count($exclude_checkout_date); $min++) {  ?>
		  addMoreRooms(null);
		  $('#exclude_checkout_date'+<?php echo $min; ?>).val("<?php echo $exclude_checkout_date[$min]; ?>");
		<?php } } ?>
	
	  /*function remove_image(hotel_id,image_name,id){
		if(hotel_id != '' && image_name != '') {
			$.ajax({
						url: '<?php echo site_url(); ?>/hotel/unlink_image/' + hotel_id + '/' + image_name,
						success: function (data, textStatus, jqXHR) {                            
							  $("#img_" + id).remove();                                                                       		   			
						}
					 });
					 }
	  }*/

	  function remove_image(hotel_id,image_name,id){
		if(hotel_id != '' && image_name != '') {


					$.ajax({
						url: '<?php echo site_url(); ?>/hotel/unlink_image/' + hotel_id + '/' + image_name,
						success: function (data, textStatus, jqXHR) {   
							$("#img_" + id).remove(); 

							var old_image_box = $('input[name=hotel_image_old]');   
							
							var oib_arr = old_image_box.val().split(',');

					     	var new_images = [];

					     	if(oib_arr.length>0)
				     		{	
				     			for(i=0;i<oib_arr.length;i++)
						     	{
						     		if(i!=(parseInt(id)))
						     		{
						     			new_images.push(oib_arr[i]);
						     		}
						     	}
						     	var new_images_a = new_images.join(',');
			     				$('input[name=hotel_image_old]').val(new_images_a);
				     		} else {
				     			$('input[name=hotel_image_old]').val('');
				     		}                                                               		   			
						}
					 });
					 }
	  }

	  function checkUniqueEmail(email){
			var sEmail = document.getElementById('email');
			if (sEmail.value != ''){
				var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
				if(!(sEmail.value.match(filter))){
					$("#email").val(email);
					return false; 
				}else{
				}
			}
			return false;
		}
</script>
<script>
function deletechecked()
    {
        if(confirm("Delete image"))
        {
            return true;
        }
        else
        {
            return false;  
        } 
    }
</script>
<script >
$('#top_deals').change(function(){
				var current_status = $('#top_deals').val();
				if(current_status == "1")
					$('#top_deals').val('0');
				else
					$('#top_deals').val('1');
			});
</script>