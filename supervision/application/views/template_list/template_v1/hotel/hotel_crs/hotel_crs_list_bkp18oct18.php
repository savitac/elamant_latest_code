<link rel="stylesheet" href="<?php echo base_url(); ?>hotel_assets/css/custom/theme.css">
<!--<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
      <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>-->

<style>



/* Show the tooltip text when you mouse over the tooltip container */

/* Show the tooltip text when you mouse over the tooltip container */

	.addnwhotl {  margin-right: 5px;  margin-top: 5px;}
	.stra_hotel {
	    display: block;
	    margin: 0px 0;
	    overflow: hidden;
	}
	.stra_hotel[data-star="5"] .fa:nth-child(1), .stra_hotel[data-star="5"] .fa:nth-child(2), .stra_hotel[data-star="5"] .fa:nth-child(3), .stra_hotel[data-star="5"] .fa:nth-child(4), .stra_hotel[data-star="5"] .fa:nth-child(5), .stra_hotel[data-star="4"] .fa:nth-child(1), .stra_hotel[data-star="4"] .fa:nth-child(2), .stra_hotel[data-star="4"] .fa:nth-child(3), .stra_hotel[data-star="4"] .fa:nth-child(4), .stra_hotel[data-star="3"] .fa:nth-child(1), .stra_hotel[data-star="3"] .fa:nth-child(2), .stra_hotel[data-star="3"] .fa:nth-child(3), .stra_hotel[data-star="2"] .fa:nth-child(1), .stra_hotel[data-star="2"] .fa:nth-child(2), .stra_hotel[data-star="1"] .fa:nth-child(1) {
    color: #fb9817;
	}
</style>
<!-- HTML BEGIN -->
<div class="bodyContent">
	<div class="panel panel-default clearfix">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<i class="fa fa-credit-card"></i> Hotel Management
			</div>
		</div>
		<a href="<?php echo site_url()."/hotels/add_hotel"; ?>" class="btn btn-primary addnwhotl pull-right">Add Hotel</a>
		<!-- PANEL HEAD START -->
		<div class="panel-body pull-left">
			<!-- PANEL BODY START -->
			<div class="table-responsive">
				<form action="" method="POST" autocomplete="off">
					<table class="table table-striped">
						<tr>
							<th>Sl No</th>
							<th>
								<div class="btn-group dropdown_hover" role="group">
									<div class="align_line">
										<input type="checkbox" id="selectall" onchange="checkAll(this)">
										<button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="glyphicon glyphicon-chevron-down"></span>
										</button>
										<div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="display: none;">
											<a href="#" class="" onclick="activate_hotels(this)"> Activate</a>
											<a href="#" class="" onclick="inactivate_hotels(this)"> Inactivate</a>
										</div>
									</div>
								</div>

							</th>
							<th>Hotel Code</th>
							<th>Hotel Name</th>
							<th>Hotel Logo</th>	
							<th width="100px">Star Rating</th>
							<th>Hotel Amenities</th>
							<th>Hotel Address</th>
							<th>Contact Number</th>	
							<!--<th>Added By</th>-->
							<th>Status</th>
							<th>Actions</th>											
						</tr>
				<tbody>
					<?php  if(isset($hotels_list['all_hotels']) && $hotels_list['all_hotels']!=''){
					 for($a=0;$a<count($hotels_list['all_hotels']);$a++){ 					 	
					 	?>

						<tr>
							<td><?php echo ($a+1); ?></td>
							<td><input type="checkbox" value="<?= $hotels_list['all_hotels'][$a]->hotel_details_id?>" id="select_hotel_id" name="select_hotel_id"></td>
							<td><?php echo "SMR". $hotels_list['all_hotels'][$a]->hotel_details_id; ?></td>
							<td><?php echo $hotels_list['all_hotels'][$a]->hotel_name; ?></td>
							<td><?php
							
							if($hotels_list['all_hotels'][$a]->thumb_image==''){
								$logo_img = base_url().'assets/images/logo.png';
							} else {
								$logo_img = base_url().'uploads/hotel_images/'.$hotels_list['all_hotels'][$a]->thumb_image;
							}

							?>
							<img src="<?=$logo_img?>" style="width:80px; heigth:60px"/>
							</td>
							<td><!-- <?php echo '<h3><span style="color:red;">'; for ($num = 1; $num <= $hotels_list['all_hotels'][$a]->star_rating; $num++) { echo '* ';
								} echo '</span></h3>'; ?> -->
								<div class="bokratinghotl rating-no">	
								<?=print_star_rating($hotels_list['all_hotels'][$a]->star_rating)?>
								</div>
							</td>
							<?php	
							  $amme = ""; $ammenities="";
							  //$ammenities = $this->Hotel_Model->show_hotel_ammenities($hotels_list['all_hotels'][$a]->hotel_amenities);
							  if($hotels_list['all_hotels'][$a]->hotel_amenities != ""){
							  	$amme = ltrim($hotels_list['all_hotels'][$a]->hotel_amenities,",");
							  	$amme = rtrim($amme,",");
							  	if($amme != "")
							  	$ammenities = $this->hotels_model->show_hotel_ammenities($amme);
							  }				  						  							  							  
							 ?>
							<td><?php echo $ammenities; ?></td>
							<td>
                              <?php echo $hotels_list['all_hotels'][$a]->hotel_address; ?>
                            </td>
							<td><?php echo $hotels_list['all_hotels'][$a]->phone_number; ?></td>
							<!--<td><?php echo $hotels_list['all_hotels'][$a]->added_by_type; ?></td>-->
							<td>
								<?php if($hotels_list['all_hotels'][$a]->status == "ACTIVE"){ ?>
									<a href="<?php echo site_url()."/hotels/inactive_hotel/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>">
										<button type="button" class="btn btn-green btn-icon icon-left">Active<i class="entypo-check"></i></button>
									</a>
								<?php }else{ ?>
										<a href="<?php echo site_url()."/hotels/active_hotel/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>">
											<button type="button" class="btn btn-orange btn-icon icon-left">InActive<i class="entypo-cancel"></i></button>
										</a>
								<?php } ?>
							</td>
							<td class="center">
								<!-- <?php if ($this->session->userdata('lgm_supplier_admin_logged_in') == "Logged_In") {
									
								} else{ ?>
								<?php if($hotels_list['all_hotels'][$a]->status == "ACTIVE"){ ?>
								  <a href="<?php echo site_url()."/hotels/inactive_hotel/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>"><button type="button" class="btn btn-orange tooltip-primary btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="InActive"><i class="glyphicon glyphicon-eye-open"></i></button>
								  <span class="tip">Hooray!</span></a>
								<?php } else { ?>	  
							     <a href="<?php echo site_url()."/hotels/active_hotel/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>"> <button type="button" class="btn btn-success tooltip-primary btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Active"><i class="glyphicon glyphicon-ok"></i></button><span class="tip">Hooray!</span></a>
								<?php } ?>	
								<?php } ?>		 -->					
								
								<a href="<?php echo site_url()."/hotels/edit_hotel/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>" ><button type="button" class="btn btn-blue  tooltip-primary  btn-sm " data-placement="top" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></button></a>
								
								
								<a href="<?php echo site_url()."/hotels/delete_hotel/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>"><button type="button" class="btn btn-red tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" title="Cancel"><i class="glyphicon glyphicon-remove"></i></button>
								<span class="tip">Delete Hotel</span></a>				
								
								<!-- <a href="<?php echo site_url()."/roomrate/hotel_list_room_rate/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_name)); ?>"><button type="button" class="btn btn-default tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" data-original-title="Room Rate"><i class="glyphicon glyphicon-th"></i></button></a> -->

								<a href="<?php echo site_url()."/hotels/manage_hotelchildgroup/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>"><button type="button" class="btn btn-default tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" title="Manage Child Group"><i class="glyphicon glyphicon-user"></i></button>
								<span class="tip">Manage Child Group</span></a><br>
																
								<a href="<?php echo site_url()."/hotels/hotel_room_types/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>"><button type="button" class="btn btn-default tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" title="Room Type"><i class="glyphicon glyphicon-home"></i></button>
								<span class="tip">Rooms List</span></a>
								
								<a href="<?php echo site_url()."/seasons/seasons_list/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>"><button type="button" class="btn btn-default tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" title="Season"><i class="glyphicon glyphicon-tree-conifer"></i></button>
								<span class="tip">Seasons List</span></a>
								
								<a href="<?php echo site_url()."/roomrate/list_room_rate/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_name)); ?>"><button type="button" class="btn btn-default tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" title="Room Rate"><i class="glyphicon glyphicon-usd"></i></button>
								<span class="tip">Rooms Rate</span></a>
								
								<!-- <a href="<?php echo base_url().'/hotel/show_room_details/'.base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>"><button type="button" class="btn btn-default tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" data-original-title="Compulsory Meals"><i class="glyphicon glyphicon-cutlery"></i></button> </a> -->
								
								<!--<a href="<?php echo site_url()."/hotel/cancellation_policy/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>"><button type="button" class="btn btn-default tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" data-original-title="Cancellation Policy"><i class="glyphicon glyphicon-copyright-mark"></i></button></a>-->
								
								<!-- <a href="<?php echo site_url()."/seasonal_offers/offers_list/".base64_encode(json_encode($hotels_list['all_hotels'][$a]->hotel_details_id)); ?>"><button type="button" class="btn btn-default tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" data-original-title="Seasonal Offer"><i class="glyphicon glyphicon-gift"></i></button></a> -->
								
							</td>
															
						</tr>						
					<?php } } ?>												
					</tbody>
				</table>
				</form>
			</div>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL END -->
</div>

<!-- Page Ends Here -->
<script type="text/javascript">

	function checkAll(ele) {
	     var checkboxes = document.getElementsByTagName('input');
	     if (ele.checked) {
	         for (var i = 0; i < checkboxes.length; i++) {
	             if (checkboxes[i].type == 'checkbox') {
	                 checkboxes[i].checked = true;
	             }
	         }
	     } else {
	         for (var i = 0; i < checkboxes.length; i++) {
	             console.log(i)
	             if (checkboxes[i].type == 'checkbox') {
	                 checkboxes[i].checked = false;
	             }
	         }
	     }
	 }

	 function activate_hotels(ele){
	 	var favorite = [];
            $.each($("input[name='select_hotel_id']:checked"), function(){            
                favorite.push($(this).val());
            });
            if(favorite != '' && favorite != null){
	            $.ajax({
					type: "POST",
					url: "<?= site_url()."/hotels/activate_multiple_hotels" ?>",
					data: 'data='+favorite,
					dataType: "json",
					success: function(data){
						if(data.status == 1){
							window.location.href = data.success_url;
						}
					}
				});
			}else{
				alert("Please select atleast one checkbox");
			}
	 }
	 function inactivate_hotels(ele){
	 	var favorite = [];
            $.each($("input[name='select_hotel_id']:checked"), function(){            
                favorite.push($(this).val());
            });
            if(favorite != '' && favorite != null){
	            $.ajax({
					type: "POST",
					url: "<?= site_url()."/hotels/inactivate_multiple_hotels" ?>",
					data: 'data='+favorite,
					dataType: "json",
					success: function(data){
						if(data.status == 1){
							window.location.href = data.success_url;
						}
					}
				});
			}
	 }
</script>

<style>
.align_line button {
    padding: 0px 2px;
    line-height: normal;
    font-size: 12px;
    position: absolute;
    margin-right: 13px;
    margin-top: 3px;
}
.align_line input {
    margin-left: -6px;
    margin-right: 2px;
}




</style>
