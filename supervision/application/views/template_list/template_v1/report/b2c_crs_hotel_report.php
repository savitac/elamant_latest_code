<!--  <script src="<?php echo SYSTEM_RESOURCE_LIBRARY?>/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo SYSTEM_RESOURCE_LIBRARY?>/datatables/dataTables.bootstrap.min.js"></script> --> 
<?
//$GLOBALS['CI']->template->isolated_view('report/email_popup')?>
<?=$GLOBALS['CI']->template->isolated_view('report/email_popup')?>

<?php
//debug($payment_currency);die;
//error_reporting(E_ALL);
if (is_array($search_params)) {
	extract($search_params);
}
$_datepicker = array(array('created_datetime_from', PAST_DATE), array('created_datetime_to', PAST_DATE));
$this->current_page->set_datepicker($_datepicker);
$this->current_page->auto_adjust_datepicker(array(array('created_datetime_from', 'created_datetime_to')));

?>
<div class="bodyContent col-md-12">
	<div class="panel panel-default clearfix"><!-- PANEL WRAP START -->
			<div class="panel-heading"><!-- PANEL HEAD START -->
				<?= $GLOBALS['CI']->template->isolated_view('report/report_tab_b2c') ?>
		</div><!-- PANEL HEAD START -->
		<div class="panel-body">
				<div class="clearfix">
					
					
				</div>
				<hr>			
				<h4>Advanced Search Panel <button class="btn btn-primary btn-sm toggle-btn" data-toggle="collapse" data-target="#show-search">+
					</button> </h4>
				<hr>
			<div id="show-search" class="collapse">
				<form method="GET" autocomplete="off">
					<input type="hidden" name="created_by_id" value="<?=@$created_by_id?>" >
					<div class="clearfix form-group">
						<div class="col-xs-4">
							<label>
							Application Reference
							</label>
							<input type="text" class="form-control" name="app_reference" value="<?=@$parent_pnr?>" placeholder="Application Reference">
						</div>
						<!-- <div class="col-xs-4">
							<label>
							Phone
							</label>
							<input type="text" class="form-control numeric" name="phone" value="<?=@$phone?>" placeholder="Phone">
						</div>
						<div class="col-xs-4">
							<label>
							Email
							</label>
							<input type="text" class="form-control" name="email" value="<?=@$email?>" placeholder="Email">
						</div> -->
						<div class="col-xs-4">
							<label>
							Status
							</label>
							<select class="form-control" name="status">
								<option>All</option>
								<?=generate_options($status_options, array(@$status))?>
							</select>
						</div>
						<div class="col-xs-4">
							<label>
							Booked From Date
							</label>
							<input type="text" readonly id="created_datetime_from" class="form-control" name="created_datetime_from" value="<?=@$created_datetime_from?>" placeholder="Request Date">
						</div>
						<div class="col-xs-4">
							<label>
							Booked To Date
							</label>
							<input type="text" readonly id="created_datetime_to" class="form-control disable-date-auto-update" name="created_datetime_to" value="<?=@$created_datetime_to?>" placeholder="Request Date">
						</div>
					</div>
					<div class="col-sm-12 well well-sm">
					<button type="submit" class="btn btn-primary">Search</button> 
					<button type="reset" class="btn btn-warning">Reset</button>
					<a href="<?php echo base_url().'index.php/report/b2c_hotel_report?' ?>" id="clear-filter" class="btn btn-primary">Clear Filter</a>
					</div>
				</form>
			</div>
		</div>
		
		<div class="clearfix" style="overflow: auto"><!-- PANEL BODY START -->
			<?php echo get_table($table_data, $total_rows);?>
		</div>
	</div>
</div>

<?php
function get_table($table_data, $total_rows)
{
	$pagination = '<div class="pull-left">'.$GLOBALS['CI']->pagination->create_links().' <span class="">Total '.$total_rows.' Bookings</span></div>';
	$report_data = '';
	$report_data .= '<div id="tableList" class="clearfix">';
	$report_data .= $pagination;
	
	$report_data .= '<table class="table table-condensed table-bordered" id="b2c_report_hotel_table">
		<thead>
		<tr>
			<th>Sno</th>
			<th>Action</th>
			<th>Reference No</th>
			<th>Status</th>
			<th>Booking ID</th>
			<th>Lead Pax details</th>
			<th>Hotel Name</th>
			<th>No. of rooms<br/>(Adult + Child)</th>
			<th>City</th>
			<th>CheckIn/<br/>CheckOut</th>
			<th>Comm.Fare</th>
			
			<th>Admin <br/>Markup</th>
			<th>Discount</th>
			
			<th>Grand Total</th>	
			<th>Booked On</th>
		</tr>
		</thead><tfoot>
		<tr>
		<th>Sno</th>
			<th>Action</th>
			<th>Reference No</th>
			<th>Status</th>
			<th>Booking ID</th>
			<th>Lead Pax details</th>
			<th>Hotel Name</th>
			<th>No. of rooms<br/>(Adult + Child)</th>
			<th>City</th>
			<th>CheckIn/<br/>CheckOut</th>
			<th>Comm.Fare</th>
			
			<th>Admin <br/>Markup</th>
			<th>Discount</th>
			
			<th>Grand Total</th>	
			<th>Booked On</th>
		</tr>
		</tfoot><tbody>';
		
		if (isset($table_data) == true and valid_array($table_data['booking_details']) == true) {
			$segment_3 = $GLOBALS['CI']->uri->segment(3);
			$current_record = (empty($segment_3) ? 1 : $segment_3);
			$booking_details = $table_data['booking_details'];
			//error_reporting(E_ALL);
			//debug($booking_details); exit;
		    foreach($booking_details as $parent_k => $parent_v) { 
		    	// debug($parent_v);die;
		        	extract($parent_v);
		        	if ($discount_amount != "") {
		        		$total_room_price = number_format(($total_room_price - $discount_amount), 2);
		        	}

		        	$total_price = $total_room_price + $admin_markup;
		        	$request = json_decode($request,true);
		    	$booking_source = $request['booking_source'];
				$action = '';
				$email='';
				$tdy_date = date ( 'Y-m-d' );
				$diff = get_date_difference($tdy_date,$check_in_date);
				//debug($diff);exit;
				$action.= crs_hotel_voucher($app_reference, $booking_source, $booking_status);
				$action.='<br/>';
				$action.= crs_hotel_pdf($app_reference, $booking_source, $booking_status);
				$action.='<br/>';
				$action.= hotel_voucher_email($app_reference, $booking_source,$booking_status,$parent_v['email_id']);
				$action.='<br/>';
		    	if($booking_status == 'CONFIRM' && $diff > 0) {
					$action .= cancel_crs_hotel_booking($app_reference, $booking_source, $booking_status);
				}
				$email = hotel_email_voucher($app_reference, $booking_source, $booking_status);
			
		$report_data .= '<tr>
					<td>'.($current_record++).'</td>
					<td><div class="btn-group dropdown_hover" role="group">
							<button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-angle-right"></i> Actions
							</button>
							<div class="dropdown-menu action_crs_hotel_report" aria-labelledby="btnGroupDrop1">
							' . $action . '
							</div>
						</div>
					</td>
					</td>
					<td>'.$parent_pnr.'</td>
					<td><span class="'.booking_status_label($booking_status).'">'.$booking_status.'</span></td>
					<td class="">'.$app_reference.'</td>
					<td>'.$contact_fname.' '.$contact_sur_name.'<br>'.$contact_email.'<br>'.$contact_mobile_number.
					'
					</td>
					<td>'.$hotel_name.'</td>
					<td>'.$room_count.'<br/>('.$adult.'+'.$child.')</td>
					<td>'.$hotel_location.'</td>
					<td>'.date('d-m-Y', strtotime($check_in_date)).'/<br/>'.date('d-m-Y', strtotime($check_out_date)).'</td>
					<td>'.$admin_currency.''.($total_room_price).'</td>
					
					<td>'.$admin_currency.''.$admin_markup.'</td>
					<td>'.$admin_currency.''.number_format($discount_amount, 2).'</td>
					<td>'.$admin_currency.''.$total_price.'</td>
					<td>'.date('d-m-Y', strtotime($book_date)).'</td>
				</tr>';
			}
		} else {
			$report_data .= '<tr><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td>
								 <td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td>
								<td>---</td><td>---</td><td>---</td><td>---</td></tr>';
		}
	$report_data .= '</tbody></table>
			</div>';
	return $report_data;
}
function get_accomodation_cancellation($courseType, $refId)
{
	return '<a href="'.base_url().'index.php/booking/accomodation_cancellation?courseType='.$courseType.'&refId='.$refId.'" class="col-md-12 btn btn-sm btn-danger "><i class="fa fa-exclamation-triangle"></i> Cancel</a>';
}
function hotel_voucher_email($app_reference, $booking_source,$status,$recipient_email)
{

	return '<a class="send_email_voucher" data-app-status="'.$status.'"   data-app-reference="'.$app_reference.'" data-booking-source="'.$booking_source.'"data-recipient_email="'.$recipient_email.'"><i class="fa fa-envelope"></i> Email Voucher</a>';
}
function crs_hotel_voucher($app_reference, $booking_source='', $status='')
{
	return '<a href="'.crs_hotel_voucher_url($app_reference, $booking_source, $status).'/show_voucher" target="_blank" class=""><i class="fa fa-file"></i> Voucher</a>';
}
function crs_hotel_voucher_url($app_reference, $booking_source='', $status='')
{
	return base_url().'index.php/voucher/hotel_crs/'.$app_reference.'/'.$status;
}
function crs_hotel_pdf($app_reference, $booking_source='', $status='')
{
	return '<a href="'.crs_hotel_voucher_pdf_url($app_reference, $booking_source, $status).'/show_pdf" target="_blank" class=""><i class="fa fa-file"></i> PDF</a>';
}
function crs_hotel_voucher_pdf_url($app_reference, $booking_source='', $status='')
{
	return base_url().'index.php/voucher/hotel_crs/'.$app_reference.'/'.$status;
}
function cancel_crs_hotel_booking($app_reference, $booking_source = '', $status = '') {
	return '<a href="' . cancel_crs_hotel_booking_url ( $app_reference, $booking_source, $status ) . '"  class="" target="_blank"><i class="fa fa-file"></i> Cancel</a>';
}
function cancel_crs_hotel_booking_url($app_reference, $booking_source = '', $status = '') {
	return base_url () . 'index.php/hotels/pre_cancellation/' . $app_reference . '/' . $booking_source . '/' . $status;
}
?>
<script>
$(document).ready(function() {
    // $('#b2c_report_hotel_table').DataTable({
    //     // Disable initial sort 
    //     "aaSorting": []
    // });

	    //send the email voucher
		$('.send_email_voucher').on('click', function(e) {
			//alert('test');
			$("#mail_voucher_modal").modal('show');
			$('#mail_voucher_error_message').empty();
	        email = $(this).data('recipient_email');
			$("#voucher_recipient_email").val(email);
	        app_reference = $(this).data('app-reference');
	        book_reference = $(this).data('booking-source');
	        app_status = $(this).data('app-status');
	        //alert('test');
		  $("#send_mail_btn").off('click').on('click',function(e){
			 
			  email = $("#voucher_recipient_email").val();
	
			  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				 if(email != ''){ 
					  if(!emailReg.test(email)){
						  $('#mail_voucher_error_message').empty().text('Please Enter Correct Email Id');
		                     return false;    
						      }
					var _opp_url = app_base_url+'index.php/voucher/hotel_crs/';
					_opp_url = _opp_url+app_reference+'/'+'/'+app_status+'/email_voucher/'+email;
					toastr.info('Please Wait!!!');
					$.get(_opp_url, function() {
						
						toastr.info('Email sent  Successfully!!!');
						$("#mail_voucher_modal").modal('hide');
					});
					 
			  }else{
				  $('#mail_voucher_error_message').empty().text('Please Enter Email ID');
				  }
		  });
	
	});
	$('#b2c_report_hotel_table a.btn-warning').on('click', function(e) {

		var _opp_url = $(this).attr('href');
		$('#hotel_cancel_modal').modal('show');
		$('#cancel_hotel_btn').on('click',function(e){
				$.ajax({
				  url: _opp_url,
				  cache: false,
				  dataType:"json",
				  success: function(html){
				    alert(html.msg);
				    $('#hotel_cancel_modal').modal('hide');
				     location.reload();
				  }
				});
		});
		e.preventDefault();
	});	
});
</script>