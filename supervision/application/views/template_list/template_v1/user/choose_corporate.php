<div id="enquiries" class="bodyContent col-md-12">
	<div class="panel panel-default">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
					<li role="presentation" class="active"><a href=""
						aria-controls="home" role="tab" data-toggle="tab"><h3>Corporate List </h3></a></li>
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
				</ul>
				
            
			</div>
		</div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="fromList">
					<div class="col-md-12">
						<div class='row'>
						
						<div class='row'>
                <div class='col-sm-14'>
                  <div class='' style='margin-bottom:0;'>
                    <div class=''>
                      <div class='responsive-table'>
                        <div class='scrollable-area'>
                        <?php  $id= $_REQUEST['eid'];//$this->uri->segment(3); ?>
                        <form id="user_previlege_form"
        action="<?php echo base_url(); ?>index.php/utilities/choose_corporate/<?php echo $id; ?>"
        method="post" enctype="multipart/form-data"
        class='form form-horizontal validate-form'>
        <input type="hidden" name="eid" value="<?php echo $id;?>"/>
        
        <?php if(isset($msg))
        {?>
       <!--  <p style="color: red;"><?php echo $msg;?></p>-->
      <?php  }?>
        
                          <table class='data-table-column-filter table table-bordered table-striped' style='margin-bottom:0;'>
                            <thead>
                              <tr>
                               <th>Sno</th>
                                 <th>Select <button class="btn btn-xs btn-primary" id="btn_select_all" data-check="" type="button">All</button></th>
                                 <th>Corporates</th>
                                 <th>Promocode Status</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                          <?php
                         //debug($page_data);  
                          //debug($privileges['left_menu_id']);  
                          if(!empty($page_data)) { $count = 1; 
                        foreach($page_data as $key => $crs_list) {
                          //debug($crs_list);
                        	//debug($corporates);
                         ?>

                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><input type="checkbox" name="corporates[]" value="<?php echo $crs_list['user_id']; ?>" <?php foreach ($corporates as $key => $value) {
                         
                        	
                        	if (($crs_list['user_id']==$value['corporate_id']) && ($value['promocode_id']==$id) )
                           {
                              echo $checked ='checked';
                           }
                           else
                           {
                               echo $checked='';
                           }
                          } ?> /></td>
                        <td><?php echo $crs_list['agency_name']; ?> </td>
                         <td><input type="text" readonly="readonly" name="sts" value="<?php foreach ($corporates as $key => $value) {
                          if (($crs_list['user_id']==$value['corporate_id']) && ($value['promocode_id'] == $id) )
                          {
                             if($value['promo_status'] == 0)
                             {
                              echo 'Not Applied';
                             }
                             else {echo 'Applied';}
                          }
                          } ?>" /></td>
                       
                      </tr>   
                  <?php $count++; } } ?>  
                      </tbody>
                          </table>
                          <input type="submit" class="btn btn-sm btn-primary" value="Submit" />
                          </form>
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL WRAP END -->
</div>
<script>
$(function(){
	$('#btn_select_all').on('click', function(){
			var check = $(this).attr('data-check');
			if(check == ''){
				$('[name="corporates[]"]').prop('checked',true);
				$(this).attr('data-check', 'checked').text('Reset');
			} else if(check == 'checked') {
				$('#user_previlege_form').get(0).reset();
				$(this).attr('data-check', '').text('All');
			}
	});
});
</script>