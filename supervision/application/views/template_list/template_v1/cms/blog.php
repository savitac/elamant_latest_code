<!-- HTML BEGIN -->
<div class="bodyContent">
	<div class="panel <?=PANEL_WRAPPER?>"><!-- PANEL WRAP START -->
		<div class="panel-heading"><!-- PANEL HEAD START -->
			<div class="panel-title">
				<i class="fa fa-edit"></i> Blogs
			</div>
		</div><!-- PANEL HEAD START -->
		<div class="panel-body"><!-- PANEL BODY START -->
			<fieldset><legend><i class="fa fa-plane"></i> Add New Blog</legend>
				<form action="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data" class="form-horizontal" method="POST" autocomplete="off">
					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Blog Title<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<input type="text" name="blog_title" id="blog_title" class="col-sm-3 " required>
							&nbsp;<div id="errormsgto" style="color: red;"></div>
						</div>
					</div>
					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Description<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<textarea name="blog_description" id="blog_description" class="col-sm-3 " required="" style="margin: 0px; width: 333px; height: 187px;"></textarea>
							&nbsp;<div id="errormsgto" style="color: red;"></div>
						</div>
					</div>
					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Image<span class="text-danger">*</span></label>
						<div class="col-sm-6">
							<input type="file" class="" id="blog_image" accept=".png, .jpg, .jpeg" required="required" name="top_destination">
						</div>
					</div>

					<div class="well well-sm">
						<div class="clearfix col-md-offset-1">
							<button class=" btn btn-sm btn-success " type="submit">Add</button>
						</div>
					</div>
				</form>
			</fieldset>
		</div><!-- PANEL BODY END -->
 	<div class="panel-body">
			<table class="table table-condensed">
				<tr>
					<th>Sno</th>
					<th>Blog Title</th>
					<th>Image</th>
					<th>Description</th>
					<th>Status</th>					
				</tr>
				<?php
				//debug($data_list);exit;
				if (valid_array($data_list) == true) {
					foreach ($data_list as $k => $v) :
				?>
					<tr>
						<td><?=($k+1)?></td>
						<td><?=$v['blog_title']?></td>
						<td><img src="<?php echo $GLOBALS ['CI']->template->domain_images ($v['blog_image']) ?>" height="100px" width="100px" class="img-thumbnail"></td>
						<td><?=$v['blog_description']?></td>
						
						<td><?php echo get_status_label($v['blog_status']).get_status_toggle_button($v['blog_status'], $v['blog_id']) ?></td>
						
					</tr>
				<?php
					endforeach;
				} else {
					echo '<tr><td>No Data Found</td></tr>';
				}
				?>
			</table>
		</div> 
	</div><!-- PANEL WRAP END -->
</div>

<?php 
function get_status_label($status)
{
	if (intval($status) == ACTIVE) {
		return '<span class="label label-success"><i class="fa fa-circle-o"></i> '.get_enum_list('status', ACTIVE).'</span>
	<a role="button" href="" class="hide">'.get_app_message('AL0021').'</a>';
	} else {
		return '';
	}
}

function get_status_toggle_button($status, $blog_id)
{
	if (intval($status) == ACTIVE) {
		return '<a role="button" href="'.base_url().'index.php/cms/deactivate_blog/'.$blog_id.'" class="text-danger">Deactivate</a>';
	} else {
		return '';		
	}
}

?>
<script type="text/javascript">
var _URL = window.URL || window.webkitURL;

$('input[type=file]').on('change',function (event) {

                var file, img;

                if ((file = this.files[0])) {
                img = new Image();
                $(img).on('load',function () {
                if(this.width >= 342  && this.height >= 449)
                {
                    var path = URL.createObjectURL(event.target.files[0]);
                   
                }
                else
                {
                $("#blog_image").attr('value','');
                 $("#blog_image").val('');
                alert('Please Upload More Than 342 X 449 Resolution Image');
                return false;
                }
              });
              img.src = _URL.createObjectURL(file);
              }        
});
</script>
