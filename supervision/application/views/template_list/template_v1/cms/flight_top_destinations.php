<!-- HTML BEGIN -->
<div class="bodyContent">
	<div class="panel <?=PANEL_WRAPPER?>"><!-- PANEL WRAP START -->
		<div class="panel-heading"><!-- PANEL HEAD START -->
			<div class="panel-title">
				<i class="fa fa-edit"></i> Top Destinations In Flight
			</div>
		</div><!-- PANEL HEAD START -->
		<div class="panel-body"><!-- PANEL BODY START -->
			<fieldset><legend><i class="fa fa-plane"></i> Airport List</legend>
				<form action="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data" class="form-horizontal" method="POST" autocomplete="off">
					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">From Country<span class="text-danger">*</span></label>
						<div class="col-sm-6">
							<select id="from_country" class="form-control" required="">
								<option value="INVALIDIP">Please Select</option>
								<?=generate_options($country_list)?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">From Aiport<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<select name="from_airport_name" id="from_airport_name" class="form-control" required="">
								<option value="INVALIDIP">Select Origin</option>
								<!-- <?// generate_options($flight_list)?> -->
							</select>
						</div>
					</div>

					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">To Country<span class="text-danger">*</span></label>
						<div class="col-sm-6">
							<select id="to_country" class="form-control" required="">
								<option value="INVALIDIP">Please Select</option>
								<?=generate_options($country_list)?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">To Airport<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<select name="to_airport_name" class="form-control" required="" id="to_airport_name">
								<option value="INVALIDIP">Select Destination</option>
								<? //generate_options($flight_list)?>
							</select>
							&nbsp;<div id="errormsgto" style="color: red;"></div>
						</div>
					</div>
					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Price<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<input type="text" name="price" maxlength="7" id="price" class="col-sm-3 control-label numbers">
							&nbsp;<div id="errormsgto" style="color: red;"></div>
						</div>
					</div>
					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Image<span class="text-danger">*</span></label>
						<div class="col-sm-6">
							<input type="file" class="" accept="image/*" required="required" name="top_destination">
						</div>
					</div>

					<div class="well well-sm">
						<div class="clearfix col-md-offset-1">
							<button class=" btn btn-sm btn-success " type="submit">Add</button>
						</div>
					</div>
				</form>
			</fieldset>
		</div><!-- PANEL BODY END -->
		<div class="panel-body">
			<table class="table table-condensed">
				<tr>
					<th>Sno</th>
					<th>Origin</th>
					<th>Destination</th>
					<th>Image</th>
					<th>Action</th>
					<th>Price</th>
				</tr>
				<?php
				//debug($data_list);exit;
				if (valid_array($data_list) == true) {
					foreach ($data_list as $k => $v) :
				?>
					<tr>
						<td><?=($k+1)?></td>
						<td><?=$v['from_airport_city']?></td>
						<td><?=$v['destination_airport_city']?></td>
						<td><img src="<?php echo $GLOBALS ['CI']->template->domain_images ($v['image_path']) ?>" height="100px" width="100px" class="img-thumbnail"></td>
						<td><?php echo get_status_label($v['top_destination']).get_status_toggle_button($v['top_destination'], $v['top_destination_multipleid']) ?></td>
						<td><?=$v['price']?></td>
					</tr>
				<?php
					endforeach;
				} else {
					echo '<tr><td>No Data Found</td></tr>';
				}
				?>
			</table>
		</div>
	</div><!-- PANEL WRAP END -->
</div>
<script>
	$('#from_country').on('change', function() {
		var _country = this.value;
		if (_country != 'INVALIDIP') {
			//load airport name for country
			$.get(app_base_url+'index.php/ajax/get_airport_name/'+_country, function(resp) {
				$('#from_airport_name').html(resp.data);
			});
		}
	});

	$('#to_country').on('change', function() {
		var _country = this.value;
		if (_country != 'INVALIDIP') {
			//load airport name for country
			$.get(app_base_url+'index.php/ajax/get_airport_name/'+_country, function(resp) {
				$('#to_airport_name').html(resp.data);
			});
		}
	});

	$('.numbers').keypress(function(e) {
                var a = [];
                var k = e.which;

                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(a.indexOf(k)>=0))
                    e.preventDefault();
            });
</script>
<?php 
function get_status_label($status)
{
	if (intval($status) == ACTIVE) {
		return '<span class="label label-success"><i class="fa fa-circle-o"></i> '.get_enum_list('status', ACTIVE).'</span>
	<a role="button" href="" class="hide">'.get_app_message('AL0021').'</a>';
	} else {
		return '';
	}
}

function get_status_toggle_button($status, $origin)
{
	if (intval($status) == ACTIVE) {
		return '<a role="button" href="'.base_url().'index.php/cms/deactivate_flight_top_destination/'.$origin.'" class="text-danger">Deactivate</a>';
	} else {
		return '';		
	}
}

?>
