<!-- HTML BEGIN -->
<div class="bodyContent">
	<div class="panel <?=PANEL_WRAPPER?>"><!-- PANEL WRAP START -->
		<div class="panel-heading"><!-- PANEL HEAD START -->
			<div class="panel-title">
				<i class="fa fa-edit"></i> Bus Deals & Offers
			</div>
		</div><!-- PANEL HEAD START -->
		<div class="panel-body"><!-- PANEL BODY START -->
			<fieldset><legend><i class="fa fa-bus"></i> Add New Deal</legend>
				<form action="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data" class="form-horizontal" method="POST" autocomplete="off">

					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">City_list<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<select name="city" class="form-control" required="">
								<option value="INVALIDIP">Please Select</option>
								<?=generate_options($bus_list)?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Price<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<input type="text" name="price" id="price" class="col-sm-3 control-label" required>
							&nbsp;<div id="errormsgto" style="color: red;"></div>
						</div>
					</div>
					

					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Expiry Date<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<input type="text" name="offer_expiry_date" id="offer_expiry_date" class="col-sm-3 control-label" readonly required>
							&nbsp;<div id="errormsgto" style="color: red;"></div>
						</div>
					</div>

				
					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Image<span class="text-danger">*</span></label>
						<div class="col-sm-6">
							<input type="file" class="" accept=".png, .jpg, .jpeg" required="required" name="top_destination">
						</div>
					</div>

					<div class="well well-sm">
						<div class="clearfix col-md-offset-1">
							<button class=" btn btn-sm btn-success " type="submit">Add</button>
						</div>
					</div>
				</form>
			</fieldset>
		</div><!-- PANEL BODY END -->
		<div class="panel-body">
			<table class="table table-condensed">
				<tr>
					<th>Sno</th>
					<th>City</th>
					<th>Image</th>
					<th>Price</th>
					<th>Action</th>
					<th>Expiry Date</th>
				</tr>
				<?php
				//debug($data_list);exit;
				if (valid_array($data_list) == true) {
					foreach ($data_list as $k => $v) :
				?>
					<tr>
						<td><?=($k+1)?></td>
						<td><?=$v['name']?></td>
						<td><img src="<?php echo $GLOBALS ['CI']->template->domain_images ($v['deal_image']) ?>" height="100px" width="100px" class="img-thumbnail"></td>
						<td><?=$v['deal_price']?></td>
						<td><?php echo get_status_label($v['deal_status']).get_status_toggle_button($v['deal_status'], $v['origin']) ?></td>
						<?php $expiry_cnv = date_create($v['offer_expiry_date']);
							  $expiry_date = date_format($expiry_cnv,"Y-m-d");?>
						<td><?=$expiry_date ?></td>
					</tr>
				<?php
					endforeach;
				} else {
					echo '<tr><td>No Data Found</td></tr>';
				}
				?>
			</table>
		</div>
	</div><!-- PANEL WRAP END -->
</div>
<?php 
function get_status_label($status)
{
	if (intval($status) == ACTIVE) {
		return '<span class="label label-success"><i class="fa fa-circle-o"></i> '.get_enum_list('status', ACTIVE).'</span>
	<a role="button" href="" class="hide">'.get_app_message('AL0021').'</a>';
	} else {
		return '';
	}
}

function get_status_toggle_button($status, $origin)
{
	if (intval($status) == ACTIVE) {
		return '<a role="button" href="'.base_url().'index.php/cms/deactivate_bus_deals/'.$origin.'" class="text-danger">Deactivate</a>';
	} else {
		return '';		
	}
}

?>
<script type="text/javascript">
	$( document ).ready(function() {
	   $('#offer_expiry_date').datepicker({
	     formatDate:'Y/m/d',
	    minDate: new Date()
	   }); 
	});
</script>