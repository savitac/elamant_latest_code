<?php
$airline_module = is_active_airline_module();
$accomodation_module = is_active_hotel_module();
$bus_module = is_active_bus_module();
$package_module = is_active_package_module();
$sightseen_module = is_active_sightseeing_module();
$car_module = is_active_car_module();
$transferv1_module = is_active_transferv1_module();

?>

					
					
<!-- HTML BEGIN -->
<div class="bodyContent">
	<div class="panel <?=PANEL_WRAPPER?>"><!-- PANEL WRAP START -->
		<div class="panel-heading"><!-- PANEL HEAD START -->
			<div class="panel-title">
				<i class="fa fa-edit"></i> Deals & Offers
			</div>
		</div><!-- PANEL HEAD START -->
		<div class="panel-body"><!-- PANEL BODY START -->
			<fieldset><legend><i class="fa fa-gift"></i> Add Deal & Offer</legend>
				<form action="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data" class="form-horizontal" method="POST" id="deal_form" autocomplete="off">

					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Category<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<select name="category" id="category" class="form-control" required="">

								<!-- <?php //if ($airline_module) { ?> -->
								<!-- <option value="flight">Flight</option> -->
								<!-- <?php //} ?> -->

								<?php if ($accomodation_module) { ?>
								<option value="hotel">Hotel</option>
								<option value="villa">Villa</option>
								<?php } ?>

								<?php if ($bus_module) { ?>
								<option value="bus">Bus</option>
								<?php } ?>

								<?php if ($package_module) { ?>
								<option value="holiday">Holiday</option>			
								<?php } ?>

								<?php if ($sightseen_module) { ?>
								<option value="activity">Activity</option>
								<?php } ?>

								<?php if ($car_module) { ?>
								<option value="car">Car</option>
								<?php } ?>

								<?php if ($transferv1_module) { ?>
								<option value="transfer">Transfer</option>
								<?php } ?>

							</select>
						</div>
					</div>

					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Offer Name<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<input type="text" name="deal_title" id="deal_title" class="col-sm-3 " required>
							&nbsp;<div id="errormsgto" style="color: red;"></div>
						</div>
					</div>
					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Promocode<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<input type="text" name="promocode" id="promocode" class="col-sm-3 " required>
							&nbsp;<div id="errormsgto" style="color: red;"></div>
						</div>
					</div>

					
					<div class="form-group">
						<label for="value_type" class="col-sm-3 control-label">Offer Type<span class="text-danger">*</span></label>
						<label for="value_type_plus" class="radio-inline">
							<input  type="radio" value="minus" id="value_type_plus" name="deal_type" class=" value_type_plus radioIp" checked="checked" required=""> Minus(- <?=get_application_default_currency()?>)
						</label>
						<label for="value_type_percent" class="radio-inline">
							<input  type="radio" value="percentage" id="value_type_percent" name="deal_type" class=" value_type_percent radioIp" required=""> Percentage(%)
						</label>
					</div>

					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Discount<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<input type="text" name="discount" id="discount" class="col-sm-3 numbers" required>
							&nbsp;<div id="errormsgto" style="color: red;"></div>
						</div>
					</div>
				 
			
					
					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Image<span class="text-danger">*</span></label>
						<div class="col-sm-6">
							<input type="file" class="" accept=".png, .jpg, .jpeg" required="required" name="top_destination">
						</div>
					</div>

					<div class="form-group">
						<label form="user" for="title" class="col-sm-3 control-label">Expiry Date<span class="text-danger">*</span></label>
						<div class="col-sm-5">
							<input type="text" name="offer_expiry_date" id="offer_expiry_date" class="col-sm-3 control-label" readonly required>
							&nbsp;<div id="errormsgto" style="color: red;"></div>
						</div>
					</div>

					<div class="well well-sm">
						<div class="clearfix col-md-offset-1">
							<button class=" btn btn-sm btn-success " id="addDeal" type="submit">Add</button>
						</div>
					</div>
				</form>
			</fieldset>
		</div><!-- PANEL BODY END -->
		<div class="panel-body">
			<table class="table table-condensed">
				<tr>
					<th>Sno</th>
					<th>Category</th>
					<th>Offer Name</th>
					<th>Promocode</th>
					<th>Offer type</th>
					<th>Discount</th>
					<th>Image</th>
					<th>Action</th>
					<th>Expiry date</th>
				</tr>
				<?php
				//debug($data_list);exit;
				if (valid_array($data_list) == true) {
					foreach ($data_list as $k => $v) :
				?>
					<tr>
						<td><?=($k+1)?></td>
						<td><?=$v['category']?></td>
						<td><?=$v['deal_name']?></td>
						<td><?=$v['deal_code']?></td>
						<td><?=$v['deal_type']?></td>
						<td><?php if($v['deal_type'] == "minus") { 
								echo get_application_default_currency();
								}?>
						<?=$v['discount']?>
							<?php if($v['deal_type'] == "percentage"){
									echo " %";
									}else{
										}?>
						</td>
						<td><img src="<?php echo $GLOBALS ['CI']->template->domain_images ($v['deal_image']) ?>" height="100px" width="100px" class="img-thumbnail"></td>
						<td><?php echo get_status_label($v['deal_status']).get_status_toggle_button($v['deal_status'], $v['deal_id']) ?></td>
						<?php $expiry_cnv = date_create($v['expiry_date']);
							  $expiry_date = date_format($expiry_cnv,"Y-m-d");?>	
						<td><?=$expiry_date ?></td>
					</tr>
				<?php
					endforeach;
				} else {
					echo '<tr><td>No Data Found</td></tr>';
				}
				?>
			</table>
		</div>
	</div><!-- PANEL WRAP END -->
</div>
<?php 
function get_status_label($status)
{
	if (intval($status) == ACTIVE) {
		return '<span class="label label-success"><i class="fa fa-circle-o"></i> '.get_enum_list('status', ACTIVE).'</span>
	<a role="button" href="" class="hide">'.get_app_message('AL0021').'</a>';
	} else {
		return '';
	}
}

function get_status_toggle_button($status, $origin)
{
	if (intval($status) == ACTIVE) {
		return '<a role="button" href="'.base_url().'index.php/cms/deactivate_bus_top_destination/'.$origin.'" class="text-danger">Deactivate</a>';
	} else {
		return '';		
	}
}

?>

<script type="text/javascript">

$( document ).ready(function() {
	   $('#offer_expiry_date').datepicker({
	     formatDate:'Y/m/d',
	    minDate: new Date()
	   }); 
	});

$("#value_type_percent").on("click",function(){
$("#discount").attr("maxlength","2");
$("#discount").val('');
});

$("#value_type_plus").on("click",function(){
$("#discount").attr("maxlength","");
$("#discount").val('');
});

$('.numbers').keypress(function(e) {
                var a = [];
                var k = e.which;

                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(a.indexOf(k)>=0))
                    e.preventDefault();
            });

</script>
