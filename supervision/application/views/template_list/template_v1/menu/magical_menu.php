<?php
$active_domain_modules = $this->active_domain_modules;


/**
 * Need to make privilege based system
 * Privilege only for loading menu and access of the web page
 * 
 * Data loading will not be based on privilege.
 * Data loading logic will be different.
 * It depends on many parameters
 */
$menu_list = array();
if (count($active_domain_modules) > 0) {
	$any_domain_module = true;
} else {
	$any_domain_module = false;
}
$airline_module = is_active_airline_module();
$accomodation_module = is_active_hotel_module();
$bus_module = is_active_bus_module();
$package_module = is_active_package_module();
$package_module = true;
$sightseen_module = is_active_sightseeing_module();
$car_module = is_active_car_module();
$transferv1_module = is_active_transferv1_module();
$bb='b2b';
$bc='b2c';
$b2b2c='b2b2c';
$corp='corporate';
$b2b = is_active_module($bb);
$b2c = is_active_module($bc);
$b2b2c = is_active_module($b2b2c);
$corporate= is_active_module($corp);

if($this->entity_user_type == 2){
	$privillege = $GLOBALS['CI']->custom_db->get_sub_admin_privilleges();
	// debug($privillege);exit;
}
//checking social login status 
$social_login = 'facebook';
$social = is_active_social_login($social_login);
//echo "ela".$accomodation_module;exit;
$accomodation_module = 1;
?>
<?php if($this->entity_user_type == 1){ ?>
<ul class="sidebar-menu" id="magical-menu">
	<li class="treeview">
		<a href="#">
			<i class="fa fa-tachometer-alt light_green"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url()?>"><i class="fa fa-bars text-fuchsia"></i> Dashboard v1</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
		<i class="fa fa-user"></i> <span>B2B Markup</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/management/b2b_airline_markup/'?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
			<li><a href="<?php echo base_url().'index.php/management/b2b_hotel_markup/'?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
			<li><a href="<?php echo base_url().'index.php/management/b2b_transfer_markup/'?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
		</ul>
	</li>

	<?php if(is_domain_user() == false) { // ACCESS TO ONLY PROVAB ADMIN ?>
	<li class="treeview">
		<a href="#">
		<i class="fa fa-wrench"></i> <span>Management</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/user/user_management'?>"><i class="fa fa-user"></i> User</a></li>
			<li><a href="<?php echo base_url().'index.php/user/domain_management'?>"><i class="fa fa-laptop"></i> Domain</a></li>
			<li><a href="<?php echo base_url().'index.php/module/module_management'?>"><i class="fa fa-sitemap"></i> Master Module</a></li>
		</ul>
	</li>
	<?php if ($any_domain_module) {?>
	<li class="treeview">
		<a href="#">
		<i class="fa fa-user"></i> <span>Markup</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<?php if ($airline_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/airline_domain_markup'?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
			<?php } ?>
			<?php if ($accomodation_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/hotel_domain_markup'?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
			<?php } ?>
			<?php if ($bus_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/bus_domain_markup'?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
			<?php } ?>
			<?php if ($transferv1_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/transfer_domain_markup'?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
			<?php } ?>

			<?php if ($sightseen_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/sightseeing_domain_markup'?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i>Sightseeing</a></li>
			<?php } ?>

		</ul>
	</li>
	<?php } ?>
	<li class="treeview">
		<a href="<?php echo base_url().'index.php/private_management/process_balance_manager'?>">
			<i class="fa fa-google-wallet"></i> 
			<span> Master Balance Manager </span>
		</a>
	</li>
	<li class="treeview">
		<a href="<?php echo base_url().'index.php/private_management/event_logs'?>">
			<i class="fa fa-shield"></i> 
			<span> Event Logs </span>
		</a>
	</li>
	<?php } else if((is_domain_user() == true)) {
		// ACCESS TO ONLY DOMAIN ADMIN
	?>
	<!-- USER ACCOUNT MANAGEMENT -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-user dark_pink"></i> 
			<span> Users </span><i class="fa fa-angle-left pull-right"></i></a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<?php if($b2b2c){	?>
			<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2B2C_USER;?>"><i class="far fa-circle"></i> B2B2C</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2B2C_USER.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2B2C_USER.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.B2B2C_USER;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php } ?>

			<?php if($b2c){	?>
			<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER;?>"><i class="far fa-circle"></i> B2C</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.B2C_USER;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php } ?>
			<?php if($b2b){	?>
			<li><a href="<?php echo base_url().'index.php/user/b2b_user?filter=user_type&q='.B2B_USER ?>"><i class="far fa-circle"></i> Agents</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/b2b_user?user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/b2b_user?user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.B2B_USER;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php }?>
			<?php if($corporate){	?>
			<!-- <li><a href="<?php echo base_url().'index.php/user/corporate_user?filter=user_type&q='.CORPORATE_USER ?>"><i class="far fa-circle"></i> Corporate</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/corporate_user?user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/corporate_user?user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.CORPORATE_USER;?>"><i class="fa fa-circle"></i> Logged In User</a></li>
				</ul>
			</li> -->
			<?php }?>
			<!-- <li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUB_ADMIN ?>"><i class="far fa-circle"></i> Sub Admin</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUB_ADMIN.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUB_ADMIN.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.SUB_ADMIN;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
			<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUPPLIER ?>"><i class="far fa-circle"></i> Supplier</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUPPLIER.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUPPLIER.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.SUPPLIER;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li> -->
		</ul>
	</li>
	<?php $any_domain_module = TRUE; if ($any_domain_module) {?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-chart-bar dark_blue"></i> 
			<span> Reports </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<!-- <li>
				<a href="#"><i class="far fa-circle"></i> B2C</a>
				<ul class="treeview-menu">

				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>

				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				<?php } ?>

				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_crs_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel CRS</a></li>
				<li><a href="<?php echo base_url().'index.php/report/b2c_villa_report/';?>"><i class="fa fa-bed"></i> Villa</a></li>
				<?php } ?>

				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>

				<?php if ($package_module) {?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_package_enquiry/';?>"><i class="<?=get_arrangement_icon(META_PACKAGE_COURSE)?>"></i> Holiday</a></li>
				<?php } ?>
				
				<?php
				   if($transferv1_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/report/b2c_transfers_report/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i> Transfer</a></li>
				<?php } ?>

				<?php if($sightseen_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/report/b2c_sightseeing_report/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Sightseeing</a></li>
				<?php } ?>

				<?php if ($car_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_car_report/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>
				<?php } ?>
				</ul>
			</li> -->
           <!-- <li><a href="#"><i class="far fa-circle"></i> Agent</a>
				<ul class="treeview-menu"> -->
				<?php $airline_module = TRUE;if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>
				<?php $accomodation_module = TRUE;if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				
				<?php } ?>
				<!-- <?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_crs_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel CRS</a></li>
				<li><a href="<?php echo base_url().'index.php/report/b2b_villa_report/';?>"><i class="fa fa-bed"></i> Villa</a></li>
				<?php } ?> -->
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>
				<?php if ($package_module) {?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_package_enquiry/';?>"><i class="<?=get_arrangement_icon(META_PACKAGE_COURSE)?>"></i> Holiday</a></li>
				<?php } ?>

				<?php $transferv1_module = TRUE;if ($transferv1_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_transfers_report/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
				<?php } ?>
			
				<?php if ($sightseen_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_sightseeing_report/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Sightseeing</a></li>
				<?php } ?>
				<?php if ($car_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_car_report/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>
				<?php } ?>
				<!-- </ul>
			</li> -->
			<!-- <li><a href="#"><i class="far fa-circle"></i> Corporate</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				<li><a href="<?php echo base_url().'index.php/report/corporate_crs_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel CRS</a></li>
				<li><a href="<?php echo base_url().'index.php/report/corporate_villa_report/';?>"><i class="fa fa-bed"></i> Villa</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>
				<?php if ($package_module) {?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_package_enquiry/';?>"><i class="<?=get_arrangement_icon(META_PACKAGE_COURSE)?>"></i> Holiday</a></li>
				<?php } ?>
					<?php if ($transferv1_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_transfers_report/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
				<?php } ?>
			
				<?php if ($sightseen_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_sightseeing_report/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Sightseeing</a></li>
				<?php } ?>
				<?php if ($car_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_car_report/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>
				<?php } ?>
				</ul>
			</li> -->


		</ul>
			<!-- <li class="treeview">
			<a href="#"> <i class="fa fa-bed light_pink"></i> <span>
			Hotel CRS </span><i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li><a
					href="<?php echo base_url().'index.php/hotels/hotel_crs_list'?>"><i
						class="fa fa-credit-card text-blue"></i>Hotel List & Room Allocation</a></li>

                <li class=""><a href="<?php //echo base_url().'index.php/hotels/hotels_enquiry';?>">
                        <i class="fas fa-circle-notch"></i> Inquiry</a></li>

                <li class=""><a href="<?php //echo base_url().'index.php/hotels/quotation_list';?>">
                        <i class="fas fa-circle-notch"></i> Quotation List</a></li>

				<li><a
					href="<?php echo base_url().'index.php/hotels/hotel_types'?>"><i
						class="fa fa-h-square text-red"></i>Hotel Type</a></li>

				<li><a
					href="<?php echo base_url().'index.php/hotels/room_types'?>"><i
						class="fa fa-building text-green"></i>Room Type</a></li>

				<li class=""><a
					href="<?php echo base_url().'index.php/hotels/hotel_ammenities'?>"><i class="fa fa-tag text-yellow"></i> Hotel Amenities</a></li>
				<li class=""><a
					href="<?php echo base_url().'index.php/hotels/room_ammenities'?>"><i class="fa fa-database text-aqua"></i> Room Amenities</a></li>
			</ul>
		</li> -->
		
		<ul class="treeview-menu">
			<!--  TYPES -->
			<li class="treeview">
				<a href="<?php echo base_url().'index.php/transaction/logs'?>">
					<i class="fa fa-shield"></i> 
					<span> Transaction Logs </span>
				</a>
			</li>
			<li class="treeview">
				<a href="<?php echo base_url().'index.php/transaction/search_history'?>">
					<i class="fa fa-search"></i> 
					<span> Search History </span>
				</a>
			</li>
			<li class="treeview">
				<a href="<?php echo base_url().'index.php/transaction/top_destinations'?>">
					<i class="fa fa-globe"></i> 
					<span> Top Destinations</span>
				</a>
			</li>
			<li class="treeview">
				<a href="<?php echo base_url().'index.php/management/account_ledger'?>">
					<i class="fal fa-chart-bar "></i> 
					<span> Account Ledger</span>
				</a>
			</li>
		</ul>
	</li>
	<!-- <li class="treeview">
		<a href="#">
		<i class="fa fa-money-bill light_brown"></i> <span>Account</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'private_management/credit_balance'?>"><i class="far fa-circle"></i> Credit Balance</a></li>
			<li><a href="<?php echo base_url().'private_management/debit_balance'?>"><i class="far fa-circle"></i> Debit Balance</a></li>
		</ul>
	</li> -->

	<?php if($b2b) {?>
		<!-- <li class="treeview">
			<a href="#">
			<i class="fa fa-briefcase dark_green"></i> <span>Commission</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/management/agent_commission?default_commission='.ACTIVE;?>"><i class="far fa-circle"></i> Default Commission</a></li>
				<li><a href="<?php echo base_url().'index.php/management/agent_commission'?>"><i class="far fa-circle"></i> Agent's Commission</a></li>
			</ul>
		</li> -->
	<?php }?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-plus-square light_orange"></i> 
			<span> Markup </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- Markup TYPES -->
		<!-- <?php if($b2c) {?>
			<li><a href="#"><i class="far fa-circle"></i> B2C</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_airline_markup/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
				<?php } ?>

				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
				<?php } ?>

				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Villa</a></li>
				<?php } ?>

				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_bus_markup/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php }  ?>
				
				<?php if ($package_module) { ?>
				<li><a href="#"><i class="<?=get_arrangement_icon(META_PACKAGE_COURSE)?>"></i> Holiday</a></li>
				<?php }  ?>

				<?php
				   if($sightseen_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2c_sightseeing_markup/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Activity</a></li>

				<?php } ?>
				<?php
				   if($transferv1_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2c_transfer_markup/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i> Transfers</a></li>

				<?php } ?>
				<?php
				   if($car_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2c_car_markup/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>

				<?php    } 
				?>
				</ul>
			</li>
			<?php } ?> -->
			<?php
			if($b2b){	?>
			<li><a href="#"><i class="far fa-circle"></i> B2B</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_airline_markup/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_bus_markup/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>

				<?php
				   if($sightseen_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2b_sightseeing_markup/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Activity</a></li>

				<?php } ?>

				<?php if ($transferv1_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_transfer_markup/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
				<?php } ?>

				<?php
				   if($car_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2b_car_markup/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>

				<?php    } 
				?>
				</ul>
			</li>
		<?php } 
		if($corporate){	?>
			<!-- <li><a href="#"><i class="far fa-circle"></i> Corporate</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/corporate_airline_markup/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/corporate_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/corporate_bus_markup/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php }
				 ?>
				 <?php
				   if($sightseen_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/corporate_sightseeing_markup/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Activity</a></li>

				<?php    } 
				   if($transferv1_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/corporate_transfer_markup/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i> Transfers</a></li>

				<?php } ?>
				<?php
				   if($car_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2b_car_markup/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>

				<?php    } 
				?>
				</ul>
			</li> -->
		<?php } ?>

		</ul>
	</li>
	<?php } ?>
	<?php if($b2b){	?>
	<!-- <li class="treeview">
		<a href="#">
			<i class="fa fa-money-bill dark_red"></i> 
			<span> Master Balance Manager </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/management/master_balance_manager'?>"><i class="fa fa-circle-o"></i> API</a></li>-->
			<!--<li><a href="<?php echo base_url().'index.php/management/b2b_balance_manager'?>"><i class="far fa-circle"></i> B2B</a></li>
		</ul>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/management/master_balance_manager'?>"><i class="fa fa-circle-o"></i> API</a></li>-->
			<!--<li><a href="<?php echo base_url().'index.php/management/corporate_balance_manager'?>"><i class="far fa-circle"></i> Corporate</a></li>
		</ul>
	</li> -->
		<?php } 
		//if ($package_module) { ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-plus-square light_yellow"></i> 
			<span> Package Management </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<li><a href="<?php echo base_url().'index.php/supplier/view_packages_types'?>"><i class="far fa-circle"></i> View Package Types </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/add_with_price'?>"><i class="far fa-circle"></i> Add New Package </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/view_with_price'?>"><i class="far fa-circle"></i> View Packages </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/enquiries'?>"><i class="far fa-circle"></i> View Packages Enquiries </a></li>
		</ul>
	</li>
	<?php //} ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-envelope dark_grey"></i> 
			<span> Email Subscriptions </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<li><a href="<?php echo base_url().'index.php/general/view_subscribed_emails'?>"><i class="far fa-circle"></i> View Emails </a></li>
			<!-- <li><a href="<?php echo base_url().'index.php/supplier/add_with_price'?>"><i class="fa fa-circle-o"></i> Add New Package </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/view_with_price'?>"><i class="fa fa-circle-o"></i> View Packages </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/enquiries'?>"><i class="fa fa-circle-o"></i> View Packages Enquiries </a></li> -->
		</ul>
	</li>
	<?php } ?>
	<li class="treeview">
		<a href="#"> 
			<i class="fa fa-trophy blue"></i> <span>Rewards </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<!-- <li><a href="<?php echo base_url().'index.php/reward/add_rewards'?>"><i class="far fa-circle"></i> Add/Manage Rewards</a></li>
			<li><a href="<?php echo base_url().'index.php/reward/reward_range'?>"><i class="far fa-circle"></i>Set reward Range</a></li>
			<li><a href="<?php echo base_url().'index.php/reward/reward_conversion'?>"><i class="far fa-circle"></i> Rewards Conversion</a></li>
			<li><a href="<?php echo base_url().'index.php/reward/reward_report'?>"><i class="far fa-circle"></i> Rewards Report</a></li> -->

			<li><a href="<?php echo base_url().'index.php/reward/loyalitypoint'?>"><i class="far fa-circle"></i> Loyality Point</a></li>

		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop orange"></i>
			<span>CMS</span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/user/banner_images'?>"><i class="fa fa-image green"></i> <span>Main Banner Image</span></a></li>
			<li><a href="<?php echo base_url().'index.php/cms/add_cms_page'?>"><i class="fa fa-image red"></i> <span>Static Page content</span></a></li>
			
			<!-- Top Destinations START -->
				<?php if ($airline_module) { ?>
				<!-- <li class=""><a href="<?php //echo base_url().'index.php/cms/flight_top_destinations'?>"><i class="fa fa-plane sandal"></i> <span>Flight Top Destinations</span></a></li> -->
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/hotel_top_destinations'?>"><i class="fas fa-bed deep_blue"></i> <span>Hotel Top Destinations</span></a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<!-- <li class=""><a href="<?php //echo base_url().'index.php/cms/bus_top_destinations'?>"><i class="fa fa-bus violet"></i> <span>Bus Top Destinations</span></a></li> -->
				<?php } ?>
			<!-- Top Destinations END -->

			<!-- Deal start-->
				<li><a href="#"><i class="fa fa-gift greenish_blue"></i> Deals & Offers</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/flight_deals'?>"><i class="fa fa-plane yellowish_orange"></i> <span>Flight Deals</span></a></li>
				<?php } ?>
				<?php if ($accomodation_module || $bus_module || $package_module ||$sightseen_module || $car_module ||$transferv1_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/other_deal'?>"><i class="fas fa-bed text-green"></i> <span>Other Deals & Offers</span></a></li>
				<?php } ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/deal_banner'?>"><i class="fas fa-image text-fuchsia"></i> <span>Deals Banner</span></a></li>
			<!-- 	<?php //if ($bus_module) { ?>
				<li class=""><a href="<?php //echo base_url().'index.php/cms/bus_deals'?>"><i class="fa fa-bus"></i> <span>Bus Deals</span></a></li>
				<?php //} ?> -->
				</ul></li>	
			<!-- Deal End -->

			<!-- Blog Start-->
			<li class=""><a href="<?php echo base_url().'index.php/cms/blog'?>"><i class="fab fa-blogger light_brown"></i> <span>Blog</span></a></li>			
			<!-- Blog End -->
		</ul>
	</li>
	<li class="treeview">
			<a href="<?php echo base_url().'index.php/management/bank_account_details'?>">
				<i class="fa fa-university text-yellow"></i> <span>Bank Account Details</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
	</li>
	<!-- 
	<li class="treeview">
			<a href="<?php //echo base_url().'index.php/utilities/deal_sheets'?>">
				<i class="fa fa-hand-o-right "></i> <span>Deal Sheets</span>
			</a>
	</li>
	 -->

	<!--   <li class="treeview">
            <a href="#">
                <i class="fa fa-circle-o"></i> 
                <span> Offline </span><i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?php //echo base_url() . 'index.php/flight/offline_flight_book' ?>"><i class="fa fa-plane"></i>Flight</a></li>
            </ul>
        </li> -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-cogs text-navy"></i> 
			<span> Settings </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li>
				<a href="<?php echo base_url().'index.php/utilities/convenience_fees'?>"><i class="fa fa-credit-card text-teal"></i>Convenience Fees</a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/manage_promo_code'?>"><i class="fa fa-tag text-olive"></i>Promo Code</a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/manage_source'?>"><i class="fa fa-database orange"></i> Manage API</a>
			</li>

			<li class="hide">
				<a href="<?php echo base_url().'index.php/utilities/sms_checkpoint'?>"><i class="fa fa-envelope blue"></i> Manage SMS</a>
			</li>

			<?php if(is_domain_user() == false) { // ACCESS TO ONLY PROVAB ADMIN ?>
			<li>
				<a href="<?php echo base_url().'index.php/utilities/module'?>"><i class="far fa-circle dark_green"></i> <span>Manage Modules</span>
				</a>
			</li>
			<?php }?>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/currency_converter'?>"><i class="fas fa-rupee-sign red"></i> Currency Conversion </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/management/event_logs'?>"><i class="fa fa-shield violet"></i> <span> Event Logs </span></a>
			</li>

			<li class="hide">
				<a href="<?php echo base_url().'index.php/utilities/app_settings'?>"><i class="fa fa-laptop greenish_blue"></i> Appearance </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/social_network'?>"><i class="fab fa-facebook-square yellowish_orange"></i> Social Networks </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/social_login'?>"><i class="fab fa-facebook light_brown"></i> Social Login </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/user/manage_domain'?>">
					<i class="fa fa-image light_orange"></i> <span>Manage Domain</span>
				</a>
			</li>

			<li>
				<a href="<?php echo base_url()?>index.php/utilities/timeline"><i class="fa fa-desktop light_yellow"></i> <span>Live Events</span></a>
			</li>

			<!-- <li>
				<a href="<?=base_url().'index.php/utilities/trip_calendar'?>"><i class="fa fa-calendar"></i> <span>Trip Calendar</span></a>
            </li> -->			
		</ul>
	</li>
</ul>
<?php } ?>
<?php if($this->entity_user_type == 2){ ?>
<ul class="sidebar-menu" id="magical-menu">
	<!-- <?php //if(in_array(1, $privillege)){ ?> -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-tachometer-alt light_green"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url()?>"><i class="fa fa-bars text-fuchsia"></i> Dashboard v1</a></li>
		</ul>
	</li>
	<!-- <?php //} ?> -->
	<?php if(is_domain_user() == false) { // ACCESS TO ONLY PROVAB ADMIN ?>
	<li class="treeview">
		<a href="#">
		<i class="fa fa-wrench"></i> <span>Management</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/user/user_management'?>"><i class="fa fa-user"></i> User</a></li>
			<li><a href="<?php echo base_url().'index.php/user/domain_management'?>"><i class="fa fa-laptop"></i> Domain</a></li>
			<li><a href="<?php echo base_url().'index.php/module/module_management'?>"><i class="fa fa-sitemap"></i> Master Module</a></li>
		</ul>
	</li>
	<?php if ($any_domain_module) {?>
	<li class="treeview">
		<a href="#">
		<i class="fa fa-user"></i> <span>Markup</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<?php if ($airline_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/airline_domain_markup'?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
			<?php } ?>
			<?php if ($accomodation_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/hotel_domain_markup'?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
			<?php } ?>
			<?php if ($bus_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/bus_domain_markup'?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
			<?php } ?>
			<?php if ($transferv1_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/transfer_domain_markup'?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
			<?php } ?>

			<?php if ($sightseen_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/sightseeing_domain_markup'?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i>Sightseeing</a></li>
			<?php } ?>

		</ul>
	</li>
	<?php } ?>
	<li class="treeview">
		<a href="<?php echo base_url().'index.php/private_management/process_balance_manager'?>">
			<i class="fa fa-google-wallet"></i> 
			<span> Master Balance Manager </span>
		</a>
	</li>
	<li class="treeview">
		<a href="<?php echo base_url().'index.php/private_management/event_logs'?>">
			<i class="fa fa-shield"></i> 
			<span> Event Logs </span>
		</a>
	</li>
	<?php } else if((is_domain_user() == true)) {
		// ACCESS TO ONLY DOMAIN ADMIN
	?>
	<!-- USER ACCOUNT MANAGEMENT -->
	<?php if(in_array(2, $privillege)){ ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-user dark_pink"></i> 
			<span> Users </span><i class="fa fa-angle-left pull-right"></i></a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<?php if($b2c){	?>
			<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER;?>"><i class="far fa-circle"></i> B2C</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.B2C_USER;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php } ?>
			<?php if($b2b){	?>
			<li><a href="<?php echo base_url().'index.php/user/b2b_user?filter=user_type&q='.B2B_USER ?>"><i class="far fa-circle"></i> Agents</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/b2b_user?user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/b2b_user?user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.B2B_USER;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php }?>
			<?php if($corporate){	?>
			<li><a href="<?php echo base_url().'index.php/user/corporate_user?filter=user_type&q='.CORPORATE_USER ?>"><i class="far fa-circle"></i> Corporate</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/corporate_user?user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/corporate_user?user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.CORPORATE_USER;?>"><i class="fa fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php }?>
		
			<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUPPLIER ?>"><i class="far fa-circle"></i> Supplier</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUPPLIER.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUPPLIER.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.SUPPLIER;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<?php } ?>
	<?php if(in_array(3, $privillege)){ ?>
	<?php if ($any_domain_module) {?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-chart-bar dark_blue"></i> 
			<span> Reports </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<li><a href="#"><i class="far fa-circle"></i> B2C</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				<li><a href="<?php echo base_url().'index.php/report/b2c_villa_report/';?>"><i class="fa fa-bed"></i> Villa</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_crs_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel CRS</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>
				
				<?php
				   if($transferv1_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/report/b2c_transfers_report/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i> Transfer</a></li>

				<?php    } 
				?>

				<?php
				   if($sightseen_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/report/b2c_sightseeing_report/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Sightseeing</a></li>

				<?php    } 
				?>
				<?php if ($car_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_car_report/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>
				<?php } ?>


				</ul>
			</li>
           <li><a href="#"><i class="far fa-circle"></i> Agent</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				<li><a href="<?php echo base_url().'index.php/report/b2b_villa_report/';?>"><i class="fa fa-bed"></i> Villa</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_crs_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel CRS</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>

				<?php if ($transferv1_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_transfers_report/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
				<?php } ?>
			
				<?php if ($sightseen_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_sightseeing_report/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Sightseeing</a></li>
				<?php } ?>
				<?php if ($car_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_car_report/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>
				<?php } ?>
				</ul>
			</li>
			<li><a href="#"><i class="far fa-circle"></i> Corporate</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				<li><a href="<?php echo base_url().'index.php/report/corporate_crs_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel CRS</a></li>
				<li><a href="<?php echo base_url().'index.php/report/corporate_villa_report/';?>"><i class="fa fa-bed"></i> Villa</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>
					<?php if ($transferv1_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_transfers_report/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
				<?php } ?>
			
				<?php if ($sightseen_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_sightseeing_report/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Sightseeing</a></li>
				<?php } ?>
				<?php if ($car_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_car_report/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>
				<?php } ?>
				</ul>
			</li>


		</ul>
		</li>
		<?php } ?>
		<?php } ?>
		<?php if(in_array(4, $privillege)){ ?>
			<li class="treeview">
			<a href="#"> <i class="fa fa-bed light_pink"></i> <span>
			Hotel CRS </span><i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li><a
					href="<?php echo base_url().'index.php/hotels/hotel_crs_list'?>"><i
						class="fa fa-credit-card text-blue"></i>Hotel List & Room Allocation</a></li>

              <!--   <li class=""><a href="<?php //echo base_url().'index.php/hotels/hotels_enquiry';?>">
                        <i class="fas fa-circle-notch"></i> Inquiry</a></li>

                <li class=""><a href="<?php //echo base_url().'index.php/hotels/quotation_list';?>">
                        <i class="fas fa-circle-notch"></i> Quotation List</a></li>
 -->
				<li><a
					href="<?php echo base_url().'index.php/hotels/hotel_types'?>"><i
						class="fa fa-h-square text-red"></i>Hotel Type</a></li>

				<li><a
					href="<?php echo base_url().'index.php/hotels/room_types'?>"><i
						class="fa fa-building text-green"></i>Room Type</a></li>

				<li class=""><a
					href="<?php echo base_url().'index.php/hotels/hotel_ammenities'?>"><i class="fa fa-tag text-yellow"></i> Hotel Amenities</a></li>
				<li class=""><a
					href="<?php echo base_url().'index.php/hotels/room_ammenities'?>"><i class="fa fa-database text-aqua"></i> Room Amenities</a></li>
			</ul>
		</li>
		<?php } ?>

		<?php if(in_array(5, $privillege)){ ?>
	<li class="treeview">
		<a href="#">
		<i class="fa fa-money-bill light_brown"></i> <span>Account</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'private_management/credit_balance'?>"><i class="far fa-circle"></i> Credit Balance</a></li>
			<li><a href="<?php echo base_url().'private_management/debit_balance'?>"><i class="far fa-circle"></i> Debit Balance</a></li>
		</ul>
	</li>
	<?php } ?>
	<?php if(in_array(6, $privillege)){ ?>
	<?php if($b2b) {?>
		<li class="treeview">
			<a href="#">
			<i class="fa fa-briefcase dark_green"></i> <span>Commission</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/management/agent_commission?default_commission='.ACTIVE;?>"><i class="far fa-circle"></i> Default Commission</a></li>
				<li><a href="<?php echo base_url().'index.php/management/agent_commission'?>"><i class="far fa-circle"></i> Agent's Commission</a></li>
			</ul>
		</li>
	<?php }?>
	<?php } ?>
	<?php if(in_array(7, $privillege)){ ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-plus-square light_orange"></i> 
			<span> Markup </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- Markup TYPES -->
		<?php if($b2c) {?>
			<li><a href="#"><i class="far fa-circle"></i> B2C</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_airline_markup/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
				<?php } ?>

				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
				<?php } ?>

				<!-- <?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Villa</a></li>
				<?php } ?> -->

				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_bus_markup/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php }  ?>
				
				<?php if ($package_module) { ?>
				<li><a href="#"><i class="<?=get_arrangement_icon(META_PACKAGE_COURSE)?>"></i> Holiday</a></li>
				<?php }  ?>

				<?php
				   if($sightseen_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2c_sightseeing_markup/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Activity</a></li>

				<?php } ?>
				<?php
				   if($transferv1_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2c_transfer_markup/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i> Transfers</a></li>

				<?php } ?>
				<?php
				   if($car_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2c_car_markup/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>

				<?php    } 
				?>
				</ul>
			</li>
			<?php } 
			if($b2b){	?>
			<li><a href="#"><i class="far fa-circle"></i> B2B</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_airline_markup/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_bus_markup/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>

				<?php
				   if($sightseen_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2b_sightseeing_markup/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Activity</a></li>

				<?php } ?>

				<?php if ($transferv1_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_transfer_markup/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
				<?php } ?>

				<?php
				   if($car_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2b_car_markup/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>

				<?php    } 
				?>
				</ul>
			</li>
			<?php } ?>
		</ul>
	</li>
	<?php } ?>
	<?php if(in_array(8, $privillege)){ ?>
	<?php if($b2b){	?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-money-bill dark_red"></i> 
			<span> Master Balance Manager </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<!--<li><a href="<?php echo base_url().'index.php/management/master_balance_manager'?>"><i class="fa fa-circle-o"></i> API</a></li>-->
			<li><a href="<?php echo base_url().'index.php/management/b2b_balance_manager'?>"><i class="far fa-circle"></i> B2B</a></li>
		</ul>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<!--<li><a href="<?php echo base_url().'index.php/management/master_balance_manager'?>"><i class="fa fa-circle-o"></i> API</a></li>-->
			<li><a href="<?php echo base_url().'index.php/management/corporate_balance_manager'?>"><i class="far fa-circle"></i> Corporate</a></li>
		</ul>
	</li>
		<?php } ?>
		<?php } ?>
		<?php if(in_array(9, $privillege)){ ?>
		<?php if ($package_module) { ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-plus-square light_yellow"></i> 
			<span> Package Management </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<li><a href="<?php echo base_url().'index.php/supplier/view_packages_types'?>"><i class="far fa-circle"></i> View Package Types </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/add_with_price'?>"><i class="far fa-circle"></i> Add New Package </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/view_with_price'?>"><i class="far fa-circle"></i> View Packages </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/enquiries'?>"><i class="far fa-circle"></i> View Packages Enquiries </a></li>
		</ul>
	</li>
	<?php } ?>
	<?php } ?>
	<?php if(in_array(10, $privillege)){ ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-envelope dark_grey"></i> 
			<span> Email Subscriptions </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<li><a href="<?php echo base_url().'index.php/general/view_subscribed_emails'?>"><i class="far fa-circle"></i> View Emails </a></li>
			<!-- <li><a href="<?php echo base_url().'index.php/supplier/add_with_price'?>"><i class="fa fa-circle-o"></i> Add New Package </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/view_with_price'?>"><i class="fa fa-circle-o"></i> View Packages </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/enquiries'?>"><i class="fa fa-circle-o"></i> View Packages Enquiries </a></li> -->
		</ul>
	</li>
	<?php } ?>
	<?php if(in_array(11, $privillege)){ ?>
	<li class="treeview">
		<a href="#"> 
			<i class="fa fa-trophy blue"></i> <span>Rewards </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/reward/add_rewards'?>"><i class="far fa-circle"></i> Add/Manage Rewards</a></li>
			<li><a href="<?php echo base_url().'index.php/reward/reward_range'?>"><i class="far fa-circle"></i>Set reward Range</a></li>
			<li><a href="<?php echo base_url().'index.php/reward/reward_conversion'?>"><i class="far fa-circle"></i> Rewards Conversion</a></li>
			<li><a href="<?php echo base_url().'index.php/reward/reward_report'?>"><i class="far fa-circle"></i> Rewards Report</a></li>
		</ul>
	</li>
	<?php } ?>
	<?php if(in_array(12, $privillege)){ ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop orange"></i>
			<span>CMS</span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/user/banner_images'?>"><i class="fa fa-image green"></i> <span>Main Banner Image</span></a></li>
			<li><a href="<?php echo base_url().'index.php/cms/add_cms_page'?>"><i class="fa fa-image red"></i> <span>Static Page content</span></a></li>
			
			<!-- Top Destinations START -->
				<?php if ($airline_module) { ?>
				<!-- <li class=""><a href="<?php //echo base_url().'index.php/cms/flight_top_destinations'?>"><i class="fa fa-plane sandal"></i> <span>Flight Top Destinations</span></a></li> -->
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/hotel_top_destinations'?>"><i class="fas fa-bed deep_blue"></i> <span>Hotel Top Destinations</span></a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<!-- <li class=""><a href="<?php //echo base_url().'index.php/cms/bus_top_destinations'?>"><i class="fa fa-bus violet"></i> <span>Bus Top Destinations</span></a></li> -->
				<?php } ?>
			<!-- Top Destinations END -->

			<!-- Deal start-->
				<li><a href="#"><i class="fa fa-gift greenish_blue"></i> Deals & Offers</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/flight_deals'?>"><i class="fa fa-plane yellowish_orange"></i> <span>Flight Deals</span></a></li>
				<?php } ?>
				<?php if ($accomodation_module || $bus_module || $package_module ||$sightseen_module || $car_module ||$transferv1_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/other_deal'?>"><i class="fas fa-bed text-green"></i> <span>Other Deals & Offers</span></a></li>
				<?php } ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/deal_banner'?>"><i class="fas fa-image text-fuchsia"></i> <span>Deals Banner</span></a></li>
			<!-- 	<?php //if ($bus_module) { ?>
				<li class=""><a href="<?php //echo base_url().'index.php/cms/bus_deals'?>"><i class="fa fa-bus"></i> <span>Bus Deals</span></a></li>
				<?php //} ?> -->
				</ul></li>	
			<!-- Deal End -->

			<!-- Blog Start-->
			<li class=""><a href="<?php echo base_url().'index.php/cms/blog'?>"><i class="fab fa-blogger light_brown"></i> <span>Blog</span></a></li>			
			<!-- Blog End -->
		</ul>
	</li>
	<?php } ?>
	<?php if(in_array(13, $privillege)){ ?>
	<li class="treeview">
			<a href="<?php echo base_url().'index.php/management/bank_account_details'?>">
				<i class="fa fa-university text-yellow"></i> <span>Bank Account Details</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
	</li>
	<?php } ?>
	<!-- 
	<li class="treeview">
			<a href="<?php //echo base_url().'index.php/utilities/deal_sheets'?>">
				<i class="fa fa-hand-o-right "></i> <span>Deal Sheets</span>
			</a>
	</li>
	 -->

	<!--   <li class="treeview">
            <a href="#">
                <i class="fa fa-circle-o"></i> 
                <span> Offline </span><i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?php //echo base_url() . 'index.php/flight/offline_flight_book' ?>"><i class="fa fa-plane"></i>Flight</a></li>
            </ul>
        </li> -->
    <?php if(in_array(14, $privillege)){ ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-cogs text-navy"></i> 
			<span> Settings </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li>
				<a href="<?php echo base_url().'index.php/utilities/convenience_fees'?>"><i class="fa fa-credit-card text-teal"></i>Convenience Fees</a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/manage_promo_code'?>"><i class="fa fa-tag text-olive"></i>Promo Code</a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/manage_source'?>"><i class="fa fa-database orange"></i> Manage API</a>
			</li>

			<li class="hide">
				<a href="<?php echo base_url().'index.php/utilities/sms_checkpoint'?>"><i class="fa fa-envelope blue"></i> Manage SMS</a>
			</li>

			<?php if(is_domain_user() == false) { // ACCESS TO ONLY PROVAB ADMIN ?>
			<li>
				<a href="<?php echo base_url().'index.php/utilities/module'?>"><i class="far fa-circle dark_green"></i> <span>Manage Modules</span>
				</a>
			</li>
			<?php }?>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/currency_converter'?>"><i class="fas fa-rupee-sign red"></i> Currency Conversion </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/management/event_logs'?>"><i class="fa fa-shield violet"></i> <span> Event Logs </span></a>
			</li>

			<li class="hide">
				<a href="<?php echo base_url().'index.php/utilities/app_settings'?>"><i class="fa fa-laptop greenish_blue"></i> Appearance </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/social_network'?>"><i class="fab fa-facebook-square yellowish_orange"></i> Social Networks </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/social_login'?>"><i class="fab fa-facebook light_brown"></i> Social Login </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/user/manage_domain'?>">
					<i class="fa fa-image light_orange"></i> <span>Manage Domain</span>
				</a>
			</li>

			<li>
				<a href="<?php echo base_url()?>index.php/utilities/timeline"><i class="fa fa-desktop light_yellow"></i> <span>Live Events</span></a>
			</li>

			<!-- <li>
				<a href="<?=base_url().'index.php/utilities/trip_calendar'?>"><i class="fa fa-calendar"></i> <span>Trip Calendar</span></a>
            </li> -->			
		</ul>
	</li>
	<?php } ?>
</ul>


<?php } ?>
<?php } ?>
