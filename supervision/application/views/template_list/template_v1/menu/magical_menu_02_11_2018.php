<?php
$active_domain_modules = $this->active_domain_modules;
/**
 * Need to make privilege based system
 * Privilege only for loading menu and access of the web page
 * 
 * Data loading will not be based on privilege.
 * Data loading logic will be different.
 * It depends on many parameters
 */
$menu_list = array();
if (count($active_domain_modules) > 0) {
	$any_domain_module = true;
} else {
	$any_domain_module = false;
}
$airline_module = is_active_airline_module();
$accomodation_module = is_active_hotel_module();
$bus_module = is_active_bus_module();
$package_module = is_active_package_module();
$sightseen_module = is_active_sightseeing_module();
$car_module = is_active_car_module();
$transferv1_module = is_active_transferv1_module();
$bb='b2b';
$bc='b2c';
$corp='corporate';
$b2b = is_active_module($bb);
$b2c = is_active_module($bc);
$corporate= is_active_module($corp);
//checking social login status 
$social_login = 'facebook';
$social = is_active_social_login($social_login);
//echo "ela".$accomodation_module;exit;
$accomodation_module = 1;
?>
<ul class="sidebar-menu" id="magical-menu">
	<li class="treeview">
		<a href="#">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url()?>"><i class="fa fa-bars"></i> Dashboard v1</a></li>
		</ul>
	</li>
	<?php if(is_domain_user() == false) { // ACCESS TO ONLY PROVAB ADMIN ?>
	<li class="treeview">
		<a href="#">
		<i class="fa fa-wrench"></i> <span>Management</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/user/user_management'?>"><i class="fa fa-user"></i> User</a></li>
			<li><a href="<?php echo base_url().'index.php/user/domain_management'?>"><i class="fa fa-laptop"></i> Domain</a></li>
			<li><a href="<?php echo base_url().'index.php/module/module_management'?>"><i class="fa fa-sitemap"></i> Master Module</a></li>
		</ul>
	</li>
	<?php if ($any_domain_module) {?>
	<li class="treeview">
		<a href="#">
		<i class="fa fa-user"></i> <span>Markup</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<?php if ($airline_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/airline_domain_markup'?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
			<?php } ?>
			<?php if ($accomodation_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/hotel_domain_markup'?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
			<?php } ?>
			<?php if ($bus_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/bus_domain_markup'?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
			<?php } ?>
			<?php if ($transferv1_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/transfer_domain_markup'?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
			<?php } ?>

			<?php if ($sightseen_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/sightseeing_domain_markup'?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i>Sightseeing</a></li>
			<?php } ?>

		</ul>
	</li>
	<?php } ?>
	<li class="treeview">
		<a href="<?php echo base_url().'index.php/private_management/process_balance_manager'?>">
			<i class="fa fa-google-wallet"></i> 
			<span> Master Balance Manager </span>
		</a>
	</li>
	<li class="treeview">
		<a href="<?php echo base_url().'index.php/private_management/event_logs'?>">
			<i class="fa fa-shield"></i> 
			<span> Event Logs </span>
		</a>
	</li>
	<?php } else if((is_domain_user() == true)) {
		// ACCESS TO ONLY DOMAIN ADMIN
	?>
	<!-- USER ACCOUNT MANAGEMENT -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-user"></i> 
			<span> Users </span><i class="fa fa-angle-left pull-right"></i></a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<?php if($b2c){	?>
			<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER;?>"><i class="far fa-circle"></i> B2C</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.B2C_USER;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php } ?>
			<?php if($b2b){	?>
			<li><a href="<?php echo base_url().'index.php/user/b2b_user?filter=user_type&q='.B2B_USER ?>"><i class="far fa-circle"></i> Agents</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/b2b_user?user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/b2b_user?user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.B2B_USER;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php }?>
			<?php if($corporate){	?>
			<li><a href="<?php echo base_url().'index.php/user/corporate_user?filter=user_type&q='.CORPORATE_USER ?>"><i class="far fa-circle"></i> Corporate</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/corporate_user?user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/corporate_user?user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.CORPORATE_USER;?>"><i class="fa fa-circle-o"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php }?>
			<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUB_ADMIN ?>"><i class="far fa-circle"></i> Sub Admin</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUB_ADMIN.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUB_ADMIN.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.SUB_ADMIN;?>"><i class="far fa-circle"></i> Logged In User</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<?php if ($any_domain_module) {?>
	<li class="treeview">
		<a href="#">
			<i class="fas fa-chart-bar"></i> 
			<span> Reports </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<li><a href="#"><i class="far fa-circle"></i> B2C</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_crs_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel CRS</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>
				
				<?php
				   if($transferv1_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/report/b2c_transfers_report/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i> Transfer</a></li>

				<?php    } 
				?>

				<?php
				   if($sightseen_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/report/b2c_sightseeing_report/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i> Sightseeing</a></li>

				<?php    } 
				?>
				<?php if ($car_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_car_report/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>
				<?php } ?>


				</ul>
			</li>
           <li><a href="#"><i class="far fa-circle"></i> Agent</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_crs_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel CRS</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>

				<?php if ($transferv1_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_transfers_report/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
				<?php } ?>
			
				<?php if ($sightseen_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_sightseeing_report/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Sightseeing</a></li>
				<?php } ?>
				<?php if ($car_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_car_report/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>
				<?php } ?>
				</ul>
			</li>
			<li><a href="#"><i class="far fa-circle"></i> Corporate</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>
					<?php if ($transferv1_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_transfers_report/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
				<?php } ?>
			
				<?php if ($sightseen_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_sightseeing_report/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Sightseeing</a></li>
				<?php } ?>
				<?php if ($car_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/corporate_car_report/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>
				<?php } ?>
				</ul>
			</li>


		</ul>
			<li class="treeview">
			<a href="#"> <i class="fa fa-bed"></i> <span>
			Hotel CRS </span><i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li><a
					href="<?php echo base_url().'index.php/hotels/hotel_crs_list'?>"><i
						class="fa fa-credit-card"></i>Hotel List & Room Allocation</a></li>

              <!--   <li class=""><a href="<?php //echo base_url().'index.php/hotels/hotels_enquiry';?>">
                        <i class="fas fa-circle-notch"></i> Inquiry</a></li>

                <li class=""><a href="<?php //echo base_url().'index.php/hotels/quotation_list';?>">
                        <i class="fas fa-circle-notch"></i> Quotation List</a></li>
 -->
				<li><a
					href="<?php echo base_url().'index.php/hotels/hotel_types'?>"><i
						class="fa fa-tag"></i>Hotel Type</a></li>

				<li><a
					href="<?php echo base_url().'index.php/hotels/room_types'?>"><i
						class="fa fa-tag"></i>Room Type</a></li>

				<li class=""><a
					href="<?php echo base_url().'index.php/hotels/hotel_ammenities'?>"><i class="fa fa-database"></i> Hotel Amenities</a></li>
				<li class=""><a
					href="<?php echo base_url().'index.php/hotels/room_ammenities'?>"><i class="fa fa-database"></i> Room Amenities</a></li>
			</ul>
		</li>
		
		<ul class="treeview-menu">
			<!--  TYPES -->
			<li class="treeview">
				<a href="<?php echo base_url().'index.php/transaction/logs'?>">
					<i class="fa fa-shield"></i> 
					<span> Transaction Logs </span>
				</a>
			</li>
			<li class="treeview">
				<a href="<?php echo base_url().'index.php/transaction/search_history'?>">
					<i class="fa fa-search"></i> 
					<span> Search History </span>
				</a>
			</li>
			<li class="treeview">
				<a href="<?php echo base_url().'index.php/transaction/top_destinations'?>">
					<i class="fa fa-globe"></i> 
					<span> Top Destinations</span>
				</a>
			</li>
			<li class="treeview">
				<a href="<?php echo base_url().'index.php/management/account_ledger'?>">
					<i class="fal fa-chart-bar "></i> 
					<span> Account Ledger</span>
				</a>
			</li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
		<i class="fal fa-money-bill"></i> <span>Account</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'private_management/credit_balance'?>"><i class="far fa-circle"></i> Credit Balance</a></li>
			<li><a href="<?php echo base_url().'private_management/debit_balance'?>"><i class="far fa-circle"></i> Debit Balance</a></li>
		</ul>
	</li>

	<?php if($b2b) {?>
		<li class="treeview">
			<a href="#">
			<i class="fa fa-briefcase"></i> <span>Commission</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/management/agent_commission?default_commission='.ACTIVE;?>"><i class="far fa-circle"></i> Default Commission</a></li>
				<li><a href="<?php echo base_url().'index.php/management/agent_commission'?>"><i class="far fa-circle"></i> Agent's Commission</a></li>
			</ul>
		</li>
	<?php }?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-plus-square"></i> 
			<span> Markup </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- Markup TYPES -->
		<?php if($b2c) {?>
			<li><a href="#"><i class="far fa-circle"></i> B2C</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_airline_markup/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_bus_markup/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php }  ?>

				<?php
				   if($transferv1_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2c_transfer_markup/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i> Transfers</a></li>

				<?php    } 
				?>


				<?php
				   if($sightseen_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2c_sightseeing_markup/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Sightseeing</a></li>

				<?php    } 
				?>
				<?php
				   if($car_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2c_car_markup/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>

				<?php    } 
				?>
				</ul>
			</li>
			<?php } 
			if($b2b){	?>
			<li><a href="#"><i class="far fa-circle"></i> B2B</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_airline_markup/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_bus_markup/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>

				<?php if ($transferv1_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_transfer_markup/';?>"><i class="<?=get_arrangement_icon(META_TRANSFERV1_COURSE)?>"></i>Transfers</a></li>
				<?php } ?>


				<?php
				   if($sightseen_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2b_sightseeing_markup/';?>"><i class="<?=get_arrangement_icon(META_SIGHTSEEING_COURSE)?>"></i> Sightseeing</a></li>

				<?php    } 
				?>
				<?php
				   if($car_module){ ?>
				   <li><a href="<?php echo base_url().'index.php/management/b2b_car_markup/';?>"><i class="<?=get_arrangement_icon(META_CAR_COURSE)?>"></i> Car</a></li>

				<?php    } 
				?>
				</ul>
			</li>
			<?php } ?>
		</ul>
	</li>
	<?php } ?>
	<?php if($b2b){	?>
	<li class="treeview">
		<a href="#">
			<i class="fal fa-money-bill"></i> 
			<span> Master Balance Manager </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<!--<li><a href="<?php echo base_url().'index.php/management/master_balance_manager'?>"><i class="fa fa-circle-o"></i> API</a></li>-->
			<li><a href="<?php echo base_url().'index.php/management/b2b_balance_manager'?>"><i class="far fa-circle"></i> B2B</a></li>
		</ul>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<!--<li><a href="<?php echo base_url().'index.php/management/master_balance_manager'?>"><i class="fa fa-circle-o"></i> API</a></li>-->
			<li><a href="<?php echo base_url().'index.php/management/corporate_balance_manager'?>"><i class="far fa-circle"></i> Corporate</a></li>
		</ul>
	</li>
		<?php } if ($package_module) { ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-plus-square"></i> 
			<span> Package Management </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<li><a href="<?php echo base_url().'index.php/supplier/view_packages_types'?>"><i class="far fa-circle"></i> View Package Types </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/add_with_price'?>"><i class="far fa-circle"></i> Add New Package </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/view_with_price'?>"><i class="far fa-circle"></i> View Packages </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/enquiries'?>"><i class="far fa-circle"></i> View Packages Enquiries </a></li>
		</ul>
	</li>
	<?php } ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-envelope"></i> 
			<span> Email Subscriptions </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<li><a href="<?php echo base_url().'index.php/general/view_subscribed_emails'?>"><i class="far fa-circle"></i> View Emails </a></li>
			<!-- <li><a href="<?php echo base_url().'index.php/supplier/add_with_price'?>"><i class="fa fa-circle-o"></i> Add New Package </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/view_with_price'?>"><i class="fa fa-circle-o"></i> View Packages </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/enquiries'?>"><i class="fa fa-circle-o"></i> View Packages Enquiries </a></li> -->
		</ul>
	</li>
	<?php } ?>
	<li class="treeview">
		<a href="#"> 
			<i class="fa fa-trophy"></i> <span>Rewards </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<!-- USER TYPES -->
			<!--<li><a href="<?php echo base_url().'index.php/reward/index'?>"><i class="fa fa-circle-o"></i> API</a></li>-->
			<li><a
				href="<?php echo base_url().'index.php/reward/add_rewards'?>"><i
					class="fa fa-circle-o"></i> Add/Manage Rewards</a></li>
					<li><a href="<?php echo base_url().'index.php/reward/reward_report'?>"><i
					class="fa fa-circle-o"></i> Rewards Report</a></li>
					<li><a
				href="<?php echo base_url().'index.php/reward/reward_conversion'?>"><i
					class="fa fa-circle-o"></i> Rewards Conversion</a></li>
					<li><a
				href="<?php echo base_url().'index.php/reward/reward_range'?>"><i
					class="fa fa-circle-o"></i>Set reward Range</a></li>
					
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop"></i>
			<span>CMS</span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/user/banner_images'?>"><i class="fa fa-image"></i> <span>Main Banner Image</span></a></li>
			<li><a href="<?php echo base_url().'index.php/cms/add_cms_page'?>"><i class="fa fa-image"></i> <span>Static Page content</span></a></li>
			
			<!-- Top Destinations START -->
				<?php if ($airline_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/flight_top_destinations'?>"><i class="fa fa-plane"></i> <span>Flight Top Destinations</span></a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/hotel_top_destinations'?>"><i class="fas fa-bed"></i> <span>Hotel Top Destinations</span></a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/bus_top_destinations'?>"><i class="fa fa-bus"></i> <span>Bus Top Destinations</span></a></li>
				<?php } ?>
			<!-- Top Destinations END -->

			<!-- Deal start-->
				<li><a href="#"><i class="far fa-gift"></i> Deals & Offers</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/flight_deals'?>"><i class="fa fa-plane"></i> <span>Flight Deals</span></a></li>
				<?php } ?>
				<?php if ($accomodation_module || $bus_module || $package_module ||$sightseen_module || $car_module ||$transferv1_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/other_deal'?>"><i class="fas fa-bed"></i> <span>Other Deals & Offers</span></a></li>
				<?php } ?>
			<!-- 	<?php //if ($bus_module) { ?>
				<li class=""><a href="<?php //echo base_url().'index.php/cms/bus_deals'?>"><i class="fa fa-bus"></i> <span>Bus Deals</span></a></li>
				<?php //} ?> -->
				</ul></li>	
			<!-- Deal End -->

			<!-- Blog Start-->
			<li class=""><a href="<?php echo base_url().'index.php/cms/blog'?>"><i class="fa fa-blog"></i> <span>Blog</span></a></li>			
			<!-- Blog End -->
		</ul>
	</li>
	<li class="treeview">
			<a href="<?php echo base_url().'index.php/management/bank_account_details'?>">
				<i class="fa fa-university"></i> <span>Bank Account Details</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
	</li>
	<!-- 
	<li class="treeview">
			<a href="<?php //echo base_url().'index.php/utilities/deal_sheets'?>">
				<i class="fa fa-hand-o-right "></i> <span>Deal Sheets</span>
			</a>
	</li>
	 -->

	<!--   <li class="treeview">
            <a href="#">
                <i class="fa fa-circle-o"></i> 
                <span> Offline </span><i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?php //echo base_url() . 'index.php/flight/offline_flight_book' ?>"><i class="fa fa-plane"></i>Flight</a></li>
            </ul>
        </li> -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-cogs"></i> 
			<span> Settings </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li>
				<a href="<?php echo base_url().'index.php/utilities/convenience_fees'?>"><i class="fa fa-credit-card"></i>Convenience Fees</a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/manage_promo_code'?>"><i class="fa fa-tag"></i>Promo Code</a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/manage_source'?>"><i class="fa fa-database"></i> Manage API</a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/sms_checkpoint'?>"><i class="fa fa-envelope"></i> Manage SMS</a>
			</li>

			<?php if(is_domain_user() == false) { // ACCESS TO ONLY PROVAB ADMIN ?>
			<li>
				<a href="<?php echo base_url().'index.php/utilities/module'?>"><i class="far fa-circle"></i> <span>Manage Modules</span>
				</a>
			</li>
			<?php }?>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/currency_converter'?>"><i class="fas fa-rupee-sign"></i> Currency Conversion </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/management/event_logs'?>"><i class="fa fa-shield"></i> <span> Event Logs </span></a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/app_settings'?>"><i class="fa fa-laptop"></i> Appearance </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/social_network'?>"><i class="fa fa-facebook-square "></i> Social Networks </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/utilities/social_login'?>"><i class="fa fa-facebook  "></i> Social Login </a>
			</li>

			<li>
				<a href="<?php echo base_url().'index.php/user/manage_domain'?>">
					<i class="fa fa-image"></i> <span>Manage Domain</span>
				</a>
			</li>

			<li>
				<a href="<?php echo base_url()?>index.php/utilities/timeline"><i class="fa fa-desktop"></i> <span>Live Events</span></a>
			</li>

			<!-- <li>
				<a href="<?=base_url().'index.php/utilities/trip_calendar'?>"><i class="fa fa-calendar"></i> <span>Trip Calendar</span></a>
            </li> -->			
		</ul>
	</li>
</ul>
