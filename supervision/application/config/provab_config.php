<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['master_module_list']	= array(
	META_AIRLINE_COURSE => 'flight',	
	META_ACCOMODATION_COURSE => 'hotel',
	META_BUS_COURSE => 'bus',
	META_PACKAGE_COURSE => 'package',
	META_TRANSFERV1_COURSE=>'transfers',
	META_SIGHTSEEING_COURSE=>'sightseeing'
	//META_CAR_COURSE => 'car',
	
);
/******** Current Module ********/
$config['current_module'] = 'admin';

/******** BOOKING ENGINE START ********/
$config['flight_engine_system'] = 'live'; //test/live
$config['hotel_engine_system'] = 'live'; //test/live
$config['bus_engine_system'] = 'live'; //test/live
$config['external_service_system'] = 'live'; //test/live
$config['sightseeing_engine_system'] = 'live';
$config['car_engine_system'] = 'test'; //test/live
$config['transfer_engine_system']='live';


$config['domain_key'] = CURRENT_DOMAIN_KEY;
$config['test_username'] = 'test229267';
$config['test_password'] = 'test@229';

//Local URL


//$config['flight_url'] = 'http://192.168.0.46/travelomatix_services/webservices/index.php/flight/service/';
//$config['hotel_url'] = 'http://192.168.0.46/travelomatix_services/webservices/index.php/hotel/service/';
//$config['bus_url'] = 'http://192.168.0.46/travelomatix_services/webservices/index.php/bus/service/';


//Testing URL
$config['flight_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/flight/service/';
$config['hotel_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/hotel_v3/service/';
//$config['hotel_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/hotel_v3/service/';

$config['bus_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/bus/service/';

$config['sightseeing_url'] =  'http://prod.services.travelomatix.com/webservices/index.php/sightseeing/service/';

$config['transferv1_url'] =  'http://prod.services.travelomatix.com/webservices/index.php/transferv1/service/';


$config['car_url'] = 'http://test.services.travelomatix.com/webservices/index.php/car/service/';
if ($config['external_service_system'] == 'live') {
	$config['external_service'] = 'http://prod.services.travelomatix.com/webservices/index.php/rest/';
} else {
	$config['external_service'] = 'http://test.services.travelomatix.com/webservices/index.php/rest/';
}

$config['services_url'] = '';

$config['live_username'] = 'TMX241790';
$config['live_password'] = 'TMX@550241';
/******** BOOKING ENGINE END ********/
