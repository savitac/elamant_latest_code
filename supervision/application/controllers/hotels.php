<?php session_start();
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
// error_reporting(E_ALL);

class Hotels extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->library ( 'Api_Interface' );
		$this->load->model ( 'hotels_model' );
		$this->load->model ( 'Validation_Model' );

	}
	/**
	 */
	function deal_sheets() {
		$bus_deals_result = json_decode ( $this->api_interface->rest_service ( 'bus_deal_sheet' ), true );
		$flight_deals_result = json_decode ( $this->api_interface->rest_service ( 'airline_deal_sheet' ), true );
		
		$page_data ['bus_deals'] = $bus_deals_result ['data'];
		$page_data ['flight_deals'] = $flight_deals_result ['data'];
		$this->template->view ( 'utilities/deal_sheets', $page_data );
	}
	
	/**
	 * Update Convenience Fees in application
	 */
	function convenience_fees() {
		$page_data ['post_data'] = $this->input->post ();
		$this->load->model ( 'transaction_model' );
		if (valid_array ( $page_data ['post_data'] ) == true) {
			$this->transaction_model->update_convenience_fees ( $page_data ['post_data'] );
			set_update_message ();
			redirect ( base_url () . 'index.php/utilities/convenience_fees' );
		}
		$convenience_fees = $this->transaction_model->get_convenience_fees ();
		$page_data ['convenience_fees'] = $this->format_convenience_fees ( $convenience_fees );
		$this->template->view ( 'utilities/convenience_fees', $page_data );
	}
	
	/**
	 * Format Convenience Fees As Per View
	 */
	private function format_convenience_fees($convenience_fees) {
		$data = array ();
		foreach ( $convenience_fees as $k => $v ) {
			$data [$k] ['origin'] = $v ['origin'];
			$data [$k] ['module'] = strtoupper ( $v ['module'] );
			$fees = '';
			if ($v ['value_type'] == 'plus') {
				$fees = '+' . floatval ( $v ['value'] );
			} else {
				$fees = floatval ( $v ['value'] ) . '%';
			}
			$data [$k] ['fees'] = $fees;
			$data [$k] ['value'] = $v ['value'];
			$data [$k] ['value_type'] = $v ['value_type'];
			$data [$k] ['per_pax'] = $v ['per_pax'];
		}
		return $data;
	}
	
	/**
	 * Manage booking source in the application
	 */
	function manage_source() {
		if(check_user_previlege('p56') === FALSE){
			redirect(base_url());
		}
		$page_data ['list_data'] = $this->module_model->get_course_list ();
		$this->template->view ( 'utilities/manage_source', $page_data );
	}
	/**
	 * Manage sms status in sms_checkpoint table
	 */
	function sms_checkpoint() {
		$sms_checkpoint_data = $this->module_model->get_sms_checkpoint ();
		$data ['sms_data'] = $sms_checkpoint_data;
		$this->template->view ( 'utilities/sms_checkpoint', $data );
	}
	/**
	 * Activate sms_checkpoint
	 */
	function activate_sms_checkpoint($condition) {
		$status = ACTIVE;
		$this->module_model->update_sms_checkpoint_status ( $status, $condition );
		redirect ( base_url () . 'index.php/utilities/sms_checkpoint' );
	}
	
	/**
	 * Deactiavte sms_checkpoint
	 */
	function deactivate_sms_checkpoint($condition) {
		$status = INACTIVE;
		$info = $this->module_model->update_sms_checkpoint_status ( $status, $condition );
		redirect ( base_url () . 'index.php/utilities/sms_checkpoint' );
	}
	/**
	 * Module Activation
	 */
	function module() {
		$domain_list = $this->module_model->get_module_list ();
		$data ['domain_list'] = $domain_list;
		$this->template->view ( 'utilities/module_list', $data );
	}
	/**
	 * Activate sms_checkpoint
	 */
	function activate_module($condition) {
		$status = ACTIVE;
		$this->module_model->update_module_status ( $status, $condition );
		redirect ( base_url () . 'index.php/utilities/module' );
	}
	
	/**
	 * Deactiavte sms_checkpoint
	 */
	function deactivate_module($condition) {
		$status = INACTIVE;
		$info = $this->module_model->update_module_status ( $status, $condition );
		redirect ( base_url () . 'index.php/utilities/module' );
	}
	/**
	 * Activate social_link
	 */
	function activate_social_link($condition) {
		$status = ACTIVE;
		$this->module_model->update_social_link_status ( $status, $condition );
		redirect ( base_url () . 'index.php/utilities/social_network' );
	}
	
	/**
	 * Deactiavte social_link
	 */
	function deactivate_social_link($condition) {
		$status = INACTIVE;
		$info = $this->module_model->update_social_link_status ( $status, $condition );
		redirect ( base_url () . 'index.php/utilities/social_network' );
	}
	/*
	 * SOcial Network Url Management
	 */
	function social_network() {
		$temp = $this->custom_db->single_table_records ( 'social_links' );
		$data ['social_links'] = $temp ['data'];
		$this->template->view ( 'utilities/social_network', $data );
	}
	/**
	 * Update_Social URL
	 */
	function edit_social_url($id) {
		$post_data = $this->input->post ();
		$url = $post_data ['social_url'];
		$info = $this->module_model->update_social_url ( $url, $id );
		redirect ( base_url () . 'index.php/utilities/social_network' );
	}
	
	/**
	 * Activate social_login
	 */
	function activate_social_login($condition) {
		$status = ACTIVE;
		$this->module_model->update_social_login_status ( $status, $condition );
		redirect ( base_url () . 'index.php/utilities/social_login' );
	}
	
	/**
	 * Deactiavte social_login
	 */
	function deactivate_social_login($condition) {
		$status = INACTIVE;
		$info = $this->module_model->update_social_login_status ( $status, $condition );
		redirect ( base_url () . 'index.php/utilities/social_login' );
	}
	/*
	 * SOcial Network Url Management
	 */
	function social_login() {
		$temp = $this->custom_db->single_table_records ( 'social_login' );
		$data ['social_login'] = $temp ['data'];
		$this->template->view ( 'utilities/social_login', $data );
	}
	/**
	 * Update social_login
	 */
	function edit_social_login($id) {
		$post_data = $this->input->post ();
		$url = $post_data ['social_login'];
		$info = $this->module_model->update_social_login_name ( $url, $id );
		redirect ( base_url () . 'index.php/utilities/social_login' );
	}
	function toggle_asm_status($bs_id, $mc_id, $status = false) {
		$list_data = $this->module_model->get_course_list ( array (
				array (
						'BS.origin',
						'=',
						$bs_id 
				),
				array (
						'MCL.origin',
						'=',
						$mc_id 
				) 
		) );
		if (valid_array ( $list_data ) == true) {
			$api_code = $list_data [0] ['booking_source_id'];
			$api_name = $list_data [0] ['booking_source'];
			$module_name = $list_data [0] ['name'];
			if ($status == 'false') {
				$status = 'inactive';
				$logger_msg = $this->entity_name . ' Deactivated ' . $module_name . ' (' . $api_code . '-' . $api_name . ') API';
			} else {
				$status = 'active';
				$logger_msg = $this->entity_name . ' Activated ' . $module_name . ' (' . $api_code . '-' . $api_name . ') API';
			}
			$this->custom_db->update_record ( 'activity_source_map', array (
					'status' => $status 
			), array (
					'booking_source_fk' => $bs_id,
					'meta_course_list_fk' => $mc_id,
					'domain_origin' => get_domain_auth_id () 
			) );
			$this->application_logger->api_status ( $logger_msg );
		}
	}
	
	/**
	 * Currency Converter Settings!!!
	 *
	 * @param float $value        	
	 * @param int $id        	
	 */
	function currency_converter($value = 0, $id = 0) {
		if (intval ( $id ) > 0 && intval ( $value ) > - 1) {
			$data ['value'] = $value;
			$this->custom_db->update_record ( 'currency_converter', $data, array (
					'id' => $id 
			) );
		} else {
			$currency_data = $this->custom_db->single_table_records ( 'currency_converter' );
			$data ['converter'] = $currency_data ['data'];
			$this->template->view ( 'utilities/currency_converter', $data );
		}
	}
	
	/**
	 * Currency Converter Status Update!!!
	 *
	 * @param float $value        	
	 * @param int $id        	
	 */
	function currency_status_toggle($id = 0, $status = ACTIVE) {
		if (intval ( $id ) > 0) {
			$data ['status'] = $status;
			$this->custom_db->update_record ( 'currency_converter', $data, array (
					'id' => $id 
			) );
		}
	}
	
	/**
	 * Update Currency Converter Values Automatically Using Live Rates
	 * Keeping COURSE_LIST_DEFAULT_CURRENCY_VALUE AS Base Currency
	 */
	function auto_currency_converter() {
		$data_set = $this->custom_db->single_table_records ( 'currency_converter' );
		if ($data_set ['status'] == true) {
			$from = COURSE_LIST_DEFAULT_CURRENCY_VALUE;
			$data ['date_time'] = date ( 'Y-m-d H:i:s' );
			foreach ( $data_set ['data'] as $k => $v ) {
				$url = 'http://download.finance.yahoo.com/d/quotes.csv?s=' . $v ['country'] . $from . '=X&f=nl1';
				$handle = fopen ( $url, 'r' );
				if ($handle) {
					$currency_data = fgetcsv ( $handle );
					fclose ( $handle );
				}
				if ($currency_data != '') {
					if (isset ( $currency_data [0] ) == true and empty ( $currency_data [0] ) == false and isset ( $currency_data [1] ) == true and empty ( $currency_data [1] ) == false) {
						$data ['value'] = $currency_data [1];
						$this->custom_db->update_record ( 'currency_converter', $data, array (
								'id' => $v ['id'] 
						) );
					}
				}
			}
		}
		redirect ( 'utilities/currency_converter' );
	}
	
	/**
	 * Load All Events Of Trip Calendar
	 */
	function trip_calendar() {
		$this->template->view ( 'utilities/trip_calendar' );
	}
	function app_settings() {
		$this->template->view ( 'utilities/app_settings' );
	}
	
	/**
	 * Show time line to user previous one month - Load Last one month by default
	 */
	function timeline() {
		$this->template->view ( 'utilities/timeline' );
	}
	
	/**
	 * Get All The Events Between Two Dates
	 */
	function timeline_rack() {
		$response ['status'] = FAILURE_STATUS;
		$response ['data'] = array ();
		$response ['msg'] = '';
		$params = $this->input->get ();
		$oe_start = intval ( $params ['oe_start'] );
		$event_limit = intval ( $params ['oe_limit'] );
		if ($oe_start > - 1 and $event_limit > - 1) {
			// Older Events
			$oe_list = $this->application_logger->get_events ( $oe_start, $event_limit );
			if (valid_array ( $oe_list ) == true) {
				$response ['oe_list'] = get_compressed_output ( $this->template->isolated_view ( 'utilities/core_timeline', array (
						'list' => $oe_list 
				) ) );
				$response ['status'] = SUCCESS_STATUS;
			}
		}
		header ( 'Content-type:application/json' );
		echo json_encode ( $response );
		exit ();
	}
	
	/**
	 * Get All The Events Between Two Dates
	 */
	function latest_timeline_events() {
		session_write_close (); // This is needed as it helps remove session locks
		$response ['status'] = FAILURE_STATUS;
		$response ['data'] = array ();
		$response ['msg'] = '';
		$waiting_for_new_event = true;
		$params = $this->input->get ();
		$last_event_id = intval ( $params ['last_event_id'] );
		if ($last_event_id > - 1) {
			$cond = array (
					array (
							'TL.origin',
							'>',
							$last_event_id 
					) 
			);
			// Older Events
			while ( $response ['status'] == false ) {
				$os_list = $this->application_logger->get_events ( 0, 10000000000, $cond );
				if (valid_array ( $os_list ) == true) {
					$response ['oa_list'] = get_compressed_output ( $this->template->isolated_view ( 'utilities/core_timeline', array (
							'list' => $os_list 
					) ) );
					$response ['status'] = SUCCESS_STATUS;
				} else {
					sleep ( 3 );
				}
			}
		}
		header ( 'Content-type:application/json' );
		echo json_encode ( $response );
		exit ();
	}
	/**
	 * Jaganath
	 * Manage Promo Codes
	 */
	function manage_promo_code($offset = 0) {
		$post_data = $this->input->post ();
		$get_data = $this->input->get ();
		$page_data = array ();
		$page_data ['from_data'] ['origin'] = 0;
		$condition = array ();
		$page_data ['promo_code_page_obj'] = new Provab_Page_Loader ( 'manage_promo_code' );
		if (isset ( $get_data ['eid'] ) == true && intval ( $get_data ['eid'] ) > 0 && valid_array ( $post_data ) == false) {
			$edit_data = $this->custom_db->single_table_records ( 'promo_code_list', '*', array (
					'origin' => intval ( $get_data ['eid'] ) 
			) );
			if ($edit_data ['status'] == true) {
				if (strtotime ( $edit_data ['data'] [0] ['expiry_date'] ) <= 0) {
					$edit_data ['data'] [0] ['expiry_date'] = ''; // If its Unlimited, setting the Expiry Date to empty
				}
				$page_data ['from_data'] = $edit_data ['data'] [0];
			} else {
				redirect ( 'security/log_event?event=InvalidID' );
			}
		} else if (valid_array ( $post_data ) == true) { // ADD
			$page_data ['promo_code_page_obj']->set_auto_validator ();
			if ($this->form_validation->run ()) {
				$origin = intval ( $post_data ['origin'] );
				unset ( $post_data ['FID'] );
				unset ( $post_data ['origin'] );
				$promo_code_list = array ();
				$promo_code_list ['module'] = trim ( $post_data ['module'] );
				$promo_code_list ['promo_code'] = trim ( $post_data ['promo_code'] );
				$promo_code_list ['description'] = trim ( $post_data ['description'] );
				$promo_code_list ['value_type'] = trim ( $post_data ['value_type'] );
				$promo_code_list ['value'] = trim ( $post_data ['value'] );
				$expiry_date = trim ( $post_data ['expiry_date'] );
				if (empty ( $expiry_date ) == false && valid_date_value ( $expiry_date )) {
					$promo_code_list ['expiry_date'] = date ( 'Y-m-d', strtotime ( $expiry_date ) );
				} else {
					$promo_code_list ['expiry_date'] = date ( '0000-00-00' );
				}
				$promo_code_list ['status'] = trim ( $post_data ['status'] );
				set_update_message ();
				if ($origin > 0) { // Update
					$this->custom_db->update_record ( 'promo_code_list', $promo_code_list, array (
							'origin' => $origin 
					) );
				} else if ($origin == 0) { // Add
					$promo_code_list ['created_by_id'] = $this->entity_user_id;
					$promo_code_list ['created_datetime'] = db_current_datetime ();
					$this->custom_db->insert_record ( 'promo_code_list', $promo_code_list );
					set_insert_message ();
				}
				redirect ( 'utilities/manage_promo_code' );
			}
		}
		// ***********FILTERS***********//
		if (isset ( $get_data ['promo_code'] ) == true) {
			$filter_promo_code = trim ( $get_data ['promo_code'] );
			if (empty ( $filter_promo_code ) == false) {
				$condition [] = array (
						'promo_code',
						'=',
						'"' . $filter_promo_code . '"' 
				);
			}
		}
		if (isset ( $get_data ['module'] ) == true) {
			$filter_module = trim ( $get_data ['module'] );
			if (empty ( $filter_module ) == false) {
				$condition [] = array (
						'module',
						'=',
						'"' . $filter_module . '"' 
				);
			}
		}
		// ***********FILTERS***********//
		$total_records = $this->module_model->promo_code_list ( $condition, true );
		$promo_code_list = $this->module_model->promo_code_list ( $condition, false, $offset, RECORDS_RANGE_2 );
		/**
		 * TABLE PAGINATION
		 */
		$this->load->library ( 'pagination' );
		if (count ( $_GET ) > 0)
			$config ['suffix'] = '?' . http_build_query ( $_GET, '', "&" );
		$config ['base_url'] = base_url () . 'index.php/utilities/manage_promo_code/';
		$config ['first_url'] = $config ['base_url'] . '?' . http_build_query ( $_GET );
		$config ['total_rows'] = $total_records;
		$config ['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize ( $config );
		/**
		 * TABLE PAGINATION
		 */
		$page_data ['promocode_module_options'] = $this->module_model->promocode_module_options ();
		$page_data ['promo_code_list'] = $promo_code_list;
		$this->template->view ( 'utilities/manage_promo_code', $page_data );
	}

	//Hotel-CRS

	function hotel_ammenities(){
		
		$ammenities['ammenities_list'] 		= $this->hotels_model->get_ammenities_list();
		//debug($ammenities);die;
		$this->template->view('hotel/hotel_ammenities/ammenities_list',$ammenities);
	}

	

	function add_hotel_ammenties(){
		if(count($_POST) > 0){
			//Adding icon image code hided

			// $iconImage = $_FILES['hotel_ammenity_image']['name'];
			// $ext = pathinfo($iconImage, PATHINFO_EXTENSION);
			// $size = $_FILES['hotel_ammenity_image']['size'];
			// echo "<pre>";
			// print_r(DOMAIN_IMAGE_DIR);
			// exit("12345");

			// if($ext == "svg" && $size < 1500){

			$this->form_validation->set_rules('hotel_ammenity_name', 'hotel_ammenity_name', 'required|is_unique[hotel_amenities.amenities_name]');
			  if ($this->form_validation->run() == TRUE)
                {
                  
			$this->hotels_model->add_hotel_ammenity_details($_POST);
			redirect('hotels/hotel_ammenities','refresh');
		}
		else
		{
			$this->session->set_flashdata(array('message' => 'Duplicate hotel Ammenties', 'type' => WARNING_MESSAGE,'override_app_msg'=>1));
                	refresh();
			$this->template->view('hotel/hotel_ammenities/add_hotel_ammenity');
		}
			// }else{

			// }
		}else{
		//echo "out";die;
			$this->template->view('hotel/hotel_ammenities/add_hotel_ammenity');
		}
	}

	function inactive_hotel_ammenity($ammenity_id1){
		$ammenity_id 	= json_decode(base64_decode($ammenity_id1));
		if($ammenity_id != ''){
			$this->hotels_model->inactive_hotel_ammenity($ammenity_id);
		}
		redirect('hotels/hotel_ammenities','refresh');
	}
	
	function active_hotel_ammenity($ammenity_id1){
		$ammenity_id 	= json_decode(base64_decode($ammenity_id1));
		if($ammenity_id != ''){
			$this->hotels_model->active_hotel_ammenity($ammenity_id);
		}
		redirect('hotels/hotel_ammenities','refresh');
	}

	function edit_hotel_ammenity($ammenity_id1){
		$ammenity_id 	= json_decode(base64_decode($ammenity_id1));
		if($ammenity_id != ''){
			
			$hotel_ammenities['ammenities_list'] 		= $this->hotels_model->get_ammenities_list($ammenity_id);
			$this->template->view('hotel/hotel_ammenities/edit_hotel_ammenity',$hotel_ammenities);
		}else{
			redirect('hotels/hotel_ammenities','refresh');
		}
	}
	
	function update_hotel_ammenity($ammenity_id1){
		$ammenity_id 	= json_decode(base64_decode($ammenity_id1));
		if($ammenity_id != ''){
			if(count($_POST) > 0){
				$this->hotels_model->update_hotel_ammenity($_POST,$ammenity_id);
				redirect('hotels/hotel_ammenities','refresh');
			}else if($hotel_type_id!=''){
				redirect('hotels/edit_hotel_ammenity/'.$ammenity_id,'refresh');
			}else{
				redirect('hotels/hotel_ammenities','refresh');
			}
		}else{
			redirect('hotels/hotel_ammenities','refresh');
		}
	}
	
	function delete_hotel_ammenity($ammenity_id1){
		$ammenity_id 	= json_decode(base64_decode($ammenity_id1));
		if($ammenity_id != ''){
			$this->hotels_model->delete_hotel_ammenity($ammenity_id);
		}
		redirect('hotels/hotel_ammenities','refresh');
	}

	//room ammenities
	function room_ammenities(){
		
		$ammenities['ammenities_list'] 		= $this->hotels_model->get_room_ammenities_list();
		$this->template->view('hotel/room_ammenities/ammenities_list',$ammenities);
	}

	function add_room_ammenties(){
		if(count($_POST) > 0){
			$this->form_validation->set_rules('hotel_ammenity_name', 'hotel_ammenity_name', 'required|is_unique[room_amenities.amenities_name]');
			  if ($this->form_validation->run() == TRUE)
                {
                  
			$this->hotels_model->add_room_ammenity_details($_POST);
			redirect('hotels/room_ammenities','refresh');
		}
		else
		{
$this->session->set_flashdata(array('message' => 'Duplicate Room Ammenties', 'type' => WARNING_MESSAGE,'override_app_msg'=>1));
                	refresh();
                	$this->template->view('hotel/room_ammenities/add_hotel_ammenity');

		}
		}else{
		
			$this->template->view('hotel/room_ammenities/add_hotel_ammenity');
		}
	}

	function inactive_room_ammenity($ammenity_id1){
		$ammenity_id 	= json_decode(base64_decode($ammenity_id1));
		if($ammenity_id != ''){
			$this->hotels_model->inactive_room_ammenity($ammenity_id);
		}
		redirect('hotels/room_ammenities','refresh');
	}
	
	function active_room_ammenity($ammenity_id1){
		$ammenity_id 	= json_decode(base64_decode($ammenity_id1));
		if($ammenity_id != ''){
			$this->hotels_model->active_room_ammenity($ammenity_id);
		}
		redirect('hotels/room_ammenities','refresh');
	}

	function edit_room_ammenity($ammenity_id1){
		$ammenity_id 	= json_decode(base64_decode($ammenity_id1));
		if($ammenity_id != ''){
			
			$hotel_ammenities['ammenities_list'] 		= $this->hotels_model->get_room_ammenities_list($ammenity_id);
			$this->template->view('hotel/room_ammenities/edit_hotel_ammenity',$hotel_ammenities);
		}else{
			redirect('hotels/room_ammenities','refresh');
		}
	}
	
	function update_room_ammenity($ammenity_id1){
		$ammenity_id 	= json_decode(base64_decode($ammenity_id1));
		if($ammenity_id != ''){
			if(count($_POST) > 0){
				$this->hotels_model->update_room_ammenity($_POST,$ammenity_id);
				redirect('hotels/room_ammenities','refresh');
			}else if($hotel_type_id!=''){
				redirect('hotels/edit_room_ammenity/'.$ammenity_id,'refresh');
			}else{
				redirect('hotels/room_ammenities','refresh');
			}
		}else{
			redirect('hotels/room_ammenities','refresh');
		}
	}
	
	function delete_room_ammenity($ammenity_id1){
		$ammenity_id 	= json_decode(base64_decode($ammenity_id1));
		if($ammenity_id != ''){
			$this->hotels_model->delete_room_ammenity($ammenity_id);
		}
		redirect('hotels/room_ammenities','refresh');
	}

	//end room ammenities

	
	function get_child_group($hotel_id ,$child_price1 = "") {		
		if($child_price1 != ""){			
			$child_price = explode("_",$child_price1);			
		}

		$child_group = $this->hotels_model->get_child_group($hotel_id); 
	    $childs_div ='';
	    
	    if($child_group[0]->child_group_a == "" && $child_group->child_group_b == "" && $child_group->child_group_c =="" && $child_group[0]->child_group_d == ""){
			$child_group     = $this->hotels_model->get_hotel_settings_list(); 
	    }
	  
	    if($child_group[0]->child_group_a != ""){	    	
	    	if(isset($child_price[0])){  
	    		$zero_child = $child_price[0]; 	    		
	    	}
	    	else{
	    		$zero_child="";
	    	}
		$childs_div .='<div class="form-group">	
											<label for="field-1" class="col-sm-3 control-label">Infant Price/ Person ('.$child_group[0]->child_group_a.')</label>		<div class="col-sm-5">							
											<input type="text" class="form-control" value="'.$zero_child .'" maxlength="7" id="child_price_a" name="child_price_a" data-message-required="Please Enter the Child Price" />
										</div></div>';
		}
		
		if($child_group[0]->child_group_b != ""){
			if(isset($child_price[1])){  
	    		$zero_child = $child_price[1]; 
	    	}
	    	else{
	    		$zero_child="";
	    	}
		$childs_div .='<div class="form-group">	
											<label for="field-1" class="col-sm-3 control-label">Child Price/ Person ('.$child_group[0]->child_group_b.')</label>					<div class="col-sm-5">				
											<input type="text" class="form-control" maxlength="7" value="'.$zero_child .'" id="child_price_b" name="child_price_b" data-message-required="Please Enter the Child Price" />
										</div></div>';
		}
		if($child_group[0]->child_group_c != ""){
			if(isset($child_price[2])){  
	    		$zero_child = $child_price[2]; 
	    	}
	    	else{
	    		$zero_child="";
	    	}
		$childs_div .='<div class="form-group">
											<label for="field-1" class="col-sm-3 control-label">Child Price/ Person ('.$child_group[0]->child_group_c.')</label>		
											<div class="col-sm-5">			
											<input type="text" class="form-control" maxlength="7" value="'.$zero_child .'" id="child_price_c" name="child_price_c" data-message-required="Please Enter the Child Price" />
										</div></div>';
		}
		if($child_group[0]->child_group_d != ""){
			if(isset($child_price[3])){  
	    		$zero_child = $child_price[3]; 
	    	}
	    	else{
	    		$zero_child="";
	    	}
		$childs_div .='<div class="form-group"> 
											<label for="field-1" class="col-sm-3 control-label">Child Price/ Person ('.$child_group[0]->child_group_d.')</label>					<div class="col-sm-5">				
											<input type="text" class="form-control" maxlength="7" value="'.$zero_child .'" id="child_price_d" name="child_price_d" data-message-required="Please Enter the Child Price" />
										</div></div>';
		}
		
		if($child_group[0]->child_group_e != ""){
			if(isset($child_price[4])){  
	    		$zero_child = $child_price[4]; 
	    	}
	    	else{
	    		$zero_child="";
	    	}
		$childs_div .='<div class="form-group"> 
											<label for="field-1" class="col-sm-3 control-label">Child Price/ Person ('.$child_group[0]->child_group_e.')</label>					<div class="col-sm-5">				
											<input type="text" class="form-control" maxlength="7" value="'.$zero_child .'" id="child_price_e" name="child_price_e" data-message-required="Please Enter the Child Price" />
										</div></div>';
		}
		//debug($childs_div);exit;
		
		echo $childs_div; exit;
	}

	//Hotel Type
	function hotel_types(){
		$hotels 						= $this->hotels_model->getHomePageSettings();
		//echo '<pre>'; print_r($hotels); exit();
		$hotels['hotel_types_list'] 	= $this->hotels_model->get_hotel_types_list("", $hotels);
		$this->template->view('hotel/hotel_types/hotel_types_list',$hotels);
	}

	
	function add_hotel_type(){
		if(count($_POST) > 0){
			$this->form_validation->set_rules('hotel_type_name', 'hotel_type_name', 'required|is_unique[hotel_type.hotel_type_name]');
			  if ($this->form_validation->run() == TRUE)
                {
                       $this->hotels_model->add_hotel_type_details($_POST);
			redirect('hotels/hotel_types','refresh');
                }
                else{
                	$this->session->set_flashdata(array('message' => 'Duplicate Hotel Type', 'type' => WARNING_MESSAGE,'override_app_msg'=>1));
                	refresh();
                	
			$this->template->view('hotel/hotel_types/add_hotel_type');
		}
			
		}else{
			
			$this->template->view('hotel/hotel_types/add_hotel_type');
		}
	}
	
	function inactive_hotel_type($hotel_type_id1){
		$hotel_type_id 	= json_decode(base64_decode($hotel_type_id1));
		if($hotel_type_id != ''){
			$this->hotels_model->inactive_hotel_type($hotel_type_id);
		}
		redirect('hotels/hotel_types','refresh');
	}
	
	function active_hotel_type($hotel_type_id1){
		$hotel_type_id 	= json_decode(base64_decode($hotel_type_id1));
		if($hotel_type_id != ''){
			$this->hotels_model->active_hotel_type($hotel_type_id);
		}
		redirect('hotels/hotel_types','refresh');
	}
	
	function edit_hotel_type($hotel_type_id1){
		$hotel_type_id 	= json_decode(base64_decode($hotel_type_id1));
		if($hotel_type_id != ''){
			
			$hotel_types['hotel_types_list'] 	= $this->hotels_model->get_hotel_types_list($hotel_type_id);
			$this->template->view('hotel/hotel_types/edit_hotel_type',$hotel_types);
		}else{
			redirect('hotels/hotel_types','refresh');
		}
	}
	
	function update_hotel_type($hotel_type_id1){
		$hotel_type_id 	= json_decode(base64_decode($hotel_type_id1));
		if($hotel_type_id != ''){
			if(count($_POST) > 0){
				$this->hotels_model->update_hotel_type($_POST,$hotel_type_id);
				redirect('hotels/hotel_types','refresh');
			}else if($hotel_type_id!=''){
				redirect('hotels/edit_hotel_type/'.$hotel_type_id,'refresh');
			}else{
				redirect('hotels/hotel_types','refresh');
			}
		}else{
			redirect('hotels/hotel_types','refresh');
		}
	}
	
	function delete_hotel_type($hotel_type_id1){
		$hotel_type_id 	= json_decode(base64_decode($hotel_type_id1));
		if($hotel_type_id != ''){
			$this->hotels_model->delete_hotel_type($hotel_type_id);
		}
		redirect('hotels/hotel_types','refresh');
	}

	function hotel_crs_list(){ //error_reporting(E_ALL);
		$hotels 						= $this->hotels_model->getHomePageSettings();
		$hotels['hotels_list'] 	   		= $this->hotels_model->get_all_hotel_crs_list("",$hotels);
		//echo"<pre>sanjy";print_r($hotels['hotels_list']);exit;
	    $this->template->view('hotel/hotel_crs/hotel_crs_list',$hotels);
	}

	function inactive_hotel($hotel_id1){
		$hotel_id 	= json_decode(base64_decode($hotel_id1));
		if($hotel_id != ''){
			$this->hotels_model->inactive_hotel($hotel_id);
		}
		redirect('hotels/hotel_crs_list','refresh');
	}
	
	function active_hotel($hotel_id1){
		$hotel_id 	= json_decode(base64_decode($hotel_id1));
		if($hotel_id != ''){
			$this->hotels_model->active_hotel($hotel_id);
		}
		redirect('hotels/hotel_crs_list','refresh');
	}

	function edit_hotel($hotel_id1){ 
		//error_reporting(E_ALL);
		$hotel_id 	= json_decode(base64_decode($hotel_id1));
		if($hotel_id != ''){
			$hotels 						= $this->hotels_model->getHomePageSettings();
			$hotels['hotel_id'] = $hotel_id; 
			$hotels['hotels_list'] 			= $this->hotels_model->get_hotel_crs_list($hotel_id, $hotels);
			$hotels['hotel_types_list'] 	= $this->hotels_model->get_hotel_types_list();
			
			$hotels['settings'] 			= $this->hotels_model->get_hotel_settings_list();
			//$hotels['country'] 				= $this->General_Model->get_country_details("");		
			
			$hotels['cancellation_details'] = $this->hotels_model->getCancellationDetailsDescription($hotel_id);
			$hotels['country'] = $this->hotels_model->get_country_details_hotel();
			// echo "<pre/>";print_r($hotel_id);exit;
			$wizard_status=$this->hotels_model->get_wizard($hotel_id);
			$wizard_status = $wizard_status[0]['wizard_status'] ;
			$wiz=explode(',', $wizard_status);
				foreach ($wiz as $wkey => $wvalue) {
					$w[]=$wvalue;
				}
			$hotels['wizard_status']=$w;
			$hotel_details=json_decode(json_encode($hotels['hotels_list'][0]),true);
			$hotels['hotel_name']=$hotel_details['hotel_name'];
			// debug($hotels);exit;
			$this->template->view('hotel/hotel_crs/edit_hotel',$hotels);
			//debug($hotels['hotels_list']);exit;
		}else{
			redirect('hotels/hotel_crs_list','refresh');
		}
	}
	
	function update_hotel($hotel_id1){

		$hotel_id 	= json_decode(base64_decode($hotel_id1)); 
		if($hotel_id != ''){
			if(count($_POST) > 0){
				$thumb_image  = $_REQUEST['thumb_image_old']; 
				$thumb_image = $this->hotels_model->upload_image_lgm($_FILES, 'hotel_images', $_REQUEST['thumb_image_old']);
				//$thumb_image = $this->General_Model->upload_image_lgm($_FILES, 'hotel_images','thumb_image',$_REQUEST['thumb_image_old']);
				//echo '<pre>'; print_r($thumb_image); exit();
				$hotel_image = ''; 
				$hotel_image1 = explode(',',$_REQUEST['hotel_image_old']);
				foreach($hotel_image1 as $hotelimage){
					$hotel_image .= $hotelimage.',';
				}
				 //echo '<pre/>';print_r($hotel_image);exit;
				for($i=0; $i< count($_FILES['hotel_image']['name']); $i++){
					$hotel_image .= $this->hotels_model->upload_images_all($_FILES, 'hotel_images','hotel_image',$hotel_image1[$i],$i);
					$hotel_image .= ",";
				}
				$hotel_image = rtrim($hotel_image,",");
				$this->hotels_model->update_hotel($_POST,$hotel_id,$thumb_image,$hotel_image);
				$this->hotels_model->update_hotel_contact_details($_POST,$hotel_id);
				redirect('hotels/hotel_crs_list','refresh');
			}else if($hotel_id!=''){
				redirect('hotels/edit_hotel'.$hotel_id,'refresh');
			}else{
				redirect('hotels/hotel_crs_list','refresh');
			}
		}else{
			redirect('hotels/hotel_crs_list','refresh');
		}
	}

	function delete_hotel($hotel_id1){
		$hotel_id 	= json_decode(base64_decode($hotel_id1));
		if($hotel_id != ''){
			$this->hotels_model->delete_hotel($hotel_id);
		}
		redirect('hotels/hotel_crs_list','refresh');
	}

	function manage_hotelchildgroup($hotel_id1){
		$hotel_id = json_decode(base64_decode($hotel_id1));
		if($hotel_id != ''){
			if(count($_POST) > 0) {
				if(isset($_POST['child_group_a']) && $_POST['child_group_a'] == ''){
		          $data['error']['child_group_a-error'] = "Enter Valid Age Group";
		        }
		        if(isset($_POST['child_group_b']) && $_POST['child_group_b'] == ''){
		          $data['error']['child_group_b-error'] = "Enter Valid Age Group";
		        }
		        // if(isset($_POST['child_group_c']) && $_POST['child_group_c'] == ''){
		        //   $data['error']['child_group_c-error'] = "Enter Valid Age Group";
		        // }
		        // if(isset($_POST['child_group_d']) && $_POST['child_group_d'] == ''){
		        //   $data['error']['child_group_d-error'] = "Enter Valid Age Group";
		        // }
		        // if(isset($_POST['child_group_e']) && $_POST['child_group_e'] == ''){
		        //   $data['error']['child_group_e-error'] = "Enter Valid Age Group";
		        // }
				if(isset($_POST['child_group_a']) && $_POST['child_group_a'] != ''){
			   $agegroup_a = $this->Validation_Model->numberWithHyphen($_POST['child_group_a']);
			   		if(!$agegroup_a){
						   $data['error']['child_group_a-error'] = "Enter Valid Age Group";
					 }
			}

			if(isset($_POST['child_group_b']) && $_POST['child_group_b'] != ''){
			   $agegroup_a = $this->Validation_Model->numberWithHyphen($_POST['child_group_b']);	
			    if(!$agegroup_a){
				   $data['error']['child_group_b-error'] = "Enter Valid Age Group";
			 }
			}

			/*if(isset($_POST['child_group_c']) && $_POST['child_group_c'] != ''){
			   $agegroup_a = $this->Validation_Model->numberWithHyphen($_POST['child_group_c']);	
			    if(!$agegroup_a){
				   $data['error']['child_group_c-error'] = "Enter Valid Age Group";
			 }
			}

			if(isset($_POST['child_group_d']) && $_POST['child_group_d'] != ''){
			   $agegroup_a = $this->Validation_Model->numberWithHyphen($_POST['child_group_d']);	
			    if(!$agegroup_a){
				   $data['error']['child_group_d-error'] = "Enter Valid Age Group";
			 }
			}

			if(isset($_POST['child_group_e']) && $_POST['child_group_e'] != ''){
			   $agegroup_a = $this->Validation_Model->numberWithHyphen($_POST['child_group_e']);	
			    if(!$agegroup_a){
				   $data['error']['child_group_e-error'] = "Enter Valid Age Group";
			 }
			}*/

			
			if(isset($data['error']) && count($data['error']) > 0) {
				 $data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			}
				
				$this->hotels_model->manage_child_group($_POST, $hotel_id);
				$this->hotels_model->update_wizard_status($hotel_id,'step2');
				$data['status'] = 1;
				
				$data['success_url'] = site_url()."/hotels/hotel_room_types/".base64_encode(json_encode($hotel_id));
				
				print  json_encode($data);
				return;
				//redirect('hotels/hotel_crs_list', 'referesh');
			}
			$hotels 					= $this->hotels_model->getHomePageSettings();
			
			$hotels['hotels_list'] 		= $this->hotels_model->get_hotel_crs_list($hotel_id, $hotels);
			$wizard_status=$this->hotels_model->get_wizard($hotel_id);
			$wizard_status = $wizard_status[0]['wizard_status'] ;
			$wiz=explode(',', $wizard_status);
				foreach ($wiz as $wkey => $wvalue) {
					$w[]=$wvalue;
				}
			$hotels['wizard_status']=$w;
			$hotel_details=json_decode(json_encode($hotels['hotels_list'][0]),true);
			$hotels['hotel_name']=$hotel_details['hotel_name'];
			// debug($hotels);exit;
		    $this->template->view('hotel/hotel_crs/manage_childgroup', $hotels);
			
		}else {
			redirect('hotels/hotel_crs_list', 'referesh');
		}
	}

	//Adding Hotel

	//Hotel Room Types
	function hotel_room_types($hotel_id1 = ""){
		$hotel_id = "";
		if($hotel_id1 != "")
	 	  $hotel_id 	= json_decode(base64_decode($hotel_id1));		

		$room_types 						= $this->hotels_model->getHomePageSettings();
		$room_types['room_types_list'] 		= $this->hotels_model->get_hotel_room_types_list($hotel_id, $room_types);
		$room_types['hotel_id'] = $hotel_id1;
		// debug($hotel_id);exit;
		$wizard_status=$this->hotels_model->get_wizard($hotel_id);
		$wizard_status = $wizard_status[0]['wizard_status'] ;
		$wiz=explode(',', $wizard_status);
			foreach ($wiz as $wkey => $wvalue) {
				$w[]=$wvalue;
			}
		$room_types['wizard_status']=$w;
		$hotels 					= $this->hotels_model->getHomePageSettings();
			
		$hotels['hotels_list'] 		= $this->hotels_model->get_hotel_crs_list($hotel_id, $hotels);
		
		$hotel_details=json_decode(json_encode($hotels['hotels_list'][0]),true);
		$room_types['hotel_name']=$hotel_details['hotel_name'];
		// debug($room_types);exit;
		$this->template->view('hotel/room_types/room_types_list',$room_types);		
	}

	function add_room_type($hotel_id1 = ""){			
		$hotel_id = "";
		if($hotel_id1 != "")
	 	  $hotel_id 	= json_decode(base64_decode($hotel_id1));	
		if(count($_POST) > 0){			
			
				$room_image = "";

				for($i=0; $i<count($_FILES['room_image']['name']); $i++){
					if(!empty($_FILES['room_image']['name'][$i])){	
						if(is_uploaded_file($_FILES['room_image']['tmp_name'][$i])) {
							$sourcePath = $_FILES['room_image']['tmp_name'][$i];
							$img_Name=time().$_FILES['room_image']['name'][$i];												
							$targetPath = "uploads/room/".$img_Name;
							if(move_uploaded_file($sourcePath,$targetPath)){
								$room_image .= $img_Name.",";
							}
						}				
					}
				}

			    $room_image = rtrim($room_image,",");				    
				$this->hotels_model->add_room_type_details($_POST,$hotel_id,$room_image);
				$this->hotels_model->update_wizard_status($hotel_id,'step3');
				if($hotel_id1 == ""){	 	  		
					redirect('hotels/room_types','refresh');
				}
				else{	
					redirect('hotels/hotel_room_types/'.$hotel_id1,'refresh');
				}	
				
		}else{
			$room_types 					= $this->hotels_model->getHomePageSettings();
			$room_types['hotels_list']	    = $this->hotels_model->get_hotel_crs_list($hotel_id, $room_types);
			//$room_types['hotel_types_list'] 	= $this->hotels_model->get_hotel_types_list();
			$room_types['room_types_list'] 	= $this->hotels_model->get_active_room_types_list();
			//debug($room_types['hotels_list']);exit;			
			//echo "<pre>";print_r($room_types);exit;
			if($hotel_id1 != ""){	 	  		
	 	  		$room_types['hotel_id'] = $hotel_id1;
	 	  	}
	 	  	// debug($room_types);exit;
			$this->template->view('hotel/room_types/add_room_type',$room_types);
		}
	}
	
	function inactive_room_type($room_type_id1,$hotel_id1 = ""){
		$room_type_id 	= json_decode(base64_decode($room_type_id1));
		if($room_type_id != ''){
			$this->hotels_model->inactive_room_type($room_type_id);
		}

		if($hotel_id1 == ""){	 	  		
			redirect('hotels/room_types','refresh');
		}
		else{	
			redirect('hotels/hotel_room_types/'.$hotel_id1,'refresh');
		}	
	}
	
	function active_room_type($room_type_id1,$hotel_id1 = ""){
		$room_type_id 	= json_decode(base64_decode($room_type_id1));
		if($room_type_id != ''){
			$this->hotels_model->active_room_type($room_type_id);
		}
		if($hotel_id1 == ""){	 	  		
			redirect('hotels/room_types','refresh');
		}
		else{	
			redirect('hotels/hotel_room_types/'.$hotel_id1,'refresh');
		}	
	}
	
	function edit_room_type($room_type_id1, $hotel_id1 = ""){
		$room_type_id 	= json_decode(base64_decode($room_type_id1));	
		//echo $room_type_id;	 exit();
		if($room_type_id != ''){
			$room_types 						= $this->hotels_model->getHomePageSettings();
			$room_types['hotels_list'] 		    = $this->hotels_model->get_hotel_crs_list('', $room_types);
			//$room_types['room_types_list'] 		= $this->hotels_model->get_room_types_list($room_type_id);
			$room_types['room_types_list'] 		= $this->hotels_model->get_room_type_detail_count($room_type_id);
			$room_types['room_types_booked'] = $this->hotels_model->get_room_type_detail_booked_count($room_type_id);
			// echo '<pre>'; print_r($room_types['hotels_list']); exit();
			$room_types['additional_meals']		= $this->hotels_model->get_additional_meal_roomtype($room_type_id);				
			if($hotel_id1 != ""){	 	  		
	 	  		$room_types['hotel_id'] = str_ireplace('"', '', base64_decode($hotel_id1));
	 	  	}	 	  	
	 	  	$room_types['hotel_id_rec'] = $this->hotels_model->get_hotel_id_roomtype($room_type_id); 		 	  	
	 	  	$room_types['room_type_id_from'] = $room_type_id;
	 	  	//$room_types['hotel_types_list'] 	= $this->hotels_model->get_hotel_types_list();		
	 	  	$room_types['room_types'] 	= $this->hotels_model->get_active_room_types_list();		 	  	
	 	  	//echo $room_type_id."<pre>"; print_r($room_types['hotel_id_rec']);exit();
	 	  	// debug($room_types);die;
			$this->template->view('hotel/room_types/edit_room_type',$room_types);
		}else{
			
		}
	}
	
	function update_room_type($room_type_id1,$hotel_id1 = ""){		
		//echo '<pre>'; print_r($_POST['rooms_tot_units_booked']); exit();
		$room_type_id 	= json_decode(base64_decode($room_type_id1));		
		if($room_type_id != ''){
			if(count($_POST) > 0){				
			//print_r($_POST);exit();
				//debug(base64_decode($hotel_id1));exit;
			$this->form_validation->set_rules('room_type_name', 'Room Name', 'required|min_length[3]|max_length[50]');
			$this->form_validation->set_rules('rooms_tot_units', 'Total number of room', 'required|numeric|min_length[1]|max_length[3]');
			$this->form_validation->set_rules('adult', 'Adult', 'required|numeric|min_length[1]|max_length[2]|callback_check_adult_value');
			$this->form_validation->set_rules('child', 'Child', 'required|numeric|min_length[1]|max_length[2]');
			$this->form_validation->set_rules('max_pax', 'Capacity', 'required|min_length[1]|max_length[2]');

			if($this->input->post('extra_bed') == ""){
			 $this->form_validation->set_rules('extra_bed_count', 'Extra bed count', 'required|numeric|min_length[1]|max_length[2]|callback_check_extrabed');
		    }	
			$this->form_validation->set_rules('room_info', 'Room Description', 'required|min_length[10]');
			//$this->form_validation->set_rules('cancellation_policy', 'Policy', 'required|min_length[10]');

			/*if($this->input->post('chk_breakfast') != ""){				
			 $this->form_validation->set_rules('break_fast_p', 'Break Fast Price', 'required|numeric|min_length[1]|max_length[5]|callback_check_price');
		    }

		    if($this->input->post('chk_half') != ""){
			 $this->form_validation->set_rules('half_board_p', 'Half Board Price', 'required|numeric|min_length[1]|max_length[5]|callback_check_price');
		    }

		    if($this->input->post('chk_full') != ""){
			 $this->form_validation->set_rules('full_board_p', 'Full Board Price', 'required|numeric|min_length[1]|max_length[5]|callback_check_price');
		    }

		    if($this->input->post('chk_dinner') != ""){
			 $this->form_validation->set_rules('dinner_p', 'Dinner Price', 'required|numeric|min_length[1]|max_length[5]|callback_check_price');
		    }

		    if($this->input->post('chk_lunch') != ""){
			 $this->form_validation->set_rules('lunch_p', 'Lunch Price', 'required|numeric|min_length[1]|max_length[5]|callback_check_price');
		    }

		    if($this->input->post('oth_meals_flag') != ""){
		     $this->form_validation->set_rules('mealtype_name', 'Meals Name', 'callback_check_mealsname');		 
			 $this->form_validation->set_rules('mealtype_price', 'Meals Price', 'callback_check_price');
		    }*/

			
			    $return_data = $this->hotels_model->get_table_data('room_uploaded_image', 'hotel_room_type', 'hotel_room_type_id', $room_type_id);
         		$image = "";
          		if ($return_data != "") {
            		foreach ($return_data as $row) {
                		$image = $row->room_uploaded_image;
            		}
          		}          		

          		if(strlen($image) > 0){
          			$hotel_image = $image.",";
          		}
          		else{
          		  $hotel_image = $image;
          		}  
					for($i=0; $i<count($_FILES['hotel_image']['name']); $i++){
						if(!empty($_FILES['hotel_image']['name'][$i]))
						{	
							if(is_uploaded_file($_FILES['hotel_image']['tmp_name'][$i])) 
							{
								if(isset($hotel_image1[$i]) && $hotel_image1[$i]!=''){
									$oldImage = "uploads/room/".$hotel_image1[$i];
									//unlink($oldImage);
								}
								$sourcePath = $_FILES['hotel_image']['tmp_name'][$i];
								$img_Name	= time().$_FILES['hotel_image']['name'][$i];
								$targetPath = "uploads/room/".$img_Name;
								if(move_uploaded_file($sourcePath,$targetPath)){
									$hotel_image .= $img_Name.",";
								}
							}				
						}
					}

				if(strlen($hotel_image) > 0){	
			 	  $hotel_image = rtrim($hotel_image,",");			 	  
			 	}			 	  
			 	// debug($_POST);die;
				$this->hotels_model->update_room_type($_POST,$room_type_id,$hotel_image);
				//$this->hotels_model->update_room_type_count($_POST['rooms_tot_units_booked'], $room_type_id);
				if($hotel_id1 == ""){	 	  		
					redirect('hotels/room_types','refresh');
				}
				else{		
					redirect('hotels/hotel_room_types/'.base64_encode(json_encode($hotel_id1)),'refresh');
				}					   
			  //}//else 		
			}//if count			
			else if($room_type_id !=''){
				redirect('hotels/edit_room_type/'.$room_type_id1."/".$hotel_id1,'refresh');
			}
			else{
				redirect();
			}
		}//ifroom_type_id
		else{		
			if($hotel_id1 == ""){	 	  		
				redirect('hotels/room_types','refresh');
			}
			else{	
				redirect('hotels/hotel_room_types/'.$hotel_id1,'refresh');
			}
		}	
					
	}
	
	function delete_room_type($room_type_id1){
		$room_type_id 	= json_decode(base64_decode($room_type_id1));
		if($room_type_id != ''){
			$this->hotels_model->delete_room_type($room_type_id);
		}
		redirect('hotels/room_types','refresh');
	}
	//End Hotel Room Types
	function add_hotel(){ 

		if(count($_POST) > 0){
			
			$thumb_image = ''; 
			$thumb_image = $this->hotels_model->upload_image_lgm($_FILES, 'hotel_images','thumb_image');
			// echo "<pre>";print_r($_POST); exit;
			$hotel_image = '';
			for($i=0; $i<count($_FILES['hotel_image']['name']); $i++){
				$hotel_image .= $this->hotels_model->upload_images_all($_FILES, 'hotel_images','hotel_image','',$i);
				$hotel_image .= ",";
			}
			$hotel_image = rtrim($hotel_image,",");
			$hotel_image = rtrim($hotel_image,",");
			//$userid=$this->session->userdata('lgm_supplier_admin_id');

			$userid='0';
			
			$hotel_id = $this->hotels_model->add_hotel($_POST,$thumb_image,$hotel_image,$userid);

			if($hotel_id != '') {
					$hotel_details = $this->hotels_model->get_hotel_crs_list($hotel_id, $hotels);

					$replace['CURRENT_YEAR'] = date('Y');
					$replace['NEXT_YEAR'] = date('Y') + 1;
					$replace['TEXT'] = 'Hotel Details are successfully added';
					//Reemove for mail
					/*$template = $this->Email_Model->get_email_template_det('email_notification');
					//echo '<pre>sanjay'; print_r($template); exit();
					$data['subject'] = $template[0]->subject;
					$template_message = $template[0]->message;
					$cc = $template[0]->email_cc;
					$bcc = $template[0]->email_bcc;
					foreach ($replace as $key => $value) {
						$template_message = str_replace('{%%' . $key . '%%}', $value, $template_message);
					}
					$data['description'] = $template_message;
					$data['mailid'] = $hotel_details[0]->email;	
					$data['cc']     = 'polisettysanjay.provab@gmail.com';

					$this->Email_Model->send_mail_to_user($data);*/
             }	
             $this->hotels_model->update_wizard_status($hotel_id,'step1');
			redirect('hotels/manage_hotelchildgroup/'.base64_encode(json_encode($hotel_id)));
		}else{

			$hotels							= $this->hotels_model->getHomePageSettings();

			$hotels['hotel_types_list'] 	= $this->hotels_model->get_hotel_types_list_add();
			//$hotels['vechile'] 				= $this->Transfer_Model->get_vechile_detail()->result();
			
			$hotels['settings'] 			= $this->hotels_model->get_hotel_settings_list();
			$hotels['country'] = $this->hotels_model->get_country_details_hotel();	
			//debug($hotels['country']);exit;
			
			//$hotels['event_details']		 = $this->Event_Model->get_event_list();
			// debug($hotels);die;
			$this->template->view('hotel/hotel_crs/add_hotel',$hotels);
		}
	}

	function get_city_name($country_id = "",$selected_city = ""){
		$options = '';
		if(!empty($selected_city))
            $options = '<option value=0>Select</option>';
		else
            $options = '<option value=0 selected>Select</option>';

        if (($country_id) != "") {
            $result = $this->hotels_model->get_active_city_list_hotel($country_id);
            //echo "<pre/>";print_r($country_id);exit;
            if ($result != "") {
                foreach ($result as $row) {

                    $options .= '<option value="' . $row->city_name . '">' . $row->city_name . '</option>';
                }//for
            }//if 
        }
        echo $options;
	}

	function show_hotel_details($hotel_id1){ //error_reporting(E_ALL);
	    $hotel_id 	= json_decode(base64_decode($hotel_id1));
		if($hotel_id != ''){
			$hotels 					= $this->hotels_model->getHomePageSettings();
			$hotels['hotels_list'] 		= $this->hotels_model->get_hotel_crs_list($hotel_id, $hotels);
			$hotels['contatc_list'] 	= $this->hotels_model->get_contatc_list($hotel_id);
			$hotels['room_list'] 		= $this->hotels_model->get_room_list($hotel_id);
			$hotels['room_count_info'] 	= $this->hotels_model->get_room_count_info($hotel_id);
			$hotels['room_rate_info'] 	= $this->hotels_model->get_room_rate_info($hotel_id);
			$hotels['hotel_id'] 		= $hotel_id;
			$hotels['country'] = $this->hotels_model->get_country_details_hotel("");			
			//echo 'ddddddddd<pre/>';print_r($hotels['room_list']);exit;
			$this->template->view('hotel/hotel_crs/show_hotel',$hotels);
		}else{
			redirect('hotels/hotel_crs_list','refresh');
		}
	}
	function delete_image($hotel_id,$hotel_image){
		if($hotel_id!=''&& $hotel_image!=''){
			$result = $this->hotels_model->delete_hotel_image($hotel_id,$hotel_image);
		}
		redirect('hotels/hotel_crs_list','refresh');
	}

	//Hotel Drop down room type
	function room_types(){
		$hotels 						= $this->hotels_model->getHomePageSettings();
		//echo '<pre>'; print_r($hotels); exit();
		$hotels['room_types_list'] 	= $this->hotels_model->get_room_types_list("", $hotels);
		$this->template->view('hotel/room_type/room_types_list',$hotels);
	}

	function add_room_types(){
		/*error_reporting(E_ALL);
		ini_set('display_error', 'on');*/
		if(count($_POST) > 0){
			$this->form_validation->set_rules('room_type_name', 'room_type_name', 'required|is_unique[room_type.room_type_name]');
			  if ($this->form_validation->run() == TRUE)
                {
			$this->hotels_model->add_room_types_details($_POST);
			redirect('hotels/room_types','refresh');
				}
				else
				{

					$this->session->set_flashdata(array('message' => 'Duplicate Room Type', 'type' => WARNING_MESSAGE,'override_app_msg'=>1));
                	refresh();
                	$this->template->view('hotel/room_type/add_room_type');
				}
		}else{
			$data = '';
			$this->template->view('hotel/room_type/add_room_type');
		}
	}
	
	function inactive_room_types($room_type_id1){
		$room_type_id 	= json_decode(base64_decode($room_type_id1));
		if($room_type_id != ''){
			$this->hotels_model->inactive_room_types($room_type_id);
		}
		redirect('hotels/room_types','refresh');
	}
	
	function active_room_types($room_type_id1){
		$room_type_id 	= json_decode(base64_decode($room_type_id1));
		if($room_type_id != ''){
			$this->hotels_model->active_room_types($room_type_id);
		}
		redirect('hotels/room_types','refresh');
	}
	
	function edit_room_types($room_type_id1){
		$room_type_id 	= json_decode(base64_decode($room_type_id1));
		if($room_type_id != ''){
			
			$room_types['room_types_list'] 	= $this->hotels_model->get_room_types_list($room_type_id);
			$this->template->view('hotel/room_type/edit_room_type',$room_types);
		}else{
			redirect('hotels/room_types','refresh');
		}
	}
	
	function update_room_types($room_type_id1){
		$room_type_id 	= json_decode(base64_decode($room_type_id1));
		if($room_type_id != ''){
			if(count($_POST) > 0){
				$this->hotels_model->update_room_types($_POST,$room_type_id);
				redirect('hotels/room_types','refresh');
			}else if($hotel_type_id!=''){
				redirect('hotels/edit_room_type/'.$room_type_id,'refresh');
			}else{
				redirect('hotels/room_types','refresh');
			}
		}else{
			redirect('hotels/room_types','refresh');
		}
	}
	
	function delete_room_types($room_type_id1){
		$room_type_id 	= json_decode(base64_decode($room_type_id1));
		if($room_type_id != ''){
			$this->hotels_model->delete_room_types($room_type_id);
		}
		redirect('hotels/room_types','refresh');
	}

	function activate_multiple_hotels(){
		if(isset($_POST) && isset($_POST['data'])){
			
			$sql = "update hotel_details set status = 'ACTIVE' where hotel_details_id in (".$_POST['data'].")";
			$this->db->query($sql);
			$data['status'] = 1;
			$data['success_url'] = site_url()."/hotels/hotel_crs_list";
			print  json_encode($data);
			return;
		}
	}
	function inactivate_multiple_hotels(){
		if(isset($_POST) && isset($_POST['data'])){
			
			$sql = "update hotel_details set status = 'INACTIVE' where hotel_details_id in (".$_POST['data'].")";
			$this->db->query($sql);
			$data['status'] = 1;
			$data['success_url'] = site_url()."/hotels/hotel_crs_list";
			print  json_encode($data);
			return;
		}
	}

	function pre_cancellation($app_reference, $booking_source, $status = '')
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			if($booking_source==CRS_HOTEL_BOOKING_SOURCE){

					if($app_reference!=''){
						$resp = $this->hotels_model->hotel_crs_cancel($app_reference);
						// debug($resp);exit;
						$resp2 = json_decode($resp,true);
						//debug($resp2);exit;
						if($resp2['status']==1){
							/*sending mail to customer*/
							$this->load->model ( 'hotel_model' );
							$booking_details = $this->hotels_model->getBookingDetails($app_reference);
							$email = $booking_details[0]->email_id;
							$voucher_details['other'] = $this->hotels_model->get_voucher_details($app_reference);
							// $this->load->library('provab_pdf');
							// $this->load->library('provab_mailer');
							// $create_pdf = new Provab_Pdf();
							// $mail_template = $this->template->isolated_view('voucher/hotels_voucher_cancel', $voucher_details);
							// $pdf = $create_pdf->create_pdf($mail_template,'');
							// $this->provab_mailer->send_mail($email, domain_name().' - Hotel Voucher',$mail_template ,$pdf);
							/*$this->load->library('provab_mailgun');
							$mail_status = $this->provab_mailgun->send_mail($email, 'Reservation Services -- Hotel Voucher',$mail_template );*/
							// echo '<script>alert("Hotel Booking Cancelled");window.close();</script>';
							redirect(base_url().'index.php/report/b2c_crs_hotel_report');

						}
					}else{
						 $response = ['status'=>0,"msg"=>"Error while cancelling room!!"];
						 echo json_encode($response); 
					}
					
			}else{
				redirect('security/log_event?event=Invalid Details');
			}
			
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}

	function test_wizard()
	{
		$hotel_id='11';
		$status='step5';
		$this->hotels_model->update_wizard_status($hotel_id,$status);
		echo $this->db->last_query(); exit;
	}

	//End Of Adding Hotel

    public function quotation_list()
    {
    	// echo "<pre>";
    	// print_r(check_user_previlege('p250'));
    	// exit;
        if (!check_user_previlege('p250')) {
            set_update_message("You Don't have permission to do this action.", WARNING_MESSAGE, array(
                'override_app_msg' => true
            ));
            redirect(base_url());
        }
        // $order_by = array('id' => 'DESC');
        // $quotation_list = $this->custom_db->single_table_records('tours_quotation_log', $cols = '*', $condition = array(), $offset = 0, $limit = 100000000,$order_by);
        // $page_data['quotation_list'] = $quotation_list['data'];
        $query = 'SELECT tql.*, u.title as a_title,u.first_name as a_f_name,u.last_name as a_l_name,t.package_name FROM tours_quotation_log AS tql LEFT JOIN tours AS t ON tql.tour_id = t.id  LEFT JOIN user AS u ON tql.created_by_id = u.user_id ORDER BY tql.id DESC';

        //  debug($query);
        // exit("1477 - hotels.php");
        $quotation_list = $this->custom_db->get_result_by_query($query);




        $page_data['quotation_list'] = json_decode(json_encode($quotation_list),true);
        $this->template->view('tours/quotation_list',$page_data);
        $array = array(
            'back_link' => base_url().$this->router->fetch_class().'/'.$this->router->fetch_method()
        );
        $this->session->set_userdata( $array );
    }

    public function hotels_enquiry() {
// debug(check_user_previlege('p250'));
// die("dfgdfgdfg");
        if (!check_user_previlege('p250')) {
            set_update_message("You Don't have permission to do this action.", WARNING_MESSAGE, array(
                'override_app_msg' => true
            ));
            redirect(base_url());
        }
$this->load->model('tours_model');
        $total_records = $this->tours_model->tours_enquiry();
        //echo "in";
        // debug($total_records);die;
        $tours_enquiry = $this->tours_model->tours_enquiry();

        $page_data['tours_enquiry'] = $tours_enquiry['tours_enquiry'];
        $page_data['tour_list']          = $this->tours_model->tour_list();
        $page_data['tours_itinerary']    = $this->tours_model->tours_itinerary_all();
        $page_data['tours_country_name'] = $this->tours_model->tours_country_name();
        $this->template->view('tours/tours_enquiry',$page_data);
        // $array = array(
        //   'back_link' => base_url().$this->router->fetch_class().'/'.$this->router->fetch_method()
        //   );
//        $this->session->set_userdata( $page_data );
    }


}
