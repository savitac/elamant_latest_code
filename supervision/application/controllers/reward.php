<?php
//error_reporting(E_ALL);
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
/**
 *
 * @package Provab - Provab Application
 * @subpackage Travel Portal
 * @author Nikhildas s
 * @version V2
 */
class Reward extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		//$this->load->model ( 'rewards_model' );
		$this->load->library('session');
		$this->load->model('rewards_model');
		// error_reporting(E_ALL);
	}
	
	/**
	 * Nikhil das s
	 * Manage domain markup for provab - Domain wise and module wise
	 */
	function reward_report($offset=0){

		// error_reporting(E_ALL);
		$get_data =$this->input->get();
		$condition[] = array('U.user_type', '=', B2C_USER);
		if(valid_array($get_data) == true){
			$email = trim(@$get_data['email']);
			if(empty($email) == false) {
		
				$condition[] = array('user_name', '=', $this->db->escape($get_data['email']));
		
			}
			if(empty($get_data['app_reference']) == false) {
			
				$condition[] = array('book_id', '=', $this->db->escape($get_data['app_reference']));
			
			}
		}
		$total_records = $this->user_model->get_b2c_booked_detail($condition, true);
		$table_data = $this->user_model->get_b2c_booked_detail($condition, false, $offset, RECORDS_RANGE_2);
		$page_data['table_data'] = $table_data;
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/reward/reward_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
	
		$user_type = B2C_USER;
		$get_b2c_user = $this->rewards_model->get_b2c_user ($user_type);
		//$page_data ['data'] = $get_b2c_user;
		//debug($page_data); die;
	
		$this->template->view ('reward/reward_report', $page_data );
		
	}

	//for update general reward for each user
	function update_general_reward_for_all_users_based_on_id($reward){
		$this->db->select('*');
		$this->db->where(array('user_type'=>B2C_USER));
		$query = $this->db->get('user');
		$users_data = $query->result_array();
		foreach ($users_data as $key => $value) {
		  $current_pending_rewards = $this->calculate_pending_rewards($value['user_id']);
		  $data = array('pending_reward'=>$current_pending_rewards+$reward);
		  $this->db->where(array('user_id'=>$value['user_id']));
		  $re = $this->db->update('user',$data);
		}
		if($re){
			return TRUE;
		}else{
			return FLASE;
		}	
	}	

    /*
	function :to calculate the pending rewards
     */
	function calculate_pending_rewards($id){

		        $this->db->where(array('user_id'=>$id));
				$this->db->select('*');
				$get_query = $this->db->get('user');
				$current_pending_rewards_tmp = $get_query->result_array();
				$current_pending_rewards = $current_pending_rewards_tmp[0]['pending_reward'];
				return $current_pending_rewards;
	}
	/**
	 * Jaganath
	 * Manages Bank Account Details
	 */

	function loyalitypoint($flag = ''){
		error_reporting(E_ALL);
		if($flag == 'add'){
        	$this->user_model->updateLoyalityPoint($_POST);
		}
		$page_data['user_loyality_point'] = $this->user_model->getUserLoyalityPoint();
    	$this->template->view ( 'reward/loyalitypoint', $page_data );
	}
	function add_rewards($offset=0) {
		// error_reporting(E_ALL);
		$post_data = $this->input->post ();
		$get_data =$this->input->get();


		$condition[] = array('U.user_type', '=', B2C_USER);
		if(valid_array($get_data) == true){
			$email = trim(@$get_data['email']);
			if(empty($email) == false) {
				$condition[] = array('email', '=', $this->db->escape($get_data['email']));
				
			}
			// debug($condition);exit();
		}
		if($post_data ['spefic_reward'] == true){
			$email = trim(@$post_data['email']);
			if(empty($email) == false) {
				$condition[] = array('email', '=', $this->db->escape($post_data['email']));
				
			}
		}
		$total_records = $this->user_model->get_b2cuser_details($condition, true);

		$table_data = $this->user_model->get_b2cuser_details($condition, false, $offset, RECORDS_RANGE_2);
		// debug($table_data);exit();
		$page_data['table_data'] = $table_data;
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/reward/add_rewards/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		
		
		// debug($post_data);exit();
		$user_type = B2C_USER;
		$get_b2c_user = $this->rewards_model->get_b2c_user ($user_type);
		//$page_data ['data'] = $get_b2c_user; 
		// debug($page_data); die;
		// debug($post_data); exit('110');	
		// debug($post_data);exit();
		if (! empty ( $post_data )) {		
			// debug($post_data);exit('143');	
			if (isset ( $post_data ['general_reward'] )) {
				$reward = $post_data ['general_reward'];
				//debug($reward);exit();				
				$user_type = B2C_USER;
				$this->db->where ( 'user_type', $user_type );
				$this->db->where ( 'status', '1' );
				$this->db->set ( 'general_reward', $post_data ['general_reward'] );
				$this->db->set ( 'modified_datetime', date ( 'Y-m-d H:i:s' ) );
				//$this->db->set ( 'pending_reward',$current_pending_rewards+$reward, FALSE );
			    $this->db->update ( 'user' );
			    $this->update_general_reward_for_all_users_based_on_id($reward);
			   
			} else{
				if (isset ( $post_data ['spefic_reward'] )) {
				$reward = $post_data ['spefic_reward'];
				$user_id = trim ( $post_data ['user_id'] );	
				$current_pending_rewards = $this->calculate_pending_rewards($user_id);
                $this->db->where ( 'user_id', $user_id );				
				$this->db->set ( 'spefic_reward', $post_data ['spefic_reward'] );
				$this->db->set ( 'modified_datetime', date ( 'Y-m-d H:i:s' ) );
				$this->db->set ( 'pending_reward',$current_pending_rewards+$reward, FALSE );
				$this->db->update ( 'user' );
			}
			}
			set_update_message();
			redirect('reward/add_rewards');
			// debug($page_data);exit();
			// $this->template->view ( 'reward/add_rewards', $page_data );
		}else{
			// debug($page_data);exit();
			$this->template->view ( 'reward/add_rewards', $page_data );
		}
		
		
		
	}
	
	function reward_conversion() {
		$post_data = $this->input->post ();
		$data ['data'] = $this->rewards_model->get_reward_conversion ();
		//debug($data ['data']);exit();
		if (! empty ( $post_data )) {
			// debug($post_data); die;
			$origin = $post_data ['origin'];
			$post_data ['created'] = date ( 'Y-m-d H:i:s' );
			// UPDATE
			if (intval ( $origin ) > 0) { // Specific Agent Commission
				$update_condition ['origin'] = $origin;
			} else { // Default Commission
				$update_condition ['origin'] = 1;
			}
			if ($origin > 0) {
				$this->custom_db->update_record ( 'rewards', $post_data, $update_condition );

			}
			set_update_message();
			redirect('/reward/reward_conversion');	

		}else{
         $this->template->view ( 'reward/reward_conversion', $data );
		}
	}
	public function reward_range($module=META_ACCOMODATION_COURSE){
		
		// debug($module);exit();
		$page_data['rewards_range'] = $this->rewards_model->get_reward_range($module);
		$page_data['current_module'] = $module;
		$this->template->view('reward/reward_range',$page_data);
	}
	public function reward_range_submit(){
		$post_data = $this->input->post();
		$re = $this->rewards_model->reward_range($post_data);
		if($re){
		// $this->session->set_flashdata('message', 'Reward range added successfully');
		 set_update_message();
		 redirect("/reward/reward_range/".$post_data['module']);	
		}else{
		redirect("/reward/reward_range/".$post_data['module']);		
		}
	
	}
	public function reward_manage(){
		$post_data = $this->input->post();
		$re = $this->rewards_model->delete_reward_range($post_data['id']);
		if($re){
			echo TRUE;
		}else{
			echo FALSE;
		}
	}
	
}
