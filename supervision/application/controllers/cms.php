<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
// error_reporting(E_ALL);
/**
 *
 * @package Provab - Provab Application
 * @subpackage Travel Portal
 * @author Arjun J Gowda<arjunjgowda260389@gmail.com>
 * @version V2
 */
class Cms extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'module_model' );
	}
	/**
	 * Manage Hotel Top Destinations
	 */
	function hotel_top_destinations($offset = 0) {
		// Search Params(Country And City)
		// CMS - Image(On Home Page)
		$page_data = array ();
		$post_data = $this->input->post ();

		if (valid_array ( $post_data ) == true) {
			$city_origin = $post_data ['city'];
			// FILE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {
				$config ['upload_path'] = $this->template->domain_image_full_path ();
				$temp_file_name = $_FILES ['top_destination'] ['name'];
				$config ['allowed_types'] = 'jpg|jpeg|png';
				$config ['file_name'] = 'top-dest-hotel-' . $city_origin;
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;
				// UPDATE
				$temp_record = $this->custom_db->single_table_records ( 'all_api_city_master', 'image', array (
						'origin' => $city_origin 
				) );
				// echo $config ['upload_path'];
				$top_destination_image = $temp_record ['data'] [0] ['image'];
				// DELETE OLD FILES
				if (empty ( $top_destination_image ) == false) {
					$temp_top_destination_image = $this->template->domain_image_full_path ( $top_destination_image ); // GETTING FILE PATH
					if (file_exists ( $temp_top_destination_image )) {
						unlink ( $temp_top_destination_image );
					}
				}
				// UPLOAD IMAGE
				$this->load->library ( 'upload', $config );
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'top_destination' )) {
					echo $this->upload->display_errors ();
				} else {
					$image_data = $this->upload->data ();
				}
				// exit("1212");
				// debug($image_data);exit;
				$this->custom_db->update_record ( 'all_api_city_master', array (
						'top_destination' => ACTIVE,
						'image' => $image_data ['file_name'] 
				), array (
						'origin' => $city_origin 
				) );
				set_update_message ();
			}
			refresh ();
		}
		$filter = array (
				'top_destination' => ACTIVE 
		);
		$country_list = $this->custom_db->single_table_records ( 'api_country_master', 'country_name,origin,iso_country_code', array (
				'country_name !=' => '' 
		), 0, 1000, array (
				'country_name' => 'ASC' 
		) );
		$data_list = $this->custom_db->single_table_records ( 'all_api_city_master', '*', $filter, 0, 100000, array (
				'top_destination' => 'DESC',
				'city_name' => 'ASC' 
		) );
		// debug($country_list);exit;
		if ($country_list ['status'] == SUCCESS_STATUS) {
			$page_data ['country_list'] = magical_converter ( array (
					'k' => 'iso_country_code',
					'v' => 'country_name' 
			), $country_list );
		}
		// debug($page_data ['country_list']);
		// exit;
		$page_data ['data_list'] = @$data_list ['data'];
		$this->template->view ( 'cms/hotel_top_destinations', $page_data );
	}
	/*
	 * Deactivate Top Destination
	 */
	function deactivate_top_destination($origin) {
		$status = INACTIVE;
		$info = $this->module_model->update_top_destination ( $status, $origin );
		redirect ( base_url () . 'cms/hotel_top_destinations' );
	}
	/**
	 * Manage Bus Top Destinations
	 */
	function bus_top_destinations($offset = 0) {
		// Search Params(Country And City)
		// CMS - Image(On Home Page)
		$page_data = array ();
		$post_data = $this->input->post ();
		if (valid_array ( $post_data ) == true) {
			$city_origin = $post_data ['city'];
			// FILE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {
				$config ['upload_path'] = $this->template->domain_image_upload_path ();
				$temp_file_name = $_FILES ['top_destination'] ['name'];
				$config ['allowed_types'] = 'jpg|jpeg|png';
				$config ['file_name'] = 'top-dest-bus-' . $city_origin;
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;
				// UPDATE
				$temp_record = $this->custom_db->single_table_records ( 'bus_stations', 'image', array (
						'origin' => $city_origin 
				) );
				$top_destination_image = $temp_record ['data'] [0] ['image'];
				// DELETE OLD FILES
				if (empty ( $top_destination_image ) == false) {
					$temp_top_destination_image = $this->template->domain_image_full_path ( $top_destination_image ); // GETTING FILE PATH
					if (file_exists ( $temp_top_destination_image )) {
						unlink ( $temp_top_destination_image );
					}
				}
				// UPLOAD IMAGE
				$this->load->library ( 'upload', $config );
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'top_destination' )) {
					echo $this->upload->display_errors ();
				} else {
					$image_data = $this->upload->data ();
				}
				// debug($image_data);exit;
				$this->custom_db->update_record ( 'bus_stations', array (
						'top_destination' => ACTIVE,
						'image' => $image_data ['file_name'] 
				), array (
						'origin' => $city_origin 
				) );
				set_update_message ();
			}
			refresh ();
		}
		$filter = array (
				'top_destination' => ACTIVE 
		);
		$bus_list = $this->custom_db->single_table_records ( 'bus_stations', 'name,origin', array (
				'name !=' => '' 
		), 0, 1000, array (
				'name' => 'ASC' 
		) );
		$data_list = $this->custom_db->single_table_records ( 'bus_stations', '*', $filter, 0, 100000, array (
				'top_destination' => 'DESC',
				'name' => 'ASC' 
		) );
		
		if ($bus_list ['status'] == SUCCESS_STATUS) {
			$page_data ['bus_list'] = magical_converter ( array (
					'k' => 'origin',
					'v' => 'name' 
			), $bus_list );
		}
		
		$page_data ['data_list'] = @$data_list ['data'];
		$this->template->view ( 'cms/bus_top_destinations', $page_data );
	}
	/**
	 * Deactivate Top Bus Destination
	 */
	function deactivate_bus_top_destination($origin) {
		$status = INACTIVE;
		$info = $this->module_model->update_bus_top_destination ( $status, $origin );
		redirect ( base_url () . 'cms/bus_top_destinations' );
	}
	
	/**
	 * Manage Flight Top Destinations
	 */
	// function flight_top_destinations($offset = 0) {
	// 	// Search Params(Country And City)
	// 	// CMS - Image(On Home Page)
	// 	$page_data = array ();
	// 	$post_data = $this->input->post ();
	// 	if (valid_array ( $post_data ) == true) {
	// 		$aiport_origin = $post_data ['airport'];
	// 		// FILE UPLOAD
	// 		if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {
	// 			$config ['upload_path'] = $this->template->domain_image_upload_path ();
	// 			$temp_file_name = $_FILES ['top_destination'] ['name'];
	// 			$config ['allowed_types'] = 'jpg|jpeg|png';
	// 			$config ['file_name'] = 'top-dest-fight-' . $aiport_origin;
	// 			$config ['max_size'] = '1000000';
	// 			$config ['max_width'] = '';
	// 			$config ['max_height'] = '';
	// 			$config ['remove_spaces'] = false;
	// 			// UPDATE
	// 			$temp_record = $this->custom_db->single_table_records ( 'flight_airport_list', 'image', array (
	// 					'origin' => $aiport_origin
	// 			) );
	// 			$top_destination_image = $temp_record ['data'] [0] ['image'];
	// 			// DELETE OLD FILES
	// 			if (empty ( $top_destination_image ) == false) {
	// 				$temp_top_destination_image = $this->template->domain_image_full_path ( $top_destination_image ); // GETTING FILE PATH
	// 				if (file_exists ( $temp_top_destination_image )) {
	// 					unlink ( $temp_top_destination_image );
	// 				}
	// 			}
	// 			// UPLOAD IMAGE
	// 			$this->load->library ( 'upload', $config );
	// 			$this->upload->initialize ( $config );
	// 			if (! $this->upload->do_upload ( 'top_destination' )) {
	// 				echo $this->upload->display_errors ();
	// 			} else {
	// 				$image_data = $this->upload->data ();
	// 			}
	// 			// debug($image_data);exit;
	// 			$this->custom_db->update_record ( 'flight_airport_list', array (
	// 					'top_destination' => ACTIVE,
	// 					'image' => $image_data ['file_name']
	// 			), array (
	// 					'origin' => $aiport_origin
	// 			) );
	// 			set_update_message ();
	// 		}
	// 		refresh ();
	// 	}
	// 	$filter = array (
	// 			'top_destination' => ACTIVE
	// 	);
	// 	$flight_list = $this->custom_db->single_table_records ( 'flight_airport_list', 'airport_city,origin', array (
	// 			'airport_city !=' => ''
	// 	), 0, 1000, array (
	// 			'airport_city' => 'ASC'
	// 	) );
	// 	$data_list = $this->custom_db->single_table_records ( 'flight_airport_list', '*', $filter, 0, 100000, array (
	// 			'top_destination' => 'DESC',
	// 			'airport_city' => 'ASC'
	// 	) );
	// 	//echo $this->db->last_query();exit;
	// 	if ($flight_list ['status'] == SUCCESS_STATUS) {
	// 		$page_data ['flight_list'] = magical_converter ( array (
	// 				'k' => 'origin',
	// 				'v' => 'airport_city'
	// 		), $flight_list );
	// 	}
	// //	debug($page_data);exit;
	// 	$page_data ['data_list'] = @$data_list ['data'];
	// 	$this->template->view ( 'cms/flight_top_destinations', $page_data );
	// }


	function flight_top_destinations($offset = 0) {
		// Search Params(Country And City)
		// CMS - Image(On Home Page)
		//error_reporting(E_ALL);
		$page_data = array ();
		$post_data = $this->input->post ();

		// debug($post_data);
		// exit("266-cms");

		if (valid_array ( $post_data ) == true) {
			$aiport_origin = $post_data ['from_airport_name'];
			$aiport_destination = $post_data ['to_airport_name'];
			// FILE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {

				$ext = pathinfo($_FILES['top_destination']['name'], PATHINFO_EXTENSION);

				$config ['upload_path'] = $this->template->domain_image_upload_path ();
				$temp_file_name = $_FILES ['top_destination'] ['name'];
				$config ['allowed_types'] = 'jpg|jpeg|png';
				$config ['file_name'] = 'top-dest-fight-' . $aiport_origin.'.'.$ext;
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;

				// $aiport_origin = $post_data ['from_airport_name'];
				// $aiport_destination = $post_data ['to_airport_name'];

				$origin_details = $this->custom_db->single_table_records('flight_airport_list','*',array('origin' => $aiport_origin));

				$origin_details = $origin_details['data'][0];

				$destination_details = $this->custom_db->single_table_records('flight_airport_list','*',array('origin' => $aiport_destination));

				$destination_details = $destination_details['data'][0];

				$data = array();

				$data['origin_code'] = $origin_details['origin'];
				$data['from_airport_code'] = $origin_details['airport_code'];
				$data['from_airport_city'] = $origin_details['airport_city'];
				$data['from_country'] = $origin_details['country'];
				$data['destination_code'] = $destination_details['origin'];
				$data['destination_airport_code'] = $destination_details['airport_code'];
				$data['destination_airport_city'] = $destination_details['airport_city'];
				$data['to_country'] = $destination_details['country'];
				$data['image_path'] = $config ['file_name'];
				$data['top_destination'] = '1';
				$data['price'] = $post_data ['price'];

				// debug($data);
				// exit("311");


				$this->load->library ( 'upload', $config );
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'top_destination' )) {
					echo $this->upload->display_errors ();
				} else {
					$image_data = $this->upload->data ();
				}
				$this->custom_db->insert_record('top_destination_multiple',$data);

			}
			refresh ();
		}
		$filter = array (
				'top_destination' => ACTIVE
		);
		// $country_list = $this->custom_db->single_table_records ( 'flight_airport_list', 'country', array (
		// 		'country !=' => '' 
		// ), 0, 1000, array (
		// 		'country' => 'ASC' 
		// ) );
		$data_list = $this->custom_db->single_table_records ( 'flight_airport_list', '*', $filter, 0, 100000, array (
				'top_destination' => 'DESC',
				'airport_name' => 'ASC' 
		) );
		$country_list = $this->custom_db->get_airport_country_list();

		//debug($country_list);exit;
		// if ($country_list ['status'] == SUCCESS_STATUS) {
		// 	$page_data ['country_list'] = magical_converter ( array (
		// 			'k' => 'origin',
		// 			'v' => 'country' 
		// 	), $country_list );
		// }

// debug($page_data);
// 		exit("354-cms")	;

		$flight_list = $this->custom_db->single_table_records ( 'flight_airport_list', 'airport_city,origin', array (
				'airport_city !=' => ''
		), 0, 10000, array (
				'airport_city' => 'ASC'
		) );
		$data_list = $this->custom_db->single_table_records ( 'top_destination_multiple', '*', $filter, 0, 100000, array (
				'top_destination' => 'DESC',
				//'airport_city' => 'ASC'
		) );
		//echo $this->db->last_query();exit;
		if ($flight_list ['status'] == SUCCESS_STATUS) {
			$page_data ['flight_list'] = magical_converter ( array (
					'k' => 'origin',
					'v' => 'airport_city'
			), $flight_list );
		}
		$page_data['country_list'] = $country_list;
	//	debug($page_data);exit;
		$page_data ['data_list'] = @$data_list ['data'];
		;
		$this->template->view ( 'cms/flight_top_destinations', $page_data );
	}
	/**
	 * Deactivate Top Bus Destination
	 */
	function deactivate_flight_top_destination($origin) {
		$status = INACTIVE;
		$info = $this->module_model->update_flight_top_destination ( $status, $origin );
		redirect ( base_url () . 'cms/flight_top_destinations' );
	}
	/**
	 * Static Page Content
	 */
	function add_cms_page($id = '') {
		
		// privilege_handler('p54');
		$this->form_validation->set_message ( 'required', 'Required.' );
		
		// check for negative id
		valid_integer ( $id );
		
		// validation rules
		$post_data = $this->input->post ();
		// get data
		$cols = ' * ';
		if (valid_array ( $post_data ) == false) {
			if (intval ( $id ) > 0) {
				// edit data
				$tmp_data = $this->custom_db->single_table_records ( 'cms_pages', '', array (
						'page_id' => $id 
				) );
				// debug($tmp_data);exit;
				if (valid_array ( $tmp_data ['data'] [0] )) {
					$data ['page_title'] = $tmp_data ['data'] [0] ['page_title'];
					$data ['page_description'] = $tmp_data ['data'] [0] ['page_description'];
					$data ['page_seo_title'] = $tmp_data ['data'] [0] ['page_seo_title'];
					$data ['page_seo_keyword'] = $tmp_data ['data'] [0] ['page_seo_keyword'];
					$data ['page_seo_description'] = $tmp_data ['data'] [0] ['page_seo_description'];
					$data ['page_position'] = $tmp_data ['data'] [0] ['page_position'];
				} else {
					redirect ( 'cms/add_cms_page' );
				}
			}
		} elseif (valid_array ( $post_data )) {
			$this->form_validation->set_rules ( 'page_title', 'Page Title', 'required' );
			$this->form_validation->set_rules ( 'page_description', 'Page Description', 'required' );
			$this->form_validation->set_rules ( 'page_seo_title', 'Page SEO Title', 'required' );
			$this->form_validation->set_rules ( 'page_seo_keyword', 'Page SEO Keyword', 'required' );
			$this->form_validation->set_rules ( 'page_seo_description', 'Page SEO Description', 'required' );
			$this->form_validation->set_rules ( 'page_position', 'Page Position', 'required' );
			
			$data ['page_title'] = $title = $this->input->post ( 'page_title' );
			$data ['page_description'] = $this->input->post ( 'page_description' );
			$data ['page_seo_title'] = $this->input->post ( 'page_seo_title' );
			$data ['page_seo_keyword'] = $this->input->post ( 'page_seo_keyword' );
			$data ['page_seo_description'] = $this->input->post ( 'page_seo_description' );
			$data ['page_position'] = $this->input->post ( 'page_position' );
			$data ['page_label'] = $this->uniqueLabel(substr($title, 0,100));
			//debug($data);exit;
			if ($this->form_validation->run ()) {
				// add / update data
				if (intval ( $id ) > 0) {
					$this->custom_db->update_record ( 'cms_pages', $data, array (
							'page_id' => $id 
					) );
				} else {
					$this->custom_db->insert_record ( 'cms_pages', $data );
				}
				redirect ( 'cms/add_cms_page' );
			}
		}
		$data ['ID'] = $id;
		// get all sub admin
		$tmp_data = $this->custom_db->single_table_records ( 'cms_pages', $cols );
		$data ['sub_admin'] = '';
		$data ['sub_admin'] = $tmp_data ['data'];
		$this->template->view ( 'cms/add_cms_page', $data );
	}
	/**
	 * Status update of Static Page Content
	 */
	function cms_status($id = '', $status = 'D') {
		if ($id > 0) {
			if (strcmp ( $status, 'D' ) == 0) {
				$status = 0;
			} else {
				$status = 1;
			}
			
			$this->custom_db->update_record ( 'cms_pages', array (
					'page_status' => $status 
			), array (
					'page_id' => $id 
			) );
		}
		redirect ( 'cms/add_cms_page' );
	}
	public function uniqueLabel($string) {
		//Lower case everything
		$string = strtolower($string);
		//Make alphanumeric (removes all other characters)
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "-", $string);
		return $string;
	}

	// public function deal(){
	// 	$page_data = array ();
	// 	$post_data = $this->input->post ();

	// 	$country_list = $this->custom_db->get_airport_country_list();
	// 	$page_data['country_list'] = $country_list;

	// 	$bus_list = $this->custom_db->single_table_records ( 'bus_stations', 'name,origin', array (
	// 			'name !=' => '' 
	// 	), 0, 1000, array (
	// 			'name' => 'ASC' 
	// 	) );
		
		
	// 	if ($bus_list ['status'] == SUCCESS_STATUS) {
	// 		$page_data ['bus_list'] = magical_converter ( array (
	// 				'k' => 'origin',
	// 				'v' => 'name' 
	// 		), $bus_list );
	// 	}

	// 		$filter = array (
	// 			'top_destination' => ACTIVE 
	// 	);
	// 	$hotel_country_list = $this->custom_db->single_table_records ( 'api_country_master', 'country_name,origin,iso_country_code', array (
	// 			'country_name !=' => '' 
	// 	), 0, 1000, array (
	// 			'country_name' => 'ASC' 
	// 	) );
	
	// 	// debug($country_list);exit;
	// 	if ($hotel_country_list ['status'] == SUCCESS_STATUS) {
	// 		$page_data ['hotel_country_list'] = magical_converter ( array (
	// 				'k' => 'iso_country_code',
	// 				'v' => 'country_name' 
	// 		), $hotel_country_list );
	// 	}

	// 	if (valid_array ( $post_data ) == true) {

	// 		debug($post_data);
	// 		//exit("504");
	// 		debug($_FILES);
	// 		exit("506");

	// 		$city_origin = $post_data ['city'];
	// 		// FILE UPLOAD
	// 		if (valid_array ( $_FILES ) == true and $_FILES ['deal_image'] ['error'] == 0 and $_FILES ['deal_image'] ['size'] > 0) {
	// 			$config ['upload_path'] = $this->template->domain_image_upload_path ();
	// 			$temp_file_name = $_FILES ['deal_image'] ['name'];
	// 			$config ['allowed_types'] = 'jpg|jpeg|png';
	// 			$config ['file_name'] = 'top-dest-bus-' . $city_origin;
	// 			$config ['max_size'] = '1000000';
	// 			$config ['max_width'] = '';
	// 			$config ['max_height'] = '';
	// 			$config ['remove_spaces'] = false;
	// 			// UPDATE
				
	// 			$this->load->library ( 'upload', $config );
	// 			$this->upload->initialize ( $config );
	// 			if (! $this->upload->do_upload ( 'deal_offer' )) {
	// 				echo $this->upload->display_errors ();
	// 			} else {
	// 				$image_data = $this->upload->data ();
	// 			}
	// 			// debug($image_data);exit;
	// 			$this->custom_db->update_record ( 'deals', array (
	// 					'deal' => ACTIVE,
	// 					'image' => $image_data ['file_name'] 
	// 			), array (
	// 					'origin' => $city_origin 
	// 			) );
	// 			set_update_message ();
	// 		}
	// 		refresh ();
	// 	}
		
	// 	$page_data ['data_list'] = @$data_list ['data'];
	// 	$this->template->view ( 'cms/deal', $page_data );

	// }

	//Deals Start

	
	function flight_deals($offset = 0) {
		
		$page_data = array ();
		$post_data = $this->input->post ();

		// debug($post_data);
		// exit("266-cms");

		if (valid_array ( $post_data ) == true) {
			$aiport_origin = $post_data ['from_airport_name'];
			$aiport_destination = $post_data ['to_airport_name'];
			// FILE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {

				$ext = pathinfo($_FILES['top_destination']['name'], PATHINFO_EXTENSION);

				$config ['upload_path'] = $this->template->domain_image_upload_path ();
				$temp_file_name = $_FILES ['top_destination'] ['name'];
				$config ['allowed_types'] = 'jpg|jpeg|png';
				$config ['file_name'] = 'top-dest-fight-' . $aiport_origin.'.'.$ext;
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;

				// $aiport_origin = $post_data ['from_airport_name'];
				// $aiport_destination = $post_data ['to_airport_name'];
				$date_cnv = date_create($post_data['offer_expiry_date']); 
				$expiry_date = date_format($date_cnv,"Y-m-d H:i:s");

				$origin_details = $this->custom_db->single_table_records('flight_airport_list','*',array('origin' => $aiport_origin));

				$origin_details = $origin_details['data'][0];

				$destination_details = $this->custom_db->single_table_records('flight_airport_list','*',array('origin' => $aiport_destination));

				$destination_details = $destination_details['data'][0];

				$data = array();

				$data['origin_code'] = $origin_details['origin'];
				$data['from_airport_code'] = $origin_details['airport_code'];
				$data['from_airport_city'] = ucfirst($origin_details['airport_city']);
				$data['from_country'] = $origin_details['country'];
				$data['destination_code'] = $destination_details['origin'];
				$data['destination_airport_code'] = $destination_details['airport_code'];
				$data['destination_airport_city'] = ucfirst($destination_details['airport_city']);
				$data['to_country'] = $destination_details['country'];
				$data['image_path'] = $config ['file_name'];
				$data['deal_status'] = '1';
				$data['expiry_date'] = $expiry_date;
				$data['price'] = $post_data ['price'];

				// debug($data);
				// exit("311");


				$this->load->library ( 'upload', $config );
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'top_destination' )) {
					echo $this->upload->display_errors ();
				} else {
					$image_data = $this->upload->data ();
				}
				$this->custom_db->insert_record('flight_deals',$data);

			}
			refresh ();
		}
		$filter = array (
				'top_destination' => ACTIVE
		);
	
		$data_list = $this->custom_db->single_table_records ( 'flight_airport_list', '*', $filter, 0, 100000, array (
				'top_destination' => 'DESC',
				'airport_name' => 'ASC' 
		) );
		$country_list = $this->custom_db->get_airport_country_list();

		$flight_list = $this->custom_db->single_table_records ( 'flight_airport_list', 'airport_city,origin', array (
				'airport_city !=' => ''
		), 0, 10000, array (
				'airport_city' => 'ASC'
		) );
		$filter = array (
				'deal_status' => ACTIVE
		);
		$data_list = $this->custom_db->single_table_records ( 'flight_deals', '*', $filter, 0, 100000, array (
				'deal_status' => 'DESC',
				//'airport_city' => 'ASC'
		) );
		//echo $this->db->last_query();exit;
		if ($flight_list ['status'] == SUCCESS_STATUS) {
			$page_data ['flight_list'] = magical_converter ( array (
					'k' => 'origin',
					'v' => 'airport_city'
			), $flight_list );
		}
		$page_data['country_list'] = $country_list;
	//	debug($page_data);exit;
		$page_data ['data_list'] = @$data_list ['data'];
		;
		$this->template->view ( 'cms/flight_deals', $page_data );
	}
	/**
	 * Deactivate Flight Deals
	 */
	function deactivate_flight_deals($origin) {
		$status = INACTIVE;
		$info = $this->module_model->update_flight_deal_status ( $status, $origin );
		redirect ( base_url () . 'cms/flight_deals' );
	}

	/**
	*Blog Start
	**/
	function blog(){
		$page_data = array ();
		$post_data = $this->input->post ();

		// debug($post_data);
		// exit("266-cms");

		if (valid_array ( $post_data ) == true) {
				$blog_title = $post_data['blog_title'];
			// FILE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {

				$ext = pathinfo($_FILES['top_destination']['name'], PATHINFO_EXTENSION);

		
				if($ext =='png' ||$ext =='jpg'||$ext =='jpeg'){
				$rand = rand(10,100);

				$config ['upload_path'] = $this->template->domain_image_upload_path ();
				$temp_file_name = $_FILES ['top_destination'] ['name'];
				$config ['allowed_types'] = 'jpg|jpeg|png';
				$config ['file_name'] = 'blog_' . $blog_title.'_'.$rand.'.'.$ext;
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;

				// $aiport_origin = $post_data ['from_airport_name'];
				// $aiport_destination = $post_data ['to_airport_name'];
			
				$data = array();
				$data['blog_title'] = ucfirst($post_data['blog_title']);
				$data['blog_image'] = $config ['file_name'];
				$data['blog_status'] = '1';
				$data['blog_description'] = ucfirst($post_data['blog_description']);
				$data['ip_address'] = $this->get_ip_address();
				// debug($data);
				// exit("311");


				$this->load->library ( 'upload', $config );
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'top_destination' )) {
					echo $this->upload->display_errors ();
				} else {
					$image_data = $this->upload->data ();
				}
				$this->custom_db->insert_record('blog',$data);
			}

			}
			refresh ();
		}
	
		$filter = array (
				'blog_status' => ACTIVE
		);
		$data_list = $this->custom_db->single_table_records ( 'blog', '*', $filter, 0, 100000, array (
				'blog_status' => 'DESC',
				'blog_title' => 'ASC'
		) );
	
		$page_data ['data_list'] = @$data_list ['data'];
		
		$this->template->view ( 'cms/blog', $page_data );
	}


	function other_deal(){
		$page_data = array ();
		$post_data = $this->input->post ();

		if (valid_array ( $post_data ) == true) {
				$deal_title = $post_data['deal_title'];
			// FILE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {

				$ext = pathinfo($_FILES['top_destination']['name'], PATHINFO_EXTENSION);

		
				if($ext =='png' ||$ext =='jpg'||$ext =='jpeg'){
				$rand = rand(10,100);

				$config ['upload_path'] = $this->template->domain_image_upload_path ();
				$temp_file_name = $_FILES ['top_destination'] ['name'];
				$config ['allowed_types'] = 'jpg|jpeg|png';
				$config ['file_name'] = 'blog_' . $deal_title.'_'.$rand.'.'.$ext;
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;

				// $aiport_origin = $post_data ['from_airport_name'];
				// $aiport_destination = $post_data ['to_airport_name'];
				$date_cnv = date_create($post_data['offer_expiry_date']); 
				$expiry_date = date_format($date_cnv,"Y-m-d H:i:s");
			
				$data = array();
				$data['deal_name'] = ucfirst($post_data['deal_title']);
				$data['deal_image'] = $config ['file_name'];
				$data['deal_status'] = '1';
				$data['deal_type'] = $post_data ['deal_type'];
				$data['discount'] = $post_data['discount'];
				$data['deal_code'] = $post_data['promocode'];
				$data['ip'] = $this->get_ip_address();
				$data['expiry_date'] = $expiry_date;
				$data['category'] = ucfirst($post_data['category']);
				// debug($data);
				// exit("311");


				$this->load->library ( 'upload', $config );
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'top_destination' )) {
					echo $this->upload->display_errors ();
				} else {
					$image_data = $this->upload->data ();
				}
				$this->custom_db->insert_record('deals',$data);
			}

			}
			refresh ();
		}
	
		$filter = array (
				'deal_status' => ACTIVE
		);
		$data_list = $this->custom_db->single_table_records ( 'deals', '*', $filter, 0, 100000, array (
				'deal_status' => 'DESC',
				'deal_name' => 'ASC'
		) );
	
		$page_data ['data_list'] = @$data_list ['data'];
		;
		$this->template->view ( 'cms/deal', $page_data );

	}

	function deactivate_deal($deal_id){
		$status = INACTIVE;
		$info = $this->module_model->update_deal_status ( $status, $deal_id );
		redirect ( base_url () . 'cms/other_deal' );
	}

	/**
	* Deactivate Blog
	**/

	function deactivate_blog($blog_id){
		$status = INACTIVE;
		$info = $this->module_model->update_blog_status ( $status, $blog_id );
		redirect ( base_url () . 'cms/blog' );
	}


	/**
	* User Ip
	**/
	function get_ip_address()
		{
		    // Get real visitor IP behind CloudFlare network
		    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
		              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
		              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
		    }
		    $client  = @$_SERVER['HTTP_CLIENT_IP'];
		    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		    $remote  = $_SERVER['REMOTE_ADDR'];

		    if(filter_var($client, FILTER_VALIDATE_IP))
		    {
		        $ip = $client;
		    }
		    elseif(filter_var($forward, FILTER_VALIDATE_IP))
		    {
		        $ip = $forward;
		    }
		    else
		    {
		        $ip = $remote;
		    }

		    return $ip;
		}

		function deal_banner(){
	
		$page_data = array ();
		$post_data = $this->input->post ();

		if (valid_array ( $post_data ) == true) {
				$category = $post_data['category'];
			// FILE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {

				$ext = pathinfo($_FILES['top_destination']['name'], PATHINFO_EXTENSION);

		
				if($ext =='png' ||$ext =='jpg'||$ext =='jpeg'){
				$rand = rand(10,100);

				$config ['upload_path'] = $this->template->domain_image_upload_path ();
				$temp_file_name = $_FILES ['top_destination'] ['name'];
				$config ['allowed_types'] = 'jpg|jpeg|png';
				$config ['file_name'] = 'deal_banner_' . $category.'_'.$rand.'.'.$ext;
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;

				// $aiport_origin = $post_data ['from_airport_name'];
				// $aiport_destination = $post_data ['to_airport_name'];
				$date_cnv = date_create($post_data['offer_expiry_date']); 
				$expiry_date = date_format($date_cnv,"Y-m-d H:i:s");
			
				$data = array();
				$data['deal_image'] = $config ['file_name'];
				$data['deal_status'] = '1';
				$data['discount'] = $post_data['discount'];
				$data['deal_code'] = $post_data['promocode'];
				$data['ip'] = $this->get_ip_address();
				$data['expiry_date'] = $expiry_date;
				$data['category'] = ucfirst($post_data['category']);
				// debug($data);
				// exit("311");


				$this->load->library ( 'upload', $config );
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'top_destination' )) {
					echo $this->upload->display_errors ();
				} else {
					$image_data = $this->upload->data ();
				}
				$insert_id=$this->custom_db->insert_record('deals_banner',$data);
				// debug($insert_id);exit("917");
				$status = ACTIVE;
				$info = $this->module_model->activate_deal_banner_status ( $status, $insert_id['insert_id']);
			}

			}
			refresh ();
		}
	
		$filter = '';
		$data_list = $this->custom_db->single_table_records ( 'deals_banner', '*', $filter, 0, 100000, array (
				'deal_status' => 'DESC',
				'deal_id' => 'DESC'
		) );
	
		$page_data ['data_list'] = @$data_list ['data'];

			$this->template->view ( 'cms/deal_banner',$page_data);
		}

		
	function deactivate_deal_banner($banner_id){
		$status = INACTIVE;
		$info = $this->module_model->update_deal_banner_status ( $status, $banner_id );
		redirect ( base_url () . 'cms/deal_banner' );
	}

	function delete_deal_banner($banner_id){
		$delete = $this->module_model->delete_deal_banner ( $banner_id );
		redirect ( base_url () . 'cms/deal_banner' );
	}

	function activate_deal_banner($banner_id){
		$status = ACTIVE;
		$info = $this->module_model->activate_deal_banner_status ( $status, $banner_id );
		redirect ( base_url () . 'cms/deal_banner' );
	}

}