<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package    Provab - Provab Application
 * @subpackage Travel Portal
 * @author     Jaganath J<jaganath.provab@gmail.com>
 * @version    V2
 */
class Report extends CI_Controller {
	private $current_module;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('bus_model');
		$this->load->model('hotel_model');
		$this->load->model('hotels_model');
		$this->load->model('flight_model');
		$this->load->model('package_model');
		$this->load->model('sightseeing_model');
		$this->load->model('transferv1_model');
		$this->load->model('car_model');
		$this->load->model('user_model');
		$this->load->library('booking_data_formatter');
		$this->current_module = $this->config->item('current_module');
//		$this->load->library('export');

	}
	function index()
	{
		redirect('general');
	}

	function monthly_booking_report()
	{
		$this->template->view('report/monthly_booking_report');
	}


	function bus($offset=0)
	{
		$get_data = $this->input->get();
		$condition = array();
		$page_data = array();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}

			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}

			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}

			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone_number', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}

			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}

			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->bus_model->booking($condition, true);
		$table_data = $this->bus_model->booking($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_bus_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		/** TABLE PAGINATION */
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/bus/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['customer_email'] = $this->entity_email;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/bus', $page_data);
	}

	function hotel($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}

			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}

			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}

			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone_number', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}

			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}

			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->hotel_model->booking($condition, true);
		$table_data = $this->hotel_model->booking($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_hotel_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/hotel/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/hotel', $page_data);
	}

	
	
	
	function b2c_bus_report($offset=0)
	{
		$get_data = $this->input->get();
		$condition = array();
		$page_data = array();

		$filter_data = $this->format_basic_search_filters('bus');
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];

		//debug($get_data); die;
		/*if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			// if (empty($get_data['phone']) == false) {
			// 	$condition[] = array('BD.phone_number', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			// }
	
			// if (empty($get_data['email']) == false) {
			// 	$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			// }
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			if (empty($get_data['pnr']) == false) {
				$condition[] = array('BD.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}*/
	
		$total_records = $this->bus_model->b2c_bus_report($condition, true);
		$table_data = $this->bus_model->b2c_bus_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_bus_booking_data($table_data,$this->current_module);
		
		$page_data['table_data'] = $table_data['data'];

		//debug($table_data); exit;

		/** TABLE PAGINATION */
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2c_bus_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['customer_email'] = $this->entity_email;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		//debug($page_data); die;
		$this->template->view('report/b2c_report_bus', $page_data);
	}
	
	
	function b2b_bus_report($offset=0)
	{
		$get_data = $this->input->get();
		$condition = array();
		$page_data = array();

		$filter_data = $this->format_basic_search_filters('bus');
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];

		//debug($condition); die;
		$total_records = $this->bus_model->b2b_bus_report($condition, true);
		$table_data = $this->bus_model->b2b_bus_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_bus_booking_data($table_data,'b2b');
		$page_data['table_data'] = $table_data['data'];
		/** TABLE PAGINATION */
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_bus_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['customer_email'] = $this->entity_email;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');

		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>B2B_USER,'domain_list_fk'=>get_domain_auth_id()));
		
		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);

		$this->template->view('report/b2b_report_bus', $page_data);
	}
	
	
	
	function b2b_hotel_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();

		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		
		$total_records = $this->hotel_model->b2b_hotel_report($condition, true);
		$table_data = $this->hotel_model->b2b_hotel_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_hotel_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_hotel_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		
		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>B2B_USER,'domain_list_fk'=>get_domain_auth_id()));
		
		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);
		$this->template->view('report/b2b_report_hotel', $page_data);
	}


	function corporate_hotel_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();

		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		
		$total_records = $this->hotel_model->corporate_hotel_report($condition, true);
		$table_data = $this->hotel_model->corporate_hotel_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_hotel_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/corporate_hotel_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		
		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>CORPORATE_USER,'domain_list_fk'=>get_domain_auth_id()));
		
		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);
		$this->template->view('report/corporate_report_hotel', $page_data);
	}
	
	
	function b2c_hotel_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();

		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];

		/*if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			// if (empty($get_data['phone']) == false) {
			// 	$condition[] = array('BD.phone_number', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			// }
	
			// if (empty($get_data['email']) == false) {
			// 	$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			// }
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', 'like',$this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}*/
		//debug($this->session->userdata('id'));die;
		$total_records = $this->hotel_model->b2c_hotel_report($condition, true);	
	//	debug($total_records); die;
		$table_data = $this->hotel_model->b2c_hotel_report($condition, false, $offset, RECORDS_RANGE_2);
			//debug($table_data['data']); exit;
		$table_data = $this->booking_data_formatter->format_hotel_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2c_hotel_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2c_report_hotel', $page_data);
	}
	/*B2c sightseeing Report*/
	function b2c_sightseeing_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();

		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];

		$total_records = $this->sightseeing_model->b2c_sightseeing_report($condition, true);	
		
	//	debug($total_records); die;
		$table_data = $this->sightseeing_model->b2c_sightseeing_report($condition, false, $offset, RECORDS_RANGE_2);
			//debug($table_data['data']); exit;
		$table_data = $this->booking_data_formatter->format_sightseeing_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2c_sightseeing_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2c_report_sightseeing', $page_data);
	}
		/**
	 * Sightseeing Report for b2b flight
	 * @param $offset
	 */
	function b2b_sightseeing_report($offset=0)
	{
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];

		$total_records = $this->sightseeing_model->b2b_sightseeing_report($condition, true);
		//echo '<pre>'; print_r($page_data); die;
		$table_data = $this->sightseeing_model->b2b_sightseeing_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_sightseeing_booking_data($table_data, $this->current_module);
		// debug($table_data);
		// exit;
		$page_data['table_data'] = $table_data['data'];
		
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_sightseeing_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');

		$user_cond = [];
		$user_cond [] = array('U.user_type','=',' (', B2B_USER, ')');
		$user_cond [] = array('U.domain_list_fk' , '=' ,get_domain_auth_id());

		//$agent_info['data'] = $this->user_model->b2b_user_list($user_cond,false);

		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>B2B_USER,'domain_list_fk'=>get_domain_auth_id()));

		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);		
		
		$this->template->view('report/b2b_sightseeing', $page_data);
	}
	/*B2B Transfer Report*/
	function b2b_transfers_report($offset=0){
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];

		$total_records = $this->transferv1_model->b2b_transferv1_report($condition, true);
		//echo '<pre>'; print_r($page_data); die;
		$table_data = $this->transferv1_model->b2b_transferv1_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_transferv1_booking_data($table_data, $this->current_module);
		// debug($table_data);
		// exit;
		$page_data['table_data'] = $table_data['data'];
		
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_transfers_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');

		$user_cond = [];
		$user_cond [] = array('U.user_type','=',' (', B2B_USER, ')');
		$user_cond [] = array('U.domain_list_fk' , '=' ,get_domain_auth_id());

		//$agent_info['data'] = $this->user_model->b2b_user_list($user_cond,false);

		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>B2B_USER,'domain_list_fk'=>get_domain_auth_id()));

		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);		
		
		$this->template->view('report/b2b_transfer', $page_data);
	}
	/*B2c Transfer Report*/
	function b2c_transfers_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();

		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];

		$total_records = $this->transferv1_model->b2c_transferv1_report($condition, true);	
		
	//	debug($total_records); die;
		$table_data = $this->transferv1_model->b2c_transferv1_report($condition, false, $offset, RECORDS_RANGE_2);
			//debug($table_data['data']); exit;
		$table_data = $this->booking_data_formatter->format_transferv1_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2c_transfers_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2c_transferv1_report', $page_data);
	}
	/* car reports */
	
	function b2c_car_report($offset=0)
	{
		$get_data = $this->input->get();
        $condition = array();
        $page_data = array();
        $filter_data = $this->format_basic_search_filters('bus');
        $page_data['from_date'] = $filter_data['from_date'];
        $page_data['to_date'] = $filter_data['to_date'];
        $condition = $filter_data['filter_condition'];

        $total_records = $this->car_model->b2c_car_report($condition, true);
   
        $table_data = $this->car_model->b2c_car_report($condition, false, $offset, RECORDS_RANGE_2);
       
        $table_data = $this->booking_data_formatter->format_car_booking_datas($table_data , $this->current_module);
       	// debug($table_data);exit;
       	$page_data['table_data'] = $table_data['data'];
        
        /** TABLE PAGINATION */
        $this->load->library('pagination');
        if (count($_GET) > 0)
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['base_url'] = base_url() . 'index.php/report/car/';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $page_data['total_rows'] = $config['total_rows'] = $total_records;
        $config['per_page'] = RECORDS_RANGE_2;
        $this->pagination->initialize($config);
        /** TABLE PAGINATION */
        $page_data['total_records'] = $config['total_rows'];
        $page_data['customer_email'] = $this->entity_email;
       $page_data['search_params'] = $get_data;
        $page_data['status_options'] = get_enum_list('booking_status_options');
        $this->template->view('report/b2c_car_report', $page_data);
        

	}
	/* car reports  for B2B*/
	
	function b2b_car_report($offset=0)
	{
		$get_data = $this->input->get();
        $condition = array();
        $page_data = array();
        $filter_data = $this->format_basic_search_filters('bus');
        $page_data['from_date'] = $filter_data['from_date'];
        $page_data['to_date'] = $filter_data['to_date'];
        $condition = $filter_data['filter_condition'];

        $total_records = $this->car_model->b2b_car_report($condition, true);
   
        $table_data = $this->car_model->b2b_car_report($condition, false, $offset, RECORDS_RANGE_2);
       	// echo $this->current_module;exit;
        $table_data = $this->booking_data_formatter->format_car_booking_datas($table_data , $this->current_module);
       	// debug($table_data);exit;
       	$page_data['table_data'] = $table_data['data'];
        
        /** TABLE PAGINATION */
        $this->load->library('pagination');
        if (count($_GET) > 0)
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['base_url'] = base_url() . 'index.php/report/car/';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $page_data['total_rows'] = $config['total_rows'] = $total_records;
        $config['per_page'] = RECORDS_RANGE_2;
        $this->pagination->initialize($config);
        /** TABLE PAGINATION */
        $page_data['total_records'] = $config['total_rows'];
        $page_data['customer_email'] = $this->entity_email;
       $page_data['search_params'] = $get_data;
        $page_data['status_options'] = get_enum_list('booking_status_options');
        $this->template->view('report/b2c_car_report', $page_data);
        

	}
	
	/**
	 * Flight Report
	 * @param $offset
	 */
	function flight($offset=0)
	{
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}

			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}

			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}

			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}

			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}

			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->flight_model->booking($condition, true);
		$table_data = $this->flight_model->booking($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/flight/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/airline', $page_data);
	}
	
	/**
	 * Flight Report for b2c flight
	 * @param $offset
	 */
	function b2c_flight_report($offset=0)
	{
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		//debug($get_data); die;
		$condition = array();

		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		
		//$condition[] = array('U.user_type', '=', B2C_USER, ' OR ', 'BD.created_by_id');
		$total_records = $this->flight_model->b2c_flight_report($condition, true);		
		
		$table_data = $this->flight_model->b2c_flight_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, 'b2c', false);
		
		//Export report


		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2c_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2c_report_airline', $page_data);
	}
	

	
	/**
	 * Flight Report for b2b flight
	 * @param $offset
	 */
	function b2b_flight_report($offset=0)
	{
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];

		$total_records = $this->flight_model->b2b_flight_report($condition, true);
		//echo '<pre>'; print_r($page_data); die;
		$table_data = $this->flight_model->b2b_flight_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');

		$user_cond = [];
		$user_cond [] = array('U.user_type','=',' (', B2B_USER, ')');
		$user_cond [] = array('U.domain_list_fk' , '=' ,get_domain_auth_id());

		//$agent_info['data'] = $this->user_model->b2b_user_list($user_cond,false);

		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>B2B_USER,'domain_list_fk'=>get_domain_auth_id()));

		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);		

		// debug($page_data);
		// exit("810-report");
		
		$this->template->view('report/b2b_report_airline', $page_data);
	}
	
	function corporate_flight_report($offset=0)
	{
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		$filter_data = $this->format_basic_search_filters();
	
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		if ($this->check_operation($offset)) {
			$op_data = $this->flight_model->corporate_flight_report($condition);
			//$ref_no=$op_data['data']['booking_details'][0]['app_reference'];
			$op_data = $this->booking_data_formatter->format_flight_booking_data($op_data, $this->current_module);
			// debug($op_data); exit;
			$x = $op_data['data']['booking_details'];
			//debug($x);exit;
			$profit = 0;
			
			foreach ($x as $k => $v) {
				$get_count= $this->custom_db->single_table_records ( 'flight_booking_passenger_details','count(*)',array('app_reference'=> $v['app_reference']));
				$passenger_count = $get_count['data'][0]['count(*)'];
				$profit = (($v['conv_fee'] + $v['admin_markup'] + $v['net_commission'])-$v['net_commission_tds']);
				//coomison+convfee-tds
				//$profit = ($v['grand_total']-$v['agent_buying_price']);
				$x[$k]['corporate_profit'] = $profit;
				if($v['payment_mode']==PNHB1)
				{
					$pmode = 'Online';
				}
				else if($v['payment_mode']==PABHB3){
					$pmode = 'Wallet';
				}
				else {
				   $pmode = $v['payment_mode'];
				}
				$x[$k]['pmode'] = $pmode;
				$x[$k]['pcount'] = $passenger_count;
			}
			$col = array(
					
					'app_reference' => 'Application Reference',
					'status'        => 'Status',
					'agency_name'   => 'Corporate Name',
					 'corporate_id' => 'Corporate ID',
					'lead_pax_name' => 'Pax name',
					'lead_pax_phone_number' => 'Contact',
					'lead_pax_email' => 'Email',
					'from_loc' => 'From',
					'to_loc' => 'To',
					'trip_type_label' => 'Type',
					'booked_date' => 'Booked On',
					'journey_start' => 'Journey Date',
					'pnr' => 'PNR',
					'pmode' => 'Payment Mode',
					'pcount'  =>'No. of pax',
					'fare' => 'Comm.Fare',
					'admin_markup' => 'Admin Markup',
					'net_commission' => 'Commission',
					'net_commission_tds' => 'TDS',
					'conv_fee' => 'Convenience Fee',
					'net_fare' => 'Admin NetFare',
					'agent_buying_price' => 'Corporate NetFare',
					'promo_discount' => 'Discount',
					'grand_total' => 'Total Fare',
					'corporate_profit' => 'Admin Profit',
					
					
			);
			// debug($col);exit;
			$this->perform_operation($offset, $x, $col, 'Flight Booking Report');
		}
		$offset = intval($offset);
		$total_records = $this->flight_model->corporate_flight_report($condition, true);
		$table_data = $this->flight_model->corporate_flight_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/corporate_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
	
		$user_cond = [];
		$user_cond [] = array('U.user_type','=',' (', CORPORATE_USER, ')');
		$user_cond [] = array('U.domain_list_fk' , '=' ,get_domain_auth_id());
	
		//$agent_info['data'] = $this->user_model->b2b_user_list($user_cond,false);
	
		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>CORPORATE_USER,'domain_list_fk'=>get_domain_auth_id()));
	
		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);
		// debug($page_data);exit;
	
		$this->template->view('report/corporate_report_airline', $page_data);
	}
	
	
	function update_flight_booking_details($app_reference, $booking_source)
	{
		load_flight_lib($booking_source);
		$this->flight_lib->update_flight_booking_details($app_reference);
		//FIXME: Return the status
	}
	
	/**
	 * Sagar Wakchaure
	 *Update pnr Details 
	 * @param unknown $app_reference
	 * @param unknown $booking_source
	 * @param unknown $booking_status
	 */
	function update_pnr_details($app_reference, $booking_source,$booking_status)
	{
		load_flight_lib($booking_source);
		$response = $this->flight_lib->update_pnr_details($app_reference);
		$get_pnr_updated_status = $this->flight_model->update_pnr_details($response,$app_reference, $booking_source,$booking_status);
		echo $get_pnr_updated_status;
	}
	
function package()
	{
		echo '<h4>Under Working</h4>';
	}
	
	function b2b_package_report()
	{
		echo '<h4>Under Working</h4>';
	}
	
	function b2c_package_report()
	{
		echo '<h4>Under Working</h4>';
	}


	function b2b_package_enquiry(){
		// error_reporting(E_ALL);
		$data ['enquiries'] = $this->package_model->b2b_package_enquiry();
		$this->template->view ( 'suppliers/b2b_package_enquiry', $data );

	}

	function b2c_package_enquiry(){
		$data ['enquiries'] = $this->package_model->b2c_package_enquiry();
		$this->template->view ( 'suppliers/b2c_package_enquiry', $data );		
	}

	function corporate_package_enquiry(){
		$data ['enquiries'] = $this->package_model->corporate_package_enquiry();
		$this->template->view ( 'suppliers/corporate_package_enquiry', $data );
	}

	private function format_basic_search_filters($module='')
	{
		$get_data = $this->input->get();


		if(valid_array($get_data) == true) {
			$filter_condition = array();
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$filter_condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$filter_condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			/*if (empty($get_data['created_by_id']) == false) {
				$filter_condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}*/
			
			if (empty($get_data['created_by_id']) == false && strtolower($get_data['created_by_id'])!='all') {
				$filter_condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$filter_condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
		
			/*if (empty($get_data['phone']) == false) {
				$filter_condition[] = array('BD.phone', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$filter_condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}*/
			
			if($module == 'bus'){
					if (empty($get_data['pnr']) == false) {
					$filter_condition[] = array('BD.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
				}
			}else{
				if (empty($get_data['pnr']) == false) {
					$filter_condition[] = array('BT.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
				}
			}
			
	
			if (empty($get_data['app_reference']) == false) {
				$filter_condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;

			//Today's Booking Data
			if(isset($get_data['today_booking_data']) == true && empty($get_data['today_booking_data']) == false) {
				$filter_condition[] = array('DATE(BD.created_datetime)', '=', '"'.date('Y-m-d').'"');
			}
			//Last day Booking Data
			if(isset($get_data['last_day_booking_data']) == true && empty($get_data['last_day_booking_data']) == false) {
				$filter_condition[] = array('DATE(BD.created_datetime)', '=', '"'.trim($get_data['last_day_booking_data']).'"');
			}
			//Previous Booking Data: last 3 days, 7 days, 15 days, 1 month and 3 month
			if(isset($get_data['prev_booking_data']) == true && empty($get_data['prev_booking_data']) == false) {
				$filter_condition[] = array('DATE(BD.created_datetime)', '>=', '"'.trim($get_data['prev_booking_data']).'"');
			}
			
			return array('filter_condition' => $filter_condition, 'from_date' => $from_date, 'to_date' => $to_date);
		}
	}
	private function export_to_excel($file_name = "Excel", $data = array(), $col = array()) {
		$excel_data = array();
		$count = 0;
		if (valid_array($col)) {
			foreach ($col as $v) {
				if (is_array($v)) {
					$v = $v ['title'];
				}
	
				$excel_data [$count] [] = $v;
			}
			$count++;
	
			foreach ($data as $v) {
				foreach ($col as $k => $cv) {
					if (is_array($cv)) {
						$val = '';
						$sep = isset($cv ['sep']) ? $cv ['sep'] : ' ';
						foreach ($cv ['cols'] as $cv1) {
							$val .= $v [$cv1] . $sep;
						}
						$v [$k] = rtrim($val, $sep);
					}
					$excel_data [$count] [] = str_replace(array(
							"</br>",
							"</ br>",
							"<br>",
							"<br/>",
							"<br />"
					), "\r\n", $v [$k]);
				}
				$count++;
			}
		} else {
			$excel_data = $excel_data + $data;
		}
	
		// debug($excel_data);exit;
		$this->load->helper('csv');
		array_to_csv($excel_data, $file_name . '.csv');
		exit();
	}
	
	private function export_to_pdf($file_name = "Pdf", $data = array(), $col = array()) {
		$pdf_data = array();
		$count = 0;
		if (valid_array($col)) {
			foreach ($col as $v) {
				if (is_array($v)) {
					$v = $v ['title'];
				}
	
				$pdf_data ['head'] [] = $v;
			}
	
			foreach ($data as $v) {
				foreach ($col as $k => $cv) {
					if (is_array($cv)) {
						$val = '';
						$sep = isset($cv ['sep']) ? $cv ['sep'] : ' ';
						foreach ($cv ['cols'] as $cv1) {
							$val .= $v [$cv1] . $sep;
						}
						$v [$k] = rtrim($val, $sep);
					}
					$pdf_data ['body'] [$count] [] = $v [$k];
				}
				$count++;
			}
		} else {
			$pdf_data ['body'] = $data;
		}
		// debug($data);exit;
		$this->load->library('provab_pdf');
		$get_view = $this->template->isolated_view('report/table', $pdf_data);
		$this->provab_pdf->create_pdf($get_view, 'D', $file_name);
		exit();
	}
	
	private function check_operation($op) {
		return (strcmp($op, 'excel') == 0 || strcmp($op, 'pdf') == 0);
	}
	private function perform_operation($type, $data, $col = array(), $filename = '') {
		
		if (empty($filename)) {
			$filename = $this->uri->segment(2) . '_' . date('d-M-Y');
		}
		if (strcmp($type, 'excel') == 0) {
			// $this->export_to_excel($filename,$data, $col);exit;
			$this->load->library('excel');
	
			// $addn['row1']=array('merge'=>true,'data'=>'Ledger Report of '.$data_logs ['user']['uuid'].' From: '.app_friendly_date($from_date).' To: '.app_friendly_date($to_date));
			//debug($data);exit;
			$this->excel->array_to_excel($data, $col, $filename);
		} else if (strcmp($type, 'pdf') == 0) {
	
			$this->export_to_pdf($filename, $data, $col);
		}
		exit();
	}

	private function format_basic_search_filters_crs($module='')
	{
		$get_data = $this->input->get();

		$filter_condition = array();
		$from_date = '';
		$to_date = '';
		$search_arr = array();
		
		if(valid_array($get_data) == true) {
			
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$filter_condition[] = array('BH.book_date', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$filter_condition[] = array('BH.book_date', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			/*if (empty($get_data['created_by_id']) == false) {
				$filter_condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}*/
			
			if (empty($get_data['created_by_id']) == false && strtolower($get_data['created_by_id'])!='all') {
				$filter_condition[] = array('BG.user_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$filter_condition[] = array('BG.booking_status', '=', $this->db->escape($get_data['status']));
			}
		
			/*if (empty($get_data['phone']) == false) {
				$filter_condition[] = array('BD.phone', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$filter_condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}*/
			
			
				if (empty($get_data['pnr']) == false) {
					$filter_condition[] = array('BG.pnr_no', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
				}
			
			
	
			if (empty($get_data['app_reference']) == false) {
				$filter_condition[] = array('BG.parent_pnr', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;

			//Today's Booking Data
			if(isset($get_data['today_booking_data']) == true && empty($get_data['today_booking_data']) == false) {
				$filter_condition[] = array('DATE(BH.book_date)', '=', '"'.date('Y-m-d').'"');
			}
			//Previous Booking Data: last 3 days, 7 days, 15 days, 1 month and 3 month
			if(isset($get_data['prev_booking_data']) == true && empty($get_data['prev_booking_data']) == false) {
				$filter_condition[] = array('DATE(BH.book_date)', '>=', '"'.trim($get_data['prev_booking_data']).'"');
			}
			
			
		}	
		$search_arr = array(
				'filter_condition' => $filter_condition,
				'from_date' => $from_date,
				'to_date' => $to_date
		);	
		return $search_arr;
	}

	function b2c_crs_hotel_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();

		// print_r($get_data);
		// exit("2095-report");


		$filter_data = $this->format_basic_search_filters_crs();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		//debug($condition);exit;
		$status_option_array = Array
        (
            'CONFIRM'=>"BOOKING_CONFIRMED",
            'PROCESS'=>"BOOKING_INPROGRESS" ,
            'HOLD'=>"BOOKING_HOLD",
            'CANCELLED'=>"BOOKING_CANCELLED",
            'PENDING'=>"BOOKING_PENDING",
            'FAILED'=>"BOOKING_FAILED" 
        );

		
		//debug($this->session->userdata('id'));die;
		$total_records = $this->hotels_model->b2c_crs_hotel_report($condition, true);	
	    //debug($total_records); die;
		$table_data = $this->hotels_model->b2c_crs_hotel_report($condition, false, $offset, RECORDS_RANGE_1);
		//debug($table_data['data']); exit;
		
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2c_crs_hotel_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_1;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = $status_option_array;

$page_data['payment_currency']=$page_data['table_data']['booking_details'][0];
//debug($page_data['payment_currency']);exit;

								 // $curr=json_decode($page_data['payment_currency'],1);
								 // debug($curr);die;
								  $page_data['payment_currency']=$page_data['payment_currency']['admin_currency'];
								  //debug($page_data['payment_currency']);die;
		
		$this->template->view('report/b2c_crs_hotel_report', $page_data);
	}

	function b2c_villa_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();

		// print_r($get_data);
		// exit("2095-report");


		$filter_data = $this->format_basic_search_filters_crs();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		//debug($condition);exit;
		$status_option_array = Array
        (
            'CONFIRM'=>"BOOKING_CONFIRMED",
            'PROCESS'=>"BOOKING_INPROGRESS" ,
            'HOLD'=>"BOOKING_HOLD",
            'CANCELLED'=>"BOOKING_CANCELLED",
            'PENDING'=>"BOOKING_PENDING",
            'FAILED'=>"BOOKING_FAILED" 
        );

		
		//debug($this->session->userdata('id'));die;
		$total_records = $this->hotels_model->b2c_villa_report($condition, true);	
	    //debug($total_records); die;
		$table_data = $this->hotels_model->b2c_villa_report($condition, false, $offset, RECORDS_RANGE_1);
		//debug($table_data['data']); exit;
		
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2c_villa_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_1;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = $status_option_array;

$page_data['payment_currency']=$page_data['table_data']['booking_details'][0];
//debug($page_data['payment_currency']);exit;

								 // $curr=json_decode($page_data['payment_currency'],1);
								 // debug($curr);die;
								  $page_data['payment_currency']=$page_data['payment_currency']['admin_currency'];
								  //debug($page_data['payment_currency']);die;
		
		$this->template->view('report/b2c_villa_report', $page_data);
	}

	function b2b_crs_hotel_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();
		$status_option_array = Array
        (
            'CONFIRM'=>"BOOKING_CONFIRMED",
            'PROCESS'=>"BOOKING_INPROGRESS" ,
            'HOLD'=>"BOOKING_HOLD",
            'CANCELLED'=>"BOOKING_CANCELLED",
            'PENDING'=>"BOOKING_PENDING",
            'FAILED'=>"BOOKING_FAILED" 
        );

		$filter_data = $this->format_basic_search_filters_crs();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		//debug($condition);exit;
		
		$total_records = $this->hotels_model->b2b_crs_hotel_report($condition, true);
		$table_data = $this->hotels_model->b2b_crs_hotel_report($condition, false, $offset, RECORDS_RANGE_1);
		
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_crs_hotel_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_1;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = $status_option_array;
		
		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>B2B_USER,'domain_list_fk'=>get_domain_auth_id()));
		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);
		//debug($page_data);exit;
		$this->template->view('report/b2b_crs_report_hotel.php', $page_data);
	}

	function b2b_villa_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();
		$status_option_array = Array
        (
            'CONFIRM'=>"BOOKING_CONFIRMED",
            'PROCESS'=>"BOOKING_INPROGRESS" ,
            'HOLD'=>"BOOKING_HOLD",
            'CANCELLED'=>"BOOKING_CANCELLED",
            'PENDING'=>"BOOKING_PENDING",
            'FAILED'=>"BOOKING_FAILED" 
        );

		$filter_data = $this->format_basic_search_filters_crs();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		//debug($condition);exit;
		
		$total_records = $this->hotels_model->b2b_villa_report($condition, true);
		$table_data = $this->hotels_model->b2b_villa_report($condition, false, $offset, RECORDS_RANGE_1);
		
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_villa_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_1;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = $status_option_array;
		
		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>B2B_USER,'domain_list_fk'=>get_domain_auth_id()));
		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);
		//debug($page_data);exit;
		$this->template->view('report/b2b_villa_report.php', $page_data);
	}


	function corporate_sightseeing_report($offset=0)
	{
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];

		$total_records = $this->sightseeing_model->corporate_sightseeing_report($condition, true);
		//echo '<pre>'; print_r($page_data); die;
		$table_data = $this->sightseeing_model->corporate_sightseeing_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_sightseeing_booking_data($table_data, $this->current_module);
		// debug($table_data);
		// exit;
		$page_data['table_data'] = $table_data['data'];
		
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/corporate_sightseeing_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');

		$user_cond = [];
		$user_cond [] = array('U.user_type','=',' (', CORPORATE_USER, ')');
		$user_cond [] = array('U.domain_list_fk' , '=' ,get_domain_auth_id());

		//$agent_info['data'] = $this->user_model->b2b_user_list($user_cond,false);

		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>CORPORATE_USER,'domain_list_fk'=>get_domain_auth_id()));

		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);		
		
		$this->template->view('report/corporate_report_sightseeing', $page_data);
	}

	function corporate_bus_report($offset=0)
	{
		//error_reporting(E_ALL);
		//echo "hi";exit();
		$get_data = $this->input->get();
		$condition = array();
		$page_data = array();

		$filter_data = $this->format_basic_search_filters('bus');
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		if ($this->check_operation($offset)) {
            $op_data = $this->bus_model->corporate_bus_report($condition);
         //debug( $op_data );exit();
            $x = $op_data['data']['booking_details'];
            foreach ($op_data['data']['booking_itinerary_details'] as $key => $value) {
             $val=json_decode($op_data['data']['booking_itinerary_details'][$key]['attributes'],true);
             
             $x[$key]['api_total_basic_fare']=$val['api_total_basic_fare'];
			 $x[$key]['api_total_tax']=$val['api_total_tax'];
			 $x[$key]['admin_markup']=$val['admin_markup'];
			 $x[$key]['tds']=$val['tds'];
			 $x[$key]['service_tax']=$val['service_tax'];
			 $x[$key]['agent_buying_price']=$val['agent_buying_price'];	
			 $x[$key]['commission']=$val['commission'];
           	 $x[$key]['operator']=$value['operator'];
           	 $x[$key]['journey_datetime']=$value['journey_datetime'];
           	 $x[$key]['departure_from']=$value['departure_from'];
           	 $x[$key]['arrival_to']=$value['arrival_to'];
           	 $x[$key]['bus_type']=$value['bus_type'];
            }
           // debug($val);exit();
            $app_references=[];
            $i=0;

            foreach ($op_data['data']['booking_customer_details'] as $akey => $value) {
            	//debug($x);
            	//debug($op_data['data']['booking_customer_details']);exit;
            		$a_ref=$value['app_reference'];
            		if($app_references[$a_ref]!='true')
            		{

                     $x[$i]['name']=$op_data['data']['booking_customer_details'][$akey]['name'];
            		 $app_references[$a_ref]='true';
            		$i++;
            		}	  

                }
          
                // debug($x);exit;
       
            foreach ($x as $k => $v) {
				//$get_count= $this->custom_db->single_table_records ( 'flight_booking_passenger_details','count(*)',array('app_reference'=> $v['app_reference']));
				//$passenger_count = $get_count['data'][0]['count(*)'];
				//$profit = (($v['conv_fee'] + $v['admin_markup'] + $v['net_commission'])-$v['net_commission_tds']);
				//coomison+convfee-tds
				//$profit = ($v['grand_total']-$v['agent_buying_price']);
				$x[$k]['corporate_profit'] = $profit;
				$x[$k]['pnr'] =$v['pnr'];
				$x[$k]['phone_number']=$v['phone_number'];
				if($v['payment_mode']==PNHB1)
				{
					$pmode = 'Online';
				}
				else if($v['payment_mode']==PABHB3){
					$pmode = 'Wallet';
				}
				else {
				   $pmode = $v['payment_mode'];
				}
				$x[$k]['pmode'] = $pmode;
				$x[$k]['pcount'] = $passenger_count;
			}
$col = array(
					
				'app_reference' => 'Application Reference',
                'operator' => 'Operator',
                'phone_number' => 'Phone',
                'agency_name' => 'Agency',
                'status' => 'Status',
                'pnr' => 'PNR',
                'name' => 'Passenger Name',
                'journey_datetime' => 'Travel Date',
                'api_total_basic_fare' => 'Base Fare',
                'api_total_tax' => 'Tax',
                'admin_markup' => 'Markup',
                //'ag_markup' => 'Agent Markup',
                'commission' => 'Agent Commission',
                'tds' => 'Agent TDS',
                //'AgentNetRate' => 'Agent Rate',
                'service_tax' => 'Service Tax',
                'agent_buying_price' => 'Total',
                'created_datetime' => 'BookedOn',
                'loc' => array(
                    'title' => 'Trip Location',
                    'cols' => array(
                        'departure_from',
                        'arrival_to'
                    ),
                    'sep' => '-'
                ),
                'bus_type' => 'Bus Type'
			);
//debug($col);exit;
            $this->perform_operation($offset, $x, $col, 'Bus Booking report');
        }
        $offset = intval($offset);
        $this->load->library('pagination');

		//debug($condition); die;
		$total_records = $this->bus_model->corporate_bus_report($condition, true);
		$table_data = $this->bus_model->corporate_bus_report($condition, false, $offset, RECORDS_RANGE_2);
		// debug($table_data);exit;
		$table_data = $this->booking_data_formatter->format_bus_booking_data($table_data,'corporate');
		$page_data['table_data'] = $table_data['data'];
		/** TABLE PAGINATION */
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/corporate_bus_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['customer_email'] = $this->entity_email;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');

		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>CORPORATE_USER,'domain_list_fk'=>get_domain_auth_id()));
		
		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);
// debug($page_data);exit;
		$this->template->view('report/corporate_report_bus', $page_data);
	}

	function corporate_package_report()
	{
		//error_reporting(E_ALL);
		//echo '<h4>Under Working</h4>';
		//$data ['enquiries'] = $this->supplierpackage_model->enquiries ();
		//$this->template->view ( 'suppliers/enquiries', $data );
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		//debug($get_data); die;
		$condition = array();

		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		$total_records =$this->package_model->corporate_package_report($condition, true);		
		
		$table_data = $this->package_model->corporate_package_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_package_booking_data($table_data, 'corporate', false);
		
		//Export report


		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/corporate_package_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/corporate_report_package', $page_data);
	}

	/*Corporate Transfer Report*/
	function corporate_transfers_report($offset=0){
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		$filter_data = $this->format_basic_search_filters();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];

		$total_records = $this->transferv1_model->corporate_transferv1_report($condition, true);
		// debug($total_records);
		// echo $this->db->last_query();die;
		//echo '<pre>'; print_r($page_data); die;
		$table_data = $this->transferv1_model->corporate_transferv1_report($condition, false, $offset, RECORDS_RANGE_2);
		// debug($table_data);die;
		
		// echo $this->db->last_query();die;
		$table_data = $this->booking_data_formatter->format_transferv1_booking_data($table_data, $this->current_module);
		// debug($table_data);
		// exit;
		$total_records=count($table_data['data']['booking_details']);
		$page_data['table_data'] = $table_data['data'];
		
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/corporate_transfers_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		// debug($config);die;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');

		$user_cond = [];
		$user_cond [] = array('U.user_type','=',' (', CORPORATE_USER, ')');
		$user_cond [] = array('U.domain_list_fk' , '=' ,get_domain_auth_id());

		//$agent_info['data'] = $this->user_model->b2b_user_list($user_cond,false);

		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>CORPORATE_USER,'domain_list_fk'=>get_domain_auth_id()));

		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);		
		
		// debug($page_data);die;
		$this->template->view('report/corporate_transfer', $page_data);
	}

	/*Corporate Hotel CRS Report*/

	function corporate_crs_hotel_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();
		$status_option_array = Array
        (
            'CONFIRM'=>"BOOKING_CONFIRMED",
            'PROCESS'=>"BOOKING_INPROGRESS" ,
            'HOLD'=>"BOOKING_HOLD",
            'CANCELLED'=>"BOOKING_CANCELLED",
            'PENDING'=>"BOOKING_PENDING",
            'FAILED'=>"BOOKING_FAILED" 
        );

		$filter_data = $this->format_basic_search_filters_crs();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		//debug($condition);exit;
		
		$total_records = $this->hotels_model->corporate_crs_hotel_report($condition, true);
		$table_data = $this->hotels_model->corporate_crs_hotel_report($condition, false, $offset, RECORDS_RANGE_1);
		
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/corporate_crs_hotel_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_1;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = $status_option_array;
		
		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>CORPORATE_USER,'domain_list_fk'=>get_domain_auth_id()));
		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);
		//debug($page_data);exit;
		$this->template->view('report/corporate_crs_report_hotel', $page_data);
	}
	
	/*Corporate Villa Report*/

	function corporate_villa_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();
		$status_option_array = Array
        (
            'CONFIRM'=>"BOOKING_CONFIRMED",
            'PROCESS'=>"BOOKING_INPROGRESS" ,
            'HOLD'=>"BOOKING_HOLD",
            'CANCELLED'=>"BOOKING_CANCELLED",
            'PENDING'=>"BOOKING_PENDING",
            'FAILED'=>"BOOKING_FAILED" 
        );

		$filter_data = $this->format_basic_search_filters_crs();
		$page_data['from_date'] = $filter_data['from_date'];
		$page_data['to_date'] = $filter_data['to_date'];
		$condition = $filter_data['filter_condition'];
		//debug($condition);exit;
		
		$total_records = $this->hotels_model->corporate_villa_report($condition, true);
		$table_data = $this->hotels_model->corporate_villa_report($condition, false, $offset, RECORDS_RANGE_1);
		
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/corporate_villa_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_1;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = $status_option_array;
		
		$agent_info = $this->custom_db->single_table_records('user','*',array('user_type'=>CORPORATE_USER,'domain_list_fk'=>get_domain_auth_id()));
		$page_data['agent_details'] = magical_converter(array('k' => 'user_id', 'v' => 'agency_name'), $agent_info);
		//debug($page_data);exit;
		$this->template->view('report/corporate_villa_report', $page_data);
	}
	
}


