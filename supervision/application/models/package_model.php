<?php
class Package_Model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
	public function getAllPackages(){
		$this->db->select('*');
		$this->db->where('status', '1');
		$query = $this->db->get('package');
		if ( $query->num_rows > 0 ) {
			return $query->result();
		}else{
			return array();
		}
	}

	/**
	 *@param Top Destination Packages
	 */
	public function get_package_top_destination()
	{
		$this->db->select('*');
		$this->db->where('top_destination',ACTIVE);
		$query = $this->db->get('package');
		if ( $query->num_rows > 0 ) {
			$data['data'] = $query->result();
			$data['total'] = $query->num_rows;
			return $data;
		}else{
			return array('data' => '', 'total' => 0);
		}
	}
	public function getPageCaption($page_name) {
		$this->db->where('page_name', $page_name);
		return $this->db->get('page_captions');
	}
	public function get_contact(){
		$contact = $this->db->get('contact_details');
		return $contact->row();
	}
	/**
	 *get country name
	 **/
	public function getCountryName($id){
		$this->db->select("*");
		$this->db->from("country");
		$this->db->where('country_id',$id);
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->row();
		}else{
			return array();
		}
	}

	/**
	 * get package itinerary
	 */
	public function getPackageItinerary($package_id){
		$this->db->select("*");
		$this->db->from("package_itinerary");
		$this->db->where('package_id',$package_id);
		$this->db->order_by('day','ASC');
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->result();
		}else{
			return array();
		}
	}

	/**
	 * get package pricing policy
	 */
	public function getPackagePricePolicy($package_id){
		$this->db->select("*");
		$this->db->from("package_pricing_policy");
		$this->db->where('package_id',$package_id);
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->row();
		}else{
			return array();
		}
	}

	/**
	 * get package traveller photos
	 */
	public function getTravellerPhotos($package_id){
		$this->db->select("*");
		$this->db->from("package_traveller_photos");
		$this->db->where('package_id',$package_id);
		$this->db->where('status','1');
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->result();
		}else{
			return array();
		}
	}
	/*8
	 * get getPackageCancelPolicy
	 */
	public function getPackageCancelPolicy($package_id){
		$this->db->select("*");
		$this->db->from("package_cancellation");
		$this->db->where('package_id',$package_id);
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->row();
		}else{
			return array();
		}
	}
	/**
	 * getPackage
	 */
	public function getPackage($package_id){
		$this->db->select("*");
		$this->db->from("package");
		$this->db->where('package_id',$package_id);
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->row();
		}else{
			return array();
		}
	}

	public function saveEnquiry($data){
		$this->db->insert('package_enquiry',$data);
		return $this->db->insert_id();
	}

	public function getPackageCountries(){
		$data = 'select Distinct package_country, C.name AS country_name FROM package P, country C WHERE P.package_country=C.country_id';
    	return $this->db->query($data)->result();
	}
	public function getPackageTypes(){
		$this->db->select("*");
		$this->db->from("package_types");
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->result();
		}else{
			return array();
		}
	}
	public function search($c,$p,$d,$b,$dmn_list_fk){
		$this->db->select("*");
		$this->db->from("package");
		$this->db->like('package_country', $c,'both');
		$this->db->like('package_type', $p,'both');
		if($d){
			$this->db->where($d);
		}else{
			$this->db->like('duration', $d,'both');
		}
		if($b){
			$this->db->where($b);
		}else{
			$this->db->like('price', $b,'both');
		}
		$this->db->where('domain_list_fk',$dmn_list_fk);
		$query=$this->db->get();
		//echo $this->db->last_query();
		//exit;
		if($query->num_rows()){
			return $query->result();
		}else{
			return array();
		}
	}


	function add_user_rating($arr_data)
	{
		$pkg_id=$arr_data['package_id'];
		$res=$this->db->insert('package_rating',$arr_data);

		if($res==true){
			 
			$this->db->select('rating');
			$this->db->where('package_id',$pkg_id);
			$res1=$this->db->get('package_rating');
			if($res1->num_rows()>0)
			{  // print_r($res1);
				$tot_no=count($res1->result());
				$results=$res1->result();
				//   sum=0;
				foreach($results as $r)
				{
					$sum+=$r->rating;
				}
				$rating=$sum/$tot_no;

				$da=array('rating'=> ceil($rating));
				$this->db->where('package_id',$pkg_id);
				$this->db->update('package',$da);

			}
			 
		}

	}

	function save_package_booking_transaction_details(
	$app_reference, $transaction_status, $status_description, $pnr, $book_id, $source, $ref_id, $attributes,
	 $currency, $total_fare)
	{
		$data['app_reference'] = $app_reference;
		$data['status'] = $transaction_status;
		$data['status_description'] = $status_description;
		$data['pnr'] = $pnr;
		$data['book_id'] = $book_id;
		$data['source'] = $source;
		$data['ref_id'] = $ref_id;
		$data['attributes'] = $attributes;
		

		$data['total_fare'] = $total_fare;
		
		$data['currency'] = $currency;

		
		
		return $this->custom_db->insert_record('package_booking_transaction_details', $data);
	}


	function save_package_booking_passenger_details(
	$app_reference, $passenger_type, $is_lead, $first_name,$last_name,
	$gender, $passenger_nationality, $status,
	$attributes, $flight_booking_transaction_details_fk)
	{
		$data['app_reference'] = $app_reference;
		$data['passenger_type'] = $passenger_type;
		$data['is_lead'] = $is_lead;
		
		$data['first_name'] = $first_name;
		
		$data['last_name'] = $last_name;
		
		$data['gender'] = $gender;
		$data['passenger_nationality'] = $passenger_nationality;
		
		$data['status'] = $status;
		$data['attributes'] = $attributes;
		$data['flight_booking_transaction_details_fk'] = $flight_booking_transaction_details_fk;
		return $this->custom_db->insert_record('package_booking_passenger_details', $data);
	}

	function save_package_booking_details(
	$domain_origin, $status, $app_reference, $booking_source, $phone, $alternate_number, $email,$payment_mode,	$attributes, $created_by_id, 
	$transaction_currency, $currency_conversion_rate,$pack_id)
	{
		$data['domain_origin'] = $domain_origin;
		$data['status'] = $status;
		$data['app_reference'] = $app_reference;
		$data['booking_source'] = $booking_source;
		$data['phone'] = $phone;
		$data['package_type'] = $pack_id;
		
		$data['email'] = $email;
		
		$data['payment_mode'] = $payment_mode;
		$data['attributes'] = $attributes;
		$data['created_by_id'] = $created_by_id;
		$data['created_datetime'] = date('Y-m-d H:i:s');

		
		
		$data['currency'] = $transaction_currency;
		$data['currency_conversion_rate'] = $currency_conversion_rate;
		$this->custom_db->insert_record('package_booking_details', $data);
	}


	public function process_booking($book_id, $booking_params)
	{

		//debug($book_id); exit;

		$response['status'] ='BOOKING_CONFIRMED';
		$condition = array();
		$condition['app_reference']=$book_id;
		$this->custom_db->update_record('package_booking_details',$response,$condition);
		$this->custom_db->update_record('package_booking_transaction_details', $response, $condition);
		$this->custom_db->update_record('package_booking_passenger_details', $response, $condition);

		$response['data']['book_id'] = $book_id;
		$response['data']['booking_params'] = $booking_params;
		$response['status'] = $response['status'];
		//debug($response); exit;
		return $response;
	}

	function get_booking_details($app_reference, $booking_source='', $booking_status='')
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		//Booking Details
		$bd_query = 'select * from package_booking_details AS BD
		   WHERE BD.app_reference like '.$this->db->escape($app_reference);
		if (empty($booking_source) == false) {
			$bd_query .= '	AND BD.booking_source = '.$this->db->escape($booking_source);
		}
		if (empty($booking_status) == false) {
			$bd_query .= ' AND BD.status = '.$this->db->escape($booking_status);
		}
		//Itinerary Details
		
		$td_query = 'select * from package_booking_transaction_details
 AS CD WHERE CD.app_reference='.$this->db->escape($app_reference).'  order by origin asc';
		//Customer and Ticket Details
		$cd_query = 'select CD.*
						from package_booking_passenger_details CD

						
						WHERE CD.flight_booking_transaction_details_fk IN 
						(select TD.origin from package_booking_transaction_details AS TD 
						WHERE TD.app_reference ='.$this->db->escape($app_reference).') order by origin asc';
		//Cancellation Details
		

		$response['data']['booking_details']			= $this->db->query($bd_query)->result_array();
		
		$response['data']['booking_transaction_details']	= $this->db->query($td_query)->result_array();
		$response['data']['booking_customer_details']	= $this->db->query($cd_query)->result_array();
		
		if (valid_array($response['data']['booking_details']) == true and valid_array($response['data']['booking_customer_details']) == true) {
			$response['status'] = SUCCESS_STATUS;
		}
		return $response;
	}

	function booking($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) AS total_records from package_booking_details BD
					where domain_origin='.get_domain_auth_id().' '.$condition;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			//Booking Details
			$bd_query = 'select * from package_booking_details AS BD
						WHERE BD.domain_origin='.get_domain_auth_id().' '.$condition.' AND BD.created_by_id ='.$GLOBALS['CI']->entity_user_id.'
						order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;
			$booking_details	= $this->db->query($bd_query)->result_array();
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				//Itinerary Details
				
				//Transaction Details
				$td_query = 'select * from package_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.') ';
				//Customer and Ticket Details
				$cd_query = 'select CD.*
						from package_booking_passenger_details CD

						
						WHERE CD.flight_booking_transaction_details_fk IN 
							(select TD.origin from package_booking_transaction_details AS TD 
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//Cancellation Details
				
				
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$booking_transaction_details = $this->db->query($td_query)->result_array();
				
			}

			//debug($booking_transaction_details);
				
			$response['data']['booking_details']= $booking_details;
			
			$response['data']['booking_transaction_details']= $booking_transaction_details;
			$response['data']['booking_customer_details']= $booking_customer_details;

			

			//debug($response);
			
			return $response;
		}
	}


	function b2c_package_report($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		error_reporting(0);
		$condition = $this->custom_db->get_custom_condition($condition);
		
		if ($count) {
			
			//echo debug($condition);exit;
			$query = 'select count(distinct(BD.app_reference)) AS total_records from package_booking_details BD
					where domain_origin='.get_domain_auth_id().' AND BD.created_by_id ='.$GLOBALS['CI']->entity_user_id.' '.$condition;
			//echo debug($query);exit;
			
			$data = $this->db->query($query)->row_array();
			
			return $data['total_records'];

		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			$payment_details = array();
			//Booking Details
			$bd_query = 'select * from package_booking_details AS BD
						WHERE BD.domain_origin='.get_domain_auth_id().' '.$condition.' 	AND(BD.created_by_id in (select user_id from user where user_type = 4 ) OR BD.created_by_id in (0)) order by BD.created_datetime desc, BD.origin desc';	

						 
						 
			$booking_details	= $this->db->query($bd_query)->result_array();
			//echo debug($bd_query); exit;
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				//Itinerary Details
				
				$td_query = 'select * from package_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.') ';
				//Customer and Ticket Details
				$cd_query = 'select CD.*
						from package_booking_passenger_details CD

						
						WHERE CD.flight_booking_transaction_details_fk IN 
							(select TD.origin from package_booking_transaction_details AS TD 
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//$payment_details_query = '';
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$booking_transaction_details = $this->db->query($td_query)->result_array();
			}
	
			$response['data']['booking_details']= $booking_details;
			
			$response['data']['booking_transaction_details']= $booking_transaction_details;
			$response['data']['booking_customer_details']= $booking_customer_details;
			//$response['data']['payment_details']	= $payment_details;
			return $response;
		}
	}


	function b2b_package_report($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) AS total_records from package_booking_details BD
			    join user U on U.user_id = BD.created_by_id
					 WHERE  
					 U.user_type='.B2B_USER.' AND BD.domain_origin='.get_domain_auth_id().' '.$condition.'
						 order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			//Booking Details

			$bd_query = 'select distinct(BD.app_reference),BD.*,U.agency_name,U.first_name,U.last_name from package_booking_details AS BD
					      join user U on U.user_id = BD.created_by_id  				      
						  WHERE  U.user_type='.B2B_USER.' AND BD.domain_origin='.get_domain_auth_id().' '.$condition.'
						  order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;

			
			$booking_details	= $this->db->query($bd_query)->result_array();
			//debug($booking_details); 
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				//Itinerary Details
				
				//Transaction Details
				$td_query = 'select * from package_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.') ';
				//Customer and Ticket Details
				$cd_query = 'select CD.*
						from package_booking_passenger_details CD

						
						WHERE CD.flight_booking_transaction_details_fk IN 
							(select TD.origin from package_booking_transaction_details AS TD 
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//Cancellation Details
				
				
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$booking_transaction_details = $this->db->query($td_query)->result_array();
				
			}

			//debug($booking_transaction_details);
				
			$response['data']['booking_details']= $booking_details;
			
			$response['data']['booking_transaction_details']= $booking_transaction_details;
			$response['data']['booking_customer_details']= $booking_customer_details;

			

			//debug($response);
			
			return $response;
		}
	}	
	function corporate_package_report($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		error_reporting(0);
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) AS total_records from package_booking_details BD
			    join user U on U.user_id = BD.created_by_id
					 WHERE  
					 U.user_type='.CORPORATE_USER.' AND BD.domain_origin='.get_domain_auth_id().' '.$condition.'
						 order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			//Booking Details

			$bd_query = 'select distinct(BD.app_reference),BD.*,U.agency_name,U.first_name,U.last_name from package_booking_details AS BD
					      join user U on U.user_id = BD.created_by_id  				      
						  WHERE  U.user_type='.CORPORATE_USER.' AND BD.domain_origin='.get_domain_auth_id().' '.$condition.'
						  order by BD.created_datetime desc, BD.origin desc';

			
			$booking_details	= $this->db->query($bd_query)->result_array();
			//debug($booking_details); 
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				//Itinerary Details
				
				//Transaction Details
				$td_query = 'select * from package_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.') ';
				//Customer and Ticket Details
				$cd_query = 'select CD.*
						from package_booking_passenger_details CD

						
						WHERE CD.flight_booking_transaction_details_fk IN 
							(select TD.origin from package_booking_transaction_details AS TD 
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//Cancellation Details
				
				
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$booking_transaction_details = $this->db->query($td_query)->result_array();
				
			}

			//debug($booking_transaction_details);
				
			$response['data']['booking_details']= $booking_details;
			
			$response['data']['booking_transaction_details']= $booking_transaction_details;
			$response['data']['booking_customer_details']= $booking_customer_details;

			

			//debug($response);
			
			return $response;
		}
	}	
function get_monthly_booking_summary($condition=array())
	{
		//Jaganath
		$condition = $this->custom_db->get_custom_condition($condition);
		$query = 'select count(distinct(BD.app_reference)) AS total_booking, 
				PBTD.total_fare as monthly_payment, 
				MONTH(BD.created_datetime) as month_number 
				from package_booking_details AS BD
				join  package_booking_transaction_details AS PBTD on BD.app_reference=PBTD.app_reference
				where (YEAR(BD.created_datetime) BETWEEN '.date('Y').' AND '.date('Y', strtotime('+1 year')).')  and BD.domain_origin='.get_domain_auth_id().' '.$condition.'
				GROUP BY YEAR(BD.created_datetime), 
				MONTH(BD.created_datetime)';
		return $this->db->query($query)->result_array();
	}

	function b2b_package_enquiry(){
		$query = "SELECT * from package_enquiry WHERE user_type =".B2B_USER."
		";
		return $this->db->query($query)->result_array();

	}

	function b2c_package_enquiry(){
		$query = "SELECT * from package_enquiry WHERE user_type =".B2C_USER." OR user_type ='0'
		";
		return $this->db->query($query)->result_array();

	}
	function corporate_package_enquiry(){
		$query = "SELECT * from package_enquiry WHERE user_type =".CORPORATE_USER." OR user_type=". EMPLOYEE_USER."
		";
		return $this->db->query($query)->result_array();

	}

}
