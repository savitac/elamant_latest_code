<?php
/**
 * Library which has generic functions to get data
 *
 * @package    Provab Application
 * @subpackage Flight Model
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class Flight_Model extends CI_Model
{
	/**
	 *TEMPORARY FUNCTION NEEDS TO BE CLEANED UP IN PRODUCTION ENVIRONMENT
	 */
	function get_static_response($token_id)
	{
		$static_response = $this->custom_db->single_table_records('test', '*', array('origin' => intval($token_id)));
		return json_decode($static_response['data'][0]['test'], true);
	}
	/**
	 * Flight booking report
	 *
	 */
	function booking($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) AS total_records from flight_booking_details BD
					where domain_origin='.get_domain_auth_id().''.$condition;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			$payment_details = array();
			//Booking Details
			$bd_query = 'select * from flight_booking_details AS BD
						WHERE BD.domain_origin='.get_domain_auth_id().' '.$condition.'
						order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;
			$booking_details	= $this->db->query($bd_query)->result_array();
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				//Itinerary Details
				$id_query = 'select * from flight_booking_itinerary_details AS ID
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				//Transaction Details
				$td_query = 'select * from flight_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.')';
				//Customer and Ticket Details
				$cd_query = 'select CD.*,FPTI.TicketId,FPTI.TicketNumber,FPTI.IssueDate,FPTI.Fare,FPTI.SegmentAdditionalInfo
							from flight_booking_passenger_details AS CD
							left join flight_passenger_ticket_info FPTI on CD.origin=FPTI.passenger_fk
							WHERE CD.flight_booking_transaction_details_fk IN 
							(select TD.origin from flight_booking_transaction_details AS TD 
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//Cancellation Details
				$cancellation_details_query = 'select FCD.*
						from flight_booking_passenger_details AS CD
						left join flight_cancellation_details AS FCD ON FCD.passenger_fk=CD.origin
						WHERE CD.flight_booking_transaction_details_fk IN 
						(select TD.origin from flight_booking_transaction_details AS TD 
						WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//$payment_details_query = '';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$booking_transaction_details = $this->db->query($td_query)->result_array();
				$cancellation_details = $this->db->query($cancellation_details_query)->result_array();
				//$payment_details = $this->db->query($payment_details_query)->result_array();
			}
				
			$response['data']['booking_details']			= $booking_details;
			$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			$response['data']['booking_transaction_details']	= $booking_transaction_details;
			$response['data']['booking_customer_details']	= $booking_customer_details;
			$response['data']['cancellation_details']	= $cancellation_details;
			//$response['data']['payment_details']	= $payment_details;
			return $response;
		}
	}
	/**
	 * Read Individual booking details - dont use it to generate table
	 * @param $app_reference
	 * @param $booking_source
	 * @param $booking_status
	 */
	function get_booking_details($app_reference, $booking_source='', $booking_status='')
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		//Booking Details
		$bd_query = 'select * from flight_booking_details AS BD WHERE BD.app_reference like '.$this->db->escape($app_reference);
		if (empty($booking_source) == false) {
			$bd_query .= '	AND BD.booking_source = '.$this->db->escape($booking_source);
		}
		if (empty($booking_status) == false) {
			$bd_query .= ' AND BD.status = '.$this->db->escape($booking_status);
		}
		//Itinerary Details
		$id_query = 'select * from flight_booking_itinerary_details AS ID WHERE ID.app_reference='.$this->db->escape($app_reference).' order by origin ASC';
		//Transaction Details
		$td_query = 'select * from flight_booking_transaction_details AS CD WHERE CD.app_reference='.$this->db->escape($app_reference);
		//Customer and Ticket Details
		$cd_query = 'select distinct CD.*,FPTI.TicketId,FPTI.TicketNumber,FPTI.IssueDate,FPTI.Fare,FPTI.SegmentAdditionalInfo
						from flight_booking_passenger_details AS CD
						left join flight_passenger_ticket_info FPTI on CD.origin=FPTI.passenger_fk
						WHERE CD.flight_booking_transaction_details_fk IN 
						(select TD.origin from flight_booking_transaction_details AS TD 
						WHERE TD.app_reference ='.$this->db->escape($app_reference).')';
		//Cancellation Details
		$cancellation_details_query = 'select FCD.*
						from flight_booking_passenger_details AS CD
						left join flight_cancellation_details AS FCD ON FCD.passenger_fk=CD.origin
						WHERE CD.flight_booking_transaction_details_fk IN 
						(select TD.origin from flight_booking_transaction_details AS TD 
						WHERE TD.app_reference ='.$this->db->escape($app_reference).')';
		
		//Baggage Details
		$baggage_query = 'select CD.flight_booking_transaction_details_fk,
						concat(CD.first_name," ", CD.last_name) as pax_name,FBG.*
						from flight_booking_passenger_details AS CD
						join flight_booking_baggage_details FBG on CD.origin=FBG.passenger_fk
						WHERE CD.flight_booking_transaction_details_fk IN 
						(select TD.origin from flight_booking_transaction_details AS TD 
						WHERE TD.app_reference ='.$this->db->escape($app_reference).')';
		//Meal Details
		$meal_query = 'select CD.flight_booking_transaction_details_fk,
						concat(CD.first_name," ", CD.last_name) as pax_name,FML.*
						from flight_booking_passenger_details AS CD
						join flight_booking_meal_details FML on CD.origin=FML.passenger_fk
						WHERE CD.flight_booking_transaction_details_fk IN 
						(select TD.origin from flight_booking_transaction_details AS TD 
						WHERE TD.app_reference ='.$this->db->escape($app_reference).')';
		//Seat Details
		$seat_query = 'select CD.flight_booking_transaction_details_fk,
						concat(CD.first_name," ", CD.last_name) as pax_name,FST.*
						from flight_booking_passenger_details AS CD
						join flight_booking_seat_details FST on CD.origin=FST.passenger_fk
						WHERE CD.flight_booking_transaction_details_fk IN 
						(select TD.origin from flight_booking_transaction_details AS TD 
						WHERE TD.app_reference ='.$this->db->escape($app_reference).')';

		$response['data']['booking_details']			= $this->db->query($bd_query)->result_array();
		$response['data']['booking_itinerary_details']	= $this->db->query($id_query)->result_array();
		$response['data']['booking_transaction_details']	= $this->db->query($td_query)->result_array();
		$response['data']['booking_customer_details']	= $this->db->query($cd_query)->result_array();
		$response['data']['cancellation_details']	= $this->db->query($cancellation_details_query)->result_array();
		$response['data']['baggage_details']	= $this->db->query($baggage_query)->result_array();
		$response['data']['meal_details']	= $this->db->query($meal_query)->result_array();
		$response['data']['seat_details']	= $this->db->query($seat_query)->result_array();
		
		if (valid_array($response['data']['booking_details']) == true and valid_array($response['data']['booking_itinerary_details']) == true and valid_array($response['data']['booking_customer_details']) == true) {
			$response['status'] = SUCCESS_STATUS;
		}
		return $response;
	}
	/**
	 * Sagar Wakchaure
	 * B2C Flight Report
	 * @param unknown $condition
	 * @param unknown $count
	 * @param unknown $offset
	 * @param unknown $limit
	 * $condition[] = array('U.user_typ', '=', B2C_USER, ' OR ', 'BD.created_by_i', '=', 0);
	 */
	function b2c_flight_report($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//$b2c_condition_array = array('U.user_type', '=', B2C_USER, ' OR ', 'BD.created_by_id', '=', 0);
		
		//BT, CD, ID

		// if(isset($condition) == true)
		// {
		// 	$offset = 0;
		// }else{
			
		// 	$offset = $offset;
		// }


		if ($count) {
			
			//echo debug($condition);exit;
			$query = 'select count(distinct(BD.app_reference)) AS total_records from flight_booking_details BD
					left join user U on U.user_id = BD.created_by_id
					left join user_type UT on UT.origin = U.user_type
					join flight_booking_transaction_details as BT on BD.app_reference = BT.app_reference	
					where (U.user_type='.B2C_USER.' OR BD.created_by_id = 0) AND BD.domain_origin='.get_domain_auth_id().''.$condition;
			//echo debug($query);exit;
			
			$data = $this->db->query($query)->row_array();
			
			return $data['total_records'];

		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			$payment_details = array();
			//Booking Details
			$bd_query = 'select BD.* ,U.user_name,U.first_name,U.last_name from flight_booking_details AS BD
					     left join user U on U.user_id = BD.created_by_id
					     left join user_type UT on UT.origin = U.user_type
					     join flight_booking_transaction_details as BT on BD.app_reference = BT.app_reference		
						 WHERE  (U.user_type='.B2C_USER.' OR BD.created_by_id = 0) AND BD.domain_origin='.get_domain_auth_id().' '.$condition.'
						 order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;		

						 
						 
			$booking_details	= $this->db->query($bd_query)->result_array();
			//echo debug($bd_query); 			exit;
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				//Itinerary Details
				$id_query = 'select * from flight_booking_itinerary_details AS ID
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				//Transaction Details
				$td_query = 'select * from flight_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.')';
				//Customer and Ticket Details
				$cd_query = 'select CD.*,FPTI.TicketId,FPTI.TicketNumber,FPTI.IssueDate,FPTI.Fare,FPTI.SegmentAdditionalInfo
							from flight_booking_passenger_details AS CD
							left join flight_passenger_ticket_info FPTI on CD.origin=FPTI.passenger_fk
							WHERE CD.flight_booking_transaction_details_fk IN
							(select TD.origin from flight_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//Cancellation Details
				$cancellation_details_query = 'select FCD.*
						from flight_booking_passenger_details AS CD
						left join flight_cancellation_details AS FCD ON FCD.passenger_fk=CD.origin
						WHERE CD.flight_booking_transaction_details_fk IN
						(select TD.origin from flight_booking_transaction_details AS TD
						WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//$payment_details_query = '';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$booking_transaction_details = $this->db->query($td_query)->result_array();
				$cancellation_details = $this->db->query($cancellation_details_query)->result_array();
				//$payment_details = $this->db->query($payment_details_query)->result_array();
			}
	
			$response['data']['booking_details']			= $booking_details;
			$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			$response['data']['booking_transaction_details']	= $booking_transaction_details;
			$response['data']['booking_customer_details']	= $booking_customer_details;
			$response['data']['cancellation_details']	= $cancellation_details;
			//$response['data']['payment_details']	= $payment_details;
			return $response;
		}
	}	
	
	
	/**
	 * Sagar Wakchaure
	 * B2C Flight Report
	 * @param unknown $condition
	 * @param unknown $count
	 * @param unknown $offset
	 * @param unknown $limit
	 * $condition[] = array('U.user_typ', '=', B2C_USER, ' OR ', 'BD.created_by_i', '=', 0);
	 */
	function b2b_flight_report($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//$b2c_condition_array = array('U.user_type', '=', B2C_USER, ' OR ', 'BD.created_by_id', '=', 0);
	
		//BT, CD, ID

		// if(isset($condition) == true)
		// {
		// 	$offset = 0;
		// }else{
		// 	$offset = $offset;
		// }

		if ($count) {
				
			//echo debug($condition);exit;
			$query = 'select count(distinct(BD.app_reference)) AS total_records from flight_booking_details BD
					  join user U on U.user_id = BD.created_by_id
					  join flight_booking_transaction_details as BT on BD.app_reference = BT.app_reference						
					  where U.user_type='.B2B_USER.' AND BD.domain_origin='.get_domain_auth_id().''.$condition;	
		
			$data = $this->db->query($query)->row_array();
			//echo debug($data);exit;
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			$payment_details = array();
			//Booking Details
			$bd_query = 'select BD.*,U.agency_name,U.first_name,U.last_name from flight_booking_details AS BD
					      join user U on U.user_id = BD.created_by_id join flight_booking_transaction_details as BT on BD.app_reference = BT.app_reference					      
						  WHERE  U.user_type='.B2B_USER.' AND BD.domain_origin='.get_domain_auth_id().' '.$condition.'
						  order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;
						  
			//echo debug($bd_query);			
			//exit;
			
			$booking_details	= $this->db->query($bd_query)->result_array();
			//echo debug($booking_details);exit;
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				//Itinerary Details
				$id_query = 'select * from flight_booking_itinerary_details AS ID
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				//Transaction Details
				$td_query = 'select * from flight_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.')';
				
				//Customer and Ticket Details
				$cd_query = 'select CD.*,FPTI.TicketId,FPTI.TicketNumber,FPTI.IssueDate,FPTI.Fare,FPTI.SegmentAdditionalInfo
							from flight_booking_passenger_details AS CD
							left join flight_passenger_ticket_info FPTI on CD.origin=FPTI.passenger_fk
							WHERE CD.flight_booking_transaction_details_fk IN
							(select TD.origin from flight_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//Cancellation Details
				$cancellation_details_query = 'select FCD.*
						from flight_booking_passenger_details AS CD
						left join flight_cancellation_details AS FCD ON FCD.passenger_fk=CD.origin
						WHERE CD.flight_booking_transaction_details_fk IN
						(select TD.origin from flight_booking_transaction_details AS TD
						WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//$payment_details_query = '';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$booking_transaction_details = $this->db->query($td_query)->result_array();
				$cancellation_details = $this->db->query($cancellation_details_query)->result_array();
				//$payment_details = $this->db->query($payment_details_query)->result_array();
			}
	
			$response['data']['booking_details']			= $booking_details;
			$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			$response['data']['booking_transaction_details']	= $booking_transaction_details;
			$response['data']['booking_customer_details']	= $booking_customer_details;
			$response['data']['cancellation_details']	= $cancellation_details;
			//$response['data']['payment_details']	= $payment_details;
			return $response;
		}
	}
	function corporate_flight_report($condition = array(), $count=false, $offset=0, $limit=100000000000)
	{
	
		$condition = $this->custom_db->get_custom_condition($condition);
		//$b2c_condition_array = array('U.user_type', '=', B2C_USER, ' OR ', 'BD.created_by_id', '=', 0);
	
		//BT, CD, ID
	
		//echo "off..".$offset;
	
		/*if($condition !=''){
		 $offset = 0;
			}else{
			$offset = $offset;
			}*/
		if ($count) {
	
			//echo debug($condition);exit;
			$query = 'select count(distinct(BD.app_reference)) AS total_records from flight_booking_details BD
					  join user U on U.user_id = BD.created_by_id
					  join flight_booking_transaction_details as BT on BD.app_reference = BT.app_reference
					  where U.user_type='.CORPORATE_USER.' AND BD.domain_origin='.get_domain_auth_id().''.$condition;
	
			$data = $this->db->query($query)->row_array();
			//echo debug($data);echo $query; //exit;
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			$payment_details = array();
				
			//Booking Details
			$bd_query = 'select distinct(BD.app_reference),BD.*,U.corporate_id,U.agency_name,U.first_name,U.last_name from flight_booking_details AS BD
					      join user U on U.user_id = BD.created_by_id join flight_booking_transaction_details as BT on BD.app_reference = BT.app_reference
						  WHERE  U.user_type='.CORPORATE_USER.' AND BD.domain_origin='.get_domain_auth_id().' '.$condition.'
						  order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;
				
			// echo debug($bd_query);
			// exit;
				
			$booking_details	= $this->db->query($bd_query)->result_array();
				
			//echo debug($booking_details);exit;
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				//Itinerary Details
				$id_query = 'select * from flight_booking_itinerary_details AS ID
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				//Transaction Details
				$td_query = 'select * from flight_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.')';
	
				//Customer and Ticket Details
				$cd_query = 'select CD.*,FPTI.TicketId,FPTI.TicketNumber,FPTI.IssueDate,FPTI.Fare,FPTI.SegmentAdditionalInfo
							from flight_booking_passenger_details AS CD
							left join flight_passenger_ticket_info FPTI on CD.origin=FPTI.passenger_fk
							WHERE CD.flight_booking_transaction_details_fk IN
							(select TD.origin from flight_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//Cancellation Details
				$cancellation_details_query = 'select FCD.*
						from flight_booking_passenger_details AS CD
						left join flight_cancellation_details AS FCD ON FCD.passenger_fk=CD.origin
						WHERE CD.flight_booking_transaction_details_fk IN
						(select TD.origin from flight_booking_transaction_details AS TD
						WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//$payment_details_query = '';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$booking_transaction_details = $this->db->query($td_query)->result_array();
				$cancellation_details = $this->db->query($cancellation_details_query)->result_array();
				//$payment_details = $this->db->query($payment_details_query)->result_array();
			}
	
			$response['data']['booking_details']			= $booking_details;
			$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			$response['data']['booking_transaction_details']= $booking_transaction_details;
			$response['data']['booking_customer_details']	= $booking_customer_details;
			$response['data']['cancellation_details']	= $cancellation_details;
			//$response['data']['payment_details']	= $payment_details;
			return $response;
		}
	}

	/**
	 * return all booking events
	 */
	function booking_events()
	{
		//BT, CD, ID
		$query = 'select * from flight_booking_details where domain_origin='.get_domain_auth_id();
		return $this->db->query($query)->result_array();
	}

	function get_monthly_booking_summary()
	{
		$query = 'select count(distinct(BD.app_reference)) AS total_booking, sum(TD.total_fare+TD.admin_markup+BD.convinence_amount) as monthly_payment, sum(TD.admin_markup+BD.convinence_amount) as monthly_earning, 
		MONTH(BD.created_datetime) as month_number 
		from flight_booking_details AS BD
		join flight_booking_transaction_details as TD on BD.app_reference=TD.app_reference
		where (YEAR(BD.created_datetime) BETWEEN '.date('Y').' AND '.date('Y', strtotime('+1 year')).') AND BD.domain_origin='.get_domain_auth_id().'
		GROUP BY YEAR(BD.created_datetime), MONTH(BD.created_datetime)';
		return $this->db->query($query)->result_array();
	}

	function monthly_search_history($year_start, $year_end)
	{
		$query = 'select count(*) AS total_search, MONTH(created_datetime) as month_number from search_flight_history where
		(YEAR(created_datetime) BETWEEN '.$year_start.' AND '.$year_end.') AND domain_origin='.get_domain_auth_id().' 
		AND search_type="'.META_AIRLINE_COURSE.'"
		GROUP BY YEAR(created_datetime), MONTH(created_datetime)';
		return $this->db->query($query)->result_array();
	}

	function top_search($year_start, $year_end)
	{
		$query = 'select count(*) AS total_search, concat(from_code, "-",to_code) label from search_flight_history where
		(YEAR(created_datetime) BETWEEN '.$year_start.' AND '.$year_end.') AND domain_origin='.get_domain_auth_id().' 
		AND search_type="'.META_AIRLINE_COURSE.'"
		GROUP BY CONCAT(from_code, to_code) order by count(*) desc, created_datetime desc limit 0, 15';
		return $this->db->query($query)->result_array();
	}
	/*
	 * Jaganath
	 * Update the Cancellation Details of the Passenger
	 */
	function update_pax_ticket_cancellation_details($ticket_cancellation_details, $pax_origin)
	{
		//1.Updating Passenger Status
		$booking_status = 'BOOKING_CANCELLED';
		$passenger_update_data = array();
		$passenger_update_data['status'] = $booking_status;
		$passenger_update_condition = array();
		$passenger_update_condition['origin'] = $pax_origin;
		$this->custom_db->update_record('flight_booking_passenger_details', $passenger_update_data, $passenger_update_condition);
		//2.Adding Cancellation Details
		$data = array();
		$cancellation_details = $ticket_cancellation_details['cancellation_details'];
		$data['RequestId'] = $cancellation_details['ChangeRequestId'];
		$data['ChangeRequestStatus'] = $cancellation_details['ChangeRequestStatus'];
		$data['statusDescription'] = $cancellation_details['StatusDescription'];
		$pax_details_exists = $this->custom_db->single_table_records('flight_cancellation_details', '*', array('passenger_fk' => $pax_origin));
		if($pax_details_exists['status'] == true) {
			//Update the Data
			$this->custom_db->update_record('flight_cancellation_details', $data, array('passenger_fk' => $pax_origin));
		} else {
			//Insert Data
			$data['passenger_fk'] = $pax_origin;
			$data['created_by_id'] = intval(@$this->entity_user_id);
			$data['created_datetime'] = date('Y-m-d H:i:s');
			$data['cancellation_requested_on'] = date('Y-m-d H:i:s');
			$this->custom_db->insert_record('flight_cancellation_details', $data);
		}
	}
	/**
	 * Update Flight Booking Transaction Status based on Passenger Ticket status
	 * @param unknown_type $transaction_origin
	 */
	public function update_flight_booking_transaction_cancel_status($transaction_origin)
	{
		$confirmed_passenger_exists = $this->custom_db->single_table_records('flight_booking_passenger_details', '*', array('flight_booking_transaction_details_fk' => $transaction_origin, 'status' => 'BOOKING_CONFIRMED'));
		if($confirmed_passenger_exists['status'] == false){
			//If all passenger cancelled the ticket for that particular transaction, then set the transaction status to  BOOKING_CANCELLED
			$transaction_update_data = array();
			$booking_status = 'BOOKING_CANCELLED';
			$transaction_update_data['status'] = $booking_status;
			$transaction_update_condition = array();
			$transaction_update_condition['origin'] = $transaction_origin;
			$this->custom_db->update_record('flight_booking_transaction_details', $transaction_update_data, $transaction_update_condition);
		}
	}
	/**
	 * Update Flight Booking Transaction Status based on Passenger Ticket status
	 * @param unknown_type $transaction_origin
	 */
	public function update_flight_booking_cancel_status($app_reference)
	{
		$confirmed_passenger_exists = $this->custom_db->single_table_records('flight_booking_passenger_details', '*', array('app_reference' => $app_reference, 'status' => 'BOOKING_CONFIRMED'));
		if($confirmed_passenger_exists['status'] == false){
			//If all passenger cancelled the ticket, then set the booking status to  BOOKING_CANCELLED
			$booking_update_data = array();
			$booking_status = 'BOOKING_CANCELLED';
			$booking_update_data['status'] = $booking_status;
			$booking_update_condition = array();
			$booking_update_condition['app_reference'] = $app_reference;
			$this->custom_db->update_record('flight_booking_details', $booking_update_data, $booking_update_condition);
		}
	}
	
	/**
	 * Check if destination are domestic
	 * @param string $from_loc Unique location code
	 * @param string $to_loc   Unique location code
	 */
	function is_domestic_flight($from_loc, $to_loc)
	{
		if(valid_array($from_loc) == true || valid_array($to_loc)) {//Multicity
			$airport_cities = array_merge($from_loc, $to_loc);
			$airport_cities = array_unique($airport_cities);
			$airport_city_codes = '';
			foreach($airport_cities as $k => $v){
				$airport_city_codes .= '"'.$v.'",';
			}
			$airport_city_codes = rtrim($airport_city_codes, ',');
			$query = 'SELECT count(*) total FROM flight_airport_list WHERE airport_code IN ('.$airport_city_codes.') AND country != "India"';
		} else {//Oneway/RoundWay
			$query = 'SELECT count(*) total FROM flight_airport_list WHERE airport_code IN ('.$this->db->escape($from_loc).','.$this->db->escape($to_loc).') AND country != "India"';
		}
		$data = $this->db->query($query)->row_array();
		if (intval($data['total']) > 0){
			return false;
		} else {
			return true;
		}
	
	}
	
	/**
	 * Sagar Wakchaure
	 * update the pnr details
	 * @param unknown $response
	 * @param unknown $app_reference
	 * @param unknown $booking_source
	 * @param unknown $booking_status
	 * @return string
	 */
	function update_pnr_details($response,$app_reference, $booking_source='',$booking_status=''){
		
		$return_response = FAILURE_STATUS;		
		$booking_details = $this->get_booking_details($app_reference, $booking_source, $booking_status);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'admin');	
		$booking_transaction_details = $table_data['data']['booking_details'][0]['booking_transaction_details'];
		$update_pnr_array = array();
		$update_itinerary_details = array();
		$update_ticket_info = array();
		
		//update flight_booking_transaction_details table and flight_passenger_ticket_info
		
		if ($booking_details['status'] == SUCCESS_STATUS && $response['status'] == SUCCESS_STATUS) {
			$i=0;
			foreach($booking_transaction_details as $key=>$transaction_detail_sub_data){				
				$update_pnr_array['pnr'] = $response['data']['BoookingTransaction'][$i]['PNR'];
				$update_pnr_array['book_id'] =$response['data']['BoookingTransaction'][$i]['BookingID'];
				$update_pnr_array['status'] =$response['data']['BoookingTransaction'][$i]['Status'];
				$sequence_no = $response['data']['BoookingTransaction'][$i]['SequenceNumber'];
				
				//update flight_booking_transaction_details
				$this->custom_db->update_record('flight_booking_transaction_details', $update_pnr_array, array('app_reference' =>$app_reference,'sequence_number'=>trim($sequence_no)));			  			  
			
				foreach($transaction_detail_sub_data['booking_customer_details'] as $k=>$booking_customer_data){
					$update_ticket_info['TicketId'] = $response['data']['BoookingTransaction'][$i]['BookingCustomer'][$k]['TicketId'];
					$update_ticket_info['TicketNumber'] = $response['data']['BoookingTransaction'][$i]['BookingCustomer'][$k]['TicketNumber'];			   	     
					
					//update flight_passenger_ticket_info
					$this->custom_db->update_record('flight_passenger_ticket_info', $update_ticket_info,array('passenger_fk' => $booking_customer_data['origin']));			    	
				}
				$i++;
				
				//update  status in flight_booking_passenger_details
				$this->custom_db->update_record('flight_booking_passenger_details',array('status'=>$update_pnr_array['status']) ,array('app_reference' => trim($app_reference)));
			}
			
			//update status in flight_booking_details
			if(isset($response['data']['MasterBookingStatus']) && !empty($response['data']['MasterBookingStatus'])){
				
				$this->custom_db->update_record('flight_booking_details', array('status'=>$response['data']['MasterBookingStatus']),array('app_reference' => $app_reference));			
			}
			
			//update flight_booking_itinerary_details table		
			foreach($booking_details['data']['booking_itinerary_details'] as $key=>$transaction_detail_sub_data){
					$update_itinerary_details['airline_pnr'] = $response['data']['BookingItineraryDetails'][$key]['AirlinePNR'];
					$from = $response['data']['BookingItineraryDetails'][$key]['FromAirlineCode'];
					$to = $response['data']['BookingItineraryDetails'][$key]['ToAirlineCode'];
					$departure_datetime = $response['data']['BookingItineraryDetails'][$key]['DepartureDatetime'];
					
					$this->custom_db->update_record('flight_booking_itinerary_details', $update_itinerary_details, 
					array('app_reference' =>$app_reference,'from_airport_code'=>trim($from),'to_airport_code'=>trim($to),'departure_datetime'=>trim($departure_datetime)));
			}
			
			$return_response = SUCCESS_STATUS;
		}
		return $return_response;
	}
	/**
	 Jaganath
	 * Returns Passenger Ticket Details based on the following parameteres
	 * @param $app_reference
	 * @param $passenger_origin
	 * @param $passenger_booking_status
	 */
	function get_passenger_ticket_info($app_reference, $passenger_origin, $passenger_booking_status='')
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		$bd_query = 'select BD.*,DL.domain_name,DL.origin as domain_id,CC.country as domain_base_currency from flight_booking_details AS BD,domain_list AS DL
						join currency_converter CC on CC.id=DL.currency_converter_fk 
						WHERE DL.origin = BD.domain_origin AND BD.app_reference like ' . $this->db->escape ( $app_reference );
		//Customer and Ticket Details
		$cd_query = 'select FBTD.book_id,FBTD.pnr,FBTD.sequence_number,CD.*,FPTI.TicketId,FPTI.TicketNumber,FPTI.IssueDate,FPTI.Fare,FPTI.SegmentAdditionalInfo
						from flight_booking_passenger_details AS CD
						join flight_booking_transaction_details FBTD on CD.flight_booking_transaction_details_fk=FBTD.origin
						left join flight_passenger_ticket_info FPTI on CD.origin=FPTI.passenger_fk
						WHERE CD.app_reference="'.$app_reference.'" and CD.origin='.intval($passenger_origin).' and CD.status="'.$passenger_booking_status.'"';
		//Cancellation Details
		$cancellation_details_query = 'select FCD.*
						from flight_booking_passenger_details AS CD
						left join flight_passenger_ticket_info FPTI on CD.origin=FPTI.passenger_fk
						left join flight_cancellation_details AS FCD ON FCD.passenger_fk=CD.origin
						WHERE CD.app_reference="'.$app_reference.'" and CD.origin='.intval($passenger_origin).' and CD.status="'.$passenger_booking_status.'"';
		$response['data']['booking_details']			= $this->db->query($bd_query)->result_array();
		$response['data']['booking_customer_details']	= $this->db->query($cd_query)->result_array();
		$response['data']['cancellation_details']	= $this->db->query($cancellation_details_query)->result_array();
		if (valid_array($response['data']['booking_details']) == true && valid_array($response['data']['booking_customer_details']) == true) {
			$response['status'] = SUCCESS_STATUS;
		}
		return $response;
	}
	/**
	 * Jaganath
	 * Update Supplier Ticket Refund Details
	 * @param unknown_type $supplier_ticket_refund_details
	 */
	public function update_supplier_ticket_refund_details($passenger_origin, $supplier_ticket_refund_details)
	{
		$update_refund_details = array();
		$supplier_ticket_refund_details = $supplier_ticket_refund_details['RefundDetails'];
		$update_refund_details['ChangeRequestStatus'] = 			$supplier_ticket_refund_details['ChangeRequestStatus'];
		$update_refund_details['statusDescription'] = 				$supplier_ticket_refund_details['StatusDescription'];
		$update_refund_details['API_refund_status'] = 				$supplier_ticket_refund_details['RefundStatus'];
		$update_refund_details['API_RefundedAmount'] = 				floatval($supplier_ticket_refund_details['RefundedAmount']);
		$update_refund_details['API_CancellationCharge'] = 			floatval($supplier_ticket_refund_details['CancellationCharge']);
		$update_refund_details['API_ServiceTaxOnRefundAmount'] =	floatval($supplier_ticket_refund_details['ServiceTaxOnRefundAmount']);
		$update_refund_details['API_SwachhBharatCess'] = 			floatval($supplier_ticket_refund_details['SwachhBharatCess']);
		
		if($supplier_ticket_refund_details['RefundStatus'] == 'PROCESSED') {
			$update_refund_details['cancellation_processed_on'] = date('Y-m-d H:i:s');
		}
		$this->custom_db->update_record('flight_cancellation_details', $update_refund_details, array('passenger_fk' => intval($passenger_origin)));
	}
	function get_booked_user_details($app_reference)
	{
		$query = "select  BD.created_by_id,U.user_type from flight_booking_details as BD join user as U on U.user_id = BD.created_by_id where app_reference = '".$app_reference."'";
		return $this->db->query($query)->result_array();
	}
	/**
	 * Extraservices(Baggage,Meal and Seats) Price
	 * @param unknown_type $app_reference
	 */
	public function get_extra_services_total_price($app_reference)
	{
		$extra_service_total_price = 0;
		
		//get baggage price
		$baggage_total_price = $this->get_baggage_total_price($app_reference);
		
		//get meal price
		$meal_total_price = $this->get_meal_total_price($app_reference);
		
		//get seat price
		$seat_total_price = $this->get_seat_total_price($app_reference);
		
		//Addig all services price
		$extra_service_total_price = round(($baggage_total_price+$meal_total_price+$seat_total_price), 2);
		
		return $extra_service_total_price;
	}
	/**
	 * 
	 * Returns Baggage Total Price
	 * @param unknown_type $app_reference
	 */
	public function get_baggage_total_price($app_reference)
	{
		$query = 'select sum(FBG.price) as baggage_total_price
			from flight_booking_passenger_details FP
			left join flight_booking_baggage_details FBG on FP.origin=FBG.passenger_fk
			where FP.app_reference="'.$app_reference.'" group by FP.app_reference';
		$data = $this->db->query($query)->row_array();
		return floatval(@$data['baggage_total_price']);
	}
	/**
	 * 
	 * Returns Meal Total Price
	 * @param unknown_type $app_reference
	 */
	public function get_meal_total_price($app_reference)
	{
		$query = 'select sum(FML.price) as meal_total_price
			from flight_booking_passenger_details FP
			left join flight_booking_meal_details FML on FP.origin=FML.passenger_fk
			where FP.app_reference="'.$app_reference.'" group by FP.app_reference';
		$data = $this->db->query($query)->row_array();
		
		return floatval(@$data['meal_total_price']);
	}
	/**
	 * 
	 * Returns Seat Total Price
	 * @param unknown_type $app_reference
	 */
	public function get_seat_total_price($app_reference)
	{
		$query = 'select sum(FST.price) as seat_total_price
			from flight_booking_passenger_details FP
			left join flight_booking_seat_details FST on FP.origin=FST.passenger_fk
			where FP.app_reference="'.$app_reference.'" group by FP.app_reference';
		$data = $this->db->query($query)->row_array();
		
		return floatval(@$data['seat_total_price']);
	}
	/**
	 * Extraservices(Baggage,Meal and Seats) Price
	 * @param unknown_type $app_reference
	 */
	public function add_extra_service_price_to_published_fare($app_reference)
	{
		$transaction_data = $this->db->query('select * from flight_booking_transaction_details where app_reference="'.$app_reference.'" order by origin asc')->result_array();
		if(valid_array($transaction_data) == true){
			foreach ($transaction_data as $tr_k => $tr_v){
				$transaction_origin = $tr_v['origin'];
				$extra_service_totla_price = $this->transaction_wise_extra_service_total_price($transaction_origin);
				
				$update_data = array();
				$update_condition = array();
				$update_data['total_fare'] = $tr_v['total_fare']+$extra_service_totla_price;
				$update_condition['origin'] = $transaction_origin;
				$this->custom_db->update_record('flight_booking_transaction_details', $update_data, $update_condition);
			}
		}
	}
	/**
	 * Transaction-wise extra service total price
	 * @param unknown_type $transaction_origin
	 */
	public function transaction_wise_extra_service_total_price($transaction_origin)
	{
		$extra_service_totla_price = 0;
		//Baggage
		$baggage_price = $this->db->query('select sum(FBG.price) as baggage_total_price
											from flight_booking_passenger_details FP
											left join flight_booking_baggage_details FBG on FP.origin=FBG.passenger_fk
											where FP.flight_booking_transaction_details_fk='.$transaction_origin.' group by FP.flight_booking_transaction_details_fk')->row_array();
				
		//Meal
		$meal_price = $this->db->query('select sum(FML.price) as meal_total_price
											from flight_booking_passenger_details FP
											left join flight_booking_meal_details FML on FP.origin=FML.passenger_fk
											where FP.flight_booking_transaction_details_fk='.$transaction_origin.' group by FP.flight_booking_transaction_details_fk')->row_array();
		//Seat
		$seat_price = $this->db->query('select sum(FST.price) as seat_total_price
											from flight_booking_passenger_details FP
											left join flight_booking_seat_details FST on FP.origin=FST.passenger_fk
											where FP.flight_booking_transaction_details_fk='.$transaction_origin.' group by FP.flight_booking_transaction_details_fk')->row_array();
		
		$extra_service_totla_price = floatval(@$baggage_price['baggage_total_price']+@$meal_price['meal_total_price']+@$seat_price['seat_total_price']);
		
		return $extra_service_totla_price;
	}

	/*
     * Upload Offline Booking information on database
     */

    function offline_flight_book($flight_data, $app_reference) {
        // debug($flight_data);exit;
        /* booking details */
        $status = empty($flight_data['status']) ? 'BOOKING_PENDING' : $flight_data['status'];
        $agent_id = $flight_data['agent_id'];


        if ($flight_data['booking_type'] == "international") {
            $ModuleType = "flight_int";
        } else {
            $ModuleType = "flight";
        }
        $domain_details = $this->domain_management_model->domain_data($flight_data['agent_id']);

        // $cm = $this->domain_management_model->get_commission_details($domain_details['origin']);

        $cm = 0;//$this->get_master_commission_details($ModuleType);
        $airline = $this->db_cache_api->get_airline_list($from = array('k' => 'code', 'v' => 'name'));
        //debug($airline);exit;
        $first_flight = 0;
        $last_flight = ($flight_data ['sect_num_onward'] - 1);
        $trp = 'onward';
        $trp1 = $flight_data['trip_type'] == 'circle' ? 'return' : 'onward';
        $trp_last = $flight_data['trip_type'] == 'circle' ? ($flight_data ['sect_num_return'] - 1) : $last_flight;
        $booking_details['domain_origin'] = $domain_details['origin'];
        $booking_details['currency'] = 'INR';//$domain_details['domain_base_currency'];
        $booking_details['app_reference'] = $app_reference;
        $booking_details['booking_source'] = $flight_data['suplier_id'];
        $booking_details['is_lcc'] = $flight_data ['is_lcc'] != 'gds' ? 1 : 0;
        $booking_details['phone'] = $flight_data ['passenger_phone'];
        $booking_details['alternate_number'] = $flight_data ['passenger_phone'];
        $booking_details['email'] = $flight_data ['passenger_email'];
        $booking_details['journey_start'] = db_current_datetime(trim($flight_data['dep_date_onward'][$first_flight] . ' ' . $flight_data['dep_time_onward'][$first_flight]));
        $booking_details['journey_end'] = db_current_datetime(trim($flight_data ['arr_date_' . $trp1][$trp_last] . ' ' . $flight_data['arr_time_' . $trp1][$trp_last]));
        $booking_details['journey_from'] = strtoupper($flight_data ['dep_loc_onward'][$first_flight]);
        $booking_details['journey_to'] = strtoupper($flight_data ['arr_loc_onward'][$last_flight]);
        $booking_details['payment_mode'] = 'PNHB1';
        $booking_details['attributes'] = '';
        $booking_details['created_by_id'] = $agent_id;
        $booking_details['created_datetime'] = date('Y-m-d H:i:s');
        $currency = 'INR';//domain_base_currency();
        $currency_obj = new Currency(array('module_type' => 'b2c_flight'));

        if ($domain_details['domain_base_currency'] == "INR") {
            $currency_conversion_rate = 1;
        } else {
            $currency_conversion_rate = 1;//$currency_obj->get_specific_domain_currency_conversion_rate($domain_details['origin']);
            // $currency_conversion_rate = $currency_obj->get_domain_currency_conversion_rate();
        }

        $booking_details['offline_supplier_name'] = $flight_data['suplier_name'];
        $booking_details['currency_conversion_rate'] = $currency_conversion_rate;

        $book_id = $this->custom_db->insert_record('flight_booking_details', $booking_details);
        $book_id = @$book_id['insert_id'];

        $pax_fare = array();
        $c = 0;
        foreach ($flight_data['pax_basic_fare_onward'] as $fk => $fv) {
            $pax_fare['onward']['basic'] = @$pax_fare['onward']['basic'] + ($fv * $flight_data['pax_type_count_onward'][$fk]);
            $pax_fare['onward']['yq'] = @$pax_fare['onward']['yq'] + ($flight_data['pax_yq_onward'][$fk] * $flight_data['pax_type_count_onward'][$fk]);
            $pax_fare['onward']['others'] = @$pax_fare['onward']['others'] + ($flight_data['pax_other_tax_onward'][$fk] * $flight_data['pax_type_count_onward'][$fk]);
            if ($flight_data['trip_type'] == 'circle' && isset($flight_data['pax_basic_fare_return'][$fk])) {
                $pax_fare['return']['basic'] = @$pax_fare['return']['basic'] + ($flight_data['pax_basic_fare_return'][$fk] * $flight_data['pax_type_count_return'][$fk]);
                $pax_fare['return']['yq'] = @$pax_fare['return']['yq'] + ($flight_data['pax_yq_return'][$fk] * $flight_data['pax_type_count_return'][$fk]);
                $pax_fare['return']['others'] = @$pax_fare['return']['others'] + ($flight_data['pax_other_tax_return'][$fk] * $flight_data['pax_type_count_return'][$fk]);
            }
        }

        $c_on = strtoupper(@$flight_data['career_onward'][0]);
        $t[$c_on][0]['career'] = @$flight_data['career_onward'];
        $t[$c_on][0]['sect_num'] = @$flight_data['sect_num_onward'];
        $t[$c_on][0]['pax_count'] = array_sum($flight_data['pax_type_count_onward']);

        $f[$c_on]['basic'] = $pax_fare['onward']['basic'];
        $f[$c_on]['yq'] = $pax_fare['onward']['yq'];
        $f[$c_on]['others'] = $pax_fare['onward']['others'];
        if ($flight_data['trip_type'] == 'circle' && valid_array(@$flight_data['career_return'])) {
            $c_rt = strtoupper(@$flight_data['career_return'][0]);
            $t[$c_rt][1]['career'] = @$flight_data['career_return'];
            $t[$c_rt][1]['sect_num'] = @$flight_data['sect_num_return'];
            $t[$c_rt][1]['pax_count'] = array_sum($flight_data['pax_type_count_return']);
            $f[$c_rt]['basic'] = intval(@$f[$c_rt]['basic']) + $pax_fare['return']['basic'];
            $f[$c_rt]['yq'] = intval(@$f[$c_rt]['yq']) + $pax_fare['return']['yq'];
            $f[$c_rt]['others'] = intval(@$f[$c_rt]['others']) + $pax_fare['return']['others'];
        }
        // debug($t);exit;
        //foreach($t as $tk => $tv ){

        $api_total_display_fare = 0;
        $api_total_tax = 0;
        $api_total_fare = 0;
        $meal_and_baggage_fare = 0;
        $other_fare = 0;
        $basic_fare = 0;
        $fuel_charge = 0;
        $handling_charge = 0;
        $api_service_tax = 0;
        $agent_commission = 0;
        $api_agent_tds_on_commision = 0;
        $dist_commission = 0;
        $api_dist_tds_on_commision = 0;
        $admin_commission = 0;
        $admin_tds_on_commission = 0;
        $agent_markup = 0;
        $admin_markup = 0;
        $dist_markup = 0;
        $app_user_buying_price = 0;


        $price1 = array();
        $tot_agent_buying_price = 0;
        // debug($t);exit;
        foreach ($t as $tk => $tv) {

            $trpc = $tk = strtoupper($tk);


            // $hc = $flight_data['hc_comm'];
            $hc = 0;
            // $cm['tds_tax_details']['tds'] = 0;
            // $cm['tds_tax_details']['service_tax'] = 0;


            if (@$flight_data['admin_markup'] > 0) {
                $service_tax = @$flight_data['admin_markup'] * 18 / 100;
            } else {
                $service_tax = 0;
            }
            if ($flight_data['trip_type'] == 'oneway') {
                $service_tax = $service_tax;
            } else {
                $service_tax = $service_tax / 2;
            }
            if ($flight_data['trip_type'] == 'oneway') {
                $agent_comm = $flight_data['basic_comm'];
            } else {
                $agent_comm = $flight_data['basic_comm'];
                $agent_comm = $agent_comm / 2;
            }
            if ($flight_data['trip_type'] == 'oneway') {
                $agent_tds_on_commission = $flight_data['basic_comm'] * 5 / 100;
            } else {
                $agent_tds_on_commission = $flight_data['basic_comm'] * 5 / 100;
                $agent_tds_on_commission = $agent_tds_on_commission / 2;
            }

            $dist_comm = 0;
            $dist_tds_on_commission = 0;
            $total = $f[$trpc]['basic'];
            $tot_markup = ( @$flight_data['agent_markup'] + @$flight_data['admin_markup'] );
            // $buying_price = $total + $hc + $service_tax + $tot_markup;
            $agent_buying_price = $f[$trpc]['basic'] + $agent_tds_on_commission + $f[$trpc]['others'] + $service_tax - $agent_comm;
            $buying_price = $f[$trpc]['basic'] + $f[$trpc]['others'] + $service_tax;
            // $agent_buying_price = $buying_price - $agent_comm + $agent_tds_on_commission - (@$flight_data['agent_markup']);

            $tot_agent_buying_price += $agent_buying_price;
            $price['api_total_display_fare'] = $buying_price;
            $price['total_breakup'] = array(
                //'api_total_tax'=> $f[$trpc]['others'] + $f[$trpc]['yq'] + $hc + $service_tax + $tot_markup,
                'api_total_tax' => $f[$trpc]['others'] + $f[$trpc]['yq'],
                'api_total_fare' => $f[$trpc]['basic'],
                'meal_and_baggage_fare' => 0
            );
            $price['price_breakup'] = array(
                'other_fare' => $f[$trpc]['others'] + $hc + $service_tax + $tot_markup,
                'basic_fare' => $f[$trpc]['basic'],
                'fuel_charge' => $f[$trpc]['yq'],
                'handling_charge' => $hc,
                'service_tax' => $service_tax,
                'meal_and_baggage_fare' => 0,
                'agent_commission' => $agent_comm,
                'agent_tds_on_commision' => $agent_tds_on_commission,
                'dist_commission' => $dist_comm,
                'dist_tds_on_commision' => $dist_tds_on_commission,
                'admin_commission' => 0,
                'admin_tds_on_commission' => 0,
                'agent_markup' => @$flight_data['agent_markup'],
                'admin_markup' => @$flight_data['admin_markup'],
                'dist_markup' => 0,
                'app_user_buying_price' => $agent_buying_price
            );



            $api_total_display_fare += $buying_price;
            //	$api_total_tax += $f[$trpc]['others'] + $f[$trpc]['yq'] + $hc + $service_tax + $tot_markup;
            $api_total_tax += $f[$trpc]['others'] + $f[$trpc]['yq'];
            $api_total_fare += $f[$trpc]['basic'];
            $meal_and_baggage_fare += 0;
            $other_fare += $f[$trpc]['others'];
            $basic_fare += $f[$trpc]['basic'];
            $fuel_charge += $f[$trpc]['yq'];
            $handling_charge += $hc;
            $api_service_tax += $service_tax;
            $agent_commission += $agent_comm;
            $api_agent_tds_on_commision += $agent_tds_on_commission;
            $dist_commission += $dist_comm;
            $api_dist_tds_on_commision += $dist_tds_on_commission;
            $admin_commission += 0;
            $admin_tds_on_commission += 0;
            $agent_markup += @$flight_data['agent_markup'];
            $admin_markup += @$flight_data['admin_markup'];
            $dist_markup += 0;
            $app_user_buying_price += $agent_buying_price;




            $transaction_details['app_reference'] = $app_reference;
            $transaction_details['source'] = $flight_data['suplier_id'];
            $transaction_details['pnr'] = strtoupper(!$booking_details['is_lcc'] ? $flight_data['gds_pnr_' . $trp][$first_flight] : $flight_data['airline_pnr_' . $trp][$first_flight]);
            $transaction_details['status'] = $status;
            $transaction_details['status_description'] = 'In Payment';
            $transaction_details['book_id'] = $book_id;
            $transaction_details['booking_source'] = $flight_data['suplier_id'];
            $transaction_details['ref_id'] = '';
            $transaction_details['total_fare'] = $buying_price;
            $transaction_details['admin_commission'] = 0;
            $transaction_details['agent_commission'] = $agent_comm;
            $transaction_details['agent_tds'] = $agent_tds_on_commission;
            if (isset($tv[0]['pax_count'])) {
                $transaction_details['domain_markup'] = 0;
            } else if (isset($tv[1]['pax_count'])) {
                $transaction_details['domain_markup'] = 0;
            }
            $transaction_details['attributes'] = '';
            $transaction_details['sequence_number'] = '';

            $flg = $this->custom_db->insert_record('flight_booking_transaction_details', $transaction_details);
            $flight_booking_transaction_details_fk = @$flg['insert_id'];
            // debug($tv);
            foreach ($tv as $sk => $sv) {



                foreach ($sv['career'] as $ik => $iv) {
                    // debug($sv);
                    $segment_indicator = $ik + 1;
                    //$ik = strtoupper($ik);
                    $from_airport_name = $this->db_cache_api->get_airport_city_name(array(
                        'airport_code' => $flight_data['dep_loc_' . $trp][$ik]
                    ));
                    $to_airport_name = $this->db_cache_api->get_airport_city_name(array(
                        'airport_code' => $flight_data['arr_loc_' . $trp][$ik]
                    ));

                    //$itenery_details['booking_source'] = $flight_data['suplier_id'];
                    // $itenery_details['flight_booking_transaction_details_fk'] = $flight_booking_transaction_details_fk;
                    $itenery_details['app_reference'] = $app_reference;
                    $itenery_details['airline_pnr'] = strtoupper($flight_data['airline_pnr_' . $trp][$ik]);
                    $itenery_details['segment_indicator'] = $segment_indicator;
                    $itenery_details['airline_code'] = strtoupper($flight_data['career_' . $trp][$ik]);
                    $itenery_details['airline_name'] = isset($airline[strtoupper($flight_data['career_' . $trp][$ik])]) ? $airline[strtoupper($flight_data['career_' . $trp][$ik])] : strtoupper($flight_data['career_' . $trp][$ik]);
                    $itenery_details['flight_number'] = $flight_data['flight_num_' . $trp][$ik];
                    $itenery_details['fare_class'] = strtoupper($flight_data['booking_class_' . $trp][$ik]);
                    $itenery_details['from_airport_code'] = $flight_data['dep_loc_' . $trp][$ik];
                    $itenery_details['from_airport_name'] = $from_airport_name['airport_city'];
                    $itenery_details['to_airport_code'] = $flight_data['arr_loc_' . $trp][$ik];
                    $itenery_details['to_airport_name'] = $to_airport_name['airport_city'];
                    $itenery_details['departure_datetime'] = db_current_datetime(trim($flight_data['dep_date_' . $trp][$ik] . ' ' . $flight_data['dep_time_' . $trp][$ik]));
                    $itenery_details['arrival_datetime'] = db_current_datetime(trim($flight_data['arr_date_' . $trp][$ik] . ' ' . $flight_data['arr_time_' . $trp][$ik]));
                    $itenery_details['status'] = $status;
                    $itenery_details['operating_carrier'] = strtoupper($flight_data['career_' . $trp][$ik]);
                    $itenery_details['attributes'] = '';

                    $flg = $this->custom_db->insert_record('flight_booking_itinerary_details', $itenery_details);
                    // debug($itenery_details);
                }
                if ($trp == 'onward') {
                    $passenger_fk = array();
                    foreach ($flight_data['pax_title'] as $pk => $pv) {

                        $customer_details['app_reference'] = $app_reference;
                        //$customer_details['pax_index'] = $pk;
                        //$customer_details['segment_indicator'] = $segment_indicator;
                        //$customer_details['passenger_type'] = $flight_data['pax_type'][$pk];
                        $customer_details['flight_booking_transaction_details_fk'] = $flight_booking_transaction_details_fk;
                        $customer_details['is_lead'] = ($pk == 0) ? 1 : 0;
                        $customer_details['title'] = $pv;
                        $customer_details['first_name'] = $flight_data['pax_first_name'][$pk];
                        $customer_details['middle_name'] = '';
                        $customer_details['last_name'] = $flight_data['pax_last_name'][$pk];
                        $customer_details['date_of_birth'] = '0000-00-00';
                        if ($pv == 1 || $pv == 4) {
                            $customer_details['gender'] = 'Male';
                        } else {
                            $customer_details['gender'] = 'Female';
                        }
                        $customer_details['passenger_nationality'] = 'IN';
                        $customer_details['passport_number'] = $flight_data['pax_passport_num'][$pk];
                        $customer_details['passport_issuing_country'] = '';
                        $customer_details['passport_expiry_date'] = db_current_datetime($flight_data['pax_pp_expiry'][$pk]);
                        //$customer_details['ff_no'] = $flight_data['pax_ff_num'][$pk];
                        //$customer_details['ticket_no'] = $flight_data['pax_ticket_num_'.$trp][$pk];
                        $customer_details['status'] = $status;
                        $k = 0;
                        if ($flight_data['pax_type'][$pk] == 'Child') {
                            $k == 1;
                        } else if ($flight_data['pax_type'][$pk] == 'Infant') {
                            $k = 2;
                        }
                        $pax_basic = $flight_data['pax_basic_fare_' . $trp][$k];
                        $pax_yq = $flight_data['pax_yq_' . $trp][$k];
                        $pax_other = $flight_data['pax_other_tax_' . $trp][$k];
                        $pax_total = $pax_basic + $pax_yq + $pax_other;

                        $attr['price_breakup'] = array('base_price' => $pax_basic, "yq" => $pax_yq, 'tax' => $pax_other, 'total_price' => $pax_total);
                        $customer_details['attributes'] = json_encode($attr);

                        // debug($customer_details);
                        $flg = $this->custom_db->insert_record('flight_booking_passenger_details', $customer_details);
                        $passenger_fk[] = @$flg['insert_id'];


                        # Storing Ticket Details on flight_passenger_ticket_info
                    }
                }
                $pax_ticket_num_onward = $flight_data['pax_ticket_num_onward'];
                $pax_basic_fare_onward = $flight_data['pax_basic_fare_onward'];
                $pax_other_tax_onward = $flight_data['pax_other_tax_onward'];
                $pax_total_fare_onward = $flight_data['pax_total_fare_onward'];
                $pax_type_count_onward = $flight_data['pax_type_count_onward'];

                $pax_ticket_num_return = $flight_data['pax_ticket_num_return'];
                $pax_basic_fare_return = $flight_data['pax_basic_fare_return'];
                $pax_other_tax_return = $flight_data['pax_other_tax_return'];
                $pax_total_fare_return = $flight_data['pax_total_fare_return'];
                $pax_type_count_return = $flight_data['pax_type_count_return'];
                // debug($flight_data);
                if ($trp == 'onward') {
                    foreach ($pax_ticket_num_onward as $key => $value) {
                        if ($flight_data['pax_type'][$key] == 'Adult') {
                            $basic_fare = $pax_basic_fare_onward[0];
                            $tax = $pax_other_tax_onward[0];
                            $total_price = $pax_total_fare_onward[0] / $pax_type_count_onward[0];
                        } else if ($flight_data['pax_type'][$key] == 'Child') {
                            $basic_fare = $pax_basic_fare_onward[1];
                            $tax = $pax_other_tax_onward[1];
                            $total_price = $pax_total_fare_onward[1] / $pax_type_count_onward[1];
                        } else {
                            $basic_fare = $pax_basic_fare_onward[2];
                            $tax = $pax_other_tax_onward[2];
                            $total_price = $pax_total_fare_onward[2] / $pax_type_count_onward[1];
                        }
                        if ($flight_data['pax_type'][$key] == 'Adult') {
                            $basic_fare += $pax_basic_fare_return[0];
                            $tax += $pax_other_tax_return[0];
                            $total_price += $pax_total_fare_return[0] / $pax_type_count_return[0];
                        } else if ($flight_data['pax_type'][$key] == 'Child') {
                            $basic_fare += $pax_basic_fare_return[1];
                            $tax += $pax_other_tax_return[1];
                            $total_price += $pax_total_fare_return[1] / $pax_type_count_return[1];
                        } else {
                            $basic_fare += $pax_basic_fare_return[2];
                            $tax += $pax_other_tax_return[2];
                            $total_price += $pax_total_fare_return[2] / $pax_type_count_return[1];
                        }

                        $data['TicketNumber'] = $value;
                        $data['passenger_fk'] = $passenger_fk[$key];
                        $attr = array('BasePrice' => $basic_fare, 'Tax' => $tax, 'TotalPrice' => $total_price);

                        $data['Fare'] = json_encode($attr);
                        // debug($data);
                        $insert_id = $this->custom_db->insert_record('flight_passenger_ticket_info', $data);
                        // return false;
                    }
                }
              

                $trp = 'return';
            }
        }

        


        $this->load->model('domain_management_model');

        if (@$flight_booking_transaction_details_fk > 0 && $tot_agent_buying_price > 0) {



            $this->deduct_flight_booking_amount($app_reference, $domain_details['origin']);

          
        }

        $this->domain_management_model->create_track_log($app_reference, 'Offline Booking - Flight');
        //debug($flight_data);
    }

    public function deduct_flight_booking_amount($app_reference, $domain_origin) {
        //  echo $app_reference;exit;
        $ci = & get_instance();
        $condition = array();
        $condition['app_reference'] = $app_reference;
        $condition['sequence_number'] = @$sequence_number;

        $data = $ci->db->query('select BD.currency,BD.currency_conversion_rate,FT.* from flight_booking_details BD
						join flight_booking_transaction_details FT on BD.app_reference=FT.app_reference
						where FT.app_reference="' . trim($app_reference) . '"'
                )->result_array();

        if (valid_array($data) == true && in_array($data[0]['status'], array('BOOKING_CONFIRMED')) == true) {//Balance Deduction only on Confirmed Booking
            $ci->load->library('booking_data_formatter');
            $transaction_details = $data;

            $agent_buying_price = $ci->booking_data_formatter->agent_buying_price($transaction_details);

            $agent_buying_price1 = '';
            foreach ($agent_buying_price as $key => $price) {
                $agent_buying_price1 += $price;
            }

            $domain_booking_attr = array();
            $domain_booking_attr['app_reference'] = $app_reference;
            $domain_booking_attr['transaction_type'] = 'flight';
            $domain_booking_attr['currency_conversion_rate'] = $transaction_details[0]['currency_conversion_rate'];

            //Deduct Domain Balance
            $credential_type = $this->config->item('flight_engine_system');

            $ci->domain_management->debit_domain_balance_hold_booking($agent_buying_price1, $credential_type, $domain_origin, $domain_booking_attr); //deduct the domain balance
            //Save to Transaction Log
            $domain_markup = $transaction_details[0]['domain_markup'];
            $level_one_markup = 0;
            $agent_transaction_amount = $agent_buying_price1 - $domain_markup;
            $currency = $transaction_details[0]['currency'];
            $currency_conversion_rate = $transaction_details[0]['currency_conversion_rate'];
            $remarks = 'flight Transaction was Successfully done';

            $ci->domain_management_model->save_transaction_details('flight', $app_reference, $agent_transaction_amount, $domain_markup, $level_one_markup, $remarks, $currency, $currency_conversion_rate, $domain_origin);
        }
    }

    public function offline_airline_list($code = '', $agent_data = '') {
        $ci = &get_instance();
        if ($code != '') {
            $hold_airline_list = 'SELECT * FROM hold_airline_list where code="' . $code . '" and domain_origin="' . $agent_data . '"';
        } else {
            $hold_airline_list = 'SELECT * FROM hold_airline_list';
        }
        return $hold_airline_list = $ci->db->query($hold_airline_list)->result();
    }
    
    public function gst_state_information($domain_origin) {
         $data=array();
         $user = 'SELECT * FROM user where domain_list_fk="' . $domain_origin . '"';
         $user_state_code = $this->db->query($user)->result_array();
         $state_code=$user_state_code[0]['state'];
         if($user_state_code[0]['country_code']==92)
         {
            $country="India"; 
         }
         $state_name = 'SELECT * FROM state_list where origin="' . $state_code . '"';
         $state_name_details = $this->db->query($state_name)->result_array();
         
         $data['state']=$state_name_details[0]['en_name'];
         $data['country']=$country;
         
         return $data;
         
    }

    /**
     * get agent commission details
     */
    function get_master_commission_details($ModuleType = '') {

        $ci = &get_instance();
        if ($ModuleType != '') {
            $tds_sql = 'SELECT * FROM `commission_master` where module_type="' . $ModuleType . '"';
        } else {
            $tds_sql = 'SELECT * FROM `commission_master` ';
        }

        $tds_result = $ci->db->query($tds_sql)->row_array();
        return array('tds_tax_details' => $tds_result);
    }

    /*
     * Checking amount got debited or not
     */

    function check_transaction_logs($app_reference) {
        $ci = &get_instance();
        //$app_reference='FB28-130640-71489';
        $transaction_logs = 'SELECT * FROM transaction_log where app_reference="' . $app_reference . '"';
        return $tds_result = $ci->db->query($transaction_logs)->row_array();
    }

    /*
     * Checking amount got debited or not
     */

    function check_transaction_logs_details($app_reference) {
        $ci = &get_instance();
        //$app_reference='FB28-130640-71489';
        $transaction_logs = 'SELECT * FROM transaction_log where app_reference="' . $app_reference . '"';
        return $tds_result = $ci->db->query($transaction_logs)->result_array();
    }
}
