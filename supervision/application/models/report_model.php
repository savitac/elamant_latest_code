<?php
/**
 * @package    Provab Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
interface Report_Model
{
	public function booking($condition, $count, $offset, $limit);
}