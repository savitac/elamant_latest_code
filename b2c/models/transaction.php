<?php
/**
 * Library which has generic functions to get data
 *
 * @package    Provab Application
 * @subpackage Flight Model
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class Transaction extends CI_Model
{
	/**
	 * Lock All the tables necessary for flight transaction to be processed
	 */
	public static function lock_tables()
	{
		echo 'Under Construction';
	}

	/**
	 * Create Payment record for payment gateway used in the application
	 */
	public function create_payment_record($app_reference, $booking_fare, $firstname, $email, $phone, $productinfo, $convenience_fees=0, $promocode_discount=0, $currency_conversion_rate)
	{
		$duplicate_pg = $this->read_payment_record($app_reference);
		if ($duplicate_pg == false) {
			$payment_gateway_currency = $this->config->item('payment_gateway_currency');
			$request_params = array('txnid' => $app_reference,
				'booking_fare' => $booking_fare,
				'convenience_amount' => $convenience_fees,
				'promocode_discount' => $promocode_discount,
				'firstname' => $firstname,
				'email'=> $email,
				'phone'=> $phone,
				'productinfo'=> $productinfo);
			//Add total amount and remove discount from total amount
			$data['amount'] = roundoff_number($booking_fare+$convenience_fees-$promocode_discount);
			$data['domain_origin'] = get_domain_auth_id();
			$data['app_reference'] = $app_reference;
			$data['request_params'] = json_encode($request_params);
			$data['currency'] = $payment_gateway_currency;
			$data['currency_conversion_rate'] = $currency_conversion_rate;
			$data['created_datetime'] = date('Y-m-d H:i:s');
			$this->custom_db->insert_record('payment_gateway_details', $data);
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Read Payment record with payment gateway reference
	 * @param $app_reference
	 */
	function read_payment_record($app_reference)
	{
		$cond['app_reference'] = $app_reference;
		$data = $this->custom_db->single_table_records('payment_gateway_details', '*', $cond);
		if ($data['status'] == SUCCESS_STATUS) {
			return $data['data'][0];
		} else {
			return false;
		}
	}
	/**
	 *PAWAN BAGGA
	 * Read CCAVENUE Payment record with payment gateway reference
	 * @param $app_reference
	 */
	
	function read_ccavenue_payment_record($app_reference)
	{  
		$cond['app_reference'] = $app_reference;   
		$data = $this->custom_db->single_table_records('ccavenue_pg_record', '*', $cond);  
		if ($data['status'] == SUCCESS_STATUS) {
			return $data['data'][0];
		} else {
			return false;
		}
	}

	/**
	 * Update Payment record with payment gateway reference
	 * @param $app_reference
	 */
	function update_payment_record_status($app_reference, $status, $response_params=array())
	{
		$cond['app_reference'] = $app_reference;
		$data['status'] = $status;
		if (valid_array($response_params) == true) {
			$data['response_params'] = json_encode($response_params);
		}
		$this->custom_db->update_record('payment_gateway_details', $data, $cond);
	}

	/**
	 * Update additional details of transaction
	 */
	function update_convinence_discount_details($book_detail_table, $app_reference, $discount=0, $promo_code, $convinence=0, $convinence_value=0, $convinence_type=0, $convinence_per_pax=0)
	{
		$data = array();
		if (empty($discount) == false) {
			$data['discount'] = $discount;
		} else {
			$data['discount'] = 0;
		}
		if (empty($promo_code) == false) {
			$data['promo_code'] = $promo_code;
		} else {
			$data['promo_code'] = '';
		}
		if (empty($convinence) == false) {
			$data['convinence_amount'] = $convinence;
		}

		if (empty($convinence_value) == false) {
			$data['convinence_value'] = $convinence_value;
		}

		if (empty($convinence_type) == false) {
			$data['convinence_value_type'] = $convinence_type;
		}

		if (empty($convinence_per_pax) == false) {
			$data['convinence_per_pax'] = $convinence_per_pax;
		}

		$cond['app_reference'] = $app_reference;
		$this->custom_db->update_record($book_detail_table, $data, $cond);

	}

	/*
	 * Sachin
	 * Returns the Payment Status
	 */
	public function get_payment_status($app_reference)
	{
		$this->db->select('status');
		$this->db->where('app_reference =', trim($app_reference));
 		$this->db->from('payment_gateway_details');
 		return $this->db->get()->row_array();
	}

	/**
	 * Unlock All The Tables
	 */
	public static function release_locked_tables()
	{
		$CI = & get_instance();
		$CI->db->query('UNLOCK TABLES');
	}
	public function getActivePG(){
		$strPGCrs = META_PAYMENTGATEWAY_COURSE;
		$this->db->select('*');
		$this->db->where('meta_course_list_id', $strPGCrs);
		$this->db->where('booking_engine_status', '1');
		$this->db->from('booking_source');
		$result = $this->db->get()->result_array();
		return $result;
	}

	public function updateTelrData($strParms,$retData,$strCartID,$strType){
		if($strType == "create"){
			$data['create_request'] 	= $strParms;
			$data['create_response'] 	= $retData;
			$cond['origin'] 			= $strCartID;
			$strTable 					= "payment_gateway_details";
			$this->custom_db->update_record($strTable, $data, $cond);
		}
		if($strType == "check"){
			$data['status_request'] 	= $strParms;
			$data['status_response'] 	= $retData;
			$cond['origin'] 			= $strCartID;
			$strTable 					= "payment_gateway_details";
			$this->custom_db->update_record($strTable, $data, $cond);
		}
		return true;
	}

	function updateTelrOrderRef($Ordref,$strBokOrg,$Ordurl,$strCartID){
		$data['order_ref'] 	= $Ordref;
		$data['order_url'] 	= $Ordurl;
		$data['book_origin']= $strBokOrg;
		$cond['origin'] 	= $strCartID;
		$strTable 			= "payment_gateway_details";
		$this->custom_db->update_record($strTable, $data, $cond);		
		return true;
	}

	public function updatePayTabData($reqParms=array(),$respData,$origin_id,$book_origin,$strType){
		if($strType == "create"){
			$data['create_request'] 	= $reqParms;
			$data['create_response'] 	= $respData;
			$data['book_origin'] 		= $book_origin;
			$cond['origin'] 			= $origin_id;
			$strTable 					= "payment_gateway_details";
			$this->custom_db->update_record($strTable, $data, $cond);
		}
		return true;
	}

	function updatePayTabOrderRef($app_reference, $status, $response_params=array(),$payment_ref)
	{
		$data['status'] 		 = $status;
		$data['response_params'] = $response_params;
		$data['order_ref'] 		 = $payment_ref;
		$cond['app_reference'] 	 = $app_reference;

		$this->custom_db->update_record('payment_gateway_details', $data, $cond);
		return true;
	}

	function read_paymentgateway_record($strCID){
		$this->db->select('*');
		$this->db->where('origin', $strCID);
		$this->db->from('payment_gateway_details');
		$result = $this->db->get()->result_array();
		return $result;
	}

	function updateTelrOrderStatus($strOrderStatus,$strOrderRef,$strCartID){
		$data['status'] 	= $strOrderStatus;
		$cond['order_ref'] 	= $strOrderRef;
		//$cond['origin'] 	= $strCartID;
		$strTable 			= "payment_gateway_details";
		$this->custom_db->update_record($strTable, $data, $cond);		
		return true;
	}

	function update_tracking_id($book_id,$tracking_id)
	{
		$this->db->where("app_reference",$book_id);
		$this->db->update('ccavenue_pg_record',array("tracking_id"=>$tracking_id));
		return true;
	}

	function check_tracking_id($tracking_id)
	{
		$this->db->select('*');
		$this->db->where('tracking_id', $tracking_id);
		$this->db->from('ccavenue_pg_record');
		$result = $this->db->get();
		// echo $this->db->last_query();exit;
		if($result->num_rows()>0)
		{
			return false;
		}else{
			return true;
		}
		
	}
	 ////////for rewards point//////////////////////////////////////
    public function user_reward_details($user_id){
		         $this->db->where(array('user_id'=>$user_id)); 
		         $this->db->select('*');
		$query = $this->db->get('user');
		if($query){
			return $query->row_array(); 
		}else{
			return FALSE;
		}
	}

	public function get_reward_details(){
		         $this->db->select('*');
		$query = $this->db->get('rewards');
		if($query){
			return $query->result_array(); 
		}else{
			return FALSE;
		}
	}
	public function update_reward_report($data_rewards,$app_reference){
		$this->db->where(array('book_id'=>$app_reference));
		$this->db->update('rewards_report',$data_rewards);
	}
	public function update_reward_report_user($data,$user_id){
		$this->db->where(array('user_id'=>$user_id));
		$this->db->update('user',$data);
	}
	public function get_reward_range(){
		$this->db->select('*');
		$query = $this->db->get('reward_range');
		if($query->num_rows()){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function update_reward_record($id,$data){
		$this->db->where(array('user_id'=>$id));
		if($this->db->update('user',$data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
