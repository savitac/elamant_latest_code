<?php

/**
 * Nikhil Das s
 * Library for rewards system
 */
class Rewards{
	// private $CI;
	protected $CI;
	function __construct(){
		 $this->CI = &get_instance();
		 $this->CI->load->database();
	}
	
	//To get pending rewards based on user
	public function usable_rewards($user_id,$module,$usable_rewards){
		$this->CI->db->where(array('user_id'=>$user_id)); 
		$this->CI->db->select('user_id,general_reward, pending_reward, spefic_reward, used_reward');
		$query = $this->CI->db->get('user');
		// debug($query->result_array());exit();
		if($query){
			$re = $query->row_array();
			$pending_rewards = $re['pending_reward'];
			///usable rewards takes as percentage
			$rewards_usable = round($pending_rewards*$usable_rewards)/100;
			// debug($rewards_usable);exit();
			return $rewards_usable;
		}else{
			return FALSE;
		}
	}
	//To get the convertion and limit details
    public function get_reward_coversion_and_limit_details(){
		$this->CI->db->select('*');
		$query = $this->CI->db->get('rewards');
		if($query){
			return $query->result_array(); 
		}else{
			return FALSE;
		}
		}
		//To find usable reward of user
		public function find_reward_amount($usable_rewards){

			$re = $this->get_reward_coversion_and_limit_details();
			$convertor = $re[0]['currency_value']/$re[0]['reward_point'];
			$amount = $convertor*$usable_rewards;
			return $amount;
		} 
		//to update the reward_report
		public function update_reward_report_data($id,$data){
			$this->CI->db->where(array('user_id'=>$id));
			if($this->CI->db->insert('rewards_report',$data)){
				// echo $this->db->last_query();exit();
				return TRUE;
			}else{
				return FALSE;
			}
		}
		//to update the user table
		public function update_reward_record($id,$data){
		$this->CI->db->where(array('user_id'=>$id));
		if($this->CI->db->update('user',$data)){
			return TRUE;
		}else{
			return FALSE;
		}
	    }

	    //for rewards
	public function get_reward_report($id){
		
		$this->CI->db->where(array('user_id'=>$id));
		$this->CI->db->select('*');
		$query = $this->CI->db->get('rewards_report');
	    if($query){
	    	return $query->result_array();
	    }else{
	    	return FALSE;	
	    }
	   }//end 
	   public function get_reward_report_module($id){
		
		$this->CI->db->where(array('rewards_report.user_id'=>$id));
		$this->CI->db->select('rewards_report.*');
		$this->CI->db->from('rewards_report');
		//$this->CI->db->join('booking_global','booking_global.user_id=rewards_report.user_id',"left");
		$this->CI->db->group_by('rewards_report.id');
		$query = $this->CI->db->get();
	    if($query){
	    	// debug($query->result_array());exit();
	    	
	    	return $query->result_array();

	    }else{
	    	return FALSE;	
	    }
	   }//end 
		
public function page_data_reward_details($module,$price,$convenience_fee=0,$admin_markup=0){
        $user_id = $this->CI->entity_user_id;
		$page_data ['total_price'] = $price+$convenience_fee+$admin_markup;
		//debug($convenience_fee);exit();
		$page_data['reward_earned'] = $this->find_reward($module,$page_data ['total_price']);
	    //debug($page_data['reward_earned']);exit();
		$reward_details = $this->get_reward_coversion_and_limit_details();
		$usable_rewards = $page_data['reward_earned'];
		$usable_rewards = $this->usable_rewards($user_id,$module,$usable_rewards);
		// debug($usable_rewards);exit('reward lib 107');
		if($reward_details[0]['reward_min']<=$usable_rewards && $reward_details[0]['reward_max']>=$usable_rewards){
			$page_data['reward_usable'] = $usable_rewards;
		}else{
			$page_data['reward_usable'] = 0;
		}
		if($page_data['reward_usable']){
			$reducing_amount = $this->find_reward_amount($page_data['reward_usable']);	
			$page_data ['total_price_with_rewards'] = $page_data ['total_price']-$reducing_amount;
			$page_data['reward_usable_amount']=round($reducing_amount);
		}
		return $page_data;
	}

	//To fetch the reward range based on module
	public function find_reward($module,$amount){
		 // echo $module;exit();
		$flag = FALSE;
		$condition = array('module'=>$module,
			'status'=>1,
		);
		// debug($condition);exit();
		$this->CI->db->select('*');
		$this->CI->db->where($condition);
		$query = $this->CI->db->get('reward_range');
		$result = $query->result_array();
		// debug($result);exit();
		foreach ($result as $key => $value) {
			if($value['reward_from']<=$amount && $value['reward_to']>=$amount){
				$flag = TRUE;
				$reward_earned = $flag ? $value['reward_value'] : FLASE;
				return $reward_earned;
				break; 
			}
		}
	}//end	
	public function update_after_booking($temp_booking){

        $user_id = $this->CI->entity_user_id;
		$data_rewards = $this->user_reward_details($user_id);
		$pending_rewards = $data_rewards['pending_reward']+$temp_booking['book_attributes']['reward_earned']-$temp_booking['book_attributes']['reward_used'];
		$data_upadte_rewards = array(
			'pending_reward'=>round($pending_rewards),
			'used_reward'=>round($temp_booking['book_attributes']['reward_used']),
		);
		$module = $this->find_module_using_booking_source($temp_booking['book_attributes']['token']['booking_source']);
		$module = $this->find_module_name_using_meta_course_list_id($module);
		$data_upadte_rewards_report = array(
			'pending_rewardpoint'=>$pending_rewards,
			'used_rewardpoint'=>round($temp_booking['book_attributes']['reward_used']),
			'earned_rewardpoint'=>round($temp_booking['book_attributes']['reward_earned']),
			'user_id'=>$user_id,
			'module'=>$module,
			'created'=>date('Y-m-d h:i:s')
		);
		$this->update_reward_report_data($user_id,$data_upadte_rewards_report);
		$re = $this->update_reward_record($user_id,$data_upadte_rewards);
		if($re){
			return TRUE;
		}else{
			return FALSE;
		}

	}
	public function find_module_using_booking_source($booking_source=""){
		$this->CI->db->select('	meta_course_list_id');
		$this->CI->db->where(array('source_id'=>$booking_source));
		$query = $this->CI->db->get('booking_source');
		$res =$query->result_array();
		return $res[0]['meta_course_list_id'];
	}
	public function find_module_name_using_meta_course_list_id($module_id=""){
		$this->CI->db->select('name as module');
		$this->CI->db->where(array('course_id'=>$module_id));
		$query = $this->CI->db->get('meta_course_list');
		$res =$query->result_array();
		return $res[0]['module'];
	}
	public function user_reward_details($user_id){
		$this->CI->db->where(array('user_id'=>$user_id)); 
		$this->CI->db->select('*');
		$query = $this->CI->db->get('user');
		if($query){
			return $query->row_array(); 
		}else{
			return FALSE;
		}
	}

}
