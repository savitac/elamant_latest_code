<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['master_module_list']	= array(
META_AIRLINE_COURSE => 'flights',
META_TRANSFER_COURSE => 'transfershotelbed',
META_ACCOMODATION_COURSE => 'hotels',
META_BUS_COURSE => 'buses',
META_PACKAGE_COURSE => 'holidays',
META_SIGHTSEEING_COURSE=>'activities',
META_CAR_COURSE=>'car',
META_TRANSFERV1_COURSE=>'transfers'
);
/******** Current Module ********/
$config['current_module'] = 'b2c';

$config['load_minified'] = false;

$config['verify_domain_balance'] = false;

/******** PAYMENT GATEWAY START ********/
//To enable/disable PG
$config['enable_payment_gateway'] = true;
$config['active_payment_gateway'] = 'PAYU';
$config['active_payment_system'] = 'live';//test/live
$config['payment_gateway_currency'] = 'INR';//INR
/******** PAYMENT GATEWAY END ********/


/******** BOOKING ENGINE START ********/
$config['flight_engine_system'] = 'live'; //test/live
$config['hotel_engine_system'] = 'live'; //test/live
$config['bus_engine_system'] = 'live'; //test/live
$config['transfer_engine_system'] = 'live'; //test/live
$config['external_service_system'] = 'live'; //test/live
$config['sightseeing_engine_system'] = 'live';
$config['car_engine_system'] = 'test'; //test/live
//$config['sightseeing_engine_system'] = 'test';

$config['domain_key'] = CURRENT_DOMAIN_KEY;
$config['test_username'] = 'test229267';
$config['test_password'] = 'test@229';


$config['flight_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/flight/service/';
$config['hotel_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/hotel_v3/service/';

$config['bus_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/bus/service/';

$config['transferv1_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/transferv1/service/';


$config['sightseeing_url'] =  'http://prod.services.travelomatix.com/webservices/index.php/sightseeing/service/';

$config['car_url'] = 'http://test.services.travelomatix.com/index.php/webservices/car/service/';

if ($config['external_service_system'] == 'live') {
	$config['external_service'] = 'http://prod.services.travelomatix.com/webservices/index.php/rest/';
} else {

	$config['external_service'] = 'http://test.services.travelomatix.com/webservices/index.php/rest/';
}

$config['live_username'] = 'TMX241790';
$config['live_password'] = 'TMX@550241';

/******** BOOKING ENGINE END ********/


/**
 * 
 * Enable/Disable caching for search result
 */
$config['cache_hotel_search'] = true;//right now not needed
$config['cache_flight_search'] = false;
$config['cache_bus_search'] = true;
$config['cache_car_search'] = false;
$config['cache_sightseeing_search'] = true;
$config['cache_transferv1_search'] = true;

/**
 * Number of seconds results should be cached in the system
 */
$config['cache_hotel_search_ttl'] = 300;
$config['cache_flight_search_ttl'] = 300;
$config['cache_bus_search_ttl'] = 300;
$config['cache_car_search_ttl'] = 300;
$config['cache_sightseeing_search_ttl'] = 300;
$config['cache_transferv1_search_ttl'] = 300;

/*$config['lazy_load_hotel_search'] = true;*/
$config['hotel_per_page_limit'] = 20;
$config['car_per_page_limit'] = 200;
$config['sightseeing_page_limit'] = 50;
$config['transferv1_page_limit'] = 50;

/*
	search session expiry period in seconds
*/
$config['flight_search_session_expiry_period'] = 600;//600
$config['flight_search_session_expiry_alert_period'] = 300;//300
