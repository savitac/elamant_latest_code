<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @package    Provab
 * @subpackage General
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */
class General extends CI_Controller {
    /* function dxb_hotels()
      {
      $HotelSearchResult = (json_decode(file_get_contents('DXB.json'), true));
      $table = '<table border=1>';
      $table .= '<tr>';
      $table .= '<th>Sno</th>';
      $table .= '<th>Hotel Name</th>';
      $table .= '<th>Location</th>';
      $table .= '</tr>';
      $hotel_list = array();
      foreach ($HotelSearchResult['HotelSearchResult']['HotelResults'] as $k => $v) {
      $hotel_list[$v['HotelName']] = $v['HotelAddress'];
      }
      file_put_contents('dxb_hotel.json', json_encode($hotel_list));
      $index = 1;
      ksort($hotel_list);
      foreach ($hotel_list as $k => $v) {
      $table .= '<tr>';
      $table .= '<td>'.($index++).'</td>';
      $table .= '<td>'.$k.'</td>';
      $table .= '<td>'.$v.'</td>';
      $table .= '</tr>';
      }
      $table .= '</table>';
      } */
    /*
      function static_hotel()
      {
      $this->load->model('hotel_model');
      $search_response = $this->hotel_model->get_static_response(28);
      debug($search_response);
      exit;
      }

      function static_bus()
      {
      $this->load->model('bus_model');
      $search_response = $this->bus_model->get_static_response(1);
      debug($search_response);
      exit;
      }

      function static_bus_seats()
      {
      $this->load->model('bus_model');
      $search_response = $this->bus_model->get_static_response(2);
      debug($search_response);
      exit;
      }

      function static_flight()
      {
      $this->load->model('flight_model');
      $search_response = $this->flight_model->get_static_response(533);
      debug($search_response);
      exit;
      } */

    public function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        $this->load->model('user_model');
        $this->load->model('Package_Model');
        $this->load->model('custom_db');
    }

    function test1() {
        $post = $this->input->post();
        $post['lang'] = 'hi';
        $this->session->set_userdata('lang', $post['lang']);
        //echo $this->session->userdata('some_name');
    }

    /**
     * index page of application will be loaded here
     */
    function index($default_view = '') {
        /* Package Data */
        $data['caption'] = $this->Package_Model->getPageCaption('tours_packages')->row();
        $data['packages'] = $this->Package_Model->getAllPackages();
        $data['countries'] = $this->Package_Model->getPackageCountries_new();
        $data['package_types'] = $this->Package_Model->getPackageTypes();
        $page_data['package_list'] = $this->custom_db->single_table_records ( 'package_types', '*', array(), 0, 6, array (
            'package_types_id' => 'ASC'
        ));
        /* Banner_Images */
        $domain_origin = get_domain_auth_id();
        $page_data['banner_images'] = $this->custom_db->single_table_records('banner_images', '*', array('added_by' => $domain_origin, 'status' => '1'), '', '100000000', array('banner_order' => 'ASC'));
        /* Package Data */
        //echo $this->db->last_query();exit;
        //debug($page_data['banner_images']);exit;
        //debug($data);exit;
         if(isset($_GET['default_view'])){

        $page_data['default_view'] = @$_GET['default_view'];
        }else{
          $url_data = explode("=", $default_view);
        $page_data['default_view'] = @$url_data[1];
        }
        $page_data['holiday_data'] = $data; //Package Data

        if (is_active_airline_module()) {
            $this->load->model('flight_model');
            $page_data['top_destination_flight'] = $this->flight_model->flight_top_destinations();
        }
        if (is_active_bus_module()) {
            $this->load->model('bus_model');
        }
        if (is_active_hotel_module()) {
            $this->load->model('hotel_model');
            $page_data['top_destination_hotel'] = $this->hotel_model->hotel_top_destinations();

            
        }
        // debug( $page_data['top_destination_hotel']);
        // exit("120 - general");
        
         if (is_active_car_module()) {
          $this->load->model('car_model');
         }
        //debug($page_data);exit;
        if (is_active_package_module()) {
            $this->load->model('package_model');
            $top_package = $this->package_model->get_package_top_destination();
            $page_data['top_destination_package'] = $top_package['data'];
            // debug($page_data['top_destination_package']);
            // exit;
            $page_data['total'] = $top_package['total'];
        }
        // debug($page_data['top_destination_package']);
        // exit;
        $currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
        $page_data['currency_obj'] = $currency_obj;
        $getSlideImages = $page_data['banner_images']['data'];
        //debug($getSlideImages);exit;
        $slideImageArray = array();

        foreach ($getSlideImages as $k) {
            $slideImageArray[] = array('image' => $GLOBALS['CI']->template->template_images() . $k['image'], 'title' => $k['title'], 'description' => $k['subtitle']);
        }
        $page_data['slideImageJson'] = $slideImageArray;

        //blog 

        $filter = array (
        'blog_status' => ACTIVE
        );
        
        $blog_list = $this->custom_db->single_table_records ( 'blog', '*', $filter, 0, 100000, array (
            'blog_status' => 'DESC',
            'blog_title' => 'ASC'
        ) );


        $page_data['blog_list'] = $blog_list['data'];

         $deal_filter = array (
        'deal_status' => ACTIVE
        );
        $deal_banner = $this->custom_db->single_table_records ( 'deals_banner', '*', $deal_filter, 0, 100000, array (
            'deal_status' => 'DESC',
            'deal_id' => 'DESC'
        ) );


        $page_data['deal_banner'] = $deal_banner['data'][0];
         $deal_filter = array (
        'deal_status' => ACTIVE
        );

         // debug($page_data['deal_banner']);exit("172");

        $deals = $this->custom_db->single_table_records ( 'deals', '*', $deal_filter, 0, 100000, array (
        'deal_status' => 'DESC',
        'deal_name' => 'ASC' 
        ) );
         $page_data['deals'] = $deals['data'];

         $domain_details = $this->custom_db->single_table_records ( 'domain_list', '*', '', 0, 100000, array () );

         $page_data['domain_details'] = $domain_details['data'][0];

        //$slideImageJson = json_encode($slideImageArray);
        //$page_data['slideImageJson'] = $array_final = preg_replace('/"([a-zA-Z]+[a-zA-Z0-9_]*)":/','$1:',$slideImageJson);
        //debug($data['slideImageJson']);exit;
        /* header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
          header("Cache-Control: post-check=0, pre-check=0", false);
          header("Pragma: no-cache"); */
        // debug($page_data);exit;
        $this->template->view('general/index', $page_data);
    }

    /**
     * Set Search id in cookie
     */
    private function save_search_cookie($module, $search_id) {
        $sparam = array();
        $sparam = $this->input->cookie('sparam', TRUE);
        if (empty($sparam) == false) {
            $sparam = unserialize($sparam);
        }
        $sparam[$module] = $search_id;

        $cookie = array(
            'name' => 'sparam',
            'value' => serialize($sparam),
            'expire' => '86500',
            'path' => PROJECT_COOKIE_PATH
        );
        $this->input->set_cookie($cookie);
    }

    /**
     * Pre Search For Flight
     */
    function pre_flight_search($search_id = '') {
        $search_params = $this->input->get();
        // debug($search_params);exit();
        //Global search Data
        $search_id = $this->save_pre_search(META_AIRLINE_COURSE);
        $this->save_search_cookie(META_AIRLINE_COURSE, $search_id);

        //Analytics
        $this->load->model('flight_model');
        $this->flight_model->save_search_data($search_params, META_AIRLINE_COURSE);
        redirect('flight/search/' . $search_id . '?' . $_SERVER['QUERY_STRING']);
    }
    /**
     * Pre Search For Car
     */
    function pre_car_search($search_id = '') {
        $search_params = $this->input->get();
        // debug($search_params);exit;
        //Global search Data
        $search_id = $this->save_pre_search(META_CAR_COURSE);
        $this->save_search_cookie(META_CAR_COURSE, $search_id);

        //Analytics
        $this->load->model('car_model');
        $this->car_model->save_search_data($search_params, META_CAR_COURSE);
        redirect('car/search/' . $search_id . '?' . $_SERVER['QUERY_STRING']);
    }
    /**
     * Pre Search For Hotel
     */
    function pre_hotel_search($search_id = '') {
       $search_params = $this->input->get();
      
        //Global search Data
        //debug($this->session);exit;
        $search_id = $this->save_pre_search(META_ACCOMODATION_COURSE);
        $this->save_search_cookie(META_ACCOMODATION_COURSE, $search_id);
        // debug($search_id);exit("1212");
        //Analytics
        $this->load->model('hotel_model');
       
        // debug($search_params);exit;
        $this->hotel_model->save_search_data($search_params, META_ACCOMODATION_COURSE);

        redirect('hotel/search/' . $search_id . '?' . $_SERVER['QUERY_STRING']);
    }

    /**
    *Pre Villa Search
    **/
      function pre_villa_search($search_id = '') {
       $search_params = $this->input->get();
       if($search_params['villa_checkin'] && $search_params['villa_checkout']){
        $search_params['hotel_checkin'] = $search_params['villa_checkin'];
        $search_params['hotel_checkout'] = $search_params['villa_checkout'];
        unset($search_params['villa_checkin']);
        unset($search_params['villa_checkout']);
       }
        //Global search Data
        //debug($this->session);exit;
        $search_id = $this->save_pre_search(META_ACCOMODATION_COURSE);
        $this->save_search_cookie(META_ACCOMODATION_COURSE, $search_id);
        // debug($search_id);exit("1212");
        //Analytics
        $this->load->model('hotel_model');
       
        // debug($search_params);exit;
        $this->hotel_model->save_search_data($search_params, META_ACCOMODATION_COURSE);

        redirect('hotel/search/' . $search_id . '?' . $_SERVER['QUERY_STRING']);
    }

    /**
     * Pre Search For Bus
     */
    function pre_bus_search($search_id = '') {
        //Global search Data
        $search_id = $this->save_pre_search(META_BUS_COURSE);
        $this->save_search_cookie(META_BUS_COURSE, $search_id);

        //Analytics
        $this->load->model('bus_model');
        $search_params = $this->input->get();
      
        $this->bus_model->save_search_data($search_params, META_BUS_COURSE);

        redirect('bus/search/' . $search_id . '?' . $_SERVER['QUERY_STRING']);
    }

    /**
     * Pre Search For Packages
     */
    function pre_package_search($search_id = '') {
        //Global search Data
        $search_id = $this->save_pre_search(META_PACKAGE_COURSE);
        redirect('tours/search' . $search_id . '?' . $_SERVER['QUERY_STRING']);
    }

    /**
  * Pre Search for SightSeen
  */
  function pre_sight_seen_search($search_id=''){

    $search_id = $this->save_pre_search(META_SIGHTSEEING_COURSE);
    $this->save_search_cookie(META_SIGHTSEEING_COURSE, $search_id);
    //Analytics
    $this->load->model('sightseeing_model');
    $search_params = $this->input->get();
    // debug($search_params);
    // exit;
    $this->sightseeing_model->save_search_data($search_params, META_SIGHTSEEING_COURSE);
    
    redirect('sightseeing/search/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
  }

  /*
  *Pre Transfer Search
  */
  function pre_transferv1_search($search_id=''){
    $search_id = $this->save_pre_search(META_TRANSFERV1_COURSE);
    $this->save_search_cookie(META_TRANSFERV1_COURSE, $search_id);
    //Analytics
    $this->load->model('transferv1_model');
    $search_params = $this->input->get();
    
    $this->transferv1_model->save_search_data($search_params, META_TRANSFERV1_COURSE);
    
    redirect('transferv1/search/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
  }

    /**
     * Pre Search For Transfer
     */
    function pre_transfer_search($search_id = '') {
        //Global search Data
        //debug($this->session);exit;
        $search_id = $this->save_pre_search(META_TRANSFER_COURSE);
        $this->save_search_cookie(META_TRANSFER_COURSE, $search_id);

        //Analytics
        $this->load->model('transfer_model');
        $search_params = $this->input->get();

       $this->transfer_model->save_search_data($search_params, META_TRANSFER_COURSE);

        redirect('transfer/search/' . $search_id . '?' . $_SERVER['QUERY_STRING']);
    }
    
    
    /**
     * Pre Search used to save the data
     *
     */
    private function save_pre_search($search_type) {
        //Save data
        $search_params = $this->input->get();
         
          if($search_params['hotel_destination'] ==8222){
        $search_params['hotel_destination'] = 715;
         $search_params['city'] = 'Bangkok (Thailand)';
       }
        if($search_params['villa_checkin'] && $search_params['villa_checkout']){
        $search_params['hotel_checkin'] = $search_params['villa_checkin'];
        $search_params['hotel_checkout'] = $search_params['villa_checkout'];
        unset($search_params['villa_checkin']);
        unset($search_params['villa_checkout']);
       }

        // debug($search_params);exit;
        $search_data = json_encode($search_params);
        $insert_id = $this->custom_db->insert_record('search_history', array('search_type' => $search_type, 'search_data' => $search_data, 'created_datetime' => date('Y-m-d H:i:s')));
        return $insert_id['insert_id'];
    }

    /**
     * oops page of application will be loaded here
     */
    public function ooops() {
        $this->template->view('utilities/404.php');
    }

    /*
     * Activating User Account.
     * Account get activated only when the url is clicked from the account_activation_mail
     */

    function activate_account_status() {
        $origin = $this->input->get('origin');
        $unsecure = substr($origin, 3);
        $secure_id = base64_decode($unsecure);
        $status = ACTIVE;
        $this->user_model->activate_account_status($status, $secure_id);
        redirect(base_url());
    }

    /**
     * Email Subscribtion
     *
     */
    public function email_subscription() {
        $data = $this->input->post();

        $mail = $data['subEmail'];
        $domain_key = get_domain_auth_id();
        $inserted_id = $this->user_model->email_subscribtion($mail, $domain_key);
        if (isset($inserted_id) && $inserted_id != "already") {
            $this->application_logger->email_subscription($mail);
            $pdata['status'] = 1;
            echo json_encode($pdata);
        } elseif ($inserted_id == "already") {
            $pdata['status'] = 0;
            echo json_encode($pdata);
        } else {
            $pdata['status'] = 2;
            echo json_encode($pdata);
        }
    }

    function cms($page_label) {
        $page_position = 'Bottom';

        if (isset($page_label)) {
            $data = $this->custom_db->single_table_records('cms_pages', 'page_title,page_description,page_seo_title,page_seo_keyword,page_seo_description', array('page_label' => $page_label, 'page_position' => $page_position, 'page_status' => 1));
            $domain_details = $this->custom_db->single_table_records ( 'domain_list', '*', '', 0, 100000, array () );
            $data['domain_details'] = $domain_details['data'][0];
            $this->template->view('cms/cms', $data);
        } else {
            redirect('general/index');
        }
    }

    function offline_payment() {
        $params = $this->input->post();
        $gotback = $this->user_model->offline_payment_insert($params);
        $url = base_url() . 'index.php/general/offline_approve/' . $gotback['refernce_code'];
        //unset($gotback['refernce_code']);
        //mail function
        /* $mail_template	=	'<!DOCTYPE html>
          <html>
          <head>
          <title>Offline payment Travelomatix</title>
          </head>
          <body style="background:#EBFCFD">
          <h3>Offline payment</h3>
          <strong>HI,'.$params["data"][0]["value"].' please click below link to confirm the offline payment.</strong>
          <p>'.$url.'</p>

          </body>
          </html>';
          $email = $params['data'][2]['value'];
          $this->load->library('provab_mailer');
          $this->provab_mailer->send_mail($email, 'OFFLINE PAYMENT CONFIRMATION', $mail_template); */

        print_r(json_encode($gotback['refernce_code']));
    }

    public function my_booking($module='')
    {
      // seo
      $pagedata['seo']['general']['header'] = "My Bookings List";
      $pagedata['seo']['general']['titel'] = "Manage my Bookings - ziphop.com";
      $pagedata['seo']['general']['desc'] = "Having already made your trip booking, use the form on this page if you want to change your travel booking details.";
      $pagedata['seo']['general']['keywords'] = "flight ticket booking,airline reservation system,online hotel booking system,book flight and hotel together,ziphop in canada.";
      
      $pagedata['seo']['general'] = json_encode($pagedata['seo']['general']);
      $pagedata['module'] = $module;
      $domain_details = $this->custom_db->single_table_records ( 'domain_list', '*', '', 0, 100000, array () );
      $pagedata['domain_details'] = $domain_details['data'][0];
      $this->template->view('general/my_booking',$pagedata);
    }
    public function getVoucher()
    { 
      $this->load->model('car_model');
      $all_post = $this->input->post();
      $module=$all_post['module'];      
      $book_id = $all_post['ticket_name'];
      $last_name = $all_post['last_name'];
       $modul_name = "";
            // debug(substr(trim($book_id), 0,strlen(B2C_HOTEL_HOTELBEDS_PREFIX)));die;
      //$book_id_explode = explode("-", $book_id);
       // debug(trim($book_id));
       // debug(substr(trim($book_id), 3,2));
       // debug(HOLIDAY_BOOKING);exit;
      if((substr(trim($book_id), 0,strlen(B2C_FLIGHT_SABRE_PREFIX)) == B2C_FLIGHT_SABRE_PREFIX) || (substr(trim($book_id), 0,strlen(B2C_FLIGHT_TRAVELPORT_PREFIX))) == B2C_FLIGHT_TRAVELPORT_PREFIX){
        $modul_name='flight';
      } else if ((substr(trim($book_id), 0,strlen(HOTEL_BOOKING)) == HOTEL_BOOKING) ) 
      {
        $modul_name='hotel';
      } else if (substr(trim($book_id), 0,strlen(B2C_CAR_CARNECT_PREFIX)) == B2C_CAR_CARNECT_PREFIX) {
        $modul_name='car';
      } else { 
       // die('aaaa');
        redirect ( base_url () . 'index.php/general/my_booking');
      }
      
      
      
      // die($modul_name);
      /*if(md5('flight')===$module)
      {
        $modul_name='flight';
      }elseif (md5('hotel')===$module) {
        $modul_name='hotel';
      }elseif (md5('car')===$module) {
        $modul_name='car';
      }elseif (md5('holiday')===$module) {
        $modul_name='holiday';        
      }else{ 
      redirect ( base_url () . 'index.php/general/my_booking');
    }*/
    //debug($modul_name);exit;
    if ($modul_name == "flight") {
      $pbook_id = explode("-", $book_id);
      //debug($pbook_id);die;
      $book_id = $pbook_id[0]."-".$pbook_id[1]."-".$pbook_id[2];
    }
    // die($book_id);die;
    if($modul_name){
      $modul_name_table = ($modul_name==='car')? 'Car': $modul_name;
      if($modul_name_table == "hotel"){
        $result=$this->custom_db->get_result_by_query('SELECT * FROM hotel_booking_details where app_reference="'.trim($book_id).'"');
        //$result=true;

        echo $this->db->last_query();
      }elseif($modul_name == "flight"){
        $result=$this->custom_db->get_result_by_query('SELECT * FROM flight_booking_details WHERE app_reference="'.trim($book_id).'" AND email="'.trim($all_post["last_name"]).'"');
        //echo $this->db->last_query();die;
      }elseif($modul_name == "holiday"){
        $result=$this->custom_db->get_result_by_query('SELECT * FROM tour_booking_details WHERE app_reference="'.trim($book_id).'" ');
      }else{
        $result=$this->custom_db->get_result_by_query('SELECT * FROM '.$modul_name_table.'_booking_details WHERE user_app_reference="'.trim($book_id).'"');
      }
        // debug($result);exit;
      // echo "$modul_name_table".$modul_name_table;
       // debug($this->db->last_query());die;
      if($result)
      {
        $result=json_decode(json_encode($result[0]),true);
        if ($modul_name == "flight") {
          redirect ( base_url () . 'index.php/voucher/flight_guest/' . trim($book_id) );
        }
        if($modul_name=='car')
        {
          $booking_details = $this->car_model->get_booking_details ( $result['app_reference'], $result['booking_source']);
          if(strtolower($booking_details['data']['booking_user_details'][0]['email']) != strtolower(trim($last_name)))
          {
            $this->session->set_flashdata($modul_name, '<span class="text-center text-danger msg">INVALID EMAIL ID.</span>');
            redirect ( base_url () . 'index.php/general/my_booking/flight');
          }else{
            $book_id = $result['app_reference'];            
            redirect ( base_url () . 'index.php/voucher/car_guest/' .trim($book_id).'/'.$result['booking_source'].'/'.$result['purchase_status'].'/show_voucher/no-mail' );
          }
        }elseif($modul_name=='holiday')
        {
          $this->load->model('tours_model');
          $condition [] = array(
            'BD.app_reference ',
            '= ',
            '"'.$result['app_reference'].'"',
            );
          $booking_details = $this->tours_model->booking($condition);
          $user_attributes=$booking_details['data'][$result['app_reference']]['booking_details']['user_attributes'];
          // debug(json_decode($user_attributes,true));die;
          $user_jsondecode = json_decode($user_attributes,true);
          // debug($last_name);
          // debug($user_jsondecode);exit;
          if(strtolower($user_jsondecode['billing_email']) != strtolower($last_name))
          {
            $this->session->set_flashdata($modul_name, '<span class="text-center text-danger msg">INVALID EMAIL ID.</span>');
            redirect ( base_url () . 'index.php/general/my_booking/flight');
          }else{
            $book_id = $result['app_reference'];            
            redirect ( base_url () . 'index.php/voucher/holiday_guest/' .trim($book_id).'/'.$result['status'].'/show_voucher/no-mail' );
          }
        }else{
          $this->load->model('hotel_model');
          // $booking_details = $this->hotel_model->get_booking_details ( $result['app_reference'], $result['booking_source']);
          // debug($booking_details);exit;
          // debug($result);
          // debug($last_name);
          if(strtolower($result['email']) != strtolower($last_name))
          {
            // echo "invalid";exit;
            $this->session->set_flashdata($modul_name, '<span class="text-center text-danger msg">INVALID EMAIL ID.</span>');
            redirect ( base_url () . 'index.php/general/my_booking');
          }else{
            // $book_id = $result['parent_pnr'];
            // debug($book_id);exit;
            redirect ( base_url () . 'index.php/voucher/hotel/' . $book_id);
          }
        }         
      }else{
        // exit("1");
        $this->session->set_flashdata($modul_name, '<span class="text-center text-danger msg">INVALID BOOKING REFERENCE OR EMAIL ID.</span>');
        redirect ( base_url () . 'index.php/general/my_booking/'.$modul_name);
      }
    }
  }

    function offline_approve($code) {//apporval by mail
        $result['data'] = $this->user_model->offline_approval($code);
        $this->template->view('general/pay', $result);
    }

    /**
     * Booking Not Allowed Popup
     */
    function booking_not_allowed() {
        $this->template->view('general/booking_not_allowed');
    }

    function test() {
        echo 'test function';
    }

  function update_citylist()
  {
    $total= 80;
    for($num=0;$num<=$total;$num++){
      $city_response = file_get_contents(FCPATH."test-export-2017-2-27/destinations-".$num.".json");
     
      $city_list = json_decode($city_response,true);
      // debug($city_list);exit;
      foreach ($city_list as $key => $value) {
        $insert_list['country_code'] = $value['country'];
        $insert_list['city_name'] = html_entity_decode($value['name']);
        $insert_list['city_code'] = $value['code'];
        $insert_list['parent_code'] = $value['parent'];
        $insert_list['latitude']  = $value['latitude'];
        $insert_list['longitude'] = $value['longitude'];
        $this->custom_db->insert_record('hotelspro_citylist',$insert_list);
      }
    }
      
  }

   public function plan_your_trip($module='')
    {
      // seo
   
      $this->template->view('general/plan_your_trip');
    }

    public function hot_deals(){
      error_reporting(1);
      $data = $this->input->get();
      if($data=="" || $data['category'] == "flight"){
        $filter = array (
        'deal_status' => ACTIVE
        );

        $flight_deals = $this->custom_db->single_table_records ( 'flight_deals', '*', $filter, 0, 100000, array (
            'deal_status' => 'DESC'
            // 'blog_title' => 'ASC'
        ) );
        $page_data['flight_deals'] = $flight_deals['data'];
      }

      if($data){
       $filter = array (
        'deal_status' => ACTIVE,
        'category' => $data['category']
        ); 
      }else{
       $filter = array (
        'deal_status' => ACTIVE
        );
      }

      if(isset($data) || $data['category'] != "flight"){
        $deals = $this->custom_db->single_table_records ( 'deals', '*', $filter, 0, 100000, array (
        'deal_status' => 'DESC',
        'deal_name' => 'ASC' 
        ) );
         $page_data['deals'] = $deals['data'];
      }

      $domain_details = $this->custom_db->single_table_records ( 'domain_list', '*', '', 0, 100000, array () );
      $page_data['domain_details'] = $domain_details['data'][0];
      // debug($pagedata['domain_details']);die;
        // $hotel_deal = $this->custom_db->single_table_records ( 'all_api_city_master', '*', $filter, 0, 100000, array (
        // 'deal_status' => 'DESC',
        // 'city_name' => 'ASC' 
        // ) );

        
       
        // $page_data['hotel_deals']= $hotel_deal['data'];

        // debug($page_data);
        // exit("605");
    $this->template->view('general/hot_deals',$page_data);	
    }

    public function tripidia_collection(){
           /* Package Data */
        $data['caption'] = $this->Package_Model->getPageCaption('tours_packages')->row();
        $data['packages'] = $this->Package_Model->getAllPackages();
        $data['countries'] = $this->Package_Model->getPackageCountries_new();
        $data['package_types'] = $this->Package_Model->getPackageTypes();
  

  
        $page_data['default_view'] = @$_GET['default_view'];
        $page_data['holiday_data'] = $data; //Package Data

        if (is_active_package_module()) {
            $this->load->model('package_model');
            $top_package = $this->package_model->get_package_top_destination();
            $page_data['top_destination_package'] = $top_package['data'];
            // debug($page_data['top_destination_package']);
            // exit;
            $page_data['total'] = $top_package['total'];
        }
        $currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
        $page_data['currency_obj'] = $currency_obj;
        
        // debug($page_data);
        // exit("606");
        
    $this->template->view('general/tripidia_collection',$page_data); 
    }

    /**
    * Blog
    **/

    public function blog(){
      $blog = $this->input->get();
      $blog_id = $blog['blog_id'];

      $result=$this->custom_db->get_result_by_query('SELECT * FROM blog where blog_id="'.trim($blog_id).'"');

      if($result)
        {
        $result=json_decode(json_encode($result[0]),true);
        $page_data['blog'] = $result;
      }else{
        redirect( base_url() );
      }
    
      // debug($page_data);
      // exit;
      $this->template->view('general/blog',$page_data); //

    }

    public function promo_details(){
      $get_data = $this->input->get();

      // debug($get_data);exit;

        $deal_filter = array (
        'deal_status' => ACTIVE,
        'deal_id'=>$get_data['deal']
        );

         // debug($page_data['deal_banner']);exit("172");

        $deals = $this->custom_db->single_table_records ( 'deals', '*', $deal_filter, 0, 100000, array () );
         $page_data['deals'] = $deals['data'][0];

      $this->template->view('general/promo_details',$page_data); //      
    }
}
