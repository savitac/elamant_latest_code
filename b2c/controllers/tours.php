<?php
if (! defined ( 'BASEPATH' ))
exit ( 'No direct script access allowed' );
class Tours extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$current_url = $_SERVER ['QUERY_STRING'] ? '?' . $_SERVER ['QUERY_STRING'] : '';
		$current_url = $this->config->site_url () . $this->uri->uri_string () . $current_url;
		$url = array (
				'continue' => $current_url 
		);
		$this->session->set_userdata ( $url );
		$this->helpMenuLink = "";
		$this->load->model ( 'Help_Model' );
		$this->helpMenuLink = $this->Help_Model->fetchHelpLinks ();
		$this->load->model ( 'Package_Model' );
	}

	/**
	 * get all tours
	 */
	public function index() {
		$data ['packages'] = $this->Package_Model->getAllPackages ();
		$data ['countries'] = $this->Package_Model->getPackageCountries ();
		$data ['package_types'] = $this->Package_Model->getPackageTypes ();
		if (! empty ( $data ['packages'] )) {
			$this->template->view ( 'holiday/tours', $data );
		} else {
			redirect ();
		}
	}
	/**
	 * get the package details
	 */
	public function details($package_id) {
		$data ['package'] = $this->Package_Model->getPackage ( $package_id );
		$data ['package_itinerary'] = $this->Package_Model->getPackageItinerary ( $package_id );
		$data ['package_price_policy'] = $this->Package_Model->getPackagePricePolicy ( $package_id );
		$data ['package_cancel_policy'] = $this->Package_Model->getPackageCancelPolicy ( $package_id );
		$data ['package_traveller_photos'] = $this->Package_Model->getTravellerPhotos ( $package_id );
		$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		$data['currency_obj'] = $currency_obj;
		if (! empty ( $data ['package'] )) {
			$this->template->view ( 'holiday/tours_detail', $data );
		} else {
			redirect ( "tours/index" );
		}
	}
	public function enquiry() {
		$package_id = $this->input->post ( 'package_id' );
		if ($package_id !='') {
			$data = $this->input->post ();
			$package = $this->Package_Model->getPackage ( $package_id );
			$data ['package_name'] = $package->package_name;
			$data ['package_duration'] = $package->duration;
			$data ['package_type'] = $package->package_type;
			$data ['with_or_without'] = $package->price_includes;
			$data ['package_description'] = $package->package_description;
			$data ['ip_address'] = $this->session->userdata ( 'ip_address' );
			$data ['status'] = '0';
			$data ['date'] = date ( 'Y-m-d' );
			$data ['domain_list_fk'] = get_domain_auth_id ();
			if($this->entity_user_id){
			$data ['created_by_id'] = $this->entity_user_id;
			$data ['user_type'] = $this->entity_user_type;
			}else{
			$data ['created_by_id'] = 0;
			$data ['user_type'] = 0;
			}
				// debug($data);exit;
			$result = $this->Package_Model->saveEnquiry ( $data );
			 $this->session->set_flashdata(array('message' => "Thank you for submitting your enquiry for this package, will get back to you soon", 'type' => SUCCESS_MESSAGE));
                       
               redirect('tours/details/'.$package_id);
		} else {
			redirect ();
		}
	}

	function temp_index($search_id)
	{
		$this->load->model('hotel_model');
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		// Get all the hotels bookings source which are active
		$active_booking_source = $this->hotel_model->active_booking_source();
		if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
			$safe_search_data['data']['search_id'] = abs($search_id);
			$this->template->view('tours/search_result_page', array('hotel_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
		} else {
			$this->template->view ( 'general/popup_redirect');
		}
	}

	public function search() {
		$data = $this->input->get ();
		$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		$domain_details = $this->custom_db->single_table_records ( 'domain_list', '*', '', 0, 100000, array () );
		// debug($data);exit;
		if (! empty ( $data )) {
			$country = $data ['country'];
			$packagetype = $data ['package_type'];
			if ($data ['duration']) {
				$duration = explode ( '-', $data ['duration'] );
				if (count ( $duration ) > 1) {
					$duration = "duration between " . $duration ['0'] . " AND " . $duration ['1'];
				} else {
					$duration = "duration >" . $duration ['0'];
				}
			} else {
				$duration = $data ['duration'];
			}
			//echo 'herre'.$duration;exit;
			
			if ($data ['budget']) {
				$budget = explode ( '-', $data ['budget'] );
				//$currecny_val = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $budget['0'] ) );
				//echo $currecny_val;exit;
				if (count ( $budget ) > 1) {
					$budget = "price between " . $budget ['0'] . " AND " . $budget ['1'];
				} else if ($budget [0]) {
					$budget = "price >" . $budget ['0'];
				}
			} else {
				$budget = $data ['budget'];
			}
			
			$domail_list_pk = get_domain_auth_id ();
			$data['currency_obj'] = $currency_obj;
			$data ['scountry'] = $country;
			$data ['spackage_type'] = $packagetype;
			$data ['sduration'] = $data ['duration'];
			$data ['sbudget'] = $data ['budget'];
			$data ['packages'] = $this->Package_Model->search ( $country, $packagetype, $duration, $budget, $domail_list_pk, $domail_list_pk );
			$data ['caption'] = $this->Package_Model->getPageCaption ( 'tours_packages' )->row ();
			$data ['countries'] = $this->Package_Model->getPackageCountries ();
			$data ['package_types'] = $this->Package_Model->getPackageTypes ();
			$data ['domain_details'] = $domain_details['data'][0];
			//debug($data);exit;
			$this->template->view ( 'holiday/tours', $data );
		} else {
			redirect ( 'tours/all_tours', array('domain_details' => $domain_details['data'][0]) );
		}
	}
	function package_user_rating() {
		$rate_data = explode ( ',', $_POST ['rate'] );
		$pkg_id = $rate_data [0];
		$rating = $rate_data [1];

		$arr_data = array (
				'package_id' => $pkg_id,
				'rating' => $rating 
		);
		$res = $this->Package_Model->add_user_rating ( $arr_data );
	}
	public function all_tours() {
		$data ['caption'] = $this->Package_Model->getPageCaption ( 'tours_packages' )->row ();
		$data ['packages'] = $this->Package_Model->getAllPackages ();
		$data ['countries'] = $this->Package_Model->getPackageCountries ();
		$data ['package_types'] = $this->Package_Model->getPackageTypes ();
		if (! empty ( $data ['packages'] )) {
			$this->template->view ( 'holiday/tours', $data );
		} else {
			redirect ();
		}
	}

	public function enquiry_package() {
		// echo 'herer I am';exit;
		//error_reporting(E_ALL);

		$data = $this->input->post ();
		// debug($data);
		$package_id = 0;
	
		if (($data['name']) && ($data['mobile']) && ($data['email']) && ($data['departurecity']) && ($data['message'])) {
			
			//$package = $this->Package_Model->getPackage ( $package_id );
			$data1 ['package_id']=$package_id;
			$data1 ['package_name'] = $data['package_name'];
			$data1 ['first_name'] = $data['name'];
			$data1 ['email'] = $data['email'];
			$data1 ['phone'] = $data['isd'].$data['mobile'];
			$data1 ['place'] = $data['departurecity'];
			$data1 ['message'] = $data['message'];
			$data1 ['package_type'] = 0;
			$data1['user_type']=B2C_USER;
			
			//$data ['package_type'] = $package->package_type;
			//$data ['with_or_without'] = $package->price_includes;
			$data1 ['package_description'] = $data['message'];
			
			$date=date_create($data['start_date']);
			$startdate = date_format($date,"Y-m-d");
			$data['days'] = $data['duration']+1;



			$findEndDate = date_add($date,date_interval_create_from_date_string($data['days']." days"));

			$end_date = date_format($findEndDate,"Y-m-d");
			
			$data1 ['start_date'] = $startdate;
			
			$data1 ['end_date'] = $end_date;
			
			$data1['package_duration'] =$data['duration'];
			$data1 ['num_passanger'] = $data['adult'] + $data['child'] ;
			$data1 ['budget'] = $data['budget'];
			$data1 ['num_adult'] = $data['adult'];
			$data1 ['num_child'] = $data['child'];
			$data1 ['num_nights'] = $data['duration'];
			$data1 ['num_days'] = $data['days'];

			$data1 ['ip_address'] = $this->session->userdata ( 'ip_address' );
			$data1 ['created_by_id'] = $this->entity_user_id;
			$data1 ['status'] = '0';
			$data1 ['date'] = date ( 'Y-m-d H:i:s' );
			$data1 ['domain_list_fk'] = get_domain_auth_id ();
			// debug($data1);exit;	
			$result = $this->Package_Model->saveEnquiry ( $data1 );
			//debug($result);exit();
			/*$status = true;
			$message = "Thank you for submitting your enquiry for this package, will get back to soon";
			header('content-type:application/json');
			echo json_encode(array('status' => $status, 'message' => $message));
			exit;*/
			  echo "<script>
                alert('Thank you for submitting your enquiry for this package, will get back to you soon');
            window.location.href='" . base_url() . "index.php';
                </script>";
			
		} 
	}
}
