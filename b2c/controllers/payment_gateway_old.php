<?php
if (! defined ( 'BASEPATH' ))
exit ( 'No direct script access allowed' );
/**
 *
 * @package Provab
 * @subpackage Transaction
 * @author Pravinkuamr P <pravinkumar.provab@gmail.com>
 * @version V1
 */
class Payment_Gateway extends CI_Controller {
	/**
	 *
	 */
	public function __construct() {
		parent::__construct ();
		// $this->output->enable_profiler(TRUE);
		$this->load->model ( 'module_model' );
		$this->load->model('transaction'); 
	}
	/**
	 * Blocked the payment gateway temporarly
	 */
	function demo_booking_blocked()
	{
		echo '<h1>Booking Not Allowed, This Is Demo Site. Go To <a href="'.base_url().'">Travelomatix</a></h1>';
	}
	/**
	 * Redirection to payment gateway
	 * @param string $book_id		Unique string to identify every booking - app_reference
	 * @param number $book_origin	Unique origin of booking
	 */
	public function payment($book_id,$book_origin)
	{
		//redirect('payment_gateway/demo_booking_blocked');//Blocked the payment gateway temporarly
		
		$this->load->model('transaction');
		$PG = $this->config->item('active_payment_gateway');     
		load_pg_lib ( $PG );
 
		$pg_record = $this->transaction->read_payment_record($book_id);  
		//Converting Application Payment Amount to Pyment Gateway Currency
		// debug($pg_record);exit;
		$ccavenue_pg_record = 
		array(
		'app_reference'  => $pg_record['app_reference'],
		'original_amount'=> $pg_record['amount'],
		'currency'       => $pg_record['currency'], 
		'conversion_rate'=> $pg_record['currency_conversion_rate'], 
		'pg_compare_amount'=> roundoff_number($pg_record['amount']*$pg_record['currency_conversion_rate']), 
		'date_time'      => $pg_record['created_datetime']
		);
		$this->db->insert('ccavenue_pg_record',$ccavenue_pg_record); 
		$pg_record['amount'] = roundoff_number($pg_record['amount']*$pg_record['currency_conversion_rate']);
		
		if (empty($pg_record) == false and valid_array($pg_record) == true) {
			$params = json_decode($pg_record['request_params'], true);
			$pg_initialize_data = array (
				'txnid' => $params['txnid'],
				'pgi_amount' => $pg_record['amount'],
				'firstname' => $params['firstname'],
				'email'=>$params['email'],
				'phone'=>$params['phone'],
				'productinfo'=> $params['productinfo']
			);
		} else {
			echo 'Under Construction :p';
			exit;
		}
		//defined in provab_config.php
		$payment_gateway_status = $this->config->item('enable_payment_gateway');

		if ($payment_gateway_status == true) {
			$this->pg->initialize ( $pg_initialize_data );
			$page_data['pay_data'] = $this->pg->process_payment (); 
			//Not to show cache data in browser  
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache"); 
			echo $this->template->view('payment/'.$PG.'/index', $page_data);
		} else {
			//directly going to process booking
//			echo 'Booking Can Not Be Done!!!';
//			exit;
			redirect('flight/secure_booking/'.$book_id.'/'.$book_origin);
			//redirect('hotel/secure_booking/'.$book_id.'/'.$book_origin);
			//redirect('bus/secure_booking/'.$book_id.'/'.$book_origin);
		}
	}
	/**
	 *
	 */
	 public function ccavRequestHandler() 
	 {
	 	$PG = $this->config->item('active_payment_gateway');
	 	if ($this->config->item('active_payment_system') == "test") {
			$access_code  = ACCESS_CODE;
			$working_key  = WORKING_KEY;
			$urlhit = 'https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
		} else {
			//live
			$access_code  = ACCESS_CODE;
			$working_key  = WORKING_KEY;
			$urlhit = 'https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
	 	}
	 	$merchant_data = '';
	 	foreach ($_POST as $key => $value){ 
			$merchant_data.=$key.'='.$value.'&'; 
		}
		//debug($merchant_data);exit;
		$page_data['encrypted_data'] = $this->encrypt($merchant_data,$working_key); // Method for encrypting the data.
		$page_data['access_code'] = $access_code;
		$page_data['urlhit'] = $urlhit;
		//debug($page_data); die;
		echo $this->template->view('payment/'.$PG.'/ccavRequestHandler', $page_data);
	 	
	 }

	 /****
	*****sudheep EBS synch*****   
	****/
	  
	 function cancel() {
	 	if ($this->config->item('active_payment_system') == "test") {
	 		$access_code  = ACCESS_CODE;
			$working_key  = WORKING_KEY;  
	 		$urlhit = 'https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
	 	} else {
	 		//live
	 		$access_code  = ACCESS_CODE;
			$working_key  = WORKING_KEY;  
	 		$urlhit = 'https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
	 	}
	 	$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
	 	$rcvdString=$this->decrypt($encResponse,$working_key);		//Crypto Decryption used as per the specified working key.
	 	
	 	
	 	$order_status="";
	 	$decryptValues=explode('&', $rcvdString);
	 	$dataSize=sizeof($decryptValues);
	 	$information=explode('=',$decryptValues[3]);
	 	
	 	if ($information[1] == "Aborted") {
	 		$tracking_id=explode('=',$decryptValues[1]);
	 		$information=explode('=',$decryptValues[3]);
	 		$booking_id=explode('=',$decryptValues[27]);
	 		$productinfo=explode('=',$decryptValues[26]);
	 		$book_id =$booking_id[1];
	 		$productinfo =$productinfo[1];
	 		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
	 				'book_id' => $book_id
	 		) );
	 		$this->load->model('transaction');
	 		
	 		$pg_record = $this->transaction->read_payment_record($book_id);
	 		
	 		if (empty($pg_record) == false and valid_array($pg_record) == true) {
	 			$response_params = $_REQUEST; 		
	 			//debug($productinfo); die;
	 			$this->transaction->update_payment_record_status($book_id, DECLINED, $response_params);
	 			$msg = "Payment Cancelled by user, Please try again.";
	 			switch ($productinfo) {
	 				
	 				case META_PACKAGE_COURSE :
	 					redirect ( base_url () . 'index.php/general/exception?op=booking_exception&notification=' . $msg );
	 					break;
	 					case 'package booking' :
	 						redirect ( base_url () . 'index.php/general/exception?op=booking_exception&notification=' . $msg );
	 						break;
	 			}
	 		}
	 		
	 	}
	 	
	 	//debug($information); die;
	 	
	 	if(!empty($_REQUEST)){
	 		//debug($_REQUEST); die;
	 		$product = $_REQUEST ['productinfo'];
	 		$book_id = $_REQUEST ['txnid'];
	 		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
	 				'book_id' => $book_id
	 		) );
	 		$pg_record = $this->transaction->read_payment_record($book_id);
	 		if (empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {
	 			$response_params = $_REQUEST;
	 			$this->transaction->update_payment_record_status($book_id, DECLINED, $response_params);
	 			$msg = "Payment Unsuccessful, Please try again.";
	 			switch ($product) {
	 				case META_AIRLINE_COURSE :
	 					redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
	 					break;
	 				case META_BUS_COURSE :
	 					redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
	 					break;
	 				case META_ACCOMODATION_COURSE :
	 					redirect ( base_url () . 'index.php/hotel/exception?op=booking_exception&notification=' . $msg );
	 					break;
	 			}
	 		}
	 	}
	  
	 }

	 function response(){
			if ($this->config->item('active_payment_system') == "test") {
	 		$access_code  = ACCESS_CODE;
			$working_key  = WORKING_KEY;
			$urlhit = 'https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
			} else {
				//live
				$access_code  = ACCESS_CODE;
			$working_key  = WORKING_KEY;
				$urlhit = 'https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
		 	}
			
			$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
			$rcvdString=$this->decrypt($encResponse,$working_key);		//Crypto Decryption used as per the specified working key.
			// debug($rcvdString); die;
			$order_status="";
			$decryptValues=explode('&', $rcvdString);
			$dataSize=sizeof($decryptValues);
			$information=explode('=',$decryptValues[3]);
			//PAYMENT GATEWAY AMOUNT DEDUCTED
			$pgad=$pg_amount_deducted=explode('=',$decryptValues[10]);
			$tracking=explode('=',$decryptValues[1]);
			// debug($tracking);exit;
			$tracking_id=$tracking[1];
			// debug($tracking_id);exit;
			$booking_id=explode('=',$decryptValues[27]); 
			$book_id=$booking_id[1];
			$final_pg_record = $this->transaction->read_ccavenue_payment_record($book_id); 
			$track_status= $this->transaction->check_tracking_id($tracking_id); 
			// debug($track_status);exit; 
			if ($information[1] == "Success" && round($pgad[1])==$final_pg_record['pg_compare_amount'] && $track_status==true) { 
				$productinfo=explode('=',$decryptValues[26]);
				$booking_id=explode('=',$decryptValues[27]);
				// debug($book_id);debug($tracking_id);die;
				$this->transaction->update_tracking_id($book_id,$tracking_id);
				redirect ( base_url () . 'index.php/payment_gateway/success/' . $booking_id[1] . '/' . $productinfo[1] );
			} 
			else 
			{  
				// $tracking_id=explode('=',$decryptValues[1]);
				$information=explode('=',$decryptValues[3]);
				$this->transaction->update_tracking_id($book_id,$tracking_id);
				/*debug($encResponse);
				echo "status";debug($information[1]);
				echo "amount";debug(round($pgad[1])==$final_pg_record['pg_compare_amount']);
				echo "track_status";debug($track_status);*/

				echo $msg="<p>your payment got failed please contact system admin with Tracking Number <span style='color:blue;font-weight:bold'>".$tracking_id."</span></p>";

				//echo "your payment got failed please contact system admin with Tracking Number".$tracking_id[1];

				die;
			}
	}


	
	/**
	 *
	 */
	function success($book_id,$product) { 

		//debug($product);die;
		//$pg_status = 1;
		//$product =META_PACKAGE_COURSE;
		$this->load->model('transaction');
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
				'book_id' => $book_id 
		) );
	   
		$pg_record = $this->transaction->read_payment_record($book_id);
		//debug($pg_record); die;
		//$pg_status = $_REQUEST['status'];
		if (empty($pg_record) == false and valid_array($pg_record) == true) {
			//update payment gateway status
			$response_params = $_REQUEST;
			// debug($response_params);exit;
			$this->transaction->update_payment_record_status($book_id, ACCEPTED, $response_params);
			$book_origin = $temp_booking ['data'] ['0'] ['id'];  
			// debug($product);debug(META_SIGHTSEEING_COURSE);exit;
			switch ($product) {
				case META_AIRLINE_COURSE : 
					redirect ( base_url () . 'index.php/flight/process_booking/' . $book_id . '/' . $book_origin ); 
					break;
				case META_BUS_COURSE :
					redirect ( base_url () . 'index.php/bus/process_booking/' . $book_id . '/' . $book_origin );
					break;
				case META_ACCOMODATION_COURSE :
					redirect ( base_url () . 'index.php/hotel/process_booking/' . $book_id . '/' . $book_origin );
					break;

				case META_PACKAGE_COURSE :
					redirect ( base_url () . 'index.php/tours/process_booking/' . $book_id . '/' . $book_origin );
					break;

				case META_SIGHTSEEING_COURSE :
					redirect ( base_url () . 'index.php/sightseeing/process_booking/' . $book_id . '/' . $book_origin );
					break;
				case META_SIGHTSEEING_COURSE_MB :
					redirect ( base_url () . 'index.php/sightseeing/process_booking/' . $book_id . '/' . $book_origin );
					break;

				case META_TRANSFER_COURSE :
					redirect ( base_url () . 'index.php/transferv1/process_booking/' . $book_id . '/' . $book_origin );
					break;

				default : die('transaction/cancels');
					redirect ( base_url().'index.php/transaction/cancel' );
					break;
			}
		}
	}
	/**
	 *
	 */
	/**     
	 *
	 */
	


	function transaction_log(){
		load_pg_lib('PAYU');
		echo $this->template->view('payment/PAYU/pay');
	}

/*for ccavenue payment gateway start start*/
	function encrypt($plainText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
	  	$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
		$plainPad = $this->pkcs5_pad($plainText, $blockSize);
	  	if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) 
		{
		      $encryptedText = mcrypt_generic($openMode, $plainPad);
	      	      mcrypt_generic_deinit($openMode);
		      			
		} 
		return bin2hex($encryptedText);
	}

	function decrypt($encryptedText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText=$this->hextobin($encryptedText);
	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
	 	mcrypt_generic_deinit($openMode);
		return $decryptedText;
		
	}
	//*********** Padding Function *********************

	function pkcs5_pad ($plainText, $blockSize)
	{
	    $pad = $blockSize - (strlen($plainText) % $blockSize);
	    return $plainText . str_repeat(chr($pad), $pad);
	}

	//********** Hexadecimal to Binary function for php 4.0 version ********

	function hextobin($hexString) 
   	 { 
        	$length = strlen($hexString); 
        	$binString="";   
        	$count=0; 
        	while($count<$length) 
        	{       
        	    $subString =substr($hexString,$count,2);           
        	    $packedString = pack("H*",$subString); 
        	    if ($count==0)
		    {
				$binString=$packedString;
		    } 
        	    
		    else 
		    {
				$binString.=$packedString;
		    } 
        	    
		    $count+=2; 
        	} 
  	        return $binString; 
    	  } 
   /*cavenue encryption technique ends here*/
}
