<style>
th,td{padding:5px;}
</style>
<?php
$booking_details = json_decode(json_encode($other[0]), true); 

$total_price = $booking_details['TotalPrice'] + $booking_details['admin_markup']+$convenience_fee;

// debug();exit;
$request = json_decode($booking_details['request'],true);
//debug($request);exit;
// $adult_count = array_sum(explode(',',$request['adults'][0]));
// $child_count = array_sum(explode(',',$request['childs'][0]));
$adult_count = $booking_details['adult'];
$child_count = $booking_details['child'];

$lead_pax_details = json_decode($booking_details['passenger_details']);

if($hotel_type == VILLA){
	 $module = "Villa ";
}else{
	$module = "Hotel ";
}

?>

<div class="table-responsive" id="tickect_hotel">
<table style="border-collapse: collapse; background: #ffffff;font-size: 14px; margin: 0 auto; font-family: arial;" width="70%" cellpadding="0" cellspacing="0" border="0">
<tbody>
	<tr>
		<td style="border-collapse: collapse; padding:10px 20px 20px" >
			<table width="100%" style="border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0">
				
				<tr>
					<td style="padding: 10px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse: collapse;">
						<tr><td style="font-size:22px; line-height:30px; width:100%; display:block; font-weight:600; text-align:center"><?php echo $module; ?> Voucher</td></tr>
				<tr>
					<td>
			<table width="100%" style="border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0"><tr>
					<td style="width: 33.33%" class="logocvr"><img
										style="max-width: 150px; height: 40px;"
										src="<?=$GLOBALS['CI']->template->domain_images($GLOBALS['CI']->template->get_domain_logo())?>"
										alt="" /></td>
					<td style="padding: 10px;width:35%">
                    	<table width="100%" style="border-collapse: collapse;text-align: right; line-height:15px;" cellpadding="0" cellspacing="0" border="0">
                    		
                    		<tr>
                    		<td style="font-size:14px;"><span style="width:100%; float:left"><?php echo $booking_details['contact_address_line1'];?></span>
                    		</td>
                    		</tr>
                         </table></td>
				</tr></table></td></tr>
							<tr>
								<td width="50%" style="padding: 10px;border: 1px solid #cccccc; font-size: 14px; font-weight: bold;"><?php echo $module; ?> Booking Lookup</td>
							</tr>
							<tr>
								<td style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 13px;">
										<tr>
											<td><strong>Booking Reference</strong></td>
											<td><strong>Booking ID</strong></td>										
											<td><strong>Booking Date</strong></td>
											<td><strong>Status</strong></td>
										</tr>
										<tr>
											<td><?php echo $booking_details['parent_pnr']; ?></td>
											<td><?php echo $booking_details['pnr_no']; ?></td>
											<td><?php echo date("d M Y",strtotime($booking_details['voucher_date'])); ?></td>
											
                                           <td>
                                           <strong class="<?php echo booking_status_label( $booking_details['booking_status']);?>" style=" font-size:14px; color: #fff !important;">
											<?php 
											switch($booking_details['booking_status']){
												case 'CONFIRM': echo 'CONFIRMED';break;
												case 'CANCELLED': echo 'CANCELLED';break;
											}
																				
											?>
											</strong></td>
                                           
										</tr>
									</table>
								</td>
							</tr>
                            <tr><td>&nbsp;</td></tr>
							<tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 14px; font-weight: bold;"><?php echo $module; ?> Information</td>
								
							</tr>
							<tr>
								<td  width="100%" style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 13px;">
										<tr>
											<td width="10%"><strong><?php echo $module; ?> Name</strong></td>
											<td width="19%"><strong><?php echo $module; ?> Address</strong></td>
											<!-- <td><strong>Phone</strong></td> -->
											<td><strong>Check-In</strong></td>
											<td><strong>Check-Out</strong></td>
											<!-- <td><strong>No of Room's</strong></td> -->
											<td><strong>Room Type</strong></td>
											<td><strong>Adult's</strong></td>
											<td><strong>Children</strong></td>
										</tr>
										<tr>
                                        <td>
                                        <?php echo @$booking_details['hotel_name']; ?>
                                        </td>
                                        <td style="line-height:16px">
                                        <?php echo @$booking_details['HotelAddress']; ?> 
                                        </td>
                                       
                                        <td><span style="width:100%; float:left"> <?=@date("d M Y",strtotime($booking_details['check_in_date']))?></span></td>
                                        <td><span style="width:100%; float:left"> 	<?=@date("d M Y",strtotime($booking_details['check_out_date']))?></span></td>
                                       <!--  <td align="center"><?php echo @$booking_details['total_rooms']; ?></span></td> -->
                                        <td  width="13%"><?php echo $booking_details['room_name']; ?></td>
                                        <td align="center"><?php echo $adult_count; ?></td>
                                        <td align="center"><?php echo $child_count; ?></td>
										</tr>
									</table>
								</td>
							</tr>
                            <tr>
                            <td>&nbsp;</td></tr>
							<tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 13px; font-weight: bold;">Contact Information</td>
								
							</tr>
							<tr>
								<td style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 13px;">
										<tr>
											<td><strong>Passenger Name</strong></td>
											<td><strong>Mobile</strong></td>
											<td><strong>Email</strong></td>
											
										</tr>
										<tr>
										<td><?=@$booking_details['contact_fname']." ".$booking_details['contact_sur_name'];?></td>
										<td><?php echo $booking_details['contact_mobile_number'] ?></td>
										<td><?php echo $booking_details['contact_email']?></td>
										</tr>
										
									</table>
								</td>
								<td></td>
							</tr>
                            <tr><td>&nbsp;</td></tr>
							<tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 13px; font-weight: bold;">Price Summary</td>
								
							</tr>
							<tr>
								<td style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 13px;">
										<tr>
											<td><strong>Fare</strong></td>
											<td><strong>Promo Code Discount</strong></td>
											<?php if($booking_details['reward_amount']){ ?>
											<td><strong>Rewards</strong></td>
											<?php } ?>
											<td><strong>Total Fare</strong></td>
										</tr>
										<tr>
                                            <td> <?php echo get_application_display_currency_preference() ?> <?php echo ceil($total_price-$other[0]->discount_amount) ; ?></td>
                                           
                                            <td> <?php echo get_application_display_currency_preference() ?>
                                            <?=ceil($other[0]->discount_amount);?>
                                            <!--number_format($other[0]->discount_amount, 2);-->
                                            </td>
                                            <?php if($booking_details['reward_amount']){ ?>
                                            <td> <?php echo get_application_display_currency_preference() ?> <?php echo $booking_details['reward_amount'] ; ?></td>
                                            <?php } ?>
                                            <td> <?php echo get_application_display_currency_preference() ?> <?php echo ceil($total_price-$booking_details['reward_amount']); ?></td>
                                             
                                            <!--number_format($total_price-$other[0]->discount_amount, 2)-->
										</tr>
										
									</table>
								</td>
								<td></td>
							</tr>

							<tr><td>&nbsp;</td></tr>
							<!-- <tr>
								<td width="50%" style="padding: 10px;border: 1px solid #cccccc; font-size: 13px; font-weight: bold;">Cancellation Policy</td>
							</tr> -->
							<!-- <tr>
								<td  width="100%" style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px 20px;font-size: 13px;">
										<tr>
                                        <td><?=@$booking_details['cancellation_policy']?></td>
										</tr>
										
									</table>
								</td>
							</tr> -->

                            <tr><td>&nbsp;</td></tr>
							<tr>
								<td width="50%" style="padding: 10px;border: 1px solid #cccccc; font-size: 13px; font-weight: bold;">Terms and Conditions</td>
							</tr>
							<tr>
								<td  width="100%" style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px 20px;font-size: 13px;">
										<tr>
                                        <td>1.Please ensure that operator PNR is filled, otherwise the ticket is not valid.</td>
										</tr>
										
									</table>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr></tbody>
</table>

<?php 

	if(isset($tour_data) && !empty($tour_data))
	{
		 echo  $this->template->isolated_view('voucher/holiday_ziphop',$tour_data);
	}
?>
<table id="printOption"onclick="w=window.open();w.document.write(document.getElementById('tickect_hotel').innerHTML);w.print();w.close(); return true;"
 style="border-collapse: collapse;font-size: 14px; margin: 10px auto; font-family: arial;" width="70%" cellpadding="0" cellspacing="0" border="0">
<tbody>
	<tr>
    <td align="center"><input style="background:#418bca; height:34px; padding:10px; border-radius:4px; border:none; color:#fff; margin:0 2px;" type="button" value="Print" />
    
    </tr>
</tbody></table>
</div>