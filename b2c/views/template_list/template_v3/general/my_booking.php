<?php 
if ($module=='flight' || $module=='') {
   $tab['flight']['btn_class']='active';
   $tab['flight']['div_class']='in active';
}elseif ($module=='hotel') {
   $tab['hotel']['btn_class']='active';
   $tab['hotel']['div_class']='in active';
}elseif ($module=='car') {
   $tab['car']['btn_class']='active';
   $tab['car']['div_class']='in active';
}elseif ($module=='holiday') {
   $tab['holiday']['btn_class']='active';
   $tab['holiday']['div_class']='in active';
}

?>
<style>
mybkng .al_lnk {
    padding: 20px 15px 20px;
    float: none;
}
.al_lnk {
    width: 100%;
    /* float: left; */
    /* max-width: 250px; */
    margin: 0 auto;
    }
    .my_bkg_lft {
    max-width: 625px;
    float: none;
    margin: 10px auto 0;
    display: block;
    position: relative;
}
.mybkng .my_bkg_lft .lkng_lft {
    border-radius: 3px;
    box-shadow: 0px 0px 3px #ccc;
    background: #fff;
    /* margin: 15px 0; */
    padding: 15px 30px 11px;
}
.lkng_lft {
    border-radius: 3px;
    box-shadow: 0px 0px 3px #ccc;
    /* margin: 15px 0; */
}
.mybkng .form-control {
    display: block;
}
.flgt_blkg1 {
    margin-top: 15px;
}
</style>
<div class="airline mybkng">
   <div class="container">
      <div class="col-xs-12 mpad">
         <div class="col-md-12 col-xs-12 airline1 nopad">
            <!-- <h2 class="hdng">My Bookings</h2> -->
            <!-- <p>Index of the most popular airlines</p> -->
            <div class="col-md-12 col-xs-12 nopad anthr_bk">
               <div class="col-xs-12 mpad text-center">
                  <div class="al_lnk">
                     <h5 style="color: #f58830;margin-top: 0px;">Access My Booking</h5>
                     <ul class="nav nav-tabs list-inline hide">
                        <li class="<?=@$tab['flight']['btn_class']?>"><a class="btn btn-default lnk_bt" data-toggle="tab" href="#flts"><i class="fa fa-plane" aria-hidden="true"></i>Flights</a></li>
                        <li class="<?=@$tab['hotel']['btn_class']?>"><a class="btn btn-default lnk_bt" data-toggle="tab" href="#htls"><i class="fa fa-bed" aria-hidden="true"></i>Hotels</a></li>
                        <li class="<?=@$tab['car']['btn_class']?>"><a class="btn btn-default lnk_bt" data-toggle="tab" href="#cars"><i class="fa fa-car" aria-hidden="true"></i>Cars</a></li>
                        <li class="<?=@$tab['holiday']['btn_class']?>"><a class="btn btn-default lnk_bt" data-toggle="tab" href="#hldys"><i class="fa fa-umbrella" aria-hidden="true"></i>Holidays</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-xs-12 my_bkg_lft">
                  <div class="col-xs-12 lkng_lft">
                     <div class="tab-content">
                        <div id="flts" class="tab-pane fade <?=@$tab['flight']['div_class']?>">
                        <form method="post" action="<?= base_url()?>index.php/general/getVoucher">
                           <input type="hidden" name="module" value="<?=md5('flight')?>">
                           <div class="al_lnk1">
                              <h4>Please provide your Reservation Code & Email ID to proceed</h4>
                              <?= $this->session->flashdata('flight') ?>
                              <input type="text" class="form-control mntxt flgt_bkg" name="ticket_name" id="ticket_name" placeholder="Reservation Code" value="" aria-required="true" required="required">
                              <div class="flgt_blkg1">
                                 <input type="email" class="form-control mntxt" name="last_name" id="last_name" placeholder="Passenger Email ID" value="" aria-required="true" required="required">
                              </div>
                              <div class="col-md-6 col-xs-12 nopad">
                                 
                              </div>
                              <div class="col-md-7 col-xs-12 nopad">
                                 <div class="modal-footer"><input type="submit" class="btn btn-default" id="send_enquiry_button" value="SEARCH"></div>
                              </div>
                           </div>
                        </form>
                        </div>
                        <div id="htls" class="tab-pane fade <?=@$tab['hotel']['div_class']?>">
                        <form method="post" action="<?= base_url()?>index.php/general/getVoucher">
                           <input type="hidden" name="module" value="<?=md5('hotel')?>">
                           <div class="al_lnk1">
                              <h2>Access a Hotel Booking</h2>
                              <?= $this->session->flashdata('hotel') ?>
                              <input type="text" class="form-control mntxt flgt_bkg" name="ticket_name" id="ticket_name" placeholder="BOOKING REFERENCE" value="" aria-required="true" required="required">
                              <div class="flgt_blkg1">
                                 <input type="text" class="form-control mntxt alpha" name="last_name" id="last_name" placeholder="PASSENGER LAST NAME" value="" aria-required="true" required="required">
                              </div>
                              <div class="col-md-6 col-xs-12 nopad">
                                 
                              </div>
                              <div class="col-md-6 col-xs-12 nopad">
                                 <div class="modal-footer"><button type="submit" class="btn btn-default" id="send_enquiry_button">Find</button></div>
                              </div>
                           </div>
                        </form>
                        </div>
                        <div id="cars" class="tab-pane fade <?=@$tab['car']['div_class']?>">
                        <form method="post" action="<?= base_url()?>index.php/general/getVoucher">
                           <input type="hidden" name="module" value="<?=md5('car')?>">
                           <div class="al_lnk1">
                              <h2>Access a Car Booking</h2>
                              <?= $this->session->flashdata('car') ?>
                              <input type="text" class="form-control mntxt flgt_bkg" name="ticket_name" id="ticket_name" placeholder="BOOKING REFERENCE" value="" aria-required="true" required="required">
                              <div class="flgt_blkg1">
                                 <input type="text" class="form-control mntxt alpha" name="last_name" id="last_name" placeholder="PASSENGER LAST NAME" value="" aria-required="true" required="required">
                              </div>
                              <div class="col-md-6 col-xs-12 nopad">
                                 
                              </div>
                              <div class="col-md-6 col-xs-12 nopad">
                                 <div class="modal-footer"><button type="submit" class="btn btn-default" id="send_enquiry_button">Find</button> </div>
                              </div>
                           </div>
                        </form>
                        </div>
                        <div id="hldys" class="tab-pane fade <?=@$tab['holiday']['div_class']?>">
                        <form method="post" action="<?= base_url()?>index.php/general/getVoucher">
                           <input type="hidden" name="module" value="<?=md5('holiday')?>">
                           <div class="al_lnk1">
                              <h2>Access a Holiday Booking</h2>
                              <?= $this->session->flashdata('holiday') ?>
                              <input type="text" class="form-control mntxt flgt_bkg" name="ticket_name" id="ticket_name" placeholder="BOOKING REFERENCE" value="" aria-required="true" required="required">
                              <div class="flgt_blkg1">
                                 <input type="text" class="form-control mntxt" name="last_name" id="last_name" placeholder="PASSENGER LAST NAME" value="" aria-required="true" required="required">
                              </div>
                              <div class="col-md-6 col-xs-12 nopad">
                                 
                              </div>
                              <div class="col-md-6 col-xs-12 nopad">
                                 <div class="modal-footer"><button type="submit" class="btn btn-default" id="send_enquiry_button">Find</button></div>
                              </div>
                           </div>
                        </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(function(){
      $("#send_enquiry_button").click(function(){
         $('.msg').hide();
      });
   });
</script>