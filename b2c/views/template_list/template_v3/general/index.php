<?php

/*$image = $banner_images ['data'] [0] ['image'];*/
$active_domain_modules = $this->active_domain_modules;
$default_active_tab = $default_view;
/**
 * set default active tab
 *
 * @param string $module_name
 *        	name of current module being output
 * @param string $default_active_tab
 *        	default tab name if already its selected otherwise its empty
 */
function set_default_active_tab($module_name, &$default_active_tab) {
	if (empty ( $default_active_tab ) == true || $module_name == $default_active_tab) {
		if (empty ( $default_active_tab ) == true) {
			$default_active_tab = $module_name; // Set default module as current active module
		}
		return 'active';
	}
}

//add to js of loader
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('backslider.css'), 'media' => 'screen');
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('owl.carousel.min.css'), 'media' => 'screen');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('owl.carousel.min.js'), 'defer' => 'defer');
 Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('backslider.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/index.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/pax_count.js'), 'defer' => 'defer');
?>

 <!-- <div class="homepage-video">
    <div class="video-container">
        <video autoplay loop class="fillWidth">
            <source src="<?php echo $GLOBALS['CI']->template->template_images('video/video2.mp4')?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="<?php echo $GLOBALS['CI']->template->template_images('video/video2.mp4')?>" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
    </div>
</div> -->

<style type="text/css">
	.topssec { background: #fff; }
	
</style>

<div class="searcharea  kk">
	<div class="srchinarea">
		<div class="container">
			<!-- <div class="captngrp">
				<div id="big1" class="bigcaption">Catch the Spirit!</div>
				<div id="desc" class="smalcaptn">More than 1000+ flights  you can search!<span class="boder" style="width: 174px;"></span></div>
			</div> -->
			<div class="captngrp1">
				<div  class="bigcaption1">Book Your Cheap Flights</div>
				<!--<div  class="smalcaptn1">More than 1000+ flights  you can search!<span class="boder1" style="width: 174px;"></span></div>-->
			</div>
		</div>
		<div class="allformst">
			
			<!-- Tab panes -->
			<div class="container inspad">
			<div class="tab_border">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs tabstab">
					<?php if (is_active_airline_module()) { ?>
					<li
						class="<?php echo set_default_active_tab(META_AIRLINE_COURSE, $default_active_tab)?>"><a
						href="#flight" role="tab" data-toggle="tab">
						<span class="sprte iconcmn">
						<!--<i class=""><img src="<?= $GLOBALS['CI']->template->template_images('flight-icon.png');?>"></i>-->
						</span><label>Flights</label></a></li>
					<?php } ?>
					<?php if (is_active_hotel_module()) { ?>
					<li
						class="<?php echo set_default_active_tab(META_ACCOMODATION_COURSE, $default_active_tab)?>"><a
						href="#hotel" role="tab" data-toggle="tab">
						<span class="sprte iconcmn"><i class=""><img src="<?= $GLOBALS['CI']->template->template_images('hotel-icon.png');?>"></i></span><label>Hotels</label></a></li>

					<?php } ?>
					<?php if (is_active_hotel_module()) { ?>
						<li
						class="<?php echo set_default_active_tab("villa001", $default_active_tab)?>" ><a
						href="#villa" role="tab" data-toggle="tab" id="villaButton">
						<span class="sprte iconcmn"><i class=""><img src="<?= $GLOBALS['CI']->template->template_images('hotel-icon.png');?>"></i></span><label >Villa</label></a></li>
					<?php } ?>
					<!-- <li class="deals"><a  target="_blank" href="http://travelomatix.com/special-fares-june2018.html"><span class="sprte iconcmn"><strong class="new_deal">New!</strong><i class="fal fa-tags"></i></span><label>Hot Deals!</label></a></li>
 -->
					<?php if (is_active_bus_module()) { ?>
					<li
						class="<?php echo set_default_active_tab(META_BUS_COURSE, $default_active_tab)?>"><a
						href="#bus" role="tab" data-toggle="tab">
						<span class="sprte iconcmn"><i class=""><img src="<?= $GLOBALS['CI']->template->template_images('buses-icon.png');?>"></i></span><label>Buses</label></a></li>
					<?php } ?>

					<?php if (is_active_package_module()) { ?>
					 <li
						class="<?php echo set_default_active_tab(META_PACKAGE_COURSE, $default_active_tab)?>"><a
						href="#holiday" role="tab"
						data-toggle="tab">
						<span class="sprte iconcmn"><i class="fal fa-tree"></i></span><label>Holidays</label></a></li>
					<?php } ?>

					<?php if (is_active_sightseeing_module()) { ?>
					<li
						class="<?php echo set_default_active_tab(META_SIGHTSEEING_COURSE, $default_active_tab)?>"><a
						href="#sightseeing" role="tab"
						data-toggle="tab">
						<span class="sprte iconcmn"><i class="fal fa-binoculars"></i></span><label>Activities</label></a></li>
					<?php } ?>

					<?php if (is_active_transferv1_module()) { ?>
					<li
						class="<?php echo set_default_active_tab(META_TRANSFERV1_COURSE, $default_active_tab)?>"><a
						href="#transferv1" role="tab" data-toggle="tab">
						<span class="sprte iconcmn"><i class=""></i></span><label>Transfers</label></a></li>
					<?php } ?>
					

					

					<?php if (is_active_car_module()) { ?>
					<li
						class="<?php echo set_default_active_tab(META_CAR_COURSE, $default_active_tab)?>"><a
						href="#car" role="tab" data-toggle="tab">
						<span lass="sprte iconcmn"><i class="fal fa-car"></i></span><label>Cars</label></a></li>
					<?php } ?>	
				</ul>
			</div>
			<div class="secndblak">
					<div class="tab-content custmtab">
						<?php if (is_active_airline_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_AIRLINE_COURSE, $default_active_tab)?>"
							id="flight">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/flight_search')?>
						</div>
						<?php } ?>
						<?php if (is_active_hotel_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_ACCOMODATION_COURSE, $default_active_tab)?>"
							id="hotel">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/hotel_search')?>
						</div>

						<div
							class="tab-pane <?php echo set_default_active_tab("villa001", $default_active_tab)?>"
							id="villa">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/villa_search')?>
						</div>
						<?php } ?>

						<?php if (is_active_bus_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_BUS_COURSE, $default_active_tab)?>"
							id="bus">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/bus_search')?>
						</div>
						<?php } ?>

						<?php if (is_active_transferv1_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_TRANSFERV1_COURSE, $default_active_tab)?>"
							id="transferv1">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/transferv1_search')?>
						</div>
						<?php } ?>
						<?php if (is_active_car_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_CAR_COURSE, $default_active_tab)?>"
							id="car">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/car_search')?>
						</div>
						<?php } ?>

						<?php if (is_active_package_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_PACKAGE_COURSE, $default_active_tab)?>"
							id="holiday">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/holiday_search',$holiday_data)?>
						</div>
						<?php } ?>

						<?php if (is_active_sightseeing_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_SIGHTSEEING_COURSE, $default_active_tab)?>"
							id="sightseeing">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/sightseeing_search',$holiday_data)?>
						</div>
						<?php } ?>


						
					</div>
				</div>
			</div>

			

		</div>
	</div>
	<div class="dot-overlay"></div>
</div>

<div class="clearfix"></div>
<!--<div class="hotel-heading">
		<h1>HOTEL</h1>
	</div>-->
	
	<div class="perhldys">
	<div class="container">
		 <div class="col-md-12 nopad">
			 <div class="col-md-10 col-md-offset-1 back_bg">
		 			<div class="main_ad_section pull-left">
		 				<div class="main_ad_img pull-left">
		 					<img src="<?php echo $GLOBALS['CI']->template->domain_images($deal_banner['deal_image']); ?>" class="img-responsive">
		 					<div class="main_ad_image_line">
		 					</div>
		 				</div>
		 				<div class="main_ad_content_section  pull-left">
		 					<div class=" pull-left dis_amount">
			 					<div class="save_sec">SAVE &nbsp;
			 						<span><i class="fas fa-rupee-sign"></i></span>
			 						<span><?= $deal_banner['discount'] ?></span>
			 					</div>
			 					<div class="save_sec_1">ON <?= strtoupper($deal_banner['category'])?> BOOKING</div>
		 					</div>
		 					<div class=" pull-left coupon_code_sec">
		 						<div class="use_coupon_code">USE CODE</div>
		 						<div class="coupon_code"><?= $deal_banner['deal_code'] ?></div>
		 					</div>
		 				</div>
		 				<div class=" pull-left pay_using_section">
		 					<div>
		 					 
		 						<img src="<?php echo $GLOBALS['CI']->template->template_images('amazon_pay.png'); ?>">
		 					</div>
		 					<a class="know_more_btn">KNOW MORE</a>
		 				</div>
		 			</div>
				 	<img  class="img-responsive hide" src="<?= $GLOBALS['CI']->template->template_images(); ?>offer.jpg" alt="">
		 </div>
	 </div>
	  </div>
	   </div>
<div class="htldeals">
	<div class="">
	<?php 
	if (in_array ( META_ACCOMODATION_COURSE, $active_domain_modules ) and valid_array ( $top_destination_hotel ) == true) : // TOP DESTINATION
	?>
		<div class="pagehdwrap">
			<h2 class="pagehding">
				<div class="span-head"></div>
				Tripidia Special Discounts
			</h2>
			<span><i class="fal fa-star"></i></span>
		</div>
		<div class="tophtls">
			<div class="grid">
			   <div class="container">	
				<div id="owl-demo2" class="owl-carousel owlindex2">
					<?php
					// debug($deals);exit;
						// if (in_array ( META_ACCOMODATION_COURSE, $active_domain_modules ) and valid_array ( $top_destination_hotel ) == true) : // TOP DESTINATION
					foreach ( $deals as $tk => $tv ) :
								?>
					
					<div class="item">
					<div class="col-sm-12 col-xs-12 nopad mmm tripidia_special_discounts">
						<div class="effect-marley figure">
						<!--<span class="tour-caption">Wedding</span>-->
						<div class="hotel-detail">
								<span class="price"><?=ucfirst($tv['category'])?></span>
								<h4><a href="<?php echo base_url().'index.php/general/promo_details?deal='.$tv['deal_id'] ?>"?><?=ucfirst($tv['deal_name'])?></a></h4>
								<span class="days">Promocode - <?=$tv['deal_code']?></span>
								<!--<a href="<?php //echo base_url().'index.php/general/promo_details' ?>"?>-->
							</div>
							<img
								class="lazy lazy_loader"
								src="<?php echo $GLOBALS['CI']->template->domain_images($tv['deal_image']); ?>"
								data-src="<?php echo $GLOBALS['CI']->template->domain_images($tv['deal_image']); ?>"
								alt="<?=$tv['deal_name']?>" />
						
							

							
						</div>
					</div>
					</div>
					<?php
						
						endforeach;
						endif; // TOP DESTINATION
					?>
					</div>
					<div class="tophtls_more_btn pull-right" style="font-size: 18px;">
						<a href="<?=base_url()?>index.php/general/hot_deals">+ More</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>
<!--<div class="explore-flight">
	<div class="">
	<?php 
	if (in_array ( META_ACCOMODATION_COURSE, $active_domain_modules ) and valid_array ( $top_destination_hotel ) == true) : // TOP DESTINATION
	?>
		<div class="pagehdwrap">
			<h2 class="pagehding">
				<div class="span-head">Motion</div>
				Explore Flight Details
			</h2>
			<span><i class="fal fa-star"></i></span>
		</div>
		<div class="tophtls">
			<div class="grid">
			   <div class="container">	
				  <div class="col-md-8 col-md-offset-2"> 
					<div id="all_deal" class="owl-carousel owlindex2 explore-flight-slide">
						<?php
						//debug($top_destination_hotel);exit;
							// if (in_array ( META_ACCOMODATION_COURSE, $active_domain_modules ) and valid_array ( $top_destination_hotel ) == true) : // TOP DESTINATION
								foreach ( $top_destination_hotel as $tk => $tv ) :
									?>
						<?php if(($tk-0)%10 == 0){?>
						<div class="item">
						<div class="col-sm-12 col-xs-12 nopad htd-wrap">
							<div class="effect-marley figure">
								<img
									class="lazy lazy_loader"
									src="<?php echo $GLOBALS['CI']->template->domain_images($tv['image']); ?>"
									data-src="<?php echo $GLOBALS['CI']->template->domain_images($tv['image']); ?>"
									alt="<?=$tv['city_name']?>" />
							</div>
						</div>
						</div>
						<?php } elseif (($tk-6)%10 == 0){ ?>
						
						<div class="item">
						<div class="col-sm-12 col-xs-12 nopad htd-wrap">
							<div class="effect-marley figure">
								<img
									class="lazy lazy_loader"
									src="<?php echo $GLOBALS['CI']->template->domain_images($tv['image']); ?>"
									data-src="<?php echo $GLOBALS['CI']->template->domain_images($tv['image']); ?>"
									alt="<?=$tv['city_name']?>" />
							</div>
						</div>
						</div>
						<?php } else {?>
						<div class="item">
						<div class="col-sm-12 col-xs-12 nopad htd-wrap">
							<div class="effect-marley figure">
								<img
									class="lazy lazy_loader"
									src="<?php echo $GLOBALS['CI']->template->domain_images($tv['image']); ?>"
									data-src="<?php echo $GLOBALS['CI']->template->domain_images($tv['image']); ?>"
									alt="<?=$tv['city_name']?>" />

							</div>
						</div>
						</div>
						<?php
							}
							endforeach;
							endif; // TOP DESTINATION
						?>
						</div>
					</div>
				  </div>			
			</div>
		</div>
	</div>
</div>-->
<!-- <div class="hide">
   <div class="container">
   <div class="org_row">
		<div class="pagehdwrap">
			<h2 class="pagehding">Top Deals</h2>
			<span><i class="fal fa-star"></i></span>
		</div>
      <div id="all_deal" class="owl-carousel owlindex3 " >
         <div class="gridItems">
            <div class="outerfullfuture">
               <div class="thumbnail_deal thumbnail_small_img">
                  <div class="lazyOwl"></div>
                  <img class="" src="<?php echo base_url(); ?>/extras/system/template_list/template_v1/images/flight_deal.jpg" alt="Lazy Owl Image">
               </div>
               <div class="caption carousel-flight-info deals_info">
                  <div class="deals_info_heading">
                     <h1>Get 14% Off on Flights</h1>
                  </div>
                  <div class="deals_info_subheading">
                     <h3>Use Coupon: FLIGHT14OFF. Check Details.</h3>
                  </div>
                  <div class="deals_info_footer">
                     <div class="pull-left validDate">Valid till : Jun 18, 2018</div>
                     <div class="pull-right viewLink">
                        <a class="" href="" target="_blank">View Details</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="gridItems">
            <div class="outerfullfuture">
               <div class="thumbnail_deal thumbnail_small_img">
                  <div class="lazyOwl"></div>
                  <img class="" src="<?php echo base_url(); ?>/extras/system/template_list/template_v1/images/hotel_deal.jpg" alt="Lazy Owl Image">
               </div>
               <div class="caption carousel-flight-info deals_info">
                  <div class="deals_info_heading">
                     <h1>Get 10% Off on Hotels</h1>
                  </div>
                  <div class="deals_info_subheading">
                     <h3>Use Coupon: RAMADAN. Check Details.</h3>
                  </div>
                  <div class="deals_info_footer">
                     <div class="pull-left validDate">Valid till : Jun 18, 2018</div>
                     <div class="pull-right viewLink">
                        <a class="" href="" target="_blank">View Details</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="gridItems">
            <div class="outerfullfuture">
               <div class="thumbnail_deal thumbnail_small_img">
                  <div class="lazyOwl"></div>
                  <img class="" src="<?php echo base_url(); ?>/extras/system/template_list/template_v1/images/holiday_deal.jpg" alt="Lazy Owl Image">
               </div>
               <div class="caption carousel-flight-info deals_info">
                  <div class="deals_info_heading">
                     <h1>Get 11% Off on Holidays</h1>
                  </div>
                  <div class="deals_info_subheading">
                     <h3>Use Coupon: TOUR11. Check Details.</h3>
                  </div>
                  <div class="deals_info_footer">
                     <div class="pull-left validDate">Valid till : Jun 18, 2018</div>
                     <div class="pull-right viewLink">
                        <a class="" href="" target="_blank">View Details</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> -->
</div>


<!--<div class="clearfix"></div>
<div class="ychoose pagehdwrap">
	<div class="container">
		<div class="col-lg-5 why-banner">
			<img src="<?= $GLOBALS['CI']->template->template_images('why-banner.png');?>">
		</div>
		<div class="allys col-lg-6 pull-right">
		<div class="pagehdwrap">
			<h2 class="pagehding">
				<div class="span-head">WE Are the best</div>
				Why Choose us?
			</h2>
			<span><i class="fal fa-star"></i></span>
		</div>
			<div class="">
				<div class="threey">
					<div class="apritopty"><i class=""><img src="<?= $GLOBALS['CI']->template->template_images('why-tab1.png');?>"></i> </div>
					<div class="dismany">
				      	<div class="number">Best price guarantee</div>
						<div class="hedsprite">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's </div>
					</div>
				</div>
			</div>
			<div class="">
				<div class="threey">
					<div class="apritopty"><i class=""><img src="<?= $GLOBALS['CI']->template->template_images('why-tab2.png');?>"></i> </div>
					<div class="dismany">
					     <div class="number">Trust & safty</div>
						<div class="hedsprite">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's </div>
					</div>
				</div>
			</div>
			<div class="">
				<div class="threey">
					<div class="apritopty"><i class=""><img class="blue" src="<?= $GLOBALS['CI']->template->template_images('why-tab3.png');?>"></i> </div>
					<div class="dismany">
				     	<div class="number blue">Honest Agent</div>
						<div class="hedsprite">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's </div>
					</div>
				</div>
			</div>
			<div class="">
				<div class="threey">
					<div class="apritopty"><i class=""><img src="<?= $GLOBALS['CI']->template->template_images('why-tab4.png');?>"></i> </div>
					<div class="dismany">
				     	<div class="number">24/7 Support</div>
						<div class="hedsprite">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>-->
 <div class="perhldys_new" >
<div class="container">
<div class="col-md-12 liv_deal_sec">
	<div class="liv_text col-md-10"><p class="col-md-3 liv_del_text">Live deals <span>(225)</span></p></div>
	<div class="col-md-2 nopad">
<div class="map"><span class="map_icon"></span><input class="loc_input" type="text" placeholder="Bangalore"></div></div>
	<div class="col-md-12"><span class="fil_text">Filter Live Deals</span> <span class="ret_text">Reset Filters</span></div>
	<div class="col-md-12 liv_deal_plac">
	<!-- <label>Places : </label>
	<input type="text" class="visit_input" placeholder="Add a place you want to visit">

	<span>Agra </span><span>Hydrabad</span><span>Cochin</span><span>Jaipur</span><span>Chennai</span>
<span>Madurai</span><span>Mangalore</span><span>Hubli</span><span> + More</span>
	</div> -->

		<div class="col-md-12 Cat_sec">
			<div class="col-md-8 nopad" ><p class="cat_text">
			<label>Categories : </label>
			<?php if(is_active_hotel_module()){?>
			<span><a href="<?= base_url()?>index.php/general/hot_deals?category=hotel">Hotel</a> </span>
			<span><a href="<?= base_url()?>index.php/general/hot_deals?category=villa">Villa</a> </span>

			<?php }?>
			<?php if(is_active_airline_module()){?>
			<span><a href="<?= base_url()?>index.php/general/hot_deals?category=flight">Flight</a></span>
			<?php } ?>
			<?php if(is_active_car_module()){?>
			<span><a href="<?= base_url()?>index.php/general/hot_deals?category=car">Car</a></span>
			<?php } ?>
			<?php if(is_active_bus_module()){?>
			<span><a href="<?= base_url()?>index.php/general/hot_deals?category=bus">Bus</a></span>
			<?php } ?>
			<?php if(is_active_transferv1_module()){ ?>
			<span><a href="<?= base_url()?>index.php/general/hot_deals?category=activity">Activity</a> </span>
			<?php }?>
			<?php if(is_active_package_module()){?>
			<span><a href="<?= base_url()?>index.php/general/hot_deals?category=holiday">Holiday</a></span>
			<?php } ?>
			<?php if (is_active_transferv1_module()) { ?>
			<span><a href="<?= base_url()?>index.php/general/hot_deals?category=transfer">Transfer</a></span>
			<?php } ?>
			</p>
			</div>
			<!-- <div class="col-md-9"> <p class="dat_txt">
			<label>Dates: </label><span>Aug</span><span>Sep</span><span>Oct</span><span>Nov</span><span>Dec</span></p></div> -->
		</div>
</div>
		<div class="pagehdwrap col-md-12 ">

			<h2 class="pagehding">Popular Destinations</h2>
			<span><i class="fal fa-star"></i></span>
		</div>

		<div class="retmnus">
		
				<?php
					//debug($top_destination_package);
					#debug($top_destination_package[0]);
					//exit; 
					$k = 0;
					// echo "total".$total;
					while ( @$total > 0 ) {
						?>	
				<div class="col-xs-12 col-md-4 nopad ">
					<div class="col-xs-12 nopad">
						<?php for($i=1;$i<=1;$i++) { 
							#echo "kk".$k.'<br/>';
							if(isset($top_destination_package[$k])){
								// debug($top_destination_package[$k]);
								// exit("724-index");
							$package_country = $this->package_model->getCountryName($top_destination_package[$k]->package_country);
							#debug($package_country);
							#echo "sdfsdf ".($k % 2).'<br/>';
							?>
						<div class="topone">
							<div class="inspd2 effect-lexi">
								<div class="imgeht2">
								<div class="dealimg">
									<img
										class="lazy lazy_loader"
										data-src="<? echo $GLOBALS['CI']->template->domain_upload_pckg_images(basename($top_destination_package[$k]->image)); ?>"
										alt="<?php echo $top_destination_package[$k]->package_name; ?>"
										src="<? echo $GLOBALS['CI']->template->domain_upload_pckg_images(basename($top_destination_package[$k]->image)); ?>"
										/>

										<div class="off_div"><?= $top_destination_package[$k]->duration-1; ?>N / <?= $top_destination_package[$k]->duration;?>D</div>
										 <input type="hidden" class="country_holiday hand-cursor" value="<?php echo $top_destination_package[$k]->package_country?>">
								  <input type="hidden" class="package_type" value="<?php echo $top_destination_package[$k]->package_type?>">
								</div>
								 
								</div>
								<div class="content_new col-md-12">
									<?php if(($k % 2) == 0) {?>
									<div class=" absintcol1  col-md-6 nopad">
										<?php } else {?>
										<div class=" absintcol2 col-md-6 nopad">
											<?php } ?>
											<div class="absinn">
												<div class="smilebig2">
												
													<h3><?php echo $top_destination_package[$k]->package_name; ?> </h3>
													
												<!--	<h4><?php echo $top_destination_package[$k]->package_city;?>, <?php echo $package_country->name; ?></h4>-->
													
												</div>
												<div class="clearfix"></div>
												
											</div>
										</div>
										<div class="col-md-6 nopad">
											<p class="quick_info" val="<?=$top_destination_package[$k]->package_id?>">
												<!--<a href="<?= base_url()?>index.php/tours/details/<?=$top_destination_package[$k]->package_id?>">Quick Info ^ </a>-->
												<a href="javascript:void(0);">Quick Info ^ </a>
											</p>
										</div>
									

									<figcaption>	
									<div class="deal_txt">
                                       
                                       <div class="col-xs-6 nopad" >
                                       	 <p class="spec_rat_text">Check Your Special Rate</p>                                        <p class="coupon_text">Redeem Points | Enter Coupon</p>
                                       </div>
                                       <div class="col-xs-6 nopad">
                                         <!--<img class="star_rat" src="<?php echo $GLOBALS['CI']->template->template_images('star_rating.png')?>" alt="">-->
                                         <p class="old_price"> &#x20b9; 2400</p>
                                         <h4 class="deal_price"> &#x20b9; <?php echo isset($top_destination_package[$k]->price)?get_converted_currency_value ( $currency_obj->force_currency_conversion ( $top_destination_package[$k]->price ) ):0; ?></h4><p class="per_text"> Per Person *</p>
                                         </div>

                                           <!--<div class="col-xs-6 nopad">
                                         <h4><?php echo isset($top_destination_package[$k]->duration)?($top_destination_package[$k]->duration-1):0; ?> Nights / <?php echo isset($top_destination_package[$k]->duration)?$top_destination_package[$k]->duration:0; ?> Days</h4> 
                                       <a class="package_dets_btn" href="<?=base_url().'index.php/tours/details/'.$top_destination_package[$k]->package_id?>">
												View details
												</a>  
									      </div>		
                                         </div>-->	
                                     </figcaption>

								</div>
								</div>
								<div class="hover_content" id="quick_content_<?= $top_destination_package[$k]->package_id?>">
									<div class=" absintcol1  col-md-6 nopad"><div class="absinn"><div class="smilebig2"><h3><?php echo $top_destination_package[$k]->package_name; ?> </h3></div><div class="clearfix"></div></div></div>
									<div class="col-md-6 nopad">
									<p class="quick_info" val="<?=$top_destination_package[$k]->package_id?>">

									<!--<a href="<?= base_url()?>index.php/tours/details/<?=$top_destination_package[$k]->package_id?>">Quick Info </a>-->
										<a href="javascript:void(0);">Quick Info </a>
									</p>
									</div>
									    <div class="col-md-12 nopad">
									    <p class="view_det"><span class="vw_text"><a href="<?= base_url()?>index.php/tours/details/<?=$top_destination_package[$k]->package_id?>">View details | </a>
									    <span class="plac_txt">Thiruvandrum</span></p>
									    <p class="off_det">Get Upto 30% OFF For this Package</p>

									  <!--   <div class="col-md-6 col-xs-6 pad4">
									   <p class="book_txt">Book Now</p></div> -->

									<div class="col-md-12  col-xs-12 pad6">
									<a href="<?= base_url()?>index.php/tours/details/<?=$top_destination_package[$k]->package_id?>" class="en_text">Submit Enquiry</a></div>
									   <div class="deal_txt">                                       
					<div class="col-xs-6 nopad">             
                          	 <p class="spc_rate">Check Your Special Rate</p> 
                          	 <p class="coupon_text nw_coupon" >Redeem Points | Enter Coupon</p>  
                          	 <p style="    font-size: 12.5px;"><span class="nw_coupon">Need Assistance ? </span> Contact Us</p>
                          	 </div>                                      
                          	  <div class="col-xs-6 nopad">
                          	                                          
                          	     <p class="ref_text">Refer and Earn Cash points</p>
								 <p class="cont_phon">Call  - +919489676576</p>  
                          	     <h4 class="deal_price"> <?php echo isset($top_destination_package[$k]->price)?get_converted_currency_value ( $currency_obj->force_currency_conversion ( $top_destination_package[$k]->price ) ):0; ?></h4>
                          	     <p class="per_text"> Per Person *</p> 
                          	     </div>                                         
                          	      </div>
                          	      </div>
									</div>
							</div>
							<?php } $k++ ;	} $total = $total-1;
								?>
						</div>
					</div>
					<?php }?>
				</div>
			
		</div>
</div> 

<div class="clearfix"></div>
 <div class="perhldys_new">
		<div class="container">
				<div class="pagehdwrap">
					<h2 class="pagehding">Popular Hotels</h2>
					<span><i class="fal fa-star"></i></span>
				</div>
				<div class="col-md-12 nopad">
				<?php 
				// debug($top_destination_hotel);
				// exit("833-index-general");
				foreach ($top_destination_hotel as $hotel) { ?>
					<div class="col-md-4 pad_3 htd-wrap">
					
					<div class="pop_img_det">
						<img class="img-responsive pop_img" src="<?= $GLOBALS['CI']->template->domain_images($hotel['image']); ?>">
						</div>
						<div class="pop_hot_det">
						<div class="hot_det">
						<p class="hot_place"><?= $hotel['city_name'] ?></p>
						<p class="hot_price"> Start from <span>&#x20b9; <?=rand(5000,10000); ?></span> </p>
						  <input type="hidden" class="top-des-val hand-cursor" value="<?=hotel_suggestion_value($hotel['city_name'], $hotel['country'])?>">
						    <input type="hidden" class="top_des_id hand-cursor" value="<?=$hotel['origin']?>">
						</div>
					</div>
</div>
				<?php }?>
				<!-- 	<div class="col-md-8 pad_3 ">
					
					<div class="pop_img_det">
						<img class="img-responsive pop_img" src="<?= $GLOBALS['CI']->template->domain_images(); ?>top-dest-hotel-82.jpg">
						</div>
						<div class="pop_hot_det">
						<div class="hot_det">
						<p class="hot_place">Europe</p>
						<p class="hot_price"> Start from <span>&#x20b9; 9500</span> </p>
						</div>
					</div>
</div>
					<div class="col-md-4 pad_3 ">
					
					<div class="pop_img_det">
						<img class="img-responsive pop_img" src="<?= $GLOBALS['CI']->template->template_images(); ?>dubai.png">
						</div>
						<div class="pop_hot_det">
						<div class="hot_det">
						<p class="hot_place">Dubai</p>
						<p class="hot_price"> Start from <span>&#x20b9; 5500</span> </p>
						</div>
						</div>
					</div>
						<div class="col-md-4 pad_3 ">
					
					<div class="pop_img_det">
						<img class="img-responsive pop_img" src="<?= $GLOBALS['CI']->template->template_images(); ?>london.png">
						</div>
						<div class="pop_hot_det">
						<div class="hot_det">
						<p class="hot_place">Lodon</p>
						<p class="hot_price"> Start from <span>&#x20b9; 5500</span> </p>
						</div>
						</div>
					</div>
						<div class="col-md-4 pad_3 ">
					
					<div class="pop_img_det">
						<img class="img-responsive pop_img" src="<?= $GLOBALS['CI']->template->template_images(); ?>greece.png">
						</div>
						<div class="pop_hot_det">
						<div class="hot_det">
						<p class="hot_place">Greece</p>
						<p class="hot_price"> Start from <span>&#x20b9; 5500</span> </p>
						</div>
						</div>
					</div>
						<div class="col-md-4 pad_3 ">
					
					<div class="pop_img_det">
						<img class="img-responsive pop_img" src="<?= $GLOBALS['CI']->template->template_images(); ?>scot.png">
						</div>
						<div class="pop_hot_det">
						<div class="hot_det">
						<p class="hot_place">Scotland</p>
						<p class="hot_price"> Start from <span>&#x20b9; 5500</span> </p>
						</div>
						</div>
					</div> -->
				</div>
		</div>
</div>
<div class="clearfix"></div>
 <div class="perhldys" >
		<div class="container">
			<!--<div class="col-md-12 dome_secs" style="">
				<div class="pagehdwrap">
					<h2 class="pagehding_new">Special Offers on Domestic Flights</h2>
				</div>

				<?php 
					$departure=Date('Y-m-d');
               		$departure = date('Y-m-d', strtotime($departure. ' + 7 day'));

               		// debug($top_destination_flight);
               		// exit("930");
				foreach ($top_destination_flight as $flight) { ?>
				<div class="col-md-3 pad_3 flight-wrap">
					<div class="col-md-12 special_offer pad0">
						<div class="col-md-5 pad0">
						<img class="img-responsive " src="<?php echo $GLOBALS ['CI']->template->domain_images ($flight['image_path']) ?>">
						</div>
							  
						<div class="col-md-7">
							<p class="flight_text">Flights From</p>
							<p class="flight_dir"><?= $flight['from_airport_city']?> - <?= $flight['destination_airport_city']?></p>
						</div>
						
						</div>
						<form action="<?php echo base_url();?>index.php/general/pre_flight_search" name="flight_home" id="flight_form-home_<?= $flight['origin_code']?>_<?= $flight['destination_code']?>">
							<input type="hidden" name="from" class="from" value="<?= $flight['from_airport_city']?>">
							<input type="hidden" name="from_loc_id" class="from_loc_id" value="<?= $flight['origin_code']?>">
							<input type="hidden" name="to" class="to" value="<?= $flight['destination_airport_city']?>">
							<input type="hidden" name="to_loc_id" class="to_loc_id" value="<?= $flight['destination_code']?>">
							<input type="hidden" name="adult" value="1">
							<input type="hidden" name="child" value="0">
							<input type="hidden" name="infant" value="0">
							<input type="hidden" name="child" value="0">
							<input type="hidden" name="v_class" class="v_class" value="<?= 'economy'?>">
							<input type="hidden" name="depature" class="depature" value="<?=$departure?>">
							<input type="hidden" name="trip_type" class="trip_type" value="<?= 'oneway'?>">
						</form>
						<input type="hidden" data-triptype="<?='oneway'?>"
               	  	data-fromcity="<?=$flight['from_airport_city']."(".$flight['from_airport_code'].")" ?>"
               	    data-tocity="<?=$flight['destination_airport_city']."(".$flight['destination_airport_code'].")" ?>"
               	    data-fromlocid="<?php echo $flight['origin_code'] ?>" data-tolocid="<?=$flight['destination_code']?>"
               	    data-departure="<?=date('d F Y',strtotime($departure))?>" 
               	   
				</div>
				<?php } ?>
				<!-- <div class="col-md-6 pad_6 ">
					<div class="col-md-12 special_offer pad0">
						<div class="col-md-5 pad0">
						<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>d5.jpg">
						</div>
						<div class="col-md-7">
							<p class="flight_text">Flights From</p>
							<p class="flight_dir">Delhi -Mumbai</p>
						</div>
						</div>
				</div>
				<div class="col-md-6 pad_6 ">
					<div class="col-md-12 special_offer pad0">
						<div class="col-md-5 pad0">
						<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>d5.jpg">
						</div>
						<div class="col-md-7">
							<p class="flight_text">Flights From</p>
							<p class="flight_dir">Delhi -Mumbai</p>
						</div>
						</div>
				</div>
				<div class="col-md-6 pad_6">
				<div class="col-md-12 special_offer pad0">
					<div class="col-md-5 pad0">
					<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>d5.jpg">
					</div>
					<div class="col-md-7">
						<p class="flight_text">Flights From</p>
						<p class="flight_dir">Delhi -Mumbai</p>
					</div>
					</div>
				</div> -->
				<!-- </div>
				<div class="col-md-6 domes_new" style=" ">
				
				<div class="pagehdwrap">
					<h2 class="pagehding_new">Special Offers on Domestic Flights</h2>
				</div> -->
				<!-- <div class="col-md-6 pad_6 ">
				<div class="col-md-12 special_offer pad0">
					<div class="col-md-5 pad0">
					<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>d5.jpg">
					</div>
					<div class="col-md-7">
						<p class="flight_text">Flights From</p>
						<p class="flight_dir">Delhi -Mumbai</p>
					</div>
					</div>
				</div>
				<div class="col-md-6 pad_6 ">
					<div class="col-md-12 special_offer pad0">
						<div class="col-md-5 pad0">
						<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>d5.jpg">
						</div>
						<div class="col-md-7">
							<p class="flight_text">Flights From</p>
							<p class="flight_dir">Delhi -Mumbai</p>
						</div>
						</div>
				</div>
				<div class="col-md-6 pad_6 ">
					<div class="col-md-12 special_offer pad0">
						<div class="col-md-5 pad0">
						<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>d5.jpg">
						</div>
						<div class="col-md-7">
							<p class="flight_text">Flights From</p>
							<p class="flight_dir">Delhi -Mumbai</p>
						</div>
						</div>
				</div>
				<div class="col-md-6 pad_6">
				<div class="col-md-12 special_offer pad0">
					<div class="col-md-5 pad0">
					<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>d5.jpg">
					</div>
					<div class="col-md-7">
						<p class="flight_text">Flights From</p>
						<p class="flight_dir">Delhi -Mumbai</p>
					</div>
					</div>
				</div>
				</div> 
		</div>	-->
</div>	
<div class="clearfix"></div>
 <div class="perhldys_new new_bg">
		<div class="container">
			<div class="col-md-4">
					<h2 class="let_text">Lets get Started ?</h2>
					<p class="let_des">Whether you are booking a plane ticket,looking for a hotel room,need help with viasas or want to insure your trip

					</p>
					<p class="help_text">Need Help?</p>
				</div>
				<div class="col-md-8">
					<div class="col-md-4">
					<div class="col-md-8 col-md-offset-2 col_box">
						<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>icon1.png">
						<p>Best Price</p>
						</div>
					</div>
					<div class="col-md-4">
					<div class="col-md-8 col-md-offset-2 col_box">
						<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>icon2.png">
						<p>Easy Booking</p>
						</div>
					</div>
					<div class="col-md-4">
					<div class="col-md-8 col-md-offset-2 col_box">
						<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>icon4.png">
						<p>Trust & Safety</p>
						</div>
						</div>
						<div class="col-md-4">
					<div class="col-md-8 col-md-offset-2 col_box">
						<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>icon3.png">
						<p>Print E-Ticket</p>
						</div>
					</div>
					<div class="col-md-4">
					<div class="col-md-8 col-md-offset-2 col_box">
						<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>icon3.png">
						<p>Cancellation</p>
						</div>
					</div>
					<div class="col-md-4">
					<div class="col-md-8 col-md-offset-2 col_box">
						<img class="img-responsive " src="<?= $GLOBALS['CI']->template->template_images(); ?>icon5.png">
						<p>Customer Support</p>
						</div>
					</div>
					</div>
				</div>
		</div>
</div>

<div class="perhldys_new">
        <div class="container">
            <div class="pagehdwrap_new">
				<h2 class="pagehding">
			      Lets Talk about Travel Blog
				</h2>  
            </div>
          
			<div class="col-md-12 nopad">					
				<div id="owl-demo3" class="owl-carousel owlindex3 tour_style">
				  <?php 
				  // debug($blog_list);	
			foreach ($blog_list as $blog) { 
				//debug($blog);
			// exit;
			?>


					<div class="item pading-right-15">
						<!-- ThemePark Hong kong -->
							<input type="hidden" class="destination_id" name="destination_id" value="1219">
							<input type="hidden" class="destination_name" name="destination_name" value="Hong kong">

							<input type="hidden" class="category_id" name="category_id" value="14">

						<div class="col-xs-12">
						<div class="main_img">
							<img class="blog_img" src="<?= $GLOBALS['CI']->template->domain_images($blog['blog_image']); ?>" alt="">
							<div class="main_details">
							<div class="main_details_image">
							<img src="<?= $GLOBALS['CI']->template->template_images(); ?>lo_icon.png" alt="">
							<p class="main-det-cont"><?= $blog['blog_title'] ?></p>
							</div>
							<p class="red_mre"><a href="<?php echo base_url(); ?>index.php/general/blog?blog_id=<?=$blog['blog_id'] ?>">Read More</a></p>
							</div>
						</div>
						</div>
					</div>
					<?php } ?>
				</div> 
			</div>
        </div>
    </div>
<div class="clearfix"></div>
		 <div class="perhldys" >
				<div class="container">
				 <div class="col-md-12 nopad">
		
		 <div class="col-md-12 nopad">
		  <div class="col-md-6 col-xs-6">
			<h2 class="ex_text">Exclusive tonight only deals.<br>
			Only in the App</h2>
	<p class="ex_det">Discover hotel,flight and rental car deals exclusively in the app.
			Download today to stay connected with important trip details -any-time,anywhere</p>
<div class="col-md-3 col-xs-6 pad0">	
<img  class="img-responsive  " src="<?= $GLOBALS['CI']->template->template_images(); ?>play.png" alt="">
</div>
<div class="col-md-3 col-xs-6 pad0">
<img  class="img-responsive  " src="<?= $GLOBALS['CI']->template->template_images(); ?>app.png" alt="">
</div>
		</div>
		  <div class="col-md-6 col-xs-6">
		  				<img  class="img-responsive ex_img " src="<?= $GLOBALS['CI']->template->template_images(); ?>mob.jpg" alt="">
		  </div>
		</div></div>
</div>
</div>
<div class="clearfix"></div>
 <div class="perhldys_new" style="padding: 40px 0 0">
		<div class="row">
				<div class="pagehdwrap">
					<h2 class="pagehding">Tripidia's More International Activity</h2>
					<span><i class="fal fa-star"></i></span>
				</div>
				<?php 
				foreach ($package_list['data'] as $k => $package) { 

					echo ($k == 0) ? get_package0($package,$k) : ''; 
					echo ($k == 1) ? '<div class="col-md-4 pad0">' : '';
						if($k == 1 || $k == 2 || $k == 3 || $k == 4) {
							
							echo get_package1($package,$k); 
						}
					echo ($k == 4) ? '</div>' : '';
					echo ($k == 5) ? get_package0($package,$k) : ''; 
					?>
					
				<?php } ?>
		</div>
</div>

			<!-- Top Airliners -->
			<!-- <div class="topAirlineOut hide">
				<div class="container">
						<div class="pagehdwrap">
							<h2 class="pagehding">Top Airlines</h2>
							<span><i class="fal fa-star"></i></span>
						</div>
						<div class="airlinecosmic">
							<div id="TopAirLine" class="owlindex2 owl-carousel owl-theme">
								<div class="item">
									<div class="airlinepart" style="line-height: 100px !important;">
										<img src="<?php echo $GLOBALS['CI']->template->template_images('top_airlines/top_air1.png'); ?>" alt="">  
										<span class="airlin_name">Cathay Pacific Airways</span>  
									</div>
								</div>
								<div class="item">
									<div class="airlinepart" style="line-height: 100px !important;">   
										<img src="<?php echo $GLOBALS['CI']->template->template_images('top_airlines/top_air2.png'); ?>" alt="">   
										<span class="airlin_name">Qatar Airways</span>  
									</div>
								</div>
								<div class="item">
									<div class="airlinepart" style="line-height: 100px !important;">     
										<img src="<?php echo $GLOBALS['CI']->template->template_images('top_airlines/top_air3.png'); ?>" alt="">  
										<span class="airlin_name">Jet Airways</span>  
									</div>
								</div>
								<div class="item">
									<div class="airlinepart" style="line-height: 100px !important;">  
										<img src="<?php echo $GLOBALS['CI']->template->template_images('top_airlines/top_air4.png'); ?>" alt=""> 
										<span class="airlin_name">Singapore Airlines</span>  
									</div>
								</div>
								<div class="item">
									<div class="airlinepart" style="line-height: 100px !important;">    
										<img src="<?php echo $GLOBALS['CI']->template->template_images('top_airlines/top_air5.png'); ?>" alt="">  
										<span class="airlin_name">Turkish Airlines</span>  
									</div>
								</div>
								<div class="item">
									<div class="airlinepart" style="line-height: 100px !important;">
										<img src="<?php echo $GLOBALS['CI']->template->template_images('top_airlines/top_air1.png'); ?>" alt="">  
										<span class="airlin_name">Cathay Pacific Airways</span>  
									</div>
								</div>
								<div class="item">
									<div class="airlinepart" style="line-height: 100px !important;">   
										<img src="<?php echo $GLOBALS['CI']->template->template_images('top_airlines/top_air2.png'); ?>" alt="">   
										<span class="airlin_name">Qatar Airways</span>  
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
<div class="ychoose pagehdwrap hide">
<h2 class="pagehding choose_head">Why Choose Us</h2>
<span><i class="fal fa-star"></i></span>
	<div class="container">
		<div class="allys">
			<div class="col-xs-4">
				<div class="threey">
					<div class="apritopty"><i class="far fa-tag"></i></div>
					<div class="dismany">
				      	<div class="number">01.</div>
						<div class="hedsprite">Best Price Guaranteed</div>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="threey">
					<div class="apritopty"><i class="far fa-smile"></i></div>
					<div class="dismany">
					     <div class="number">02.</div>
						<div class="hedsprite">99.9% Customer Satisfaction</div>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="threey">
					<div class="apritopty"><i class="far fa-headphones"></i></div>
					<div class="dismany">
				     	<div class="number">03.</div>
						<div class="hedsprite">24/7 Customer Support</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->





<?=$this->template->isolated_view('share/js/lazy_loader')?>
<?php 
	function get_package0($list_item,$k)
	{
		$img_name ="honey.jpg";
		if($k==0){
			$img_name = "holiday_packages.jpg";
		}
		else if($k==5){
			$img_name = "kualalumpur_1.jpg";
		}
		return '<div class="col-md-4 pad0">
					<a href="'.base_url().'index.php/tours/search?package_type='.$list_item['package_types_id'].'">
						<img  class="img-responsive int_img" src="'.$GLOBALS['CI']->template->template_images().$img_name.'" alt="" as="'.$k.'">
						<div class="details">
						 <div class="new_det"><p class="new_det_p">'.$list_item['package_types_name'].'</p>
						<p class="trip_amt">Starting at &#x20b9; 9736</p>
						</div>
						</div>
					</a>
				</div>';
	} 

	function get_package1($list_item,$k) {
		$img_name ="beach.jpg";
		if($k==1){
			$img_name = "honey.jpg";
		}
		else if($k==2){
			$img_name = "kashmir_packages.jpg";
		}
		else if($k==3){
			$img_name = "kualalumpur.jpg";
		}
		else if($k==4){
			$img_name = "europe.jpg";
		}

		return '<div class="col-md-6 pad0">
					<a href="'.base_url().'index.php/tours/search?package_type='.$list_item['package_types_id'].'">
							<img  class="img-responsive smal_img" src="'.$GLOBALS['CI']->template->template_images().$img_name.'" alt="" as="'.$k.'">
							<div class="details">
							 <div class="new_det"><p class="new_det_p">'.$list_item['package_types_name'].'</p>
							 <p class="trip_amt">Starting at &#x20b9; 9736</p>
							</div>
							</div>
					</a>
				</div>';
	}
?>
<script>
    $(document).ready(function() {
        var owl3 = $("#owl-demo3");

        owl3.owlCarousel({      
            itemsCustom : [
                [0, 1],
                [450, 1],
                [551, 1],
                [700, 1],
                [1000, 3],
                [1200, 3],
                [1400, 3],
                [1600, 3]
			],
			navigation: true,
        	pagination: false

        });

    });
</script>

<script type="text/javascript">
	    $(document).ready(function() {
           

			$(".quick_info").click(function (event) {
				 var val = $(this).attr('val');
				$('#quick_content_'+val).slideToggle();
				event.stopPropagation();
				console.log(val);
			});
           
 //           $("#villaButton").click(function(){
	// 	var input = document.createElement("input");
	// 	input.setAttribute("type", "hidden");
	// 	input.setAttribute("name", "villa");
	// 	input.setAttribute("value", "villa");
	// 	//append to form element that you want .
	// 	document.getElementById("villa-form-submit").appendChild(input);
	// });

                
    });
</script>