  <?php
/*$image = $banner_images ['data'] [0] ['image'];*/
$active_domain_modules = $this->active_domain_modules;
$default_active_tab = $default_view;
/**
 * set default active tab
 *
 * @param string $module_name
 *          name of current module being output
 * @param string $default_active_tab
 *          default tab name if already its selected otherwise its empty
 */
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/index.js'), 'defer' => 'defer');

function set_default_active_tab($module_name, &$default_active_tab) {
   if (empty ( $default_active_tab ) == true || $module_name == $default_active_tab) {
      if (empty ( $default_active_tab ) == true) {
         $default_active_tab = $module_name; // Set default module as current active module
      }
      return 'active';
   }
}
?>

    <div class="allformst">
         
         <!-- Tab panes -->
         <div class="container inspad hide">
         <div class="tab_border ">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs tabstab">

               <?php if (is_active_package_module()) { ?>
               <li
                  class="<?php echo set_default_active_tab(META_PACKAGE_COURSE, $default_active_tab)?>"><a
                  href="#holiday" role="tab"
                  data-toggle="tab">
                  <span class="sprte iconcmn"><i class="fal fa-tree"></i></span><label>Holidays</label></a></li>
               <?php } ?>               
            </ul>
         </div>
         <div class="secndblak">
               <div class="tab-content custmtab">
                  <?php if (is_active_package_module()) { ?>
                  <div
                     class="tab-pane <?php echo set_default_active_tab(META_PACKAGE_COURSE, $default_active_tab)?>"
                     id="holiday">
                     <?php echo $GLOBALS['CI']->template->isolated_view('share/holiday_search',$holiday_data)?>
                  </div>
                  <?php } ?>                  
               </div>
            </div>
         </div>
         </div>
<div class="perhldys">
   <div class="container">
    
         <div class="pagehdwrap col-md-12 ">

         <h2 class="pagehding">Popular Destinations</h2>
         <span><i class="fal fa-star"></i></span>
      </div>

      <div class="retmnus">
      
            <?php
               //debug($top_destination_package);
               #debug($top_destination_package[0]);
               //exit; 
               $k = 0;
               #echo "total".$total;
               while ( @$total > 0 ) {
                  ?> 
            <div class="col-xs-12 col-md-4 nopad hol-wrap">
               <div class="col-xs-12 nopad">
                  <?php for($i=1;$i<=1;$i++) { 
                     #echo "kk".$k.'<br/>';
                     if(isset($top_destination_package[$k])){
                        // debug($top_destination_package[$k]);
                        // exit("724-index");
                     $package_country = $this->package_model->getCountryName($top_destination_package[$k]->package_country);
                     #debug($package_country);
                     #echo "sdfsdf ".($k % 2).'<br/>';
                     ?>
                  <div class="topone">
                     <div class="inspd2 effect-lexi">
                        <div class="imgeht2">
                        <div class="dealimg">
                           <img
                              class="lazy lazy_loader"
                              data-src="<? echo $GLOBALS['CI']->template->domain_upload_pckg_images(basename($top_destination_package[$k]->image)); ?>"
                              alt="<?php echo $top_destination_package[$k]->package_name; ?>"
                              src="<? echo $GLOBALS['CI']->template->domain_upload_pckg_images(basename($top_destination_package[$k]->image)); ?>"
                              />

                              <div class="off_div"><?= $top_destination_package[$k]->duration-1; ?>N / <?= $top_destination_package[$k]->duration;?>D</div>
                        </div>


                          <input type="hidden" class="country_holiday hand-cursor" value="<?php echo $top_destination_package[$k]->package_country?>">
                           <input type="hidden" class="package_type" value="<?php echo $top_destination_package[$k]->package_type?>">
                        </div>
                        <div class="content_new col-md-12">
                           <?php if(($k % 2) == 0) {?>
                           <div class=" absintcol1  col-md-6 nopad">
                              <?php } else {?>
                              <div class=" absintcol2 col-md-6 nopad">
                                 <?php } ?>
                                 <div class="absinn">
                                    <div class="smilebig2">
                                    
                                       <h3><?php echo $top_destination_package[$k]->package_name; ?> </h3>
                                       
                                    <!--  <h4><?php echo $top_destination_package[$k]->package_city;?>, <?php echo $package_country->name; ?></h4>-->
                                       
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                 </div>
                              </div>
                              <div class="col-md-6 nopad">
                                 <p class="quick_info">Quick Info ^</p>
                              </div>
                           

                           <figcaption>   
                           <div class="deal_txt">
                                       
                                       <div class="col-xs-6 nopad" >
                                           <p class="spec_rat_text">Check Your Special Rate</p>                                        <p class="coupon_text">Redeem Points | Enter Coupon</p>
                                       </div>
                                       <div class="col-xs-6 nopad">
                                         <!--<img class="star_rat" src="<?php echo $GLOBALS['CI']->template->template_images('star_rating.png')?>" alt="">-->
                                         <p class="old_price"> &#x20b9; 2400</p>
                                         <h4 class="deal_price"> &#x20b9; <?php echo isset($top_destination_package[$k]->price)?get_converted_currency_value ( $currency_obj->force_currency_conversion ( $top_destination_package[$k]->price ) ):0; ?></h4><p class="per_text"> Per Person *</p>
                                         </div>

                                           <!--<div class="col-xs-6 nopad">
                                         <h4><?php echo isset($top_destination_package[$k]->duration)?($top_destination_package[$k]->duration-1):0; ?> Nights / <?php echo isset($top_destination_package[$k]->duration)?$top_destination_package[$k]->duration:0; ?> Days</h4> 
                                       <a class="package_dets_btn" href="<?=base_url().'index.php/tours/details/'.$top_destination_package[$k]->package_id?>">
                                    View details
                                    </a>  
                                 </div>      
                                         </div>--> 
                                     </figcaption>

                        </div>
                        </div>
                        <div class="hover_content">
                           <div class=" absintcol1  col-md-6 nopad"><div class="absinn"><div class="smilebig2"><h3>Meadows &amp; Mountains (Tour Code – KAS 03) </h3></div><div class="clearfix"></div></div></div>
                           <div class="col-md-6 nopad">
                           <p class="quick_info">Quick Info <span> </span></p>
                           </div>
                               <div class="col-md-12 nopad">
                               <p class="view_det"><span class="vw_text">View details | </span>
                               <span class="plac_txt">Thiruvandrum</span></p>
                               <p class="off_det">Get Upto 30% OFF For this Package</p><div class="col-md-6 col-xs-6 pad4"><p class="book_txt">Book Now</p></div>
                           <div class="col-md-6  col-xs-6 pad4"><p class="en_text">Submit Enquiry</p></div>
                              <div class="deal_txt">                                       
               <div class="col-xs-6 nopad">             
                            <p class="spc_rate">Check Your Special Rate</p> 
                            <p class="coupon_text nw_coupon" >Redeem Points | Enter Coupon</p>  
                            <p style="    font-size: 12.5px;"><span class="nw_coupon">Need Assistance ? </span> Contact Us</p>
                            </div>                                      
                             <div class="col-xs-6 nopad">
                                                                     
                                <p class="ref_text">Refer and Earn Cash points</p>
                         <p class="cont_phon">Call  - +919489676576</p>  
                                <h4 class="deal_price"> ₹ 11600</h4>
                                <p class="per_text"> Per Person *</p> 
                                </div>                                         
                                 </div>
                                 </div>
                           </div>
                     </div>
                     <?php } $k++ ; } $total = $total-2;
                        ?>
                  </div>
               </div>
               <?php }?>
            </div>
         
      </div>
   </div>
</div>
<!-- <script type="text/javascript">
    $('.hol-wrap').on('click', function(e) {

      // alert("1");
        e.preventDefault();
        var curr_destination = $('.country_holiday', this).val();

        alert(curr_destination);
        $('#country').val(curr_destination);
        $('#package_type').val("");
        $('#duration').val("");
        $('#holiday_search').submit()
    });
</script> -->
