<div class="promo_section">
	<div class="promo_panel">
		<h1>Register and Get Discount on Booking First <?php echo ucfirst($deals['category']); ?> with Us </h1>
		<div class="col-xs-12 nopad offers-wrapper">
			<div class="col-xs-12 col-md-8 details-left nopad">
				<div class="col-xs-6 nopad">
                    <div class="offer_section">
                        <span> BOOKING PERIOD </span>
						<div>                                 
                            <p class="ofr_dtl">Till 
                            <?php 
                            $date_cnv = date_create($deal['expiry_date']);
                            $expiry_date = date_format($date_cnv,"jS F Y "); 
                            echo $expiry_date;?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 nopad">
                    <div class="promo_code_section">
                        <widget type="ticket" class="--flex-column">
                            <div class="top --flex-column">
                                <div class="deetz --flex-row-j!sb">
                                    <div class="event --flex-column">
                                        <div class="font12 txt-cntr">PROMO CODE</div>
                                    </div>
                                </div>
                            </div>
                            <div class="rip"></div>
                            <div class="bottom --flex-row-j!sb">
                                <div class="cp_code"><?php echo $deals['deal_code']?></div>
                            </div>
                       	</widget>
                    </div>
                </div>
				<div class="col-xs-12 nopad">
                    <div class="details-box">                                
                        <h3>What You Get?</h3>
                        <div class="content">
                        	<ul class="dot-list">
                                <ul>
                                	<li>Users will get Rs. 500 discounts on domestic flights, Rs.1000 discounts on international flights.</li>
                                </ul>
								<table class="table_ofrall" width="100%" cellspacing="0" cellpadding="0">			
									<tbody>
										<tr>
											<th width="16%" height="50" align="center" valign="middle">
												<strong style="color:#FFF;">Product</strong>
											</th>
      										<th width="29%" height="50" align="center" valign="middle">
      											<strong style="color:#FFF;">Offer</strong>
      											<br>
        										<span style="font-size:10px;color:#fff;font-weight:normal">(Min Transaction: Rs.3500)</span>
        									</th>
										</tr>   
									    <tr>
									      <td width="16%" height="40" align="center" valign="middle"><strong style="color:#000000">DOMESTIC FLIGHT</strong></td>
									      <td height="40" align="center" valign="middle" style="color:#000000 !important"><strong>Rs 500</strong><br>
									        instant discount<br>
									        <span style="font-size:10px;">(Minimum Booking Rs.3500/-)</span></td>
									    </tr>
								     	<tr>
									      <td width="16%" height="40" align="center" valign="middle"><strong style="color:#000000">INTERNATIONAL FLIGHT</strong></td>
									      <td height="40" align="center" valign="middle" style="color:#000000 !important"><strong>Rs 1,000</strong><br>
									        instant discount<br>
									        <span style="font-size:10px;">(Minimum Booking Rs.7500/-)</span></td>
									    </tr>
    
    
  									</tbody>
								</table>
                            </ul>
                        </div>
                    </div>
                    <div class="details-box">
                        <h3>How do you get it?</h3>
                        <div class="content">
	                        <ul class="dot-list">
	                            <div><ul><li>To avail discounts, users have to register/sign-up for booking <?php echo ucfirst($deals['category']); ?> tickets for their preferred destination by applying <b>coupon code: <?php echo $deals['deal_code']?></b></li><li>This offer is valid for limited period</li><li>The offer is valid on <?php echo ucfirst($deals['category']); ?> bookings</li><li>The offer is valid for bookings made on Tripidia's website, Mobile site, Android & iOS App</li></ul></div>
	                        </ul>
                        </div>
                    </div>
                    <div class="details-box">                                
                        <h3>What else do you need to know?</h3>
                        <div class="content">
                            <ul class="dot-list">
                                <ul>
                                	<li>Convenience fee will be charged as per the applicability</li>
                                	<li>The offer can’t be clubbed with any other promotional offers</li>
                                	<li>Bookings with the valid promo codes will be only eligible for this offer</li>
                                	<li>This offer is applicable for only new customers, to be redeemed only once</li>
                                	<li>This offer cannot be clubbed with any other offer running on Tripidia</li>
                                	<li>To avail this offer customer need to process his booking with registered mail id</li>
                                	<li>In case of partial/full cancellation the offer stands void and discount will be rolled back before processing the refunds.</li>
                                	<li>Child / infant discount, date or flight change, refund charges, weekend surcharge, black out period, travel restrictions and / or flight restriction will be also applicable as per the fare rule</li>
                                	<li>Changes in flights and dates are allowed with change fees and fare difference</li>
                                	<li>Changes in names are not allowed</li>
                                </ul>
                            </ul>
                    	</div>
                	</div>
                	<div class="details-box">
                        <h3>Terms & Conditions</h3>
                        <div class="content">
                            <ul class="dot-list">
                                <div>
                                	<ul>
                                		<li>In the event of any misuse or abuse of the offer, Tripidia reserves the right to deny the offer to the customers.</li>
                                		<li>Tripidia is the sole authority for interpretation of these terms.</li>
                                		<li>In addition, Tripidia standard booking and privacy policy on www.Tripidia.com shall apply.</li>
                                		<li>In the event of any dispute, Courts of New Delhi will have jurisdiction.</li>
                                		<li>Tripidia reserves the right, at any time, without prior notice and liability and without assigning any reason whatsoever, to add/alter/modify/change or vary all of these terms and conditions or to replace, wholly or in part, this offer by another offer, whether similar to this offer or not, or to extend or withdraw it altogether.</li>
                                		<li>Tripidia shall not be liable for any loss or damage arising due to force majeure.</li>
                                	</ul>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-xs-12 col-md-4 details-right">
				<div class="coupon_offer_details">
					<img class="img-responsive int_img" src="<?php echo $GLOBALS['CI']->template->template_images('honey.jpg'); ?>" alt="">
					<div class="img_price">
						Get
						<span>                  
                    <?php if($deals['deal_type'] == "minus") { 
                        echo "Rs.";
                        }?>
                    <?=$deals['discount']?>  
                    <?php if($deals['deal_type'] == "percentage"){
                    echo " %";
                    }else{
                        }?>
                        
                    </span> Off on <?php echo ucfirst($deals['category']); ?>				
					</div>
					<div class="code_text">Use Coupon: <span><b><?php echo $deals['deal_code'];?></b></span>.</div>
					<div class="code_text">Valid Till: <span><?php echo $expiry_date; ?></span></div>
				</div>				
			</div>
		</div>
	</div>
</div>