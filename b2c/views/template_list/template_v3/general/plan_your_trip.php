<?php 
if ($module=='flight' || $module=='') {
   $tab['flight']['btn_class']='active';
   $tab['flight']['div_class']='in active';
}elseif ($module=='hotel') {
   $tab['hotel']['btn_class']='active';
   $tab['hotel']['div_class']='in active';
}elseif ($module=='car') {
   $tab['car']['btn_class']='active';
   $tab['car']['div_class']='in active';
}elseif ($module=='holiday') {
   $tab['holiday']['btn_class']='active';
   $tab['holiday']['div_class']='in active';
}

?>
<style>
.drop_menu{
    padding: 0;
}
.invalid-ip{
  border: 1px solid #ff5b5b !important;
}
</style>
<div class="airline mybkng">
    <div class="row new-colbg">
          <div class="container" style="padding: 0;"> 
              <div class="col-md-4">
              <div class="new-grey-bg">
              <div class="grey-title"><i class="fa fa-phone" style="transform: rotate(110deg); "aria-hidden="true"></i>&nbsp;For quick help</div>
              <p style="">+91 8156 84 84 84</p>
              </div>
              </div>
              <div class="col-md-4"> 
               <div class="new-grey-bg">
               <div class="grey-title"><i class="fa fa-clock" aria-hidden="true"></i>&nbsp;Save Time</div> 
               <p style="">No need to surf Multiple Sites for packages, quotes, travel plans </p>
               </div>                 
                 </div>
                 <div class="col-md-4"> 
                  <div class="new-grey-bg">
                  <div class="grey-title"><i class="fa fa-phone"  style="transform: rotate(110deg); " aria-hidden="true"></i>&nbsp;Customer Support</div>                    
                  <p style="">24X7 support</p>
          </div>
          </div>
          </div>
    </div>

   <div class="container-fluid">
      <div class="col-md-12 perhldys">


      <div class="col-md-8 col-md-push-2"><div class="col-md-12 left_pan">
              <div class="trip-title">Tell Us Your Requirement <hr></div>
        <div class="col-md-12 sub-title">Trip Details</div>
         <form id="plan-trip" name="plan-trip" method="post" action="<?=base_url().'index.php/tours/enquiry_package' ?>">

          <div class="clearfix"></div>

          <div class="col-md-12"></div>
          <div class="form-group col-md-12 date-ctrl-pl">
                  <input type="text" class="fromactivity form-control element" name="package_name" id="package_name" placeholder="Going to" required>
                  <input class="hide loc_id_holder" name="destination_id" type="hidden" id="destination_id" value="" >
                  <i class="fa fa-map-marker" aria-hidden="true"></i>
          </div>

          <div class="clearfix"></div>

          <div class="form-group col-md-6">
              <div class="nice-select nicSelct element" tabindex="0">
                  <select class="newslterinput form-control" name="duration" required>
                      <option value="">Duration</option>
                      <option value="1">1 Night/2 Days</option>
                      <option value="2">2 Nights/3 Days</option>
                      <option value="3">3 Nights/4 Days</option>
                      <option value="4">4 Nights/5 Days</option>
                      <option value="5">5 Nights/6 Days</option>
                      <option value="6">6 Nights/7 Days</option>
                      <option value="7">7 Nights/8 Days</option>
                      <option value="8">8 Nights/9 Days</option>
                      <option value="9">9 Nights/10 Days</option>
                      <option value="10">10 Nights/11 Days</option>
                      <option value="11">11 Nights/12 Days</option>
                      <option value="12">12 Nights/13 Days</option>
                      <option value="13">13 Nights/14 Days</option>
                      <option value="14">14 Nights/15 Days</option>
                      <option value="15+">More than 15 Days</option>
                  </select>
              </div>
          </div>
          <div class="form-group col-md-6 date-ctrl-pl date-small-screen">
                <input type="text" class="form-control element" name="start_date" id="plan_traveldate" placeholder="Travel date" id="plan_traveldate" readonly required>
                <i class="fa fa-calendar" style="top:15px;"></i>
          </div>

          <div class="clearfix"></div>

          <div class="form-group col-md-3 mb15px-imp">                     
                <div class="nice-select nicSelct element" tabindex="0">
                    <select class="newslterinput form-control" name="adult" required>
                        <option value="">Adults</option>
                        <option value="6+">Group</option>
                        <option value="6">6 people</option>
                        <option value="5">5 people</option>
                        <option value="4">4 people</option>
                        <option value="3">3 people</option>
                        <option value="2">2 people</option>
                        <option value="1">1 person</option>
                    </select>
                </div>
          </div>
          <div class="form-group col-md-3 mb15px-imp">
                <div class="nice-select nicSelct element" tabindex="0">
                    <select class="newslterinput form-control" class="newslterinput" name="child" required>
                      <option value="0">0 Kid</option>
                      <option value="1">1 Kids</option>
                      <option value="2">2 Kids</option>
                      <option value="3">3 Kids</option>
                      <option value="4">4 Kids</option>
                      <option value="5">5 Kids</option>
                    </select>
                </div>
          </div>
          <div class="form-group col-md-6 mb15px-imp">
              <div class="nice-select nicSelct element" tabindex="0">
                  <select class="newslterinput form-control" name="budget" required="">
                      <option value="">Budget</option>
                      <option value="economy">Economy (0 - 2 star)</option>
                      <option value="standard">Standard (3 - 4 star)</option>
                      <option value="luxury">Luxury (5 star &amp; above)</option>
                  </select>
              </div>
          </div>

          <div class="clearfix"></div>

          <div class="form-group col-md-12">
                <textarea name="message" id="message" class="form-control element" placeholder="Requirements" style="" required=""></textarea>
          </div>
          <div class="col-md-12 sub-title">Contact Details</div>
          <div class="form-group col-md-12 date-ctrl-pl">
                <input type="text" class="fromactivity form-control element" name="departurecity" id="departurecity" placeholder="Departure City" required>
                <i class="fa fa-map-marker" aria-hidden="true" ></i>
          </div>

          <div class="clearfix"></div>

          <div class="form-group col-md-6">
              <input type="text" class="form-control element" name="name" id="name" placeholder="Name" value="" onkeyup="chk_names(this);" required>
          </div>
          <div class="form-group col-md-6">
              <input type="email" class="form-control element" name="email" id="email" placeholder="Email" value="" onblur="validateEmail(this);" required>
          </div>

          <div class="clearfix"></div>

          <div class="form-group col-md-2">
              <input type="text" class="numbers form-control element" name="isd" id="isd" placeholder="91" maxlength="3" required>
          </div>
          <div class="form-group col-md-10 pad-when-small" style="padding-left: 0;">
              <input type="text" class="numbers form-control element" name="mobile" id="mobile" placeholder="Mobile" maxlength="10" minlength="10" value="" required>
          </div>

          <div class="clearfix"></div>

          <input type="hidden" name="packname" id="packname">

          <div class="col-md-12">
              <input type="submit" value="Get Your Tour Quote" name="submit" class="btn btn-orange btn-primary col-md-12 element sndenqry">
          </div>
    </form>
                
  </div>
</div>
</div>
</div>

<script type="text/javascript">
$( document ).ready(function() {
   $('#plan_traveldate').datepicker({
     formatDate:'Y/m/d',
    minDate: new Date()
   }); 
});

 $(function(){
    $("#send_enquiry_button").click(function(){
       $('.msg').hide();
    });
 });

 $('.numbers').keypress(function(e) {
          var a = [];
          var k = e.which;

          for (i = 48; i < 58; i++)
              a.push(i);

          if (!(a.indexOf(k)>=0))
              e.preventDefault();
      });

 function chk_names(obj){
  var valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ";
  var ok = "yes";
  var temp;
  var validchars="";
  var dot = 0;
  for (var i=0; i<obj.value.length; i++) {
      temp = obj.value.substring(i,i+1);
      if (valid.indexOf(temp) == "-1") {
          ok = "no";
          break;
      } else {
          validchars=validchars+temp;
      }
  }

  if (ok == "no") {
      alert("Only Alpha characters are allowed!");
      obj.value = validchars;
      return false;
  }
  return true;
}

function validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField.value) == false) 
        {
            alert('Invalid Email Address');
            emailField.value = "";
            return false;
        }

        return true;
}

</script>
<script type="text/javascript">
   
  $(document).ready(function(){


  $(".fromactivity").catcomplete({

        source: function(request, response) {
            var term = request.term;
            //console.log(cache);
            // if (term in cache) {
            //     response(cache[term]);
            //     return
            // }
            $.getJSON(app_base_url + "index.php/ajax/get_sightseen_city_list", request, function(data, status, xhr) {
                //cache[term] = data;
                response(data)
            })
        },
        minLength: 3,
        autoFocus: true,
        select: function(event, ui) {
            var label = ui.item.label;
            var category = ui.item.category;
            $(this).siblings('.loc_id_holder').val(ui.item.id);
            
           
        },
        change: function(ev, ui) {
            if (!ui.item) {
                $(this).val("")
            }
        }
    }).bind('focus', function() {
        $(this).catcomplete("search")
    }).catcomplete("instance")._renderItem = function(ul, item) {
        var auto_suggest_value = highlight_search_text(this.term.trim(), item.value, item.label);
        var hotel_count = '';
        var count = parseInt(item.count);
        if (count > 0) {
            var h_lab = '';
            if (count > 1) {
                h_lab = 'Hotels'
            } else {
                h_lab = 'Hotel'
            }
            hotel_count = '<span class="hotel_cnt">(' + parseInt(item.count) + ' ' + h_lab + ')</span>'
        }
        return $("<li class='custom-auto-complete'>").append('<a> <span class="fal fa-map-marker-alt"></span> ' + auto_suggest_value + ' ' + hotel_count + '</a>').appendTo(ul)
    };
  });

</script>