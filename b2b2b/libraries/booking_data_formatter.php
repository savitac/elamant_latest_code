<?php
/** 
 *
 * Formates the Booking Data in the application
 *
 * @package	Provab
 * @subpackage	provab
 * @category	Libraries
 * @author		Jaganath<jaganath.provab@gmail.com>
 * @link		http://www.provab.com
 */
class Booking_Data_Formatter {
	public function __construct()
	{
		
	}
	/**
	 * Jaganath
	 * @param array $booking_details
	 */
	function format_bus_booking_data($complete_booking_details, $module)
	{
		//debug($complete_booking_details); die;
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$booking_details = array();
		$currency_obj = new Currency();
		$master_booking_details = $complete_booking_details['data']['booking_details'];
		$itinerary_details = $this->format_itinerary_details($complete_booking_details['data']['booking_itinerary_details']);		
		$customer_details = $this->format_customer_details($complete_booking_details['data']['booking_customer_details']);
		
		//echo debug($customer_details);exit;
		foreach($master_booking_details as $book_k => $book_v) {
			
			$core_booking_details = $book_v;
			$app_reference = $core_booking_details['app_reference'];
			$booking_itinerary_details = $itinerary_details[$app_reference];
			$booking_customer_details = $customer_details[$app_reference];
			
			if($module == 'b2b' || $module == 'b2c' ){
				//get the converted currency rate for booking transaction
				$booking_customer_details = $this->display_currency_for_bus($book_v,$booking_customer_details);
				//get the converted currency rate for convenience amount
				$core_booking_details = $this->display_convenience_amount($book_v);
			}
			
			//Calculating Price
			$fare = 0;
			$admin_commission = 0;
			$agent_commission = 0;
			$admin_tds = 0;
			$agent_tds = 0;
			$admin_markup = 0;
			$agent_markup = 0;
			$seat_numbers = '';
			
			foreach($booking_customer_details as $customer_k => $customer_v) {
				$fare 				+= floatval($customer_v['fare']);
				$admin_commission 	+= floatval($customer_v['admin_commission']);
				$agent_commission 	+= floatval($customer_v['agent_commission']);
				$admin_tds 			+= floatval($customer_v['admin_tds']);
				$agent_tds 			+= floatval($customer_v['agent_tds']);
				$admin_markup 		+= floatval($customer_v['admin_markup']);
				$agent_markup 		+= floatval($customer_v['agent_markup']);
				$seat_numbers .= trim($customer_v['seat_no']).',';
			}
			$core_booking_details['fare'] = roundoff_number($fare);
			$core_booking_details['admin_commission'] = roundoff_number($admin_commission);
			$core_booking_details['agent_commission'] = roundoff_number($agent_commission);
			$core_booking_details['admin_tds'] = roundoff_number($admin_tds);
			$core_booking_details['agent_tds'] = roundoff_number($agent_tds);
			$core_booking_details['admin_markup'] = roundoff_number($admin_markup);
			$core_booking_details['agent_markup'] = roundoff_number($agent_markup);
			$admin_buying_price = $this->admin_buying_price($core_booking_details);
			$core_booking_details['admin_buying_price'] = roundoff_number($admin_buying_price[0]);
			if($module == 'b2b') {
				$agent_buying_price = $this->agent_buying_price($core_booking_details);
				$core_booking_details['agent_buying_price'] = roundoff_number($agent_buying_price[0]);
			} else {
				$core_booking_details['agent_buying_price'] = 0;
			}
			$grand_total = $this->total_fare($core_booking_details, $module);
			$core_booking_details['grand_total'] = $grand_total[0];
			
			//currency
			$currency = admin_base_currency();
			if($module == 'b2b' || $module == 'b2c' ){
				$currency = $book_v['currency'];			
			}
			$core_booking_details['currency'] = $currency_obj->get_currency_symbol($currency);			
			$core_booking_details['departure_from'] = $booking_itinerary_details[0]['departure_from'];
			$core_booking_details['arrival_to'] = $booking_itinerary_details[0]['arrival_to'];
			$core_booking_details['journey_datetime'] = $booking_itinerary_details[0]['journey_datetime'];
			$core_booking_details['departure_datetime'] = $booking_itinerary_details[0]['departure_datetime'];
			$core_booking_details['arrival_datetime'] = $booking_itinerary_details[0]['arrival_datetime'];
			$core_booking_details['pax_count'] = count($booking_customer_details);
			$core_booking_details['operator'] = $booking_itinerary_details[0]['operator'];
			$core_booking_details['bus_type'] = $booking_itinerary_details[0]['bus_type'];
			$core_booking_details['seat_numbers'] = rtrim($seat_numbers, ',');
			//Lead Pax Details
			$core_booking_details['lead_pax_name'] = $booking_customer_details[0]['name'];
			$core_booking_details['lead_pax_phone_number'] = $core_booking_details['phone_number'];
			$core_booking_details['lead_pax_email'] = $core_booking_details['email']; 
			//Domain Details
			$domain_details = $this->domain_details($core_booking_details['domain_origin']);
			$core_booking_details['domain_name'] = $domain_details['domain_name'];
			$core_booking_details['domain_ip'] = $domain_details['domain_ip'];
			$core_booking_details['domain_key'] = $domain_details['domain_key'];
			$core_booking_details['theme_id'] = $domain_details['theme_id'];
			$core_booking_details['domain_logo'] = $domain_details['domain_logo'];
			$core_booking_details['booked_date'] = app_friendly_absolute_date($core_booking_details['created_datetime']);
			//Formating the data
			//Booking Details	
			$booking_details[$app_reference] = $core_booking_details;
			
			//Itinerary Details
			$booking_details[$app_reference]['booking_itinerary_details'] = $booking_itinerary_details;
			
			//Customer Details
			$booking_details[$app_reference]['booking_customer_details'] = $booking_customer_details;
			
		}
		$booking_details = $this->convert_as_array($booking_details);
		//debug($booking_details);die;
		$response['data']['booking_details'] = $booking_details;
		return $response;
	}
	
	/**Sagar Wakchaure
	 * 
	 * @param array $currency_rate
	 * @param unknown $booking_customer_details
	 * @return number
	 */
	function display_currency_for_bus($currency_rate = array(),$booking_customer_details = array()){
		//echo debug($booking_customer_details);exit;
				$conversion_rate = $currency_rate['currency_conversion_rate'];
				foreach($booking_customer_details as $key => $sub_booking_customer_details){
					$booking_customer_details[$key]['fare'] =  $this->add_currencyrate($sub_booking_customer_details['fare'],$conversion_rate);
					$booking_customer_details[$key]['admin_commission'] =  $this->add_currencyrate($sub_booking_customer_details['admin_commission'],$conversion_rate);
					$booking_customer_details[$key]['agent_commission'] =  $this->add_currencyrate($sub_booking_customer_details['agent_commission'],$conversion_rate);
					$booking_customer_details[$key]['admin_tds'] =  $this->add_currencyrate($sub_booking_customer_details['admin_tds'],$conversion_rate);
					$booking_customer_details[$key]['agent_tds'] =  $this->add_currencyrate($sub_booking_customer_details['agent_tds'],$conversion_rate);
					$booking_customer_details[$key]['admin_markup'] =  $this->add_currencyrate($sub_booking_customer_details['admin_markup'],$conversion_rate);
					$booking_customer_details[$key]['agent_markup'] =  $this->add_currencyrate($sub_booking_customer_details['agent_markup'],$conversion_rate);
				}
				return $booking_customer_details;
	}
	
	/**
	 * Jaganath
	 * @param array $booking_details
	 */
	function format_hotel_booking_data($complete_booking_details, $module)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$booking_details = array();
		$currency_obj = new Currency();
		$master_booking_details = $complete_booking_details['data']['booking_details'];
	   //echo debug($master_booking_details);exit;
		$itinerary_details = $this->format_itinerary_details($complete_booking_details['data']['booking_itinerary_details']);
	    //echo debug($itinerary_details);exit;
	
		$customer_details = $this->format_customer_details($complete_booking_details['data']['booking_customer_details']);
		$cancellation_details = $this->format_hotel_cancellation_details($complete_booking_details['data']['cancellation_details']);
		foreach($master_booking_details as $book_k => $book_v) {
			$core_booking_details = $book_v;
			
			$app_reference = $core_booking_details['app_reference'];
			$booking_itinerary_details = $itinerary_details[$app_reference];
			$booking_customer_details = $customer_details[$app_reference];
			$booking_cancellation_details = @$cancellation_details[$app_reference];		
			if($module == 'b2b' || $module == 'b2c' ){
				//get the converted currency rate for booking transaction
				$booking_itinerary_details = $this->display_currency_for_hotel($book_v,$itinerary_details[$app_reference]);
				//get the converted currency rate for convenience amount
				$core_booking_details = $this->display_convenience_amount($book_v);
			}
			//Calculating Price
			$fare = 0;
			$admin_markup = 0;
			$agent_markup = 0;
			foreach($booking_itinerary_details as $itinerary_k => $itinerary_v) {
				$fare += $itinerary_v['total_fare'];
				$admin_markup += floatval($itinerary_v['admin_markup']);
				$agent_markup += floatval($itinerary_v['agent_markup']);
			}
			//PaxCount
			$adult_count = 0;
			$child_count = 0;
			foreach($booking_customer_details as $customer_k => $customer_v) {
				$pax_type = $customer_v['pax_type'];
				if($pax_type == 'Adult') {
					$adult_count++;
				} else if($pax_type == 'Child'){
					$child_count++;
				}
			}
			//Formatiing the data
			//Booking Details
			$attributes = json_decode($core_booking_details['attributes'], true);
			$total_nights = get_date_difference($core_booking_details['hotel_check_in'], $core_booking_details['hotel_check_out']);
			$core_booking_details['hotel_image'] = $attributes['HotelImage'];
			$core_booking_details['hotel_location'] = $booking_itinerary_details[0]['location'];
			$core_booking_details['hotel_address'] = $attributes['HotelAddress'];
			$core_booking_details['cancellation_policy'] = $attributes['CancellationPolicy'];
			$core_booking_details['total_nights'] = $total_nights;
			$core_booking_details['total_rooms'] = count($booking_itinerary_details);
			$core_booking_details['adult_count'] = $adult_count;
			$core_booking_details['child_count'] = $child_count;
			$core_booking_details['fare'] = roundoff_number($fare);
			$core_booking_details['admin_markup'] = roundoff_number($admin_markup);
			$core_booking_details['agent_markup'] = roundoff_number($agent_markup);
			$admin_buying_price = $this->admin_buying_price($core_booking_details);
			$core_booking_details['admin_buying_price'] = roundoff_number($admin_buying_price[0]);
			if($module == 'b2b') {
				$agent_buying_price = $this->agent_buying_price($core_booking_details);
				$core_booking_details['agent_buying_price'] = roundoff_number($agent_buying_price[0]);
			} else {
				$core_booking_details['agent_buying_price'] = 0;
			}
			$grand_total = $this->total_fare($core_booking_details, $module);
			$core_booking_details['grand_total'] = roundoff_number($grand_total[0]);
			
			//get currency
			$currency = admin_base_currency();
			if($module == 'b2b' || $module == 'b2c' ){
			    $currency = $book_v['currency'];
			}
			$core_booking_details['currency'] = $currency_obj->get_currency_symbol($currency);
			
			$core_booking_details['voucher_date'] = app_friendly_absolute_date($core_booking_details['created_datetime']);
			//Lead Pax Details
			$core_booking_details['cutomer_city'] = $attributes['billing_city'];
			$core_booking_details['cutomer_zipcode'] = $attributes['billing_zipcode'];
			$core_booking_details['cutomer_address'] = $attributes['address'];
			$core_booking_details['cutomer_country'] = $attributes['billing_country'];
			$core_booking_details['lead_pax_name'] = $booking_customer_details[0]['title'].' '.$booking_customer_details[0]['first_name'].' '.$booking_customer_details[0]['last_name'];
			$core_booking_details['lead_pax_phone_number'] = $core_booking_details['phone_number'];
			$core_booking_details['lead_pax_email'] = $core_booking_details['email']; 
			//Domain Details
			$domain_details = $this->domain_details($core_booking_details['domain_origin']);
			$core_booking_details['domain_name'] = $domain_details['domain_name'];
			$core_booking_details['domain_ip'] = $domain_details['domain_ip'];
			$core_booking_details['domain_key'] = $domain_details['domain_key'];
			$core_booking_details['theme_id'] = $domain_details['theme_id'];
			$core_booking_details['domain_logo'] = $domain_details['domain_logo'];
			//Formating the data
			//Booking Details
			$booking_details[$app_reference] = $core_booking_details;
			//Itenary Details
			$booking_details[$app_reference]['itinerary_details'] = $booking_itinerary_details;
			//Customer Details
			$booking_details[$app_reference]['customer_details'] = $booking_customer_details;
			$booking_details[$app_reference]['cancellation_details'] = $booking_cancellation_details;
		}
	
		$booking_details = $this->convert_as_array($booking_details);
		$response['data']['booking_details'] = $booking_details;
		return $response;
	}

	/**
	 * Convert price details  into respective currency rate for hotel
	 * @param array $currency_rate
	 * @param unknown $itinerary_details
	 * @return number
	 */
	function display_currency_for_hotel($currency_rate = array(),$itinerary_details){
		$conversion_rate = $currency_rate['currency_conversion_rate'];
		foreach($itinerary_details as $key => $sub_itinerary_details){
			$itinerary_details[$key]['total_fare'] =  $this->add_currencyrate($sub_itinerary_details['total_fare'],$conversion_rate);
			$itinerary_details[$key]['admin_markup'] =  $this->add_currencyrate($sub_itinerary_details['admin_markup'],$conversion_rate);
			$itinerary_details[$key]['agent_markup'] =  $this->add_currencyrate($sub_itinerary_details['agent_markup'],$conversion_rate);
			$itinerary_details[$key]['RoomPrice'] =  $this->add_currencyrate($sub_itinerary_details['RoomPrice'],$conversion_rate);
			$itinerary_details[$key]['Discount'] =  $this->add_currencyrate($sub_itinerary_details['Discount'],$conversion_rate);
		}
		return $itinerary_details;
	}
	
	/**
	 * Jaganath
	 * @param array $booking_details
	 */
	function format_flight_booking_data($complete_booking_details, $module)
	{
		$GLOBALS['CI']->load->library('currency');
		$GLOBALS['CI']->load->helper('currency');
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		/*if($module == "b2b"){
			echo 'b2b\b2c';exit;
			return $response;
		}*/
		$booking_details = array();
		$currency_obj = new Currency();
		$master_booking_details = $complete_booking_details['data']['booking_details'];
		$itinerary_details = $this->format_itinerary_details($complete_booking_details['data']['booking_itinerary_details']);
		$segment_details = $this->format_segment_details($complete_booking_details['data']['booking_itinerary_details']);
		$transaction_details = $this->format_flight_transaction_details($complete_booking_details['data']['booking_transaction_details']);		
		$customer_details = $this->format_flight_customer_details($complete_booking_details['data']['booking_customer_details'], $complete_booking_details['data']['cancellation_details']);
		foreach($master_booking_details as $book_k => $book_v) {
		  
		    $is_domestic = $GLOBALS['CI']->flight_model->is_domestic_flight($book_v['from_loc'], $book_v['to_loc']);
			$core_booking_details = $book_v;
			$app_reference = $core_booking_details['app_reference'];
			$booking_itinerary_details = $itinerary_details[$app_reference];
	
			if(isset($transaction_details[$app_reference])) {
				$booking_transaction_details = $transaction_details[$app_reference];					
				//check the module
				if($module == 'b2b' || $module == 'b2c' ){			
					//get the converted currency rate for booking transaction  
					$booking_transaction_details = $this->display_currency_for_flight($book_v,$transaction_details[$app_reference]);
					//get the converted currency rate for convenience amount
					$book_v = $this->display_convenience_amount($book_v);
				}
				
			} else {
				$booking_transaction_details = array();//Remove Later
			}
			
		   
			$fare = 0;
			$admin_commission = 0;
			$agent_commission = 0;
			$admin_tds = 0;
			$agent_tds = 0;
			$admin_markup = 0;
			$agent_markup = 0;
			$pnr = '';
			$trip_type_label = '';
			foreach($booking_transaction_details as $transaction_k => $transaction_v) {
				//Assign Segment Details for the Transaction
				if($core_booking_details['trip_type'] == 'circle' && $is_domestic == true) {
					$booking_transaction_details[$transaction_k]['segment_details'] = @$segment_details[($transaction_k+1)];
				} else {
					$booking_transaction_details[$transaction_k]['segment_details'] = $segment_details;
				}
				//Assign Pax Details for the Transaction
				$booking_transaction_details[$transaction_k]['booking_customer_details'] = @$customer_details[$transaction_v['origin']];
				//Calculating Price
				$fare 				+= $transaction_v['total_fare'];
				$admin_commission	+=$transaction_v['admin_commission'];
				$agent_commission 	+=$transaction_v['agent_commission'];
				$admin_tds 			+= floatval($transaction_v['admin_tds']);
				$agent_tds 			+= floatval($transaction_v['agent_tds']);
				
				$admin_markup +=$transaction_v['admin_markup'];
				$agent_markup +=$transaction_v['agent_markup'];
				$pnr .= $transaction_v['pnr'].'/';
			}
			if($core_booking_details['trip_type'] == 'oneway') {
				$trip_type_label = 'Oneway';
			} else if($core_booking_details['trip_type'] == 'circle') {
				$trip_type_label = 'Roundway';
			} else if($core_booking_details['trip_type'] == 'multicity') {//FIXME:Miltiway
				$trip_type_label = 'MutiCity';
			}
			$core_booking_details['trip_type_label'] = $trip_type_label;
			$core_booking_details['is_domestic'] = @$is_domestic;
			$core_booking_details['pnr'] = rtrim($pnr, '/');
			$core_booking_details['fare'] = roundoff_number($fare);
			$core_booking_details['admin_commission'] = roundoff_number($admin_commission);
			$core_booking_details['agent_commission'] = roundoff_number($agent_commission);
			$core_booking_details['admin_tds'] = roundoff_number($admin_tds);
			$core_booking_details['agent_tds'] = roundoff_number($agent_tds);
			
			$net_commission = ($admin_commission+$agent_commission);
			$net_commission_tds = ($admin_tds+$agent_tds);
			$core_booking_details['net_commission'] = roundoff_number($net_commission);
			$core_booking_details['net_commission_tds'] = roundoff_number($net_commission_tds);
			$core_booking_details['net_fare'] = roundoff_number($fare-$net_commission+$net_commission_tds);
			$core_booking_details['admin_markup'] = roundoff_number($admin_markup);
			$core_booking_details['agent_markup'] = roundoff_number($agent_markup);
			$admin_buying_price = $this->admin_buying_price($core_booking_details);
			$core_booking_details['admin_buying_price'] = roundoff_number($admin_buying_price[0]);
			$agent_buying_price = $this->agent_buying_price($core_booking_details);
			$core_booking_details['agent_buying_price'] = roundoff_number($agent_buying_price[0]);
			//echo debug($core_booking_details);exit;
			$grand_total = $this->total_fare($core_booking_details, $module);
			$core_booking_details['grand_total'] = roundoff_number($grand_total[0]);
			//echo $core_booking_details['grand_total'];exit;
			//currency
			$currency = admin_base_currency();
			if($module == 'b2b' || $module == 'b2c' ){
				$currency = $book_v['currency'];
			}
			$core_booking_details['currency'] = $currency_obj->get_currency_symbol($currency);//FIXME: Remove the hardcoded value "INR" Later
			
			//Lead Pax Details
			$attributes = json_decode($core_booking_details['attributes'], true);
			$core_booking_details['cutomer_city'] = $attributes['city'];
			$core_booking_details['cutomer_zipcode'] = $attributes['zipcode'];
			$core_booking_details['cutomer_address'] = $attributes['address'];
			$core_booking_details['cutomer_country'] = $attributes['country'];
			$lead_pax_details = @$booking_transaction_details[0]['booking_customer_details'][0];
			$core_booking_details['lead_pax_name'] = $lead_pax_details['title'].' '.$lead_pax_details['first_name'].' '.$lead_pax_details['last_name'];
			$core_booking_details['lead_pax_phone_number'] = $core_booking_details['phone'];
			$core_booking_details['lead_pax_email'] = $core_booking_details['email']; 
			//Domain Details
			$domain_details = $this->domain_details($core_booking_details['domain_origin']);
			$core_booking_details['domain_name'] = $domain_details['domain_name'];
			$core_booking_details['domain_ip'] = $domain_details['domain_ip'];
			$core_booking_details['domain_key'] = $domain_details['domain_key'];
			$core_booking_details['theme_id'] = $domain_details['theme_id'];
			$core_booking_details['domain_logo'] = $domain_details['domain_logo'];
			$core_booking_details['booked_date'] = app_friendly_absolute_date($core_booking_details['created_datetime']);
			//Formating the data
			//Booking Details	
			$booking_details[$app_reference] = $core_booking_details;
			//Itinerary Details
			$booking_details[$app_reference]['booking_itinerary_details'] = $booking_itinerary_details;
			//Customer Details
			$booking_details[$app_reference]['booking_transaction_details'] = $booking_transaction_details;
		}
		$booking_details = $this->convert_as_array($booking_details);
		$response['data']['booking_details'] = $booking_details;
		return $response;
	}
	
	
	 /**
	 *Convert price details  into respective currency rate for flight  
	 * Sagar Wakchaure
	 * @param array $currency_rate
	 * @param array $transaction_details
	 * @return unknown
	 */
	function display_currency_for_flight($currency_rate = array(),$transaction_details = array()){
			$conversion_rate = $currency_rate['currency_conversion_rate']; 
			foreach($transaction_details as $key => $sub_transaction_details){
					$transaction_details[$key]['total_fare'] =  $this->add_currencyrate($sub_transaction_details['total_fare'],$conversion_rate);
					$transaction_details[$key]['admin_commission'] =  $this->add_currencyrate($sub_transaction_details['admin_commission'],$conversion_rate);
					$transaction_details[$key]['agent_commission'] =  $this->add_currencyrate($sub_transaction_details['agent_commission'],$conversion_rate);
					$transaction_details[$key]['admin_tds'] =  $this->add_currencyrate($sub_transaction_details['admin_tds'],$conversion_rate);
					$transaction_details[$key]['agent_tds'] =  $this->add_currencyrate($sub_transaction_details['agent_tds'],$conversion_rate);
					$transaction_details[$key]['admin_markup'] =  $this->add_currencyrate($sub_transaction_details['admin_markup'],$conversion_rate);
					$transaction_details[$key]['agent_markup'] =  $this->add_currencyrate($sub_transaction_details['agent_markup'],$conversion_rate);
			}		
			return $transaction_details;
	}
	
	 /**
	  * calculate total price with currency rate
	 *Sagar Wakchaure
	 * @param unknown $value
	 * @param unknown $conversion_rate
	 */
	function add_currencyrate($value,$conversion_rate){
		return ($value*$conversion_rate);
	}
	
	/**
	 * convert convenience amount into respective currency rate
	 * Sagar Wakchaure
	 * @param array $v_book
	 * @return unknown
	 */
	function display_convenience_amount($v_book = array()){		
		$v_book['convinence_amount'] =  $this->add_currencyrate($v_book['convinence_amount'],$v_book['currency_conversion_rate']);
		$v_book['discount'] =  $this->add_currencyrate($v_book['discount'],$v_book['currency_conversion_rate']);
		return $v_book;
	}
	
	/**Jaganath
	 * Get Domain Details
	 * @param unknown_type $domain_origin
	 */
	private function domain_details($domain_origin)
	{ 
		$domain_details = $GLOBALS['CI']->custom_db->single_table_records('domain_list', '*', array('origin' => intval(1)));
		return $domain_details['data'][0];
	}
	/**
	 * Jaganath
	 * Returns Total Fare
	 * @param array $fare_details
	 */
	function total_fare($fare_details, $module)
	{
		$fare_details = force_multple_data_format($fare_details);
		$total_fare = array();
		if($module == 'b2c') {//B2C Total Fare
			foreach($fare_details as $k => $v) {
				$fare = (isset($v['fare']) == true ? $v['fare'] : $v['total_fare']);
				$total_fare[$k] = roundoff_number($fare+$v['admin_markup']+$v['convinence_amount']-$v['discount']);
			}
		} else if($module == 'b2b') {//B2B Total Fare
			foreach($fare_details as $k => $v) {
				$fare = (isset($v['fare']) ? $v['fare'] : $v['total_fare']);
				$total_fare[$k] = roundoff_number($fare+$v['admin_markup']+$v['convinence_amount']+$v['agent_markup']);
			}
		}else if($module == 'admin') {//B2B Total Fare
			foreach($fare_details as $k => $v) {
				$fare = (isset($v['fare']) ? $v['fare'] : $v['total_fare']);
				$total_fare[$k] = roundoff_number($fare+$v['admin_markup']+$v['convinence_amount']+$v['agent_markup']-$v['discount']);
			}
		}
		return $total_fare;
	}
	/**
	 * Jaganath
	 * Returns Agent Buying Fare
	 * @param array $fare_details
	 */
	function admin_buying_price($fare_details)
	{
		$fare_details = force_multple_data_format($fare_details);
		//debug($fare_details);exit;
		$admin_buying_price = array();
		foreach($fare_details as $k => $v) {
			$fare = (isset($v['fare']) ? $v['fare'] : $v['total_fare']);
			$admin_commission	= floatval(@$v['admin_commission']);
			$tds_on_commission	= floatval(@$v['admin_tds']);
			$admin_buying_price[$k] = ($fare-$admin_commission);//FIXME:Check Calculation -- Jaganath
		}
		return $admin_buying_price;
	}
	/**
	 * Jaganath
	 * Returns Agent Buying Fare
	 * @param array $fare_details
	 */
	function agent_buying_price($fare_details)
	{
		$fare_details = force_multple_data_format($fare_details);
		$agent_buying_price = array();
		foreach($fare_details as $k => $v) {
			$fare = (isset($v['fare']) ? $v['fare'] : $v['total_fare']);
			$agent_commission	= floatval(@$v['agent_commission']);
			$tds_on_commission	= floatval(@$v['agent_tds']);
			$agent_buying_price[$k] = $fare+$v['admin_markup']+$tds_on_commission-$agent_commission;
		}
		return $agent_buying_price;
	}
	/**
	 * Format Itinerary Details
	 * @param unknown_type $itinerary_details
	 */
	private function format_itinerary_details($itinerary_details)
	{
		$GLOBALS['CI']->load->helper('custom/date_helper');
		$booking_itinerary_details = array();
		foreach($itinerary_details as $itinerary_k => $itinerary_v) {
			$itinerary_v['total_duration'] = get_duration_label(calculate_duration(@$itinerary_v['arrival_datetime'], @$itinerary_v['departure_datetime']));
			$booking_itinerary_details[$itinerary_v['app_reference']][] = $itinerary_v;
		}
		return $booking_itinerary_details;
	}
	/**
	 * Formatting the Segment details based on Segment Indicator(Onward/Return)
	 */
	private function format_segment_details($itinerary_details)
	{
		$booking_segment_details = array();
		foreach($itinerary_details as $itinerary_k => $itinerary_v) {
			$itinerary_v['total_duration'] = get_duration_label(calculate_duration($itinerary_v['arrival_datetime'], $itinerary_v['departure_datetime']));
			$booking_segment_details[$itinerary_v['segment_indicator']][] = $itinerary_v;
			//$booking_segment_details[$itinerary_k] = $itinerary_v;
		}
		return $booking_segment_details;
	}
	private function format_customer_details($customer_details)
	{
		$booking_customer_details = array();
		foreach($customer_details as $customer_k => $customer_v) {
			$booking_customer_details[$customer_v['app_reference']][] = $customer_v;
		}
		return $booking_customer_details;
	}
	private function format_hotel_cancellation_details($cancellation_details)
	{
		$booking_cancellation_details = array();
		foreach($cancellation_details as $cancel_k => $cancel_v) {
			$booking_cancellation_details[$cancel_v['app_reference']][] = $cancel_v;
		}
		return $booking_cancellation_details;
	}
	/**
	 * Format Flight Transaction Details
	 * @param unknown_type $itinerary_details
	 */
	private function format_flight_transaction_details($transaction_details)
	{
		$booking_transaction_details = array();
		foreach($transaction_details as $transaction_k => $transaction_v) {
			$booking_transaction_details[$transaction_v['app_reference']][] = $transaction_v;
		}
		return $booking_transaction_details;
	}
	/**
	 * Format Flight Customer Details
	 * @param unknown_type $itinerary_details
	 */
	private function format_flight_customer_details($customer_details, $cancellation_details)
	{
		$cancellation_details = $this->format_flight_cancellation_details($cancellation_details);
		
		$booking_customer_details = array();
		foreach($customer_details as $customer_k => $customer_v) {
			$temp_customer_details = $customer_v;
			//Assigning cancellation Details
			if(isset($cancellation_details[$temp_customer_details['origin']]) == true && valid_array($cancellation_details[$temp_customer_details['origin']]) == true) {
				$temp_customer_details['cancellation_details'] = $cancellation_details[$temp_customer_details['origin']];
			}
			$booking_customer_details[$customer_v['flight_booking_transaction_details_fk']][] = $temp_customer_details;
		}
		return $booking_customer_details;
	}
	/**
	 * 
	 */
	private function format_flight_cancellation_details($cancellation_details)
	{
		$customer_cancellation_details = array();
		foreach($cancellation_details as $cancel_k => $cancel_v) {
			if(intval($cancel_v['passenger_fk']) > 0) {
				$customer_cancellation_details[$cancel_v['passenger_fk']] = $cancel_v;
			}
		}
		return $customer_cancellation_details;
	}
	/*
	 * Total Booking Count
	 */
	function get_booking_counts()
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$data = array();
		$condition = array();
		$GLOBALS['CI']->load->model('bus_model');
		$GLOBALS['CI']->load->model('hotel_model');
		$GLOBALS['CI']->load->model('flight_model');
		$data['bus_booking_count'] = $GLOBALS['CI']->bus_model->booking($condition, true);
		$data['hotel_booking_count'] = $GLOBALS['CI']->hotel_model->booking($condition, true);
		$data['flight_booking_count'] = $GLOBALS['CI']->flight_model->booking($condition, true);
		$response['data'] = $data;
		return $response;
	}
	/*
	 * Jaganath
	 * Recent Activities
	 */
	function format_recent_transactions($transaction_details, $module)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$data = array();
		$details = array();
		foreach($transaction_details as $transaction_k => $transaction_v) {
			$admin_buying_price = $this->admin_buying_price($transaction_v);
			$core_booking_details['admin_buying_price'] = $admin_buying_price[0];
			if($module == 'b2b') {
				$agent_buying_price = $this->agent_buying_price($transaction_v);
				$core_booking_details['agent_buying_price'] = $agent_buying_price[0];
			} else {
				$core_booking_details['agent_buying_price'] = 0;
			}
			$total_fare = $this->total_fare($transaction_v, $module);
			$details[$transaction_k] = $transaction_v;
			$details[$transaction_k]['grand_total'] = $total_fare[0];
		}
		$data['transaction_details'] = $details;
		$response['data'] = $data;
		return $response;
	}
	/*
	 * Jaganath
	 * Implode Reference Ids
	 */
	function implode_app_reference_ids($booking_details)
	{
		$app_reference_ids = '';
		foreach($booking_details as $k => $v) {
			$app_reference_ids .= '"'.$v['app_reference'].'",';
		}
		$app_reference_ids = rtrim($app_reference_ids, ',');
		return $app_reference_ids;
	}
	/**
	 * Jaganath
	 * @param $booking_details
	 */
	function convert_as_array($booking_details)
	{
		if(count($booking_details) == 1) {
			$booking_details = array_values($booking_details);//FIXME: Jaganath
		}
		return $booking_details;
	}
	
	/**Sagar Wakchaure
	 * format master  transaction balance
	 */
	function format_master_transaction_balance($transacion_data = array(),$module){
		
		foreach ($transacion_data as $key => $transacion_sub_data){
			$transacion_data[$key]['amount'] = round($transacion_sub_data['amount']);
			if($module == 'b2b' || $module == 'b2c'){
				$transacion_data[$key]['amount'] = round($this->add_currencyrate($transacion_sub_data['amount'],$transacion_sub_data['currency_conversion_rate']));
			}
			
		}
		return $transacion_data;
	}
}
