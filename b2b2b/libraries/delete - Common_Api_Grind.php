<?php

/**
 * Provab Common Functionality For API Class
 *
 *
 * @package	Provab
 * @subpackage	provab
 * @category	Libraries
 * @author		Arjun J<arjun.provab@gmail.com>
 * @link		http://www.provab.com
 */
abstract class Common_Api_Grind {
	protected $DomainKey;
	function __construct()
	{
		 $CI = &get_instance();
		 $domain_key = trim($CI->session->userdata(DOMAIN_KEY));
		 if (empty($domain_key) == false) {
				$this->DomainKey =  (trim($domain_key));
			} else {
				$this->DomainKey = '';
			}
	}
	
	/**
	 *  Arjun J Gowda
	 * convert search params to format required by booking source
	 * @param number $search_id unique id which identifies search details
	 */
	abstract function search_data($search_id);

	/**
	 * Arjun J Gowda
	 * update markup currency and return summary
	 *
	 * @param array	 $price_summary
	 * @param object $currency_obj
	 */
	abstract function update_markup_currency(& $price_summary, & $currency_obj);

	/**
	 * Arjun J Gowda
	 * calculate and return total price details
	 * @param array $price_summary - price which has to be added
	 */
	abstract function total_price($price_summary);

	/**
	 * Arjun J Gowda
	 *Process Booking
	 * @param array $booking_params
	 */
	abstract function process_booking($book_id, $booking_params);
	
	/**
	 * return booking url to be used in the application for all the modules
	 */
	abstract function booking_url($search_id);
}
