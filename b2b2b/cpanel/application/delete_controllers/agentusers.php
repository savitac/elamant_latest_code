<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Agentusers extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		
		$this->load->model('General_Model');
		$this->load->model('Usertype_Model');
		$this->load->model('Users_Model');
		$this->load->model('Agents_Model');
		$this->load->model('Domain_Model');
		$this->load->model('Product_Model');
		$this->load->model('Api_Model');
		$this->load->model('Agentusers_Model');
		$this->load->model('Email_Model');
		$this->load->model('Promo_Model');
		$this->load->library('form_validation');
        $this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	$this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
  
     
	function agentPanel($user_id){
		$user_id 	= json_decode(base64_decode($user_id));
		$agentusers = $this->General_Model->getHomePageSettings();
		$agentusers['agentadmin_list'] = $this->Agentusers_Model->getAgentadminList($user_id);
		//echo '<pre>'; print_r($agentusers['agentadmin_list']); exit();
		$agentusers['parent_agent'] = $user_id;
		//$agentusers['promo'] = $this->Promo_Model->get_promo();
		$agentusers['parent_user'] = $user_id;
		$this->load->view('agents/agentusers/agentadmin_list',$agentusers);
	}


	function addAgentUsers($user_id1){
		$agentadmin 				=  $this->General_Model->getHomePageSettings();
		$agentadmin['country'] 		=  $this->General_Model->get_country_details();
		if(count($_POST) > 0){
			$form_validator = $this->formValidator('add');
			if($form_validator == FALSE) {
			  $this->load->view('agents/agentusers/add_adminagent',$agentadmin);
			} else{
			$email = $_POST['email_id'];
			$Query="select * from  user_details  where user_email ='".$email."' AND user_type_id='2' ";
			$query=$this->db->query($Query);
			if ($query->num_rows() > 0)
		{
			$data['status'] = '<div class="alert alert-block alert-danger">
							   <a href="#" data-dismiss="alert" class="close">×</a>
							   <h4 class="alert-heading">Already Email Id Registered!</h4>
							   kindly Use With Another Email Address !!!
							   </div>';
			$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$res = "";
			for ($i = 0; $i < 10; $i++) {
				$res .= $chars[mt_rand(0, strlen($chars)-1)];
			}
			redirect('agentusers/addAgentUsers/'.$user_id1,'refresh');
		}else{
			$user_profile_name = $this->General_Model->upload_image($_FILES, 'agent');
			try{
		    $this->General_Model->begin_transaction() ;
			$this->Agentusers_Model->addUseragentDetails($_POST,$user_profile_name);
			$user['email_template'] = $this->Email_Model->get_email_template('add_user_active')->row();
			$this->Email_Model->sendAddUserRegistration($user,$_POST);
			$this->General_Model->commit_transaction() ;
			redirect('agentusers/agentPanel/'.$user_id1,'refresh');
		
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction() ;
			return $e;
		} }
	}
		}else{
			
			$agentadmin['user']			= $user_id1;
			$agentadmin['admin_type']	=  $this->Usertype_Model->get_user_type_list();
			$this->load->view('agents/agentusers/add_adminagent',$agentadmin);
		}
	}

	function check_unique_email_agent($email){
		if($email!=''){
			if(preg_match("/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $email)){
				$Status 	=	$this->Agents_Model->getAllAgentEmail($email);
				if($Status  == "YES") {
					echo '<span class="nosuccess validate-has-error" for="email_id" style="display:inline-block;">This Email Id already Exists, Please Choose diffrent Email Id</span>';exit;
				}else{
					echo '<span class="success" style="color: green;">valid</span>';exit;
				}
			}else{
				echo '<span class="nosuccess">Please Enter the Valid Email Id</span>';exit;
			}
		}else{	
			 echo '<span class="nosuccess">Please Enter the Valid Email Id</span>';exit;
		}
	}

	function activeUsers($user_id1,$parent_user){
		$user_id 	= json_decode(base64_decode($user_id1));
		
		if($user_id != ''){
			$user['user_info'] = $this->Agentusers_Model->getAgentActivationadminList($user_id);
			$this->Agentusers_Model->activeUsers($user_id);
			$user['email_template'] = $this->Email_Model->get_email_template('add_user_active')->row();
			 
			$this->Email_Model->sendAddUserActive($user);
		}
		redirect('agentusers/agentPanel/'.$parent_user,'refresh');
	}

	function inactiveUsers($user_id1,$parent_user){
		$user_id 	= json_decode(base64_decode($user_id1));
		
		if($user_id != ''){
			$user['user_info'] = $this->Agentusers_Model->getAgentActivationadminList($user_id);
			$this->Agentusers_Model->inactiveUsers($user_id);
		$user['email_template'] = $this->Email_Model->get_email_template('user_deactivate')->row();

  	    $this->Email_Model->sendAddUserActive($user);
		}
		redirect('agentusers/agentPanel/'.$parent_user,'refresh');
	}

	function editAdminagent($user_id1,$parent_agent){ 
		$user_id 	= json_decode(base64_decode($user_id1));
		$parent_user = json_decode(base64_decode($parent_agent));
		if($user_id != ''){
			$adminagent 				= $this->General_Model->getHomePageSettings();
			$adminagent['parent_user']  = $parent_user;
			$adminagent['country'] 	=  $this->General_Model->get_country_details();
			$adminagent['users'] 	= $this->Agentusers_Model->getB2B2BAgentadminList($user_id,$parent_user);
			$adminagent['user_id'] = $user_id1;
			$this->load->view('agents/agentusers/edit_adminagent',$adminagent);
		}else{
			redirect('agentusers/agentPanel','refresh');
		}
		
	}

	function updateAdminagent($user_id1,$parent_user)
	{ 	
		$user_id 	= json_decode(base64_decode($user_id1));
		if($user_id != ''){
			if(count($_POST) > 0){
			/*$form_validator = $this->formValidator('edit');
				if($form_validator == FALSE) {
					redirect('agents/agentList');	
			    }else{*/
			$image_info_name = $this->General_Model->upload_image($_FILES, 'agent', $_REQUEST['old_image']);
			
				$this->Agentusers_Model->updateAdminagent($_POST,$user_id, $image_info_name);
				redirect('agents/agentList');
			//}
			}else{
				redirect('agents/agentList');
			}
		}else{
			redirect('agents/agentList');
		}
	}

	function sendEmail($user_id1)
	{ 
		$user_id 	= json_decode(base64_decode($user_id1));
		if($user_id != ''){
			$user 				= $this->General_Model->getHomePageSettings(); 
			$user['user_list'] 	= $this->Agentusers_Model->getSubAgentsUsers($user_id);
			$this->load->view('agents/agentusers/send_mail',$user);
		}else{
			redirect('agentusers/agentPanel/'.$user_id1,'refresh');
		}
	}

	 

	function sendMail($user_id1) 
	{  
		$data['firstname'] = $firstname = $this->input->post('firstname');
        $data['mailid']= $mailid = $this->input->post('mailid'); 
        $data['subject']= $subject = $this->input->post('subject');
        $data['description']=  $message = $this->input->post('description');
        $branchid = $this->input->post('branchid');
        $this->Email_Model->sendMailToUser($data);
        //echo '<script> alert("Mail Sent."); </script>';
        //redirect('adminagentusers/agentpanel/'.$user_id1, 'refresh');
        redirect('agentusers/agentPanel/'.$branchid,'refresh');
    }

    public function orders($user_details_id = ""){

		if($user_details_id != ""){
			$user =  base64_decode($user_details_id);
		$user_id = json_decode($user);  
		} else{
		
		$user_id = "";
		}
		//echo 'sanjay'; print_r(); 
		
		$booking 					= $this->General_Model->getHomePageSettings();
		$booking['booking_list'] 	=  $this->Users_Model->getBookingByParentPnr($user_id);
			//echo 'sanjay<pre>'; print_r($booking['booking_list']); exit();
		$booking['hotel_booking_list'] 	=  $this->Users_Model->getHotelBookingByParentPnr($user_id);
		//echo 'hi<pre>'; print_r($booking['hotel_booking_list']); exit();
		$booking['car_booking_list'] 	=  $this->Users_Model->getCarBookingByParentPnr($user_id);

		$booking['bundle_booking_list'] = $this->Users_Model->getBundleBookingByParentPnr($user_id);
		//echo 'sanjay<pre>'; print_r($booking['bundle_booking_list']); exit();
		$this->load->view('booking/booking_list',$booking);
	
	}

	function getProductPrivileges($user_id1,$agent){
		$user_id = json_decode(base64_decode($user_id1));
		$user 				= $this->General_Model->getHomePageSettings();
		$user['agent_id']	= $user_id1;
		$user['agent'] = $agent; 
	    $user['product_list'] 	= $this->Agents_Model->getProducts();
	    $this->load->view('agents/agentusers/productprivilege_info',$user); 
	}

	function update_product_privileges($user_id1){
		$user_id = json_decode(base64_decode($user_id1));
		if(count($_POST) > 0) 
		{
			$this->Agents_Model->update_product_privileges($_POST, $user_id);
		}
		redirect('agentusers/getProductPrivileges/'.$user_id1);
	}

     function formValidator($type){
	   $this->form_validation->set_rules('first_name', 'First Name', 'required');
	   $this->form_validation->set_rules('last_name', 'Last Name', 'required');
	   $this->form_validation->set_rules('email_id', 'Email', 'required');
	   $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required');
	   $this->form_validation->set_rules('company_name', 'Company Name', 'required');
	   $this->form_validation->set_rules('address', 'Address', 'required');
	   $this->form_validation->set_rules('city', 'City', 'required');
	   $this->form_validation->set_rules('state_name', 'State Name', 'required');
	   $this->form_validation->set_rules('zip_code', 'Zip Code', 'required');
	   
	   
	   if($type == 'add') {
	   	$this->form_validation->set_rules('first_name', 'First name', 'required|min_length[1]|max_length[50]');
	    $this->form_validation->set_rules('last_name', 'Last name', 'required|min_length[1]|max_length[50]');
	    
	    $this->form_validation->set_rules('email_id', 'Email', 'required|valid_email');
	    $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('company_name', 'Company Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('address', 'Address', 'required|min_length[3]|max_length[100]');
	    $this->form_validation->set_rules('state_name', 'State Name', 'required|min_length[1]|max_length[50]');
	    $this->form_validation->set_rules('zip_code', 'Zip Code', 'numeric|xss_clean');
	    
	    }

	  
	   return $this->form_validation->run();
    }	
}
