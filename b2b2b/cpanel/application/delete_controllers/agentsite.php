<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
ob_start();
class Agentsite extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Agentcontrol_Model');
			
		$this->load->library('form_validation');
	    $this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	 $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
   function index(){
	  $user = $this->General_Model->getHomePageSettings();
	  $user['b2b_site'] =$this->Agentcontrol_Model->get_all_menus();
	  $this->load->view('agent_controls/agent_control_list',$user );
   }
   
   function inactive_menu($menu_id1){
	   $menuid = json_decode(base64_decode($menu_id1));
	   if($menuid != ''){
		   $this->Agentcontrol_Model->inactive_menu($menuid);
		 }
		 redirect("agentsite");
	  
   }
   
   function active_menu($menu_id1){
	   $menuid = json_decode(base64_decode($menu_id1));
	   if($menuid != ''){
		   $this->Agentcontrol_Model->active_menu($menuid);
		 }
		 redirect("agentsite");
	  
   }
   
   function edit_menu($menu_id1){
	  $menuid = json_decode(base64_decode($menu_id1));
	  $user = $this->General_Model->getHomePageSettings();
	  $user['b2b_site'] =$this->Agentcontrol_Model->get_all_menus($menuid);
	  $this->load->view('agent_controls/edit_agent_menu',$user );
   }
   
   function update_menu($menu_id1){
	  $menuid = json_decode(base64_decode($menu_id1));
	 if($menuid != ''){
		 if(count($_POST) >0){
			$this->Agentcontrol_Model->update_agent_menu($_POST, $menuid); 
			redirect("agentsite"); 
		 }else{
			redirect("agentsite"); 
		 }
	 }else{
		  redirect("agentsite");
	 }
   }
	 
} 
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>
