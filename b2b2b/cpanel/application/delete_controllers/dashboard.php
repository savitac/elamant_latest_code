<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Dashboard extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->library('form_validation');
		
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	 $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
    public function index(){
		$data 			= $this->General_Model->getHomePageSettings();
		$data['default_section'] = $this->General_Model->getDefaultSections();
		$data['admin_id'] = $this->session->userdata('provabAdminId');
		$this->load->view('dashboard/index',$data);
	}
	
	public function loadBodyContent(){
		if(isset($_POST)){
				$data 				= $this->General_Model->getHomePageSettings();
				$dashboard_boday 	= $this->load->view('dashboard/dashboard_boday', $data,true);
				echo $dashboard_boday;exit;
		}
	}
		
	public function loginCheck(){		
	 	$resp = array();
		$username = $_POST["username"];
		$password = $_POST["password"];
		$resp['submitted_data'] = $_POST;
		$login_status = 'invalid';
		if($username == 'sunil' && $password == 'sunil')
		{
			$login_status = 'success';
		}
		$resp['login_status'] = $login_status;
       if($login_status == 'success')
		{
		 $resp['redirect_url'] = site_url().'login/dashboard';
		}
		echo json_encode($resp);
	}
	
	public function dashboard(){
		$data = $this->General_Model->getHomePageSettings();
		$this->load->view('dashboard/index',$data);
	}

	public function forgotPassword(){
		$this->load->view('login/forgot_password');
	}
	
	public function logout(){
		redirect('login','refresh');
	}
	
	function logsActivity($status=''){
		$data 						= $this->General_Model->getHomePageSettings();
		$data['status']				= $status;
	    $data['logs_activity'] 		= $this->Dashboard_Model->get_admin_logs_activity();
		$this->load->view('dashboard/activity_logs',$data);
	}

	function deleteLogs(){
		$data = $this->General_Model->getHomePageSettings();
		$data['logs_activity'] 		= $this->Dashboard_Model->delete_admin_logs_activity();
		redirect('dashboard/logsActivity');
	}
	
	function changePassword($status=''){
		try{
		$data						= $this->General_Model->getHomePageSettings();
		$data['status']				= $status;
		$data['admin_profile_info'] = $this->Dashboard_Model->getAdminDetails();
		$this->load->view('login/change_password', $data);
	   } catch(Exception $ex){
		  return $ex;
	  }
	}
	
	function updatePassword(){
		
		$this->form_validation->set_rules('current_password', 'Old Password', 'required');
		$this->form_validation->set_rules('new_password_text', 'New Password', 'required');
		$this->form_validation->set_rules('confirm_password', 'Re-Enter Password', 'required');	
		
		$status = 0;
		
		if($this->form_validation->run()==FALSE){
			redirect('dashboard/changePassword','refresh');
		}else{
			//$old_password 			= $this->input->post('current_password');
			$old_password = $password = "AES_ENCRYPT(".$this->input->post('current_password').",'".SECURITY_KEY."')";
			 $old_password_status 	= $this->Security_Model->checkAdminPassword($old_password);
		
			if(trim($this->input->post('new_password_text')) == $this->input->post('confirm_password')){
				if($old_password_status['status'] == 1 ){
					/*$new_password = trim($this->input->post('new_password_text'));*/
					$new_password = $password = "AES_ENCRYPT(".$this->input->post('new_password_text').",'".SECURITY_KEY."')";
					if ($this->Security_Model->updateAdminPassword($new_password)){ $status=1; }else{ $status=0; }
				}else{
				  $status	= 3;
				}
			}else{
				$status	= 4;
			}
			redirect('dashboard/changePassword/'.$status,'refresh');
		}
	}

	function settings($status = '',$link = 'ip'){
		$data 						= $this->General_Model->getHomePageSettings();
		$data['status']				= $status;
		$data['link']				= $link;
		$data['admin_profile_info'] = $this->Dashboard_Model->getAdminDetails();
		$data['white_list_ip'] 		= $this->Dashboard_Model->get_white_list_ip_details();
		$this->load->view('dashboard/settings',$data);
	}
	
	function add_white_list(){
		if(count($_POST) > 0){
			try{ 
			$this->General_Model->begin_transaction();
			$this->Dashboard_Model->add_white_list($_POST);
			$this->General_Model->commit_transaction();
			redirect('dashboard/settings','refresh');
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction();
			return $e;
		}
		}else{
			$this->load->view('dashboard/add_white_list_ip');
		}
	}
	
	function updateIpStatus($white_list_ip_id1, $status1) {
		$white_list_ip_id = json_decode(base64_decode($white_list_ip_id1));
		 $status = json_decode(base64_decode($status1)); 
		if($white_list_ip_id !='')
			$this->Dashboard_Model->updateIpStatus($white_list_ip_id, $status);
		redirect('dashboard/settings','refresh');
	}
	
	function active_white_list_ip($white_list_ip_id){
		if($white_list_ip_id!='')
			$this->Dashboard_Model->active_white_list_ip($white_list_ip_id);
		redirect('dashboard/settings','refresh');
	}
	
	function inactive_white_list_ip($white_list_ip_id){
		if($white_list_ip_id!='')
			$this->Dashboard_Model->inactive_white_list_ip($white_list_ip_id);
		redirect('dashboard/settings','refresh');
	}
	
	function deleteWhitelistIp($white_list_ip_id1){
		$white_list_ip_id = json_decode(base64_decode($white_list_ip_id1));
		if($white_list_ip_id!='')
			$this->Dashboard_Model->delete_white_list_ip($white_list_ip_id);
		redirect('dashboard/settings','refresh');
	}

	function editWhiteListIp($white_list_ip_id1){
		$white_list_ip_id = json_decode(base64_decode($white_list_ip_id1));
		$data 						= $this->General_Model->getHomePageSettings();
		$data['white_list_ip'] 		= $this->Dashboard_Model->get_white_list_ip_details($white_list_ip_id);
		$this->load->view('dashboard/edit_white_list_ip',$data);
	}
	
	function updateWhiteListIp($white_list_ip_id){
		if(count($_POST) > 0){
			try{
			$this->General_Model->begin_transaction();
			$this->Dashboard_Model->update_white_list_ip($_POST,$white_list_ip_id);
			$this->General_Model->commit_transaction();
			redirect('dashboard/settings','refresh');
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction();
			return $e;
		}
		}else if($product_id!=''){
			redirect('dashboard/edit_white_list_ip/'.$white_list_ip_id,'refresh');
		}else{
			redirect('dashboard/settings','refresh');
		}
	}

	function profileInfo(){
		$data 						= $this->General_Model->getHomePageSettings();
		$data['country'] 			= $this->General_Model->get_country_details();
		$data['admin_profile_info'] = $this->Dashboard_Model->getAdminDetails();
	    $this->load->view('dashboard/profile_info',$data);
	}
	
	function updateProfileInfo(){
		
		if(count($_POST) > 0) {
		      
			    $form_validator = $this->profileFormValidation();	
			    if($form_validator == FALSE) {
					$data 						= $this->General_Model->getHomePageSettings();
					$data['country'] 			= $this->General_Model->get_country_details();
					$data['admin_profile_info'] = $this->Dashboard_Model->getAdminDetails();
					$data['form_validation'] =  $form_validator;
					$this->load->view('dashboard/profile_info', $data);
			  }else {
					try {  
						$this->General_Model->begin_transaction();
						$data['admin_profile_info'] = $this->Dashboard_Model->getAdminDetails();
						$this->Dashboard_Model->updateAddressDetails($_POST, $data);
						$this->Dashboard_Model->updateProfileInfo($_POST, $data);
						$this->General_Model->commit_transaction();
					} catch(Exception $ex) {
						$this->General_Model->rollback_transaction();
						$ex->getMessage();
					}
		  }
			redirect('dashboard/profileInfo','refresh');
		}
	}
	
	 
  function profileFormValidation()
  {
	 $this->form_validation->set_rules('first_name', 'First Name', 'required', 'required|min_length['.minNameLength.']|max_lengt['.maxNameLength.']');
	 $this->form_validation->set_rules('last_name', 'Last Name', 'required', 'required|min_length['.minNameLength.']|max_lengt['.minNameLength.']');
	 $this->form_validation->set_rules('account_number', 'Account Number', 'required|min_length['.minAccountNumber.']|max_lengt['.maxAccountNumber.']');
	 $this->form_validation->set_rules('email', 'Email', 'required|min_length['.minEmailLength.']|max_lengt['.maxEmailLength.']');
	 $this->form_validation->set_rules('home_phone', 'Phone Number', 'required|min_length['.minLandlineNumberLength.']|max_lengt['.maxLandlineNumberLength.']');
	 $this->form_validation->set_rules('cell_phone', 'mobile Number', 'required|min_length['.mobileNumberLength.']|max_lengt['.mobileNumberLength.']');
	 $this->form_validation->set_rules('address', 'Address','required|min_length['.minAddressLength.']|max_lengt['.maxAddressLength.']');
	 $this->form_validation->set_rules('city_name', 'City Name', 'required|min_length['.minCityLength.']|max_lengt['.maxCityLength.']');
	 $this->form_validation->set_rules('state_name', 'State', 'required|min_length['.minCityLength.']|max_lengt['.minCityLength.']');
	 $this->form_validation->set_rules('zip_code', 'Zip Code','required|min_length['.minZipCode.']|max_lengt['.maxZipCode.']');
	 return $this->form_validation->run();
  }

  // Function to change language of the website
	public function change_language(){
		$language = $this->input->post('language');
		$_SESSION['India95']['language'] = $language;
        $response = array(
        	'status' => 1
        );
        echo json_encode($response);
	}
  
}
?>
