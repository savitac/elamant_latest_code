<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Promo extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Promo_Model');
		$this->load->model('Crud_Model');
		$this->load->model('Product_Model');
		$this->load->model('Email_Model');
		$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
	function promoCodeList(){
		$promo 						= $this->General_Model->getHomePageSettings();
		$promo['promo_code_list'] 	= $this->Promo_Model->getPromoCodeList();
		$this->load->view('promo/promo_list',$promo);
	}
	
	function addPromoCode(){
		
		$api = $this->General_Model->getHomePageSettings();
		$api['product'] =  $this->Product_Model->getProductList();
			
		if(count($_POST) > 0){
			$form_validator = $this->profileFormValidation();
		    if($form_validator == false){
			$this->load->view('promo/add_promo',$api);
			}else{
				
			$promo_code = $_POST['promo_code'];
			$Query="select * from  promo_code_details  where promo_code ='".$promo_code."' ";
			$query = $this->Crud_Model->dataQuery($Query);
		if ($query->num_rows() > 0)
		{
			$data['status'] = '<div class="alert alert-block alert-danger">
							   <a href="#" data-dismiss="alert" class="close">×</a>
							   <h4 class="alert-heading">Already Promo Code Registered!</h4>
							   kindly Use Another One !!!
							   </div>';
			$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$res = "";
			for ($i = 0; $i < 10; $i++) {
				$res .= $chars[mt_rand(0, strlen($chars)-1)];
			}
			$data['promo_code'] = $res;
			$data['product'] =  $this->Product_Model->getProductList();
			$this->load->view('promo/add_promo',$data);
		
		}else{
			$this->Promo_Model->addPromoCodeDetails($_POST);
			redirect('promo/promoCodeList','refresh');
		}
		}
		} else{
			
			$this->load->view('promo/add_promo',$api);
			
		}
	}
    
    function updateStatus($promo_id1, $status1){
		$promo_id 	= json_decode(base64_decode($promo_id1));
		 $status 	= json_decode(base64_decode($status1));
		
		if($promo_id != ""){
			$this->Promo_Model->updateStatus($promo_id, $status);
		}
	   redirect('promo/promoCodeList','refresh');
	}
	
	function deletePromo($promo_id1){
		$promo_id 	= json_decode(base64_decode($promo_id1));
		if($promo_id != ''){
			$this->Promo_Model->delete_promo($promo_id);
		}
		redirect('promo/promoCodeList','refresh');
	}
	
	function editPromo($promo_id1)
	{
		$promo_id 	= json_decode(base64_decode($promo_id1));
		if($promo_id != ''){
			$promo 		= $this->General_Model->getHomePageSettings();
			$promo['promo'] = $this->Promo_Model->getPromoCodeList($promo_id);
			$this->load->view('promo/edit_promo',$promo);
		}else{
			redirect('promo/promoCodeList','refresh');
		}
	}

	function updatePromo($promo_id1){
		$promo_id 	= json_decode(base64_decode($promo_id1));
		$promo['promo_id'] 	= json_decode(base64_decode($promo_id1));
		if($promo_id != ''){
			if(count($_POST) > 0){
                 $this->Promo_Model->update_promo($_POST,$promo_id);
				redirect('promo/promoCodeList','refresh');
		}else{
			redirect('promo/promoCodeList','refresh');
		}
	}		
	}

	function sendEmail($promo_id1)
	{
	 $promo_id 	= json_decode(base64_decode($promo_id1));
		if($promo_id != ''){
		$user = $this->General_Model->getHomePageSettings();
		$promo['promo'] = $this->Promo_Model->getPromoCodeList($promo_id);
		$this->load->view('promo/send_mail',$promo);
		}else{
		redirect('promo/promo_code_list');
		}
		
	}

	function send_mail_user_new($promo_id1)
	{ 
		// $promo_id 	= json_decode(base64_decode($promo_id1));  
		 $user = $this->General_Model->getHomePageSettings();
		$email  = '';
		
		if(isset($_POST['b2b']))	
		{
			$b2b = $this->Promo_Model->get_agent_list();
			
			for($i=0;$i<count($b2b);$i++)
			{
				$ee[] = $b2b[$i]->user_email;
			}
		}
		
		
		if(isset($_POST['newsletter'])){
			$subs = $this->Promo_Model->get_all_subscribers();
			foreach($subs as $sb){
				$ee[] = $sb->email_id;
			}
		}
		$email = implode(',', $ee);

			$data = $this->Promo_Model->getPromoCodeList($promo_id1);
			
			$data['discount'] = $data[0]->discount;
			$data['promo_code'] =  $data[0]->promo_code;
	        $exp_date = date('M j,Y', strtotime($data[0]->exp_date));
			$data['emailid'] = $email;
			$data['subject'] = $_POST['subject']; 
			$data['description'] = $_POST['description'];
			$data['email_template'] = $this->Email_Model->get_email_template('promo_code')->row();
			$this->Email_Model->send_promocode_mail($data);
		redirect('promo/promoCodeList','refresh');
		
	}
	
	function profileFormValidation()
  {
	  $this->form_validation->set_rules('promo_code', 'Promo Code', 'required', 'required|min_length['.minNameLength.']|max_lengt['.maxNameLength.']');
	 $this->form_validation->set_rules('promo_type', 'Promo Type', 'required', 'required');
	 $this->form_validation->set_rules('product', 'Product', 'required');
	 $this->form_validation->set_rules('discount', 'discount','required');
	// $this->form_validation->set_rules('exp_date', 'Expiry date', 'required');
	 return $this->form_validation->run();
  }
}
