<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Noticeboard extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Noticeboard_Model');
		$this->load->model('Agents_Model');
		$this->load->library('form_validation');
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function noticeboardList(){
		$notice 					= $this->General_Model->getHomePageSettings();
		$notice['noticeboard_list'] 	= $this->Noticeboard_Model->getNoticeboardList();
		$this->load->view('noticeboard/noticeboard_list',$notice);
	}
	
	function addNoticeboard(){ 
		$notice = $this->General_Model->getHomePageSettings();
		$notice['agents'] = $this->Noticeboard_Model->getAgentList();
		if(count($_POST) > 0){
			$form_validator = $this->noticeFormValidation('add');	
			if($form_validator == FALSE  ) {
				$this->load->view('noticeboard/add_noticeboard',$notice);	
			    }else{
			    	
			$this->Noticeboard_Model->addNoticeboardDetails($_POST);
			redirect('noticeboard/noticeboardList','refresh');
		}
		} else{
			
			
			$this->load->view('noticeboard/add_noticeboard',$notice);
		}
	}
	
	function activeNoticeboard($noticeboard_id){
		$noticeboard_id 	= json_decode(base64_decode($noticeboard_id));
		$this->Noticeboard_Model->activeNoticeboard($noticeboard_id);
		redirect('noticeboard/noticeboardList','refresh');
	}
	
	function inactiveNoticeboard($noticeboard_id){
		$noticeboard_id 	= json_decode(base64_decode($noticeboard_id));
		$this->Noticeboard_Model->inactiveNoticeboard($noticeboard_id);
		redirect('noticeboard/noticeboardList','refresh');
	}
	
	function deleteNoticeboard($noticeboard_id){
		$noticeboard_id 	= json_decode(base64_decode($noticeboard_id));
		$this->Noticeboard_Model->deleteNoticeboard($noticeboard_id);
		redirect('noticeboard/noticeboardList','refresh');
	}

	function noticeFormValidation($type)
  {
  	if($type == 'add') {
	 
	 $this->form_validation->set_rules('noticemessage', 'Notice Board', 'required|xss_clean||This Field Is Required');
	 }
	 return $this->form_validation->run();
  }
}
