<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
error_reporting(0);
class Domain extends CI_Controller {	
   
    public function __construct(){
		parent::__construct();
		$this->load->model('General_Model');		
		$this->load->model('Domain_Model');
	    $this->checkAdminLogin();
	    $this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
	 	
	function domain_list(){
		$domain = $this->General_Model->gethomePageSettings();
		$domain['domain_list'] = $this->Domain_Model->get_domain_list();
		//print_r($domain['domain_list']);exit;
		$this->load->view('domain/domain_list',$domain);
	}
	
	function add_domain(){
		$domain = $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
			try{
			$this->General_Model->begin_transaction();
			$domain_logo_name = $this->General_Model->upload_image($_FILES, 'domain');
		        $this->Domain_Model->add_domain($_POST,$domain_logo_name);
                        $subdomain = str_replace("www.","",$_POST['domain_name']);
                        $subdomain  = str_replace(".com","",$_POST['domain_name']);
                        $buildRequest = "/frontend/paper_lantern/addon/doadddomain.html?domain=".$_POST['domain_name']."&subdomain=".$subdomain."&ftpuser=".$_POST['user_name']."&dir=public_html/".$_POST['domain_name']."&pass=".$_POST['password']."&pass2=".$_POST['password']."";
	
		$openSocket = fsockopen('localhost',2082);
		if(!$openSocket) {
			return "Socket error";
			exit();
		}
		
	$authString = "dalxiistravels:cN5BB(@d-vJQ";
    $authPass = base64_encode($authString);
    $buildHeaders  = "GET " . $buildRequest ."\r\n";
    $buildHeaders .= "HTTP/1.0\r\n";
    $buildHeaders .= "Host:localhost\r\n";
    $buildHeaders .= "Authorization: Basic " . $authPass . "\r\n";
    $buildHeaders .= "\r\n";
   
 
    fputs($openSocket, $buildHeaders);
    while(!feof($openSocket)) {
    fgets($openSocket,128);
    }
  
    fclose($openSocket); 
    	
    	mkdir($_SERVER['DOCUMENT_ROOT'].'/'.$_POST['domain_name']);

        $s  = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/b2b2b/my-index/index.php');
  		if(copy($_SERVER['DOCUMENT_ROOT'].'/b2b2b//my-index/index.php', $_SERVER['DOCUMENT_ROOT'].'/'.$_POST['domain_name'].'/index.php'))
		{
			 //echo "copied";
		}else {
			echo "Error While Creating Domain !";
		}
		
		if(copy($_SERVER['DOCUMENT_ROOT'].'/b2b2b/my-index/.htaccess', $_SERVER['DOCUMENT_ROOT'].'/'.$_POST['domain_name'].'/.htaccess'))
		{
			 //echo "copied";
		}else {
			echo "Error While Creating Domain !";
		} 
	 	
	$this->General_Model->commit_transaction();
	redirect('domain/domain_list','refresh');
			
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction;
			return $e;
		}
	
		}else{
			$domain['agents'] = $this->Domain_Model->get_B2B_users();
			$this->load->view('domain/add_domain',$domain);
		}
	}
	
	function remove_domain($domain_id){
	  $domain_details = $this->Domain_Model->get_domain_list($domain_id);
	  if(count($domain_details) > 0 && $domain_details != ''){
		  $domain_name = $domain_details[0]['domain_name'];
		  $user_name = $domain_details[0]['domain_user_name'];
		  $subdomain = $domain_details[0]['domain_user_name'].'_travellights.net';
		  $fullsubdomain = $domain_details[0]['domain_user_name'].'.travellights.net';
		  $buildRequest = "/frontend/paper_lantern/addon/dodeldomain.html?domain=".$domain_name."&subdomain=".$subdomain."&user=".$user_name."&fullsubdomain=".$fullsubdomain;
		  $openSocket = fsockopen('localhost',2082);
			if(!$openSocket) {
				return "Socket error";
				exit();
			}
			$authString = "travvhsw:Khaled!@#";
			$authPass = base64_encode($authString);
			$buildHeaders  = "GET " . $buildRequest ."\r\n";
			$buildHeaders .= "HTTP/1.0\r\n";
			$buildHeaders .= "Host:localhost\r\n";
			$buildHeaders .= "Authorization: Basic " . $authPass . "\r\n";
			$buildHeaders .= "\r\n";
			fputs($openSocket, $buildHeaders);
				while(!feof($openSocket)) {
				fgets($openSocket,128);
				}
            fclose($openSocket);
           $this->Domain_Model->delete_domain($domain_id);
           redirect('domain/domain_list','refresh');
		 }else{
			 redirect('domain/domain_list','refresh');
		 }
		
	}
	
	function get_B2B_users(){

		  $options = '';			
          $options          .= '<option value=""></option>';
		   $result 	         = $this->Domain_Model->get_B2B_users();
			if($result!=''){
			foreach($result as $row){ 
				$options .= '<option value="'.$row->user_details_id.'">'.str_replace("-"," ",$row->user_name).'</option>';				
			}}		
		echo json_encode(array(
            'options' 		=> $options,
        ));
	}

	
	function active_domain($domain_id1){
		$domain_id 	= json_decode(base64_decode($domain_id1));
		if($domain_id != ''){
			$this->Domain_Model->active_domain($domain_id);
		}
		redirect('domain/domain_list','refresh');
	}
	
	function inactive_domain($domain_id1){
		$domain_id 	= json_decode(base64_decode($domain_id1));
		if($domain_id != ''){
			$this->Domain_Model->inactive_domain($domain_id);
		}
		redirect('domain/domain_list','refresh');
	}
	
	function delete_domain($domain_id1){
		$domain_id 	= json_decode(base64_decode($domain_id1));
		if($domain_id != ''){
			$this->Domain_Model->delete_domain($domain_id);
		}
		redirect('domain/domain_list','refresh');
	}
	
	function edit_domain($domain_id1)
	{
		 $domain_id 	= json_decode(base64_decode($domain_id1));
		
		if($domain_id != ''){
			$domain 			= $this->General_Model->getHomePageSettings();
			$domain['domain'] 	= $this->Domain_Model->get_domain_list($domain_id);
			$domain['agents'] = $this->Domain_Model->get_B2B_users();
			$this->load->view('domain/edit_domain',$domain);
		}else{
			redirect('domain/domain_list','refresh');
		}
	}
	
	function update_domain($domain_id1)
	{
	/*	print_r($_POST);
		echo "<pre>";
		print_r($_FILES);
		die('here');*/
		$domain_id 	= $domain_id1;
		//echo $domain_id;die();
		if($domain_id != ''){
			if(count($_POST) > 0){
				if(isset($_REQUEST['old_image']))
				{
					$image_info_name = $this->General_Model->upload_image($_FILES, 'domain', $_REQUEST['old_image']);
				}
				else{
					$image_info_name=$_POST['domain_logo'];
				}
			/*$domain_logo_name = $this->General_Model->upload_image($_FILES, 'domain');*/
			$this->Domain_Model->update_domain($_POST,$domain_id, $image_info_name);
			redirect('domain/domain_list','refresh');
		
			}else if($domain_id!=''){
				redirect('domain/edit_domain/'.$domain_id,'refresh');
			}else{
				redirect('domain/domain_list','refresh');
			}
		}else{
			redirect('domain/domain_list','refresh');
		}
	}
	
	
	function domain_form_data($type){
		        $this->form_validation->set_rules('domain_name', 'Domain Name');
		         $this->form_validation->set_rules('domain_url', 'Domain URL');
		         if($type == 'add'){
    	          $this->form_validation->set_rules('domain_url', 'Domain Url', 'required|xss_clean||is_unique[domain_details.domain_url]|max_length[150]');
			  }
                   return $this->form_validation->run();
	}
}
?>
