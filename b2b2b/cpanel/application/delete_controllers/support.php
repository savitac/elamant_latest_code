<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
error_reporting(0); 
ob_start();
class Support extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Support_Model');
		$this->load->model('Domain_Model');
		$this->load->model('Email_Model');
		$this->load->model('Country_Model');
		$this->load->model('Usertype_Model');
		
		$this->load->library('form_validation');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	 $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	  function download_file($file)
	{
		$this->load->helper('download');
			$name=	base64_decode($file);
		$data = file_get_contents($name); // Read the file's contents
		$a = explode('support_ticket', $name);
		$name1 = substr($a[1],2);

force_download($name1, $data); 


	
	
	}
	function view_ticket($id)
	{	
		$data 		= $this->General_Model->getHomePageSettings();
		if(!empty($id)) {		
		$data['status']='';
		$data['support'] = $this->Support_Model->get_support_list_id($id);
		$data['ticketrow'] = $this->Support_Model->get_support_list_id_row($id);
		$data['id']=$id;
			//echo "<pre/>";print_r($data);die;
		//if(isset($this->domain_id) && !empty($this->domain_id))
		
		$this->load->view('support/edit_support',$data);
	}else{
		redirect('support/support_view');
	}
	}
	
	function reply_ticket($id)
	{
		
		
		$this->form_validation->set_rules('textcounter', 'textcounter', 'required');
		
		if($this->form_validation->run()==FALSE){
	  		redirect('support/view_ticket/'.$id,'refresh');
		}else{
			
			
			$domain = $_POST['domain'];
			$usertype = $_POST['user_type'];
			$user = $_POST['user_id'];
			$sub = $_POST['subject'];
			$message = $_POST['textcounter'];
			$support_ticket_id = $_POST['support_ticket_id'];
			
					
				//	  echo 'asdas';exit;
		
				$config['upload_path'] = './upload_files/support_ticket/'.$domain;
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);

	if($_FILES['file_name']['name']!='')
	{
				if ( ! $this->upload->do_upload('file_name'))
				{
					$error = array('error' => $this->upload->display_errors());
					$data['status'] ='<div class="alert alert-block alert-danger">
										  <a href="#" data-dismiss="alert" class="close">×</a>
										  <h4 class="alert-heading">Attachment File Failed!</h4>
										  '.$error['error'].'
										</div>';
			$data['ticket'] = $this->Support_Model->get_support_list_id($id); 
		   $data['ticketrow'] = $this->Support_Model->get_support_list_id_row($id); 
		    $data['id']=$id;
			
			$this->load->view('support/view_ticket',$data);
				}
					$cc = $this->upload->data('file');
					$image_path = WEB_DIR.'upload_files/support_ticket/'.$cc['file_name'];
	}
	else
	{
		
					$image_path = '';
	}
				
				
				 
					$this->Support_Model->add_new_support_ticket_updates($support_ticket_id,$domain,$usertype,$user,$sub,$message,$image_path);
					
					
					
		redirect('support/reply_ticket/'.$id,'refresh');
		
					
				
			
	
		}
	
		
	}
	function update_support($support_id1,$user_email){
		$support_id 	= json_decode(base64_decode($support_id1)); 
		$user_email 	= json_decode(base64_decode($user_email)); 
		//print_r($_POST); exit();
		if($support_id != ''){
			if(count($_POST) > 0){
				$support_logo_name  = $_REQUEST['attachment_old'];
				$config['upload_path'] = './uploads/support/'.$domain;
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);
				if(!empty($_FILES['attachment']['name']))
				{	
					if ( ! $this->upload->do_upload('attachment'))
				{
					$error = array('error' => $this->upload->display_errors());
					$data['status'] ='<div class="alert alert-block alert-danger">
										  <a href="#" data-dismiss="alert" class="close">×</a>
										  <h4 class="alert-heading">Attachment File Failed!</h4>
										  '.$error['error'].'
										</div>';
			
			//$this->load->view('support/view_ticket',$data);

				} else {
		
					$image_path = '';
				}
					$cc = $this->upload->data('file');
					$image_path = base_url().'uploads/support/'.$cc['file_name'];
				} else {
					$image_path = '';
				}
				$this->Support_Model->add_new_support_ticket_updates($support_id,$_POST['message'] ,$image_path);
				$this->Email_Model->sendAgentSupportAlertMail($user_email,$support_id);
				redirect('support/support_view','refresh');
			}else if($support_id!=''){
				redirect('support/support_view','refresh');
			}else{
				redirect('support/support_view','refresh');
			}
		}else{
			redirect('support/support_view','refresh');
		}		
	}
	function edit_support($support_id1)
	{
		
		//$support_id 	= json_decode(base64_decode($support_id1));echo "<pre/>";print_r($support_id1);die;
		if($support_id1 != ''){ 
			$support 		= $this->General_Model->getHomePageSettings();
			$support['domain'] = $this->Domain_Model->get_domain_list(); 
			$support['user_type'] = $this->Usertype_Model->get_user_type_list();
			$support['subject'] = $this->Support_Model->get_subject_list();
			$support['support'] = $this->Support_Model->get_support_list_id($support_id1);//echo "string";die;
			//echo "<pre/>";print_r($support['support']);die;
			$this->load->view('support/edit_support',$support);
		}else{
			redirect('support/support_view','refresh');
		}
	}
	function get_user_mailid($user_type_id) { 
    	$user_mail =  $this->Agents_Model->getAgentList($user_type_id)->result();
    	$option = "";
    	for($i=0; $i< count($user_mail); $i++ ){
    		$option .= '<option value='.$user_mail[$i]->user_details_id.' data-iconurl="" role="option">'.$user_mail[$i]->user_email.'</option>';
    	}
        echo $option;
        exit;
     }
	function close_ticket($sid)
	{
		 $this->Support_Model->close_ticket($sid); 
		redirect('support/support_view','refresh');	
	}
	
	public function support_view()
		{	


			$data 		= $this->General_Model->getHomePageSettings();
			if(isset($this->domain_id) && !empty($this->domain_id))	
			{ //echo "cbhdfnbh";die;
				$data['support_view'] = $this->Support_Model->get_support_list($this->domain_id); 
				$data['support_pending'] = $this->Support_Model->get_support_list_pending($this->domain_id); 
				$data['support_sent'] = $this->Support_Model->get_support_list_sent($this->domain_id); 
				$data['support_close'] = $this->Support_Model->get_support_list_close($this->domain_id); 
			}
			else
			{
				$data['support_view'] = $this->Support_Model->get_support_list(); 
				$data['support_pending'] = $this->Support_Model->get_support_list_pending(); 
				$data['support_sent'] = $this->Support_Model->get_support_list_sent(); 
				$data['support_close'] = $this->Support_Model->get_support_list_close(); 
			}	
			//echo "<pre/>";print_r($data);die;
			$this->load->view('support/ticket_view',$data);
			//$this->load->view('support/view',$data);
		}
		
		public function add_subject()
		{
			$support = $this->General_Model->getHomePageSettings();
			$data['support_ticket_subject'] = $this->Support_Model->get_support_subject_list(); 
			//$this->load->view('support/support_ticket_subject',$data);
			$this->load->view('support/add_subject',$support);
		}
		function subject_list(){
			$support 				= $this->General_Model->getHomePageSettings();
			$support['subject_list'] 	= $this->Support_Model->get_support_subject_list(); 
			$this->load->view('support/subject_list',$support);
		}
		public function add_ticket(){
				$data['country'] = $this->Country_Model->getCountryList(); 
				
			$data['domain_list'] = $this->Domain_Model->get_domain_list(); 
			$data['usertypes'] = $this->Usertype_Model->get_user_type_list(); 
			$data['support_ticket_subject'] = $this->Support_Model->get_support_subject_list(); 
			$this->load->view('support/add_ticket',$data);
		}
		function get_user_data_ajax($domain,$uertype){
				$get_data = $this->Support_Model->get_user_data_ajax_list($domain,$uertype); 
				if($get_data!=''){
					echo '<select class="form-control" data-rule-required="true" id="user" name="user">';
                          
					for($i=0;$i<count($get_data);$i++){
						echo '<option value="'.$get_data[$i]->id.'">'.$get_data[$i]->email_id.'</option>';
					}
					
					echo '</select>';
				}else{
					echo "<input class='form-control' data-rule-required='true' id='user' name='user' placeholder='User' type='text' readonly>";
				}
		}
		function get_other_sub_ajax($id){
			if($id==1){
				echo "<input class='form-control' data-rule-required='true' id='user' name='user' placeholder='User' type='text'>
				<span class='input-group-addon'><i class='icon-external-link'></i></span>
                            <a style='margin-left:10px;'' ><button class='btn btn-primary' type='button' onClick='others(2)'>
                                <i class='icon-reply'></i> Back</button></a>";
										
			}if($id==2){
					$support_ticket_subject = $this->Support_Model->get_support_subject_list(); 
					echo "<select class='form-control' data-rule-required='true' id='sub' name='sub'>";
                    for($i=0;$i<count($support_ticket_subject);$i++){
						echo '<option value="'.$support_ticket_subject[$i]->support_ticket_subject_value.'">'.$support_ticket_subject[$i]->support_ticket_subject_value.'</option>';
					}
					echo "</select><span class='input-group-addon'><i class='icon-th-list'></i></span>
                            <a style='margin-left:10px;' ><button class='btn btn-primary' type='button' onClick='others(1)'>
                                <i class='icon-external-link'></i> Other</button></a>";
			}
		}
		function add_subject_new(){
			
			$this->Support_Model->addSubject($_POST);
			
			redirect('support/subject_list','refresh');
		}
		public function inactive_subject($subject_id)
		{
			$where['support_ticket_subject_id'] = json_decode(base64_decode($subject_id));
			$data['status'] = 'INACTIVE';
			$this->Support_Model->update_subject($data,$where);
			redirect('support/subject_list','refresh');
		}
		public function active_subject($subject_id)
		{
			$where['support_ticket_subject_id'] = json_decode(base64_decode($subject_id));
			$data['status'] = 'ACTIVE';
			$this->Support_Model->update_subject($data,$where);
			redirect('support/subject_list','refresh');
		}
		public function update_subject($subject_id)
		{
			//echo "<pre/>";print_r($_POST);die;
			$where['support_ticket_subject_id'] = json_decode(base64_decode($subject_id));
			$data['support_ticket_subject_value'] = $_POST['subject'];
			if(isset($_POST['status'])){
				$data['status'] = $_POST['status'];//print_r($data['status']);die;
			}
			$this->Support_Model->update_subject($data,$where);
			redirect('support/subject_list','refresh');
		}
		function edit_subject($subject_id){ 
		$subject_id 	= json_decode(base64_decode($subject_id));//print_r($subject_id);die;
		if(($subject_id != '') &&($subject_id > 0)) {
		$support = $this->General_Model->getHomePageSettings();//print_r($subject_id);die;
		$support['subject_list'] 	= $this->Support_Model->get_subject_list($subject_id);
		$this->load->view('support/edit_subject',$support);
		}else {
			redirect('support/subject_list',"refresh");
		}
		
	}
		function add_new_ticket(){
			$this->form_validation->set_rules('user', 'User', 'required');
			$this->form_validation->set_rules('message', 'Message', 'required');
		
			if($this->form_validation->run()==FALSE){
	  			redirect('support/add_ticket','refresh');
			}else{
				$domain = $_POST['domain'];
				$usertype = $_POST['usertype'];
				$user = $_POST['user'];
				$sub = $_POST['sub'];
				$message = $_POST['message'];
			
				$config['upload_path'] = './upload_files/support_ticket/'.$domain;
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);

				if($_FILES['file_name']['name']!=''){
					
					if ( ! $this->upload->do_upload('file_name')){
						$error = array('error' => $this->upload->display_errors());
						$data['status'] = "<div class='alert alert-danger alert-dismissable'>
								             <a class='close' data-dismiss='alert' href='#'>&times;</a>
								                File Attachment Failed!
								              </div>";
						$data['country'] = $this->Domain_Model->get_country_list(); 
					
						$data['domain_list'] = $this->Domain_Model->get_domain_list(); 
						$data['usertypes'] = $this->Domain_Model->get_usertypes_list(); 
						$data['support_ticket_subject'] = $this->Support_Model->get_support_subject_list(); 
						$this->load->view('support/add_ticket',$data);
					}
						$cc = $this->upload->data('file');
						$image_path = 'upload_files/support_ticket/'.$domain.'/'.$cc['file_name'];
				}else{
		
					$image_path = '';
				}
				
				
				 
					$this->Support_Model->add_new_support_ticket($domain,$usertype,$user,$sub,$message,$image_path);
					
					$data['status'] = "<div class='alert alert-success alert-dismissable'>
						             <a class='close' data-dismiss='alert' href='#'>&times;</a>
						                Ticket has been created Successfully!
						              </div>";
					$data['country'] = $this->Domain_Model->get_country_list(); 
				
					$data['domain_list'] = $this->Domain_Model->get_domain_list(); 
					$data['usertypes'] = $this->Domain_Model->get_usertypes_list(); 
					$data['support_ticket_subject'] = $this->Support_Model->get_support_subject_list(); 
					$this->load->view('support/add_ticket',$data);
			}
		}
		function delete_ticket($id)
		{
			 $wheres = "support_ticket_id = '$id'";
				$this->db->delete('support_ticket', $wheres);
			redirect('support/support_view','refresh');
		}
		function delete_subject($id)
		{
			$id = json_decode(base64_decode($id));
			  $wheres = "support_ticket_subject_id = $id";
				$this->db->delete('support_ticket_subject', $wheres);
			redirect('support/subject_list','refresh');
		}
		
		function checkuserdomain()
		{
			$domain_id = 1;
			if(!empty($domain_id))
			{
				return $domain_id;
			}
			else
			{
				return false;
			}	
		}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
