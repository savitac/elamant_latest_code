<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
// error_reporting(0)
ob_start();
class Payment extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Payment_Model');
		$this->load->library('form_validation');
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	 $this->checkAdminLogin();
		
	}

	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function payment_api_list(){
		$payment 					= $this->General_Model->getHomePageSettings();
		$payment['payment_api_list'] = $this->Payment_Model->get_payment_api_list();
		$this->load->view('payment/payment_api_list',$payment);
	}
	
	function add_payment_api(){
		if(count($_POST) > 0){
			$this->Payment_Model->add_payment_api_details($_POST);
			redirect('payment/payment_api_list','refresh');
		}else{
			$payment 					= $this->General_Model->getHomePageSettings();
			$this->load->view('payment/add_payment_api',$payment);
		}
	}
	
	function active_payment_api($payment_api_id){
		$payment_api_id = json_decode(base64_decode($payment_api_id));
		if($payment_api_id){
			$this->Payment_Model->active_payment_api($payment_api_id);
		}
		redirect('payment/payment_api_list','refresh');
	}
	
	function inactive_payment_api($payment_api_id){
		$payment_api_id = json_decode(base64_decode($payment_api_id));
		if($payment_api_id){
			$this->Payment_Model->inactive_payment_api($payment_api_id);
		}
		redirect('payment/payment_api_list','refresh');
	}
	
	function delete_payment_api($payment_api_id){
		$payment_api_id = json_decode(base64_decode($payment_api_id));
		if($payment_api_id){
			$this->Payment_Model->delete_payment_api($payment_api_id);
		}
		redirect('payment/payment_api_list','refresh');
	}
	
	function edit_payment_api($payment_api_id){
		$payment_api_id = json_decode(base64_decode($payment_api_id));
		if($payment_api_id){
			$payment 					= $this->General_Model->getHomePageSettings();
			$payment['api'] = $this->Payment_Model->get_payment_api_list($payment_api_id);
			$this->load->view('payment/edit_payment_api',$payment);
		} else {
			redirect('payment/payment_api_list','refresh');
		}
	}

	function update_payment_api($payment_api_id){
		if(count($_POST) > 0){
			$payment_api_id = json_decode(base64_decode($payment_api_id));
			if($payment_api_id != ''){
				
				$this->Payment_Model->update_payment_api($_POST,$payment_api_id);
			}
			redirect('payment/payment_api_list','refresh');
		}else if($payment_api_id!=''){
			redirect('payment/payment_api_list','refresh');
		}else{
			redirect('payment/payment_api_list','refresh');
		}
	}
}
