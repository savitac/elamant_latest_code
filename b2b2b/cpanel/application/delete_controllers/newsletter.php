<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Newsletter extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('Security_Model');        
        $this->load->model('General_Model');
        $this->load->model('Newsletter_Model');
        $this->load->model('Roles_Model');
        $this->load->model('Users_Model');
        $this->load->model('Agents_Model');
        $this->load->model('Admin_Model');
        $this->load->model('Subscriber_Model');
        $this->load->model('Email_Model');
        $this->load->library('form_validation');  
        if(!isset($_SESSION['ses_id'])){
          $sec_res      = session_id();
            $_SESSION['ses_id'] = $sec_res;
        }
       
      $this->lang->load('english','Dynamic_Languages');
   
    $this->TravelLights = $this->lang->line('TravelLights');
       $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }

   
    function index() {
        $data = $this->General_Model->getHomePageSettings();
        $data['newsletter_template'] = $this->Newsletter_Model->getNewsletterTemplateDetails();
        $this->load->view('newsletter/newsletter_template_list', $data);
    }

    function addNewsletterTemplate() {
        $data = $this->General_Model->getHomePageSettings();
        if (count($_POST) > 0) {
            $this->form_validation->set_rules('template_name', 'NewsLetter Name', 'required|min_length[3]|max_length[30]');
            /*$this->form_validation->set_rules('email_from', 'NewsLetter From', 'required|valid_email');*/
            $this->form_validation->set_rules('email_from_name', 'NewsLetter From Name', 'required|min_length[3]|max_length[30]');
            $this->form_validation->set_rules('subject', 'NewsLetter Subject', 'required|min_length[3]|max_length[30]');
            $this->form_validation->set_rules('message', 'NewsLetter Description','required');
            if ($this->form_validation->run() == FALSE) {
                $data['status'] = $this->input->post('status');
                $data['template_name'] = $this->input->post('template_name');
                /*$data['email_from'] = $this->input->post('email_from');*/
                $data['email_from_name'] = $this->input->post('email_from_name');
                $data['subject'] = $this->input->post('subject');
                $data['message'] = $this->input->post('message');                                                 
                $this->load->view('newsletter/add_newsletter_template', $data);
            } else {
                $this->Newsletter_Model->addNewsletterTemplate($_POST);
                redirect('newsletter', 'refresh');
            }
        } else {
             $data['status'] = "ACTIVE" ;
            $this->load->view('newsletter/add_newsletter_template', $data);
        }
    }

    function activeNewsletterTemplate($template_id1 = "") {
        $template_id = json_decode(base64_decode($template_id1));
        if ($template_id != '') {
            $this->Newsletter_Model->activeNewsletterTemplate($template_id);
        }
        redirect('newsletter', 'refresh');
    }

    function inactiveNewsletterTemplate($template_id1 = "") {
        $template_id = json_decode(base64_decode($template_id1));
        if ($template_id != '') {
            $this->Newsletter_Model->inactiveNewsletterTemplate($template_id);
        }
        redirect('newsletter', 'refresh');
    }

    function deleteNewsletterTemplate($template_id1 = "") {
        $template_id = json_decode(base64_decode($template_id1));
        if ($template_id != '') {
            $this->Newsletter_Model->deleteNewsletterTemplateList($template_id);
        }
        redirect('newsletter', 'refresh');
    }
    function deleteSubscribeTemplate($template_id = "") {
        //$template_id = json_decode(base64_decode($template_id1));
        if ($template_id != '') {
            $this->Newsletter_Model->deleteSubscribeTemplate($template_id);
        }
        redirect('newsletter/subscribe_list', 'refresh');
    }

  function editNewsletterTemplate($template_id1 = "") {
    $ori_template_id = json_decode(base64_decode($template_id1));   
    if($ori_template_id == ''){
       redirect('newsletter', 'refresh');
    }    
     if(count($_POST) > 0){        
       $this->form_validation->set_rules('template_name', 'Template Name', 'required|min_length[5]|max_length[60]|callback_check_uniquerole['.$ori_template_id.']');     
       /*$this->form_validation->set_rules('email_from', 'From Address', 'required|valid_email|min_length[8]|max_length[60]');            */
       $this->form_validation->set_rules('email_from_name', 'From Name', 'required|min_length[3]|max_length[30]');
       $this->form_validation->set_rules('subject', 'Subject', 'required|min_length[5]|max_length[80]');  
       $this->form_validation->set_rules('message', 'Message', 'required');                

       if($this->form_validation->run()==FALSE){ //Validation false               
         $email = $this->General_Model->getHomePageSettings();         
         $email['email_template']=array(
                         'template_name' => $this->input->post('template_name'),
                         /*'email_from' => $this->input->post('email_from'),*/
                         'email_from_name' => $this->input->post('email_from_name'),                         
                         'subject' => $this->input->post('subject'),
                         'message' => $this->input->post('message'),
                         'status' => $this->input->post('status'),
                         'newsletter_template_id' => $ori_template_id
                       );                  
         $this->load->view('newsletter/edit_newsletter_template',$email); 
       }
       else{          
         $this->Newsletter_Model->updateNewsletterTemplate($_POST, $ori_template_id);
         redirect('newsletter', 'refresh');
       }       
     }
     else{            
         $email = $this->General_Model->getHomePageSettings();
         $email['email_template']  = $this->Newsletter_Model->getNewsletterTemplateDetails($ori_template_id);
         $this->load->view('newsletter/edit_newsletter_template',$email); 
     }    
    }

    function sendEmail($template_id1 = "") {
      $ori_template_id = json_decode(base64_decode($template_id1));   
      if($ori_template_id == ''){
       redirect('newsletter', 'refresh');
      }
      $data = $this->General_Model->getHomePageSettings();
      $data['template'] = $this->Newsletter_Model->getNewsletterTemplateDetails($ori_template_id);
      $data['role'] = $this->Roles_Model->get_roles_list('','ACTIVE');
      $this->load->view('newsletter/send_mail', $data);
    }  

    function sendMailToUser($template_id1 = "") {
      $ori_template_id = json_decode(base64_decode($template_id1));   
      if($ori_template_id == ''){
       redirect('newsletter/sendEmail/'.$template_id1,'refresh'); 
      }
      if(count($_POST) > 0){
        $option = $this->input->post('option');
        if($option == ""){          
          redirect('newsletter/sendEmail/'.$template_id1,'refresh'); 
        }
        $email_ids = array();
        for($loop = 0; $loop < sizeof($option); $loop++){
            if($option[$loop] == "b2b"){
              $b2b_user = $this->Agents_Model->getAgentList('','ACTIVE');
             
              if($b2b_user != ""){
                for ($i = 0; $i < count($b2b_user['user_info']); $i++) {
                  $email_ids[] = array('id' => $b2b_user['user_info'][$i]->user_details_id, 'email' => $b2b_user['user_info'][$i]->user_email, 'user_name' => $b2b_user['user_info'][$i]->user_name, 'type' => 'b2b');
                 
                }
              }
            }
            elseif($option[$loop] == "subscriber"){
              $subscribers = $this->Subscriber_Model->getSubscriberNewsletterList('','ACTIVE');
            if($subscribers != ""){
               for ($subs = 0; $subs < count($subscribers); $subs++) {              
                 $email_ids[] = array('id' => $subscribers[$subs]->subscriber_details_id, 'email' => $subscribers[$subs]->email_id, 'user_name' => $subscribers[$subs]->email_id, 'type' => 'subscriber');
               }
              }
            }
            
            elseif($option[$loop] == "Other"){              
              $this->form_validation->set_rules('email','Email Id','required|valid_emails');
              if($this->form_validation->run() == false){
                $data = $this->General_Model->getHomePageSettings();
                $data['template'] = $this->Newsletter_Model->getNewsletterTemplateDetails($ori_template_id);
                $data['email'] = $this->input->post('email');
                $data['role'] = $this->Roles_Model->get_roles_list('','ACTIVE');
                $data['option'] = $this->input->post('option');
                $this->load->view('newsletter/send_mail', $data);
              }
              $other_email = $this->input->post('email');              
              $explode_email = explode(",",$other_email);
              //echo $other_email."<pre>"; print_r($explode_email);
              for($inloop = 0; $inloop < count($explode_email); $inloop++){
               if(strlen($explode_email[$inloop]) > 1) 
                $email_ids[] = array('id' => $inloop, 'email' => $explode_email[$inloop], 'user_name' => $explode_email[$inloop] , 'type' => 'other');
              }              
            }
        }
        if($email_ids == ""){
            redirect('newsletter/sendEmail/'.$template_id1,'refresh'); 
        }
        $data = $this->Newsletter_Model->getNewsletterTemplateDetails($ori_template_id);
        if($data == ""){
            redirect('newsletter/sendEmail/'.$template_id1,'refresh'); 
        }
        $data['subject'] = $data[0]->subject;
        $data['description'] = $data[0]->message; 
        
        $replace['CURRENT_YEAR'] = date('Y');
		$replace['NEXT_YEAR']  = date('Y') + 1;		
		foreach ($replace as $key => $value) {
				$data['description'] = str_replace('{%%' . $key . '%%}', $value, $data['description']);
		}
			       
        foreach ($email_ids as $key => $value) {
         if($value['email'] != ""){
          $data['mailid'] = $value['email'];
         
          $this->Email_Model->sendMailToUser($data);
		  
          sleep(4);
         }
        }
        redirect('newsletter','refresh');  
      }
      else{
       redirect('newsletter/sendEmail/'.$template_id1,'refresh'); 
      }

    }

    function check_uniquerole($value,$user_id){            
        $roles = $this->Newsletter_Model->check_uniquename($value,$user_id);        
        if($roles == '1'){                      
          $this->form_validation->set_message('check_uniquerole','The %s field must contain a unique value.');  
          return false;
        }
    }
    function subscribeList(){
    	$data = $this->General_Model->getHomePageSettings();
		$data['subscribe']=$this->Newsletter_Model->subscribeList();
		
		$this->load->view('newsletter/subscribe_list', $data);
	}
	function activeSubscribeTemplate($template_id = "") {
		
        if ($template_id != '') {
            $this->Newsletter_Model->activeSubscribeTemplate($template_id);
        }
        redirect('newsletter/subscribeList', 'refresh');
    }

    function inactiveSubscribeTemplate($template_id = "") {
	 $this->Newsletter_Model->inactiveSubscribeTemplate($template_id);
      redirect('newsletter/subscribeList', 'refresh');
    }
}

?>
