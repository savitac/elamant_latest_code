<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
error_reporting(0);
class Pages extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Pages_Model');
		
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function pagesList(){
		$pages 					= $this->General_Model->getHomePageSettings();
		$pages['pages_list'] 	= $this->Pages_Model->getPagesList();
		$this->load->view('pages/pages_list',$pages);
	}
	
	function addPages(){
		$api = $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
			
			$title = $this->input->post('page_name');
			$label = $this->uniqueLabel(substr($title, 0,100));
			$page_content_body = $this->input->post('page_content_body');
			$page_content_side = $this->input->post('page_content_side');
			$status = $this->input->post('status');
			$form_validator = $this->formValidator('add');
			if($form_validator == FALSE  ) {
				$this->load->view('pages/add_pages',$api);	
			    }else{
			$labels =  array('page_name' => $title,
							'page_content_body' => $page_content_body,
							'page_content_side' => $page_content_side,
							'slug' => $label,
							'page_url' => site_url()."pages/page/".$label,
							'status' => $status,
							'creation_date'			=> (date('Y-m-d H:i:s'))
			 );
			
			$this->Pages_Model->addPagesDetails($labels);
			redirect('pages/pagesList','refresh');
			}
		}else{
			
			$this->load->view('pages/add_pages',$api);
		}
	}

	public function uniqueLabel($string) {
		//Lower case everything
		$string = strtolower($string);
		//Make alphanumeric (removes all other characters)
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "-", $string);
		return $string;
	}
	
	function activePages($pages_id){
		$pages_id 	= json_decode(base64_decode($pages_id));
		$this->Pages_Model->activePages($pages_id);
		redirect('pages/pagesList','refresh');
	}
	
	function inactivePages($pages_id){
		$pages_id 	= json_decode(base64_decode($pages_id));
		$this->Pages_Model->inactivePages($pages_id);
		redirect('pages/pagesList','refresh');
	}
	
	function deletePages($pages_id){
		$pages_id 	= json_decode(base64_decode($pages_id));
		$this->Pages_Model->deletePages($pages_id);
		redirect('pages/pagesList','refresh');
	}
	
	function editPages($pages_id)
	{   
		$pages 					= $this->General_Model->getHomePageSettings();
		$pages_id 	= json_decode(base64_decode($pages_id));
		$pages['pages_list'] 	= $this->Pages_Model->getPagesList($pages_id);
		
		$this->load->view('pages/edit_pages',$pages);
	

	}

	function updatePages($pages_id)
	{
		$pages_id1 	= json_decode(base64_decode($pages_id));
		
		if(count($_POST) > 0){ 
			$form_validator = $this->formValidator('edit');
			if($form_validator == FALSE  ) {
				
				redirect('pages/editPages/'.$pages_id,'refresh');	
			    }else{
			    $this->Pages_Model->updatePages($_POST,$pages_id1);
			    redirect('pages/pagesList','refresh');
		}
			
		}else if($pages_id1!=''){
			redirect('pages/edit_pages/'.$pages_id1,'refresh');
		}else{
			redirect('pages/pagesList','refresh');
		}
	}

	function formValidator($type){
	   $this->form_validation->set_rules('page_name', 'Page Name', 'required');
	   $this->form_validation->set_rules('page_content_side', 'Page Content Side', 'required');
	   $this->form_validation->set_rules('page_content_body', 'Page Content Body', 'required');
	   
	   if($type == 'add') {
	    $this->form_validation->set_rules('page_name', 'Page name', 'required|xss_clean||is_unique[static_page_details.page_name]|max_length[50]');
	    $this->form_validation->set_rules('page_content_side', 'Page Content Side','required');
	    $this->form_validation->set_rules('page_content_body', 'Page Content Body','required');
	    }
	    if($type == 'edit') {
	    
	    $this->form_validation->set_rules('page_content_side', 'Page Content Side','required');
	    $this->form_validation->set_rules('page_content_body', 'Page Content Body','required');
	    }
	   return $this->form_validation->run();
    }	
    
    function homepage_contents(){
		$home 					= $this->General_Model->getHomePageSettings();
		$home['contents'] 	=  $this->Pages_Model->get_homepage_contents();
		$this->load->view('pages/homelist',$home);
	}
	
	function addHomeContent(){
		if(count($_POST) > 0){
			$user_profile_name = $this->General_Model->upload_image($_FILES, 'home');
			$this->Pages_Model->add_home_content($_POST, $user_profile_name);
			redirect('pages/homepage_contents');
		}else{
			$this->load->view('pages/add_home_content');
		}
	}
	
	function active_home($pages_id){ 
		$pages_id 	= json_decode(base64_decode($pages_id));
		$this->Pages_Model->activehome($pages_id);
		redirect('pages/homepage_contents','refresh');
	}
	
	function inactive_home($pages_id){
		$pages_id 	= json_decode(base64_decode($pages_id));
		$this->Pages_Model->inactivehome($pages_id);
		redirect('pages/homepage_contents','refresh');
	}

	function updateStatus($pagesid1, $status1){
		 $pagesid 	= json_decode(base64_decode($pagesid1));
		 $status 	= json_decode(base64_decode($status1));
		
		if($currency_id != ''){
			$this->Pages_Model->updateStatus($pagesid, $status);
		}
		redirect('pages/homelist','refresh');
	}

	function editHomelist($pages_id)
	{   
		$pages 					= $this->General_Model->getHomePageSettings();
		$pages_id 	= json_decode(base64_decode($pages_id));
		$pages['pages_list'] 	= $this->Pages_Model->gethomepage($pages_id);
		
		$this->load->view('pages/edit_homelist',$pages);
	}

	

	 function UpdateHomelist($id1) { 
	  
        if (count($_POST) > 0) { 
            $id = json_decode(base64_decode($id1));
			if($id != ''){
				$image_info_name = $this->General_Model->upload_image($_FILES, 'home', $_REQUEST['old_image']);
				$this->Pages_Model->update_homelist($_POST, $id, $image_info_name);
			}
            redirect('pages/homepage_contents', 'refresh');
        } else if ($id1 != '') {
            redirect('pages/editHomelist/'.$id1, 'refresh');
        } else {
            redirect('pages/homepage_contents', 'refresh');
        }
    }
	
	function deletehome($pages_id){
		$pages_id 	= json_decode(base64_decode($pages_id));
		$this->Pages_Model->deletePages($pages_id);
		redirect('pages/pagesList','refresh');
	}
	
}
