<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); echo '<pre>'; print_r('hi'); exit();
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
error_reporting(0);
class Users extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Users_Model');		
		//$this->load->model('Users_Model');
		$this->load->model('Product_Model');
		$this->load->model('Domain_Model');		
		$this->load->model('Usertype_Model');
		$this->load->model('Currency_Model');		
		$this->load->model('Email_Model');
		$this->load->model('Promo_Model');
		$this->load->model('Api_Model');
		$this->load->model('Booking_Model');
		$this->load->library('form_validation');
		$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	 $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function users_list(){ 
		$user 				= $this->General_Model->getHomePageSettings();
		
		$user['user_list'] 	= $this->Users_Model->get_user_list($user_id);
		$user['promo'] = $this->Promo_Model->get_promo(); //print_r($user['promo'][0]->promo_code_details_id); exit();
		 //echo '<pre/>';print_r($user['user_list']);exit;
		$this->load->view('users/user_list',$user);
	}

	function add_user(){
		if(count($_POST) > 0){
			$email = $_POST['email_id'];
			$Query="select * from  user_details  where user_email ='".$email."' AND user_type_id='1' ";
			$query=$this->db->query($Query);
			if ($query->num_rows() > 0)
		{
			
			$data['status'] = '<div class="alert alert-block alert-danger">
							   <a href="#" data-dismiss="alert" class="close">×</a>
							   <h4 class="alert-heading">Already Email Id Registered!</h4>
							   kindly Use With Another Email Address !!!
							   </div>';
			$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$res = "";
			for ($i = 0; $i < 10; $i++) {
				$res .= $chars[mt_rand(0, strlen($chars)-1)];
			}
			
			$this->load->view('users/add_user',$data);
		
		}else{
			$user_profile_name = $this->General_Model->upload_image($_FILES, 'users');
			$this->Users_Model->add_users_details($_POST,$user_profile_name);
			$user['email_template'] = $this->Email_Model->get_email_template('add_user_active')->row();
			$this->Email_Model->send_add_user_registration($user,$_POST);
			//$this->Email_Model->add_users_send_mail($_POST);
			redirect('users/users_list','refresh');
		}
		}else{
			$users 				= $this->General_Model->getHomePageSettings();
			$users['domain'] 	=  $this->Domain_Model->get_domain_list();
			$users['product'] 	=  $this->Product_Model->get_product_list();
			$users['country'] 	=  $this->General_Model->get_country();
			$users['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$users['api']		=  $this->Api_Model->get_api_list();
			// echo '<pre/>';print_r($users);exit;
			$this->load->view('users/add_user',$users);
		}
	}

	function check_unique_email($email){
		if($email!=''){
			if(preg_match("/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $email)){
				$Status 	=	$this->Users_Model->get_all_user_email($email);
				if($Status  == "YES") {
					echo '<span class="nosuccess validate-has-error" for="email_id" style="display:inline-block;">This Email Id already Exists, Please Choose diffrent Email Id</span>';exit;
				}else{
					echo '<span class="success" style="color: green;">valid</span>';exit;
				}
			}else{
				echo '<span class="nosuccess">Please Enter the Valid Email Id</span>';exit;
			}
		}else{	
			 echo '<span class="nosuccess">Please Enter the Valid Email Id</span>';exit;
		}
	}
	
	function active_users($user_id1){
		$user_id 	= json_decode(base64_decode($user_id1));
		$user = $this->Users_Model->get_user_list($user_id); //echo '<pre>'; print_r($user['user_info']); exit();

		if($user_id != ''){
			$this->Users_Model->active_users($user_id);
			$user['email_template'] = $this->Email_Model->get_email_template('add_user_active')->row();
			$this->Email_Model->send_add_user_active($user);
			// $this->Email_Model->send_add_user_active($user);
		}
		redirect('users/users_list','refresh');
	}
	
	function inactive_users($user_id1){
		$user_id 	= json_decode(base64_decode($user_id1));
		$user = $this->Users_Model->get_user_list($user_id); //echo '<pre>'; print_r($user['user_info']); exit();
		if($user_id != ''){
			$this->Users_Model->inactive_users($user_id);
			//$this->Email_Model->send_add_user_inactive($user);
			$user['email_template'] = $this->Email_Model->get_email_template('user_deactivate')->row();
  	        $this->Email_Model->send_add_user_active($user);
		}
		redirect('users/users_list','refresh');
	}
	
	function delete_users($user_id1){
		$user_id 	= json_decode(base64_decode($user_id1));
		if($user_id != ''){
			$this->Users_Model->delete_users($user_id);
		}
		redirect('users/users_list','refresh');
	}
	
	function edit_users($user_id1)
	{
		$user_id 	= json_decode(base64_decode($user_id1));
		if($user_id != ''){			
			$users 				= $this->General_Model->getHomePageSettings();
			$users['domain'] 	=  $this->Domain_Model->get_domain_list();
			$users['product'] 	=  $this->Product_Model->get_product_list();
			$users['country'] 	=  $this->General_Model->get_country_details();
			$users['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$users['api']		=  $this->Api_Model->get_api_list();
			$users['users'] 	= $this->Users_Model->get_user_list($user_id);
			// echo '<pre/>';print_r($users);exit;
			$this->load->view('users/edit_user',$users);
		}else{
			redirect('users/users_list','refresh');
		}
	}
	
	function update_users($user_id1)
	{
		$user_id 	= json_decode(base64_decode($user_id1)); //print_r($user_id); exit();
		if($user_id != ''){
			if(count($_POST) > 0){
				$image_info_name = $this->General_Model->upload_image($_FILES, 'users', $_REQUEST['old_image']);
				$this->Users_Model->update_users($_POST,$user_id, $image_info_name);
				redirect('users/users_list','refresh');
			}else{
				redirect('users/users_list','refresh');
			}
		}else{
			redirect('users/users_list','refresh');
		}
	}
	
	function send_email($user_id1)
	{
		$user_id 	= json_decode(base64_decode($user_id1));
		if($user_id != ''){
			$user 				= $this->General_Model->getHomePageSettings();
			$user['user_list'] 	= $this->Users_Model->get_user_list($user_id); //echo '<pre>'; print_r($user['user_list']); exit();
			 // echo '<pre/>';print_r($user['user_list']);exit;
			$this->load->view('general/send_email',$user);
		}else{
			redirect('users/users_list','refresh');
		}
	}

	function send_mail() 
	{
		$data['firstname'] = $firstname = $this->input->post('firstname');
        $data['mailid']= $mailid = $this->input->post('mailid'); //print_r($data['mailid']); exit();
        $data['subject']= $subject = $this->input->post('subject');
        $data['description']=  $message = $this->input->post('description');
        $this->Email_Model->send_mail_to_user($data);
        //echo '<script> alert("Mail Sent."); </script>';
        redirect('users/users_list', 'refresh');
    }


    function send_user_promo($user_id1) {
    	$user_id 	= json_decode(base64_decode($user_id1));
    	$user = $this->General_Model->getHomePageSettings();
        $promo_id = $_POST['promoid']; 
        $user = $this->Users_Model->get_user_list($user_id1); 

        $promo_value = $this->Promo_Model->get_promo_code_list($promo_id);
		$data['promo_code'] = $promo_value; 
        $data['exp_date'] = date('M j,Y', strtotime($promo_value->exp_date));
        $data['emailid'] = $user['user_info'][0]->user_email; 
        $data['firstname'] = $user['user_info'][0]->user_name; 
        $data['email_template'] = $this->Email_Model->get_email_template('user_promo_code')->row(); 
        $this->Email_Model->send_promo_to_user($data);
        redirect('users/users_list', 'refresh');
    }

    // Export Excel File
function export_b2c_users(){

	$selected_ids = $this->input->post('cid'); //print_r($selected_ids); exit();
	
  if(!empty($selected_ids)){
  	
	$this->load->library("Excel");
	$phpExcel = new PHPExcel();
	$prestasi = $phpExcel->setActiveSheetIndex(0);
	//merger
	$phpExcel->getActiveSheet()->mergeCells('A1:F1');
	//manage row hight
	$phpExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
	//style alignment
		$styleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			),
		);
	$phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);					
	$phpExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
	//border
	$styleArray1 = array(
		  'borders' => array(
			'allborders' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
	);
	//background
		$styleArray12 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => '00acec',
					//'rgb' => '009933',
				),
			),
		);
	//freeepane
		$phpExcel->getActiveSheet()->freezePane('A3');
				//coloum width
		$phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4.1);
		$phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		$prestasi->setCellValue('A1', 'Users Report');
		$phpExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray);
		$phpExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray1);
		$phpExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray12);
		$prestasi->setCellValue('A2', 'Sl No');				
		$prestasi->setCellValue('B2', 'Name');
		$prestasi->setCellValue('C2', 'Email');
		$prestasi->setCellValue('D2', 'Phone No');
		$prestasi->setCellValue('E2', 'Mobile No');
		$prestasi->setCellValue('F2', 'Status');
		
		$user_data = $this->Users_Model->export_b2c_users($selected_ids)->result();
		
		$no=0;
		$rowexcel = 2;
		foreach($user_data as $row){
			$no++;
			$rowexcel++;
			$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel.':F'.$rowexcel)->applyFromArray($styleArray);
			$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel.':F'.$rowexcel)->applyFromArray($styleArray1);
			$prestasi->setCellValue('A'.$rowexcel, $no);
			$prestasi->setCellValue('B'.$rowexcel, $row->user_name);
			$prestasi->setCellValue('C'.$rowexcel, $row->user_email);
			$prestasi->setCellValue('D'.$rowexcel, $row->user_home_phone);
			$prestasi->setCellValue('E'.$rowexcel, $row->user_cell_phone);
			$prestasi->setCellValue('F'.$rowexcel, $row->user_status);
				
		}
		$prestasi->setTitle('Users Report');
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"Users_Report.xlsx\"");
		header("Cache-Control: max-age=0");
		$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
		$objWriter->save("php://output");
	}
	else {
		
		redirect('users/users_list');
		}
	}
	
// Export Excel file End

	//CSV
		 function create_csv(){
		 	$selected_ids = $this->input->post('csv'); //print_r($selected_ids); exit();
	
  if(!empty($selected_ids)){
  	
	$this->load->library("Excel");
	$phpExcel = new PHPExcel();
	$prestasi = $phpExcel->setActiveSheetIndex(0);
	//merger
	$phpExcel->getActiveSheet()->mergeCells('A1:F1');
	//manage row hight
	$phpExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
	//style alignment
		$styleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			),
		);
	$phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);					
	$phpExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
	//border
	$styleArray1 = array(
		  'borders' => array(
			'allborders' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
	);
	//background
		$styleArray12 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => '00acec',
					//'rgb' => '009933',
				),
			),
		);
	//freeepane
		$phpExcel->getActiveSheet()->freezePane('A3');
				//coloum width
		$phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4.1);
		$phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		$prestasi->setCellValue('A1', 'Users Report');
		$phpExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray);
		$phpExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray1);
		$phpExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray12);
		$prestasi->setCellValue('A2', 'Sl No');				
		$prestasi->setCellValue('B2', 'Name');
		$prestasi->setCellValue('C2', 'Email');
		$prestasi->setCellValue('D2', 'Phone No');
		$prestasi->setCellValue('E2', 'Mobile No');
		$prestasi->setCellValue('F2', 'Status');
		
		$user_data = $this->Users_Model->export_b2c_users($selected_ids)->result();
		
		$no=0;
		$rowexcel = 2;
		foreach($user_data as $row){
			$no++;
			$rowexcel++;
			$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel.':F'.$rowexcel)->applyFromArray($styleArray);
			$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel.':F'.$rowexcel)->applyFromArray($styleArray1);
			$prestasi->setCellValue('A'.$rowexcel, $no);
			$prestasi->setCellValue('B'.$rowexcel, $row->user_name);
			$prestasi->setCellValue('C'.$rowexcel, $row->user_email);
			$prestasi->setCellValue('D'.$rowexcel, $row->user_home_phone);
			$prestasi->setCellValue('E'.$rowexcel, $row->user_cell_phone);
			$prestasi->setCellValue('F'.$rowexcel, $row->user_status);
				
		}
		$prestasi->setTitle('Users Report');
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"Users_Report.xlsx\"");
		header("Cache-Control: max-age=0");
		$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
		$objWriter->save("php://output");
	}
	else {
		
		redirect('users/users_list');
		}
            
        }

	//PDF 
   public function pdf1()
{
    
   		$selected_ids = $this->input->post('pdf'); //print_r($selected_ids); exit();
        $user_data['user_list'] = $this->Users_Model->export_b2c_users($selected_ids)->result();
        //echo '<pre>'; print_r($user_data['user_list']); exit();
      
       //$this->load->view('users/users_pdf',$data);
       $this->load->view('pdfreport',$user_data);

    //redirect('users/users_list');
}


	function supplier_list(){
		$user = $this->General_Model->getHomePageSettings();
		$user['user_list'] 	= $this->Agents_Model->getAgentList();
		$user['promo'] = $this->Promo_Model->getPromoCodeList();
		$this->load->view('agents/agent_list',$user);
	}

    //PDF END
}
