<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); error_reporting(E_ALL);
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Banner extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Banner_Model');
		
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function bannerList(){
		$banner 				= $this->General_Model->getHomePageSettings();
		$banner['banner_list'] 	= $this->Banner_Model->getBannerList();
		$this->load->view('banner/banner_list',$banner);
	}
	
	function addBanner(){
		$banner = $this->General_Model->getHomePageSettings();
		$banner['agents'] 		= $this->Banner_Model->getAgentList();
		if(count($_POST) > 0){
			$form_validator = $this->formValidator('add');
			if($form_validator == FALSE  ) {
				$this->load->view('banner/add_banner',$banner);
			    }else{
			$user_profile_name = $this->General_Model->upload_image($_FILES, 'banner');
                        
			$this->Banner_Model->addBannerDetails($_POST,$user_profile_name);
			redirect('banner/bannerList','refresh');
		}
		}else{
			
			$this->load->view('banner/add_banner',$banner);
		}
	}
	
	function activeBanner($banner_id){
		$banner_id 	= json_decode(base64_decode($banner_id));
		$this->Banner_Model->activeBanner($banner_id);
		redirect('banner/bannerList','refresh');
	}
	
	function inactiveBanner($banner_id){
		$banner_id 	= json_decode(base64_decode($banner_id));
		$this->Banner_Model->inactiveBanner($banner_id);
		redirect('banner/bannerList','refresh');
	}
	
	function deleteBanner($banner_id){
		$banner_id 	= json_decode(base64_decode($banner_id));
		$this->Banner_Model->deleteBanner($banner_id);
		redirect('banner/bannerList','refresh');
	}
	
	function editBanner($banner_id)
	{
		$banner_id 	= json_decode(base64_decode($banner_id));
		$banner 				= $this->General_Model->getHomePageSettings();
		$banner['banner_list'] 	= $this->Banner_Model->getBannerList($banner_id);
		$this->load->view('banner/edit_banner',$banner);
	}

	function updateBanner($banner_id)
	{
		$banner_id1 	= json_decode(base64_decode($banner_id));
		if(count($_POST) > 0){
			$form_validator = $this->formValidator('edit');
				if($form_validator == FALSE  ) {
				redirect('banner/editBanner/'.$banner_id,'refresh');	
			    }else{
			$image_info_name = $this->General_Model->upload_image($_FILES, 'banner', $_REQUEST['old_image']);          
			$this->Banner_Model->updateBanner($_POST,$banner_id1, $image_info_name);
			redirect('banner/bannerList','refresh');
		}
		}else if($banner_id1!=''){
			redirect('banner/bannerList','refresh');
		}else{
			redirect('banner/bannerList','refresh');
		}
	}

		function formValidator($type){
	   
	   $this->form_validation->set_rules('title', 'Title', 'required');
	   $this->form_validation->set_rules('img_alt_text', 'Img Alt Text', 'required');
	   
	   
	   if($type == 'add') {
	    $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('img_alt_text', 'Img Alt Text', 'trim|required');
	    }
	    if($type == 'edit') {
	    $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[2]|max_length[50]');
	    $this->form_validation->set_rules('img_alt_text', 'Img Alt Text', 'trim|required');
	   
	    }
	   return $this->form_validation->run();
    }
}
