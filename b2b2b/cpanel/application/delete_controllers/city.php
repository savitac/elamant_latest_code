<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting(E_ALL);
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class City extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('City_Model');
		$this->load->model('Taxrate_Model');
		$this->load->library('form_validation');	
		if(!isset($_SESSION['ses_id'])){
			$sec_res 			= session_id();
	    	$_SESSION['ses_id'] = $sec_res;
		}
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function index(){
		$city 					= $this->General_Model->getHomePageSettings();
		$city['city_list'] 	= $this->City_Model->getCityList();
		//echo '<pre>sanjay'; print_r($state['state_list']); exit();
		$this->load->view('city/city_list',$city);
	}
	
	function addCity(){ //echo '<pre>'; print_r($_POST); exit();
		$city = $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
			$form_validator = $this->formValidator('add');	
			if($form_validator == FALSE  ) {
				$this->load->view('city/add_city',$city);	
			    }else{
			$this->City_Model->addCityDetails($_POST);
			redirect('city','refresh');
		}
		}else{
			
			$this->load->view('city/add_city',$city);
		}
	}
	
	function active_city($city_id1){
		$city_id 	= json_decode(base64_decode($city_id1));
		if($city_id != ''){
			$this->City_Model->active_city($city_id);
		}
		redirect('city','refresh');
	}
	
	function inactive_city($city_id1){
		$city_id 	= json_decode(base64_decode($city_id1));
		if($city_id != ''){
			$this->City_Model->inactive_city($city_id);
		}
		redirect('city','refresh');
	}
	
	function delete_city($city_id1){
		$city_id 	= json_decode(base64_decode($city_id1));
		if($city_id != ''){
			$this->City_Model->delete_city($city_id);
		}
		redirect('city','refresh');
	}
	
	function edit_city($city_id1)
	{
		$city_id 	= json_decode(base64_decode($city_id1));
		if($city_id != ''){
			$city 					= $this->General_Model->getHomePageSettings();
			$city['city_list'] 		= $this->City_Model->getCityList($city_id);
			//echo '<pre>'; print_r($city['city_list']); exit();
			$city['country'] 		=  $this->General_Model->get_country_details();
			$this->load->view('city/edit_city',$city);
		}else{
			redirect('city','refresh');
		}
	}

	function update_city($city_id1)
	{
		$city_id 	= json_decode(base64_decode($city_id1));
		if($city_id != ''){
			if(count($_POST) > 0){
				$this->City_Model->update_city($_POST,$city_id);
				redirect('city','refresh');
			}else if($city_id!=''){
				redirect('city/edit_city/'.$city_id,'refresh');
			}else{
				redirect('city','refresh');
			}
		}else{
			redirect('city','refresh');
		}

	}

	public function getCountry()
	{
		$term = $this->input->get('term');
        $term = trim(strip_tags($term));
        $hotelsp = $this->Taxrate_Model->getCountry($term)->result();
        //echo "<pre>"; print_r($hotelsp); exit();
        foreach($hotelsp as $hotelp){
            $apts['label'] = $hotelp->country_name;
            $apts['value'] = $hotelp->country_name;
            $apts['id'] = $hotelp->country_id;
            $result[] = $apts; 
        }
        
        echo json_encode($result);
	}
	public function getstateName()
	{
		//print_r($this->input->post('CityName'));
		$city = $this->input->post('CityName');
		
		$hotelsp = $this->Taxrate_Model->getstateName($city)->result();
		//echo "<pre/>";print_r($hotelsp); exit();
		//$html = '';
		$html = "<option value=''>Select State</option>";
		foreach ($hotelsp as $key => $hotelp) {
			$html .= '<option value="'.$hotelp->state_name.'">'.$hotelp->state_name.'</option>';
		}
		echo json_encode(array('phphtml' => $html));//format the array into json data
	}
	function formValidator($type){
	   
	   $this->form_validation->set_rules('city_name', 'City Name', 'required');
	   if($type == 'add') {
	   	$this->form_validation->set_rules('city_name', 'City Name', 'trim|required|min_length[3]|max_length[150]');
	    }
	   return $this->form_validation->run();
    }	
	

}
