<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Cancelbuffer extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Cancelbuffer_Model');
		$this->load->model('Product_Model');
		$this->load->model('Api_Model');
		$this->load->library('form_validation');
		
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	$this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function cancelbufferList(){
		$buffer 				= $this->General_Model->getHomePageSettings();
		$buffer['cancelbuffer_list'] 	= $this->Cancelbuffer_Model->getBufferList();
		$buffer['product'] 	= $this->Product_Model->getProductList();
		
		$this->load->view('cancelbuffer/cancelbuffer_list',$buffer);
	}
	
	function addCancelBuffer(){
			$buffer = $this->General_Model->getHomePageSettings();
			
			$buffer['product'] 	= $this->Product_Model->getActiveProductList();
			$buffer['api'] = $this->Api_Model->getActiveApiList();
		if(count($_POST) > 0){

				$cancel = $this->Cancelbuffer_Model->get_available_cancelbuffer($_POST['product_id'], $_POST['api']);
				
				if(count($cancel) > 0){
					 $buffer['error'] = 'Entered Details is already available';
					 $this->load->view('cancelbuffer/add_cancelbuffer',$buffer);	
				}
				else{
					
			$this->Cancelbuffer_Model->addBufferDetails($_POST);
			redirect('cancelbuffer/cancelbufferList','refresh');
		
				}


			
		}else{
			
			$this->load->view('cancelbuffer/add_cancelbuffer',$buffer);
		}
	}
	
	function activecancelbuffer($buffer_id1){
		$buffer_id 	= json_decode(base64_decode($buffer_id1));
		if($buffer_id != ''){
			$this->Cancelbuffer_Model->activecancelbuffer($buffer_id);
		}
		redirect('cancelbuffer/cancelbufferList','refresh');
	}
	
	function inactivecancelbuffer($buffer_id1){
		$buffer_id 	= json_decode(base64_decode($buffer_id1));
		if($buffer_id != ''){
			$this->Cancelbuffer_Model->inactivecancelbuffer($buffer_id);
		}
		redirect('cancelbuffer/cancelbufferList','refresh');
	}
	
	function deletecancelbuffer($buffer_id1){ 
		$buffer_id 	= json_decode(base64_decode($buffer_id1));
		if($buffer_id != ''){
			$this->Cancelbuffer_Model->deletecancelbuffer($buffer_id);
		}
		redirect('cancelbuffer/cancelbufferList','refresh');
	}
	
	function editcancelbuffer($buffer_id1)
	{
		$buffer_id 	= json_decode(base64_decode($buffer_id1));
		if($buffer_id != ''){
			$buffer 					= $this->General_Model->getHomePageSettings();
			$buffer['buffer_list'] 		= $this->Cancelbuffer_Model->getBufferList($buffer_id);
			$this->load->view('cancelbuffer/edit_cancelbuffer',$buffer);
		}else{
			redirect('cancelbuffer/cancelbufferList','refresh');
		}
	}

	function updatecancelbuffer($buffer_id)
	{
		$buffer_id 	= json_decode(base64_decode($buffer_id)); 
		if($buffer_id != ''){			
			if(count($_POST) > 0){
				
				$this->Cancelbuffer_Model->updatecancelbuffer($_POST,$buffer_id);
				redirect('cancelbuffer/cancelbufferList','refresh');
			}else if($buffer_id!=''){
				redirect('cancelbuffer/editcancelbuffer/'.$buffer_id,'refresh');
			}else{
				redirect('cancelbuffer/cancelbufferList','refresh');
			}
		}else{
			redirect('cancelbuffer/cancelbufferList','refresh');
		}
	}
	
}
?>
