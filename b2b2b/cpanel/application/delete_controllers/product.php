<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Product extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Product_Model');
		$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
		$this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
     
	function productList(){
		$product 					= $this->General_Model->getHomePageSettings();
		$product['product_list'] 	= $this->Product_Model->getProductList();
		$this->load->view('product/product_list',$product);
	}
	
	function addProduct(){
		$product = $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
			$form_validator = $this->formValidator('add');	
			  if($form_validator == FALSE  ) {
				$this->load->view('product/add_product',$product);	
			    }else{
			$this->Product_Model->addProduct($_POST);
			redirect('product/productList','refresh');
		}
		}else{
			$this->load->view('product/add_product',$product);
		}
	}
	
	function updateStatus($product_id1, $status1){
		$product_id 	= json_decode(base64_decode($product_id1));
		$status 	= json_decode(base64_decode($status1));
		
		if($product_id != ''){
			$this->Product_Model->updateStatus($product_id, $status);
		}
		redirect('product/productList','refresh');
	}
	
	
	function deleteProduct($product_id1){
		$product_id 	= json_decode(base64_decode($product_id1));
		if($product_id != ''){
			$this->Product_Model->delete_product($product_id);
		}
		redirect('product/product_list','refresh');
	}
	
	function editProduct($product_id1)
	{
		$product_id 	= json_decode(base64_decode($product_id1));
        if($product_id != ''){
			$product 			= $this->General_Model->getHomePageSettings();
			$product['product'] = $this->Product_Model->getProductList($product_id);
			$this->load->view('product/edit_product',$product);
		}else{
			redirect('product/productList','refresh');
		}
	}
	
	function updateProduct($product_id){   
		$product_id 	= json_decode(base64_decode($product_id));
		$product['product_id'] = $product_id;
		if($product_id != ''){
			if(count($_POST) > 0){
				$form_validator = $this->formValidator('edit');
				if($form_validator == FALSE) {
					$this->load->view('product/edit_product',$product);	
			    }else{
				$this->Product_Model->updateProduct($_POST,$product_id);
				redirect('product/productList','refresh');
			    }
				
			}else if($product_id!=''){
				redirect('product/editProduct/'.$product_id,'refresh');
			}else{
				redirect('product/productList','refresh');
			}
		}else{
			redirect('product/productList','refresh');
		}
	}
	
	 function formValidator($type){
	   $this->form_validation->set_rules('product_name', 'Product Name', 'required');
	   if($type == 'add') {
	    $this->form_validation->set_rules('product_name', 'product Name', 'required|xss_clean||is_unique[product_details.product_name]|max_length[150]');
	    }
	   return $this->form_validation->run();
    }	

}
