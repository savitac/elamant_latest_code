<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Popularhotels extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Popularhotels_Model');
		$this->load->model('Banner_Model');
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
    function index(){
		$categories							= $this->General_Model->getHomePageSettings();
		$categories['category_list'] 		= $this->Popularhotels_Model->get_deals_categories();
		
		$this->load->view('deals/categories_list',$categories);
	}
	
	function add_category(){
			if(count($_POST) > 0){ //print_r($_POST); exit();
			
			$this->form_validation->set_rules('country','Country','required');
			if($this->form_validation->run()==TRUE){
				$this->Popularhotels_Model->add_category_details($_POST);
				redirect('popularhotels','refresh');
			} else {
				$categories = $this->General_Model->getHomePageSettings();
				$this->load->view('deals/add_category',$categories);
			}
		}else{
			$categories = $this->General_Model->getHomePageSettings();
			$categories['agents'] 		= $this->Banner_Model->getAgentList();
			$this->load->view('deals/add_category',$categories);
		}
	}
	
	function inactive_category($category_id){
		$category_id = json_decode(base64_decode($category_id));
		if($category_id != ''){
			$this->Popularhotels_Model->inactive_category($category_id);
		}
		redirect('popularhotels','refresh');
	}
	
	function active_category($category_id){
		$category_id = json_decode(base64_decode($category_id));
		if($category_id != ''){
			$this->Popularhotels_Model->active_category($category_id);
		}
		redirect('popularhotels','refresh');
	}
	 
	
	
	function delete_category($category_id){
		$category_id = json_decode(base64_decode($category_id));
		if($category_id != ''){
			$this->Popularhotels_Model->delete_category($category_id);
		}
		redirect('popularhotels','refresh');
	}
	
	
	function deals_list($category_id){
		$category_id = json_decode(base64_decode($category_id));
		if($category_id != ''){
			$categories 				= $this->General_Model->getHomePageSettings();
			$categories['category_id']	= $category_id;
			$categories['deals_list'] 	= $this->Popularhotels_Model->get_deals_list($id='',$category_id);
			$this->load->view('deals/deal_list', $categories);
		} else {
			redirect('popularhotels','refresh');
		}
	}

	function add_deal($category_id) { 
		$category_id1 = json_decode(base64_decode($category_id)); 
        if (count($_POST) > 0) {
        	$category_id1 = json_decode(base64_decode($category_id));
            $user_profile_name = $this->General_Model->upload_image($_FILES, 'hotels');
            $this->Popularhotels_Model->add_deal($_POST,$category_id1, $user_profile_name);
            redirect('popularhotels/deals_list/'.$category_id, 'refresh');
        } else {
            $data = $this->General_Model->getHomePageSettings();
            $data['category_id']	= $category_id;
            $this->load->view('deals/add_deal', $data);
        }
    }
	public function getHotelCitycountry()
	{
		$term = $this->input->get('term');
        $term = trim(strip_tags($term));
        $hotelsp = $this->Popularhotels_Model->getAirportList($term)->result();
        foreach($hotelsp as $hotelp){
            $apts['label'] = $hotelp->city_name.', '.$hotelp->city;
            $apts['value'] = $hotelp->city_name.', '.$hotelp->city;
            $apts['id'] = $hotelp->id;
            $result[] = $apts; 
        }
        
        echo json_encode($result);
	}
	function inactive_deal($id, $category_id){
		$id = json_decode(base64_decode($id));
		if($id != ''){
			$this->Popularhotels_Model->inactive_deal($id);
		}
		redirect('popularhotels/deals_list/'.$category_id,'refresh');
	}
	function active_deal($id, $category_id){
		$id = json_decode(base64_decode($id));
		if($id != ''){
			$this->Popularhotels_Model->active_deal($id);
		}
		redirect('popularhotels/deals_list/'.$category_id,'refresh');
	}
	function edit_deal($id, $category_id){
		$id = json_decode(base64_decode($id));
		$category_id1 = json_decode(base64_decode($category_id));
		if($id != '' && $category_id1 != ''){
			$deal 					= $this->General_Model->getHomePageSettings();
			$deal['deal'] 			= $this->Popularhotels_Model->get_deals_list($id);
			$deal['category_id'] 	= $category_id1;
			$this->load->view('deals/edit_deal',$deal);
		} else {
			redirect('popularhotels/deals_list/'.$category_id,'refresh');
		}
	}
	
	  function update_deal($id1,$category_id) { 
	  
        if (count($_POST) > 0) { 
            $id = json_decode(base64_decode($id1));
			if($id != ''){
				$image_info_name = $this->General_Model->upload_image($_FILES, 'hotels', $_REQUEST['old_image']);
				$this->Popularhotels_Model->update_deal($_POST, $id, $image_info_name);
			}
            redirect('popularhotels/deals_list/'.$category_id, 'refresh');
        } else if ($id != '') {
            redirect('popularhotels/deals_list/'.$category_id, 'refresh');
        } else {
            redirect('popularhotels/deals_list/'.$category_id, 'refresh');
        }
    }
	function delete_deal($id, $category_id){
		$id = json_decode(base64_decode($id));
		if($id != ''){
			$this->Popularhotels_Model->delete_deal($id);
		}
		redirect('deals/deals_list/'.$category_id,'refresh');
	}

	function getHotelCity()
	{
		$term = $this->input->get('term');
        $term = trim(strip_tags($term));
        $hotelsp = $this->Popularhotels_Model->getAirportList($term)->result();
        foreach($hotelsp as $hotelp){
            $apts['label'] = $hotelp->city_name.', '.$hotelp->city;
            $apts['value'] = $hotelp->city_name;
            $apts['id'] = $hotelp->id;
            $result[] = $apts; 
        }
        
        echo json_encode($result);
	}

  	public function gethotelName()
	{
		//print_r($this->input->post('CityName'));
		$city = $this->input->post('CityName');
		$city = explode(",", $city); //print_r($city); exit();
		$data['City_name'] = $city[1];
		$data['Country_name'] = ltrim($city[2]);
		$hotelsp = $this->Popularhotels_Model->get_Hotel_Name($data)->result();
		//echo "<pre/>";print_r($hotelsp); exit();
		$html = '';
		foreach ($hotelsp as $key => $hotelp) {
			$html .= '<option value="'.$hotelp->HotelName.'">'.$hotelp->HotelName.'</option>';
		}
		echo json_encode(array('phphtml' => $html));//format the array into json data
	}
} 
?>
