<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Privilege extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Roles_Model');
		$this->load->model('Privilege_Model');
		
       	$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	     $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	
	
	function privilegeList(){
		$privilege 						= $this->General_Model->getHomePageSettings();
		$privilege['privilege_list'] 	= $this->Privilege_Model->getPrivilegeList();
		$this->load->view('privilege/privilege_list',$privilege);
	}
	
	function addPrivilege(){
		if(count($_POST) > 0){
			$this->Privilege_Model->add_privilege_details($_POST);
			redirect('privilege/privilege_list','refresh');
		}else{
			$privilege							= $this->General_Model->getHomePageSettings();
			$privilege['roles_list'] 			= $this->Roles_Model->get_roles_list();
			$privilege['module_list'] 			= $this->Privilege_Model->getModuleList();
			$privilege['module_details_list'] 	= $this->Privilege_Model->getModuleDetailsList();
			$this->load->view('privilege/add_privilege',$privilege);
		}
	}
	
	function active_privilege($privilege_id){
		$this->Privilege_Model->active_privilege($privilege_id);
		redirect('privilege/privilege_list','refresh');
	}
	
	function inactive_privilege($privilege_id){
		$this->Privilege_Model->inactive_privilege($privilege_id);
		redirect('privilege/privilege_list','refresh');
	}
	
	function delete_privilege($privilege_id){
		$this->Privilege_Model->delete_privilege($privilege_id);
		redirect('privilege/privilege_list','refresh');
	}
	
	function edit_privilege($privilege_id)
	{
		$privilege 		= $this->General_Model->get_home_page_settings();
		$privilege['privilege'] = $this->Privilege_Model->get_privilege_list($privilege_id);
		$this->load->view('privilege/edit_privilege',$privilege);
	}

	function update_privilege($privilege_id)
	{
		if(count($_POST) > 0){
			$this->Privilege_Model->update_privilege($_POST,$privilege_id);
			redirect('privilege/privilege_list','refresh');
		}else if($privilege_id!=''){
			redirect('privilege/privilege_list','refresh');
		}else{
			redirect('privilege/privilege_list','refresh');
		}
	}
	function privilegeInfo($role_id='')
	{ 
		$role 	= json_decode(base64_decode($role_id));
		$data['role_id']=$role;
		$data['info']=$this->General_Model->getLeftMenuDetailsWorking();
		$data['default']=$this->General_Model->getDefaultSections();
		$this->load->view('privilege/privilege_info',$data);
	}
}
