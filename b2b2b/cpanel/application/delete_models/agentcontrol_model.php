<?php
class Agentcontrol_Model extends CI_Model {

    function __construct()
    {
       
        parent::__construct();
    }   
    function get_all_menus($details_id=''){
		
		$this->db->select('*');
		$this->db->from('homepage_leftmenu');
		if($details_id != ''){
		$this->db->where("homepage_details_id", $details_id);
		}
	   return $this->db->get()->result();
	} 
	
	function inactive_menu($menuid){
		$data = array("dashboard_status" => "INACTIVE");
		$this->db->where("homepage_details_id", $menuid);
		$this->db->update("homepage_leftmenu", $data);
		return;
	}
	
	function active_menu($menuid){
		$data = array("dashboard_status" => "ACTIVE");
		$this->db->where("homepage_details_id", $menuid);
		$this->db->update("homepage_leftmenu", $data);
		return ;
	}
	
	function update_agent_menu($data, $menuid){
		$data = array("dashboard_name" => $data['dashboard_name'],
		              "user_types"=> implode(",", $data["user_type"]));
		              
		$this->db->where("homepage_details_id", $menuid);
		$this->db->update("homepage_leftmenu", $data);
		return ;
	}
	
}
?>
