<?php
class Email_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_email_acess()
		{
			$this->db->select('*');
			$this->db->from('email_access');
			$query = $this->db->get();
			if ( $query->num_rows > 0 ) {
			 return $query->row();
			}
			return false;
		}

    function get_max_email_number($email_type)
    {
		$this->db->where('mail_label',$email_type);
		$this->db->from('mail_box_details');
		return $this->db->count_all_results();
	}

	function insert_email_details($insert)
	{
		$this->db->select('message_number');
		$this->db->from('mail_box_details');
		$this->db->where('message_number',$insert['message_number']);
		$this->db->where('mail_label',$insert['mail_label']);
		$query = $this->db->get();
		if($query->num_rows > 0){}else{
			$insert_data = array(
								'message_number' 				=> $insert['message_number'],
								'message_id' 					=> $insert['message_id'],
								'admin_id' 						=> '1',
								'mail_label'					=> $insert['mail_label'],
								'header_info'	 				=> json_encode($insert['headerinfo']),
								'body_info' 					=> json_encode($insert['imap_body']),
								'structure_info' 				=> json_encode($insert['imap_fetchstructure']),
								'mail_box_details_time_stamp' 	=> (date('Y-m-d H:i:s'))			
							);
			$this->db->insert('mail_box_details',$insert_data);
		}
	}

	function get_email_details($id = '')
	{
		$this->db->select('*');
		$this->db->from('mail_box_details');
		if($id!='')
			$this->db->where('mail_box_details_id',$id);
		$query = $this->db->get();
		if($query->num_rows > 0) 
        {
			return $query->result();
		}
		else
		{
			return '';
		}
	}

	public function get_email_template($email_type) {
        $this->db->where('email_type', $email_type);
        return $this->db->get('email_template');
    }
	
	function getEmailTemplateDetails($email_template_id = ''){
		$this->db->select('*');
		$this->db->from('email_template');
		if($email_template_id!='')
			$this->db->where('email_template_id',$email_template_id);
		$query = $this->db->get();
		if($query->num_rows > 0) {
			return $query->result();
		}else{
			return '';
		}
	}

	function activeEmailTemplate($email_template_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('email_template_id', $email_template_id);
		$this->db->update('email_template', $data); 
		$this->General_Model->insert_log('3','activeEmailTemplate',json_encode($data),'updating  Email Template status to active','email_template','email_template_id',$email_template_id);
	}
	
	function inactiveEmailTemplate($email_template_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('email_template_id', $email_template_id);
		$this->db->update('email_template', $data); 
		$this->General_Model->insert_log('3','inactiveEmailTemplate',json_encode($data),'updating  Email Template status to inactive','email_template','email_template_id',$email_template_id);
	}
	
	function deleteEmailTemplate($email_template_id){
		$this->db->where('email_template_id', $email_template_id);
		$this->db->delete('email_template'); 
		$this->General_Model->insert_log('3','deleteEmailTemplate',json_encode(array()),'deleting  Email Template from database','email_template','email_template_id',$email_template_id);
	}
	
	function updateMailTemplate($update,$email_template_id){
		if(!isset($update['status']))
			$update['status'] = "INACTIVE";
		$update_data = array(
							'email_type' 			=> $update['email_type'],
							'email_from' 			=> $update['email_from'],
							'email_from_name' 		=> $update['email_from_name'],
							'subject' 				=> $update['subject'],
							'message' 				=> $update['message'],
							'status' 				=> $update['status']					
						);
		$this->db->where('email_template_id', $email_template_id);
		$this->db->update('email_template', $update_data);
		$this->General_Model->insert_log('3','updateMailTemplate',json_encode($update_data),'updating  Email template  to database','email_template','email_template_id',$email_template_id);
	}

	 function fetch_email_template($id)
    {
        $this->db->select('message');
        $this->db->from('email_template');
        $this->db->where('email_template_id', $id);

        $query = $this->db->get();
        if($query->num_rows() > 0)
        { 
            return $query->row();
        }    
        else 
        {
            return '';
        }     
    }

	function sendMailToUser($data){
	        $mail = $data['mailid'];
	        $message = $data['description'];
	        $subject = $data['subject'];
			$data['email_access'] = $this->get_email_acess();
		        $config = Array(
		            'protocol' => $data['email_access']->smtp,
		            'smtp_host' => $data['email_access']->host,
		            'smtp_port' => $data['email_access']->port,
		            'smtp_user' => $data['email_access']->username,
		            'smtp_pass' => $data['email_access']->password,
		            'mailtype' => 'html',
		            'charset' => 'iso-8859-1',
		            'wordwrap' => TRUE
		        );
	       
	        $this->load->library('email', $config);
	        $this->email->set_newline("\r\n");
	        $this->email->from($data['email_access']->username);
	      	$test =  $this->email->to($mail);
	        $this->email->subject($subject);
	        $this->email->message($message);
	        $this->email->send();
	        
   }

   
 		 public function send_promocode_mail($data){ //echo '<pre>sanjay'; print_r($data); exit();
		     	$delimiters =$data['emailid'];
		     	$subject = $data['subject'];
				$message1 = $data['email_template']->message;
		   $message1 = str_replace("{%%PROMOCODE%%}", $data[0]->promo_code, $message1);
		   $message1 = str_replace("{%%PromoType%%}", $data[0]->promo_type, $message1);
		   $message1 = str_replace("{%%PRODUCT%%}", $data[0]->product, $message1);
		   $message1 = str_replace("{%%BookingRange%%}", $data[0]->booking_date_from.' '.'to'.' '.$data[0]->booking_date_to, $message1);
		   $message1 = str_replace("{%%TravelDate%%}", $data[0]->travel_date_from.' '.'to'.' '.$data[0]->travel_date_to, $message1);
		   $message1 = str_replace("{%%AmountRange%%}", $data[0]->amount_valid_from.' '.'AUD'.' '.'to'.' '.$data[0]->amount_valid_to.' '.'AUD', $message1);
			$message1 = str_replace("{%%DISCOUNT%%}", $data[0]->discount, $message1);
			$message1 = str_replace("{%%SUBJECT%%}", $data['description'], $message1);
			$message1 = str_replace("{%%EXPIRYDATE%%}", date('M j,Y', strtotime($data[0]->exp_date)), $message1);
			
			$data['email_access'] = $this->get_email_acess();
			$config = Array(
			    'protocol' => $data['email_access']->smtp,
			     'smtp_host' => $data['email_access']->host,
			    'smtp_port' => $data['email_access']->port,
			    'smtp_user' => $data['email_access']->username,
			    'smtp_pass' => $data['email_access']->password,
			    'mailtype' => 'html',
			    'charset' => 'iso-8859-1',
			    'wordwrap' => TRUE
			);

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($data['email_template']->email_from, $data['email_template']->email_from_name);
			
			$this->email->to($delimiters); 
			$this->email->subject($subject);
			$this->email->message($message1);
			$this->email->send();
		 }




  		public function sendPromoToUser($data){
		     	$delimiters =$data['emailid'];
				$message1 = $data['email_template']->message;
			$message1 = str_replace("{%%FIRSTNAME%%}", $data['firstname'], $message1);
			$message1 = str_replace("{%%PROMOCODE%%}", $data['promo_code'][0]->promo_code, $message1);
			$message1 = str_replace("{%%PromoType%%}", $data['promo_code'][0]->promo_type, $message1);
		   $message1 = str_replace("{%%PRODUCT%%}", $data['promo_code'][0]->product, $message1);
		   $message1 = str_replace("{%%BookingRange%%}", $data['promo_code'][0]->booking_date_from.' '.'to'.' '.$data['promo_code'][0]->booking_date_to, $message1);
		   $message1 = str_replace("{%%TravelDate%%}", $data['promo_code'][0]->travel_date_from.' '.'to'.' '.$data['promo_code'][0]->travel_date_to, $message1);
		   $message1 = str_replace("{%%AmountRange%%}", $data['promo_code'][0]->amount_valid_from.' '.'AUD'.' '.'to'.' '.$data['promo_code'][0]->amount_valid_to, $message1);
			$message1 = str_replace("{%%DISCOUNT%%}", $data['promo_code'][0]->discount, $message1);
			$message1 = str_replace("{%%EXPIRYDATE%%}", date('M j,Y', strtotime($data['promo_code'][0]->exp_date)), $message1);
			$data['email_access'] = $this->get_email_acess();
			
			$config = Array(
			    'protocol' => $data['email_access']->smtp,
			    'smtp_host' => $data['email_access']->host,
			    'smtp_port' => $data['email_access']->port,
			    'smtp_user' => $data['email_access']->username,
			    'smtp_pass' => $data['email_access']->password,
			    'mailtype' => 'html',
			    'charset' => 'iso-8859-1',
			    'wordwrap' => TRUE
			);

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($data['email_template']->email_from, $data['email_template']->email_from_name);
			$this->email->to($delimiters);
			$this->email->subject($data['email_template']->subject);
			$this->email->message($message1);
			$this->email->send();
			 }

  

   public function sendAddUserActive($data){
   	$mail = $data['user_info'][0]->user_email;
	$message1 = $data['email_template']->message; //echo '<pre>'; print_r($message1);
    $message1 = str_replace("{%%FIRSTNAME%%}", $data['user_info'][0]->user_name, $message1);
	$message1 = str_replace("{%%USERNAME%%}", $data['user_info'][0]->user_email, $message1);
	$message1 = str_replace("{%%URL%%}","provabextranet.com/WDMA/TCG-DEMO/",$message1);
	$message1 = str_replace("{%%USER%%}", $data['user_info'][0]->user_email, $message1);
    $message1 = str_replace("{%%USERIMAGE%%}", $data['user_info'][0]->user_profile_pic, $message1);
	$subject = $data['email_template']->subject;
	$subject = str_replace("{%%USER%%}", $data['user_info'][0]->user_name, $subject);

 

	$data['email_access'] = $this->get_email_acess();
	$delimiters = $data['user_info'][0]->user_email;
        $config = Array(
	        'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        	
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
          $this->email->to($mail);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
   }
   
   function send_add_user_inactive($data){ 
	        $mail = $data['user_info'][0]->user_email; //echo 'hi'; print_r($mail); exit();
	        $message = 'Your user Account is De-Activated, If you have any Queries.. please contact our Customer Support';
	        $subject =  'TheChinaGap';
	        
			$data['email_access'] = $this->get_email_acess();
		        $config = Array(
		            'protocol' => $data['email_access']->smtp,
		           'smtp_host' => $data['email_access']->host,
		            'smtp_port' => $data['email_access']->port,
		            'smtp_user' => $data['email_access']->username,
		            'smtp_pass' => $data['email_access']->password,
		            'mailtype' => 'html',
		            'charset' => 'iso-8859-1',
		            'wordwrap' => TRUE
		        );
	        $this->load->library('email', $config);
	        $this->email->set_newline("\r\n");
	        $this->email->from($data['email_access']->username);
	      	$test =  $this->email->to($mail);
	        $this->email->subject($subject);
	        $this->email->message($message);
	        $this->email->send();
    }

  public function sendAgentDepositMail($data,$deposit){
    	$message1 = $data['email_template']->message;
        $message1 = str_replace("{%%FIRSTNAME%%}", $data['user_info'][0]->user_name, $message1);
        $message1 = str_replace("{%%AMOUNT%%}", $deposit[0]->amount_credit, $message1);
    	$message1 = str_replace("{%%CURAMOUNT%%}", $deposit[0]->amount_credit, $message1);
    	$message1 = str_replace("{%%TRANSID%%}", $deposit[0]->transaction_id, $message1);
    	$message1 = str_replace("{%%TOTALAMOUNT%%}", $deposit[0]->amount_credit, $message1);
    	$subject = $data['email_template']->subject;
    	$data['email_access'] = $this->get_email_acess();
    	$mail = $data['user_info'][0]->user_email;
        $config = Array(
            'protocol' => $data['email_access']->smtp,
	        'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
          $this->email->to($mail);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
    }

   function add_supplier_send_mail($data){ 
	        $mail = $data['email_id'];
	        $message = 'Now you can login to your Suppiler Account:'.''.'Your '.' '. 'Username:'.' '.$data['email_id'];
	        $subject =  'Thechinagap';
	        
			$data['email_access'] = $this->get_email_acess();
		        $config = Array(
		           'protocol' => $data['email_access']->smtp,
		            'smtp_host' => $data['email_access']->host,
		            'smtp_port' => $data['email_access']->port,
		            'smtp_user' => $data['email_access']->username,
		            'smtp_pass' => $data['email_access']->password,
		            'mailtype' => 'html',
		            'charset' => 'iso-8859-1',
		            'wordwrap' => TRUE
		        );
	        $this->load->library('email', $config);
	        $this->email->set_newline("\r\n");
	        $this->email->from($data['email_access']->username);
	      	$test =  $this->email->to($mail);
	        $this->email->subject($subject);
	        $this->email->message($message);
	        $this->email->send();
	 }

		  public function send_add_supplier_active($data){ 
		   	$mail = $data['supplier_info'][0]->supplier_email;
			$message1 = $data['email_template']->message; //echo '<pre>'; print_r($message1);
			$message1 = str_replace("{%%FIRSTNAME%%}", $data['supplier_info'][0]->supplier_email, $message1);
			$message1 = str_replace("{%%USERNAME%%}", $data['supplier_info'][0]->supplier_name, $message1);
			$message1 = str_replace("{%%URL%%}","http://provabextranet.com/WDMA/TheChinaGap/supplier_login",$message1);
			$message1 = str_replace("{%%USER%%}", $data['supplier_info'][0]->supplier_email, $message1);
		    $message1 = str_replace("{%%USERIMAGE%%}", $data['supplier_info'][0]->supplier_profile_pic, $message1);
		   $subject = $data['email_template']->subject;
			$subject = str_replace("{%%USER%%}", $data['supplier_info'][0]->supplier_name, $subject);
            $data['email_access'] = $this->get_email_acess();
		    $delimiters = $data['supplier_info'][0]->supplier_email;
			$config = Array(
			    //'protocol' => 'mail',
			    'protocol' => $data['email_access']->smtp,
			    'smtp_host' => $data['email_access']->host,
			    'smtp_port' => $data['email_access']->port,
			    'smtp_user' => $data['email_access']->username,
			    'smtp_pass' => $data['email_access']->password,
			    'mailtype' => 'html',
			    'charset' => 'iso-8859-1',
			    'wordwrap' => TRUE
			);
			
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($data['email_access']->username);
		    $this->email->to($mail);
			$this->email->subject($subject);
			$this->email->message($message1);
			$this->email->send();
		 }
   		function add_admin_send_mail($data){  
   			 $mail = $data['email_id']; 
	        $message = 'Now you can login to your Sub-admin Account:'.''.'Your '.' '. 'Username:'.' '.$data['email_id'];
	        $subject =  'TheChinaGap';
	        
			$data['email_access'] = $this->get_email_acess();
		        $config = Array(
		           'protocol' => $data['email_access']->smtp,
		            'smtp_host' => $data['email_access']->host,
		            'smtp_port' => $data['email_access']->port,
		            'smtp_user' => $data['email_access']->username,
		            'smtp_pass' => $data['email_access']->password,
		            'mailtype' => 'html',
		            'charset' => 'iso-8859-1',
		            'wordwrap' => TRUE
		        );
	        $this->load->library('email', $config);
	        $this->email->set_newline("\r\n");
	        $this->email->from($data['email_access']->username);
	      	$test =  $this->email->to($mail);
	        $this->email->subject($subject);
	        $this->email->message($message);
	        $this->email->send();
	    }

   		public function send_subadmin_active($data){ //echo '<pre>'; print_r($data); exit();
   	$mail = $data['admin_info'][0]->admin_email;
	$message1 = $data['email_template']->message; //echo '<pre>'; print_r($message1);
        $message1 = str_replace("{%%FIRSTNAME%%}", $data['admin_info'][0]->admin_email, $message1);
	$message1 = str_replace("{%%USERNAME%%}", $data['admin_info'][0]->admin_name, $message1);
	$message1 = str_replace("{%%URL%%}","http://provabextranet.com/WDMA/TheChinaGap/cpanel",$message1);
	$message1 = str_replace("{%%USER%%}", $data['admin_info'][0]->admin_email, $message1);
    $message1 = str_replace("{%%USERIMAGE%%}", $data['admin_info'][0]->admin_profile_pic, $message1);
     $subject = $data['email_template']->subject;
	$subject = str_replace("{%%USER%%}", $data['admin_info'][0]->admin_name, $subject);

 

	$data['email_access'] = $this->get_email_acess();
     $delimiters = $data['admin_info'][0]->admin_email;
        $config = Array(
	       'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        	
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
        $this->email->to($mail);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
     }

   public function sendAddUserRegistration($data,$post){ 
   	$mail = $post['email_id'];
	$message1 = $data['email_template']->message; //echo '<pre>'; print_r($message1);
        $message1 = str_replace("{%%FIRSTNAME%%}", $post['first_name'], $message1);
	$message1 = str_replace("{%%USERNAME%%}", $post['email_id'], $message1);
	$message1 = str_replace("{%%URL%%}","http://provabextranet.com/WDMA/TCG-DEMO/",$message1);
	$message1 = str_replace("{%%USER%%}", $post['email_id'], $message1);
    $subject = $data['email_template']->subject;
	$subject = str_replace("{%%USER%%}", $post['first_name'], $subject);

 

	$data['email_access'] = $this->get_email_acess();
  	$delimiters = $post['email_id'];
        $config = Array(
	        'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        	
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
          $this->email->to($mail);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
    }
function send_newsletter($data){ 
	        $mail_to = $data['email_id'];
	        $message = $data['message'];
	        $subject = $data['subject'];
			$data['email_access'] = $this->get_email_acess();
		        $config = Array(
		           'protocol' => $data['email_access']->smtp,
		            'smtp_host' => $data['email_access']->host,
		            'smtp_port' => $data['email_access']->port,
		            'smtp_user' => $data['email_access']->username,
		            'smtp_pass' => $data['email_access']->password,
		            'mailtype' => 'html',
		            'charset' => 'iso-8859-1',
		            'wordwrap' => TRUE
		        );
	       
	        $this->load->library('email', $config);
	        $this->email->set_newline("\r\n");
	        $this->email->from($data['email_from']);
	      	$test =  $this->email->to($mail_to);
	        $this->email->subject($subject);
	        $this->email->message($message);
	        $this->email->send();
	}

   public function sendmail_voucher($data) {
        $message1 = $data['message'];
        $email = $data['email'];
        $subject = "Your TheChinaGap Voucher";
        $data['email_access'] = $this->get_email_acess('BOOKING');
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username, "TheChinaGap");
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message1);
        if(!$this->email->send()){
            $error = $this->email->print_debugger();
            return false;
        } else {
            return true;
        }
    }

     public function sendmail_hotelVoucher($email) {
       
        $message = $email['message'];

        $to = $email['to'];
        
        $subject = 'Hotel Booking Voucher';
       
        $config = Array(
            'protocol' => $email['email_access']->smtp,
            'smtp_host' => $email['email_access']->host,
            'smtp_port' => $email['email_access']->port,
            'smtp_user' => $email['email_access']->username,
            'smtp_pass' => $email['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE
        ); 
        
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($email['email_template']->email_from, $email['email_template']->email_from_name);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
         if ($this->email->send()) {
            return 1;
        }else{
            return 0;
        }
    }

    public function sendmail_transferVoucher($email) {
       
        $message = $email['message'];

        $to = $email['to'];
        
        $subject = 'Transfer Booking Voucher';
       
        $config = Array(
            'protocol' => $email['email_access']->smtp,
            'smtp_host' => $email['email_access']->host,
            'smtp_port' => $email['email_access']->port,
            'smtp_user' => $email['email_access']->username,
            'smtp_pass' => $email['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE
        ); 
        
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($email['email_template']->email_from, $email['email_template']->email_from_name);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
         if ($this->email->send()) {
            return 1;
        }else{
            return 0;
        }
    }

     public function sendAgentSupportAlertMail($user_email,$support_id){
       //echo '<pre>hi'; print_r($test['sub']); exit();
        
        $message1 = 'Hi'. $user.'we recieved your ticket for'.' '. $support_id.'Check your support inbox for message';;
        $subject = 'Reply  of Support Ticket From Admin';
        $data['email_access'] = $this->get_email_acess();
        $mail = $user_email;
        
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
        $this->email->to($mail);
        $this->email->cc('Shashi@thechinagap.com');
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
    }

     public function sendmail_forgot_password($data) {
        $message1 = $data['email_template']->message;
        $message1 = str_replace("{%%FIRSTNAME%%}", str_replace('-', ' ', $data['user_data']->admin_name), $message1);
        $message1 = str_replace("{%%USERNAME%%}", $data['user_data']->admin_email, $message1);
        $message1 = str_replace("{%%WEB_URL%%}", base_url(), $message1);
        $message1 = str_replace("{%%RESETLINK%%}", $data['reset_link'], $message1);

        $subject = $data['email_template']->subject;
        $subject = str_replace("{%%USER%%}", str_replace('-', ' ', $data['user_data']->admin_name), $subject);

        $data['email_access'] = $this->get_email_acess();
        
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username, "TheChinaGap");
        $this->email->to($data['user_data']->admin_email);
        $this->email->subject($data['email_template']->subject);
        $this->email->message($message1);
        if ($this->email->send()) {
            return 1;
        } else {
            return 0;
        }
    }
}
?>
