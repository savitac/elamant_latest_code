<?php
class Country_Model extends CI_Model {

    function __construct(){
        
        parent::__construct();
    }

    function getCountryList($country_id = ''){
		$this->db->select('*');
		$this->db->from('country_details');
		if($country_id !='')
			$this->db->where('country_id', $country_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
		
	}
	
    function add_country_details($input){
		if(!isset($input['country_status']))
			$input['country_status'] = "INACTIVE";
		$insert_data = array(
							'iso_code' 			=> $input['iso_code'],
							'iso3_code' 		=> $input['iso3_code'],
							'country_name' 		=> $input['country_name'],
							'iso_numeric' 		=> $input['iso_numeric'],
							'phone_code' 		=> $input['phone_code'],
							'currency_name' 	=> $input['currency_name'],
							'currency_code' 	=> $input['currency_code'],
							'currency_symbol' 	=> $input['currency_symbol'],
							'country_status' 	=> $input['country_status'],
							'creation_date'		=> (date('Y-m-d H:i:s'))					
						);			
		$this->db->insert('country_details',$insert_data);
		$country_id = $this->db->insert_id();
		$this->General_Model->insert_log('9','add_country_details',json_encode($insert_data),'Adding  Country Details to database','country_details','country_id',$country_id);
	}

	function activeCountry($country_id){
		$data = array(
					'country_status' => 'ACTIVE'
					);
		$this->db->where('country_id', $country_id);
		$this->db->update('country_details', $data);
		$this->General_Model->insert_log('9','activeCountry',json_encode($data),'updating   Country Details to active','country_details','country_id',$country_id); 
	}
	
	function inactiveCountry($country_id){
		$data = array(
					'country_status' => 'INACTIVE'
					);
		$this->db->where('country_id', $country_id);
		$this->db->update('country_details', $data);
		$this->General_Model->insert_log('9','inactiveCountry',json_encode($data),'updating  Country Details status to inactive','country_details','country_id',$country_id); 
	}
	
	function delete_country($country_id){
		$this->db->where('country_id', $country_id);
		$this->db->delete('country_details'); 
		 $this->General_Model->insert_log('9','delete_country',json_encode(array()),'deleting  Country Details from database','country_details','country_id',$country_id);
	}
	
	function updateCountry($update,$country_id){
		if(!isset($update['country_status']))
			$update['country_status'] = "INACTIVE";
		$update_data = array(
							'iso_code' 			=> $update['iso_code'],
							'iso3_code' 		=> $update['iso3_code'],
							'country_name' 		=> $update['country_name'],
							'iso_numeric' 		=> $update['iso_numeric'],
							'phone_code' 		=> $update['phone_code'],
							'currency_name' 	=> $update['currency_name'],
							'currency_code' 	=> $update['currency_code'],
							'currency_symbol' 	=> $update['currency_symbol'],
							'country_status' 	=> $update['country_status']				
						);			
		$this->db->where('country_id', $country_id);
		$this->db->update('country_details', $update_data);
		$this->General_Model->insert_log('9','update_country',json_encode($update_data),'updating  Country Details  to database','country_details','country_id',$country_id);
	}
}
?>
