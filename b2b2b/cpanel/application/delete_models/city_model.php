<?php
class City_Model extends CI_Model {

    function __construct(){
        
        parent::__construct();
    }

    function getCityList($city_id = ''){
		$this->db->select('*');
		$this->db->from('city_details');
		if($city_id !='')
			$this->db->where('city_details_id', $city_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
		
	}
	
    function addCityDetails($input){ //echo '<pre>'; print_r($input); exit();
		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
		$insert_data = array(
							'country_id' 		=> $input['country'][0],
							'state_details_id' 	=> $input['state_name'],
							'city_name' 		=> $input['city_name'],
							'city'				=>$input['city_value'],
							'city_code' 		=> $input['city_code'],
							'latitude' 			=> $input['latitude'],
							'longitude' 		=> $input['longitude'],
							'zipcode' 			=> $input['zipcode'],
							'status' 			=> $input['status'],
							'creation_date'		=> (date('Y-m-d H:i:s'))					
						);
						//echo '<pre>'; print_r($insert_data); exit();		
		$this->db->insert('city_details',$insert_data);
		$city_details_id = $this->db->insert_id();
		$this->General_Model->insert_log('9','add_city_details',json_encode($insert_data),'Adding  city List to database','city_details','city_details_id',$city_details_id);
	}

	function active_city($city_details_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('city_details_id', $city_details_id);
		$this->db->update('city_details', $data); 
		$this->General_Model->insert_log('9','active_city',json_encode($data),'updating  city List status to active','city_details','city_details_id',$city_details_id);
	}
	
	function inactive_city($city_details_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('city_details_id', $city_details_id);
		$this->db->update('city_details', $data); 
		$this->General_Model->insert_log('9','inactive_city',json_encode($data),'updating  city List status to inactive','city_details','city_details_id',$city_details_id);
	}
	
	function delete_city($city_details_id){
		$this->db->where('city_details_id', $city_details_id);
		$this->db->delete('city_details'); 
		$this->General_Model->insert_log('9','delete_city',json_encode(array()),'deleting  city List from database','city_details','city_details_id',$city_details_id);
	}
	
	function update_city($update,$city_details_id){
		if(!isset($update['country_status']))
			$update['country_status'] = "INACTIVE";
		$update_data = array(
							
							'city_name' 		=> $update['city_name'],
							'city' 				=> $update['city'],
							'city_code' 		=> $update['city_code'],
							'latitude' 			=> $update['latitude'],
							'longitude' 		=> $update['longitude'],
							'zipcode' 			=> $update['zipcode'],
							'status' 			=> $update['country_status']					
						);				
		$this->db->where('city_details_id', $city_details_id);
		$this->db->update('city_details', $update_data);
		$this->General_Model->insert_log('9','update_city',json_encode($update_data),'updating  city List  to database','city_details','city_details_id',$city_details_id);
	}
}
?>
