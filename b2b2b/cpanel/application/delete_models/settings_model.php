<?php
class Settings_Model extends CI_Model {

    function __construct(){
        
        parent::__construct();
        
    }
    
    function get_settings_list($settings_id = ''){
		
		$this->db->select('*');
		$this->db->from('general_settings');
		if($settings_id !='')
			$this->db->where('general_settings_id', $settings_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function update_settings_details($input){		
		$this->db->truncate('general_settings'); 
		
		$insert_data = array(
							'site_title' 					=> $input['site_title'],
							'tag_line' 						=> $input['tag_line'],
							'site_url' 						=> $input['site_url'],
							'email_address' 				=> $input['email_address'],
							'contact_number' 				=> $input['contact_number'],
							'contact_address' 				=> $input['contact_address'],
							'sub_admin_default_role' 		=> $input['sub_admin_role'],
							'new_user_activation_method' 	=> $input['new_user_activation_method'],
							'login_attempts' 				=> $input['login_attempts'],
							'date_format' 					=> $input['date_format'],			
							'date_format_value' 			=> $input[$input['date_format'].'_value'],
							
						);			
		$this->db->insert('general_settings',$insert_data);
		$subscriber_id = $this->db->insert_id();
		$this->General_Model->insert_log('12','update_settings_details',json_encode($insert_data),'updating  Setting Details from database','general_settings','general_settings_id',$subscriber_id);
	}
}
?>
