<?php
class Usertype_Model extends CI_Model {

    function __construct()
    {
       parent::__construct();
    }

     function get_user_type_list($user_type_id = ''){
		$this->db->select('*');
		$this->db->from('user_type');
		$this->db->where('status', 'ACTIVE');
		if($user_type_id !='')
			$this->db->where('user_type_id', $user_type_id);
		    
		$query=$this->db->get();
		
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	} 

	function get_user_type($user_type_id = ''){
		$this->db->select('*');
		$this->db->from('user_type');
		if($user_type_id !='')
			$this->db->where('user_type_id', $user_type_id);
			$this->db->where('role', 'Agent');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	} 

	function get_admin_type_list($admin_id = ''){
		$this->db->select('*');
		$this->db->from('admin_details');
		if($admin_id !='')
			$this->db->where('admin_id', $admin_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
}
?>
