<?php
class Cancelbuffer_Model extends CI_Model {

    function __construct(){
        
        parent::__construct();
    }

    function getBufferList($cancelbuffer_id = ''){
		$this->db->select('*');
		$this->db->from('cancelbuffer');
		if($cancelbuffer_id !='')
			$this->db->where('cancelbuffer_id', $cancelbuffer_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addBufferDetails($input){
		if(!isset($input['cancel_status']))
			$input['cancel_status'] = "INACTIVE";
		$insert_data = array(
							'api_id' 				=> $input['api'],
							'product_id' 			=> $input['product_id'],
							'cancel_days' 		    => $input['canceldays'],
							'buffer_status' 		=> $input['cancel_status'],
							'creation_date'			=> (date('Y-m-d H:i:s'))					
						);		
			
		$this->db->insert('cancelbuffer',$insert_data);
	}
   
	function activecancelbuffer($buffer_id){
		$data = array(
					'buffer_status' => 'ACTIVE'
					);
		$this->db->where('cancelbuffer_id', $buffer_id);
		$this->db->update('cancelbuffer', $data); 
	}
	
	function inactivecancelbuffer($buffer_id){
		$data = array(
					'buffer_status' => 'INACTIVE'
					);
		$this->db->where('cancelbuffer_id', $buffer_id);
		$this->db->update('cancelbuffer', $data); 
	}
	
	function deletecancelbuffer($buffer_id){
		$this->db->where('cancelbuffer_id', $buffer_id);
		$this->db->delete('cancelbuffer'); 
	}
	
	function updatecancelbuffer($update,$buffer_id){
		if(!isset($update['cancel_status']))
			$update['cancel_status'] = "INACTIVE";
		$update_data = array(
							
							'cancel_days' 				=> $update['canceldays'],
							'buffer_status' 			=> $update['cancel_status'],
							'updated_date'			=> (date('Y-m-d H:i:s'))	
						);
		$this->db->where('cancelbuffer_id', $buffer_id);
		$this->db->update('cancelbuffer', $update_data);
	}

	function get_available_cancelbuffer($product_id, $api_id){
         
       $this->db->select("*");
      $this->db->from("cancelbuffer");
      $this->db->where("api_id", $api_id);
      $this->db->where("product_id", $product_id);
     
      $query = $this->db->get()->row();

      return $query;
  }
	
    
}
?>
