<?php
class Homepagedashboard_Model extends CI_Model {

    function __construct(){
        
        parent::__construct();
    }

    function getHeaderList($homepage_details_id = ''){
		$this->db->select('*');
		$this->db->from('homepage_leftmenu');
		if($homepage_details_id !='')
			$this->db->where('homepage_details_id', $homepage_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addHomepagedashboardDetails($input){
		if(!isset($input['dashboard_status']))
			$input['dashboard_status'] = "INACTIVE";
		$insert_data = array(
							'dashboard_name' 			=> $input['dashboard_name'],
							'dashboard_icon' 			=> $input['dashboard_icon'],
							'dashboard_url' 				=> $input['dashboard_url'],
							'position' 				=> $input['position'],
							'dashboard_status' 			=> $input['dashboard_status'],
							'dashboard_creation_date'	=> (date('Y-m-d H:i:s'))					
						);		
			
		$this->db->insert('homepage_leftmenu',$insert_data);
	}
   
	function activehomepagedashboard($header_id){
		$data = array(
					'dashboard_status' => 'ACTIVE'
					);
		$this->db->where('homepage_details_id', $header_id);
		$this->db->update('homepage_leftmenu', $data); 
	}
	
	function inactivehomepagedashboard($header_id){
		$data = array(
					'dashboard_status' => 'INACTIVE'
					);
		$this->db->where('homepage_details_id', $header_id);
		$this->db->update('homepage_leftmenu', $data); 
	}
	
	function deleteHeader($header_id){
		$this->db->where('homepage_details_id', $header_id);
		$this->db->delete('homepage_leftmenu'); 
	}
	
	function updatehomepagedashboardDetails($update,$api_id){
		if(!isset($update['dashboard_status']))
			$update['dashboard_status'] = "INACTIVE";
		$update_data = array(
							'dashboard_name' 		=> $update['dashboard_name'],
							'dashboard_name_china'  => $update['dashboard_name_china'],
							'position' 				=> $update['position'],
							'dashboard_status' 		=> $update['dashboard_status']			
						);
		$this->db->where('homepage_details_id', $api_id);
		$this->db->update('homepage_leftmenu', $update_data);
	}
	
    
}
?>
