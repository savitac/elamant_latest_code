<?php
class Product_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    }
	
	function getProductList($product_id = ''){
		$this->db->select('*');
		$this->db->from('product_details');
		if($product_id !='')
			$this->db->where('product_details_id', $product_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function getActiveProductList($product_id = ''){
		$this->db->select('*');
		$this->db->from('product_details');
		if($product_id !='')
			$this->db->where('product_details_id', $product_id);
		$this->db->where('product_status', '1');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function addProduct($input){	
		$insert_data = $this->form_data($input);
		$insert_data['product_created_by'] = $this->session->userdata('provabAdminId');
		$insert_data['product_creation_date'] = date('Y-m-d');
		$this->db->insert('product_details',$insert_data);
		$product_id = $this->db->insert_id();
		$this->General_Model->insert_log('2','add_product',json_encode($insert_data),'Adding  Product Details to database','product_details','product_details_id',$product_id);
	}
	
   function updatestatus($product_id, $status) {
	   $data = array(
					'product_status' => $status ,
					'product_modified_by' => $this->session->userdata('provabAdminId'), 
		            'product_modified_date' => date('Y-m-d')
					);
		
		$this->db->where('product_details_id', $product_id);
		$this->db->update('product_details', $data); 
		$this->General_Model->insert_log('2','active_product',json_encode($data),'updating Product Details status to '.$status,'product_details','product_details_id',$product_id);   
   }
	
	function delete_product($product_id){
		$this->db->where('product_details_id', $product_id);
		$this->db->delete('product_details'); 
		$this->General_Model->insert_log('2','delete_product',json_encode(array()),'deleting  Product Details from database','product_details','product_details_id',$product_id);
	}
	
	function updateProduct($update,$product_id){
		$update_data = $this->form_data($update);
		$update_data['product_modified_by'] = $this->session->userdata('provabAdminId');
		$update_data['product_modified_date'] = date('Y-m-d');
		$this->db->where('product_details_id', $product_id);
		$this->db->update('product_details', $update_data);
		$this->General_Model->insert_log('2','update_product',json_encode($update_data),'updating Product Details to database','product_details','product_details_id',$product_id);
	}
	
	
   function form_data($input){
	   	if(!isset($input['product_status']))
			$input['product_status'] = "1";
			
	   $insert_data = array(
							'product_name' 			=> $input['product_name'],
							'product_icon' 		=> $input['product_icon'],
							'product_order' 		=> $input['product_order'],
							'product_status' 		=> $input['product_status'],
						);
		return 	$insert_data;
   }
   
  
}
?>
