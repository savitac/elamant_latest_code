<?php
class Airport_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function get_airPort_list($airline_details_id = ''){
    	$rpp = 500;
		$page = 1;

		$this->db->select('*');
		$this->db->from('flight_airport_list');
		if($airline_details_id !='')
			$this->db->where('airport_id', $airline_details_id);
		$query=$this->db->get();
		//echo '<pre>'; print_r($query->num_rows); exit();
		//$sql = 'SELECT COUNT(*) as count';
		$sql = $query->num_rows;
		$count = $sql;
		
		$total_page = ceil($count/$rpp);
		//print_r($total_page); exit();
			if($page=1){
				$top=0;
				} else {
				$top=$rpp * $page;
				}
		$sql = 'SELECT * FROM flight_airport_list LIMIT $top,$rpp';

		$query1 = $sql;

		if($query->num_rows() ==''){
			return '';
		}else{
			 $query->result();
		}

		echo '<pre>'; print_r($query1); exit();
	}
	
    function add_airline_details($input){

		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
		$insert_data = array(
							'airline_name' 			=> $input['airline_name'],
							'airline_code' 			=> $input['airline_code'],
							'status' 				=> $input['status']				
						);	

		$this->db->insert('airlines_list',$insert_data);
	}

	function active_airline($airline_details_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('id', $airline_details_id);
		$this->db->update('airlines_list', $data); 
	}
	
	function inactive_airline($airline_details_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('id', $airline_details_id);
		$this->db->update('airlines_list', $data); 
	}
	
	function delete_airline($airline_details_id){
		$this->db->where('id', $airline_details_id);
		$this->db->delete('airlines_list'); 
	}
	
	function update_airline($update,$airline_details_id){
		if(!isset($update['status']))
			$update['status'] = "INACTIVE";
		$update_data = array(
							'airline_name' 			=> $update['airline_name'],
							'airline_code' 			=> $update['airline_code'],
							
							'status' 				=> $update['status']				
						);

		$this->db->where('id', $airline_details_id);
		
		$this->db->update('airlines_list', $update_data);
	}
}
?>
