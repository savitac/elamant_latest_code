<?php
class Currency_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function getCurrencyList($currency_id = ''){
		$this->db->select('*');
		$this->db->from('currency_details');
		if($currency_id !='')
			$this->db->where('currency_details_id', $currency_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addCurrencyDetails($input){
		$insert_data = $this->form_data($input);
		$insert_data['date_time'] = date('Y-m-d h:m:s');
		$insert_data['added_by'] = $this->session->userdata('provabAdminId');
		$this->db->insert('currency_details',$insert_data);
		$currency_id = $this->db->insert_id();
		$this->General_Model->insert_log('3','add_currency_details',json_encode($insert_data),'Adding  API to database','currency_details','currency_details_id',$currency_id);
	}
	
	
	
	 function add_SiteCurrency_Details($input){
		
	      $string = implode(',', $input['currency']);
	      $sql = "DELETE FROM site_currency";
	      $result=$this->db->query($sql);
	      if( $result){
			  $sql = "insert into site_currency(site_currency_ids)values('$string')";
			 return  $result=$this->db->query($sql);
		  }


	}
	function get_selected_site_currency(){
		
			  $sql = "select * from site_currency";
			 return  $result=$this->db->query($sql)->row_array();

	}
	
	
	function updateStatus($currency_id, $status) {
		$data = array(
					'status' => $status
					);
		$this->db->where('currency_details_id', $currency_id);
		$this->db->update('currency_details', $data); 
		$this->General_Model->insert_log('3','active_currency',json_encode($data),'updating  API status to '.$status,'currency_details','currency_details_id',$currency_id);
	}
	
	
	function delete_currency($currency_id){
		$this->db->where('currency_details_id', $currency_id);
		$this->db->delete('currency_details'); 
		$this->General_Model->insert_log('3','delete_currency',json_encode(array()),'deleting  API from database','currency_details','currency_details_id',$currency_id);
	}
	
	function update_currency($update,$currency_id){
        $update_data = $this->form_data($update);
        $update_data = $this->form_data($update);
		$this->db->where('currency_details_id', $currency_id);
		$this->db->update('currency_details', $update_data);
		$this->General_Model->insert_log('3','update_currency',json_encode($update_data),'updating  API  to database','currency_details','currency_details_id',$currency_id);
	}
	
    function form_data($input){
		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
		$insert_data = array(
							'currency_code' 		=> $input['currency_code'],
							'country_name' 			=> $input['country_name'],
							'currency_symbol' 		=> $input['currency_symbol'],
							'currency_name' 		=> $input['currency_name'],
							'value' 				=> $input['value'],
							'status' 				=> $input['status'],
							'date_time'				=> (date('Y-m-d H:i:s'))					
						);
		return $insert_data;
	}

	public function currency_convertor($amount,$from,$to){
    	$to = strtoupper($to);
    	$this->db->where('currency_code',$from);
    	$price = $this->db->get('currency_details')->row();
    	$value = $price->value;
    	if($from === $to){
    		$amount = $amount/1; 
    		return number_format(($amount) ,2,'.','');
    	}else{
    		$amount = ($amount)/($value);
    		return number_format(($amount) ,2,'.','');
    	}
    	
    }
    
    public function get_currencies(){
		
		$sql = "select currency_code from currency_details";
		return $this->db->query($sql)->result_array();
	}
	
	
	
	
}
?>
