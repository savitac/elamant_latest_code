<?php
class Security_Model extends CI_Model {

    function __construct(){
          parent::__construct();
    }
    
      public function adminIpTrack($type){
		$data = array(
					'admin_user_name' => $_POST["username"],
					'login_attempt_type' => $type,
					'login_track_details_ip' => $_SERVER['REMOTE_ADDR'],
					'login_track_status_info' => $_SERVER['HTTP_USER_AGENT'],
					'login_track_details_system_info' => $_SERVER['REMOTE_ADDR'].'||'.$_SERVER['REMOTE_PORT']
				);

		$this->db->insert('admin_login_tracking_details', $data);
		$this->checkIpCount($_SERVER['REMOTE_ADDR']);
		return $this->db->insert_id();
    }
    
    function checkIpCount($ip_addr) {
		$this->db->select("*");
		$this->db->from("admin_white_iplist");
		$this->db->where("ip_address", $ip_addr);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return "";
		}else {
		 $this->insertNewIpLog($ip_addr);
		}
	}
	
	function insertNewIpLog($ip_addr){
		$data = array("ip_address" => $ip_addr, 
		              "admin_id" => 1,
		              "created_date" => date('Y-m-d'),
		              "status" => 1);
		$this->db->insert("admin_white_iplist", $data);
	}
    
    function updateLoginStatus($adminId, $trackId){
		$data = array("admin_id" => $adminId,
		              "login_status" => 1);
		$this->db->where('admin_login_tracking_details_id', $trackId);
		$this->db->update('admin_login_tracking_details', $data);
	}

	function insertStatus($updatestatus){
		
		$data = array(
					'user_id' => $updatestatus['provabAdminId'],
					'user_name' => $updatestatus['provabAdminName'],
					'user_email' => $updatestatus['provabAdminEmail'],
					'status' => $updatestatus['status'],
					'loggedin' => $updatestatus['provabAdminLoggedIn'],
					'created_date' => (date('Y-m-d H:i:s'))
				);
		 
		$this->db->insert('login_trackings', $data);
		return $this->db->insert_id();
	}
	
	function updateStatus($status, $logout){
		$data = array(
		              "status" => 0
		              );
		$this->db->where('user_id', $logout);
		$this->db->update('login_trackings', $data);
	}
	
	public function adminIpLogoutTrack($type){
		$data = array(
					'login_attempt_type' => $type,
					'login_track_details_ip' => $_SERVER['REMOTE_ADDR'],
					'login_track_status_info' => $_SERVER['HTTP_USER_AGENT'],
					'login_track_details_system_info' => $_SERVER['REMOTE_ADDR'].'||'.$_SERVER['REMOTE_PORT'],
					'admin_id' => $this->session->userdata('provabAdminId'),
					'logout_status' => 1
				);
		$this->db->insert('admin_logout_tracking_details', $data);
		return $this->db->insert_id();
    }
	
	function checkActiveLogins($userName){
		$this->db->where('admin_user_name', $userName);
		$this->db->where('login_status', 1);
		return $this->db->get('admin_login_tracking_details')->result();
		
	}
	
	function login_attempts() {
		$this->db->select('*');
		$this->db->from('general_settings');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
    public function adminIpAttempt($track_id){
		$last_track_id	= $track_id;
		$sql 	= "SELECT * FROM admin_login_tracking_details WHERE login_tracking_details_time_stamp >= DATE_SUB(NOW(), INTERVAL 2 HOUR) AND login_attempt_type !='URL'";
		$result = $this->db->query($sql);
		if (!$result->num_rows() || $result->num_rows()== 0){
			$data['row_count']	=	0;
			$data['ins_id']		=	'';
			return $data; 
		}else{
			if($result->num_rows() < 5){
				$data['row_count']	=	$result->num_rows();
				$data['ins_id']		=	'';
				return $data;
			}else{
				$signval 	=  $track_id.'||||'.$_SERVER['REMOTE_ADDR'];
				$signurl 	=  $this->encrypt_string('encrypt',$signval);
				$data 		=  array(
									'signurl' 							=> urlencode($signurl),
									'status' 							=> 0,
									'admin_login_tracking_details_id' 	=> $track_id
							   );
				$this->db->set('expiry_date', 'NOW() + INTERVAL 12 HOUR', FALSE); 
				$this->db->insert('admin_login_retrive', $data);
				
				$ins_id 			= $this->db->insert_id();
				$data 				= array('status ' => 'BLOCK');
				$this->db->update('admin_white_iplist', $data);				

				$data['row_count']	= $result->num_rows();
				$data['ins_id']		= $ins_id;
				return $data; 
			}
		}
	}
	
      public function adminIpCheck(){
		$this->db->select('ip_address')->from('admin_white_iplist')->where('ip_address', $_SERVER['REMOTE_ADDR'])->where('status', '1');
		$query = $this->db->get();	
		if ( $query->num_rows > 0 ){
				return "ACTIVE";	
		}else{
			$this->db->select('ip_address')->from('admin_white_iplist')->where('ip_address', $_SERVER['REMOTE_ADDR'])->where('status', 'BLOCK');
			$query = $this->db->get();
			if ( $query->num_rows > 0 ){
	     		return "BLOCK";
			}else{
				return "DENIED";	
			}
		}
    }
    
    
	public function checkLoginStatus($username,$password){
		$username	=	($username);
		$password	= $password;
		
		$this->db->select('a.admin_id, a.admin_email, a.admin_name,a.admin_account_number,a.admin_profile_pic,a.role_details_id ');
		$this->db->from('admin_details a');
		$this->db->join('admin_login_details al', 'a.admin_id = al.admin_id');
		$this->db->where('al.admin_user_name', $username);
		$this->db->where('al.admin_password', $password);
	    $this->db->where('a.admin_status','ACTIVE');
	    $query = $this->db->get();
	 
		if( $query->num_rows > 0 ){
			$data['status']	=	1;
			$data['result']	=	$query->row();
			return $data;	
	    }else{
			$data['status']	=	0;
			$data['result']	=	'';
			return $data;
		}
    }

   /* public function checkLoginStatus($username,$password){
    	print_r($username); exit();
		$this->db->select('*');
		$this->db->from('admin_login_details');
		$this->db->join('admin_details', 'admin_details.admin_email = admin_login_details.admin_user_name');
		$this->db->where('admin_login_details.admin_password', $password);
		$this->db->where('admin_login_details.admin_user_name', $username);
		//$this->db->where('user_login_details.user_type_id', $user_type);
		$this->db->where('admin_details.admin_email', $username);
//		print_r($this->db);exit;
		return  $query = $this->db->get();
		echo $this->db->last_query($query); exit();
	}*/
     
   
    
    public function check_login_status_by_Id($provab_admin_id,$password){
		$provab_admin_id	=	mysql_real_escape_string($provab_admin_id);
		$password			=	mysql_real_escape_string($password);
		$sql 				= 	"SELECT a.admin_id, a.admin_email, a.admin_name,a.admin_account_number,a.admin_profile_pic FROM admin_details a JOIN admin_login_details al on a.admin_id = al.admin_id where al.admin_id = '$provab_admin_id' AND al.admin_password  =  AES_ENCRYPT('$password','".SECURITY_KEY."')  AND a.admin_status='ACTIVE'";
		$query 				= 	$this->db->query($sql);
		if( $query->num_rows > 0 ){
			$data['status']	=	1;
			$data['result']	=	$query->row();
			return $data;	
	    }else{
			$data['status']	=	0;
			$data['result']	=	'';
			return $data;
		}
    }
   
    function get_previous_url(){
		$this->db->select('url');
		$this->db->from('admin_lock_screen_details');
		$this->db->where('admin_id', $this->session->userdata('provabAdminId'));
		$this->db->where('provab_admin_session_id', $this->session->userdata('provabAdminSessionId'));
		$this->db->where('status', 'ACTIVE');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			$result = $query->row();
		}
		$data = array('status' => 'INACTIVE');
		$where = array('admin_id' => $this->session->userdata('provabAdminId'),'provab_admin_session_id' => $this->session->userdata('provabAdminSessionId'));
		$this->db->where($where);
		$this->db->update('admin_lock_screen_details', $data);
		return $result;
	}
   
	function encrypt_string($action, $string){
		$output 		= false;
		$encrypt_method = "AES-256-CBC";
		$secret_key 	= SECURITY_KEY;
		$secret_iv 		= 'DFJKIUHJGTUHJUJNJHGUIKSOXUCJDDJDJDJDJDC';
		$key =  substr(hash('sha256', $secret_key), 0, 32); // hash('sha256', $secret_key);
		$iv = substr(hash('sha256', $secret_iv), 0, 16); // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		if( $action == 'encrypt' ) {
			$output = base64_encode($string); // $output = openssl_encrypt($string, $encrypt_method, $key, $iv);
		}else if( $action == 'decrypt' ){
			$output = base64_decode($string); // $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, $iv);
		}
		return $output;
	}

	/*public function checkAdminPassword($password){
		$password 	= mysql_real_escape_string($password);
	    //$sql 		= "SELECT admin_id,admin_user_name, md5(admin_password,'".SECURITY_KEY."') as password  FROM admin_login_details WHERE admin_password  =  AES_ENCRYPT('$password','".SECURITY_KEY."') ";
		$sql 		= "SELECT admin_id,admin_user_name, MD5(admin_password) as password  FROM admin_login_details WHERE admin_password  =  md5('$password') ";
		$query 		= $this->db->query($sql);
		if ( $query->num_rows > 0 ){
			$data['status']	= 1;
			$data['result']	= $query->row();
			return $data;	
	    }else{
			$data['status']	= 0;
			$data['result']	= '';
			return $data;
		}
    }*/
    public function checkAdminPassword($password){
		//$username	=	($username);
		$password	= $password;
		
		$this->db->select('admin_id,admin_user_name,admin_password');
		$this->db->from('admin_login_details');
		
		$this->db->where('admin_password', $password);
	    
	    $query = $this->db->get();
	 
		if( $query->num_rows > 0 ){
			$data['status']	=	1;
			$data['result']	=	$query->row();
			return $data;	
	    }else{
			$data['status']	=	0;
			$data['result']	=	'';
			return $data;
		}
    }
   
	/*public function updateAdminPassword($password){
		$password	=	mysql_real_escape_string($password);
		$admin_id 	= 	$this->session->userdata('provabAdminId');
		//$update_sql	= 	"UPDATE admin_login_details SET admin_password  =  AES_ENCRYPT('$password','".SECURITY_KEY."') WHERE admin_id  =  '$admin_id' ";
		$update_sql	= 	"UPDATE admin_login_details SET admin_password  =  MD5('$password') WHERE admin_id  =  '$admin_id' ";
		if (  $this->db->query($update_sql) ){
			return true;	
	    }else{
			return false;
		}
	}*/

	function updateAdminPassword($password){
		$password	= $password;
		$admin_id 	= 	$this->session->userdata('provabAdminId');
		$update_data_address = array(
								'admin_password' 		=> $password				
								);
		$this->db->where('admin_id', $admin_id);
		$this->db->update('admin_login_details', $update_data_address);
          
	}

	
	
	public function update_supplier_password($password){
		$password	=	mysql_real_escape_string($password);
		$admin_id 	= 	$this->session->userdata('lgm_supplier_admin_id');
		$update_sql	= 	"UPDATE supplier_login_details SET supplier_password  =  AES_ENCRYPT('$password','".SECURITY_KEY."') WHERE supplier_details_id  =  '$admin_id' ";
		if (  $this->db->query($update_sql) ){
			return true;	
	    }else{
			return false;
		}
	}
   
   public function get_supplier_id($admin_id){
		$password 	= mysql_real_escape_string($admin_id);
		$sql 		= "SELECT * FROM supplier_details WHERE admin_id = $admin_id";
		$query 		= $this->db->query($sql);
		if ( $query->num_rows > 0 ){
			$data	= $query->row();
			return $data->supplier_details_id;	
	    }else{
			return '';
		}
    }

     function insert_lock_screen_details($input){
		$data = array(
					'admin_id' 						=> $this->session->userdata('provab_admin_id'),
					'provab_admin_session_id' 		=> $this->session->userdata('provab_admin_session_id'),
					'user_name' 					=> $this->session->userdata('provab_admin_name'),
					'url'		 					=> $input['current_url'],
					'status'						=> 'ACTIVE',
					'admin_lock_screen_timestamp' 	=> (date('Y-m-d H:i:s'))
				);
		$this->db->insert('admin_lock_screen_details', $data);
		return $this->db->insert_id();
    }
}
?>
