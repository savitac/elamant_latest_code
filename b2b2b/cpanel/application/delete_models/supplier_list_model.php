<?php
class Supplier_list_Model extends CI_Model {

    function __construct()
    {
       
        parent::__construct();
    }   
    function getAgentList($user_id = '7'){
		 
		$this->db->select('*');
		$this->db->from('user um');
		$this->db->join('user_type d', 'um.user_type = d.user_type_id');
		$this->db->distinct();
		//changed user_type 5 from 7 since supplier user type is 5 @ajaz
		$this->db->where('user_type', '5');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	} 
	
	function get_domain_details($user_details_id) {
		$this->db->select('um.domain_details_id, d.domain_name');
		$this->db->from('user_management_details um');
		$this->db->join('domain_details d', 'um.domain_details_id = d.domain_details_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_user_type_details($user_details_id) {
		$this->db->select('um.user_type_id, ut.user_type_name');
		$this->db->from('user_management_details um');
		$this->db->join('user_type ut', 'um.user_type_id = ut.user_type_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_product_details($user_details_id) {
		$this->db->select('um.product_details_id, p.product_name');
		$this->db->from('user_management_details um');
		$this->db->join('product_details p', 'um.product_details_id = p.product_details_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_api_details($user_details_id) {
		$this->db->select('um.api_details_id, a.api_name, a.api_alternative_name');
		$this->db->from('user_management_details um');
		$this->db->join('api_details a', 'um.api_details_id = a.api_details_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_country_details($user_details_id) {
		$this->db->select('um.product_details_id, c.country_name, c.iso3_code');
		$this->db->from('user_management_details um');
		$this->db->join('country_details c', 'um.country_id = c.country_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function get_all_user_email($user_email) {
		$this->db->select('user_email');
		$this->db->from('user_details');
		if($user_email !='')
			$this->db->where('user_email', $user_email);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return 'NO';
		}else{
			return 'YES';
		}
	}

	function addAgentDetails($input,$user_profile_name){
		if(!isset($input['user_status']))
			$input['status'] = "INACTIVE";
		$insert_data_address = array(
								'address' 		=> $input['address'],
								'city' 			=> $input['city'],
								'pin_code' 		=> $input['zip_code'],
								'state' 		=> $input['state_name'],
								'country_code' 	=> $input['country'],					
							);	
		$this->db->insert('user',$insert_data_address);
		$address_details_id = $this->db->insert_id();
		$this->General_Model->insert_log('4','addAgentDetails',json_encode($insert_data_address),'adding  user address  Details to database','address_details','address_details_id',$address_details_id);
		$insert_data_user = array(
								'domain_list_fk' 			=>$input['domain_id'],
								//'user_type' 				=> '7',
								'first_name' 			    => $input['comapany_name'], 
								'address' 					=> $input['address'],
								'user_name' 				=> $input['first_name']." ".$input['last_name'],
								'position'					=> $input['position'],
								'agent_tradingname'			=> $input['agency_tradename'],
								'agent_licenseno'			=> $input['agent_licenseno'],
								'email' 					=> $input['email_id'],
								'office_phone' 				=> $input['phone_no'],
								'phone' 					=> $input['mobile_no'],										
								/*'user_profile_pic' 			=> $user_profile_name,*/
								/*'country_id' 				=> $input['country'],*/				
								//'status' 					=> $input['user_status'],					
								'created_datetime' 			=> (date('Y-m-d H:i:s'))					
							);	

		// newly added code for supplier registeration by @Ajaz
						if($input['position']=="supplier"){
							$insert_data_user['email']=$input['email_id'];
							$insert_data_user['password']=md5($input['new_password']);
							$insert_data_user['user_name']=$input['email_id'];
							$insert_data_user['status']=$input['user_status'];
							if($insert_data_user['status']=="ACTIVE"){
								$insert_data_user['status']=1;
							}else{
								$insert_data_user['status']=0;
							}
							$insert_data_user['user_type']=5;
							
						}		
		// debug code				
		/*echo "<pre>";
		print_r($insert_data_user);
		exit("are you tehrer1");
		*/
		$this->db->insert('user',$insert_data_user);
		$user_details_id = $this->db->insert_id();
		// commeted because not use full  start  Ajaz
		$this->General_Model->insert_log('4','addAgentDetails',json_encode($insert_data_user),'adding  user  Details to database','user_details','user_details_id',$user_details_id);
		
		$insert_data_user_login = array(
									'user_details_id' 	=> $user_details_id,
									'user_name' 		=> $input['email_id'],
									'user_password' 	=> "AES_ENCRYPT(".$input['new_password'].",'".SECURITY_KEY."')",
									'user_type_id'      => '2',
									'admin_pattren' 	=> ''					
								);
		$this->db->insert('user_login_details',$insert_data_user_login);
		$user_id = $this->db->insert_id();
		$this->General_Model->insert_log('4','addAgentDetails',json_encode($insert_data_user_login),'adding  user login Details to database','user_login_details','user_login_details_id',$user_id);
		// end here
		
	}
	
	function activeAgents($user_id){
		$data = array(
					'user_status' => 'ACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
		$this->General_Model->insert_log('4','activeAgents',json_encode($data),'updating  user status to active','user_details','user_details_id',$user_id);
	}
	
	function inactiveAgent($user_id){
		$data = array(
					'user_status' => 'INACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
		$this->General_Model->insert_log('4','inactiveAgent',json_encode($data),'updating  user status to inactive','user_details','user_details_id',$user_id);
	}
	
	function delete_users($user_id){
		$this->db->where('user_id', $user_id);
		$this->db->delete('user'); 
		$this->General_Model->insert_log('4','delete_users',json_encode(array()),'deleting  user details from database','user_details','user_details_id',$user_id);				
	}
	
	function updateAgents($update,$user_id,$user_profile_name){ 
		if(!isset($update['user_status']))
			$update['user_status'] = "INACTIVE";
			
		$update_data_address = array(
								'address' 		=> $update['address'],
								'city_name' 	=> $update['city'],
								'zip_code' 		=> $update['zip_code'],
								'state_name' 	=> $update['state_name'],
								'country_id' 	=> $update['country']					
		 					);
		$this->db->where('address_details_id', $update['address_details_id']);
		$this->db->update('address_details', $update_data_address);
		$this->General_Model->insert_log('4','updateAgents',json_encode($update_data_address),'updating  user address  Details to database','address_details','address_details_id',$update['address_details_id']);
		
		$update_data_user = array(
								'user_type_id' 				=> $update['user_type'][0],
								'company_name' 				=> $update['comapany_name'],
								
								'position'					=> $update['position'],
								'agent_tradingname'			=> $update['agency_tradename'],
								'agent_licenseno'			=> $update['agent_licenseno'],
								'qq'						=> $update['qq'],
								'wechat_number'				=> $update['wechatnumber'],
								'grouptravel'				=> $update['grouptravel'],
								'fit'						=> $update['fit'],
								'user_home_phone' 			=> $update['phone_no'],
								'user_cell_phone' 			=> $update['mobile_no'],					
								'user_profile_pic' 			=> $user_profile_name,					
								'user_status' 				=> $update['user_status'],
								'country_id' 				=> $update['country']				
							);		
		$this->db->where('user_details_id', $user_id);						
		$this->db->update('user_details',$update_data_user);
		$this->General_Model->insert_log('4','updateAgents',json_encode($update_data_user),'updating  user  Details to database','user_details','user_details_id',$user_id);
		
	}
	
	function getDepositList($user_details_id) {
		$this->db->select('*,agent_deposit.branch_id as branchId');
		$this->db->from('agent_deposit');
		$this->db->join('user_details', 'user_details.user_details_id = agent_deposit.user_details_id');
		$this->db->where('user_details.user_type_id', 2);
		if($user_details_id !='')
		$this->db->where('agent_deposit.branch_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function getAgentDepsoiteList($user_details_id) {
		$this->db->select('*');
		$this->db->from('agent_deposit');
		if($user_details_id !='')
			$this->db->where('agent_deposit_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function approveDeposit($user_id,$adminRemarks){
		$data = array(
					'status' => 'Accepted',
					'admin_remarks' => $adminRemarks
					);
		$this->db->where('agent_deposit_id', $user_id);
		$this->db->update('agent_deposit', $data); 
	}

	//~ function insertAgentBalance($user_id,$amount){
		//~ $data = array(
					//~ 'balance_credit' => $amount,
					//~ 'last_credit' => $amount,
					//~ 'agent_id' => $user_id,
					//~ );
		//~ $this->db->insert('b2b_acc_info', $data); 
	//~ }
	//~ 
	function insertAgentBalance($user_id,$amount){
		$data = array(
					'balance_credit' => $amount,
					'last_credit' => $amount,
					'user_id' => $user_id,
					);
		$this->db->insert('user_accounts', $data); 
	}


	//~ function updateAgentBalance($user_id,$amount,$update_balance_amount){
		//~ $data = array(
					//~ 'balance_credit' => $update_balance_amount,
					//~ 'last_credit' => $amount,
					//~ );
		//~ $this->db->where('agent_id', $user_id);
		//~ $this->db->update('b2b_acc_info', $data); 
	//~ }
	
	function updateAgentBalance($user_id,$amount,$update_balance_amount){
		$data = array(
					'balance_credit' => $update_balance_amount,
					'last_credit' => $amount,
					);
		$this->db->where('user_id', $user_id);
		$this->db->update('user_accounts', $data); 
	}


	function update_agent_balance_debit($user_id,$amount,$update_balance_amount){
		$data = array(
					'balance_credit' => $update_balance_amount,
					'last_debit' => $amount,
					);
		$this->db->where('agent_id', $user_id);
		$this->db->update('b2b_acc_info', $data); 
	}
//~ 
	//~ function getAgentDepositBalance($user_id){
		//~ $this->db->select('*');
		//~ $this->db->where('agent_id', $user_id);
		//~ $query = $this->db->get('b2b_acc_info');
		//~ if($query->num_rows() ==''){
			//~ return '';
		//~ }else{
			//~ return $query->row();
		//~ }
	//~ }
	//~ 
	
	function getAgentDepositBalance($user_id){
		$this->db->select('*');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('user_accounts');
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->row();
		}
	}
	
	function cancelDepoist($user_id){
		$data = array(
					'status' => 'Cancelled',
					'admin_remarks' => $adminRemarks
					);
		$this->db->where('agent_deposit_id', $user_id);
		$this->db->update('agent_deposit', $data); 		
	}
	
	function addDepositDetails($input,$exchange,$user_id){	
		$input['product_status'] = "Pending";
		$insert_data = array(
							'user_details_id' 	=> $user_id,
							'user_type_id'      => '2',
							'deposit_type' 		=> $input['deposit_type'],
							'current_exchangerate' => $exchange,
							'amount_credit' 	=> $input['amount_credit'],
							'deposited_date' 	=> (date('Y-m-d H:i:s')),
							'remarks' 			=> $input['remarks'],
							'currencycode'      => $input['currencycode'],
							'status' 			=> $input['product_status']				
						);
		
		$this->db->insert('agent_deposit',$insert_data);
	}

	function add_b2b2b_deposite_details($input,$user_id,$branch_id){	
		$input['product_status'] = "Pending";
		$insert_data = array(
							'user_details_id' 	=> $user_id,
							'user_type_id'      => '2',
							'branch_id'			=> $branch_id,
							'deposit_type' 		=> $input['deposit_type'],
							'amount_credit' 	=> $input['amount_credit'],
							'deposited_date' 	=> (date('Y-m-d H:i:s')),
							'remarks' 			=> $input['remarks'],
							'currencycode'      => $input['currencycode'],
							'status' 			=> $input['product_status']				
						); //print_r($insert_data); exit();
		$this->db->insert('agent_deposit',$insert_data);
	}

	function getAllAgentEmail($user_email) {
		$this->db->select('user_email');
		$this->db->from('user_details');
		if($user_email !='')
		$this->db->where('user_email', $user_email);
		$this->db->where('user_type_id', '2');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return 'NO';
		}else{
			return 'YES';
		}
	}
	  public function export_b2b_users($ids) {
        $this->db->where_in('user_details_id', $ids);
        return $this->db->get('user_details');
}

public function getCountryListAgent(){
		$this->db->select('*')->from('country_details');
		$query = $this->db->get();
	    if ( $query->num_rows > 0 ) {
         return $query->result();
        }
      return false;
   	}

 function currenyAmount($currency_code){
		$this->db->select('*');
		$this->db->from('currency_details');
		$this->db->where('currency_code', $currency_code);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function getAgentModules(){
		$this->db->select('*');
		$this->db->from('homepage_leftmenu');
		$this->db->where_in("user_types", 2);
		$this->db->where('dashboard_status', 'ACTIVE');
		return $this->db->get()->result();
	}
	
	function update_privileges($data, $user_id, $usertype){
		if(count($data['privilege_ids']) > 0){
		
		$this->db->where('user_details_id', $user_id);
		$this->db->where('user_type_id', $usertype);
		$this->db->delete('user_privileges');
		
		for($i=0; $i< count($data['privilege_ids']); $i++){ 
			$post_data= array("dashbaord_id" =>  $data['privilege_ids'][$i],
			                  "user_details_id" => $user_id,
			                  "user_type_id" => $usertype,
			                  "last_updated_by" => $this->session->userdata('provabAdminId'),
			                  "updated_by_flag" => 'A');
			 
			$this->db->insert("user_privileges", $post_data);
		}
	return;	
	}
		
	}
	
	function getagentprivilege($user_id){
		$this->db->select('*');
		$this->db->from('user_privileges');
		$query = $this->db->get();
		return $query->result();
	}

	function getAgentDepositList($user_details_id,$depositid) {
		$this->db->select('*');
		$this->db->from('agent_deposit');
		$this->db->join('user_details', 'user_details.user_details_id = agent_deposit.user_details_id');
		if($user_details_id !='')
		$this->db->where('agent_deposit.user_details_id', $user_details_id);
		$this->db->where('agent_deposit.agent_deposit_id', $depositid);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function getProducts(){
		$this->db->select('*');
		$this->db->from('product_details');
		
		$this->db->where('product_status', '1');
		return $this->db->get()->result();
	}

	function update_product_privileges($data, $user_id){
		if(count($data['privilege_ids']) > 0){
		$this->db->where('user_details_id', $user_id);
		$this->db->delete('user_product_privileges');
		
		for($i=0; $i< count($data['privilege_ids']); $i++){ 
			$post_data= array("product_id" =>  $data['privilege_ids'][$i],
			                  "user_details_id" => $user_id);
			$this->db->insert("user_product_privileges", $post_data);
		}
	return;	
	}
		
	}

	function getAgentListDeposit($user_id = ''){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		if($user_id !='')
			$this->db->where('user_details_id', $user_id);
		//$this->db->where('branch_id', 0 );
		$this->db->where('user_type_id', '2');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			$data['user_info'] = '';
		}else{
			$data['user_info'] = $query->result();
		}
		if($data['user_info']!=''){
			for($u=0;$u<count($data['user_info']);$u++){
				$data['domain_details'][$u] 	= $this->Users_Model->get_domain_details($data['user_info'][$u]->user_details_id);
				$data['user_type_details'][$u] 	= $this->Users_Model->get_user_type_details($data['user_info'][$u]->user_details_id);
				$data['product_details'][$u] 	= $this->Users_Model->get_product_details($data['user_info'][$u]->user_details_id);
				$data['api_details'][$u] 		= $this->Users_Model->get_api_details($data['user_info'][$u]->user_details_id);
				$data['country_details'][$u] 	= $this->Users_Model->get_country_details($data['user_info'][$u]->user_details_id);
			}
		}
		return $data;	
	} 
    
    function get_currenct_balance($user_id){
		return $this->db->get('user_accounts');
	}
	
	function get_booking_balance_history($user_id){
		$this->db->select('*');
		$this->db->from('booking_global');
		$this->db->join('product_details', 'product_details_id = product_id');
		$this->db->join('booking_transaction', 'booking_transaction.booking_transaction_id = booking_global.booking_transaction_id');
		$this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id');
		$this->db->where('branch_id', $user_id);
		$this->db->where('user_type_id', 2);
		$this->db->order_by('booking_global_id', 'desc');
		$query = $this->db->get();
		return $query;
	}
	function domainlist()
	{
		$this->db->select('*');
		$this->db->from('domain_details');
		return $this->db->get()->result();
	}
}
?>
