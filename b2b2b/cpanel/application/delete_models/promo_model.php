<?php
class Promo_Model extends CI_Model {

    function __construct(){
         parent::__construct();
    }

    function getPromoCodeList($promo_code_details_id = ''){
		$this->db->select('*');
		$this->db->from('promo_code_details');
		if($promo_code_details_id !='')
			$this->db->where('promo_code_details_id', $promo_code_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addPromoCodeDetails($input){
    	 $insert_data = $this->form_data($input);
    	$insert_data['promo_added_by'] = $this->session->userdata('provabAdminId');
		$insert_data['promo_created_at'] = date('Y-m-d');
		$this->db->insert('promo_code_details',$insert_data);
		$promo_code_details_id = $this->db->insert_id();
		$this->General_Model->insert_log('3','add_promo_code_details',json_encode($insert_data),'Adding  PromoCode Details to database','promo_code_details','promo_code_details_id',$promo_code_details_id);
	}
	
	function updateStatus($promo_id, $status){
		
		 $sql = "update  promo_code_details set status=".$status." where promo_code_details_id=".$promo_id;
		$this->db->query($sql);
		//~ $data = array(
					//~ 'status' => $status
					//~ );
	    //~ $this->db->where('promo_code_details_id', $promo_id);
		//~ $this->db->update('promo_code_details', $data); 
		//~ echo $this->db->last_query(); exit;
		$this->General_Model->insert_log('3','active_promo',json_encode($data),'updating  PromoCode Details status is changed to '.$status,'promo_code_details','promo_code_details_id',$promo_id);
	}
	
	function delete_promo($promo_code_details_id){
		$this->db->where('promo_code_details_id', $promo_code_details_id);
		$this->db->delete('promo_code_details'); 
		$this->General_Model->insert_log('3','delete_promo',json_encode(array()),'deleting  PromoCode Details from database','promo_code_details','promo_code_details_id',$promo_code_details_id);
	}
	
	function update_promo($update,$promo_code_details_id){
		
		$update_data = $this->form_data($update);
		$update_data['promo_modified_by'] = $this->session->userdata('provabAdminId');
		$update_data['promo_modified_at'] = date('Y-m-d');
		$this->db->where('promo_code_details_id', $promo_code_details_id);
		$this->db->update('promo_code_details', $update_data);
		$this->General_Model->insert_log('3','update_promo',json_encode($update_data),'updating  PromoCode Details to database','promo_code_details','promo_code_details_id',$promo_code_details_id);
	}
	
	function form_data($input) {
		
		if(!isset($input['promo_status']))
			$input['promo_status'] = "INACTIVE";
		 
		if($input['booking_date'] != '') {
			$booking_dates =  explode(" - ", $input['booking_date']);
			 $booking_date_from = date('Y-m-d', strtotime($booking_dates[0]));
			 $booking_date_to = date('Y-m-d', strtotime($booking_dates[1]));
		}	
		
		if($input['travel_date'] != '') {
			$travel_dates =  explode("-", $input['travel_date']);
			$from_travel_date_arr = explode("/", trim($travel_dates[0]));
			$to_travel_date_arr = explode("/", trim($travel_dates[1]));
			 $travelling_date_from = $from_travel_date_arr[2]."-".$from_travel_date_arr[0]."-".$from_travel_date_arr[1];
			 $travelling_date_to = $to_travel_date_arr[2]."-".$to_travel_date_arr[0]."-".$to_travel_date_arr[1];
			//~ $travelling_date_from = date('Y-m-d', strtotime($travel_dates[0]));
			//~ $travelling_date_to = date('Y-m-d', strtotime($travel_dates[1]));
		}	
	  //'exp_date' 				=> date_format(date_create($input['exp_date']), "Y-m-d"),	 
		$insert_data = array(
							'promo_code' 			=> $input['promo_code'],
							'promo_type' 			=> $input['promo_type'],
							'discount' 				=> $input['discount'],
							'creation_date' 		=> (date('Y-m-d H:i:s')),
							'booking_date_range'    => $input['booking_date'],
							'booking_date_from'    => $booking_date_from,
							'booking_date_to'    =>   $booking_date_to,
							'travel_date_range'    => $input['travel_date'],
							'travel_date_from'    => $travelling_date_from,
							'travel_date_to'    => $travelling_date_to,
							'amount_valid_from'    => $input['amount_valid_from'],
							'amount_valid_to'    => $input['amount_valid_to'],
							'status' 				=> $input['promo_status']
			
						);	
			
		 /*if($input['promo_type'] != 'Promo code by %') {
		   $insert_data['promo_amount'] 	= $input['amount'];
		  }	*/
		  
		  if(isset($input['product'])){
		if(count($input['product']) > 0) {
			$product = implode(',',$input['product']);
			$insert_data['product']	        = $product;
		}
	}
	
		return $insert_data;
	}

	function get_user_list()   	{
   
		$this->db->select('user_email')
		->from('user_details');
		$this->db->where('user_type_id', 1);
		$query = $this->db->get();

	      if ( $query->num_rows > 0 ) {
	      
	         return $query->result();
	      }
      		return false;
   }
   
   	function get_agent_list(){
   		$this->db->select('user_email')
		->from('user_details');
		$this->db->where('user_type_id', 2);
		$query = $this->db->get();

	      if ( $query->num_rows > 0 ) {
	      
	         return $query->result();
	      }
      		return false;
   	}

   	function get_all_subscribers(){

   		$this->db->select('email_id')
   		->from('newsletter_subscriptions');
   		$query = $this->db->get();

	      if ( $query->num_rows > 0 ) {
	      
	         return $query->result();
	      }
      		return false;
   	}

   	function get_promo(){
   		$this->db->select('*')
   		->from('promo_code_details');
   		$this->db->where('status','ACTIVE');
   		//$this->db->where('exp_date >=', CURDATE());
   		$query = $this->db->get();

	      if ( $query->num_rows > 0 ) {
	      
	         return $query->result();
	      }
      		return false;
   	}
}
?>
