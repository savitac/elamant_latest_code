<?php
class Supplierdashboard_Model extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    public function get_supplier_details(){
		$lgm_supplier_admin_id = $this->session->userdata('lgm_supplier_admin_id');
		$this->db->select('*')->from('supplier_details')->where('supplier_details.supplier_details_id', $lgm_supplier_admin_id)->where('supplier_details.supplier_status', 'ACTIVE');
		$this->db->join('address_details a', 'a.address_details_id = supplier_details.address_details_id');
		$this->db->join('country_details c', 'c.country_id = a.country_id');
		$query = $this->db->get();
		
		if($query->num_rows() ==''){
			$data['supplier_info'] = '';
		}else{
			$data['supplier_info'] = $query->result();
		}
		
		if($data['supplier_info']!=''){
			$data['supplier_type_info'] = $this->get_supplier_type($data['supplier_info'][0]->supplier_details_id);
		
		}
		
		return $data;	
	}
     
    function get_supplier_type($supplier_details_id)
    {
		$this->db->select('*');
		$this->db->from('supplier_type_management sm');
		$this->db->join('supplier_type st', 'sm.supplier_type_id = st.supplier_type_id');
		$this->db->distinct();
		$this->db->where('supplier_details_id', $supplier_details_id);
		$query=$this->db->get();
	  
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	public function get_admin_logs_activity(){
		$provab_admin_id 	= $this->session->userdata('lgm_supplier_admin_id');
		$this->db->select('*')->from('supplier_login_tracking_details')->where('admin_id',$provab_admin_id)->order_by('login_tracking_details_time_stamp','desc');
		$query = $this->db->get();	
    	if ( $query->num_rows > 0 ){
			return $query->result();	
	    }else{
			return '';	
		}
    }

    public function get_white_list_ip_details($white_list_ip_id =''){
		$admin_id = $this->session->userdata('lgm_supplier_admin_id');
		$this->db->select('*')->from('supplier_white_iplist')->where('admin_id',$admin_id);
		if($white_list_ip_id !='')
			$this->db->where('supplier_white_iplist_id', $white_list_ip_id);
		$query = $this->db->get();	
    	if ( $query->num_rows > 0 ) {
			return $query->result();	
		}else{
			return '';	
		}
	}

	function add_white_list($input){	
		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
		$admin_id = $this->session->userdata('lgm_supplier_admin_id');
		$insert_data = array(
							'supplier_id' 		=> $admin_id,
							'ip_address' 	=> $input['ip_address'],
							'status' 		=> $input['status']
						);
		 try {
		$this->db->insert('supplier_white_iplist',$insert_data);
		$white_list_ip_id = $this->db->insert_id();
		$this->General_Model->insert_log('11','add_white_list',json_encode($insert_data),'Adding  White list Details to database','admin_white_iplist','admin_white_iplist_id',$white_list_ip_id);
	 } catch(Exception $e) {
		 return $e;
	}
	}

	function active_white_list_ip($white_list_ip_id){
		$data = array('status' => 'ACTIVE');
		$this->db->where('admin_white_iplist_id', $white_list_ip_id);
		$this->db->update('admin_white_iplist', $data); 
		$this->General_Model->insert_log('11','active_white_list_ip',json_encode($data),'updating  White list Details to active','admin_white_iplist','admin_white_iplist_id',$white_list_ip_id); 
	}
	
	function inactive_white_list_ip($white_list_ip_id){
		$data = array('status' => 'INACTIVE');
		$this->db->where('admin_white_iplist_id', $white_list_ip_id);
		$this->db->update('admin_white_iplist', $data); 
		$this->General_Model->insert_log('11','inactive_white_list_ip',json_encode($data),'updating  White list Details status to inactive','admin_white_iplist','admin_white_iplist_id',$white_list_ip_id); 
	}
	
	function delete_white_list_ip($white_list_ip_id){
		$this->db->where('admin_white_iplist_id', $white_list_ip_id);
		$this->db->delete('admin_white_iplist'); 
		$this->General_Model->insert_log('11','delete_white_list_ip',json_encode(array()),'deleting  White list Details from database','admin_white_iplist','admin_white_iplist_id',$white_list_ip_id);
	}

	function update_white_list_ip($update,$white_list_ip_id){
		if(!isset($update['status']))
			$update['status'] = "INACTIVE";
		$update_data = array(
							'ip_address' 	=> $update['ip_address'],
							'status' 		=> $update['status']				
						);
		try {
		$this->db->where('admin_white_iplist_id', $white_list_ip_id);
		$this->db->update('admin_white_iplist', $update_data);
		$this->General_Model->insert_log('11','update_white_list_ip',json_encode($update_data),'updating  White list Details  to database','admin_white_iplist','admin_white_iplist_id',$white_list_ip_id);
	} catch(Exception $e) {
		 return $e;
	}
	} 
	
   
}
