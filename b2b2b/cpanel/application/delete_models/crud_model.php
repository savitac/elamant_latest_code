<?php
Class Crud_Model extends CI_Model {	
	public function selectRow($selectColumn, $tableName, $limit='') {
		$this->db->select($selectColumn);
		$this->db->from($tableName);
		if($limit != '') {
			$this->db->limit(1);
		}
		$result = $this->db->get();
		return $result->result();
	}

    public function selectRowWithWhere($selectColumn, $tableName, $whereArray, $limit='') {
		$this->db->select($selectColumn);
		$this->db->from($tableName);
		$this->db->where($whereArray);
		if($limit != '') {
			$this->db->limit(1);
		}
		$result = $this->db->get();
		return $result->result();
    }

    public function selectRowWithoutWhereAndOrderBy($selectColumn, $tableName, $title, $order, $limit='') {
		$this->db->select($selectColumn);
		$this->db->from($tableName);
		$this->db->order_by($title, $order);
		if($limit != '') {
			$this->db->limit(1);
		}
		$result = $this->db->get();
		return $result->result();
    }

    public function selectRowWithWhereAndOrderBy($selectColumn, $tableName, $whereArray, $title, $order, $limit='') {
		$this->db->select($selectColumn);
		$this->db->from($tableName);
		$this->db->where($whereArray);
		$this->db->order_by($title, $order);
		if($limit != '') {
			$this->db->limit(1);
		}
		$result = $this->db->get();
		return $result->result();
    }

    public function selectRowWithLikeAndOrderBy($selectColumn, $tableName, $like_title, $match, $title, $order, $limit='') {
		$this->db->select($selectColumn);
		$this->db->from($tableName);
		$this->db->like($like_title, $match, 'both');
		$this->db->order_by($title, $order);
		if($limit != '') {
			$this->db->limit(1);
		}
		$result = $this->db->get();
		return $result->result();
    }

   
    public function selectRowsWithIn($selectColumn, $tableName, $whereArray, $limit='') {
		$this->db->select($selectColumn);
		$this->db->from($tableName);
		$this->db->where_in('id', $whereArray);
		$result = $this->db->get();
		if($limit != '') {
			$this->db->limit(1);
		}
		return $result->result();
    }
    
    public function dataQuery($sqlQuery) {
        return $this->db->query($sqlQuery);
    }
    
    public function insertRow($tableName, $dataArray) {
		$result = $this->db->insert($tableName, $dataArray);
		return $result;
	}
	public function insertRowReturnId($tableName, $dataArray) {
		$this->db->insert($tableName, $dataArray);
        $result = $this->db->insert_id();
        return $result;
    }
  
   public function update_row_with_where($table, $where_array, $data_array) {
        $this->db->where($where_array);
        $result = $this->db->update($table, $data_array);
        return $result;
    }

    public function updateQuery($tableName, $dataArray, $whereArray) {
		$this->db->where($whereArray);
        $this->db->update($tableName,$dataArray);
    }
    
    public function deleteRowWithWhere($tableName, $whereArray) {
        $this->db->where($whereArray);
        $result=$this->db->delete($tableName);
        return $result;
    }
    
    public function deleteQuery($tableName) {
        $result=$this->db->delete($tableName);
        return $result;
    }
}
?>
