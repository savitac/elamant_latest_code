<?php
class Booking_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }


   function b2BHotelBookings($booking = ''){
		$this->db->select('*');
		$this->db->from('booking_global');
		
			$this->db->where('booking_global.module', 'HOTEL');
			$this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.booking_hotel_id');
			$this->db->where('booking_global.user_type_id','2');
			$this->db->order_by('booking_global.booking_global_id','desc');
		$query=$this->db->get();

		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    public function get_all_order()
    { 
        $this->db->select('*');
        $this->db->from('booking_global');
        $result = $this->db->get();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;
}

    public function get_passanger_details($referal_id)
    {
         
        $this->db->select('*');
        $this->db->where('booking_hotel.booking_hotel_id',$referal_id);

        $result = $this->db->get('booking_hotel');
    $status='';
        if($result->num_rows() > 0 )
        {
            $status = $result->result();
        }
        
        return $status ;

    }

        public function get_apiByid($api_id)
    { 
        $this->db->where('api_details_id',$api_id);
        $result = $this->db->get('api_details');
        if($result->num_rows() > 0 )
        {
            $result = $result->row();
        }
        
        return $result;
    }

	 function getMarkupData($module_type, $ref_id) {
        switch($module_type) {
            
            case "hotel":
                $this->db->where('booking_hotel_id', $ref_id);
                return $this->db->get('booking_hotel');
                break;
            default:
                return false;
                break;
        }
    }

 function get_transaction_details_all($booking_transaction_id)
    {            
        $this->db->where('booking_transaction_id',$booking_transaction_id);
        $result = $this->db->get('booking_transaction');        
        if($result->num_rows() > 0 )
        {           
        return  $result->row();
        }
        
        return '' ;

    }
function get_product_name_byid($product_details_id){
                $this->db->where('product_details_id',$product_details_id);
         $data = $this->db->get('product_details')->row();
         return $data->product_name;
    }

    function currency_convertor($amount){
		// echo 'amount--';$amount;exit;
		if($this->display_currency === CURR){
    		$amount = $amount*1;
    		$float = number_format(($amount) ,3,'.','');
    		////echo $float;exit;
    		return $float;
    		//echo 'hi';die;
    	}else if($this->display_currency === 'AUD'){
    		$amount = ($amount)*($this->curr_val);
    		$float = number_format(($amount),3,'.','');
    		///echo 'dsfdsfds--';$float;exit;
    		return $float;
    	}else{
    		$amount = ($amount)*($this->curr_val);
    		$float=number_format(($amount),3,'.','');
    		//~ echo 'dsfdsfdrtrets--';$float;exit;
    		return $float;
    	}
	}

	function get_user_type_details($user_type_id){
        $this->db->where('user_type_id', $user_type_id);
        $contact = $this->db->get('user_type');
        return $contact->row();
    }

    function getBookingPnr($pnr_no){
        $this->db->where('pnr_no',$pnr_no);
        return $this->db->get('booking_global');
    }
    public function getBookingHotelData($ref_id) {
        $this->db->where('booking_hotel_id', $ref_id);
        return $this->db->get('booking_hotel');
    }

    function getBookingbyPnr($pnr_no,$module){
        if($module == 'HOTEL'){
            $this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.booking_hotel_id');
        }
        $this->db->where('pnr_no',$pnr_no);
        return $this->db->get('booking_global');
    }
    
    public function Update_Booking_Global($booking_no, $update_booking, $module=""){
        $this->db->where('booking_no',$booking_no);
        $this->db->update('booking_global', $update_booking);
    }

    function get_contact(){
        $contact = $this->db->get('general_settings');
        return $contact->row();
    }

    function get_product_details($module){
        $this->db->select('product_details_id');
        $this->db->like('product_name', $module);
        $query=$this->db->get('product_details')->row();
         return $query->product_id;
    }

    public function get_order_details($pnr_no,$user_type_id='')
    {
 
        $this->db->select('booking_global.*,booking_transaction.*,user_details.user_name,user_details.user_email,user_details.user_profile_pic,booking_payment.*,booking_billing_address.*,api_details.api_name,product_details.product_name');
        $this->db->join('booking_transaction','booking_transaction.booking_transaction_id=booking_global.booking_transaction_id','LEFT');
        $this->db->join('user_details','user_details.user_details_id=booking_global.user_id','LEFT');
        $this->db->join('booking_billing_address','booking_billing_address.billing_address_id=booking_global.billing_address_id','LEFT');
        $this->db->join('api_details','api_details.api_details_id=booking_global.api_id','LEFT');
        $this->db->join('booking_payment','booking_payment.payment_id=booking_global.payment_id','LEFT');
        $this->db->join('product_details','product_details.product_details_id=booking_global.product_id','LEFT');
        
        $this->db->join('booking_hotel','booking_hotel.booking_hotel_id','LEFT');
        //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
    
        if($user_type_id!='')       
        {
            $this->db->where('booking_global.user_id',$user_type_id);
        }
        $this->db->where('booking_global.pnr_no',$pnr_no);
        
        $result = $this->db->get('booking_global');
        if($result->num_rows() > 0 )
        {
        return $result->row();
        }
        
        return '' ;
}

    public function get_hotel_booking_details($hotel_id)
    { 
        $this->db->where('booking_hotel.booking_hotel_id',$hotel_id);
    //  $this->db->join('cart_hotel','cart_hotel.cart_id=booking_hotel.shopping_cart_hotel_id','LEFT');
        $result = $this->db->get('booking_hotel');
        if($result->num_rows() > 0 )
        {
            $status = $result->row();
        }
        
        return $status ;

    }

    function get_user_type_list($user_type_id = ''){
        $this->db->select('*');
        $this->db->from('user_type');
        if($user_type_id !='')
            $this->db->where('user_type_id', $user_type_id);

        $query=$this->db->get();
        
        if($query->num_rows() ==''){
            return '';
        }else{
            return $query->result();
        }
    } 

        public function getRefineSearchResult($post){
        
        $this->db->select('booking_global.*,booking_hotel.*,booking_transaction.*,user_details.user_name,user_details.user_email,user_details.user_profile_pic,booking_payment.*,booking_billing_address.*,api_details.api_name,product_details.product_name');
        $this->db->join('booking_transaction','booking_transaction.booking_transaction_id=booking_global.booking_transaction_id','LEFT');
        $this->db->join('user_details','user_details.user_details_id=booking_global.user_id','LEFT');
        $this->db->join('booking_billing_address','booking_billing_address.billing_address_id=booking_global.billing_address_id','LEFT');
        $this->db->join('api_details','api_details.api_details_id=booking_global.api_id','LEFT');
        $this->db->join('booking_payment','booking_payment.payment_id=booking_global.payment_id','LEFT');
        $this->db->join('product_details','product_details.product_details_id=booking_global.product_id','LEFT');
        
        $this->db->join('booking_hotel','booking_hotel.booking_hotel_id','LEFT');
        //print_r($post); exit();
        if ($post['module'] == 'HOTEL') {

        $from =  explode('/', $post['from']);
        $from_date = $from[2].'-'.$from[0].'-'.$from[1];
        $to =  explode('/', $post['to']);
        $to_date = $to[2].'-'.$to[0].'-'.$to[1];
        
        if($post['datetype']!='') {
            if($post['datetype']=='vdate'){
                $this->db->where('booking_global.voucher_date >=', $from_date);
                $this->db->where('booking_global.voucher_date <=', $to_date);
            }
            if($post['datetype']=='tdate'){
                $this->db->where('booking_global.travel_date >', $from_date);
                $this->db->where('booking_global.travel_date <', $to_date);
            }
        }
        if($post['pnr_no'] !='') {
            $this->db->where('booking_global.pnr_no', $post['pnr_no']);
        }
        /* if($post['email'] !='') {
            $email = $this->booking_email($post['email']); echo '<pre>'; print_r($email); exit();
            $this->db->where('booking_global.parent_pnr', $post['email']);
        }*/
       
        if($post['bookingstatus'] !=''){
            $this->db->where('booking_global.booking_status', $post['bookingstatus']);
        }

       
        
        if($post['user_type'] !=''){
            if ($post['user_type'] == '4') {
              $this->db->where('booking_global.user_id', $post['agent'][0]); 
            }
            
        }
        //echo '<pre>'; print_r($post); exit();
        if($post['user_type'] !=''){

            if ($post['user_type'] == '2') {
                if($post['user'][0] !=''){
              $this->db->where('booking_global.user_id', $post['user'][0]); 
            }else {
                $this->db->where('booking_global.user_type_id', '2');
            }
        }
    }
        $this->db->where('booking_global.product_id',1);
        $this->db->order_by('booking_global.booking_global_id','desc');
        
        //$query= $this->db->get();
       //echo $this->db->last_query(); exit();
        //echo "<pre>"; print_r($query->row()); exit;
       // return $query;
    } /*else{
        $from =  explode('/', $post['from']);
        $from_date = $from[2].'-'.$from[0].'-'.$from[1];
        $to =  explode('/', $post['to']);
        $to_date = $to[2].'-'.$to[0].'-'.$to[1];
        
        if($post['datetype']!='') {
            if($post['datetype']=='vdate'){
                $this->db->where('booking_global.voucher_date >=', $from_date);
                $this->db->where('booking_global.voucher_date <=', $to_date);
            }
            if($post['datetype']=='tdate'){
                $this->db->where('booking_global.travel_date >', $from_date);
                $this->db->where('booking_global.travel_date <', $to_date);
            }
        }
        if($post['pnr_no'] !='') {
            $this->db->where('booking_global.pnr_no', $post['pnr_no']);
        }
       
       
        if($post['bookingstatus'] !=''){
            $this->db->where('booking_global.booking_status', $post['bookingstatus']);
        }

       
        //echo '<pre>'; print_r($post['user'][0]); exit();
        if($post['user_type'] !=''){
            if ($post['user_type'] == '4') {
              $this->db->where('booking_global.user_id', $post['agent'][0]); 
            }
            
        }
        //echo '<pre>'; print_r($post['user'][0]); exit();
        if($post['user_type'] !=''){
            if ($post['user_type'] == '2') {
              $this->db->where('booking_global.user_id', $post['user'][0]); 
            }
        }
        $this->db->where('booking_global.product_id',19);
        $this->db->order_by('booking_global.booking_global_id','desc');
        
        
       $this->db->group_by("booking_global.pnr_no");
        $result = $this->db->get('booking_global');
        echo $this->db->last_query($result); exit();
         return $result->result();
        //echo "<pre>"; print_r($query->row()); exit;
        //return $query;
    }*/
         $this->db->group_by("booking_global.pnr_no");
        $result = $this->db->get('booking_global');
        //echo $this->db->last_query(); exit();
         return $result->result();
    }

    public function getRefinetransferSearchResult($post){
        $this->db->select('booking_global.*,booking_transfer.*,booking_transaction.*,user_details.user_name,user_details.user_email,user_details.user_profile_pic,booking_payment.*,booking_billing_address.*,api_details.api_name,product_details.product_name');
        $this->db->join('booking_transaction','booking_transaction.booking_transaction_id=booking_global.booking_transaction_id','LEFT');
        $this->db->join('user_details','user_details.user_details_id=booking_global.user_id','LEFT');
        $this->db->join('booking_billing_address','booking_billing_address.billing_address_id=booking_global.billing_address_id','LEFT');
        $this->db->join('api_details','api_details.api_details_id=booking_global.api_id','LEFT');
        $this->db->join('booking_payment','booking_payment.payment_id=booking_global.payment_id','LEFT');
        $this->db->join('product_details','product_details.product_details_id=booking_global.product_id','LEFT');
        
        $this->db->join('booking_transfer','booking_transfer.booking_transfer_id','LEFT');
        
        $from =  explode('/', $post['from']);
        $from_date = $from[2].'-'.$from[0].'-'.$from[1];
        $to =  explode('/', $post['to']);
        $to_date = $to[2].'-'.$to[0].'-'.$to[1];
        
        if($post['datetype']!='') {
            if($post['datetype']=='vdate'){
                $this->db->where('booking_global.voucher_date >=', $from_date);
                $this->db->where('booking_global.voucher_date <=', $to_date);
            }
            if($post['datetype']=='tdate'){
                $this->db->where('booking_global.travel_date >', $from_date);
                $this->db->where('booking_global.travel_date <', $to_date);
            }
        }
        if($post['pnr_no'] !='') {
            $this->db->where('booking_global.pnr_no', $post['pnr_no']);
        }
       
       
        if($post['bookingstatus'] !=''){
            $this->db->where('booking_global.booking_status', $post['bookingstatus']);
        }

       
        //echo '<pre>'; print_r($post['user'][0]); exit();
        if($post['user_type'] !=''){
            if ($post['user_type'] == '4') {
              $this->db->where('booking_global.user_id', $post['agent'][0]); 
            }
            
        }
       
        if($post['user_type'] !=''){

            if ($post['user_type'] == '2') {
                if($post['user'][0] !=''){
              $this->db->where('booking_global.user_id', $post['user'][0]); 
            }else {
                $this->db->where('booking_global.user_type_id', '2');
            }
        }
    }
        $this->db->where('booking_global.product_id',19);
        $this->db->order_by('booking_global.booking_global_id','desc');
        
        
      
        //echo "<pre>"; print_r($query->row()); exit;
        //return $query;
    
         $this->db->group_by("booking_global.pnr_no");
        $result = $this->db->get('booking_global');
        //echo $this->db->last_query(); exit();
         return $result->result();
    }
    public function getHotelBookingVoucher($pnr_no){

         $this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.booking_hotel_id');
         //$this->db->where('booking_global.parent_pnr_no', $pnr_no);
         //$this->db->where('booking_global.module','HOTEL');
        $query=$this->db->get('booking_global');
        //echo $this->db->last_query();die;
        if($query->num_rows() ==''){
            return '';
        }else{
            return $query->row();
        }
    }

    function get_transaction_details_all_test()
    {            
        //$this->db->where('booking_transaction_id',$booking_transaction_id);
        $result = $this->db->get('booking_transaction');        
        if($result->num_rows() > 0 )
        {           
        return  $result->result();
        }
        
        return '' ;

    }

    public function get_booking_details($user_type_id='')
    {
        /*if($module == 'Hotels'){
            $this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.booking_hotel_id');
        }*/
 
        $this->db->select('booking_global.*,booking_transaction.*,booking_payment.*,booking_billing_address.*,api_details.api_name,product_details.product_name');
        $this->db->join('booking_transaction','booking_transaction.booking_transaction_id=booking_global.booking_transaction_id','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('booking_billing_address','booking_billing_address.billing_address_id=booking_global.billing_address_id','LEFT');
        $this->db->join('api_details','api_details.api_details_id=booking_global.api_id','LEFT');
        $this->db->join('booking_payment','booking_payment.payment_id=booking_global.payment_id','LEFT');
        $this->db->join('product_details','product_details.product_details_id=booking_global.product_id','LEFT');
        $this->db->join('booking_hotel','booking_hotel.booking_hotel_id','LEFT');
        $this->db->where('booking_global.product_id', '1');
        //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
    
        if($user_type_id!='')       
        {
            $this->db->where('booking_global.user_id',$user_type_id);
        }
        
        $this->db->group_by('booking_global.pnr_no');
        $result = $this->db->get('booking_global');
        //echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;
	}

    public function get_booking_hotel_details(){

        $this->db->select('hotel_booking_details.*,hotel_booking_details.app_reference as app_reference_,hotel_booking_itinerary_details.*,hotel_booking_pax_details.*,hotel_booking_pax_details.*,hotel_cancellation_details.*,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('hotel_booking_itinerary_details','hotel_booking_itinerary_details.app_reference=hotel_booking_details.app_reference','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('hotel_booking_pax_details','hotel_booking_pax_details.app_reference=hotel_booking_details.app_reference','LEFT');
        $this->db->join('hotel_cancellation_details','hotel_cancellation_details.app_reference=hotel_booking_details.app_reference','LEFT');
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','hotel_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','hotel_booking_details.user_details_id=user_details.user_details_id');
        $this->db->group_by('hotel_booking_details.app_reference');
        $result = $this->db->get('hotel_booking_details');
        //echo $this->db->last_query(); exit();
        //debug($result->result()); die(); 
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;

    }


     public function get_booking_flight_details($user_type_id='')
    {
         /*if($module == 'Hotels'){
            $this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.booking_hotel_id');
        }*/
 
       $this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('flight_booking_transaction_details','flight_booking_transaction_details.app_reference=flight_booking_details.app_reference','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('flight_booking_passenger_details','flight_booking_passenger_details.app_reference=flight_booking_details.app_reference','LEFT');
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','flight_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','flight_booking_details.user_details_id=user_details.user_details_id');
        $this->db->order_by("flight_booking_details`.`created_datetime", "desc");
        $this->db->group_by('flight_booking_details.app_reference');
        $result = $this->db->get('flight_booking_details');
        #echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        { 
        return $result->result();
        }
        
        return '' ;

    }

         function get_transfer_order_details($user_type_id='')
    {  
        
        $this->db->select('booking_global.*,booking_transfer.*,booking_transaction.*,user_details.user_name,user_details.user_email,user_details.user_profile_pic,booking_payment.*,booking_billing_address.*,api_details.api_name,product_details.product_name');
        $this->db->join('booking_transaction','booking_transaction.booking_transaction_id=booking_global.booking_transaction_id','LEFT');
        $this->db->join('user_details','user_details.user_details_id=booking_global.user_id','LEFT');
        $this->db->join('booking_billing_address','booking_billing_address.billing_address_id=booking_global.billing_address_id','LEFT');
        $this->db->join('api_details','api_details.api_details_id=booking_global.api_id','LEFT');
        $this->db->join('booking_payment','booking_payment.payment_id=booking_global.payment_id','LEFT');
        $this->db->join('product_details','product_details.product_details_id=booking_global.product_id','LEFT');
        $this->db->join('booking_transfer','booking_transfer.booking_transfer_id = booking_global.referal_id');
        //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
       
        $this->db->where('booking_global.product_id', 19);
        if($user_type_id!='')       
        {
            $this->db->where('booking_global.user_id',$user_type_id);
        }
        $result = $this->db->get('booking_global');
            //echo $this->db->last_query(); exit();
         return $result->result();
        
    }

	public function getBookingByParentPnr($parent_pnr){
        $this->db->where('booking_global.parent_pnr_no',$parent_pnr);
		$this->db->join('product_details', 'product_details.product_details_id = booking_global.product_id','LEFT');
		$this->db->join('api_details', 'api_details.api_details_id = booking_global.api_id','LEFT');
		$this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id','LEFT');
        return $this->db->get('booking_global');
    }
    public function getBookingbyPnr_v1($pnr_no,$module){
		 if($module == 'Hotels'){
			$this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.booking_hotel_id');
		}
        if($module == 'Transfer'){
            $this->db->join('booking_transfer','booking_global.referal_id = booking_transfer.booking_transfer_id');
        }
		$this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id','LEFT');
		$this->db->join('booking_transaction', 'booking_transaction.booking_transaction_id = booking_global.booking_transaction_id','LEFT');
	    $this->db->join('booking_supplier', 'booking_supplier.booking_supplier_id = booking_global.booking_supplier_id','LEFT');
			
		$this->db->join('booking_billing_address', 'booking_billing_address.billing_address_id = booking_global.billing_address_id','LEFT');
		
		$this->db->join('user_type', 'user_type.user_type_id = booking_global.user_type_id','LEFT');
		
		
		$this->db->where('booking_global.pnr_no',$pnr_no);
        return $this->db->get('booking_global');
    }
    public function getPassengerbyPnr($pnr_no){
		$this->db->where('booking_global_id',$pnr_no);
        return $this->db->get('booking_passenger');
    }

    function getTransferAirports($aiport_code = ''){
        $this->db->select('*, airport_code as "iata_code" ');
        $this->db->from('flight_airport_list');
        if($aiport_code != ''){
        $this->db->where('airport_code', $aiport_code);
        }
        return $this->db->get();
        
    }

    function getTransferHotels($CityCode ='', $hotel_code = ''){
        $this->db->select('*');
        $this->db->from('gta_Hotels');
        if($CityCode != ''){
        $this->db->where('CityCode', $CityCode);
        }
        if($hotel_code != ''){
        $this->db->where('HotelCode', $hotel_code);
        }
        $query = $this->db->get();
        return $query;
    }

    function getStationsDetails($city_code = '', $station_code =''){
        $this->db->select('*');
        $this->db->from('gta_transfer_station');
        if($city_code != ''){
            $this->db->where('city_code', $city_code);
        }
        if($station_code != ''){
            $this->db->where('station_code', $station_code);
        }
        $query = $this->db->get();
        return $query;
        
    }

    function getPortCities($city_code = ''){
        $this->db->select('*');
        $this->db->from('gta_port_cities');
        if($city_code != ''){
            $this->db->where('city_code', $city_code);
        }
        return $this->db->get();
        
    }

    //Booking Logs
    

    public function get_booking_logs($parent_pnr){
        $this->db->where('booking_global.parent_pnr_no',$parent_pnr);
        $this->db->join('booking_hotel', 'booking_hotel.booking_hotel_id = booking_global.referal_id','LEFT');
        $this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id','LEFT');
        $query = $this->db->get('booking_global');
        return $query->result();
    }

    public function get_transferbooking_logs($parent_pnr){
        $this->db->where('booking_global.parent_pnr_no',$parent_pnr);
        $this->db->join('booking_transfer', 'booking_transfer.booking_transfer_id = booking_global.referal_id','LEFT');
        $this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id','LEFT');
        $query = $this->db->get('booking_global');
        return $query->result();
    }
  
}
