<?php
class General_Model extends CI_Model {
   function __construct(){
         parent::__construct();
        $this->load->model('Settings_Model');
        $this->load->model('Supplierdashboard_Model');
    }

    public function isRegistered($email) {
        $this->db->where('admin_email', $email);
        return $this->db->get('admin_details');
    }

    public function getLogin($email, $admin_id) {
        $this->db->where('admin_user_name', $email);
        $this->db->where('admin_id', $admin_id);
        return $this->db->get('admin_login_details');
    }

    public function updatePwdResetLink($adminid, $key, $secret) {
        $update = array(
            'pwd_reset_random_key' => $key,
            'pwd_reset_secret_key' => $secret
        );
        $this->db->where('admin_id', $adminid);
        return $this->db->update('admin_details', $update);
    }

    public function isvalidSecrect($key, $secret) {
        $this->db->where('pwd_reset_random_key', $key);
        $this->db->where('pwd_reset_secret_key', $secret);
        return $this->db->get('admin_details');
    }

    public function update_agent($password, $email) {
        $update = array(
            'pwd_reset_random_key' => ''
        );
        $this->db->where('admin_email', $email);
        $this->db->update('admin_details', $update);
        $login_update = array(
            'admin_password' => $password
        );
        $this->db->where('admin_user_name', $email);
        $this->db->update('admin_login_details', $login_update);
    }
        
    function getLeftMenuDetails()
    {
		$this->db->select('*');
		$this->db->from('dashboard_module');
		$this->db->where('dashboard_module_status','Active');
		$this->db->order_by('dashboard_module_order');
		$module = $this->db->get();
		if($module->num_rows > 0) 
        {
            $result = $module->result();
            $i= 0;
            foreach($result as $row)
            {
				$this->db->select('*');
				$this->db->from('dashboard_module_details');
				$this->db->where('dashboard_module_id',$row->dashboard_module_id);
				$this->db->where('dashboard_module_details_status','Active');
				$this->db->order_by('dashboard_module_details_order');
				$module_details = $this->db->get();
				$final_result[($row->dashboard_module_id)][0]['dashboard_name']=$row->dashboard_module_name;
				$final_result[($row->dashboard_module_id)][0]['dashboard_name_china']=$row->dashboard_module_name_china;
				$final_result[($row->dashboard_module_id)][0]['dashboard_module_controller']=$row->dashboard_module_controller;
				$final_result[($row->dashboard_module_id)][0]['dashboard_icon']=$row->dashboard_module_icon ;
				$final_result[($row->dashboard_module_id)][0]['dashboard_details']=$module_details->result_array();
				  $i =   $i+1;
			}
		   return $final_result;
        }		
        else
			return '';

	}

	function getLeftMenuPrevdetails(){
		$this->db->select('*');
		$this->db->from('privilege_details');
		$this->db->where('role_details_id',$this->session->userdata('adminRole'));
		$prev = $this->db->get();
		
		if($prev->num_rows > 0) {
			$result_prev = $prev->result(); 
			$i =0;
			foreach($result_prev as $row_prev){
				$this->db->select('*');
				$this->db->from('dashboard_module');
				$this->db->where('dashboard_module_status','Active');
				$this->db->where('dashboard_module_id',$row_prev->dashboard_module_id);
				$this->db->order_by('dashboard_module_order');
				$module = $this->db->get();
				
				if($module->num_rows > 0) {
					$result = $module->result(); 
					foreach($result as $row){
						$this->db->select('dashboard_module_details.*');
						$this->db->from('dashboard_module_details');
						$this->db->join('privilege_details', 'privilege_details.dashboard_module_details_id = dashboard_module_details.dashboard_module_details_id');
						$this->db->where('dashboard_module_details.dashboard_module_id',$row->dashboard_module_id);
                        $this->db->where('privilege_details.role_details_id',$this->session->userdata('adminRole'));
						$this->db->where('dashboard_module_details.dashboard_module_details_status','Active');
						$this->db->order_by('dashboard_module_details.dashboard_module_details_order');
                        $module_details = $this->db->get();
                        $final_result[($row->dashboard_module_id)][$i]['dashboard_name']=$row->dashboard_module_name;
						$final_result[($row->dashboard_module_id)][$i]['dashboard_name_china']=$row->dashboard_module_name_china;
						$final_result[($row->dashboard_module_id)][$i]['dashboard_module_controller']=$row->dashboard_module_controller;
						$final_result[($row->dashboard_module_id)][$i]['dashboard_icon']=$row->dashboard_module_icon ;
						$final_result[($row->dashboard_module_id)][$i]['dashboard_details']=$module_details->result_array();

					}
				}	
			}
		   return $final_result;
		} else return ''; 
	}
	
	
	public function getHomePageSettings() {
		$general_settings =  $this->Settings_Model->get_settings_list(); 
	    
	    if($this->session->userdata('lgm_supplier_admin_logged_in') == "Logged_In") {
			$data['supplier_rights'] = 1 ;
			$data['supplier'] 		= $this->Supplierdashboard_Model->get_supplier_details();
	    }else {
			$data['supplier_rights'] = 0 ;	
	    }

	    if($data['supplier_rights'] == 1 ) {
			$data['admin_id'] =$this->session->userdata('lgm_supplier_admin_id') ;
		} else {
			$data['admin_id'] = $this->session->userdata('provab_admin_id') ;
		}		
		
		if(false){
		$colors 				= explode(",",$general_settings[0]->theme_colors);
		$color_key 				= array_rand($colors, 1);
		$data['color'] 			= $colors[$color_key];
		
		//$transitions 			= array('page-left-in','page-right-in','page-fade','page-fade-only');
		$transitions 			= explode(",",$general_settings[0]->theme_transitions) ;
		$transition_key 		= array_rand($transitions, 1);
		$data['transition'] 	= $transitions[$transition_key];
		
		// $headers				= array('header_left','header_top','header_right');
		$headers				= array('header_left');
		$header_key 		    = array_rand($headers, 1);
		$data['header'] 	    = $headers[$header_key];
	 
		$data['sidebar'] 	    = $general_settings[0]->theme_sidebar;
		}
		//$data['sidebar'] 	    = "sidebar-collapsed";
		$data['sidebar'] 	    = "";
		$transitions 			= array('page-left-in','page-right-in','page-fade','page-fade-only');
		$transition_key 		= array_rand($transitions, 1);
		$data['transition'] 	= $transitions[$transition_key];
		$headers				= array('header_left');
		$header_key 		    = array_rand($headers, 1);
		$data['header'] 	    = $headers[$header_key];
		$headers				= array('header_left','header_top','header_right');
		return $data;
	}
	
	

	public function get_country_details($country_id = '')
	{
		$this->db->select('*');
		$this->db->from('country_details');
		$this->db->where('country_status', 'ACTIVE');
		if($country_id !='')
			$this->db->where('country_id', $country_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	public function get_country()
	{
		$this->db->select('*');
		$this->db->from('country_details');
		$this->db->order_by("country_name", "asc");
		
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function get_state_info($country_id)
	{
		$que="select state_details_id, country_id,state_name from state_details where country_id = '".$country_id."' GROUP BY state_name";
		$query= $this->db->query($que);
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	public function get_currency_list($id = '', $currency = ''){
		$this->db->select('*');
		if($id!=''){
			$this->db->where('currency_details_id',$id);
		} if($currency != ''){
			$this->db->where('currency_code',$currency);
		}
		$query = $this->db->get('currency_details');
		if ( $query->num_rows > 0 ) {
			return $query->result();
		}
		return false;
	}
	
	function get_log_list($user_type="0")
	{
		$this->db->select('log_history.*,dashboard_module.dashboard_module_name,admin_details.admin_name');
		$this->db->from('log_history');
		$this->db->join('dashboard_module','dashboard_module.dashboard_module_id = log_history.module_id');
		$this->db->join('admin_details','admin_details.admin_id = log_history.user_id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return '';
		}
		
	}
	function insert_log($value_id,$activity,$data,$description,$table_name,$table_column_name,$id)
	{
		$log_data['session_id'] = $this->session->userdata('session_id');
		$log_data['ip_address'] = $_SERVER['REMOTE_ADDR'];
		$log_data['user_id'] = $this->session->userdata('provab_admin_id');
		$log_data['module_id'] = $value_id;
		$log_data['function_name'] = $activity;
		$log_data['function_description'] = $description;
		$log_data['data_manipulation'] = $data;
		$log_data['system_browser_info'] =  $_SERVER['HTTP_USER_AGENT'];
		$log_data['system_info'] =  $_SERVER['REMOTE_ADDR'].'||'.$_SERVER['REMOTE_PORT'];
		$log_data['created_date'] = date('Y-m-d H:i:s');
		$log_data['function_reference_name'] = $table_name;
		$log_data['function_reference_field'] = $table_column_name;
		$log_data['function_record_id'] = $id;
		try{
	    $this->begin_transaction();
		$this->db->insert('log_history',$log_data);
		$this->commit_transaction();
	} catch(Exception $e) {
		$this->rollback_transaction();
		return $e;
	}
	}
	
	function encode_data($string){
		$this->load->library('encrypt');
		$encrypted_string = $this->encrypt->encode($string, ADMIN_SECURITY_KEY);
		return ($encrypted_string);
	}
	
	function decode_data($string){
		$this->load->library('encrypt');
		$encrypted_string = $this->encrypt->decode($string, ADMIN_SECURITY_KEY);
		return ($encrypted_string);
	}
	
	function begin_transaction(){
		$this->db->trans_begin();
	}
	
	function commit_transaction(){
		$this->db->trans_commit();
	}
	
	function rollback_transaction(){
		$this->db->trans_rollback();
	}
	
	function upload_image_lgm($image_info, $module, $old_image_name=''){
		
		$image_info_name  = $old_image_name;
	
		if(!empty($image_info['thumb_image']['name'])){	
			if(is_uploaded_file($image_info['thumb_image']['tmp_name'])) {
				if($image_info_name !=''){
					$oldImage = "uploads/".$module."/".$image_info_name;
					unlink($oldImage);
				}
				 
				$image_type = explode("/",$image_info['thumb_image']['type']);
				if($image_type[0] == "image"){				
					$sourcePath = $image_info['thumb_image']['tmp_name'];
					$img_Name	= time().$image_info['thumb_image']['name'];
					$targetPath = "uploads/".$module."/".$img_Name;
					if(move_uploaded_file($sourcePath,$targetPath)){
						$image_info_name = $img_Name;
					}
				}
			}				
		}
		return $image_info_name;
	}
	
	
	
	function upload_image($image_info, $module, $old_image_name=''){
		$image_info_name  = $old_image_name;
		if(!empty($image_info['image_info']['name'])){	
			if(is_uploaded_file($image_info['image_info']['tmp_name'])) {
				if($image_info_name !=''){
					$oldImage = "uploads/".$module."/".$image_info_name;
					//unlink($oldImage);
				}
				 
				$image_type = explode("/",$image_info['image_info']['type']);
				if($image_type[0] == "image"){				
					$sourcePath = $image_info['image_info']['tmp_name']; 
					$img_Name	= time().$image_info['image_info']['name']; 
					$targetPath = "uploads/".$module."/".$img_Name;
					if(move_uploaded_file($sourcePath,$targetPath)){
						$image_info_name = $img_Name;
					}
				}
			}				
		}
		return $image_info_name;
	}
	
	function upload_images_all($image_info, $module, $name, $old_image_name='',$id){
		$user_profile_name  = $old_image_name;
		if(!empty($image_info[$name]['name'])){	
			if(is_uploaded_file($image_info[$name]['tmp_name'][$id])) {
				$image_type = explode("/",$image_info[$name]['type'][$id]);
				if($image_type[0] == "image"){
					if($user_profile_name !=''){
						$oldImage = "uploads/".$module."/".$user_profile_name;
						unlink($oldImage);
					}			
					$sourcePath = $image_info[$name]['tmp_name'][$id];
					$img_Name	= time().$image_info[$name]['name'][$id];
					$targetPath = "uploads/".$module."/".$img_Name;
					if(move_uploaded_file($sourcePath,$targetPath)){
						$user_profile_name = $img_Name;
					}
				}
			}				
		}
		return $user_profile_name;
	}
	
	public function getDefaultSections() {
		$this->db->select('*');
		$this->db->from('default_sections');
		$this->db->where('default_section_status', 1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			 $sections = $query->result_array();
			 for($res = 0; $res < count($sections); $res++) {
				 $sections[$res]['modules'] = $this->getDefaultModule($sections[$res]['default_section_id']);
			 }
		}
		return $sections;
	}
	
	function getDefaultModule($sectionId)
	{
		$this->db->select('*');
		$this->db->from('default_module');
		$this->db->where('default_section_id',$sectionId);
		$this->db->where('default_module_status','Active');
		$this->db->order_by('default_module_order');
		$module_details = $this->db->get();
		return $module_details->result_array();
	}
	
	function getLeftMenuDetailsWorking()
    {
		$this->db->select('*');
		$this->db->from('dashboard_module');
		$this->db->where('dashboard_module_status','Active');
		$this->db->order_by('dashboard_module_order');
		$module = $this->db->get();
		if($module->num_rows > 0) 
        {
            $result = $module->result();
            foreach($result as $row)
            {
				$this->db->select('*');
				$this->db->from('dashboard_module_details');
				$this->db->where('dashboard_module_id',$row->dashboard_module_id);
				$this->db->where('dashboard_module_details_status','Active');
				$this->db->order_by('dashboard_module_details_order');
				$module_details = $this->db->get();
				$final_result[($row->dashboard_module_id)]['dashboard_name']=$row->dashboard_module_name;
				$final_result[($row->dashboard_module_id)]['dashboard_name_china']=$row->dashboard_module_name_china;
				$final_result[($row->dashboard_module_id)]['dashboard_icon']=$row->dashboard_module_icon ;
				$final_result[($row->dashboard_module_id)]['dashboard_details']=$module_details->result_array();
			}
			return $final_result;
        }		
        else
			return '';
	}
	public function get_api_credentials($api){
		$this->db->where('api_name', $api);
		$this->db->where('api_status', 'ACTIVE');
		return $this->db->get('api_details');
	}
	public function get_api($api,$api_usage = ''){
		$this->db->where('api_details_id', $api);
		if($api_usage!=''){
			$this->db->where('api_credential_type', $api_usage);
		} else {
			$this->db->where('api_credential_type', 'TEST');
		}
        $this->db->where('api_status', 'ACTIVE');
        $query = $this->db->get('api_details');
        return $query;
	}
	function store_logs($connection_response,$status,$error_text='')
    {
        $data = array(
                'api_name' => $connection_response['api_id'],
                'xml_type' => $connection_response['xml_type'],
                'xml_request' => $connection_response['request'],
                'xml_response' =>  $connection_response['response'],
				'ip_address' =>  $this->input->ip_address(),
				//'xml_url' => $connection_response['xml_url'],
				'xml_status' =>  $status,
				//'error_text' =>  $error_text
                );
        $this->db->insert('xml_logs', $data);
		return $this->db->insert_id();
    }
    function getCancellationBuffer($module, $api) {
		$this->db->select('*');
		$this->db->from('cancelbuffer');
		if($module != ''){
			$this->db->where('product_id', $module);
		}
		if($api != ''){
			$this->db->where('api_id', $api);
		}
		$this->db->where('buffer_status', 'ACTIVE');
		return $this->db->get();
	}
	public function get_user_details($user_details_id,$user_type){
		$this->db->select('user_login_details.*,user_details.*,user_type.user_type_name,address_details.*,country_details.*');
		$this->db->where('user_login_details.user_details_id', $user_details_id);
		$this->db->where('user_login_details.user_type_id', $user_type);
		$this->db->join('user_details', 'user_details.user_details_id=user_login_details.user_details_id', 'left');
		$this->db->join('user_type', 'user_type.user_type_id=user_login_details.user_type_id', 'left');
		$this->db->join('address_details', 'address_details.address_details_id=user_details.address_details_id', 'left');
		$this->db->join('country_details', 'country_details.country_id=address_details.country_id', 'left');
		
		$this->db->from('user_login_details');
		$query = $this->db->get();   
		
		if ($query->num_rows() > 0) {
			 
			return $query->row();
		}else{
			return false;
		}
	}

	public function currency_convertor($amount,$from,$to){
    	$to = strtoupper($to);
    	$this->db->where('currency_code',$from);
    	$price = $this->db->get('currency_details')->row();
    	$value = $price->value;
    	if($from === $to){
    		$amount = $amount/1; 
    		return number_format(($amount) ,2,'.','');
    	}else{
    		$amount = ($amount)/($value);
    		return number_format(($amount) ,2,'.','');
    	}
    	
    }
    
   
}
?>
