<?php
class Header_Model extends CI_Model {

    function __construct(){
        
        parent::__construct();
    }

    function getHeaderList($header_details_id = ''){
		$this->db->select('*');
		$this->db->from('header_details');
		if($header_details_id !='')
			$this->db->where('header_details_id', $header_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addHeaderDetails($input){
		if(!isset($input['menu_status']))
			$input['menu_status'] = "INACTIVE";
		$insert_data = array(
							'menu_name' 			=> $input['menu_name'],
							'mneu_type' 			=> $input['menu_type'],
							'menu_url' 				=> $input['menu_url'],
							'position' 				=> $input['position'],
							'menu_status' 			=> $input['menu_status'],
							'menu_creation_date'	=> (date('Y-m-d H:i:s'))					
						);		
			
		$this->db->insert('header_details',$insert_data);
	}
   
	function activeHeader($header_id){
		$data = array(
					'menu_status' => 'ACTIVE'
					);
		$this->db->where('header_details_id', $header_id);
		$this->db->update('header_details', $data); 
	}
	
	function inactiveHeader($header_id){
		$data = array(
					'menu_status' => 'INACTIVE'
					);
		$this->db->where('header_details_id', $header_id);
		$this->db->update('header_details', $data); 
	}
	
	function deleteHeader($header_id){
		$this->db->where('header_details_id', $header_id);
		$this->db->delete('header_details'); 
	}
	
	function updateHeaderDetails($update,$api_id){
		if(!isset($update['menu_status']))
			$update['menu_status'] = "INACTIVE";
		$update_data = array(
							'menu_name' 			=> $update['menu_name'],
							'mneu_type' 			=> $update['menu_type'],
							'menu_url' 				=> $update['menu_url'],
							'position' 				=> $update['position'],
							'menu_status' 			=> $update['menu_status']			
						);
		$this->db->where('header_details_id', $api_id);
		$this->db->update('header_details', $update_data);
	}
	
    //Products
	function getHeaderProductList($header_details_id = ''){
		$this->db->select('*');
		$this->db->from('header_product');
		if($header_details_id !='')
			$this->db->where('header_product_id', $header_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addHeaderProductDetails($input,$product_image){
		if(!isset($input['menu_status']))
			$input['menu_status'] = "INACTIVE";
		$insert_data = array(
							'user_type'             => $input['user_type'],
							'agent_id'				=> $input['agent'],
							'product_name' 			=> $input['menu_name'],
							'product_name_china' 			=> $input['menu_name_china'],
							'header_id' 			=> $input['headerid'],
							'product_description'   => $input['product_description'],
							'product_description_china'   => $input['product_description_china'],
							'position' 				=> $input['position'],
							'status' 				=> $input['menu_status'],
							'product_image'			=> $product_image,
							'creation_date'			=> (date('Y-m-d H:i:s'))
						);		
			//echo '<pre>'; print_r($insert_data); exit();
		$this->db->insert('header_product',$insert_data);
	}

	function get_users_name($agent_id){
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('user_details_id', $agent_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function inactiveHeaderProduct($header_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('header_product_id', $header_id);
		$this->db->update('header_product', $data); 
	}

	function activeHeaderProduct($header_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('header_product_id', $header_id);
		$this->db->update('header_product', $data); 
	}

	function deleteHeaderProduct($header_id){
		$this->db->where('header_product_id', $header_id);
		$this->db->delete('header_product'); 
	}

	function updateProduct($update,$image_info_name, $header_id){
		if(!isset($update['menu_status']))
			$update['menu_status'] = "INACTIVE";
		$update_data = array(
							'product_name' 			=> $update['menu_name'],
							'product_name_china' 			=> $update['menu_name_china'],
							'product_description' 	=> $update['product_description'],
							'product_description_china' 	=> $update['product_description_china'],
							'product_image' 		=> $image_info_name,
							'position' 				=> $update['position'],
							'status' 				=> $update['menu_status'],
							'updation_date'			=> 	(date('Y-m-d H:i:s'))	
						);
		$this->db->where('header_product_id', $header_id);
		$this->db->update('header_product', $update_data);
	}

	//Services

	function getHeaderServiceList($header_service_id = ''){
		$this->db->select('*');
		$this->db->from('header_service');
		if($header_service_id !='')
			$this->db->where('header_service_id', $header_service_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function addHeaderServiceDetails($input,$product_image){
		if(!isset($input['menu_status']))
			$input['menu_status'] = "INACTIVE";
		$insert_data = array(
							'user_type'             => $input['user_type'],
							'agent_id'				=> $input['agent'],
							'service_name' 			=> $input['menu_name'],
							'service_name_china' 			=> $input['menu_name_china'],
							'header_id' 			=> $input['headerid'],
							'service_description'   => $input['service_description'],
							'service_description_china'   => $input['service_description_china'],
							'position' 				=> $input['position'],
							'status' 				=> $input['menu_status'],
							'service_image'			=> $product_image,
							'creation_date'			=> (date('Y-m-d H:i:s'))
						);		
			//echo '<pre>'; print_r($insert_data); exit();
		$this->db->insert('header_service',$insert_data);
	}



	function inactiveHeaderService($header_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('header_service_id', $header_id);
		$this->db->update('header_service', $data); 
	}

	function activeHeaderService($header_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('header_service_id', $header_id);
		$this->db->update('header_service', $data); 
	}

	function deleteHeaderService($header_id){
		$this->db->where('header_service_id', $header_id);
		$this->db->delete('header_service'); 
	}

	function updateService($update,$image_info_name, $header_id){
		if(!isset($update['menu_status']))
			$update['menu_status'] = "INACTIVE";
		$update_data = array(
							'service_name' 			=> $update['menu_name'],
							'service_description' 	=> $update['service_description'],
							'service_name_china' 			=> $update['menu_name_china'],
							'service_description_china' 	=> $update['service_description_china'],
							'service_image' 		=> $image_info_name,
							'position' 				=> $update['position'],
							'status' 				=> $update['menu_status'],
							'updation_date'			=> 	(date('Y-m-d H:i:s'))	
						);
		$this->db->where('header_service_id', $header_id);
		$this->db->update('header_service', $update_data);
	}

	//About-Us
	function getHeaderaboutus($aboutus_id = ''){
		$this->db->select('*');
		$this->db->from('header_aboutus');
		if($aboutus_id !='')
			$this->db->where('headerabout_id', $aboutus_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function addHeaderAboutus($input,$product_image){
		if(!isset($input['menu_status']))
			$input['menu_status'] = "INACTIVE";
		$insert_data = array(
							'user_type'             => $input['user_type'],
							'agent_id'				=> $input['agent'],
							'about_name' 			=> $input['menu_name'],
							'about_name_china' 		=> $input['menu_name_china'],
							'header_id' 			=> $input['headerid'],
							'about_description'   	=> $input['about_description'],
							'about_description_china'  => $input['about_description_china'],
							'position' 				=> $input['position'],
							'status' 				=> $input['menu_status'],
							'service_images'			=> $product_image,
							'sub_aboutus'			=> $input['sub_aboutus'],
							'creation_date'			=> (date('Y-m-d H:i:s'))
						);		
			//echo '<pre>'; print_r($insert_data); exit();
		$this->db->insert('header_aboutus',$insert_data);
	}

	function getHeaderManageaboutus($aboutus_id = ''){
		$this->db->select('*');
		$this->db->from('header_manageaboutus');
		if($aboutus_id !='')
			$this->db->where('header_id', $aboutus_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function addHeaderManageAboutus($input,$product_image){
		if(!isset($input['menu_status']))
			$input['menu_status'] = "INACTIVE";
		$insert_data = array(
							
							'aboutmanage_name' 		=> $input['menu_name'],
							'aboutmanage_name_china' 		=> $input['menu_name_china'],
							'header_id' 			=> $input['headerid'],
							'aboutmanage_description' => $input['about_description'],
							'aboutmanage_description_china' => $input['about_description_china'],
							'position' 				=> $input['position'],
							'status' 				=> $input['menu_status'],
							'service_image'			=> $product_image,
							
							'creation_date'			=> (date('Y-m-d H:i:s'))
						);		
			//echo '<pre>'; print_r($insert_data); exit();
		$this->db->insert('header_manageaboutus',$insert_data);
	}

	function getHeadercontact($contactid = ''){
		$this->db->select('*');
		$this->db->from('header_contact');
		if($contactid !='')
			$this->db->where('header_contact_id', $contactid);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function addHeaderContact($input){
		if(!isset($input['menu_status']))
			$input['menu_status'] = "INACTIVE";
		$insert_data = array(
							'user_type' => $input['user_type'],
							'agent_id' 	=> $input['agent'],
							'city' 		=> $input['city'],
							'header_id' => $input['headerid'],
							'address' 	=> $input['address'],
							'address_china' 	=> $input['address_china'],
							'position' 	=> $input['position'],
							'status' 	=> $input['menu_status'],
							'contact'	=> $input['Contact'],
							'creation_date'			=> (date('Y-m-d H:i:s'))
						);		
			//echo '<pre>'; print_r($insert_data); exit();
		$this->db->insert('header_contact',$insert_data);
	}

	function inactiveHeaderContact($header_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('header_contact_id', $header_id);
		$this->db->update('header_contact', $data); 
	}

	function activeHeaderContact($header_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('header_contact_id', $header_id);
		$this->db->update('header_contact', $data); 
	}

	function deleteHeaderContact($header_id){
		$this->db->where('header_contact_id', $header_id);
		$this->db->delete('header_contact'); 
	}

	function updateContact($update, $header_id){
		if(!isset($update['menu_status']))
			$update['menu_status'] = "INACTIVE";
		$update_data = array(
							'city' 					=> $update['city'],
							'address' 				=> $update['address'],
							'address_china' 		=> $update['address_china'],
							'contact' 				=> $update['Contact'],
							'position' 				=> $update['position'],
							'status' 				=> $update['menu_status'],
							'updation_date'			=> 	(date('Y-m-d H:i:s'))	
						);
		$this->db->where('header_contact_id', $header_id);
		$this->db->update('header_contact', $update_data);
	}

	function inactiveHeaderaboutus($header_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('headerabout_id', $header_id);
		$this->db->update('header_aboutus', $data); 
	}

	function activeHeaderaboutus($header_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('headerabout_id', $header_id);
		$this->db->update('header_aboutus', $data); 
	}

	function deleteHeaderaboutus($header_id){
		$this->db->where('headerabout_id', $header_id);
		$this->db->delete('header_aboutus'); 
	}

	function updateHeaderaboutus($update, $header_id){ 
		if(!isset($update['menu_status']))
			$update['menu_status'] = "INACTIVE";
		$update_data = array(
							'about_name' 			=> $update['menu_name'],
							'about_name_china' 			=> $update['menu_name_china'],
							'about_description' 	=> $update['service_description'],
							'about_description_china' 	=> $update['about_description_china'],
							'position' 				=> $update['position'],
							'status' 				=> $update['menu_status'],
							'updation_date'			=> 	(date('Y-m-d H:i:s'))	
						);
		$this->db->where('headerabout_id', $header_id);
		$this->db->update('header_aboutus', $update_data);
	}

	//Manageabout
	function inactiveHeaderManageaboutus($header_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('header_manageaboutus_id', $header_id);
		$this->db->update('header_manageaboutus', $data); 
	}

	function activeHeaderManageaboutus($header_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('header_manageaboutus_id', $header_id);
		$this->db->update('header_manageaboutus', $data); 
	}

	function deleteHeaderManageaboutus($header_id){
		$this->db->where('header_manageaboutus_id', $header_id);
		$this->db->delete('header_manageaboutus'); 
	}

	function getHeaderManageabout($aboutus_id = ''){
		$this->db->select('*');
		$this->db->from('header_manageaboutus');
		if($aboutus_id !='')
			$this->db->where('header_manageaboutus_id', $aboutus_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function updateHeaderManageaboutus($update, $header_id){ //echo '<pre>'; print_r($update); exit();
		if(!isset($update['menu_status']))
			$update['menu_status'] = "INACTIVE";
		$update_data = array(
							'aboutmanage_name' 		=> $update['menu_name'],
							'aboutmanage_name_china' 		=> $update['menu_name_china'],
							'aboutmanage_description' => $update['service_description'],
							'aboutmanage_description_china' => $update['service_description_china'],
							'position' 				=> $update['position'],
							'status' 				=> $update['menu_status'],
							'updation_date'			=> 	(date('Y-m-d H:i:s'))	
						);
		$this->db->where('header_manageaboutus_id', $header_id);
		$this->db->update('header_manageaboutus', $update_data);
	}
	//EnD Manageabout
}
?>
