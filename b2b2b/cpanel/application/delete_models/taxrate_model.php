<?php
class Taxrate_Model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    } 
    
    function get_tax_rate_list($tax_rate_info_id = '', $taxrate){
		$this->db->select('*');
		$this->db->from('tax_rate_info');
		//~ $this->db->where('added_by', $this->session->userdata('lgm_supplier_id'));
		if($tax_rate_info_id !='')
			$this->db->where('tax_rate_info_id', $tax_rate_info_id);
		$query=$this->db->get();

		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function add_tax_rate($input){	
		
		if(!isset($input['status']))
			$input['status'] = "ACTIVE";	
		
		//$date_range[0] = $date_range[1] = '';
		//if(isset($input['date_rane_rate']) && $input['date_rane_rate']!='')
			//$date_range = explode("-",$input['date_rane_rate']);
		$insert_data = array(
							//'from_date' 				=> trim(date('Y-m-d', strtotime($date_range[0]))),
							//'to_date' 					=> trim(date('Y-m-d', strtotime($date_range[1]))),
							'country' 						=> $input['country'][0],
							'state_id'			=> $input['state_name'],
							'gst' 						=> $input['gst'],
							'service_charge' 						=> $input['service_charge'],
							'tax' 			=> $input['tax'],
							//'vat' 				=> $input['vat'],
							'status' 					=> $input['status'],
							'creation_date'				=> (date('Y-m-d H:i:s'))	
						);
	    $insert_data['added_by'] =$this->session->userdata('provabAdminId') ;
		
		try{		
		$this->db->insert('tax_rate_info',$insert_data);
		$tax_rate_info_id = $this->db->insert_id();
		$this->General_Model->insert_log('12','add_tax_rate',json_encode($insert_data),'Adding  Tax to database','tax_rate_info','tax_rate_info_id',$tax_rate_info_id);
	} catch(Exception $e) {
		return $e;
	}
	}
	
	function inactive_taxrate($tax_rate_info_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('tax_rate_info_id', $tax_rate_info_id);
		$this->db->update('tax_rate_info', $data);
		$this->General_Model->insert_log('12','inactive_taxrate',json_encode($data),'updating Tax status to inactive','tax_rate_info','tax_rate_info_id',$tax_rate_info_id);
	}
	
	function active_taxrate($tax_rate_info_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('tax_rate_info_id', $tax_rate_info_id);
		$this->db->update('tax_rate_info', $data);
		$this->General_Model->insert_log('12','inactive_data',json_encode($data),'updating Tax status to active','tax_rate_info','tax_rate_info_id',$tax_rate_info_id);
	}
	function delete_taxrate($tax_rate_info_id){
		$this->db->where('tax_rate_info_id', $tax_rate_info_id);
		$this->db->delete('tax_rate_info');
		$this->General_Model->insert_log('12','active_taxrate',json_encode(array()),'deleting  Tax from database','tax_rate_info','tax_rate_info_id',$tax_rate_info_id);
	}
	
	function update_tax_rate($input,$tax_rate_info_id){		
		
			
		if(!isset($input['status']))
			$input['status'] = "ACTIVE";	
		
		$date_range[0] = $date_range[1] = '';
		if(isset($input['date_rane_rate']) && $input['date_rane_rate']!='')
			$date_range = explode("-",$input['date_rane_rate']);
					
		$insert_data =  array(
							//'from_date' 				=> trim(date('Y-m-d', strtotime($date_range[0]))),
							//'to_date' 					=> trim(date('Y-m-d', strtotime($date_range[1]))),
							'gst' 						=> $input['gst'],
							'service_charge' 						=> $input['service_charge'],
							'tax' 			=> $input['tax'],
							//'vat' 				=> $input['vat'],
							'status' 					=> $input['status'],
							'creation_date'				=> (date('Y-m-d H:i:s'))	
						);	
		$insert_data['added_by'] =$this->session->userdata('lgm_supplier_id') ;		
		try{			
		$this->db->where('tax_rate_info_id', $tax_rate_info_id);
		$this->db->update('tax_rate_info', $insert_data);
		$this->General_Model->insert_log('12','update_tax_rate',json_encode($insert_data),'updating  Tax house offer to database','tax_rate_info','tax_rate_info_id',$tax_rate_info_id);
	} catch(Exception $e) {
		return $e;
	}
	}

	 public function getCountry($query){
        $this->db->distinct();
        $this->db->like('country_name', $query); 
        $this->db->limit(8);
        return $this->db->get('country_details');
    }

    public function getstateName($data){
       
        $this->db->like('country_name', $data); 
        
        return $this->db->get('states');
    }

    public function getstates($data){
       echo 'sanjay'; print_r($data);
        $this->db->where('State_id',$data);
        
        return $this->db->get('states');
    }
	
}
