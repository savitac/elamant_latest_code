<?php 
error_reporting(E_ALL);	
class Domain_Model extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    } 
	
	function get_domain_list($domain_id = ''){
		/*$this->db->select('domain_details_id,domain_name,domain_url,domain_logo,ddelete_domainomain_status');
		$this->db->from('domain_details');
		if($domain_id !='')
			$this->db->where('domain_details_id', $domain_id);
			
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}*/
		$sql = '';
		if($domain_id != ''){
			$sql =" where t1.domain_details_id =".$domain_id; 
		}
		
		 $sql = "select t1.*,t2.company_name from domain_details as t1 
		        join 
		        user_details as t2 
		        on 
		        t1.agent_id=t2.user_details_id".$sql;
		
		$result=$this->db->query($sql)->result_array();
		return $result;
		
		
	}

	function getAllDomains($domain) {
		$this->db->select('user_email');
		$this->db->from('domain_details');
		if($domain !='')
		$this->db->where('user_email', $domain);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return 'NO';
		}else{
			return 'YES';
		}
	}
	function add_domain($input,$domain_logo_name){
		$insert_data = array(
							'domain_name' 			=> $input['domain_name'],
							/*'access_key' 			=> $input['access_key'],
							'working_key' 			=> $input['working_key'],*/
							'domain_logo' 			=> $domain_logo_name,
							'domain_status' 		=> $input['domain_status'],
							'agent_id'              => $input['agent_name'],
							'domain_user_name'            => $input['user_name'],
							'domain_password'             => $input['password'],
							'domain_host'             => $input['domain_host'],
							'domain_port'             => $input['domain_port'],
							'required_access'             => implode(",", $input['access']),
							'domain_creation_date'	=> (date('Y-m-d H:i:s'))					
						);		
	   
		try{
		$this->db->insert('domain_details',$insert_data);

		$domain_id = $this->db->insert_id();

		$data=array('domain_list_fk' => $domain_id,);
		$check=$this->db->where('user_details_id',$input['agent_name']);
		$this->db->update('user_details',$data);
		$this->General_Model->insert_log('1','add_domain',json_encode($insert_data),'Adding  Domain Details to database','domain_details','domain_details_id',$domain_id);
	} catch(Exception $e) {
		return $e;
	}
	}
	
	function active_domain($domain_id){
		$data = array(
					'domain_status' => 'ACTIVE'
					);
		$this->db->where('domain_details_id', $domain_id);
		$this->db->update('domain_details', $data); 
		$this->General_Model->insert_log('1','active_domain',json_encode($data),'updating  Domain Details to active','domain_details','domain_details_id',$domain_id); 
	}
	
	function inactive_domain($domain_id){
		$data = array(
					'domain_status' => 'INACTIVE'
					);
		$this->db->where('domain_details_id', $domain_id);
		$this->db->update('domain_details', $data); 
		$this->General_Model->insert_log('1','inactive_domain',json_encode($data),'updating  Domain Details status to inactive','domain_details','domain_details_id',$domain_id);  
	}
	
	function delete_domain($domain_id){
		$this->db->where('domain_details_id', $domain_id);
		$this->db->delete('domain_details'); 
		$this->General_Model->insert_log('1','delete_domain',json_encode(array()),'deleting  Domain Details from database','domain_details','domain_details_id',$domain_id);
	}
	
	function update_domain($update,$domain_id, $image_info_name){
		/*echo $domain_id;
		echo "<pre>";
		print_r($update);
*/		$update_data= array(
							'required_access' 			=> implode(",",$update['required_access']),
							'domain_logo' 			=> $image_info_name,
						);
		try{
			$this->db->query('Update domain_details SET domain_name = "'.$update['domain_name'].'", domain_user_name = "'.$update['domain_user_name'].'", domain_password = "'.$update['domain_password'].'",domain_host = "'.$update['domain_host'].'",domain_port = "'.$update['domain_port'].'", required_access = "'.$update_data['required_access'].'",domain_status="'.$update['domain_status'].'",domain_logo = "'.$update_data['domain_logo'].'" WHERE domain_details_id = "'.$domain_id.'"');
		$this->General_Model->insert_log('1','update_domain',json_encode($update),'updating  Domain Details  to database','domain_details','domain_details_id',$domain_id);
	} catch(Exception $e) {
		return $e;
	}
	}
	
	   function get_B2B_users(){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('user_type_id', 2);
		$this->db->where('user_status', 'ACTIVE');
		$this->db->where('branch_id', 0);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}

		//echo $this->db->last_query(); exit();
	}
	
	   function get_b2b_name_acc_no($user_details_id){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}

		//echo $this->db->last_query(); exit();
	}
	
	
		
}
?>
