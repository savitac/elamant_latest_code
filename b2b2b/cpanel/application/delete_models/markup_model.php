<?php
class Markup_Model extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }   
	
	function get_markup_list($markup_details_id = ''){		 
		$this->db->select('*');
		$this->db->from('markup_details m');
		
		$this->db->join('api_details a', 'a.api_details_id = m.api_details_id');
		$this->db->join('product_details p', 'p.product_details_id = m.product_details_id');
        $this->db->join('country_details c', 'c.country_id = m.country_id','LEFT');
        $this->db->join('user_details u', 'u.user_details_id = m.user_details_id','LEFT');
		
		if($markup_details_id !='')
			$this->db->where('markup_details_id', $markup_details_id);
		$query=$this->db->get();
        if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	public function add_markup($input) { 
		 if (!isset($input['country'])) {
            $input['country'] = array(0 => 0);
            $cloop = 0;
        }
        else {
			
			$cloop = count($input['country']);
		}
		
		  if (!isset($input['markup_status'])) {
            $input['markup_status'] = "INACTIVE";
        }       

        if (!isset($input['markup_value'])) {
            $input['markup_value'] = "0";
        }       

        if (!isset($input['markup_fixed'])) {
            $input['markup_fixed'] = "0";
        }       

        if (!isset($input['api'])) {
            $input['api'] = " ";
        }
        if ($input['markup_type'] == 'GENERAL') {
            $insert_markup_data = array(
                            'user_type_id'              => $input['user_type'],
                            'product_details_id'        => $input['product'],
                            'api_details_id'            => '3',
                            'markup_type'               => $input['markup_type'],
                            'markup_value_type'         => $input['markup_value_type'],
                            'markup_value'              => round($input['markup_value'],2),
                            'markup_fixed'              => round($input['markup_fixed'],2),
                            'markup_status'             => $input['markup_status'],
                            'markup_created_date'       => date('Y-m-d H:i:s'),
                        );
                    
                        $this->insert_update_markup($insert_markup_data); 
        }
        if ($input['markup_type'] == 'SPECIFIC') {
             
            if(isset($input['agent']) && $input['agent'] != "") { 
               //$agent =  $input['agent']=array('0');
                 $agent = $input['agent'][0]; 
            } else{
                $agent = '0';
            }
            
            if(isset($input['airline']) && $input['airline'] != "") { 
               //$agent =  $input['agent']=array('0');
                 $airline = implode(",", $input['airline']); 
            } else{
                $airline = '0';
            }   
            $insert_markup_data = array(
                            'user_type_id'               => $input['user_type'],
                            'user_details_id'            => $agent,
                            'airline_details_code'       => $airline,
                            'api_details_id'             => '3',
                            'product_details_id'         => $input['product'],
                            'markup_type'                => $input['markup_type'],
                            'country_id'                 => $input['country'][0] ,
                            'markup_value_type'          => $input['markup_value_type'],
                            'markup_value'               => round($input['markup_value'],2),
                            'markup_fixed'               => round($input['markup_fixed'],2),
                            'markup_status'              => $input['markup_status'],
                            'markup_created_date'        => date('Y-m-d H:i:s'),
                         ); 

             //echo '<pre>';print_r($insert_markup_data);exit();  
            $this->insert_update_markup($insert_markup_data); 
        }
        /*if ($input['user_type'] == 2) { //B2B          
			  
            if(in_array(0,$input['agent'])) { // All user
                $input['agent']=array('0');
            } 
            
            if(isset($input['markup_date_range']) && $input['markup_date_range'] != "") {
				$date_range = explode("-", $input['markup_date_range']);
				$from_date = date('Y-m-d', trim(strtotime($date_range[0])));
				$to_date = date('Y-m-d', trim(strtotime($date_range[1])));
			}else{
				$from_date ="0000-00-00";
				$to_date = "0000-00-00";
			}
			
			 if(isset($input['exp_date']) && $input['exp_date'] != "") {
				$exp_date = date('Y-m-d', trim(strtotime($input['exp_date'])));
			
			}else{
				$exp_date ="0000-00-00";
			}
			
                for ($cloop = 0; $cloop < sizeof($input['agent']); $cloop++) {   
                        $insert_markup_data = array(
                            'user_type_id' 				=> $input['user_type'],
                            'user_details_id' 			=> $input['agent'][$cloop],
                            'product_details_id' 		=> $input['product'],
                            'api_details_id' 			=> $input['api'],
                            'markup_type' 				=> $input['markup_type'],
                            'country_id' 				=> $input['country'][$cloop],
                            'markup_value_type'         => $input['markup_value_type'],
                            'markup_value' 				=> round($input['markup_value'],2),
                            'markup_fixed' 				=> round($input['markup_fixed'],2),
                            'markup_status' 					=> $input['markup_status'],
                            'markup_created_date' 		=> date('Y-m-d H:i:s'),
                            'markup_start_date' 		=>$from_date,
                            'markup_end_date' 		=> $to_date,
                            'markup_expiry_date' 		=> $exp_date,
                            
                        );
                     
                        $this->insert_update_markup($insert_markup_data);                                       
                }
           //
        }*/elseif ($input['user_type'] == 5) { //Supplier          
			  
            if(in_array(0,$input['supplier_user'])) { // All user
                $input['supplier_user']=array('0');
            }       
            
            
            for ($uloop = 0; $uloop < sizeof($input['supplier_user']); $uloop++) {
                for ($cloop = 0; $cloop < sizeof($input['country']); $cloop++) {      
					    
					    if($input['country'][$cloop]!=1) {
							$input['markup_type'] 	= 'SPECIFIC';
							$country_id				= $input['country'][$cloop];
						} 
						else {
							$input['markup_type'] 	= 'GENERAL'; 
							$country_id				= 1;
						}
					               
                        $insert_markup_data = array(
                            'user_type_id' 				=> $input['user_type'],
                            'user_details_id' 			=> $input['supplier_user'][$uloop],
                            //'domain_details_id' 		=> 3, //$input['domain'][$d],
                            'product_details_id' 		=> $input['product'],
                            'api_details_id' 			=> $input['api'],
                           // 'supplier_details_id' 		=> $suppliers,
							//'hotel_details_id' 			=> $hotels,						
                            'markup_type' 				=> $input['markup_type'],
                            //'markets' 					=> $markets,
                            //'country_id' 				=> $input['country'][$cloop],
                            'country_id' 				=> $country_id,
                            //'airline_details_id' 		=> 2,
                            'markup_value' 				=> round($input['markup_value'],2),
                            'markup_fixed' 				=> round($input['markup_fixed'],2),
                            'markup_status' 					=> trim($input['markup_status']),
                            'markup_created_date' 		=> date('Y-m-d H:i:s'),
                            'markup_added_by_mgmt'		=> $this->session->userdata('provab_admin_id')
                        );
                        $this->insert_update_markup($insert_markup_data);                                       
                }//cloop                            
            }//uloop 
        }//else   
        
        
    }

    function insert_update_markup($input){
          $this->db->insert('markup_details', $input);
          $markup_id = $this->db->insert_id();
          $this->General_Model->insert_log('6', 'add_markup', json_encode($input), 'Adding  Markup to database', 'markup_details', 'markup_details_id', $markup_id);
      
    
    }
 function update_markup($update, $markup_id) {

		
         if (!isset($update['country'])) {
            $update['country'] = array(0 => 0);
            $cloop = 0;
        }
        else {
            
            $cloop = count($update['country']);
        }
      
       if (!isset($update['markup_status'])) {
            $update['markup_status'] = "INACTIVE";
        }       

        if (!isset($update['markup_value'])) {
            $update['markup_value'] = "0";
        }       

        if (!isset($update['markup_fixed'])) {
            $update['markup_fixed'] = "0";
        }       

        if (!isset($update['api'])) {
            $update['api'] = " ";
        }
        if ($update['markup_type'] == 'GENERAL') {
            $insert_markup_data = array(
                            
                            'product_details_id'        => $update['product'],
                            'api_details_id'            => '3',
                            'markup_type'               => $update['markup_type'],
                            'markup_value_type'         => $update['markup_value_type'],
                            'markup_value'              => round($update['markup_value'],2),
                            'markup_fixed'              => round($update['markup_fixed'],2)
                            //'markup_status'             => $update['markup_status'],
                            //'markup_created_date'       => date('Y-m-d H:i:s'),
                        );
                   /*echo '<pre>'; print_r($insert_markup_data); exit;*/
                           $this->updateActive_markup($insert_markup_data, $markup_id); 
        }
        /*exit;*/
        if ($update['markup_type'] == 'SPECIFIC') {

            if(isset($update['agent']) && $update['agent'] != "") { 
               //$agent =  $input['agent']=array('0');
                 $agent = $update['agent'][0]; 
            } else{
                $agent = '0';
            }

          
            $insert_markup_data = array(
                           
                           // 'user_details_id'           => $agent,
                            'product_details_id'        => $update['product'],
                            'api_details_id'            => '3',
                            'markup_type'               => $update['markup_type'],
                            'country_id'                => $update['country'][0] ,
                            'markup_value_type'         => $update['markup_value_type'],
                            'markup_value'              => round($update['markup_value'],2),
                            'markup_fixed'              => round($update['markup_fixed'],2),
                             );
                  
            $this->updateActive_markup($insert_markup_data,$markup_id); 
        }
        elseif (isset($update['user_type']) == 5) { //Supplier          
              
            if(in_array(0,$update['supplier_user'])) { // All user
                $update['supplier_user']=array('0');
            }       
            
            
            for ($uloop = 0; $uloop < sizeof($update['supplier_user']); $uloop++) {
                for ($cloop = 0; $cloop < sizeof($update['country']); $cloop++) {      
                        
                        if($update['country'][$cloop]!=1) {
                            $update['markup_type']   = 'SPECIFIC';
                            $country_id             = $update['country'][$cloop];
                        } 
                        else {
                            $input['markup_type']   = 'GENERAL'; 
                            $country_id             = 1;
                        }
                                   
                        $insert_markup_data = array(
                            'user_type_id'              => $update['user_type'],
                            'user_details_id'           => $update['supplier_user'][$uloop],
                            //'domain_details_id'       => 3, //$input['domain'][$d],
                            'product_details_id'        => $update['product'],
                            'api_details_id'            => '3',
                           // 'supplier_details_id'         => $suppliers,
                            //'hotel_details_id'            => $hotels,                     
                            'markup_type'               => $update['markup_type'],
                            //'markets'                     => $markets,
                            //'country_id'              => $input['country'][$cloop],
                            'country_id'                => $country_id,
                            //'airline_details_id'      => 2,
                            'markup_value'              => round($update['markup_value'],2),
                            'markup_fixed'              => round($update['markup_fixed'],2),
                            'markup_status'                     => trim($update['markup_status']),
                            'markup_created_date'       => date('Y-m-d H:i:s'),
                            'markup_added_by_mgmt'      => $this->session->userdata('provab_admin_id')
                        );
                        $this->updateActive_markup($insert_markup_data,$markup_id);                                       
                }//cloop                            
            }//uloop 
        }//else 
        
    }

    function updateActive_markup($update,$markup_id){
          $this->db->where('markup_details_id', $markup_id);
          $this->db->update('markup_details', $update); 
          $this->General_Model->insert_log('6', 'updateActive_markup', json_encode($update), 'Update  Markup to database', 'markup_details', 'markup_details_id', $markup_id);
      
    
    }
	function active_markup($markup_id){
		$data = array( 'markup_status' => 'ACTIVE' );
		$this->db->where('markup_details_id', $markup_id);
		$this->db->update('markup_details', $data); 
		$this->General_Model->insert_log('6','active_markup',json_encode($data),'updating  Markup status to active','markup_details','markup_details_id',$markup_id);
	}
	
	function inactive_markup($markup_id){
		$data = array('markup_status' => 'INACTIVE');
		$this->db->where('markup_details_id', $markup_id);
		$this->db->update('markup_details', $data); 
        
		$this->General_Model->insert_log('6','inactive_markup',json_encode($data),'updating  Markup status to inactive','markup_details','markup_details_id',$markup_id);
	}
	
	function delete_markup($markup_details_id){
		$this->db->where('markup_details_id', $markup_details_id);
		$this->db->delete('markup_details'); 
		$this->General_Model->insert_log('6','delete_markup',json_encode(array()),'deleting  Markup from  database','markup_details','markup_details_id',$markup_details_id);									
	}

	function get_user_list_by_type($user_type_id = ''){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		if($user_type_id !='')
			$this->db->where('user_type_id', $user_type_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_api_info() {
        $this->db->select('api_details_id, api_name, api_alternative_name');
        $this->db->from('api_details');
       // $this->db->where('product_details_id', $product_id);
        $this->db->where('api_status','ACTIVE');
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
    function get_supplier_list_by_type(){
		 
		$this->db->select('*');
		$this->db->from('supplier_details');
		//if($user_type_id !='')
			//$this->db->where('user_type_id', $user_type_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}

		//echo $this->db->last_query(); exit();
	}

    /*function get_api_list($api_id = ''){
		$this->db->select('*');
		$this->db->from('api_details');
		if($api_id !='')
			$this->db->where('api_details_id', $api_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}*/

    function get_available_general_markup($product_id, $markup_type, $markup_value_type = '', $markupvalue = '', $markup_fixed = '', $markup_id = '' ){
         
       $this->db->select("*");
      $this->db->from("markup_details");
      $this->db->where("markup_type", $markup_type);
      $this->db->where("product_details_id", $product_id);
      $this->db->where("markup_type", "GENERAL");
      if($markupvalue != ''){
        $this->db->where("markup_value", $markupvalue);  
         } 
         if($markup_fixed != ''){
        $this->db->where("markup_fixed", $markup_fixed);  
         } 
      if($markup_id != ''){
        $this->db->where("markup_details_id !=", $markup_id);  
      }
      $query = $this->db->get()->row();

      return $query;
  }
  
   function get_available_specific_markup($product_id, $api_id, $markup_id = '', $country,$markupvalue = '', $markup_fixed = '', $sub_agent_id =''){
     // print_r($markupvalue);
     // print_r($markup_fixed); exit();
      $this->db->select("*");
      $this->db->from("markup_details");
      $this->db->where("api_details_id", $api_id);
      $this->db->where("product_details_id", $product_id);
      $this->db->where("markup_type", "SPECIFIC");
      if($country != ''){
      $this->db->where("country_id", $country);
      }
      
      if($sub_agent_id != 0){
          $this->db->where("user_details_id", $sub_agent_id); 
      }
      
     if($expiry_date != ''){
         $this->db->where("markup_expiry_date",date('Y-m-d', strtotime($expiry_date)));  
      }else{
         $this->db->where("markup_expiry_date", '0000-00-00');     
      }
      if($markupvalue != ''){
        $this->db->where("markup_value", $markupvalue);  
         } 
         if($markup_fixed != ''){
        $this->db->where("markup_fixed", $markup_fixed);  
         } 
      if($markup_id != ''){
        $this->db->where("markup_details_id !=", $markup_id);  
      }
      $query = $this->db->get()->row();
      //echo $this->db->last_query(); exit;
      return $query;
  }
  
  function get_airline_list(){
	  $this->db->select('*');
	  $this->db->from('airline_list');
	  return $this->db->get()->result();
  }
}
?>
