<?php
class Dashboard_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    }
    
    public function getAdminDetails(){
		try {
		$provabAdminId = $this->session->userdata('provabAdminId');
		$this->db->select('*')->from('admin_details ad')->where('admin_id', $provabAdminId)->where('admin_status', 'ACTIVE');
		$this->db->join('address_details a', 'a.address_details_id = ad.address_details_id');
		$this->db->join('country_details c', 'c.country_id = a.country_id');
		$query = $this->db->get();
	
		if ( $query->num_rows > 0 ) {
			return $query->row();	
	    }else{
			return '';	
		}
	}catch(Exception $e){
		return $e;
	}
	}

	public function get_admin_logs_activity(){
		$provab_admin_id 	= $this->session->userdata('provabAdminId');
		$this->db->select('*')->from('admin_login_tracking_details')->where('admin_id',$provab_admin_id)->order_by('login_tracking_details_time_stamp','desc');
		$query = $this->db->get();	
    	if ( $query->num_rows > 0 ){
			return $query->result();	
	    }else{
			return '';	
		}
    }

    public function delete_admin_logs_activity(){
    	$this->db->truncate('admin_login_tracking_details');
    }

    public function get_white_list_ip_details($white_list_ip_id =''){
		$admin_id = $this->session->userdata('provabAdminId');
		$this->db->select('*')->from('admin_white_iplist')->where('admin_id',$admin_id);
		if($white_list_ip_id !='')
			$this->db->where('admin_white_iplist_id', $white_list_ip_id);
		$query = $this->db->get();	
    	if ( $query->num_rows > 0 ) {
			return $query->result();	
		}else{
			return '';	
		} 
	}

	function add_white_list($input){	
		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
		$admin_id = $this->session->userdata('provabAdminId');
		$insert_data = array(
							'admin_id' 		=> $admin_id,
							'ip_address' 	=> $input['ip_address'],
							'status' 		=> $input['status'],
							'created_date'  => date('Y-m-d'),
						);
		 try {
		$this->db->insert('admin_white_iplist',$insert_data);
		$white_list_ip_id = $this->db->insert_id();
		$this->General_Model->insert_log('11','add_white_list',json_encode($insert_data),'Adding  White list Details to database','admin_white_iplist','admin_white_iplist_id',$white_list_ip_id);
	 } catch(Exception $e) {
		 return $e;
	}
	}
 
  function updateIpStatus($ip_id, $status){
	   $data = array('status' => $status);
		$this->db->where('admin_white_iplist_id', $ip_id);
		$this->db->update('admin_white_iplist', $data);
		$this->General_Model->insert_log('11','active_white_list_ip',json_encode($data),'updating  White list Details to active','admin_white_iplist','admin_white_iplist_id',$ip_id); 
  }
	
	function delete_white_list_ip($white_list_ip_id){
		$this->db->where('admin_white_iplist_id', $white_list_ip_id);
		$this->db->delete('admin_white_iplist'); 
		$this->General_Model->insert_log('11','delete_white_list_ip',json_encode(array()),'deleting  White list Details from database','admin_white_iplist','admin_white_iplist_id',$white_list_ip_id);
	}

	function update_white_list_ip($update,$white_list_ip_id){
		if(!isset($update['status']))
			$update['status'] = "INACTIVE";
		$update_data = array(
							'ip_address' 	=> $update['ip_address'],
							'status' 		=> $update['status']				
						);
		try {
		$this->db->where('admin_white_iplist_id', $white_list_ip_id);
		$this->db->update('admin_white_iplist', $update_data);
		$this->General_Model->insert_log('11','update_white_list_ip',json_encode($update_data),'updating  White list Details  to database','admin_white_iplist','admin_white_iplist_id',$white_list_ip_id);
	} catch(Exception $e) {
		 return $e;
	}
	} 
	
	function updateAddressDetails($admin_address_details, $data) {

		$address_details_id = $data['admin_profile_info']->address_details_id;
		$admin_data = array('address' 	=> $admin_address_details['address'],
							 'city_name'=> $admin_address_details['city_name'],
							 'zip_code'	=> $admin_address_details['zip_code'],
							 'state_name'=> $admin_address_details['state_name'],
							 'country_id'=> $admin_address_details['country'] ) ;
         
        try {
        	$this->db->where('address_details_id', $address_details_id);
	        $this->db->update('address_details', $admin_data);
        } catch(Exception $ex) {
            $ex->getMessage();
        }
	}

	function updateProfileInfo($admin_info, $data) {
		$address_details_id = $data['admin_profile_info']->address_details_id;
		$admin_id = $this->session->userdata('provabAdminId');
		$admin_name = $admin_info['salution']."-".$admin_info['first_name']."-".$admin_info['middle_name']."-".$admin_info['last_name'] ;
		$admin_data = array( 'admin_name' 				=> $admin_name,
							 'admin_email'				=> $admin_info['email'],
							 'address_details_id'		=> $address_details_id ,
							 'admin_home_phone'			=> $admin_info['home_phone'],
							 'admin_cell_phone'			=> $admin_info['cell_phone'],
							 'admin_account_number'		=> $admin_info['account_number'] ) ;
		try {
			$this->db->where('admin_id', $admin_id);
			$this->db->update('admin_details', $admin_data);
		} catch(Exception $ex) {
			 $ex->getMessage();
        }
	}
   
}
