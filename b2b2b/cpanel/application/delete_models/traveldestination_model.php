<?php
class Traveldestination_Model extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function get_travel_list($travel_destinations_id = ''){
		$this->db->select('*');
		$this->db->from('travel_destinations');
		if($travel_destinations_id !='')
			$this->db->where('travel_destinations_id', $travel_destinations_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function add_traveldestination_details($input,$image){
		if(!isset($input['traveldestination_status']))
			$input['traveldestination_status'] = "INACTIVE";
		$insert_data = array(
							'users_id'        => $input['agents_id'],
							'city' 	          => $input['country'][0],
							'checkin_date' 	  => $input['ckeckin'],
							'checkout_date'   => $input['checkout'],
							'exp_date' 	  	  => $input['expdate'],
							'price' 		  => $input['price'],
							'package_name' 	  => $input['package_name'],
							'package_name_china' => $input['package_name_china'],
							'city_image' 	  => $image,
							'position' 		  => $input['position'],
							'status' 		  => $input['traveldestination_status'],
							'creation_time'	  => (date('Y-m-d H:i:s'))					
						);		
			//echo '<pre>'; print_r($insert_data); exit();	
		$this->db->insert('travel_destinations',$insert_data);
	}
   
	function active_traveldestination($travel_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('travel_destinations_id', $travel_id);
		$this->db->update('travel_destinations', $data); 
	}
	
	function inactive_traveldestination($travel_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('travel_destinations_id', $travel_id);
		$this->db->update('travel_destinations', $data); 
	}
	
	function delete_traveldestinations($travel_id){
		$this->db->where('travel_destinations_id', $travel_id);
		$this->db->delete('travel_destinations'); 
	}
	
	function update_traveldestination_details($update,$id,$city_image_name){
		if(!isset($update['traveldestination_status']))
			$update['traveldestination_status'] = "INACTIVE";
		$update_data = array(
							/*'city' 	          => $update['country'][0],*/
							'checkin_date' 	  => $update['ckeckin'],
							'checkout_date'   => $update['checkout'],
							'exp_date' 	  	  => $update['expdate'],
							'price' 		  => $update['price'],
							'package_name' 	  => $update['package_name'],
							'package_name_china' => $update['package_name_china'],
							'city_image' 	  => $city_image_name,
							'position' 		  => $update['position'],
							'status' 		  => $update['traveldestination_status'],
							'updation_time'		  => (date('Y-m-d H:i:s'))			
						); 
		$this->db->where('travel_destinations_id', $id);
		$this->db->update('travel_destinations', $update_data);
	}

   public function getAirportList($query){
        $this->db->distinct();
        $this->db->like('cityName', $query); 
        $this->db->limit(8);
        return $this->db->get('hotelbeds_cities');
    }

    function getUsers($users = ''){
		$this->db->select('*');
        $this->db->from('user_details');
        if($users!=''){
			$this->db->where('user_details_id',$users);
		}
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
	}
}
?>
