<?php
class Footer_Model extends CI_Model {

    function __construct(){
        
        parent::__construct();
    }

    function getFooterList($footer_id = ''){
		$this->db->select('*');
		$this->db->from('footer_details');
		if($footer_id !='')
			$this->db->where('footer_details_id', $footer_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addFooterDetails($input){
		if(!isset($input['footer_status']))
			$input['footer_status'] = "INACTIVE";
		$insert_data = array(
							'footer_name' 				=> $input['footer_name'],
							'footer_name_china'         => $input['footer_name_china'],
							'position' 					=> $input['position'],
							'status' 					=> $input['footer_status'],
							'creation_date'				=> (date('Y-m-d H:i:s'))					
						);
		$this->db->insert('footer_details',$insert_data);
	}

    function activeFooter($footer_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('footer_details_id', $footer_id);
		$this->db->update('footer_details', $data); 
	}
	
	function inactiveFooter($footer_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('footer_details_id', $footer_id);
		$this->db->update('footer_details', $data); 
	}
	
	function deleteFooter($footer_id){
		$this->db->where('footer_details_id', $footer_id);
		$this->db->delete('footer_details'); 
	}
	
	function updateFooter($update,$footer_id){
		if(!isset($update['footer_status']))
			$update['footer_status'] = "INACTIVE";
		$update_data = array(
							'footer_name' 				=> $update['footer_name'],
							'footer_name_china'         => $update['footer_name_china'],
							'position' 					=> $update['position'],
							'status' 					=> $update['footer_status'],			
						);			
		$this->db->where('footer_details_id', $footer_id);
		$this->db->update('footer_details', $update_data);
	}

  function getsubFooterList($footer_id = ''){
		$this->db->select('*');
		$this->db->from('subfooter_details');
		if($footer_id !='')
			$this->db->where('footer_details_id', $footer_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
 
  function addSubFooterDetails($input){
		if(!isset($input['footer_status']))
			$input['footer_status'] = "INACTIVE";
		$insert_data = array(
							'footer_name' 				=> $input['footer_name'],
							'position' 					=> $input['position'],
							'status' 					=> $input['footer_status'],
							'creation_date'				=> (date('Y-m-d H:i:s'))					
						);
		$this->db->insert('subfooter_details',$insert_data);
	}

		function activeSubFooter($footer_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('footer_details_id', $footer_id);
		$this->db->update('subfooter_details', $data); 
	}
	
	function inactiveSubFooter($footer_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('footer_details_id', $footer_id);
		$this->db->update('subfooter_details', $data); 
	}

	function updateSubFooter($update,$footer_id){
		if(!isset($update['footer_status']))
			$update['footer_status'] = "INACTIVE";
		$update_data = array(
							'footer_name' 				=> $update['footer_name'],
							'position' 					=> $update['position'],
							'status' 					=> $update['footer_status'],			
						);			
		$this->db->where('footer_details_id', $footer_id);
		$this->db->update('subfooter_details', $update_data);
	}

	function deleteSubFooter($footer_id){
		$this->db->where('footer_details_id', $footer_id);
		$this->db->delete('subfooter_details'); 
	}

	function add_managefooter($input,$category_id1){
		if(!isset($input['footer_status']))
			$input['footer_status'] = "INACTIVE";
		$insert_data = array(
							'class' 					=> $input['class'],
							'footerid' 					=> $category_id1,
							'description' 				=> $input['description'],
							'description_china'         => $input['description_china'],
							'position' 					=> $input['position'],
							'status' 					=> $input['footer_status'],
							'creation_date'				=> (date('Y-m-d H:i:s'))					
						);
		
		$this->db->insert('managefooter_details',$insert_data);
	}

	function getManageFooterList($footer_id = ''){
		$this->db->select('*');
		$this->db->from('managefooter_details');
		if($footer_id !='')
			$this->db->where('footerid', $footer_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function activeManageFooter($footer_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('footer_id', $footer_id);
		$this->db->update('managefooter_details', $data); 
	}
	
	function inactiveManageFooter($footer_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('footer_id', $footer_id);
		$this->db->update('managefooter_details', $data); 
	}

	function getFooterManagementList($footer_id = ''){
		$this->db->select('*');
		$this->db->from('managefooter_details');
		if($footer_id !='')
			$this->db->where('footer_id', $footer_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function updateManageFooter($update,$footer_id){
		if(!isset($update['footer_status']))
			$update['footer_status'] = "INACTIVE";
		$update_data = array(
							'description' 				=> $update['description'],
							'description_china'         => $update['description_china'],
							'position' 					=> $update['position'],
							'status' 					=> $update['footer_status'],			
						);			
		$this->db->where('footer_id', $footer_id);
		$this->db->update('managefooter_details', $update_data);
	}

	function deleteManageFooter($footer_id){
		$this->db->where('footer_id', $footer_id);
		$this->db->delete('managefooter_details'); 
	}

	//Static Page
		function getFooterCarrersList($contactid = ''){
		$this->db->select('*');
		$this->db->from('footer_carrer');
		if($contactid !='')
			$this->db->where('footer_carrer_id', $contactid);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function addFooterCarrersList($input){
		if(!isset($input['menu_status']))
			$input['menu_status'] = "INACTIVE";
		$insert_data = array(
							'user_type'             => $input['user_type'],
							'agent_id'				=> $input['agent'],
							'footer_id' 		=> $input['headerid'],
							'carrer_description' => $input['service_description'],
							'carrer_description_china' => $input['service_description_china'],
							'position' 				=> $input['position'],
							'status' 				=> $input['menu_status'],
							'creation_date'			=> (date('Y-m-d H:i:s'))
						);		
			//echo '<pre>'; print_r($insert_data); exit();
		$this->db->insert('footer_carrer',$insert_data);
	}

	function inactiveFooterCarrer($header_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('footer_carrer_id', $header_id);
		$this->db->update('footer_carrer', $data); 
	}

	function activeFooterCarrer($header_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('footer_carrer_id', $header_id);
		$this->db->update('footer_carrer', $data); 
	}

	function deleteFooterCarrer($header_id){
		$this->db->where('footer_carrer_id', $header_id);
		$this->db->delete('footer_carrer'); 
	}

	function updateCarrer($update, $header_id){
		if(!isset($update['menu_status']))
			$update['menu_status'] = "INACTIVE";
		$update_data = array(
							'carrer_description' 	=> $update['service_description'],
							'carrer_description_china' 	=> $update['service_description_china'],
							'position' 				=> $update['position'],
							'status' 				=> $update['menu_status'],
							'updation_date'			=> 	(date('Y-m-d H:i:s'))	
						);
		$this->db->where('footer_carrer_id', $header_id);
		$this->db->update('footer_carrer', $update_data);
	}

	//Terms
		function getFooterTermsnconditionsList($contactid = ''){
		$this->db->select('*');
		$this->db->from('footer_termsconditions');
		if($contactid !='')
			$this->db->where('footer_termsconditions_id', $contactid);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function addFooterTermsnconditions($input){
		if(!isset($input['menu_status']))
			$input['menu_status'] = "INACTIVE";
		$insert_data = array(
							'user_type'             => $input['user_type'],
							'agent_id'				=> $input['agent'],
							'footer_id' 		=> $input['headerid'],
							'termsconditions_description' => $input['service_description'],
							'termsconditions_description_china' => $input['service_description_china'],
							'position' 				=> $input['position'],
							'status' 				=> $input['menu_status'],
							'creation_date'			=> (date('Y-m-d H:i:s'))
						);		
			//echo '<pre>'; print_r($insert_data); exit();
		$this->db->insert('footer_termsconditions',$insert_data);
	}

	function inactiveFooterTermsnconditions($header_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('footer_termsconditions_id', $header_id);
		$this->db->update('footer_termsconditions', $data); 
	}

	function activeFooterTermsnconditions($header_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('footer_termsconditions_id', $header_id);
		$this->db->update('footer_termsconditions', $data); 
	}

	function deleteFooterTermsnconditions($header_id){
		$this->db->where('footer_termsconditions_id', $header_id);
		$this->db->delete('footer_termsconditions'); 
	}

	function updateTermsnconditions($update, $header_id){
		if(!isset($update['menu_status']))
			$update['menu_status'] = "INACTIVE";
		$update_data = array(
							'termsconditions_description' 	=> $update['service_description'],
							'termsconditions_description_china' 	=> $update['service_description_china'],
							'position' 				=> $update['position'],
							'status' 				=> $update['menu_status'],
							'updation_date'			=> 	(date('Y-m-d H:i:s'))	
						);
		
		$this->db->where('footer_termsconditions_id', $header_id);
		$this->db->update('footer_termsconditions', $update_data);
	}
	//End Terms
	//End OF Static Page
}

?>
