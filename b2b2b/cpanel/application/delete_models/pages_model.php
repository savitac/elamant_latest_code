<?php
class Pages_Model extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function getPagesList($page_id = ''){
		$this->db->select('*');
		$this->db->from('static_page_details');
		if($page_id !='')
			$this->db->where('static_page_details_id', $page_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addPagesDetails($labels){
		
		$this->db->insert('static_page_details',$labels);
		$page_id = $this->db->insert_id();
		$this->General_Model->insert_log('12','addPagesDetails',json_encode($labels),'Adding  Page Details to database','static_page_details','static_page_details_id',$page_id);
	}

	function activePages($page_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('static_page_details_id', $page_id);
		$this->db->update('static_page_details', $data); 
		$this->General_Model->insert_log('12','activePages',json_encode($data),'updating Page Details status to active','static_page_details','static_page_details_id',$page_id);  
	}
	
	function inactivePages($page_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('static_page_details_id', $page_id);
		$this->db->update('static_page_details', $data); 
		$this->General_Model->insert_log('12','inactivePages',json_encode($data),'updating Page Details status to inactive','static_page_details','static_page_details_id',$page_id);
	}
	
	function deletePages($page_id){
		$this->db->where('static_page_details_id', $page_id);
		$this->db->delete('static_page_details'); 
		$this->General_Model->insert_log('12','deletePages',json_encode(array()),'deleting  Page Details from database','static_page_details','static_page_details_id',$page_id);
	}
	
	function updatePages($update,$page_id){
		if(!isset($update['status']))
			$update['status'] = "INACTIVE";
		$update_data = array(
							'page_name' 			=> $update['page_name'],
							'page_content_body' 	=> $update['page_content_body'],
							'page_content_side' 	=> $update['page_content_side'],
							'page_url' 				=> str_replace(" ","-",trim($update['page_url'])),
							'status' 				=> $update['status']					
						);		
		$this->db->where('static_page_details_id', $page_id);
		$this->db->update('static_page_details', $update_data);
		$this->General_Model->insert_log('12','updatePages',json_encode($update_data),'updating Page Details to database','static_page_details','static_page_details_id',$page_id);
	}
	
	function get_homepage_contents($homeId = ''){ 
		$this->db->select('*');
		$this->db->from('home_page_content');
		if($homeId != ""){
		$this->db->where("home_page_content_id", $homeId);
	    }
	    $query = $this->db->get()->result();

	     return $query;
	}
	
	function add_home_content($data, $homeImage) {
		$data['content_image'] = $homeImage;
		$data['status'] = 1;
		$data['created_on'] =date('Y-m-d');
		$data['created_by'] = $this->session->userdata('provabAdminId');
		$this->db->insert("home_page_content", $data);
		return;
		
	}
	function activehome($pages_id){
		
		$data = array(
					'status' => '0'
					);
		
		$this->db->where('home_page_content_id', $pages_id);
		$this->db->update('home_page_content', $data); 
		
	}
	
	function inactivehome($pages_id){
		$data = array(
					'status' => '1'
					);
		
		$this->db->where('home_page_content_id', $pages_id);
		$this->db->update('home_page_content', $data); 
		
	}
	
	function updateStatus($pages_id, $status) {
		$data = array(
					'status' => $status
					);
		$this->db->where('home_page_content_id', $currency_id);
		$this->db->update('home_page_content', $data); 
		$this->General_Model->insert_log('3','active_currency',json_encode($data),'updating   status to '.$status,'home_page_content','home_page_content_id',$pages_id);
	}

	function gethomepage($id){
		$this->db->select('*');
		$this->db->from('home_page_content');
		$this->db->where('home_page_content_id', $id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function update_homelist($update,$id,$image){

		$update_data = array(
							'content_title' 			=> $update['content_title'],
							'content_title_china'   => $update['content_title_china'],
							'content_description' 	=> $update['content_description'],
							'content_description_china' 	=> $update['content_description_china'],
							'display_order' 	=> $update['display_order'],
							'content_image'			=> $image
							);		
		$this->db->where('home_page_content_id', $id);
		$this->db->update('home_page_content', $update_data);
		$this->General_Model->insert_log('12','updatePages',json_encode($update_data),'updating Homepage Details to database','home_page_content','home_page_content_id',$id);
	
	}
}
?>
