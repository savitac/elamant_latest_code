<?php
class State_Model extends CI_Model {

    function __construct(){
        
        parent::__construct();
    }

    function getStateList($state_id = ''){
		$this->db->select('*');
		$this->db->from('states');
		if($state_id !='')
			$this->db->where('State_id', $state_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
		
	}
	
    function addStateDetails($input){
    	
    	$test = explode('-', $input['country'][0]);
    	//echo '<pre>sanj'; print_r($test); exit();
		if(!isset($input['country_status']))
			$input['country_status'] = "INACTIVE";
		$insert_data = array(
							'country_name' 	=> $test[0],
							'state_name' 	=> $input['statename'],
							'country_id' 	=> $test[1],
							'state_status' 	=> $input['country_status'],
							'cerated_date'		=> (date('Y-m-d H:i:s'))
						);	
		//echo '<pre>sanjay'; print_r($insert_data); exit();		
		$this->db->insert('states',$insert_data);
		$country_id = $this->db->insert_id();
		$this->General_Model->insert_log('9','addStateDetails',json_encode($insert_data),'Adding  State Details to database','country_details','State_id',$country_id);
	}

	function activeState($state_id){
		$data = array(
					'state_status' => 'ACTIVE'
					);
		$this->db->where('State_id', $state_id);
		$this->db->update('states', $data);
		$this->General_Model->insert_log('9','activeState',json_encode($data),'updating   State Details to active','states','State_id',$state_id); 
	}
	
	function inactiveState($state_id){
		$data = array(
					'state_status' => 'INACTIVE'
					);
		$this->db->where('State_id', $state_id);
		$this->db->update('states', $data);
		$this->General_Model->insert_log('9','inactiveState',json_encode($data),'updating  State Details status to inactive','states','State_id',$state_id); 
	}
	
	function delete_country($country_id){
		$this->db->where('country_id', $country_id);
		$this->db->delete('country_details'); 
		 $this->General_Model->insert_log('9','delete_country',json_encode(array()),'deleting  Country Details from database','country_details','country_id',$country_id);
	}
	
	function updateState($update,$country_id){
		if(!isset($update['country_status']))
			$update['country_status'] = "INACTIVE";
		$update_data = array(
							'state_name' 	=> $update['statename'],
							'state_status' 	=> $update['country_status'],
							'updation_date'	=> (date('Y-m-d H:i:s'))			
						);			
		$this->db->where('State_id', $country_id);
		$this->db->update('states', $update_data);
		$this->General_Model->insert_log('9','updateState',json_encode($update_data),'updating  State Details  to database','country_details','State_id',$country_id);
	}
}
?>
