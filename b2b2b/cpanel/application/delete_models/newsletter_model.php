<?php
class Newsletter_Model extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

	function getNewsletterTemplateDetails($newsletter_template_id = ''){
		$this->db->select('*');
		$this->db->from('newsletter_template');
		if($newsletter_template_id!='')
			$this->db->where('newsletter_template_id',$newsletter_template_id);
		$query = $this->db->get();
		if($query->num_rows > 0) {
			return $query->result();
		}else{
			return '';
		}
	}
	
	function addNewsletterTemplate($input){
		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
		$input_data = array(
							'template_name' 		=> $input['template_name'],
							/*'email_from' 			=> $input['email_from'],*/
							'email_from_name' 		=> $input['email_from_name'],
							'subject' 				=> $input['subject'],
							'message' 				=> $input['message'],
							'status' 				=> $input['status']					
						);
		$this->db->insert('newsletter_template',$input_data);
		$newsletter_template_id = $this->db->insert_id();
		
		$this->General_Model->insert_log('3','addNewsletterTemplate',json_encode($input_data),'Inserting  Email template  to database','newsletter_template','newsletter_template_id',$newsletter_template_id);
	}
	
	function activeNewsletterTemplate($newsletter_template_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('newsletter_template_id', $newsletter_template_id);
		$this->db->update('newsletter_template', $data); 
		$this->General_Model->insert_log('3','activeNewsletterTemplate',json_encode($data),'updating  Email Template status to active','newsletter_template','newsletter_template_id',$newsletter_template_id);
	}
	
	function inactiveNewsletterTemplate($newsletter_template_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('newsletter_template_id', $newsletter_template_id);
		$this->db->update('newsletter_template', $data); 
		$this->General_Model->insert_log('3','inactiveNewsletterTemplate',json_encode($data),'updating  Email Template status to inactive','newsletter_template','newsletter_template_id',$newsletter_template_id);
	}
	function activeSubscribeTemplate($sub_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('newsletter_subscription_id', $sub_id);
		$this->db->update('newsletter_subscriptions', $data); 
		//$this->General_Model->insert_log('3','active_newsletter_template',json_encode($data),'updating  Email Template status to active','newsletter_template','newsletter_template_id',$newsletter_template_id);
	}
	function inactiveSubscribeTemplate($newsletter_template_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('newsletter_subscription_id', $newsletter_template_id);
		$this->db->update('newsletter_subscriptions', $data); 
		//$this->General_Model->insert_log('3','inactive_newsletter_template',json_encode($data),'updating  Email Template status to inactive','newsletter_template','newsletter_template_id',$newsletter_template_id);
	}
	function delete_newsletter_template($newsletter_template_id){
		$this->db->where('newsletter_template_id', $newsletter_template_id);
		$this->db->delete('newsletter_subscriptions'); 
		$this->General_Model->insert_log('3','delete_newsletter_template',json_encode(array()),'deleting  Email Template from database','newsletter_template','newsletter_template_id',$newsletter_template_id);
	}

   function deleteSubscribeTemplate($newsletter_template_id){
		$this->db->where('newsletter_subscription_id', $newsletter_template_id);
		$this->db->delete('newsletter_subscriptions'); 
		$this->General_Model->insert_log('3','delete_newsletter_template',json_encode(array()),'deleting  Email Template from database','newsletter_template','newsletter_template_id',$newsletter_template_id);
	}	
	function updateNewsletterTemplate($update,$newsletter_template_id){						
		if(!isset($update['status']))
			$update['status'] = "INACTIVE";
		$update_data = array(
							//'template_name' 	    => $update['template_name'],
							/*'email_from' 			=> $update['email_from'],*/
							'email_from_name' 		=> $update['email_from_name'],
							'subject' 				=> $update['subject'],
							'message' 				=> $update['message'],
							'status' 				=> $update['status']					
						);
		$this->db->where('newsletter_template_id', $newsletter_template_id);
		$this->db->update('newsletter_template', $update_data);
		$this->General_Model->insert_log('3','updateNewsletterTemplate',json_encode($update_data),'updating  Email template  to database','newsletter_template','newsletter_template_id',$newsletter_template_id);
	}

	function check_uniquename($template_name, $template_id){
      $this->db->select('newsletter_template_id');      
      $this->db->where('LOWER(template_name)',$template_name);
      if($template_id != ""){
        $this->db->where('newsletter_template_id != ', $template_id);
      }
      $query = $this->db->get('newsletter_template');
      if($query->num_rows() > 0){
       return "1";
      }
      else{
       return "0";
      }     
    }
    function subscribeList(){
	$this->db->select('*');
	$this->db->from('newsletter_subscriptions');	
    $query = $this->db->get();
		if($query->num_rows > 0) {
			return $query->result();
		}else{
			return '';
		}
}

function deleteNewsletterTemplateList($newsletter_template_id){
		$this->db->where('newsletter_template_id', $newsletter_template_id);
		$this->db->delete('newsletter_template'); 
		$this->General_Model->insert_log('3','deleteNewsletterTemplateList',json_encode(array()),'deleting  Email Template from database','newsletter_template','newsletter_template_id',$newsletter_template_id);
	}
}
?>
