<?php
class Api_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    /*function getApiList($api_id = ''){
		$this->db->select('*');
		$this->db->from('api_details');
		if($api_id !='')
			$this->db->where('api_details_id', $api_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}*/

	function getApiList($api_id = ''){
		$this->db->select('*');
		$this->db->from('booking_source');
		if($api_id !='')
			$this->db->where('origin', $api_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	 function getActiveApiList($api_id = ''){
		$this->db->select('*');
		$this->db->from('api_details');
		if($api_id !='')
			$this->db->where('api_details_id', $api_id);
		$this->db->where('api_status', 'ACTIVE');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addApiDetails($input,$api_logo_name){
		$insert_data = $this->form_data($input, $api_logo_name);
		$insert_data['api_created_by'] = $this->session->userdata('provabAdminId');
		$insert_data['api_creation_date'] = date('Y-m-d');
		$this->db->insert('api_details',$insert_data);
		$api_id = $this->db->insert_id();
		$this->General_Model->insert_log('3','addApiDetails',json_encode($insert_data),'Adding  API to database','api_details','api_details_id',$api_id);
	}

	function activeApi($api_id){
		$data = array(
					'booking_engine_status' => '1'
					);
		$this->db->where('origin', $api_id);
		$this->db->update('booking_source', $data); 
		$this->General_Model->insert_log('3','activeApi',json_encode($data),'updating  API status to active','booking_source','origin',$api_id);
	}
	
	function inactiveApi($api_id){
		$data = array(
					'booking_engine_status' => '0'
					);
		$this->db->where('origin', $api_id);
		$this->db->update('booking_source', $data); 
		$this->General_Model->insert_log('3','inactiveApi',json_encode($data),'updating  API status to inactive','booking_source','origin',$api_id);
	}
	
	function deleteApi($api_id){
		$this->db->where('api_details_id', $api_id);
		$this->db->delete('api_details'); 
		$this->General_Model->insert_log('3','delete_api',json_encode(array()),'deleting  API from database','api_details','api_details_id',$api_id);
	}
	
	function updateApi($update,$api_id, $api_logo_name){
		$update_data = $this->form_data($update, $api_logo_name);
		$update_data['api_modified_by'] = $this->session->userdata('provabAdminId');
		$update_data['api_modified_date'] = date('Y-m-d');
		$this->db->where('api_details_id', $api_id);
		$this->db->update('api_details', $update_data);
		$this->General_Model->insert_log('3','updateApi',json_encode($update_data),'updating  API  to database','api_details','api_details_id',$api_id);
	}
	
	function form_data($input, $api_logo_name){
		if(!isset($input['api_status']))
			$input['api_status'] = "INACTIVE";
		$insert_data = array(
							'api_name' 				=> $input['api_name'],
							'product_id'            => $input['product_id'],
							'api_alternative_name' 	=> $input['api_name_alternative'],
							'api_logo' 				=> $api_logo_name,
							'api_username' 			=> $input['api_user_name'],
							'api_username1' 		=> $input['api_user_name1'],
							'api_url' 				=> $input['api_url'],
							'api_url1' 				=> $input['api_url1'],
							'api_password' 			=> $input['api_password'],
							'api_credential_type' 	=> $input['api_mode'],
							'api_status' 			=> $input['api_status'],
							'api_position'          => $input['position'],
							'api_creation_date'		=> (date('Y-m-d H:i:s'))					
						);		
		return $insert_data;
	}
}
?>
