<?php

class Lastminute_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_last_min_list($id = '') {
        $this->db->select('*');
        $this->db->from('last_minute_deals');
        if ($id != '')
            $this->db->where('last_minute_deals_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }

    function add_last_minute_deal_details($input, $image) {
        if (!isset($input['deals_status']))
            $input['deals_status'] = "INACTIVE";
            $city = explode(',', $input['city_name']); 
            $city = $city[1].','.$city[2];
        $insert_data = array(
            'name' => $input['name'],
            'name_china' => $input['name_china'],
            'users_id' => $input['agents_id'],
            'city_name' => $city,
            'hotel_name' => $input['hotel_name'],
            'description' => $input['description'],
            'description_china' => $input['description_china'],
            'exp_date' => $input['expdate'],
            'link' => $input['link'],
            'position' => $input['position'],
            'hotel_image' => $image,
            'status' => $input['deals_status'],
            'deals_creation_date' => (date('Y-m-d H:i:s'))
        );
         
        $this->db->insert('last_minute_deals', $insert_data);
        $id = $this->db->insert_id();
        $this->General_Model->insert_log('9','add_last_minute_deal_details',json_encode($insert_data),'Adding  Home page deals to database','last_minute_deals','last_minute_deals_id',$id);
    }

    function active_deal($id) {
        $data = array(
            'status' => 'ACTIVE'
        );
        $this->db->where('last_minute_deals_id', $id);
        $this->db->update('last_minute_deals', $data);
        $this->General_Model->insert_log('9','active_deal',json_encode($data),'updating  Home page deals status to inactive','last_minute_deals','last_minute_deals_id',$id);
    }

    function inactive_deal($id) {
        $data = array(
            'status' => 'INACTIVE'
        );
        $this->db->where('last_minute_deals_id', $id);
        $this->db->update('last_minute_deals', $data);
        $this->General_Model->insert_log('9','inactive_deal',json_encode($data),'updating  Home page deals status to active','last_minute_deals','last_minute_deals_id',$id);
    }

    function delete_deal($id) {
        $this->db->where('last_minute_deals_id', $id);
        $this->db->delete('last_minute_deals');
        $this->General_Model->insert_log('9','delete_deal',json_encode(array()),'deleting  Home page deals from database','last_minute_deals','last_minute_deals_id',$id);
    }

    function update_deal($update, $id, $image) {
        if (!isset($update['deals_status']))
            $update['deals_status'] = "INACTIVE";
        
        $update_data = array(
            'name' => $update['name'],
            'description' => $update['description'],
            'description_china' => $update['description_china'],
            'exp_date' => $update['expdate'],
            'link' => $update['link'],
            'position' => $update['position'],
            'hotel_image' => $image,
            'status' => $update['deals_status']
        );
        $this->db->where('last_minute_deals_id', $id);
        $this->db->update('last_minute_deals', $update_data);
        $this->General_Model->insert_log('9','update_deal',json_encode($update_data),'updating  Home page deals house offer to database','last_minute_deals','last_minute_deals_id',$id);

    }
    
    function get_deal_info_list($last_minute_deals_id , $deals_information_id = '') {
        $this->db->select('d.*,l.name');
        $this->db->from('deals_information d');
        if ($last_minute_deals_id != '')
            $this->db->where('d.last_minute_deals_id', $last_minute_deals_id);
         if ($deals_information_id != '')
            $this->db->where('deals_information_id', $deals_information_id);
        $this->db->join('last_minute_deals l', 'd.last_minute_deals_id = l.last_minute_deals_id');
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
	
	 function add_deal_info($input, $image) {
        if (!isset($input['deals_status']))
            $input['deals_status'] = "INACTIVE";

        $insert_data = array(
            'last_minute_deals_id' => $input['holiday_type'],
            'title' => $input['name'],
            'description' => $input['description'],
            'position' => $input['position'],
            'image' => $image,
            'status' => $input['deals_status'],
            'deals_creation_date' => (date('Y-m-d H:i:s'))
        );
        $this->db->insert('deals_information', $insert_data);
        $id = $this->db->insert_id();
        $this->General_Model->insert_log('9','add_deal_info',json_encode($insert_data),'Adding  Deals to database','deals_information','deals_information_id',$id);
    }
    
    function active_deal_info($id) {
        $data = array(
            'status' => 'ACTIVE'
        );
        $this->db->where('deals_information_id', $id);
        $this->db->update('deals_information', $data);
        $this->General_Model->insert_log('9','active_deal_info',json_encode($data),'updating  Holiday deals status to inactive','deals_information','deals_information_id',$id);
    }

    function inactive_deal_info($id) {
        $data = array(
            'status' => 'INACTIVE'
        );
        $this->db->where('deals_information_id', $id);
        $this->db->update('deals_information', $data);
        $this->General_Model->insert_log('9','inactive_deal',json_encode($data),'updating  Holiday deals status to active','deals_information','deals_information_id',$id);
    }

    function delete_deal_info($id) {
        $this->db->where('deals_information_id', $id);
        $this->db->delete('deals_information');
        $this->General_Model->insert_log('9','delete_deal_info',json_encode(array()),'deleting Holiday deals from database','deals_information','deals_information_id',$id);
    }
	
	function update_deal_info($update, $id, $image) {
        if (!isset($update['deals_status']))
            $update['deals_status'] = "INACTIVE";
        
        $update_data = array(
			'last_minute_deals_id' => $update['holiday_type'],
            'title' => $update['name'],
            'description' => $update['description'],
            'position' => $update['position'],
            'image' => $image,
            'status' => $update['deals_status']
        );
        $this->db->where('deals_information_id', $id);
        $this->db->update('deals_information', $update_data);
        $this->General_Model->insert_log('9','update_deal_info',json_encode($update_data),'updating  Deals offer to database','deals_information','deals_information_id',$id);

    }
    
    function update_spa($update,$spa_details_id, $hotel_spa_image){
		if(!isset($update['status']))
			$update['status'] = "INACTIVE";
		$update_data =  $this->form_data($update, $hotel_spa_image);
		// echo '<pre/>';print_r($update);exit;
		$this->db->where('spa_details_id', $spa_details_id);
		$this->db->update('spa_details', $update_data);
		$this->General_Model->insert_log('10','update_spa',json_encode($update_data),'updating SPA Details to database','spa_details','spa_details_id',$spa_details_id);
	}
	
    function get_spa_list($deals_information_id , $spa_details_id = '') {
         $this->db->select('s.*,d.title');
        $this->db->from('spa_details s');
        if ($deals_information_id != '')
            $this->db->where('s.deals_information_id', $deals_information_id);
         if ($spa_details_id != '')
            $this->db->where('s.spa_details_id', $spa_details_id);
        $this->db->join('deals_information d', 'd.deals_information_id = s.deals_information_id');
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
    
    function get_dinning_list($deals_information_id , $dining_details_id = '') {
        $this->db->select('s.*,d.title');
        $this->db->from('dining_details s');
        if ($deals_information_id != '')
            $this->db->where('s.deals_information_id', $deals_information_id);
         if ($dining_details_id != '')
            $this->db->where('s.dining_details_id', $dining_details_id);
        $this->db->join('deals_information d', 'd.deals_information_id = s.deals_information_id');
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
    
    function get_activities_list($deals_information_id , $activity_details_id = '') {
        $this->db->select('s.*,d.title');
        $this->db->from('activity_details s');
        if ($deals_information_id != '')
            $this->db->where('s.deals_information_id', $deals_information_id);
         if ($activity_details_id != '')
            $this->db->where('s.activity_details_id', $activity_details_id);
        $this->db->join('deals_information d', 'd.deals_information_id = s.deals_information_id');
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
    
    function get_ammenities_list($deals_information_id , $amenities_id = '') {
        $this->db->select('s.*,d.title');
        $this->db->from('special_amenities s');
        if ($deals_information_id != '')
            $this->db->where('s.deals_information_id', $deals_information_id);
         if ($amenities_id != '')
            $this->db->where('s.amenities_id', $amenities_id);
        $this->db->join('deals_information d', 'd.deals_information_id = s.deals_information_id');
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
 
    function get_reviews_list($deals_information_id , $reviews_details_id = '') {
        $this->db->select('s.*,d.title');
        $this->db->from('reviews_details s');
        if ($deals_information_id != '')
            $this->db->where('s.deals_information_id', $deals_information_id);
         if ($reviews_details_id != '')
            $this->db->where('s.reviews_details_id', $reviews_details_id);
        $this->db->join('deals_information d', 'd.deals_information_id = s.deals_information_id');
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
    
     function add_spa_details($input,$hotel_spa_image){
		
		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
		$insert_data =  $this->form_data($input, $hotel_spa_image);
		$insert_data['creation_date'] = date('Y-m-d H:i:s');
		$this->db->insert('spa_details',$insert_data);
		$hotel_spa_details_id = $this->db->insert_id();
		$this->General_Model->insert_log('10','add_spa_details',json_encode($insert_data),'Adding  SPA Details to database','spa_details','spa_details_id',$spa_details_id);
	}
	
	function form_data($post, $hotel_spa_image) {
 		return $data = array(
							'deals_information_id' 	=> $post['deals_information_id'],
							'hotel_spa_name' 	=> $post['hotel_spa_name'],
							'hotel_spa_image' 	=> $hotel_spa_image,
							'spa_description' 	=> $post['spa_description'],
							'position' 			=> $post['position'],
							'status' 			=> $post['status']
						);
 	}
    
    function active_spa($id) {
        $data = array(
            'status' => 'ACTIVE'
        );
       $this->db->where('spa_details_id', $id);
        $this->db->update('spa_details', $data);
        $this->General_Model->insert_log('9','active_spa',json_encode($data),'updating  SPA deals status to inactive','spa_details','spa_details_id',$id);
    }

    function inactive_spa($id) {
        $data = array(
            'status' => 'INACTIVE'
        );
        $this->db->where('spa_details_id', $id);
        $this->db->update('spa_details', $data);
        $this->General_Model->insert_log('9','inactive_spa',json_encode($data),'updating  SPA deals status to active','spa_details','spa_details_id',$id);
    }

    function delete_spa($id) {
        $this->db->where('spa_details_id', $id);
        $this->db->delete('spa_details');
        $this->General_Model->insert_log('9','delete_spa',json_encode(array()),'deleting SPA deals from database','spa_details','spa_details_id',$id);
    }

    function getUsers($users = ''){
        $this->db->select('*');
        $this->db->from('user_details');
        if($users!=''){
            $this->db->where('user_details_id',$users);
        }
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
}

?>
