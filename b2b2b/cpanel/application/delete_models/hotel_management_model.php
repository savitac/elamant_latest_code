<?php
class Hotel_Management_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    }
    function gtaFacilityList(){
		$this->db->select('*');
		$this->db->from('gta_Facilities');
		$query = $this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
    function gtaHotelList($city = ''){
		$this->db->select('*');
		$this->db->from('gta_Hotels');
		if($city != '')
			$this->db->where('CityCode',$city);
		$query = $this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function gtaHotelCityCodeList() {
		$this->db->select('CityCode,CityName');
		$this->db->from('gta_Hotels');
		$this->db->group_by('CityCode');
		$query = $this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function hotelBedsHotelList($country = ''){
		$this->db->select('*');
		$this->db->from('hotelbeds_Hotels');
		if($country != '')
			$this->db->where('countryCode',$country);
		$query = $this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function hotelBedsHotelCountryCodeList() {
		$this->db->select('countryCode');
		$this->db->from('hotelbeds_Hotels');
		$this->db->group_by('countryCode');
		$query = $this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function hotelBedsDestinationCountryCodeList() {
		$this->db->select('CountryCode,CountryName');
		$this->db->from('hotelbeds_Destinations');
		$this->db->group_by('CountryCode');
		$query = $this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function hotelBedsFacilitiesCodeList() {
		$this->db->select('CODE');
		$this->db->from('hotelbeds_facilities');
		$this->db->group_by('CODE');
		$query = $this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function hotelBedsFacilities($code='') {
		$this->db->select('HF.HOTELCODE,HF.CODE,HF.GROUP_,HFD.NAME');
		$this->db->from('hotelbeds_facilities HF');
		$this->db->join('hotelbeds_facilities_description HFD','HFD.CODE = HF.CODE');
		if($code != '')
			$this->db->where('HF.CODE',$code);
		$query = $this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function apiHotelCityCountryList() {
		$this->db->select('*');
		$this->db->from('api_hotels_cities');
		$this->db->group_by('country');
		$query = $this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function selectRecords($tableName,$columns,$condition='') {
		$this->db->select($columns);
		$this->db->from($tableName);
		$this->db->where($condition);
		$query = $this->db->get();
		//~ echo $this->db->last_query();exit;
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function updateRecords($tableName,$data,$condition='') {
		if($condition != ''):
			$this->db->update($tableName, $data, $condition);
		endif;
	}
}
?>
