<?php
class Banner_Model extends CI_Model {

    function __construct(){
       
        parent::__construct();
    }

    function getBannerList($banner_id = ''){
		$this->db->select('*');
		$this->db->from('banner_details');
		if($banner_id !='')
			$this->db->where('banner_details_id', $banner_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addBannerDetails($input,$banner_logo_name){
		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
		$insert_data = array(
							'title' 				=> $input['title'],
							'users_id'				=> $input['agents_id'],
							'banner_type' 			=> $input['banner_type'],
							'banner_type' 			=> $input['banner_type'],
							'banner_image' 			=> $banner_logo_name,
							'img_alt_text' 			=> $input['img_alt_text'],
							'position' 				=> $input['position'],
							'status' 				=> $input['status'],
							'creation_date'	=> (date('Y-m-d H:i:s'))					
						);			
		$this->db->insert('banner_details',$insert_data);
		$banner_id = $this->db->insert_id();
		$this->General_Model->insert_log('9','addBannerDetails',json_encode($insert_data),'Adding  Banner to database','banner_details','banner_details_id',$banner_id);
	}

	function activeBanner($banner_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('banner_details_id', $banner_id);
		$this->db->update('banner_details', $data); 
		$this->General_Model->insert_log('9','activeBanner',json_encode($data),'updating  Banner status to active','banner_details','banner_details_id',$banner_id);
	}
	
	function inactiveBanner($banner_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('banner_details_id', $banner_id);
		$this->db->update('banner_details', $data); 
		$this->General_Model->insert_log('9','inactiveBanner',json_encode($data),'updating  Banner status to inactive','banner_details','banner_details_id',$banner_id);
	}
	
	function deleteBanner($banner_id){
		$this->db->where('banner_details_id', $banner_id);
		$this->db->delete('banner_details'); 
		$this->General_Model->insert_log('9','deleteBanner',json_encode(array()),'deleting  Banner from database','banner_details','banner_details_id',$banner_id);
	}
	
	function updateBanner($update,$banner_id, $image_info_name){
		if(!isset($update['status']))
			$update['status'] = "INACTIVE";
		$update_data = array(
							'title' 				=> $update['title'],
							'banner_type' 			=> $update['banner_type'],
							'banner_image' 			=> $image_info_name,
							'img_alt_text' 			=> $update['img_alt_text'],
							
							'position' 				=> $update['position'],
							'status' 				=> $update['status']				
						);	
		
		$this->db->where('banner_details_id', $banner_id);
		$this->db->update('banner_details', $update_data);
		$this->General_Model->insert_log('9','updateBanner',json_encode($update_data),'updating  Banner  to database','banner_details','banner_details_id',$banner_id);
	}

	function getAgentList($user_id = ''){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		if($user_id !='')
			$this->db->where('user_details_id', $user_id);
		
		$this->db->where('user_type_id', '2');
		$this->db->where('branch_id', '0');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			$data['user_info'] = '';
		}else{
			$data['user_info'] = $query->result();
		}
		
		return $data;	
	} 

	function getUsers($users = ''){
        $this->db->select('*');
        $this->db->from('user_details');
        if($users!=''){
            $this->db->where('user_details_id',$users);
        }
        
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
}
?>
