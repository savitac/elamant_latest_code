<?php
class Supplier_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

     function get_supplier_list($supplier_id = ''){
		 
		$this->db->select('*');
		$this->db->from('supplier_details u');
		$this->db->join('address_details a', 'a.address_details_id = u.address_details_id');
		$this->db->join('country_details c', 'c.country_id = a.country_id');
		if($supplier_id !='')
			$this->db->where('supplier_details_id', $supplier_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			$data['supplier_info'] = '';
		}else{
			$data['supplier_info'] = $query->result();
		}
	
		if($data['supplier_info'] != ''){
			for($u=0;$u<count($data['supplier_info']);$u++){
				$data['supplier_type_details'][$u] 	= $this->get_supplier_type_details($data['supplier_info'][$u]->supplier_details_id);
				$data['supplier_country_details'][$u] 	= $this->get_country_details($data['supplier_info'][$u]->country_id);
				$data['supplier_company_country_details'][$u] 	= $this->get_country_details($data['supplier_info'][$u]->supplier_company_countryid);
				$data['bank_info'][$u] 	= $this->get_supplier_bank_info($data['supplier_info'][$u]->supplier_details_id);
			}
		}
		return $data;	
	}
	
	function get_country_details($country_id) {
		$this->db->select('*');
		$this->db->from('country_details');
		$this->db->where('country_id', $country_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

    function get_supplier_type_details($supplier_details_id) {
		$this->db->select('*');
		$this->db->from('supplier_type');
		$this->db->join('supplier_type_management s', 's.supplier_type_id = supplier_type.supplier_type_id');
		$this->db->where('supplier_details_id', $supplier_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
		
	}
	
	
	function get_supplier_bank_info($supplier_id){
		$this->db->select('*');
		$this->db->from('supplier_bank_info');
		$this->db->where('supplier_details_id', $supplier_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function get_supplier_type($supplier_details_id = '')
	{
		$this->db->select('*');
		$this->db->from('supplier_type');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function add_supplier_details($input,$user_profile_name){
		if(!isset($input['user_status']))
			$input['user_status'] = "ACTIVE";
		
		try{	
		$insert_data_address = array(
								'address' 		=> $input['address'],
								'city_name' 	=> $input['city'],
								'zip_code' 		=> $input['zip_code'],
								'state_name' 	=> $input['state_name'],
								'country_id' 	=> $input['country']					
							);			
		$this->db->insert('address_details',$insert_data_address);
		$address_details_id = $this->db->insert_id();
		$this->General_Model->insert_log('4','add_users_details',json_encode($insert_data_address),'adding  user address  Details to database','address_details','address_details_id',$address_details_id);
	} catch(Exception $e) {
		return $e;
	}
	 try {
		$insert_data_supplier = array(
								'address_details_id' 		=> $address_details_id,
								'supplier_name' 				=> $input['salution']."-".$input['first_name']."-".$input['middle_name']."-".$input['last_name'],
								'supplier_email' 				=> $input['email_id'],
								'supplier_cell_phone' 			=> $input['mobile_no'],					
								'supplier_account_number' 		=> 'TGC-'.rand(1,40),					
								'supplier_profile_pic' 			=> $user_profile_name,					
								'supplier_status' 				=> 'INACTIVE',
								'supplier_company_name'			=> $input['company_name'],
								'supplier_company_address'      => $input['company_address']."::". $input['company_city']."::". $input['company_state_name']."::". $input['company_zipcode'],
								'supplier_company_countryid'	=> $input['company_country'],
								'supplier_company_phone'		=> $input['phone_no'],
					          	'supplier_creation_date_time' 	=> (date('Y-m-d H:i:s'))	
															
							);		
	
			//echo '<pre>'; print_r($insert_data_supplier); exit();
		$this->db->insert('supplier_details',$insert_data_supplier);
		$supplier_details_id = $this->db->insert_id();
		$this->General_Model->insert_log('4','add_supplier_details',json_encode($insert_data_supplier),'adding  supplier  Details to database','supplier_details','supplier_details',$supplier_details_id);
	} catch(Exception $ex){
		return $ex;
	}
	  try{
		$insert_data_supplier_login = array(
									'supplier_details_id' 	=> $supplier_details_id,
									'supplier_uname' 		=> $input['email_id'],
									'supplier_password' 	=> "AES_ENCRYPT(".$input['new_password'].",'".SECURITY_KEY."')",
									'admin_pattren' 	=> ''					
								);			
		$this->db->insert('supplier_login_details',$insert_data_supplier_login);
		$supplier_id = $this->db->insert_id();
		$this->General_Model->insert_log('4','add_supplier_details',json_encode($insert_data_supplier_login),'adding  supplier login Details to database','supplier_login_details','supplier_login_details_id',$supplier_details_id);
		for($d=0;$d<count($input['supplier_type']);$d++){
			            $insert_data_supplier_management = array('supplier_details_id' => $supplier_details_id,
			                                                    'supplier_type_id' => $input['supplier_type'][$d],
			                                                    'status' => 'ACTIVE') ;
			            $this->db->insert('supplier_type_management',$insert_data_supplier_management);
					
						$msupplier_id = $this->db->insert_id();
						$this->General_Model->insert_log('4','add_supplier_details',json_encode($insert_data_supplier_management),'adding  supplier management Details to database','supplier_management_details','supplier_details_id',$supplier_details_id);
					}
		} catch(Exception $ex1) {
			return $ex1;
		}
		
		try{
			$insert_supplier_bank_data = array(
									'supplier_details_id' 	=> $supplier_details_id,
									'payee_name' 	=> $input['payee_name'],
									'bank_name' 		=> $input['bank_name'],
									'account_number' 	=> $input['account_number'],
									'ifsc_code' 	=> $input['ifsc_code'],
									'bank_address' 	=> $input['bank_address'],
									'bank_city_name' 	=> $input['bank_city_name'],
									'bank_state_name' =>  $input['bank_state_name'],	
									'bank_zipcode' =>  $input['bank_zipcode'],	
									'bank_country' =>  $input['bank_country'],	
									'status' =>  $input['user_status'],		
								);			
		$this->db->insert('supplier_bank_info',$insert_supplier_bank_data);
		$this->General_Model->insert_log('4','add_supplier_details',json_encode($insert_data_supplier_management),'adding  supplier bank Details to database','supplier_bank_info','supplier_details_id',$supplier_details_id);
		}catch(Exception $ex1) {
			return $ex1;
		}
		
	}
	
	
	function add_supplier_bank_info($input, $supplier_id)
	{
		$this->db->where('supplier_details_id',$supplier_id);
		$this->db->delete('supplier_bank_info');
		$insert_supplier_bank_data = array(
									'supplier_details_id' 	=> $supplier_id,
									'payee_name' 		=> $input['payee_name'],
									'bank_name' 		=> $input['bank_name'],
									'account_number' 	=> $input['account_number'],
									'ifsc_code' 	=> $input['ifsc_code'],
									'bank_address' 	=> "Test Address",
									'bank_city_name' 	=> $input['bank_city_name'],
									'bank_state_name' =>  $input['bank_state_name'],	
									'bank_zipcode' =>  $input['bank_zipcode'],	
									'bank_country' =>  $input['bank_country'],	
									'status' =>  'ACTIVE',		
								);			
		$this->db->insert('supplier_bank_info',$insert_supplier_bank_data);
	}


	function active_supplier($supplier_details_id){
		$data = array(
					'supplier_status' => 'ACTIVE'
					);
		$this->db->where('supplier_details_id', $supplier_details_id);
		$this->db->update('supplier_details', $data); 
		$this->General_Model->insert_log('4','active_supplier',json_encode($data),'updating  user status to active','supplier_details','supplier_details_id',$supplier_details_id);
		
	}
	
	function inactive_supplier($supplier_details_id){
		$data = array(
					'supplier_status' => 'INACTIVE'
					);
		$this->db->where('supplier_details_id', $supplier_details_id);
		$this->db->update('supplier_details', $data); 
		$this->General_Model->insert_log('4','inactive_supplier',json_encode($data),'updating  supplier status to inactive','supplier_details','supplier_details_id',$supplier_details_id);
		
	}
	
	function delete_supplier($supplier_details_id){
		$this->db->where('supplier_details_id', $supplier_details_id);
		$this->db->delete('supplier_details'); 
		$this->General_Model->insert_log('4','delete_supplier',json_encode(array()),'deleting  supplier details from database','supplier_details','supplier_details_id',$supplier_details_id);				
	}
	
	function update_supplier($update,$supplier_id,$user_profile_name){
		
		if(!isset($update['user_status']))
			$update['user_status'] = "INACTIVE";
		
		try{
		$update_data_address = array(
								'address' 		=> $update['address'],
								'city_name' 	=> $update['city'],
								'zip_code' 		=> $update['zip_code'],
								'state_name' 	=> $update['state_name'],
								'country_id' 	=> $update['country']					
							);
		$this->db->where('address_details_id', $update['address_details_id']);
		$this->db->update('address_details', $update_data_address);
		$this->General_Model->insert_log('4','update_supplier',json_encode($update_data_address),'updating  supplier address  Details to database','address_details','address_details_id',$update['address_details_id']);
	} catch(Exception $e){
		return $e;
	}
		
		try {
		$update_data_supplier = array(
								'supplier_name' 				=> $update['salution']."-".$update['first_name']."-".$update['middle_name']."-".$update['last_name'],
								'supplier_email'                =>$update['email_id'],
								'supplier_cell_phone' 			=> $update['mobile_no'],					
								'supplier_profile_pic' 			=> $user_profile_name,					
								'supplier_status' 				=> $update['user_status'],
								'supplier_company_name'			=> $update['company_name'],
								'supplier_company_address'      => $update['company_address']."::". $update['company_city']."::". $update['company_state_name']."::". $update['company_zipcode'],
								'supplier_company_countryid'	=> $update['company_country'],
								'supplier_company_phone'		=> $update['phone_no'],			
							);		
		$this->db->where('supplier_details_id', $supplier_id);						
		$this->db->update('supplier_details',$update_data_supplier);
		$this->General_Model->insert_log('4','update_supplier',json_encode($update_data_supplier),'updating  supplier  Details to database','supplier_details','supplier_details_id',$supplier_id);
	} catch(Exception $ex){
		return $ex;
	}try{
		$update_email_address = array(
								'supplier_uname' 		=> $update['email_id']
							);
		$this->db->where('supplier_details_id', $supplier_id);
		$this->db->update('supplier_login_details', $update_email_address);
		$this->General_Model->insert_log('4','update_supplier',json_encode($update_email_address),'updating  supplier address  Details to database','address_details','address_details_id',$update['address_details_id']);
	} catch(Exception $es){
		return $es;
	}
		try{
		$this->db->where('supplier_details_id', $supplier_id);
		$this->db->delete('supplier_type_management'); 
		
		for($d=0;$d<count($update['supplier_type']);$d++){
			            $insert_data_supplier_management = array('supplier_details_id' => $supplier_id,
			                                                    'supplier_type_id' => $update['supplier_type'][$d],
			                                                    'status' => 'ACTIVE') ;
			            $this->db->insert('supplier_type_management',$insert_data_supplier_management);
					
						$msupplier_id = $this->db->insert_id();
						$this->General_Model->insert_log('4','add_supplier_details',json_encode($insert_data_supplier_management),'adding  supplier management Details to database','supplier_management_details','supplier_details_id',$msupplier_id);
					}
				
		}
		catch(Exception $ex1){
		return $ex1;
	}
	try{
		$this->db->where('supplier_details_id',$supplier_id);
		$this->db->delete('supplier_bank_info');
		$insert_supplier_bank_data = array(
									'supplier_details_id' 	=> $supplier_id,
									'payee_name' 		=> $update['payee_name'],
									'bank_name' 		=> $update['bank_name'],
									'ifsc_code' 		=> $update['ifsc_code'],
									'account_number' 	=> $update['account_number'],
									'bank_address' 	=> "Test  Address",
									'bank_city_name' 	=> $update['bank_city_name'],
									'bank_state_name' =>  $update['bank_state_name'],	
									'bank_zipcode' =>  $update['bank_zipcode'],	
									'bank_country' =>  $update['bank_country'],	
									'status' =>  'ACTIVE',		
								);			
		$this->db->insert('supplier_bank_info',$insert_supplier_bank_data);
		}catch(Exception $ex1) {
			return $ex1;
		}
	
	
	} 

	function get_all_supplier_email($user_email) {
		$this->db->select('supplier_email');
		$this->db->from('supplier_details');
		if($user_email !='')
			$this->db->where('supplier_email', $user_email);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return 'NO';
		}else{
			return 'YES';
		}
	}
	public function export_supplier_users($ids) {
        $this->db->where_in('supplier_details_id', $ids);
        return $this->db->get('supplier_details');
	}

}
?>
