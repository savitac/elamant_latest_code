<?php
class Users_Model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

     function get_user_list($user_id = ''){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		// $this->db->join('address_details a', 'a.address_details_id = u.address_details_id');
		// $this->db->join('country_details c', 'c.country_id = a.country_id');
		if($user_id !='')
			$this->db->where('user_details_id', $user_id);
		$this->db->where('user_type_id', '1');
		$this->db->where('b2c_branch_id', '0');
		$query=$this->db->get();
		//echo "<pre/>dfg";print_r($this->db->last_query());die();
		if($query->num_rows() ==''){
			$data['user_info'] = '';
		}else{
			$data['user_info'] = $query->result();
		}
		if($data['user_info']!=''){
			for($u=0;$u<count($data['user_info']);$u++){
				$data['domain_details'][$u] 	= $this->Users_Model->get_domain_details($data['user_info'][$u]->user_details_id);
				$data['user_type_details'][$u] 	= $this->Users_Model->get_user_type_details($data['user_info'][$u]->user_details_id);
				$data['product_details'][$u] 	= $this->Users_Model->get_product_details($data['user_info'][$u]->user_details_id);
				$data['api_details'][$u] 		= $this->Users_Model->get_api_details($data['user_info'][$u]->user_details_id);
				$data['country_details'][$u] 	= $this->Users_Model->get_country_details($data['user_info'][$u]->user_details_id);
			}
		}
		return $data;	
	}
	
	function get_domain_details($user_details_id) {
		$this->db->select('um.domain_details_id, d.domain_name');
		$this->db->from('user_management_details um');
		$this->db->join('domain_details d', 'um.domain_details_id = d.domain_details_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_user_type_details($user_details_id) {
		$this->db->select('um.user_type_id, ut.user_type_name');
		$this->db->from('user_management_details um');
		$this->db->join('user_type ut', 'um.user_type_id = ut.user_type_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function get_all_user_type(){
		$this->db->select('*');
		$this->db->from('user_type');
		$this->db->where(array('status'=>'ACTIVE'));
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return ;
		}else{
			return $query->result();
		}
	
   }

	function get_product_details($user_details_id) {
		$this->db->select('um.product_details_id, p.product_name');
		$this->db->from('user_management_details um');
		$this->db->join('product_details p', 'um.product_details_id = p.product_details_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_api_details($user_details_id) {
		$this->db->select('um.api_details_id, a.api_name, a.api_alternative_name');
		$this->db->from('user_management_details um');
		$this->db->join('api_details a', 'um.api_details_id = a.api_details_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_country_details($user_details_id) {
		$this->db->select('um.product_details_id, c.country_name, c.iso3_code');
		$this->db->from('user_management_details um');
		$this->db->join('country_details c', 'um.country_id = c.country_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function get_all_user_email($user_email) {
		$this->db->select('user_email');
		$this->db->from('user_details');
		if($user_email !='')
		$this->db->where('user_email', $user_email);
		$this->db->where('user_type_id', '1');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return 'NO';
		}else{
			return 'YES';
		}
	}
	
	function get_all_user_email_by_user_type($user_type){
		$this->db->select('user_details_id,user_email');
		$this->db->from('user_details');
		$this->db->where('user_type_id', $user_type);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return "" ;
		}else{
			return $query->result();
		}
	}

	function add_users_details($input,$user_profile_name){
		//echo '<pre/>';print_r($input);exit;
		if(!isset($input['user_status']))
			$input['user_status'] = "INACTIVE";
			
		$insert_data_address = array(
								'address' 		=> $input['address'],
								'city_name' 	=> $input['city'],
								'zip_code' 		=> $input['zip_code'],
								'state_name' 	=> $input['state_name'],
								'country_id' 	=> $input['country']					
							);			
		$this->db->insert('address_details',$insert_data_address);
		$address_details_id = $this->db->insert_id();
		$this->General_Model->insert_log('4','add_users_details',json_encode($insert_data_address),'adding  user address  Details to database','address_details','address_details_id',$address_details_id);
		$insert_data_user = array(
								'user_type_id' 				=> $input['user_type'][0],
								'user_name' 				=> $input['first_name']."-".$input['last_name'],
								'Address'					=> $input['address'],
								'City' 						=> $input['city'],
								'country'  					=> $input['country'],
								'State'						=> $input['state_name'],
								'Zip_code'					=> $input['zip_code'],
								'user_email' 				=> $input['email_id'],
								'user_home_phone' 			=> $input['phone_no'],
								'user_cell_phone' 			=> $input['mobile_no'],					
								'user_account_number' 		=> 'PROVAB-'.rand(1,40),					
								'user_profile_pic' 			=> $user_profile_name,					
								'user_status' 				=> $input['user_status'],					
								'user_creation_date_time' 	=> (date('Y-m-d H:i:s'))					
							);			
		$this->db->insert('user_details',$insert_data_user);
		$user_details_id = $this->db->insert_id();
		$this->General_Model->insert_log('4','add_users_details',json_encode($insert_data_user),'adding  user  Details to database','user_details','user_details_id',$user_details_id);
		$insert_data_user_login = array(
									'user_details_id' 	=> $user_details_id,
									'user_name' 		=> $input['email_id'],
									'user_password' 	=> "AES_ENCRYPT(".$input['new_password'].",'".SECURITY_KEY."')",
									'user_type_id'      => '1',
									'admin_pattren' 	=> ''					
								);			
		$this->db->insert('user_login_details',$insert_data_user_login);
		$user_id = $this->db->insert_id();
		$this->General_Model->insert_log('4','add_users_details',json_encode($insert_data_user_login),'adding  user login Details to database','user_login_details','user_login_details_id',$user_id);
		if(false){
			for($d=0;$d<count($input['doamin']);$d++){
				for($u=0;$u<count($input['user_type']);$u++){
					for($p=0;$p<count($input['product']);$p++){
						for($a=0;$a<count($input['api']);$a++){
							$insert_data_user_management = array(
										'user_details_id' 			=> $user_details_id,
										'domain_details_id' 		=> $input['doamin'][$d],
										'user_type_id' 				=> $input['user_type'][$u],
										'product_details_id' 		=> $input['product'][$p],					
										'api_details_id' 			=> $input['api'][$a],				
										'country_id' 				=> '1'				
									);			
							$this->db->insert('user_management_details',$insert_data_user_management);	
							$muser_id = $this->db->insert_id();
							$this->General_Model->insert_log('4','add_users_details',json_encode($insert_data_user_management),'adding  user management Details to database','user_management_details','user_details_id',$muser_id);
							
						}
					}
				}
			}
		}
	}
	
	function active_users($user_id){
		$data = array(
					'user_status' => 'ACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
		$this->General_Model->insert_log('4','active_users',json_encode($data),'updating  user status to active','user_details','user_details_id',$user_id);
	}
	
	function inactive_users($user_id){
		$data = array(
					'user_status' => 'INACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
		$this->General_Model->insert_log('4','inactive_users',json_encode($data),'updating  user status to inactive','user_details','user_details_id',$user_id);
	}
	
	function delete_users($user_id){
		$this->db->where('user_details_id', $user_id);
		$this->db->delete('user_details'); 
		$this->General_Model->insert_log('4','delete_users',json_encode(array()),'deleting  user details from database','user_details','user_details_id',$user_id);				
	}
	
	

	function update_users($update,$user_id,$user_profile_name){
		if(!isset($update['user_status']))
			$update['user_status'] = "INACTIVE";
			
		$update_data_address = array(
								'address' 		=> $update['address'],
								'city_name' 	=> $update['city'],
								'zip_code' 		=> $update['zip_code'],
								'state_name' 	=> $update['state_name'],
								'country_id' 	=> $update['country']					
							);
		$this->db->where('address_details_id', $update['address_details_id']);
		$this->db->update('address_details', $update_data_address);
		$this->General_Model->insert_log('4','update_users',json_encode($update_data_address),'updating  user address  Details to database','address_details','address_details_id',$update['address_details_id']);
		
		$update_data_user = array(
								'user_type_id' 				=> '1',
								'user_name' 				=> $update['first_name']."-".$update['last_name'],
								'user_home_phone' 			=> $update['phone_no'],
								'user_cell_phone' 			=> $update['mobile_no'],					
								'user_profile_pic' 			=> $user_profile_name,					
								'user_status' 				=> $update['user_status'],
								'Address'					=> $update['address'],
								'City' 						=> $update['city'],
								'country'  					=> $update['country'],
								'State'						=> $update['state_name'],
								'Zip_code'					=> $update['zip_code'],				
							);		
		$this->db->where('user_details_id', $user_id);						
		$this->db->update('user_details',$update_data_user);
		$this->General_Model->insert_log('4','update_users',json_encode($update_data_user),'updating  user  Details to database','user_details','user_details_id',$user_id);
		
		$this->db->where('user_details_id', $user_id);
		$this->db->delete('user_management_details'); 
		if(false){
			for($d=0;$d<count($update['doamin']);$d++){
				for($u=0;$u<count($update['user_type']);$u++){
					for($p=0;$p<count($update['product']);$p++){
						for($a=0;$a<count($update['api']);$a++){
							$update_data_user_management = array(
										'user_details_id' 			=> $user_id,
										'domain_details_id' 		=> $update['doamin'][$d],
										'user_type_id' 				=> $update['user_type'][$u],
										'product_details_id' 		=> $update['product'][$p],					
										'api_details_id' 			=> $update['api'][$a],
										'country_id' 				=> '1'				
									);	
							$this->db->insert('user_management_details',$update_data_user_management);	
							$this->General_Model->insert_log('4','update_users',json_encode($update_data_user_management),'updating  user management Details to database','user_management_details','user_details_id',$user_id);

						}
					}
				}
			}
		}
	}

	public function export_b2c_users($ids) {
        $this->db->where_in('user_details_id', $ids);
        return $this->db->get('user_details');
    }

    public function get_country_list_user(){
		$this->db->select('*')->from('country_details');
		$query = $this->db->get();
	    if ( $query->num_rows > 0 ) {
         return $query->result();
        }
      return false;
   	}

   	 public function getBookingByParentPnr($user_id = ""){
   	 	
       $condition= "`booking_global`.`module` in ('FLIGHT','Flight-CRS')";
        $this->db->join('booking_flight','booking_global.ref_id = booking_flight.ID');
	   $this->db->where($condition);
	   $this->db->where('booking_global.user_id', $user_id);
	  /* if($user_id != ""){
       $this->db->where('booking_global.user_id',$user_id);
   }*/
        $query=$this->db->get('booking_global');
        //echo $this->db->last_query(); exit();
        if($query->num_rows() ==''){
            return '';
        }else{
            return $query->result();
        }
    }

     public function getHotelBookingByParentPnr($user_id){
        $condition= "`booking_global`.`module` in ('HOTEL','CRS-Hotel')";
        $this->db->join('booking_hotel','booking_hotel.id = booking_global.ref_id');
        $this->db->where($condition);
        $this->db->where('booking_global.user_id', $user_id);
        //$this->db->where('booking_global.user_id',$user_id);
        //$this->db->where('booking_global.module','CRS-Hotel');
        $query=$this->db->get('booking_global');
        if($query->num_rows() ==''){
            return '';
        }else{
            return $query->result();
        }
    }

     public function getCarBookingByParentPnr($user_id){
            //$condition= " `booking_global`.`module` = 'Car-CRS'";
            // $condition= "`booking_global`.`module` in (Car-CRS')";
            $this->db->join('booking_car','booking_global.ref_id = booking_car.booking_car_id');
            $this->db->where('booking_global.module', 'Car-CRS');
            $this->db->where('booking_global.user_id', $user_id);
          //  $this->db->where('booking_global.user_id',$user_id);
            $query=$this->db->get('booking_global');
            if($query->num_rows() ==''){
                return '';
            }else{
                return $query->result();
            }
        }

        public function getBundleBookingByParentPnr($user_id){
            //$condition= " `booking_global`.`module` = 'Car-CRS'";
            // $condition= "`booking_global`.`module` in (Car-CRS')";
            $this->db->join('booking_bundle','booking_global.ref_id = booking_bundle.id');
            $this->db->where('booking_global.module', 'Bundle');
            $this->db->where('booking_global.user_id',$user_id);
            $query=$this->db->get('booking_global');
            if($query->num_rows() ==''){
                return '';
            }else{
                return $query->result();
            }
        }
}
?>
