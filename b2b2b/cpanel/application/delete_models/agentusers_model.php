<?php
class Agentusers_Model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function getAgentadminList($admin_id){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('user_type_id', 4);
		$this->db->where('branch_id', $admin_id);
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
   
   function getSubAgentsUsers($agent_id){
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('user_details_id', $agent_id);
		
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	

	function addUseragentDetails($input,$user_profile_name){
		if(!isset($input['user_status']))  
			$input['user_status'] = "INACTIVE";
		
		try {
		$insert_data_address = array(
								'address' 		=> $input['address'],
								'city_name' 	=> $input['city'],
								'zip_code' 		=> $input['zip_code'],
								'state_name' 	=> $input['state_name'],
								'country_id' 	=> $input['country']					
							);			
		$this->db->insert('address_details',$insert_data_address);
		$address_details_id = $this->db->insert_id();
		$this->General_Model->insert_log('12','addUseragentDetails',json_encode($insert_data_address),'Adding  user to database','address_details','address_details_id',$address_details_id);
	} catch(Exception $e){
		return $e;
	}
      
     try{ 
		$insert_data_admin = array(
								'address_details_id' 		=> $address_details_id,
								'user_name' 				=> $input['first_name']." ".$input['last_name'],
								'user_type_id' 				=> 4,
								'position'					=> $input['position'],
								'agent_tradingname'			=> $input['agency_tradename'],
								'agent_licenseno'			=> $input['agent_licenseno'],
								'qq'						=> $input['qq'],
								'wechat_number'				=> $input['wechatnumber'],
								'grouptravel'				=> $input['grouptravel'],
								'fit'						=> $input['fit'],
								'user_email' 				=> $input['email_id'],
								'user_cell_phone' 			=> $input['mobile_no'],
								'company_name'				=> $input['company_name'],
								'branch_id'					=> json_decode(base64_decode($input['branch_id'])),
								'user_account_number' 		=> 'PROVAB-'.rand(1,100),					
								'user_profile_pic' 			=> $user_profile_name,
								'country_id' 	            => $input['country'],			
								'user_status' 				=> $input['user_status'],					
								'user_creation_date_time' 	=> (date('Y-m-d H:i:s'))					
							);			
		$this->db->insert('user_details',$insert_data_admin);
		$user_details_id = $this->db->insert_id();
		$this->General_Model->insert_log('12','add_useragent_details',json_encode($insert_data_admin),'Adding  user to database','user_details','user_details_id',$user_details_id);
	} catch(Exception $ex) {
		return $e;
	}
	
	 try{ 
		$insert_data_admin_login = array(
									'user_details_id' 			=> $user_details_id,
									'user_name' 	=> $input['email_id'],
									'user_password' 	=> "AES_ENCRYPT(".$input['new_password'].",'".SECURITY_KEY."')",
									'user_type_id'      => '2',
									'admin_pattren' 	=> ''					
								);			
		$this->db->insert('user_login_details',$insert_data_admin_login);
		$update_sql		= 	"UPDATE user_login_details SET user_password  =  AES_ENCRYPT('".$input['new_password']."','".SECURITY_KEY."') WHERE user_details_id  =  '$user_details_id' ";
		$this->db->query($update_sql);
		$this->General_Model->insert_log('12','add_useragent_details',json_encode($insert_data_admin_login),'Adding  user to database','user_login_details','user_details_id',$user_details_id);
	} catch(Exception $ex1) {
		return $e;
	}
	
	}
	
	function activeUsers($user_id){
		$data = array(
					'user_status' => 'ACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
		$this->General_Model->insert_log('4','activeUsers',json_encode($data),'updating  user status to active','user_details','user_details_id',$user_id);
	}

	function inactiveUsers($user_id){
		$data = array(
					'user_status' => 'INACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
		$this->General_Model->insert_log('4','inactiveUsers',json_encode($data),'updating  user status to inactive','user_details','user_details_id',$user_id);
	}

	 function getB2B2BAgentadminList($admin_id,$parent_user){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('user_type_id', 4);
		$this->db->where('user_details_id', $admin_id);
		$this->db->where('branch_id', $parent_user);
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function updateAdminagent($update,$user_id,$user_profile_name){
		
		if(!isset($update['user_status']))
			$update['user_status'] = "INACTIVE";
			
		$update_data_address = array(
								'address' 		=> $update['address'],
								'city_name' 	=> $update['city'],
								'zip_code' 		=> $update['zip_code'],
								'state_name' 	=> $update['state_name'],
								'country_id' 	=> $update['country']					
							);
		
		$this->db->where('address_details_id', $update['address_details_id']);
		$this->db->update('address_details', $update_data_address);
		
		$this->General_Model->insert_log('4','updateAdminagent',json_encode($update_data_address),'updating  user address  Details to database','address_details','address_details_id',$update['address_details_id']);
		
		$update_data_user = array(
								'user_type_id' 				=> $update['user_type'][0],
								'company_name' 				=> $update['company_name'],
								'position'					=> $update['position'],
								'agent_tradingname'			=> $update['agency_tradename'],
								'agent_licenseno'			=> $update['agent_licenseno'],
								'qq'						=> $update['qq'],
								'wechat_number'				=> $update['wechatnumber'],
								'grouptravel'				=> $update['grouptravel'],
								'fit'						=> $update['fit'],
								
								'user_home_phone' 			=> $update['phone_no'],
								'user_cell_phone' 			=> $update['mobile_no'],
								
								'user_profile_pic' 			=> $user_profile_name,					
								'user_status' 				=> $update['user_status'],
								'country_id' 				=> $update['country']				
							);		
		$this->db->where('user_details_id', $user_id);						
		$this->db->update('user_details',$update_data_user);
		$this->General_Model->insert_log('4','updateAdminagent',json_encode($update_data_user),'updating  user  Details to database','user_details','user_details_id',$user_id);
		
		
	}

	function getAgentActivationadminList($admin_id){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('user_type_id', 2);
		$this->db->where('user_details_id', $admin_id);
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function inactive_users($user_id){
		$data = array(
					'user_status' => 'INACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
		$this->insert_log('4','inactive_users',json_encode($data),'updating  user status to inactive','user_details','user_details_id',$user_id);
	}
}
?>
