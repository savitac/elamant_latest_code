<?php
class Airline_Model extends CI_Model {

    function __construct(){
        
        parent::__construct();
    }

    function getAirlineList($airline_details_id = ''){
		$this->db->select('*');
		$this->db->from('airlines_list');
		if($airline_details_id !='')
			$this->db->where('id', $airline_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addAirlineDetails($input,$airline_image){

		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
		$insert_data = array(
							'airline_name' 			=> $input['airline_name'],
							'airline_code' 			=> $input['airline_code'],
							'airline_image'         => $airline_image,
							'status' 				=> $input['status']				
						);

		$this->db->insert('airlines_list',$insert_data);
	}

	function activeAirline($airline_details_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('id', $airline_details_id);
		$this->db->update('airlines_list', $data); 
	}
	
	function inactiveAirline($airline_details_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('id', $airline_details_id);
		$this->db->update('airlines_list', $data); 
	}
	
	function deleteAirline($airline_details_id){
		$this->db->where('id', $airline_details_id);
		$this->db->delete('airlines_list'); 
	}
	
	function updateAirline($update,$image_info_name,$airline_details_id){
		if(!isset($update['status']))
			$update['status'] = "INACTIVE";
		$update_data = array(
							'airline_name' 			=> $update['airline_name'],
							'airline_code' 			=> $update['airline_code'],
							'airline_image'			=> $image_info_name,
							'status' 				=> $update['status']				
						);

		$this->db->where('id', $airline_details_id);
		$this->db->update('airlines_list', $update_data);
	}
}
?>
