<?php
class Popularhotels_Model extends CI_Model {
	
	
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_deals_categories($category_id = ''){
		$this->db->select('*');
        $this->db->from('best_deals');
        if($category_id!=''){
			$this->db->where('best_deals_id',$category_id);
		}
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
	}
	
	function add_category_details($input){
		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
		$insert_data = array(
							'users_id' 		=> $input['agents_id'],
							'country'   	=> $input['country'][0],
							'position'				=>$input['position'],
							'status' 				=> $input['status'],
							'creation_time'			=> (date('Y-m-d H:i:s'))					
						);			
		$this->db->insert('best_deals',$insert_data);
	}
	
	function active_category($category_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('best_deals_id', $category_id);
		$this->db->update('best_deals', $data); 
	}
	function inactive_category($category_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('best_deals_id', $category_id);
		$this->db->update('best_deals', $data); 
	}
	function delete_category($category_id){
		$this->db->where('best_deals_id', $category_id);
		$this->db->delete('best_deals'); 
	}
    function update_category($update,$category_id){
		$update_data = array(
							
							'status' 			=> $update['status']					
						);				
		$this->db->where('best_deals_id', $category_id);
		$this->db->update('best_deals', $update_data);
	}
	
	function get_deals_list($id='',$category_id=''){
		$this->db->select('*');
        $this->db->from('hotel_deals');
        if($id!=''){
			$this->db->where('hotel_deals_id',$id);
		}
		if($category_id!='')
			$this->db->where('users_id', $category_id);
        $query = $this->db->get(); //echo $this->db->last_query(); exit();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
	}
	
	public function add_deal($input,$category_id,$image){ 
		if(!isset($input['deals_status']))
			$input['deals_status'] = "INACTIVE";
			
		$date_range[0] = $date_range[1] = '';
		if(isset($input['date_range']) && $input['date_range']!='')
			$date_range = explode("-",$input['date_range']);
		$cityname = explode(",", $input['city_name']);
		$cityname = $cityname[1].','.$cityname[2]; 
		$insert_data = array(
							'hotel_name' 		=> $input['hotel_name'],
							'city_name' 		=> $cityname,
							'checkin_date' 		=> date('d-m-Y', strtotime($date_range[0])),
							'checkout_date' 	=> date('d-m-Y', strtotime($date_range[1])),
							'price_offer' 		=> $input['price_offer'],
							'expiry_date'	 	=> $input['expdate'],
							'hotel_image' 		=> $image,
							'position' 			=> $input['position'],
							'users_id'			=> $category_id,
							'star' 		        => $input['star_rating'],
							'status'			=> $input['deals_status'],
							'creation_time'		=> (date('Y-m-d H:i:s'))					
						);	 //echo '<pre>'; print_r($insert_data); exit();		
		$this->db->insert('hotel_deals',$insert_data);
	}
	
	function inactive_deal($id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('hotel_deals_id', $id);
		$this->db->update('hotel_deals', $data); 
	}
	function active_deal($id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('hotel_deals_id', $id);
		$this->db->update('hotel_deals', $data); 
	}
	function update_deal($update, $id, $image_name){ 
		if(!isset($update['status']))
			$update['status'] = "INACTIVE";
		$date_range[0] = $date_range[1] = '';
		if(isset($update['date_range']) && $update['date_range']!='')
			//$date_range = explode("-",$update['date_range']);
			$test =  explode("-", $update['date_range']);
				
		$update_data = array(
							'hotel_name' 		=> $update['hotel_name'],
							
							//'checkin_date' 		=> date('d-m-Y', strtotime($test[0])),
							//'checkout_date' 	=> date('d-m-Y', strtotime($test[1])),
							'price_offer' 		=> $update['price_offer'],
							
							'hotel_image' 		=> $image_name,
							'position' 			=> $update['position'],
							'star' 		        => $update['star_rating'],
							'status'			=> $update['deals_status'],
							'updation_time'		=> (date('Y-m-d H:i:s'))											
						); //echo '<pre>'; print_r($update_data); exit();
		$this->db->where('hotel_deals_id', $id);
		$this->db->update('hotel_deals', $update_data);	
	}
	
	function delete_deal($id){
		$this->db->where('hotel_deals_id', $id);
		$this->db->delete('hotel_deals'); 
	}

	public function getAirportList($query){
        $this->db->distinct();
        $this->db->like('city_name', $query); 
        $this->db->limit(8);
        return $this->db->get('api_hotels_cities');

    }
    public function get_Hotel_Name($value)
    {	
    	$city = $value['City_name'];
    	$this->db->select('*');
		$this->db->from('gta_Hotels');
		$this->db->join('gta_cities', 'gta_cities.city_code = gta_Hotels.CityCode');
		
		$this->db->where('gta_cities.city_name',trim($city));
		$query=$this->db->get(); 
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query;
		}
}

	function getUsers($users = ''){
		$this->db->select('*');
        $this->db->from('user_details');
        if($users!=''){
			$this->db->where('user_details_id',$users);
		}
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
	}
}
?>
