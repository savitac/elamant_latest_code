<?php
class Noticeboard_Model extends CI_Model {

    function __construct(){
        
        parent::__construct();
    }

    function getNoticeboardList($noticeboard_id = ''){
		$this->db->select('*');
		$this->db->from('noticeboard');
		if($noticeboard_id !='')
			$this->db->where('noticeboard_id', $noticeboard_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
    function addNoticeboardDetails($input){

		if(!isset($input['status']))
			$input['status'] = "INACTIVE";
	  
	    if(isset($input['b2b_agents']))
	        $agent_ids = implode(",", $input['b2b_agents']);
	    
	    
	 
		$insert_data = array(
							'b2b_id' 			    => $agent_ids,
							'message' 		=>  trim($input['noticemessage']),
							'status' 				=> $input['status'],
							'creation_date' 	    => (date('Y-m-d H:i:s'))				
						);
        $this->db->insert('noticeboard',$insert_data);
	}

	function activeNoticeboard($noticeboard_id){
		$data = array(
					'status' => 'ACTIVE'
					);
		$this->db->where('noticeboard_id', $noticeboard_id);
		$this->db->update('noticeboard', $data); 
	}
	
	function inactiveNoticeboard($noticeboard_id){
		$data = array(
					'status' => 'INACTIVE'
					);
		$this->db->where('noticeboard_id', $noticeboard_id);
		$this->db->update('noticeboard', $data); 
	}
	
	function deleteNoticeboard($noticeboard_id){
		$this->db->where('noticeboard_id', $noticeboard_id);
		$this->db->delete('noticeboard'); 
	}
	
	

	function getAgentList(){
		 
		 /*if($user_id != ""){
			 $subsql = "and user_details_id in(".$user_id.")";
		 }*/
		
		$sql = "select * from user_details where `branch_id` = 0 AND `user_type_id` = '2'";
		
		$query=$this->db->query($sql);
		if($query->num_rows() ==''){
			$data['user_info'] = '';
		}else{
			$data['user_info'] = $query->result();
		}
		return $data;	
	} 
}
?>
