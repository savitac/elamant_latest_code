<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Admin extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		
		$this->load->model('General_Model');
		$this->load->model('Admin_Model');
		$this->load->model('Roles_Model');
		$this->load->model('Domain_Model');
		$this->load->model('Usertype_Model');
		$this->load->model('Product_Model');
		$this->load->model('Api_Model');
		$this->load->model('Email_Model');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
		
	}
   
   function admin_list(){
	 	$admin 					= $this->General_Model->getHomePageSettings();
		$admin['admin_list'] 	= $this->Admin_Model->get_admin_list();
		$this->load->view('admin/admin_list',$admin);
	}
	function logList()
	{
		$admin 					= $this->General_Model->getHomePageSettings();
		$admin['admin_list'] 	= $this->General_Model->get_log_list();
		$this->load->view('log/log_list',$admin);
	}
	function add_admin(){
		
		    $admin 					=  $this->General_Model->getHomePageSettings();
			$admin['country'] 		=  $this->General_Model->get_country();
			$admin['admin_type']	=  $this->Roles_Model->get_roles();
			
		if(count($_POST) > 0){
			$form_validator = $this->profileFormValidation();
		
			if($form_validator == false){
			 $this->load->view('admin/add_admin',$admin);	
			}else{
			
			$email = $_POST['email_id'];
			
			$Query="select * from  admin_details  where admin_email ='".$email."'";
			$query=$this->db->query($Query);
			if ($query->num_rows() > 0)
		    {
			
			$data['status'] = '<div class="alert alert-block alert-danger">
							   <a href="#" data-dismiss="alert" class="close">×</a>
							   <h4 class="alert-heading">Already Email Id Registered!</h4>
							   kindly Use With Another Email Address !!!
							   </div>';
			$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$res = "";
			for ($i = 0; $i < 10; $i++) {
				$res .= $chars[mt_rand(0, strlen($chars)-1)];
			}
			
			$this->load->view('admin/add_admin',$data);
		
		}else{
			
			 if($_FILES['image_info']['name'] != ''){
			 $user_profile_name = $this->General_Model->upload_image($_FILES, 'users');
		 }else{
			 $user_profile_name = "";
		 }
		
			try{
		    $this->General_Model->begin_transaction() ;
			$this->Admin_Model->add_admin_details($_POST,$user_profile_name);
			$this->Email_Model->add_admin_send_mail($_POST);
			$this->General_Model->commit_transaction() ;
			redirect('admin/admin_list','refresh');
		
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction() ;
			return $e;
		}
		}
        }
        }else{
			
		    $this->load->view('admin/add_admin',$admin);
		}
	}

	function check_unique_email($email){
		if($email!=''){
			if(preg_match("/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $email)){
				$Status 	=	$this->Admin_Model->get_all_admin_email($email);
				if($Status  == "YES") {
					echo '<span class="nosuccess validate-has-error">This Email Id already Exists, Please Choose diffrent Email Id</span>';exit;
				}else{
					echo '<span class="success" style="color: green;">valid</span>';exit;
				}
			}else{
				echo '<span class="nosuccess">Please Enter the Valid Email Id</span>';exit;
			}
		}else{	
			 echo '<span class="nosuccess">Please Enter the Valid Email Id</span>';exit;
		}
	}
	
	function active_admin($admin_id1){
		$admin_id 	= json_decode(base64_decode($admin_id1));
		$admin = $this->Admin_Model->get_admin_list($admin_id);
		if($admin_id != ''){
			$this->Admin_Model->active_admin($admin_id);
			$admin['email_template'] = $this->Email_Model->get_email_template('add_subadmin_active')->row();
			$this->Email_Model->send_subadmin_active($admin);
		}
		redirect('admin/admin_list','refresh');
	}
	
	function inactive_admin($admin_id1){
		$admin_id 	= json_decode(base64_decode($admin_id1));
		$admin = $this->Admin_Model->get_admin_list($admin_id);
		if($admin_id != ''){
			$this->Admin_Model->inactive_admin($admin_id);
			$admin['email_template'] = $this->Email_Model->get_email_template('add_subadmin_inactive')->row();
			$this->Email_Model->send_subadmin_active($admin);
		}
		redirect('admin/admin_list','refresh');
	}


	function delete_admin($admin_id1){
		$admin_id 	= json_decode(base64_decode($admin_id1));
		if($admin_id != ''){
			$this->Admin_Model->delete_admin($admin_id);
		}
		redirect('admin/admin_list','refresh');
	}
	
	function edit_admin($admin_id1)
	{
		
		$admin_id 	= json_decode(base64_decode($admin_id1)); 
		if($admin_id != ''){
			$admin 				= $this->General_Model->getHomePageSettings();
			$admin['domain'] 	=  $this->Domain_Model->get_domain_list();
			$admin['product'] 	=  $this->Product_Model->getProductList();
			$admin['country'] 	=  $this->General_Model->get_country_details();
			$admin['user_type']	=  $this->Usertype_Model->get_admin_type_list();
			$admin['api']		=  $this->Api_Model->getApiList();
			$admin['admin'] 	= $this->Admin_Model->get_admin_list($admin_id);
			$this->load->view('admin/edit_admin',$admin);
		}else{
			redirect('admin/admin_list','refresh');
		}
	}
	
	function update_admin($admin_id1)
	{
		$admin_id 	= json_decode(base64_decode($admin_id1)); 
		if($admin_id != ''){  
			if(count($_POST) > 0){
				$form_validator = $this->profileFormValidation();
		
			if($form_validator == false){
			 $this->load->view('admin/edit_admin',$admin_id);	
			}else{
			
				$image_info_name = $this->General_Model->upload_image($_FILES, 'users', $_REQUEST['old_image']);
				try{
			    $this->Admin_Model->update_admin($_POST,$admin_id, $image_info_name);
				redirect('admin/admin_list','refresh');
			} catch(Exception $ex) {
				$this->General_Model->rollback_transaction() ;
				return $ex;
		  } 
			}
			}else if($admin_id!=''){
				redirect('admin/edit_admin/'.$admin_id,'refresh');
			}else{
				redirect('admin/admin_list','refresh');
			}
		}else{
			redirect('admin/admin_list','refresh');
		}
	}

function profileFormValidation()
  {
	  $this->form_validation->set_rules('first_name', 'First Name', 'required', 'required|min_length[1]|max_length[50]');
	 $this->form_validation->set_rules('last_name', 'Last Name', 'required', 'required|min_length[1]|max_length[50]');
	 $this->form_validation->set_rules('mobile_no', 'Phone Number', 'required|min_length['.minNameLength.']|max_lengt['.maxNameLength.']');
	 $this->form_validation->set_rules('address', 'Address','required|min_length['.minAddressLength.']|max_lengt['.maxAddressLength.']');
	 $this->form_validation->set_rules('city', 'City Name', 'required|min_length['.minCityLength.']|max_lengt['.maxCityLength.']');
	 $this->form_validation->set_rules('state_name', 'State', 'required|min_length['.minCityLength.']|max_lengt['.minCityLength.']');
	 $this->form_validation->set_rules('zip_code', 'Zip Code','required|min_length['.minZipCode.']|max_lengt['.maxZipCode.']');
	 return $this->form_validation->run();
  }

}
