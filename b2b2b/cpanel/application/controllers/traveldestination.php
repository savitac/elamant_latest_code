<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Traveldestination extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Traveldestination_Model');
		$this->load->model('Banner_Model');
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
	}

	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function index(){
		$travel 				= $this->General_Model->getHomePageSettings();
		$travel['travel_list'] 	= $this->Traveldestination_Model->get_travel_list();
		
		$this->load->view('traveldestination/traveldestination_list',$travel);
	}
	
	function traveldestinationList(){
		$travel 				= $this->General_Model->getHomePageSettings();
		$travel['travel_list'] 	= $this->Traveldestination_Model->get_travel_list();
		$this->load->view('traveldestination/traveldestination_list',$travel);
	}
	
	function addTraveldestination(){
		if(count($_POST) > 0){ //echo '<pre>'; print_r($_POST); exit();
			$user_profile_name = $this->General_Model->upload_image($_FILES, 'traveldestination');
			$this->Traveldestination_Model->add_traveldestination_details($_POST,$user_profile_name);
			redirect('traveldestination/traveldestinationList','refresh');
		}else{
			$travel = $this->General_Model->getHomePageSettings();
			$travel['agents'] 		= $this->Banner_Model->getAgentList();
			$travel['country'] 	=  $this->General_Model->get_country_details();
			$this->load->view('traveldestination/add_traveldestination',$travel);
		}
	}
	
	function activeTraveldestination($travel_id){ 
		$travel_id 	= json_decode(base64_decode($travel_id));
		if($travel_id != ''){
			$this->Traveldestination_Model->active_traveldestination($travel_id);
		}
		redirect('traveldestination/traveldestinationList','refresh');
	}
	
	function inactiveTraveldestination($travel_id){
		$travel_id 	= json_decode(base64_decode($travel_id));
		if($travel_id != ''){
			$this->Traveldestination_Model->inactive_traveldestination($travel_id);
		}
		redirect('traveldestination/traveldestinationList','refresh');
	}
	
	function deleteTraveldestination($travel_id){
		$travel_id 	= json_decode(base64_decode($travel_id));
		if($travel_id != ''){
			$this->Traveldestination_Model->delete_traveldestinations($travel_id);
		}
		redirect('traveldestination/traveldestinationList','refresh');
	}
	
	function editTraveldestination($travel_id){
		$travel_id 	= json_decode(base64_decode($travel_id));
		if($travel_id != ''){
			$travel 					= $this->General_Model->getHomePageSettings();
			$travel['travel_list'] 		= $this->Traveldestination_Model->get_travel_list($travel_id);
			$travel['country'] 	=  $this->General_Model->get_country_details();
			$this->load->view('traveldestination/edit_traveldestination',$travel);
		} else {
			redirect('traveldestination/traveldestinationList','refresh');
		}
	}

	function update_traveldestination($travel_id){
		$travel_id 	= json_decode(base64_decode($travel_id));
		if(count($_POST) > 0){
			$image_info_name = $this->General_Model->upload_image($_FILES, 'traveldestination', $_REQUEST['old_image']);
			
			if($travel_id != ''){
				$this->Traveldestination_Model->update_traveldestination_details($_POST,$travel_id,$image_info_name);
			}
			redirect('traveldestination/traveldestinationList','refresh');
		}else if($travel_id!=''){
			redirect('traveldestination/editTraveldestination/'.$travel_id,'refresh');
		}else{
			redirect('traveldestination/traveldestinationList','refresh');
		}
	}

    public function getHotelCity()
	{
		$term = $this->input->get('term');
        $term = trim(strip_tags($term));
        $hotelsp = $this->Traveldestination_Model->getAirportList($term)->result();
        foreach($hotelsp as $hotelp){
            $apts['label'] = $hotelp->cityName.', '.$hotelp->countryName;
            $apts['value'] = $hotelp->cityName.', '.$hotelp->countryName;
            $apts['id'] = $hotelp->sid;
            $result[] = $apts; 
        }
        
        echo json_encode($result);
	}
}
