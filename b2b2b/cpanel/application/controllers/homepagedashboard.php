<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Homepagedashboard extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Homepagedashboard_Model');
		$this->load->library('form_validation');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	$this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function index(){
		$header 				= $this->General_Model->getHomePageSettings();
		$header['header_list'] 	= $this->Homepagedashboard_Model->getHeaderList();
		$this->load->view('homepagedashboard/homepagedashboard_list',$header);
	}
	
	function homepagedashboardList(){
		$header 				= $this->General_Model->getHomePageSettings();
		$header['header_list'] 	= $this->Homepagedashboard_Model->getHeaderList();
		$this->load->view('homepagedashboard/homepagedashboard_list',$header);
	}
	
	function addhomepagedashboard(){
		if(count($_POST) > 0){
			$this->Homepagedashboard_Model->addHomepagedashboardDetails($_POST);
			redirect('homepagedashboard/homepagedashboardList','refresh');
		}else{
			$header = $this->General_Model->getHomePageSettings();
			$this->load->view('homepagedashboard/add_homepagedashboard',$header);
		}
	}
	
	function activehomepagedashboard($header_id1){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Homepagedashboard_Model->activehomepagedashboard($header_id);
		}
		redirect('homepagedashboard/homepagedashboardList','refresh');
	}
	
	function inactivehomepagedashboard($header_id1){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Homepagedashboard_Model->inactivehomepagedashboard($header_id);
		}
		redirect('homepagedashboard/homepagedashboardList','refresh');
	}
	
	
	function edithomepagedashboard($header_id1)
	{
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$header 					= $this->General_Model->getHomePageSettings();
			$header['header_list'] 		= $this->Homepagedashboard_Model->getHeaderList($header_id);
			$this->load->view('homepagedashboard/edit_homepagedashboard.',$header);
		}else{
			redirect('homepagedashboard/homepagedashboardList','refresh');
		}
	}

	function updatehomepagedashboard($header_id)
	{
		$header_id 	= json_decode(base64_decode($header_id)); 
		if($header_id != ''){			
			if(count($_POST) > 0){
				
				$this->Homepagedashboard_Model->updatehomepagedashboardDetails($_POST,$header_id);
				redirect('homepagedashboard/homepagedashboardList','refresh');
			}else if($header_id!=''){
				redirect('homepagedashboard/edithomepagedashboard/'.$header_id,'refresh');
			}else{
				redirect('homepagedashboard/homepagedashboardList','refresh');
			}
		}else{
			redirect('homepagedashboard/homepagedashboardList','refresh');
		}
	}
	
}
