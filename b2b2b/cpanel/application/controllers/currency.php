<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
error_reporting(-1);
class Currency extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Currency_Model');
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    

	function index(){
		$currency 					= $this->General_Model->getHomePageSettings();
		$currency['currency_list'] 	= $this->Currency_Model->getCurrencyList();
		$this->load->view('currency/currency_list',$currency);
	}
	
	
	function site_currency(){
		
	
		if(count($_POST) > 0){
			
		    $this->Currency_Model->add_SiteCurrency_Details($_POST);
			redirect('currency/site_currency','refresh');

		}else{
			
			$selected_currncies =  $this->Currency_Model->get_selected_site_currency();
		   $selected_currncies = isset( $selected_currncies['site_currency_ids'] )? explode(',',$selected_currncies['site_currency_ids']):'';
		  // print_r($selected_currncies);
		//	exit;
			//print_r($selected_currncies);exit;
			$currencies = $this->Currency_Model->get_currencies();
			$data['currencies']=$currencies;
			$data['selected_currncies'] = $selected_currncies;
			$this->load->view('currency/add_currency',$data);
		}
		
	}
	
	function addCurrency(){
		$currency = $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
			$form_validator = $this->currencyFormValidation('add');
			if($form_validator == FALSE  ) {
				$this->load->view('currency/add_currency',$currency);	
			    }else{
			$this->Currency_Model->addCurrencyDetails($_POST);
			redirect('currency','refresh');
		}
		}else{
			
			$this->load->view('currency/add_currency',$currency);
		}
	}
	
	function updateStatus($currencyid1, $status1){
		$currency_id 	= json_decode(base64_decode($currencyid1));
		 $status 	= json_decode(base64_decode($status1));
		
		if($currency_id != ''){
			$this->Currency_Model->updateStatus($currency_id, $status);
		}
		redirect('currency','refresh');
	}
	
	function editCurrency($currency_id1)
	{
		$currency_id 	= json_decode(base64_decode($currency_id1));
		if($currency_id != ''){
			$currency 		= $this->General_Model->getHomePageSettings();
			$currency['currency'] = $this->Currency_Model->getCurrencyList($currency_id);
			$this->load->view('currency/edit_currency',$currency);
		}else{
			redirect('currency','refresh');
		}
	}

	function updateCurrency($currency_id1){
		$currency_id 	= json_decode(base64_decode($currency_id1));
		if($currency_id != ''){
			if(count($_POST) > 0){
				$this->Currency_Model->update_currency($_POST,$currency_id);
				redirect('currency','refresh');
			}else if($currency_id!=''){
				redirect('currency','refresh');
			}else{
				redirect('currency','refresh');
			}
		}else{
			redirect('currency','refresh');
		}	
		$result = 'success';
		
		echo json_encode($result);	
	}

	function currencyUpdate(){	
		$country_currencies = $this->General_Model->get_currency_list();     
		//echo '<pre>'; print_r($country_currencies); exit();
		for($i=0; $i< count($country_currencies);$i++) 
		{
			$restricAr	= array();
			if(true){
				$value[$i] = round($this->ConverCurrency('USD',$country_currencies[$i]->currency_code),4);
				$data	   = array('value' => $value[$i]);
				$this->db->where('currency_details_id',$country_currencies[$i]->currency_details_id);
				$this->db->update('currency_details',$data);
			}
		}
		
		$flight_country_currencies =  $this->General_Model->get_flights_currency_list();     
		
		$result = 'Currency updated Successfully';
		echo $result;

	}
	function ConverCurrency($from_code, $to_code) {
		ini_set('max_execution_time', 600);
		$url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s='. $from_code . $to_code .'=X';
		$handle = @fopen($url, 'r');
	   	if ($handle) {
			$result = fgets($handle, 4096);
			fclose($handle);
			$allData = explode(',',$result); /* Get all the contents to an array */
			return $dollarValue = $allData[1];
	  	} else {
			return 0;
		}
	}



function currencyFormValidation($type)
  {
	 
	 $this->form_validation->set_rules('currency_code', 'Currency Code', 'required');
	 if($type == 'add') {
	    
	    $this->form_validation->set_rules('currency_code', 'Currency Code', 'required|xss_clean||is_unique[currency_details.currency_code]|max_length[3]');
	    }
	 return $this->form_validation->run();
  }
}
