<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting(E_ALL);
/**
 *
 * @package    Provab
 * @subpackage Bus
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */
error_reporting(0);
class Voucher extends CI_Controller {
	private $current_module;
	public function __construct()
	{
		parent::__construct();
		$this->load->library('booking_data_formatter');
		$this->load->library('provab_mailer');
		$this->load->library('session');
		$this->load->model('custom_db');
		$this->load->model('users_model');
		$this->current_module = $this->config->item('current_module');
		//$this->load->library('provab_pdf');
		
		//we need to activate bus api which are active for current domain and load those libraries
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 *
	 */
	function bus($app_reference, $booking_source='', $booking_status='', $operation='show_voucher',$email='')
	{
		//echo 'under working';exit;
		$this->load->model('bus_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->bus_model->get_booking_details($app_reference, $booking_source, $booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_bus_booking_data($booking_details, $this->current_module);
				$page_data['data'] = $assembled_booking_details['data'];
				
				if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
					$domain_address = $this->custom_db->single_table_records ( 'domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					if($assembled_booking_details['data']['booking_details'][0]['created_by_id'] > 0){
						$get_agent_info = $this->user_model->get_agent_info($assembled_booking_details['data']['booking_details'][0]['created_by_id']);
						if(!empty($get_agent_info)){
							$page_data['data']['address'] = $get_agent_info[0]['address'];
							$page_data['data']['logo'] = $get_agent_info[0]['logo'];
						}
					}
				
				}
				switch ($operation) {
					case 'show_voucher' : $this->template->view('voucher/bus_voucher', $page_data);
					break;
					case 'show_pdf' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->template->isolated_view('voucher/bus_pdf', $page_data);
						$create_pdf->create_pdf($get_view,'show');					
						break;
						
					case 'email_voucher' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->template->isolated_view('voucher/bus_pdf', $page_data);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$this->provab_mailer->send_mail($email, domain_name().' - Bus Ticket',$mail_template ,$pdf);
						break;
				}
			} else {
				redirect('security/log_event?event=Invalid AppReference');
			}
		} else {
			redirect('security/log_event?event=Invalid AppReference');
		}
	}

	

 
	/**
	 *
	 */
	function hotel($email='',$app_reference, $booking_source='', $booking_status='', $operation='show_voucher')
	{ 
		$this->load->model('hotel_model'); 
		if (empty($app_reference) == false) {
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data 
				$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, $this->current_module); 
				$page_data['data'] = $assembled_booking_details['data'];
				if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
				
					$domain_address = $this->custom_db->single_table_records ('domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					if($assembled_booking_details['data']['booking_details'][0]['created_by_id'] > 0){
						$get_agent_info = $this->user_model->get_agent_info($assembled_booking_details['data']['booking_details'][0]['created_by_id']);
						if(!empty($get_agent_info)){
							$page_data['data']['address'] = $get_agent_info[0]['address'];
							$page_data['data']['logo'] = $get_agent_info[0]['logo'];
						}
					}
				
				} 
				switch ($operation) {
					case 'show_voucher' : 		
					$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('vouchers/hotel_pdf', $page_data,TRUE);
						$pdf = $create_pdf->create_pdf($mail_template,'show');
					break;
					case 'show_pdf' :	 					
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->template->isolated_view('voucher/hotel_pdf', $page_data);						
						$create_pdf->create_pdf($get_view,'show');
						
					break;
					case 'email_voucher' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('vouchers/hotel_voucher', $page_data,TRUE);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$email_status=$this->provab_mailer->send_mail($email, domain_name().' - Hotel Ticket',$mail_template ,$pdf); 
						if($email_status=1)
						{
						$this->session->set_flashdata('email_status', '1');
						redirect('booking/hotelOrders','refresh'); 
						}  
						else  
						{
						$this->session->set_flashdata('email_status', '2');
						redirect('booking/hotelOrders','refresh');   
						} 
						break;
				}
			}
		}
	}
/*function load_flight_lib($source)
{
	$CI = &get_instance();
	switch ($source) {
		case PROVAB_FLIGHT_BOOKING_SOURCE :
			$CI->load->library('flight/provab_private', '', 'flight_lib');
			break;
		default : redirect(base_url());
	}
}*/
	/* commented by savita
	function flight($email='',$app_reference, $booking_source='', $booking_status='', $operation='show_voucher')
	*/
	function flight($app_reference, $booking_source='', $booking_status='', $operation='show_voucher',$email='')
	{

		$this->load->model('flight_model');
		if (empty($app_reference) == false) {

			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
/*debug($booking_details);
exit("43");*/
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//$this->load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2c');				
				$page_data['data'] = $assembled_booking_details['data'];
				/*debug($page_data);
				exit();*/
				switch ($operation) {
					case 'show_voucher' : $this->load->view('vouchers/flight_voucher', $page_data);
					break;
					case 'show_pdf' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->load->view('vouchers/flight_voucher', $page_data,TRUE);
						$create_pdf->create_pdf($get_view,'show'); 
						break;
			  		 case 'email_voucher':
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('vouchers/flight_pdf', $page_data,TRUE); 
						$pdf = $create_pdf->create_pdf($mail_template,'');  
						$email = $booking_details[0]['email'];
						$email_status=$this->provab_mailer->send_mail($email, domain_name().' - Flight Ticket',$mail_template ,$pdf);
						if($email_status=1)
						{
						$this->session->set_flashdata('email_status', '1');
						redirect('booking/flightOrders','refresh'); 
						}  
						else
						{
						$this->session->set_flashdata('email_status', '2');
						redirect('booking/flightOrders','refresh');   
						} 
						break;
				}  
			}
		}
	}
	function flight_email()
	{
		$app_reference=$_POST['app_reference'];
		$booking_source=$_POST['booking_source'];
		$booking_status=$_POST['status'];
		$operation="email_voucher";
		$email=$_POST['email'];  
		$this->load->model('flight_model');
		if (empty($app_reference) == false) {

			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);

			if ($booking_details['status'] == SUCCESS_STATUS) {
				//$this->load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				switch ($operation) {
				   case 'email_voucher': 
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('vouchers/flight_pdf', $page_data,TRUE);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$email_status=$this->provab_mailer->send_mail($email, domain_name().' - Flight Ticket',$mail_template ,$pdf);
						if($email_status=1)
						{
						$this->session->set_flashdata('email_status', '1');
						redirect('booking/flightOrders','refresh'); 
						}  
						else
						{
						$this->session->set_flashdata('email_status', '2');
						redirect('booking/flightOrders','refresh');   
						} 
						; 
						break; 
				}
			}
		}
	}
	function hotel_email()
	{
		$app_reference=$_POST['app_reference'];
		$booking_source=$_POST['booking_source'];
		$booking_status=$_POST['status'];
		$operation="email_voucher";
		$email=$_POST['email'];
		$this->load->model('hotel_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $booking_status);
			//debug($booking_details);die(); 
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, 'b2c');
				//debug($assembled_booking_details); exit;
				$page_data['data'] = $assembled_booking_details['data'];
                if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
				
					$domain_address = $this->custom_db->single_table_records ('domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					
				}

				switch ($operation) {
				   case 'email_voucher': 
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('vouchers/hotel_pdf', $page_data,TRUE);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$email_status=$this->provab_mailer->send_mail($email, domain_name().' - Hotel Ticket',$mail_template ,$pdf);
						if($email_status=1)
						{
						$this->session->set_flashdata('email_status', '1');
						redirect('booking/hotelOrders','refresh'); 
						}  
						else  
						{
						$this->session->set_flashdata('email_status', '2');
						redirect('booking/hotelOrders','refresh');   
						} 
						; 
						break; 
				}
			}
		}
	} 
	function flight_invoice($app_reference, $booking_source='', $booking_status='', $operation='show_voucher')
	{
		$this->load->model('flight_model');
		if (empty($app_reference) == false) {
			$data = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
			//debug($data);exit;
			if ($data['status'] == SUCCESS_STATUS) {
				//depending on booking source we need to convert to view array
				load_flight_lib($data['data']['booking_details']['booking_source']);
				$page_data = $this->flight_lib->parse_voucher_data($data['data']);
				$domain_details = $this->custom_db->single_table_records('domain_list', '*', array('origin' => $page_data['booking_details']['domain_origin']));
				$page_data['domain_details'] = $domain_details['data'][0];
				switch ($operation) {
					case 'show_voucher' : $this->template->view('voucher/flight_invoice', $page_data);
					break;
				}
			}
		}
	}
	function hotel_crs($app_reference, $booking_status='', $operation='show_voucher',$email='')
	{
		//error_reporting(E_ALL);
		// echo $booking_status;exit();
		$this->load->model('hotels_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->hotels_model->get_booking_crs_details($app_reference,$booking_status);
			   // debug($booking_details);exit();
			    if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				
			    //get agent address & logo for b2b voucher
			    	$page_data['data'] = $booking_details['data'];
				    $domain_address = $this->custom_db->single_table_records ('domain_list','address,domain_logo,phone,domain_name',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					$page_data['data']['phone'] = $domain_address['data'][0]['phone'];
					$page_data['data']['domainname'] = $domain_address['data'][0]['domain_name'];
						$get_agent_info = $this->users_model->get_agent_info($booking_details['data']['booking_details'][0]['user_id']);
				
						if(!empty($get_agent_info)) {
							$page_data['data']['address'] = $get_agent_info[0]['address'];
							$page_data['data']['domainname'] = (!empty($get_agent_info[0]['agency_name']) ? $get_agent_info[0]['agency_name'] : $domain_address['data'][0]['domain_name']);
							$page_data['data']['logo'] = (!empty($get_agent_info[0]['logo']) ? $get_agent_info[0]['logo'] : $domain_address['data'][0]['domain_logo']);
						}
					//print_r($page_data);
					//exit;
				    switch ($operation) {
					case 'show_voucher' : 					
                    $this->load->view('vouchers/hotel_crs_new', $page_data);
                    // exit("string");
					break;
					case 'show_pdf' :
						//print_r("string5");
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						//debug($page_data);exit();
						$get_view = $this->load->view('vouchers/hotel_crs_pdf', $page_data);
						$create_pdf->create_pdf($get_view,'show');
						
					break;
					case 'email_voucher' :
						//debug($page_data);exit;
						/*$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->template->isolated_view('voucher/hotel_pdf', $page_data);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$this->provab_mailer->send_mail($email, domain_name().' - Hotel Ticket',$mail_template ,$pdf);*/
						if($email == ''){
		    				$email = $voucher_details['other'][$b]->check_in_date->email_id;
		    			}
						$this->load->model ( 'hotels_model' );
						$this->load->library ( 'provab_mailer' );
						$mail_template = $this->load->isolated_view('vouchers/hotel_voucher_crs', $page_data,true);
						$this->session->set_flashdata('email_message', 'Email sent successfully');
		                $email_subject = "Hotel Booking Confirmation-".$page_data['data']['booking_details'][0]['parent_pnr'];

						$mail_status = $this->provab_mailer->send_mail($email, $email_subject, $mail_template, "");
						break;
				}
			}
		}
	}
	function transferv1($app_reference, $booking_source='', $booking_status='', $operation='show_voucher',$email='')
	{
		//error_reporting(E_ALL);
		$this->load->model('transferv1_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->transferv1_model->get_booking_details($app_reference, $booking_source, $booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//load_transferv1_lib(PROVAB_TRANSFERV1_BOOKING_SOURCE);
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_transferv1_booking_data($booking_details, $this->current_module);				
				$page_data['data'] = $assembled_booking_details['data'];
				
			if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
					
					$domain_address = $this->custom_db->single_table_records ( 'domain_list','address,domain_logo,phone,domain_name',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					$page_data['data']['phone'] = $domain_address['data'][0]['phone'];
					$page_data['data']['domainname'] = $domain_address['data'][0]['domain_name'];

					if($assembled_booking_details['data']['booking_details'][0]['created_by_id'] > 0){
						$get_agent_info = $this->user_model->get_agent_info($assembled_booking_details['data']['booking_details'][0]['created_by_id']);
						if(!empty($get_agent_info)){
							$page_data['data']['address'] = $get_agent_info[0]['address'];
							$page_data['data']['logo'] = (!empty($get_agent_info[0]['logo']) ? $get_agent_info[0]['logo'] : $domain_address['data'][0]['domain_logo']);
							$page_data['data']['phone'] = $get_agent_info[0]['phone'];
							$page_data['data']['domainname'] = $get_agent_info[0]['agency_name'];
						}
					}
			
				}
				switch ($operation) {
					case 'show_voucher' : $this->load->view('vouchers/transferv1_voucher', $page_data);
					break;
					case 'show_pdf' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->load->view('vouchers/transfer_pdf', $page_data, TRUE);
						$create_pdf->create_pdf($get_view,'show');
						break;
				   case 'email_voucher':
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('voucher/transfer_pdf', $page_data);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$this->provab_mailer->send_mail($email, domain_name().' - Transfers Ticket',$mail_template ,$pdf);
						break;
				}
			}
		}
	}

	/**
	 *Sightseeing Voucher
	 */
	function sightseeing($app_reference, $booking_source='', $booking_status='', $operation='show_voucher',$email='')
	{	
		
		$this->load->model('sightseeing_model');
		
		if (empty($app_reference) == false) {
			$booking_details = $this->sightseeing_model->get_booking_details($app_reference, $booking_source, $booking_status);
			
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_sightseeing_booking_data($booking_details, $this->current_module);

				$page_data['data'] = $assembled_booking_details['data'];
				if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
				
					$domain_address = $this->custom_db->single_table_records ('domain_list','address,domain_logo,phone,domain_name',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					$page_data['data']['phone'] = $domain_address['data'][0]['phone'];
					$page_data['data']['domainname'] = $domain_address['data'][0]['domain_name'];
					if($assembled_booking_details['data']['booking_details'][0]['created_by_id'] > 0){
						$get_agent_info = $this->user_model->get_agent_info($assembled_booking_details['data']['booking_details'][0]['created_by_id']);
						if(!empty($get_agent_info)){
							$page_data['data']['address'] = $get_agent_info[0]['address'];
							$page_data['data']['logo'] = $get_agent_info[0]['logo'];
							$page_data['data']['phone'] = $get_agent_info[0]['phone'];
							$page_data['data']['domainname'] = $get_agent_info[0]['agency_name'];

						}
					}
				
				}
				//debug($page_data);exit;
				switch ($operation) {
					case 'show_voucher' : $this->load->view('vouchers/sightseeing_voucher', $page_data);
					break;
					case 'show_pdf' :						
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->load->view('vouchers/sightseeing_pdf', $page_data,true);						
						$create_pdf->create_pdf($get_view,'show');
						
					break;
					case 'email_voucher' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('vouchers/sightseeing_pdf', $page_data,true);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$this->provab_mailer->send_mail($email, domain_name().' - Sightseeing Ticket',$mail_template ,$pdf);
						break;
				}
			}
		}
	}

}
