<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Api extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->library('form_validation');
		$this->load->model('Api_Model');
		$this->load->model('Product_Model');
	    if(!isset($_SESSION['ses_id'])){
			$sec_res 			= session_id();
	    	$_SESSION['ses_id'] = $sec_res;
		}
		
		$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	

	function apiList(){
		$api 				= $this->General_Model->getHomePageSettings();
		$api['api_list'] 	= $this->Api_Model->getApiList();
		$this->load->view('api/api_list',$api);
	}
	 
	function addApi(){
		$api = $this->General_Model->getHomePageSettings();
		$api['product'] = $this->Product_Model->getProductList();
		if(count($_POST) > 0){
			$form_validator = $this->formValidator('add');
			if($form_validator == FALSE  ) {
				$this->load->view('api/add_api',$api);
			    }else{
			$user_profile_name = $this->General_Model->upload_image($_FILES, 'api');

			$this->Api_Model->addApiDetails($_POST,$user_profile_name);
			redirect('api/apiList','refresh');
		}
		}else{
			
			$this->load->view('api/add_api',$api);
		}
	}
	
	function activeApi($api_id){

		//$api_id 	= json_decode(base64_decode($api_id1));
		if($api_id != ''){
			$this->Api_Model->activeApi($api_id);
		}
		redirect('api/apiList','refresh');
	}
	
	function inactiveApi($api_id){ 
		//$api_id 	= json_decode(base64_decode($api_id));
		if($api_id != ''){
			$this->Api_Model->inactiveApi($api_id);
		}   
		redirect('api/apiList','refresh');
	}
	function b2c_activeApi($api_id){

		//$api_id 	= json_decode(base64_decode($api_id1));
		if($api_id != ''){
			$this->Api_Model->b2c_activeApi($api_id);
		}
		redirect('api/apiList','refresh');
	}
	
	function b2c_inactiveApi($api_id){
		//$api_id 	= json_decode(base64_decode($api_id1));
		if($api_id != ''){
			$this->Api_Model->b2c_inactiveApi($api_id);
		}
		redirect('api/apiList','refresh');
	}
	
	function deleteApi($api_id1){
		$api_id 	= json_decode(base64_decode($api_id1));
		if($api_id != ''){
			$this->Api_Model->deleteApi($api_id);
		}
		redirect('api/apiList','refresh');
	}
	
	function editApi($api_id1)
	{
		$api_id 	= json_decode(base64_decode($api_id1));
		if($api_id != ''){
			$api 		= $this->General_Model->getHomePageSettings();
			$api['api'] = $this->Api_Model->getApiList($api_id);
			$this->load->view('api/edit_api',$api);
		}else{
			redirect('api/apiList','refresh');
		}
	}

	function updateApi($api_id1){
		$api_id 	= json_decode(base64_decode($api_id1));
		if($api_id != ''){
			if(count($_POST) > 0){
			$form_validator = $this->formValidator('edit');
				if($form_validator == FALSE  ) {
				redirect('api/editApi/'.$api_id1,'refresh');	
			    }else{
				$image_info_name = $this->General_Model->upload_image($_FILES, 'api', $_REQUEST['old_image']);
				$this->Api_Model->updateApi($_POST,$api_id, $image_info_name);
				redirect('api/apiList','refresh');
			}
			}else if($api_id!=''){
				redirect('api/apiList','refresh');
			}else{
				redirect('api/apiList','refresh');
			}
		}else{
			redirect('api/apiList','refresh');
		}		
	}

	function formValidator($type){
	   
	   $this->form_validation->set_rules('api_name', 'API Name', 'required');
	   $this->form_validation->set_rules('api_name_alternative', 'Alternative Name', 'required');
	   $this->form_validation->set_rules('api_password', 'API Password', 'required');
	   $this->form_validation->set_rules('api_user_name', 'API Username','required');
	   
	   if($type == 'add') {
	    $this->form_validation->set_rules('api_name', 'API Name', 'trim|required|min_length[2]|max_length[50]');
	    $this->form_validation->set_rules('api_name_alternative', 'Alternative Name', 'trim|required');
	    $this->form_validation->set_rules('api_password', 'API Password','required');
	    $this->form_validation->set_rules('api_user_name', 'API UserName','required');
	    }
	    if($type == 'edit') {
	    $this->form_validation->set_rules('api_name', 'API Name', 'trim|required|min_length[2]|max_length[50]');
	    $this->form_validation->set_rules('api_name_alternative', 'Alternative Name', 'trim|required');
	    $this->form_validation->set_rules('api_password', 'API Password','required');
	    $this->form_validation->set_rules('api_user_name', 'API UserName','required');
	    }
	   return $this->form_validation->run();
    }	
}
