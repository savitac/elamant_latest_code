<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
error_reporting(0);
ob_start();
class Header extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Header_Model');
		$this->load->model('Product_Model');
		$this->load->model('Usertype_Model');
		$this->load->model('Markup_Model');
		$this->load->library('form_validation');
		
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	$this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function index(){
		$header 				= $this->General_Model->getHomePageSettings();
		$header['header_list'] 	= $this->Header_Model->getHeaderList();
		$header['logo_list'] 	= $this->Header_Model->get_logo_list();
		$this->load->view('header/header_list',$header);
	}
	
	function headerList(){
		$header 				= $this->General_Model->getHomePageSettings();
		$header['header_list'] 	= $this->Header_Model->getHeaderList();
		$this->load->view('header/header_list',$header);
	}
	
	function addHeader(){
		if(count($_POST) > 0){
			$this->Header_Model->addHeaderDetails($_POST);
			redirect('header/headerList','refresh');
		}else{
			$header = $this->General_Model->getHomePageSettings();
			$this->load->view('header/add_header',$header);
		}
	}
	
	function activeHeader($header_id1){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->activeHeader($header_id);
		}
		redirect('header/headerList','refresh');
	}
	
	function inactiveHeader($header_id1){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->inactiveHeader($header_id);
		}
		redirect('header/headerList','refresh');
	}
	
	function deleteHeader($header_id1){ 
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->deleteHeader($header_id);
		}
		redirect('header/headerList','refresh');
	}
	
	function editHeader($header_id1)
	{
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$header 					= $this->General_Model->getHomePageSettings();
			$header['header_list'] 		= $this->Header_Model->getHeaderList($header_id);
			$this->load->view('header/edit_header',$header);
		}else{
			redirect('header/headerList','refresh');
		}
	}

	function updateHeader($header_id)
	{
		$header_id 	= json_decode(base64_decode($header_id)); 
		if($header_id != ''){			
			if(count($_POST) > 0){
				
				$this->Header_Model->updateHeaderDetails($_POST,$header_id);
				redirect('header/headerList','refresh');
			}else if($header_id!=''){
				redirect('header/edit_header/'.$header_id,'refresh');
			}else{
				redirect('header/headerList','refresh');
			}
		}else{
			redirect('header/headerList','refresh');
		}
	}

	function get_user_data($user_type){
		$options = '';			
       
		if(!empty($user_type)){
			$result 			= $this->Markup_Model->get_user_list_by_type($user_type);
			if($result!=''){
			foreach($result as $row){ 
				$options .= '<option value="'.$row->user_details_id.'">'.str_replace("-"," ",$row->user_name).'('.$row->user_email.')</option>';				
			}}		
		}
		echo json_encode(array(
            'options' 		=> $options,
        ));
	}

	//Products
	function ProductList($header_id){
		$header 				= $this->General_Model->getHomePageSettings();
		$header['headerid'] = $header_id;
		$header['product_list'] 	= $this->Header_Model->getHeaderProductList();
		$this->load->view('header/product_list',$header);
	}

	function Products($header_id1){
		$header_id 	= json_decode(base64_decode($header_id1));
		if(count($_POST) > 0){ 
			$product_image = $this->General_Model->upload_image($_FILES, 'header');
			$this->Header_Model->addHeaderProductDetails($_POST,$product_image);
			redirect('header/ProductList/'.$header_id1,'refresh');
		}else{

			$header = $this->General_Model->getHomePageSettings();
			$header['headerid'] = $header_id;
			$header['product'] 	=  $this->Product_Model->getProductList();
			$header['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$this->load->view('header/add_product',$header);
		}
	}

	

	function inactiveHeaderProduct($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->inactiveHeaderProduct($header_id);
		}
		redirect('header/ProductList/'.$header,'refresh');
	}

	function activeHeaderProduct($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->activeHeaderProduct($header_id);
		}
		redirect('header/ProductList/'.$header,'refresh');
	}

	function deleteHeaderProduct($header_id1,$header){ 
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->deleteHeaderProduct($header_id);
		}
		redirect('header/ProductList/'.$header,'refresh');
	}

	function editHeaderProduct($header_id1,$headers)
	{	
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$header 					= $this->General_Model->getHomePageSettings();
			$header['header_list'] 		= $this->Header_Model->getHeaderProductList($header_id);
			$header['product_id'] = $headers; 
			$this->load->view('header/edit_headerproduct',$header);
		}else{
			redirect('header/ProductList/'.$headers,'refresh');
		}
	}

	function updateProduct($header_id,$header)
	{	
		$header_id 	= json_decode(base64_decode($header_id)); 
		if($header_id != ''){			
			if(count($_POST) > 0){
				$image_info_name = $this->General_Model->upload_image($_FILES, 'header', $_POST['old_image']);
				$this->Header_Model->updateProduct($_POST,$image_info_name, $header_id);
				redirect('header/ProductList/'.$header,'refresh');
			}else if($header_id!=''){
				redirect('header/editHeaderProduct/'.$header_id.'/'.$header,'refresh');
			}else{
				redirect('header/ProductList/'.$header,'refresh');
			}
		}else{
			redirect('header/ProductList/'.$header,'refresh');
		}
	}
	//Product End

	//Services

	function ServiceList($header_id){
		$header 				= $this->General_Model->getHomePageSettings();
		$header['headerid'] = $header_id;
		$header['service_list'] 	= $this->Header_Model->getHeaderServiceList();
		$this->load->view('header/service_list',$header);
	}
	
	function Services($header_id1){
		$header_id 	= json_decode(base64_decode($header_id1));
		if(count($_POST) > 0){ 
			$product_image = $this->General_Model->upload_image($_FILES, 'header');
			$this->Header_Model->addHeaderServiceDetails($_POST,$product_image);
			redirect('header/ServiceList/'.$header_id1,'refresh');
		}else{

			$header = $this->General_Model->getHomePageSettings();
			$header['headerid'] = $header_id;
			$header['product'] 	=  $this->Product_Model->getProductList();
			$header['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$this->load->view('header/add_service',$header);
		}
	}

	function inactiveHeaderService($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->inactiveHeaderService($header_id);
		}
		redirect('header/ServiceList/'.$header,'refresh');
	}

	function activeHeaderService($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->activeHeaderService($header_id);
		}
		redirect('header/ServiceList/'.$header,'refresh');
	}

	function deleteHeaderService($header_id1,$header){ 
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->deleteHeaderService($header_id);
		}
		redirect('header/ServiceList/'.$header,'refresh');
	}

	function editHeaderService($header_id1,$headers)
	{	
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$header 					= $this->General_Model->getHomePageSettings();
			$header['service_list'] 		= $this->Header_Model->getHeaderServiceList($header_id);
			$header['service_id'] = $headers; 
			$this->load->view('header/edit_headerservice',$header);
		}else{
			redirect('header/ServiceList/'.$headers,'refresh');
		}
	}

	function updateHeaderService($header_id,$header)
	{
		$header_id 	= json_decode(base64_decode($header_id)); 
		if($header_id != ''){			
			if(count($_POST) > 0){
				$image_info_name = $this->General_Model->upload_image($_FILES, 'header', $_POST['old_image']);
				$this->Header_Model->updateService($_POST,$image_info_name, $header_id);
				redirect('header/ServiceList/'.$header,'refresh');
			}else if($header_id!=''){
				redirect('header/editHeaderService/'.$header_id.'/'.$header,'refresh');
			}else{
				redirect('header/ServiceList/'.$header,'refresh');
			}
		}else{
			redirect('header/ServiceList/'.$header,'refresh');
		}
	}

	function aboutusList($header_id){
		$header 				= $this->General_Model->getHomePageSettings();
		$header['headerid'] = $header_id;
		$header['aboutus'] 	= $this->Header_Model->getHeaderaboutus();
		$this->load->view('header/aboutus_list',$header);
	}

	function Aboutus($header_id1){
		$header_id 	= json_decode(base64_decode($header_id1));
		if(count($_POST) > 0){ 
			$product_image = $this->General_Model->upload_image($_FILES, 'header');
			$this->Header_Model->addHeaderAboutus($_POST,$product_image);
			redirect('header/aboutusList/'.$header_id1,'refresh');
		}else{

			$header = $this->General_Model->getHomePageSettings();
			$header['headerid'] = $header_id;
			$header['product'] 	=  $this->Product_Model->getProductList();
			$header['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$this->load->view('header/add_aboutus',$header);
		}
	}

	function inactiveHeaderaboutus($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->inactiveHeaderaboutus($header_id);
		}
		redirect('header/aboutusList/'.$header,'refresh');
	}

	function activeHeaderaboutus($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->activeHeaderaboutus($header_id);
		}
		redirect('header/aboutusList/'.$header,'refresh');
	}

	function deleteHeaderaboutus($header_id1,$header){ 
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->deleteHeaderaboutus($header_id);
		}
		redirect('header/aboutusList/'.$header,'refresh');
	}

	function editHeaderaboutus($header_id1,$headers)
	{	
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$header 					= $this->General_Model->getHomePageSettings();
			$header['service_list'] 		= $this->Header_Model->getHeaderaboutus($header_id);
			$header['service_id'] = $headers; 
			$this->load->view('header/edit_headerabout',$header);
		}else{
			redirect('header/aboutusList/'.$headers,'refresh');
		}
	}

	function updateHeaderaboutus($header_id,$header)
	{
		$header_id 	= json_decode(base64_decode($header_id)); 
		if($header != ''){			
			if(count($_POST) > 0){
				
				$this->Header_Model->updateHeaderaboutus($_POST, $header_id);
				redirect('header/aboutusList/'.$header,'refresh');
			}else if($header!=''){
				redirect('header/editHeaderService/'.$header_id.'/'.$header,'refresh');
			}else{
				redirect('header/aboutusList/'.$header,'refresh');
			}
		}else{
			redirect('header/aboutusList/'.$header,'refresh');
		}
	}
	//Manageaboutus
	function Manageaboutus($header_id,$headers){
		$header 				= $this->General_Model->getHomePageSettings();
		$header['headerid'] = $header_id;
		$header['header'] = $headers; 
		$tes = json_decode(base64_decode($header_id));
		$header['aboutus'] 	= $this->Header_Model->getHeaderManageaboutus($tes);
		$this->load->view('header/manageabout_list',$header);
	}

	function AddManageAbout($header_id1,$headers){
		$header_id 	= json_decode(base64_decode($header_id1)); 
		if(count($_POST) > 0){  //print_r($_POST); exit();
			$product_image = $this->General_Model->upload_image($_FILES, 'header');
			$this->Header_Model->addHeaderManageAboutus($_POST,$product_image);
			redirect('header/Manageaboutus/'.$header_id1 .'/'. $headers,'refresh');
		}else{

			$header = $this->General_Model->getHomePageSettings();
			$header['headerid'] = $header_id;
			$header['header'] = $headers;
			$header['product'] 	=  $this->Product_Model->getProductList();
			$header['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$this->load->view('header/add_manageaboutus',$header);
		}
	}

	function inactiveHeaderManageaboutus($manage_id1,$header_id1,$header){
		$manage_id 	= json_decode(base64_decode($manage_id1));
		if($manage_id != ''){
			$this->Header_Model->inactiveHeaderManageaboutus($manage_id);
		}
		redirect('header/Manageaboutus/'.$header_id1.'/'.$header,'refresh');
	}

	function activeHeaderManageaboutus($manage_id1,$header_id1,$header){
		$manage_id 	= json_decode(base64_decode($manage_id1));
		if($manage_id != ''){
			$this->Header_Model->activeHeaderManageaboutus($manage_id);
		}
		redirect('header/Manageaboutus/'.$header_id1.'/'.$header,'refresh');
	}
	function deleteHeaderManageaboutus($manage_id1,$header_id1,$header){ 
		$manage_id 	= json_decode(base64_decode($manage_id1));
		if($manage_id1 != ''){
			$this->Header_Model->deleteHeaderManageaboutus($manage_id);
		}
		redirect('header/Manageaboutus/'.$header_id1.'/'.$header,'refresh');
	}

	function editHeaderManageaboutus($manage_id1,$header_id1,$headers)
	{	
		$manage_id 	= json_decode(base64_decode($manage_id1));

		if($manage_id != ''){
			$header 					= $this->General_Model->getHomePageSettings();
			$header['service_list'] 		= $this->Header_Model->getHeaderManageabout($manage_id);
			//echo '<pre>sanjay'; print_r($header['service_list']); exit();
			$header['service_id'] = $headers; 
			$header['header_id1'] = $header_id1; 
			$header['manage_id1'] = $manage_id1;
			$this->load->view('header/edit_headermanageabout',$header);
		}else{
			redirect('header/Manageaboutus/'.$header_id1.'/'.$header,'refresh');
		}
	}

	function updateHeaderManageaboutus($manage_id1,$header_id1,$header)
	{
		$manage_id 	= json_decode(base64_decode($manage_id1)); 
		if($manage_id1 != ''){			
			if(count($_POST) > 0){
				
				$this->Header_Model->updateHeaderManageaboutus($_POST, $manage_id);
				redirect('header/Manageaboutus/'.$header_id1.'/'.$header,'refresh');
			}else if($header!=''){
				redirect('header/editHeaderService/'.$header_id.'/'.$header,'refresh');
			}else{
				redirect('header/Manageaboutus/'.$header_id1.'/'.$header,'refresh');
			}
		}else{
			redirect('header/Manageaboutus/'.$header_id1.'/'.$header,'refresh');
		}
	}
	// End Manageaboutus
	//Contact
	function ContactList($header_id){
		$header 				= $this->General_Model->getHomePageSettings();
		$header['headerid'] = $header_id;
		$header['aboutus'] 	= $this->Header_Model->getHeadercontact();
		$this->load->view('header/contact_list',$header);
	}

	function Contact($header_id1){
		$header_id 	= json_decode(base64_decode($header_id1));
		if(count($_POST) > 0){ 
			
			$this->Header_Model->addHeaderContact($_POST);
			redirect('header/ContactList/'.$header_id1,'refresh');
		}else{

			$header = $this->General_Model->getHomePageSettings();
			$header['headerid'] = $header_id;
			$header['product'] 	=  $this->Product_Model->getProductList();
			$header['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$this->load->view('header/add_contact',$header);
		}
	}

	function inactiveHeaderContact($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->inactiveHeaderContact($header_id);
		}
		redirect('header/ContactList/'.$header,'refresh');
	}

	function activeHeaderContact($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->activeHeaderContact($header_id);
		}
		redirect('header/ContactList/'.$header,'refresh');
	}

	function deleteHeaderContact($header_id1,$header){ 
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Header_Model->deleteHeaderContact($header_id);
		}
		redirect('header/ContactList/'.$header,'refresh');
	}

	function editHeaderContact($header_id1,$headers)
	{	
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$header 					= $this->General_Model->getHomePageSettings();
			$header['contact_list'] 		= $this->Header_Model->getHeadercontact($header_id);
			$header['product_id'] = $headers; 
			$this->load->view('header/edit_headercontact',$header);
		}else{
			redirect('header/ContactList/'.$headers,'refresh');
		}
	}

	function updateContact($header_id,$header)
	{	
		$header_id 	= json_decode(base64_decode($header_id)); 
		if($header_id != ''){			
			if(count($_POST) > 0){
				
				$this->Header_Model->updateContact($_POST, $header_id);
				redirect('header/ContactList/'.$header,'refresh');
			}else if($header_id!=''){
				redirect('header/editHeaderContact/'.$header_id.'/'.$header,'refresh');
			}else{
				redirect('header/ContactList/'.$header,'refresh');
			}
		}else{
			redirect('header/ContactList/'.$header,'refresh');
		}
	}

	////Contact End
}
