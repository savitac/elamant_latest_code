<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
error_reporting(0);
class Markup extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Product_Model');		
		$this->load->model('Domain_Model');		
		$this->load->model('Usertype_Model');		
		$this->load->model('Api_Model');
		$this->load->model('Markup_Model');
		$this->load->library('form_validation');
		
		if(!isset($_SESSION['ses_id'])){
			$sec_res 			= session_id();
	    	$_SESSION['ses_id'] = $sec_res;
		}
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	$this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	
	 function index(){
		$markup 			= $this->General_Model->getHomePageSettings();
		$markup['markup'] 	= $this->Markup_Model->get_markup_list();
		$this->load->view('markup/markup_list',$markup);
	}
	
	 function add_markup(){ 
		
	 		$data 				=  $this->General_Model->getHomePageSettings();
			$data['domain'] 	=  $this->Domain_Model->get_domain_list();
			$data['product'] 	=  $this->Product_Model->getMarkupActiveProductList();
			$data['country'] 	=  $this->General_Model->get_country();
			$data['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$data['airline_list']	=  $this->Markup_Model->get_airline_list();
			$data['api']		=  $this->Api_Model->getActiveApiList();
	 
		if(count($_POST) > 0){
			if($_POST['markup_type'] == 'GENERAL') {

				$markup = $this->Markup_Model->get_available_general_markup($_POST['product'], $_POST['markup_type']);
				
				if(count($markup) > 0){
					 $data['error'] = 'Entered markup is already available';
					 $this->load->view('markup/add_markup',$data);	
				}
				else{
					try {
			$this->General_Model->begin_transaction();
			
			$this->Markup_Model->add_markup($_POST);
			$this->General_Model->commit_transaction();
			redirect('markup','refresh');
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction();
			return $e;
		}
				}
		   }else{
		 //$markup = $this->Markup_Model->get_available_specific_markup($_POST['product'], $_POST['api'], $markup_id='', $_POST['country'][0],$_POST['exp_date'] );
				
					try {
			$this->General_Model->begin_transaction();
			
			$this->Markup_Model->add_markup($_POST);
			$this->General_Model->commit_transaction();
			redirect('markup','refresh');
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction();
			return $e;
		}
				
			}
			
		}else{
			
			$this->load->view('markup/add_markup',$data);	
		}
	}	
	
	function add_convenience()
	{
		$domain_id = isset($_GET['domain']) ? $_GET['domain'] : '';
		$convenience_fees = $this->Markup_Model->get_convenience_fees ($domain_id);
 		$data 				=  $this->General_Model->getHomePageSettings();
		$data['domain'] 	=  $this->Domain_Model->get_domain_list();
		$convenience_fees = $this->format_convenience_fees ( $convenience_fees );
		$modules = array('DOMESTIC_FLIGHT', 'INTERNATIONAL_FLIGHT', 'DOMESTIC_HOTEL', 'INTERNATIONAL_HOTEL', 'BUS', 'TRANSFERS', 'SIGHTSEEING');
		$temp = array();
		for($i = 0; $i < count($modules); $i++) {
				$temp[$i]['modules'] = $modules[$i];
				if(!empty($convenience_fees[$i])) {
					$temp[$i]['data'] = $convenience_fees[$i];
				}
		}
		$data['convenience_fees'] = $temp;
		
		if(count($_POST) > 0 && isset($domain_id))
		{	
			try {
			$this->General_Model->begin_transaction();
			$this->Markup_Model->add_convenience($_POST);
			$this->General_Model->commit_transaction();
			redirect('markup/add_convenience?domain='.$domain_id); 
			} catch(Exception $e) {
				$this->General_Model->rollback_transaction();
				return $e;
			}	
		} else {
			$this->load->view('markup/add_convenience',$data);	
		}
	
	}
	
	function convenience_list()
	{	
		$data=$this->General_Model->getHomePageSettings();
		$data['convenience_list']=$this->Markup_Model->convenience_list(); 
		$this->load->view('markup/convenience_list',$data);    
	}
	function delete_convenience($id)
	{
		$this->Markup_Model->convenience_delete($id);  
	} 
	function active_markup($markup_id){  
		$markup_id=base64_encode(json_encode($markup_id));
		$user_id 	= json_decode(base64_decode($markup_id));
		if($user_id != ''){
			$this->Markup_Model->active_markup($user_id);
		}
			redirect('markup','refresh');
	}
	
	function inactive_markup($markup_id){
		$markup_id=base64_encode(json_encode($markup_id)); 
		$user_id 	= json_decode(base64_decode($markup_id));
		if($user_id != ''){
			$this->Markup_Model->inactive_markup($user_id);
		}
			redirect('markup','refresh');
	}
	
	function delete_markup($markup_id){
		$this->Markup_Model->delete_markup($markup_id);
		redirect('markup','refresh');
	}
	
	function edit_markup($markup_id,$error = '')
	{
		//echo 'sanjay'; print_r(base64_decode($data)); exit();
		$data 				=  $this->General_Model->getHomePageSettings();
		$data['domain'] 	=  $this->Domain_Model->get_domain_list();
		$data['product'] 	=  $this->Product_Model->getMarkupActiveProductList();
		$data['country'] 	=  $this->General_Model->get_country_details();

		$data['user_type']	=  $this->Usertype_Model->get_user_type_list();
		$data['api']		=  $this->Api_Model->getApiList(); 
		$data['markup'] 	= $this->Markup_Model->get_markup_list($markup_id);
		$data['error']   =  base64_decode($error);
		$this->load->view('markup/edit_markup',$data);
	}
	
	function update_markup($markup_id)
	{	
		if(count($_POST) > 0){ 
			
		if($_POST['markup_type'] == 'GENERAL') {
			//echo '<pre>'; print_r($_POST); exit();
				$markup = $this->Markup_Model->get_available_general_markup($_POST['product'], '3',$_POST['markup_value_type'],$_POST['markup_value'], $_POST['markup_fixed']);
				
				if(count($markup) > 0){
					 $data = 'Entered markup is already available';
					 $data = base64_encode($data);

					 redirect('markup/edit_markup/'.$markup_id.'/'.$data);
				}
				else{
					try {
			$this->General_Model->begin_transaction();
			
			$this->Markup_Model->update_markup($_POST,$markup_id);
			$this->General_Model->commit_transaction();
			redirect('markup','refresh');
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction();
			return $e;
		}
				}
		   }else{

		 $markup = $this->Markup_Model->get_available_specific_markup($_POST['product'], $_POST['api'], $markups_id='', $_POST['country'][0],$_POST['markup_value'], $_POST['markup_fixed'] );
				if(count($markup) > 0){
					 $data = 'Entered markup is already available';
					 $data = base64_encode($data);
					 redirect('markup/edit_markup/'.$markup_id.'/'.$data);
				}else{
					try {
			$this->General_Model->begin_transaction();
			
			$this->Markup_Model->update_markup($_POST,$markup_id);
			$this->General_Model->commit_transaction();
			redirect('markup','refresh');
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction();
			return $e;
		}
				}
			}
		}else if($markup_id!=''){
			redirect('markup/edit_markup/'.$markup_id,'refresh');
		}else{
			redirect('markup','refresh');
		}
	}

	 function get_user_data($user_type){
		$options = '';			
        $options .= '<option value=""></option>';
		if(!empty($user_type)){
			$result 			= $this->Markup_Model->get_user_list_by_type($user_type);
			if($result!=''){
			foreach($result as $row){ 
				$options .= '<option value="'.$row->user_details_id.'">'.str_replace("-"," ",$row->user_name).'</option>';				
			}}		
		}
		echo json_encode(array(
            'options' 		=> $options,
        ));
	}

	function get_supplier_data(){
		$options = '';			
        $options .= '<option value=""></option>';
		//if(!empty($user_type)){
			$result 			= $this->Markup_Model->get_supplier_list_by_type();
			if($result!=''){
			foreach($result as $row){ 
				$options .= '<option value="'.$row->supplier_details_id.'">'.str_replace("-"," ",$row->supplier_name).'</option>';				
			}}		
		//}
		echo json_encode(array(
            'options' 		=> $options,
        ));
	}

	/**
	 * Format Convenience Fees As Per View
	 */
	private function format_convenience_fees($convenience_fees) {
		$data = array ();
		foreach ( $convenience_fees as $k => $v ) {
			$data [$k] ['origin'] = $v ['origin'];
			$data [$k] ['module'] = strtoupper ( $v ['module'] );
			$fees = '';
			if ($v ['value_type'] == 'plus') {
				$fees = '+' . floatval ( $v ['value'] );
			} else {
				$fees = floatval ( $v ['value'] ) . '%';
			}
			$data [$k] ['fees'] = $fees;
			$data [$k] ['value'] = $v ['value'];
			$data [$k] ['value_type'] = $v ['value_type'];
			$data [$k] ['per_pax'] = $v ['per_pax'];
		}
		return $data;
	}
}
