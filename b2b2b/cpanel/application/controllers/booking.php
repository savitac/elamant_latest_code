<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(session_status() == PHP_SESSION_NONE){ session_start(); } 
//error_reporting(E_ALL);
ob_start();
class Booking extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('hotel_model');
		$this->load->model('user_model');
		$this->load->model('General_Model');
		$this->load->model('Booking_Model');
		$this->load->model('Api_Model');
        $this->load->model('Usertype_Model');
        $this->load->model('Email_Model');
		$this->load->library('form_validation');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
		$this->checkAdminLogin();
		$this->current_module = $this->config->item('current_module');
		define('CANCEL_BUFFER',7);
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }

    private function format_basic_search_filters($module='')
	{
		$filter_condition = array();
		$post_data = $this->input->post();
		if(valid_array($post_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$post_data['from']);
			$to_date = trim(@$post_data['to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$filter_condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$filter_condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			/*if (empty($get_data['created_by_id']) == false) {
				$filter_condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}*/
			
			if (empty($post_data['created_by_id']) == false && strtolower($post_data['created_by_id'])!='all') {
				$filter_condition[] = array('BD.created_by_id', '=', $this->db->escape($post_data['created_by_id']));
			}
	
			if (!empty($post_data['status'])) {
				$filter_condition[] = array('BD.status', '=', $this->db->escape($post_data['status']));
			}
		
			/*if (empty($get_data['phone']) == false) {
				$filter_condition[] = array('BD.phone', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$filter_condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}*/
			
			// if($module == 'bus'){
			if (empty($post_data['pnr_no']) == false) {
				$filter_condition[] = array('BT.pnr', ' like ', $this->db->escape('%'.$post_data['pnr_no'].'%'));
			}
			// }else{
			// 	if (empty($get_data['pnr']) == false) {
			// 		$filter_condition[] = array('BT.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
			// 	}
			// }
			
	
			// if (empty($get_data['app_reference']) == false) {
			// 	$filter_condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			// }
			
			$page_data['from'] = $from_date;
			$page_data['to'] = $to_date;

			//Today's Booking Data
			
			//Previous Booking Data: last 3 days, 7 days, 15 days, 1 month and 3 month
			// if(isset($get_data['prev_booking_data']) == true && empty($get_data['prev_booking_data']) == false) {
			// 	$filter_condition[] = array('DATE(BD.created_datetime)', '>=', '"'.trim($get_data['prev_booking_data']).'"');
			// }
			
			return array('filter_condition' => $filter_condition, 'from_date' => $from_date, 'to_date' => $to_date);
		}
		return array('filter_condition' => $filter_condition);
	}
 
    function hotelOrders() {
    	$condition = array();
   		$filter_data = $this->format_basic_search_filters();
   		$condition = $filter_data['filter_condition'];
   		$data['search_params'] = $_POST;
   		$data['from'] = $filter_data['from_date'];
		$data['to'] = $filter_data['to_date'];
		$email_status = $this->session->flashdata('email_status');
		 //debug($data['search_params']);die;
		if($email_status) 
		{

			$data = $this->General_Model->getHomePageSettings();
			if($email_status == 1 )
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email Sent Sucessfully</h4>!!!
				</div>';
			}
			else
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email was not Sent</h4>!!!
				</div>';
			}  
		}
  		$userType = array(2,4);
  		
        $data['orders'] = $this->Booking_Model->get_booking_hotel_details($userType, $condition);
        // echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
		//die;	
		// debug($data);
		// exit;
        $this->load->view('bookings/userBookings', $data);
	}

	function activityOrders() {
		//error_reporting(E_ALL);
		$condition = array();
		$email_status = $this->session->flashdata('email_status');
		if($email_status) 
		{

			$data = $this->General_Model->getHomePageSettings();
			if($email_status == 1 )
			{

				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email Sent Sucessfully</h4>!!!
				</div>';

			}
			else
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email was not Sent</h4>!!!
				</div>';
			}  
			$total_records = $this->sightseeing_model->b2c_sightseeing_report($condition, true);	
		}
  		$userType = array(2,4);
        $data['orders'] = $this->Booking_Model->activity_report($userType);
        //echo '<pre>'; print_r($data['orders']); exit('hftgyh');
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
        //$data['table_data'] = $table_data['data'];
        // echo count($data['table_data']);die;
			
        $this->load->view('bookings/activity_reports', $data);
  
	}

	function activityOrdersb2c() {
		//error_reporting(E_ALL);
		$condition = array();
		$email_status = $this->session->flashdata('email_status');
		if($email_status) 
		{

			$data = $this->General_Model->getHomePageSettings();
			if($email_status == 1 )
			{

				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email Sent Sucessfully</h4>!!!
				</div>';

			}
			else
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email was not Sent</h4>!!!
				</div>';
			}  
			$total_records = $this->sightseeing_model->b2c_sightseeing_report($condition, true);	
		}
  		$userType = '5,6';
        $table_data = $this->Booking_Model->activity_report($userType);
        // echo '<pre>'; print_r($table_data['data']); exit('hftgyh');
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
        $data['table_data'] = $table_data['data'];
        // echo count($data['table_data']);die;
			
        $this->load->view('bookings/activity_reports', $data);
  
	}

 	function hotelOrdersb2c() {
 		// die;
 		$condition = array();
   		$filter_data = $this->format_basic_search_filters();
   		$condition = $filter_data['filter_condition'];
   		$data['search_params'] = $_POST;
   		$data['from'] = $filter_data['from_date'];
		$data['to'] = $filter_data['to_date'];
		$email_status = $this->session->flashdata('email_status');
			 
		if($email_status) 
		{

			$data = $this->General_Model->getHomePageSettings();
			if($email_status == 1 )
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email Sent Sucessfully</h4>!!!
				</div>';
			}
			else
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email was not Sent</h4>!!!
				</div>';
			}  
		} 
  		$userType = array(5,6);
        $data['orders'] = $this->Booking_Model->get_booking_hotel_details($userType, $condition);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/userBookingsb2c', $data);
	}

	function flightOrders() 
	{	
		$condition = array();
   		$filter_data = $this->format_basic_search_filters();
   		$condition = $filter_data['filter_condition'];
   		$data['search_params'] = $_POST;
   		$data['from'] = $filter_data['from_date'];
		$data['to'] = $filter_data['to_date'];
		$email_status = $this->session->flashdata('email_status');
		 
		if($email_status) 
		{

			$data = $this->General_Model->getHomePageSettings();
			if($email_status == 1 )
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email Sent Sucessfully</h4>!!!
				</div>';
			} 
			else
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email was not Sent</h4>!!!
				</div>';
			}  
		}
		$userType = array(2,4);
		$data['orders'] = $this->Booking_Model->get_booking_flight_details($userType, $condition);
		//echo '<pre>'; print_r($data['orders']); exit("emptty");
		$data['user_type'] = $this->Usertype_Model->get_user_type_list();
		//debug($data['user_type']);die;
		$data['user_id'] = '0';
		$this->load->view('bookings/flight_reports', $data);
	}

	function flightOrdersb2c() 
	{	
		//error_reporting(E_ALL);
		$condition = array();
   		$filter_data = $this->format_basic_search_filters();
   		$condition = $filter_data['filter_condition'];
   		$data['search_params'] = $_POST;
   		$data['from'] = $filter_data['from_date'];
		$data['to'] = $filter_data['to_date'];

		$email_status = $this->session->flashdata('email_status');
		if($email_status) 
		{

			$data = $this->General_Model->getHomePageSettings();
			if($email_status == 1 )
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email Sent Sucessfully</h4>!!!
				</div>';
			} 
			else
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email was not Sent</h4>!!!
				</div>';
			}  
		}
		$userType = array(5,6);
		$data['orders'] = $this->Booking_Model->get_booking_flight_details($userType, $condition);
		//echo '<pre>'; print_r($data['orders']); exit("emptty");
		$data['user_type'] = $this->Usertype_Model->get_user_type_list();
		$data['user_id'] = '0';  
		$this->load->view('bookings/flight_reports_b2c', $data);
	}

	function hotelcrsOrders() {
		$email_status = $this->session->flashdata('email_status');
		$condition = array();
		if($email_status) 
		{
			$data = $this->General_Model->getHomePageSettings();
			if($email_status == 1 )
			{

			$data['email_status'] = '<div class="alert alert-block alert-danger">
			<a href="#" data-dismiss="alert" class="close">×</a>
			<h4 class="alert-heading">Email Sent Sucessfully</h4>!!!
			</div>';

			}
			else
			{
			$data['email_status'] = '<div class="alert alert-block alert-danger">
			<a href="#" data-dismiss="alert" class="close">×</a>
			<h4 class="alert-heading">Email was not Sent</h4>!!!
			</div>';
			}  
		}
  		$userType = array(2,4);
  		$condition[] = array('HD.hotel_type_id', '!=', VILLA);
        $table_data = $this->Booking_Model->get_booking_hotelcrs_details($condition, false, 0, RECORDS_RANGE_1, $userType);
        $data['table_data'] = $table_data['data'];
        //echo '<pre>'; print_r($data['table_data']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/hotelcrsBookings', $data);
	}

	function hotelcrsOrdersb2c() {
		$email_status = $this->session->flashdata('email_status');
		$condition = array();
		if($email_status) 
		{
			$data = $this->General_Model->getHomePageSettings();
			if($email_status == 1 )
			{

			$data['email_status'] = '<div class="alert alert-block alert-danger">
			<a href="#" data-dismiss="alert" class="close">×</a>
			<h4 class="alert-heading">Email Sent Sucessfully</h4>!!!
			</div>';

			}
			else
			{
			$data['email_status'] = '<div class="alert alert-block alert-danger">
			<a href="#" data-dismiss="alert" class="close">×</a>
			<h4 class="alert-heading">Email was not Sent</h4>!!!
			</div>';
			}  
		}
  		$userType = array(5,6);
  		$condition[] = array('HD.hotel_type_id', '!=', VILLA);
        $table_data = $this->Booking_Model->get_booking_hotelcrs_details($condition, false, 0, RECORDS_RANGE_1, $userType);
        $data['table_data'] = $table_data['data'];
        //echo '<pre>'; print_r($data['table_data']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/hotelcrsBookings', $data);
	}

	function villaOrders() {
		//error_reporting(E_ALL);
		$email_status = $this->session->flashdata('email_status');
		$condition = array();
		if($email_status) 
		{
			$data = $this->General_Model->getHomePageSettings();
			if($email_status == 1 )
			{

				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email Sent Sucessfully</h4>!!!
				</div>';

			}
			else
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email was not Sent</h4>!!!
				</div>';
			}  
		}
		$userType = '2,4';
        $table_data = $this->Booking_Model->get_booking_villa_details($condition, false, 0, RECORDS_RANGE_1, $userType);
        $data['table_data'] = $table_data['data'];
        //echo '<pre>'; print_r($data['table_data']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/villaBookings', $data);
	}

	function villaOrdersb2c() {
		$email_status = $this->session->flashdata('email_status');
		$condition = array();
		if($email_status) 
		{
			$data = $this->General_Model->getHomePageSettings();
			if($email_status == 1 )
			{

				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email Sent Sucessfully</h4>!!!
				</div>';

			}
			else
			{
				$data['email_status'] = '<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Email was not Sent</h4>!!!
				</div>';
			}  
		}
  		$userType = '5,6';
        $table_data = $this->Booking_Model->get_booking_villa_details($condition, false, 0, RECORDS_RANGE_1, $userType);
        $data['table_data'] = $table_data['data'];
        //echo '<pre>'; print_r($data['table_data']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/villaBookings', $data);
	}

 	function transferOrders() {
 		//error_reporting(E_ALL);
    	$data = $this->General_Model->getHomePageSettings(); 
    	$userType = '2,4';
        $table_data = $this->Booking_Model->transfer_report($userType);
        $table_data = $this->booking_data_formatter->format_transferv1_booking_data($table_data, $this->current_module);
        $data['table_data'] = $table_data['data'];
        //echo '<pre>'; print_r($table_data); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/transferBookings', $data);
	}

	function transferOrdersb2c() {
		//error_reporting(E_ALL);
    	$data = $this->General_Model->getHomePageSettings();
    	$userType = '5,6'; 
        $table_data = $this->Booking_Model->transfer_report($userType);    
        $table_data = $this->booking_data_formatter->format_transferv1_booking_data($table_data, $this->current_module);
        $data['orders'] = $table_data['data'];
        //echo '<pre>'; print_r($table_data); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/transferBookings', $data);
	}

	public function cancel($module,$pnr_no,$parent_pnr_no){
		$pnr_no = base64_decode(base64_decode($pnr_no));
		$parent_pnr_no = base64_decode(base64_decode($parent_pnr_no));
        if($module == 'Hotels'){
            $count = $this->Booking_Model->getBookingPnr($pnr_no)->num_rows();

            if($count == 1) {
                $b_data = $this->Booking_Model->getBookingPnr($pnr_no)->row(); 
                $api_details = $this->Api_Model->getApiList($b_data->api_id);            
                $reference_id = $b_data->referal_id;                 
                $getBookingHotelData = $this->Booking_Model->getBookingHotelData($reference_id)->row();
                $getBookingHotelData_v1 = $this->Booking_Model->getBookingbyPnr_v1($pnr_no,$module)->row();
                //~ echo "data: <pre>";print_r($getBookingHotelData_v1);exit;
                $api = $api_details[0]->api_name;
                $CancelTime = date('Y-m-d H:i:s');
                $book_usertype = $b_data->user_type_id;

                if($b_data->booking_status == 'CONFIRMED' || $b_data->booking_status == 'PENDING'){ 
					
					//~ Cancellation Buffer
					
					$cancel_buffer = $this->General_Model->getCancellationBuffer(1,$b_data->api_id)->row();

					if(empty($cancel_buffer) && $cancel_buffer == '') {
						$buffer_days = 0;
					}
					else {
						$buffer_days = $cancel_buffer->cancel_days;
					}
					
					//~ End
					
                    if($api == "GTA"){
						$api_cancel_pnr = $b_data->api_confirmation_no;
						$this->load->helper('gta_helper');
                        $CancelReq_Res = HotelBookingCancel($api_cancel_pnr);
                         //  echo 'CancelReq_Res : <pre/>';print_r($CancelReq_Res);
                        $CancelReq = $CancelReq_Res['HotelBookingCancelRQ'];
                        $CancelRes = $CancelReq_Res['HotelBookingCancelRS'];
                        
                        if($CancelRes != '') {
							$HotelBookingCancelRS = new SimpleXMLElement($CancelRes);
							$HotelBookingCancelRS = $HotelBookingCancelRS->children()->ResponseDetails->BookingResponse;                
							if(isset($HotelBookingCancelRS->BookingStatus)) {
								$BookingStatus = (string)$HotelBookingCancelRS->BookingStatus;
								$BookingStatus = trim($BookingStatus);  

								if($BookingStatus == "Cancelled"){  //check if api status is sent as cancelled                            
									$update_booking = array(
														'api_status' => 'Cancelled',
														'booking_status' => 'CANCELLED',
														'cancellation_status' => 'Cancelled',
														'cancel_request_time' => $CancelTime,
														'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
														'cancel_response' => $CancelReq_Res['HotelBookingCancelRS']
													);
									//~ echo '<pre/>';print_r($update_booking);exit;
									$cancellation_data = $getBookingHotelData->cancellation_data;
									$profit_percent = $getBookingHotelData->profit_percent;  
									
									                                                                                                 
									if(!empty($cancellation_data)){                                 
										$now = strtotime(date('Y-m-d H:i'));                                    
										$cancellation_data = json_decode($cancellation_data);
										foreach ($cancellation_data as $key => $cancellation) {
											if($buffer_days == 0) {
												$from = strtotime($cancellation->ToDate);
												$to = strtotime($cancellation->FromDate);
											}
											else {
												$from = strtotime($cancellation->ToDate." -".$buffer_days." days");
												$to = strtotime($cancellation->FromDate." -".$buffer_days." days");
											}
											$amount = $cancellation->ChargeAmount;
											$type = $cancellation->Type;
											if ($from == $to) {
												if ($now >= $from) {
													if($type == '1'){
														$charge[] = $amount;
													} else {
														$charge[] = 0;
													}
												}
												else {
													$charge[] = 0;
												}
											} else {
												if ($now >= $from && $now <= $to) {
													if($type == '1'){
														$charge[] = $amount;
													}
												} else {
													$charge[] = 0;
												}
											}                             
										}
										$payment_details = json_decode($getBookingHotelData->TravelerDetails);
										$TotalPrice_API = $payment_details->total;                       
										$Charge_API = array_sum($charge);
										$Refund = $TotalPrice_API - $Charge_API;
										
										$Refund = ($getBookingHotelData_v1->transaction_amount - $getBookingHotelData_v1->PG_Charge) - $Charge_API;
										
										$update_booking = array(
															'api_status' => 'Cancelled',
															'booking_status' => 'CANCELLED',
															'cancellation_status' => 'Cancelled',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
															'cancel_response' => $CancelReq_Res['HotelBookingCancelRS'],
															'cancellation_amount' => $Charge_API,
															'refund_amount' => $Refund,
															'refund_status' => 0
														);
									}
									//~ echo '<pre/>';print_r($update_booking);exit;
									$this->Booking_Model->Update_Booking_Global($b_data->booking_no, $update_booking);
									//~ Commented as of now 
									//~ $this->cancel_mail_voucher($pnr_no);
									
									$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr_no)->result();

									foreach($data['pnr_nos'] as $pnr_nos):
										$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
										$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
										$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
										//echo '<pre>sanjay'; print_r($data['user_details'][0]->user_email); exit();
										$email['message'] = $this->load->view('vouchers/hotel_mailVoucher', $data, TRUE);
										$email['to'] = $data['user_details'][0]->user_email;
										//echo 'sanjay'; print_r($email['to']); exit();
						                $email['email_access'] = $this->Email_Model->get_email_acess();
						                
					            		$Response = $this->Email_Model->sendmail_hotelVoucher($email);
										redirect('booking/hotelOrders', 'refresh');
									endforeach;
									
									$response = array('status' => 1);
									//~ echo json_encode($response);
									redirect('booking/hotelOrders','refresh');
								} 
								else {
									$update_booking = array(
															'cancellation_status' => 'Cancellation Pending',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
															'cancel_response' => $CancelReq_Res['HotelBookingCancelRS']
														);
									$this->Booking_Model->Update_Booking_Global($pnr_no, $update_booking);
									$xml_log = array(
													'api_id' => 'GTA',
													'xml_type' => 'Hotel - Cancel Request',
													'xml_request' => $CancelReq_Res['HotelBookingCancelRQ'],
													'xml_response' => $CancelReq_Res['HotelBookingCancelRS'],
													'ip_address' => $this->input->ip_address()
												);
									$this->General_Model->store_logs($xml_log,'SUCCESS');
									$response = array('status' => 0);
									//~ echo json_encode($response);
									redirect('booking/hotelOrders','refresh');
								} 
							} 
							else {
								$update_booking = array(
														'cancellation_status' => 'Cancellation Pending',
														'cancel_request_time' => $CancelTime,
														'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
														'cancel_response' => $CancelReq_Res['HotelBookingCancelRS']
													);
								$this->Booking_Model->Update_Booking_Global($b_data->booking_no, $update_booking);
								$xml_log = array(
												'api_id' => 'GTA',
												'xml_type' => 'Hotel - Cancel Request',
												'xml_request' => $CancelReq_Res['HotelBookingCancelRQ'],
												'xml_response' => $CancelReq_Res['HotelBookingCancelRS'],
												'ip_address' => $this->input->ip_address()
											);
								$this->General_Model->store_logs($xml_log,'SUCCESS');
								$response = array('status' => 0);
								//~ echo json_encode($response);
								redirect('booking/hotelOrders','refresh');
							}
						}
						else {
							redirect('booking/hotelOrders','refresh');
						}
                    }
                    if($api == "HOTELBEDS") {
						$api_cancel_pnr = $b_data->api_confirmation_no;
						$this->load->helper('hb_helper');
                        $CancelReq_Res = PurchaseCancelRQ($b_data->api_id, $api_cancel_pnr);
                        
                        $CancelReq = $CancelReq_Res['HotelBookingCancelRQ'];
                        $CancelRes = $CancelReq_Res['HotelBookingCancelRS'];
                        
                        /*
                        
                        $CancelReq = $CancelReq_Res['HotelBookingCancelRQ'] = '<?xml version="1.0" encoding="UTF-8"?>
								<PurchaseCancelRQ xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseCancelRQ.xsd" type="C">                                                                      
									<Language>ENG</Language>				
									<Credentials>
										<User>VIBRANIN26193</User>
										<Password>VIBRANIN26193</Password>
									</Credentials>
									<PurchaseReference>
										<FileNumber>654041</FileNumber>
										<IncomingOffice code="148"/>
									</PurchaseReference>
								</PurchaseCancelRQ>';
                        $CancelRes = $CancelReq_Res['HotelBookingCancelRS'] = '<PurchaseCancelRS xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseCancelRS.xsd" type="C"><AuditData><ProcessTime>2773</ProcessTime><Timestamp>2016-09-28 13:03:16.824</Timestamp><RequestHost>14.141.47.106</RequestHost><ServerName>FORM</ServerName><ServerId>FO</ServerId><SchemaRelease>2005/06</SchemaRelease><HydraCoreRelease>3.11.4.20160619</HydraCoreRelease><HydraEnumerationsRelease>3#v4Pc5fpmQCWzGOiKoLXJbA</HydraEnumerationsRelease><MerlinRelease>0</MerlinRelease></AuditData><Purchase purchaseToken="O3292320094" timeToExpiration="1799962"><Reference><FileNumber>654041</FileNumber><IncomingOffice code="148"></IncomingOffice></Reference><Status>CANCELLED</Status><Agency><Code>26193</Code><Branch>1</Branch></Agency><Language>ENG</Language><CreationDate date="20160920"/><CreationUser>VIBRANIN26193</CreationUser><Holder type="AD"><Age>0</Age><Name>GULSAN</Name><LastName>MAHAKUD</LastName></Holder><AgencyReference>LIYNOS57Z0XD</AgencyReference><ServiceList><Service xsi:type="ServiceHotel" SPUI="148#H#1"><Reference><FileNumber>654041-H1</FileNumber><IncomingOffice code="148"></IncomingOffice></Reference><Status>CANCELLED</Status><ContractList><Contract><Name>FIT-BB</Name><IncomingOffice code="148"></IncomingOffice><CommentList><Comment type="CONTRACT">Extra beds on demand YES (without additional debit notes). Check-in hour 14:00 â€“ 14:00. Wi-fi YES (with additional debit notes) 30 AED Per unit/night. Airport Shuttle. Identification card at arrival. Marriage certificate required for a couple to share room. </Comment></CommentList></Contract></ContractList><Supplier name="HOTELBEDS SPAIN S.L.U." vatNumber="ESB28916765"/><CommentList><Comment type="SERVICE">Test</Comment><Comment type="INCOMING">Test</Comment></CommentList><DateFrom date="20170222"/><DateTo date="20170223"/><Currency code="USD">US Dollar</Currency><TotalAmount>0.000</TotalAmount><AdditionalCostList><AdditionalCost type="AG_COMMISSION"><Price><Amount>0.000</Amount></Price></AdditionalCost><AdditionalCost type="COMMISSION_VAT"><Price><Amount>0.000</Amount></Price></AdditionalCost><AdditionalCost type="COMMISSION_PCT"><Price><Amount>0.000</Amount></Price></AdditionalCost></AdditionalCostList><ModificationPolicyList><ModificationPolicy>Cancellation</ModificationPolicy><ModificationPolicy>Confirmation</ModificationPolicy><ModificationPolicy>Modification</ModificationPolicy></ModificationPolicyList><HotelInfo xsi:type="ProductHotel"><Code>8919</Code><Name>Panorama Bur Dubai</Name><Category type="SIMPLE" code="2EST">2 STARS</Category><Destination type="SIMPLE" code="DXB"><Name>Dubai</Name><ZoneList><Zone type="SIMPLE" code="1">Dubai</Zone></ZoneList></Destination></HotelInfo><AvailableRoom><HotelOccupancy><RoomCount>1</RoomCount><Occupancy><AdultCount>1</AdultCount><ChildCount>0</ChildCount></Occupancy></HotelOccupancy><HotelRoom SHRUI="daEfWyakJtcgXAF4baYzSA==" availCount="1" status="CANCELLED"><Board type="SIMPLE" code="BB-E10">BED AND BREAKFAST</Board><RoomType type="SIMPLE" code="DBL-E10" characteristic="ST">DOUBLE STANDARD</RoomType><Price><Amount>0.000</Amount></Price><HotelRoomExtraInfo><ExtendedData><Name>INFO_ROOM_AGENCY_BOOKING_STATUS</Name><Value>O</Value></ExtendedData><ExtendedData><Name>INFO_ROOM_INCOMING_BOOKING_STATUS</Name><Value>O</Value></ExtendedData></HotelRoomExtraInfo></HotelRoom></AvailableRoom></Service></ServiceList><Currency code="USD"></Currency><PaymentData><PaymentType code="C"></PaymentType><Description>*55* Hotelbeds, S.L.U, Bank: CITIBANK(Citigroup Centre, Canary Wharf, London, E14 5LB. United Kingdom) Account:GB13 CITI 185008 13242420,  SWIFT:CITIGB2L,  7 days prior to clients arrival (except group bookings with fixed days in advance at the time of the confirmation) . Please indicate our reference number when making payment. Thank you for your cooperation.</Description></PaymentData><TotalPrice>0.000</TotalPrice></Purchase><Currency code="USD"></Currency><Amount>0.000</Amount></PurchaseCancelRS>';
                        
                        */
                        
                        if ($CancelRes != '') {
							$dom = new DOMDocument();
							$dom->loadXML($CancelRes);
							$Charge_API = $dom->getElementsByTagName("Amount")->item(0)->nodeValue;
							
							if ($dom->getElementsByTagName("PurchaseCancelRS")->item(0)->getElementsByTagName("DetailedMessage")->length > 0) {
								$data['exits'] = $dom->getElementsByTagName("PurchaseCancelRS")->item(0)->getElementsByTagName("DetailedMessage")->item(0)->nodeValue;
								$data['exits'] .= "<br /> OR Your hotel booking has not cancelled, !Please contact admin for further details";
							}
							if ($dom->getElementsByTagName("ServiceList")->length > 0) {
								$status = $dom->getElementsByTagName("ServiceList")->item(0)->getElementsByTagName("Status")->item(0)->nodeValue;
								$totalprice = $dom->getElementsByTagName("ServiceList")->item(0)->getElementsByTagName("TotalPrice")->item(0)->nodeValue;
								$Currency_code = $dom->getElementsByTagName("ServiceList")->item(0)->getElementsByTagName("Currency")->item(0)->getAttribute("code");
								
								if ($status == 'CANCELLED') {
									$hotel_amount = $totalprice;
									$inramount = $totalPrice;

									if ($inramount <= 0) {
										$inramount = 0;
									}
									
									$Refund = ($getBookingHotelData_v1->transaction_amount - $getBookingHotelData_v1->PG_Charge) - $Charge_API;
									
									$update_booking = array(
															'api_status' => 'Cancelled',
															'booking_status' => 'CANCELLED',
															'cancellation_status' => 'Cancelled',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
															'cancel_response' => $CancelReq_Res['HotelBookingCancelRS'],
															'cancellation_amount' => $Charge_API,
															'refund_amount' => $Refund
													);
									
									$data['exits'] = "Booking has been Sucessfully " . $HOTEL_STATUS;
									$this->Booking_Model->Update_Booking_Global($b_data->booking_no, $update_booking);
									//Cancel Email
									$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr_no)->result();

									foreach($data['pnr_nos'] as $pnr_nos):
										$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
										$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
										$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
										//echo '<pre>sanjay'; print_r($data['user_details'][0]->user_email); exit();
										$email['message'] = $this->load->view('vouchers/hotel_mailVoucher', $data, TRUE);
										$email['to'] = $data['user_details'][0]->user_email;
										//echo 'sanjay'; print_r($email['to']); exit();
						                $email['email_access'] = $this->Email_Model->get_email_acess();
						                
					            		$Response = $this->Email_Model->sendmail_hotelVoucher($email);
										redirect('booking/hotelOrders', 'refresh');
									endforeach;
									//Cancel Email End
								} else {
									$data['exits'] = "Your hotel booking has not cancelled, !Please contact admin for further details";
								}
							}
						}
						redirect('booking/hotelOrders','refresh');
					} 

                } else {
                    $response = array('status' => 0);
                    //~ echo json_encode($response);
                    redirect('booking/hotelOrders','refresh');
                }
            } else {
                $response = array('status' => 0);
                //~ echo json_encode($response);
                redirect('booking/hotelOrders','refresh');
            }
        }elseif($module == 'Transfer'){
			             $this->load->library('../controllers/Transferbooking', false);
		                $transferBooking = Transferbooking::transfer_cancel($pnr_no);
		                redirect('booking/transferOrders','refresh');
		}	
    }

	function flight_pre_cancellation($app_reference, $booking_source,$booking_status)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$this->load->model ('flight_model');
			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source,$booking_status);
			// debug($booking_details);
			// exit;
			if ($booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);
				$page_data['data'] = $assembled_booking_details['data'];
				// debug($page_data);
				// exit;
				$this->load->view('flight/pre_cancellation', $page_data) ;
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}

function flight_cancel_booking()
	{
		$post_data = $this->input->post();
		// debug($post_data);
		// exit;
		if (isset($post_data['app_reference']) == true && isset($post_data['booking_source']) == true && isset($post_data['transaction_origin']) == true &&
			valid_array($post_data['transaction_origin']) == true && isset($post_data['passenger_origin']) == true && valid_array($post_data['passenger_origin']) == true) {
			$app_reference = trim($post_data['app_reference']);
			$booking_source = trim($post_data['booking_source']);
			$booking_status = trim($post_data['booking_status']);
			$transaction_origin = $post_data['transaction_origin'];
			$passenger_origin = $post_data['passenger_origin']; 	
			$this->load->model ('flight_model');
			$booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source,$booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib($booking_source);
				//Formatting the Data
				$this->load->library('booking_data_formatter');
				$booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);

				// debug($booking_details);
				// exit;
				$booking_details = $booking_details['data'];
				//Grouping the Passenger Ticket Ids
				$grouped_passenger_ticket_details = $this->flight_lib->group_cancellation_passenger_ticket_id($booking_details, $passenger_origin); 
				// debug($grouped_passenger_ticket_details);
				// exit;
				if($booking_source == SABRE_FLIGHT_BOOKING_SOURCE)
				{
					$cancellation_details['status'] = $grouped_passenger_ticket_details; 
				}
				$passenger_origin = $grouped_passenger_ticket_details['passenger_origin'];
				$passenger_ticket_id = $grouped_passenger_ticket_details['passenger_ticket_id'];
				if($booking_source == PROVAB_FLIGHT_BOOKING_SOURCE)
				{
					// debug($booking_details);
					// debug($passenger_origin);
					// debug($passenger_ticket_id);
					// exit("212");
				$cancellation_details = $this->flight_lib->cancel_booking($booking_details, $passenger_origin, $passenger_ticket_id); 
				}
				if($cancellation_details['status'] ==1){
					$status = "BOOKING_CANCELLED";
				}else{
					$status = $cancellation_details['status'];
				}
				// debug($cancellation_details);
				// exit;
				redirect('booking/flight_cancellation_details/'.$app_reference.'/'.$booking_source.'/'.$status);    
			} else {  
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}

	function flight_cancellation_details($app_reference, $booking_source,$cancellation_status)
	{  
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$this->load->model ('flight_model');
			$master_booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source,$cancellation_status);
			// debug($master_booking_details);
			// exit("booking ctrlr-871");
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$page_data = array();
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_flight_booking_data($master_booking_details, $this->current_module);
				$page_data['data'] = $master_booking_details['data'];
				$page_data['cancellation_status'] = $cancellation_status; 
				// debug($page_data);
				// exit;
				$this->load->view('flight/cancellation_details', $page_data);
			} else {   
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}

	}

    function transfer_cancel() 
    {
		$pnr_no = '19835179';
		$this->load->helper('gtatransfer_helper');
		$response = TransferBookingCancel($pnr_no);	
	}

	function PercentageToAmount($total,$percentage){
        $amount = ($percentage/100) * $total;
        $total = $total + $amount;
        return $total;
    }
	
    function view_bookings_old($pnr_no='')
	{
		$user_type_id='';
		if(isset($_POST['pnr_no']) && $_POST['pnr_no']!='')
		{
			$pnr_no =$_POST['pnr_no'];
			$user_type_id =$_POST['user_type_id'];
			
		}
			$data['orders'] = $this->Booking_Model->get_order_details($pnr_no,$user_type_id);
			
			$data['passanger'] = $this->Booking_Model->get_passanger_details($data['orders']->booking_global_id);
			$data['transactions'] = $this->Booking_Model->get_transaction_details_all($data['orders']->booking_transaction_id);
			
			if($data['orders']->product_name == 'Hotels'){
				
				$data['hotel_data'] = $this->Booking_Model->get_hotel_booking_details($data['orders']->referal_id);
			}
			//echo '<pre>'; print_r($data); exit();
			$this->load->view('vouchers/hotel_voucher_view', $data); 
	} 

	public function view_bookings($parent_pnr=''){
		if(!empty($parent_pnr)){
			$parent_pnr = json_decode(base64_decode($parent_pnr));
			$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
			if($count > 0){
				$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();
				foreach($data['pnr_nos'] as $pnr_nos):
					$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
					$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					if($pnr_nos->product_id == 'Hotels' || $pnr_nos->product_id == 1){
					  $this->load->view('vouchers/hotel_voucher', $data);
				    }else{
					   $this->load->view('vouchers/transfer_voucher', $data);	
					}
				endforeach;
				
			}else{
				 $this->load->view('errors/404');
			}
		}else{
			 $this->load->view('errors/404');
		}
	}

	public function view_flight_bookings($parent_pnr=''){
		if(!empty($parent_pnr)){

			$pnr = json_decode(base64_decode($pnr));
			$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
			
			if($count > 0){
				$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();
				foreach($data['pnr_nos'] as $pnr_nos):
					$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
					$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					if($pnr_nos->product_id == 'Hotels' || $pnr_nos->product_id == 1){
					  $this->load->view('vouchers/hotel_voucher', $data);
				    }else{
					   $this->load->view('vouchers/transfer_voucher', $data);	
					}
				endforeach;
				
			}else{
				 $this->load->view('errors/404');
			}
		}else{
			 $this->load->view('errors/404');
		}
	}

	public function sendVoucher($parent_pnr=''){
		if(!empty($parent_pnr)){
			$parent_pnr = json_decode(base64_decode($parent_pnr));
			$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
			if($count > 0){
				$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();

				foreach($data['pnr_nos'] as $pnr_nos):
					$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
					$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					//echo '<pre>sanjay'; print_r($data['user_details'][0]->user_email); exit();
					$email['message'] = $this->load->view('vouchers/hotel_mailVoucher', $data, TRUE);
					$email['to'] = $data['user_details'][0]->user_email;
	                $email['email_access'] = $this->Email_Model->get_email_acess();
	                
            		$Response = $this->Email_Model->sendmail_hotelVoucher($email);
					redirect('booking/hotelOrders', 'refresh');
				endforeach;
				
			}else{
				  echo 'Invalid Data';
			}
		}else{
			  echo 'Invalid Data';
		}
	}

	public function refineSearch(){
		//echo '<pre>'; print_r($_POST); exit();
		$booking 					= $this->General_Model->getHomePageSettings();
		//$data['moduleType'] = $this->input->post('module');
		$data['moduleType'] = 'HOTEL';
		
		$data['orders'] = $this->Booking_Model->getRefineSearchResult($_POST);
		
		$data['transactions'] = $this->Booking_Model->get_transaction_details_all($data['Bookings'][0]->booking_transaction_id);
		//echo "<pre>"; print_r($data['transactions']); 
		$this->load->view('bookings/ajax_refine/ajaxRefineResults', $data);
	}

	public function transferrefineSearch(){
		//echo '<pre>'; print_r($_POST); exit();
		$booking 					= $this->General_Model->getHomePageSettings();
		//$data['moduleType'] = $this->input->post('module');
		$data['moduleType'] = 'Transfer';
		$data['orders'] 			= $this->Booking_Model->getRefinetransferSearchResult($_POST);
		$data['transactions'] = $this->Booking_Model->get_transaction_details_all($data['Bookings'][0]->booking_transaction_id);
		//echo "<pre>"; print_r($data['transactions']); 
		echo $this->load->view('bookings/ajax_refine/ajaxTranserRefineResults', $data, TRUE);
	}

	public function sendTransferVoucher($parent_pnr=''){
		if(!empty($parent_pnr)){
			$parent_pnr = json_decode(base64_decode($parent_pnr));
			$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
			if($count > 0){
				$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();

				foreach($data['pnr_nos'] as $pnr_nos):
					$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
					$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					//echo '<pre>sanjay'; print_r($data['user_details'][0]->user_email); exit();
					$email['message'] = $this->load->view('vouchers/transfer_emailvoucher', $data, TRUE);
					$email['to'] = $data['user_details'][0]->user_email;
	                $email['email_access'] = $this->Email_Model->get_email_acess();
	                
            		$Response = $this->Email_Model->sendmail_transferVoucher($email);
					redirect('booking/transferOrders', 'refresh');
				endforeach;
				
			}else{
				  echo 'Invalid Data';
			}
		}else{
			  echo 'Invalid Data';
		}
	}

	//Booknig Logs

	function view_request($parent_pnr){
		$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_booking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/requestBooking', $data);
	}

	function view_response($parent_pnr){
		$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_booking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/responseBooking', $data);
	}

	function view_payment_response($parent_pnr){
		$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_booking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/paymentresponseBooking', $data);
	}

	function view_transferrequest($parent_pnr){
		$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_transferbooking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/requestTransferBooking', $data);
	}

	function view_transferresponse($parent_pnr){
		$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_transferbooking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/responseTransferBooking', $data);
	}

	function view_transferpayment_response($parent_pnr){
		$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_transferbooking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/paymenttransferresponseBooking', $data);
	}

	/**
	 * Jaganath
	 */
	function hotel_pre_cancellation($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source);

			if ($booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				// debug($page_data);exit;
				$this->load->view('hotel/pre_cancellation', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}

	/*
	 * Jaganath
	 * Process the Booking Cancellation
	 * Full Booking Cancellation
	 *
	 */
	function hotel_cancel_booking($app_reference, $booking_source)
	{
		if(empty($app_reference) == false) {
			$master_booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_hotel_booking_data($master_booking_details, 'b2c');
				$master_booking_details = $master_booking_details['data']['booking_details'][0];
				load_hotel_lib($booking_source);
				
				$cancellation_details = $this->hotel_lib->cancel_booking($master_booking_details);//Invoke Cancellation Methods
				// debug($cancellation_details);
				// exit("11");
				if($cancellation_details['status'] == false) {
					$query_string = '?error_msg='.$cancellation_details['msg'];
				} else {
					$query_string = '';
				}
				redirect('booking/hotel_cancellation_details/'.$app_reference.'/'.$booking_source.$query_string);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	/**
	 * Jaganath
	 * Cancellation Details
	 * @param $app_reference
	 * @param $booking_source
	 */
	function hotel_cancellation_details($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $GLOBALS['CI']->hotel_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$page_data = array();
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_hotel_booking_data($master_booking_details, 'b2c');
				$page_data['data'] = $master_booking_details['data'];
				$this->load->view('hotel/cancellation_details', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}

	}

	/**
	 * Updates Cancellation Refund Details
	 */
	public function hotel_update_refund_details()
	{
		$post_data = $this->input->post();
		$redirect_url_params = array();
		$this->form_validation->set_rules('app_reference', 'app_reference', 'trim|required|xss_clean');
		$this->form_validation->set_rules('status', 'passenger_status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('status', 'passenger_status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('refund_payment_mode', 'refund_payment_mode', 'trim|required|xss_clean');
		$this->form_validation->set_rules('refund_amount', 'refund_amount', 'trim|numeric');
		$this->form_validation->set_rules('cancellation_charge', 'cancellation_charge', 'trim|numeric');
		$this->form_validation->set_rules('refund_status', 'refund_status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('refund_comments', 'refund_comments', 'trim|required');
		if ($this->form_validation->run()) {
			$app_reference = 				trim($post_data['app_reference']);
			$booking_source = 				trim($post_data['booking_source']);
			$status = 						trim($post_data['status']);
			$refund_payment_mode = 			trim($post_data['refund_payment_mode']);
			$refund_amount = 				floatval($post_data['refund_amount']);
			$cancellation_charge = 			floatval($post_data['cancellation_charge']);
			$refund_status = 				trim($post_data['refund_status']);
			$refund_comments = 				trim($post_data['refund_comments']);
			//Get Booking Details
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $status);
			if($booking_details['status'] == SUCCESS_STATUS){
				$master_booking_details = $booking_details['data']['booking_details'][0];
				$booking_currency = $master_booking_details['currency'];//booking currency
				$booked_user_id = intval($master_booking_details['created_by_id']);
				$user_condition[] = array('U.user_id' ,'=', $booked_user_id);
				$booked_user_details = $this->user_model->get_user_details($user_condition);
				$is_agent = false;
				if(valid_array($booked_user_details) == true && $booked_user_details[0]['user_type'] == B2B_USER){
					$is_agent = true;
				}
				//REFUND AMOUNT TO AGENT
				$currency_obj = new Currency(array('from' => get_application_default_currency() , 'to' => $booking_currency));
				$currency_conversion_rate = $currency_obj->currency_conversion_value(true, get_application_default_currency(), $booking_currency);
				if($refund_status == 'PROCESSED' && floatval($refund_amount) > 0 && $is_agent == true){
					//1.Crdeit the Refund Amount to Respective Agent
					$agent_refund_amount = ($currency_conversion_rate*$refund_amount);//converting to agent currency

					//2.Add Transaction Log for the Refund
					$fare = -($refund_amount);//dont remove: converting to negative
					$domain_markup=0;
					$level_one_markup=0;
					$convinence = 0;
					$discount = 0;
					$remarks = 'hotel Refund was Successfully done';
					$this->domain_management_model->save_transaction_details('hotel', $app_reference, $fare, $domain_markup, $level_one_markup, $remarks, $convinence, $discount, $booking_currency, $currency_conversion_rate, $booked_user_id);

					// update agent balance
					$this->domain_management_model->update_agent_balance($agent_refund_amount, $booked_user_id);
				}
				//UPDATE THE REFUND DETAILS
				//Update Condition
				$update_refund_condition = array();
				$update_refund_condition['app_reference'] =	$app_reference;
				//Update Data
				$update_refund_details = array();
				$update_refund_details['refund_payment_mode'] = 			$refund_payment_mode;
				$update_refund_details['refund_amount'] =					$refund_amount;
				$update_refund_details['cancellation_charge'] = 			$cancellation_charge;
				$update_refund_details['refund_status'] = 					$refund_status;
				$update_refund_details['refund_comments'] = 				$refund_comments;
				$update_refund_details['currency'] = 						$booking_currency;
				$update_refund_details['currency_conversion_rate'] = 		$currency_conversion_rate;
				if($refund_status == 'PROCESSED'){
					$update_refund_details['refund_date'] = 				date('Y-m-d H:i:s');
				}
				$this->custom_db->update_record('hotel_cancellation_details', $update_refund_details, $update_refund_condition);
				
				$redirect_url_params['app_reference'] = $app_reference;
				$redirect_url_params['booking_source'] = $master_booking_details['booking_source'];
				$redirect_url_params['status'] = $status;
			}
		}
		redirect('booking/hotel_cancellation_refund_details?'.http_build_query($redirect_url_params));
	}
	/**
	 * Jaganath
	 * Get supplier cancellation status
	 */
	public function hotel_update_supplier_cancellation_status_details()
	{
		$get_data = $this->input->get();
		if(isset($get_data['app_reference']) == true && isset($get_data['booking_source']) == true && isset($get_data['status']) == true && $get_data['status'] == 'BOOKING_CANCELLED'){
			$app_reference = trim($get_data['app_reference']);
			$booking_source = trim($get_data['booking_source']);
			$status = trim($get_data['status']);
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $status);
			if($booking_details['status'] == SUCCESS_STATUS){
				$master_booking_details = $booking_details['data']['booking_details'];
				$booking_customer_details = $booking_details['data']['booking_customer_details'][0];
				$cancellation_details = $booking_details['data']['cancellation_details'][0];
				$ChangeRequestId =		$cancellation_details['ChangeRequestId'];
				load_hotel_lib($booking_source);
				$response = $this->hotel_lib->get_cancellation_refund_details($ChangeRequestId, $app_reference);
				if($response['status'] == SUCCESS_STATUS){
					$cancellation_details = $response['data'];
					$this->hotel_model->update_cancellation_refund_details($app_reference, $cancellation_details);
				}
			}
		}
	}

	public function hotel_cancellation_refund_details()
	{
		$get_data = $this->input->get();

		if(isset($get_data['app_reference']) == true && isset($get_data['booking_source']) == true && isset($get_data['status']) == true && $get_data['status'] == 'BOOKING_CANCELLED'){
			$app_reference = trim($get_data['app_reference']);
			$booking_source = trim($get_data['booking_source']);
			$status = trim($get_data['status']);

			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $status);

			if($booking_details['status'] == SUCCESS_STATUS){
				$booked_user_id = intval($booking_details['data']['booking_details'][0]['user_details_id']);
				$booked_user_details = array();
				$is_agent = false;
				$user_condition[] = array('U.user_details_id' ,'=', $booked_user_id);
				// debug($booking_details);exit;
				$booked_user_details = $this->user_model->get_user_details($user_condition);
			// 	echo $this->db->last_query();
			// exit;
				// debug($booked_user_details);
				// exit;	
				if(valid_array($booked_user_details) == true){
					$booked_user_details = $booked_user_details[0];
					if($booked_user_details['user_type'] == B2B_USER){
						$is_agent = true;
					}else{
						$is_agent =false;
					}
				}
				$page_data = array();
				$page_data['booking_data'] = 		$booking_details['data'];
				$page_data['booked_user_details'] =	$booked_user_details;
				$page_data['is_agent'] = 			$is_agent;
				$this->load->view('booking/hotel_cancellation_refund_details', $page_data);
			} else {
				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	}

	//End of Booking Logs
}
