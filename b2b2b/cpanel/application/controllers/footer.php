<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Footer extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Footer_Model');
		$this->load->model('Header_Model');
		$this->load->model('Product_Model');
		$this->load->model('Usertype_Model');
		$this->load->library('form_validation');
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	 $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
	function footerList(){
		$footer 				= $this->General_Model->getHomePageSettings();
		$footer['footer_list'] 	= $this->Footer_Model->getFooterList();
		$this->load->view('footer/footer_list',$footer);
	}
	
	function addFooter(){
		if(count($_POST) > 0){
			
			$this->Footer_Model->addFooterDetails($_POST);
			redirect('footer/footerList','refresh');
		}else{
			$footer = $this->General_Model->getHomePageSettings();
			$this->load->view('footer/add_footer',$footer);
		}
	}
	
	function activeFooter($footer_id){
		$footer_id = json_decode(base64_decode($footer_id));
		if($footer_id != ''){
			$this->Footer_Model->activeFooter($footer_id);
		}
		redirect('footer/footerList','refresh');
	}
	
	function inactiveFooter($footer_id){
		$footer_id = json_decode(base64_decode($footer_id));
		if($footer_id != ''){
			$this->Footer_Model->inactiveFooter($footer_id);
		}
		redirect('footer/footerList','refresh');
	}
	
	function deleteFooter($footer_id){
		$footer_id = json_decode(base64_decode($footer_id));
		if($footer_id != ''){
			$this->Footer_Model->deleteFooter($footer_id);
		}
		redirect('footer/footerList','refresh');
	}
	
	function editFooter($footer_id)
	{
		$footer_id = json_decode(base64_decode($footer_id));
		if($footer_id != ''){
			$footer 			   = $this->General_Model->getHomePageSettings();
			$footer['footer_list'] = $this->Footer_Model->getFooterList($footer_id);
			$this->load->view('footer/edit_footer',$footer);
		} else {
			redirect('footer/footerList','refresh');
		}
	}

	function updateFooter($footer_id){
		if(count($_POST) > 0){
			$footer_id = json_decode(base64_decode($footer_id));
			if($footer_id != ''){
				//echo '<pre>'; print_r($_POST); exit();
				$this->Footer_Model->updateFooter($_POST,$footer_id);
			} 
			redirect('footer/footerList','refresh');
		}else if($footer_id!=''){
			redirect('footer/footerList','refresh');
		}else{
			redirect('footer/footerList','refresh');
		}
	}

	function subFooterList(){
		$footer 				= $this->General_Model->getHomePageSettings();
		$footer['footer_list'] 	= $this->Footer_Model->getsubFooterList();
		$this->load->view('footer/subfooter_list',$footer);
	}

	function addSubFooter(){
		if(count($_POST) > 0){
			
			$this->Footer_Model->addSubFooterDetails($_POST);
			redirect('footer/subFooterList','refresh');
		}else{
			$footer = $this->General_Model->getHomePageSettings();
			$this->load->view('footer/add_subfooter',$footer);
		}
	}

  	function activeSubFooter($footer_id){
		$footer_id = json_decode(base64_decode($footer_id));
		if($footer_id != ''){
			$this->Footer_Model->activeSubFooter($footer_id);
		}
		redirect('footer/subFooterList','refresh');
	}
	
	function inactiveSubFooter($footer_id){
		$footer_id = json_decode(base64_decode($footer_id));
		if($footer_id != ''){
			$this->Footer_Model->inactiveSubFooter($footer_id);
		}
		redirect('footer/subFooterList','refresh');
	}

		function deleteSubFooter($footer_id){
		$footer_id = json_decode(base64_decode($footer_id));
		if($footer_id != ''){
			$this->Footer_Model->deleteSubFooter($footer_id);
		}
		redirect('footer/subFooterList','refresh');
	}
	
	function editSubFooter($footer_id)
	{
		$footer_id = json_decode(base64_decode($footer_id));
		if($footer_id != ''){
			$footer 			   = $this->General_Model->getHomePageSettings();
			$footer['footer_list'] = $this->Footer_Model->getsubFooterList($footer_id);
			$this->load->view('footer/edit_subfooter',$footer);
		} else {
			redirect('footer/subFooterList','refresh');
		}
	}

	function footer_list($footer_id){
		$footer_id = json_decode(base64_decode($footer_id));
		if($footer_id != ''){
			$footers 				= $this->General_Model->getHomePageSettings();
			$footers['category_id']	= $footer_id;
			$footers['deals_list'] 	= $this->Footer_Model->getManageFooterList($footer_id);
			$this->load->view('footer/footermanage_list', $footers);
		} else {
			redirect('footerList','refresh');
		}
	}

	function addManageFooter($footer_id) { 
		$category_id1 = json_decode(base64_decode($footer_id)); 
        if (count($_POST) > 0) {
        	$category_id1 = json_decode(base64_decode($footer_id));
           
            $this->Footer_Model->add_managefooter($_POST,$category_id1);
            redirect('footer/footer_list/'.$footer_id, 'refresh');
        } else {
            $data = $this->General_Model->getHomePageSettings();
            $data['footer_id']	= $footer_id;
            $this->load->view('footer/add_footermanage', $data);
        }
    }

    function activeManageFooter($footer_id,$footer){
		$footer_id1 = json_decode(base64_decode($footer_id));
		if($footer_id1 != ''){
			$this->Footer_Model->activeManageFooter($footer_id1);
		}
		redirect('footer/footer_list/'.$footer,'refresh');
	}
	
	function inactiveManageFooter($footer_id,$footer){
		$footer_id1 = json_decode(base64_decode($footer_id));
		if($footer_id1 != ''){
			$this->Footer_Model->inactiveManageFooter($footer_id1);
		}
		redirect('footer/footer_list/'.$footer,'refresh');
	}

	function editManageFooter($footer_id,$footers)
	{
		$footer_id1 = json_decode(base64_decode($footer_id));
		$footer['footer_id'] = $footer_id; //print_r($footer['footer_id']);
		if($footer_id != ''){
			$footer 			   = $this->General_Model->getHomePageSettings();
			$footer['footer_list'] = $this->Footer_Model->getFooterManagementList($footer_id1);
			
			$this->load->view('footer/edit_managefooter',$footer);
		} else {
			redirect('footer/footerList','refresh');
		}
	}

	function updateManageFooter($footer_id,$footer){
		if(count($_POST) > 0){
			$footer_id = json_decode(base64_decode($footer_id));
			if($footer_id != ''){
				
				$this->Footer_Model->updateManageFooter($_POST,$footer_id);
			} 
			redirect('footer/footer_list/'.$footer,'refresh');
		}else if($footer_id!=''){
			redirect('footer/footer_list/'.$footer,'refresh');
		}else{
			redirect('footer/footer_list/'.$footer,'refresh');
		}
	}

	function updateSubFooter($footer_id){
		if(count($_POST) > 0){
			$footer_id = json_decode(base64_decode($footer_id));
			if($footer_id != ''){
				
				$this->Footer_Model->updateSubFooter($_POST,$footer_id);
			} 
			redirect('footer/subFooterList','refresh');
		}else if($footer_id!=''){
			redirect('footer/subFooterList','refresh');
		}else{
			redirect('footer/subFooterList','refresh');
		}
	}
	function deleteManageFooter($footer_id,$footer){
		$footer_id = json_decode(base64_decode($footer_id));
		if($footer_id != ''){
			$this->Footer_Model->deleteManageFooter($footer_id);
		}
		redirect('footer/footer_list/'.$footer,'refresh');
	}

	//Static Page Management
	function CarrersList($header_id){
		$footer 				= $this->General_Model->getHomePageSettings();
		$footer['headerid'] = $header_id;
		$footer['service_list'] 	= $this->Footer_Model->getFooterCarrersList();

		$this->load->view('footer/carrer_list',$footer);
	}

	function Carrers($header_id1){
		$header_id 	= json_decode(base64_decode($header_id1));
		if(count($_POST) > 0){ 
			
			$this->Footer_Model->addFooterCarrersList($_POST);
			redirect('footer/CarrersList/'.$header_id1,'refresh');
		}else{

			$header = $this->General_Model->getHomePageSettings();
			$header['headerid'] = $header_id;
			$header['product'] 	=  $this->Product_Model->getProductList();
			$header['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$this->load->view('footer/add_carrer',$header);
		}
	}

	function inactiveFooterCarrer($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Footer_Model->inactiveFooterCarrer($header_id);
		}
		redirect('footer/CarrersList/'.$header,'refresh');
	}

	function activeFooterCarrer($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Footer_Model->activeFooterCarrer($header_id);
		}
		redirect('footer/CarrersList/'.$header,'refresh');
	}

	function deleteFooterCarrer($header_id1,$header){ 
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Footer_Model->deleteFooterCarrer($header_id);
		}
		redirect('footer/CarrersList/'.$header,'refresh');
	}

	function editFooterCarrer($header_id1,$headers)
	{	
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$header 					= $this->General_Model->getHomePageSettings();
			$header['header_list'] 		= $this->Footer_Model->getFooterCarrersList($header_id);
			$header['product_id'] = $headers; 
			$this->load->view('footer/edit_footercarrer',$header);
		}else{
			redirect('footer/CarrersList/'.$headers,'refresh');
		}
	}

	function updateCarrer($header_id,$header)
	{	
		$header_id 	= json_decode(base64_decode($header_id)); 
		if($header_id != ''){			
			if(count($_POST) > 0){
				
				$this->Footer_Model->updateCarrer($_POST, $header_id);
				redirect('footer/CarrersList/'.$header,'refresh');
			}else if($header_id!=''){
				redirect('footer/editFooterCarrer/'.$header_id.'/'.$header,'refresh');
			}else{
				redirect('footer/CarrersList/'.$header,'refresh');
			}
		}else{
			redirect('footer/CarrersList/'.$header,'refresh');
		}
	}
		//Termsnconditions
	function TermsnconditionsList($header_id){
		$footer 				= $this->General_Model->getHomePageSettings();
		$footer['headerid'] = $header_id;
		$footer['service_list'] 	= $this->Footer_Model->getFooterTermsnconditionsList();

		$this->load->view('footer/termsncondition_list',$footer);
	}

	function Termsnconditions($header_id1){
		$header_id 	= json_decode(base64_decode($header_id1));
		if(count($_POST) > 0){ 
			
			$this->Footer_Model->addFooterTermsnconditions($_POST);
			redirect('footer/TermsnconditionsList/'.$header_id1,'refresh');
		}else{

			$header = $this->General_Model->getHomePageSettings();
			$header['headerid'] = $header_id;
			$header['product'] 	=  $this->Product_Model->getProductList();
			$header['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$this->load->view('footer/add_termsncondition',$header);
		}
	}

	function inactiveFooterTermsnconditions($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Footer_Model->inactiveFooterTermsnconditions($header_id);
		}
		redirect('footer/TermsnconditionsList/'.$header,'refresh');
	}

	function activeFooterTermsnconditions($header_id1,$header){
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Footer_Model->activeFooterTermsnconditions($header_id);
		}
		redirect('footer/TermsnconditionsList/'.$header,'refresh');
	}

	function deleteFooterTermsnconditions($header_id1,$header){ 
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$this->Footer_Model->deleteFooterTermsnconditions($header_id);
		}
		redirect('footer/TermsnconditionsList/'.$header,'refresh');
	}

	function editFooterTermsnconditions($header_id1,$headers)
	{	
		$header_id 	= json_decode(base64_decode($header_id1));
		if($header_id != ''){
			$header 					= $this->General_Model->getHomePageSettings();
			$header['header_list'] 		= $this->Footer_Model->getFooterTermsnconditionsList($header_id);
			$header['product_id'] = $headers; 
			$this->load->view('footer/edit_footerterms',$header);
		}else{
			redirect('footer/TermsnconditionsList/'.$headers,'refresh');
		}
	}

	function updateTermsnconditions($header_id,$header)
	{	
		$header_id 	= json_decode(base64_decode($header_id)); 
		if($header_id != ''){			
			if(count($_POST) > 0){
				
				$this->Footer_Model->updateTermsnconditions($_POST, $header_id);
				redirect('footer/TermsnconditionsList/'.$header,'refresh');
			}else if($header_id!=''){
				redirect('footer/editFooterTermsnconditions/'.$header_id.'/'.$header,'refresh');
			}else{
				redirect('footer/TermsnconditionsList/'.$header,'refresh');
			}
		}else{
			redirect('footer/TermsnconditionsList/'.$header,'refresh');
		}
	}

	//End Terms N Conditions
	//End Of Static Page Management
}
