<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Country extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Country_Model');
		$this->load->library('form_validation');	
		if(!isset($_SESSION['ses_id'])){
			$sec_res 			= session_id();
	    	$_SESSION['ses_id'] = $sec_res;
		}
		
		$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function index(){
		$country 					= $this->General_Model->getHomePageSettings();
		$country['country_list'] 	= $this->Country_Model->getCountryList();
		$this->load->view('country/country_list',$country);
	}
	
	function add_country(){
		$country = $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
			$form_validator = $this->formValidator('add');	
			if($form_validator == FALSE  ) {
				$this->load->view('country/add_country',$country);	
			    }else{
			$this->Country_Model->add_country_details($_POST);
			redirect('country','refresh');
		}
		}else{
			
			$this->load->view('country/add_country',$country);
		}
	}
	
	function activeCountry($country_id1){
		$country_id 	= json_decode(base64_decode($country_id1));
		if($country_id != ''){
			$this->Country_Model->activeCountry($country_id);
		}
		redirect('country','refresh');
	}
	
	function inactiveCountry($country_id1){
		$country_id 	= json_decode(base64_decode($country_id1));
		if($country_id != ''){
			$this->Country_Model->inactiveCountry($country_id);
		}
		redirect('country','refresh');
	}
	
	function delete_country($country_id1){
		$country_id 	= json_decode(base64_decode($country_id1));
		if($country_id != ''){
			$this->Country_Model->delete_country($country_id);
		}
		redirect('country','refresh');
	}
	
	function editCountry($country_id1)
	{
		$country_id 	= json_decode(base64_decode($country_id1));
		if($country_id != ''){
			$country 					= $this->General_Model->getHomePageSettings();
			$country['country_list'] 	= $this->Country_Model->getCountryList($country_id);
			$this->load->view('country/edit_country',$country);
		}else{
			redirect('country','refresh');
		}
	}

	function update_country($country_id1)
	{
		$country_id 	= json_decode(base64_decode($country_id1));
		if($country_id != ''){
			if(count($_POST) > 0){
				
				$this->Country_Model->updateCountry($_POST,$country_id);
				redirect('country','refresh');
			}else if($country_id!=''){
				redirect('country/edit_country/'.$country_id,'refresh');
			}else{
				redirect('country','refresh');
			}
		}else{
			redirect('country','refresh');
		}
	}

	function formValidator($type){
	   $this->form_validation->set_rules('iso_code', 'Iso Code', 'required');
	   $this->form_validation->set_rules('iso3_code', 'Iso3 Code', 'required');
	   $this->form_validation->set_rules('country_name', 'Country Name', 'required');
	   if($type == 'add') {
	   	$this->form_validation->set_rules('country_name', 'Country Name', 'trim|required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('iso_code', 'Iso Code', 'required|xss_clean||is_unique[country_details.iso_code]|max_length[2]');
	    $this->form_validation->set_rules('iso3_code', 'Iso3 Code', 'required|xss_clean||is_unique[country_details.iso3_code]|max_length[3]');
	    }
	   return $this->form_validation->run();
    }	
	

}
