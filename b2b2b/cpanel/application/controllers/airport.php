<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Airport extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Airport_Model');
		$this->load->library('form_validation');
		if(!isset($_SESSION['ses_id'])){
			$sec_res 			= session_id();
	    	$_SESSION['ses_id'] = $sec_res;
		}
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	   $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
	function index(){
		$airline 					= $this->General_Model->getHomePageSettings();
		$airline['airport_list'] 	= $this->Airport_Model->get_airPort_list();
		$this->load->view('airport/airPortList',$airline);
	}
	
	function add_airline(){
		if(count($_POST) > 0){
			//print_r($_POST); exit();
			
			$this->Airline_Model->add_airline_details($_POST);
			redirect('airline','refresh');
		}else{
			$airline = $this->General_Model->getHomePageSettings();
			$this->load->view('airline/add_airline',$airline);
		}
	}
	
	function active_airline($airline_id){
		$this->Airline_Model->active_airline($airline_id);
		redirect('airline','refresh');
	}
	
	function inactive_airline($airline_id){
		$this->Airline_Model->inactive_airline($airline_id);
		redirect('airline','refresh');
	}
	
	function delete_airline($airline_id){
		$this->Airline_Model->delete_airline($airline_id);
		redirect('airline','refresh');
	}
	
	function edit_airline($airline_id)
	{
		$airline 					= $this->General_Model->getHomePageSettings();
		$airline['airline_list'] 	= $this->Airline_Model->get_airline_list($airline_id);
		$this->load->view('airline/edit_airline',$airline);
	}

	function update_airline($airline_id)
	{
		if(count($_POST) > 0){
			
			
			$this->Airline_Model->update_airline($_POST,$airline_id);
			redirect('airline','refresh');
		}else if($airline_id!=''){
			redirect('airline/edit_airline/'.$airline_id,'refresh');
		}else{
			redirect('airline','refresh');
		}
	}

	function airline_details_insert(){
		$airline_list 	= $this->Airline_Model->get_airline_list();
		for($i=0;$i<count($airline_list);$i++){
			$image = FCPATH."assets/airline_logo/".$airline_list[$i]->AirLineName.".jpg";
			if(file_exists($image)) {
				echo "The file $image exists";
			} else {
				echo "The file $image does not exist";
			}
			echo "<br/>";
			$insert_data = array(
							'airline_name' 			=> $airline_list[$i]->AirLineName,
							'airline_code' 			=> $airline_list[$i]->AirLineCode,
							'airline_logo' 			=> '',
							'provider_type' 		=> $airline_list[$i]->ProviderType,
							'status' 				=> 'ACTIVE',
							'airline_creation_date'	=> (date('Y-m-d H:i:s'))					
						);			
			// $this->db->insert('airline_details',$insert_data);
		}
		
	}
}
