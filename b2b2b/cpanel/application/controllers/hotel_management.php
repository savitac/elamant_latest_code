<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Hotel_management extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Product_Model');
		$this->load->model('Hotel_Management_Model');
		$this->load->library('form_validation');
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	$this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
	function index(){
		$this->load->view('hotel/hotel_management/index');
	}
	function gtaIndex(){
		$this->load->view('hotel/hotel_management/gta/index');
	}
	function gtaFacilities() {
		$data['gtaFacilities'] = $this->Hotel_Management_Model->gtaFacilityList();
		$this->load->view('hotel/hotel_management/gta/facilities',$data);
	}
	function editGTAFacility($id='') {
		$GTAFacility = $this->General_Model->getHomePageSettings();
		if($id != ''):
			$id = json_decode(base64_decode($id));
			$Facility['details'] = $this->Hotel_Management_Model->selectRecords('gta_Facilities',implode(',',array('*')),array('id'=>$id));
			$this->load->view('hotel/hotel_management/gta/edit_facility',$Facility);
		else:
			redirect('hotel_management/gtaFacilities','refresh');
		endif;
	}
	function updateGTAFacility($id='') {
		if($id != ''):
			$id = json_decode(base64_decode($id));
			if(count($_POST) > 0):
				$updateData = array(
									'facility' => $this->input->post('facility_name')
							);
				$condition = array(
								'id' => $id
							);
				//~ echo "data: <pre>";print_r($condition);exit;
				$this->Hotel_Management_Model->updateRecords('gta_Facilities',$updateData,$condition);
			else:
			
			endif;
			redirect('hotel_management/gtaFacilities','refresh');
		else:
			redirect('hotel_management/gtaFacilities','refresh');
		endif;
	}
	function gtaHotels() {
		if(!empty($this->input->get('city'))):
			$city = $this->input->get('city');
		else:
			$city = 'BLR';
		endif;
		$data['gtaHotelsCityCodes'] = $this->Hotel_Management_Model->gtaHotelCityCodeList($city);
		$data['gtaHotels'] = $this->Hotel_Management_Model->gtaHotelList($city);
		$data['city_code'] = $city;
		$this->load->view('hotel/hotel_management/gta/hotels',$data);
	}	
	function editGTAHotel($id='') {
		$GTAHotel = $this->General_Model->getHomePageSettings();
		if($id != ''):
			$id = json_decode(base64_decode($id));
			$GTAHotel['details'] = $this->Hotel_Management_Model->selectRecords('gta_Hotels',implode(',',array('*')),array('id'=>$id));
			$this->load->view('hotel/hotel_management/gta/edit_hotel',$GTAHotel);
		else:
			redirect('hotel_management/gtaHotels','refresh');
		endif;
	}
	function updateGTAHotel($id='') {
		if($id != ''):
			$id = json_decode(base64_decode($id));
			if(count($_POST) > 0):
				$updateData = array(
									'HotelName' => $this->input->post('hotel_name'),
									'ReportInfo' => $this->input->post('report_info'),
									'RoomFacilityName' => $this->input->post('room_facility_name'),
									'HotelFacilityName' => $this->input->post('hotel_facility_name')
							);
				$condition = array(
								'id' => $id
							);
				//~ echo "data: <pre>";print_r($condition);exit;
				$this->Hotel_Management_Model->updateRecords('gta_Hotels',$updateData,$condition);
			else:
			
			endif;
			redirect('hotel_management/gtaHotels','refresh');
		else:
			redirect('hotel_management/gtaHotels','refresh');
		endif;
	}
	function hotelbedsIndex(){
		$this->load->view('hotel/hotel_management/hotelbeds/index');
	}
	function hotelBedsHotels() {
		if(!empty($this->input->get('country'))):
			$country = $this->input->get('country');
		else:
			$country = 'IN';
		endif;
		$data['hotelBedsHotelsCountryCodes'] = $this->Hotel_Management_Model->hotelBedsHotelCountryCodeList($country);
		$data['hotelBedsHotels'] = $this->Hotel_Management_Model->hotelBedsHotelList($country);
		$data['country_code'] = $country;
		$this->load->view('hotel/hotel_management/hotelbeds/hotels',$data);
	}
	function editHotelBedsHotel($id='') {
		$hotelBedsHotel = $this->General_Model->getHomePageSettings();
		if($id != ''):
			$id = json_decode(base64_decode($id));
			$hotelBedsHotel['details'] = $this->Hotel_Management_Model->selectRecords('hotelbeds_Hotels',implode(',',array('*')),array('fly2escape_id'=>$id));
			$this->load->view('hotel/hotel_management/hotelbeds/edit_hotel',$hotelBedsHotel);
		else:
			redirect('hotel_management/hotelBedsHotels','refresh');
		endif;
	}
	function updateHotelBedsHotel($id='') {
		if($id != ''):
			$id = json_decode(base64_decode($id));
			if(count($_POST) > 0):
				$updateData = array(
									'Name' => $this->input->post('hotel_name'),
									'hotel_info' => $this->input->post('hotel_info')
							);
				$condition = array(
								'fly2escape_id' => $id
							);
				//~ echo "data: <pre>";print_r($condition);exit;
				$this->Hotel_Management_Model->updateRecords('hotelbeds_Hotels',$updateData,$condition);
			else:
			
			endif;
			redirect('hotel_management/hotelBedsHotels','refresh');
		else:
			redirect('hotel_management/hotelBedsHotels','refresh');
		endif;
	}
	function hotelBedsDestinations() {
		if(!empty($this->input->get('country'))):
			$country = $this->input->get('country');
		else:
			$country = 'IN';
		endif;
		//~ $data['hotelBedsDestinationsCountryCodes'] = $this->Hotel_Management_Model->selectRecords('hotelbeds_Destinations',implode(',',array('CountryCode,CountryName')),array('CountryCode'=>$country));
		$data['hotelBedsDestinationsCountryCodes'] = $this->Hotel_Management_Model->hotelBedsDestinationCountryCodeList();
		$data['hotelBedsDestinations'] = $this->Hotel_Management_Model->selectRecords('hotelbeds_Destinations',implode(',',array('*')),array('CountryCode'=>$country));
		//~ echo "country: <pre>";print_r($data['hotelBedsDestinationsCountryCodes']);exit;
		$data['country_code'] = $country;
		$this->load->view('hotel/hotel_management/hotelbeds/destinations',$data);
	}
	function editHotelBedsDestination($id='') {
		$hotelBedsDestination = $this->General_Model->getHomePageSettings();
		if($id != ''):
			$id = json_decode(base64_decode($id));
			$hotelBedsDestination['details'] = $this->Hotel_Management_Model->selectRecords('hotelbeds_Destinations',implode(',',array('*')),array('DestinationCode'=>$id));
			$this->load->view('hotel/hotel_management/hotelbeds/edit_destination',$hotelBedsDestination);
		else:
			redirect('hotel_management/hotelBedsHotels','refresh');
		endif;
	}
	function updateHotelBedsDestination($id='') {
		if($id != ''):
			$id = json_decode(base64_decode($id));
			if(count($_POST) > 0):
				$updateData = array(
									'DestinationName' => $this->input->post('destination_name')
							);
				$condition = array(
								'DestinationCode' => $id
							);
				//~ echo "data: <pre>";print_r($condition);exit;
				$this->Hotel_Management_Model->updateRecords('hotelbeds_Destinations',$updateData,$condition);
			else:
			
			endif;
			redirect('hotel_management/hotelBedsDestinations','refresh');
		else:
			redirect('hotel_management/hotelBedsDestinations','refresh');
		endif;
	}
	function hotelBedsFacilities() {
		if(!empty($this->input->get('code'))):
			$code = $this->input->get('code');
		else:
			$code = 10;
		endif;
		//~ ini_set('memory_limit',-1);
		$data['hotelBedsFacilitiesCodes'] = $this->Hotel_Management_Model->hotelBedsFacilitiesCodeList();
		$data['hotelBedsFacilities'] = $this->Hotel_Management_Model->hotelBedsFacilities($code);
		//~ echo "country: <pre>";print_r($data['hotelBedsFacilities']);exit;
	}
	
	function cityMappingIndex(){
		$this->load->view('hotel/hotel_management/citymapping/index');
	}
	function citylist(){
		if(!empty($this->input->get('country'))):
			$country = $this->input->get('country');
		else:
			$country = 'India';
		endif;
		$data['distinctCountryList'] = $this->Hotel_Management_Model->apiHotelCityCountryList();
		$data['countryList'] = $this->Hotel_Management_Model->selectRecords('api_hotels_cities',implode(',',array('*')),array('country'=>$country));
		$data['country_code'] = $country;
		exit;
		$this->load->view('hotel/hotel_management/citymapping/citylist',$data);
	}
}
