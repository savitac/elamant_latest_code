<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controller having Creating Roles/Sub Admin User Functionality 
 * @package    Provab Whitelabel Application
 * @author     PAWAN BAGGA
 * @version    V1
**/ 
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Roles extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Roles_Model');
		$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
	function managePrivilege($roles_id1)
	{
		
		$this->db->select('role_details_id'); 
		$this->db->where('role_details_id',$_POST['role_id']);
		$query=$this->db->get('privilege_details');
         $id = $_POST['role_id'];
		
		if($query->num_rows()>0){
			$this->db->where('role_details_id',$_POST['role_id']);
			$this->db->delete('privilege_details');
			$action_key = "modified_by";
			$action_date_key = "modification_date";
	    }
		else{ $action_key = "created_by"; $action_date_key = "creation_date";}
		for($c=0;$c<count($_POST['privilege_ids']);$c++){
			
			$priv = explode('-', $_POST['privilege_ids'][$c]);
		    $insert_privilege_data = array(
            "role_details_id"=>$id,
			"dashboard_module_id"=>$priv[0],
			"dashboard_module_details_id"=>$priv[1],
			"status"=> 'ACTIVE',
			$action_key => $this->session->userdata('provabAdminId'),
			$action_date_key => date('Y-m-d H:i:s'),
          );
         $this->db->insert('privilege_details',$insert_privilege_data);
        }
       redirect('roles/rolesList','refresh');
	}
	function rolesList(){
	
		$roles 					= $this->General_Model->getHomePageSettings();
		$roles['roles_list']    = $this->Roles_Model->get_roles_list();
		$this->load->view('roles/roles_list',$roles);
	}
	
	function addRoles(){
		$roles = $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
				$form_validator = $this->formValidator('add');	
			  if($form_validator == FALSE  ) {
				$this->load->view('roles/add_roles',$roles);	
			    }else{
			if($_FILES['image_info']['name'] != ""){
			$roles_logo_name = $this->General_Model->upload_image($_FILES, 'roles','hotel_image');
		    }else {
			 $roles_logo_name = "";
			}
			$this->Roles_Model->add_roles_details($_POST,$roles_logo_name);
			
			redirect('roles/rolesList','refresh');
		}
		}else{
			
			$this->load->view('roles/add_roles',$roles);
		}
	}
	
	function active_roles($roles_id){
		//$roles_id = json_decode(base64_decode($roles_id));
		if($roles_id != ''){
			$this->Roles_Model->active_roles($roles_id);
		}
		redirect('roles/rolesList','refresh');
	} 
	
	function inactive_roles($roles_id){
		//$roles_id = json_decode(base64_decode($roles_id));
		if($roles_id != ''){
			$this->Roles_Model->inactive_roles($roles_id);
		}
		redirect('roles/rolesList','refresh');
	}
	
	function delete_roles($roles_id){
		//$roles_id = json_decode(base64_decode($roles_id));
		if($roles_id != ''){
			$this->Roles_Model->delete_roles($roles_id);
		}
		redirect('roles/rolesList','refresh');
	}
	
	function edit_roles($roles_id){
		//$roles_id = json_decode(base64_decode($roles_id));
		if($roles_id != ''){
			$roles 					= $this->General_Model->getHomePageSettings();
			$roles['roles_list'] 	= $this->Roles_Model->get_roles_list($roles_id);
			$this->load->view('roles/edit_roles',$roles);
		} else {
			redirect('roles/rolesList','refresh');
		}
	}

	function updateRoles($roles_id){
		if(count($_POST) > 0){
			//$roles_id = json_decode(base64_decode($roles_id));
			$roles_logo_name = $this->General_Model->upload_image($_FILES, 'roles',$_REQUEST['old_image']);
			
		    $this->Roles_Model->update_roles($_POST,$roles_id, $roles_logo_name);
		    redirect('roles/rolesList','refresh');
		
		}else if($roles_id!=''){
			redirect('roles/edit_roles/'.$roles_id,'refresh');
		}else{
			redirect('roles/rolesList','refresh');
		}
	}

	 function formValidator($type){
	   $this->form_validation->set_rules('role_name', 'Role Name', 'required');
	   if($type == 'add') {
	    $this->form_validation->set_rules('role_name', 'Role Name', 'required|xss_clean||is_unique[role_details.role_name]|max_length[50]');
	    }
	   return $this->form_validation->run();
    }
    function add_user()
    {
    	if($_POST)
    	{ 

    		$this->Roles_Model->add_roles_user($_POST,$_FILES); 
    		redirect('roles/rolesList','refresh'); 
    	  
    	}
    	$roles_list 	= json_encode($this->Roles_Model->get_roles_list());
    	$data['roles_list']=$roles_list;  
    	$this->load->view('roles/add_user',$data); 
    	 
    }
    function user_list()
    {
		$users_list=json_encode($this->Roles_Model->roles_user_list());
		$data['users_list']=$users_list;
		$this->load->view('roles/user_list', $data); 
    }
    function edit_user($admin_id='')
    {
    	if($_POST)
    	{
		$this->Roles_Model->update_user_data($_POST); 
    	redirect('roles/user_list','refresh');     	 
    	}
    	$data['form_data']=$this->Roles_Model->user_data($admin_id); 
    	$roles_list 	= json_encode($this->Roles_Model->get_roles_list());
    	$data['roles_list']=$roles_list;      
    	$this->load->view('roles/edit_user', $data); 

    }
    function change_user_status($admin_id,$status)
    {
    	$this->Roles_Model->update_status($admin_id,$status);
    	redirect('roles/user_list','refresh'); 
    }
} 
