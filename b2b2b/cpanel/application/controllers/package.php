<?php
// error_reporting(E_ALL);
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Package extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		$this->load->model('General_Model');	
		$this->load->model ( 'Package_Model' );
		$this->lang->load('english','Dynamic_Languages');
		$this->load->model ( 'Supplierpackage_Model' );
		$this->load->library('session');
		$this->load->library('template');
		$this->TravelLights = $this->lang->line('TravelLights');
	} 
	
	public function add_package() {  
		$data ['package_type_data'] = $this->Package_Model->package_view_data_types ()->result ();
		$data ["country"] = $this->Package_Model->get_countries ();
		$this->load->view ( 'package/add_package', $data );
	}

	public function get_crs_city($city_id) {
		$city = $this->Package_Model->get_crs_city_list ( $city_id );
		$sHtml = "";
		$sHtml .= '<option value="">Select City</option>';
		if (! empty ( $city )) {
			foreach ( $city as $key => $value ) {
				$sHtml .= '<option value="' . $value->city . '">' . $value->city . '</option>';
			}
		}
		echo json_encode ( array (
				'result' => $sHtml 
		) );
		exit ();
	}

	public function add_package_new() {

		$photo = $_FILES;
		$cpt1 = count ( $_FILES ['photo'] ['name'] );
		$targetPath = '../extras/custom/TMX1512291534825461/uploads/packages/';
		// debug($_FILES);exit;
		if ($_FILES ['photo'] ['name'] != '') {
			$_FILES ['photo'] ['name'] = $photo ['photo'] ['name'];
			$_FILES ['photo'] ['type'] = $photo ['photo'] ['type'];
			$_FILES ['photo'] ['tmp_name'] = $photo ['photo'] ['tmp_name'];
			$_FILES ['photo'] ['error'] = $photo ['photo'] ['error'];
			$_FILES ['photo'] ['size'] = $photo ['photo'] ['size'];
			// $targetPath = '../../extras/custom/TMX1512291534825461/images/'.$_FILES ['photo'] ['name'];
			// echo $targetPath;die;
			$img_Name	= time().$_FILES ['photo'] ['name'];
			$moved = move_uploaded_file ( $_FILES ['photo'] ['tmp_name'], $targetPath.$img_Name);
			$photo = $img_Name;

		}

		$disn = $this->input->post ( 'disn' );
		$name = $this->input->post ( 'name' );
		$country = $this->input->post ( 'country' );
		$city = $this->input->post ( 'cityname' );
		$duration = $this->input->post ( 'duration' );
		$description = $this->input->post ( 'Description' );
		$location = $this->input->post ( 'location' );
		$state = $this->input->post ( 'state' );
		$deal = $this->input->post ( 'deal' );
		$homepage = $this->input->post ( 'homepage' );
		$rating = $this->input->post ( 'rating' );
		$tourss = $this->input->post ( 'tourss' );
		$with_price = $this->input->post ( 'with_price' );
		$p_price = $this->input->post ( 'p_price' );
		// itinerary
		$itinerarydesc = $this->input->post ( 'desc' );
		$days = $this->input->post ( 'days' );
		$place = $this->input->post ( 'place' );
		$price = $this->input->post ( 'price' );
		// price includes
		$includes = $this->input->post ( 'includes' );
		$excludes = $this->input->post ( 'excludes' );
		// cancellation
		$advance = $this->input->post ( 'advance' );
		$penality = $this->input->post ( 'penality' );
		// question and answer
		$no_questions = $this->input->post ( 'no_of_questions' );
		$quest = $this->input->post ( 'quest' );
		$answer = $this->input->post ( 'answer' );
		$sup_id = $this->session->userdata ( 'sup_id' );
		$sup_type = $this->session->userdata ( 'sup_type' );
		// price
		$sd = $this->input->post ( 'sd' );
		$ed = $this->input->post ( 'ed' );
		$adult = $this->input->post ( 'adult' );
		$child = $this->input->post ( 'child' );
		$pricee = $this->input->post ( 'pricee' );
		// deals
		$value = $this->input->post ( 'value' );
		$discount = $this->input->post ( 'discount' );
		$save = $this->input->post ( 'save' );
		$seats = $this->input->post ( 'seats' );
		$time = $this->input->post ( 'time' );
		$packageaw = $this->input->post ( 'a_wo_p' );
		$domain_list_fk = get_domain_auth_id ();
		$newpackage = array (
				'package_type' => $disn,
				'package_name' => $name,
				'supplier_id' => $sup_id,
				'price_includes' => $pricee,
				'package_country' => $country,
				'package_city' => $city,
				'package_location' => $location,
				'package_description' => $description,
				'deals' => $deal,
				'duration' => $duration,
				'image' => $photo,
				'no_que' => $no_questions,
				'home_page' => $homepage,
				'rating' => $rating,
				'tour_types' => $tourss,
				'price' => $p_price,
				'domain_list_fk' => $domain_list_fk 
		);
		//echo "<pre>"; print_r($newpackage);exit;
		
		// print_r($country);exit;
		$package = $this->Supplierpackage_Model->add_new_package ( $newpackage );
		if ($package != 0) {
			$packcode = array (
					'package_code' => 'SKY' . $package 
			);
			$this->Supplierpackage_Model->update_code_package ( $packcode, $package );
		}
		$img = $_FILES;
		$cp = count ( $_FILES ['image'] ['name'] );
		if ($_FILES ['image'] ['name'] [0] != '') {
			for($i = 0; $i < $duration; $i ++) {
				$_FILES ['image'] ['name'] = $img ['image'] ['name'] [$i];
				$_FILES ['image'] ['type'] = $img ['image'] ['type'] [$i];
				$_FILES ['image'] ['tmp_name'] = $img ['image'] ['tmp_name'] [$i];
				$_FILES ['image'] ['error'] = $img ['image'] ['error'] [$i];
				$_FILES ['image'] ['size'] = $img ['image'] ['size'] [$i];
				$itenary_imge = 'itenary-' . time () . $_FILES ['image'] ['name'];

				move_uploaded_file ( $_FILES ['image'] ['tmp_name'], $targetPath.$itenary_imge);
				$image = $itenary_imge;
				$itinerary = array (
						'itinerary_description' => $itinerarydesc[$i],
						'day' => $days[$i],
						'place' => $place[$i],
						'itinerary_image' => $image,
						'package_id' => $package 
				);
				$itineraryid = $this->Supplierpackage_Model->itinerary ( $itinerary );
			}
		}
		for($i = 0; $i < $no_questions; $i ++) {
			if (! empty ( $quest [$i] )) {
				$qus_ans = array (
						'question' => $quest [$i],
						'answer' => $answer [$i],
						'package_id' => $package,
						'user_id' => $sup_id,
						'usertype' => $sup_type 
				);
				$queans = $this->Supplierpackage_Model->que_ans ( $qus_ans );
			}
		}
		$pricingpolicy = array (
				'package_id' => $package,
				'price_includes' => $includes,
				'price_excludes' => $excludes 
		);
		$policy = $this->Supplierpackage_Model->pricing_policy ( $pricingpolicy );
		$cancellation = array (
				'package_id' => $package,
				'cancellation_advance' => $advance,
				'cancellation_penality' => $penality 
		);
		$cancel = $this->Supplierpackage_Model->cancellation_penality ( $cancellation );
		if (! empty ( $value )) {
			$deals = array (
					'value' => $value,
					'discount' => $discount,
					'you_save' => $save,
					'seats' => $seats,
					'time' => $time,
					'package_id' => $package 
			);
			$deall = $this->Supplierpackage_Model->deals ( $deals );
		}
		for($i = 0; $i < $duration; $i ++) {
			if (! empty ( $sd [$i] )) {
				$incl = array (
						'from_date' => $sd [$i],
						'to_date' => $ed [$i],
						'duration' => $duration,
						'adult_price' => $adult [$i],
						'child_price' => $child [$i],
						'package_id' => $package 
				);
				$in = $this->Supplierpackage_Model->incl ( $incl );
			}
		}
		$tra = $_FILES;
		$c = count ( $_FILES ['traveller'] ['name'] );
		if ($_FILES ['traveller'] ['name'] [0] != '') {
			for($i = 0; $i < $c; $i ++) {
				$_FILES ['traveller'] ['name'] = $tra ['traveller'] ['name'] [$i];
				$_FILES ['traveller'] ['type'] = $tra ['traveller'] ['type'] [$i];
				$_FILES ['traveller'] ['tmp_name'] = $tra ['traveller'] ['tmp_name'] [$i];
				$_FILES ['traveller'] ['error'] = $tra ['traveller'] ['error'] [$i];
				$_FILES ['traveller'] ['size'] = $tra ['traveller'] ['size'] [$i];
				$name_of_traveller_img = 'traveller-' . time () . $_FILES ['traveller'] ['name'];
				move_uploaded_file ( $_FILES ['traveller'] ['tmp_name'],  $targetPath.$name_of_traveller_img );
				$traveller_img = $name_of_traveller_img;
				// $sup_id = $this->entity_user_id;
				// debug($this->session->userdata);exit("fsfsd");
				$traveller = array (
						'traveller_image' => $traveller_img,
						'user_id' => $sup_id,
						'package_id' => $package 
				);
				$travel = $this->Supplierpackage_Model->travel_images ( $traveller );
			}
		}
		redirect ( 'package/view_with_price' );
	}

	public function view_with_price() {
		$data ['newpackage'] = $this->Supplierpackage_Model->with_price ();
		$this->load->view ( 'package/view_with_price', $data );
	}

	public function update_status($package_id, $status) {
		$this->Supplierpackage_Model->update_status ( $package_id, $status );
		redirect ( 'package/view_with_price/', 'refresh' );
	}

	public function delete_package($id) {
		$this->Supplierpackage_Model->delete_package ( $id );
		redirect ( 'package/view_with_price' );
	}

	public function view_enquiries($package_id) {
		$data ['enquiries'] = $this->Supplierpackage_Model->view_enqur ( $package_id );
		$data ['package_id'] = $package_id;
		$this->load->view ( 'package/enquiry_package', $data );
	}

	public function enquiries() {
		$data ['enquiries'] = $this->Supplierpackage_Model->enquiries ();
		//debug($data);exit;
		$this->load->view ( 'package/enquiries', $data );
	}

	public function images($package_id) {
		$targetPath = '../extras/custom/TMX1512291534825461/uploads/packages/';
		if ($_FILES) {
			$tra = $_FILES;
			$_FILES ['traveller'] ['name'] = $tra ['traveller'] ['name'];
			$_FILES ['traveller'] ['type'] = $tra ['traveller'] ['type'];
			$_FILES ['traveller'] ['tmp_name'] = $tra ['traveller'] ['tmp_name'];
			$_FILES ['traveller'] ['error'] = $tra ['traveller'] ['error'];
			$_FILES ['traveller'] ['size'] = $tra ['traveller'] ['size'];
			$name_of_traveller_img = 'traveller-' . time () . $_FILES ['traveller'] ['name'];
			move_uploaded_file ( $_FILES ['traveller'] ['tmp_name'], $targetPath.$name_of_traveller_img );
			$traveller_img = $name_of_traveller_img;
			//$sup_id = $this->entity_user_id;
			$sup_id = $this->session->userdata('provabAdminId');
			$pckge_id = $this->input->post ( 'pckge_id' );
			$traveller = array (
					'traveller_image' => $traveller_img,
					'user_id' => $sup_id,
					'package_id' => $pckge_id 
			);
			$travel = $this->Supplierpackage_Model->travel_images ( $traveller );
			redirect ( base_url () . 'package/images/' . $package_id . '/w' );
		}
		$data ['traveller'] = $this->Supplierpackage_Model->get_image ( $package_id );
		$data ['package_id'] = $package_id;
		$this->load->view ( 'package/images', $data );
	}

	public function delete_traveller_img($pack_id,$img_id) {
		$this->Supplierpackage_Model->delete_traveller_img ( $pack_id,$img_id );
		redirect ( 'package/images/'.$img_id.'/w' );
	}

	public function delete_enquiry($id, $package_id) {
		$this->Supplierpackage_Model->delete_enquiry ( $id );
		redirect ( 'package/enquiries/' );
	}

	public function edit_with_price($package_id) {
		$data ['packdata'] = $this->Supplierpackage_Model->get_package_id ( $package_id );
		$data ['price'] = $this->Supplierpackage_Model->get_price ( $package_id );
		$data ['countries'] = $this->Supplierpackage_Model->get_country_city_list ();
		// print_r($data);exit;
		$this->load->view ( 'package/edit_with_price', $data );
	}

	public function update_package($package_id) {
		$name = $this->input->post ( 'name' );
		$location = $this->input->post ( 'location' );
		$description = $this->input->post ( 'Description' );
		$photo = $_FILES;
		if ($_FILES ['photo'] ['name'] != '') {
			$_FILES ['photo'] ['name'] = $photo ['photo'] ['name'];
			$_FILES ['photo'] ['type'] = $photo ['photo'] ['type'];
			$_FILES ['photo'] ['tmp_name'] = $photo ['photo'] ['tmp_name'];
			$_FILES ['photo'] ['error'] = $photo ['photo'] ['error'];
			$_FILES ['photo'] ['size'] = $photo ['photo'] ['size'];
			
			move_uploaded_file ( $_FILES ['photo'] ['tmp_name'], $this->template->domain_uploads_packages ( $_FILES ['photo'] ['name'] ) );
			$photo = $_FILES ['photo'] ['name'];
		} else {
			$photo = $this->input->post ( 'hidephoto' );
		}
		$includes = $this->input->post ( 'includes' );
		$excludes = $this->input->post ( 'excludes' );
		$price = $this->input->post ( 'price' );
		$advance = $this->input->post ( 'advance' );
		$penality = $this->input->post ( 'penality' );
		$value = $this->input->post ( 'value' );
		$discount = $this->input->post ( 'discount' );
		$save = $this->input->post ( 'save' );
		$seats = $this->input->post ( 'seats' );
		$time = $this->input->post ( 'time' );
		$sd = $this->input->post ( 'sd' );
		$ed = $this->input->post ( 'ed' );
		$adult = $this->input->post ( 'adult' );
		$child = $this->input->post ( 'child' );
		$packtypew = $this->input->post ( 'w_wo_d' );
		$rating = $this->input->post ( 'rating' );
		$p_price = $this->input->post ( 'p_price' );
		$data = array (
				'package_name' => $name,
				'package_location' => $location,
				'package_description' => $description,
				'image' => $photo,
				'rating' => $rating,
				'price' => $p_price 
		);
		// print_r($data);exit;
		$policy = array (
				'price_includes' => $includes,
				'price_excludes' => $excludes 
		);
		$can = array (
				'cancellation_advance' => $advance,
				'cancellation_penality' => $penality 
		);
		$dea = array (
				'value' => $value,
				'discount' => $discount,
				'you_save' => $save,
				'seats' => $seats,
				'time' => $time 
		);
		if (! empty ( $sd )) {
			for($i = 0; $i < count ( $sd ); $i ++) {
				$pri = array (
						'from_date' => $sd [$i],
						'to_date' => $ed [$i],
						'adult_price' => $adult [$i],
						'child_price' => $child [$i] 
				);
				$this->Supplierpackage_Model->update_edit_pri ( $package_id, $pri );
			}
		}
		// print_r($data);die;
		$this->Supplierpackage_Model->update_edit_package ( $package_id, $data );
		$this->Supplierpackage_Model->update_edit_policy ( $package_id, $policy );
		$this->Supplierpackage_Model->update_edit_can ( $package_id, $can );
		$this->Supplierpackage_Model->update_edit_dea ( $package_id, $dea );
		
		if ($packtypew == "w") {
			redirect ( 'package/view_with_price' );
		} elseif ($packtypew == 'wo') {
			redirect ( 'package/view_without_price' );
		} else {
			redirect ( 'package/view_deals' );
		}
		
		redirect ( 'package/view_with_price' );
	}

	public function edit_itinerary($package_id) {
		$data ['pack_data'] = $this->Supplierpackage_Model->get_itinerary_id ( $package_id );
		// print_r($data);die;
		$data ['package_id'] = $package_id;
		$this->load->view ( 'package/edit_itinerary', $data );
	}

	public function update_itinerary() {
		$package_id = $this->input->post ( 'package_id' );
		$itinerary_id = $this->input->post ( 'itinerary_id' );
		$itinerarydesc = $this->input->post ( 'desc' );
		$days = $this->input->post ( 'days' );
		$place = $this->input->post ( 'place' );
		$hiddenimage = $this->input->post ( 'hiddenimage' );
		// print_r($hiddenimage);
		// exit;
		$img = $_FILES;
		$cp = count ( $itinerary_id );
		// print_r($cp);exit;
		for($i = 0; $i < $cp; $i ++) {
			
			if (! empty ( $_FILES ['imagelable' . $i] ['name'] )) {
				$_FILES ['imagelable'] ['name'] = $img ['imagelable' . $i] ['name'];
				$_FILES ['imagelable'] ['type'] = $img ['imagelable' . $i] ['type'];
				$_FILES ['imagelable'] ['tmp_name'] = $img ['imagelable' . $i] ['tmp_name'];
				$_FILES ['imagelable'] ['error'] = $img ['imagelable' . $i] ['error'];
				$_FILES ['imagelable'] ['size'] = $img ['imagelable' . $i] ['size'];
				move_uploaded_file ( $_FILES ['imagelable'] ['tmp_name'], $this->template->domain_uploads_packages ( $_FILES ['imagelable' . $i] ['name'] ) );
				$image = $this->template->domain_uploads_packages ( $_FILES ['imagelable' . $i] ['name'] );
			} else {
				$image = $hiddenimage [$i];
			}
			
			$data = array (
					'itinerary_description' => $itinerarydesc [$i],
					'place' => $place [$i],
					'day' => $days [$i],
					'itinerary_image' => $image 
			);
			
			$this->Supplierpackage_Model->update_itinerary ( $package_id, $itinerary_id [$i], $data );
		}
		redirect ( 'package/edit_itinerary/' . $package_id, 'refresh' );
	}
}