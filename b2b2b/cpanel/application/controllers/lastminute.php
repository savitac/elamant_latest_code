<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
//error_reporting(E_ALL);
ob_start();
class Lastminute extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('General_Model');
        $this->load->model('Lastminute_Model');
        $this->load->model('Banner_Model');
        
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
    function last_minute_list() {
        $last_minute = $this->General_Model->getHomePageSettings();
        $last_minute['last_min_list'] = $this->Lastminute_Model->get_last_min_list();
        $this->load->view('last_minute/last_minute_list', $last_minute);
    }

    function add_last_minute_deal() {
        if (count($_POST) > 0) {
            $hotel_image_name = '';
            if (!empty($_FILES['hotel_image']['name'])) {
                if (is_uploaded_file($_FILES['hotel_image']['tmp_name'])) {
                    $sourcePath = $_FILES['hotel_image']['tmp_name'];
                    $img_Name = time() . $_FILES['hotel_image']['name'];
                    $targetPath = "uploads/last_minut_deals/" . $img_Name;
                    if (move_uploaded_file($sourcePath, $targetPath)) {
                        $hotel_image_name = $img_Name;
                    }
                }
            }
            $this->Lastminute_Model->add_last_minute_deal_details($_POST, $hotel_image_name);
            redirect('lastminute/last_minute_list', 'refresh');
        } else {

            $last_minute = $this->General_Model->getHomePageSettings();
            $last_minute['agents'] 		= $this->Banner_Model->getAgentList();
            $this->load->view('last_minute/add_last_minute_deal', $last_minute);
        }
    }
    
    
	 function active_deal($id1) {
        $id 	= json_decode(base64_decode($id1));
		if($id != ''){
			$this->Lastminute_Model->active_deal($id);
        }
		redirect('lastminute/last_minute_list', 'refresh');
    }
    
	function inactive_deal($id1) {
        $id 	= json_decode(base64_decode($id1));
		if($id != ''){
			$this->Lastminute_Model->inactive_deal($id);
        }
		redirect('lastminute/last_minute_list', 'refresh');
    }

    function delete_deal($id1) {
        $id 	= json_decode(base64_decode($id1));
		if($id != ''){
			$this->Lastminute_Model->delete_deal($id);
        }
		redirect('lastminute/last_minute_list', 'refresh');
    }

    function edit_deal($id1) {
       $id 	= json_decode(base64_decode($id1));
		if($id != ''){
			 $last_minute = $this->General_Model->getHomePageSettings();
			$last_minute['last_min_list'] = $this->Lastminute_Model->get_last_min_list($id);
			$this->load->view('last_minute/edit_last_minute_deal', $last_minute);
		} else {
            redirect('lastminute/last_minute_list', 'refresh');
        }
    }
    
   

    function update_last_minute_deal($id1) {
        $id 	= json_decode(base64_decode($id1));
		if($id != ''){
			if (count($_POST) > 0) {
				$hotel_image_name = $_REQUEST['hotel_image_old'];
				if (!empty($_FILES['hotel_image']['name'])) {
					if (is_uploaded_file($_FILES['hotel_image']['tmp_name'])) {
						$oldImage = "uploads/last_minut_deals/" . $hotel_image_name;
						unlink($oldImage);
						$sourcePath = $_FILES['hotel_image']['tmp_name'];
						$img_Name = time() . $_FILES['hotel_image']['name'];
						$targetPath = "uploads/last_minut_deals/" . $img_Name;
						if (move_uploaded_file($sourcePath, $targetPath)) {
							$hotel_image_name = $img_Name;
						}
					}
				}
				$this->Lastminute_Model->update_deal($_POST, $id, $hotel_image_name);
				redirect('lastminute/last_minute_list', 'refresh');
			} else if ($id != '') {
				redirect('lastminute/last_minute_list', 'refresh');
			} else {
				redirect('lastminute/last_minute_list', 'refresh');
			}
		} else {
            redirect('lastminute/last_minute_list', 'refresh');
        }
    }
	
	function manage_deal($last_minute_deals_id1) {
       $last_minute_deals_id 	= json_decode(base64_decode($last_minute_deals_id1));
		if(true){
			// echo $id;exit;
			$dealInfo = $this->General_Model->getHomePageSettings();
			$dealInfo['last_minute_deals_id'] 	= $last_minute_deals_id1;
			$dealInfo['dealInfo_list'] 			= $this->Lastminute_Model->get_deal_info_list($last_minute_deals_id);
			$this->load->view('last_minute/deal_info/deal_info_list', $dealInfo);
		}else{
			redirect('lastminute/last_minute_list', 'refresh');
		}
	}
	
	function add_deal_info($last_minute_deals_id1) {
       $last_minute_deals_id 	= json_decode(base64_decode($last_minute_deals_id1));
       if($last_minute_deals_id!=''){
			if (count($_POST) > 0) {
				$hotel_image_name = '';
				if (!empty($_FILES['hotel_image']['name'])) {
					if (is_uploaded_file($_FILES['hotel_image']['tmp_name'])) {
						$sourcePath = $_FILES['hotel_image']['tmp_name'];
						$img_Name = time() . $_FILES['hotel_image']['name'];
						$targetPath = "uploads/deal_info/" . $img_Name;
						if (move_uploaded_file($sourcePath, $targetPath)) {
							$hotel_image_name = $img_Name;
						}
					}
				}
				$this->Lastminute_Model->add_deal_info($_POST, $hotel_image_name);
				redirect('lastminute/manage_deal/'.$last_minute_deals_id1, 'refresh');
			}else {
				$last_minute = $this->General_Model->getHomePageSettings();
				$last_minute['last_min_list'] = $this->Lastminute_Model->get_last_min_list();
				$last_minute['last_minute_deals_id'] = $last_minute_deals_id;
				$this->load->view('last_minute/deal_info/add_deal_info', $last_minute);
			}
		}else{
			redirect('lastminute/last_minute_list', 'refresh');
		}
    }   
    
    function active_deal_info($id1,$last_minute_id1) {
        $id 	= json_decode(base64_decode($id1));
        $last_minute_id 	= json_decode(base64_decode($last_minute_id1));
		if($id != ''){
			$this->Lastminute_Model->active_deal_info($id);
        }
		redirect('lastminute/manage_deal/'.$last_minute_id1, 'refresh');
    }

    function inactive_deal_info($id1,$last_minute_id1) {
        $id 	= json_decode(base64_decode($id1));
        $last_minute_id 	= json_decode(base64_decode($last_minute_id1));
		if($id != ''){
			$this->Lastminute_Model->inactive_deal_info($id);
        }
		redirect('lastminute/manage_deal/'.$last_minute_id1, 'refresh');
    }

    function delete_deal_info($id1,$last_minute_id1) {
        $id 	= json_decode(base64_decode($id1));
        $last_minute_id 	= json_decode(base64_decode($last_minute_id1));
		if($id != ''){
			$this->Lastminute_Model->delete_deal_info($id);
        }
		redirect('lastminute/manage_deal/'.$last_minute_id1, 'refresh');
    }

	
    function edit_deal_info($id1,$last_minute_deals_id1) {
       $id 	= json_decode(base64_decode($id1));
       $last_minute_deals_id 	= json_decode(base64_decode($last_minute_deals_id1));
		if($id != ''){
			$last_minute 							= $this->General_Model->getHomePageSettings();
			$last_minute['dealInfo_list'] 			= $this->Lastminute_Model->get_deal_info_list($last_minute_deals_id,$id);
			$last_minute['last_min_list'] 			= $this->Lastminute_Model->get_last_min_list();
			$last_minute['last_minute_deals_id'] 	= $last_minute_deals_id;
			$this->load->view('last_minute/deal_info/edit_deal_info', $last_minute);
		} else {
            redirect('lastminute/last_minute_list'.$last_minute_id1, 'refresh');
        }
    }
    function update_deal_info($id1,$last_minute_deals_id1) {
        $id 	= json_decode(base64_decode($id1));
        $last_minute_deals_id 	= json_decode(base64_decode($last_minute_deals_id1));
		if($id != ''){
			if (count($_POST) > 0) {
				$hotel_image_name = $_REQUEST['hotel_image_old'];
				if (!empty($_FILES['hotel_image']['name'])) {
					if (is_uploaded_file($_FILES['hotel_image']['tmp_name'])) {
						$oldImage = "uploads/deal_info/" . $hotel_image_name;
						unlink($oldImage);
						$sourcePath = $_FILES['hotel_image']['tmp_name'];
						$img_Name = time() . $_FILES['hotel_image']['name'];
						$targetPath = "uploads/deal_info/" . $img_Name;
						if (move_uploaded_file($sourcePath, $targetPath)) {
							$hotel_image_name = $img_Name;
						}
					}
				}
				$this->Lastminute_Model->update_deal_info($_POST, $id, $hotel_image_name);
				redirect('lastminute/manage_deal/'.$last_minute_deals_id1, 'refresh');
			} else if ($id != '') {
				redirect('lastminute/last_minute_list', 'refresh');
			} else {
				redirect('lastminute/last_minute_list', 'refresh');
			}
		} else {
            redirect('lastminute/last_minute_list', 'refresh');
        }
    }
    
    function spa_info($id1,$last_minute_id1 = '') {
       // echo $id1;exit;
        $id 	= json_decode(base64_decode($id1));
        if($id != ''){
			$last_minute 							= $this->General_Model->getHomePageSettings();
			$last_minute['spa_list'] 				= $this->Lastminute_Model->get_spa_list($id);
			$last_minute['deals_information_id'] 	= $id1;
			$this->load->view('last_minute/spa/spa_list', $last_minute);
		}else{
			redirect('lastminute/manage_deal/'.$last_minute_id1, 'refresh');
		}
	}
	function add_spa($deals_information_id1){
		$deals_information_id 	= json_decode(base64_decode($deals_information_id1));
		if(count($_POST) > 0){
		     $spa_logo_name = '';
			if(!empty($_FILES['spa_logo']['name']))
			{	
				if(is_uploaded_file($_FILES['spa_logo']['tmp_name'])) 
				{
					$sourcePath = $_FILES['spa_logo']['tmp_name'];
					$img_Name=time().$_FILES['spa_logo']['name'];
					$targetPath = "uploads/hotel/spa/".$img_Name;
					if(move_uploaded_file($sourcePath,$targetPath)){
						$spa_logo_name = $img_Name;
					}
				}				
			}
			$this->Lastminute_Model->add_spa_details($_POST,$spa_logo_name);
			redirect('lastminute/spa_info/'.$deals_information_id1, 'refresh');
		}else{
			$spa 						= $this->General_Model->getHomePageSettings();
			$spa['dealInfo_list'] 		= $this->Lastminute_Model->get_deal_info_list('',$deals_information_id);
			$spa['deals_information_id'] 		= $deals_information_id1;
			$spa['deals_information_id1'] 		= $deals_information_id;
			$this->load->view('last_minute/spa/add_spa',$spa);
		}
	}
	function active_spa($id1,$deals_information_id1) {
		$id 	= json_decode(base64_decode($id1));
        if($id != ''){
			$this->Lastminute_Model->active_spa($id);
        }
		redirect('lastminute/spa_info/'.$deals_information_id1, 'refresh');
    }

    function inactive_spa($id1,$deals_information_id1) {
        $id 	= json_decode(base64_decode($id1));
        if($id != ''){
			$this->Lastminute_Model->inactive_spa($id);
        }
		redirect('lastminute/spa_info/'.$deals_information_id1, 'refresh');
    }

    function delete_spa($id1,$deals_information_id1) {
        $id 	= json_decode(base64_decode($id1));
        if($id != ''){
			$this->Lastminute_Model->delete_spa($id);
        }
		redirect('lastminute/spa_info/'.$deals_information_id1, 'refresh');
    }
    
    function edit_spa($id1,$deals_information_id1) {
		$id 	= json_decode(base64_decode($id1));
		$deals_information_id 	= json_decode(base64_decode($deals_information_id1));
        if($id != ''){
			$spa 								= $this->General_Model->getHomePageSettings();
			$spa['dealInfo_list'] 				= $this->Lastminute_Model->get_deal_info_list('',$deals_information_id);
			$spa['spa_list'] 					= $this->Lastminute_Model->get_spa_list($deals_information_id,$id);
			// echo '<pre/>';print_r($spa['spa_list']);exit;
			$spa['deals_information_id'] 		= $deals_information_id1;
			$spa['deals_information_id1'] 		= $deals_information_id;
			$this->load->view('last_minute/spa/edit_spa',$spa);
		}else{
			redirect('spa/spa_list','refresh');
		}
	}
	
	function update_spa($spa_id1,$deals_information_id1){
		$spa_id 	= json_decode(base64_decode($spa_id1));
		if($spa_id != ''){
			if(count($_POST) > 0){
				$spa_logo_name  = $_REQUEST['spa_logo_old'];
				if(!empty($_FILES['spa_logo']['name']))
				{	
					if(is_uploaded_file($_FILES['spa_logo']['tmp_name'])) 
					{
						$oldImage = "uploads/hotel/spa/".$spa_logo_name;
						unlink($oldImage);
						$sourcePath = $_FILES['spa_logo']['tmp_name'];
						$img_Name=time().$_FILES['spa_logo']['name'];
						$targetPath = "uploads/hotel/spa/".$img_Name;
						if(move_uploaded_file($sourcePath,$targetPath)){
							$spa_logo_name = $img_Name;
						}
					}				
				}
				// echo '<pre/>';print_r($_POST);exit;
				$this->Lastminute_Model->update_spa($_POST,$spa_id, $spa_logo_name);
				redirect('lastminute/spa_info/'.$deals_information_id1, 'refresh');
			}else if($spa_id!=''){
				redirect('lastminute/spa_info/'.$deals_information_id1, 'refresh');
			}else{
				redirect('lastminute/spa_info/'.$deals_information_id1, 'refresh');
			}
		}else{
			redirect('lastminute/spa_info/'.$deals_information_id1, 'refresh');
		}
	}
	
    function dinning_info($id1,$last_minute_id1) {
        $id 	= json_decode(base64_decode($id1));
        $last_minute_id 	= json_decode(base64_decode($last_minute_id1));
		if($id != ''){
			$last_minute 							= $this->General_Model->getHomePageSettings();
			$last_minute['spa_list'] 				= $this->Lastminute_Model->get_dinning_list($id);
			$last_minute['deals_information_id'] 	= $id;
			$this->load->view('last_minute/dining/dining_list', $last_minute);
		}else{
			redirect('lastminute/manage_deal/'.$last_minute_id1, 'refresh');
		}
	}
	
	function reviews_info($id1,$last_minute_id1) {
        $id 	= json_decode(base64_decode($id1));
        $last_minute_id 	= json_decode(base64_decode($last_minute_id1));
		if($id != ''){
			$last_minute 							= $this->General_Model->getHomePageSettings();
			$last_minute['spa_list'] 				= $this->Lastminute_Model->get_reviews_list($id);
			$last_minute['deals_information_id'] 	= $id;
			$this->load->view('last_minute/review/review_list', $last_minute);
		}else{
			redirect('lastminute/manage_deal/'.$last_minute_id1, 'refresh');
		}
	}
	
	function activities_info($id1,$last_minute_id1) {
        $id 	= json_decode(base64_decode($id1));
        $last_minute_id 	= json_decode(base64_decode($last_minute_id1));
		if($id != ''){
			$last_minute 							= $this->General_Model->getHomePageSettings();
			$last_minute['spa_list'] 				= $this->Lastminute_Model->get_activities_list($id);
			$last_minute['deals_information_id'] 	= $id;
			$this->load->view('last_minute/activity/activity_list', $last_minute);
		}else{
			redirect('lastminute/manage_deal/'.$last_minute_id1, 'refresh');
		}
	}
	
	function ammenities_info($id1,$last_minute_id1) {
        $id 	= json_decode(base64_decode($id1));
        $last_minute_id 	= json_decode(base64_decode($last_minute_id1));
		if($id != ''){
			$last_minute 							= $this->General_Model->getHomePageSettings();
			$last_minute['spa_list'] 				= $this->Lastminute_Model->get_ammenities_list($id);
			$last_minute['deals_information_id'] 	= $id;
			$this->load->view('last_minute/ammenities/ammenities_list', $last_minute);
		}else{
			redirect('lastminute/manage_deal/'.$last_minute_id1, 'refresh');
		}
	}
    
}
