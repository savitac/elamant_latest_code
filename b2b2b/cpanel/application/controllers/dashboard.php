<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
// error_reporting(E_ALL);
ob_start();
class Dashboard extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->library('form_validation');
		$this->load->helper('date_helper');
		$this->load->model('hotel_model');
		$this->load->model('hotels_model');
		$this->load->model('flight_model');
		$this->load->model('bus_model');
		$this->load->model('sightseeing_model');
		$this->load->model('transferv1_model');	
		$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	 $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
       public function index(){
		$page_data = $this->General_Model->getHomePageSettings();
		$page_data['default_section'] = $this->General_Model->getDefaultSections();

		

		$page_data['admin_id'] = $this->session->userdata('provabAdminId');
			if (is_active_bus_module()) {
				
				//$active_bus = true;
				$active_bus = false;
				// echo "3";
			}
			if (is_active_hotel_module()) {
				$active_hotel = true;
				// echo "4";
			}
			if (is_active_airline_module()) {
				$active_airline = true;
				// echo "5";
			}
			if(is_active_sightseeing_module()){
				//$active_sightseeing = true;
				$active_sightseeing = false;
				// echo "6";
			}
			if(is_active_transferv1_module()){
				$active_transfers = true;
				// echo "7";
			}

			// exit("59");
			$this->load->library('booking_data_formatter');
			$days_duration = -1; // ADD day count to filter result
			$condition = array();
			$condition[] = array('BD.status', ' IN ', '("BOOKING_CONFIRMED","BOOKING_PENDING")');
			if ($days_duration > 0) {
				$condition = array(
				array('BD.created_datetime', '>=', $this->db->escape(date('Y-m-d', strtotime(subtract_days_from_date($days_duration)))))
				
				);
			}
			//load for current year only
			$time_line_interval = get_month_names();
			
			$hotel_earning = $flight_earning = $bus_earning = $sightseeing_earning = $transfers_earning= array();
			if (!empty($active_hotel)) {
				$module_total_earning[0]['name'] = 'Hotel';
				$module_total_earning[0]['y'] = 0;
				$time_line_report[0]['name'] = 'Hotel';
				$time_line_report[0]['data'] = array();
				$time_line_report[0]['color'] = '#00a65a';
				$tmp_hotel_booking = $this->hotel_model->get_monthly_booking_summary();
				$month_index_hotel = index_month_number($tmp_hotel_booking);
			}
			if (!empty($active_airline)) {
				$module_total_earning[1]['name'] = 'Flight';
				$module_total_earning[1]['y'] = 0;
				$time_line_report[1]['name'] = 'Flight';
				$time_line_report[1]['data'] = array();
				$time_line_report[1]['color'] = '#0073b7';
				$tmp_flight_booking = $this->flight_model->get_monthly_booking_summary();
				$month_index_flight = index_month_number($tmp_flight_booking);
			}
			if (!empty($active_bus)) {
				$module_total_earning[2]['bus'] = 'Bus';
				$module_total_earning[2]['y'] = 0;
				$time_line_report[2]['name'] = 'Bus';
				$time_line_report[2]['data'] = array();
				$time_line_report[2]['color'] = '#dd4b39';
				$tmp_bus_booking = $this->bus_model->get_monthly_booking_summary();
				$month_index_bus = index_month_number($tmp_bus_booking);
			}
			if (!empty($active_sightseeing)) {
				$module_total_earning[3]['sightseeing'] = 'Sightseeing';
				$module_total_earning[3]['y'] = 0;
				$time_line_report[3]['name'] = 'Sightseeing';
				$time_line_report[3]['data'] = array();
				$time_line_report[3]['color'] = '#ff9800';
				$tmp_sightseeing_booking = $this->sightseeing_model->get_monthly_booking_summary();

				$month_index_sightseeing = index_month_number($tmp_sightseeing_booking);
			}
			
			if (!empty($active_transfers)) {
				$module_total_earning[5]['transfers'] = 'Transfers';
				$module_total_earning[5]['y'] = 0;
				//$time_line_report[5]['name'] = 'Transfers';
				$time_line_report[5]['name'] = 'Car Booking';
				$time_line_report[5]['data'] = array();
				$time_line_report[5]['color'] = '#456F13';
				$tmp_transfers_booking = $this->transferv1_model->get_monthly_booking_summary();
				//debug($tmp_transfers_booking);

				$month_index_transfers = index_month_number($tmp_transfers_booking);
			}

			$time_line_report_average = array();
			$monthly_hotel_booking = array();
			$monthly_flight_booking = array();
			$monthly_bus_booking = array();
			$monthly_sightseeing_booking = array();
			
			$monthly_transfers_booking = array();

			foreach ($time_line_interval as $k => $v) {
				if (!empty($active_hotel)) {
					if (isset($month_index_hotel[$k])) {
						//HOTEL
						$monthly_hotel_booking[$k] = intval($month_index_hotel[$k]['total_booking']);
						$hotel_earning[$k] = round($month_index_hotel[$k]['monthly_earning']);
					} else {
						$monthly_hotel_booking[$k] = 0;
						$hotel_earning[$k] = 0;
					}
					@($time_line_report_average[$k] += round($hotel_earning[$k]))/(intval($monthly_hotel_booking[$k]) > 0 ? $monthly_hotel_booking[$k] : 1);
					($module_total_earning[0]['y'] += round($hotel_earning[$k]));
				}
				if (!empty($active_airline)) {
					if (isset($month_index_flight[$k])) {
						//FLIGHT
						$monthly_flight_booking[$k] = intval($month_index_flight[$k]['total_booking']);
						$flight_earning[$k] = round($month_index_flight[$k]['monthly_earning']);
					} else {
						$monthly_flight_booking[$k] = 0;
						$flight_earning[$k] = 0;
					}
					@($time_line_report_average[$k] += round($flight_earning[$k]))/(intval($monthly_flight_booking[$k]) > 0 ? $monthly_flight_booking[$k] : 1);
					($module_total_earning[1]['y'] += round($flight_earning[$k]));
				}
				if (!empty($active_bus)) {
					if (isset($month_index_bus[$k])) {
						//BUS
						$monthly_bus_booking[$k] = intval($month_index_bus[$k]['total_booking']);
						$bus_earning[$k] = round($month_index_bus[$k]['monthly_earning']);
					} else {
						$monthly_bus_booking[$k] = 0;
						$bus_earning[$k] = 0;
					}
					@($time_line_report_average[$k] += round($bus_earning[$k]))/(intval($monthly_bus_booking[$k]) > 0 ? $monthly_bus_booking[$k] : 1);
					($module_total_earning[2]['y'] += round($bus_earning[$k]));
				}
				if (!empty($active_sightseeing)) {

					if (isset($month_index_sightseeing[$k])) {
						//Sightseeing
						$monthly_sightseeing_booking[$k] = intval($month_index_sightseeing[$k]['total_booking']);
						$sightseeing_earning[$k] = round($month_index_sightseeing[$k]['monthly_earning']);
					} else {
						$monthly_sightseeing_booking[$k] = 0;
						$sightseeing_earning[$k] = 0;
					}
					@($time_line_report_average[$k] += round($sightseeing_earning[$k]))/(intval($monthly_sightseeing_booking[$k]) > 0 ? $monthly_sightseeing_booking[$k] : 1);
					($module_total_earning[3]['y'] += round($sightseeing_earning[$k]));
				}

				if (!empty($active_transfers)) {

					if (isset($month_index_transfers[$k])) {
						//Transfers
						$monthly_transfers_booking[$k] = intval($month_index_transfers[$k]['total_booking']);
						$transfers_earning[$k] = round($month_index_transfers[$k]['monthly_earning']);
					} else {
						$monthly_transfers_booking[$k] = 0;
						$transfers_earning[$k] = 0;
					}
					@($time_line_report_average[$k] += round($transfers_earning[$k]))/(intval($monthly_transfers_booking[$k]) > 0 ? $monthly_transfers_booking[$k] : 1);
					($module_total_earning[5]['y'] += round($transfers_earning[$k]));
				}


			}
			// debug($module_total_earning);
			// exit;
			
			if (!empty($active_hotel)) {
				$time_line_report[0]['data'] = $monthly_hotel_booking;
				$module_total_earning[0]['color'] = $time_line_report[0]['color'];
				$group_time_line_report[] = array('type' => 'column', 'name' => 'Hotel', 'data' => $hotel_earning, 'color' => $time_line_report[0]['color']);
			}
			if (!empty($active_airline)) {
				$time_line_report[1]['data'] = $monthly_flight_booking;
				$module_total_earning[1]['color'] = $time_line_report[1]['color'];
				$group_time_line_report[] = array('type' => 'column','name' => 'Flight', 'data' => $flight_earning, 'color' => $time_line_report[1]['color']);
			}
			if (!empty($active_bus)) {
				$time_line_report[2]['data'] = $monthly_bus_booking;
				$module_total_earning[2]['color'] = $time_line_report[2]['color'];
				$group_time_line_report[] = array('type' => 'column','name' => 'Bus', 'data' => $bus_earning, 'color' => $time_line_report[2]['color']);
			}
			if (!empty($active_sightseeing)) {
				$time_line_report[3]['data'] = $monthly_sightseeing_booking;
				$module_total_earning[3]['color'] = $time_line_report[3]['color'];
				$group_time_line_report[] = array('type' => 'column','name' => 'Sightseeing', 'data' => $sightseeing_earning, 'color' => $time_line_report[3]['color']);
			}
			if (!empty($active_transfers)) {
				$time_line_report[5]['data'] = $monthly_transfers_booking;
				$module_total_earning[5]['color'] = $time_line_report[5]['color'];
				//$group_time_line_report[] = array('type' => 'column','name' => 'Transfers', 'data' => $transfers_earning, 'color' => $time_line_report[5]['color']);
				$group_time_line_report[] = array('type' => 'column','name' => 'Car Booking', 'data' => $transfers_earning, 'color' => $time_line_report[5]['color']);
			}

			
		
			$max_count = max(array_merge($monthly_hotel_booking, $monthly_flight_booking, $monthly_bus_booking,$monthly_sightseeing_booking,$monthly_transfers_booking));
			foreach ($time_line_report_average as $k => $v) {
				if ($v > 0) {
					$time_line_report_average[$k] = round($v/3);
				}
			}
			$group_time_line_report[] = array(
				'type' => 'spline', 'name' => 'Average', 'data' => $time_line_report_average,
				'marker' => array('lineColor' => '#e65100', 'color' => '#ff5722', 'lineWidth' => 2, 'fillColor' => '#FFF')
			);


			/*debug($group_time_line_report);exit;*/

		// exit;
			$page_data ['group_time_line_report']=$group_time_line_report;
			$page_data['module_total_earning']=$module_total_earning;
			$page_data['time_line_interval']=$time_line_interval;
			$page_data['max_count']=$max_count;
			$page_data['time_line_report']=$time_line_report;
			$page_data['time_line_report_average']=$time_line_report_average;

			
			if (!empty($active_hotel)) {
				$page_data['hotel_booking_count'] = $this->hotel_model->booking($condition, true);
				$page_data['villa_booking_count'] = $this->hotels_model->villa_booking($condition, true);
			}
			if (!empty($active_airline)) {
				$page_data['flight_booking_count'] = $this->flight_model->booking($condition, true);
			}
			if (!empty($active_bus)) {
				$page_data['bus_booking_count'] = $this->bus_model->booking($condition, true);
			}

			if (!empty($active_sightseeing)) {
				$page_data['sightseeing_booking_count'] = $this->sightseeing_model->booking($condition, true);
			}
			if (!empty($active_transfers)) {
				$page_data['transfers_booking_count'] = $this->transferv1_model->booking($condition, true);
			}
			// debug($page_data);
			// exit("291");
		$this->load->view('dashboard/index',$page_data);
	}
	
	public function loadBodyContent(){
		if(isset($_POST)){
				$data 				= $this->General_Model->getHomePageSettings();
				$dashboard_boday 	= $this->load->view('dashboard/dashboard_boday', $data,true);
				echo $dashboard_boday;exit;
		}
	}
		
	public function loginCheck(){		
	 	$resp = array();
		$username = $_POST["username"];
		$password = $_POST["password"];
		$resp['submitted_data'] = $_POST;
		$login_status = 'invalid';
		if($username == 'sunil' && $password == 'sunil')
		{
			$login_status = 'success';
		}
		$resp['login_status'] = $login_status;
       if($login_status == 'success')
		{
		 $resp['redirect_url'] = site_url().'login/dashboard';
		}
		echo json_encode($resp);
	}
	
	public function dashboard(){
		$data = $this->General_Model->getHomePageSettings();
		$this->load->view('dashboard/index',$data);
	}

	public function forgotPassword(){
		$this->load->view('login/forgot_password');
	}
	
	public function logout(){
		redirect('login','refresh');
	}
	
	function logsActivity($status=''){
		$data 						= $this->General_Model->getHomePageSettings();
		$data['status']				= $status;
	    $data['logs_activity'] 		= $this->Dashboard_Model->get_admin_logs_activity();
		$this->load->view('dashboard/activity_logs',$data);
	}

	function deleteLogs(){
		$data = $this->General_Model->getHomePageSettings();
		$data['logs_activity'] 		= $this->Dashboard_Model->delete_admin_logs_activity();
		redirect('dashboard/logsActivity');
	}
	
	function changePassword($status=''){
		try{
		$data						= $this->General_Model->getHomePageSettings();
		$data['status']				= $status;
		$data['admin_profile_info'] = $this->Dashboard_Model->getAdminDetails();
		$this->load->view('login/change_password', $data);
	   } catch(Exception $ex){
		  return $ex;
	  }
	}
	
	function updatePassword(){
		
		$this->form_validation->set_rules('current_password', 'Old Password', 'required');
		$this->form_validation->set_rules('new_password_text', 'New Password', 'required');
		$this->form_validation->set_rules('confirm_password', 'Re-Enter Password', 'required');	
		
		$status = 0;
		
		if($this->form_validation->run()==FALSE){
			redirect('dashboard/changePassword','refresh');
		}else{
			//$old_password 			= $this->input->post('current_password');
			$old_password = $password = "AES_ENCRYPT(".$this->input->post('current_password').",'".SECURITY_KEY."')";
			 $old_password_status 	= $this->Security_Model->checkAdminPassword($old_password);
		
			if(trim($this->input->post('new_password_text')) == $this->input->post('confirm_password')){
				if($old_password_status['status'] == 1 ){
					/*$new_password = trim($this->input->post('new_password_text'));*/
					$new_password = $password = "AES_ENCRYPT(".$this->input->post('new_password_text').",'".SECURITY_KEY."')";
					if ($this->Security_Model->updateAdminPassword($new_password)){ $status=1; }else{ $status=0; }
				}else{
				  $status	= 3;
				}
			}else{
				$status	= 4;
			}
			redirect('dashboard/changePassword/'.$status,'refresh');
		}
	}

	function settings($status = '',$link = 'ip'){
		$data 						= $this->General_Model->getHomePageSettings();
		$data['status']				= $status;
		$data['link']				= $link;
		$data['admin_profile_info'] = $this->Dashboard_Model->getAdminDetails();
		$data['white_list_ip'] 		= $this->Dashboard_Model->get_white_list_ip_details();
		$this->load->view('dashboard/settings',$data);
	}
	
	function add_white_list(){
		if(count($_POST) > 0){
			try{ 
			$this->General_Model->begin_transaction();
			$this->Dashboard_Model->add_white_list($_POST);
			$this->General_Model->commit_transaction();
			redirect('dashboard/settings','refresh');
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction();
			return $e;
		}
		}else{
			$this->load->view('dashboard/add_white_list_ip');
		}
	}
	
	function updateIpStatus($white_list_ip_id1, $status1) {
		$white_list_ip_id = json_decode(base64_decode($white_list_ip_id1));
		 $status = json_decode(base64_decode($status1)); 
		if($white_list_ip_id !='')
			$this->Dashboard_Model->updateIpStatus($white_list_ip_id, $status);
		redirect('dashboard/settings','refresh');
	}
	
	function active_white_list_ip($white_list_ip_id){
		if($white_list_ip_id!='')
			$this->Dashboard_Model->active_white_list_ip($white_list_ip_id);
		redirect('dashboard/settings','refresh');
	}
	
	function inactive_white_list_ip($white_list_ip_id){
		if($white_list_ip_id!='')
			$this->Dashboard_Model->inactive_white_list_ip($white_list_ip_id);
		redirect('dashboard/settings','refresh');
	}
	
	function deleteWhitelistIp($white_list_ip_id1){
		$white_list_ip_id = json_decode(base64_decode($white_list_ip_id1));
		if($white_list_ip_id!='')
			$this->Dashboard_Model->delete_white_list_ip($white_list_ip_id);
		redirect('dashboard/settings','refresh');
	}

	function editWhiteListIp($white_list_ip_id1){
		$white_list_ip_id = json_decode(base64_decode($white_list_ip_id1));
		$data 						= $this->General_Model->getHomePageSettings();
		$data['white_list_ip'] 		= $this->Dashboard_Model->get_white_list_ip_details($white_list_ip_id);
		$this->load->view('dashboard/edit_white_list_ip',$data);
	}
	
	function updateWhiteListIp($white_list_ip_id){
		if(count($_POST) > 0){
			try{
			$this->General_Model->begin_transaction();
			$this->Dashboard_Model->update_white_list_ip($_POST,$white_list_ip_id);
			$this->General_Model->commit_transaction();
			redirect('dashboard/settings','refresh');
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction();
			return $e;
		}
		}else if($product_id!=''){
			redirect('dashboard/edit_white_list_ip/'.$white_list_ip_id,'refresh');
		}else{
			redirect('dashboard/settings','refresh');
		}
	}

	function profileInfo(){
		$data 						= $this->General_Model->getHomePageSettings();
		$data['country'] 			= $this->General_Model->get_country_details();
		$data['admin_profile_info'] = $this->Dashboard_Model->getAdminDetails();
	    $this->load->view('dashboard/profile_info',$data);
	}
	
	function updateProfileInfo(){
		
		if(count($_POST) > 0) {
		      
			    $form_validator = $this->profileFormValidation();	
			    if($form_validator == FALSE) {
					$data 						= $this->General_Model->getHomePageSettings();
					$data['country'] 			= $this->General_Model->get_country_details();
					$data['admin_profile_info'] = $this->Dashboard_Model->getAdminDetails();
					$data['form_validation'] =  $form_validator;
					$this->load->view('dashboard/profile_info', $data);
			  }else {
					try {  
						$this->General_Model->begin_transaction();
						$data['admin_profile_info'] = $this->Dashboard_Model->getAdminDetails();
						$this->Dashboard_Model->updateAddressDetails($_POST, $data);
						$this->Dashboard_Model->updateProfileInfo($_POST, $data);
						$this->General_Model->commit_transaction();
						$this->session->set_flashdata(array('message' => 'Profile Update Successfully', 'type' => SUCCESS_MESSAGE,'override_app_msg'=>1));
					} catch(Exception $ex) {
						$this->General_Model->rollback_transaction();
						$ex->getMessage();
					}
		  }
			redirect('dashboard/profileInfo','refresh');
		}
	}
	
	 
  function profileFormValidation()
  {
	 $this->form_validation->set_rules('first_name', 'First Name', 'required', 'required|min_length['.minNameLength.']|max_lengt['.maxNameLength.']');
	 $this->form_validation->set_rules('last_name', 'Last Name', 'required', 'required|min_length['.minNameLength.']|max_lengt['.minNameLength.']');
	 $this->form_validation->set_rules('account_number', 'Account Number', 'required|min_length['.minAccountNumber.']|max_lengt['.maxAccountNumber.']');
	 $this->form_validation->set_rules('email', 'Email', 'required|min_length['.minEmailLength.']|max_lengt['.maxEmailLength.']');
	 $this->form_validation->set_rules('home_phone', 'Phone Number', 'required|min_length['.minLandlineNumberLength.']|max_lengt['.maxLandlineNumberLength.']');
	 $this->form_validation->set_rules('cell_phone', 'mobile Number', 'required|min_length['.mobileNumberLength.']|max_lengt['.mobileNumberLength.']');
	 $this->form_validation->set_rules('address', 'Address','required|min_length['.minAddressLength.']|max_lengt['.maxAddressLength.']');
	 $this->form_validation->set_rules('city_name', 'City Name', 'required|min_length['.minCityLength.']|max_lengt['.maxCityLength.']');
	 $this->form_validation->set_rules('state_name', 'State', 'required|min_length['.minCityLength.']|max_lengt['.minCityLength.']');
	 $this->form_validation->set_rules('zip_code', 'Zip Code','required|min_length['.minZipCode.']|max_lengt['.maxZipCode.']');
	 return $this->form_validation->run();
  }

  // Function to change language of the website
	public function change_language(){
		$language = $this->input->post('language');
		$_SESSION['India95']['language'] = $language;
        $response = array(
        	'status' => 1
        );
        echo json_encode($response);
	}
  
}
?>
