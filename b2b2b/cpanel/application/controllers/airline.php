<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Airline extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');		
		$this->load->model('Airline_Model');
		$this->load->library('form_validation');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	 $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
	function index(){
		$airline 					= $this->General_Model->getHomePageSettings();
		$airline['airline_list'] 	= $this->Airline_Model->getAirlineList();
		$this->load->view('airline/airline_list',$airline);
	}
	
	function addAirline(){ 
		$airline = $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
			$form_validator = $this->airlineFormValidation('add');
			if($form_validator == FALSE  ) {
				$this->load->view('airline/add_airline',$airline);	
			    }else{
			$airline_image = $this->General_Model->upload_image($_FILES, 'airline');
			$this->Airline_Model->addAirlineDetails($_POST,$airline_image);
			redirect('airline','refresh');
			}
		} else{
			
			$this->load->view('airline/add_airline',$airline);
		}
	}
	
	function activeAirline($airline_id){
		$this->Airline_Model->activeAirline($airline_id);
		redirect('airline','refresh');
	}
	
	function inactiveAirline($airline_id){
		$this->Airline_Model->inactiveAirline($airline_id);
		redirect('airline','refresh');
	}
	
	function deleteAirline($airline_id){
		$this->Airline_Model->deleteAirline($airline_id);
		redirect('airline','refresh');
	}
	
	function editAirline($airline_id)
	{
		$airline 					= $this->General_Model->getHomePageSettings();
		$airline['airline_list'] 	= $this->Airline_Model->getAirlineList($airline_id);
		$this->load->view('airline/edit_airline',$airline);
	}

	function updateAirline($airline_id)
	{ 
		if(count($_POST) > 0){
			$image_info_name = $this->General_Model->upload_image($_FILES, 'airline', $_REQUEST['old_image']);
			$this->Airline_Model->updateAirline($_POST,$image_info_name,$airline_id);
			redirect('airline','refresh');
		}else if($airline_id!=''){
			redirect('airline/edit_airline/'.$airline_id,'refresh');
		}else{
			redirect('airline','refresh');
		}
	}

	function airlineFormValidation($type)
  {
	 
	 $this->form_validation->set_rules('airline_code', 'Airline Code', 'required');
	 if($type == 'add') {
	    
	    $this->form_validation->set_rules('airline_code', 'Airline Code', 'required|xss_clean||is_unique[airlines_list.airline_code]|max_length[2]');
	    }
	 return $this->form_validation->run();
  }


}
