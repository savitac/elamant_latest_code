<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
// error_reporting(E_ALL);
ob_start();
class Login extends CI_Controller {	
	/*
	 * The China Gap
	 *
	 * @package		India95 Controller
	 * @author		Sunil G R
	 * @copyright	Copyright (c) 2016 - 2017, PROVABTECHNOSOFT PVT. LTD.
	 * 
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('General_Model');
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
		$this->load->model('Crud_Model');
		$this->load->model('Security_Model');
		$this->load->model('Email_Model');
	}
	
	
	public function index(){
		if($this->session->userdata('provabAdminLoggedIn') == ""){
	        $this->load->view('login/login');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In"){
			redirect('dashboard','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen"){
			redirect('login/lock_screen','refresh');
		}else{			
			$this->load->view('login/login');
		}
	}
	
	public function loginCheck(){		
		if($this->session->userdata('provab_admin_logged_in')){
			redirect('dashboard','refresh');
		} 			
		$type 					 =	"URL";
		$recent_tracking_id 	 = $this->Security_Model->adminIpTrack($type);
		$ip_white_list_status 	 = $this->Security_Model->adminIpCheck();
		$admin_login_attempt = $this->Security_Model->login_attempts();
		$login_attempts = $admin_login_attempt[0]->login_attempts;
		//if($ip_white_list_status == "ACTIVE"){
		if(true){
			$ip_track = $this->Security_Model->adminIpAttempt($recent_tracking_id);
			if($ip_track['row_count'] < $login_attempts){
				if(count($_POST) > 0){	
					$resp = array();
					$username = $_POST["username"];
					$password = "AES_ENCRYPT(".$_POST["password"].",'".SECURITY_KEY."')";
					$resp['submitted_data'] = base64_encode(serialize($_POST)); 	
					//$resp['login_status'] =  $login_status = 'invalid';
					$result = $this->Security_Model->checkLoginStatus($username,$password);
					/*debug($result);
					exit;*/
					if($result['status']	==	1){
						$resp['loginStatus'] =  $login_status = 'success';
						$sessionUserInfo = array( 
													'provabAdminId' 			=> $result['result']->admin_id,
													'provabAdminName'	 		=> $result['result']->admin_name,
													'adminRole'	 				=> $result['result']->role_details_id,
													'provabAdminLoggedIn' 		=> "Logged_In",
													'domaninListfk'=>$result['result']->domain_list_fk
												);
						$this->session->set_userdata($sessionUserInfo);
						
						$updatestatus = array(
							'provabAdminId'    => $result['result']->admin_id,
							'provabAdminName'  => $result['result']->admin_name,
							'provabAdminEmail' => $result['result']->admin_email,
							'status' => '1',
							'provabAdminLoggedIn' 		=> "Logged_In"
							);
						$this->Security_Model->insertStatus($updatestatus);
						$this->Security_Model->updateLoginStatus($this->session->userdata('provabAdminId'),$recent_tracking_id);
						$resp['redirectUrl'] = site_url('dashboard/index');
					}else{
						$type 					 = "FAIL"; $recent_tracking_id = '';
						$recent_tracking_id 	 = $this->Security_Model->adminIpTrack($type);
						$ip_track 				 = $this->Security_Model->adminIpAttempt($recent_tracking_id);
						if($ip_track['row_count'] < $login_attempts){
							$resp['loginStatus'] =  $login_status = 'invalid';
						}else{
							$resp['loginStatus'] 	=  $login_status 		= 'success';
							$resp['redirectUrl']	= site_url('login/blocked');
						}
					}					
				}else{
					redirect('login','refresh');
				}
			}else{
				$resp['loginStatus'] 	=  $login_status 		= 'success';
				$resp['redirectUrl']	= site_url('login/blocked');
			}
		}else if($ip_white_list_status == "BLOCK"){
			$resp['loginStatus'] 	=  $login_status 		= 'success';
			$resp['redirectUrl']	= site_url('login/blocked');
		}else{
			$resp['loginStatus'] 	=  $login_status 		= 'success';
			$resp['redirectUrl']	= site_url('login/denied');
		}
	echo json_encode($resp);	
	}
	
	public function blocked(){
		$this->load->view('error/blocked');
	}
	
	public function denied(){
		$this->load->view('error/denied');
	}
	
	public function notfound(){
		$this->load->view('error/not_found');
	}
	
	public function forgot_password(){
		$this->load->view('login/forgot_password');
	}
	
	public function logout(){
		$logout = $this->session->userdata('provabAdminId');

		$status = array(
							'status' => '0'
							);
		$this->Security_Model->updateStatus($status,$logout);
		$this->Security_Model->adminIpLogoutTrack("URL");
		$this->session->unset_userdata('sessionUserInfo');
		$this->session->sess_destroy();
		redirect('login','refresh');
	}
	
	public function lockScreen(){
		if(count($_POST) > 0){
			$resp = array(); 	$provab_admin_id = $this->session->userdata('provab_admin_id'); $password = $_POST["password"];
			$resp['submitted_data'] = $_POST; 	$resp['login_status'] =  $login_status = 'invalid';
			$result = $this->Security_Model->check_login_status_by_Id($provab_admin_id,$password);
			if($result['status']	==	1){
				$resp['login_status'] 	=  $login_status 		= 'success';
				$sessionUserInfo 		= array( 
												'provab_admin_id' 			=> $result['result']->admin_id,
												'provab_admin_name'	 		=> $result['result']->admin_name,
												'domain_list_fk'	 		=> $result['result']->domain_list_fk,
												'provab_admin_logged_in' 	=> "Logged_In"
											);
				$this->session->set_userdata($sessionUserInfo);
				$url_details = $this->Security_Model->get_previous_url();
				if($url_details->url =='')
					$resp['redirect_url'] = site_url().'dashboard';
				else
					$resp['redirect_url'] = $url_details->url;
			}else{
				$resp['login_status'] =  $login_status = 'invalid';
				$resp['redirect_url']	= site_url('login/lockScreen');
			}
			echo json_encode($resp);	
		}else{
			if($this->session->userdata('provab_admin_logged_in') == ""){
				$this->load->view('login/login');
			}else if($this->session->userdata('provab_admin_logged_in') == "Logged_In"){
				redirect('dashboard','refresh');
			}else if($this->session->userdata('provab_admin_logged_in') == "Lock_Screen"){
				$this->load->view('login/logoff');
			}else{			
				$this->load->view('login/login');
			}
		}
	}
	/*public function logoff(){
		$resp['login_status'] 	=  $login_status 		= 'success';
		$this->session->set_userdata('provab_admin_logged_in','Lock_Screen') ;
		if($this->session->userdata('provab_admin_session_id') == ""){
			$this->session->set_userdata('provab_admin_session_id',session_id()) ;
		}
		$lockData = $this->lockData($_POST);
		$this->Crud_Model->insertRowReturnId('admin_lock_screen_details', $lockData);
		$this->Security_Model->insert_lock_screen_details($_POST);
		$resp['redirect_url']	= site_url('login/lock_screen');
		echo json_encode($resp);
	}*/

	public function logoff(){
		$resp['login_status'] 	=  $login_status 		= 'success';
		$this->session->set_userdata('provab_admin_logged_in','Lock_Screen') ;
		if($this->session->userdata('provab_admin_session_id') == ""){
			$this->session->set_userdata('provab_admin_session_id',session_id()) ;
		}
		$this->Security_Model->insert_lock_screen_details($_POST);
		$resp['redirect_url']	= site_url('login/lockScreen');
		echo json_encode($resp);
	}
	
	
	function loginTrackData($type){
		$data = array(
					'admin_id' => '1',
					'login_attempt_type' => $type,
					'login_track_details_ip' => $_SERVER['REMOTE_ADDR'],
					'login_track_status_info' => $_SERVER['HTTP_USER_AGENT'],
					'login_track_details_system_info' => $_SERVER['REMOTE_ADDR'].'||'.$_SERVER['REMOTE_PORT']
				);
	   return $data;
	}
	
	function lockData($lockData){
		 $data = array(
					'admin_id' 						=> $this->session->userdata('provab_admin_id'),
					'provab_admin_session_id' 		=> $this->session->userdata('provab_admin_session_id'),
					'user_name' 					=> $this->session->userdata('provab_admin_name'),
					'url'		 					=> $lockData['current_url'],
					'status'						=> 'ACTIVE',
					'admin_lock_screen_timestamp' 	=> (date('Y-m-d H:i:s'))
				);
		return $data;
	}

	public function send_password_reset_link() {
//echo "<pre>"; print_r($_REQUEST);echo "</pre>"; exit();
        $email = $data['user_email'] = $_REQUEST['email_id'];
        $count = $this->General_Model->isRegistered($email)->num_rows();
        if ($count == 1) {
            $userInfo = $this->General_Model->isRegistered($email)->row();
            if ($userInfo->admin_status == 'ACTIVE') {
                $user_password = $this->General_Model->getLogin($email, $userInfo->admin_id)->row();
                
                $password = "AES_ENCRYPT(".$user_password->admin_password.",'".SECURITY_KEY."')";
                //$password = md5($user_password->admin_password);
                $status = $this->get_mail_content_forgotpass($email, $password);
                if ($status == '1') {
                    $response = array(
                        'status' => '1',
                        'success' => 'true',
                        'msg' => "A link to reset your password has been sent to " . $email . ".!"
                    );
                } else {
                    $response = array(
                        'status' => '0',
                        'success' => 'false',
                        'msg' => "Please contact the Administrator."
                    );
                }
            } else {
                $response = array(
                    'status' => '0',
                    'success' => 'false',
                    'msg' => "Invalid Email."
                );
            }
        } else {
            $response = array(
                'status' => '0',
                'success' => 'false',
                'msg' => 'Invalid Email.'
            );
        }
        echo json_encode($response);
    }
    
   public function get_mail_content_forgotpass($email, $password) {
        $data['password'] = $password;
        $data['user_data'] = $this->General_Model->isRegistered($email)->row();
        $email_type = 'forgotpassword';
        $data['email_template'] = $this->Email_Model->get_email_template($email_type)->row();
        $key = $this->generate_random_key();
        $secret = md5($email);
        $this->General_Model->updatePwdResetLink($data['user_data']->admin_id, $key, $secret);
        $data['reset_link'] = base_url() . 'login/set_password/' . $key . '/' . $secret;

        $Response = $this->Email_Model->sendmail_forgot_password($data);
        return $Response;
    }

    public function generate_random_key($length = 50) {
        $alphabets = range('A', 'Z');
        $numbers = range('0', '9');
        $additional_characters = array('_', '.');
        $final_array = array_merge($alphabets, $numbers, $additional_characters);
        $id = '';
        while ($length--) {
            $key = array_rand($final_array);
            $id .= $final_array[$key];
        }
        return $id;
    } 

    public function set_password($key, $secret) {
        if ($key == '' || $secret == '') {
            $data['msg'] = 'sorry link has been expired, plese reset again';
            $data['status'] = '0';
        } else {
            $count = $this->General_Model->isvalidSecrect($key, $secret)->num_rows();
            if ($count == 1) {
                $user_data = $this->General_Model->isvalidSecrect($key, $secret)->row();
                $data['status'] = '1';
                $data['email'] = $user_data->admin_email;
            } else {
                $data['msg'] = 'sorry link has been expired, plese reset again';
                $data['status'] = '0';
            }
        }
        $this->load->view('login/reset_password', $data);
    }

     public function resetpwd() {
        $email = $this->input->get('email_id');
      //  $password = "AES_ENCRYPT(" . $this->input->get('new_password') . ",'" . SECURITY_KEY . "')";
        //$password = md5($this->input->get('new_password'));
        $password = "AES_ENCRYPT(".$this->input->get('new_password').",'".SECURITY_KEY."')";
        $this->General_Model->update_agent($password, $email);
        $response = array(
            'status' => '1',
            'success' => 'true',
            'msg' => "Your password has been changed you can login now!"
        );
        echo json_encode($response);
    }

    // Function to change language of the website
	public function change_language(){
		$language = $this->input->post('language');
		$_SESSION['India95']['language'] = $language;
        $response = array(
        	'status' => 1
        );
        echo json_encode($response);
	}
	
}
?>
