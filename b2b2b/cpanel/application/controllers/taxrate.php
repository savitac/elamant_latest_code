<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Taxrate extends CI_Controller {	

	public function __construct(){
		
		parent::__construct();	
		$this->load->model('General_Model');	
		$this->load->model('Taxrate_Model');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
		$this->check_admin_login();		
	}

	private function check_admin_login(){
		if($this->session->userdata('provabAdminLoggedIn') == ""){
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In"){
		 }else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen"){
			redirect('login/lock_screen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In"){
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lock_screen','refresh');
		}
    }
    
   function get_tax_data($hotel_id){
		$options = '';		
		if(!empty($hotel_id)){
			$result = $this->Taxrate_Model->get_tax_list_by_hotel($hotel_id);
			if($result!=''){
			foreach($result as $row){
				$options .= '<option value="'.$row->hotel_tax_type_id.'">'.$row->tax_type_name.'</option>';				
			}}		
		}
		echo $options;exit;
	}
	
	public function index(){
		$hotels 						= $this->General_Model->getHomePageSettings();
		$hotels['tax_rate_list'] 	   	= $this->Taxrate_Model->get_tax_rate_list("",$hotels);
		$this->load->view('taxrate/tax_rate_list',$hotels);
	}
	
	function add_tax_rate(){
		$hotels 		= $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
			$form_validator = $this->formValidator('add');
			if($form_validator == FALSE  ) {
				$this->load->view('taxrate/tax_rate',$hotels);
			    }else{
			try{
			$this->General_Model->begin_transaction();
			$this->Taxrate_Model->add_tax_rate($_POST);
			$this->General_Model->commit_transaction();
			redirect('taxrate','refresh');
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction();
			return $e;
		}
	}
		}else{
			
     		$this->load->view('taxrate/tax_rate',$hotels);
		}
	} 
	
	function delete_taxrate($rate_id1){
		$rate_id 	= json_decode(base64_decode($rate_id1));
		if($rate_id != ''){
			$this->Taxrate_Model->delete_taxrate($rate_id);
		}
		redirect('taxrate','refresh');
	}

	function inactive_taxrate($rate_id1){
		$rate_id 	= json_decode(base64_decode($rate_id1));
		if($rate_id != ''){
			$this->Taxrate_Model->inactive_taxrate($rate_id);
		}
		redirect('taxrate','refresh');
	}
	
	function active_taxrate($rate_id1){
		$rate_id 	= json_decode(base64_decode($rate_id1));
		if($rate_id != ''){
			$this->Taxrate_Model->active_taxrate($rate_id);
		}
		redirect('taxrate','refresh');
	}
	
	function edit_taxrate($rate_id1)
	{   
		$rate_id 	= json_decode(base64_decode($rate_id1));
		if($rate_id != ''){
			$hotels 						= $this->General_Model->getHomePageSettings();
			$hotels['tax_rate_list'] 	   	= $this->Taxrate_Model->get_tax_rate_list($rate_id);
			$this->load->view('taxrate/edit_tax_rate',$hotels);
		}else{
			redirect('taxrate','refresh');
		}
	}
	
	function update_tax_rate($rate_id1){
		$hotels 	= $this->General_Model->getHomePageSettings();
		$rate_id 	= json_decode(base64_decode($rate_id1));
		if($rate_id != ''){
			if(count($_POST) > 0){
				$form_validator = $this->formValidator('edit');
				if($form_validator == FALSE  ) {
				redirect('taxrate/edit_taxrate/'.$rate_id1,'refresh');	
			    }else{
				try{
				$this->General_Model->begin_transaction();
				$this->Taxrate_Model->update_tax_rate($_POST,$rate_id);
				$this->General_Model->commit_transaction();
				redirect('taxrate','refresh');
			} catch(Exception $e) {
			  $this->General_Model->rollback_transaction();
			  return $e;
			}
		}
			}else{
				
				$hotels['hotels_list'] 	   		= $this->Hotel_Model->get_hotel_crs_list();
				$hotels['settings'] 			= $this->Hotel_Model->get_hotel_settings_list();
				$this->load->view('hotel/taxrate/tax_rate',$hotels);
			}
		}else{
			redirect('taxrate','refresh');
		}
	} 

	public function getCountry()
	{
		$term = $this->input->get('term');
        $term = trim(strip_tags($term));
        $hotelsp = $this->Taxrate_Model->getCountry($term)->result();
        foreach($hotelsp as $hotelp){
            $apts['label'] = $hotelp->country_name;
            $apts['value'] = $hotelp->country_name;
            $apts['id'] = $hotelp->country_id;
            $result[] = $apts; 
        }
        
        echo json_encode($result);
	}

	public function getstateName()
	{
		//print_r($this->input->post('CityName'));
		$city = $this->input->post('CityName');
		
		$hotelsp = $this->Taxrate_Model->getstateName($city)->result();
		//echo "<pre/>";print_r($hotelsp); exit();
		//$html = '';
		$html = "<option value=''>Select State</option>";
		foreach ($hotelsp as $key => $hotelp) {
			$html .= '<option value="'.$hotelp->State_id.'">'.$hotelp->state_name.'</option>';
		}
		echo json_encode(array('phphtml' => $html));//format the array into json data
	}

	function formValidator($type){
	   $this->form_validation->set_rules('gst', 'GST', 'required');
	   $this->form_validation->set_rules('tax', 'Tax', 'required');
	   $this->form_validation->set_rules('service_charge', 'service charge', 'required');
	   $this->form_validation->set_rules('vat', 'Vat', 'required');
	   
	   
	   if($type == 'add') {
	    $this->form_validation->set_rules('gst', 'GST', 'numeric|xss_clean');
	    $this->form_validation->set_rules('tax', 'Tax', 'numeric|xss_clean|');
	    $this->form_validation->set_rules('service_charge', 'service charge', 'numeric|xss_clean|');
	    $this->form_validation->set_rules('vat', 'vat', 'numeric|xss_clean|');
	    
	    }

	   if($type == 'edit') {
	    $this->form_validation->set_rules('gst', 'GST', 'numeric|xss_clean');
	    $this->form_validation->set_rules('tax', 'Tax', 'numeric|xss_clean|');
	    $this->form_validation->set_rules('service_charge', 'service charge', 'numeric|xss_clean|');
	    $this->form_validation->set_rules('vat', 'vat', 'numeric|xss_clean|');
	    
	    }
	    
	   return $this->form_validation->run();
    }	
}
