<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Social extends CI_Controller {	
	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Social_Model');
		$this->load->model('Banner_Model');
		$this->load->library('form_validation');
		$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	 	$this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
    function socialLinks(){
		$social 					= $this->General_Model->getHomePageSettings();
		$social['links_list'] 		= $this->Social_Model->getSocialLinksList();
		$this->load->view('social_links/social_links_list',$social);
	}
	
	function addSocialLink(){
		if(count($_POST) > 0){
			$this->Social_Model->addSocialLinks($_POST);
			redirect('social/socialLinks','refresh');
		}else{

			$social_links 	= $this->General_Model->getHomePageSettings();
			$social_links['agents'] 		= $this->Banner_Model->getAgentList();
			$this->load->view('social_links/add_social_link',$social_links);
		}
	}
	
	function inactiveSocialLink($social_link_id1){
		$social_link_id 	= json_decode(base64_decode($social_link_id1));
		if($social_link_id != ''){
			$this->Social_Model->inactiveSocialLink($social_link_id);
		}
		redirect('social/socialLinks','refresh');
	}
	
	function activeSocialLink($social_link_id1){
		$social_link_id 	= json_decode(base64_decode($social_link_id1));
		if($social_link_id != ''){
			$this->Social_Model->activeSocialLink($social_link_id);
		}
		redirect('social/socialLinks','refresh');
	}
	
	function editSocialLink($social_link_id1){
		$social_link_id 	= json_decode(base64_decode($social_link_id1));
		if($social_link_id != ''){
			$social_links 						= $this->General_Model->getHomePageSettings();
			$social_links['links_list'] 		= $this->Social_Model->getSocialLinksList($social_link_id);
			$this->load->view('social_links/edit_social_link',$social_links);
		}else{
			redirect('social/socialLinks','refresh');
		}
	}
	
	function updateSocialLink($social_link_id1){
		$social_link_id 	= json_decode(base64_decode($social_link_id1));
		if($social_link_id != ''){
			if(count($_POST) > 0){
				$this->Social_Model->updateSocialLink($_POST,$social_link_id);
				redirect('social/socialLinks','refresh');
			}else if($social_link_id!=''){
				redirect('social/edit_social_link/'.$social_link_id,'refresh');
			}else{
				redirect('social/socialLinks','refresh');
			}
		}else{
			redirect('social/socialLinks','refresh');
		}
	}
	
	function deleteSocialLink($social_link_id1){
		$social_link_id 	= json_decode(base64_decode($social_link_id1));
		if($social_link_id != ''){
			$this->Social_Model->deleteSocialLink($social_link_id);
		}
		redirect('social/socialLinks','refresh');
	}
	
	function newsletters(){
		$newsletters 							= $this->General_Model->getHomePageSettings();
		$newsletters['subscribers_list'] 		= $this->Social_Model->get_newsletter_subscribers_list();
		$this->load->view('newsletter_subscribers/subscribers_list',$newsletters);
	}
	
	function inactive_subscriber($subscriber_id){
		$this->Social_Model->inactive_subscriber($subscriber_id);
		redirect('social/newsletters','refresh');
	}
	
	function active_subscriber($subscriber_id){
		$this->Social_Model->active_subscriber($subscriber_id);
		redirect('social/newsletters','refresh');
	}
	
	function delete_subscriber($subscriber_id){
		$this->Social_Model->delete_subscriber($subscriber_id);
		redirect('social/newsletters','refresh');
	}
	
	function send_mail_to_subscriber($subscriber_id){
		$newsletters 						= $this->General_Model->getHomePageSettings();
		$newsletters['subscribers_list'] 	= $this->Social_Model->get_newsletter_subscribers_list($subscriber_id);
		$this->load->view('newsletter_subscribers/mailto_subscriber',$newsletters);
	}
	
	function subscriber_mailing($subscriber_id){
		if(count($_POST) > 0){
			$this->Social_Model->sendmail_subscriber($_POST,$subscriber_id);
			redirect('social/newsletters','refresh');
		}else if($subscriber_id!=''){
			redirect('social/send_mail_to_subscriber/'.$subscriber_id,'refresh');
		}else{
			redirect('social/newsletters','refresh');
		}
	}
}
