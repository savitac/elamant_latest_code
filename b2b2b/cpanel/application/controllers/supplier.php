<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class Supplier extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Supplier_Model');		
		$this->load->model('Product_Model');		
		$this->load->model('Domain_Model');		
		$this->load->model('Usertype_Model');		
		$this->load->model('Api_Model');
		$this->load->model('Email_Model');
	    $this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	}
    
	function supplier_list(){
		$supplier 				= $this->General_Model->getHomePageSettings();
		$supplier['supplier_list'] 	= $this->Supplier_Model->get_supplier_list();
		$this->load->view('supplier/supplier_list',$supplier);
	}

	function add_supplier(){
		 $suppliers 				= $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
			$form_validator = $this->formValidator('add');
			if($form_validator == FALSE) {
			  $this->load->view('supplier/add_supplier',$suppliers);
			} else{
			$email = $_POST['email_id'];
			$Query="select * from  supplier_details  where supplier_email ='".$email."'";
			$query=$this->db->query($Query);
			if ($query->num_rows() > 0)
		{
			
			$data['status'] = '<div class="alert alert-block alert-danger">
							   <a href="#" data-dismiss="alert" class="close">×</a>
							   <h4 class="alert-heading">Already Email Id Registered!</h4>
							   kindly Use With Another Email Address !!!
							   </div>';
			$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$res = "";
			for ($i = 0; $i < 10; $i++) {
				$res .= $chars[mt_rand(0, strlen($chars)-1)];
			}
			
			$this->load->view('supplier/add_supplier',$data);
		
		}else{
			$user_profile_name = $this->General_Model->upload_image($_FILES, 'users');
			try{
			$this->General_Model->begin_transaction();
			$this->Supplier_Model->add_supplier_details($_POST,$user_profile_name);
			$this->Email_Model->add_supplier_send_mail($_POST);
			$this->General_Model->commit_transaction();
			redirect('supplier/supplier_list','refresh');
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction();
		}
		} }
		}else{
			
			$suppliers['country'] 	=  $this->General_Model->get_country_details();
			$suppliers['supplier_type'] 	=  $this->Supplier_Model->get_supplier_type();
		   $this->load->view('supplier/add_supplier',$suppliers);
		}
	}

	function check_unique_email($email){
		if($email!=''){
			if(preg_match("/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $email)){
				$Status 	=	$this->Supplier_Model->get_all_supplier_email($email);
				if($Status  == "YES") {
					echo '<span class="nosuccess validate-has-error">This Email Id already Exists, Please Choose diffrent Email Id</span>';exit;
				}else{
					echo '<span class="success" style="color: green;">valid</span>';exit;
				}
			}else{
				echo '<span class="nosuccess">Please Enter the Valid Email Id</span>';exit;
			}
		}else{	
			 echo '<span class="nosuccess">Please Enter the Valid Email Id</span>';exit;
		}
	}
	
	function active_supplier($supplier_id1){
		$supplier_id 	= json_decode(base64_decode($supplier_id1));
		$supplier = $this->Supplier_Model->get_supplier_list($supplier_id);
		if($supplier_id != ''){
			$this->Supplier_Model->active_supplier($supplier_id);
			$supplier['email_template'] = $this->Email_Model->get_email_template('add_supplier_active')->row();
			$this->Email_Model->send_add_supplier_active($supplier);
		}
		redirect('supplier/supplier_list','refresh');
	}
	
	function inactive_supplier($supplier_id1){
		$supplier_id 	= json_decode(base64_decode($supplier_id1));
		$supplier = $this->Supplier_Model->get_supplier_list($supplier_id); //echo '<pre>'; print_r($supplier); exit();
		if($supplier_id != ''){
			$this->Supplier_Model->inactive_supplier($supplier_id);
			$supplier['email_template'] = $this->Email_Model->get_email_template('supplier_deactive')->row();
			$this->Email_Model->send_add_supplier_active($supplier);
		}
		redirect('supplier/supplier_list','refresh');
	}
	
	function delete_supplier($supplier_id1){
		$supplier_id 	= json_decode(base64_decode($supplier_id1));
		if($supplier_id != ''){
			$this->Supplier_Model->delete_supplier($supplier_id);
		}
		redirect('supplier/supplier_list','refresh');
	}
	
	function edit_supplier($supplier_id1)
	{
		$supplier_id 	= json_decode(base64_decode($supplier_id1));
		
		if($supplier_id != ''){			
			$suppliers 				= $this->General_Model->getHomePageSettings();
			$suppliers['country'] 	=  $this->General_Model->get_country_details();
			$suppliers['supplier'] 	= $this->Supplier_Model->get_supplier_list($supplier_id);
			$suppliers['supplier_type'] 	=  $this->Supplier_Model->get_supplier_type();
			$this->load->view('supplier/edit_supplier',$suppliers);
		}else{
			redirect('supplier/supplier_list','refresh');
		}
	}
	
	function update_supplier($supplier_id1)
	{
		$supplier_id 	= json_decode(base64_decode($supplier_id1));
		if($supplier_id != ''){
			if(count($_POST) > 0){
				$form_validator = $this->formValidator('edit');
				if($form_validator == FALSE) {
					redirect('supplier/edit_supplier',$supplier_id1);	
			    }else{
				$image_info_name = $this->General_Model->upload_image($_FILES, 'users', $_REQUEST['old_image']);
				try{
				$this->General_Model->begin_transaction();
				$this->Supplier_Model->update_supplier($_POST,$supplier_id, $image_info_name);
				$this->General_Model->commit_transaction();
				redirect('supplier/supplier_list','refresh');
			   } catch(Exception $e){
				   $this->General_Model->rollback_transaction();
				   return $e;
			   }
			}
			   
			}else if($supplier_id!=''){
				redirect('supplier/edit_supplier/'.$supplier_id,'refresh');
			}else{
				redirect('supplier/supplier_list','refresh');
			}
		}else{
			redirect('supplier/supplier_list','refresh');
		}
	}
	
	function bankInfo($supplier_id1){
	    $supplier_id 	= json_decode(base64_decode($supplier_id1));
		if(count($_POST) > 0){
			$this->Supplier_Model->add_supplier_bank_info($_POST,$supplier_id);
			redirect('supplier/supplier_list/','refresh');
		
		}else{
			$suppliers 				= $this->General_Model->getHomePageSettings();
			$suppliers['supplier_id']= $supplier_id;
			$suppliers['bank_info'] = $this->Supplier_Model->get_supplier_bank_info($supplier_id);
			$suppliers['country'] 	=  $this->General_Model->get_country_details();
		    $this->load->view('supplier/bank_info',$suppliers);
		}
	}

     function formValidator($type){
	   $this->form_validation->set_rules('first_name', 'First Name', 'required');
	   
	   $this->form_validation->set_rules('email_id', 'Email', 'required');
	   $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required');
	   $this->form_validation->set_rules('company_name', 'Company Name', 'required');
	   $this->form_validation->set_rules('address', 'Address', 'required');
	   $this->form_validation->set_rules('city', 'City', 'required');
	   $this->form_validation->set_rules('state_name', 'State Name', 'required');
	   $this->form_validation->set_rules('zip_code', 'Zip Code', 'required');
	   $this->form_validation->set_rules('company_address', 'Company Address', 'required');
	   $this->form_validation->set_rules('company_city', 'Company City', 'required');
	   $this->form_validation->set_rules('company_state_name', 'Company State Name', 'required');
	   $this->form_validation->set_rules('company_zipcode', 'Company ZipCode', 'required');
	   $this->form_validation->set_rules('phone_no', 'Phone Number', 'required');
	   $this->form_validation->set_rules('payee_name', 'Payee Name', 'required');
	   $this->form_validation->set_rules('bank_name', 'Bank Name', 'required');
	   $this->form_validation->set_rules('account_number', 'Account Number', 'required');
	   $this->form_validation->set_rules('ifsc_code', 'IFSC Code', 'required');
	   $this->form_validation->set_rules('bank_address', 'Bank Address', 'required');	   
	   $this->form_validation->set_rules('bank_city_name', 'Bank City Name', 'required');
	   $this->form_validation->set_rules('bank_state_name', 'Bank State Name', 'required');
	   $this->form_validation->set_rules('bank_zipcode', 'Bank ZipCode', 'required');
	   
	   if($type == 'add') {
	   	$this->form_validation->set_rules('first_name', 'First name', 'required|min_length[1]|max_length[50]');
	    
	    $this->form_validation->set_rules('email_id', 'Email', 'required|valid_email');
	    $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('company_name', 'Company Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('address', 'Address', 'required|min_length[1]|max_length[100]');
	    $this->form_validation->set_rules('state_name', 'State Name', 'required|min_length[1]|max_length[50]');
	    $this->form_validation->set_rules('zip_code', 'Zip Code', 'numeric|xss_clean');
	    $this->form_validation->set_rules('company_address', 'Company Address', 'required|min_length[3]|max_length[100]');
	    $this->form_validation->set_rules('company_city', 'Company City', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('company_state_name', 'Company State Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('company_zipcode', 'Company ZipCode', 'numeric|xss_clean');
	    $this->form_validation->set_rules('phone_no', 'Phone Number', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('payee_name', 'Payee name', 'required|min_length[3]|max_length[150]');
	    $this->form_validation->set_rules('bank_name', 'Bank name', 'required|min_length[3]|max_length[150]');
	    $this->form_validation->set_rules('account_number', 'Account Number', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('ifsc_code', 'IFSC Code', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('bank_address', 'Bank Address', 'required|min_length[3]|max_length[150]');
	    $this->form_validation->set_rules('bank_city_name', 'Bank City Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('bank_state_name', 'Bank State Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('bank_zipcode', 'Bank ZipCode', 'numeric|xss_clean');
	    }

	    if($type == 'edit') {
	   	$this->form_validation->set_rules('first_name', 'First name', 'required|min_length[1]|max_length[50]');
	    
	    $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('company_name', 'Company Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('address', 'Address', 'required|min_length[3]|max_length[100]');
	    $this->form_validation->set_rules('state_name', 'State Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('zip_code', 'Zip Code', 'numeric|xss_clean');
	    $this->form_validation->set_rules('company_address', 'Company Address', 'required|min_length[3]|max_length[100]');
	    $this->form_validation->set_rules('company_city', 'Company City', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('company_state_name', 'Company State Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('company_zipcode', 'Company ZipCode', 'numeric|xss_clean');
	    $this->form_validation->set_rules('phone_no', 'Phone Number', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('payee_name', 'Payee name', 'required|min_length[3]|max_length[150]');
	    $this->form_validation->set_rules('bank_name', 'Bank name', 'required|min_length[3]|max_length[150]');
	    $this->form_validation->set_rules('account_number', 'Account Number', 'required|min_length[3]|max_length[50]');
	   	$this->form_validation->set_rules('ifsc_code', 'IFSC Code', 'required|min_length[3]|max_length[50]');
	   	$this->form_validation->set_rules('bank_address', 'Bank Address', 'required|min_length[3]|max_length[150]');
	    $this->form_validation->set_rules('bank_city_name', 'Bank City Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('bank_state_name', 'Bank State Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('bank_zipcode', 'Bank ZipCode', 'numeric|xss_clean');
	    }
	    
	   return $this->form_validation->run();
    }	

}
?>
