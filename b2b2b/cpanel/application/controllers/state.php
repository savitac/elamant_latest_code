<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting(E_ALL);
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
class State extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('State_Model');
		$this->load->model('Taxrate_Model');
		$this->load->library('form_validation');	
		if(!isset($_SESSION['ses_id'])){
			$sec_res 			= session_id();
	    	$_SESSION['ses_id'] = $sec_res;
		}
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	 
	function index(){
		$state 					= $this->General_Model->getHomePageSettings();
		$state['state_list'] 	= $this->State_Model->getStateList();
		//echo '<pre>sanjay'; print_r($state['state_list']); exit();
		$this->load->view('state/state_list',$state);
	}
	
	function addState(){ //echo '<pre>'; print_r($_POST); exit();
		$state = $this->General_Model->getHomePageSettings();
		if(count($_POST) > 0){
			$form_validator = $this->formValidator('add');	
			if($form_validator == FALSE  ) {
				$this->load->view('state/add_state',$state);	
			    }else{
			$this->State_Model->addStateDetails($_POST);
			redirect('state','refresh');
		}
		}else{
			
			$this->load->view('state/add_state',$state);
		}
	}
	
	function activeState($country_id1){
		$country_id 	= json_decode(base64_decode($country_id1));
		if($country_id != ''){
			$this->State_Model->activeState($country_id);
		}
		redirect('state','refresh');
	}
	
	function inactiveState($country_id1){
		$country_id 	= json_decode(base64_decode($country_id1));
		if($country_id != ''){
			$this->State_Model->inactiveState($country_id);
		}
		redirect('state','refresh');
	}
	
	function delete_country($country_id1){
		$country_id 	= json_decode(base64_decode($country_id1));
		if($country_id != ''){
			$this->State_Model->delete_country($country_id);
		}
		redirect('state','refresh');
	}
	
	function editState($country_id1)
	{
		$country_id 	= json_decode(base64_decode($country_id1));
		if($country_id != ''){
			$state 					= $this->General_Model->getHomePageSettings();
			$state['state_list'] 	= $this->State_Model->getStateList($country_id);
			//echo '<pre>'; print_r($state['state_list']); exit();
			$this->load->view('state/edit_state',$state);
		}else{
			redirect('state','refresh');
		}
	}

	function updateState($country_id1)
	{
		$country_id 	= json_decode(base64_decode($country_id1));
		if($country_id != ''){
			if(count($_POST) > 0){
				
				$this->State_Model->updateState($_POST,$country_id);
				redirect('state','refresh');
			}else if($country_id!=''){
				redirect('state/editState/'.$country_id,'refresh');
			}else{
				redirect('state','refresh');
			}
		}else{
			redirect('state','refresh');
		}
	}

	public function getCountry()
	{
		$term = $this->input->get('term');
        $term = trim(strip_tags($term));
        $hotelsp = $this->Taxrate_Model->getCountry($term)->result();
        //echo "<pre>"; print_r($hotelsp); exit();
        foreach($hotelsp as $hotelp){
            $apts['label'] = $hotelp->country_name;
            $apts['value'] = $hotelp->country_name.'-'.$hotelp->country_id;
            $apts['id'] = $hotelp->country_id;
            $result[] = $apts; 
        }
        
        echo json_encode($result);
	}

	function formValidator($type){
	   
	   $this->form_validation->set_rules('statename', 'State Name', 'required');
	   if($type == 'add') {
	   	$this->form_validation->set_rules('statename', 'State Name', 'trim|required|min_length[3]|max_length[150]');
	    }
	   return $this->form_validation->run();
    }	
	

}
