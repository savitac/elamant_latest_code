<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
ob_start();
error_reporting(0);
class Supplier_list extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Agents_Model');		
		$this->load->model('Users_Model');
		$this->load->model('Product_Model');
		$this->load->model('Domain_Model');		
		$this->load->model('Usertype_Model');
		$this->load->model('Currency_Model');		
		$this->load->model('Email_Model');
		$this->load->model('Promo_Model');
		$this->load->model('Api_Model');
		$this->load->model('Booking_Model');
		$this->load->library('form_validation');
		$this->lang->load('english','Dynamic_Languages');
		$this->load->model('Supplier_list_Model');		
		
		$this->TravelLights = $this->lang->line('TravelLights');
	 $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
    
    
	function supplierList(){
		$user = $this->General_Model->getHomePageSettings();
		$user['user_list'] 	= $this->Supplier_list_Model->getAgentList();
		/*echo "<pre>";
		print_r($user['user_list']);
		exit("user");*/
		$user['promo'] = $this->Promo_Model->getPromoCodeList();
		$this->load->view('agents/supplier_list',$user);
	}
	
	function addsupplierList(){ 

		$users 		= $this->General_Model->getHomePageSettings();
		$users['country'] 		=  $this->General_Model->get_country_details();
		$users['domain_info'] 	=  $this->Supplier_list_Model->domainlist();
 		if(count($_POST) > 0){
			$form_validator = $this->formValidator('add');
			if($form_validator == FALSE) {
				
			  $this->load->view('agents/add_supplier',$users);
			} else{
			$email = $_POST['email_id'];
			$Query="select * from  user  where user_name ='".$email."' AND user_type='5' ";
			$query=$this->db->query($Query);
			if ($query->num_rows() > 0)
		{
			$data['status'] = '<div class="alert alert-block alert-danger">
							   <a href="#" data-dismiss="alert" class="close">×</a>
							   <h4 class="alert-heading">Already Email Id Registered!</h4>
							   kindly Use With Another Email Address !!!
							   </div>';
			$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$res = "";
			for ($i = 0; $i < 10; $i++) {
				$res .= $chars[mt_rand(0, strlen($chars)-1)];
			}
			
			$this->load->view('agents/supplier_list',$data);
		
		}else{
			$user_profile_name = $this->General_Model->upload_image($_FILES, 'agent');
			
			$this->Supplier_list_Model->addAgentDetails($_POST,$user_profile_name);
			$user['email_template'] = $this->Email_Model->get_email_template('add_user_active')->row();
			$this->Email_Model->sendAddUserRegistration($user,$_POST);
			redirect('supplier_list/supplierList'); 
		}
	}
		}else{
			$users['domain'] 	=  $this->Domain_Model->get_domain_list();
			$users['product'] 	=  $this->Product_Model->getProductList();
			$users['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$users['api']		=  $this->Api_Model->getApiList();
			$this->load->view('agents/add_supplier',$users);
		}
	}

	function check_unique_email_agent($email){
		if($email!=''){
			if(preg_match("/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $email)){
				$Status 	=	$this->Agents_Model->getAllAgentEmail($email);
				if($Status  == "YES") {
					echo '<span class="nosuccess validate-has-error" for="email_id" style="display:inline-block;">This Email Id already Exists, Please Choose diffrent Email Id</span>';exit;
				}else{
					echo '<span class="success" style="color: green;">valid</span>';exit;
				}
			}else{
				echo '<span class="nosuccess">Please Enter the Valid Email Id</span>';exit;
			}
		}else{	
			 echo '<span class="nosuccess">Please Enter the Valid Email Id</span>';exit;
		}
	}
	
	function activeUsers($user_id1){
		$user_id 	= json_decode(base64_decode($user_id1));
		$user = $this->Agents_Model->getAgentList($user_id);
		if($user_id != ''){
			$this->Agents_Model->activeAgents($user_id);
			
			$user['email_template'] = $this->Email_Model->get_email_template('add_user_active')->row();
			$this->Email_Model->sendAddUserActive($user);
		}
			redirect('agents/agentList','refresh');
	}
	
	function inactiveUsers($user_id1){
		$user_id 	= json_decode(base64_decode($user_id1));
		$user = $this->Agents_Model->getAgentList($user_id);
		if($user_id != ''){
			$this->Agents_Model->inactiveAgent($user_id);
			 
			$user['email_template'] = $this->Email_Model->get_email_template('user_deactivate')->row();
  	       $this->Email_Model->sendAddUserActive($user);
		}
			redirect('agents/agentList','refresh');
	}
	
	function delete_users($user_id1){
		$user_id 	= $user_id1; 
		if($user_id != ''){
			$this->Supplier_list_Model->delete_users($user_id);

		}
		redirect('supplier_list/supplierList','refresh');
	}
	
	function editUsers($user_id1)
	{
		$user_id 	= json_decode(base64_decode($user_id1));
		if($user_id != ''){			
			$users 				= $this->General_Model->getHomePageSettings();
			$users['domain'] 	=  $this->Domain_Model->get_domain_list();
			$users['product'] 	=  $this->Product_Model->getProductList();
			$users['country'] 	=  $this->General_Model->get_country_details();
			$users['user_type']	=  $this->Usertype_Model->get_user_type_list();
			$users['api']		=  $this->Api_Model->getApiList();
			$users['users'] 	= $this->Agents_Model->getAgentList($user_id);
			$this->load->view('agents/edit_agent',$users);
		}else{
			redirect('agents/agentList','refresh');
		}
	}
	
	function updateUsers($user_id1)
	{   
		$user_id 	= json_decode(base64_decode($user_id1));
		if($user_id != ''){
			if(count($_POST) > 0){
				/*$form_validator = $this->formValidator('edit');
				if($form_validator == FALSE) { 
					redirect('agents/editUsers',$user_id1);	
			    }else{*/
			    	
				$image_info_name = $this->General_Model->upload_image($_FILES, 'agent', $_REQUEST['old_image']);
				$this->Agents_Model->updateAgents($_POST,$user_id, $image_info_name);
				redirect('agents/agentList','refresh');
			//}
			}else{
				redirect('agents/agentList','refresh');
			}
		}else{
			redirect('agents/agentList','refresh');
		}
	}
	
	function depositList($user_id1){
		$user_id 	= json_decode(base64_decode($user_id1));
		
		if($user_id != ''){
			$users 				= $this->General_Model->getHomePageSettings();
			$users['user_list'] = $this->Agents_Model->getAgentList($user_id);
			$users['depsoite'] 	= $this->Agents_Model->getDepositList($user_id);
			
			$this->load->view('agents/agent_deposite',$users);
		}else{
			redirect('agents/agentList','refresh');
		}
	}

	function b2b2b_deposite_list($user_id1,$parent_id){
		$user_id 	= json_decode(base64_decode($user_id1));
		$parent_user = json_decode(base64_decode($parent_id));
		if($user_id != ''){
			$users 				= $this->General_Model->getHomePageSettings();
			$users['user_list'] = $this->Agents_Model->getAgentList($user_id);
			$users['depsoite'] 	= $this->Agents_Model->getDepositList($user_id);
			$users['parent_user'] = $parent_user;
			$this->load->view('agent_admin/b2b2b/b2b2b_deposite',$users);
		}else{
			redirect('agents/agentList','refresh');
		}
	}
	
	function addDeposit($user_id1){
		$user_id 	= json_decode(base64_decode($user_id1));
		$user = $this->Agents_Model->getAgentList($user_id); 
		if($user_id != ''){
			if(count($_POST) > 0){
				//echo '<pre>'; print_r($_POST['currencycode'];); exit();
				$currency_code = $_POST['currencycode'];
				$curreny =	$this->Agents_Model->currenyAmount($currency_code);
				$exchange = $curreny[0]->value;
				$this->Agents_Model->addDepositDetails($_POST,$exchange,$user_id);
				//$this->Email_Model->send_agent_deposit_mail($_POST,$user);
				redirect('agents/depositList/'.$user_id1,'refresh');
			}else{
				$users 				= $this->General_Model->getHomePageSettings();
				$users['user_id']	= $user_id;
				$users['currency'] = $this->Currency_Model->getCurrencyList();
				$this->load->view('agents/add_agent_deposite',$users);
			}
		}else{
			redirect('agents/depositList/'.$user_id1,'refresh');
		}
	}

	function add_b2b2b_deposite($user_id1){
		$user_id 	= json_decode(base64_decode($user_id1));
		$user = $this->Agents_Model->user_list($user_id); 
		$branch_id = $user['user_info'][0]->branch_id;
		if($user_id != ''){
			if(count($_POST) > 0){
				$this->Agents_Model->add_b2b2b_deposite_details($_POST,$user_id,$branch_id);
				$this->Email_Model->send_agent_deposit_mail($_POST,$user);
				redirect('agents/depositList/'.$user_id1,'refresh');
			}else{
				$users 				= $this->General_Model->getHomePageSettings();
				$users['user_id']	= $user_id;
				$users['currency'] = $this->Currency_Model->getCurrencyList(); //print_r($users['currency']); exit();
				$this->load->view('agent_admin/b2b2b/add_b2b2b_deposite',$users);
			}
		}else{
			redirect('agents/depositList/'.$user_id1,'refresh');
		}
	}
	
	function approveDeposit($user_id1,$deposit_id1,$apprv_amount){
		$user_id 		= json_decode(base64_decode($user_id1));
		$user = $this->Agents_Model->getAgentList($user_id);
		$deposit_id 	= json_decode(base64_decode($deposit_id1));
		$amount 		= json_decode(base64_decode($apprv_amount));
		 $deposit = $this->Agents_Model->getAgentDepsoiteList($deposit_id); 
		if($user_id != ''){
			$acceptedornot = $this->Agents_Model->getAgentDepsoiteList($deposit_id);
			//Currency Conversion Start
								$pdata = $acceptedornot[0]->currencycode;
                                $TotalPrice_Curr  = 'AUD';
                                $TotalPrice =  $acceptedornot[0]->amount_credit;
                                $amount = $this->General_Model->currency_convertor($TotalPrice,$pdata,$TotalPrice_Curr);
            //Currency Conversion End
			if($acceptedornot[0]->status != "Accepted"){
			$this->Agents_Model->approveDeposit($deposit_id);
			$deposit_check = $this->Agents_Model->getAgentDepositBalance($user_id);
			 if ($deposit_check == '') {
				  $this->Agents_Model->insertAgentBalance($user_id,$amount);
				}else{
					$balance_amount = $deposit_check->balance_credit;
					$update_balance_amount = $amount + $balance_amount;
					$this->Agents_Model->updateAgentBalance($user_id,$amount,$update_balance_amount);
				}
			
			$user['email_template'] = $this->Email_Model->get_email_template('agent_deposit_accepted_mail')->row();
			$this->Email_Model->sendAgentDepositMail($user,$deposit);
		}
		redirect('agents/depositList/'.$user_id1,'refresh');
		
	}
}

	function approvingDeposit($user_id1,$deposit_id1){
		$user_id 		= json_decode(base64_decode($user_id1));
		$user = $this->Agents_Model->getAgentListDeposit($user_id); //echo 'sanjay<pre>'; print_r($user['user_info'][0]->branch_id); exit();
		$deposit_id 	= json_decode(base64_decode($deposit_id1));
		$deposit_type = $_POST['deposit_type'];
		$amount 		= $_POST['amount'];
		$adminRemarks = $_POST['admin_remarks'];
		 $deposit = $this->Agents_Model->getAgentDepsoiteList($deposit_id);
		
		if($user_id != ''){
			$acceptedornot = $this->Agents_Model->getAgentDepsoiteList($deposit_id);
			if($acceptedornot[0]->status != "Accepted"){
				if ($deposit_type == "Accepted") {
					$this->Agents_Model->approveDeposit($deposit_id,$adminRemarks);
					       //Currency Conversion 
							 	$pdata = $acceptedornot[0]->currencycode;
                                $TotalPrice_Curr  = 'AUD';
                                $TotalPrice =  $acceptedornot[0]->amount_credit;
                            	$amount = $this->Currency_Model->currency_convertor($TotalPrice,$pdata,$TotalPrice_Curr);
					       //Currency Conversion End    
                            	
                            
					$deposit_check = $this->Agents_Model->getAgentDepositBalance($user_id);
					 if ($deposit_check == '') {
						  $this->Agents_Model->insertAgentBalance($user_id,$amount);
					}else{
						$balance_amount = $deposit_check->balance_credit;
						$update_balance_amount = $amount + $balance_amount;
						$this->Agents_Model->updateAgentBalance($user_id,$amount,$update_balance_amount);

					}
					
				
					$user['email_template'] = $this->Email_Model->get_email_template('agent_deposit_accepted_mail')->row();
					$this->Email_Model->sendAgentDepositMail($user,$deposit);
				}else{
					$this->Agents_Model->cancelDepoist($deposit_id,$adminRemarks);
					$user['email_template'] = $this->Email_Model->get_email_template('agent_deposit_cancel_mail')->row();
					$this->Email_Model->sendAgentDepositMail($user,$deposit);
				}
			
		}
		redirect('agents/depositList/'.$user_id1,'refresh');
		
	}
	}

	
	
	function cancelDepoist($user_id1,$deposit_id1){
		$user_id 		= json_decode(base64_decode($user_id1));
		$user = $this->Agents_Model->getAgentList($user_id);
		$deposit_id 	= json_decode(base64_decode($deposit_id1));
		$deposit = $this->Agents_Model->getAgentDepsoiteList($deposit_id);
		if($deposit_id != ''){
			$this->Agents_Model->cancelDepoist($deposit_id);
			$user['email_template'] = $this->Email_Model->get_email_template('agent_deposit_cancel_mail')->row();
			$this->Email_Model->sendAgentDepositMail($user,$deposit);
		}
		redirect('agents/depositList/'.$user_id1,'refresh');
	}

	function sendEmail($user_id1)
	{
		$user_id 	= json_decode(base64_decode($user_id1)); 
		if($user_id != ''){
			$user 				= $this->General_Model->getHomePageSettings();
			$user['user_list'] 	= $this->Agents_Model->getAgentList($user_id);
			$this->load->view('agents/agent_email',$user); 
		}else{
			redirect('agents/agentList','refresh'); exit();
		}
	}

	function sendMail() 
	{
		$data['firstname'] = $firstname = $this->input->post('firstname');
        $data['mailid']= $mailid = $this->input->post('mailid');
        $data['subject']= $subject = $this->input->post('subject');
        $data['description']=  $message = $this->input->post('description');
        $this->Email_Model->sendMailToUser($data);
        
        redirect('agents/agentList', 'refresh');
    }

    function sendAgentPromo($user_id1) {
    	$user_id 	= json_decode(base64_decode($user_id1));
    	$user = $this->General_Model->getHomePageSettings();
        $promo_id = $_POST['promoid']; 
        $user = $this->Agents_Model->getAgentList($user_id);
        $promo_value = $this->Promo_Model->getPromoCodeList($promo_id);
		$data['promo_code'] = $promo_value; 
        $data['exp_date'] = $promo_value[0]->exp_date;
        $data['emailid'] = $user['user_info'][0]->user_email; 
        $data['firstname'] = $user['user_info'][0]->user_name;
        $data['email_template'] = $this->Email_Model->get_email_template('user_promo_code')->row(); 
        $this->Email_Model->sendPromoToUser($data);
        redirect('agents/agentList', 'refresh');
    }

    function changeDeposit($user_id1,$deposit_id1){
    	$deposit    = $this->General_Model->getHomePageSettings();
    	$user_id 	= json_decode(base64_decode($user_id1));
    	$deposit_id 	= json_decode(base64_decode($deposit_id1));
    	$deposit['user_id'] = $user_id;
    	$deposit['deposit_id'] = $deposit_id;
    	$deposit['user_list'] = $this->Agents_Model->getAgentList($user_id);
		$deposit['depsoite'] 	= $this->Agents_Model->getAgentDepositList($user_id,$deposit_id);
    	
    	$this->load->view('agents/changeDeposit',$deposit);
    }

    function formValidator($type){
	   $this->form_validation->set_rules('first_name', 'First Name', 'required');
	   $this->form_validation->set_rules('last_name', 'Last Name', 'required');
	   $this->form_validation->set_rules('email_id', 'Email', 'required');
	   $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required');
	   $this->form_validation->set_rules('comapany_name', 'Company Name', 'required');
	   $this->form_validation->set_rules('address', 'Address', 'required');
	   $this->form_validation->set_rules('city', 'City', 'required');
	   $this->form_validation->set_rules('state_name', 'State Name', 'required');
	   $this->form_validation->set_rules('zip_code', 'Zip Code', 'required');
	   
	   
	   if($type == 'add') {
	   	$this->form_validation->set_rules('first_name', 'First name', 'required|min_length[1]|max_length[25]');
	    $this->form_validation->set_rules('last_name', 'Last name', 'required|min_length[1]|max_length[25]');
	    
	    $this->form_validation->set_rules('email_id', 'Email', 'required|valid_email');
/*	    $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('comapany_name', 'Company Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('address', 'Address', 'required|min_length[1]|max_length[100]');
	    $this->form_validation->set_rules('city', 'City', 'required|min_length[1]|max_length[15]');
	    $this->form_validation->set_rules('state_name', 'State Name', 'required|min_length[1]|max_length[50]');
	    $this->form_validation->set_rules('zip_code', 'Zip Code', 'numeric|xss_clean');*/
	    
	    }

	    if($type == 'edit') {
	   	
	    $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required|min_length[3]|max_length[20]');
	    $this->form_validation->set_rules('comapany_name', 'Company Name', 'required|min_length[3]|max_length[50]');
	    $this->form_validation->set_rules('address', 'Address', 'required|min_length[3]|max_length[100]');
	    $this->form_validation->set_rules('city', 'City', 'required|min_length[1]|max_length[15]');
	    $this->form_validation->set_rules('state_name', 'State Name', 'required|min_length[1]|max_length[50]');
	    $this->form_validation->set_rules('zip_code', 'Zip Code', 'numeric|xss_clean');
	    
	    }
	    
	   return $this->form_validation->run();
    }	
    
    function getAgentPrivileges($user_id1, $user_type1){
		$user_id = json_decode(base64_decode($user_id1));
		$user_type = json_decode(base64_decode($user_type1));
		$user 				= $this->General_Model->getHomePageSettings();
		$user['agent_id']	= $user_id1;
		$user['user_type']	= $user_type1;
		if($user_id != '' && $user_type != ''){
	    $user['agent_list'] 	= $this->Agents_Model->getAgentModules();
	    $this->load->view('agents/privilege_info',$user); 
	    }else{
		redirect('agents/agentList');
		}
	}
	
	function update_user_privileges($user_id1, $user_type1){
		$user_id = json_decode(base64_decode($user_id1));
		$user_type = json_decode(base64_decode($user_type1));
		if(count($_POST) > 0) 
		{
			$this->Agents_Model->update_privileges($_POST, $user_id, $user_type);
		}
		redirect('agents/getAgentPrivileges/'.$user_id1);
	}

    function getProductPrivileges($user_id1){
		$user_id = json_decode(base64_decode($user_id1));
		$user 				= $this->General_Model->getHomePageSettings();
		$user['agent_id']	= $user_id1;
	    $user['product_list'] 	= $this->Agents_Model->getProducts();
	    $this->load->view('agents/productprivilege_info',$user); 
	}
    
    function update_product_privileges($user_id1){
		$user_id = json_decode(base64_decode($user_id1));
		if(count($_POST) > 0) 
		{
			$this->Agents_Model->update_product_privileges($_POST, $user_id);
		}
		redirect('agents/getProductPrivileges/'.$user_id1);
	}
	function hotelOrders($user_id1) {
		$user_id = json_decode(base64_decode($user_id1));
    	$data = $this->General_Model->getHomePageSettings();
        $data['user_id'] = $user_id;
        $data['orders'] = $this->Booking_Model->get_booking_details($user_id);	
       
        $this->load->view('bookings/userBookings', $data);
   
}
	 function transferOrders($user_id1) {
	 	$user_id = json_decode(base64_decode($user_id1));
    	$data = $this->General_Model->getHomePageSettings();
        $data['orders'] = $this->Booking_Model->get_transfer_order_details($user_id);
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = $user_id;
			
        $this->load->view('bookings/transferBookings', $data);
    
}
  
  function getCurrentBalance($user_id1){
		  $user_id = json_decode(base64_decode($user_id1));
		  $data = $this->General_Model->getHomePageSettings();
		  $data['currenct_balance'] = $this->Agents_Model->get_currenct_balance($user_id)->row();
		  $data['balance_history'] = $this->Agents_Model->get_booking_balance_history($user_id)->result();
		  $this->load->view('agents/balance_history', $data);
   }
}
 