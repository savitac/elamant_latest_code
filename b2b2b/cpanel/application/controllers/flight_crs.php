<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL); 
ini_set('max_execution_time', 300);
/**
 *
 * @package    Provab
 * @subpackage Flight RS
 * @author     Priyanka <@gmail.com>
 * @version    V1
 */

class Flight_Crs extends CI_Controller {


	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('City_Model');
		$this->load->model('Taxrate_Model');
		$this->load->library('form_validation');
		$this->load->model('flight_crs_model');	
		if(!isset($_SESSION['ses_id'])){
			$sec_res 			= session_id();
	    	$_SESSION['ses_id'] = $sec_res;
		}
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
	    $this->checkAdminLogin();
		
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
	
	

	function supplier_crs_list()
	{

		$flight_crs = array ();
		$flight_group_crs_list = $this->flight_crs_model->supplier_crs_list();

		$page_data = $flight_group_crs_list;
		$flight_crs ['page_data'] = $page_data;
		//var_dump($flight_crs ['page_data']); 
		$this->load->view('flight/supplier_crs_list', $flight_crs);
		//$this->load->view('flight/supplier_crs_list', $flight_crs);
		//$this->load->view('flight/flight_crs', $page_data);
		
	}

	

	function ariline_list()
	{
		$flight_crs = array ();
		$flight_ariline_list =  $this->flight_crs_model->ariline_list ();
		//debug($flight_ariline_list);
		$page_data = $flight_ariline_list;
		$ariline_list ['page_data'] = $page_data;
		////debug($ariline_list ['page_data']); exit;
		$this->load->view('flight/ariline_list', $ariline_list);
		//$this->load->view('flight/flight_crs', $page_data);
		
	}
	function add_ariline_form()
	{
		$this->load->view('flight/add_ariline');
	}

	function add_ariline_list() {
		$this->load->model ('custom_db' );
		$post_data = $this->input->post ();
			$request ['crs_name'] = trim ( $post_data ['name'] );
			$request ['crs_code'] = trim ( $post_data ['code'] );

			$name = $post_data ['code'];

			$type = ".gif";
			$request ['crs_path'] =  $name.''.$type;

                        $path=$_FILES['crs_path']['name']; 
				$type_b		=pathinfo($path,PATHINFO_EXTENSION);
				$img_name	= $name.''.$type;
		       	        if($path== '')
				{
				 echo $big_image 	=	"";
				}
				else
				{
				  $big_image="../../extras/custom/images/crs_airline_logo/".$img_name;      
				   copy($_FILES['crs_path']['tmp_name'],$big_image );
				}
					$config['upload_path'] = $big_image; 
					$config['allowed_types'] = '*';
					$config['file_name'] = $name;
					$config['max_size'] = '1000000';
					$config['max_width']  = '';
					$config['max_height']  = '';
					$config['remove_spaces']  = false;

			$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('crs_path')) {
						//echo $this->upload->display_errors();
					} else {
						$image_data =  $this->upload->data();
					}

			$insert_id = $this->custom_db->insert_record ( 'crs_airline_list', $request );

			redirect ( base_url () . 'index.php/flight_crs/ariline_list' );

		
		
	}

	function delete_ariline_list($orgin_id)
	{ 
		$this->flight_crs_model->delete_ariline($orgin_id);  
		redirect ( base_url () . 'index.php/flight_crs/ariline_list');
		
		
	}


	function alot_supplier_ariline() {
		$this->load->model ( 'custom_db' );
		$post_data = $this->input->post();  
		if ($post_data) {

	        $request ['ariline_id'] = trim ( $post_data ['ariline_id'] );
			$request ['suppier_id'] = trim(implode(",", $post_data ['supplier_id']));  
			$insert_id = $this->custom_db->insert_record ( 'crs_ariline_supplier', $request );
			redirect ( 'flight_crs/supplier_crs_list' );
		}  
		$supplier_list = $this->flight_crs_model->supplier_list();
		$flight_ariline_list =  $this->flight_crs_model->ariline_list ();
		$page_data = $supplier_list;
		$supplier_list['page_data'] = $page_data;
		$supplier_list['ariline_data'] = $flight_ariline_list; 
		$this->load->view('flight/alot_supplier_ariline',$supplier_list);
		
	}

	function view_flight($supplier_id)
	{

		$flight_crs = array ();
		$flight_group_crs_list = $this->flight_crs_model->flight_crs_request_details ($supplier_id);
		$page_data = $flight_group_crs_list;
		$flight_crs ['page_data'] = $page_data;
		//debug($flight_crs ['page_data']); exit;
		$this->load->view('flight/flight_crs_list', $flight_crs);
		//$this->load->view('flight/flight_crs', $page_data);
		
	}


	

	function add_flight_list() { 
		$this->load->model ( 'custom_db' );
		$this->load->library('session');
		$post_data = $this->input->post (); 
		$uid=intval(@$GLOBALS['CI']->entity_user_id);
		//$request ['domain_id']=$GLOBALS['CI']->entity_domain_id;
		
		if ($post_data) {
			$request ['flight_trip'] = trim ( $post_data ['trip_type'] );
			$request ['ariline_code'] = trim ( $post_data ['airline_name'] );
			$request ['from_city'] = trim ( $post_data ['from_city'] );
			$request ['to_city'] = trim ( $post_data ['to_city'] );
			
			$from_date =$this->convert_date_format("sql",trim ( @$post_data ['jourany_date'] ));
			$request ['jourany_date'] =trim ( @$post_data ['jourany_date'] );
			$request ['retuen_date'] = trim ( @$post_data ['retuen_date'] );

			$request ['no_of_seats'] = trim ( @$post_data ['no_of_seats'] );
			$request ['left_seats'] = trim ( @$post_data ['no_of_seats'] );
			$request ['class'] = trim ( @$post_data ['class'] );
			$request ['supplier_id'] =$uid;
			$request ['created_by'] =$uid;
			$request ['status'] = '0';
			$request ['till_date'] = $this->convert_date_format("sql",trim ( @$post_data ['till_date'] ));
			//dummy added
			$request ['pnr']=substr(md5(microtime()*rand(0,99)),0,6);
			$days=$post_data ['no_days'];
			if($request ['flight_trip']=="circle"){
				 $create_filght=2;
				
			}else{
				 $create_filght=1;

			}
				
			foreach ( $days as $key=> $day) {
						// get array from the function 
					 $insert_dates=$this->get_dates($from_date,$request ['till_date'],$day);
					 $get_size=sizeof($insert_dates);// count innner array element 
					 for($i=0;$i<$get_size;$i++){
					 	$single_dates[]=$this->convert_date_format("sql",$insert_dates[$i]);// make a single element
					 }
					 //$c['dates']= $insert_dates + $insert_dates;
			}
			$single_dates_size=sizeof($single_dates);
			//debug($single_dates_size);die(" 1564"); 
			// for loop help to create multiple*2 if circel else oneway 1
			/*for($create_filght_no=0;$create_filght_no<$create_filght;$create_filght_no++){ */
				for($date=0;$date<$single_dates_size;$date++){
					$request ['jourany_date']=$single_dates[$date];
					if($request ['flight_trip']=='circel'){
						$request['retuen_date']=$single_dates[$date];
					}
					$insert_id = $this->custom_db->insert_record ( 'flight_crs_list', $request );
					$inserted_ids[]=$insert_id['insert_id'];
				/*}*/
			}
			$this->session->set_userdata('inserted_keys',$inserted_ids);
			$count_session_ids=count($this->session->userdata('inserted_keys'));
			if($count_session_ids>0){
				redirect ( base_url () . 'index.php/flight_crs/add_flight_segement/'.$request ['flight_trip'] );
			}else{
				redirect ( base_url () . 'index.php/flight_crs/add_flight_list/');
			}

			//redirect ( base_url () . 'index.php/flight_crs/add_flight_segement/'.$insert_id['insert_id'].'/'.$request ['flight_trip'] );

			//redirect ( base_url () . 'index.php/flight_crs/flight_crs_list' );
		}

		$uid=intval(@$GLOBALS['CI']->entity_user_id);
		$flight_ariline_list =   $this->flight_crs_model->supplier_crs_list($uid);
		$page_data = $flight_ariline_list;
		$ariline_list ['page_data'] = $page_data;
		$this->load->view('flight/addflight_crs_list',$ariline_list);
		
	} 

	function flight_crs_list()
	{
        
        $uid=intval(@$GLOBALS['CI']->entity_user_id);
        $ariline_list = $this->flight_crs_model->supplier_crs_list($uid);
		$flight_crs = array ();
		//$today_date=$this->input->get('today');
		$today_date=$this->input->get('today');
		if(!empty($today_date)){
			$today_date=1;
		}
		$flight_group_crs_list = $this->flight_crs_model->flight_crs_request_details ($uid,$today_date);
		$page_data = $flight_group_crs_list;
		$flight_crs ['page_data'] = $page_data;
		$flight_crs ['ariline_data'] = $ariline_list;
		$flight_crs['display']=$this->input->get('show');
		
		//$flight_crs['display']=$this->input->get('show');
		//debug($flight_crs ['jourany_date']);
		//exit("flight_crs");

		$this->load->view('flight/flight_crs_list', $flight_crs);
		//$this->template->view('flight/flight_crs', $page_data);
		
	} 

	function alot_supplier($id,$trip_type) {
		$this->load->model ( 'custom_db' );
		$post_data = $this->input->post ();
		if (valid_array ( $post_data )) {

	      // debug($post_data); exit;
	        $orgin_id = $id;
	        $request ['alotted'] = trim ( $post_data ['alotted'] );
			$request ['supplier_id'] = trim ( $post_data ['supplier_id'] );
			
			
			$this->custom_db->update_record ( 'flight_crs_list', $request, array (
					'orgin' => $orgin_id ) );
			redirect ( base_url () . 'index.php/flight_crs/flight_crs_list' );
		}
        //echo $id;
		$supplier_list = $this->flight_crs_model->supplier_list();
		$page_data = $supplier_list;
		//debug($page_data); exit;
		$supplier_list['page_data'] = $page_data;
		$this->load->view('flight/alot_supplier',$supplier_list);
		
	}


	function edit_flight_list($id) {
		$this->load->model ( 'custom_db' );
		$post_data = $this->input->post ();
		if (valid_array ( $post_data )) {

	       
	        $orgin_id = $id;
	        $request ['ariline_code'] = trim ( $post_data ['airline_name'] );
			$request ['flight_trip'] = trim ( $post_data ['trip_type'] );
			$request ['from_city'] = trim ( $post_data ['from_city'] );
			$request ['to_city'] = trim ( $post_data ['to_city'] );
			$request ['jourany_date'] = trim ( @$post_data ['jourany_date'] );
			$request ['retuen_date'] = trim ( @$post_data ['retuen_date'] );
			
			$request ['no_of_seats'] = trim ( @$post_data ['no_of_seats'] );
			$request ['class'] = trim ( @$post_data ['class'] );
			
			$request ['status'] = trim ( @$post_data ['class'] );
			
			$this->custom_db->update_record ( 'flight_crs_list', $request, array (
					'orgin' => $orgin_id ) );
			redirect ( base_url () . 'index.php/flight_crs/flight_crs_list' );
		}

		$flight_crs_list = $this->flight_crs_model->flight_crs_edit_details ($id);
		$page_data = $flight_crs_list;
		//debug($page_data); exit;
		$flight_crs['page_data'] = $page_data;
		$this->load->view('flight/editflight_crs_list',$flight_crs);
		
	}

	function update_status_flight_list($id,$status) {
		
           if($status==0)
           {
           	  $request ['status'] = 1;
           }
           if($status==1)
           {
           	  $request ['status'] = 0;
           }
	       
	        $orgin_id = $id;
					
			$this->custom_db->update_record ( 'flight_crs_list', $request, array (
					'orgin' => $orgin_id ) );
			redirect ( base_url () . 'index.php/flight_crs/flight_crs_list' );
		

		
		
	}

	function view_flight_details($id,$trip_type)
	{
        //$uid=intval(@$GLOBALS['CI']->entity_user_id);
		$flight_crs = array ();
		$flight_group_crs_list = $this->flight_crs_model->flight_crs_view_details ($id,$trip_type);
		$page_data = $flight_group_crs_list;
		$flight_crs ['page_data'] = $page_data;
		//debug($flight_crs ['page_data']);
		$this->load->view('flight/view_flight_crs_list', $flight_crs);
		//$this->load->view('flight/flight_crs', $page_data);
		
	}


	function flight_segement_list($id,$trip_type,$supplier)
	{

		$flight_crs = array ();
		$flight_segment_crs_list = $this->flight_crs_model->flight_segment_request_details ($id,$trip_type);
		$page_data = $flight_segment_crs_list;
		$flight_segement ['page_data'] = $page_data;
		$this->load->view('flight/flight_segement_list', $flight_segement);
		
		
	}

	function add_flight_segement($trip_type) {
		
		//function add_flight_segement($id,$trip_type) {
		$this->load->model ( 'custom_db' );
		$inserted_keys=$this->session->userdata('inserted_keys');
		
		$post_data = $this->input->post ();
		if ($post_data) {
			//$request ['flight_crs_no'] = $id;

			$request ['onwards_flight_name'] = trim ( $post_data ['onwards_flight_name'] );
			$request ['onwards_flight_number'] = trim ( $post_data ['onwards_flight_number'] );
			$request ['onwards_flight_equipment'] = trim ( $post_data ['onwards_flight_equipment'] );
			$request ['onwards_from_city'] = trim ( @$post_data ['onwards_from_city'] );
			$request ['onwards_to_city'] = trim ( @$post_data ['onwards_to_city'] );
			$request ['onwards_departure_time'] = trim ( $post_data ['onwards_departure_time'] );
			$request ['onwards_arrival_at'] = trim ( $post_data ['onwards_arrival_at'] );
			$request ['onwards_arrival_time'] = trim ( $post_data ['onwards_arrival_time'] );
			$request ['onwards_duration'] = trim ( $post_data ['onwards_duration'] );
			$request ['retuen_flight_name'] = trim ( @$post_data ['retuen_flight_name'] );
			$request ['return_flight_number'] = trim ( @$post_data ['return_flight_number'] );
			$request ['return_flight_equipment'] = trim ( @$post_data ['return_flight_equipment'] );
			$request ['return_from_city'] = trim ( @$post_data ['return_from_city'] );
			$request ['return_to_city'] = trim ( $post_data ['return_to_city'] );
			$request ['return_departure_time'] = trim ( @$post_data ['return_departure_time'] );
			$request ['return_arrival_at'] = trim ( @$post_data ['return_arrival_at'] );
			$request ['return_arrival_time'] = trim ( @$post_data ['return_arrival_time'] );
			$request ['return_duration'] = trim ( $post_data ['return_duration'] );
			
			foreach ($inserted_keys as  $value) {
			$request ['flight_crs_no'] = $value;
				$insert_id = $this->custom_db->insert_record ( 'flight_segement_list', $request );
			}
			redirect ( base_url () . 'index.php/flight_crs/add_flight_price/'.$trip_type );
			//redirect ( base_url () . 'index.php/flight_crs/add_flight_price/'.$id.'/'.$trip_type );

		}
		$uid=intval(@$GLOBALS['CI']->entity_user_id);
		$id=$inserted_keys[0];
		$flight_group_crs_list = $this->flight_crs_model->flight_crs_edit_request_details ($uid,$id);
		$page_data = $flight_group_crs_list;
		$flight_crs ['page_data'] = $page_data;
		//debug($flight_crs);exit("hello");
		$this->load->view('flight/addflight_segement_list',$flight_crs);
		
	}

	function flight_price_list($id)
	{

		$flight_crs = array ();
		$flight_price_crs_list = $this->flight_crs_model->flight_price_request_details ($id);
		$page_data = $flight_price_crs_list;
		$flight_price ['page_data'] = $page_data;
		$this->load->view('flight/flight_price_list', $flight_price);
		
		
	}

	function add_flight_price($id) {
		$this->load->model ( 'custom_db' );
		$post_data = $this->input->post ();
		if (valid_array ( $post_data )) {

	        $request ['flight_crs_no'] = $id;
			$request ['adult_price'] = trim ( $post_data ['adult_price'] );
			$request ['adult_tax'] = trim ( $post_data ['adult_tax'] );
			$request ['adult_markup'] = trim ( $post_data ['adult_markup'] );
			$request ['child_price'] = trim ( $post_data ['child_price'] );
			$request ['child_tax'] = trim ( @$post_data ['child_tax'] );
			$request ['child_markup'] = trim ( $post_data ['child_markup'] );
			$request ['infant_price'] = trim ( @$post_data ['infant_price'] );
			$request ['infant_tax'] = trim ( $post_data ['infant_tax'] );
			$request ['infant_markup'] = trim ( $post_data ['infant_markup'] );
			$request ['fare_rules'] = trim ( $post_data ['fare_rules'] );
			
			
			
			
			$insert_id = $this->custom_db->insert_record ( 'flight_crs_price', $request );
			redirect ( base_url () . 'index.php/flight_crs/flight_price_list/'.$id);
		}
		$this->load->view('flight/addflight_price_list');
		
	}

	function edit_flight_price($orgin_id,$id) {
		$this->load->model ( 'custom_db' );
		$post_data = $this->input->post ();
		if (valid_array ( $post_data )) {

	       
	        $orgin_id = trim ( $post_data ['orgin'] );
			$request ['adult_price'] = trim ( $post_data ['adult_price'] );
			$request ['adult_tax'] = trim ( $post_data ['adult_tax'] );
			$request ['adult_markup'] = trim ( $post_data ['adult_markup'] );
			$request ['child_price'] = trim ( $post_data ['child_price'] );
			$request ['child_tax'] = trim ( @$post_data ['child_tax'] );
			$request ['child_markup'] = trim ( $post_data ['child_markup'] );
			$request ['infant_price'] = trim ( @$post_data ['infant_price'] );
			$request ['infant_tax'] = trim ( $post_data ['infant_tax'] );
			$request ['infant_markup'] = trim ( $post_data ['infant_markup'] );
			$request ['fare_rules'] = trim ( $post_data ['fare_rules'] );
			
			$this->custom_db->update_record ( 'flight_crs_price', $request, array (
					'orgin' => $orgin_id,'flight_crs_no'=>$id ) );
			redirect ( base_url () . 'index.php/flight_crs/flight_price_list/'.$id);
		}

		$flight_price_crs_list = $this->flight_crs_model->flight_price_edit_details ($orgin_id);
		$page_data = $flight_price_crs_list;
		$flight_price ['page_data'] = $page_data;
		$this->load->view('flight/editflight_price_list',$flight_price);
		
	}

	function delete_flight_price_list($orgin_id,$id)
	{

		$this->load->model ( 'custom_db' );
		$where = array('orgin' => $orgin_id); 
		$this->custom_db->delete_record ( 'flight_crs_price', $where );
		redirect ( base_url () . 'index.php/flight_crs/flight_price_list/'.$id);
		
		
	}

	function crs_city_list()
	{

		$flight_crs = array ();
		$flight_ariline_list =  $this->flight_crs_model->crs_city_list();
		//debug($flight_ariline_list);
		$page_data = $flight_ariline_list;
		$crs_city_list ['page_data'] = $page_data;
		////debug($ariline_list ['page_data']); exit;
		$this->load->view('flight/crs_city_list', $crs_city_list);
		//$this->load->view('flight/flight_crs', $page_data);
		
	}

	function add_crs_city_list() {
		$this->load->model ( 'custom_db' );
		$post_data = $this->input->post ();

		if ($post_data) {
			$request ['airport_code'] = trim ( $post_data ['airport_code'] );
			$request ['airport_name'] = trim ( $post_data ['airport_name'] );
			$request ['airport_city'] = trim ( $post_data ['airport_city'] );
			$request ['country'] = trim ( $post_data ['country'] );
			$request ['top_destination'] = '0';
			
			$insert_id = $this->custom_db->insert_record ( 'crs_city_list', $request );
			redirect ( base_url () . 'index.php/flight_crs/crs_city_list' );
			
		}
		$this->load->view('flight/add_crs_city_list');
		
	}

	function delete_crs_city_list($orgin_id)
	{

		$this->flight_crs_model->delete_crs_city($orgin_id);   
		redirect ( base_url () . 'index.php/flight_crs/crs_city_list' );
		
		
	}
	// added by ajaz convert sql format
	function convert_date_format($format,$date){
		$arr = explode('-', $date);
		if($format=="sql"){
		return $newDate = $arr[2].'-'.$arr[1].'-'.$arr[0];
		}else{
			return $newDate = $arr[0].'-'.$arr[1].'-'.$arr[0];
		//return $newDate = $arr[0].'-'.$arr[1].'-'.$arr[2];
			
		}
	} 
	function get_dates($start, $end, $weekday = 0){
  		$weekdays="Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday";
  		$arr_weekdays=explode(",", $weekdays);
  		$weekday = $arr_weekdays[$weekday];
  		if(!$weekday)
  		  die("Invalid Weekday!");
  		$start= strtotime("+0 day", strtotime($start) );
  		$end= strtotime($end);
  		$dateArr = array();
  		$selected_date = strtotime($weekday, $start);
 		// $friday = strtotime($weekday, $start);
 	 	while($selected_date <= $end) {
   		 $dateArr[] = date("Y-m-d", $selected_date);
   		 $selected_date = strtotime("+1 weeks", $selected_date);
  		}
 		 $dateArr[] = date("Y-m-d", $selected_date);
 		array_pop($dateArr);
 		return $dateArr;
	}
 

	
	
}