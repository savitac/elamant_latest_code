<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['master_module_list']	= array(
META_AIRLINE_COURSE => 'flight',
META_TRANSFERS_COURSE => 'transfer',
META_ACCOMODATION_COURSE => 'hotel',
META_BUS_COURSE => 'bus',
META_PACKAGE_COURSE => 'package',
META_SIGHTSEEING_COURSE => 'activities'
);
/******** Current Module ********/
/*$CI = & get_instance();
$CI->load->library('session');   
$user_type=$CI->session->userdata('user_type');
if($user_type== Agent)
{
$config['current_module'] = 'b2b';
}
elseif ($user_type== Sub_Agent) {
	$config['current_module'] = 'b2b2b';
}
else
{
	$config['current_module'] = 'b2c';
}*/
$config['current_module'] = 'admin';
$config['load_minified'] = false;

$config['verify_domain_balance'] = false;

/******** PAYMENT GATEWAY START ********/
//To enable/disable PG
$config['enable_payment_gateway'] = false;
$config['active_payment_gateway'] = 'PAYU';//EBS
$config['active_payment_system'] = 'test';//test/live
$config['payment_gateway_currency'] = 'INR';//INR
/******** PAYMENT GATEWAY END ********/


/******** BOOKING ENGINE START ********/
$config['flight_engine_system'] = 'test'; //test/live
$config['hotel_engine_system'] = 'test'; //test/live
$config['bus_engine_system'] = 'test'; //test/live
$config['external_service_system'] = 'test'; //test/live   
$config['transfer_engine_system'] = 'test'; //test/live
$config['sightseeing_engine_system'] = 'test';

$config['domain_key'] = CURRENT_DOMAIN_KEY;
$config['test_username'] = 'test229267';
$config['test_password'] = 'test@229';

//Test Url 
$config['flight_url'] = 'http://test.services.travelomatix.com/webservices/index.php/flight/service/';
/*$config['hotel_url'] = 'http://test.services.travelomatix.com/webservices/index.php/hotel_server/';*/
$config['hotel_url'] = 'http://test.services.travelomatix.com/webservices/index.php/hotel_v3/service/'; 
 
$config['bus_url'] = 'http://test.services.travelomatix.com/webservices/index.php/bus/service/';

$config['sightseeing_url'] =  'http://test.services.travelomatix.com/webservices/index.php/sightseeing/service/';

$config['transferv1_url'] = 'http://test.services.travelomatix.com/webservices/index.php/transferv1/service/';

 
//Live Url
/*$config['flight_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/flight/service/';
$config['hotel_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/hotel_server/';
$config['bus_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/bus_server/';  
*/
if ($config['external_service_system'] == 'live') {
	$config['external_service'] = 'http://prod.services.travelomatix.com/webservices/index.php/rest/';
} else {
	$config['external_service'] = 'http://test.services.travelomatix.com/webservices/index.php/rest/';

	//$config['external_service'] = 'http://192.168.0.63/provab/webservices/index.php/rest/';
}

// $config['live_username'] = 'TMX110848';
// $config['live_password'] = 'TMX@617110';
/******** BOOKING ENGINE END ********/

// Test Credentials TRAVELPORT API uAPI CREDENTIALS
$config ['travelport_flight_test'] = array (
		//'api_url' => 'https://emea.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/Service',
		'api_url' => 'https://apac.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/AirService', 
		//'api_url' => 'https://americas.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/AirService',
		'username' => 'uAPI5971857509-a673c0a1',
		'password' => '5Ys-$7XfK&',
		'target_branch' => 'P7083404',
		'myserviceprovider' => '41',
		'api_cancellation_url' => ''
);
$config ['sabre_flight_live'] = array (
		'api_url' => 'https://sws3-crt.cert.sabre.com/websvc',
		'username' => '895623', 
		'password' => 'WS336214', 
		'ipcc' => 'XP2I', 
		'sabre_email' => 'asad@boost-intl.com'
);

/*$config['hotelgta_live'] = array(
	'api_details_id' => 1,
	'product_id' => 1,
	'api_name' => 'GTA',
	'api_alternative_name' => 'bhjdfk',
	'api_logo' => '',
	'api_username' => 'XML.PROVAB@BOOKURFLIGHT.COM',
	'api_username1' => '1444',
	'api_url' => 'https://rbs.gta-travel.com/rbsusapi/RequestListenerServlet',
	'api_url1' => 'https://rbs.gta-travel.com/rbsusapi/RequestListenerServlet',
	'api_password' => 'BOOST@123@',
	'api_credential_type' => 'LIVE',
	'api_status' => 'ACTIVE',
	'responseCallbackURL' => '192.168.0.153/b2b2b/hotel/lost'
);
$config['hotelgta_test'] = array(	
	'api_details_id' => 1,
	'product_id' => 1,
	'api_name' => 'GTA',
	'api_alternative_name' => 'bhjdfk',
	'api_logo' => '',
	'api_username' => 'XML.PROVAB@AKAYTURIZM.COM',
	'api_username1' => '2000',
	'api_url' => 'https://interface.demo.gta-travel.com/wbsapi/RequestListenerServlet',
	'api_url1' => 'https://interface.demo.gta-travel.com/wbsapi/RequestListenerServlet',
	'api_password' => 'PASS',
	'api_credential_type' => 'TEST',
	'api_status' => 'ACTIVE',
	'responseCallbackURL' => '192.168.0.153/b2b2b/hotel/lost'	
);*/
// $config['hotelgta_live'] = array(
// 	'api_details_id' => 1,
// 	'product_id' => 1,
// 	'api_name' => 'GTA',
// 	'api_alternative_name' => 'bhjdfk',
// 	'api_logo' => '',
// 	'api_username' => 'XML.PROVAB@AIRLINERS.COM',
// 	'api_username1' => '443',
// 	'api_url' => 'https://rbs.gta-travel.com/rbsusapi/RequestListenerServlet',
// 	'api_url1' => 'https://rbs.gta-travel.com/rbsusapi/RequestListenerServlet',
// 	'api_password' => 'PROVAB12!#',
// 	'api_credential_type' => 'LIVE',
// 	'api_status' => 'ACTIVE',
// 	'responseCallbackURL' => '192.168.0.63/airliners/hotel/lost'
// );
// $config['hotelgta_test'] = array(	
// 	'api_details_id' => 1,
// 	'product_id' => 1,
// 	'api_name' => 'GTA',
// 	'api_alternative_name' => 'bhjdfk',
// 	'api_logo' => '',
// 	'api_username' => 'XML.PROVAB@AKAYTURIZM.COM',
// 	'api_username1' => '2000',
// 	'api_url' => 'https://interface.demo.gta-travel.com/wbsapi/RequestListenerServlet',
// 	'api_url1' => 'https://interface.demo.gta-travel.com/wbsapi/RequestListenerServlet',
// 	'api_password' => 'PASS',
// 	'api_credential_type' => 'TEST',
// 	'api_status' => 'ACTIVE',
// 	'responseCallbackURL' => '192.168.0.63/airliners/hotel/lost'	
// ); 

/**   
 * 
 * Enable/Disable caching for search result
 */
$config['cache_hotel_search'] = true;
$config['cache_flight_search'] = true;
$config['cache_bus_search'] = true;


/**
 * Number of seconds results should be cached in the system
 */
$config['cache_hotel_search_ttl'] = 300;
$config['cache_flight_search_ttl'] = 3000;
$config['cache_bus_search_ttl'] = 300;


/*$config['lazy_load_hotel_search'] = true;*/
$config['hotel_per_page_limit'] = 20;
$config['sightseeing_page_limit'] = 50;
