<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
        define('minNameLength', 1);
    	define('maxNameLength', 50);
    	define('minEmailLength',10);
    	define('maxEmailLength', 250);
    	define('mobileNumberLength',10);
    	define('minLandlineNumberLength',8);
    	define('maxLandlineNumberLength',12);
    	define('minAddressLength',5);
    	define('maxAddressLength',200);
    	define('minCityLength',5);
    	define('maxCityLength',200);
    	define('minAccountNumber',6);
    	define('maxAccountNumber',25);



/* End of file constants.php */
/* Location: ./application/config/constants.php */
