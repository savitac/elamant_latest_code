<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['master_module_list']	= array(
	META_AIRLINE_COURSE => 'flight',
	META_TRANSFERS_COURSE => 'transfer',
	META_ACCOMODATION_COURSE => 'hotel',
	META_BUS_COURSE => 'bus',
	META_PACKAGE_COURSE => 'package'
);
/******** Current Module ********/
$config['current_module'] = 'admin';

/******** BOOKING ENGINE START ********/
$config['flight_engine_system'] = 'test'; //test/live
$config['hotel_engine_system'] = 'test'; //test/live
$config['bus_engine_system'] = 'test'; //test/live
$config['external_service_system'] = 'test'; //test/live

$config['domain_key'] = CURRENT_DOMAIN_KEY;
$config['test_username'] = 'test';
$config['test_password'] = 'password';


//Production URL
$config['flight_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/flight/service/';
$config['hotel_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/hotel_server/';
$config['bus_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/bus_server/';



if ($config['external_service_system'] == 'live') {
		$config['external_service'] = 'http://prod.services.travelomatix.com/webservices/index.php/rest/';
} else {
	$config['external_service'] = 'http://test.services.travelomatix.com/webservices/index.php/rest/';
	//$config['external_service'] = 'http://test.services.travelomatix.com/webservices/index.php/rest/';
}


$config['services_url'] = '';

$config['live_username'] = 'TMX110848';
$config['live_password'] = 'TMX@617110';
/******** BOOKING ENGINE END ********/
