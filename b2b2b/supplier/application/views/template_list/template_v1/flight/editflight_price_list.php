<?php
$flight_datepicker = array(array('flight_datepicker1', FUTURE_DATE), array('flight_datepicker2', FUTURE_DATE));
$this->current_page->set_datepicker($flight_datepicker);
$this->current_page->auto_adjust_datepicker(array(array('flight_datepicker1', 'flight_datepicker2')));
//$airline_list = $GLOBALS['CI']->db_cache_api->get_airline_code_list();
?>
<div id="Package" class="bodyContent col-md-12">
	<div class="panel panel-default">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
					<li role="presentation" class="active" id="add_package_li"><a
						href="#add_package" aria-controls="home" role="tab"
						data-toggle="tab">Edit Price </a></li>
					
				</ul>
			</div>
		</div>
		<?php // debug($page_data); ?>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<form
				action="<?php echo base_url(); ?>index.php/flight_crs/edit_flight_price/<?php echo $this->uri->segment(3);?>/<?php echo $this->uri->segment(4)?>"
				method="post" enctype="multipart/form-data"
				class='form form-horizontal validate-form'>
				<div class="tab-content">
					<!-- Add Package Starts -->
					<div role="tabpanel" class="tab-pane active" id="add_package">
						
						  <input type="hidden" value="<?php echo $page_data['orgin']; ?>" name="orgin" />
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Adult Price</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="number" name="adult_price" id="adult_price"
											data-rule-required='true' placeholder="Adult Price"
											class='form-control itenary_elements' required value="<?php echo $page_data['adult_price']; ?>">
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Adult Tax</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="number" name="adult_tax" id="adult_tax"
											data-rule-required='true' placeholder="Adult Tax"
											class='form-control itenary_elements' required value="<?php echo $page_data['adult_tax']; ?>">
									</div>
								</div>
							</div>

							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Adult Bagage</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="number" name="adult_bagage" id="adult_bagage"
											data-rule-required='true' placeholder="Child Price"
											class='form-control itenary_elements' required value="<?php echo $page_data['adult_bagage']; ?>">
									</div>
								</div>
							</div>
							
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Child Price</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="number" name="child_price" id="child_price"
											data-rule-required='true' placeholder="Child Price"
											class='form-control itenary_elements' required value="<?php echo $page_data['child_price']; ?>">
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Child Tax</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="number" name="child_tax" id="child_tax"
											data-rule-required='true' placeholder="Child Tax"
											class='form-control itenary_elements' required value="<?php echo $page_data['child_tax']; ?>">
									</div>
								</div>
							</div>
                           <div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Child Bagage</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="number" name="child_bagage" id="child_bagage"
											data-rule-required='true' placeholder="Child child_bagage"
											class='form-control itenary_elements' required value="<?php echo $page_data['child_bagage']; ?>" >
									</div>
								</div>
							</div>
							

							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Infant Price</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="number" name="infant_price" id="infant_price"
											data-rule-required='true' placeholder="Infant Price"
											class='form-control itenary_elements' required value="<?php echo $page_data['infant_price']; ?>">
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Infant Tax</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="number" name="infant_tax" id="infant_tax"
											data-rule-required='true' placeholder="Infant Tax"
											class='form-control itenary_elements' required value="<?php echo $page_data['infant_tax']; ?>">
									</div>
								</div>
							</div>
							
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Fare Rules </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<textarea name="fare_rules" id="fare_rules"
											data-rule-required='true' placeholder="Fare Rules"
											class='form-control itenary_elements' required><?php echo $page_data['fare_rules']; ?></textarea>
									</div>
								</div>
							</div>
							
							
							
							
							
									<div class='form-actions' style='margin-bottom: 0'>
								<div class='row'>
									<div class='col-sm-9 col-sm-offset-3'>
										<button class='btn btn-primary' type='submit'>submit</button>
									</div>
								</div>
							</div>
								
							
						</div>
					</div>
					

				</div>
			</form>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL WRAP END -->
</div>


<?php
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/flight_suggest.js'), 'defer' => 'defer');
?>

