<div id="enquiries" class="bodyContent col-md-12">
  <div class="panel panel-default">
    <!-- PANEL WRAP START -->
    <div class="panel-heading">
      <!-- PANEL HEAD START -->
      <div class="panel-title">
        <ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
          <!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
          <li role="presentation" class="active"><a href=""
            aria-controls="home" role="tab" data-toggle="tab"><h3>Flight Report</h3></a></li>
          <!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
        </ul>
        
      </div>
    </div>
    <!-- PANEL HEAD START -->
    <div class="panel-body">
      <!-- PANEL BODY START -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="fromList">
          <div class="col-md-12">
            <div class='row'>
            
            <div class='row'>
                <div class='col-sm-14'>
                  <div class='' style='margin-bottom:0;'>
                    <div class=''>
                      <div class='responsive-table'>
                        <div class='scrollable-area'>
                       


                          <table class='data-table-column-filter table table-bordered table-striped' style='margin-bottom:0;'  id="example">
                            <thead>
                              <tr>
                               <th>Sno</th>
                                <th>Trip Type</th>
                                 <th>Airline Services</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Start Date</th>
                                <th>Return Date</th>
                                <th>Alloted Seats</th>
                                <th>Availability Seats </th>
                                 <th>Booked Seats </th>
                                 <th>Hold Seats </th>
                                <th>Class</th>
                                <th>Status</th>
                                
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                          <?php
                         // debug($page_data);  
                          if(!empty($page_data)) { $count = 1; 
                        foreach($page_data as $key => $crs_list) { 
                          $path=$GLOBALS ['CI']->template->domain_images('crs_airline_logo/'.$crs_list['crs_path'])?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $crs_list['flight_trip']; ?></td>
                        <td><img class="airline-logo" alt="" src="<?php echo $path ?>">
                                      <?php echo $crs_list['airline_name']; ?></td>
                        <td><?php echo $crs_list['from_city']; ?></td>
                        <td><?php echo $crs_list['to_city']; ?></td>
                        <td><?php echo $crs_list['jourany_date']; ?></td>
                        <td><?php echo $crs_list['retuen_date']; ?></td>
                        <td><?php echo $crs_list['no_of_seats']; ?></td>
                        <td><?php echo $crs_list['left_seats']; ?></td>
                        <td><?php echo $crs_list['booked_seats']; ?>
                        <td><?php echo $crs_list['hold_seats']; ?>  
                        
                        
                        <td><?php echo $crs_list['class']; ?></td>
                       <td><?php 
              if($crs_list['status'] == 1){
              echo '<span class="label label-success"><i class="fa fa-check"></i> ACTIVE</span>';
              }else{
              echo '<span class="label label-danger">INACTIVE</span>    ';
              }
                       ?></td>
                       
                       
                         <td class="center">                       
                                  <div class="" role="group">
                                  <a href="<?php echo base_url(); ?>index.php/flight_crs/b2b_flight_report_crs/<?php echo 0; ?>/<?php echo $crs_list['orgin']; ?>/<?php echo $crs_list['jourany_date']; ?>/<?php echo $crs_list['onwards_flight_number']; ?>/<?php echo $crs_list['return_flight_number']; ?>"  class="btn btn-sm btn-warning"><i class="fa fa-wrench"></i>view booking</a>
                                   <a href="<?php echo base_url(); ?>index.php/report/export_excel_pdf/<?php echo $crs_list['orgin']; ?>/<?php echo $crs_list['jourany_date']; ?>"  class="btn btn-sm btn-sucess" ><i class="fa fa-wrench"></i> Manifest</a>
                                   <a href="<?php echo base_url(); ?>index.php/flight_crs/b2b_flight_sales_report/<?php echo 0; ?>/<?php echo $crs_list['orgin']; ?>/<?php echo $crs_list['jourany_date']; ?>/<?php echo $crs_list['onwards_flight_number']; ?>/<?php echo $crs_list['return_flight_number']; ?>"  class="btn btn-sm btn-info" target="_blank"><i class="fa fa-wrench"></i> Sales Report</a>
                                  </div>
                        </td>
                      </tr>   
                  <?php $count++; } } ?>  
                      </tbody>
                          </table>
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- PANEL BODY END -->
  </div>
  <!-- PANEL WRAP END -->
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable();
	});
</script>

<?php
Js_Loader::$js [] = array (
		'src' => $GLOBALS ['CI']->template->template_js_dir ( 'page_resource/flight_suggest.js'), 'defer' => 'defer');
Js_Loader::$js [] = array (
		'src' => SYSTEM_RESOURCE_LIBRARY.'/DataTables/datatables.js', 'defer' => 'defer');
?>