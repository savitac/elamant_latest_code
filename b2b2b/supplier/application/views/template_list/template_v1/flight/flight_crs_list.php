<div id="enquiries" class="bodyContent col-md-12">
  <div class="panel panel-default">
    <!-- PANEL WRAP START -->
     <?php if($display==1){?>
    <div class="panel-heading">
      <!-- PANEL HEAD START -->
      <div class="panel-title">
        <ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
          <!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
          <li role="presentation" class="active"><a href=""
            aria-controls="home" role="tab" data-toggle="tab"><h3>Flight List </h3></a></li>
          <!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
        </ul>
        <a href="<?php echo base_url().'index.php/flight_crs/add_flight_list'?>"
            atarget="_blank" class="btn btn-sm btn-primar hide">Add Flights</a>
      </div>
    </div>
    <div class="panel-body">
      <!-- PANEL BODY START -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="fromList">
          <div class="col-md-12">
            <div class='row'>
            
            <div class='row'>
                <div class='col-sm-14'>
                  <div class='' style='margin-bottom:0;'>
                    <div class=''>
                      <div class='responsive-table'>
                        <div class='scrollable-area'>
                        <table class='data-table-column-filter table table-bordered table-striped' style='margin-bottom:0;' >
                            <thead>
                              <tr>
                               <th>Sno</th>
                                <th>Image</th>
                                <th>Ariline Code</th>
                                <th>Ariline Name</th>
                                <th>Supplier Name</th>
                                </tr>
                            </thead>
                            <tbody>
                          <?php
                        // debug($page_data);  
                          if(!empty($ariline_data)) { $count = 1; 
                        foreach($ariline_data as $key => $ariline) { 
                          $path=$GLOBALS ['CI']->template->domain_images('crs_airline_logo/'.$ariline['crs_path'])
                          ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><img height="75px" width="75px" src="<?php echo $path; ?>" alt="Ariline Logo"></td>
                        <td><?php echo $ariline['crs_code']; ?></td>
                         <td><?php echo $ariline['crs_name']; ?></td>
                         <td><?php echo $ariline['supplier_name']; ?></td>
                      </tr>   
                  <?php $count++; } } ?>  
                      </tbody>
                          </table>
                        </div>
                      </div> 
                    </div>
                  </div>

  <?php }?>
    <!-- PANEL HEAD START -->
     <?php if($display==0){?>
    
      <div class="panel-body">
      <div class="pull-right">
        <div class="btn-group">
    <div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    Filter <span class="caret"></span></button>
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list?show=0&today=1'?>">Today's Flights</a></li>
      <li><a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list?show=0'?>">Remove Fliter</a></li>
    </ul>
  </div>
</div>  </div>

      
      <!-- PANEL BODY START -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="fromList">
          <div class="col-md-12">
            <div class='row'>
            
            <div class='row'>
                <div class='col-sm-14'>
                  <div class='' style='margin-bottom:0;'>
                    <div class='responsive'>
                      <div class='responsive-table'>
                        <div class='scrollable-area'>

                          <table class='data-table-column-filter table table-bordered table-striped' style='margin-bottom:0;' id="example" >
                            <thead>
                              <tr>
                               <th>Sno</th>
                                <th>Trip Type</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Alloted Seats</th>
                                <th>Start</th>
                                <th>Return</th>
                                <th>Avail</th>
                                <th>Booked</th>
                                <th class="hide">Avail(2way)</th>
                                <th  class="hide">Booked(2way)</th>
                                <th>Hold Seats</th>
                                <th>Class</th>
                                <th>Status</th>
                                
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                          <?php
                         // debug($page_data);  
                          if(!empty($page_data)) { $count = 1; 
                          foreach($page_data as $key => $crs_list) { 
                          ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $crs_list['flight_trip']; ?></td>
                        <td><?php echo $crs_list['from_city']; ?></td>
                        <td><?php echo $crs_list['to_city']; ?></td>
                        <td><?php echo $crs_list['no_of_seats']; ?></td>
                        <td><?php echo $crs_list['jourany_date']; ?></td>
                        <td><?php echo $crs_list['retuen_date']; ?></td>
                        <td><?php echo $crs_list['left_seats']; ?></td>
                        <td><?php echo $crs_list['booked_seats']; ?> 
                        <!--2way-->
                        <?php //if($crs_list['flight_trip']=="circle"){?>
                        <td><?php echo $crs_list['left_seats']; ?></td>
                        <td><?php echo $crs_list['booked_seats']; ?> 
                        <?php //}else{?>
                        <td  class="hide">---</td>
                        <td  class="hide">---</td> 
                        
                        <?php //}?>
                        <td><?php echo $crs_list['hold_seats']; ?>  
                        <a href="<?php echo base_url(); ?>index.php/flight_crs/b2b_flight_report_crs/<?php echo 0; ?>/<?php echo $crs_list['orgin']; ?>/<?php echo $crs_list['jourany_date']; ?>"  class="btn btn-sm btn-warning"><i class="fa fa-wrench"></i>view booking</a></td>
                        
                        <td><?php echo $crs_list['class']; ?></td>
                       <td><?php 
              if($crs_list['status'] == 1){
              echo '<span class="label label-success"><i class="fa fa-check"></i> ACTIVE</span>';
              }else{
              echo '<span class="label label-danger">INACTIVE</span>    ';
              }
                       ?></td>
                       <td class="center">                       
                          <div class="btn-group">
      <div class="btn-group">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    Action <span class="caret"></span></button>
    <ul class="dropdown-menu" role="menu">
      <li>  <a href="<?php echo base_url(); ?>index.php/flight_crs/flight_segement_list/<?php echo $crs_list['orgin']; ?>/<?php echo $crs_list['flight_trip'] ?> "><i class="fa fa-file-o"></i>Segement</a></li>
      <li><a href="<?php echo base_url(); ?>index.php/flight_crs/flight_price_list/<?php echo $crs_list['orgin']; ?>"><i class="fa fa-money"></i>Price</a></li>
      <li><a href="<?php echo base_url(); ?>index.php/flight_crs/edit_flight_list/<?php echo $crs_list['orgin']; ?>"><i class="fa fa-pencil"></i> Edit</a></li>
      <li><a href="<?php echo base_url(); ?>index.php/flight_crs/update_status_flight_list/<?php echo $crs_list['orgin']; ?>/<?php echo $crs_list['status']; ?>"><i class="fa fa-wrench"></i>Status</a>  </li>
      <li><a href="<?php echo base_url(); ?>index.php/flight_crs/view_flight_details/<?php echo $crs_list['orgin']; ?>/<?php echo $crs_list['flight_trip'];?>"><i class="fa fa-wrench"></i> View Details</a></li>
    </ul>
  </div>
</div> 
                        </td>
                      </tr>   
                  <?php $count++; } } ?>  
                      </tbody>
                          </table>
                        </div>
                      </div> 
                   
            </div>
          </div>
        </div>
      </div>
    <?php }?>
    </div>
    <!-- PANEL BODY END -->
  </div>
  <!-- PANEL WRAP END -->
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>

<?php
Js_Loader::$js [] = array (
        'src' => $GLOBALS ['CI']->template->template_js_dir ( 'page_resource/flight_suggest.js'), 'defer' => 'defer');
Js_Loader::$js [] = array (
        'src' => SYSTEM_RESOURCE_LIBRARY.'/DataTables/datatables.js', 'defer' => 'defer');
?>
