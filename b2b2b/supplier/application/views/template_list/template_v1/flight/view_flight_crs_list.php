<div id="enquiries" class="bodyContent col-md-12">
	<div class="panel panel-default">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
					<li role="presentation" class="active"><a href=""
						aria-controls="home" role="tab" data-toggle="tab"><h3>View Flight Details </h3></a></li>
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
				</ul>
       
				
        <a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list'?>"><button class='btn btn-sm btn-primary' type='button'>Back</button></a>
			</div>
		</div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="fromList">
					<div class="col-md-12 ov_flow" style="overflow-x:scroll;">
						<div class='row'>
						
						<div class='row'>
                <div class='col-sm-14'>
                  <div class='' style='margin-bottom:0;'>
                    <div class=''>
                      <div class='responsive-table'>
                        <div class='scrollable-area'>
                        <?php //debug($page_data); ?>
                          <?php  if(!empty($page_data)) { 
                        $count = 1; 
                        foreach($page_data as $key => $crs_list) {
                        $path=$GLOBALS ['CI']->template->domain_images('crs_airline_logo/'.$crs_list['onward_path']);
                        $path_b=$GLOBALS ['CI']->template->domain_images('crs_airline_logo/'.$crs_list['return_path']) ; ?>
                        <table class='data-table-column-filter table table-bordered table-striped' style='margin-bottom:0;' id="example">
                              <tr>
                                  <td>Trip Type</td> 
                                  <td><?php echo $crs_list['flight_trip']; ?></td>
                              </tr>
                             <?php  if($crs_list['flight_trip']=='oneway') { ?>
                             <tr>
                                  <td>Onwards</td>
                             </tr>
                             <tr>
                                 <td>Flight</td>
                                 <td>Flight Number</td>
                                 <td>Equipment</td>
                             </tr>
                             <tr>
                                  <td><img class="airline-logo" alt="" src="<?php echo $path; ?>">
                                      <?php echo $crs_list['airline_name']; ?>
                                  </td>
                                  <td><strong> <?php echo $crs_list['onward_code']; echo $crs_list['onwards_flight_number']; ?></strong></td>
                                  <td><?php echo $crs_list['onwards_flight_equipment']; ?></td>
                             </tr>
                             <tr>
                                 <td>From</td>
                                 <td>To</td>
                             </tr>
                             <tr>
                                 <td><?php echo  $crs_list['from_city']; ?></td>
                                 <td><?php echo  $crs_list['to_city']; ?></td>
                             </tr>
                             <tr>
                                 <td>Departure</td>
                                 <td>Arrival</td>
                                  <td>Duration</td>
                             </tr>
                             <tr>
                                 <td><?php echo $crs_list['jourany_date']; ?> <?php echo $crs_list['onwards_departure_time'] ?></td>
                                 <td><?php echo $crs_list['onwards_arrival_time'] ?></td>
                                  <td><?php echo $crs_list['onwards_duration'] ?></td>
                             </tr>
                             <tr>
                                 <td>Adult Fare</td> <td>Adult Tax</td>
                                 <td>Child Fare</td> <td>Child Tax</td>
                                  <td>Infant Fare</td> <td>Infant Tax</td>
                             </tr>
                             <tr>
                                 <td><?php echo $crs_list['adult_price']; ?></td><td><?php echo $crs_list['adult_tax']; ?></td>
                                 <td><?php echo $crs_list['child_price'] ?></td><td><?php echo $crs_list['child_tax']; ?></td>
                                  <td><?php echo $crs_list['infant_price'] ?></td><td><?php echo $crs_list['infant_tax']; ?></td>
                             </tr>
                             <tr>
                                 <td>Fare Rule</td>
                                 
                             </tr>
                             <tr>
                                 <td><?php echo $crs_list['fare_rules']; ?></td>
                                 
                             </tr>
                             

                            <?php  } ?>

                             <?php  if($crs_list['flight_trip']=='circle') { ?>
                             <tr>
                                  <td>Onwards</td>
                             </tr>
                             <tr>
                                 <td>Flight</td>
                                 <td>Flight Number</td>
                                 <td>Equipment</td>
                             </tr>
                             <tr>
                                  <td><img class="airline-logo" alt="" src="<?php echo $path_b; ?>">
                                      <?php echo $crs_list['airline_name']; ?>
                                  </td>
                                  <td><strong> <?php echo $crs_list['onward_code']; echo $crs_list['onwards_flight_number']; ?></strong></td>
                                  <td><?php echo $crs_list['onwards_flight_equipment']; ?></td>
                             </tr>
                             <tr>
                                 <td>From</td>
                                 <td>To</td>
                             </tr>
                             <tr>
                                 <td><?php echo  $crs_list['from_city']; ?></td>
                                 <td><?php echo  $crs_list['to_city']; ?></td>
                             </tr>
                             <tr>
                                 <td>Departure</td>
                                 <td>Arrival</td>
                                  <td>Duration</td>
                             </tr>
                             <tr>
                                 <td><?php echo $crs_list['jourany_date']; ?> <?php echo $crs_list['onwards_departure_time'] ?></td>
                                 <td><?php echo $crs_list['onwards_arrival_time'] ?></td>
                                  <td><?php echo $crs_list['onwards_duration'] ?></td>
                             </tr>
                             <tr>
                                 <td>Adult Fare</td> <td>Adult Tax</td>
                                 <td>Child Fare</td> <td>Child Tax</td>
                                  <td>Infant Fare</td> <td>Infant Tax</td>
                             </tr>
                             <tr>
                                 <td><?php echo $crs_list['adult_price']; ?></td><td><?php echo $crs_list['adult_tax']; ?></td>
                                 <td><?php echo $crs_list['child_price'] ?></td><td><?php echo $crs_list['child_tax']; ?></td>
                                  <td><?php echo $crs_list['infant_price'] ?></td><td><?php echo $crs_list['infant_tax']; ?></td>
                             </tr>
                             <tr>
                                 <td>Fare Rule</td>
                                 
                             </tr>
                             <tr>
                                 <td><?php echo $crs_list['fare_rules']; ?></td>
                                 
                             </tr>

                              <tr>
                                  <td>Return</td>
                             </tr>
                             <tr>
                                 <td>Flight</td>
                                 <td>Flight Number</td>
                                 <td>Equipment</td>
                             </tr>
                             <tr>
                                  <td><img class="airline-logo" alt="" src="<?=SYSTEM_IMAGE_DIR.'airline_logo/' . $crslist['return_code']?>.gif">
                                      <?php echo $crslist['airline_namee']; ?>
                                  </td>
                                  <td><strong> <?php echo $crslist['return_code']; echo $crslist['return_flight_number']; ?></strong></td>
                                  <td><?php echo $crs_list['return_flight_equipment']; ?></td>
                             </tr>
                             <tr>
                                 <td>From</td>
                                 <td>To</td>
                             </tr>
                             <tr>
                                 <td><?php echo  $crslist['return_from_city']; ?></td>
                                 <td><?php echo  $crslist['return_to_city']; ?></td>
                             </tr>
                             <tr>
                                 <td>Departure</td>
                                 <td>Arrival</td>
                                  <td>Duration</td>
                             </tr>
                             <tr>
                                 <td><?php echo $crs_list['retuen_date']; ?> <?php echo $crslist['return_departure_time'] ?></td>
                                 <td><?php echo $crslist['return_arrival_time'] ?></td>
                                  <td><?php echo $crslist['return_duration'] ?></td>
                             </tr>
                             <tr>
                                 <td>Adult Fare</td> <td>Adult Tax</td>
                                 <td>Child Fare</td> <td>Child Tax</td>
                                  <td>Infant Fare</td> <td>Infant Tax</td>
                             </tr>
                             <tr>
                                 <td><?php echo $crs_list['adult_price']; ?></td><td><?php echo $crs_list['adult_tax']; ?></td>
                                 <td><?php echo $crslist['child_price'] ?></td><td><?php echo $crs_list['child_tax']; ?></td>
                                  <td><?php echo $crslist['infant_price'] ?></td><td><?php echo $crs_list['infant_tax']; ?></td>
                             </tr>
                             <tr>
                                 <td>Fare Rule</td>
                                 
                             </tr>
                             <tr>
                                 <td><?php echo $crs_list['fare_rules']; ?></td>
                                 
                             </tr>
                             

                             <?php } ?>
                            
                            
                          
                   
                      
                          </table>
                          
                         <?php } 
                         }?>
                          
                     
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL WRAP END -->
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
  });
</script>
<?php
Js_Loader::$js [] = array (
    'src' => $GLOBALS ['CI']->template->template_js_dir ( 'page_resource/flight_suggest.js'), 'defer' => 'defer');
Js_Loader::$js [] = array (
    'src' => SYSTEM_RESOURCE_LIBRARY.'/DataTables/datatables.js', 'defer' => 'defer');
?>