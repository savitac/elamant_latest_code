<?php
$flight_datepicker = array(array('flight_datepicker1', FUTURE_DATE), array('flight_datepicker2', FUTURE_DATE));
$this->current_page->set_datepicker($flight_datepicker);
$this->current_page->auto_adjust_datepicker(array(array('flight_datepicker1', 'flight_datepicker2')));
//$airline_list = $GLOBALS['CI']->db_cache_api->get_airline_code_list();
?>
<div id="Package" class="bodyContent col-md-12">
	<div class="panel panel-default">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
					<li role="presentation" class="active" id="add_package_li"><a
						href="#add_package" aria-controls="home" role="tab"
						data-toggle="tab">Edit Flight </a></li>
					
				</ul>
			</div>
		</div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<form
				action="<?php echo base_url(); ?>index.php/flight_crs/edit_flight_list/<?php echo $this->uri->segment(3);?>"
				method="post" enctype="multipart/form-data"
				class='form form-horizontal validate-form'>
				<div class="tab-content">
					<!-- Add Package Starts -->
					<div role="tabpanel" class="tab-pane active" id="add_package">
						<div class="col-md-12">

						<?php ///debug($page_data);
						if($page_data['flight_trip']=='oneway')
						{
							$checked='checked'; 
					    } else { $checked=''; }
					    if($page_data['flight_trip']=='circle')
						{
							$checked='checked'; 
					    }else { $checked=''; } ?>	
							<div class="col-md-6 text-right">
			<div class="form-group">
				<label class="radio-inline">
					<input type="radio" name="trip_type" <?=(@$page_data['flight_trip'] == 'oneway' ? 'checked="checked"' : '')?> id="onew-trp" value="oneway" <?php  echo $checked  ?> > One Way
				</label>
				<label class="radio-inline">
					<input type="radio" name="trip_type" <?=(@$page_data['flight_trip'] == 'circle' ? 'checked="checked"' : '')?> id="rnd-trp" value="circle" <?php  echo $checked  ?> > Round Trip 
				</label>
				
			</div>
		</div>
		</div>
		<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Airline Name</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
									<select class='select2 form-control add_pckg_elements'
										data-rule-required='true' name='airline_name' id="airline_name" required>
										<?php foreach($ariline_data as $airline_list_k => $airline_list_v) { ?>
										<option value="<?php echo $airline_list_v['crs_code']; ?>"><?php echo $airline_list_v['crs_name']; ?></option>
									     <?php } ?>             
																								
                                  </select>
										
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>From
									City </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" autocomplete="off" name="from_city" 
										class="normalinput auto-focus valid_class fromflight form-control " 
										id="from_city" placeholder="Type Departure City" value="<?php echo $page_data['from_city']; ?>" required />
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>To
									City </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" autocomplete="off" name="to_city" 
										class="normalinput auto-focus valid_class fromflight form-control " 
										id="to_city" placeholder="Type to City" value="<?php echo $page_data['to_city']; ?>" required />
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Depature</label>
								<div class='col-sm-4 controls'>
									<input type="text" readonly class="auto-focus hand-cursor form-control b-r-0" id="flight_datepicker1" 
									placeholder="dd-mm-yy"  name="jourany_date" value="<?php echo $page_data['jourany_date']; ?>"  required>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Return <?php echo $page_data['retuen_date']; ?></label>
								<div class='col-sm-4 controls'>
									<input type="text" readonly class="auto-focus hand-cursor form-control b-r-0" id="flight_datepicker2" 
									placeholder="dd-mm-yy"  name="retuen_date"  value="<?php echo $page_data['retuen_date']; ?>" required>
								</div>
							</div>
							<div class='form-group'>
									<label class='control-label col-sm-3' for='validation_name'>No of Seats
									</label>
									<div class='col-sm-4 controls'>
										<input type="text" name="no_of_seats" id="seats"
											data-rule-required='true' placeholder="seats"
											class='form-control itenary_elements' required value="<?php echo $page_data['no_of_seats']; ?>">
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_name'>Class
									</label>
								<div class='col-sm-4 controls'>
									<select class='select2 form-control add_pckg_elements'
										data-rule-required='true' name='class' id="class" required>
										<option value="<?php echo $page_data['class']; ?>"><?php echo $page_data['class']; ?></option>
										<option value="All">All</option>
											<option value="Economy With Restrictions">Economy With
												Restrictions</option>
											<option value="Economy Without Restrictions">Economy Without
												Restrictions</option>
											<option value="Economy Premium">Economy Premium</option>
											<option value="Business">Business</option>
											<option value="First">First</option>
                       
																								
                                  </select> <span id="distination"
										style="color: #F00; display: none;">validate</span>
								</div>
							</div>
							
							
									<div class='form-actions' style='margin-bottom: 0'>
								<div class='row'>
									<div class='col-sm-9 col-sm-offset-3'>
									<input type="hidden" name="booked_seat" value="<?php echo $page_data['booked_seats']; ?>" />
										<button class='btn btn-primary' type='submit'>submit</button>
										 <a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list'?>"><button class='btn btn-primary' type='button'>Back</button></a>
									</div>
								</div>
							</div>
								
							
						</div>
					</div>
					

				</div>
			</form>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL WRAP END -->
</div>


<?php
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/flight_crs_suggest.js'), 'defer' => 'defer');
?>

