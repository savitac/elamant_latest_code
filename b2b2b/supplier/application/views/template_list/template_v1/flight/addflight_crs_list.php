<?php
$flight_datepicker = array(array('flight_datepicker1', FUTURE_DATE), array('flight_datepicker2', FUTURE_DATE));
$this->current_page->set_datepicker($flight_datepicker);
$this->current_page->auto_adjust_datepicker(array(array('flight_datepicker1', 'flight_datepicker2')));
//$airline_list = $GLOBALS['CI']->db_cache_api->get_airline_code_list();
?>
<div id="Package" class="bodyContent col-md-12">
	<div class="panel panel-default">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
					<li role="presentation" class="active" id="add_package_li"><a
						href="#add_package" aria-controls="home" role="tab"
						data-toggle="tab">Add Flight </a></li>
					
				</ul>
			</div>
		</div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<form
				action="<?php echo base_url(); ?>index.php/flight_crs/add_flight_list"
				method="post" enctype="multipart/form-data"
				class='form form-horizontal validate-form'>
				<div class="tab-content">
					<!-- Add Package Starts -->
					<div role="tabpanel" class="tab-pane active" id="add_package">
						<div class="col-md-12">

							
							<div class="col-md-6 text-right">
			<div class="form-group">
				<label class="radio-inline">
					<input type="radio" name="trip_type" <?=(@$flight_search_params['trip_type'] == 'oneway' ? 'checked="checked"' : '')?> id="onew-trp" value="oneway" checked> One Way
				</label>
				<label class="radio-inline">
					<input type="radio" name="trip_type" <?=(@$flight_search_params['trip_type'] == 'circle' ? 'checked="checked"' : '')?> id="rnd-trp" value="circle"> Round Trip
				</label>
				
			</div>
		</div>
		</div>
		<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Airline Name</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
									<select class='select2 form-control add_pckg_elements'
										data-rule-required='true' name='airline_name' id="airline_name" required>
										<?php foreach($page_data as $airline_list_k => $airline_list_v) { ?>
										<option value="<?php echo $airline_list_v['crs_code']; ?>"><?php echo $airline_list_v['crs_name']; ?></option>
									     <?php } ?>             
																								
                                  </select>
										
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>From
									City </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" autocomplete="off" name="from_city" 
										class="normalinput auto-focus valid_class fromflight form-control " 
										id="from_city" placeholder="Type Departure City" value="" required />
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>To
									City </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" autocomplete="off" name="to_city" 
										class="normalinput auto-focus valid_class fromflight form-control " 
										id="to_city" placeholder="Type to City" value="" required />
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Depature</label>
								<div class='col-sm-4 controls'>
									<input type="text" readonly class="auto-focus hand-cursor form-control b-r-0" id="flight_datepicker1" 
									placeholder="dd-mm-yy" value="" name="jourany_date" required>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Return</label>
								<div class='col-sm-4 controls'>
									<input type="text" readonly class="auto-focus hand-cursor form-control b-r-0" id="roundway" 
									placeholder="dd-mm-yy" value="" name="retuen_date" required readonly="">
								</div>
							</div>

							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Till Date</label>
								<div class='col-sm-4 controls'>
									<input type="text"  class="auto-focus hand-cursor form-control b-r-0" 
									id="till_date" 
									placeholder="dd-mm-yy" value="" name="till_date" required>
								</div>
							</div>
							<?php 
							$weekdays=array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"); ?>
							
								<div class='form-group'>
									<label class='control-label col-sm-3' for='validation_name'>Select Days
									</label>
									<div class='col-sm-4 controls'>
										<?php foreach($weekdays as $index=> $value){?>
									<label class="checkbox-inline"><input type="checkbox" name ="no_days[]" value="<?=$index?>"><?=$value?></label>
								<?php }?>
									</div>
								</div>


								<div class='form-group'>
									<label class='control-label col-sm-3' for='validation_name'>No of Seats
									</label>
									<div class='col-sm-4 controls'>
										<input type="text" name="no_of_seats" id="seats"
											data-rule-required='true' placeholder="seats"
											class='form-control itenary_elements' required>
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_name'>Class
									</label>
								<div class='col-sm-4 controls'>
									<select class='select2 form-control add_pckg_elements'
										data-rule-required='true' name='class' id="class" required>
										<option value="All">All</option>
											<option value="Economy With Restrictions">Economy With
												Restrictions</option>
											<option value="Economy Without Restrictions">Economy Without
												Restrictions</option>
											<option value="Economy Premium">Economy Premium</option>
											<option value="Business">Business</option>
											<option value="First">First</option>
                       
																								
                                  </select> <span id="distination"
										style="color: #F00; display: none;">validate</span>
								</div>
							</div>
							
							
									<div class='form-actions' style='margin-bottom: 0'>
								<div class='row'>
									<div class='col-sm-9 col-sm-offset-3'>
										<button class='btn btn-primary' type='submit'>Next</button>&nbsp;
										<a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list'?>"><button class='btn btn-primary' type='button'>Back</button></a>
									</div>
								</div>
							</div>
								
							
						</div>
					</div>
					

				</div>
			</form>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL WRAP END -->
</div>


<?php
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/flight_crs_suggest.js'), 'defer' => 'defer');
?>

