<?php
$airline_list = $GLOBALS['CI']->db_cache_api->get_airline_code_list();
$flight_datepicker = array(array('flight_datepicker1', FUTURE_DATE), array('flight_datepicker2', FUTURE_DATE));
$this->current_page->set_datepicker($flight_datepicker);
$this->current_page->auto_adjust_datepicker(array(array('flight_datepicker1', 'flight_datepicker2')));
//debug($page_data);
//echo $page_data['0']['code'];
//
?>
<div id="Package" class="bodyContent col-md-12">
	<div class="panel panel-default">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
					<li role="presentation" class="active" id="add_package_li"><a
						href="#add_package" aria-controls="home" role="tab"
						data-toggle="tab">Add Segement </a></li>
					
				</ul>
			</div>
		</div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<form
				action="<?php echo base_url(); ?>index.php/flight_crs/edit_flight_segement/<?php echo $this->uri->segment(3);?>/<?php echo $this->uri->segment(4)?>/<?php echo $this->uri->segment(5)?>"
				method="post" enctype="multipart/form-data"
				class='form form-horizontal validate-form'>
				<div class="tab-content">
					<!-- Add Package Starts -->
					<div role="tabpanel" class="tab-pane active" id="add_package">
						
						<?php if ($this->uri->segment(5)=='circle'){ ?>
						    <h3>Onwards</h3>
							
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Flight Number </label>
								<div class='col-sm-4 controls'>
									<div class="controls">

										<input type="text" name="onwards_flight_number" id="onwards_flight_number"
											data-rule-required='true' placeholder="Flight Number" value="<?php echo $page_data['onwards_flight_number']; ?>"
											class='form-control itenary_elements' required>
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Flight Equipment</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" name="onwards_flight_equipment" id="onwards_flight_equipment"
											data-rule-required='true' placeholder="Flight Equipment" value="<?php echo $page_data['onwards_flight_equipment']; ?>"
											class='form-control itenary_elements' required>
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>From City </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" autocomplete="off" name="onwards_from_city" 
										class="normalinput auto-focus valid_class fromflight form-control " 
										id="from_city" placeholder="Type Departure City"  readonly="readonly" value="<?php echo $page_data['onwards_from_city']; ?>" required />
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>To City </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" autocomplete="off" name="onwards_to_city" 
										class="normalinput auto-focus valid_class fromflight form-control " 
										id="to_city" placeholder="Type to City" readonly="readonly" value="<?php echo $page_data['onwards_to_city']; ?>" required />
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Depature Time</label>
								<div class='col-sm-4 controls'>
									<input type="time"  class="auto-focus hand-cursor form-control b-r-0" id="" value="<?php echo $page_data['onwards_departure_time']; ?>"
									placeholder="" value="" name="onwards_departure_time" required>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Arrival Time</label>
								<div class='col-sm-4 controls'>
									<input type="time" class="auto-focus hand-cursor form-control b-r-0" id="" 
									placeholder="" value="" name="onwards_arrival_time" required>
								</div>
							</div>

							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Travel Duration</label>
								<div class='col-sm-4 controls'>
									<input type="text"  class="auto-focus hand-cursor form-control b-r-0" id=""  value="<?php echo $page_data['onwards_duration']; ?>"
									placeholder="" value="" name="onwards_duration" required>
								</div>
							</div>


							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_name'>Arrival At
									</label>
								<div class='col-sm-4 controls'>
									<select class='select2 form-control add_pckg_elements'
										data-rule-required='true' name='onwards_arrival_at' id="disn" required>
										<option value="Same-day">Same Day</option>
											<option value="Next-day">Next Day</option>
											
                       
																								
                                  </select> <span id="distination"
										style="color: #F00; display: none;">validate</span>
								</div>
							</div>



							<h3>Return</h3>
							
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Flight Number </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" name="return_flight_number" id="onwards_flight_number"
											data-rule-required='true' placeholder="Flight Number"
											class='form-control itenary_elements' required>
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Flight Equipment</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" name="return_flight_equipment" id="onwards_flight_equipment" value="<?php echo $page_data['onwards_flight_equipment']; ?>"
											data-rule-required='true' placeholder="Flight Equipment"
											class='form-control itenary_elements' required>
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>From City </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" autocomplete="off" name="return_from_city" 
										class="normalinput auto-focus valid_class fromflight form-control " 
										id="from_city" placeholder="Type Departure City" readonly="readonly" value="<?php echo $page_data['return_from_city']; ?>" required />
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>To City </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" autocomplete="off" name="return_to_city" 
										class="normalinput auto-focus valid_class fromflight form-control " 
										id="to_city" placeholder="Type to City" readonly="readonly" value="<?php echo $page_data['return_to_city']; ?>" required />
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Depature Time</label>
								<div class='col-sm-4 controls'>
									<input type="time"  class="auto-focus hand-cursor form-control b-r-0" id="" value="<?php echo $page_data['return_departure_time']; ?>"
									placeholder="" value="" name="return_departure_time" required>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Arrival Time</label>
								<div class='col-sm-4 controls'>
									<input type="time"  class="auto-focus hand-cursor form-control b-r-0" id="" value="<?php echo $page_data['return_arrival_time']; ?>"
									placeholder="" value="" name="return_arrival_time" required>
								</div>
							</div>

							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Travel Duration</label>
								<div class='col-sm-4 controls'>
									<input type="text"  class="auto-focus hand-cursor form-control b-r-0" id="" value="<?php echo $page_data['return_duration']; ?>"
									placeholder="" value="" name="return_duration" required>
								</div>
							</div>


							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_name'>Arrival At
									</label>
								<div class='col-sm-4 controls'>
									<select class='select2 form-control add_pckg_elements'
										data-rule-required='true' name='return_arrival_at' id="disn" required>
										<option value="Same-day">Same Day</option>
											<option value="Next-day">Next Day</option>
											
                       
																								
                                  </select> <span id="distination"
										style="color: #F00; display: none;">validate</span>
								</div>
							</div>

							<?php }

							if ($this->uri->segment(5)=='oneway'){ ?>
							<h3>Onwards</h3>
							
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Flight Number </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" name="onwards_flight_number" id="onwards_flight_number" value="<?php echo $page_data['onwards_flight_number']; ?>"
											data-rule-required='true' placeholder="Flight Number"
											class='form-control itenary_elements' required>
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>Flight Equipment</label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" name="onwards_flight_equipment" id="onwards_flight_equipment" value="<?php echo $page_data['onwards_flight_equipment']; ?>"
											data-rule-required='true' placeholder="Flight Equipment"
											class='form-control itenary_elements' required>
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>From City </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" autocomplete="off" name="onwards_from_city" 
										class="normalinput auto-focus valid_class fromflight form-control " 
										id="from_city" placeholder="Type Departure City" readonly="readonly" value="<?php echo $page_data['onwards_from_city']; ?>" required />
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_current'>To City </label>
								<div class='col-sm-4 controls'>
									<div class="controls">
										<input type="text" autocomplete="off" name="onwards_to_city" 
										class="normalinput auto-focus valid_class fromflight form-control " 
										id="to_city" placeholder="Type to City" readonly="readonly" value="<?php echo $page_data['onwards_to_city']; ?>" required />
									</div>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Depature Time</label>
								<div class='col-sm-4 controls'>
									<input type="time"  class="auto-focus hand-cursor form-control b-r-0" id=""  value="<?php echo $page_data['onwards_departure_time']; ?>"
									placeholder="" value="" name="onwards_departure_time" required>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Arrival Time</label>
								<div class='col-sm-4 controls'>
									<input type="time"  class="auto-focus hand-cursor form-control b-r-0" id="" value="<?php echo $page_data['onwards_arrival_time']; ?>"
									placeholder="" value="" name="onwards_arrival_time" required>
								</div>
							</div>

							<div class='form-group'>
								<label class='control-label col-sm-3' for='adult'>Travel Duration</label>
								<div class='col-sm-4 controls'>
									<input type="text"  class="auto-focus hand-cursor form-control b-r-0" id="" value="<?php echo $page_data['onwards_duration']; ?>"
									placeholder="" value="" name="onwards_duration" required>
								</div>
							</div>

							<div class='form-group'>
								<label class='control-label col-sm-3' for='validation_name'>Arrival At
									</label>
								<div class='col-sm-4 controls'>
									<select class='select2 form-control add_pckg_elements'
										data-rule-required='true' name='disn' id="disn" required>
										<option value="Same-day">Same Day</option>
											<option value="Next-day">Next Day</option>
											
                       
																								
                                  </select> <span id="distination"
										style="color: #F00; display: none;">validate</span>
								</div>
							</div>
							<?php } ?>

							
							
							
									<div class='form-actions' style='margin-bottom: 0'>
								<div class='row'>
									<div class='col-sm-9 col-sm-offset-3'>
										<button class='btn btn-primary' type='submit'>submit</button>
									</div>
								</div>
							</div>
								
							
						</div>
					</div>
					

				</div>
			</form>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL WRAP END -->
</div>


<?php
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/flight_crs_suggest.js'), 'defer' => 'defer');
?>
