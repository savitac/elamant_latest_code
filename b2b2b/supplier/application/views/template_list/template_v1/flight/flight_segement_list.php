<div id="enquiries" class="bodyContent col-md-12">
	<div class="panel panel-default">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
					<li role="presentation" class="active"><a href=""
						aria-controls="home" role="tab" data-toggle="tab"><h3>Flight Segement List </h3></a></li>
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
				</ul>
       <?php  if(empty($page_data)) { ?>
         <a href="<?php echo base_url()?>index.php/flight_crs/add_flight_segement/<?php echo $this->uri->segment(3);?>/<?php echo $this->uri->segment(4)?>"
            atarget="_blank" class="btn btn-sm btn-primary">Add Segement</a>
      <?php  } ?>
				
        <a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list'?>"><button class='btn btn-sm btn-primary' type='button'>Back</button></a>
			</div>
		</div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="fromList">
					<div class="col-md-12 ov_flow" style="overflow-x:scroll;">
						<div class='row'>
						
						<div class='row'>
                <div class='col-sm-14'>
                  <div class='' style='margin-bottom:0;'>
                    <div class=''>
                      <div class='responsive-table'>
                        <div class='scrollable-area'>
                          <table class='data-table-column-filter table table-bordered table-striped' style='margin-bottom:0;' id="example">
                            <thead>
                              <tr>
                               <th>Sno</th>
                                <th>Onwards Flight Name</th>
                                <th>Onwards Flight Number</th>
                                 <th>Onwards Flight Equipement</th>
                                 <th>Onwards Flight From</th>
                                  <th>Onwards Flight To</th>
                                   <th>Onwards Depature Time</th>
                                    <th>Onwards Arrival Time</th>
                                     <th>Onwards Arrival At</th>
                                      <th>Onwards Duration</th>

                                 <th>Return Flight Name</th>
                                 <th>Return Flight Number</th>
                                <th>Return Flight Equipement</th>
                                 <th>Return Flight From</th>
                                <th>Return Flight To</th>
                                <th>Return Depature Time</th>
                                 <th>Return  Arrival Time</th>
                                 <th>Return  Arrival At</th>
                                  <th>Return Duration</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                          <?php
                         // debug($page_data);  
                          if(!empty($page_data)) { $count = 1; 
                        foreach($page_data as $key => $segment_list) { ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $segment_list['onwards_flight_name']; ?></td>
                        <td><?php echo $segment_list['onwards_flight_number']; ?></td>
                        <td><?php echo $segment_list['onwards_flight_equipment']; ?></td>
                        <td><?php echo $segment_list['onwards_from_city']; ?></td>
                        <td><?php echo $segment_list['onwards_to_city']; ?></td>
                        <td><?php echo $segment_list['onwards_departure_time']; ?></td>
                        <td><?php echo $segment_list['onwards_arrival_time']; ?></td>
                         <td><?php echo $segment_list['onwards_arrival_at']; ?></td>
                         <td><?php echo $segment_list['onwards_duration']; ?></td>

                        <td><?php echo $segment_list['retuen_flight_name']; ?></td>
                        <td><?php echo $segment_list['return_flight_number']; ?></td>
                        <td><?php echo $segment_list['return_flight_equipment']; ?></td>
                        <td><?php echo $segment_list['return_from_city']; ?></td>
                        <td><?php echo $segment_list['return_to_city']; ?></td>
                        <td><?php echo $segment_list['return_departure_time']; ?></td>
                         <td><?php echo $segment_list['return_arrival_time']; ?></td>
                        <td><?php echo $segment_list['return_arrival_at']; ?></td>
                        <td><?php echo $segment_list['return_duration']; ?></td>
                        <td class="center">                       
                                  <div class="" role="group">
                                  <a href="<?php echo base_url(); ?>index.php/flight_crs/edit_flight_segement/<?php echo $segment_list['orgin']; ?>/<?php echo $this->uri->segment(3);?>/<?php echo $this->uri->segment(4)?> " class="btn btn-sm btn-default"><i class="fa fa-file-o"></i>Edit</a>
                                  
                                                                       
                        </td>
                       
                       
                         
                      </tr>   
                  <?php $count++; } } ?>  
                      </tbody>
                          </table>
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL WRAP END -->
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
  });
</script>
<?php
Js_Loader::$js [] = array (
    'src' => $GLOBALS ['CI']->template->template_js_dir ( 'page_resource/flight_suggest.js'), 'defer' => 'defer');
Js_Loader::$js [] = array (
    'src' => SYSTEM_RESOURCE_LIBRARY.'/DataTables/datatables.js', 'defer' => 'defer');
?>