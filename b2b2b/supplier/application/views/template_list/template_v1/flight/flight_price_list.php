<div id="enquiries" class="bodyContent col-md-12">
	<div class="panel panel-default">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
					<li role="presentation" class="active"><a href=""
						aria-controls="home" role="tab" data-toggle="tab"><h3>Flight Price List </h3></a></li>
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
				</ul>
        <?php  if(empty($page_data)) { ?>
				<a href="<?php echo base_url()?>index.php/flight_crs/add_flight_price/<?php echo $this->uri->segment(3);?>/<?php echo $this->uri->segment(4)?>"
						atarget="_blank" class="btn btn-sm btn-primary">Add Price</a>
            <?php } ?>
        <a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list'?>"><button class='btn btn-sm btn-primary' type='button'>Back</button></a>
			</div>
		</div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="fromList">
					<div class="col-md-12">
						<div class='row'>
						
						<div class='row'>
                <div class='col-sm-14'>
                  <div class='' style='margin-bottom:0;'>
                    <div class=''>
                      <div class='responsive-table'>
                        <div class='scrollable-area'>
                          <table class='data-table-column-filter table table-bordered table-striped' style='margin-bottom:0;' id="example">
                            <thead>
                              <tr>
                               <th>Sno</th>
                                <th>Adult Price</th>
                                <th>Adult Tax</th>
                               <th>Adult Bagage</th>
                                 <th>Child Price</th>
                                 <th>Child Tax</th>
                                 <th>Child Bagage</th> 
                                  <th>Infant Price</th>
                                   <th>Infant Tax</th>
                                  <!-- <th>Infant Markup</th> -->
                                    <th>Fare Rules</th>
                                     <th>Action</th>
                                     
                                
                              </tr>
                            </thead>
                            <tbody>
                          <?php
                         // debug($page_data);  
                          if(!empty($page_data)) { $count = 1; 
                        foreach($page_data as $key => $price_list) { ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $price_list['adult_price']; ?></td>
                        <td><?php echo $price_list['adult_tax']; ?></td>
                        <td><?php echo $price_list['adult_bagage']; ?></td> 
                        <td><?php echo $price_list['child_price']; ?></td>
                        <td><?php echo $price_list['child_tax']; ?></td>
                         <td><?php echo $price_list['child_bagage']; ?></td> 
                        <td><?php echo $price_list['infant_price']; ?></td>
                        <td><?php echo $price_list['infant_tax']; ?></td>
                         <!--<td><?php echo $price_list['infant_markup']; ?></td> -->
                        <td><?php echo $price_list['fare_rules']; ?></td>
                         <td class="center">                       
                                  <div class="" role="group">
                                  <a href="<?php echo base_url(); ?>index.php/flight_crs/edit_flight_price/<?php echo $price_list['orgin']; ?>/<?php echo $price_list['flight_crs_no'] ?> " class="btn btn-sm btn-default"><i class="fa fa-file-o"></i>Edit</a>
                                  <a href="<?php echo base_url(); ?>index.php/flight_crs/delete_flight_price_list/<?php echo $price_list['orgin']; ?>/<?php echo $price_list['flight_crs_no'] ?>"  class="btn btn-sm btn-primary"><i class="fa fa-file-o"></i>Delete</a>
                                                                       
                        </td>
                        
                       
                       
                         
                      </tr>   
                  <?php $count++; } } ?>  
                      </tbody>
                          </table>
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL WRAP END -->
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
  });
</script>
<?php
Js_Loader::$js [] = array (
    'src' => $GLOBALS ['CI']->template->template_js_dir ( 'page_resource/flight_suggest.js'), 'defer' => 'defer');
Js_Loader::$js [] = array (
    'src' => SYSTEM_RESOURCE_LIBRARY.'/DataTables/datatables.js', 'defer' => 'defer');
?>