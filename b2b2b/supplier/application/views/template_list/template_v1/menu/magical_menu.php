<?php
$active_domain_modules = $this->active_domain_modules;
/**
 * Need to make privilege based system
 * Privilege only for loading menu and access of the web page
 * 
 * Data loading will not be based on privilege.
 * Data loading logic will be different.
 * It depends on many parameters
 */
$menu_list = array();
if (count($active_domain_modules) > 0) {
	$any_domain_module = true;
} else {
	$any_domain_module = false;
}
$airline_module = is_active_airline_module();
$accomodation_module = is_active_hotel_module();
$bus_module = is_active_bus_module();
$package_module = is_active_package_module();
$bb='b2b';
$bc='b2c';
$b2b = is_active_module($bb);
$b2c = is_active_module($bc);
//checking social login status 
$social_login = 'facebook';
$social = is_active_social_login($social_login);
?>
<ul class="sidebar-menu">
	<li class="treeview">
		<a href="#">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url()?>"><i class="fa fa-bars"></i> Dashboard v1</a></li>
		</ul>
	</li>
	<?php if(is_domain_user() == false) { // ACCESS TO ONLY PROVAB ADMIN ?>
	<!--<li class="treeview">
		<a href="#">
		<i class="fa fa-wrench"></i> <span>Management</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/user/user_management'?>"><i class="fa fa-user"></i> User</a></li>
			<li><a href="<?php echo base_url().'index.php/user/domain_management'?>"><i class="fa fa-laptop"></i> Domain</a></li>
			<li><a href="<?php echo base_url().'index.php/module/module_management'?>"><i class="fa fa-sitemap"></i> Master Module</a></li>
		</ul>
	</li>-->
	<?php if ($any_domain_module) {?>
	<!--<li class="treeview">
		<a href="#">
		<i class="fa fa-user"></i> <span>Markup</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<?php if ($airline_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/airline_domain_markup'?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
			<?php } ?>
			<?php if ($accomodation_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/hotel_domain_markup'?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
			<?php } ?>
			<?php if ($bus_module) { ?>
			<li><a href="<?php echo base_url().'index.php/private_management/bus_domain_markup'?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
			<?php } ?>
		</ul>
	</li>-->
	<?php } ?>
	<!--<li class="treeview">
		<a href="<?php echo base_url().'index.php/private_management/process_balance_manager'?>">
			<i class="fa fa-google-wallet"></i> 
			<span> Master Balance Manager </span>
		</a>
	</li>-->
	<!--<li class="treeview">
		<a href="<?php echo base_url().'index.php/private_management/event_logs'?>">
			<i class="fa fa-shield"></i> 
			<span> Event Logs </span>
		</a>
	</li> -->
	<?php } else if((is_domain_user() == true)) {
		// ACCESS TO ONLY DOMAIN ADMIN
	?>
	<!-- USER ACCOUNT MANAGEMENT -->
	<!--<li class="treeview">
		<a href="#">
			<i class="fa fa-user"></i> 
			<span> Users </span><i class="fa fa-angle-left pull-right"></i></a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<?php if($b2c){	?>
			<!--<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER;?>"><i class="fa fa-circle-o"></i> B2C</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/b2c_user?filter=user_type&q='.B2C_USER.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.B2C_USER;?>"><i class="fa fa-circle-o"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php } ?>
			<?php if($b2b){	?>
			<li><a href="<?php echo base_url().'index.php/user/b2b_user?filter=user_type&q='.B2B_USER ?>"><i class="fa fa-circle-o"></i> Agents</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/b2b_user?user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/b2b_user?user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.B2B_USER;?>"><i class="fa fa-circle-o"></i> Logged In User</a></li>
				</ul>
			</li>
			<?php }?>
			<li><a href="<?php echo base_url().'index.php/user/supplier_user?filter=user_type&q='.SUPPLIER_USER ?>"><i class="fa fa-circle-o"></i>Suppliers</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/supplier_user?filter=user_type&q='.SUPPLIER_USER.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/supplier_user?filter=user_type&q='.SUPPLIER_USER.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.SUPPLIER_USER;?>"><i class="fa fa-circle-o"></i> Logged In User</a></li>
				</ul>
			</li>
			<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUB_ADMIN ?>"><i class="fa fa-circle-o"></i> Sub Admin</a>
				<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUB_ADMIN.'&user_status='.ACTIVE;?>"><i class="fa fa-check"></i> Active</a></li>
				<li><a href="<?php echo base_url().'index.php/user/user_management?filter=user_type&q='.SUB_ADMIN.'&user_status='.INACTIVE;?>"><i class="fa fa-times"></i> InActive</a></li>
				<li><a href="<?php echo base_url().'index.php/user/get_logged_in_users?filter=user_type&q='.SUB_ADMIN;?>"><i class="fa fa-circle-o"></i> Logged In User</a></li>
				</ul>
			</li>
		</ul>
	</li>-->
	<?php if ($any_domain_module) {?>
	<!--<li class="treeview">
		<a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list_booking';?>">
			<i class="fa fa-bar-chart"></i> 
			<span> Manage Booking </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<!--<li><a href="#"><i class="fa fa-circle-o"></i> B2C</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2c_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>
				</ul>
			</li>-->
           <!--<li><a href="#"><i class="fa fa-circle-o"></i> CRS </a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_flight_report/';?>"><i class="fa fa-plane"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<!--<li><a href="<?php echo base_url().'index.php/report/b2b_hotel_report/';?>"><i class="fa fa-bed"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/report/b2b_bus_report/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?> -->
				<!--</ul>
			</li>


		</ul>
		<!--<ul class="treeview-menu">
			<!--  TYPES -->
			<!--<li class="treeview">
				<a href="<?php echo base_url().'index.php/transaction/logs'?>">
					<i class="fa fa-shield"></i> 
					<span> Transaction Logs </span>
				</a>
			</li>
			<li class="treeview">
				<a href="<?php echo base_url().'index.php/transaction/search_history'?>">
					<i class="fa fa-search"></i> 
					<span> Search History </span>
				</a>
			</li>
			<li class="treeview">
				<a href="<?php echo base_url().'index.php/transaction/top_destinations'?>">
					<i class="fa fa-globe"></i> 
					<span>Top Destinations</span>
				</a>
			</li>
		</ul>-->
	</li>
	<?php if($b2b) {?>
		<!--<li class="treeview">
			<a href="#">
			<i class="fa fa-briefcase"></i> <span>Commission</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url().'index.php/management/agent_commission?default_commission='.ACTIVE;?>"><i class="fa fa-circle-o"></i> Default Commission</a></li>
				<li><a href="<?php echo base_url().'index.php/management/agent_commission'?>"><i class="fa fa-circle-o"></i> Agent's Commission</a></li>
			</ul>
		</li> -->
	<?php }?>
	<!--<li class="treeview">
		<a href="#">
			<i class="fa fa-plus-square"></i> 
			<span> Markup </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- Markup TYPES -->
		<?php if($b2c) {?>
			<!--<li><a href="#"><i class="fa fa-circle-o"></i> B2C</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_airline_markup/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2c_bus_markup/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php }  ?>
				</ul>
			</li>
			<?php } 
			if($b2b){	?>
			<li><a href="#"><i class="fa fa-circle-o"></i> B2B</a>
				<ul class="treeview-menu">
				<?php if ($airline_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_airline_markup/';?>"><i class="<?=get_arrangement_icon(META_AIRLINE_COURSE)?>"></i> Flight</a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_hotel_markup/';?>"><i class="<?=get_arrangement_icon(META_ACCOMODATION_COURSE)?>"></i> Hotel</a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li><a href="<?php echo base_url().'index.php/management/b2b_bus_markup/';?>"><i class="<?=get_arrangement_icon(META_BUS_COURSE)?>"></i> Bus</a></li>
				<?php } ?>
				</ul>
			</li>
			<?php } ?>
		</ul>
	</li> -->
	<?php } ?>
	<?php if($b2b){	?>
	<!--<li class="treeview">
		<a href="#">
			<i class="fa fa-money"></i> 
			<span> Master Balance Manager </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<!--<li><a href="<?php echo base_url().'index.php/management/master_balance_manager'?>"><i class="fa fa-circle-o"></i> API</a></li>-->
			<!--<li><a href="<?php echo base_url().'index.php/management/b2b_balance_manager'?>"><i class="fa fa-circle-o"></i> B2B</a></li>
		</ul>
	</li> -->
		<?php } if ($package_module) { ?>
	<!--<li class="treeview">
		<a href="#">
			<i class="fa fa-plus-square"></i> 
			<span> Package Management </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<!--<li><a href="<?php echo base_url().'index.php/supplier/view_packages_types'?>"><i class="fa fa-circle-o"></i> View Package Types </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/add_with_price'?>"><i class="fa fa-circle-o"></i> Add New Package </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/view_with_price'?>"><i class="fa fa-circle-o"></i> View Packages </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/enquiries'?>"><i class="fa fa-circle-o"></i> View Packages Enquiries </a></li>
		</ul>
	</li> -->
	<?php } ?>
	<!--<li class="treeview">
		<a href="#">
			<i class="fa fa-envelope"></i> 
			<span> Email Subscriptions </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<!-- USER TYPES -->
			<!--<li><a href="<?php echo base_url().'index.php/general/view_subscribed_emails'?>"><i class="fa fa-circle-o"></i> View Emails </a></li>
			<!-- <li><a href="<?php echo base_url().'index.php/supplier/add_with_price'?>"><i class="fa fa-circle-o"></i> Add New Package </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/view_with_price'?>"><i class="fa fa-circle-o"></i> View Packages </a></li>
			<li><a href="<?php echo base_url().'index.php/supplier/enquiries'?>"><i class="fa fa-circle-o"></i> View Packages Enquiries </a></li> -->
		<!--</ul>
	</li> -->
	<?php } ?>
	<!--<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop"></i>
			<span>CMS</span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/user/banner_images'?>"><i class="fa fa-image"></i> <span>Main Banner Image</span></a></li>
			<li><a href="<?php echo base_url().'index.php/cms/add_cms_page'?>"><i class="fa fa-image"></i> <span>Static Page content</span></a></li>
			
			<!-- Top Destinations START -->
				<?php if ($airline_module) { ?>
				<!--<li class=""><a href="<?php echo base_url().'index.php/cms/flight_top_destinations'?>"><i class="fa fa-plane"></i> <span>Flight Top Destinations</span></a></li>
				<?php } ?>
				<?php if ($accomodation_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/hotel_top_destinations'?>"><i class="fa fa-hotel"></i> <span>Hotel Top Destinations</span></a></li>
				<?php } ?>
				<?php if ($bus_module) { ?>
				<li class=""><a href="<?php echo base_url().'index.php/cms/bus_top_destinations'?>"><i class="fa fa-bus"></i> <span>Bus Top Destinations</span></a></li>
				<?php } ?>
			<!-- Top Destinations END -->
		<!--</ul>
	</li> -->
	<!--<li class="treeview">
			<a href="<?php echo base_url().'index.php/management/bank_account_details'?>">
				<i class="fa fa-bank"></i> <span>Bank Account Details</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
	</li> -->

	<li class="treeview">
			<a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list_booking';?>">
			<i class="fa fa-bar-chart"></i> 
			<span>Manage Booking </span><i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url().'index.php/flight_crs/manage_booking';?>"><i class="fa fa-angle-left pull-right"></i> <span>Manage Booking</span></a></li>
			
		</ul>
	</li>

	<li class="treeview">
			<a href="<?php echo base_url().'index.php/flight_crs/sales_report';?>">
			<i class="fa fa-bar-chart"></i> 
			<span> Sales Report </span><i class="fa fa-angle-left pull-right"></i>
		</a>
	</li>

	<li class="treeview">
			<a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list_booking';?>">
			<i class="fa fa-bar-chart"></i> 
			<span> Flight Report </span><i class="fa fa-angle-left pull-right"></i>
		</a>
	</li>

	<li class="treeview">
			<a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list?show=0&today=1'?>">
				<i class="fa fa-plane"></i> <span>Flight Crs List</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
	</li>


	<li class="treeview">
			<a href="<?php echo base_url().'index.php/flight_crs/flight_crs_list?show=1'?>">
				<i class="fa fa-plane"></i> <span>Airline's List </span> <i class="fa fa-angle-left pull-right"></i>
			</a>
	</li>


<li class="treeview">
			<a href="<?php echo base_url().'index.php/flight_crs/add_flight_list'?>">
				<i class="fa fa-plane"></i> <span>Add New Flight</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
	</li>
	
	<!-- 
	<li class="treeview">
			<a href="<?php //echo base_url().'index.php/utilities/deal_sheets'?>">
				<i class="fa fa-hand-o-right "></i> <span>Deal Sheets</span>
			</a>
	</li>
	 -->
	</ul>
