<!-- HTML BEGIN -->
<div class="bodyContent">
	<div class="panel panel-default clearfix"><!-- PANEL WRAP START -->
		<div class="panel-heading"><!-- PANEL HEAD START -->
			<div class="panel-title"><i class="fa fa-shield"></i> Transaction Logs</div>
		</div>
		<!-- PANEL HEAD START -->
		<div class="panel-body"><!-- PANEL BODY START -->
			<div class="">
				<?php echo $this->pagination->create_links();?> <span class="">Total <?php echo $total_rows ?> Records</span>
			</div>
			<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>Sno</th>
					<th>Type</th>
					<th>Transaction ID</th>
					<th>Fare</th>
					<th>Margin</th>
					<th>Total</th>
					<th>Booking Reference</th>
					<th>Remarks</th>
					<th>User</th>
					<th>Date</th>
					<th>Action</th>
				</tr>
			<?php
			if (valid_array($table_data)) {
				foreach ($table_data as $k => $v) {
					if ($v['created_by_id'] == 0) {
						$user_info = 'Guest';
					} else {
						$user_info = $v['username'];
					}
					$voucher = ''; $transaction_icon = '';
					switch ($v['transaction_type']) {
						case 'flight' :
							$voucher = flight_voucher($v['app_reference'],'0','0');
							$transaction_icon = '<i class="'.get_arrangement_icon(META_AIRLINE_COURSE).'"></i>';
							break;
						case 'hotel' :
							$voucher = hotel_voucher($v['app_reference']);
							$transaction_icon = '<i class="'.get_arrangement_icon(META_ACCOMODATION_COURSE).'"></i>';
							break;
						case 'bus' :
							$voucher = bus_voucher($v['app_reference']);
							$transaction_icon = '<i class="'.get_arrangement_icon(META_BUS_COURSE).'"></i>';
							break;
					}
					$action = $voucher;
				?>
					<tr>
						<td><?=($k+1)?></td>
						<td class="<?=$v['transaction_type'].'-l-bg'?>"><?=$transaction_icon?></td>
						<td><?=$v['system_transaction_id']?></td>
						<th><?=$v['fare']?></th>
						<th><?=$v['profit']?></th>
						<th><?=($v['grand_total']).'-'.$v['currency']?></th>
						<td><?=$v['app_reference']?></td>
						<td><?=$v['remarks']?></td>
						<td><?=$user_info?></td>
						<td><?=app_friendly_date($v['created_datetime'])?></td>
						<td><?=$action?></td>
					</tr>
				<?php
				}
			} else {
				echo '<tr><td>No Data Found</td></tr>';
			}
			?>
			</table>
			</div>
		</div><!-- PANEL BODY END -->
	</div><!-- PANEL END -->
</div>
