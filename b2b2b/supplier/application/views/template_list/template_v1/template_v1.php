<style>
.user-footer .btn-flat {
    padding: 5px 8px;
    margin: 0px 2px;
}
.navbar-nav > .user-menu > .dropdown-menu {
  width: 281px !important;
}
</style>
<?php
$___favicon_ico = $GLOBALS['CI']->template->domain_images('favicon.ico');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<?=$___favicon_ico?>" type="image/x-icon">
  <link rel="icon" href="<?=$___favicon_ico?>" type="image/x-icon">
    <title><?php echo get_app_message( 'AL001'). ' '.HEADER_TITLE_SUFFIX; ?></title>
    <?php //Loading Common CSS and JS
      $this->current_page->header_css_resource();
      $this->current_page->header_js_resource();
      echo $GLOBALS ['CI']->current_page->css ();
    ?>
    <link href="<?php echo $GLOBALS['CI']->template->template_css_dir('bootstrap-toastr/toastr.min.css');?>" rel="stylesheet" defer>
    <script src="<?php echo $GLOBALS['CI']->template->template_js_dir('bootstrap-toastr/toastr.min.js'); ?>"></script>
    
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <!-- Theme style -->
    <link href="<?php echo $this->template->template_css_dir('AdminLTE.min.css')?>" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo $this->template->template_css_dir('_all-skins.min.css')?>" rel="stylesheet" type="text/css" />
  <script>
  var app_base_url = "<?=base_url()?>";
  var tmpl_img_url = '<?=$GLOBALS['CI']->template->template_images(); ?>';
  </script>
  </head>
  <body class="fixed skin-black-light sidebar-mini sidebar-collapse">
  <noscript><img src="<?php echo $GLOBALS['CI']->template->template_images('default_loading.gif'); ?>"
      class="img-responsive center-block"></img></noscript>
    <div class="wrapper">

  <!-- HEADER starts -->  
  <?php 
  //check if the user is loggedin and load respective data
  //START IF - PAGE After LOGIN
  //debug("this is id ".is_logged_in_user());

  //$user_id = @$GLOBALS['CI']->session->userdata (AUTH_USER_POINTER);
 // debug($user_id);
 
  if (!empty(is_logged_in_user())) {
    //error_reporting(E_ALL);
    ?>

      <header class="main-header">
      <!-- Logo -->
        <a href="<?php echo base_url()?>" class="logo bg-white">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?php echo $GLOBALS['CI']->template->domain_images($GLOBALS['CI']->template->get_domain_logo())?>" alt="logo" class="img-responsive center-block"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="<?php echo $GLOBALS['CI']->template->domain_images($GLOBALS['CI']->template->get_domain_logo())?>" alt="logo" class="img-responsive center-block"></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
               <?php
                 if (is_domain_user()) { ?>
                  <!--<li class="">
                    <a href="#"><strong><span>Balance</span> : <span><?php //$balance = current_application_balance(); echo $balance['face_value']?></span></strong></a>
                  </li>-->
              <?php
         }
              ?>

              <li class="dropdown messages-menu hide">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 4 messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <img src="  " class="img-circle" alt="User Image"/>
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="user image"/>
                          </div>
                          <h4>
                            AdminLTE Design Team
                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="user image"/>
                          </div>
                          <h4>
                            Developers
                            <small><i class="fa fa-clock-o"></i> Today</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="user image"/>
                          </div>
                          <h4>
                            Sales Department
                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="user image"/>
                          </div>
                          <h4>
                            Reviewers
                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="get_event_notification">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning" id="active_notifications_count"></span>
                </a>
                <ul class="dropdown-menu">
                  <li>
                  <?php 
                  $notification_loading_image  = '<div class="text-center loader-image"><img src="'.$GLOBALS['CI']->template->template_images('loader_v3.gif').'" alt="Loading........"/></div>';
                  ?>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu" id="notification_dropdown"><?=$notification_loading_image?></ul>
                  </li>
                  <li class="footer hide" id="view_all_notification"><a href="<?=base_url()?>index.php/utilities/notification_list">View more</a></li>
                </ul>
              </li>
              <!-- Tasks: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu ">
                <a href="#" class="dropdown-toggle hide" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 9 tasks</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Create a nice theme
                            <small class="pull-right">40%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">40% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Some task I need to do
                            <small class="pull-right">60%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">60% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Make beautiful transitions
                            <small class="pull-right">80%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">80% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=$GLOBALS['CI']->template->template_images('face.png')?>" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $GLOBALS['CI']->entity_name?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?=$GLOBALS['CI']->template->domain_images($GLOBALS['CI']->template->get_domain_logo())?>" class="img-circle" alt="User Image" />
                    <p>
                      <?=$GLOBALS['CI']->entity_name.' - '.app_friendly_absolute_date($GLOBALS['CI']->entity_date_of_birth)?>
                      <small>Active since <?=app_friendly_absolute_date($GLOBALS['CI']->entity_creation)?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body hide">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url().'index.php/user/account?uid='.intval($GLOBALS['CI']->entity_user_id); ?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                     <div class="pull-left">
                      <a href="<?php echo base_url().'user/change_password?uid='.intval($GLOBALS['CI']->entity_user_id);?>" class="btn btn-default btn-flat">Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url().'index.php/general/initilize_logout'?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li class="hide">
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>

        </nav>
      </header>
      <!-- HEADER ends -->

      <!-- MENU starts -->
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            
            <div class="pull-left info">
              <p><?php echo $GLOBALS['CI']->entity_name;?></p>
              <a href="#">Supplier</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <?php include_once 'menu/magical_menu.php';?>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- MENU ends -->
  
      <!-- BODY CONTENT starts -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!--<section class="content-header">
          <h1>
            <a href="<?php echo base_url()?>">Dashboard - <?php echo date('Y')?></a>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Need Dynamic Links to be Activated</a></li>
            <li class="active">breadcrums</li>
          </ol>
        </section>
        --><!-- Main content -->
        <section class="content">
        <!-- UTILITY NAV -->
    <div class="container-fluid utility-nav clearfix">
      <!-- ROW -->
      <?php 
        if($this->session->flashdata('message')!="") {
          $message = $this->session->flashdata('message');
          $msg_type = $this->session->flashdata('type');
          $_override_app_msg = $this->session->flashdata('override_app_msg');
          if(empty($_override_app_msg) == false) {
            $override_app_msg = true;
          } else {
            $override_app_msg = false;
          }
        
          $toastr_msg = extract_message($message, $override_app_msg);
          $toastr = get_toastr_index($msg_type);
          ?>
          <script>
            toastr.<?=$toastr;?>("<?=$toastr_msg?>");
          </script>
        <?php
        }
      ?>
      <!-- /ROW -->
    </div>
          <!-- Info boxes -->
          <div class="row">
            <?php echo $body ?>
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <!-- BODY CONTENT ends -->

      <!-- FOOTER starts -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.0
        </div>
        <strong>Copyright &copy; <?php echo date('Y')?> <?=HEADER_TITLE_SUFFIX?> </strong> All rights reserved.
      </footer>
      <!-- FOOTER ends -->
      <?php include_once 'menu/support_privilege_helper.php';
      
    //END IF - PAGE After LOGIN
    } else {
      //Page without LOGIN
      echo $body;
    }
    ?>
    </div><!-- ./wrapper -->
  <?php
  Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/datepicker.js'), 'defer' => 'defer');
  Provab_Page_Loader::load_core_resource_files();
  echo $GLOBALS ['CI']->current_page->js ();
  ?>
  <script src='<?php echo SYSTEM_RESOURCE_LIBRARY; ?>/fastclick/fastclick.min.js'></script>
    <!-- Sparkline -->
    <script src="<?php echo SYSTEM_RESOURCE_LIBRARY?>/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="<?php echo SYSTEM_RESOURCE_LIBRARY;?>/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    </body>
</html>
