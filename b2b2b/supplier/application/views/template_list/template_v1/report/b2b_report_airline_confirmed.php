<script src="<?php echo SYSTEM_RESOURCE_LIBRARY?>/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo SYSTEM_RESOURCE_LIBRARY?>/datatables/dataTables.bootstrap.min.js"></script>

<?php
if (is_array($search_params)) {
	extract($search_params);
}
$_datepicker = array(array('created_datetime_from', PAST_DATE), array('created_datetime_to', PAST_DATE));
$this->current_page->set_datepicker($_datepicker);
$this->current_page->auto_adjust_datepicker(array(array('created_datetime_from', 'created_datetime_to')));
//debug($table_data['booking_details']); 
?>

<div class="bodyContent col-md-12">
	<div class="panel panel-default clearfix"><!-- PANEL WRAP START -->
		<div class="panel-heading"><!-- PANEL HEAD START -->
			
		</div><!-- PANEL HEAD START -->
		<div class="panel-body">
			
			
		</div>
		<div class="clearfix table-responsive"><!-- PANEL BODY START -->
					
					<table class="table table-condensed table-bordered" id="b2b_report_airline_table">
						<thead>
							<tr>
								<th>Sno</th>
								
								<th>Lead Pax</th>
								<th>From</th>
								<th>To</th>
								<th>Type</th>
								<th>BookedOn</th>
								<th>JourneyDate</th>
								<th>PNR</th>
								
								
								
								
								<th>TotalFare</th>
								<th>Status</th>
								
							</tr>
						</thead>
						<tbody>
						<?php
							
							if(valid_array($table_data['booking_details']) == true) {

				        		$booking_details = $table_data['booking_details'];

								$segment_3 = $GLOBALS['CI']->uri->segment(3);
								$current_record = (empty($segment_3) ? 1 : $segment_3);

					        	foreach($booking_details as $parent_k => $parent_v) {
					        		$crs_status=$parent_v['crs_status'];
					        		$booked_on=$parent_v['created_datetime']; 
					        		$flight_orgin=$parent_v['booking_itinerary_details'];
					        		//debug($flight_orgin);
					        		
					        		
					        		
					        		extract($parent_v);
					        		
					        		if($booking_source == CRS_FLIGHT_BOOKING_SOURCE && $status=="BOOKING_CONFIRMED")
					        		{

					        			$booked_on; 
					        		 $minutes_to_add = 30;
					        		 $time = new DateTime($booked_on);
                                     $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
					        			$expiry_booking = $time->format('Y-m-d h:i:s'); 
                                      $hold_date=date('Y-m-d h:i:s');    

                                      if($status=="BOOKING_HOLD" && $expiry_booking < $hold_date )
					        		{
					        			
					        			$requests ['status'] = "BOOKING_INACTIVE";
				                        $requests ['crs_status'] = "BOOKING_INACTIVE";
                                  $this->custom_db->update_record ( 'flight_booking_details', $requests, array (
					                   'app_reference' => $app_reference 
			                         ) );
              

                               $requestss ['status'] = "BOOKING_INACTIVE";

			$this->custom_db->update_record ( 'flight_booking_passenger_details', $requestss, array (
					'app_reference' => $app_reference 
			) );

              $requestsing ['status'] = "BOOKING_INACTIVE";
			$this->custom_db->update_record ( 'flight_booking_transaction_details', $requestsing, array (
					'app_reference' => $app_reference 
			) ); 

					        		}

					        		foreach ($flight_orgin as $value_flight) {
					        			$attr=json_decode($value_flight['attributes']);
					        			foreach ($attr as $value_attr) {
					        				
					        		$flight_orgin=$value_attr->orgin;
					        				$jourany_date=$value_attr->jourany_date;
					        				if($flight_orgin==$this->uri->segment(4) && $jourany_date==$this->uri->segment(5) )
					        				{ 
					        					$action = '';
									$cancellation_btn = '';
									$voucher_btn = '';
								
									$booked_by = '';
									
									//Status Update Button
									/*if (in_array($status, array('BOOKING_CONFIRMED')) == false) {
										switch ($booking_source) {
											case PROVAB_FLIGHT_BOOKING_SOURCE :
												$status_update_btn = '<button class="btn btn-success btn-sm update-source-status" data-app-reference="'.$app_reference.'"><i class="fa fa-database"></i> Update Status</button>';
												break;
										}
									}*/
									
									
									
									$voucher_btn = flight_voucher($app_reference, $booking_source, $status);
									$invoice = flight_invoice($app_reference, $booking_source, $status);
									

									
									//$cancel_btn = flight_cancel_request($app_reference, $booking_source, $status);
									//$pdf_btn= flight_pdf($app_reference, $booking_source, $status);
									$update_ticket=flight_update_voucher($app_reference, $booking_source, $status);
									$email_btn = flight_voucher_email($app_reference, $booking_source,$status,$email);
									$jrny_date = date('Y-m-d', strtotime($journey_start));
									$tdy_date = date ( 'Y-m-d' );
									$diff = get_date_difference($tdy_date,$jrny_date);
					        		
									
									
									
									
								?>
									<tr>
										<td><?=($current_record++)?></td>
										
										<td>
										<?php echo $lead_pax_name;
										 ?>
										</td>
										<td><?php echo $from_loc?></td>
										<td><?php echo $to_loc?></td>
										<td><?php echo $trip_type_label?></td>
										<td><?php echo date('d-m-Y', strtotime($booked_date))?></td>
										<td><?php echo date('d-m-Y', strtotime($journey_start))?></td>
										<td><?=@$pnr?></td>
										
										
										
										
										<td><?php echo $grand_total?></td>
										<td><span class="<?php echo booking_status_label($status) ?>"><?php echo $status?></span></td>
										
									</tr>
									<?php
					        				}
					        				
					        			}
					        			
					        		}
					        		


					        			
								
					        		}
					        				        		
					        		
									
								}
							} else {
								echo '<tr><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td>
										   <td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td>
										   <td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td></tr>';
							}
						?>
						</tbody>
					</table>
				
		</div>
	</div>
</div>

<?php
function get_accomodation_cancellation($courseType, $refId)
{
	return '<a href="'.base_url().'index.php/booking/accomodation_cancellation?courseType='.$courseType.'&refId='.$refId.'" class="btn btn-sm btn-danger "><i class="fa fa-exclamation-triangle"></i> Cancel</a>';
}
function update_booking_details($app_reference, $booking_source,$booking_status)
{
	
	return '<a class="btn btn-danger update_flight_booking_details" data-app-reference="'.$app_reference.'" data-booking-source="'.$booking_source.'"data-booking-status="'.$booking_status.'">Update PNR Details</a>';
}
function flight_voucher_email($app_reference, $booking_source,$status,$recipient_email)
{

	return '<a class="btn btn-sm btn-primary send_email_voucher fa fa-envelope-o" data-app-status="'.$status.'"   data-app-reference="'.$app_reference.'" data-booking-source="'.$booking_source.'"data-recipient_email="'.$recipient_email.'">Email Voucher</a>';
}
function get_cancellation_details_button($app_reference, $booking_source, $master_booking_status, $booking_customer_details)
{
	$status = 'BOOKING_CONFIRMED';
	if($master_booking_status == 'BOOKING_CANCELLED'){
		$status = 'BOOKING_CANCELLED';
	} else{
		foreach($booking_customer_details as $tk => $tv){
			foreach($tv['booking_customer_details'] as $pk => $pv){
				if($pv['status'] == 'BOOKING_CANCELLED'){
					$status = 'BOOKING_CANCELLED';
					break;
				}
			}
		}
	}
	if($status == 'BOOKING_CANCELLED'){
		return '<a target="_blank" href="'.base_url().'index.php/flight/ticket_cancellation_details?app_reference='.$app_reference.'&booking_source='.$booking_source.'&status='.$master_booking_status.'" class="col-md-12 btn btn-sm btn-info "><i class="fa fa-info"></i> Cancellation Details</a>';
	}
}
?>

