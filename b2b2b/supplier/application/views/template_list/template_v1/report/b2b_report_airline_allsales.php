<script src="<?php echo SYSTEM_RESOURCE_LIBRARY?>/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo SYSTEM_RESOURCE_LIBRARY?>/datatables/dataTables.bootstrap.min.js"></script>
<?php
if (is_array($search_params)) {
	extract($search_params);
}
$_datepicker = array(array('created_datetime_from', PAST_DATE), array('created_datetime_to', PAST_DATE));
$this->current_page->set_datepicker($_datepicker);
$this->current_page->auto_adjust_datepicker(array(array('created_datetime_from', 'created_datetime_to')));
//debug($table_data['booking_details']); 
foreach($flight_name as $name)
{
	$flight_ids[]=$name['orgin'];
} 
?>
<script type="text/JavaScript">
         setTimeout(function(){
   window.location.reload();
}, 900000);
         //-->
      </script>
<div class="bodyContent col-md-12">
	<div class="panel panel-default clearfix"><!-- PANEL WRAP START -->
		<div class="panel-heading"><!-- PANEL HEAD START -->
			<?=$GLOBALS['CI']->template->isolated_view('report/report_tab_b2b')?>
		</div><!-- PANEL HEAD START -->
		<div class="panel-body">
			<h4>Search Panel</h4>
			<hr>
			<form method="GET" autocomplete="off">
				<input type="hidden" name="created_by_id" value="<?=@$created_by_id?>" >
				<div class="clearfix form-group">
					
					
					<div class="col-xs-4">
						<label>
						Status
						</label>
						<select class="form-control" name="status">
							<option>All</option>
							<?=generate_options($status_options, array(@$status))?>
						</select>
					</div>
					<div class="col-xs-4">
						<label>
						Booked From Date
						</label>
						<input type="text" readonly id="created_datetime_from" class="form-control" name="created_datetime_from" value="<?=@$created_datetime_from?>" placeholder="Request Date">
					</div>
					<div class="col-xs-4">
						<label>
						Booked To Date
						</label>
						<input type="text" readonly id="created_datetime_to" class="form-control disable-date-auto-update" name="created_datetime_to" value="<?=@$created_datetime_to?>" placeholder="Request Date">
					</div>
				</div>
				<div class="col-sm-12 well well-sm">
				<button type="submit" class="btn btn-primary">Search</button> 
				<button type="reset" class="btn btn-warning">Reset</button>
				</div>
			</form>
			 <div class="col-sm-12 well well-sm">
				

                               <a href="<?php echo base_url(); ?>index.php/report/export_excel_all/" >
					<button type="submit" class="btn btn-primary">Export to Excel</button> 
				</a>
				
				
				</div>
		</div>
		<div class="clearfix table-responsive"><!-- PANEL BODY START -->
					<!-- <div class="pull-left">
						<?php //echo $this->pagination->create_links();?> <span class="">Total <?php //echo $total_rows ?> Bookings</span>
					</div> -->
					<table class="table table-condensed table-bordered" id="b2b_report_airline_table">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Application Reference</th>
								<th class="hide">Agency Name</th>
								<th>Lead Pax <br/>Details</th>
								<th>From</th>
								<th>To</th>
								<th>Type</th>
								<th>BookedOn</th>
								<th>JourneyDate</th>
								<th>PNR</th>
								
								<th class="hide">Admin NetFare</th>
								
								<th>TotalFare</th>
								<th>Status</th>
								
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Sno</th>
								<th>Application Reference</th>
								<th class="hide">Agency Name</th>
								<th>Lead Pax <br/>Details</th>
								<th>From</th>
								<th>To</th>
								<th>Type</th>
								<th>BookedOn</th>
								<th>JourneyDate</th>
								<th>PNR</th>
								
								<th class="hide">Admin NetFare</th>
								
								<th>TotalFare</th>
								<th>Status</th>
								
							</tr>
						</tfoot><tbody>
						<?php
							
							if(valid_array($table_data['booking_details']) == true) {

				        		$booking_details = $table_data['booking_details'];

								$segment_3 = $GLOBALS['CI']->uri->segment(3);
								$current_record = (empty($segment_3) ? 1 : $segment_3);

					        	foreach($booking_details as $parent_k => $parent_v) {
					        		$crs_status=$parent_v['crs_status'];
					        		$booked_on=$parent_v['created_datetime']; 
					        		$flight_orgin=$parent_v['booking_itinerary_details'];
					        		$flight_trr=$parent_v['booking_transaction_details'];
					        		foreach ($flight_trr as $value_atrr) {
					        			$attr_trr=json_decode($value_atrr['attributes']);
					        		
					        		}
					        		foreach ($attr_trr as  $valuess) {
					        		
					        		
					        		
					        		extract($parent_v);
					        		
					        		if($booking_source == "CRS_FLIGHT_BOOKING_S")
					        		//if($booking_source == CRS_FLIGHT_BOOKING_SOURCE)
					        		{
					        			if($status == "BOOKING_CONFIRMED"|| $status == "BOOKING_FLOWN" )
                                        {
                                        	$booked_on; 
					        		 $minutes_to_add = 30;
					        		 $time = new DateTime($booked_on);
                                     $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
					        			$expiry_booking = $time->format('Y-m-d h:i:s'); 
                                      $hold_date=date('Y-m-d h:i:s');    

                                     

					        		foreach ($flight_orgin as $value_flight) {
					        			$attr=json_decode($value_flight['attributes']);
					        			foreach ($attr as $value_attr) {
					        				
					        		$flight_orgin=$value_attr->orgin;
					        				$jourany_date=$value_attr->jourany_date;
					        				if(in_array($flight_orgin,$flight_ids))
					        				{ 
					        					$action = '';
									$cancellation_btn = '';
									$voucher_btn = '';
								
									$booked_by = '';
									
									//Status Update Button
									/*if (in_array($status, array('BOOKING_CONFIRMED')) == false) {
										switch ($booking_source) {
											case PROVAB_FLIGHT_BOOKING_SOURCE :
												$status_update_btn = '<button class="btn btn-success btn-sm update-source-status" data-app-reference="'.$app_reference.'"><i class="fa fa-database"></i> Update Status</button>';
												break;
										}
									}*/
									
									
									
									$voucher_btn = flight_voucher($app_reference, $booking_source, $status);
									$invoice = flight_invoice($app_reference, $booking_source, $status);
									//$cancel_btn = flight_cancel_request($app_reference, $booking_source, $status);
									//$pdf_btn= flight_pdf($app_reference, $booking_source, $status);
									$update_ticket=flight_update_voucher($app_reference, $booking_source, $status);
									$email_btn = flight_voucher_email($app_reference, $booking_source,$status,$email);
									$jrny_date = date('Y-m-d', strtotime($journey_start));
									$tdy_date = date ( 'Y-m-d' );
									$diff = get_date_difference($tdy_date,$jrny_date);
					        		$action .= $voucher_btn;
					        		$action .=  '<br />'.$update_ticket;
					        		//$action .=  '<br />'.$pdf_btn;
					        		$action .=  '<br />'.$email_btn;
									if($crs_status=='BOOKING_CANCEL_REQUEST')
									{
										$cancel_btn = flight_cancel_request_supplier($app_reference, $booking_source, $status,$crs_status);
										$action .= '<br />'.$cancel_btn;
									}
									
									
									
									
									$action .= get_cancellation_details_button($parent_v['app_reference'], $parent_v['booking_source'], $parent_v['status'], $parent_v['booking_transaction_details']);
								?>
									<tr>
										<td><?=($current_record++)?></td>
										<td><?php echo $app_reference;?></td>
										<td class="hide"><?php echo $agency_name;?></td>
										<td>
										<?php echo $lead_pax_name. '<br/>'.
										  $email."<br/>".
										  $phone;?>
										</td>
										<td><?php echo $from_loc?></td>
										<td><?php echo $to_loc?></td>
										<td><?php echo $trip_type_label?></td>
										<td><?php echo date('d-m-Y', strtotime($booked_date))?></td>
										<td><?php echo date('d-m-Y', strtotime($journey_start))?></td>
										<td><?=@$pnr?></td>
										
										<td><?php echo $valuess?></td>
										
										<td class="hide"><?php echo $valuess?></td>
										<td><span class="<?php echo booking_status_label($status) ?>"><?php echo $status?></span></td>
										
									</tr>
									<?php
					        				}
					        				
					        			}
					        			
					        		}
					        		


                                        }
					        			

					        			
								
					        		}
					        		}
					        		
					        		
					        		
					        		
					        				        		
					        		
									
								}
							} else {
								echo '<tr><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td>
										   <td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td>
										   <td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td></tr>';
							}
						?>
						</tbody>
					</table>
				
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	//update-source-status update status of the booking from api
	$(document).on('click', '.update-source-status', function(e) {
		e.preventDefault();
		$(this).attr('disabled', 'disabled');//disable button
		var app_ref = $(this).data('app-reference');
		$.get(app_base_url+'index.php/flight/get_booking_details/'+app_ref, function(response) {
			console.log(response);
		});
	});
	/*$('.update_flight_booking_details').on('click', function(e) {
		e.preventDefault();
		var _user_status = this.value;
		var _opp_url = app_base_url+'index.php/report/update_flight_booking_details/';
		_opp_url = _opp_url+$(this).data('app-reference')+'/'+$(this).data('booking-source');
		toastr.info('Please Wait!!!');
		$.get(_opp_url, function() {
			toastr.info('Updated Successfully!!!');
		});
	});*/
		$('.update_flight_booking_details').on('click', function(e) {
		e.preventDefault();
		var _user_status = this.value;
		var _opp_url = app_base_url+'index.php/report/update_pnr_details/';
		_opp_url = _opp_url+$(this).data('app-reference')+'/'+$(this).data('booking-source')+'/'+$(this).data('booking-status');
		toastr.info('Please Wait!!!');
		$.get(_opp_url, function() {
			toastr.info('Updated Successfully!!!');
			location.reload();
		});
	});

	//send the email voucher
	$('.send_email_voucher').on('click', function(e) {
		$("#mail_voucher_modal").modal('show');
		$('#mail_voucher_error_message').empty();
        email = $(this).data('recipient_email');
		$("#voucher_recipient_email").val(email);
        app_reference = $(this).data('app-reference');
        book_reference = $(this).data('booking-source');
        app_status = $(this).data('app-status');
	  $("#send_mail_btn").off('click').on('click',function(e){
		  email = $("#voucher_recipient_email").val();
		  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		  if(email != ''){
			  if(!emailReg.test(email)){
				  $('#mail_voucher_error_message').empty().text('Please Enter Correct Email Id');
                     return false;    
				      }
		      
					var _opp_url = app_base_url+'index.php/voucher/flight/';
					_opp_url = _opp_url+app_reference+'/'+book_reference+'/'+app_status+'/email_voucher/'+email;
					toastr.info('Please Wait!!!');
					$.get(_opp_url, function() {
						
						toastr.info('Email sent  Successfully!!!');
						$("#mail_voucher_modal").modal('hide');
					});
		  }else{
			  $('#mail_voucher_error_message').empty().text('Please Enter Email ID');
		  }
	  });

});
	
});
</script>
<?php
function get_accomodation_cancellation($courseType, $refId)
{
	return '<a href="'.base_url().'index.php/booking/accomodation_cancellation?courseType='.$courseType.'&refId='.$refId.'" class="btn btn-sm btn-danger "><i class="fa fa-exclamation-triangle"></i> Cancel</a>';
}
function update_booking_details($app_reference, $booking_source,$booking_status)
{
	
	return '<a class="btn btn-danger update_flight_booking_details" data-app-reference="'.$app_reference.'" data-booking-source="'.$booking_source.'"data-booking-status="'.$booking_status.'">Update PNR Details</a>';
}
function flight_voucher_email($app_reference, $booking_source,$status,$recipient_email)
{

	return '<a class="btn btn-sm btn-primary send_email_voucher fa fa-envelope-o" data-app-status="'.$status.'"   data-app-reference="'.$app_reference.'" data-booking-source="'.$booking_source.'"data-recipient_email="'.$recipient_email.'">Email Voucher</a>';
}
function get_cancellation_details_button($app_reference, $booking_source, $master_booking_status, $booking_customer_details)
{
	$status = 'BOOKING_CONFIRMED';
	if($master_booking_status == 'BOOKING_CANCELLED'){
		$status = 'BOOKING_CANCELLED';
	} else{
		foreach($booking_customer_details as $tk => $tv){
			foreach($tv['booking_customer_details'] as $pk => $pv){
				if($pv['status'] == 'BOOKING_CANCELLED'){
					$status = 'BOOKING_CANCELLED';
					break;
				}
			}
		}
	}
	if($status == 'BOOKING_CANCELLED'){
		return '<a target="_blank" href="'.base_url().'index.php/flight/ticket_cancellation_details?app_reference='.$app_reference.'&booking_source='.$booking_source.'&status='.$master_booking_status.'" class="col-md-12 btn btn-sm btn-info "><i class="fa fa-info"></i> Cancellation Details</a>';
	}
}
?>
<script>
$(document).ready(function() {
    $('#b2b_report_airline_table').DataTable({
        // Disable initial sort 
        "aaSorting": []
    });
});
</script>
