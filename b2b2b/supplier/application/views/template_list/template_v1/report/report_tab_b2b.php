<?php
$active_domain_modules = $GLOBALS ['CI']->active_domain_modules;
//$master_module_list = $GLOBALS ['CI']->config->item ( 'master_module_list' );
$master_module_list = array('flight');

$query_string = '';
if (isset($_GET['created_by_id']) == true) {
	$query_string = 'created_by_id='.$_GET['created_by_id'];
}
?>
<ul class="nav nav-tabs">
<?php
$method = $GLOBALS['CI']->uri->segment('2');
foreach ($master_module_list as $k => $v) {
	if (in_array ( $k, $active_domain_modules )) {
		if ($method == 'b2b_'.$v.'_report') {
			$act_tab = 'active';
		} else {
			$act_tab = '';
		}
	?>
		<li role="presentation" class="<?=$act_tab?>"><a href=""><i class="<?=get_arrangement_icon($k)?>"></i> <?='B2B '.$v.' Report'?></a></li>
	<?php
	}
}
?>
</ul>
