<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/** *
 * @package Provab - Provab Application
 * @subpackage Travel Portal
 * @author Pravinkumar<pravinkuamr.provab@gmail.com>
 * @version V1
 */
class Cron extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('hotel_model');
		$this->load->model('flight_model');
		$this->load->model('bus_model');
		
	}
	public function daily_report(){
		
		$tmp_bus_booking = $this->bus_model->get_daily_booking_summary();
		//debug($tmp_bus_booking);exit;
		$tmp_hotel_booking = $this->hotel_model->get_monthly_booking_summary();
		$tmp_flight_booking = $this->flight_model->get_monthly_booking_summary();
		
		$this->template->view('report/daily_report');
	}

	public function hold_report(){
		
		$tmp_booking = $this->flight_model->get_monthly_booking_summary();
		
	}


}
