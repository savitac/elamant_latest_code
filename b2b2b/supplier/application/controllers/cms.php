<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
/**
 *
 * @package Provab - Provab Application
 * @subpackage Travel Portal
 * @author Arjun J Gowda<arjunjgowda260389@gmail.com>
 * @version V2
 */
class Cms extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'module_model' );
	}
	/**
	 * Manage Hotel Top Destinations
	 */
	function hotel_top_destinations($offset = 0) {
		// Search Params(Country And City)
		// CMS - Image(On Home Page)
		$page_data = array ();
		$post_data = $this->input->post ();
		if (valid_array ( $post_data ) == true) {
			$city_origin = $post_data ['city'];
			// FILE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {
				$config ['upload_path'] = $this->template->domain_image_upload_path ();
				$temp_file_name = $_FILES ['top_destination'] ['name'];
				$config ['allowed_types'] = '*';
				$config ['file_name'] = 'top-dest-hotel-' . $city_origin;
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;
				// UPDATE
				$temp_record = $this->custom_db->single_table_records ( 'api_city_list', 'image', array (
						'origin' => $city_origin 
				) );
				$top_destination_image = $temp_record ['data'] [0] ['image'];
				// DELETE OLD FILES
				if (empty ( $top_destination_image ) == false) {
					$temp_top_destination_image = $this->template->domain_image_full_path ( $top_destination_image ); // GETTING FILE PATH
					if (file_exists ( $temp_top_destination_image )) {
						unlink ( $temp_top_destination_image );
					}
				}
				// UPLOAD IMAGE
				$this->load->library ( 'upload', $config );
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'top_destination' )) {
					echo $this->upload->display_errors ();
				} else {
					$image_data = $this->upload->data ();
				}
				// debug($image_data);exit;
				$this->custom_db->update_record ( 'api_city_list', array (
						'top_destination' => ACTIVE,
						'image' => $image_data ['file_name'] 
				), array (
						'origin' => $city_origin 
				) );
				set_update_message ();
			}
			refresh ();
		}
		$filter = array (
				'top_destination' => ACTIVE 
		);
		$country_list = $this->custom_db->single_table_records ( 'api_country_list', 'name,origin', array (
				'name !=' => '' 
		), 0, 1000, array (
				'name' => 'ASC' 
		) );
		$data_list = $this->custom_db->single_table_records ( 'api_city_list', '*', $filter, 0, 100000, array (
				'top_destination' => 'DESC',
				'destination' => 'ASC' 
		) );
		
		if ($country_list ['status'] == SUCCESS_STATUS) {
			$page_data ['country_list'] = magical_converter ( array (
					'k' => 'origin',
					'v' => 'name' 
			), $country_list );
		}
		
		$page_data ['data_list'] = @$data_list ['data'];
		$this->template->view ( 'cms/hotel_top_destinations', $page_data );
	}
	/*
	 * Deactivate Top Destination
	 */
	function deactivate_top_destination($origin) {
		$status = INACTIVE;
		$info = $this->module_model->update_top_destination ( $status, $origin );
		redirect ( base_url () . 'cms/hotel_top_destinations' );
	}
	/**
	 * Manage Bus Top Destinations
	 */
	function bus_top_destinations($offset = 0) {
		// Search Params(Country And City)
		// CMS - Image(On Home Page)
		$page_data = array ();
		$post_data = $this->input->post ();
		if (valid_array ( $post_data ) == true) {
			$city_origin = $post_data ['city'];
			// FILE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {
				$config ['upload_path'] = $this->template->domain_image_upload_path ();
				$temp_file_name = $_FILES ['top_destination'] ['name'];
				$config ['allowed_types'] = '*';
				$config ['file_name'] = 'top-dest-bus-' . $city_origin;
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;
				// UPDATE
				$temp_record = $this->custom_db->single_table_records ( 'bus_stations', 'image', array (
						'origin' => $city_origin 
				) );
				$top_destination_image = $temp_record ['data'] [0] ['image'];
				// DELETE OLD FILES
				if (empty ( $top_destination_image ) == false) {
					$temp_top_destination_image = $this->template->domain_image_full_path ( $top_destination_image ); // GETTING FILE PATH
					if (file_exists ( $temp_top_destination_image )) {
						unlink ( $temp_top_destination_image );
					}
				}
				// UPLOAD IMAGE
				$this->load->library ( 'upload', $config );
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'top_destination' )) {
					echo $this->upload->display_errors ();
				} else {
					$image_data = $this->upload->data ();
				}
				// debug($image_data);exit;
				$this->custom_db->update_record ( 'bus_stations', array (
						'top_destination' => ACTIVE,
						'image' => $image_data ['file_name'] 
				), array (
						'origin' => $city_origin 
				) );
				set_update_message ();
			}
			refresh ();
		}
		$filter = array (
				'top_destination' => ACTIVE 
		);
		$bus_list = $this->custom_db->single_table_records ( 'bus_stations', 'name,origin', array (
				'name !=' => '' 
		), 0, 1000, array (
				'name' => 'ASC' 
		) );
		$data_list = $this->custom_db->single_table_records ( 'bus_stations', '*', $filter, 0, 100000, array (
				'top_destination' => 'DESC',
				'name' => 'ASC' 
		) );
		
		if ($bus_list ['status'] == SUCCESS_STATUS) {
			$page_data ['bus_list'] = magical_converter ( array (
					'k' => 'origin',
					'v' => 'name' 
			), $bus_list );
		}
		
		$page_data ['data_list'] = @$data_list ['data'];
		$this->template->view ( 'cms/bus_top_destinations', $page_data );
	}
	/**
	 * Deactivate Top Bus Destination
	 */
	function deactivate_bus_top_destination($origin) {
		$status = INACTIVE;
		$info = $this->module_model->update_bus_top_destination ( $status, $origin );
		redirect ( base_url () . 'cms/bus_top_destinations' );
	}
	
	/**
	 * Manage Flight Top Destinations
	 */
	function flight_top_destinations($offset = 0) {
		// Search Params(Country And City)
		// CMS - Image(On Home Page)
		$page_data = array ();
		$post_data = $this->input->post ();
		if (valid_array ( $post_data ) == true) {
			$aiport_origin = $post_data ['airport'];
			// FILE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['top_destination'] ['error'] == 0 and $_FILES ['top_destination'] ['size'] > 0) {
				$config ['upload_path'] = $this->template->domain_image_upload_path ();
				$temp_file_name = $_FILES ['top_destination'] ['name'];
				$config ['allowed_types'] = '*';
				$config ['file_name'] = 'top-dest-fight-' . $aiport_origin;
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;
				// UPDATE
				$temp_record = $this->custom_db->single_table_records ( 'flight_airport_list', 'image', array (
						'origin' => $aiport_origin
				) );
				$top_destination_image = $temp_record ['data'] [0] ['image'];
				// DELETE OLD FILES
				if (empty ( $top_destination_image ) == false) {
					$temp_top_destination_image = $this->template->domain_image_full_path ( $top_destination_image ); // GETTING FILE PATH
					if (file_exists ( $temp_top_destination_image )) {
						unlink ( $temp_top_destination_image );
					}
				}
				// UPLOAD IMAGE
				$this->load->library ( 'upload', $config );
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'top_destination' )) {
					echo $this->upload->display_errors ();
				} else {
					$image_data = $this->upload->data ();
				}
				// debug($image_data);exit;
				$this->custom_db->update_record ( 'flight_airport_list', array (
						'top_destination' => ACTIVE,
						'image' => $image_data ['file_name']
				), array (
						'origin' => $aiport_origin
				) );
				set_update_message ();
			}
			refresh ();
		}
		$filter = array (
				'top_destination' => ACTIVE
		);
		$flight_list = $this->custom_db->single_table_records ( 'flight_airport_list', 'airport_city,origin', array (
				'airport_city !=' => ''
		), 0, 1000, array (
				'airport_city' => 'ASC'
		) );
		$data_list = $this->custom_db->single_table_records ( 'flight_airport_list', '*', $filter, 0, 100000, array (
				'top_destination' => 'DESC',
				'airport_city' => 'ASC'
		) );
	
		if ($flight_list ['status'] == SUCCESS_STATUS) {
			$page_data ['flight_list'] = magical_converter ( array (
					'k' => 'origin',
					'v' => 'airport_city'
			), $flight_list );
		}
	
		$page_data ['data_list'] = @$data_list ['data'];
		$this->template->view ( 'cms/flight_top_destinations', $page_data );
	}
	/**
	 * Deactivate Top Bus Destination
	 */
	function deactivate_flight_top_destination($origin) {
		$status = INACTIVE;
		$info = $this->module_model->update_flight_top_destination ( $status, $origin );
		redirect ( base_url () . 'cms/flight_top_destinations' );
	}
	/**
	 * Static Page Content
	 */
	function add_cms_page($id = '') {
		
		// privilege_handler('p54');
		$this->form_validation->set_message ( 'required', 'Required.' );
		
		// check for negative id
		valid_integer ( $id );
		
		// validation rules
		$post_data = $this->input->post ();
		// get data
		$cols = ' * ';
		if (valid_array ( $post_data ) == false) {
			if (intval ( $id ) > 0) {
				// edit data
				$tmp_data = $this->custom_db->single_table_records ( 'cms_pages', '', array (
						'page_id' => $id 
				) );
				// debug($tmp_data);exit;
				if (valid_array ( $tmp_data ['data'] [0] )) {
					$data ['page_title'] = $tmp_data ['data'] [0] ['page_title'];
					$data ['page_description'] = $tmp_data ['data'] [0] ['page_description'];
					$data ['page_seo_title'] = $tmp_data ['data'] [0] ['page_seo_title'];
					$data ['page_seo_keyword'] = $tmp_data ['data'] [0] ['page_seo_keyword'];
					$data ['page_seo_description'] = $tmp_data ['data'] [0] ['page_seo_description'];
					$data ['page_position'] = $tmp_data ['data'] [0] ['page_position'];
				} else {
					redirect ( 'cms/add_cms_page' );
				}
			}
		} elseif (valid_array ( $post_data )) {
			$this->form_validation->set_rules ( 'page_title', 'Page Title', 'required' );
			$this->form_validation->set_rules ( 'page_description', 'Page Description', 'required' );
			$this->form_validation->set_rules ( 'page_seo_title', 'Page SEO Title', 'required' );
			$this->form_validation->set_rules ( 'page_seo_keyword', 'Page SEO Keyword', 'required' );
			$this->form_validation->set_rules ( 'page_seo_description', 'Page SEO Description', 'required' );
			$this->form_validation->set_rules ( 'page_position', 'Page Position', 'required' );
			
			$data ['page_title'] = $this->input->post ( 'page_title' );
			$data ['page_description'] = $this->input->post ( 'page_description' );
			$data ['page_seo_title'] = $this->input->post ( 'page_seo_title' );
			$data ['page_seo_keyword'] = $this->input->post ( 'page_seo_keyword' );
			$data ['page_seo_description'] = $this->input->post ( 'page_seo_description' );
			$data ['page_position'] = $this->input->post ( 'page_position' );
			if ($this->form_validation->run ()) {
				// add / update data
				if (intval ( $id ) > 0) {
					$this->custom_db->update_record ( 'cms_pages', $data, array (
							'page_id' => $id 
					) );
				} else {
					$this->custom_db->insert_record ( 'cms_pages', $data );
				}
				redirect ( 'cms/add_cms_page' );
			}
		}
		$data ['ID'] = $id;
		// get all sub admin
		$tmp_data = $this->custom_db->single_table_records ( 'cms_pages', $cols );
		$data ['sub_admin'] = '';
		$data ['sub_admin'] = $tmp_data ['data'];
		$this->template->view ( 'cms/add_cms_page', $data );
	}
	/**
	 * Status update of Static Page Content
	 */
	function cms_status($id = '', $status = 'D') {
		if ($id > 0) {
			if (strcmp ( $status, 'D' ) == 0) {
				$status = 0;
			} else {
				$status = 1;
			}
			
			$this->custom_db->update_record ( 'cms_pages', array (
					'page_status' => $status 
			), array (
					'page_id' => $id 
			) );
		}
		redirect ( 'cms/add_cms_page' );
	}
}