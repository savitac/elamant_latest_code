<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package    Provab
 * @subpackage General
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */

class General extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
		$this->load->model('user_model');
	}

	/**
	 * index page of application will be loaded here
	 */
	function index()
	{
		if (is_logged_in_user()) { 
			//$this->load->view('dashboard/reminder');
			redirect(base_url().'index.php/menu/index');
		} else {

			//show login
		
			echo $this->template->view('general/login');
			
		}
	}

	/**
	 * Logout function for logout from account and unset all the session variables
	 */
	function initilize_logout() {
		if (is_logged_in_user()) {
			$this->user_model->update_login_manager($this->session->userdata(LOGIN_POINTER));
			$this->session->unset_userdata(array(AUTH_USER_POINTER => '',LOGIN_POINTER => '', DOMAIN_AUTH_ID => '', DOMAIN_KEY => ''));
			redirect('general/index');
		}
	}
	/**
	 * oops page of application will be loaded here
	 */
	public function ooops()
	{
		$this->template->view('utilities/404.php');
	}

	/*
	 * @domain Key
	 */
	public function view_subscribed_emails()
	{
		$domain_key = get_domain_auth_id();
		if(intval($domain_key) > 0) {
			$data['domain_admin_exists'] = true;
		} else {
			$data['domain_admin_exists'] = false;
		}
		$data['subscriber_list'] = $this->user_model->get_subscribed_emails($domain_key);
		$this->template->view('user/subscribed_email',$data);
	}

	function event_location_map()
	{
		$details = $this->input->get();
		$geo_codes['data']['latitude'] = $details['latitude'];
		$geo_codes['data']['longtitude'] = $details['longtitude'];
		$geo_codes['data']['name'] = 'Event Log Location';
		$geo_codes['data']['ip'] = $details['ip'];
		echo $this->template->isolated_view('general/event_location_map', $geo_codes);
	}
}