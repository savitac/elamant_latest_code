<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('max_execution_time', 300);
/**
 *
 * @package    Provab
 * @subpackage Flight RS
 * @author     Ajaz <@gmail.com>
 * @version    V1
 */

class Flight_Crs extends CI_Controller {
	
	public function __construct()
	{
		
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
		$this->load->model('flight_crs_model');
		$this->load->library ( "provab_pdf" );
		$this->load->library ( 'provab_mailer' );
		$this->load->library('session');
		
		
		
	}

	function flight_crs_list()
	{
        
        $uid=intval(@$GLOBALS['CI']->entity_user_id);
        $ariline_list = $this->flight_crs_model->supplier_crs_list($uid);
		$flight_crs = array ();
		//$today_date=$this->input->get('today');
		$today_date=$this->input->get('today');
		if(!empty($today_date)){
			$today_date=1;
		}
		$flight_group_crs_list = $this->flight_crs_model->flight_crs_request_details ($uid,$today_date);
		$page_data = $flight_group_crs_list;
		$flight_crs ['page_data'] = $page_data;
		$flight_crs ['ariline_data'] = $ariline_list;
		$flight_crs['display']=$this->input->get('show');
		
		//$flight_crs['display']=$this->input->get('show');
		//debug($flight_crs ['jourany_date']);
		//exit("flight_crs");

		$this->template->view('flight/flight_crs_list', $flight_crs);
		//$this->template->view('flight/flight_crs', $page_data);
		
	}


	

	function get_dates($start, $end, $weekday = 0){
  		$weekdays="Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday";
  		$arr_weekdays=split(",", $weekdays);
  		$weekday = $arr_weekdays[$weekday];
  		if(!$weekday)
  		  die("Invalid Weekday!");
  		$start= strtotime("+0 day", strtotime($start) );
  		$end= strtotime($end);
  		$dateArr = array();
  		$selected_date = strtotime($weekday, $start);
 		// $friday = strtotime($weekday, $start);
 	 	while($selected_date <= $end) {
   		 $dateArr[] = date("Y-m-d", $selected_date);
   		 $selected_date = strtotime("+1 weeks", $selected_date);
  		}
 		 $dateArr[] = date("Y-m-d", $selected_date);
 		array_pop($dateArr);
 		return $dateArr;
	}

/*$dateArr = getDateForSpecificDayBetweenDates("2017-07-27", "2017-07-31", 5); //0 Sun, 1 Mon, etc.
 $get_extactdate=array_pop($dateArr);
 print_r($dateArr);*/

	function edit_flight_list($id) {
		$this->load->model ( 'custom_db' );
		$post_data = $this->input->post ();
		if (valid_array ( $post_data )) {

	       
	        $orgin_id = $id;
	        $request ['ariline_code'] = trim ( $post_data ['airline_name'] );
			$request ['flight_trip'] = trim ( $post_data ['trip_type'] );
			$request ['from_city'] = trim ( $post_data ['from_city'] );
			$request ['to_city'] = trim ( $post_data ['to_city'] );
			$request ['jourany_date'] = trim ( @$post_data ['jourany_date'] );
			$request ['retuen_date'] = trim ( @$post_data ['retuen_date'] );
			
			$request ['no_of_seats'] = trim ( @$post_data ['no_of_seats'] );
			$booked_seats = trim ( @$post_data ['booked_seat'] );
			$left=$post_data ['no_of_seats']- $booked_seats;
			$request ['left_seats'] = $left;
			$request ['class'] = trim ( @$post_data ['class'] );
			
			
			
			$this->custom_db->update_record ( 'flight_crs_list', $request, array (
					'orgin' => $orgin_id ) );
			redirect ( base_url () . 'index.php/flight_crs/flight_crs_list' );
		}
		$uid=intval(@$GLOBALS['CI']->entity_user_id);
		$flight_ariline_list =   $this->flight_crs_model->supplier_crs_list($uid);
		

		$flight_crs_list = $this->flight_crs_model->flight_crs_edit_details ($id);
		$page_data = $flight_crs_list;
		//debug($page_data); exit;
		$flight_crs['page_data'] = $page_data;
		$flight_crs ['ariline_data'] = $flight_ariline_list;
		$this->template->view('flight/editflight_crs_list',$flight_crs);
		
	}

	function update_status_flight_list($id,$status) {
		
           if($status==0)
           {
           	  $request ['status'] = 1;
           }
           if($status==1)
           {
           	  $request ['status'] = 0;
           }
	       
	        $orgin_id = $id;
					
			$this->custom_db->update_record ( 'flight_crs_list', $request, array (
					'orgin' => $orgin_id ) );
			redirect ( base_url () . 'index.php/flight_crs/flight_crs_list' );
		

		
		
	}

	

	function view_flight_details($id,$trip_type)
	{
        $uid=intval(@$GLOBALS['CI']->entity_user_id);
		$flight_crs = array ();
		$flight_group_crs_list = $this->flight_crs_model->flight_crs_view_details ($uid,$id,$trip_type);
		$page_data = $flight_group_crs_list;
		$flight_crs ['page_data'] = $page_data;
		//debug($flight_crs ['page_data']);
		$this->template->view('flight/view_flight_crs_list', $flight_crs);
		//$this->template->view('flight/flight_crs', $page_data);
		
	}

	function flight_segement_list($id,$trip_type)
	{

		$flight_crs = array ();
		$flight_segment_crs_list = $this->flight_crs_model->flight_segment_request_details ($id,$trip_type);
		$page_data = $flight_segment_crs_list;
		$flight_segement ['page_data'] = $page_data;
		$this->template->view('flight/flight_segement_list', $flight_segement);
		
		
	}

	function add_flight_list() {
		
		
		$this->load->model ( 'custom_db' );
		$this->load->library('session');
		$post_data = $this->input->post ();
		$uid=intval(@$GLOBALS['CI']->entity_user_id);
		if (valid_array ( $post_data )) {
			$request ['flight_trip'] = trim ( $post_data ['trip_type'] );
			$request ['ariline_code'] = trim ( $post_data ['airline_name'] );
			$request ['from_city'] = trim ( $post_data ['from_city'] );
			$request ['to_city'] = trim ( $post_data ['to_city'] );
			
			$from_date =$this->convert_date_format("sql",trim ( @$post_data ['jourany_date'] ));
			$request ['jourany_date'] =trim ( @$post_data ['jourany_date'] );
			$request ['retuen_date'] = trim ( @$post_data ['retuen_date'] );

			$request ['no_of_seats'] = trim ( @$post_data ['no_of_seats'] );
			$request ['left_seats'] = trim ( @$post_data ['no_of_seats'] );
			$request ['class'] = trim ( @$post_data ['class'] );
			$request ['supplier_id'] =$uid;
			$request ['created_by'] =$uid;
			$request ['status'] = '0';
			$request ['till_date'] = $this->convert_date_format("sql",trim ( @$post_data ['till_date'] ));
			//dummy added
			$request ['pnr']=substr(md5(microtime()*rand(0,99)),0,6);
			$days=$post_data ['no_days'];
			if($request ['flight_trip']=="circle"){
				 $create_filght=2;
				
			}else{
				 $create_filght=1;

			}
			foreach ( $days as $key=> $day) {
						// get array from the function 
					 $insert_dates=$this->get_dates($from_date,$request ['till_date'],$day);
					 $get_size=sizeof($insert_dates);// count innner array element 
					 for($i=0;$i<$get_size;$i++){
					 	$single_dates[]=$this->convert_date_format("sql",$insert_dates[$i]);// make a single element
					 }
					 //$c['dates']= $insert_dates + $insert_dates;
			}
			$single_dates_size=sizeof($single_dates);
			// for loop help to create multiple*2 if circel else oneway 1
			/*for($create_filght_no=0;$create_filght_no<$create_filght;$create_filght_no++){ */
				for($date=0;$date<$single_dates_size;$date++){
					$request ['jourany_date']=$single_dates[$date];
					if($request ['flight_trip']=='circel'){
						$request['retuen_date']=$single_dates[$date];
					}
					$insert_id = $this->custom_db->insert_record ( 'flight_crs_list', $request );
					$inserted_ids[]=$insert_id['insert_id'];
				/*}*/
			}
			$this->session->set_userdata('inserted_keys',$inserted_ids);
			$count_session_ids=count($this->session->userdata('inserted_keys'));
			if($count_session_ids>0){
				redirect ( base_url () . 'index.php/flight_crs/add_flight_segement/'.$request ['flight_trip'] );
			}else{
				redirect ( base_url () . 'index.php/flight_crs/add_flight_list/');
			}

			//redirect ( base_url () . 'index.php/flight_crs/add_flight_segement/'.$insert_id['insert_id'].'/'.$request ['flight_trip'] );

			//redirect ( base_url () . 'index.php/flight_crs/flight_crs_list' );
		}
		$uid=intval(@$GLOBALS['CI']->entity_user_id);
		$flight_ariline_list =   $this->flight_crs_model->supplier_crs_list($uid);
		
		$page_data = $flight_ariline_list;
		$ariline_list ['page_data'] = $page_data;
		$this->template->view('flight/addflight_crs_list',$ariline_list);
		
	}

	function add_flight_segement($trip_type) {
		
		//function add_flight_segement($id,$trip_type) {
		$this->load->model ( 'custom_db' );
		$inserted_keys=$this->session->userdata('inserted_keys');
		
		$post_data = $this->input->post ();
		if (valid_array ( $post_data )) {
			//$request ['flight_crs_no'] = $id;

			$request ['onwards_flight_name'] = trim ( $post_data ['onwards_flight_name'] );
			$request ['onwards_flight_number'] = trim ( $post_data ['onwards_flight_number'] );
			$request ['onwards_flight_equipment'] = trim ( $post_data ['onwards_flight_equipment'] );
			$request ['onwards_from_city'] = trim ( @$post_data ['onwards_from_city'] );
			$request ['onwards_to_city'] = trim ( @$post_data ['onwards_to_city'] );
			$request ['onwards_departure_time'] = trim ( $post_data ['onwards_departure_time'] );
			$request ['onwards_arrival_at'] = trim ( $post_data ['onwards_arrival_at'] );
			$request ['onwards_arrival_time'] = trim ( $post_data ['onwards_arrival_time'] );
			$request ['onwards_duration'] = trim ( $post_data ['onwards_duration'] );
			$request ['retuen_flight_name'] = trim ( @$post_data ['retuen_flight_name'] );
			$request ['return_flight_number'] = trim ( @$post_data ['return_flight_number'] );
			$request ['return_flight_equipment'] = trim ( @$post_data ['return_flight_equipment'] );
			$request ['return_from_city'] = trim ( @$post_data ['return_from_city'] );
			$request ['return_to_city'] = trim ( $post_data ['return_to_city'] );
			$request ['return_departure_time'] = trim ( @$post_data ['return_departure_time'] );
			$request ['return_arrival_at'] = trim ( @$post_data ['return_arrival_at'] );
			$request ['return_arrival_time'] = trim ( @$post_data ['return_arrival_time'] );
			$request ['return_duration'] = trim ( $post_data ['return_duration'] );
			
			foreach ($inserted_keys as  $value) {
			$request ['flight_crs_no'] = $value;
				$insert_id = $this->custom_db->insert_record ( 'flight_segement_list', $request );
			}
			redirect ( base_url () . 'index.php/flight_crs/add_flight_price/'.$trip_type );
			//redirect ( base_url () . 'index.php/flight_crs/add_flight_price/'.$id.'/'.$trip_type );

		}
		$uid=intval(@$GLOBALS['CI']->entity_user_id);
		$id=$inserted_keys[0];
		$flight_group_crs_list = $this->flight_crs_model->flight_crs_edit_request_details ($uid,$id);
		$page_data = $flight_group_crs_list;
		$flight_crs ['page_data'] = $page_data;
		//debug($flight_crs);exit("hello");
		$this->template->view('flight/addflight_segement_list',$flight_crs);
		
	}
	function add_flight_price($id) {
		$this->load->model ( 'custom_db' );
		$post_data = $this->input->post ();
		$inserted_keys=$this->session->userdata('inserted_keys');
		if (valid_array ( $post_data )) {

	        //$request ['flight_crs_no'] = $id;
			$request ['adult_price'] = trim ( $post_data ['adult_price'] );
			$request ['adult_tax'] = trim ( $post_data ['adult_tax'] );
			$request ['adult_markup'] = 0;
			$request ['child_price'] = trim ( $post_data ['child_price'] );
			$request ['child_tax'] = trim ( @$post_data ['child_tax'] );
			$request ['child_markup'] = 0;
			$request ['infant_price'] = trim ( @$post_data ['infant_price'] );
			$request ['infant_tax'] = trim ( $post_data ['infant_tax'] );
			$request ['infant_markup'] = 0;
			$request ['fare_rules'] = trim ( $post_data ['fare_rules'] );
			$request ['adult_bagage'] = trim ( $post_data ['adult_bagage'] );
			$request ['child_bagage'] = trim ( $post_data ['child_bagage'] );			
			$status_update ['status'] = trim ( $post_data ['status'] );
			//added by ajaz
			foreach ($inserted_keys as  $value) {
				$request ['flight_crs_no']=$value;
				$id=$value;
				$insert_id = $this->custom_db->insert_record ( 'flight_crs_price', $request );
				$update_status=$this->custom_db->update_record ( 'flight_crs_list', $status_update, array (
					'orgin' => $id ) );
			}

			redirect ( base_url () . 'index.php/flight_crs/flight_crs_list/');
			//redirect ( base_url () . 'index.php/flight_crs/flight_price_list/'.$id);
		}
		$this->template->view('flight/addflight_price_list');
		
	}


     function edit_flight_segement($orgin_id,$id,$trip_type) {
			$this->load->model ( 'custom_db' );
			$post_data = $this->input->post ();
			if (valid_array ( $post_data )) {
			$orgin_id = trim ( $post_data ['orgin'] );
			$request ['flight_crs_no'] = $id;
			$request ['onwards_flight_name'] = trim ( $post_data ['onwards_flight_name'] );
			$request ['onwards_flight_number'] = trim ( $post_data ['onwards_flight_number'] );
			$request ['onwards_flight_equipment'] = trim ( $post_data ['onwards_flight_equipment'] );
			$request ['onwards_from_city'] = trim ( @$post_data ['onwards_from_city'] );
			$request ['onwards_to_city'] = trim ( @$post_data ['onwards_to_city'] );
			$request ['onwards_departure_time'] = trim ( $post_data ['onwards_departure_time'] );
			$request ['onwards_arrival_at'] = trim ( $post_data ['onwards_arrival_at'] );
			$request ['onwards_arrival_time'] = trim ( $post_data ['onwards_arrival_time'] );
			$request ['onwards_duration'] = trim ( $post_data ['onwards_duration'] );
			$request ['retuen_flight_name'] = trim ( @$post_data ['retuen_flight_name'] );
			$request ['return_flight_number'] = trim ( @$post_data ['return_flight_number'] );
			$request ['return_flight_equipment'] = trim ( @$post_data ['return_flight_equipment'] );
			$request ['return_from_city'] = trim ( @$post_data ['return_from_city'] );
			$request ['return_to_city'] = trim ( $post_data ['return_to_city'] );
			$request ['return_departure_time'] = trim ( @$post_data ['return_departure_time'] );
			$request ['return_arrival_at'] = trim ( @$post_data ['return_arrival_at'] );
			$request ['return_arrival_time'] = trim ( @$post_data ['return_arrival_time'] );
			$request ['return_duration'] = trim ( $post_data ['return_duration'] );
			
			$this->custom_db->update_record ( 'flight_segement_list', $request, array (
					'orgin' => $orgin_id,'flight_crs_no'=>$id ) );
			redirect ( base_url () . 'index.php/flight_crs/flight_segement_list/'.$id.'/'.$trip_type);
		}

		$flight_price_crs_list = $this->flight_crs_model->flight_segemnet_edit_details ($orgin_id);
		$page_data = $flight_price_crs_list;
		$flight_price ['page_data'] = $page_data;
		$this->template->view('flight/editflight_segement_list',$flight_price);
		
	}

	function flight_price_list($id)
	{

		$flight_crs = array ();
		$flight_price_crs_list = $this->flight_crs_model->flight_price_request_details ($id);
		$page_data = $flight_price_crs_list;
		$flight_price ['page_data'] = $page_data;
		$this->template->view('flight/flight_price_list', $flight_price);
		
		
	}

	

	function edit_flight_price($orgin_id,$id) {
		$this->load->model ( 'custom_db' );
		$post_data = $this->input->post ();
		if (valid_array ( $post_data )) {

	       
	        $orgin_id = trim ( $post_data ['orgin'] );
			$request ['adult_price'] = trim ( $post_data ['adult_price'] );
			$request ['adult_tax'] = trim ( $post_data ['adult_tax'] );
			
			$request ['child_price'] = trim ( $post_data ['child_price'] );
			$request ['child_tax'] = trim ( @$post_data ['child_tax'] );
			
			$request ['infant_price'] = trim ( @$post_data ['infant_price'] );
			$request ['infant_tax'] = trim ( $post_data ['infant_tax'] );
			
			$request ['fare_rules'] = trim ( $post_data ['fare_rules'] );
			
			$this->custom_db->update_record ( 'flight_crs_price', $request, array (
					'orgin' => $orgin_id,'flight_crs_no'=>$id ) );
			redirect ( base_url () . 'index.php/flight_crs/flight_price_list/'.$id);
		}

		$flight_price_crs_list = $this->flight_crs_model->flight_price_edit_details ($orgin_id);
		$page_data = $flight_price_crs_list;
		$flight_price ['page_data'] = $page_data;
		$this->template->view('flight/editflight_price_list',$flight_price);
		
	}

	function delete_flight_price_list($orgin_id,$id)
	{

		$this->load->model ( 'custom_db' );
		$where = array('orgin' => $orgin_id); 
		$this->custom_db->delete_record ( 'flight_crs_price', $where );
		redirect ( base_url () . 'index.php/flight_crs/flight_price_list/'.$id);
		
		
	}


function b2b_flight_report_crs($offset=0,$flight_id,$date)
	{
                $this->load->model ( 'flight_model' );
		
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['pnr']) == false) {
				$condition[] = array('CD.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
                
		$total_records = $this->flight_model->b2b_flight_report($condition, true);
        $table_data = $this->flight_model->b2b_flight_report($condition, false, $offset, RECORDS_RANGE_2);

		
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];

		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		//debug($page_data); exit;
		$this->template->view('report/b2b_report_airline', $page_data);
	}


	function b2b_flight_sales_report($offset=0,$flight_id,$date)
	{
                $this->load->model ( 'flight_model' );
		
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		//debug($get_data);
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['pnr']) == false) {
				$condition[] = array('CD.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
                
		$total_records = $this->flight_model->b2b_flight_report($condition, true);
               //debug($total_records);
		$table_data = $this->flight_model->b2b_flight_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2b_report_airline_sales', $page_data);
	}


	function flight_crs_list_booking()
	{
        $uid=intval(@$GLOBALS['CI']->entity_user_id);
        $ariline_list = $this->flight_crs_model->supplier_crs_list($uid);
		$flight_crs = array ();
		$flight_group_crs_list = $this->flight_crs_model->flight_crs_request_booking_details ($uid);
		$page_data = $flight_group_crs_list;
		$flight_crs ['page_data'] = $page_data;
		$flight_crs ['ariline_data'] = $ariline_list;
		//debug($flight_crs ['page_data']); exit;
		$this->template->view('flight/flight_crs_list_booking', $flight_crs);
		//$this->template->view('flight/flight_crs', $page_data);
		
	}

	function b2b_flight_report_crs_confirmed($offset=0,$flight_id,$date)
	{
         $this->load->library ( "provab_pdf" );
		$this->load->library ( 'provab_mailer' );
         $this->load->model ( 'flight_model' );
		
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		//debug($get_data);
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['pnr']) == false) {
				$condition[] = array('BD.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
                
		$total_records = $this->flight_model->b2b_flight_report($condition, true);
               //debug($total_records);
		$table_data = $this->flight_model->b2b_flight_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		
	   $mail_template = $this->template->isolated_view ( 'report/b2b_report_airline_confirmed', $page_data ); 
		echo $pdf = $this->provab_pdf->create_pdf ( $mail_template );
		//$email = $this->entity_email;
	    //$this->provab_mailer->send_mail ( $email, domain_name () . ' - Report Manifest', $mail_template, $pdf );
	}


	function b2b_flight_report_crs_sales_pdf($offset=0,$flight_id,$date)
	{
                $this->load->model ( 'flight_model' );
		
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		//debug($get_data);
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['pnr']) == false) {
				$condition[] = array('CD.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
                
		$total_records = $this->flight_model->b2b_flight_report($condition, true);
               //debug($total_records);
		$table_data = $this->flight_model->b2b_flight_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$mail_template = $this->template->isolated_view ( 'report/b2b_report_airline_sales_pdf', $page_data ); 
		echo $pdf = $this->provab_pdf->create_pdf ( $mail_template );
		
	}


	function manage_booking($offset=0)
	{
        // error_reporting(E_ALL);
		$this->load->model ( 'flight_model' );
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		//debug($get_data);
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['pnr']) == false) {
				$condition[] = array('CD.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
               
		$total_records = $this->flight_model->b2b_flight_report($condition, true);
         //debug($total_records);
         
		$table_data = $this->flight_model->b2b_flight_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$uid=intval(@$GLOBALS['CI']->entity_user_id);
        $flight_group_crs_list = $this->flight_crs_model->flight_crs_request_details ($uid);    
       	$page_data['flight_name'] = $flight_group_crs_list;
		$this->template->view('report/b2b_report_airline_manage', $page_data);
	}

	function sales_report($offset=0)
	{
                $this->load->model ( 'flight_model' );
		
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		//debug($get_data);
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['pnr']) == false) {
				$condition[] = array('CD.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
                
		$total_records = $this->flight_model->b2b_flight_report($condition, true);
               //debug($total_records);
		$table_data = $this->flight_model->b2b_flight_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$uid=intval(@$GLOBALS['CI']->entity_user_id);
         $flight_group_crs_list = $this->flight_crs_model->flight_crs_request_details ($uid);     
		$page_data['flight_name'] = $flight_group_crs_list;
		
		$this->template->view('report/b2b_report_airline_allsales', $page_data);
	}
// added by ajaz convert sql format
	function convert_date_format($format,$date){
		$arr = explode('-', $date);
		if($format=="sql"){
		return $newDate = $arr[2].'-'.$arr[1].'-'.$arr[0];
		}else{
			return $newDate = $arr[0].'-'.$arr[1].'-'.$arr[0];
		//return $newDate = $arr[0].'-'.$arr[1].'-'.$arr[2];
			
		}
	}



	
	
}
