<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package    Provab - Provab Application
 * @subpackage Travel Portal
 * @author     Jaganath J<jaganath.provab@gmail.com>
 * @version    V2
 */

class Report extends CI_Controller {
	private $current_module;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('bus_model');
		$this->load->model('hotel_model');
		$this->load->model('flight_model');
		$this->load->library('booking_data_formatter');
		$this->current_module = $this->config->item('current_module');
//		$this->load->library('export');

	}
	function index()
	{
		redirect('general');
	}

	function monthly_booking_report()
	{
		$this->template->view('report/monthly_booking_report');
	}


	function bus($offset=0)
	{
		$get_data = $this->input->get();
		$condition = array();
		$page_data = array();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}

			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}

			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}

			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone_number', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}

			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}

			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->bus_model->booking($condition, true);
		$table_data = $this->bus_model->booking($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_bus_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		/** TABLE PAGINATION */
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/bus/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['customer_email'] = $this->entity_email;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/bus', $page_data);
	}

	function hotel($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}

			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}

			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}

			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone_number', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}

			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}

			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->hotel_model->booking($condition, true);
		$table_data = $this->hotel_model->booking($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_hotel_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/hotel/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/hotel', $page_data);
	}

	
	
	
	function b2c_bus_report($offset=0)
	{
		$get_data = $this->input->get();
		$condition = array();
		$page_data = array();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone_number', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->bus_model->b2c_bus_report($condition, true);
		$table_data = $this->bus_model->b2c_bus_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_bus_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];

		//debug($table_data); exit;

		/** TABLE PAGINATION */
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/bus/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['customer_email'] = $this->entity_email;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2c_report_bus', $page_data);
	}
	
	
	function b2b_bus_report($offset=0)
	{
		$get_data = $this->input->get();
		$condition = array();
		$page_data = array();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone_number', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->bus_model->b2b_bus_report($condition, true);
		$table_data = $this->bus_model->b2b_bus_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_bus_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		/** TABLE PAGINATION */
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/bus/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['customer_email'] = $this->entity_email;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2b_report_bus', $page_data);
	}
	
	
	
	function b2b_hotel_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone_number', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->hotel_model->b2b_hotel_report($condition, true);
		$table_data = $this->hotel_model->b2b_hotel_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_hotel_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/hotel/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2b_report_hotel', $page_data);
	}
	
	
	function b2c_hotel_report($offset=0)
	{
		$condition = array();
		$get_data = $this->input->get();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone_number', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->hotel_model->b2c_hotel_report($condition, true);
		$table_data = $this->hotel_model->b2c_hotel_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_hotel_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/hotel/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		//debug($page_data);exit;
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2c_report_hotel', $page_data);
	}
	
	
	
	/**
	 * Flight Report
	 * @param $offset
	 */
	function flight($offset=0)
	{
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}

			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}

			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}

			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}

			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}

			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->flight_model->booking($condition, true);
		$table_data = $this->flight_model->booking($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data,$this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/flight/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/airline', $page_data);
	}
	
	/**
	 * Flight Report for b2c flight
	 * @param $offset
	 */
	function b2c_flight_report($offset=0)
	{
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		//$condition[] = array('U.user_type', '=', B2C_USER, ' OR ', 'BD.created_by_id');
		$total_records = $this->flight_model->b2c_flight_report($condition, true);
		$table_data = $this->flight_model->b2c_flight_report($condition, false, $offset);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		//debug($table_data); exit;

		//Export report


		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2c_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2c_report_airline', $page_data);
	}
	

	
	/**
	 * Flight Report for b2b flight
	 * @param $offset
	 */
	function b2b_flight_sales_report($offset=0)
	{
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->flight_model->b2b_flight_report($condition, true);
		$table_data = $this->flight_model->b2b_flight_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2b_report_airline_sales', $page_data);
	}

	function b2b_flight_report_crs($offset=0,$flight_id,$date)
	{
		echo $offset; echo $flight_id; echo $date; exit;
		$current_user_id = $GLOBALS['CI']->entity_user_id;
		$get_data = $this->input->get();
		$condition = array();
		//debug($get_data);
		if(valid_array($get_data) == true) {
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$condition[] = array('BD.created_datetime', '>=', $this->db->escape(db_current_datetime($from_date)));
			}
			if(empty($to_date) == false) {
				$condition[] = array('BD.created_datetime', '<=', $this->db->escape(db_current_datetime($to_date)));
			}
	
			if (empty($get_data['created_by_id']) == false) {
				$condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
	
			if (empty($get_data['phone']) == false) {
				$condition[] = array('BD.phone', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}
	
			if (empty($get_data['app_reference']) == false) {
				$condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;
		}
		$total_records = $this->flight_model->b2b_flight_report($condition, true);
		$table_data = $this->flight_model->b2b_flight_report($condition, false, $offset, RECORDS_RANGE_2);
		$table_data = $this->booking_data_formatter->format_flight_booking_data($table_data, $this->current_module);
		$page_data['table_data'] = $table_data['data'];
		$this->load->library('pagination');
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['base_url'] = base_url().'index.php/report/b2b_flight_report/';
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$page_data['search_params'] = $get_data;
		$page_data['status_options'] = get_enum_list('booking_status_options');
		$this->template->view('report/b2b_report_airline', $page_data);
	}
	
	
	function update_flight_booking_details($app_reference, $booking_source)
	{
		load_flight_lib($booking_source);
		$this->flight_lib->update_flight_booking_details($app_reference);
		//FIXME: Return the status
	}
	
	/**
	 * Sagar Wakchaure
	 *Update pnr Details 
	 * @param unknown $app_reference
	 * @param unknown $booking_source
	 * @param unknown $booking_status
	 */
	function update_pnr_details($app_reference, $booking_source,$booking_status){		
		load_flight_lib($booking_source);		
		$response = $this->flight_lib->update_pnr_details($app_reference);		
		$get_pnr_updated_status = $this->flight_model->update_pnr_details($response,$app_reference, $booking_source,$booking_status);
		echo $get_pnr_updated_status;
		
	}
	
	
	
function package()
	{
		echo '<h4>Under Working</h4>';
	}
	
	function b2b_package_report()
	{
		echo '<h4>Under Working</h4>';
	}
	
	function b2c_package_report()
	{
		echo '<h4>Under Working</h4>';
	}

	function export_excel($flight_id,$journey_date) {

             $this->load->model('flight_crs_model');
             $joury=date("Y-m-d", strtotime($journey_date));
             $flight_crs_list = $this->flight_crs_model->flight_crs_edit_details ($flight_id);
             $trip=$flight_crs_list['flight_trip'];
             $flight_segment_crs_list = $this->flight_crs_model->flight_segment_request_details ($flight_id,$trip);
             //debug($flight_segment_crs_list);
           $flight_number=$flight_segment_crs_list['0']['onwards_flight_number'];
           $flight_return_number=$flight_segment_crs_list['0']['return_flight_number']; 
                $today=date('Y-m-d H:i:s');
             
              if($trip=='oneway' && $flight_number!=0 )


              {

  


                  
              	
                  $tr_query = $this->db->query ( " SELECT BD.origin,CONCAT(PI.first_name,' ' ,PI.last_name) as name ,
                  	BD.phone,BT.pnr,PTI.TicketNumber,BI.flight_number,BD.journey_from,
                  	BD.to_loc,BD.created_datetime as Booked_date,PTI.fare as base_fare
			FROM flight_booking_details BD
			JOIN flight_booking_transaction_details BT ON BD.app_reference=BT.app_reference
			JOIN flight_booking_passenger_details PI ON BD.app_reference=PI.app_reference
			JOIN  flight_passenger_ticket_info PTI ON PI.origin=PTI.passenger_fk
            JOIN  flight_booking_itinerary_details BI ON BD.app_reference=BI.app_reference
			where BD.journey_start >='$joury' and BI.flight_number='$flight_number' and  (BD.status='BOOKING_CONFIRMED' OR BD.status='BOOKING_FLOWN' ) 

order by BD.created_datetime DESC " );

$this->load->helper ( 'csv' ); // custom_helper.php
		
		query_to_csv ( $tr_query, TRUE, 'flight_sales_report.csv' );
              }

             if($trip=='circle' && $flight_number!=0 &&  $flight_return_number!=0 )
              {
              	
                  $tr_query = $this->db->query ( " SELECT BD.origin,CONCAT(PI.first_name,' ' ,PI.last_name) as name ,
                  	BD.phone,BT.pnr,PTI.TicketNumber,BI.flight_number,BD.journey_from,
                  	BD.to_loc,BD.created_datetime as Booked_date,PTI.fare as base_fare
			FROM flight_booking_details BD
			JOIN flight_booking_transaction_details BT ON BD.app_reference=BT.app_reference
			JOIN flight_booking_passenger_details PI ON BD.app_reference=PI.app_reference
			JOIN  flight_passenger_ticket_info PTI ON PI.origin=PTI.passenger_fk
            JOIN  flight_booking_itinerary_details BI ON BD.app_reference=BI.app_reference
			where BD.journey_start >='$journey_date' and BI.flight_number='$flight_number'  (BD.status='BOOKING_CONFIRMED' OR BD.status='BOOKING_FLOWN' ) 

 order by BD.created_datetime DESC " );

$this->load->helper ( 'csv' ); // custom_helper.php
		
		query_to_csv ( $tr_query, TRUE, 'flight_sales_report.csv' );
              }


              
	
}


                function export_excel_all() 
                {

                           $this->load->model('flight_crs_model');
                           $uid=intval(@$GLOBALS['CI']->entity_user_id);
                        $flight_group_crs_list = $this->flight_crs_model->flight_crs_request_booking_details ($uid);
                       foreach ($flight_group_crs_list as $key => $value) {
                            //debug($value);
                            $trip[]=$value['flight_trip'];
                            $flight_number[]=$value['onwards_flight_number'];
                            $flight[]=$value['return_flight_number'];
                            $journey_date[]=date("Y-m-d", strtotime($value['jourany_date']));
                       }
                      //debug($trip);
                        $out=array_merge($flight_number,$flight);
                        $in= "(".implode(',',array_filter($out)).')';
                         
                         
                       if(in_array('oneway', $trip) )
                       {
                       	                    $tr_query = $this->db->query ( "SELECT BD.origin,CONCAT(PI.first_name,' ' ,PI.last_name) as name ,
                  	BD.phone,BT.pnr,PTI.TicketNumber,BI.flight_number,BD.journey_from,
                  	BD.to_loc,BD.created_datetime as Booked_date,PTI.fare as base_fare
			FROM flight_booking_details BD
			JOIN flight_booking_transaction_details BT ON BD.app_reference=BT.app_reference
			JOIN flight_booking_passenger_details PI ON BD.app_reference=PI.app_reference
			JOIN  flight_passenger_ticket_info PTI ON PI.origin=PTI.passenger_fk
            JOIN  flight_booking_itinerary_details BI ON BD.app_reference=BI.app_reference
			where  BI.flight_number IN ".$in." and   (BD.status='BOOKING_CONFIRMED' OR BD.status='BOOKING_FLOWN' )

 order by BD.created_datetime DESC");
                       	                    $this->load->helper ( 'csv' ); // custom_helper.php
		
		query_to_csv ( $tr_query, TRUE, 'sales_report.csv' );
                       }

                       if(in_array('circle', $trip))
                       {
                       	  $tr_query = $this->db->query ("SELECT BD.origin,CONCAT(PI.first_name,' ' ,PI.last_name) as name ,
                  	BD.phone,BT.pnr,PTI.TicketNumber,BI.flight_number,BD.journey_from,
                  	BD.to_loc,BD.created_datetime as Booked_date,PTI.fare as base_fare
			FROM flight_booking_details BD
			JOIN flight_booking_transaction_details BT ON BD.app_reference=BT.app_reference
			JOIN flight_booking_passenger_details PI ON BD.app_reference=PI.app_reference
			JOIN  flight_passenger_ticket_info PTI ON PI.origin=PTI.passenger_fk
            JOIN  flight_booking_itinerary_details BI ON BD.app_reference=BI.app_reference
			where  BI.flight_number IN ".$in."  and  (BD.status='BOOKING_CONFIRMED' OR BD.status='BOOKING_FLOWN' ) 
 
order by BD.created_datetime DESC");
                       	  $this->load->helper ( 'csv' ); // custom_helper.php
		
		query_to_csv ( $tr_query, TRUE, 'sales_report.csv' );
                       }
             
                }


                function export_excel_pdf($flight_id,$journey_date) {

             $this->load->model('flight_crs_model');
             $joury=date("Y-m-d", strtotime($journey_date));
             $flight_crs_list = $this->flight_crs_model->flight_crs_edit_details ($flight_id);
             $trip=$flight_crs_list['flight_trip'];
             $flight_segment_crs_list = $this->flight_crs_model->flight_segment_request_details ($flight_id,$trip);
             //debug($flight_segment_crs_list);
           $flight_number=$flight_segment_crs_list['0']['onwards_flight_number'];
           $flight_return_number=$flight_segment_crs_list['0']['return_flight_number']; 
                $today=date('Y-m-d H:i:s');
             
              if($trip=='oneway' && $flight_number!=0 )


              {
              	
                  $tr_query = $this->db->query ( " SELECT BD.origin,CONCAT(PI.first_name,' ' ,PI.last_name) as name ,
                  	BD.phone,BT.pnr,PTI.TicketNumber,BI.flight_number,BD.journey_start as FlightDate
,BD.journey_from,BD.journey_to
			FROM flight_booking_details BD
			JOIN flight_booking_transaction_details BT ON BD.app_reference=BT.app_reference
			JOIN flight_booking_passenger_details PI ON BD.app_reference=PI.app_reference
			JOIN  flight_passenger_ticket_info PTI ON PI.origin=PTI.passenger_fk
            JOIN  flight_booking_itinerary_details BI ON BD.app_reference=BI.app_reference
			where BD.journey_start >='$joury' and BI.flight_number='$flight_number' and  BD.status='BOOKING_CONFIRMED' order by BD.created_datetime DESC " );

$this->load->helper ( 'csv' ); // custom_helper.php
		
		query_to_csv ( $tr_query, TRUE, 'flight_manifest.csv' );
              }

             if($trip=='circle' && $flight_number!=0 &&  $flight_return_number!=0 )
              {
              	
                  $tr_query = $this->db->query ( " SELECT BD.origin,CONCAT(PI.first_name,' ' ,PI.last_name) as name ,
                  	BD.phone,BT.pnr,PTI.TicketNumber,BI.flight_number,BD.journey_start as FlightDate
,BD.journey_from,BD.journey_to
			FROM flight_booking_details BD
			JOIN flight_booking_transaction_details BT ON BD.app_reference=BT.app_reference
			JOIN flight_booking_passenger_details PI ON BD.app_reference=PI.app_reference
			JOIN  flight_passenger_ticket_info PTI ON PI.origin=PTI.passenger_fk
            JOIN  flight_booking_itinerary_details BI ON BD.app_reference=BI.app_reference
			where BD.journey_start >='$journey_date' and BI.flight_number='$flight_number'  BD.status='BOOKING_CONFIRMED'  order by BD.created_datetime DESC " );

$this->load->helper ( 'csv' ); // custom_helper.php
		
		query_to_csv ( $tr_query, TRUE, 'flight_manifest.csv' );
              }


              
	
}


}
