<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package    Provab - Provab Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com> on 01-06-2015
 * @version    V2
 */

class Private_Management extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('private_management_model');
	}

	/**
	 * Manage domain markup for provab - Domain wise and module wise
	 */
	function airline_domain_markup()
	{
		$page_data['form_data'] = $this->input->post();
		if (valid_array($page_data['form_data']) == true) {
			switch($page_data['form_data']['form_values_origin']) {
				case 'generic':
					$this->private_management_model->save_markup_data(
					$page_data['form_data']['markup_origin'], $page_data['form_data']['form_values_origin'], 'b2c_flight', 0,
					$page_data['form_data']['generic_value'], $page_data['form_data']['value_type'], $page_data['form_data']['domain_origin']
					);
					break;
				case 'specific':
					if (valid_array($page_data['form_data']['domain_origin'])) {
						foreach($page_data['form_data']['domain_origin'] as $__k => $__domain_origin) {
							if ($page_data['form_data']['specific_value'][$__k] != '' && intval($page_data['form_data']['specific_value'][$__k]) > -1
							&& empty($page_data['form_data']['value_type_'.$__domain_origin]) == false
							) {
								$this->private_management_model->save_markup_data(
								$page_data['form_data']['markup_origin'][$__k], $page_data['form_data']['form_values_origin'], 'b2c_flight', $page_data['form_data']['domain_origin'][$__k],
								$page_data['form_data']['specific_value'][$__k], $page_data['form_data']['value_type_'.$__domain_origin], $page_data['form_data']['domain_origin'][$__k]
								);
							}
						}
					}
					break;
			}
			redirect(base_url().'index.php/private_management/'.__FUNCTION__);
		}
		//Airline would have All - general and domain wise markup
		$data_list = $this->private_management_model->airline_domain_markup();
		$this->template->view('private_management/airline_domain_markup', $data_list['data']);
	}

	/**
	 * Manage domain markup for provab - Domain wise and module wise
	 */
	function hotel_domain_markup()
	{
		//Hotel would have All - general and domain wise markup
		$page_data['form_data'] = $this->input->post();
		if (valid_array($page_data['form_data']) == true) {
			switch($page_data['form_data']['form_values_origin']) {
				case 'generic':
					$this->private_management_model->save_markup_data(
					$page_data['form_data']['markup_origin'], $page_data['form_data']['form_values_origin'], 'b2c_hotel', 0,
					$page_data['form_data']['generic_value'], $page_data['form_data']['value_type'], $page_data['form_data']['domain_origin']
					);
					break;
				case 'specific':
					if (valid_array($page_data['form_data']['domain_origin'])) {
						foreach($page_data['form_data']['domain_origin'] as $__k => $__domain_origin) {
							if ($page_data['form_data']['specific_value'][$__k] != '' && intval($page_data['form_data']['specific_value'][$__k]) > -1
							&& empty($page_data['form_data']['value_type_'.$__domain_origin]) == false
							) {
								$this->private_management_model->save_markup_data(
								$page_data['form_data']['markup_origin'][$__k], $page_data['form_data']['form_values_origin'], 'b2c_hotel', $page_data['form_data']['domain_origin'][$__k],
								$page_data['form_data']['specific_value'][$__k], $page_data['form_data']['value_type_'.$__domain_origin], $page_data['form_data']['domain_origin'][$__k]
								);
							}
						}
					}
					break;
			}
			redirect(base_url().'index.php/private_management/'.__FUNCTION__);
		}
		//Airline would have All - general and domain wise markup
		$data_list = $this->private_management_model->hotel_domain_markup();
		$this->template->view('private_management/hotel_domain_markup', $data_list['data']);
	}

	/**
	 * Manage domain markup for provab - Domain wise and module wise
	 */
	function bus_domain_markup()
	{
		//Bus would have All - general and domain wise markup
		$page_data['form_data'] = $this->input->post();
		if (valid_array($page_data['form_data']) == true) {
			switch($page_data['form_data']['form_values_origin']) {
				case 'generic':
					$this->private_management_model->save_markup_data(
					$page_data['form_data']['markup_origin'], $page_data['form_data']['form_values_origin'], 'b2c_bus', 0,
					$page_data['form_data']['generic_value'], $page_data['form_data']['value_type'], $page_data['form_data']['domain_origin']
					);
					break;
				case 'specific':
					if (valid_array($page_data['form_data']['domain_origin'])) {
						foreach($page_data['form_data']['domain_origin'] as $__k => $__domain_origin) {
							if ($page_data['form_data']['specific_value'][$__k] != '' && intval($page_data['form_data']['specific_value'][$__k]) > -1
							&& empty($page_data['form_data']['value_type_'.$__domain_origin]) == false
							) {
								$this->private_management_model->save_markup_data(
								$page_data['form_data']['markup_origin'][$__k], $page_data['form_data']['form_values_origin'], 'b2c_bus', $page_data['form_data']['domain_origin'][$__k],
								$page_data['form_data']['specific_value'][$__k], $page_data['form_data']['value_type_'.$__domain_origin], $page_data['form_data']['domain_origin'][$__k]
								);
							}
						}
					}
					break;
			}
			redirect(base_url().'index.php/private_management/'.__FUNCTION__);
		}
		//Airline would have All - general and domain wise markup
		$data_list = $this->private_management_model->bus_domain_markup();
		$this->template->view('private_management/bus_domain_markup', $data_list['data']);
	}

	/**
	 * Arjun J Gowda
	 * Process Balance Request with provab
	 */
	function process_balance_manager()
	{
		
		if (!is_domain_user()) {
			$page_data['form_data'] = $this->input->post();
			if (valid_array($page_data['form_data']) == true) {
				$process_details = $this->private_management_model->process_balance_request($page_data['form_data']['request_origin'], $page_data['form_data']['system_request_id'], $page_data['form_data']['status_id'], $page_data['form_data']['update_remarks']);
				redirect(base_url().'index.php/private_management/'.__FUNCTION__);
			}
			$page_data['provab_balance_requests'] = get_enum_list('provab_balance_status');
			$page_data['table_data'] = $this->private_management_model->master_transaction_request_list();
			$this->template->view('private_management/process_balance_manager', $page_data);
		}
	}

	/**
	 * Event logging
	 * @param number $offset
	 */
	function event_logs($offset=0)
	{
		$condition = array();
		$page_data['table_data'] = $this->private_management_model->event_logs($condition, false, $offset, RECORDS_RANGE_2);
		$total_records = $this->private_management_model->event_logs($condition, true);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/private_management/event_logs/';
		$page_data['total_rows'] = $config['total_rows'] = $total_records;
		$config['per_page'] = RECORDS_RANGE_2;
		$this->pagination->initialize($config);
		/** TABLE PAGINATION */
		$page_data['total_records'] = $config['total_rows'];
		$this->template->view('private_management/event_logs', $page_data);
	}
}
