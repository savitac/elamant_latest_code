<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package    Provab
 * @subpackage Bus
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */

class Voucher extends CI_Controller {
	private $current_module;
	public function __construct()
	{
		parent::__construct();
		$this->load->library('booking_data_formatter');
		$this->load->library('provab_mailer');
		$this->current_module = $this->config->item('current_module');
		//$this->load->library('provab_pdf');
		
		//we need to activate bus api which are active for current domain and load those libraries
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 *
	 */
	function bus($app_reference, $booking_source='', $booking_status='', $operation='show_voucher',$email='')
	{
		//echo 'under working';exit;
		$this->load->model('bus_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->bus_model->get_booking_details($app_reference, $booking_source, $booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_bus_booking_data($booking_details, $this->current_module);
				$page_data['data'] = $assembled_booking_details['data'];
				
				if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
					$domain_address = $this->custom_db->single_table_records ( 'domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					if($assembled_booking_details['data']['booking_details'][0]['created_by_id'] > 0){
						$get_agent_info = $this->user_model->get_agent_info($assembled_booking_details['data']['booking_details'][0]['created_by_id']);
						if(!empty($get_agent_info)){
							$page_data['data']['address'] = $get_agent_info[0]['address'];
							$page_data['data']['logo'] = $get_agent_info[0]['logo'];
						}
					}
				
				}
				switch ($operation) {
					case 'show_voucher' : $this->template->view('voucher/bus_voucher', $page_data);
					break;
					case 'show_pdf' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->template->isolated_view('voucher/bus_pdf', $page_data);
						$create_pdf->create_pdf($get_view,'show');					
						break;
						
					case 'email_voucher' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->template->isolated_view('voucher/bus_pdf', $page_data);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$this->provab_mailer->send_mail($email, domain_name().' - Bus Ticket',$mail_template ,$pdf);
						break;
				}
			} else {
				redirect('security/log_event?event=Invalid AppReference');
			}
		} else {
			redirect('security/log_event?event=Invalid AppReference');
		}
	}

	

 
	/**
	 *
	 */
	function hotel($app_reference, $booking_source='', $booking_status='', $operation='show_voucher',$email='')
	{
		$this->load->model('hotel_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, $this->current_module);
				$page_data['data'] = $assembled_booking_details['data'];
				if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
				
					$domain_address = $this->custom_db->single_table_records ('domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					if($assembled_booking_details['data']['booking_details'][0]['created_by_id'] > 0){
						$get_agent_info = $this->user_model->get_agent_info($assembled_booking_details['data']['booking_details'][0]['created_by_id']);
						if(!empty($get_agent_info)){
							$page_data['data']['address'] = $get_agent_info[0]['address'];
							$page_data['data']['logo'] = $get_agent_info[0]['logo'];
						}
					}
				
				}
				switch ($operation) {
					case 'show_voucher' : $this->template->view('voucher/hotel_voucher', $page_data);
					break;
					case 'show_pdf' :						
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->template->isolated_view('voucher/hotel_pdf', $page_data);						
						$create_pdf->create_pdf($get_view,'show');
						
					break;
					case 'email_voucher' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->template->isolated_view('voucher/hotel_pdf', $page_data);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$this->provab_mailer->send_mail($email, domain_name().' - Hotel Ticket',$mail_template ,$pdf);
						break;
				}
			}
		}
	}
	
	function flight($app_reference, $booking_source='', $booking_status='', $operation='show_voucher',$email='')
	{
		//error_reporting(E_ALL);
		$this->load->model('flight_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
		
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);				
				$page_data['data'] = $assembled_booking_details['data'];
				
			if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
					
					$domain_address = $this->custom_db->single_table_records ( 'domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					if($assembled_booking_details['data']['booking_details'][0]['created_by_id'] > 0){
						$get_agent_info = $this->user_model->get_agent_info($assembled_booking_details['data']['booking_details'][0]['created_by_id']);
						if(!empty($get_agent_info)){
						$page_data['data']['address'] = $get_agent_info[0]['address'];
						$page_data['data']['logo'] = $get_agent_info[0]['logo'];
						}
					}		
			
				}
					
				switch ($operation) {
					case 'show_voucher' : $this->template->view('voucher/flight_voucher', $page_data);
					break;
					case 'show_pdf' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->template->isolated_view('voucher/flight_pdf', $page_data);
						$create_pdf->create_pdf($get_view,'show');
						break;
				   case 'email_voucher':
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->template->isolated_view('voucher/flight_pdf', $page_data);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$this->provab_mailer->send_mail($email, domain_name().' - Flight Ticket',$mail_template ,$pdf);
						break;
				}
			}
		}
	}

	function flight_update_crs_ticket($app_reference, $booking_source='', $booking_status='', $operation='show_voucher',$email='')
	{
		$this->load->model ( 'custom_db' );
		$this->load->model('flight_model');
		$post_data = $this->input->post ();
		if (valid_array ( $post_data )) {

			$pass ['booking_id'] = trim ( $post_data ['booking_id'] );
			$pass ['app_refernce'] = trim ( $post_data ['app_refernce'] );
			$pnr=substr(md5(microtime()*rand(0,99)),0,6);
			$request ['pnr']=strtoupper($pnr);
			$request ['status'] = trim ( $post_data ['status'] );
			$request ['origin'] = trim ( $post_data ['origin'] );

			$flight_orgin =trim ( $post_data ['flight_orgin'] );
			$joury_date =trim ( $post_data ['joury_date'] );
			$flight_number =trim ( $post_data ['flight_number'] );


			$app_refernce=trim ( $post_data ['app_refernce'] );
			

			$this->custom_db->update_record ( 'flight_booking_transaction_details', $request, array (
					'app_reference' => $app_refernce 
			) );

			$requests ['status'] = trim ( $post_data ['status'] );
			$requests ['crs_status'] = trim ( $post_data ['status'] );

			$this->custom_db->update_record ( 'flight_booking_details', $requests, array (
					'app_reference' => $app_refernce 
			) );

			$requestss ['status'] = trim ( $post_data ['status'] );

			$this->custom_db->update_record ( 'flight_booking_passenger_details', $requestss, array (
					'app_reference' => $app_refernce 
			) );

            $ticket_id=$post_data ['ticket_id'];
			$ticket_no=$post_data ['ticket_status'];
			$fare=$post_data ['fare'];
			
			
			$tickets=array_combine($ticket_id,$ticket_no); 
            $passenger_fk=$this->flight_model->get_passenger_info_details($post_data ['booking_id'],$post_data ['app_refernce']);
            $pass_info=$this->flight_model->get_passenger_ticket();
            $flight_details=$this->flight_model->get_seats_crs($flight_orgin);
            //debug($flight_details);
            $ticket_count=count($ticket_no);
            $no_of_seats=$flight_details['0']['no_of_seats'];
            
            $booked_seats=$flight_details['0']['booked_seats'] + $ticket_count;
            $hold_seats=$flight_details['0']['hold_seats'] - $ticket_count;

            $holddd_seats=$flight_details['0']['hold_seats'] - $ticket_count;
            $left_seats=$flight_details['0']['left_seats'] + $ticket_count;

            $count_row=count($passenger_fk);
            $count_pass=count($pass_info);
            $ticket_count=count($ticket_no);
            $j=0;
            foreach ($tickets as $key => $value) 
            {
            	
            	foreach ($pass_info as $key_s => $pass)
                {
            	    $pass_info_orgin[]=$pass['passenger_fk']; 
                } 
                $id=$passenger_fk[$j]['origin']; 
                $fare_price=$fare[$j]; 
            	if(in_array($id,$pass_info_orgin))
            	   {
            	 	   if($post_data ['status'] =='BOOKING_CONFIRMED')
			   {
            	                $ticket['TicketId']=$key; 
                                $ticket['TicketNumber']=$value; 
                                $this->custom_db->update_record ('flight_passenger_ticket_info',$ticket, array (
					              'passenger_fk' => $id
			                 ) );
            		    }
                            $j++;
            		    
            	 	  
            	   }
                 if(!in_array($id,$pass_info_orgin))
            	 {
                    if($post_data ['status']=='BOOKING_CONFIRMED')
		    {
                        $ticketsind['hold_seats']=$hold_seats;
		        $ticketsind['booked_seats']=$booked_seats;
                        $this->custom_db->update_record ('flight_crs_list',$ticketsind,array('orgin' => $flight_orgin) );
                        
                        $passenger_fk_value = $id; 
            	        $TicketId = $key;
			$TicketNumber = $value; 
			$IssueDate = date('Y-m-d H:i:s');
			$Fare = $fare_price;
			$SegmentAdditionalInfo = '';
			$ValidatingAirline = '';
			$CorporateCode = '';
			$TourCode = '';
			$Endorsement = '';
			$Remarks = '';
			$ServiceFeeDisplayType = '';
					
			$this->flight_model->save_passenger_ticket_info($passenger_fk_value, $TicketId, $TicketNumber,
				$IssueDate, $Fare,$SegmentAdditionalInfo,$ValidatingAirline, $CorporateCode, $TourCode, 
				$Endorsement, $Remarks, $ServiceFeeDisplayType);
		      }
		      if($post_data ['status']!='BOOKING_CONFIRMED')
		    {
                        $ticketsind['hold_seats']=$holddd_seats;
		        $ticketsind['left_seats']=$left_seats;
                        $this->custom_db->update_record ('flight_crs_list',$ticketsind,array('orgin' => $flight_orgin) );
            }
            	      $j++;
                      
            	 }
                 
                   
            	   
          
            }
            $this->test($flight_orgin,$joury_date,$flight_number);
           	
		    
		}
		$this->load->model('flight_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);				
				$page_data['data'] = $assembled_booking_details['data'];
				
			if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
					
					$domain_address = $this->custom_db->single_table_records ( 'domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					if($assembled_booking_details['data']['booking_details'][0]['created_by_id'] > 0){
						$get_agent_info = $this->user_model->get_agent_info($assembled_booking_details['data']['booking_details'][0]['created_by_id']);
						if(!empty($get_agent_info)){
						$page_data['data']['address'] = $get_agent_info[0]['address'];
						$page_data['data']['logo'] = $get_agent_info[0]['logo'];
						}
					}		
			
				}
				switch ($operation) {
					case 'show_voucher' :
						$this->template->view ( 'voucher/flight_update_crs_ticket', $page_data );
						break;
				}
			}
		}
	}

	function test($flight_orgin,$joury_date,$flight_number)
	{
		redirect('flight_crs/b2b_flight_report_crs/0/'.$flight_orgin.'/'.$joury_date.'/'.$flight_number.'/0');
	}



		function flight_invoice($app_reference, $booking_source='', $booking_status='', $operation='show_voucher')
	{
		$this->load->model('flight_model');
		if (empty($app_reference) == false) {
			$data = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
			//debug($data);exit;
			if ($data['status'] == SUCCESS_STATUS) {
				//depending on booking source we need to convert to view array
				load_flight_lib($data['data']['booking_details']['booking_source']);
				$page_data = $this->flight_lib->parse_voucher_data($data['data']);
				$domain_details = $this->custom_db->single_table_records('domain_list', '*', array('origin' => $page_data['booking_details']['domain_origin']));
				$page_data['domain_details'] = $domain_details['data'][0];
				switch ($operation) {
					case 'show_voucher' : $this->template->view('voucher/flight_invoice', $page_data);
					break;
				}
			}
		}
	}
}
