<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('max_execution_time', 300);
/**
 *
 * @package    Provab
 * @subpackage Flight
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */

class Flight extends CI_Controller {
	private $current_module;
	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
		$this->load->model('flight_model');
		$this->load->model('domain_management_model');
		$this->current_module = $this->config->item('current_module');
	}

	function get_booking_details($app_reference)
	{
		//
		$condition[] = array('BD.app_reference', '=', $this->db->escape($app_reference));
		$details = $this->flight_model->get_booking_details($app_reference);
		if ($details['status'] == SUCCESS_STATUS) {
			$booking_source = $details['data']['booking_details']['booking_source'];
			load_flight_lib($booking_source);
			$this->flight_lib->get_booking_details($details['data']['booking_details'], $details['data']['booking_transaction_details']);
		}
	}
	/**
	 * Cancellation
	 * Jaganath
	 */
	function pre_cancellation($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details,$this->current_module);
				$page_data['data'] = $assembled_booking_details['data'];
				$this->template->view('flight/pre_cancellation', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}

	function crs_cancel_supplier_request($app_reference, $booking_source,$crs_status,
		$flight_id,$joury_date,$flight_numberin,$flight_number_out)
	{

                  
		
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source);
			//debug($booking_details);
			foreach ($booking_details as $key => $value) 
			{
				$count_pax=count($value['booking_customer_details']);
				$flight_deatils=json_decode($value['booking_itinerary_details']['0']['attributes']);

			}
			foreach ($flight_deatils as $key ) 
			{
				$orgin=$key->orgin;
			}
			$flight=$this->flight_model->get_seats_crs($orgin);
			$seats['left_seats']=$flight['0']['left_seats'] + $count_pax;
		    $seats['booked_seats']=$flight['0']['booked_seats'] - $count_pax;
            if ($booking_details['status'] == SUCCESS_STATUS) {
                
				$requests ['status'] = "BOOKING_CANCELLED";
				$requests ['crs_status'] = "BOOKING_CANCELLED";
               $this->custom_db->update_record ('flight_booking_details',$requests, array (
					'app_reference' => $app_reference 
			) );


               $this->custom_db->update_record ('flight_crs_list',$seats, array (
					              'orgin' => $orgin
			                 ) );

               $requestss ['status'] = "BOOKING_CANCELLED";

			$this->custom_db->update_record ('flight_booking_passenger_details',$requestss, array (
					'app_reference' => $app_reference 
			) );

              $requestsing ['status'] = "BOOKING_CANCELLED";
			$this->custom_db->update_record ('flight_booking_transaction_details',$requestsing, array (
					'app_reference' => $app_reference 
			) );
			
             $this->test($flight_id,$joury_date,$flight_numberin);
             
               
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}

	function test($flight_orgin,$joury_date,$flight_number)
	{
		redirect('flight_crs/b2b_flight_report_crs/0/'.$flight_orgin.'/'.$joury_date.'/'.$flight_number.'/0');
	}




	function update_refund_status($app_reference)
	{
		
		if(isset($app_reference))
		{
			$passenger_fk=$this->flight_model->get_passenger_info_details_in($app_reference);
			foreach ($passenger_fk as $key => $value)
			{
				 //debug($value);
				 $value['origin'];
				 $post_data = $this->input->post ();
				$requestss ['Remarks'] = $post_data ['remarks'];
				$this->custom_db->update_record ('flight_passenger_ticket_info',$requestss,
				 array ('passenger_fk' => $value['origin'] ) );
			}
			redirect('report/b2b_flight_report/');
		}
		else
		{
			redirect('security/log_event?event=Invalid Details');
		}
	}
	
	/**
	 * Jaganath
	 * @param $app_reference
	 */
	function cancel_booking()
	{
		$post_data = $this->input->post();
		if (isset($post_data['app_reference']) == true && isset($post_data['booking_source']) == true && isset($post_data['transaction_origin']) == true &&
			valid_array($post_data['transaction_origin']) == true && isset($post_data['passenger_origin']) == true && valid_array($post_data['passenger_origin']) == true) {
			$app_reference = trim($post_data['app_reference']);
			$booking_source = trim($post_data['booking_source']);
			$transaction_origin = $post_data['transaction_origin'];
			$passenger_origin = $post_data['passenger_origin'];
			$booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib($booking_source);
				//Formatting the Data
				$this->load->library('booking_data_formatter');
				$booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);
				$booking_details = $booking_details['data'];
				//Grouping the Passenger Ticket Ids
				$grouped_passenger_ticket_details = $this->flight_lib->group_cancellation_passenger_ticket_id($booking_details, $passenger_origin);
				$passenger_origin = $grouped_passenger_ticket_details['passenger_origin'];
				$passenger_ticket_id = $grouped_passenger_ticket_details['passenger_ticket_id'];
				$cancellation_details = $this->flight_lib->cancel_booking($booking_details, $passenger_origin, $passenger_ticket_id);
				redirect('flight/cancellation_details/'.$app_reference.'/'.$booking_source);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	/**
	 *
	 * @param $app_reference
	 * @param $booking_source
	 */
	function cancellation_details($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$page_data = array();
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_flight_booking_data($master_booking_details, 'b2c');
				$page_data['data'] = $master_booking_details['data'];
				$this->template->view('flight/cancellation_details', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}

	}
	/**
	 * Jaganath
	 * Get supplier cancellation status
	 */
	public function update_supplier_cancellation_status_details()
	{
		$get_data = $this->input->get();
		if(isset($get_data['app_reference']) == true && isset($get_data['booking_source']) == true && isset($get_data['passenger_status']) == true && $get_data['passenger_status'] == 'BOOKING_CANCELLED' && isset($get_data['passenger_origin']) == true && intval($get_data['passenger_origin']) > 0){
			$app_reference = trim($get_data['app_reference']);
			$booking_source = trim($get_data['booking_source']);
			$passenger_origin = trim($get_data['passenger_origin']);
			$passenger_status = trim($get_data['passenger_status']);
			$booking_details = $this->flight_model->get_passenger_ticket_info($app_reference, $passenger_origin, $passenger_status);
			if($booking_details['status'] == SUCCESS_STATUS){
				$master_booking_details = $booking_details['data']['booking_details'][0];
				$booking_customer_details = $booking_details['data']['booking_customer_details'][0];
				$cancellation_details = $booking_details['data']['cancellation_details'][0];
				$booking_source = $master_booking_details['booking_source'];
				$request_data = array();
				$request_data['AppReference'] = 		$booking_customer_details['app_reference'];
				$request_data['SequenceNumber'] =		$booking_customer_details['sequence_number'];
				$request_data['BookingId'] = 			$booking_customer_details['book_id'];
				$request_data['PNR'] = 					$booking_customer_details['pnr'];
				$request_data['TicketId'] = 			$booking_customer_details['TicketId'];
				$request_data['ChangeRequestId'] =	$cancellation_details['RequestId'];
				load_flight_lib($booking_source);
				$supplier_ticket_refund_details = $this->flight_lib->get_supplier_ticket_refund_details($request_data);
				if($supplier_ticket_refund_details['status'] == SUCCESS_STATUS){
					$this->flight_model->update_supplier_ticket_refund_details($passenger_origin, $supplier_ticket_refund_details['data']);
				}
			}
		}
	}
	/**
	 * Jaganath
	 * Displays Cancellation Ticket Details
	 */
	public function ticket_cancellation_details()
	{
		$get_data = $this->input->get();
		if(isset($get_data['app_reference']) == true && isset($get_data['booking_source']) == true && isset($get_data['status']) == true){
			$app_reference = trim($get_data['app_reference']);
			$booking_source = trim($get_data['booking_source']);
			$status = trim($get_data['status']);
			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $status);
			if($booking_details['status'] == SUCCESS_STATUS){
				$this->load->library('booking_data_formatter');
				$booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->config->item('current_module'));
				$page_data = array();
				$booked_user_id = intval($booking_details['data']['booking_details'][0]['created_by_id']);
				$booked_user_details = array();
				$is_agent = false;
				$user_condition[] = array('U.user_id' ,'=', $booked_user_id);
				$booked_user_details = $this->user_model->get_user_details($user_condition);
				if(valid_array($booked_user_details) == true){
					$booked_user_details = $booked_user_details[0];
					if($booked_user_details['user_type'] == B2B_USER){
						$is_agent = true;
					}
				}
				$page_data['booking_data'] = $booking_details['data'];
				$page_data['booked_user_details'] =	$booked_user_details;
				$page_data['is_agent'] = 			$is_agent;
				$this->template->view('flight/ticket_cancellation_details', $page_data);
			} else {
				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	}
	/**
	 * Jaganath
	 * Displays Ticket cancellation Refund details
	 */
	public function cancellation_refund_details()
	{
		$get_data = $this->input->get();
		if(isset($get_data['app_reference']) == true && isset($get_data['booking_source']) == true && isset($get_data['passenger_status']) == true && $get_data['passenger_status'] == 'BOOKING_CANCELLED' && isset($get_data['passenger_origin']) == true && intval($get_data['passenger_origin']) > 0){
			$app_reference = trim($get_data['app_reference']);
			$booking_source = trim($get_data['booking_source']);
			$passenger_origin = trim($get_data['passenger_origin']);
			$passenger_status = trim($get_data['passenger_status']);
			$booking_details = $this->flight_model->get_passenger_ticket_info($app_reference, $passenger_origin, $passenger_status);
			if($booking_details['status'] == SUCCESS_STATUS){
				$booked_user_id = intval($booking_details['data']['booking_details'][0]['created_by_id']);
				$booked_user_details = array();
				$is_agent = false;
				$user_condition[] = array('U.user_id' ,'=', $booked_user_id);
				$booked_user_details = $this->user_model->get_user_details($user_condition);
				if(valid_array($booked_user_details) == true){
					$booked_user_details = $booked_user_details[0];
					if($booked_user_details['user_type'] == B2B_USER){
						$is_agent = true;
					}
				}
				$page_data = array();
				$page_data['booking_data'] = $booking_details['data'];
				$page_data['booked_user_details'] =	$booked_user_details;
				$page_data['is_agent'] = 			$is_agent;
				$this->template->view('flight/cancellation_refund_details', $page_data);
			} else {
				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	}
	/**
	 * Jaganath
	 * Update Ticket Refund Details
	 */
	public function update_ticket_refund_details()
	{
		$post_data = $this->input->post();
		$redirect_url_params = array();
		$this->form_validation->set_rules('app_reference', 'app_reference', 'trim|required|xss_clean');
		$this->form_validation->set_rules('passenger_origin', 'passenger_origin', 'trim|required|min_length[1]|numeric');
		$this->form_validation->set_rules('passenger_status', 'passenger_status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('refund_payment_mode', 'refund_payment_mode', 'trim|required|xss_clean');
		$this->form_validation->set_rules('refund_amount', 'refund_amount', 'trim|numeric');
		$this->form_validation->set_rules('cancellation_charge', 'cancellation_charge', 'trim|numeric');
		$this->form_validation->set_rules('service_tax_on_refund_amount', 'service_tax_on_refund_amount', 'trim|numeric');
		$this->form_validation->set_rules('swachh_bharat_cess', 'swachh_bharat_cess', 'trim|numeric');
		$this->form_validation->set_rules('refund_status', 'refund_status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('refund_comments', 'UserId', 'trim|required');
		if ($this->form_validation->run()) {
			$app_reference = 				trim($post_data['app_reference']);
			$passenger_origin = 			intval($post_data['passenger_origin']);
			$passenger_status = 			trim($post_data['passenger_status']);
			$refund_payment_mode = 			trim($post_data['refund_payment_mode']);
			$refund_amount = 				floatval($post_data['refund_amount']);
			$cancellation_charge = 			floatval($post_data['cancellation_charge']);
			$service_tax_on_refund_amount =	floatval($post_data['service_tax_on_refund_amount']);
			$swachh_bharat_cess = 			floatval($post_data['swachh_bharat_cess']);
			$refund_status = 				trim($post_data['refund_status']);
			$refund_comments = 				trim($post_data['refund_comments']);
			//Get Ticket Details
			$booking_details = $this->flight_model->get_passenger_ticket_info($app_reference, $passenger_origin, $passenger_status);
			if($booking_details['status'] == SUCCESS_STATUS){
				$master_booking_details = $booking_details['data']['booking_details'][0];
				$booking_customer_details = $booking_details['data']['booking_customer_details'][0];
				$cancellation_details = $booking_details['data']['cancellation_details'][0];
				$booking_currency = $master_booking_details['currency'];//booking currency
				$booked_user_id = intval($master_booking_details['created_by_id']);
				$user_condition[] = array('U.user_id' ,'=', $booked_user_id);
				$booked_user_details = $this->user_model->get_user_details($user_condition);
				$is_agent = false;
				if(valid_array($booked_user_details) == true && $booked_user_details[0]['user_type'] == B2B_USER){
					$is_agent = true;
				}
				$currency_obj = new Currency(array('from' => get_application_default_currency() , 'to' => $booking_currency));
				$currency_conversion_rate = $currency_obj->currency_conversion_value(true, get_application_default_currency(), $booking_currency);
				if($refund_status == 'PROCESSED' && floatval($refund_amount) > 0 && $is_agent == true){
					//1.Crdeit the Refund Amount to Respective Agent
					$agent_refund_amount = ($currency_conversion_rate*$refund_amount);//converting to agent currency
					$this->domain_management_model->update_agent_balance($agent_refund_amount, $booked_user_id);
					//2.Add Transaction Log for the Refund
					$fare = -($refund_amount);//dont remove: converting to negative
					$domain_markup=0;
					$level_one_markup=0;
					$convinence = 0;
					$discount = 0;
					$remarks = 'flight Refund was Successfully done';
					$this->domain_management_model->save_transaction_details('flight', $app_reference, $fare, $domain_markup, $level_one_markup, $remarks, $convinence, $discount, $booking_currency, $currency_conversion_rate);
				}
				//UPDATE THE REFUND DETAILS
				//Update Condition
				$update_refund_condition = array();
				$update_refund_condition['passenger_fk'] =	$passenger_origin;
				//Update Data
				$update_refund_details = array();
				$update_refund_details['refund_payment_mode'] = 			$refund_payment_mode;
				$update_refund_details['refund_amount'] =					$refund_amount;
				$update_refund_details['cancellation_charge'] = 			$cancellation_charge;
				$update_refund_details['service_tax_on_refund_amount'] =	$service_tax_on_refund_amount;
				$update_refund_details['swachh_bharat_cess'] = 				$swachh_bharat_cess;
				$update_refund_details['refund_status'] = 					$refund_status;
				$update_refund_details['refund_comments'] = 				$refund_comments;
				$update_refund_details['currency'] = 						$booking_currency;
				$update_refund_details['currency_conversion_rate'] = 		$currency_conversion_rate;
				if($refund_status == 'PROCESSED'){
					$update_refund_details['refund_date'] = 				date('Y-m-d H:i:s');
				}
				$this->custom_db->update_record('flight_cancellation_details', $update_refund_details, $update_refund_condition);
				
				$redirect_url_params['app_reference'] = $app_reference;
				$redirect_url_params['booking_source'] = $master_booking_details['booking_source'];
				$redirect_url_params['passenger_status'] = $passenger_status;
				$redirect_url_params['passenger_origin'] = $passenger_origin;
			}
		}
		redirect('flight/cancellation_refund_details?'.http_build_query($redirect_url_params));
	}
	/**
	 * Arjun J Gowda
	 */
	function exception()
	{
		$module = META_AIRLINE_COURSE;
		$op = @$_GET['op'];
		$notification = @$_GET['notification'];
		$eid = $this->module_model->log_exception($module, $op, $notification);
		//set ip log session before redirection
		$this->session->set_flashdata(array('log_ip_info' => true));
		redirect(base_url().'index.php/flight/event_logger/'.$eid);
	}

	function event_logger($eid='')
	{
		$log_ip_info = $this->session->flashdata('log_ip_info');
		$this->template->view('flight/exception', array('log_ip_info' => $log_ip_info, 'eid' => $eid));
	}
}