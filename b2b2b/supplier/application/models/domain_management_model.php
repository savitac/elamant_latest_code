<?php
require_once 'abstract_management_model.php';
/**
 * @package    Provab Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class Domain_Management_Model extends Abstract_Management_Model
{
	function __construct() {
		parent::__construct('level_2');
	}
	
/**
	 * Arjun J Gowda
	 * Get markup based on different modules
	 * @return array('value' => 0, 'type' => '')
	 */
	function get_markup($module_name)
	{
		$markup_data = '';
		switch ($module_name) {
			case 'flight' : $markup_data = $this->b2b_airline_markup();
			break;
			case 'hotel' : $markup_data = $this->b2b_hotel_markup();
			break;
			case 'bus' : $markup_data = $this->b2b_bus_markup();
			break;
		}
		return $markup_data;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for b2b domain
	 */
	function b2b_airline_markup()
	{
		if (empty($this->airline_markup) == true) {
			$response['specific_markup_list'] = $this->specific_airline_markup('b2b_flight');
			$response['generic_markup_list'] = $this->generic_domain_markup('b2b_flight');
			$this->airline_markup = $response;
		} else {
			$response = $this->airline_markup;
		}
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for b2b domain
	 */
	function b2b_hotel_markup()
	{
		if (empty($this->hotel_markup) == true) {
			$response['specific_markup_list'] = '';
			$response['generic_markup_list'] = $this->generic_domain_markup('b2b_hotel');
			$this->hotel_markup = $response;
		} else {
			$response = $this->hotel_markup;
		}
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for b2b domain
	 */
	function b2b_bus_markup()
	{
		if (empty($this->bus_markup) == true) {
			$response['specific_markup_list'] = '';
			$response['generic_markup_list'] = $this->generic_domain_markup('b2b_bus');
			$this->bus_markup = $response;
		} else {
			$response = $this->bus_markup;
		}
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for provab - Domain wise and module wise
	 */
	function b2c_airline_markup()
	{
		$response['data'] = '';
		$response['data']['specific_markup_list'] = $this->specific_airline_markup('b2c_flight');
		$response['data']['generic_markup_list'] = $this->generic_domain_markup('b2c_flight');
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for provab - Domain wise and module wise
	 */
	function b2c_hotel_markup()
	{
		$response['data'] = '';
		$response['data']['specific_markup_list'] = '';
		$response['data']['generic_markup_list'] = $this->generic_domain_markup('b2c_hotel');
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for provab - Domain wise and module wise
	 */
	function b2c_bus_markup()
	{
		$response['data'] = '';
		$response['data']['specific_markup_list'] = '';
		$response['data']['generic_markup_list'] = $this->generic_domain_markup('b2c_bus');
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Get generic markup based on the module type
	 * @param $module_type
	 * @param $markup_level
	 */
	function generic_domain_markup($module_type)
	{
		$query = 'SELECT ML.origin AS markup_origin, ML.type AS markup_type, ML.reference_id, ML.value, ML.value_type
		FROM markup_list AS ML where ML.module_type = "'.$module_type.'" and
		ML.markup_level = "'.$this->markup_level.'" and ML.type="generic" and ML.domain_list_fk='.get_domain_auth_id();
		$generic_data_list = $this->db->query($query)->result_array();
		return $generic_data_list;
	}

	/**
	 * Get specific markup based on module type
	 * @param string $module_type	Name of the module for which the markup has to be returned
	 * @param string $markup_level	Level of markup
	 */
	function specific_airline_markup($module_type)
	{
		$sub_query = 'SELECT AL.origin 
		FROM airline_list AS AL 
		JOIN markup_list AS ML ON
		ML.module_type = "'.$module_type.'" and ML.markup_level = "'.$this->markup_level.'" and AL.origin=ML.reference_id and ML.type="specific"
		and ML.domain_list_fk != 0  and ML.domain_list_fk='.get_domain_auth_id();
		
		$query = 'SELECT AL.origin AS airline_origin, AL.name AS airline_name, AL.code AS airline_code,
		ML.origin AS markup_origin, ML.type AS markup_type, ML.reference_id, ML.value, ML.value_type
		FROM airline_list AS AL LEFT JOIN markup_list AS ML ON
		ML.module_type = "'.$module_type.'" and ML.markup_level = "'.$this->markup_level.'" and AL.origin=ML.reference_id and ML.type="specific"
		and ML.domain_list_fk != 0  and ML.domain_list_fk='.get_domain_auth_id().' 
		where (AL.has_specific_markup='.ACTIVE.' OR AL.origin in ('.$sub_query.')) order by AL.name ASC';
		$specific_data_list = $this->db->query($query)->result_array();
		return $specific_data_list;
	}
	/**
	 * Get Details based on Airline Code
	 * @param string $module_type	Name of the module for which the markup has to be returned
	 * @param string $markup_level	Level of markup
	 */
	function individual_airline_markup_details($module_type, $airline_code)
	{
		$query = 'SELECT ML.origin as markup_list_origin,AL.origin as airline_list_origin 
		FROM airline_list AS AL 
		left JOIN markup_list AS ML ON
		ML.module_type = "'.$module_type.'" and ML.markup_level = "'.$this->markup_level.'" and AL.origin=ML.reference_id and ML.type="specific"
		and ML.domain_list_fk != 0  and ML.domain_list_fk='.get_domain_auth_id().' where AL.code="'.$airline_code.'"';
		$specific_data_list = $this->db->query($query)->row_array();
		return $specific_data_list;
	}
	
	/**
	 * Get specific markup based on module type
	 * @param string $module_type	Name of the module for which the markup has to be returned
	 * @param string $markup_level	Level of markup
	 */
	function specific_agent_markup($module_type)
	{
		//FIXME
		$query = 'SELECT AL.origin AS airline_origin, AL.name AS airline_name, AL.code AS airline_code,
		ML.origin AS markup_origin, ML.type AS markup_type, ML.reference_id, ML.value, ML.value_type
		FROM airline_list AS AL LEFT JOIN markup_list AS ML ON
		ML.module_type = "'.$module_type.'" and ML.markup_level = "'.$this->markup_level.'" and AL.origin=ML.reference_id and ML.type="specific"
		and ML.domain_list_fk != 0  and ML.domain_list_fk='.get_domain_auth_id().' order by AL.name ASC';
		$specific_data_list = $this->db->query($query)->result_array();
		return $specific_data_list;
	}

	/**
	 * save master transaction details request
	 * @param array $details
	 */
	function save_master_transaction_details($details)
	{
		$master_transaction_details['system_transaction_id'] = 'DEP-'.$this->entity_user_id.time();
		$master_transaction_details['domain_list_fk'] = get_domain_auth_id();
		$master_transaction_details['transaction_type'] = $details['transaction_type'];
		$master_transaction_details['amount'] = $details['amount'];
		$master_transaction_details['currency_converter_origin'] = $details['currency_converter_origin'];
		$master_transaction_details['conversion_value'] = $details['conversion_value'];
		$master_transaction_details['date_of_transaction'] = valid_date_value($details['date_of_transaction']);
		$master_transaction_details['bank'] = $details['bank'];
		$master_transaction_details['branch'] = $details['branch'];
		$master_transaction_details['transaction_number'] = isset($details['transaction_number']) ? $details['transaction_number'] : 'N/A';
		$master_transaction_details['status'] = 'pending';
		$master_transaction_details['remarks'] = $details['remarks'];
		$master_transaction_details['created_datetime'] = db_current_datetime();
		$master_transaction_details['created_by_id'] = $this->entity_user_id;
		$master_transaction_details['user_oid'] = $this->entity_user_id;
		$insert_id = $this->custom_db->insert_record('master_transaction_details', $master_transaction_details);
		return $insert_id['insert_id'];
	}

	/**
	 * Master Transaction Request List
	 */
	function master_transaction_request_list($type='provab', $data_list_filt=array())
	{
		$data_list_cond = '';
		if (valid_array($data_list_filt) == true) {
			$data_list_cond = $this->custom_db->get_custom_condition($data_list_filt);
		}
		$query = 'select MTD.*, CONCAT(U.first_name, " ", U.last_name) request_user, U.email, U.agency_name AS requested_from from master_transaction_details MTD, user U
		where MTD.created_by_id=U.user_id AND MTD.type="'.$type.'" and MTD.domain_list_fk = '.get_domain_auth_id().' '.$data_list_cond.'
		order by MTD.updated_datetime DESC, MTD.created_datetime DESC';
		return $this->db->query($query)->result_array();
	}

	/**
	 *
	 */
	function event_logs($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		//BT, CD, ID
		if ($count) {
			$query = 'select count(*) as total_records from exception_logger where domain_origin='.get_domain_auth_id();
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$query = 'select * from exception_logger where domain_origin='.get_domain_auth_id().' order by origin desc limit '.$offset.', '.$limit;
			return $this->db->query($query)->result_array();
		}
	}

	/**
	 * Process Update Request
	 * @param number $origin
	 * @param string $system_request_id
	 * @param string $status_id
	 * @param string $update_remarks
	 *
	 * @return $response status of the update operation
	 */
	function process_balance_request($origin, $system_request_id, $status_id, $update_remarks)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		//get amount details to process - safety
		$transaction_details_cond = array('origin' => intval($origin), 'system_transaction_id' => $system_request_id, 'type' => 'b2b');
		//Depending on status update
		$transaction_details = $this->custom_db->single_table_records('master_transaction_details', '*', $transaction_details_cond);
		if (valid_array($transaction_details['data']) == true && strtoupper($transaction_details['data'][0]['status']) == 'PENDING') {
			$response['data'] = $transaction_details['data'][0];
			//data to be updated
			$transaction_data = array(
							'update_remarks' => $update_remarks, 'status' => strtolower($status_id),
							'updated_datetime' => db_current_datetime(), 'updated_by_id' => intval($this->entity_user_id)
			);
			$amount = ($transaction_details['data'][0]['amount']*$transaction_details['data'][0]['currency_conversion_rate']);//FORCE TO INR
			if (strtoupper($status_id) == 'ACCEPTED') {
				//Add to current balance and continue
				$domain_origin = $transaction_details['data'][0]['domain_list_fk'];
				//update balance details and notification
				$this->load->model('private_management_model');
				//passing negative so balance gets deducted before processing
				//No need to deduct Domain balance
				//$response['data']['current_balance'] = $this->private_management_model->update_domain_balance(get_domain_auth_id(), -$amount);
				
				$response['data']['agent_balance'] = $this->private_management_model->update_b2b_balance($transaction_details['data'][0]['created_by_id'], $amount);
				$this->custom_db->update_record('master_transaction_details', $transaction_data, $transaction_details_cond);
				//Application Logger
				$user_id = intval($transaction_details['data'][0]['created_by_id']);
				$user_condition[] = array('user_id' ,'=', $user_id);
				$user_details = $this->user_model->get_user_details($user_condition);
				$agency_name = $user_details[0]['agency_name'];
				$remarks = 'Deposit Request <span class="label label-success">'.strtoupper($status_id).'</span>:'.$amount.' '.get_application_default_currency().'('.$agency_name.')';
				$admin_user_id = $this->user_model->get_admin_user_id();
				$notification_users = array_merge($admin_user_id, array($user_id));
				$this->application_logger->balance_deposit_request($remarks, array('system_transaction_id' => $system_request_id), $notification_users);
			} elseif (strtoupper($status_id) != 'ACCEPTED') {
				$this->custom_db->update_record('master_transaction_details', $transaction_data, $transaction_details_cond);
			}
		} else {
			$response['status']	= FAILURE_STATUS;
		}
		return  $response;
	}

	/**
	 * update domain balance details
	 * @param number $domain_origin	doamin unique key
	 * @param number $amount		amount to be added or deducted(-100 or +100)
	 */
	function update_domain_balance($origin, $amount)
	{
		$current_balance = 0;
		$cond = array('origin' => intval($origin));
		$details = $this->custom_db->single_table_records('b2b_user_details', 'balance', $cond);
		if ($details['status'] == true) {
			$details['data'][0]['balance'] = $current_balance = ($details['data'][0]['balance'] + $amount);
			$this->custom_db->update_record('b2b_user_details', $details['data'][0], $cond);
		}
		return $current_balance;
	}
	/**
	 * Jaganath
	 * B2B Agent Commission Details: Flight, Hotel, Bus
	 * @param $agent_fk
	 */
	function get_commission_details($agent_fk)
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		
		$agent_details_query = 'select U.*,BUD.logo as agent_logo, UT.user_type
								FROM user AS U 
								INNER JOIN user_type AS UT ON U.user_type=UT.origin
								left JOIN b2b_user_details AS BUD ON U.user_id=BUD.user_oid 
								WHERE U.domain_list_fk ='.get_domain_auth_id().' and U.user_id = '.intval($agent_fk);
		$flight_commission_query = 'select BFCD.* from b2b_flight_commission_details as BFCD 
								inner join user as U on BFCD.agent_fk = U.user_id
								where BFCD.domain_list_fk ='.get_domain_auth_id().' and BFCD.agent_fk='.intval($agent_fk).' and BFCD.type="specific"'; 
		$bus_commission_query = 'select BBCD.* from b2b_bus_commission_details as BBCD 
								inner join user as U on BBCD.agent_fk = U.user_id
								where BBCD.domain_list_fk ='.get_domain_auth_id().' and BBCD.agent_fk='.intval($agent_fk).' and BBCD.type="specific"';
		$response['data']['agent_details']				= $this->db->query($agent_details_query)->row_array();
		$response['data']['flight_commission_details']	= $this->db->query($flight_commission_query)->row_array();;
		$response['data']['hotel_commission_details']	= '';
		$response['data']['bus_commission_details']		= $this->db->query($bus_commission_query)->row_array();
		if (valid_array($response['data']['agent_details']) == true) {
			$response['status'] = SUCCESS_STATUS;
		}
		return $response;
	}
	/**
	 * Jaganath
	 * B2B Agent Commission Details: Flight, Bus
	 */
	function agent_commission_details($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		if (!$count) {
			$agent_details_query = 'select U.*,BFCD.value as flight_commission_value,BFCD.api_value as flight_api_value,BFCD.value_type as flight_commission_type,
									BBCD.value as bus_commission_value,BBCD.api_value as bus_api_value,BBCD.value_type as bus_commission_type
									FROM user AS U 
									INNER JOIN user_type AS UT ON U.user_type=UT.origin
									left join b2b_flight_commission_details as BFCD on U.user_id=BFCD.agent_fk and BFCD.type="specific"
									left join b2b_bus_commission_details as BBCD on U.user_id=BBCD.agent_fk and BBCD.type="specific"
									WHERE U.user_type = '.B2B_USER.' and U.domain_list_fk ='.get_domain_auth_id().$condition.'
									ORDER BY U.user_id DESC limit '.$offset.', '.$limit;
			$response['data']['agent_commission_details']				= $this->db->query($agent_details_query)->result_array();
			return $response;
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U, user_type AS UT, api_country_list AS ACL
				 WHERE U.user_type=UT.origin 
				 AND U.country_code=ACL.origin and U.domain_list_fk ='.get_domain_auth_id().$condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}
	/**
	 * Jaganath
	 * B2B Agent Commission Details: Flight, Bus
	 * @param $search_filter_condition => (condition)
	 */
	function filter_agent_commission_details($search_filter_condition = '', $count=false, $offset=0, $limit=100000000000)
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		if(empty($search_filter_condition) == false) {
			$search_filter_condition = ' and'.$search_filter_condition;
		}
		if (!$count) {
			$agent_details_query = 'select U.*,BFCD.value as flight_commission_value,BFCD.api_value as flight_api_value,BFCD.value_type as flight_commission_type,
									BBCD.value as bus_commission_value,BBCD.api_value as bus_api_value,BBCD.value_type as bus_commission_type
									FROM user AS U 
									INNER JOIN user_type AS UT ON U.user_type=UT.origin
									left join b2b_flight_commission_details as BFCD on U.user_id=BFCD.agent_fk and BFCD.type="specific"
									left join b2b_bus_commission_details as BBCD on U.user_id=BBCD.agent_fk and BBCD.type="specific"
									WHERE U.user_type = '.B2B_USER.' and U.domain_list_fk ='.get_domain_auth_id().$search_filter_condition.'
									ORDER BY U.user_id DESC limit '.$offset.', '.$limit;
			$response['data']['agent_commission_details']				= $this->db->query($agent_details_query)->result_array();
			return $response;
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U, user_type AS UT, api_country_list AS ACL
				 WHERE U.user_type=UT.origin AND U.user_type = '.B2B_USER.' and U.domain_list_fk ='.get_domain_auth_id().' and  U.country_code=ACL.origin'.$search_filter_condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}
	/**
	 * Jaganath
	 * B2B Default Commission Details: Flight, Hotel, Bus
	 * @param $agent_fk
	 */
	function default_commission_details()
	{
		$response['status'] = SUCCESS_STATUS;
		$response['data'] = array();
		
		$flight_commission_query = 'select BFCD.* from b2b_flight_commission_details as BFCD
								where BFCD.domain_list_fk ='.get_domain_auth_id().' and BFCD.type="generic"';
		$bus_commission_query = 'select BBCD.* from b2b_bus_commission_details as BBCD
								where BBCD.domain_list_fk ='.get_domain_auth_id().' and BBCD.type="generic"';
		$response['data']['flight_commission_details']	= $this->db->query($flight_commission_query)->row_array();;
		$response['data']['hotel_commission_details']	= '';
		$response['data']['bus_commission_details']		= $this->db->query($bus_commission_query)->row_array();
		return $response;
	}
	/**
	 * Jaganath 
	 */
	function auto_suggest_agency_name($chars, $limit=15)
	{
		$query = 'select U.*
					FROM user AS U 
					INNER JOIN user_type AS UT ON U.user_type=UT.origin 
					WHERE U.agency_name!="" and U.domain_list_fk ='.get_domain_auth_id().' and 
					(U.uuid like "%'.$chars.'%" OR U.agency_name like "%'.$chars.'%" OR U.first_name like "%'.$chars.'%" OR U.last_name like "%'.$chars.'%" OR U.email like "%'.$chars.'%" OR U.phone like "%'.$chars.'%" )
					order by U.agency_name asc limit 0, '.$limit;
		return $this->db->query($query)->result_array();
	}
	/**
	 * Jaganath
	 * Bank Account Details
	 */
	function bank_account_details() 
	{
		$query='SELECT BAD.*,CONCAT(U.first_name," ",U.last_name) as created_by_name 
		        FROM bank_account_details BAD
		        JOIN user U on U.user_id=BAD.created_by_id
		        where BAD.domain_list_fk='.get_domain_auth_id();
		$tmp_data = $this->db->query($query);
		if($tmp_data->num_rows()>0) {
			$tmp_data=$tmp_data->result_array();
			$data = array('status' => QUERY_SUCCESS, 'data' => $tmp_data);
		} else {
			$data = array('status' => QUERY_FAILURE);
		 }
		 return $data;
	}
	/**
	 * Update Balance of Agent
	 * @param number $amount Amount to be added or deducted
	 */
	function update_agent_balance($amount, $agent_user_id)
	{
		$current_balance = 0;
		$cond = array('user_oid' => intval($agent_user_id));
		$details = $this->custom_db->single_table_records('b2b_user_details', 'balance', $cond);
		if ($details['status'] == true) {
			$details['data'][0]['balance'] = $current_balance = ($details['data'][0]['balance'] + $amount);
			$this->custom_db->update_record('b2b_user_details', $details['data'][0], $cond);
			$this->balance_notification($current_balance);
		}
		return $current_balance;
	}
	/**
	 * Jaganath
	 * if less than limit then send notification
	 */
	function balance_notification($current_balance)
	{
		$condition = array('agent_fk' => intval($this->entity_user_id));
		$details = $this->custom_db->single_table_records('agent_balance_alert_details', '*', $condition);
		if ($details['status'] == true) {
			$threshold_amount = $details['data'][0]['threshold_amount'];
			$mobile_number = trim($details['data'][0]['mobile_number']);
			$email_id = trim($details['data'][0]['email_id']);
			$enable_sms_notification = $details['data'][0]['enable_sms_notification'];
			$enable_email_notification = $details['data'][0]['enable_email_notification'];
			if($current_balance <= $threshold_amount) {
				//FIXME:Send Notification
				//SMS ALERT
				if($enable_sms_notification == ACTIVE && empty($mobile_number) == false) {
					//Send SMS Alert for Low Balance
				}
				//EMAIL NOTIFICATION
				if($enable_email_notification == ACTIVE && empty($email_id) == false) {
					//Send Email Notification for Low Balance
					$subject = $this->agency_name.'- Low Balance Alert';
					$message = 'Dear '.$this->entity_name.'<br/> <h1>Your Agent Balance is Low.</h1><br/><h2>Agent Balance as on '.date("Y-m-d h:i:sa").'is : '.COURSE_LIST_DEFAULT_CURRENCY_VALUE.' '.$threshold_amount.'/-</h2><h3>Please Recharge Your Account to enjoy UnInterrupted Bookings. :)</h3>';
					$this->load->library('provab_mailer');
					$mail_status = $this->provab_mailer->send_mail($email_id, $subject, $message);
				}
			}
		}
	}
	/**
	 * Save transaction logging for security purpose
	 * @param string $transaction_type
	 * @param string $app_reference
	 * @param number $fare
	 * @param number $domain_markup
	 * @param number $level_one_markup
	 * @param string $remarks
	 */
	function save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $remarks, $convinence=0, $discount=0, $currency='INR', $currency_conversion_rate=1)
	{
		$transaction_log['system_transaction_id']	= date('Ymd-His').'-S-'.rand(1, 10000);
		$transaction_log['transaction_type']		= $transaction_type;
		$transaction_log['domain_origin']			= get_domain_auth_id();
		$transaction_log['app_reference']			= $app_reference;
		$transaction_log['fare']					= $fare;
		$transaction_log['level_one_markup']		= $level_one_markup;
		$transaction_log['domain_markup']			= $domain_markup;
		$transaction_log['remarks']					= $remarks;
		$transaction_log['created_by_id']			= intval(@$this->entity_user_id) ;
		$transaction_log['created_datetime']		= date('Y-m-d H:i:s', time());
		
		$transaction_log['convinence_fees']			= $convinence;
		$transaction_log['promocode_discount']		= $discount;
		$transaction_log['currency']				= $currency;
		$transaction_log['currency_conversion_rate']= $currency_conversion_rate;
		$this->custom_db->insert_record('transaction_log', $transaction_log);
	}
}
