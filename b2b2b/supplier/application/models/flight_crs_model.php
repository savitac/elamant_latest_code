<?php
/**
 * Library which has generic functions to get data
 *
 * @package    Provab Application
 * @subpackage Flight CRS  Model
 * @author     Priyanka
 * @version    V1
 */
Class Flight_Crs_Model extends CI_Model
{

    function supplier_crs_list($uid)
    {
         $query = "SELECT S.orgin,S.suppier_id,S.ariline_id,CA.crs_name,CA.crs_code,CA.crs_path,CONCAT(U.first_name,'',U.last_name) as supplier_name 
            from crs_ariline_supplier S
            JOIN crs_airline_list CA ON CA.crs_code=S.ariline_id
            JOIN user U ON U.user_id=S.suppier_id
            WHERE S.suppier_id='$uid'
         ";
        return $this->db->query($query)->result_array();
    }

    function ariline_list()
    {
        $query = "SELECT * from crs_airline_list";
        return $this->db->query($query)->result_array();
    }

    function flight_crs_request_details($uid,$today_date)
    {
        
        if(!empty($today_date)){ 
            $today_date=date('d-m-Y');  
            $show_today='AND CRS.jourany_date='."'$today_date'".'AND CRS.till_date>='."'$today_date'";
            
        }else{
            $show_today="";
        }
           $query = "SELECT CRS.orgin,CRS.flight_trip,CRS.ariline_code,CRS.from_city,CRS.from_loc_id,CRS.to_city,CRS.no_of_seats,CRS.left_seats,CRS.booked_seats,CRS.hold_seats,
        CRS.to_loc_id,CRS.jourany_date,CRS.retuen_date,CRS.no_of_seats,CRS.class,CRS.status,CRS.created_by,CRS.supplier_id,
        AL.crs_name as airline_name,AL.crs_code as code,AL.crs_path
        FROM flight_crs_list CRS
        JOIN crs_airline_list AL ON CRS.ariline_code=AL.crs_code 
        where CRS.supplier_id='$uid' $show_today ORDER BY CRS.jourany_date DESC ";
        //exit("query exit");
        return $this->db->query($query)->result_array();
    }

    function flight_crs_request_booking_details($uid,$today_date)
    {
        //added by ajaz.
        // make change condition
        
        $query = "SELECT CRS.orgin,CRS.flight_trip,CRS.ariline_code,CRS.from_city,CRS.from_loc_id,CRS.to_city,CRS.no_of_seats,CRS.left_seats,CRS.booked_seats,CRS.hold_seats,
        CRS.to_loc_id,CRS.jourany_date,CRS.retuen_date,CRS.no_of_seats,CRS.class,CRS.status,CRS.created_by,CRS.supplier_id,
        AL.crs_name as airline_name,AL.crs_code as code,AL.crs_path,FS.onwards_flight_number,FS.return_flight_number
        FROM flight_crs_list CRS
        JOIN crs_airline_list AL ON CRS.ariline_code=AL.crs_code 
        JOIN flight_segement_list FS ON CRS.orgin=FS.flight_crs_no
        where CRS.supplier_id='$uid' ORDER BY CRS.orgin,CRS.jourany_date DESC ";
        return $this->db->query($query)->result_array();
    }


        

    function flight_crs_edit_request_details($uid,$id)
    {
        $query = "SELECT CRS.orgin,CRS.flight_trip,CRS.ariline_code,CRS.from_city,CRS.from_loc_id,CRS.to_city,CRS.no_of_seats,CRS.left_seats,CRS.booked_seats,
        CRS.to_loc_id,CRS.jourany_date,CRS.retuen_date,CRS.no_of_seats,CRS.class,CRS.status,CRS.created_by,CRS.supplier_id,
        AL.crs_name as airline_name,AL.crs_code as code,AL.crs_path
        FROM flight_crs_list CRS
        JOIN crs_airline_list AL ON CRS.ariline_code=AL.crs_code 
        where CRS.supplier_id='$uid' and CRS.orgin='$id'";
        return $this->db->query($query)->result_array();
    }

    function flight_crs_edit_details($id)
    {

        $query = "select * from flight_crs_list where orgin='$id'";
        return $this->db->query($query)->row_array();
    }

    function flight_segment_request_details($id,$trip_type)
    {
        $query = "select * from flight_segement_list where flight_crs_no='$id'";
        return $this->db->query($query)->result_array();
    }

    function flight_price_request_details($id)
    {
        $query = "select * from flight_crs_price where flight_crs_no='$id'";
        return $this->db->query($query)->result_array();
    }

        function flight_segemnet_edit_details($id)
    {
        $query = "select * from flight_segement_list where orgin='$id'";
        return $this->db->query($query)->row_array();
    }

    function flight_price_edit_details($id)
    {
        $status=$this->get_status($id);
        $query = "select * from flight_crs_price where orgin='$id'";
        $response=$this->db->query($query)->row_array();
        $response['status']=$status;
        return $response;
    }

    function get_status($id){
        $query="select status from flight_crs_list where orgin='$id'";
        return $this->db->query($query)->row_array();
    }

    function flight_crs_view_details($uid,$id,$trip_type)
    {
        $query = " SELECT 
FCR.orgin,FCR.flight_trip,FCR.from_city,FCR.to_city,FCR.jourany_date,FCR.retuen_date,FCR.no_of_seats,FCR.class,FCR.status,
FS.flight_crs_no,FS.onwards_flight_name,FS.onwards_flight_number,FS.onwards_flight_equipment,FS.onwards_from_city,FS.onwards_to_city,
FS.onwards_departure_time,FS.onwards_arrival_at,FS.onwards_arrival_time,FS.retuen_flight_name,FS.return_flight_number,
FS.return_flight_equipment,FS.return_from_city,FS.return_to_city,FS.return_departure_time,FS.return_arrival_at,FS.return_arrival_time,FS.onwards_duration,FS.return_duration,
FP.adult_price,FP.adult_tax,FP.child_price,FP.child_tax,FP.infant_price,FP.infant_tax,FP.fare_rules,FP.adult_markup,FP.child_markup,FP.infant_markup,
AL.crs_name as airline_name,AL.crs_code as onward_code,AL.crs_path as onward_path,
ALA.crs_name as airline_namee,ALA.crs_code as retuen_code,ALA.crs_path as return_path

FROM flight_crs_list FCR
JOIN flight_segement_list FS ON FCR.orgin=FS.flight_crs_no
JOIN flight_crs_price FP ON FCR.orgin=FP.flight_crs_no

JOIN crs_airline_list AL ON FS.onwards_flight_name=AL.crs_code
JOIN crs_airline_list ALA ON FS.onwards_flight_name=ALA.crs_code
 where FCR.supplier_id='$uid'  and FCR.orgin='$id' ";
        return $this->db->query($query)->result_array();
    }

    
    
}
