<?php
/**
 *
 * @author Arjun<arjunjgowda260389@gmail.com>
 *
 */
class application {
	var $CI; // code igniter object
	var $userId; // user id to identify user
	var $page_configuration;
	var $skip_validation;

	/**
	 * constructor to initialize data
	 */
	function __construct() {
		$this->CI = &get_instance ();
		$this->CI->load->library ( 'provab_page_loader.php' );
		$this->CI->load->helper ( 'url' );
		if (! isset ( $this->CI->session )) {
			$this->CI->load->library ( 'session' );
		}
		$this->footer_needle = $this->header_needle = $this->CI->uri->segment ( 2 );
		$this->skip_validation = false;
		$this->CI->language_preference = 'english';
		$this->CI->lang->load ( 'form', $this->CI->language_preference );
		$this->CI->lang->load ( 'utility', $this->CI->language_preference );
		$this->CI->lang->load ( 'application', $this->CI->language_preference );
		// $this->CI->session->set_userdata(array(AUTH_USER_POINTER => 10, LOGIN_POINTER => intval(100)));

		// print_r($this->CI->session->all_userdata()); exit('sdfsd');
	}

	/**
	 * We need to initialize all the domain key details here
	 * Written only for provab
	 */
	function initialize_domain_key() {
		$this->CI->application_default_template = 'template_v1';
		// Jaganath
		$domain_auth_id = $GLOBALS ['CI']->session->userdata ( DOMAIN_AUTH_ID );
		$domain_key = $GLOBALS ['CI']->session->userdata ( DOMAIN_KEY );
		$domain_details = $GLOBALS ['CI']->custom_db->single_table_records ( 'domain_list', '*', array (
				'domain_key' => CURRENT_DOMAIN_KEY 
		) );
		if (valid_array ( $domain_details ) == true and count ( $domain_details ) >= 1) {
			$domain_details = $domain_details ['data'] [0];
			$this->CI->entity_domain_name = $domain_details ['domain_name'];
			$this->CI->application_domain_logo = $domain_details['domain_logo'];
			if (intval ( $domain_auth_id ) == 0 && empty ( $domain_key ) == true and strlen ( trim ( $domain_details ['domain_name'] ) ) > 0) {
				// IF DOMAIN KEY IS NOT SET, THEN SET THE DOMANIN DETAILS
				$domain_session_data = array ();
				// SETTING DOMAIN KEY
				$domain_session_data [DOMAIN_AUTH_ID] = intval ( $domain_details ['origin'] );
				// SETTING DOMAIN CONFIGURATION
				$domain_session_data [DOMAIN_KEY] = base64_encode ( trim ( $domain_details ['domain_key'] ) );
				$this->CI->session->set_userdata ( $domain_session_data );
			}
		}

		if (empty($this->CI->entity_domain_name) == false) {
			define('HEADER_TITLE_SUFFIX', ' - '.$this->CI->entity_domain_name); // Common Suffix For All Pages
		} else {
			define('HEADER_TITLE_SUFFIX', ' - Travels'); // Common Suffix For All Pages
		}
	}

	/**
	 * Set all the active modules for doamin
	 */
	function initialize_domain_modules() {
		// set domain active modules based on auth key
		$domain_key = base64_decode ( $this->CI->session->userdata ( DOMAIN_KEY ) );
		$domain_auth_id = $this->CI->session->userdata ( DOMAIN_AUTH_ID );
		// set global modules data
		$active_domain_modules = $this->CI->module_model->get_active_module_list ( $domain_auth_id, $domain_key );
		$this->CI->active_domain_modules = $active_domain_modules;
	}

	/**
	 * Following pages will not have any validations
	 */
	function bouncer_page_validation() {
		$skip_validation_list = array (
				'forgot_password' 
				); // SKIP LIST
				if (in_array ( $this->header_needle, $skip_validation_list )) {
					$this->skip_validation = true;
				}
	}

	/**
	 * Handle hook for multiple page login system
	 */
	function initilize_multiple_login33() {			
		$this->bouncer_page_validation ();
		if ($this->skip_validation == false) {
			$email_id = isset ( $_POST ['email'] ) ? $_POST ['email'] : '';
			$password = isset ( $_POST ['password'] ) ? $_POST ['password'] : '';
			$auth_login_id = $this->CI->session->userdata ( AUTH_USER_POINTER );
			if (empty ( $email_id ) == false and empty ( $password ) == false) {
				$condition ['email'] = $email_id;
				$condition ['password'] = md5 ( $password );
				$condition ['status'] = ACTIVE;
			} elseif (intval ( $auth_login_id ) > 0) {
				$condition ['uuid'] = $auth_login_id;
			}
			if (isset ( $condition ) == true and is_array ( $condition ) == true and count ( $condition ) > 0) {
				$condition ['status'] = ACTIVE;
				$user_details = $this->CI->db->get_where ( 'user', $condition )->row_array ();
				if (valid_array ( $user_details ) == true) {
					$this->CI->entity_uuid = $user_details ['uuid'];
					$this->CI->entity_user_type = $user_details ['user_type'];
					if (empty ( $email_id ) == false and empty ( $password ) == false) {
						$this->CI->session->set_userdata ( array (
						AUTH_USER_POINTER => $user_details ['uuid'],
						LOGIN_POINTER => intval ( $this->update_login_manager () )
						) );
					}
				}
			}
		}
	}

	/**
	 * Handle hook for dedicated page login system
	 */
	function initilize_dedicated_login() {

		$this->bouncer_page_validation ();
		if ($this->skip_validation == false) {
			$email = isset ( $_POST ['email'] ) ? $_POST ['email'] : '';
			$password = isset ( $_POST ['password'] ) ? $_POST ['password'] : '';

			//debug($this->CI->session->all_userdata ()); exit;
			// check session when the user is not in the login page
			$user_id = $this->CI->session->userdata ( AUTH_USER_POINTER );
			// segments
			$segment1 = $this->CI->uri->segment ( 1 );
			$segment2 = $this->CI->uri->segment ( 2 );

			if (isset($user_id) && !empty($user_id)) {
				$user_details = $this->CI->db->get_where ( 'user', array (
						'user_id' => $user_id,
						'status' => ACTIVE 
				) )->row_array ();

				if(empty($user_details) || !is_array($user_details) ){
					$this->CI->session->unset_userdata ( array (
						AUTH_USER_POINTER => '',
						LOGIN_POINTER => ''
					));
				}
				/*exit("...1");*/
			}

			else if ((empty($segment1) || ($segment1 == 'general' && $segment2 == 'index')) && !empty($email) && !empty($password)) {
			
				// USER Logging in with credential
				/*print_r($user_id );
				exit("/////");
				*/$this->CI->form_validation->set_rules ( 'email', 'Email', 'trim|required|valid_email|min_length[4]|max_length[45]|xss_clean' );
				$this->CI->form_validation->set_rules ( 'password', 'Password', 'required|min_length[5]|max_length[45]|xss_clean' );
				if (true) {

					$condition ['password'] = md5 ( $this->CI->db->escape_str ( $password ) );
					$condition ['user_name'] = $email;
					$condition ['status'] = ACTIVE;
					$domain_auth_id = get_domain_auth_id ();
					$domain_key = get_domain_key ();
					if (intval ( $domain_auth_id ) > 0 && empty ( $domain_key ) == false) { // IF DOMAIN KEY EXISTS
						// $condition['domain_list_fk'] = intval(get_domain_auth_id());
						$this->CI->db->where_in ( 'domain_list_fk', array (
						intval ( get_domain_auth_id () ),
						0
						) );
					}
					/**
					 * USER TYPES *
					 */
					$user_types = array (SUPPLIER_USER);
					//echo $this->CI->db->last_query();
					/*print_r($user_types);
					exit("<br>...");*/	
					// Merge condition with super admin also
					$user_details = $this->CI->db->where_in ( 'user_type', $user_types )->get_where ( 'user', $condition );
					
					// echo $this->CI->db->last_query(); 
					if ($user_details->num_rows() == 1) {
						$user_details = $user_details->row_array ();
					}
					
				}
			} else {
					$this->CI->session->unset_userdata ( array (
					AUTH_USER_POINTER => '',
					LOGIN_POINTER => ''
					) );
					if (($this->CI->uri->segment ( 1 ) != 'general' || $this->CI->uri->segment ( 2 ) != 'index')) {
						redirect ( 'general/index' );
					}
					}
			// set the details when the user details is present
			if (isset ($user_details) and valid_array($user_details) and count ( $user_details ) > 0) {
				$this->CI->entity_user_id = $user_details ['user_id'];
				$this->CI->entity_domain_id = $user_details ['domain_list_fk'];
				$this->CI->entity_uuid = $user_details ['uuid'];
				$this->CI->entity_user_type = $user_details ['user_type'];
				$this->CI->entity_email = $user_details ['email'];
				$this->CI->entity_name = get_enum_list ( 'title', $user_details ['title'] ) . ' ' . ucfirst ( $user_details ['first_name'] ) . ' ' . ucfirst ( $user_details ['last_name'] );
				$this->CI->entity_address = $user_details ['address'];
				$this->CI->entity_phone = $user_details ['phone'];
				$this->CI->entity_country_code = $user_details ['country_code'];
				$this->CI->entity_status = $user_details ['status'];
				$this->CI->entity_date_of_birth = $user_details ['date_of_birth'];
				$this->CI->entity_image = $user_details ['image'];
				$this->CI->entity_creation = $user_details ['created_datetime'];
				if (!empty($email) && !empty($password)) {
					// SETTING SESSION DATA
					$user_session_data = array ();
					$user_session_data [AUTH_USER_POINTER] = $user_details ['user_id'];
					//$user_session_data [LOGIN_POINTER] = intval ( $this->update_login_manager () );
					$user_session_data [LOGIN_POINTER] = time(); 
					$this->CI->session->set_userdata ( $user_session_data );

					redirect('/menu/index');
					// $this->CI->session->set_userdata(array(AUTH_USER_POINTER => $user_details['user_id'], LOGIN_POINTER => intval($this->update_login_manager())));
				}
			}
		}
	}

	/**
	 * function to update login time and logout time details of user when user
	 * login or logout.
	 */
	function update_login_manager() {
		return $this->CI->user_model->create_login_auth_record ( $this->CI->entity_uuid, $this->CI->entity_user_type, $this->CI->entity_user_id, $this->CI->entity_name );
	}

	/*
	 * load current page configuration
	 */
	function load_current_page_configuration() {
		//$this->set_page_configuration ();
		$this->page_configuration ['current_page'] = $this->CI->current_page = new Provab_Page_Loader ();
	}

	/**
	 * This file specifies which systems should be loaded by default for each page.
	 *
	 * @param unknown_type $controller
	 * @param unknown_type $method
	 */
	function set_page_configuration() {
		/*$controller = $this->CI->uri->segment ( 1 );
		 $method = $this->CI->uri->segment ( 2 );
		 $temp_configuration ['general'] ['index'] = array (
		 'header_title' => 'AL001',
		 'menu' => false,
		 'page_keywords' => array (
		 'meta' => '',
		 'author' => ''
		 ),
		 'page_small_icon' => ''
		 );
		 $this->page_configuration = $temp_configuration ['general'] ['index'];*/
	}
}
