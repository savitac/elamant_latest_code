  <div id="Bus" class="tab-pane"> 
    <form autocomplete="on" name="bus" id="bus_form" action="<?php echo base_url();?>index.php/bus/pre_bus_search" method="get" class="activeForm oneway_frm" style="">
	<div class="tabspl forbusonly">
    	<div class="tabrow bus_search">
			<div class="col-md-8 col-sm-6 col-xs-12 nopad">
				<div class="col-xs-6 padfive full_smal_tab">
					<div class="lablform">From</div>
					<div class="plcetogo locatiomarker ">
                    <span class="maskimg  hfrom"></span>
						<input type="text" required=""  value="" placeholder="Type Departure City" id="bus-station-from" class="ft normalinput bus-station auto-focus form-control b-r-0 valid_class bus-station-from" name="bus_station_from" autocomplete="on">
						<input class="hide loc_id_holder" name="from_station_id" type="hidden" value="<?=@$bus_search_params['from_station_id']?>" >
					</div>
				</div>
				<div class="col-xs-6 padfive full_smal_tab">
					<div class="lablform">To</div>
					<div class="plcetogo locatiomarker ">
                    <span class="maskimg  hfrom"></span>
						<input type="text" required="" value="" placeholder="Type Destination City" id="bus-station-to" class="ft normalinput bus-station bus-station auto-focus form-control b-r-0 valid_class bus-station-to ui-autocomplete-input" name="bus_station_to" autocomplete="on">
						<input class="hide loc_id_holder" name="to_station_id" type="hidden" value="<?=@$bus_search_params['to_station_id']?>" >
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="alert-box" id="bus-alert-box">
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 nopad">
				<div class="col-xs-6 padfive">
					<div class="lablform">Date of Journey</div>
					<div class="plcetogo datemark ">
                    <span class="maskimg   caln"></span>
						<input type="text" class="forminput auto-focus hand-cursor form-control b-r-0" id="bus-date-1" placeholder="dd-mm-yy" value="05-07-2017" name="bus_date_1" required>
					</div>
				</div>
				<div class="col-xs-6 padfive">
					<div class="lablform">&nbsp;</div>
					<div class="searchsbmtfot">
						<input type="submit" id="bus-form-submit" class="searchsbmt" value="search" />
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script>
$("#bus-date-1").datepicker({
    numberOfMonths: 1,
    minDate: 0,
    dateFormat: 'dd-mm-yy'
});
</script>
    <script type="text/javascript">
	$(document).ready(function() {
    function show_alert_content(content, container) {
        if (container == '') {
            container = '.alert-danger'
        }
        $(container).text(content);
        if (content.length > 0) {
            $(container).removeClass('hide')
        } else {
            $(container).addClass('hide')
        }
    }
    
    var cache = {};
    var from_station = $('#bus-station-from').val();
    var to_station = $('#bus-station-to').val();
    $(".bus-station").catcomplete({
        source: function(request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[term]);
                return
            }
            $.getJSON(app_base_url + "index.php/ajax/bus_stations", request, function(data, status, xhr) {
                cache[term] = data;
                response(data)
            })
        },
        minLength: 0,
        autoFocus: true,
        select: function(event, ui) {
            var label = ui.item.label;
            var category = ui.item.category;
            if (this.id == 'bus-station-from') {
                from_station = ui.item.value
            } else if (this.id == 'bus-station-to') {
                to_station = ui.item.value
            }
            $(this).siblings('.loc_id_holder').val(ui.item.id);
            auto_focus_input(this.id)
        },
        change: function(ev, ui) {
            if (!ui.item) {
                $(this).val("")
            }
        }
    }).bind('focus', function() {
        $(this).catcomplete("search")
    }).catcomplete("instance")._renderItem = function(ul, item) {
        var auto_suggest_value = (this.term.trim(), item.value, item.label);
        return $("<li class='custom-auto-complete'>").append('<a>' + auto_suggest_value + '</a>').appendTo(ul)
    };
    $("#bus-station-to").catcomplete("instance")._renderItem = function(ul, item) {
        var auto_suggest_value = highlight_search_text(this.term.trim(), item.value, item.label);
        return $("<li class='custom-auto-complete'>").append('<a>' + auto_suggest_value + '</a>').appendTo(ul)
    };
    $('#bus-station-from, #bus-station-to').change(function() {
      auto_focus_input(this.id)
    });
    $('#bus-form-submit').click(function(e) {
        if ($('#bus-station-from').val() == $('#bus-station-to').val()) {
            e.preventDefault();
            show_alert_content('From location and To location can not be same.', '#bus-alert-box');
            return ''
        }
    })
   
});
 
var _auto_focus = $('.auto-focus').map(function() {
    return this.id;
}).get();
function auto_focus_input(cur_ele)
{
    //check if current element has auto-focus enabled
    if ($('#'+cur_ele).hasClass('auto-focus')) {
        var _tmp_index = _focus_index = _auto_focus.indexOf(cur_ele);
        while(_focus_index = _auto_focus[++_tmp_index]) {
            if ($('#'+_focus_index).is(':visible:not([disabled])')) {
                $('#'+_focus_index).focus();
                break;
            }
        }
    }
}

function highlight_search_text(searched_text, value, label)
{
    var auto_suggest_value = '';
    if(searched_text !='' ) {
        auto_suggest_value = String(value).replace(new RegExp(searched_text, "gi"),"<strong>$&</strong>");
    } else {
        auto_suggest_value = label;
    }
    return auto_suggest_value;
}

function set_bus_cookie_data() {
    var s_params = $('#bus_form').serialize().trim();
    setCookie('bus_search', s_params, 100)
}
</script>
<?php

?>