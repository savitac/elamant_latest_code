<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
error_reporting(-1);
ob_start();
class Account extends CI_Controller {	
	function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Validation_Model');
		$this->load->model('Email_Model');
		die(" 1231888888888");  
		$this->load->model('Account_Model');
		$this->load->library('provab_mailer'); 
		$this->TravelLights = $this->lang->line('TravelLights');
	}


	function createAccount(){ 	

		if(count($_POST) > 0) 
		{
			if(isset($_POST['agency_name']) && $_POST['agency_name'] == '')
			{
				$data['error']['agency_name-error'] = "Please enter valid agency name";
			}

			if(isset($_POST['agncy_fname']) && $_POST['agncy_fname'] == '')
			{
				$data['error']['agency_name-error'] = "Please enter valid Name";
			}

			if(isset($_POST['mobile_num']) && $_POST['mobile_num'] != '')
			{
				$contact_validation = $this->Validation_Model->numberWithContact($_POST['mobile_num']);	

				if(!$contact_validation)
				{
					$data['error']['mobile_num-error'] = "Please enter valid  Number";
				}
			}

			if(isset($_POST['email'])  && $_POST['email'] != '')
			{
				$email_validation = $this->Validation_Model->emailIdFormat($_POST['email']);	
				if(!$email_validation)
				{
					$data['error']['email-error'] = "Please enter valid Email";
				}
			}

			if(isset($_POST['password']) && $_POST['password'] != '')
			{
				$password_validation = $this->Validation_Model->passwordValidation($_POST['password']);	
				if(!$password_validation)
				{
					$data['error']['password-error'] = "Password should contain atleast on Capital letter, one Numbers and one Special Characters";
				}
			}

			if(isset($_POST['cpassword']) && $_POST['cpassword'] != '')
			{
				$password_validation = $this->Validation_Model->checkPasswords($_POST['password'], $_POST['cpassword']);	
				if(!$password_validation)
				{
					$data['error']['password-error'] = "Password not valid";
				}
			}

			if(isset($_POST['mobile']) && $_POST['mobile'] != '')
			{
				$contact_validation = $this->Validation_Model->numberWithContact($_POST['mobile']);	
				if(!$contact_validation)
				{
					$data['error']['mobile-error'] = "Please enter valid Mobile Number";
				}
			}

			if(!isset($_POST['terms']) || $_POST['terms'] == '')
			{
				$data['error']['terms-error'] = "Please Check terms And Conditions";
			}

			if(isset($data['error']) && count($data['error']) > 0) 
			{
				$data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			}

			$address = $this->Account_Model->default_address()->row();
			$acc_no = 'TRVL-'.rand(1,100);
			$postData = array(
				'company_name'=>$_POST['agency_name'],
				'user_name' => $_POST['agncy_fname'],
				'user_email' => $_POST['email'],
				'user_account_number'=>$acc_no,
				'user_cell_phone' => $_POST['mobile_num'],
				'termsnconditions' =>$_POST['terms'], 
				'address_details_id' => $address->address_details_id,
				'user_profile_pic' =>  'user-avatar.jpg',
				'user_status' => 'INACTIVE',
				'user_creation_date_time' => date('Y-m-d H:i:s')
				);
			
			if($this->session->userdata('branch_id') != '') 
			{
				$postData['branch_id'] =  $this->session->userdata('branch_id');
				$postData['domain_list_fk'] =  $this->session->userdata('domain_id');
				$postData['user_type_id']= 4;
			}
			else
			{
				$postData['user_type_id'] = 2;
			}

			$count = $this->Account_Model->isRegisteredB2B($_POST['email'])->num_rows();
			if($count == 0)
			{
				$this->Account_Model->createUsers($postData);
				$user_data = $this->Account_Model->isRegisteredB2B($_POST['email'])->row();

				$login_data = array(
					'user_details_id' => $user_data->user_details_id,
					'user_name' => $_POST['email'],
					'user_type_id' => $user_data->user_type_id,
					'user_password' => "AES_ENCRYPT(".$_POST['password'].",'".SECURITY_KEY."')"
					);

				$this->session->set_flashdata("regi_success","Agent Registration Success ");

				if($this->Account_Model->createLogin($login_data))
				{
					$b2c_session =  array(
						'user_id' => $user_data->user_details_id,
						);

					$data['user'] 		= "B2B User";
					$data['user_email'] = $_POST['email'];
					$data['name']   	= $_POST['agncy_fname'];
				//$data['user_name'] 	= $_POST['email'];
					$data['user_acc_no'] 	= $user_data->user_account_number;
					$data['url'] 			= base_url();
					$data['profile_photo']= $user_data->user_profile_pic;

					$data['email_template'] = $this->Email_Model->get_email_template('Registration')->row();
					$this->Email_Model->sendmail_reg($data);

					$response = array(
						'status' => '1',
						'success' => 'true',
						'msg' => 'Successfully Created!',
						'rid' => $user_data->user_details_id,
						'fname' =>  $user_data->user_name,
						'profile_photo' =>  $user_data->user_profile_pic,
						'success_url' => base_url()
						);
				}
				redirect(base_url());	
			}
			else
			{
				$response = array(
					'status' => '0',
					'success' => 'false',
					'email-error' => 'That email address is already in use. Please log in or use Another Email.'
					);
			}

			echo json_encode($response); 
		}
		else
		{		
			$country['nationality_countries'] = $this->General_Model->getNationalityCountries()->result_array();
			$this->load->view('b2b/b2b_account',$country);
		}

	}
	

	function create_b2bc_Account(){ 
		if(count($_POST) > 0) {

			if(isset($_POST['b2bc_user_name']) && $_POST['b2bc_user_name'] == ''){
				$data['error']['b2bc_user_name-error'] = "Please enter user  Name";
			}


			if(isset($_POST['mobile_num']) && $_POST['mobile_num'] != ''){
				$contact_validation = $this->Validation_Model->numberWithContact($_POST['mobile_num']);	
				if(!$contact_validation){
					$data['error']['mobile_num-error'] = "Please enter valid  Number";
				}
			}
			
			if(isset($_POST['email'])  && $_POST['email'] != ''){
				$email_validation = $this->Validation_Model->emailIdFormat($_POST['email']);	
				if(!$email_validation){
					$data['error']['email-error'] = "Please enter valid Email";
				}
			}
			if(isset($_POST['password']) && $_POST['password'] != ''){
				$password_validation = $this->Validation_Model->passwordValidation($_POST['password']);	
				if(!$password_validation){
					$data['error']['password-error'] = "Password should contain atleast on Capital letter, one Numbers and one Special Characters";
				}
			}
			
			if(isset($_POST['cpassword']) && $_POST['cpassword'] != ''){
				$password_validation = $this->Validation_Model->checkPasswords($_POST['password'], $_POST['cpassword']);	
				if(!$password_validation){
					$data['error']['password-error'] = "Password not valid";
				}
			}

			
			if(isset($_POST['mobile']) && $_POST['mobile'] != ''){
				$contact_validation = $this->Validation_Model->numberWithContact($_POST['mobile']);	
				if(!$contact_validation){
					$data['error']['mobile-error'] = "Please enter valid Mobile Number";
				}
			}
			
			if(!isset($_POST['terms']) || $_POST['terms'] == ''){
				$data['error']['terms-error'] = "Please Check terms And Conditions";
			}
			
			if(isset($data['error']) && count($data['error']) > 0) {
				$data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			}
			
			$address = $this->Account_Model->default_address()->row();
			$branch_id= $this->session->userdata('branch_id'); 
         	/*$addressing = array( 
				'country_id'        => $_POST['country'],
				'address' => $_POST['address'],
				'zip_code' => $_POST['postcode'],
				'city_name' => $_POST['cityname'],
				); 
			$address = $this->Account_Model->userAddress($addressing);
			
			$offers = json_encode($_POST['offers']);*/
			
			$acc_no = 'TRVL-'.rand(1,100);
			$postData = array(
			//'company_name'=>$_POST['b2bc_user_name'],
				'user_name' => $_POST['b2bc_user_name'],
				'user_email' => $_POST['email'],
				'user_account_number'=>$acc_no,
				'user_cell_phone' => $_POST['mobile_num'],
				'termsnconditions' =>$_POST['terms'], 
				'user_type_id' => 5,
				'address_details_id' => $address->address_details_id,
				'user_profile_pic' =>  'user-avatar.jpg',
				'user_status' => 'ACTIVE', 
			//'country_id'=$_POST['country'],
				'user_creation_date_time' => date('Y-m-d H:i:s')
				);
			
			if($this->session->userdata('branch_id') != '') {
				$postData['branch_id'] =  $this->session->userdata('branch_id');
				$postData['domain_list_fk'] =  $this->session->userdata('domain_id');
			}
			$count = $this->Account_Model->isRegisteredB2B($_POST['email'])->num_rows();
			if($count == 0){
				$this->Account_Model->createUsers($postData);
				$user_data = $this->Account_Model->isRegisteredB2B($_POST['email'])->row();
				$login_data = array(
					'user_details_id' => $user_data->user_details_id,
					'user_name' => $_POST['email'],
					'user_type_id' => 5,
					'user_password' => "AES_ENCRYPT(".$_POST['password'].",'".SECURITY_KEY."')"
					);

				if($this->Account_Model->createLogin($login_data)){
					$b2c_session =  array(
						'user_id' => $user_data->user_details_id,
						'user_type' =>5,
						);

					$data['user'] 		= "B2BC User";
					$data['user_email'] = $_POST['email'];
					$data['name'] 	= $_POST['b2bc_user_name'];
					$data['user_acc_no'] 	= $user_data->user_account_number;
					$data['url'] 			= base_url();
					$data['profile_photo']= $user_data->user_profile_pic;

					$data['email_template'] = $this->Email_Model->get_email_template('Registration')->row();   
					$this->Email_Model->sendmail_reg($data);
 
					$response = array(
						'status' => '1',
						'success' => 'true',
						'msg' => 'Successfully Created!',
						'rid' => $user_data->user_details_id,
						'fname' =>  $user_data->user_name,
						'profile_photo' =>  $user_data->user_profile_pic,
						'success_url' => base_url().'account/sucss_msg',
						);
				}	
			}else{

				$response = array(
					'status' => '0',
					'success' => 'false',
					'email-error' => 'That email address is already in use. Please log in or use Another Email.'
					);
			}
			echo json_encode($response); 
		}
	}
	
	public function register(){
		$country['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
		$this->load->view('b2b/register',$country);
	}
	
	
	public function login(){

		$email = $this->input->post('email');
		 //~ $agent_id = $this->input->post('agent_id'); 
		 //~ $name = $this->input->post('name'); 
		 //$user_type = $this->input->post('user_type'); 
		$password = "AES_ENCRYPT(".$this->input->post('password').",'".SECURITY_KEY."')";
		$count = $this->Account_Model->isRegistered($email)->num_rows();

		if($count == 1){ 
			$userInfo = $this->Account_Model->isRegistered($email)->row();

			if($userInfo->user_type_id==6){
				$guest =true;
			}else{
				$guest =false;
			}
			$users = '';
			
			if($this->session->userdata('domain_id') != ''){
				$users  = $this->input->post('users') ;
			}
			if($userInfo->user_status == 'ACTIVE'){                            
				$count = $this->Account_Model->isValidUser($email, $password, $users,$guest)->num_rows();
				
				if($count == 1){ 
					$user_data = $this->Account_Model->isValidUser($email, $password,$users)->row();
					if($user_data->user_type_id== 2||4) 
					{ 
						if($user_data->user_type_id==4)
						{
						$this->session->set_userdata(array(
							'domain_logo'=> $user_data->user_profile_pic));	
						}  
						$this->session->set_userdata(array(
							'user_logged_in'=>1 
							)); 
					} 
					$b2c_session =  array(
						'user_details_id' => $userInfo->user_details_id,
						'user_session_id' => $userInfo->user_details_id,
						'user_type' => $userInfo->user_type_id,
						'user_account_no' => $userInfo->user_account_number,
						'user_country_id' => $userInfo->country_id,
						'user_state_id' => $userInfo->state_id,
						'role_id' => $userInfo->user_role_id,
						'user_email' => $userInfo->user_email,
						'provider' => 'Bookurflight'
						);

					if($userInfo->branch_id == 0) {
						$b2c_session['branch_id']= $userInfo->user_details_id;
					}else{
						$b2c_session['branch_id']= $userInfo->branch_id;	
					}


					$_SESSION['b2c_details_id'] = $userInfo->user_details_id;
					$this->session->set_userdata($b2c_session); 				
					$continue = $this->session->userdata('continue');
					
					$response = array(
						'status' => '1',
						'success' => 'true',
						'msg' => 'Success!',
						'rid' => $user_data->user_login_details_id,
						'fname' =>  $userInfo->user_name,
						'profile_photo' =>  $userInfo->user_profile_pic,
						'continue' => $continue,
						'url' =>base_url().'dashboard'
						);


				}else{
					$response = array(
						'status' => '0',
						'success' => 'false',
						'password-error' => 'Invalid Password.'
						);
				}
			}else{
				$response = array(
					'status' => '0',
					'success' => 'false',
					'account-error' => 'Invalid Account.'
					);
			}	
		}else{
			$response = array(
				'status' => '0',
				'success' => 'false',
				'account-error' => 'Invalid Account.'
				);
		}	
		echo json_encode($response); 
	}
	
	public function forgetPassword(){
		$email = $data['user_email'] = $this->input->post('fp_email'); 
		$account = $data['account_id'] = $this->input->post('account_id'); 
		$count = $this->Account_Model->isRegistered($email,$account)->num_rows();
		
		if($count == 1){ 
			$userInfo = $this->Account_Model->isRegistered($email,$account)->row();

			if($userInfo->user_status == 'ACTIVE'){
				$user_password = $this->Account_Model->getLoginParticular($email,$account)->row();
				
				$password = "AES_DECRYPT(".$user_password->user_password." ,'".SECURITY_KEY."')";

				$status = $this->get_mail_content_forgotpass($email,$account, $password);

				if($status == '1'){
					$response = array(
						'status' => '1',
						'success' => 'true',
						'msg' => "A link to reset your password has been sent to ".$email.".!"
						);
				}
			}else{
				$response = array(
					'status' => '0',
					'success' => 'false',
					'msg' => "Invalid Email."
					);
			}
		}else{
			$response = array(
				'status' => '0',
				'success' => 'false',
				'msg' => 'Invalid Email.'
				);
		}
		print json_encode($response);
	}
	
	public function logout(){
		$this->load->library('session');
		$this->session->unset_userdata('user_session_id');
		$this->session->sess_destroy();
		redirect(base_url());
	}
	
	public function success(){
		$this->load->view('b2b/thankyou');
	}
	
	public function get_mail_content_forgotpass($email,$account, $password) {

		$data['password'] = $password;

		$data['user_data'] = $this->Account_Model->isRegistered($email,$account)->row();
		$email_type = 'forgotpassword';
		$data['email_template'] = $this->Email_Model->get_email_template($email_type)->row();
		
		$key = $this->generate_random_key();
		$secret = md5($email);

		$this->Account_Model->updatePwdResetLink(@$data['user_data']->user_details_id, $key, $secret);

		$data['reset_link'] = base_url().'account/set_password/'.$key.'/'.$secret;
		
		$Response = $this->Email_Model->sendmail_forgot_password($data);

		$data['facebook_social_url'] = 'facebook.com';
		$data['twitter_social_url'] = 'twitter.com';
		$data['google_social_url'] = 'google.com';
		return $Response;
	}
	
	public function generate_random_key($length = 50) {
		$alphabets = range('A','Z');
		$numbers = range('0','9');
		$additional_characters = array('_','.');
		$final_array = array_merge($alphabets,$numbers,$additional_characters);
		$id = '';
		while($length--) {
			$key = array_rand($final_array);
			$id .= $final_array[$key];
		}
		return $id;
	}
	public function set_password($key,$secret){
		if($key == '' || $secret == ''){
			$data['msg'] = 'sorry link has been expired, plese reset again';
			$data['status'] = '0';
		}else{
			$count = $this->Account_Model->isvalidSecrect($key,$secret)->num_rows();
			if($count == 1){
				$user_data = $this->Account_Model->isvalidSecrect($key,$secret)->row();
				$data['status'] = '1';
				$data['email'] = $user_data->user_email;
				$data['account_number'] = $user_data->user_account_number;
				$data['user_details_id'] = $user_data->user_details_id;
			}else{
				$data['msg'] = 'sorry link has been expired, plese reset again';
				$data['status'] = '0';
			}
		}
		$this->load->view('general/reset_password', $data);
	}

	public function resetpwd(){
		$email = $this->input->post('email'); 
		$account_number = $this->input->post('account_number');
		$user_details_id = $this->input->post('user_details_id'); 
		$password = "AES_ENCRYPT(".$this->input->post('confirm_password').",'".SECURITY_KEY."')"; 
		$this->Account_Model->updateB2b($password,$email,$account_number,$user_details_id);
		$response = array(
			'status' => '1',
			'success' => 'true',
			'msg' => "Your password has been changed you can login now!"
			);

		redirect(base_url()); 
	}

	public function changePassword(){
		$user_id = $this->input->post('user_details_id'); 
		$current_password = $this->input->post('current_password');
		$password = $this->input->post('password'); 
		$cpassword = $this->input->post('cpassword'); 
		$current_password = "AES_ENCRYPT(".$current_password.",'".SECURITY_KEY."')";  
		$count =  $this->Account_Model->checkUserPassword($current_password, $user_id)->num_rows();
		if($count == 1) {
			
			if($this->input->post('password') != ''){
				$password_validation = $this->Validation_Model->passwordValidation($this->input->post('password'));	
				if(!$password_validation){
					$data['error']['password-error'] = "Password should contain atleast on Capital letter, one Numbers and one Special Characters";
				}
			}
			
			
			
			if($this->input->post('cpassword')!= ''){
				$password_validation = $this->Validation_Model->checkPasswords($this->input->post('password'), $this->input->post('cpassword'));	
				if(!$password_validation){
					$data['error']['password-error'] = "Password not valid";
				}
			}



			if(isset($data['error']) && count($data['error']) > 0) {
				$data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			}
			

			$password = "AES_ENCRYPT(".$this->input->post('cpassword').",'".SECURITY_KEY."')";  
			
			$update =	$this->Account_Model->update_user_password($password,$user_id);
			$response = array(
				'status' => '1',
				'success' => 'true',
				'msg' => "Your password has been changed!"
				);
			
		} else {
			$response = array(
				'status' => '2',
				'success' => 'fail',
				'msg' => "Your current password is not matched!"
				);
		}
		
		print json_encode($response);
	}		

	public function update_user_profile_image(){ 

		$ssid = $this->session->Userdata("user_details_id");
		
		$user_profile_name = '';
		if(!empty($_FILES[0]['name']))
		{	
			if(is_uploaded_file($_FILES['0']['tmp_name'])) 
			{
				$sourcePath = $_FILES['0']['tmp_name']; 
				$img_Name=time().$_FILES['0']['name'];
				$targetPath = "cpanel/uploads/agent/".$img_Name;
				if(move_uploaded_file($sourcePath,$targetPath)){

					$user_profile_name = $img_Name;
				}
			}				
		}
		$this->Account_Model->add_user_image($ssid,$user_profile_name);
			// redirect(base_url().'dashboard');
		echo json_encode(true);
	}
	
	public function update_user_profile_image_b2b2b(){
		$ssid = $this->session->Userdata("user_details_id");
		
		$user_profile_name = '';
		
		if(!empty($_FILES['profilePhoto']['name']))
		{	
			if(is_uploaded_file($_FILES['profilePhoto']['tmp_name'])) 
			{
				$sourcePath = $_FILES['profilePhoto']['tmp_name']; 
				$img_Name=time().$_FILES['profilePhoto']['name'];
				$targetPath = "../b2b2b/cpanel/uploads/agent/".$img_Name;
				if(move_uploaded_file($sourcePath,$targetPath)){
					$user_profile_name = $img_Name;
				}
			}				
		}

		$this->Account_Model->add_user_image($ssid,$user_profile_name);
		redirect(base_url().'usermanagement/editProfile');
			//echo json_encode(true);
	}

	public function sucss_msg(){ 
		$this->load->view('b2b/thankyou');
	}
	
	

	public function change_theme(){
		$theme                 = isset($_POST['theme_name'])?$_POST['theme_name']:'';
		$user_id               = isset($_POST['user_details_id'])?$_POST['user_details_id']:'';		
		$update                 =	$this->Account_Model->update_theme($theme,$user_id);
		echo json_encode(true); 
	}

      	//End Of Language
	public function change_url(){     
		$url_name                 = isset($_POST['url_name'])?$_POST['url_name']:'';
		$url                      = base_url()."root/".$url_name ;
		$user_id                  = isset($_POST['user_details_id'])?$_POST['user_details_id']:'';	
		$update                   =	$this->Account_Model->update_url($url_name,$url,$user_id);
		echo json_encode(true); 
	}
	
	public function regB2BUser(){
		
		$country['nationality_countries'] = $this->General_Model->getNationalityCountries()->result_array();
		$this->load->view('b2b/B2BC_register',$country);
		
	}


	/**
	* Dinesh Kumar
	* Forgot password
	**/
	public function send_password_reset_link() {
	// echo "<pre>"; print_r($_REQUEST);echo "</pre>"; exit();
		$email = $data['user_email'] = $_REQUEST['email'];

		$count = $this->General_Model->isRegistered($email)->num_rows();

		if ($count == 1) {
			$userInfo = $this->General_Model->isRegistered($email)->row();

			if ($userInfo->user_status == 'ACTIVE') {

				$user_password = $this->General_Model->getLogin($email, $userInfo->user_details_id)->row();

				$password = "AES_ENCRYPT(".$user_password->user_password.",'".SECURITY_KEY."')";
                //$password = md5($user_password->admin_password);

				$status = $this->get_mail_content_forgotpass($email,$account='', $password);

				if ($status == '1') {
					$response = array(
						'status' => '1',
						'success' => 'true',
						'msg' => "A link to reset your password has been sent to " . $email . ".!"
						);
				} else {
					$response = array(
						'status' => '0',
						'success' => 'false',
						'msg' => "Please contact the Administrator."
						);
				}
			} else {
				$response = array(
					'status' => '0',
					'success' => 'false',
					'msg' => "Invalid Email."
					);
			}
		} else {
			$response = array(
				'status' => '0',
				'success' => 'false',
				'msg' => 'Invalid Email.'
				);
		}
		$this->load->library('session');
		$this->session->set_flashdata('forget_pass_msg', $response);

		redirect(base_url().'home/forget_password');
       /* echo json_encode($response);
       exit;*/
        //

   }

	/**
	* 
	* Ends Here
	**/


	
}
?>
