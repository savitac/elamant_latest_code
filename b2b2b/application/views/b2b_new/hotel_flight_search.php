<?php error_reporting(-1)?>
<?php if(isset($flight_search_params['adult_config']) == false || intval($flight_search_params['adult_config']) < 1) {
	$flight_search_params['adult_config'] = 1;
} 
?>

	<div id="Flight" class="tab-pane">
		<form autocomplete="off" onSubmit="return validateform();" action="<?php echo base_url(); ?>flight/pre_flight_search" name="flight_search_form" id="flight_search_form">
			<div class="intabs">  
			   <div class="waywy">
				<div class="smalway">
					<label class="wament hand-cursor ">
						<input class="hide" type="radio" name="trip_type" id="onew-trp" value="oneway" checked> One Way</label>
						<label class="wament hand-cursor">
						<input class="hide" type="radio" name="trip_type" id="rnd-trp" value="circle"> Roundtrip</label>
						<label class="wament hand-cursor"><input class="hide" type="radio" name="trip_type" id="multi-trp" value="multicity"> Multi City</label></div>
				</div>    			
			  <div class="clearfix"></div>
			  <div class="outsideserach col-md-9 nopad" id="normal">
				<div class="col-lg-4 col-md-6 col-sm-6 fiveh"> 
				  <div class="marginbotom10">
				  <span class=" formlabel">From </span>
				  <div class="relativemask "> <span class="maskimg  hfrom"></span>
					<input type="text" name="from" id="from" value="<?php echo @$onw_rndw_segment_search_params['from'] ?>" placeholder="Airport City" class="fromflight ft" required >
					<input class="hide loc_id_holder" name="from_loc_id" type="hidden" value="<?=@$onw_rndw_segment_search_params['from_loc_id']?>">
				  </div>
				  </div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 fiveh"> 
				  <div class="marginbotom10">
				  <span class=" formlabel">To</span>
				  <div class="relativemask"> <span class="maskimg  hfrom"></span>
					<input type="text" name="to" id="to" value="<?php echo @$onw_rndw_segment_search_params['to'] ?>" placeholder="Airport City" class="arrivalflight ft" required>   
					<input class="loc_id_holder" name="to_loc_id" type="hidden" value="<?=@$onw_rndw_segment_search_params['to_loc_id']?>">
				  </div>
				  </div>
				</div>
				
				<div class="col-md-4 nopad">
				  <div class="col-xs-6 fiveh"> 
					<div class="marginbotom10">
					<span class=" formlabel">Departure</span>
					<div class="relativemask"> <span class="maskimg caln"></span>
					  <input type="text"   id="st_date" name="depature" placeholder="Select Date" value="<?php echo @$onw_rndw_segment_search_params['depature'] ?>" class="forminput" required>
					</div>
					</div> 
				  </div>
				  <div class="col-xs-6 fiveh" id="returns"> 
					<div class="marginbotom10">
					<span class=" formlabel">Return</span>
					<div class="relativemask"> <span class="maskimg caln"></span>
					  <input type="text" id="en_date" name="return" placeholder="Select Date" value="<?php echo @$onw_rndw_segment_search_params['return'] ?>" class="forminput" required>
					</div>
					</div>
				  </div>
				</div>
		</div> 
		<?php echo $this->load->view('share/flight_multi_way_search_1'); ?>
	    <div class="interst_clone_wrapper"></div>
 	<div class="col-xs-3 nopad" >
 <div class="col-xs-6 fiveh">
					<div class="lablform">&nbsp;</div>
					<div class="totlall">
						<span class="remngwd"><span class="total_pax_count">1</span> Traveller(s)</span>
						<div class="roomcount pax_count_div">
							<div class="inallsn">
								<div class="oneroom fltravlr">
									<div class="roomrow">
										<div class="celroe col-xs-4">Adults
											<span class="agemns">(12+)</span>
										</div>
										<div class="celroe col-xs-8">
											<div class="input-group countmore pax-count-wrapper adult_count_div"> <span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="minus" data-field="adult"> <span class="glyphicon glyphicon-minus"></span> </button>
												</span>
												<input type="text" id="OWT_adult" name="adult" class="form-control input-number centertext valid_class pax_count_value" value="<?=(int)@$flight_search_params['adult_config']?>" min="1" max="9" readonly>
												<span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="adult"> <span class="glyphicon glyphicon-plus"></span> </button>
												</span> 
											</div>
										</div>
									</div>
									<div class="roomrow">
										<div class="celroe col-xs-4">Children
											<span class="agemns">(2-11)</span>
										</div>
										<div class="celroe col-xs-8">
											<div class="input-group countmore pax-count-wrapper child_count_div"> <span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="minus" data-field="child"> <span class="glyphicon glyphicon-minus"></span> </button>
												</span>
												<input type="text" id="OWT_child" name="child" class="form-control input-number centertext pax_count_value" value="<?=(int)@$flight_search_params['child_config']?>" min="0" max="9" readonly>
												<span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="child"> <span class="glyphicon glyphicon-plus"></span> </button>
												</span> 
											</div>
										</div>
									</div>
									<div class="roomrow">
										<div class="celroe col-xs-4">Infants
											<span class="agemns">(0-2)</span>
										</div>
										<div class="celroe col-xs-8">
											<div class="input-group countmore pax-count-wrapper infant_count_div"> <span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="minus" data-field="infant"> <span class="glyphicon glyphicon-minus"></span> </button>
												</span>
												<input type="text" id="OWT_infant" name="infant" class="form-control input-number centertext pax_count_value" value="<?=(int)@$flight_search_params['infant_config']?>" min="0" max="9" readonly>
												<span class="input-group-btn">
						search_flight						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="infant"> <span class="glyphicon glyphicon-plus"></span> </button>
												</span> 
											</div>
										</div>
									</div>
									<!-- Infant Error Message-->
									<div class="roomrow">
										<div class="celroe col-xs-8">
										<div class="alert-wrapper hide">
										<div role="alert" class="alert alert-error">
											<span class="alert-content"></span>
										</div>
										</div>
										</div>
									</div>
									<!-- Infant Error Message-->
								</div>
							</div>
						</div>
					</div>
				</div>
<!-- 				<?php 
//Preferred Airlines
		if(isset($flight_search_params['carrier'][0]) == true && empty($flight_search_params['carrier'][0]) == false) {
			$choosen_airline_name = $airline_list[$flight_search_params['carrier'][0]];
		} else {
			$choosen_airline_name = ''.get_legend('Preferred Airline').'';
		}
		$preferred_airlines = '<a class="adscrla choose_preferred_airline" data-airline_code="">All</a>';
		foreach($airline_list as $airline_list_k => $airline_list_v) {
			$preferred_airlines .= '<a class="adscrla choose_preferred_airline" data-airline_code="'.$airline_list_k.'">'.$airline_list_v.'</a>';
		}
 ?> -->
				<div class="col-md-6 nopad">
				   
				<div class="col-md-12 fiveh">
					<div class="formsubmit">  
						<input type="hidden" autocomplete="off" name="carrier[]" id="carrier" value="">
						<input type="hidden"   id="search_flight" name="search_flight" placeholder="Depature Date" value="search" class="forminput">
						<!-- <input type="hidden" autocomplete="off" name="v_class" id="class" value="All"> -->
						<!--<button class="srchbutn comncolor" style="float: right;">Search Flights <span class="srcharow"></span></button>-->

						<div class="searchsbmtfot">
						<input type="submit" class="searchsbmt" value="search" />
					</div>
					</div>
				</div>
				  <div class="col-md-12 col-xs-12 padfive">
			<button style="display:none" class="add_city_btn" id="add_city"> <span class="fa fa-plus"></span> Add City</button>
			</div>
				  </div>  
				 </div>
				 			<div class="clearfix"></div>
			<div class="alert-box" id="flight-alert-box"></div>
			<div class="clearfix"></div>
			<div class="csearch_flightol-xs-10 nopad">
			<div class="togleadvnce">
				<div class="advncebtn">
					<div class="labladvnce">Advanced options</div>
				</div>
				<div class="advsncerdch">
						<div class="col-xs-3 nopad"> 
						<div class="marginbotom10">
						<!-- <span class="formlabel">Class</span> -->
						<div class="selectedwrap alladvnce">
						  <select class="mySelectBoxClass flyinputsnor" name="v_class" >
							<option>All</option>
							<option>Economy With Restrictions</option>
							<option>Economy Without Restrictions</option>
							<option>Economy Premium</option>
							<option>Business</option>
							<option>First</option>
						  </select>
						</div>
					  </div>
					  </div>
					<div class="col-xs-3 nopad">
					   <div class="marginbotom10">
					      <div class="alladvnce pr_air">
					         <span class="remngwd" id="choosen_preferred_airline">Preferred Airline</span>
					         <input type="hidden" autocomplete="off" name="carrier[]" id="carrier" value="">
					         <div class="advncedown spladvnce preferred_airlines_advance_div">
					            <div class="inallsnnw">
					               <div class="scroladvc">
					                  <a class="adscrla choose_preferred_airline" data-airline_code="">All</a><a class="adscrla choose_preferred_airline" data-airline_code="SU">Aeroflot airline-(SU)</a><a class="adscrla choose_preferred_airline" data-airline_code="G9">Air arabia-(G9)</a><a class="adscrla choose_preferred_airline" data-airline_code="I5">Air asia-(I5)</a><a class="adscrla choose_preferred_airline" data-airline_code="KC">Air astana-(KC)</a><a class="adscrla choose_preferred_airline" data-airline_code="AC">Air canada-(AC)</a><a class="adscrla choose_preferred_airline" data-airline_code="CA">Air china-(CA)</a><a class="adscrla choose_preferred_airline" data-airline_code="AF">Air france-(AF)</a><a class="adscrla choose_preferred_airline" data-airline_code="AI">Air india-(AI)</a><a class="adscrla choose_preferred_airline" data-airline_code="IX">Air india express-(IX)</a><a class="adscrla choose_preferred_airline" data-airline_code="MK">Air mauritius-(MK)</a><a class="adscrla choose_preferred_airline" data-airline_code="UK">Air vistara-(UK)</a><a class="adscrla choose_preferred_airline" data-airline_code="LB">Aircosta-(LB)</a><a class="adscrla choose_preferred_airline" data-airline_code="OP">Airpegasus-(OP)</a><a class="adscrla choose_preferred_airline" data-airline_code="AZ">Alitalia-(AZ)</a><a class="adscrla choose_preferred_airline" data-airline_code="AA">American airlines-(AA)</a><a class="adscrla choose_preferred_airline" data-airline_code="OZ">Asiana airlines-(OZ)</a><a class="adscrla choose_preferred_airline" data-airline_code="OS">Austrian airways-(OS)</a><a class="adscrla choose_preferred_airline" data-airline_code="BG">Biman bangladesh-(BG)</a><a class="adscrla choose_preferred_airline" data-airline_code="BA">British airways-(BA)</a><a class="adscrla choose_preferred_airline" data-airline_code="BD">British midland-(BD)</a><a class="adscrla choose_preferred_airline" data-airline_code="SN">Brussels airlines-(SN)</a><a class="adscrla choose_preferred_airline" data-airline_code="CX">Cathay pacific-(CX)</a><a class="adscrla choose_preferred_airline" data-airline_code="CI">China airlines-(CI)</a><a class="adscrla choose_preferred_airline" data-airline_code="MU">China eastern airlines-(MU)</a><a class="adscrla choose_preferred_airline" data-airline_code="CZ">China southern-(CZ)</a><a class="adscrla choose_preferred_airline" data-airline_code="CO">Continental airways-(CO)</a><a class="adscrla choose_preferred_airline" data-airline_code="CY">Cyprus airways-(CY)</a><a class="adscrla choose_preferred_airline" data-airline_code="DL">Delta airlines-(DL)</a><a class="adscrla choose_preferred_airline" data-airline_code="KB">Druk air-(KB)</a><a class="adscrla choose_preferred_airline" data-airline_code="MS">Egypt air-(MS)</a><a class="adscrla choose_preferred_airline" data-airline_code="LY">Elal israel airways-(LY)</a><a class="adscrla choose_preferred_airline" data-airline_code="EK">Emirates airlines-(EK)</a><a class="adscrla choose_preferred_airline" data-airline_code="ET">Ethiopian airlines-(ET)</a><a class="adscrla choose_preferred_airline" data-airline_code="EY">Etihad airways-(EY)</a><a class="adscrla choose_preferred_airline" data-airline_code="BR">Eva air-(BR)</a><a class="adscrla choose_preferred_airline" data-airline_code="AY">Finnair-(AY)</a><a class="adscrla choose_preferred_airline" data-airline_code="GA">Garuda airlines-(GA)</a><a class="adscrla choose_preferred_airline" data-airline_code="Z5">Gmg airlines-(Z5)</a><a class="adscrla choose_preferred_airline" data-airline_code="G8">Goair-(G8)</a><a class="adscrla choose_preferred_airline" data-airline_code="GF">Gulf air-(GF)</a><a class="adscrla choose_preferred_airline" data-airline_code="IC">Indian airlines-(IC)</a><a class="adscrla choose_preferred_airline" data-airline_code="6E">Indigo-(6E)</a><a class="adscrla choose_preferred_airline" data-airline_code="IR">Iran air-(IR)</a><a class="adscrla choose_preferred_airline" data-airline_code="JL">Japan airlines-(JL)</a><a class="adscrla choose_preferred_airline" data-airline_code="J9">Jazeera airways-(J9)</a><a class="adscrla choose_preferred_airline" data-airline_code="9W">Jet airways-(9W)</a><a class="adscrla choose_preferred_airline" data-airline_code="S2">Jetlite-(S2)</a><a class="adscrla choose_preferred_airline" data-airline_code="KQ">Kenya airways-(KQ)</a><a class="adscrla choose_preferred_airline" data-airline_code="IT">Kingfisher airlines-(IT)</a><a class="adscrla choose_preferred_airline" data-airline_code="KL">Klm northwest-(KL)</a><a class="adscrla choose_preferred_airline" data-airline_code="KE">Korean air-(KE)</a><a class="adscrla choose_preferred_airline" data-airline_code="KU">Kuwait airways-(KU)</a><a class="adscrla choose_preferred_airline" data-airline_code="LH">Lufthansa-(LH)</a><a class="adscrla choose_preferred_airline" data-airline_code="W5">Mahan air-(W5)</a><a class="adscrla choose_preferred_airline" data-airline_code="MH">Malaysian airline-(MH)</a><a class="adscrla choose_preferred_airline" data-airline_code="CE">Nationwide-(CE)</a><a class="adscrla choose_preferred_airline" data-airline_code="NW">Northwest airline-(NW)</a><a class="adscrla choose_preferred_airline" data-airline_code="WY">Oman air-(WY)</a><a class="adscrla choose_preferred_airline" data-airline_code="OTH">Other airlines-(OTH)</a><a class="adscrla choose_preferred_airline" data-airline_code="PK">Pakistan airlines-(PK)</a><a class="adscrla choose_preferred_airline" data-airline_code="QF">Qantas airways-(QF)</a><a class="adscrla choose_preferred_airline" data-airline_code="QR">Qatar airways-(QR)</a><a class="adscrla choose_preferred_airline" data-airline_code="RJ">Royal jordanian-(RJ)</a><a class="adscrla choose_preferred_airline" data-airline_code="RA">Royal nepal-(RA)</a><a class="adscrla choose_preferred_airline" data-airline_code="SV">Saudi arabian-(SV)</a><a class="adscrla choose_preferred_airline" data-airline_code="MI">Silk air-(MI)</a><a class="adscrla choose_preferred_airline" data-airline_code="SQ">Singapore airlines-(SQ)</a><a class="adscrla choose_preferred_airline" data-airline_code="SA">South african-(SA)</a><a class="adscrla choose_preferred_airline" data-airline_code="SG">Spicejet-(SG)</a><a class="adscrla choose_preferred_airline" data-airline_code="UL">Srilankan airlines-(UL)</a><a class="adscrla choose_preferred_airline" data-airline_code="LX">Swiss air-(LX)</a><a class="adscrla choose_preferred_airline" data-airline_code="RB">Syrian air-(RB)</a><a class="adscrla choose_preferred_airline" data-airline_code="TG">Thai airways-(TG)</a><a class="adscrla choose_preferred_airline" data-airline_code="2T">Trujet-(2T)</a><a class="adscrla choose_preferred_airline" data-airline_code="TK">Turkish airlines-(TK)</a><a class="adscrla choose_preferred_airline" data-airline_code="UA">United airlines-(UA)</a><a class="adscrla choose_preferred_airline" data-airline_code="US">Us airways-(US)</a><a class="adscrla choose_preferred_airline" data-airline_code="HY">Uzbekistan airways-(HY)</a><a class="adscrla choose_preferred_airline" data-airline_code="VN">Vietnam airways-(VN)</a><a class="adscrla choose_preferred_airline" data-airline_code="VS">Virgin atlantic-(VS)</a><a class="adscrla choose_preferred_airline" data-airline_code="IY">Yemena airways-(IY)</a>									
					               </div>
					            </div>
					         </div>
					      </div>
					   </div>
					</div>
				</div>
			</div>
		</div>

				 </div>
	   </form> 
	</div>

     
<?php echo $this->load->view('dashboard/flight_search_prob'); ?>

