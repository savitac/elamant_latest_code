<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Travel Lights</title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" />
</head>
<body>
	
    <?php 
      $this->session->userdata('user_details_id');
    if($this->session->userdata('user_details_id') == '') { 
	echo $this->load->view('core/header'); 
	} else {
	echo $this->load->view('dashboard/top'); 	 
	 } ?>
	
	<div class="allpagewrp top80">
	    <?php echo $this->load->view('b2b/offer'); ?>		   
	</div>
	<?php echo $this->load->view('core/bottom_footer'); ?>

	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/backslider.js"></script> 
	
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/general.js"></script> 
	 
		<script type="text/javascript">
		$(document).ready(function(){
		$('.scrolltop').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
		 });
		 
		 
		 
		 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	

	});
	</script> 
<!--<script type="text/javascript">
    jQuery(document).ready(function( $ ) {
        /* ============ LAYER SLIDER ================*/
        jQuery("#layerslider").layerSlider({
            responsive: true,
            responsiveUnder: 1280,
            layersContainer: 1170,
            skin: 'fullwidth',
            hoverPrevNext: true,
            skinsPath: 'layerslider/skins/'
        });
    });
</script>-->
<script type="text/javascript">
$(document).ready(function(){
	
	 
	 //homepage slide show
$(function($){
	
				var url = '<?php echo ASSETS; ?>/assets/images/';
				
				
				$.supersized({
				
					// Functionality
					slide_interval          :   5000,		// Length between transitions
					transition              :   1, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed		:	700,		// Speed of transition
															   
					// Components							
					slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
					slides 					:  	[			// Slideshow Images
												{image : url+'/slide2.jpg' ,title : '  Start Your Online' ,description:'travel agency today', price:'REGISTER FOR B2B'},
												{image : url+'/slide3.jpg' ,title : '  Start Your Online' ,description:'travel agency today', price:'REGISTER FOR B2B'},
												]
					
				});
		    });

//homepage slide show end

});
</script> 
</body>
</html>
