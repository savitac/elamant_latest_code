<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title><?= $this->session->userdata('company_name')?></title>
<?php echo $this->load->view('core/load_css'); ?>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>

<link href="<?php echo ASSETS;?>assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<style>
	.info-box {
  background: #fff none repeat scroll 0 0;
  border-radius: 2px;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  display: block;
  margin-bottom: 15px;
  min-height: 90px;
  width: 100%;
}
.info-box-icon {
  background: rgba(0, 0, 0, 0.2) none repeat scroll 0 0;
  border-radius: 2px 0 0 2px;
  display: block;
  float: left;
  font-size: 45px;
  height: 90px;
  line-height: 90px;
  text-align: center;
  width: 90px;
}
.info-box-content {
  margin-left: 90px;
  padding: 5px 10px;
}
.info-box-text {
  text-transform: uppercase;
}
.progress-description, .info-box-text {
  display: block;
  font-size: 14px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}
.info-box-number {
  display: block;
  font-size: 18px;
  font-weight: bold;
}
.hotel-l-bg {
  background-color: #00a65a;
  border: 1px solid #00a65a;
  color: #000;
}
.bus-l-bg {
  background-color: #dd4b39;
  border: 1px solid #dd4b39;
  color: #000;
}
.bg-yellow, .callout.callout-warning, .alert-warning, .label-waring, .modal-warning .modal-body {
  background-color: #f39c12;
}
.flight-l-bg {
  background-color: #0073b7;
  border: 1px solid #0073b7;
  color: #000;
}
</style>
</head>

<body>
<!-- Navigation -->

<?php echo $this->load->view('dashboard/top'); ?> 
<!-- /Navigation -->





<section id="main-content">
  
  <section class="wrapper">
	<div class="main-chart">
		
        <div id="BookingList">
        <span class="profile_head"><?php echo $this->TravelLights['Bookings']['MyBookings']; ?></span>

<div class="rowit">
	 
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box"> <span class="info-box-icon flight-l-bg"><i class="fa fa-suitcase"></i></span>
                <div class="info-box-content"> <span class="info-box-text">
                	<a href="<?php echo base_url().'package/view_packages_types'; ?>">
				View Package Types 
				</a>
                </span> 
                     </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box"> <span class="info-box-icon hotel-l-bg"><i class="fa fa-suitcase"></i></span>
                <div class="info-box-content"> <span class="info-box-text">	<a href="<?php echo base_url().'package/add_with_price'; ?>">
				Add New Package 
				</a></span>  </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box"> <span class="info-box-icon bus-l-bg"><i class="fa fa-suitcase"></i></span>
                <div class="info-box-content"> <span class="info-box-text"><a href="<?php echo base_url().'package/view_with_price'; ?>">
				View Packages 
				</a></span>  </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box"> <span class="info-box-icon bg-yellow"><i class="fa fa-suitcase"></i></span>
                <div class="info-box-content"> <span class="info-box-text">	<a href="<?php echo base_url().'package/enquiries'; ?>">
				View Packages Enquiries 
				</a></span>  </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
 </div>
</div>
</div>
</div>

                                
                                
                              
                                
        </div>
  </section>

</section>


<!-- /.container --> 

<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
 <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>

  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
    
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 




<script type="text/javascript">
$(document).ready(function(){
	
  //get_booking_hotel_records();
	
   $( "#datepicker11,#datepicker111" ).datepicker({
	   onClose: function( selectedDate ) {
		$( "#datepicker22" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker22' ).focus();
		
		$( "#datepicker222" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker222' ).focus();
	}
	   });
	   
   $( "#datepicker22, #datepicker222" ).datepicker();
   $( "#datepicker3" ).datepicker({  maxDate: new Date() });
   
  
    $("#by_user").on("change", function(){
		 if($(this).val() == 'ByStaff'){
			 $('#b2b2b').css('display', 'none');
			 $('#staff').css('display', 'inherit');
		 }else if($(this).val() == 'BySubAgents'){
			  $('#b2b2b').css('display', 'inherit');
			 $('#staff').css('display', 'none');
		 }
		
	});
	
	$("#t_by_user").on("change", function(){
		 if($(this).val() == 'ByStaff'){
			 $('#t_b2b2b').css('display', 'none');
			 $('#t_staff').css('display', 'inherit');
		 }else if($(this).val() == 'BySubAgents'){
			  $('#t_b2b2b').css('display', 'inherit');
			 $('#t_staff').css('display', 'none');
		 }
		
	});
	  
	  
	  $("#by_type").on("change", function(){
		 if($(this).val() == 'Booking'){
			 $('#payment_status').css('display', 'none');
			 $('#booking_status').css('display', 'inherit');
		 }else if($(this).val() == 'payment'){
			  $('#payment_status').css('display', 'inherit');
			 $('#booking_status').css('display', 'none');
		 }
		
	});
  
	
	$('#hotel_books').click(function(){
		$('#BookingList').fadeOut(500, function(){
				$('#hotel_show').fadeIn();
				get_booking_hotel_records();
			});
	}); 
	
    $('#flights_books').click(function(){
	
             $('#BookingList').fadeOut(500, function(){
				$('#flight_show').fadeIn();
				get_flight_list();
			});;
	});
	
	$('#transfer_books').click(function(){
		$('#BookingList').fadeOut(500, function(){
				$('#transfer_show').fadeIn();
				$("#booking_transfer_record").css("display","inherit");
				get_transfer_list();
			});
	}); 
	
 
	
	$('#back_list').click(function(){
		$('#hotel_show').fadeOut(500, function(){
				$('#BookingList').fadeIn();
			});
	});
	
	
	$('#flight_back_list').click(function(){
		$('#flight_show').fadeOut(500, function(){
				$('#BookingList').fadeIn();
			});
	});
	
	
	
	$('#trans_back_list').click(function(){
		$('#transfer_show').fadeOut(500, function(){
				$('#BookingList').fadeIn();
				$("#booking_transfer_record").css("display","none");
			});
	});

	
	$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
	});
	
	
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	
});




$("#owl-demobaners1").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	  $("#owl-demobaners2").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	   $("#owl-demobaners3").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });


$("#owl-demo3").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });

$("#owl-demo4").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });
	  
	  $("#owl-demo5").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });


$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});
var Script = function () {


//    sidebar dropdown menu auto scrolling





//    sidebar toggle

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                //$('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });
	
/*	$('.fa-bars').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });*/




// widget tools

    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });




}();


//Prevent mousewheel on hover of sidebar
/*$( '.top_dash' ).bind( 'mousewheel DOMMouseScroll', function ( e ) {
    var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;

    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
    e.preventDefault();
});*/


</script>

<script>
$(document).ready(function(){
		
		$('#editquestion').click(function(){
			$('.fullquestionswrp').slideToggle(500);
		});
		
		$('#editprivatepub').click(function(){
			$('.fullquestionswrpshare').slideToggle(500);
		});
		
		$('#smsalert').click(function(){
			$('.fullquestionswrp2').slideToggle(500);
		});
		
		$('#changepaswrd').click(function(){
			$('.fullquestionswrp3').slideToggle(500);
		});

        $('#addMarkUp').click(function() {
            $('.fullquestionswrp5').slideToggle(500);
        })
			
	});
	

	</script>
	<script >
		
		 
	  function get_booking_hotel_filter_result(){
		  
		   if($("#reference_type").val() != ''){
			 
			 if($("#reference_no").val() == ''){
				 $("#reference-no-error").html("Please enter the reference number");
				 return false;
			 }else{
				  $("#reference-no-error").html("");
			 }
		  }
		  
		  if($("#reference_no").val() != ''){
			 
			 if($("#reference_type").val() == ''){
				 $("#reference-type-error").html("Please enter the reference type");
				 return false;
			 }else{
				  $("#reference-type-error").html("");
			 }
		  }
		 
		
		 if($("#by_booking").val() != ''){
			 
			 if($("#datepicker11").val() == ''){
				 $("#start-type-error").html("Please select start date");
				 return false;
			 }else{
				 $("#start-type-error").html("");
			 }
			 
			 if($("#datepicker22").val() == ''){
				 $("#end-type-error").html("Please select end date");
				 return false;
			 }else{
				 $("#end-type-error").html("");
			 }
		 }
		 
		 if($("#datepicker11").val() != '' || $("#datepicker22").val() != '' ){
			 if($("#by_booking").val() == ''){
				 $("#date-type-error").html("Please select date type");
				 return false;
			 }else{
				 $("#date-type-error").html(""); 
			 }
			 
		 }
		 
		 if($("#datepicker11").val() != '' || $("#datepicker22").val() != '' ){
			 if($("#by_booking").val() == ''){
				 $("#date-type-error").html("Please select date type");
				 return false;
			 }else{
				 $("#date-type-error").html(""); 
			 }
			 
		 }
		 
		 if($("#staff_id").val() != 0  ){
			 if($("#by_user").val() == ''){
				   $("#user-type-error").html("Please select user type");
			 }else{
				 $("#user-type-error").html("");
			 }
		 }
		 
		 if($("#by_type").val() != 0 ){
			 if($("#status").val() == ''){
				   $("#by-status-error").html("Please select status");
			 }else{
				 $("#by-status-error").html("");
			 }
		 }
		 
		 if($("#status").val() != 0 ){
			 if($("#by_type").val() == ''){
				   $("#by-type-error").html("Please select status type");
			 }else{
				 $("#by-type-error").html("");
			 }
		 }
		
		 $.ajax({
			type: "POST",
			url: $("#bookins_hotel").attr('action'),
			data: $("#bookins_hotel").serialize(),
			dataType: "json",
			success: function(data){
			  	$("#booking_hotel_record").html(data.hotel_bookings); 
				return false;
			}
		});
		 
	 }
	 
	 
	 function get_booking_transfer_filter_result(){
		  
		   if($("#t_reference_type").val() != ''){
			 
			 if($("#t_reference_no").val() == ''){
				 $("#t_reference-no-error").html("Please enter the reference number");
				 return false;
			 }else{
				  $("#t_reference-no-error").html("");
			 }
		  }
		  
		  if($("#t_reference_no").val() != ''){
			 
			 if($("#t_reference_type").val() == ''){
				 $("#t_reference-type-error").html("Please enter the reference type");
				 return false;
			 }else{
				  $("#t_reference-type-error").html("");
			 }
		  }
		 
		
		 if($("#t_by_booking").val() != ''){
			 
			 if($("#datepicker111").val() == ''){
				 $("#t_start-type-error").html("Please select start date");
				 return false;
			 }else{
				 $("#t_start-type-error").html("");
			 }
			 
			 if($("#datepicker222").val() == ''){
				 $("#t_end-type-error").html("Please select end date");
				 return false;
			 }else{
				 $("#t_end-type-error").html("");
			 }
		 }
		 
		 if($("#datepicker111").val() != '' || $("#datepicker222").val() != '' ){
			 if($("#t_by_booking").val() == ''){
				 $("#t_date-type-error").html("Please select date type");
				 return false;
			 }else{
				 $("#t_date-type-error").html(""); 
			 }
			 
		 }
		 
		 if($("#datepicker111").val() != '' || $("#datepicker222").val() != '' ){
			 if($("#t_by_booking").val() == ''){
				 $("#t_date-type-error").html("Please select date type");
				 return false;
			 }else{
				 $("#t_date-type-error").html(""); 
			 }
			 
		 }
		 
		 if($("#t_staff_id").val() != 0  ){
			 if($("#by_user").val() == ''){
				   $("#t_user-type-error").html("Please select user type");
			 }else{
				 $("#t_user-type-error").html("");
			 }
		 }
		 
		 if($("#t_by_type").val() != 0 ){
			 if($("#status").val() == ''){
				   $("#t_by-status-error").html("Please select status");
			 }else{
				 $("#t_by-status-error").html("");
			 }
		 }
		 
		 if($("#t_status").val() != 0 ){
			 if($("#t_by_type").val() == ''){
				   $("#t_by-type-error").html("Please select status type");
			 }else{
				 $("#bt_y-type-error").html("");
			 }
		 }
		
		 $.ajax({
			type: "POST",
			url: $("#bookins_transfer").attr('action'),
			data: $("#bookins_transfer").serialize(),
			dataType: "json",
			success: function(data){
			  	$("#booking_transfer_record").html(data.transfer_bookings); 
				return false;
			}
		});
		 
	 }
	 
		
	 function  get_booking_hotel_records(){
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().'bookings/get_hotel_bookings' ?>",
			dataType: "json",
			success: function(data){
					$("#booking_hotel_record").html(data.hotel_bookings); 
				return false;
			}
		}); 
     }
     
     function get_transfer_list(){
		 $.ajax({
			type: 'POST',
			url: "<?php echo base_url().'bookings/get_transfer_bookings' ?>",
			dataType: "json",
			success: function(data){
					$("#booking_transfer_record").html(data.transfer_bookings); 
				return false;
			}
		}); 
	 }
	 
	  function get_flight_list(){
		 $.ajax({
			type: 'POST',
			url: "<?php echo base_url().'bookings/get_flight_bookings' ?>",
			dataType: "json",
			success: function(data){
				
		     	$("#booking_flight_record").html(data.transfer_bookings); 
					return false;
			},
			error:function(){
				alert('error');
			}
		}); 
	 }
     
 
	 
	</script>


  
</body>
</html>
