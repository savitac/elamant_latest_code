<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo  $this->session->userdata('company_name');?></title>
  <?php echo $this->load->view('core/load_css'); ?>
  <link href="<?php echo ASSETS;?>assets/css/dashboard.css"[A-Za-z]{3} rel="stylesheet">
  <link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<?php echo ASSETS; ?>assets/js/daterangepicker/daterangepicker-bs3.css">
</head>
<body>
  
  <?php echo $this->load->view('dashboard/top'); ?> 

<section id="main-content">
<section class="wrapper">
<div id="package_types" class="bodyContent col-md-12">
  <div class="panel panel-default">
    <!-- PANEL WRAP START -->
    <div class="panel-heading">
      <!-- PANEL HEAD START -->
      <div class="panel-title">
        <ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
          <!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
          <li role="presentation" class="active">
            <a href="#fromList"
              aria-controls="home" role="tab" data-toggle="tab">
              <h1>Edit Package
              </h1>
            </a>
          </li>
          <!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
        </ul>
      </div>
    </div>
    <!-- PANEL HEAD START -->
    <div class="panel-body">
      <!-- PANEL BODY START -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="fromList">
          <div class="col-md-12">
            <div class='row'>
              <div class='row'>
                <div class='col-sm-12'>
                  <div class='' style='margin-bottom:0;'>
                    <div class='box-content'>
  <form class='form form-horizontal validate-form' style='margin-bottom: 0;' action="<?php echo base_url(); ?>supplier/update_package/<?php echo $packdata->package_id;?>" method="post" enctype="multipart/form-data">
                       <input type="hidden" name="w_wo_d" value="w">
                      
                        <div class='form-group'>
                           <label class='control-label col-sm-3'  for='validation_current'>Package Name
 </label>
                             <div class='col-sm-4 controls'>
                        
                            <div class="controls">
                          <input type="text" name="name" id="name" value ="<?php echo $packdata->package_name;?>" data-rule-required='true' class='form-control'>
                          </div>
                     
                          </div>
                          </div>
                        
                          <div class='form-group'>
                           <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fiveh">
                      <span class="form-group">Duration</span> -->
                      <label class='control-label col-sm-3'  for='validation_current'>Duration
  </label>
      <div class="col-sm-4 controls"> 
      <input type="text" name="duration" class="form-control" id="duration" value ="<?php echo $packdata->duration;?>" onchange="show_duration_info(this.value)" size="40" disabled>                          
      
       
      </div>                   
                  </div>
                 
                        <div class='form-group'>
                          <label class='control-label col-sm-3' for='validation_country'>Country</label>
                          <div class='col-sm-4 controls'>
                            <select class='select2 form-control' data-rule-required='true' name='country' id="validation_country" value ="<?php echo $packdata->package_country;?>">
                            <!--  <input type="text" name="country" id="country" data-rule-required='true' class='form-control'>  -->
                              <?php foreach ($countries as $country) {?><option  value='<?php echo $country->country_id;?>'<?php if($country->country_id == $packdata->package_country) { echo "selected=selected"; } ?>><?php echo $country->name;?></option>
                                 <?php }?>
                           </select>
                          </div>
                        </div>
                       

                        <div class='form-group'>
                           <label class='control-label col-sm-3'  for='validation_current'>City
 </label>
                             <div class='col-sm-4 controls'>

                            <div class="controls">
                          <input type="text" name="city" id="country" value ="<?php echo $packdata->package_city;?>" data-rule-required='true' class='form-control' disabled>
                          </div>
                     
                          </div>
                          </div>
                          <div class='form-group'>
                           <label class='control-label col-sm-3'  for='validation_current'>Location
  </label>
                             <div class='col-sm-4 controls'>
                   
                      <input type="text" name="location" id="location" data-rule-required='true' class='form-control' value ="<?php echo $packdata->package_location;?>">
                        
                          </div>
                          </div>
                          
                                 <div class='form-group'>
                          <label class='control-label col-sm-3' for='validation_company'>Package Main Image</label>
                          <div class='col-sm-3 controls'>
                         <!--  <input type="hidden" value="<?php echo $packdata->image; ?>" name="photo"> -->
                                 <input type="file" title='Image to add to add' class=''   id='photo' name='photo'>                                 
                                  <input type="hidden" name='hidephoto' value="<?php echo $packdata->image; ?>">
                                 <img src="<?php echo ASSETS.DOMAIN_PCKG_UPLOAD_DIR.$packdata->image; ?>" width="100" name="photo">
                                 </div>
                          
                           
                        </div>

                                 <div class='form-group'>
                            <label class='control-label col-sm-3' for='validation_name'>Description</label>
                          <div class='col-sm-3 controls'>
                               <textarea name="Description" class="form-control" data-rule-required="true"  cols="70" rows="3" placeholder="Description" value =""><?php echo $packdata->package_description;?></textarea>
                             <!--   <span id="dorigin_error" style="color:#F00;  display:none;"></span> -->
                           </div> 
                           </div>
                      
                  <div class='form-group'>
                         
                      <label class='control-label col-sm-3'  for='validation_rating' >Rating  </label>
      <div class="col-sm-4 controls" >
      <select class='form-control' data-rule-required='true' name='rating' id="rating" value ="" >
     
                <option><?php echo $packdata->rating;?></option>
                                <option value="0">0</option>
                                <option value="1">1</option> 
                                <option value="2">2</option>
                                <option value="3">3</option>  
                                <option value="4">4</option>
                                <option value="5">5</option>
                                 
                                 </select>
                                    </div>  
                                     </div>
                 
                     <div class='box-header blue-background'>
                      <div class=''><h4>Price Info</h4></div>

                      
                    </div>

                          <div><h2></h2></div>
                  
                          
                     
                         <!--  </div> -->
                          <div class='form-group'>
                          <label class='control-label col-sm-3'  for='validation_number'>Price
 </label>
                             <div class='col-sm-3 controls'>
                        
                         
                          <input type="text"  name="p_price" id="p_price" data-rule-number='true' data-rule-required='true' class='form-control' value ="<?php echo $packdata->price;?>">
                         
                     
                          </div>
                          </div>
                           <div class='form-group'>
                                 <div class='form-group' id="addMultiCity"> </div> 
                              <div id="addCityButton" class="col-lg-2" style="display:none">
                                <input type="button" class="srchbutn comncolor" id="addCityInput" value="Add Month" style="padding:3px 10px;">
                                <input type="hidden" value="1" id="multiCityNo" name="no_of_days">
                           </div>
                           <div id="removeCityButton" class="col-lg-2" style="display:none;">
                                <input type="button" class="srchbutn comncolor" id="removeCityInput" value="Remove One Month" style="padding:3px 10px;">
                           </div>
                       </div>
                   
                         
                          
                         
                          
                          <div class='box-header blue-background'>
                      <div class=''><h4>Pricing Policy</h4></div>
                      
                      
                    </div>
                      <div><h2></h2></div>
                     
                          <div class='form-group'>
                           <label class='control-label col-sm-3'  for='validation_includes'>Price Includes
  </label>
                             <div class='col-sm-4 controls'>
                   
                      <!-- <input type="text" name="includes" id="includes" data-rule-required='true' class='form-control'> -->
                      <textarea name="includes" class="form-control" data-rule-required="true" value ="" cols="70" rows="3" placeholder="Price Includes"><?php echo $packdata->price_includes;?></textarea>

                        
                          </div>
                          </div>
                          <div class='form-group'>
                           <label class='control-label col-sm-3'  for='validation_excludes'>Price Excludes
  </label>
                             <div class='col-sm-4 controls'>
                   
                      <!-- <input type="text" name="excludes" id="excludes" data-rule-required='true' class='form-control'> -->
                       <textarea name="excludes" class="form-control" data-rule-required="true" value =""  cols="70" rows="3" placeholder="Price Excludes"><?php echo $packdata->price_excludes;?></textarea>
                        
                          </div>
                          </div>
                          <div class='box-header blue-background'>
                      <div class=''><h4>Cancellation & Refund Policy</h4></div>
                     
                    </div>
                      <div><h1></h1></div>
                      <div class='form-group'>
                           <label class='control-label col-sm-3'  for='validation_advance'>Cancellation In Advance
  </label>
                             <div class='col-sm-4 controls'>
                   
                      <!-- <input type="text" name="excludes" id="excludes" data-rule-required='true' class='form-control'> -->
                       <textarea name="advance" class="form-control" data-rule-required="true"  cols="70" rows="3" placeholder="Cancellation In Advance"><?php echo $packdata->cancellation_advance;?></textarea>
                        
                          </div>
                          </div>
                          <div class='form-group'>
                           <label class='control-label col-sm-3'  for='validation_excludes'>Cancellation Penalty
  </label>
                             <div class='col-sm-4 controls'>
                   
                      <!-- <input type="text" name="excludes" id="excludes" data-rule-required='true' class='form-control'> -->
                       <textarea name="penality" class="form-control" data-rule-required="true"  cols="70" rows="3" placeholder="Cancellation Penalty"><?php echo $packdata->cancellation_penality;?></textarea>
                        
                          </div>
                          </div>
                        
                          </div>

                      
                    </div>
                           
                     </div>
                     

                     <div class='form-actions' style='margin-bottom:0'>
                          <div class='row'>
                            <div class='col-sm-9 col-sm-offset-3'>
                              <a href="<?php echo base_url(); ?>supplier/view_with_price">
                              <button class='btn btn-primary' type='button'>
                                <i class='icon-reply'></i>
                                Go Back
                              </button></a>
                              <button class='btn btn-primary' type='submit'>
                                <i class='icon-save'></i>
                                Update
                              </button>
                            </div>
                          </div>
                        </div>
                        
                  </div>
                </div>
                </form>
              
        </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- PANEL BODY END -->
</div>
<!-- PANEL WRAP END -->
</div>
</section>
</section>
  
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <!-- <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script> -->
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
  <script src="<?php echo ASSETS; ?>assets/js/daterangepicker/moment.min.js"></script>
  <script src="<?php echo ASSETS; ?>assets/js/daterangepicker/daterangepicker.js"></script>
<script>
$.validator.addMethod("buga", (function(value) {
  return value === "buga";
}), "Please enter \"buga\"!");

$.validator.methods.equal = function(value, element, param) {
  return value === param;
};


$(function () {
  $('#datetimepicker2').datetimepicker({
      startDate: new Date()
  });

  $('#datetimepicker1').datetimepicker({
      startDate: new Date()
  });
});


    </script>
