<?php
//debug($raw_hotel_list['HotelSearchResult']);die('Now this');
$CI =& get_instance();
$CI->load->helper('api_share_helper'); 
$template_images = ASSETS.'assets/images/';
$mini_loading_image = '<div class="text-center loader-image">Please Wait</div>';
@$trace_id = $raw_hotel_list['HotelSearchResult']['TraceId'];

foreach ($raw_hotel_list['HotelSearchResult'] as $hd) {
	$current_hotel_rate = $hd['star_rating'];  
?>
<div class="item list-group-item rowresult1 r-r-i">

	<div class="col-xs-12 nopad">
		<div class="madgrid">
			<div class="sidenamedesc ">
				<div class="celhtl width22 midlbord">
					<div class="hotel_image">
						<img  data-src="<?=@$img?>" class="lazy lazy_loader h-img">
						<a data-lat="<?=@$hd['latitude']?>" data-lon="<?=@$hd['longitude']?>" data-target="#map_view_hotel" data-toggle="modal" class="hotel_location fa fa-map-marker hide"></a>
					</div>
				</div>
				<div class="celhtl width60">
					<div class="waymensn">
						<div class="flitruo_hotel">
							<div class="hoteldist">
								<span class="hotel_name h-name"><?=$hd['name'].'-<span class="a-t" data-cstr="'.@$hd['accomodation_cstr'].'">'.(@$hd['accomodation_type']).'</span>'?>
								<span class="a-t" data-cstr="Hotel"></span></span>
								<div class="clearfix"></div>
								<div style="height: 15px; overflow: hidden; display: none;"><?=@$hd['category_display'].' '.@$hd['hotel_chain']?></div>
								<span class="h-sr hide"><?php echo $hd['star_rating']?></span>

								<span class="hotel_address elipsetool"><?=@$hd['address']?></span>

								<div data-star="<?=$hd['star_rating']?>" class="stra_hotel">
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class=""><?//=@$hd['category_code']?></span>
								</div>
								
								<span class="h-loc hide"><?=@$hd['location']?></span>
								<span class="deal-status hide" data-deal="0"></span>
								<div class="col-md-7 nopad">
								<div class="side_amnties">
									<ul class="hotel_fac">
									<?php
									 $hdfacilityp = getGtaFacilities($hd['facilities']);
									if(empty($hdfacilityp) == false && valid_array($hdfacilityp)) {
										foreach($hdfacilityp as $fv) {
											echo '<li data-cstr="'.$fv['cstr'].'" data-class="'.$fv['icon_class'].'" class="'.$fv['icon_class'].' tooltipv h-f" title="'.$fv['name'].'"></li>';
										}
									} else {
										//echo '<li class="" data-toggle="tooltip" title="NA"></li>';
									}
									?>
									</ul>
									<!-- <input type="hidden" class="m-f-l" value="<?=@$hdfacilityp['icon_class']?>"> -->
									<input type="hidden" class="m-f-l" value="<?=@$hd['facility_cstr']?>">
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="celhtl width18">
					<div class="hotel_sideprice">
					<!-- <p>Avg Per Night</p> -->
						<div class="sideprice_hotel">
							<span class="currency_symbol"><?=$hd['currency']?></span> <span><?=number_format($hd['price'],2)?></span>
							<span class="h-p hide"><?php echo number_format($hd['price'],2); ?></span>
							<span class="prce_per">Price per Night</span>
						</div>
						
						<div class="bookbtn_htl"> <a href="<?=base_url()?>index.php/hotel/hotel_details/<?=$search_id?>?booking_source=<?=$hd['booking_source']?>&hotel_id=<?=$hd['hotel_code']?>" class="booknow">Details</a> </div>
					</div>

						<!-- <div class="ribbon-green">Offer</div> -->
				</div>
			</div>
		</div>
	</div>
</div>

<?php
}?>


