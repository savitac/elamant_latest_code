<?php
$template_images = $GLOBALS['CI']->template->template_images();
$mini_loading_image = '<div class="text-center loader-image">Please Wait</div>';
$search_id = intval($attr['search_id']);
foreach ($raw_hotel_list['HotelSearchResult'] as $hk => $hd) {
	//echo 'gta';debug($hd);
	$pssLatitude = $hd['latitude'];
    $pssLongitude = $hd['longitude'];
    $psshotelname = $hd['name'];
    $psskey = '0731cf5df59141869f672680686624d2';
   // $key = '4eaddeb7de0145cdb54b9bce95a947e7';
$url = 'http://api.tripadvisor.com/api/partner/2.0/map/'.$pssLatitude.','.$pssLongitude.'?key='.$psskey.'&q='.$psshotelname;
//echo $url;die;
$tripadvisor_data = file_get_contents($url);
	$tripadvisor_data_jsondecoded = json_decode($tripadvisor_data,1);
	//echo "<pre/>"; print_r($tripadvisor_data_jsondecoded['data'][0]['rating']);
	$tripadvisor_rating = $tripadvisor_data_jsondecoded['data'][0]['rating'];
	$img = (array_key_exists('img', $hd) == true) ? $hd['img']: NO_IMG; 
?>
<div class="item list-group-item rowresult1 r-r-i">
	<div class="col-xs-12 nopad">
		<div class="madgrid">
			<div class="sidenamedesc ">
				<div class="celhtl width22 midlbord">
					<div class="hotel_image">
						<img alt="<?=@$hd['name']?>" data-src="<?=@$img?>" class="lazy lazy_loader h-img">
						<a data-lat="<?=@$hd['latitude']?>" data-lon="<?=@$hd['longitude']?>" data-target="#map_view_hotel" data-toggle="modal" class="hotel_location fa fa-map-marker hide"></a>
					</div>
				</div>
				<div class="celhtl width60">
					<div class="waymensn">
						<div class="flitruo_hotel">
							<div class="hoteldist">
								<span class="hotel_name h-name"><?=$hd['name'].'-<span class="a-t" data-cstr="'.@$hd['accomodation_cstr'].'">'.(@$hd['accomodation_type']).'</span>'?></span>
								<span class="a-t" data-cstr="Hotel">HOTEL</span>
								<div class="clearfix"></div>
								<div style="height: 15px; overflow: hidden; display: none;"><?=@$hd['category_display'].' '.@$hd['hotel_chain']?></div>
								<span class="h-sr hide"><?php echo $hd['star_rating']?></span>

								<span class="hotel_address elipsetool"><?=@$hd['address']?></span>

								<div data-star="<?=$hd['star_rating']?>" class="stra_hotel">
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class=""><?//=@$hd['category_code']?></span>
								</div>
								
								<span class="h-loc hide"><?=@$hd['location']?></span>
								<span class="deal-status hide" data-deal="0"></span>
								<div class="col-md-7 nopad">
								<div class="side_amnties">
									<ul class="hotel_fac">
									<?php
									 $hdfacilityp = getGtaFacilities($hd['facility']);
									if(empty($hdfacilityp) == false && valid_array($hdfacilityp)) {
										foreach($hdfacilityp as $fv) {
											echo '<li data-cstr="'.$fv['cstr'].'" data-class="'.$fv['icon_class'].'" class="'.$fv['icon_class'].' tooltipv h-f" title="'.$fv['name'].'"></li>';
										}
									} else {
										echo '<li class="" data-toggle="tooltip" title="NA"></li>';
									}
									?>
									</ul>
									<input type="hidden" class="m-f-l" value="<?=@$hdfacilityp['icon_class']?>">
								</div>
								</div>

								<div class="col-md-5 nopad trip_adv">
								<span>Tripadvisor Rating : <?= $tripadvisor_rating;?>/5</span>
                                     <!-- <img class="pull-right" style="margin-right: 10px;" src="<?=$GLOBALS['CI']->template->template_images('trip_a.png'); ?>"> -->
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="celhtl width18">
					<div class="hotel_sideprice">
					<!-- <p>Avg Per Night</p> -->
						<div class="sideprice_hotel">
							<span class="currency_symbol"><?=$hd['currency']?></span> <span><?=$hd['price']?></span>
							<span class="h-p hide"><?php echo ($hd['price']); ?></span>
						</div>
						
						<div class="bookbtn_htl"> <a href="<?=base_url()?>index.php/hotel/hotel_details/<?=$search_id?>?booking_source=<?=$booking_source?>&hotel_id=<?=$hd['hotel_code']?>" class="booknow">Details</a> </div>
					</div>

						<!-- <div class="ribbon-green">Offer</div> -->
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
});
</script>

<?php
}


