

<div class="all_loading imgLoader loader-image fulloading result-pre-loader-wrapper loader-image forhoteload">
<div class="load_inner">
	<div class="loadmask hide"></div>
	<div class="centload">
		<div class="loadcity hide"></div>
		
		
		<div class="loader-wrap">
  <svg class="loader-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 116.031 116.031" style="enable-background:new 0 0 116.031 116.031;" xml:space="preserve" >
  <rect x="0" y="0" style="display:none;fill:#f52007;" width="116.031" height="116.031"/>
  <g id="BG">
    <path style="opacity:0.2;fill-rule:evenodd;clip-rule:evenodd;fill:#f52007;" d="M58.639,0.019
      c32.031,0.344,57.718,26.589,57.374,58.62c-0.344,32.031-26.589,57.718-58.62,57.374c-32.031-0.344-57.718-26.589-57.374-58.62
      C0.363,25.362,26.608-0.325,58.639,0.019z"/>
    <path style="fill-rule:evenodd;clip-rule:evenodd;fill:#f5f5f5;" d="M58.542,9.019c27.06,0.291,48.761,22.463,48.471,49.524
      c-0.291,27.06-22.463,48.761-49.524,48.471C30.429,106.722,8.728,84.55,9.019,57.489C9.309,30.429,31.482,8.728,58.542,9.019z"/>
  </g>
  <g id="ICON-GROUP">
    <defs>
		<path id="SVGID_1_" d="M40.909,103.933C15.55,94.485,2.651,66.268,12.099,40.909c9.448-25.359,37.665-38.258,63.024-28.81
			c25.359,9.448,38.258,37.665,28.81,63.024C94.485,100.482,66.268,113.38,40.909,103.933z"/>
	</defs>
	<clipPath id="SVGID_2_">
		<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
	</clipPath>
	<g id="ICONS" style="clip-path:url(#SVGID_2_);">
    <g>
      <animateTransform 
                        id="down" 
                        attributeName="transform" 
                        attributeType="XML" 
                        type="translate" 
                        from="0,0"
                        to="0, -95"
                        dur="0.475s" 
                        calcMode="spline"
                        keySplines=".25,.1,.25,1" 
                        keyTimes="0;1"
                        begin="0s; next.end"
                         /> 
        <animateTransform 
                        id="left"
                        attributeName="transform" 
                        attributeType="XML" 
                        type="translate" 
                        from="0, -95"
                        to="-95, -95"
                        dur="0.475s" 
                        calcMode="spline"
                        keySplines=".25,.1,.25,1" 
                        keyTimes="0;1"
                        begin="0s; down.end"/> 
              <animateTransform 
                        id="next"
                        attributeName="transform" 
                        attributeType="XML" 
                        type="translate" 
                        from="-95, -95"
                        to="-194, -95"
                        dur="0.475s" 
                        calcMode="spline"
                        keySplines=".25,.1,.25,1" 
                        keyTimes="0;1"
                        begin="0s; left.end"/> 
      <g id="ICON-PAINT">
          <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M62.45,45.23v45.999h-7.539v-4.977v-2h-2h-6.923h-2v2v4.977H36.45V45.23H62.45 M64.45,43.23h-30v49.999h11.538v-6.977
    h6.923v6.977H64.45C64.45,76.563,64.45,59.896,64.45,43.23L64.45,43.23z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M45.441,49.881v4h-4v-4H45.441 M47.441,47.881h-8v8h8V47.881L47.441,47.881z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M78.45,59.229v4h-4v-4H78.45 M80.45,57.229h-8v8h8V57.229L80.45,57.229z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M78.45,71.229v4h-4v-4H78.45 M80.45,69.229h-8v8h8V69.229L80.45,69.229z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M78.45,83.229v4h-4v-4H78.45 M80.45,81.229h-8v8h8V81.229L80.45,81.229z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M45.441,61.881v4h-4v-4H45.441 M47.441,59.881h-8v8h8V59.881L47.441,59.881z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M45.441,73.975v4h-4v-4H45.441 M47.441,71.975h-8v8h8V71.975L47.441,71.975z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M57.447,49.881v4h-4v-4H57.447 M59.447,47.881h-8v8h8V47.881L59.447,47.881z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M57.447,61.881v4h-4v-4H57.447 M59.447,59.881h-8v8h8V59.881L59.447,59.881z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M57.447,73.975v4h-4v-4H57.447 M59.447,71.975h-8v8h8V71.975L59.447,71.975z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M82.45,55.229v36h-12v-36H82.45 M84.45,53.229h-16v40h16V53.229L84.45,53.229z"/>       
      </g>
      <g id="ICON-PAINT_1_">
        <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M256.99,134v45.999h-7.539v-4.977v-2h-2h-6.922h-2v2v4.977h-7.539V134H256.99 M258.99,132h-30v49.999h11.539v-6.977h6.922
    v6.977h11.539C258.99,165.333,258.99,148.666,258.99,132L258.99,132z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M239.982,138.65v4h-4v-4H239.982 M241.982,136.65h-8v8h8V136.65L241.982,136.65z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M272.99,147.999v4h-4v-4H272.99 M274.99,145.999h-8v8h8V145.999L274.99,145.999z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M272.99,159.999v4h-4v-4H272.99 M274.99,157.999h-8v8h8V157.999L274.99,157.999z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M272.99,171.999v4h-4v-4H272.99 M274.99,169.999h-8v8h8V169.999L274.99,169.999z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M239.982,150.65v4h-4v-4H239.982 M241.982,148.65h-8v8h8V148.65L241.982,148.65z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M239.982,162.744v4h-4v-4H239.982 M241.982,160.744h-8v8h8V160.744L241.982,160.744z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M251.988,138.65v4h-4v-4H251.988 M253.988,136.65h-8v8h8V136.65L253.988,136.65z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M251.988,150.65v4h-4v-4H251.988 M253.988,148.65h-8v8h8V148.65L253.988,148.65z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M251.988,162.744v4h-4v-4H251.988 M253.988,160.744h-8v8h8V160.744L253.988,160.744z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M276.99,143.999v36h-12v-36H276.99 M278.99,141.999h-16v40h16V141.999L278.99,141.999z"/>
      </g>
      <g id="ICON-ALBUM_1_">
        <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M51.431,134v45.999h-7.539v-4.977v-2h-2H34.97h-2v2v4.977h-7.539V134H51.431 M53.431,132h-30v49.999H34.97v-6.977h6.922
    v6.977h11.539C53.431,165.333,53.431,148.666,53.431,132L53.431,132z"/>
  <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M34.423,138.65v4h-4v-4H34.423 M36.423,136.65h-8v8h8V136.65L36.423,136.65z"/>
  <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M67.431,147.999v4h-4v-4H67.431 M69.431,145.999h-8v8h8V145.999L69.431,145.999z"/>
  <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M67.431,159.999v4h-4v-4H67.431 M69.431,157.999h-8v8h8V157.999L69.431,157.999z"/>
  <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M67.431,171.999v4h-4v-4H67.431 M69.431,169.999h-8v8h8V169.999L69.431,169.999z"/>
  <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M34.423,150.65v4h-4v-4H34.423 M36.423,148.65h-8v8h8V148.65L36.423,148.65z"/>
  <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M34.423,162.744v4h-4v-4H34.423 M36.423,160.744h-8v8h8V160.744L36.423,160.744z"/>
  <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M46.429,138.65v4h-4v-4H46.429 M48.429,136.65h-8v8h8V136.65L48.429,136.65z"/>
  <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M46.429,150.65v4h-4v-4H46.429 M48.429,148.65h-8v8h8V148.65L48.429,148.65z"/>
  <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M46.429,162.744v4h-4v-4H46.429 M48.429,160.744h-8v8h8V160.744L48.429,160.744z"/>
  <path style="opacity:0.7;fill:#f52007;enable-background:new;" d="M71.431,143.999v36h-12v-36H71.431 M73.431,141.999h-16v40h16V141.999L73.431,141.999z"/>       
      </g>
      <g id="ICON-DATE_1_">
        <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M159.014,134v45.999h-7.539v-4.977v-2h-2h-6.922h-2v2v4.977h-7.539V134H159.014 M161.014,132h-30v49.999h11.539v-6.977
    h6.922v6.977h11.539C161.014,165.333,161.014,148.666,161.014,132L161.014,132z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M142.006,138.65v4h-4v-4H142.006 M144.006,136.65h-8v8h8V136.65L144.006,136.65z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M175.014,147.999v4h-4v-4H175.014 M177.014,145.999h-8v8h8V145.999L177.014,145.999z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M175.014,159.999v4h-4v-4H175.014 M177.014,157.999h-8v8h8V157.999L177.014,157.999z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M175.014,171.999v4h-4v-4H175.014 M177.014,169.999h-8v8h8V169.999L177.014,169.999z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M142.006,150.65v4h-4v-4H142.006 M144.006,148.65h-8v8h8V148.65L144.006,148.65z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M142.006,162.744v4h-4v-4H142.006 M144.006,160.744h-8v8h8V160.744L144.006,160.744z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M154.012,138.65v4h-4v-4H154.012 M156.012,136.65h-8v8h8V136.65L156.012,136.65z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M154.012,150.65v4h-4v-4H154.012 M156.012,148.65h-8v8h8V148.65L156.012,148.65z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M154.012,162.744v4h-4v-4H154.012 M156.012,160.744h-8v8h8V160.744L156.012,160.744z"/>
  <path style="opacity:0.6;fill:#f52007;enable-background:new;" d="M179.014,143.999v36h-12v-36H179.014 M181.014,141.999h-16v40h16V141.999L181.014,141.999z"/>       
      </g>
      </g>
    </g>

  </g> 
  <g id="RING-GROUP" >
      <animateTransform attributeName="transform" 
                        attributeType="XML" 
                        type="rotate" 
                        from="0 58.02 58.02" 
                        to="360 58.02 58.02" 
                        dur="1.75s" 
                        repeatCount="indefinite"
                        calcMode="spline"
                        keySplines="0.4, 0, 0.2, 1" 
                        keyTimes="0;1"/> 
    
      <path style="opacity:0.5;fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#f52007;stroke-width:9;stroke-linecap:round;stroke-miterlimit:10;" d="
      M32.557,105.36C15.569,96.206,4.093,78.183,4.273,57.545c0.152-17.39,8.547-32.779,21.444-42.49"/>

      <path style="opacity:0.5;fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#f52007;stroke-width:9;stroke-linecap:round;stroke-miterlimit:10;" d="
      M58.481,4.743c1.013,0.009,2.019,0.046,3.017,0.11 M86.788,102.859 M61.498,4.853c28.004,1.81,50.038,25.219,49.79,53.628
      c-0.163,18.656-9.897,34.989-24.5,44.378"/>
  </g>
  </svg>
</div>
		
		
		
		
		
		
		<div class="clodnsun1 hide"></div>
		<div class="reltivefligtgo hide">
			<div class="flitfly1"></div>
		</div>
		<div class="relativetop">
			<!--<div class="paraload">
				Searching for the best hotels
			</div>
			<div class="clearfix"></div>
			<div class="placenametohtl"><?php echo ucfirst($result['location']); ?></div>-->
			<div class="clearfix"></div>
			<div class="sckintload">
				<div class="ffty">
					<div class="borddo brdrit">
						<span class="lblbk">Check In</span>
					</div>
				</div>
				<div class="ffty">
					<div class="borddo">
						<span class="lblbk">Check Out</span>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="tabledates">
					<div class="tablecelfty">
						<div class="borddo brdrit">
							<div class="fuldate">
								<span class="bigdate"><?php echo date("d",strtotime($result['from_date']));?></span>
								<div class="biginre">
									<?php echo date("M",strtotime($result['from_date']));?><br />
									<?php echo date("Y",strtotime($result['from_date']));?>
								</div>
							</div>
						</div>
					</div>
					<div class="tablecelfty">
						<div class="borddo">
							<div class="fuldate">
								<span class="bigdate"><?php echo date("d",strtotime($result['to_date']));?></span>
								<div class="biginre">
									<?php echo date("M",strtotime($result['to_date']));?><br />
									<?php echo date("Y",strtotime($result['to_date']));?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="nigthcunt"><?php echo $hotel_search_params['no_of_nights'];?> <?=(intval($hotel_search_params['no_of_nights']) > 1 ? 'Nights' : 'Night')?></div>
			</div>
		</div>
		<div class="clearfix"></div>
		<!--<div class="busrunning">
			<div class="runbus"></div>
			<div class="runbus2"></div>
			<div class="roadd"></div>
		</div>-->
	</div>
	
				<div class="progress flghtloas">
			<div class="progress-bar progress-bar-striped active" id="progressbar" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">Please Wait...</div>
			</div>
</div>
</div>