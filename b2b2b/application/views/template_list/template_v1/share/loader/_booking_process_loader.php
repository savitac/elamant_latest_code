<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?=$this->session->userdata('company_name')?></title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="<?php echo ASSETS;?>assets/js/jquery-2.1.1.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANXPM-4Tdxq9kMnI8OpL-M6kGsFFWreIY" type="text/javascript"></script>
    <link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo ASSETS;?>assets/css/theme_style_new.css" rel="stylesheet"/>
    <link href="<?php echo ASSETS;?>assets/css/shared.css" rel="stylesheet"/>
    <link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet"/> 
    <link href="<?php echo ASSETS;?>assets/js/jquery-ui-1.11.2.custom/jquery-ui.theme.min.css" rel="stylesheet"/> 
    <link href="<?php echo ASSETS;?>assets/js/jquery-ui-1.11.2.custom/jquery-ui.structure.min.css" rel="stylesheet"/> 
    <link href="<?php echo ASSETS;?>assets/css/owl.carousel.min.css" rel="stylesheet" />
    <link href="<?php echo ASSETS;?>assets/css/front_end.css" rel="stylesheet" /> 
    <link href="<?php echo ASSETS; ?>assets/css/media.css" rel="stylesheet"> 
     </head>
    <body>
        <?php 
        if($this->session->userdata('user_logged_in') == 0) {  
      echo $this->load->view('core/header'); 
    } else {
      echo $this->load->view('dashboard/top');   
    }

   ?>
  <div class="clearfix"></div>  
<?php
$template_images = ASSETS.'assets/images/';
?>
<div class="fulloading result-pre-loader-wrapper">
  <div class="loadmask"></div>
  <div class="centerload cityload">
  
    <div class="relativetop">
      <div class="paraload">
        <h3>Processing your booking...</h3>
        <img src="<?=$template_images?>default_loading.gif" alt="" />
        <div class="clearfix"></div>
        <small>please wait</small>
      </div>
    </div>
  </div>
</div>

<form id="form"  action="<?=$form_url?>" method="<?=$form_method?>"><?php
  foreach ( $form_params as $key => $val ) {
    ?><input type="hidden" name="<?=$key?>" value="<?=$val?>" /><?php
  }
  ?>
</form>
</body>
    <script src="<?php echo ASSETS;?>assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/general.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/hotel_search.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/jquery.jsort.0.4.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/jquery.nicescroll.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/hotel_search_opt.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/jquery.lazy.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/hotel_suggest.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/app.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/javascript.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/login.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/datepicker.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/modernizr.custom.js"></script>

<script type="text/javascript">
$('.fulloading').show();
document.getElementById("form").submit();

</script>
