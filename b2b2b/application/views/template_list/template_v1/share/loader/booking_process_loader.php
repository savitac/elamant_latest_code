<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?=$this->session->userdata('company_name')?></title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="<?php echo ASSETS;?>assets/js/jquery-2.1.1.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANXPM-4Tdxq9kMnI8OpL-M6kGsFFWreIY" type="text/javascript"></script>
    <link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo ASSETS;?>assets/css/theme_style_new.css" rel="stylesheet"/>
    <link href="<?php echo ASSETS;?>assets/css/shared.css" rel="stylesheet"/>
    <link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet"/> 
    <link href="<?php echo ASSETS;?>assets/js/jquery-ui-1.11.2.custom/jquery-ui.theme.min.css" rel="stylesheet"/> 
    <link href="<?php echo ASSETS;?>assets/js/jquery-ui-1.11.2.custom/jquery-ui.structure.min.css" rel="stylesheet"/> 
    <link href="<?php echo ASSETS;?>assets/css/owl.carousel.min.css" rel="stylesheet" />
    <link href="<?php echo ASSETS;?>assets/css/front_end.css" rel="stylesheet" /> 
    <link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" /> 
    <link href="<?php echo ASSETS; ?>assets/css/theme_style.css" rel="stylesheet"> 
    <link href="<?php echo ASSETS; ?>assets/css/core.css" rel="stylesheet">
    <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
    <link href="<?php echo ASSETS; ?>assets/css/media.css" rel="stylesheet">  
     </head>
     <style type="text/css">
       .centerload {
    background: none repeat scroll 0 0 #fff;
    border-radius: 5px;
    box-shadow: 0 0 10px -5px #000;
    display: block;
    margin: 45px auto auto;
    width: 50%;
    max-width: 600px;
    min-width: 600px;
    padding: 20px;
    text-align: center;
}
.serht {
    color: #444;
    display: block;
    font-size: 16px;
    padding: 0;
    text-align: center;
}
.progress-bar {
    float: left;
    width: 0;
    height: 100%;
    font-size: 12px;
    line-height: 20px;
    color: #fff;
    text-align: center;
    background-color: #337ab7;
    -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
    box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
    -webkit-transition: width .6s ease;
    -o-transition: width .6s ease;
    transition: width .6s ease;
}
.progress-bar-striped, .progress-striped .progress-bar {
    background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
    background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
    background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
    -webkit-background-size: 40px 40px;
    background-size: 40px 40px;
}
.progress-bar-success {
    background-color: #5cb85c;
}
.progress-bar.active, .progress.active .progress-bar {
    -webkit-animation: progress-bar-stripes 2s linear infinite;
    -o-animation: progress-bar-stripes 2s linear infinite;
    animation: progress-bar-stripes 2s linear infinite;
}
.progress-bar {
    background-color: #198C8C;
}
.paraload { color: #F52007; }
     </style>
    <body>
        <?php 
        if($this->session->userdata('user_logged_in') == 0) {  
      echo $this->load->view('core/header'); 
    } else {
      echo $this->load->view('dashboard/top');   
    }

   ?>
  <div class="clearfix"></div>  
<?php
$template_images = ASSETS.'assets/images/';
?>
<!-- <div class="fulloading result-pre-loader-wrapper">
  <div class="loadmask"></div>
  <div class="centerload cityload">
  
    <div class="relativetop">
      <div class="paraload">
        <h3>Processing your booking...</h3>
        <img src="<?=$template_images?>default_loading.gif" alt="" />
        <div class="clearfix"></div>
        <small>please wait</small>
      </div>
    </div>
  </div>
</div> -->
<div class="all_loading imgLoader loader-image">
   <div class="load_inner">
      <div class="loadcity hide"></div>
      <div class="result-pre-loader-wrapper result-pre-loader">
         <div class="result-pre-loader-container">
<!--       <div class="loader-wrap">
  <img src="<?=$template_images?>logo.png" alt="" />
</div> -->
      <div class="loader-wrap">
  <svg class="loader-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 116.031 116.031" style="enable-background:new 0 0 116.031 116.031;" xml:space="preserve" >
  <rect x="0" y="0" style="display:none;fill:#055D84;" width="116.031" height="116.031"/>
  <g id="BG">
    <path style="opacity:0.2;fill-rule:evenodd;clip-rule:evenodd;fill:#f52007;" d="M58.639,0.019
      c32.031,0.344,57.718,26.589,57.374,58.62c-0.344,32.031-26.589,57.718-58.62,57.374c-32.031-0.344-57.718-26.589-57.374-58.62
      C0.363,25.362,26.608-0.325,58.639,0.019z"/>
    <path style="fill-rule:evenodd;clip-rule:evenodd;fill:#f5f5f5;" d="M58.542,9.019c27.06,0.291,48.761,22.463,48.471,49.524
      c-0.291,27.06-22.463,48.761-49.524,48.471C30.429,106.722,8.728,84.55,9.019,57.489C9.309,30.429,31.482,8.728,58.542,9.019z"/>
  </g>
  <g id="ICON-GROUP">
    <defs>
    <path id="SVGID_1_" d="M40.909,103.933C15.55,94.485,2.651,66.268,12.099,40.909c9.448-25.359,37.665-38.258,63.024-28.81
      c25.359,9.448,38.258,37.665,28.81,63.024C94.485,100.482,66.268,113.38,40.909,103.933z"/>
  </defs>
  <clipPath id="SVGID_2_">
    <use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
  </clipPath>
  <g id="ICONS" style="clip-path:url(#SVGID_2_);">
    <g>
      <animateTransform 
                        id="down" 
                        attributeName="transform" 
                        attributeType="XML" 
                        type="translate" 
                        from="0,0"
                        to="0, -95"
                        dur="0.475s" 
                        calcMode="spline"
                        keySplines=".25,.1,.25,1" 
                        keyTimes="0;1"
                        begin="0s; next.end"
                         /> 
        <animateTransform 
                        id="left"
                        attributeName="transform" 
                        attributeType="XML" 
                        type="translate" 
                        from="0, -95"
                        to="-95, -95"
                        dur="0.475s" 
                        calcMode="spline"
                        keySplines=".25,.1,.25,1" 
                        keyTimes="0;1"
                        begin="0s; down.end"/> 
              <animateTransform 
                        id="next"
                        attributeName="transform" 
                        attributeType="XML" 
                        type="translate" 
                        from="-95, -95"
                        to="-194, -95"
                        dur="0.475s" 
                        calcMode="spline"
                        keySplines=".25,.1,.25,1" 
                        keyTimes="0;1"
                        begin="0s; left.end"/> 
      <g id="ICON-PAINT">
        <path style="opacity:1.0;fill:#f52007;enable-background:new    ;" d="M50.621,63.108l-9.538-0.317c-3.192,0-5.788,2.471-5.788,5.508s2.596,5.508,5.81,5.508l9.59-0.32L61.438,93.41h5.583
  c0.938,0,1.7-0.762,1.7-1.701v-0.094l-5.381-18.557l9.304-0.379c0.733,0,1.445-0.094,2.124-0.279l3.774,6.41h3.616
  c0.796-0.002,1.445-0.65,1.447-1.447v-0.107l-2.817-8.615v-0.822l2.816-8.615v-0.106c0-0.388-0.151-0.751-0.423-1.023
  c-0.273-0.273-0.636-0.424-1.023-0.424l-3.615-0.001l-3.844,6.529c-0.659-0.173-1.342-0.26-2.027-0.26l-9.37-0.381l5.421-18.693
  V44.75c0-0.939-0.762-1.7-1.7-1.7h-5.583L50.621,63.108z M67.021,44.395c0.171,0,0.314,0.121,0.348,0.284l-5.838,20.132
  l11.112,0.452c0.75,0,1.473,0.116,2.15,0.348l0.519,0.176l4-6.793l2.846,0.001c0.034,0,0.058,0.016,0.071,0.03
  c0.005,0.004,0.01,0.009,0.014,0.016l-2.799,8.564v1.25l2.799,8.564c-0.019,0.028-0.05,0.047-0.086,0.047l-2.846-0.001l-3.935-6.681
  l-0.521,0.183c-0.692,0.243-1.436,0.367-2.238,0.368L61.57,71.785l5.799,19.996c-0.034,0.162-0.177,0.285-0.348,0.285h-4.78
  L51.482,72.114l-10.4,0.349c-2.45,0-4.442-1.869-4.442-4.164s1.993-4.164,4.42-4.164l10.35,0.346l10.833-20.086L67.021,44.395
  L67.021,44.395z"/>       
      </g>
      <g id="ICON-PAINT_1_">
        <path style="opacity:1.0;fill:#f52007;enable-background:new    ;" d="M245.163,158.633l-9.538-0.316c-3.192,0-5.788,2.471-5.788,5.508s2.596,5.508,5.811,5.508l9.59-0.32l10.742,19.924h5.583
  c0.938,0,1.7-0.762,1.7-1.701v-0.094l-5.382-18.557l9.304-0.379c0.733,0,1.445-0.094,2.125-0.279l3.773,6.41h3.615
  c0.797-0.002,1.445-0.65,1.447-1.447v-0.107l-2.816-8.615v-0.822l2.815-8.615v-0.107c0-0.387-0.151-0.75-0.423-1.023
  c-0.273-0.273-0.637-0.424-1.023-0.424h-3.615l-3.844,6.529c-0.658-0.174-1.342-0.26-2.027-0.26l-9.369-0.381l5.42-18.693v-0.094
  c0-0.939-0.762-1.701-1.699-1.701h-5.583L245.163,158.633z M261.563,139.92c0.171,0,0.314,0.121,0.348,0.285l-5.838,20.131
  l11.111,0.453c0.75,0,1.473,0.115,2.15,0.348l0.518,0.176l4-6.793h2.846c0.035,0,0.059,0.016,0.072,0.031
  c0.004,0.004,0.01,0.008,0.014,0.016l-2.799,8.564v1.25l2.799,8.564c-0.02,0.027-0.051,0.047-0.086,0.047l-2.846-0.002l-3.936-6.68
  l-0.521,0.182c-0.691,0.244-1.436,0.367-2.238,0.369l-11.045,0.449l5.799,19.996c-0.033,0.162-0.178,0.285-0.348,0.285h-4.781
  l-10.758-19.953l-10.4,0.35c-2.449,0-4.442-1.869-4.442-4.164s1.993-4.164,4.42-4.164l10.35,0.346l10.833-20.086H261.563
  L261.563,139.92z"/>
      </g>
      <g id="ICON-ALBUM_1_">
        <path style="opacity:1.0;fill:#f52007;enable-background:new    ;" d="M39.603,148.783l-9.538-0.316c-3.192,0-5.788,2.471-5.788,5.508s2.596,5.508,5.81,5.508l9.59-0.32l10.743,19.924h5.583
  c0.938,0,1.699-0.762,1.699-1.701v-0.094l-5.381-18.557l9.304-0.379c0.733,0,1.445-0.094,2.124-0.279l3.774,6.41h3.615
  c0.797-0.002,1.445-0.65,1.447-1.447v-0.107l-2.817-8.615v-0.822l2.816-8.615v-0.107c0-0.387-0.151-0.75-0.424-1.023
  s-0.636-0.424-1.022-0.424h-3.615l-3.844,6.529c-0.659-0.174-1.343-0.26-2.027-0.26l-9.37-0.381l5.421-18.693v-0.094
  c0-0.939-0.762-1.701-1.699-1.701H50.42L39.603,148.783z M56.002,130.07c0.172,0,0.314,0.121,0.349,0.285l-5.839,20.131
  l11.112,0.453c0.749,0,1.473,0.115,2.149,0.348l0.519,0.176l4-6.793h2.846c0.034,0,0.059,0.016,0.071,0.031
  c0.005,0.004,0.01,0.008,0.015,0.016l-2.8,8.564v1.25l2.8,8.564c-0.02,0.027-0.051,0.047-0.086,0.047l-2.846-0.002l-3.936-6.68
  l-0.521,0.182c-0.692,0.244-1.436,0.367-2.238,0.369l-11.046,0.449l5.8,19.996c-0.034,0.162-0.178,0.285-0.349,0.285h-4.78
  l-10.759-19.953l-10.399,0.35c-2.45,0-4.442-1.869-4.442-4.164s1.992-4.164,4.42-4.164l10.35,0.346l10.832-20.086H56.002
  L56.002,130.07z"/>        
      </g>
      <g id="ICON-DATE_1_">
        <path style="opacity:1.0;fill:#f52007;enable-background:new    ;" d="M147.186,152.262l-9.538-0.316c-3.192,0-5.788,2.471-5.788,5.508s2.596,5.508,5.811,5.508l9.59-0.32l10.742,19.924h5.583
  c0.938,0,1.7-0.762,1.7-1.701v-0.094l-5.382-18.557l9.304-0.379c0.733,0,1.445-0.094,2.125-0.279l3.773,6.41h3.615
  c0.797-0.002,1.445-0.65,1.447-1.447v-0.107l-2.816-8.615v-0.822l2.815-8.615v-0.107c0-0.387-0.151-0.75-0.423-1.023
  c-0.273-0.273-0.637-0.424-1.023-0.424h-3.615l-3.844,6.529c-0.658-0.174-1.342-0.26-2.027-0.26l-9.369-0.381l5.42-18.693v-0.094
  c0-0.939-0.762-1.701-1.699-1.701h-5.583L147.186,152.262z M163.586,133.549c0.171,0,0.314,0.121,0.348,0.285l-5.838,20.131
  l11.111,0.453c0.75,0,1.473,0.115,2.15,0.348l0.518,0.176l4-6.793h2.846c0.035,0,0.059,0.016,0.072,0.031
  c0.004,0.004,0.01,0.008,0.014,0.016l-2.799,8.564v1.25l2.799,8.564c-0.02,0.027-0.051,0.047-0.086,0.047l-2.846-0.002l-3.936-6.68
  l-0.521,0.182c-0.691,0.244-1.436,0.367-2.238,0.369l-11.045,0.449l5.799,19.996c-0.033,0.162-0.178,0.285-0.348,0.285h-4.781
  l-10.758-19.953l-10.4,0.35c-2.449,0-4.442-1.869-4.442-4.164s1.993-4.164,4.42-4.164l10.35,0.346l10.833-20.086H163.586
  L163.586,133.549z"/>        
      </g>
      </g>
    </g>

  </g> 
  <g id="RING-GROUP" >
      <animateTransform attributeName="transform" 
                        attributeType="XML" 
                        type="rotate" 
                        from="0 58.02 58.02" 
                        to="360 58.02 58.02" 
                        dur="1.75s" 
                        repeatCount="indefinite"
                        calcMode="spline"
                        keySplines="0.4, 0, 0.2, 1" 
                        keyTimes="0;1"/> 
    
      <path style="opacity:0.5;fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#f52007;stroke-width:9;stroke-linecap:round;stroke-miterlimit:10;" d="
      M32.557,105.36C15.569,96.206,4.093,78.183,4.273,57.545c0.152-17.39,8.547-32.779,21.444-42.49"/>

      <path style="opacity:0.5;fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#f52007;stroke-width:9;stroke-linecap:round;stroke-miterlimit:10;" d="
      M58.481,4.743c1.013,0.009,2.019,0.046,3.017,0.11 M86.788,102.859 M61.498,4.853c28.004,1.81,50.038,25.219,49.79,53.628
      c-0.163,18.656-9.897,34.989-24.5,44.378"/>
  </g>
  </svg>
</div>
            <div class="paraload">Please Wait Your Request Is Under Process...</br>
        Please Do Not Press Back Or Refresh</div>
            <div class="normal_load hide"></div>
            <div class="clearfix"></div>            <div class="progress flghtloas">
               <div class="progress-bar progress-bar-striped active" id="progressbar" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">Please Wait...</div>
            </div>
         </div>
      </div>
   </div>
</div>


<form id="form"  action="<?=$form_url?>" method="<?=$form_method?>"><?php
  foreach ( $form_params as $key => $val ) {
    ?><input type="hidden" name="<?=$key?>" value="<?=$val?>" /><?php
  }
  ?>
</form>
</body>
    <script src="<?php echo ASSETS;?>assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/general.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/hotel_search.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/jquery.jsort.0.4.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/jquery.nicescroll.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/hotel_search_opt.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/jquery.lazy.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/hotel_suggest.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/app.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/javascript.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/login.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/datepicker.js"></script>
    <script src="<?php echo ASSETS;?>assets/js/modernizr.custom.js"></script>

<script type="text/javascript">
$('.fulloading').show();
document.getElementById("form").submit();

</script>
