   <?php
$flight_datepicker = array(array('flight_datepicker1', FUTURE_DATE), array('flight_datepicker2', FUTURE_DATE));
//~ $this->current_page->set_datepicker($flight_datepicker);
//~ $this->current_page->auto_adjust_datepicker(array(array('flight_datepicker1', 'flight_datepicker2')));
$airline_list = $GLOBALS['CI']->db_cache_api->get_airline_code_list();


?>	
	<div id="Flight" class="tab-pane active">
							<form autocomplete="off" onSubmit="return validateform();" action="<?php echo base_url(); ?>flight/pre_flight_search" name="hotelSearchForm" id="hotelSearchForm">
								<div class="intabs">
								  <div class="waywy">
									<div class="smalway">
										<label class="wament hand-cursor active">
											<input class="hide" type="radio" name="trip_type" id="onew-trp" value="oneway" <?=(@$flight_search_params['trip_type'] == 'oneway' ? 'checked="checked"' : '')?> > One Way</label>
											<label class="wament hand-cursor">
											<input class="hide" type="radio" name="trip_type" id="rnd-trp" value="circle" <?=(@$flight_search_params['trip_type'] == 'circle' ? 'checked="checked"' : '')?>> Round Way</label>
											<label class="wament hand-cursor"><input class="hide" type="radio" name="trip_type" id="multi-trp" value="multicity" <?=(@$flight_search_params['trip_type'] == 'multicity' ? 'checked="checked"' : '')?> > Multi City</label></div>
								  <div class="clearfix"></div>
								  
								<?php if( $flight_search_params['trip_type'] == 'oneway' ||$flight_search_params['trip_type'] =='circle') { ?>
								  <div class="outsideserach" id="normal">
									  
									<div class="col-lg-4 col-md-6 col-sm-6 fiveh"> 
									  <div class="marginbotom10">
									  <span class="formlabel">From</span>
									  <div class="relativemask"> <span class="maskimg hfrom"></span>
										<input type="text" name="from" id="from" value="<?php echo @$flight_search_params['from'] ?>" placeholder="Airport City" class="fromflight ft" >
										<input class="hide loc_id_holder" name="from_loc_id" type="hidden" value="<?=@$flight_search_params['from_loc_id']?>">
									  </div>
									  </div>
									</div>
									
									<div class="col-lg-4 col-md-6 col-sm-6 fiveh"> 
									  <div class="marginbotom10">
									  <span class="formlabel">To</span>
									  <div class="relativemask"> <span class="maskimg hfrom"></span>
										<input type="text" name="to" id="to" value="<?php echo @$flight_search_params['to'] ?>" placeholder="Airport City" class="departflight ft">
										<input class="loc_id_holder" name="to_loc_id" type="hidden" value="<?=@$flight_search_params['to_loc_id']?>">
									  </div>
									  </div>
									</div>
									
								<div class="col-md-4 nopad">
									  <div class="col-xs-6 fiveh"> 
										<div class="marginbotom10">
										<span class="formlabel">Departure</span>
										<div class="relativemask"> <span class="maskimg caln"></span>
										  <input type="text"   id="st_date" name="depature" placeholder="Depature Date" value="<?php echo @$flight_search_params['depature'] ?>" class="forminput">
										</div>
										</div>
									  </div>
									  
									  <div class="col-xs-6 fiveh" id="returns"> 
										<div class="marginbotom10">
										<span class="formlabel">Return</span>
										<div class="relativemask"> <span class="maskimg caln"></span>
										  <input type="text" id="en_date" name="return" placeholder="Return Date" value="<?php echo @$flight_search_params['return'] ?>" class="forminput">
										</div>
										</div>
									  </div>
									  
									</div>
							 </div>
							 <?php }?>
							 
								<div class="clearfix"></div>
								   <?php
								  
								     for($multi =0; $multi < count($flight_search_params['depature']); $multi++ ){
									?>
									
									<div class="outsideserach" id="multi" >
									 <?php if($multi >= 1) { ?>		
										 <div id="segment_<?php echo $multi; ?>" class="segments">										
								  	
									  <?php } ?>						
									<div class="col-lg-4 col-md-6 col-sm-6 fiveh"> 
									  <div class="marginbotom10">
									  <span class="formlabel">From</span>
									  <div class="relativemask"> <span class="maskimg hfrom"></span>
										<input type="text" name="from[]" id="m_from1" value="<?php echo $flight_search_params['from'][$multi]; ?>" placeholder="Airport City" class="fromflight ft" >
										<input class="hide loc_id_holder" name="from_loc_id[]" type="hidden" value="<?php echo $flight_search_params['from_loc_id'][$multi]; ?>">
									  </div>
									  </div>
									</div>
									
									<div class="col-lg-4 col-md-6 col-sm-6 fiveh"> 
									  <div class="marginbotom10">
									  <span class="formlabel">To</span>
									  <div class="relativemask"> <span class="maskimg hfrom"></span>
										<input type="text" name="to[]" id="m_to1" value="<?php echo $flight_search_params['to'][$multi]; ?>" placeholder="Airport City" class="departflight ft">
										<input class="loc_id_holder" name="to_loc_id[]" type="hidden" value="<?php echo $flight_search_params['to_loc_id'][$multi]; ?>">
									  </div>
									  </div>
									</div>
									
									<div class="col-lg-4 col-md-5 nopad">
									  <div class="col-xs-8 fiveh"> 
										<div class="marginbotom10">
										<span class="formlabel">Departure</span>
										<div class="relativemask"> <span class="maskimg caln"></span>
										  <input type="text"   id="m_flight_datepicker<?php echo $multi+1; ?>" name="depature[]" placeholder="Depature Date" value="<?php echo $flight_search_params['depature'][$multi]; ?>" class="forminput">
										</div>
										</div>
									  </div>
									  <?php if($multi == 1) { ?>
									  <div id="a_btn">
									  <a style=""  id="add_city" onclick="clone_segment();"> <span class="fa fa-plus"></span> Add City</a>
									  </div>
									  <?php }elseif($multi > 1){  ?>
										   <a  id="remove_city" onclick="remove_segment(<?php echo $multi; ?>);"> <span class="fa fa-minus"></span> Remove</a>
										  <?php } ?>
								
								</div>
								
								<?php if($multi >= 1) { ?>												
								  	 </div>	
							   <?php } 
							   } ?>		
							   
							   <div class="interst_clone_wrapper">
							   
							   </div>	
							   		
							  </div>
								
								  
									
								 <div class="col-md-4 nopad">
									  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fiveh"> 
										<div class="marginbotom10">
										<span class="formlabel">Adult</span>
										<div class="selectedwrap">
										  <select name="adult" id="adult" class="mySelectBoxClass flyinputsnor">
											<option value="1" <?php if($flight_search_params['adult_config'] == 1) echo "selected"; ?> >1</option>
											<option value="2" <?php if($flight_search_params['adult_config'] == 2) echo "selected"; ?> >2</option>
											<option value="3" <?php if($flight_search_params['adult_config'] == 3) echo "selected"; ?>>3</option>
											<option value="4" <?php if($flight_search_params['adult_config'] == 4) echo "selected"; ?>>4</option>
										  </select>
										</div>
										</div>
									  </div>
									  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fiveh"> 
										<div class="marginbotom10">
										<span class="formlabel">Child<strong>(2-11 yrs)</strong></span>
										<div class="selectedwrap">
										  <select id="child" name="child" class="mySelectBoxClass flyinputsnor">
											 <option value="0" <?php if($flight_search_params['child_config'] == 0) echo "selected"; ?> >0</option>
											<option value="1" <?php if($flight_search_params['child_config'] == 1) echo "selected"; ?>  >1</option>
											<option value="2" <?php if($flight_search_params['child_config'] == 2) echo "selected"; ?> >2</option>
											<option value="3" <?php if($flight_search_params['child_config'] == 3) echo "selected"; ?>  >3</option>
											<option value="4" <?php if($flight_search_params['child_config'] == 4) echo "selected"; ?> >4</option>
										  </select>
										</div>
										</div>
									  </div>
									  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fiveh"> 
										<div class="marginbotom10">
										<span class="formlabel">Infant<strong>(0-2 yrs)</strong></span>
										<div class="selectedwrap">
										  <select name="infant"  id="infant" class="mySelectBoxClass flyinputsnor">
											<option value="0" <?php if($flight_search_params['infant_config'] == 0) echo "selected"; ?> >0</option>
											<option value="1" <?php if($flight_search_params['infant_config'] == 1) echo "selected"; ?>  >1</option>
											<option value="2" <?php if($flight_search_params['infant_config'] == 2) echo "selected"; ?> >2</option>
											<option value="3" <?php if($flight_search_params['infant_config'] == 3) echo "selected"; ?>  >3</option>
											<option value="4" <?php if($flight_search_params['infant_config'] == 4) echo "selected"; ?> >4</option>
										  </select>
										</div>
										</div>
									  </div>
									</div>
									<!--<div class="clearfix"></div>-->
									<div class="col-md-6 nopad">
									  <div class="col-xs-6 fiveh"> 
										<div class="marginbotom10">
										<span class="formlabel">Class</span>
										<div class="selectedwrap">
										  <select class="mySelectBoxClass flyinputsnor" name="v_class">
											<option>All</option>
											<option>Economy With Restrictions</option>
											<option>Economy Without Restrictions</option>
											<option>Economy Premium</option>
											<option>Business</option>
											<option>First</option>
										  </select>
										</div>
									  </div>
									  </div>
									</div>
									
									
                                  
									<div class="col-md-2">
									  <div class="formsubmit">
										    <input type="hidden" autocomplete="off" name="carrier[]" id="carrier" value="">
									<input type="hidden"   id="search_flight" name="search_flight" placeholder="Depature Date" value="search" class="forminput">
									<!-- <input type="hidden" autocomplete="off" name="v_class" id="class" value="<?php echo "All";?>"> -->
										<button class="srchbutn comncolor">Search Flights <span class="srcharow"></span></button>
									  </div>
									</div>
								</div>
									</div>
						   </form>
						
							</div>
							
<?php echo $this->load->view('dashboard/flight_search_prob'); ?>
	
