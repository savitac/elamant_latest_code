<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title><?php echo $this->session->userdata('company_name')?></title>
        <?php echo $this->load->view("core/load_css"); ?>
        <link href="<?php echo ASSETS; ?>assets/css/flight_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script>
		<script src="<?php echo ASSETS; ?>assets/js/jquery-ui.min.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/general.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/css/owl.carousel.min.css"></script>
		<script src="<?php echo ASSETS; ?>assets/js/owl.carousel.min.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/flight_search_crs.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/provablib.js"></script>
		<script src="<?php echo ASSETS; ?>assets/js/jquery.jsort.0.4.min.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/pax_count.js"></script>  
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/map.js"></script>
    </head> 
    <body>
      <?php 
        if($this->session->userdata('user_logged_in') == 0) {  
			echo $this->load->view('core/header'); 
		} else {
			echo $this->load->view('dashboard/top'); 	 
		}
	 ?>
	
<?php
foreach ($active_booking_source as $t_k => $t_v) {
	$active_source[] = $t_v['source_id'];
}
$active_source = json_encode($active_source);
?>
<script>
var app_base_url = "<?php echo base_url(); ?>";
var load_flights = function(){
	$.ajax({
		type: 'GET',
		url: app_base_url+'index.php/ajax/flight_crs_list?booking_source=<?=$active_booking_source[0]['source_id']?>&search_id=<?=$flight_search_params['search_id']?>&op=load',
			async: true,
			cache: true,
			dataType: 'json',
			success: function(res) {
			var dui;
			var r = res;
			dui = setInterval(function(){
			if (typeof(process_result_update) != "undefined" && $.isFunction(process_result_update) == true) {
				clearInterval(dui);
				process_result_update(r);
			}
			}, 1);
		}
	});
}
load_flights();
</script>
<span class="hide">
	<input type="hidden" id="pri_preferred_currency" value='<?php echo BASE_CURRENCY ?>'>
	<input type="hidden" id="pri_trip_type" value='<?=$is_domestic_one_way_flight?>'>
	<input type="hidden" id="pri_active_source" value='<?=$active_source?>'>
	<input type="hidden" id="pri_search_id" value='<?=$flight_search_params['search_id']?>'>
	<input type="hidden" id="pri_airline_lg_path" value='<?=ASSETS.'assets/images/airline_logo/'?>'>
	<input type="hidden" id="pri_search_params" value='<?=json_encode($flight_search_params)?>'>
	<input type="hidden" id="pri_def_curr" value='<?php echo BASE_CURRENCY ?> '>
</span>
<?php
$data['trip_details'] = $flight_search_params;
$data['airline_list'] = $airline_list;//Jaganath
$mini_loading_image = '<div class="text-center loader-image"><img src="'.ASSETS.'assets/images/loader_v3.gif" alt="Loading........"/></div>';
$loading_image = '<div class="text-center loader-image"><img src="'.ASSETS.'assets/images/loader_v1.gif" alt="Loading........"/></div>';
$flight_o_direction_icon = '<img src="'.ASSETS.'assets/images/icons/flight-search-result-up-icon.png" alt="Flight Search Result Up Icon">';
echo  $GLOBALS['CI']->load->view('flight/search_panel_summary_crs', true);
?>
<!-- Page Scripts -->
<section class="contentsec">  
	<div class="container"  id="page-parent">
		<?php echo $GLOBALS['CI']->load->view('share/loader/flight_result_pre_loader',$data, true);?>
		<div class="resultalls open">
			<div class="coleft">
				<div class="boxtop">
					<div class="filtersho">
						<div class="avlhtls"><strong id="total_records"> </strong> flight found
						</div>
					</div>
				</div>
				<div class="flteboxwrp">
					
					<!-- Refine Search Filters Start -->
					<div class="fltrboxin">
						<div class="celsrch norfilterr">
							<div class="row">
								<a class="pull-right placenamefil" id="reset_filters">RESET ALL</a>
								<div class="resultfilt">Filter Results</div>
							</div>
							<div class="rangebox">
								<div data-target="#collapse501" data-toggle="collapse" class="ranghead collapsed ">
								Price
								</div>
								<div id="collapse501" class="collapse in">
									<div class="price_slider1">
										<div id="core_min_max_slider_values" class="hide">
											<input type="hiden" id="core_minimum_range_value" value="">
											<input type="hiden" id="core_maximum_range_value" value="">
										</div>
										<p id="amount" class="level"></p>
										<div id="slider-range" class="" aria-disabled="false"></div>
									</div>
								</div>
							</div>
							<!-- <div class="septor"></div> -->
							<div class="rangebox">
								<div data-target="#collapse502" data-toggle="collapse" class="ranghead collapsed " type="button">
								No. of Stops
								</div>
								<div id="collapse502" class="collapse in">
									<div class="boxins marret" id="stopCountWrapper">
										<a class="stopone toglefil stop-wrapper">
											<input type="checkbox" class="hidecheck stopcount" value="1"></input>
											<div class="starin">
												<div class="stopbig"> 0 <span class="stopsml">stop</span></div>
												<span class="htlcount min-price">-</span>
											</div>
										</a>
										<a class="stopone toglefil stop-wrapper">
											<input type="checkbox" class="hidecheck stopcount" value="2"></input>
											<div class="starin">
												<div class="stopbig"> 1 <span class="stopsml">stop</span></div>
												<span class="htlcount min-price">-</span>
											</div>
										</a>
										<a class="stopone toglefil stop-wrapper">
											<input type="checkbox" class="stopcount hidecheck" value="3"></input>
											<div class="starin">
												<div class="stopbig"> 1+ <span class="stopsml">stop</span></div>
												<span class="htlcount min-price">-</span>
											</div>
										</a>
									</div>
								</div>
							</div>
							<!-- <div class="septor"></div> -->
							<div class="rangebox">
								<div data-target="#collapse503" data-toggle="collapse" class="ranghead collapsed" type="button">
								Departure Time
								</div>
								<div id="collapse503" class="collapse in">
									<div class="boxins marret" id="departureTimeWrapper">
										<a class="timone toglefil time-wrapper">
											<input type="checkbox" class="time-category hidecheck" value="1"></input>
											<div class="starin">
												<div class="flitsprt mng1"></div>
												<span class="htlcount">12-6AM</span>
											</div>
										</a>
										<a class="timone toglefil time-wrapper">
											<input type="checkbox" class="time-category hidecheck" value="2"></input>
											<div class="starin">
												<div class="flitsprt mng2"></div>
												<span class="htlcount">6-12PM</span>
											</div>
										</a>
										<a class="timone toglefil time-wrapper">
											<input type="checkbox" class="time-category hidecheck" value="3"></input>
											<div class="starin">
												<div class="flitsprt mng3"></div>
												<span class="htlcount">12-6PM</span>
											</div>
										</a>
										<a class="timone toglefil time-wrapper">
											<input type="checkbox" class="hidecheck time-category" value="4"></input>
											<div class="starin">
												<div class="flitsprt mng4"></div>
												<span class="htlcount">6-12AM</span>
											</div>
										</a>
									</div>
								</div>
							</div>
							<!-- <div class="septor"></div> -->
							<div class="rangebox">
								<div data-target="#collapse504" data-toggle="collapse" class="ranghead collapsed" type="button">
								Arrival Time
								</div>
								<div id="collapse504" class="collapse in">
									<div class="boxins marret" id="arrivalTimeWrapper">
										<a class="timone toglefil time-wrapper">
											<input type="checkbox" class="time-category hidecheck" value="1"></input>
											<div class="starin">
												<div class="flitsprt mng1"></div>
												<span class="htlcount">12-6AM</span>
											</div>
										</a>
										<a class="timone toglefil time-wrapper">
											<input type="checkbox" class="time-category hidecheck" value="2"></input>
											<div class="starin">
												<div class="flitsprt mng2"></div>
												<span class="htlcount">6-12PM</span>
											</div>
										</a>
										<a class="timone toglefil time-wrapper">
											<input type="checkbox" class="time-category hidecheck" value="3"></input>
											<div class="starin">
												<div class="flitsprt mng3"></div>
												<span class="htlcount">12-6PM</span>
											</div>
										</a>
										<a class="timone toglefil time-wrapper">
											<input type="checkbox" class="time-category hidecheck" value="4"></input>
											<div class="starin">
												<div class="flitsprt mng4"></div>
												<span class="htlcount">6-12AM</span>
											</div>
										</a>
									</div>
								</div>
							</div>
							<!-- <div class="septor"></div> -->
							<div class="rangebox">
								<div data-target="#collapse505" data-toggle="collapse" class="ranghead collapsed" type="button">
								Airlines
								</div>
								<div id="collapse505" class="collapse in">
									<div class="boxins" id="allairlines">
									</div>
								</div>
							</div>
							<!-- <div class="septor"></div> -->
						</div>
					</div>
				</div>
				<!-- Refine Search Filters End -->
			</div>
			<div class="colrit">
				<div class="insidebosc">
					<!-- Fare Calander -->
					<?php if ($is_domestic_one_way_flight == true) : ?>
					<div class="calandcal" id="fare_calendar_wrapper">
						<div class="col-xs-12 nopad">
							<div class="farenewcal">
								<div class="matrx">
									<div id="farecal" class="owl-carousel matrixcarsl">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<?php endif; // Checking if one way domestic?>
					<!-- Fare Calander End -->
					<!-- Airline Slider Start -->
					<div class="airlinrmatrix" id="clone-list-container">
						<div class="inside_shadow_airline">
							<div class="linefstr">
								<div class="airlineall">All Airline</div>
							</div>
							<div class="linescndr">
								<div class="matrx">
									<div id="arlinemtrx" class="owl-carousel matrixcarsl">
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Airline Slider End -->
					<div class="clearfix"></div>
					<!--  Current Selection  -->
					<div class="fixincrmnt hide" id="multi-flight-summary-container">
						<div class="insidecurent">
							<div class="col-xs-9 nopad">
								<div class="col-xs-6 nopad">
									<div class="selctarln colorretn">
										<div class="col-xs-2 nopad flightimage">
											<div class="fligthsmll">
												<img class="departure-flight-icon" src="<?=ASSETS?>assets/images/airline.png" alt="" />
											</div>
											<div class="airlinename departure-flight-name">Please Select</div>
										</div>
										<div class="col-xs-10 nopad listfull">
											<div class="sidenamedesc">
												<div class="celhtl width80">
													<div class="waymensn">
														<div class="flitruo">
															<div class="outbound-details">
															</div>
															<div class="detlnavi outbound-timing-details">
																<div class="col-xs-4 padflt widfty">
																	<span class="timlbl departure"></span>
																</div>
																<div class="col-xs-4 padflt nonefity">
																	<div class="lyovrtime">
																		<span class="flect duration"></span>
																		<span class="flect stop-count"></span>
																	</div>
																</div>
																<div class="col-xs-4 padflt widfty">
																	<span class="timlbl arrival text_algn_rit"></span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-6 nopad">
									<div class="selctarln cloroutbnd">
										<div class="col-xs-2 nopad flightimage">
											<div class="fligthsmll">
												<img class="arrival-flight-icon" src="<?=ASSETS?>assets/images/airline.png" alt="" />
											</div>
											<div class="airlinename arrival-flight-name"></div>
										</div>
										<div class="col-xs-10 nopad listfull">
											<div class="sidenamedesc">
												<div class="celhtl width80">
													<div class="waymensn">
														<div class="flitruo">
															<div class="inbound-details">
															</div>
															<div class="detlnavi inbound-timing-details">
																<div class="col-xs-4 padflt widfty">
																	<span class="timlbl departure"></span>
																</div>
																<div class="col-xs-4 padflt nonefity">
																	<div class="lyovrtime">
																		<span class="flect duration"></span>
																		<span class="flect stop-count"></span>
																	</div>
																</div>
																<div class="col-xs-4 padflt widfty">
																	<span class="timlbl arrival text_algn_rit"></span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-3 nopad">
								<div class="sidepricewrp">
									<div class="col-xs-12 nopad">
										<div class="sidepricebig">
											<strong class="currency"></strong> <span class="f-p"></span>
										</div>
									</div>
									<div class="col-xs-8 nopad pull-right">
										<div class="bookbtn">
											<input type="hidden" id="flight-from-price" value="0">
											<input type="hidden" id="flight-to-price" value="0">
											<form id="multi-flight-form" action="" method="POST" target="_blank">
												<div class="hide" id="trip-way-wrapper"></div>
												<button class="btn-flat booknow" type="submit" id="multi-flight-booking-btn">Book</button>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--  Current Selection  End  -->
					<div class="clearfix"></div>
					<div class="filter_tab fa fa-filter"></div>
					<?php $sorting_list = '';?>
					<div class="filterforall addtwofilter" id="top-sort-list-wrapper">
						<div class="topmisty" id="top-sort-list-1">
							<div class="col-xs-12 nopad">
								<div class="divinsidefltr">
									<div class="insidemyt col-xs-10 nopad">
										<?php
											$sorting_list .= '<ul class="sortul">';
												$sorting_list .= '<li class="sortli hide_lines">';
												$sorting_list .= '<a class="sorta name-l-2-h loader asc"><i class="fa fa-plane"></i> <strong>Airline</strong></a>';
												$sorting_list .= '<a class="sorta name-h-2-l hide loader des"><i class="fa fa-plane"></i> <strong>Airline</strong></a>';
												$sorting_list .= '</li>';
												$sorting_list .= '<li class="sortli">';
												$sorting_list .= '<a class="sorta departure-l-2-h loader asc"><i class="fa fa-calendar"></i> <strong>Depart</strong></a>';
												$sorting_list .= '<a class="sorta departure-h-2-l hide loader des"><i class="fa fa-calendar"></i> <strong>Depart</strong></a>';
												$sorting_list .= '</li>';
												$sorting_list .= '<li class="sortli">';
													$sorting_list .= '<a class="sorta arrival-l-2-h loader asc"><i class="fa fa-calendar"></i> <strong>Arrive</strong></a>';
													$sorting_list .= '<a class="sorta arrival-h-2-l hide loader des"><i class="fa fa-calendar"></i> <strong>Arrive</strong></a>';
												$sorting_list .= '</li>';
												$sorting_list .= '<li class="sortli hide_lines">';
													$sorting_list .= '<a class="sorta duration-l-2-h loader asc"><i class="fa fa-clock-o"></i> <strong>Duration</strong></a>';
													$sorting_list .= '<a class="sorta duration-h-2-l hide loader des"><i class="fa fa-clock-o"></i> <strong>Duration</strong></a>';
												$sorting_list .= '</li>';
												$sorting_list .= '<li class="sortli">';
													$sorting_list .= '<a class="sorta price-l-2-h loader asc"><i class="fa fa-tag"></i> <strong>Price</strong></a>';
													$sorting_list .= '<a class="sorta price-h-2-l hide loader des"><i class="fa fa-tag"></i> <strong>Price</strong></a>';
												$sorting_list .= '</li>';
											$sorting_list .= '</ul>';
											echo $sorting_list;
											?>
									</div>
								</div>
							</div>
						</div>
						<div class="topmisty" id="top-sort-list-2">
							<div class="col-xs-10 nopad divinsidefltr">
								<div class="insidemyt">
									<?=$sorting_list?>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<!-- FLIGHT SEARCH RESULT START -->
					<div  class="allresult" id="flight_search_result">
					</div>
					<div  class="" id="empty_flight_search_result" style="display:none">
						<div class="noresultfnd">
							<div class="imagenofnd"><img src="<?php echo ASSETS; ?>assets/images/empty.jpg" alt="Empty" /></div>
							<div class="lablfnd">No Result Found!!!</div>
						</div>
					</div>
					<!-- FLIGHT SEARCH RESULT END -->
				</div>
			</div>
		</div>
	</div>
	<div id="empty-search-result" class="jumbotron container" style="display:none">
		<h1><i class="fa fa-plane"></i> Oops!</h1>
		<p>No flights were found for this route today.</p>
		<p>
			Search results change daily based on availability.If you have an urgent requirement, please get in touch with our call center using the contact details mentioned on the home page. They will assist you to the best of their ability.
		</p>
	</div>

</section>



<?php echo  $this->load->view('core/bottom_footer'); ?>
 
</body>
<script>
	$(document).ready(function() {
		//************************** **********/
		//airline price sort
		<?php
		$i=1;
		for ($i=1; $i<=2; $i++) ://Multiple Filters Need to be loaded - Arjun
		?>
		$("#top-sort-list-<?=$i?> .price-l-2-h").click(function() {
			$(this).addClass('hide');
			$('#top-sort-list-<?=$i?> .price-h-2-l').removeClass('hide');
			$("#flight_search_result #t-w-i-<?=$i?>").jSort({
				sort_by: '.f-p:first',
				item: '.r-r-i',
				order: 'asc',
				is_num: true
			});
		});
		$("#top-sort-list-<?=$i?> .price-h-2-l").click(function() {
			$(this).addClass('hide');
			$('#top-sort-list-<?=$i?> .price-l-2-h').removeClass('hide');
			$("#flight_search_result #t-w-i-<?=$i?>").jSort({
				sort_by: '.f-p:first',
				item: '.r-r-i',
				order: 'desc',
				is_num: true
			});
		});
		//airline name sort
		$("#top-sort-list-<?=$i?> .name-l-2-h").click(function() {
			$(this).addClass('hide');
			$('#top-sort-list-<?=$i?> .name-h-2-l').removeClass('hide');
			$("#flight_search_result #t-w-i-<?=$i?>").jSort({
				sort_by: '.a-n:first',
				item: '.r-r-i',
				order: 'asc',
				is_num: false
			});
		});
		$("#top-sort-list-<?=$i?> .name-h-2-l").click(function() {
			$(this).addClass('hide');
			$('#top-sort-list-<?=$i?> .name-l-2-h').removeClass('hide');
			$("#flight_search_result #t-w-i-<?=$i?>").jSort({
				sort_by: '.a-n:first',
				item: '.r-r-i',
				order: 'desc',
				is_num: false
			});
		});
		//duration sort
		$("#top-sort-list-<?=$i?> .duration-l-2-h").click(function() {
			$(this).addClass('hide');
			$('#top-sort-list-<?=$i?> .duration-h-2-l').removeClass('hide');
			$("#flight_search_result #t-w-i-<?=$i?>").jSort({
				sort_by: '.f-d:first',
				item: '.r-r-i',
				order: 'asc',
				is_num: true
			});
		});
		$("#top-sort-list-<?=$i?> .duration-h-2-l").click(function() {
			$(this).addClass('hide');
			$('#top-sort-list-<?=$i?> .duration-l-2-h').removeClass('hide');
			$("#flight_search_result #t-w-i-<?=$i?>").jSort({
				sort_by: '.f-d:first',
				item: '.r-r-i',
				order: 'desc',
				is_num: true
			});
		});
		//departure name sort
		$("#top-sort-list-<?=$i?> .departure-l-2-h").click(function() {
			$(this).addClass('hide');
			$('#top-sort-list-<?=$i?> .departure-h-2-l').removeClass('hide');
			$("#flight_search_result #t-w-i-<?=$i?>").jSort({
				sort_by: '.fdtv:first',
				item: '.r-r-i',
				order: 'asc',
				is_num: true
			});
		});
		$("#top-sort-list-<?=$i?> .departure-h-2-l").click(function() {
			$(this).addClass('hide');
			$('#top-sort-list-<?=$i?> .departure-l-2-h').removeClass('hide');
			$("#flight_search_result #t-w-i-<?=$i?>").jSort({
				sort_by: '.fdtv:first',
				item: '.r-r-i',
				order: 'desc',
				is_num: true
			});
		});
		//arrival sort
		$("#top-sort-list-<?=$i?> .arrival-l-2-h").click(function() {
			$(this).addClass('hide');
			$('#top-sort-list-<?=$i?> .arrival-h-2-l').removeClass('hide');
			$("#flight_search_result #t-w-i-<?=$i?>").jSort({
				sort_by: '.fatv:first',
				item: '.r-r-i',
				order: 'asc',
				is_num: true
			});
		});
		$("#top-sort-list-<?=$i?> .arrival-h-2-l").click(function() {
			$(this).addClass('hide');
			$('#top-sort-list-<?=$i?> .arrival-l-2-h').removeClass('hide');
			$("#flight_search_result #t-w-i-<?=$i?>").jSort({
				sort_by: '.fatv:first',
				item: '.r-r-i',
				order: 'desc',
				is_num: true
			});
		});
		<?php
		endfor;
		//End Multiple Filter Looping - Arjun
		?>
	});
</script>
