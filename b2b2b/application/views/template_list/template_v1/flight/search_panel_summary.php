
<main class="allpagewrp top80">
	 <div class="newmodify">
		<div class="modnew_s">
			<div class="contentsdw">
				<div class="panel_ds">
					<div class="container">
						<div class="col-xs-5 p-tb-5">
						<div class="col-sm-6 col-xs-6 p-tb-5 text-center b-r rslt_iconcntr">
							<?php if($flight_search_params['trip_type']=='oneway' || $flight_search_params['trip_type'] == 'circle' ){
								 ?>
							<div class="pad_ten">
								<img src="<?php echo ASSETS; ?>assets/images/icons/flight-search-result-up-icon.png" alt="Flight Search Result Up Icon">
								<h3 class="contryname"><?php echo $flight_search_params['from'] ?></h3>
							</div>
					         <?php	}else {?>
							<div class="pad_ten">
								<img src="<?php echo ASSETS; ?>assets/images/icons/flight-search-result-up-icon.png" alt="Flight Search Result Up Icon">
								<h3 class="contryname"><?php echo $flight_search_params['from'][0] ?></h3>
							</div>
							<?php }?>
						</div>
						
						<div class="col-sm-6 col-xs-6 p-tb-5 text-center b-r rslt_iconcntr">
							
							<?php if($flight_search_params['trip_type']=='oneway' || $flight_search_params['trip_type'] == 'circle' ){
								 ?>
							<div class="pad_ten">
								<img src="<?php echo ASSETS; ?>assets/images/icons/flight-search-result-down-icon.png" alt="Flight Search Result Up Icon">
								<h3 class="contryname"><?php echo $flight_search_params['to'] ?></h3>
							</div>
					         <?php	}else {?>
								<div class="pad_ten">
								<img src="<?php echo ASSETS; ?>assets/images/icons/flight-search-result-down-icon.png" alt="Flight Search Result Up Icon">
								<h3 class="contryname"><?php echo end($flight_search_params['to'])?></h3>
							</div>					 
							 <?php }?>
						</div>
						</div>
						<!-- <div class="clearfix visible-sm-block"></div> -->
						<div class="col-xs-7 p-tb-5">
						<div class="col-md-3 col-sm-6 col-xs-6 p-tb-5 b-r">
							<div class="pad_ten">
								<h2 class="boxlabl">Journey</h2>
								<?php
								$date_tag = '';
								if($flight_search_params['trip_type']=='oneway'){
									echo '<h3 class="h5">OneWay Trip</h3>';
									$date_tag .= '<p class="h6 m-b-0">'.date('jS \ M Y',strtotime($flight_search_params['depature']));
								}elseif($flight_search_params['trip_type']=='circle'){
									echo '<h3 class="h5">Round Trip</h3>';
									$date_tag .= date('jS \ M Y',strtotime($flight_search_params['depature'])).'- '.
									date('jS \ M Y',strtotime($flight_search_params['return']));
									//$date_tag .= '- '.date('jS \ M Y',strtotime($flight_search_params['return']));
								}else{
									echo '<h3 class="h5">Multicity</h3>';
									$date_tag .= date('jS \ M Y',strtotime($flight_search_params['depature'][0])).'- '.
									date('jS \ M Y',strtotime(end($flight_search_params['depature'])));
									
								}
								$date_tag .= '</p>';
								echo $date_tag;
								$total_pax = ($flight_search_params['adult_config'] + $flight_search_params['child_config'] + $flight_search_params['infant_config']);
								?>
							</div>
						</div>
						<!-- <div class="clearfix visible-md-block"></div> -->
						<div class="col-md-3 col-sm-6 col-xs-6 p-tb-5 b-r">
							<div class="pad_ten">
								<h2 class="boxlabl">Class</h2>
								<h3 class="h5"><?php echo $flight_search_params['v_class'] ?></h3>
							</div>
						</div>
						<!-- <div class="clearfix visible-sm-block"></div> -->
						<div class="col-md-3 col-sm-6 col-xs-12 p-tb-5 b-r">
							<div class="pad_ten">
								<h2 class="boxlabl">Passengers</h2>
								<div class="btn-toolbar" role="toolbar">
									<div class="btn-group btn-group-xs" role="group">
										<button type="button" class="btn btn-default b-r-0 adulticn">
											<img src="<?php echo ASSETS; ?>assets/images/icons/male-icon.png" alt="Male Icon"> <?php echo $flight_search_params['adult_config']; ?>
										</button>
									</div>
									<div class="btn-group btn-group-xs" role="group">
										<button type="button" class="btn btn-default b-r-0 adulticn">
											<img src="<?php echo ASSETS; ?>assets/images/icons/child-icon.png" alt="Child Icon"> <?php echo $flight_search_params['child_config']; ?>
										</button>
									</div>
									<div class="btn-group btn-group-xs" role="group">
										<button type="button" class="btn btn-default b-r-0 adulticn">
											<img src="<?php echo ASSETS; ?>assets/images/icons/infant-icon.png" alt="Infant Icon"> <?php echo $flight_search_params['infant_config']; ?>
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 p-tb-5">
							<div class="pad_ten nopadL">
								<button type="button" class="modifysrch flight_search_form" data-toggle="collapse" href="#flight_search_form"><strong>Modify Search </strong><span class="down_caret"></span></button>
							</div>	
						</div>
					</div>
					</div>
					<div class="clearfix"></div>
					<div class="modify_search_wrap">
						<div class="container">
							<div id="flight_search_form" class="collapse">
								<div class="insplarea">
									<div class="panel_body">
										<?php echo $GLOBALS['CI']->load->view('dashboard/flight_search') ?>  
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
			</div>
		</div>
	</div>
</main>
