<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo  $this->session->userdata('company_name');?></title>
        <?php echo $this->load->view("core/load_css"); ?>
        <link href="<?php echo ASSETS; ?>assets/css/index.css" rel="stylesheet">
		<link href="<?php echo ASSETS; ?>assets/css/pre_booking.css" rel="stylesheet">
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery-1.11.0.js">	
		</script>
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/provablib.js">	
		</script>
    </head>
    <body>
<?php
    if($this->session->userdata('user_logged_in') == 0) { 
			echo $this->load->view('core/header'); 
		} else {
			echo $this->load->view('dashboard/top'); 	 
		}
	// debug($Host_token_data);exit;
	include_once 'process_tbo_response.php';
	//$template_images = $GLOBALS['CI']->template->template_images();
	//Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('provablib.js'), 'defer' => 'defer');
	//Flight Booking Summary

	//debug($new_price_Changed);exit;
	$pricing_xml = $pricing_xml; 


// Added By bhola

// Calculation Admin markup by bhola
		$FareDetails = $pre_booking_summery['FareDetails']['b2c_PriceDetails'];
		//$admin_markup_Fare = '';
       /*$admin_markup_Fare = $this->flight_model->travelport_markup($FareDetails['Admin_Markup']['markup_type'], $FareDetails['Admin_Markup']['markup_val'], $FareDetails['TotalFare'], $FareDetails['Admin_Markup']['total_pax'],$FareDetails['Admin_Markup']['currency_val']);
       debug($admin_markup_Fare);die();*/
// Calculation Admin markup by bhola

// End Added By bhola
    $segment_solution_xml = $pre_booking_summery['segment_solution_xml'];
	#debug($segment_solution_xml); die;
	$PassengerFareBreakdown = @$pre_booking_summery['PassengerFareBreakdown'];
	$SegmentDetails = $pre_booking_summery['SegmentDetails'];
	$SegmentSummary = $pre_booking_summery['SegmentSummary'];
	/********************************* Convenience Fees *********************************/
	$flight_total_amount = $FareDetails['TotalFare']+$convenience_fees;
	/********************************* Convenience Fees *********************************/

	/********************************* Admin Markup Fees *********************************/
	
	/********************************* Admin Markup Fees *********************************/
	
	$currency_symbol = $FareDetails['CurrencySymbol'];
	$is_domestic = $search_data['is_domestic'];
	//Segment Summary and Details
	$flight_segment_details =  flight_segment_details($SegmentDetails, $SegmentSummary, $segment_solution_xml);
	if ($is_domestic != true) {
		$pass_mand = '<sup class="text-danger">*</sup>';
		$pass_req = 'required="required"';
	} else {
		$pass_mand = '';
		$pass_req = '';
	}
	$mandatory_filed_marker = '<sup class="text-danger">*</sup>';
	//Jaganath
	$is_domestic_flight = $search_data['is_domestic_flight'];
	if($is_domestic_flight) {
		$temp_passport_expiry_date = date('Y-m-d', strtotime('+5 years'));
		$static_passport_details = array();
		$static_passport_details['passenger_passport_expiry_day'] = date('d', strtotime($temp_passport_expiry_date));
		$static_passport_details['passenger_passport_expiry_month'] = date('m', strtotime($temp_passport_expiry_date));
		$static_passport_details['passenger_passport_expiry_year'] = date('Y', strtotime($temp_passport_expiry_date));
	}
	if(is_logged_in_user()) {
		$review_active_class = ' success ';
		$review_tab_details_class = '';
		$review_tab_class = ' inactive_review_tab_marker ';
		$travellers_active_class = ' active ';
		$travellers_tab_details_class = ' gohel ';
		$travellers_tab_class = ' travellers_tab_marker ';
	} else {
		$review_active_class = ' active ';
		$review_tab_details_class = ' gohel ';
		$review_tab_class = ' review_tab_marker ';
		$travellers_active_class = '';
		$travellers_tab_details_class = '';
		$travellers_tab_class = ' inactive_travellers_tab_marker ';
	}
?>
<style>
	.topssec::after{display:none;}
</style>
<div class="fldealsec">
	<div class="container">
		<div class="tabcontnue">
			<div class="col-xs-4 nopadding">
				<div class="rondsts <?=$review_active_class?>">
					<a class="taba core_review_tab <?=$review_tab_class?>" id="stepbk1">
						<div class="iconstatus fa fa-eye"></div>
						<div class="stausline"><?php echo ('Review'); ?> TP </div>
					</a>
				</div>
			</div>
			<div class="col-xs-4 nopadding">
				<div class="rondsts <?=$travellers_active_class?>">
					<a class="taba core_travellers_tab <?=$travellers_tab_class?>" id="stepbk2">
						<div class="iconstatus fa fa-group"></div>
						<div class="stausline"><?php echo ('Travellers'); ?></div>
					</a>
				</div>
			</div>
			<div class="col-xs-4 nopadding">
				<div class="rondsts">
					<a class="taba" id="stepbk3">
						<div class="iconstatus fa fa-money"></div>
						<div class="stausline"><?php echo ('Payments'); ?></div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="alldownsectn">
	<div class="container">
		<?php if($is_price_Changed == true) {?>
			<div class="farehd arimobold">
				<span class="text-danger">* <?php echo ('Price has been changed from supplier end'); ?></span>
			</div>
		<?php } ?>
		<div class="ovrgo">
			<div class="bktab1 xlbox <?=$review_tab_details_class?> ">
				<!-- Jaganath - Fare Summery -->
				<div class="col-xs-4 nopadding rit_summery pull-right">
				<!-- Modified By Bhola -->
					<?php echo get_fare_summary($FareDetails, $PassengerFareBreakdown, $convenience_fees, $search_data);?> 
				</div>
				<div class="moreflt boksectn">
								<?=$flight_segment_details['segment_full_details'];?>
							</div>
				<div class="col-xs-8 nopadding full_summery_tab hide">
					<div class="fligthsdets">
						<div class="flitab1">
							<!-- Segment Details Starts-->
							
							<!-- Segment Details Ends-->
							<div class="clearfix"></div>
							<div class="sepertr"></div>
							<!-- LOGIN SECTION STARTS -->
							<?php if(is_logged_in_user() == false) { ?>
							<div class="loginspld">
								<div class="logininwrap">
									<!-- <div class="signinhde">
										<?php echo ('Sign in now to Book Online'); ?>
									</div> -->
									<div class="newloginsectn">
										<div class="col-xs-5 celoty nopad">
											<div class="insidechs">
												<div class="mailenter">
													<input type="text" name="booking_user_name" id="booking_user_name" maxlength="80"  placeholder="<?php echo ('Email'); ?>" class="newslterinput nputbrd _guest_validate">
												</div>
												<div class="noteinote"><?php echo ('Your booking details will be sent to this email address'); ?>.</div>
												<div class="clearfix"></div>
												<div class="havealrdy">
													<div class="squaredThree">
														<input id="alreadyacnt" type="checkbox" name="check" value="None">
														<label for="alreadyacnt"></label>
													</div>
													<label for="alreadyacnt" class="haveacntd"><?php echo ('I have an Account'); ?></label>
												</div>
												<div class="clearfix"></div>
												<div class="twotogle">
													<div class="cntgust">
														<div class="phoneumber">
															<div class="col-xs-2 nopadding">
																<select class="newslterinput nputbrd _numeric_only " >
																<?php echo diaplay_phonecode($phone_code,$active_data); ?>
															</select> 
															</div>
															<div class="col-xs-1 nopadding">
																<div class="sidepo">-</div>
															</div>
															<div class="col-xs-9 nopadding">
																<input type="text" id="booking_user_mobile" placeholder="<?php echo ('Mobile Number'); ?>" class="newslterinput _numeric_only _guest_validate" maxlength="15">
															</div>
															<div class="clearfix"></div>
															<div class="noteinote"><?php echo ('Well use this number to send possible update alerts'); ?>.</div>
														</div>
														<div class="clearfix"></div>
														<div class="continye col-xs-8 nopad">
															<button class="bookcont" id="continue_as_guest"><?php echo ('Book as Guest'); ?></button>
														</div>
													</div>
													<div class="alrdyacnt">
														<div class="col-xs-12 nopad">
															<div class="relativemask"> 
																<input type="password" name="booking_user_password" id="booking_user_password" class="clainput" placeholder="<?php echo ('Password'); ?>" />
															</div>
															<div class="clearfix"></div>
															<a class="frgotpaswrd"><?php echo ('Forgot Password'); ?>?</a>
															<div style="" class="hide alert alert-danger"></div>
														</div>
														<div class="clearfix"></div>
														<div class="continye col-xs-8 nopad">
															<button class="bookcont" id="continue_as_user"><?php echo ('Proceed to Book'); ?></button>
														</div>
													</div>
												</div>
											</div>
										</div>
										<?php $no_social=no_social(); if($no_social != 0) {?>
										<div class="col-xs-2 celoty nopad linetopbtm">
											<div class="orround"><?php echo ('Or'); ?></div>
										</div>
										<?php } ?>
										<div class="col-xs-5 celoty nopad">
											<div class="insidechs booklogin">
												<div class="leftpul">
													<?php 
														$social_login1 = 'facebook';
														$social1 = is_active_social_login($social_login1);
														if($social1){
															$GLOBALS['CI']->load->library('social_network/facebook');
															echo $GLOBALS['CI']->facebook->login_button ();
														} 
														$social_login2 = 'twitter';
														$social2 = is_active_social_login($social_login2);
														if($social2){
													?>
														<a class="logspecify tweetcolor">
															<span class="fa fa-twitter"></span>
															<div class="mensionsoc"><?php echo ('Login with Twitter'); ?></div>
														</a>
													<?php } 
														$social_login3 = 'googleplus';
														$social3= is_active_social_login($social_login3);
														if($social3){
															$GLOBALS['CI']->load->library('social_network/google');
															echo $GLOBALS['CI']->google->login_button ();
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							<!-- LOGIN SECTION ENDS -->
						</div>
					</div>
				</div>
			</div>
			<div class="bktab2 xlbox <?=$travellers_tab_details_class?>">
				<div class="topalldesc">
					<div class="col-xs-8 nopadding celtbcel segment_seg">
						<?=$flight_segment_details['segment_abstract_details'];?>
					</div>
					<!-- Outer Summary -->
					<div class="col-xs-4 nopadding celtbcel colrcelo">
						<div class="bokkpricesml">
							<div class="travlrs"><?php echo ('Travellers'); ?>: <span class="fa fa-male"></span> <?php echo $search_data['adult'];?> |  <span class="fa fa-child"></span> <?php echo $search_data['child'];?> |  <span class="infantbay"><img src="<?=$template_images?>infant.png" alt="" /></span> <?php echo $search_data['infant'];?></div>
							<div class="totlbkamnt"> <?php echo ('Total Amount'); ?> <?php echo $currency_symbol;?> <span id='total_amt'><?php echo roundoff_number($flight_total_amount);?></span></div>
							<a class="fligthdets" data-toggle="collapse" data-target="#fligtdetails"><?php echo ('FlightA'); ?> <?php echo ('Details'); ?> </a>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- Segment Details Starts-->
				<div class="collapse splbukdets" id="fligtdetails">
					<div class="moreflt insideagain">
						<?=$flight_segment_details['segment_full_details'];?>
					</div>
				</div>
				<!-- Segment Details Ends-->
				<div class="clearfix"></div>
				<div class="padpaspotr">
					<div class="col-xs-8 nopadding tab_pasnger">
						<div class="fligthsdets">
							<?php
								/**
								 * Collection field name 
								 */
								//Title, Firstname, Middlename, Lastname, Phoneno, Email, PaxType, LeadPassenger, Age, PassportNo, PassportIssueDate, PassportExpDate
								$total_adult_count	= is_array($search_data['adult_config']) ? array_sum($search_data['adult_config']) : intval($search_data['adult_config']);
								$total_child_count	= is_array($search_data['child_config']) ? array_sum($search_data['child_config']) : intval($search_data['child_config']);
								$total_infant_count	= is_array($search_data['infant_config']) ? array_sum($search_data['infant_config']) : intval($search_data['infant_config']);
								//------------------------------ DATEPICKER START
								$i = 1;
								$datepicker_list = array();
								if ($total_adult_count > 0) {
									for ($i=1; $i<=$total_adult_count; $i++) {
										$datepicker_list[] = array('adult-date-picker-'.$i, ADULT_DATE_PICKER);
									}
								}
								
								if ($total_child_count > 0) {
									//id should be auto picked so initialize $i to previous value of $i
									for ($i=$i; $i<=($total_child_count+$total_adult_count); $i++) {
										$datepicker_list[] = array('child-date-picker-'.$i, CHILD_DATE_PICKER);
									}
								}
								if ($total_infant_count > 0) {
									//id should be auto picked so initialize $i to previous value of $i
									for ($i=$i; $i<=($total_child_count+$total_adult_count+$total_infant_count); $i++) {
										$datepicker_list[] = array('infant-date-picker-'.$i, INFANT_DATE_PICKER);
									}
								}
								//$GLOBALS['CI']->current_page->set_datepicker($datepicker_list);
								//------------------------------ DATEPICKER END
								$total_pax_count	= $total_adult_count+$total_child_count+$total_infant_count;
								//First Adult is Primary and and Lead Pax
								$adult_enum = $child_enum = get_enum_list('title');
								$gender_enum = get_enum_list('gender');
								unset($adult_enum[MASTER_TITLE]); // Master is for child so not required
								unset($child_enum[MASTER_TITLE]); // Master is not supported in TBO list
								$adult_title_options = generate_options($adult_enum, false, true);
								$child_title_options = generate_options($child_enum, false, true);
								$gender_options	= generate_options($gender_enum);
								$nationality_options = generate_options($iso_country_list, array(INDIA_CODE));//FIXME get ISO CODE --- ISO_INDIA
								$passport_issuing_country_options = generate_options($country_list);
								if($search_data['trip_type'] == 'oneway') {
									$passport_minimum_expiry_date = date('Y-m-d', strtotime($search_data['depature']));
								} else if($search_data['trip_type'] == 'circle') {
									$passport_minimum_expiry_date = date('Y-m-d', strtotime($search_data['return']));
								} else {
									$passport_minimum_expiry_date = date('Y-m-d', strtotime(end($search_data['depature'])));
								}
								//lowest year wanted
								$cutoff = date('Y', strtotime('+20 years', strtotime($passport_minimum_expiry_date)));
								$cutoff = date('Y', strtotime($passport_minimum_expiry_date))+20;
								//current year
								$now = date('Y', strtotime($passport_minimum_expiry_date));
								$day_options	= generate_options(get_day_numbers());
								$month_options	= generate_options(get_month_names());
								$year_options	= generate_options(get_years($now, $cutoff));
								/**
								 * check if current print index is of adult or child by taking adult and total pax count
								 * @param number $total_pax		total pax count
								 * @param number $total_adult	total adult count
								 */
								function pax_type($pax_index, $total_adult, $total_child, $total_infant)
								{
									if ($pax_index <= $total_adult) {
										$pax_type = 'adult';
									} elseif ($pax_index <= ($total_adult+$total_child)) {
										$pax_type = 'child';
									} else {
										$pax_type = 'infant';
									}
									return $pax_type;
								}
								/**
								 * check if current print index is of adult or child by taking adult and total pax count
								 * @param number $total_pax		total pax count
								 * @param number $total_adult	total adult count
								 */
								function is_adult($pax_index, $total_adult)
								{
									return ($pax_index>$total_adult ?	false : true);
								}
								/**
								 * check if current print index is of adult or child by taking adult and total pax count
								 * @param number $total_pax		total pax count
								 * @param number $total_adult	total adult count
								 */
								function is_lead_pax($pax_count)
								{
									return ($pax_count == 1 ? true : false);
								}

								function diaplay_phonecode($phone_code,$active_data)
								{
									$list='';
									foreach($phone_code as $code){
									if($active_data['api_country_list_fk']==$code['origin']){
											$selected ="selected";
										}
										else {
											$selected="";
										}
									
										$list .="<option value=".$code['country_code']."  ".$selected." >".$code['country_code']." ".$code['name']."</option>";
									}
									 return $list;
									
								}
								?>
							<form action="<?=base_url().'index.php/flight/pre_booking/'.$search_data['search_id']?>" method="POST" autocomplete="off" id="pre-booking-form">
							<!-- <div class="hide">
									<input type="hidden" required="required" name="search_id"		value="<?=$search_data['search_id'];?>" />
									<?php $dynamic_params_url = serialized_data($pre_booking_params);?>
									<input type="hidden" required="required" name="token"		value="<?=$dynamic_params_url;?>" />
									<input type="hidden" required="required" name="token_key"	value="<?=md5($dynamic_params_url);?>" />
									<input type="hidden" required="required" name="op"			value="book_room">
									<input type="hidden" required="required" name="flights_data" value="<?=base64_encode(json_encode($pre_booking_summery));?>">
									<input type="hidden" name="pricing_xml" class="pricing_xml" value="<?=base64_encode(json_encode($pricing_xml))?>">
									<input type="hidden" name="segment_solution_xml" readonly="true" class="pricing_xml" value="<?=base64_encode(json_encode($segment_solution_xml))?>">
									<input type="hidden" name="host_token_data_xml" value="<?=base64_encode(json_encode($Host_token_data))?>">
									<input type="hidden" required="required" name="booking_source"		value="<?=$booking_source?>" readonly>
									<!--<input type="hidden" required="required" name="provab_auth_key" value="?=$ProvabAuthKey ?>" readonly>
								</div> -->
								<div class="flitab1">
									<div class="moreflt boksectn">
										<div class="ontyp">
											<div class="labltowr arimobold"><?php echo ('Please enter names as on passport'); ?>. </div>
											<?php
												$pax_index = 1;
												$lead_pax_details = @$pax_details[0];
												if(is_logged_in_user()) {
												$traveller_class = ' user_traveller_details ';
												} else {
												$traveller_class = '';
												}
													for($pax_index=1; $pax_index <= $total_pax_count; $pax_index++) {//START FOR LOOP FOR PAX DETAILS
													$cur_pax_info = is_array($pax_details) ? array_shift($pax_details) : array();
													$pax_type = pax_type($pax_index, $total_adult_count, $total_child_count, $total_infant_count);
													?>
											<div class="pasngrinput _passenger_hiiden_inputs">
												<div class="hide hidden_pax_details">
													<input type="hidden" name="passenger_type[]" value="<?=ucfirst($pax_type)?>">
													<input type="hidden" name="lead_passenger[]" value="<?=(is_lead_pax($pax_index) ? true : false)?>">
													<input type="hidden" name="gender[]" value="1" class="pax_gender">
													<input type="hidden" required="required" name="passenger_nationality[]" id="passenger-nationality-<?=$pax_index?>" value="92">
												</div>
												<div class="col-xs-1 nopadding full_dets_aps">
													<div class="adltnom"><?=ucfirst(($pax_type))?><?=$mandatory_filed_marker?></div>
												</div>
												<div class="col-xs-11 nopadding full_dets_aps">
													<div class="inptalbox">
														<div class="col-xs-2 spllty">
															<div class="selectedwrap">
																<select class="mySelectBoxClass flyinputsnor name_title" name="name_title[]" required="required">
																<?php echo (is_adult($pax_index, $total_adult_count) ? $adult_title_options : $child_title_options)?>
																</select>
															</div>
														</div>
														<div class="col-xs-4 spllty">
															<input value="<?=@$cur_pax_info['first_name']?>" required="required" type="text" name="first_name[]" id="passenger-first-name-<?=$pax_index?>" class="clainput  <?=$traveller_class?>" maxlength="45" placeholder="<?php echo ('First Name'); ?>" data-row-id="<?=($pax_index);?>"/>
														</div>
														<div class="col-xs-3 spllty">
															<input value="<?=@$cur_pax_info['middle_name']?>"  type="text" name="middle_name[]" id="passenger-middle-name-<?=$pax_index?>" class="clainput alpha <?=$traveller_class?>" maxlength="45" placeholder="<?php echo ('Middle Name'); ?>" data-row-id="<?=($pax_index);?>"/>
														</div>
														<div class="col-xs-3 spllty">
															<input value="<?=@$cur_pax_info['last_name']?>" required="required" type="text" name="last_name[]" id="passenger-last-name-<?=$pax_index?>" class="clainput alpha" maxlength="45" placeholder="<?php echo ('Last Name'); ?>" />
														</div>
														<?php if($pax_type == 'adult') { ?>
														<div class="col-xs-3 spllty">
															<input value=""  type="text" name="ffnumber[]" id="ffnumber-<?=$pax_index?>" class="clainput alpha" maxlength="45" placeholder="<?php echo ('Enter FF Number'); ?>" />
														</div>
														<?php } ?>
														<?php if($pax_type == 'infant') { //Only For Infant?>
														<div class="col-xs-6 spllty infant_dob_div">
															<div class="col-xs-4 nopadding"><span class="fmlbl"><?php echo ('DOB'); ?> <?=$mandatory_filed_marker?></span></div>
															<div class="col-xs-8 nopadding">
																<input placeholder="<?php echo ('DOB'); ?>" type="text" class="clainput"  name="date_of_birth[]" readonly="readonly" <?=(is_adult($pax_index, $total_adult_count) ? 'required="required"' : 'required="required"')?> id="<?=strtolower(pax_type($pax_index, $total_adult_count, $total_child_count, $total_infant_count))?>-date-picker-<?=$pax_index?>">
																<?php echo ('Note New borns to 23month olds only'); ?>
															</div>
														</div>


														<?php } else{ //Adult/Child
															if($pax_type == 'adult') {
																$static_date_of_birth = date('Y-m-d', strtotime('-30 years'));;
															} else if($pax_type == 'child') {
																$static_date_of_birth = date('Y-m-d', strtotime('-8 years'));;
															}
															?>
														<div class="adult_child_dob_div hide">
															<input type="hidden" name="date_of_birth[]" value="<?=$static_date_of_birth?>">
														</div>
														<?php } ?>
														<div class="clearfix"></div>
														<!-- Passport Section Starts -->
														<div class="passport_content_div">
															<?php if($is_domestic_flight == false) { //For Internatinal Travel?>
															<div class="international_passport_content_div">
																<div class="col-xs-4 spllty">
																	<span class="formlabel"><?php echo ('Passport Number'); ?> <?=$pass_mand?></span>
																	<div class="relativemask"> 
																		<input type="text" name="passenger_passport_number[]" <?=$pass_req?> id="passenger_passport_number_<?=$pax_index?>" class="clainput" maxlength="10" placeholder="<?php echo ('Passport Number'); ?>" />
																	</div>
																</div>
																<div class="col-xs-3 spllty">
																	<span class="formlabel"><?php echo ('Issuing Country'); ?> <?=$pass_mand?></span>
																	<div class="selectedwrap">
																		<select name="passenger_passport_issuing_country[]" <?=$pass_req?> id="passenger_passport_issuing_country_<?=$pax_index?>" class="mySelectBoxClass flyinputsnor">
																			<option value="INVALIDIP"><?php echo ('Please Select'); ?></option>
																			<?=$passport_issuing_country_options?>
																		</select>
																	</div>
																</div>
																<div class="col-xs-5 spllty">
																	<span class="formlabel"><?php echo ('Date of Expire'); ?> <?=$pass_mand?></span>
																	<div class="relativemask">
																		<div class="col-xs-4 splinmar">
																			<div class="selectedwrap">
																				<select name="passenger_passport_expiry_day[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_day" data-expiry-type="day" id="passenger_passport_expiry_day_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
																					<option value="INVALIDIP">DD</option>
																					<?=$day_options;?>
																				</select>
																			</div>
																		</div>
																		<div class="col-xs-4 splinmar">
																			<div class="selectedwrap">
																				<select name="passenger_passport_expiry_month[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_month" data-expiry-type="month" id="passenger_passport_expiry_month_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
																					<option value="INVALIDIP">MM</option>
																					<?=$month_options;?>
																				</select>
																			</div>
																		</div>
																		<div class="col-xs-4 splinmar">
																			<div class="selectedwrap">
																				<select name="passenger_passport_expiry_year[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_year" data-expiry-type="year" id="passenger_passport_expiry_year_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
																					<option value="INVALIDIP">YYYY</option>
																					<?=$year_options;?>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
															<div class="pull-right text-danger hide" id="passport_error_msg_<?=$pax_index?>"></div>
															</div>
															<?php } else { //For Domestic Travel, Set Static Passport Data
																$passport_number = rand(1111111111,9999999999);
																$passport_issuing_country = 92;
																?>
															<div class="domestic_passport_content_div hide">
																<input type="hidden" name="passenger_passport_number[]" value="" id="passenger_passport_number_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_issuing_country[]" value="" id="passenger_passport_issuing_country_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_expiry_day[]" value="<?=$static_passport_details['passenger_passport_expiry_day']?>" id="passenger_passport_expiry_day_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_expiry_month[]" value="<?=$static_passport_details['passenger_passport_expiry_month']?>" id="passenger_passport_expiry_month_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_expiry_year[]" value="<?=$static_passport_details['passenger_passport_expiry_year']?>" id="passenger_passport_expiry_year_<?=$pax_index?>">
															</div>
															<?php }?>
														</div>
														<!-- Passport Section Ends-->
													</div>
												</div>
											</div>
											<?php
												}//END FOR LOOP FOR PAX DETAILS
												?>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="sepertr"></div>
									<div class="clearfix"></div>
									<div class="contbk">
										<div class="contcthdngs"><?php echo ('CONTACT DETAILS'); ?></div>
										<div class="col-xs-6 nopad full_smal_forty">
											<div class="col-xs-2 nopadding">
												<div class="hide">
													<input type="hidden" name="billing_country" value="92">
													<input type="hidden" name="billing_city" value="test">
													<input type="hidden" name="billing_zipcode" value="test">
													<input type="hidden" name="billing_address_1" value="test">
												</div>
												<select name="country_code" class="newslterinput  nputbrd _numeric_only " required="required">
											<?php echo diaplay_phonecode($phone_code,$active_data); ?>
										</select> 
											</div>
											<div class="col-xs-2">
												<div class="sidepo">-</div>
											</div>
											<div class="col-xs-8 nopadding">
												<input value="<?=@$lead_pax_details['phone'] == 0 ? '' : @$lead_pax_details['phone'];?>" type="text" name="passenger_contact" id="passenger-contact" placeholder="<?php echo ('Mobile Number'); ?>" class="newslterinput nputbrd _numeric_only"  required="required">
											</div>
											<div class="clearfix"></div>
											<div class="emailperson col-xs-12 nopad full_smal_forty">
												<input value="<?=@$lead_pax_details['email']?>" type="text" maxlength="80" required="required" id="billing-email" class="newslterinput nputbrd" placeholder="<?php echo ('Email'); ?>" name="billing_email">
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="notese"><?php echo ('Your mobile number will be used only for sending flight related communication'); ?>.</div>
									</div>

						<!-- PROMO CODE Start -->
								<div style="padding:2%;margin-bottom:2%">
								<div class="contcthdngs"><?php echo ('PROMO CODE'); ?></div>
								<div class="col-xs-6 nopad full_smal_forty">           
								    <div class="tablesign">
										<div class="inputsign">
											<input type="text" name="promocode" id="promo_code" maxlength="60"  placeholder="<?php echo ('PROMO CODE'); ?>" class="newslterinput ">
										</div>
									    <input type='hidden' id='withoutpromo' value="<?php echo 
									    $flight_total_amount ?>" >
										<div class="submitsign"> 
										      <button class="promobtn btn-promo" type="button"><?php echo ('Apply'); ?></button>
										 </div>
									</div>
									<div class="clearfix"></div>
									<strong><span class="pull-left promo-msg"></span></strong>
								</div>
								</div>
						<!-- PROMO CODE End -->

									<div class="clikdiv">
										<div class="squaredThree">
											<input id="terms_cond1" type="checkbox" name="tc" checked="checked" required="required">
											<label for="terms_cond1"></label>
										</div>
										<span class="clikagre" id="clikagre">
										<?php echo ('Terms & Conditions'); ?>
										</span>
									</div>
									<div class="clearfix"></div>
									<div class="loginspld">
										<div class="collogg">
											<?php
												//If single payment option then hide selection and select by default
												if (count($active_payment_options) == 1) {
													$payment_option_visibility = 'hide';
													$default_payment_option = 'checked="checked"';
												} else {
													$payment_option_visibility = 'show';
													$default_payment_option = '';
												}
												
												?>
											<div class="row <?=$payment_option_visibility?>">
												<?php if (in_array(PAY_NOW, $active_payment_options)) {?>
												<div class="col-md-3">
													<div class="form-group">
														<label for="payment-mode-<?=PAY_NOW?>">
														<input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_NOW?>" id="payment-mode-<?=PAY_NOW?>" class="form-control b-r-0" placeholder="<?php echo ('Payment Mode'); ?>">
														<?php echo ('Pay Now'); ?>
														</label>
													</div>
												</div>
												<?php } ?>
												<?php if (in_array(PAY_AT_BANK, $active_payment_options)) {?>
												<div class="col-md-3">
													<div class="form-group">
														<label for="payment-mode-<?=PAY_AT_BANK?>">
														<input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_AT_BANK?>" id="payment-mode-<?=PAY_AT_BANK?>" class="form-control b-r-0" placeholder="<?php echo ('Payment Mode'); ?>">
														<?php echo ('Pay At Bank'); ?>
														</label>
													</div>
												</div>
												<?php } ?>
											</div>
											<input type="hidden" id="promocode_discount" name="promocode_discount" value="0">
											<input type="hidden" id="promo_codevalue" name="promocode" value="0">
											<div class="continye col-sm-3 col-xs-6">
												<button onclick="if(!this.form.tc.checked){alert('You must agree to the Terms and Conditions.');return false}" type="submit" id="flip" name="flight" class="bookcont"><?php echo ('Continue'); ?></button>
											</div>
											<div class="clearfix"></div>
											<div class="sepertr"></div>
											<div class="temsandcndtn">
												<?php echo ('Your mobile number will be used only for sending flight related communication'); ?>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php if(is_logged_in_user() == true) { ?>
					<div class="col-xs-4 nopadding">
						<div class="insiefare">
							<div class="farehd arimobold"><?php echo ('Passenger List'); ?></div>
							<div class="fredivs">
								<div class="psngrnote">
									<?php
										if(valid_array($traveller_details)) {
											$traveller_tab_content = ''.('you_have_saved_passenger').'';
										} else {
											$traveller_tab_content = ''.('you_do_not_have_any_passenger').'. <a href="'.base_url().'index.php/user/profile?active=traveller" target="_blank">'.('Add Now').'</a>';
										}
										?>
									<?=$traveller_tab_content;?>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<span class="hide">
<input type="hidden" id="pri_passport_min_exp" value="<?=$passport_minimum_expiry_date?>">
</span>
</body>
<?php
/*
 * Jaganath
 * Flight segment details
 * Outer summary and Inner Summary
 */
function flight_segment_details($SegmentDetails, $SegmentSummary, $segment_xml)
{
	$loc_dir_icon = '<span class="fadr fa fa-long-arrow-right textcntr"></span>';
	$inner_summary = $outer_summary = '';

	//Inner Summary
	foreach($SegmentDetails as $k => $SegmentDetails){
		foreach($SegmentDetails as $__segment_k => $__segment_v) {
			$segment_summary = $SegmentSummary[$k][$__segment_k];
			//Calculate Total Duration of Onward/Return Journey
			$inner_summary .= '<div class="ontyp">';
			//Way Summary in one line - Start
			$inner_summary .= '<div class="labltowr arimobold">';
			$inner_summary .= $segment_summary['OriginDetails']['CityName'].' to '.$segment_summary['DestinationDetails']['CityName'].'<strong>(&rlm;'.$segment_summary['TotalDuarationLabel'].')</strong>';
			$inner_summary .= '</div>';
			//Way Summary in one line - End
			foreach ($__segment_v as $__stop => $__segment_flight) { 
				//Summary of Way - Start
				$inner_summary .= '<div class="allboxflt">';
					//airline
					$inner_summary .= '<div class="col-xs-3 nopadding width_adjst">
										<div class="jetimg">
										<img  alt="'.$__segment_flight['AirlineDetails']['AirlineCode'].'" src="'.SYSTEM_IMAGE_DIR.'airline_logo/'.$__segment_flight['AirlineDetails']['AirlineCode'].'.gif" >
										</div>
										<div class="alldiscrpo">
										'.$__segment_flight['AirlineDetails']['AirlineName'].'
										<span class="sgsmal">'.$__segment_flight['AirlineDetails']['AirlineCode'].' 
										<br />'.$__segment_flight['AirlineDetails']['FlightNumber'].'</span>
										</div>
									  </div>';
					//depart
					$inner_summary .= '<div class="col-xs-7 nopadding width_adjst">';
					$inner_summary .= '<div class="col-xs-5">
										<span class="airlblxl">'.($__segment_flight['OriginDetails']['DepartureTime']) .', '. $__segment_flight['OriginDetails']['_DepartureTime'].'</span>
										<span class="portnme">'.$__segment_flight['OriginDetails']['AirportName'].'</span>
										</div>';
					//direction indicator
					$inner_summary .= '<div class="col-xs-2">
					'.$loc_dir_icon.'</div>';
					//arrival
					$inner_summary .= '<div class="col-xs-5"> 
										<span class="airlblxl">'.($__segment_flight['DestinationDetails']['ArrivalTime'] .', '. $__segment_flight['DestinationDetails']['_ArrivalTime']).'</span>
										<span class="portnme">'.$__segment_flight['DestinationDetails']['AirportName'].'</span>
										</div>';
					$inner_summary .= '</div>';
						
					//Between Content -----
					$inner_summary .= '<div class="col-xs-2 nopadding width_adjst">
									<span class="portnme textcntr">'.$__segment_flight['SegmentDuration'].'</span>
									<span class="portnme textcntr">'.('Stop').' : '.($__stop).'</span>
									<span class="portnmeter textcntr">'.$__segment_flight['AirlineDetails']['FareClassLabel'].'</span>
									</div>
									<input type="hidden" value="'.base64_encode(json_encode(@$segment_xml[$__stop])).'" name="segment_xml"/>
									';
					//Summary of Way - End
					$inner_summary .= '</div>';
				if (isset($__segment_v['WaitingTime']) == true) {
					$next_seg_info = $seg_v[$seg_details_k+1];
					$waiting_time = $__segment_v['WaitingTime'];
					$inner_summary .= '
				<div class="clearfix"></div>
				<div class="connectnflt">
					<div class="conctncentr">
					<span class="fa fa-plane"></span>'.('Change of planes at').' '.$next_seg_info['OriginDetails']['AirportName'].' | <span class="fa fa-clock-o"></span> '.('Waiting').' : '.$waiting_time.'
				</div>
				</div>
				<div class="clearfix"></div>';
				}
			}
			$inner_summary .= '</div>';
		}
	}

	//Outer Summry
	$total_stop_count = 0;
	$outer_summary .='<div class="moreflt spltopbk">';
	foreach($SegmentSummary as $k =>$SegmentSummary){
	foreach ($SegmentSummary as $__segment_k => $__segment_v) { 
		//debug($__segment_v); exit;
		$total_segment_travel_duration = $__segment_v['TotalDuarationLabel'];
		$__stop_count = $__segment_v['TotalStops'];
		$total_stop_count	+= $__stop_count;
		$outer_summary .= '<div class="ontypsec">
						<div class="allboxflt">';
		//airline
		$outer_summary .= '<div class="col-xs-3 nopadding width_adjst">
							<div class="jetimg">
							<img class="airline-logo" alt="'.$__segment_v['AirlineDetails']['AirlineCode'].'" src="'.SYSTEM_IMAGE_DIR.'airline_logo/'.$__segment_v['AirlineDetails']['AirlineCode'].'.gif">
							</div>
							<div class="alldiscrpo">
									'.$__segment_v['AirlineDetails']['AirlineName'].'
									<span class="sgsmal"> '.$__segment_v['AirlineDetails']['AirlineCode'].'<br />'.$__segment_v['AirlineDetails']['FlightNumber'].'</span>
							</div>
						  </div>';
		$outer_summary .= '<div class="col-xs-7 nopadding width_adjst">';
		//depart
		$outer_summary .= '<div class="col-xs-5">
									<span class="airlblxl">'.$__segment_v['OriginDetails']['AirportName'].'</span>
									<span class="portnme">'.(($__segment_v['OriginDetails']['DepartureTime']) .', '. $__segment_v['OriginDetails']['_DepartureTime']).'</span>
							</div>';
		//direction indicator
		$outer_summary .= '<div class="col-xs-2"><span class="fadr fa fa-long-arrow-right textcntr"></span></div>';
		//arrival
		$outer_summary .= '<div class="col-xs-5">
									<span class="airlblxl">'.$__segment_v['DestinationDetails']['AirportName'].'</span>
									<span class="portnme">'.($__segment_v['DestinationDetails']['ArrivalTime'].', '.$__segment_v['DestinationDetails']['_ArrivalTime']).'</span>
								</div>
								</div>';
		//Stops/Class details
		$outer_summary .= '<div class="col-xs-2 nopadding width_adjst">
								<span class="portnme textcntr">'.($total_segment_travel_duration).'</span>
								<span class="portnme textcntr" >'.('Stop').':'.($__stop_count).'</span>
						</div>';
		$outer_summary .= '</div></div>';
	}
}
	$outer_summary .='</div>';
	return array('segment_abstract_details' => $outer_summary, 'segment_full_details' => $inner_summary);
}
function get_fare_summary($FareDetails, $PassengerFareBreakdown, $convenience_fees, $search_data)
{
	//debug($search_data);die(" down");
	$total_tax = $FareDetails['TotalTax']; // Modified By bhola
	$flight_total_price = roundoff_number($FareDetails['TotalFare']+$convenience_fees); // Modified By bhola
	$currency_symbol = $FareDetails['CurrencySymbol'];
	$fare_summary = '<div class="insiefare">
				<div class="farehd arimobold">'.('Fare Summary').'</div>
				<div class="fredivs">';
				$pax_base_fare_details = '<div class="kindrest">
							<div class="freshd">'.('Base Fare').'</div>';
				$pax_tax_details = '<div class="kindrest">
								<div class="freshd">'.('Taxes').'</div>';
				//foreach($PassengerFareBreakdown as $k => $v) {
					$pax_type = $search_data['adult_config'];
					$pax_base_fare = $FareDetails['BaseFare'];
					$pax_count = $search_data['adult_config'] + $search_data['child_config']+ $search_data['infant_config'];
					$pax_base_fare_details .= '<div class="reptallt">
						<div class="col-xs-8 nopadding">
							<div class="faresty">‎(&rlm; '.$pax_count .'X '.($pax_base_fare/$pax_count).')</div>
						</div>
						<div class="col-xs-4 nopadding">
							<div class="amnter">'.$currency_symbol.' '.$pax_base_fare.' </div>
						</div>
					</div>';
					//} 
					$pax_tax_details .= '<div class="reptallt">
						<div class="col-xs-8 nopadding">
							<div class="faresty">'.('Taxes & Fees').'</div>
						</div>
						<div class="col-xs-4 nopadding">
							<div class="amnter arimobold">'.$currency_symbol.' '.roundoff_number($total_tax).' </div>
						</div>
						<div class="col-xs-8 nopadding">
							<div class="faresty">'.('Convenience Fees').'</div>
						</div>
						<div class="col-xs-4 nopadding">
							<div class="amnter arimobold">'.$currency_symbol.' '.$convenience_fees.' </div>
						</div>
					</div>';
					$pax_base_fare_details .= '</div>';
					$pax_tax_details .= '</div>';
					$fare_summary .= $pax_base_fare_details;
					$fare_summary .= $pax_tax_details;
					$fare_summary .='
					<div class="clearfix"></div>
						<div class="reptalltftr">
							<div class="col-xs-8 nopadding">
								<div class="farestybig">'.('grand_total').'</div>
							</div>
							<div class="col-xs-4 nopadding">
								<div class="amnterbig arimobold">'.$currency_symbol.'<span id="g_total">'.$flight_total_price.'</span></div>
							</div>
						</div>
					</div>
					</div>';
	return $fare_summary;
}
?>
<?php echo $this->load->view('core/bottom_footer'); ?>
</body>
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/flight_session_expiry_script.js"></script> 
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/booking_script.js"></script> 
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/flight_booking.js"></script> 
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script>


<script type="text/javascript">
	/* 
		session time out variables defined
	*/
	var  search_session_expiry = "<?php echo $GLOBALS ['CI']->config->item ( 'flight_search_session_expiry_period' ); ?>";
	var search_session_alert_expiry = "<?php echo $GLOBALS ['CI']->config->item ( 'flight_search_session_expiry_alert_period' ); ?>";
	var search_hash = "<?php echo $session_expiry_details['search_hash']; ?>";
	var start_time = "<?php echo $session_expiry_details['session_start_time']; ?>";
	var session_time_out_function_call = 1; 
</script>
