
<?php

array_unshift(Js_Loader::$css, array('href' => $GLOBALS['CI']->template->template_css_dir('tp_booking_page.css'), 'media' => 'screen'));//debug($pre_booking_params['token']['total_price']);exit;
//debug($flight_details);exit;
$change_panelty = @$flight_details['change_panelty'][0];
$cncel_panelty = @$flight_details['cncel_panelty'][0];
$total_price = $flight_details['total_price'];
$currency = $flight_details['total_price_curr'];
$base_price = $flight_details['base_price'];
$taxes = $flight_details['taxes'];
$lead_pax_details = $pax_details[0];
$dob_dp_placeholder = 'dd/mm/yy';

$is_domestic = $search_data['is_domestic_flight'];
$pre_booking_params['token']['total_price'] = $total_price;
$pre_booking_params['token']['currency'] = $currency;

$template_images = $GLOBALS['CI']->template->template_images();
$mandatory_filed_marker = '<sup class="text-danger">*</sup>';

$pax_default_country = (isset($lead_pax_details['country_code']) ? $lead_pax_details['country_code'] : INDIA_CODE);
$nationality_options = generate_options($iso_country_list, array($pax_default_country));//FIXME get ISO CODE --- ISO_INDIA

Js_Loader::$js[]    = array('src' => $GLOBALS['CI']->template->template_js_dir('provablib.js'), 'defer' => 'defer');
Js_Loader::$css[]   = array('href' => $GLOBALS['CI']->template->template_css_dir('pre_booking.css'));
Js_Loader::$css[]   = array('href' => $GLOBALS['CI']->template->template_css_dir('flight_result.css'));
Js_Loader::$js[]    = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/booking_script.js'), 'defer' => 'defer');
Js_Loader::$js[]    = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/flight_booking.js'), 'defer' => 'defer');

    /******************************** Reward Points   ************************************/
        #$user_earned_reward_point = $this->reward_model->get_current_user_reward_point_details();
        //get reward point details.
        #$reward_point_details = $this->reward_model->get_reward_point_details();
        #$total_reward_point = 0;
        #$reward_point_discount = 0;
        #$flight_total_amount = $total_price;
        /*if($flight_total_amount >= $reward_point_details['flight_minimum_spending']) {
            //calculate reward point percentage.
           $total_reward_point = round(($reward_point_details['flight_reward_point'] / 100) * $flight_total_amount);
            $reward_point_discount = ($user_earned_reward_point * $reward_point_details['get_reward_point']);
        }*/
    /********************************  End Reward points  *********************************/

$flights_array = $flight_details['flights'];
//First Adult is Primary and and Lead Pax
$adult_enum = $child_enum = $form_params['title'];
$gender_enum = $form_params['gender'];

//unset($adult_enum[MASTER_TITLE]); // Master is for child so not required
//unset($child_enum[MASTER_TITLE]); // Master is not supported in TBO list
$title_options = generate_options($adult_enum, false, true);
$gender_options = generate_options($gender_enum);
if(is_logged_in_user()) {
        $review_active_class = ' success ';
        $review_tab_details_class = '';
        $review_tab_class = ' inactive_review_tab_marker ';
        $travellers_active_class = ' active ';
        $travellers_tab_details_class = ' gohel ';
        $travellers_tab_class = ' travellers_tab_marker ';
    } else {
        $review_active_class = ' active ';
        $review_tab_details_class = ' gohel ';
        $review_tab_class = ' review_tab_marker ';
        $travellers_active_class = '';
        $travellers_tab_details_class = '';
        $travellers_tab_class = ' inactive_travellers_tab_marker ';
    }

?>
<style>
    .topssec::after{display:none;}
</style>
<div class="full onlycontent top80">
    <div class="martopbtm">
        <div class="fldealsec">
            <div class="container">
                <div class="tabcontnue">
                    <div class="col-xs-4 nopadding">
                        <div class="rondsts <?=$review_active_class?>">
                            <a class="taba core_review_tab <?=$review_tab_class?>" id="stepbk1">
                                <div class="iconstatus fa fa-eye"></div>
                                <div class="stausline">Review</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-4 nopadding">
                        <div class="rondsts <?=$travellers_active_class?>">
                            <a class="taba core_travellers_tab <?=$travellers_tab_class?>" id="stepbk2">
                                <div class="iconstatus fa fa-group"></div>
                                <div class="stausline">Travelers</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-4 nopadding">
                        <div class="rondsts">
                            <a class="taba" id="stepbk3">
                                <div class="iconstatus fa fa-money"></div>
                                <div class="stausline">Payments</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="alldownsectn">
        <div class="container">
        <div class="paymentpage">
<div class="bktab1 xlbox <?=$review_tab_details_class?>">
            <div class="col-md-4 col-sm-4 nopad sidebuki">
            <div class="farehd arimobold">Fare Summary</div>
                <div class="cartbukdis">
                    <ul class="liscartbuk">
<!--                        <li class="lostcart">-->
<!--                            <div class="cartlistingbuk">-->
<!--                                <div class="cartitembuk">-->
<!--                                    <div class="col-md-12">-->
<!--                                        <div class="payblnhmxm">Promo code</div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="clearfix"></div>-->
<!--                                <div class="cartitembuk prompform">-->
<!--                                    <form action="" name="promocode" id="promocode" novalidate="novalidate">-->
<!--                                        <div class="col-md-8 col-xs-8">-->
<!--                                            <div class="cartprc">-->
<!--                                                <div class="payblnhm singecartpricebuk ritaln">-->
<!--                                                    <input type="text" required="" placeholder="Enter Promo" name="code" id="code" class="promocode" aria-required="true" />-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-4 col-xs-4">-->
<!--                                            <input type="submit" value="Apply" name="apply" class="promosubmit">-->
<!--                                        </div>-->
<!--                                    </form>-->
<!--                                </div>-->
<!--                                <div class="clearfix"></div>-->
<!--                                <div class="savemessage"></div>-->
<!--                            </div>-->
<!--                        </li>-->
                        
                        <li class="lostcart" id="booking-fare-summary">
                            <div class="kindrest">
                                <div class="freshd">Base Fare</div>
                                <div class="reptallt">
                                    <div class="col-xs-8 nopadding">
                                        <div class="faresty">Base Fare</div>
                                    </div>
                                    <div class="col-xs-4 nopadding">
                                        <div class="amnter"><?=$currency?> <span class="amount"><?=$base_price?></span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="kindrest">
                                <div class="freshd">Taxes</div>
                                <div class="reptallt">
                                    <div class="col-xs-8 nopadding">
                                        <div class="faresty">Taxes & Fees</div>
                                    </div>
                                    <div class="col-xs-4 nopadding">
                                        <div class="amnter"><?=$currency?> <span class="amount"><?=$taxes?></span></div>
                                    </div>
                                    <div class="col-xs-8 nopadding">
                                        <div class="faresty">Convenience Fees</div>
                                    </div>
                                    <div class="col-xs-4 nopadding">
                                        <div class="amnter"><?=$currency.'  '?> 0</div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="reptalltftr">
                                <div class="col-xs-6 nopadding">
                                    <div class="farestybig">Grand Total</div>
                                </div>
                                <div class="col-xs-6 nopadding">
                                    <div class="amnterbig arimobold"><?=$currency?> <span class="amount"><?=$total_price?></span></div>
                                </div>
                            </div>
                      
                            <div class="clear"></div>
                            <div class="cartlistingbuk nomarr hide">
                                <div class="cartitembuk">
                                    <div class="col-md-6 celcart">
                                        <div class="payblnhm">Total</div>
                                    </div>
                                    <div class="col-md-6 celcart">
                                        <div class="cartprc">
                                            <div class="ritaln cartcntamnt bigclrfnt finalAmt"><?=$currency?> <span class="amount"><?=$total_price?></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 nopad fulbuki">
             <?php //debug($search_data['trip_type']) ;
                          if ($search_data['trip_type'] == 'multicity') {
                         for ($pi=0; $pi < count($search_data['from_city']); $pi++) {
                    ?>
                             <div class="labltowr arimobold">
                                <?=$search_data['from_city'][$pi].'   '?> to</span> <?='   '.$search_data['to_city'][$pi]?>
                            </div>
                    <?php } } else { ?>
                             <div class="labltowr arimobold">
                                <?=$search_data['from_city'].'   '?> to <?='   '.$search_data['to_city']?>
                            </div>
                    <?php }
                          
                    ?>
           
                <div class="sumry_wrap">
                    <!--  Flight Summery  -->
                    <div class="pre_summery">
                   
                        <!-- <div class="prebok_hding">
                            <?=$search_data['from_city']?><span class="fa fa-exchange"></span> <?=$search_data['to_city']?>
                        </div> -->
                        <div class="pad_summery_sec">
                        <?php
                            foreach ($flights_array as $k => $v) {
                            ?>
                            <div class="sidenamedesc">
                            <div class="col-xs-3 nopadding width_adjst">
                                <div class="jetimg">
                                    <img alt="" src="<?=SYSTEM_IMAGE_DIR . '/airline_logo/'.$v['carrier']?>.gif">
                                </div>
                                 <div class="alldiscrpo"> <?=$v['carrier_name']?>
                                    <span class="sgsmal"> <?=$v['carrier']?> <br />
                                        <?=@$v['flight_number']?>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-7 nopadding width_adjst">
                                <div class="col-xs-5">
                                    <div class="col-xs-12 padflt widfty nopad">
                                      <!--   <span class="timlbl"> <span class="flname"><span class="sprite reflone"></span><?=$v['origin']?><span class="fltime"></span></span> </span> -->
                                        <div class="clearfix"></div>
                                        <span class="flitrlbl elipsetool allboxflt"><?=$v['departure_date'].'  '?> <?=$v['departure_time']?> </span>
                                        <div class="rndplace"><?=$v['origin_city']?></div>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <span class="fadr fa fa-long-arrow-right textcntr"></span>
                                </div>
                                <div class="col-xs-5">
                                     <div class="col-xs-12 padflt widfty nopad">
                                       <!--  <span class="timlbl left"> <span class="flname"><span class="sprite refltwo"></span><?=$v['destination']?><span class="fltime"></span> </span> </span> -->
                                        <div class="clearfix"></div>
                                        <span class="flitrlbl elipsetool allboxflt"><?=$v['arrival_date'].'  '?><?=$v['arrival_time']?></span>
                                        <div class="rndplace"><?=$v['destination_city']?></div>
                                     </div>
                                </div>
                            </div>
                            <div class="col-xs-2 nopadding width_adjst">
                                <div class="col-xs-12 nopad padflt widfty">
                                    <div class="lyovrtime">
                                        <span class="flect"> <span class="sprite retime"></span><?=@$v['duration']?></span>
                                        <div class="instops ">
                                        <!--if one stop add class  'morestop' - more than one, also add class 'plusone'-->
                                            <a class="stopone">
                                            <label class="rounds"></label>
                                            </a> <a class="stopone">
                                            <label class="rounds oneonly"></label>
                                            <label class="rounds oneplus"></label>
                                            </a> <a class="stopone">
                                            <label class="rounds"></label>
                                            </a> 
                                        </div>
                                        
<!--                                                        <span class="flects"> <?=$k?> stop</span> -->
                                    </div>
                                </div>
                            </div>
                                <div class="celhtl width20 midlbord hide">
                                    <div class="fligthsmll"> <img alt="" src="<?=SYSTEM_IMAGE_DIR . '/airline_logo/'.$v['carrier']?>.gif"> </div>
                                    <div class="airlinename"><?=$v['carrier']?> - <?=@$v['flight_number']?></div>
                                </div>
                                <div class="celhtl width80 hide">
                                    <div class="waymensn">
                                        <div class="flitruo cloroutbnd">
                                            <div class="detlnavi">
                                                <div class="col-xs-4 padflt widfty">
                                                    <span class="timlbl right"> <span class="flname"><span class="sprite reflone"></span><?=$v['origin']?><span class="fltime"><?=$v['departure_time']?></span></span> </span>
                                                    <div class="clearfix"></div>
                                                    <span class="flitrlbl elipsetool"><?=$v['departure_date']?> </span>
                                                    <div class="rndplace"><?=$v['origin_city']?></div>
                                                </div>
                                                <div class="col-xs-4 nopad padflt widfty">
                                                    <div class="lyovrtime">
                                                        <span class="flect"> <span class="sprite retime"></span><?=@$v['duration']?></span>
                                                        <div class="instops ">
                                                            <!--if one stop add class  'morestop' - more than one, also add class 'plusone'-->
                                                            <a class="stopone">
                                                            <label class="rounds"></label>
                                                            </a> <a class="stopone">
                                                            <label class="rounds oneonly"></label>
                                                            <label class="rounds oneplus"></label>
                                                            </a> <a class="stopone">
                                                            <label class="rounds"></label>
                                                            </a> 
                                                        </div>
<!--                                                        <span class="flects"> <?=$k?> stop</span> -->
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 padflt widfty">
                                                    <span class="timlbl left"> <span class="flname"><span class="sprite refltwo"></span><?=$v['destination']?><span class="fltime"><?=$v['arrival_time']?></span> </span> </span>
                                                    <div class="clearfix"></div>
                                                    <span class="flitrlbl elipsetool"><?=$v['arrival_date']?></span>
                                                    <div class="rndplace"><?=$v['destination_city']?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php
                                if (isset($flight_details[$k+1]) == true) {?>
                                    <div class="layoverdiv hide">
                                        <div class="centovr"> <span class="fa fa-plane"></span> 
                                            Change of planes at <?=$flight_details[$k+1]['origine']?> Airport | 
                                            <span class="fa fa-clock-o"></span> Connection Time : 5h 40m 
                                        </div>
                                    </div>
                                <?php
                                }
                            }?>
                        </div>
                    </div>
                    </div>
                    <!--  Flight Summery Ends Here -->
                    <div class="clearfix"></div>
                    <?php if(is_logged_in_user() == false) { ?>
                    <div class="loginspld">
                                <div class="logininwrap">
                                    <div class="signinhde">
                                        Sign in now to Book Online
                                    </div>
                                    <div class="newloginsectn">
                                        <div class="col-xs-5 celoty nopad">
                                            <div class="insidechs">
                                                <div class="mailenter">
                                                    <input type="text" required name="booking_user_name" id="booking_user_name" maxlength="80"  placeholder="Your mail id" class="newslterinput nputbrd _guest_validate">
                                                </div>
                                                <div class="noteinote">Your booking details will be sent to this email address.</div>
                                                <div class="clearfix"></div>
                                                    <div class="clearfix"></div>
                                                    <div class="havealrdy">
                                                <div class="col-xs-12 nopad">
                                                    <div class="relativemask"> 
                                                        <input type="password" required name="booking_user_password" id="booking_user_password" class="clainput" placeholder="Password" />
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <a class="frgotpaswrd">Forgot Password?</a>
                                                    <div style="" class="hide alert alert-danger"></div>
                                                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="continye col-xs-8 nopad">
                                                    <button class="bookcont" id="continue_as_user">Proceed to Book</button>
                                                </div>
                                                <!-- <div class="havealrdy">
                                                    <div class="squaredThree">
                                                        <input id="alreadyacnt" type="checkbox" name="check" value="None">
                                                        <label for="alreadyacnt"></label>
                                                    </div>
                                                    <label for="alreadyacnt" class="haveacntd">I have an Account</label>
                                                </div> -->
                                                <div class="clearfix"></div>
                                                <div class="twotogle">
                                                    <!-- <div class="cntgust">
                                                        <div class="phoneumber">
                                                            <div class="col-xs-3 nopadding">
                                                                <select class="newslterinput nputbrd _numeric_only " >
                                                                
                                                            </select> 
                                                            </div>
                                                            <div class="col-xs-1 nopadding">
                                                                <div class="sidepo">-</div>
                                                            </div>
                                                            <div class="col-xs-8 nopadding">
                                                                <input type="text" id="booking_user_mobile" placeholder="Mobile Number" class="newslterinput _numeric_only _guest_validate" maxlength="10">
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="noteinote">We'll use this number to send possible update alerts.</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="continye col-xs-8 nopad">
                                                            <button class="bookcont" id="continue_as_guest">Book as Guest</button>
                                                        </div>
                                                    </div> -->
                                                    <div class="alrdyacnt">
                                                        <div class="col-xs-12 nopad">
                                                            <div class="relativemask"> 
                                                                <input type="password" name="booking_user_password" id="booking_user_password" class="clainput" placeholder="Password" />
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <a class="frgotpaswrd">Forgot Password?</a>
                                                            <div style="" class="hide alert alert-danger"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="continye col-xs-8 nopad">
                                                            <button class="bookcont" id="continue_as_user">Proceed to Book</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $no_social=no_social(); if($no_social != 0) {?>
                                        <div class="col-xs-2 celoty nopad linetopbtm">
                                            <div class="orround">OR</div>
                                        </div>
                                        <?php } ?>
                                        <div class="col-xs-5 celoty nopad">
                                            <div class="insidechs booklogin">
                                                <div class="leftpul">
                                                    <?php 
                                                        $social_login1 = 'facebook';
                                                        $social1 = is_active_social_login($social_login1);
                                                        if($social1){
                                                            $GLOBALS['CI']->load->library('social_network/facebook');
                                                            echo $GLOBALS['CI']->facebook->login_button ();
                                                        } 
                                                        $social_login2 = 'twitter';
                                                        $social2 = is_active_social_login($social_login2);
                                                        if($social2){
                                                    ?>
                                                        <a class="logspecify tweetcolor">
                                                            <span class="fa fa-twitter"></span>
                                                            <div class="mensionsoc">Login with Twitter</div>
                                                        </a>
                                                    <?php } 
                                                        $social_login3 = 'googleplus';
                                                        $social3= is_active_social_login($social_login3);
                                                        if($social3){
                                                            $GLOBALS['CI']->load->library('social_network/google');
                                                            echo $GLOBALS['CI']->google->login_button ();
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php } ?>
                </div>
            </div>
           <?php //debug($flight_det); die; ?>
            <div class="bktab2 xlbox <?=$travellers_tab_details_class?>">
                <div class="topalldesc">
                    <div class="col-xs-8 nopadding celtbcel segment_seg">
                        <div class="moreflt spltopbk">
                            <div class="ontypsec">
                                <div class="allboxflt">

                                    <div class="col-xs-3 nopadding width_adjst">
                                        <div class="jetimg"><img class="airline-logo" alt="<?= $flight_det['operator'];?>" src="<?=SYSTEM_IMAGE_DIR . '/airline_logo/'.$flight_det['operator']?>.gif"></div>
                                        <div class="alldiscrpo"><?=ucwords(strtolower($flight_det['operator_name']))?><span class="sgsmal"><?= $flight_det['operator'];?><br><?=$flight_det['flight_number'];?></span></div>
                                    </div>

                                    <div class="col-xs-7 nopadding width_adjst">
                                        <div class="col-xs-5"><span class="airlblxl"><?= $flight_det['origine_city'];?></span><span class="portnme"><?= $flight_det['departure_date'].'  '.$flight_det['departure_time'];?> </span></div>
                                        <div class="col-xs-2"><span class="fadr fa fa-long-arrow-right textcntr"></span></div>
                                        <div class="col-xs-5"><span class="airlblxl"><?= $flight_det['destination_city'];?></span><span class="portnme"><?= $flight_det['arrival_date'].'  '.$flight_det['arrival_time'];?></span></div>
                                    </div>
                                    <div class="col-xs-2 nopadding width_adjst">
                                        <span class="portnme textcntr"><?= $flight_det['duration'];?></span>
                                        <span class="portnme textcntr"><?= $flight_det['no_of_stops'];?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Outer Summary -->
                    <div class="col-xs-4 nopadding celtbcel colrcelo">
                        <div class="bokkpricesml">
                            <div class="travlrs">Travelers: <span class="fa fa-male"></span> <?=$search_data['adult_config']?> |  <span class="fa fa-child"></span> <?=$search_data['child_config']?> |  <span class="infantbay"><img src="<?=$template_images?>infant.png" alt=""></span> <?=$search_data['infant_config']?></div>
                            <div class="totlbkamnt"> Total Amount <?=$currency?> <span class="flight_original_amount"><?=roundoff_number_comm($total_price)?></span></div>
                            <a class="fligthdets" data-toggle="collapse" data-target="#fligtdetails">Flight Details</a>
                         </div>
                    </div>
                </div>
                <div class="hide">
                    <input type="hidden" value="<?=$total_price;?>" id="flight_grand_total">
                </div>
                <div class="clearfix"></div>
                <!-- Segment Details Starts-->
                <div class="collapse splbukdets" id="fligtdetails">
                    <div class="moreflt insideagain">
                        <div class="ontyp">
                         <?php //debug($search_data['trip_type']) ;
                          if ($search_data['trip_type'] == 'multicity') {
                         for ($pi=0; $pi < count($search_data['from_city']); $pi++) {
                    ?>
                             <div class="labltowr arimobold">
                                <?=$search_data['from_city'][$pi].'   '?> to <?='   '.$search_data['to_city'][$pi]?>
                            </div>
                    <?php } } else { ?>
                             <div class="labltowr arimobold">
                                <?=$search_data['from_city'].'   '?> to <?='   '.$search_data['to_city']?>
                            </div>
                    <?php }
                          
                    ?>
                            
                           <?php foreach($flight_details['flights'] as $__flights) { ?>
                            <div class="allboxflt">
                                <div class="col-xs-3 nopadding width_adjst">
                                    <div class="jetimg"><img alt="<?= $__flights['carrier'];?>" src="<?=SYSTEM_IMAGE_DIR . 'airline_logo/'.$__flights['carrier'];?>.gif"></div>
                                    <div class="alldiscrpo"><?=ucwords(strtolower($__flights['carrier_name']));?><span class="sgsmal"><?=$__flights['carrier'];?> <br><?=$__flights['flight_number'];?></span></div>
                                </div>
                                <div class="col-xs-7 nopadding width_adjst">
                                    <div class="col-xs-5"><span class="airlblxl"><?=$__flights['departure_date'].'   '.$__flights['departure_time'];?></span><span class="portnme"><?=$__flights['origin_city'];?></span></div>
                                    <div class="col-xs-2"><span class="fadr fa fa-long-arrow-right textcntr"></span></div>
                                    <div class="col-xs-5"><span class="airlblxl"><?=$__flights['arrival_date'].'   '.$__flights['arrival_time'];?></span><span class="portnme"><?=$__flights['destination_city'];?></span></div>
                                </div>
                                <div class="col-xs-2 nopadding width_adjst">
                                    <span class="portnme textcntr"><?=$__flights['duration']?></span>
                                    <span class="portnme textcntr hide">Stop : 0</span>
                                    <span class="portnmeter textcntr hide">Buiness Class</span>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="fullcard" id="show_reward_point_details_table" style="display:none">
                    <div class="price_htlin">
                        <ul>
                            <li class="baseli hedli">
                                <ul>
                                    <li class="wid20">Flight Price</li>
                                    <li class="wid20">Reward Point discount</li>
                                    <li class="wid20">Total Amount</li>
                                </ul>
                            </li>
                            <li class="baseli secf">
                                <ul class="responsive_li">
                                    <li class="wid20">BDT <?=roundoff_number_comm($total_price)?></li>
                                    <li class="wid20">BDT <span class="reward_discount_amount"></span></li>
                                    <li class="wid20">BDT <span class="new_grand_total"></span></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <!-- Segment Details Ends-->
                <div class="clearfix"></div>
            </div>
            <div <?=(is_logged_in_user() == true ? '' : 'style="display:none"')?> class="bktab2 col-md-8 col-sm-8 nopad fulbuki ">
                <form action="<?=base_url().'index.php/flight/payment/'.$search_data['search_id']?>" method="POST" autocomplete="off" id="checkout-passenger" id="pre-booking-form">
                    <div class="hide">
                        <input type="hidden" required="required" name="search_id"       value="<?=$search_data['search_id'];?>" />
                        <?php $dynamic_params_url = serialized_data($pre_booking_params);?>
                        <input type="hidden" required="required" name="token"       value="<?=$dynamic_params_url;?>" />
                        <input type="hidden" required="required" name="token_key"   value="<?=md5($dynamic_params_url);?>" />
                        <input type="hidden" required="required" name="op"  value="book_flight">
                        <input type="hidden" required="required" name="booking_source"  value="<?=$booking_source?>" readonly>
                    </div>
                    <div class="col-md-12 padleftpay">
                        <div class="wrappay leftboks">
                        <h3 class="inpagehed">Please enter names as on passport.</h3>
                            <div class="comon_backbg">
                                
                                <div class="sectionbuk">
                                    <div id="collapse102" class="collapse in">
                                        <div class="onedept">
                                            <div class="evryicon hide"><span class="fa fa-plane"></span></div>
                                            <div class="pasenger_location hide">
                                                <?php //debug($search_data['trip_type']) ;
                                                  if ($search_data['trip_type'] == 'multicity') {
                                                 for ($pi=0; $pi < count($search_data['from_city']); $pi++) {
                                            ?>
                                                      <h3 class="inpagehedbuk"> 
                                                    <span class="bookingcnt">1.</span> <span class="aptbokname"><?=$search_data['from_city'][$pi]?> - <?=$search_data['to_city'][$pi]?></span>
                                                </h3>
                                            <?php } } else { ?>
                                                      <h3 class="inpagehedbuk"> 
                                                    <span class="bookingcnt">1.</span> <span class="aptbokname"><?=$search_data['from_city']?> - <?=$search_data['to_city']?></span>
                                                </h3>
                                            <?php }
                                                  
                                            ?>
                                                <span class="hwonum">Adult <?=$search_data['adult_config']?></span> 
                                                <span class="hwonum">Child <?=$search_data['child_config']?></span> 
                                                <span class="hwonum">Infant <?=$search_data['infant_config']?></span>
                                            </div>
                                            <div class="clearfix"></div><?php 
                                            $passport_issuing_country_options = generate_options($country_list);
                                            if($search_data['trip_type'] == 'oneway') {
                                                $passport_minimum_expiry_date = date('Y-m-d', strtotime($search_data['depature']));
                                            } else if($search_data['trip_type'] == 'circle') {
                                                $passport_minimum_expiry_date = date('Y-m-d', strtotime($search_data['return']));
                                            } else {
                                                $passport_minimum_expiry_date = date('Y-m-d', strtotime(end($search_data['depature'])));
                                            }
                                            //lowest year wanted
                                            $cutoff = date('Y');
                                            //current year

                                            $now = date('Y', strtotime($passport_minimum_expiry_date));
                                            $day_options    = generate_options(get_day_numbers());
                                            $month_options  = generate_options(get_month_names());
                                            $year_options   = generate_options(get_years($now, $cutoff));
                                            if(!empty($pax_details[0]['passport_expiry_date']))
                                            {
                                                $passport_details = explode("-",$pax_details[0]['passport_expiry_date']);
                                                $cur_passport_expiry_year = $passport_details[0];
                                                $cur_passport_expiry_month = $passport_details[1];
                                                $cur_passport_expiry_date = $passport_details[2];
                                                $cur_passport_number = $pax_details[0]['passport_number'];
                                                $cus_passpost_issue_country = $pax_details[0]['passport_issuing_country'];
                                                $passport_issuing_country_options = generate_options($country_list,(array)$cus_passpost_issue_country);


                                                $now = date('Y', strtotime($passport_minimum_expiry_date));

                                                $day_options    = generate_options(get_day_numbers(),(array)@$cur_passport_expiry_date);
                                                $month_options  = generate_options(get_month_names(),(array)@$cur_passport_expiry_month);
                                               // $year_options   = generate_options(get_years($now, $cutoff),(array)@$cur_passport_expiry_year);
                                                $year_options   = generate_options(get_years($now, $cutoff),(array)@$cur_passport_expiry_year);
                                            }
                                           if ($is_domestic != true) {
                                                $pass_mand = '<sup class="text-danger">*</sup>';
                                                $pass_req = 'required="required"';
                                            } else {
                                                $pass_mand = '';
                                                $pass_req = '';
                                            }

                                            $adult_dp_view = $adult_dp_val = '';
                                            $pass_no_val = '';
                                            $pass_exp_dp_val = '';
                                            $pass_dp_placeholder = 'Passport Expiry Date';
                                              ?>
                                                <div class="pasngr_input pasngrinput _passenger_hiiden_inputs">
                                                <div class="payrow">
                                                <?php
                                                for ($pax_index=1; $pax_index<=$search_data['total_passenger']; $pax_index++) {
                                                $cur_pax_info = is_array($pax_details) ? array_shift($pax_details) : array();
                                                ?>
                                                    <div class="repeatprows">
                                                        <div class="col-md-1 downsrt set_margin">
                                                            <div class="lokter"><?php 
                                                                $pax_type = pax_type($pax_index, $search_data['adult_config'], $search_data['child_config'], $search_data['infant_config']);
                                                                $dp_type = '';
                                                                $dp_wrapper = '';
                                                                $dp_date_val = '';
                                                                switch ($pax_type) {
                                                                    case 'adult' :
                                                                        echo '<span class="fa fa-user hide"></span>';
                                                                        $dp_id = 'adult-date-picker-'.$pax_index;
                                                                        $dp_type = ADULT_DATE_PICKER;
                                                                        
                                                                        break;
                                                                    case 'child' :
                                                                        echo '<span class="fa fa-male hide"></span>';
                                                                        $dp_id = 'child-date-picker-'.$pax_index;
                                                                        $dp_type = CHILD_DATE_PICKER;
                                                        
                                                                        break;
                                                                    case 'infant' :
                                                                        echo '<span class="fa fa-child"></span>';
                                                                        $dp_id = 'infant-date-picker-'.$pax_index;
                                                                        $dp_type = INFANT_DATE_PICKER;
                                                                        
                                                                        break;
                                                                }
                                                                $datepicker_list[] = array($dp_id, $dp_type);
                                                            ?><span class="whoare"><?=(ucfirst($pax_type))?>(<?=$pax_index?>)</span><sup class="text-danger">*</sup> </div>
                                                            </div>
                                                            <div class="col-md-3 set_margin">
                                                                <div class="selectedwrap">
                                                                    <select name="title[]" class="flpayinput name_title">
                                                                        <?=$title_options?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 set_margin">
                                                                <input value="<?=@$cur_pax_info['first_name']?>" required="required" type="text" name="first_name[]" id="passenger-first-name-<?=$pax_index?>" class="payinput clainput alpha <?=(is_logged_in_user() ? 'user_traveller_details' : '')?>" maxlength="45" placeholder="First Name" data-row-id="<?=($pax_index);?>"/>
                                                            </div>
                                                            <div class="col-md-4 set_margin">
                                                                <input value="<?=@$cur_pax_info['last_name']?>" required="required" type="text" name="last_name[]" id="passenger-last-name-<?=$pax_index?>" class="payinput clainput alpha" maxlength="45" placeholder="Last Name" />
                                                            </div>

                                                            <div class="col-md-3 hide set_margin <?=$dp_wrapper?>">
                                                                <input data-format="dd/mm/yy" value="<?=$dp_date_val?>" placeholder="<?=$dob_dp_placeholder?>" type="text" class="payinput adt"  name="date_of_birth[]" readonly <?=(is_adult($pax_index, $search_data['adult_config']) ? 'required="required"' : 'required="required"')?> id="<?=$dp_id?>">
                                                            </div>
                                                        </div>
                                                        
                                                        <?php if($is_domestic == false) { ?>
                                                        <div class="international_passport_content_div">
                                                            <div class="col-xs-4 spllty">
                                                                <span class="formlabel">Passport Number <sup class="text-danger">*</sup></span>
                                                                <div class="relativemask"> 
                                                                    <input value="<?php echo @$cur_passport_number; ?>" type="text" name="passenger_passport_number[]" required="required" id="passenger_passport_number_<?=$pax_index?>" class="payinput clainput" maxlength="10" placeholder="Passport Number" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4 spllty">
                                                                <span class="formlabel">Issuing Country <sup class="text-danger">*</sup></span>
                                                                <div class="selectedwrap">
                                                                    <select name="passenger_passport_issuing_country[]" required id="passenger_passport_issuing_country_<?=$pax_index?>" class="mySelectBoxClass flyinputsnor">
                                                                        <option value="INVALIDIP">Please Select</option>
                                                                       <?=$passport_issuing_country_options?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            $dp_id = 'passport'.$pax_index;
                                                            $datepicker_list[] = array($dp_id, FUTURE_DATE);
                                                            ?>
                                                            <!-- <div class="col-xs-5 spllty">
                                                                    <span class="formlabel">Date of Expire <?=$pass_mand?></span>
                                                                    <div class="relativemask">
                                                                        <div class="col-xs-4 splinmar">
                                                                            <div class="selectedwrap">
                                                                                <select name="passenger_passport_expiry_day[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_day" data-expiry-type="day" id="passenger_passport_expiry_day_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
                                                                                    <option value="INVALIDIP">DD</option>
                                                                                    <?=$day_options;?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4 splinmar">
                                                                            <div class="selectedwrap">
                                                                                <select name="passenger_passport_expiry_month[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_month" data-expiry-type="month" id="passenger_passport_expiry_month_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
                                                                                    <option value="INVALIDIP">MM</option>
                                                                                    <?=$month_options;?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4 splinmar">
                                                                            <div class="selectedwrap">
                                                                                <select name="passenger_passport_expiry_year[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_year" data-expiry-type="year" id="passenger_passport_expiry_year_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
                                                                                    <option value="INVALIDIP">YYYY</option>
                                                                                    <?=$year_options;?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> -->
                                                                <div class="col-xs-4 spllty">
                                                                <span class="formlabel">Date of Expire <sup class="text-danger">*</sup></span>
                                                                <input value="<?=$pass_exp_dp_val?>" placeholder="<?=$pass_dp_placeholder?>" data-format="dd/mm/yy" type="text" id="<?=$dp_id?>" name="passenger_passport_expiry_date[]" class="payinput">
                                                            </div>
                                                           

                                                        </div>
                                                        
                                                        <?php } 
                                                        else { 
                                                            $dp_id = 'passport'.$pax_index;
                                                            $datepicker_list[] = array($dp_id, FUTURE_DATE);
                                                            $passport_number = rand(1111111111,9999999999);
                                                            $passport_issuing_country = 92;
                                                            $temp_passport_expiry_date = date('d-m-Y', strtotime('+5 years'));

                                                            ?>
                                                        <div class="hide">
                                                            <input type="hidden" name="passenger_passport_issuing_country[]" value="<?=$passport_issuing_country;?>" ?>
                                                            <input value="<?=$passport_number?>" type="text" name="passenger_passport_number[]"  id="passenger_passport_number_<?=$pax_index?>" class="payinput clainput" maxlength="10"  />
                                                             <input value="<?=$temp_passport_expiry_date?>"  type="hidden" id="<?=$dp_id?>" name="passenger_passport_expiry_date[]" class="payinput">
                                                        </div>
                                                        
                                                        <?php } ?>
                                                        
                                                        <div class="hide hidden_pax_details">
                                                            <input type="hidden" name="passenger_type[]" value="<?=ucfirst($pax_type)?>">
                                                            <input type="hidden" name="lead_passenger[]" value="<?=(is_lead_pax($pax_index) ? true : false)?>">
                                                            <input type="hidden" name="gender[]" value="1" class="pax_gender">
                                                            <input type="hidden" required="required" name="passenger_nationality[]" id="passenger-nationality-<?=$pax_index?>" value="92">
                                                        </div>
                                                        <?php 
                                            }?>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="comon_backbg">
                            <div class="sectionbuk">
                                <h3 class="contcthdngs">CONTACT DETAILS</h3>
                                <div class="comon_backbg contactDetails">
                            
                            <div class="sectionbuk billingnob contactIn">
                                <?php
                                $option_view = '';
                                $address = @$lead_pax_details['address'];
                                if (isset($trans_mand['BillingAddress']) == false) {
                                    $option_view = 'hide';
                                    if (empty($address)) {
                                        $address = 'Address';
                                    }
                                }
                                ?>
                                <div class="col-xs-6 nopad full_smal_forty">
                                    <div class="col-xs-3 nopadding">
                                        <select class="newslterinput nputbrd _numeric_only" required>
                                            <?php echo diaplay_phonecode($phone_code,$active_data); ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="sidepo">-</div>
                                    </div>
                                    <div class="col-xs-7 nopadding">
                                        <input value="<?=@$lead_pax_details['phone'] == 0 ? '' : @$lead_pax_details['phone'];?>" type="text" name="billing_passenger_contact" id="passenger-contact" placeholder="Mobile Number" class="newslterinput nputbrd _numeric_only" maxlength="10" required="required">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="emailperson col-xs-12 nopad full_smal_forty">
                                         <div class="paylabel">Email Address</div>
                                        <input value="<?=@$lead_pax_details['email']?>" type="text" maxlength="80" required="required" id="billing-email" class="newslterinput nputbrd" placeholder="Email" name="billing_email">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="notese">
                                    Your mobile number will be used only for sending flight related communication.
                                </div>
                                <div class="payrow hide">
                                    <div class="col-md-4">
                                        <div class="paylabel">Street</div>
                                        <input  type="text" maxlength="80" value="Electronic city" required="required" id="street" class="payinput newslterinput nputbrd" name="street">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="paylabel">City</div>
                                        <input type="text" required=""  value="Bangalore" class="payinput" name="city" id="billing_city">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="paylabel">State</div>
                                        <input type="text" required=""  value="Karnataka" class="payinput" name="state" id="billing_state">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="paylabel">Postal Code</div>
                                        <input type="text" required=""  value="560100" class="payinput" name="postal_code" id="postal_code">
                                    </div>
                                    
                                </div>
                                <div class="payrow hide">
                                    <div class="col-md-4">
                                        <div class="paylabel">Country</div>
                                        <select  name="billing_country" id="country" class="flpayinput">
                                            <?=$nationality_options?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="paylabel">Contact</div>
                                        <input value="<?=@$lead_pax_details['phone'] == 0 ? '' : @$lead_pax_details['phone'];?>" type="text" name="billing_passenger_contact" id="passenger-contact" placeholder="Mobile Number" class="payinput newslterinput nputbrd _numeric_only" maxlength="10" required="required">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="paylabel">Postal Code</div>
                                        <input type="text"  value="560100" class="payinput" name="postal_code" id="billing_postal_code">
                                    </div>
                                </div>
                                <div class="payrow hide">
                                    <div class="col-md-4">
                                        <div class="paylabel">Email Address</div>
                                        <input value="<?=@$lead_pax_details['email']?>" type="text" maxlength="80" required="required" id="billing-email" class="payinput newslterinput nputbrd" placeholder="Email" name="billing_email">
                                    </div>
                                </div>
                                <span class="noteclick hide"> After clicking "Book it" you will be redirected to payment gateway. You must complete the process or the transaction will not occur. </span> 
                            </div>
                        </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="clearfix"></div>
                        
                        <div class="col-md-12 nopad">
                            <div class="checkcontent">
                                <div class="squaredThree">
                                    <input type="checkbox" required="" id="terms_cond1" class="filter_airline newtc" name="tc" value="0">
                                    <label for="terms_cond1"></label>
                                </div>
                                <label class="lbllbl" for="terms_cond1">Terms and Conditions</label>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                      
                        <div class="payrowsubmt">
                            <div class="col-md-3 col-xs-3 fulat500 nopad full_mobile">
                            <?php
                                //If single payment option then hide selection and select by default
                                if (count($active_payment_options) == 1) {
                                    $payment_option_visibility = 'hide';
                                    $default_payment_option = 'checked="checked"';
                                } else {
                                    $payment_option_visibility = 'show';
                                    $default_payment_option = '';
                                }
                                
                                ?>
                            <div class="row <?=$payment_option_visibility?>">
                                <?php if (in_array(PAY_NOW, $active_payment_options)) {?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="payment-mode-<?=PAY_NOW?>">
                                        <input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_NOW?>" id="payment-mode-<?=PAY_NOW?>" class="form-control b-r-0" placeholder="Payment Mode">
                                        Pay Now
                                        </label>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if (in_array(PAY_AT_BANK, $active_payment_options)) {?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="payment-mode-<?=PAY_AT_BANK?>">
                                        <input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_AT_BANK?>" id="payment-mode-<?=PAY_AT_BANK?>" class="form-control b-r-0" placeholder="Payment Mode">
                                        Pay At Bank
                                        </label>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="hide">
                             <input type="hidden" required="required" name="reward_amount"  value="0" class="booking_reward_amount">
                                <input type="hidden" name="user_reward_point_details" id="user_reward_point_details" value=""/>
                            </div>
                                <input type="submit" target="_blank" name="flight" value="Continue" id="continue_to_travelport_booking" name="continue" class="paysubmit">
                            </div>
                            <div class="col-md-9 col-xs-3 fulat500 nopad"> </div>
                            <div class="clear"></div>
                            <div class="lastnote"> </div>
                        </div>
                    </div>
                </form>
                </div>
                  
               
            </div>
        </div><!-- paymentpage -->
        </div>
        </div>
    </div>
</div>
<span class="hide">
<input type="hidden" id="pri_passport_min_exp" value="<?=$passport_minimum_expiry_date?>">
</span>
<?php echo $GLOBALS['CI']->template->isolated_view('share/flight_session_expiry_popup');?>
<?php echo $GLOBALS['CI']->template->isolated_view('share/passenger_confirm_popup');?>

<div class="modal fade" id="TermsModal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Terms &amp; Condition</h4>
        </div>
        <div class="modal-body">
          <p>Visa, MasterCard and American Express Card payments are processed through an online payment gateway system (SSL commerce) without any information passing through us. Two-Step verification* protocol implemented by your bank guarantees your transaction will be 100% safe and secure.

GoZayaan, as a Verisign Certified Site, uses the latest 128 bit encryption technology and state-of-the-art vaulted data security to protect your credit card information. We do not retain any credit card information.

GoZayaan offers you the highest standards of security protocols currently available on the internet so as to ensure that your shopping experience is private, safe and secure.

On the instance of a declined credit card payment, alternate payment instructions must be provided to GoZayaan within 72 hours prior to the time of departure; else, the order is liable to be cancelled.

GoZayaan charges a service fee on all domestic airline bookings. In case of cancellation, this fee is non-refundable.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 
$GLOBALS['CI']->current_page->set_datepicker($datepicker_list);
/**
 * check if current print index is of adult or child by taking adult and total pax count
 * @param number $total_pax     total pax count
 * @param number $total_adult   total adult count
 */
function pax_type($pax_index, $total_adult, $total_child, $total_infant)
{
    if ($pax_index <= $total_adult) {
        $pax_type = 'adult';
    } elseif ($pax_index <= ($total_adult+$total_child)) {
        $pax_type = 'child';
    } else {
        $pax_type = 'infant';
    }
    return $pax_type;
}

/**
 * check if current print index is of adult or child by taking adult and total pax count
 * @param number $total_pax     total pax count
 * @param number $total_adult   total adult count
 */
function is_adult($pax_index, $total_adult)
{
    return ($pax_index>$total_adult ?   false : true);
}

/**
 * check if current print index is of adult or child by taking adult and total pax count
 * @param number $total_pax     total pax count
 * @param number $total_adult   total adult count
 */
function is_lead_pax($pax_count)
{
    return ($pax_count == 1 ? true : false);
}
function diaplay_phonecode($phone_code,$active_data)
{
    $list='';
    foreach($phone_code as $code){
    if($active_data['api_country_list_fk']==$code['origin']){
            $selected ="selected";
        }
        else {
            $selected="";
        }
    
        $list .="<option value=".$code['country_code']."  ".$selected." >".$code['country_code']."</option>";
    }
     return $list;
    
}
?>
<script type="text/javascript">
    $('.newtc').click(function() {
        if($(".newtc").prop("checked") == true){
            $('#TermsModal').modal('show');
        } 
    });
</script>
<script type="text/javascript">
    $(".applyreward").click(function() {
        var total_reward_amount = $('#total_reward_amount').val();
        var applied_rewarder_amount = $("#reward_point_new").val();
        var flight_amount = $("#flight_grand_total").val();
        if(applied_rewarder_amount ==''){
            $('.reward_err_msg').text('Please Enter Amount to Submit');

        }else if((parseInt(total_reward_amount)) > (parseInt(applied_rewarder_amount)) == false){
            $('.reward_err_msg').text('Please Enter Valid Amount');
        }else{
            $('.reward_err_msg').text('');
            //minus the amount with original amount
            var flight_amount = $("#flight_grand_total").val() - applied_rewarder_amount;
            var val1 = flight_amount .toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
            $(".reward_discount_amount").text(applied_rewarder_amount);
            $(".new_grand_total").text(val1);
             $(".booking_reward_amount").val(applied_rewarder_amount);
            $(".flight_original_amount").text(val1);
            $('.remove_reward').removeClass('hide');
            $("#show_reward_point_details_table").show(); 
        }
    });
$(".remove_reward").click(function () {
        var flight_amount = $("#flight_grand_total").val();
        $("#reward_point_new").val("");
$(".flight_original_amount").text(flight_amount);
 $("#show_reward_point_details_table").hide();
 $(".booking_reward_amount").val(0);
});
</script>