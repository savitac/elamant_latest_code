<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo  $this->session->userdata('company_name');?></title>
        <?php echo $this->load->view("core/load_css"); ?>
        <link href="<?php echo ASSETS; ?>assets/css/hotel_result.css" rel="stylesheet">
        <!-- <link href="<?php echo ASSETS; ?>assets/css/red_theme_style.css" rel="stylesheet"> -->
        <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script>
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/map.js"></script>
    </head>
    <body>
        <!-- Navigation -->
		<?php 
            if($this->session->userdata('user_logged_in') == 0) { 
            echo $this->load->view('core/header'); 
            } else {
            echo $this->load->view('dashboard/top');     
            }
        ?>

<div class="jumbotron text-center">
<h1>OOOOPS!!!!!!!</h1>
<p class="text-danger">Flight Booking Engine Could Not Process Your Request
!!!</p>
<p class="text-danger">Please Try Again To Process Your New Request</p>
<?php
if (isset($eid) == true and strlen($eid) > 0) {
	?>
	<p class="text-primary">You Can Use <strong><?=$eid?></strong> Reference Number To Talk To Our Customer Support</p>
	<?php
}
?>
<p><a class="btn btn-primary btn-lg" href="<?php echo base_url(); ?>dashboard"
	role="button">Click Here To Start New Search</a></p>
</div>
<?php
if (isset($log_ip_info) and $log_ip_info == true and isset($eid) == true) {
?>

<script>
$(document).ready(function() {
	$.getJSON("http://ip-api.com/json", function(json) {
		$.post(app_base_url+"index.php/ajax/log_event_ip_info/<?php echo $eid?>", json);
	});
});
</script>
<?php
}
?>

