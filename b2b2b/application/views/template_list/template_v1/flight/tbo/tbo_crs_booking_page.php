<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo  $this->session->userdata('company_name');?></title>
        <?php echo $this->load->view("core/load_css"); ?>
        <link href="<?php echo ASSETS; ?>assets/css/index.css" rel="stylesheet">
		<link href="<?php echo ASSETS; ?>assets/css/pre_booking.css" rel="stylesheet">
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery-1.11.0.js">	
		</script>
    </head>
    <body>
	<?php 
        if($this->session->userdata('user_logged_in') == 0) { 
			echo $this->load->view('core/header'); 
		} else {
			echo $this->load->view('dashboard/top'); 	 
		}
	include_once 'process_tbo_response.php';

?>
<?php 
	/*debug($supplier_id);*/
    //debug($search_data);
   /* debug($pre_booking_params);*/

	$adult_count=$search_data['adult_config'];
   $child_count=$search_data['child_config'];
   $infant_count=$search_data['infant_config'];
   $trip_type=$search_data['trip_type'];
   $search_data['search_id']= $search_id; 

    foreach ($pre_booking_params as $key => $crslist) {

    $adult=($crslist['adult_price'] + $crslist['adult_tax'] + $crslist['adult_markup']) * $adult_count ;
    $child=($crslist['child_price'] + $crslist['child_tax'] + $crslist['child_markup']) * $child_count ;
    $infant=($crslist['infant_price'] + $crslist['infant_tax'] + $crslist['infant_markup'] ) * $infant_count ;
    $total_fare=$adult + $child + $infant;
    /*$path=$GLOBALS ['CI']->template->domain_images('crs_airline_logo/'.$crslist['onward_path']);
	$path_b=$GLOBALS ['CI']->template->domain_images('crs_airline_logo/'.$crslist['return_path']) ;*/
      $path="";
      $path_b="";
    }

if(@$pre_booking_params[0]['connection_type']=="yes"){
	/*debug($pre_booking_params);
	exit("...");*/
	$onword_airline_name=$crslist['airline_namee'];
	$return_airline_name=$crslist['return_airline_name'];		
}else{
	$onword_airline_name=$crslist['airline_name'];
	$return_airline_name=$crslist['airline_name'];
}
 
    // echo $total_fare;
    
    if($trip_type=="circle" && $pre_booking_params[0]['connection_type']!="yes"){
    	$total_fare=$total_fare*2;
    }
	include_once 'process_tbo_response.php';
	
	//Flight Booking Summary
	//$FareDetails = $pre_booking_summery['FareDetails']['b2c_PriceDetails'];
	//$PassengerFareBreakdown = $pre_booking_summery['PassengerFareBreakdown'];
	//$SegmentDetails = $pre_booking_summery['SegmentDetails'];
	//$SegmentSummary = $pre_booking_summery['SegmentSummary'];
	/********************************* Convenience Fees *********************************/
	//$flight_total_amount = $FareDetails['TotalFare']+$convenience_fees;
	/********************************* Convenience Fees *********************************/
	$currency_symbol = '$';
	$is_domestic = $search_data['is_domestic'];
	//Segment Summary and Details
	$flight_segment_details =  flight_segment_details($pre_booking_params,$trip_type);
	if (isset($is_domestic)) {
		$pass_mand = '<sup class="text-danger">*</sup>';
		$pass_req = 'required="required"';
	} else {
		$pass_mand = '';
		$pass_req = '';
	}
	$mandatory_filed_marker = '<sup class="text-danger">*</sup>';
	//Jaganath
	$is_domestic_flight = $search_data['is_domestic_flight'];

		$temp_passport_expiry_date = date('Y-m-d', strtotime('+5 years'));
		$static_passport_details = array();
		$static_passport_details['passenger_passport_expiry_day'] = date('d', strtotime($temp_passport_expiry_date));
		$static_passport_details['passenger_passport_expiry_month'] = date('m', strtotime($temp_passport_expiry_date));
		$static_passport_details['passenger_passport_expiry_year'] = date('Y', strtotime($temp_passport_expiry_date));

	if(is_logged_in_user()) {
		$review_active_class = ' success ';
		$review_tab_details_class = '';
		$review_tab_class = ' inactive_review_tab_marker ';
		$travellers_active_class = ' active ';
		$travellers_tab_details_class = ' gohel ';
		$travellers_tab_class = ' travellers_tab_marker ';
	} else {
		$review_active_class = ' active ';
		$review_tab_details_class = ' gohel ';
		$review_tab_class = ' review_tab_marker ';
		$travellers_active_class = '';
		$travellers_tab_details_class = '';
		$travellers_tab_class = ' inactive_travellers_tab_marker ';
	}
?>
<style>
	.topssec::after{display:none;}
</style>
<div class="fldealsec">
  <div class="container">
	<div class="tabcontnue">
	<div class="col-xs-4 nopadding">
			<div class="rondsts <?=$review_active_class?>">
			<a class="taba core_review_tab <?=$review_tab_class?>" id="stepbk1">
				<div class="iconstatus fa fa-eye"></div>
				<div class="stausline">Review</div>
			</a>
			</div>
		</div>
		<div class="col-xs-4 nopadding">
			<div class="rondsts <?=$travellers_active_class?>">
			<a class="taba core_travellers_tab <?=$travellers_tab_class?>" id="stepbk2">
				<div class="iconstatus fa fa-group"></div>
				<div class="stausline">Travellers</div>
			</a>
			</div>
		</div>
		<div class="col-xs-4 nopadding">
			<div class="rondsts">
			<a class="taba" id="stepbk3">
				<div class="iconstatus fa fa-money"></div>
				<div class="stausline">Payments</div>
			</a>
			</div>
		</div>
	 </div>
	
  </div>
</div>

<div class="clearfix"></div>
<div class="alldownsectn">
	<div class="container">
		<?php if($is_price_Changed == true) {?>
			<div class="farehd arimobold">
				<span class="text-danger">* Price has been changed from supplier end</span>
			</div>
		<?php } ?>
		<div class="ovrgo">
			<div class="bktab1 xlbox <?=$review_tab_details_class?>">
				<!-- Jaganath - Fare Summery -->
				<div class="col-xs-4 nopadding rit_summery">
					<?php echo get_fare_summary($pre_booking_params,$search_data);?>
				</div>
				<div class="col-xs-8 nopadding full_summery_tab">
					<div class="fligthsdets">
						<div class="flitab1">
							<!-- Segment Details Starts-->
							<div class="moreflt boksectn">
								<?=$flight_segment_details['segment_full_details'];?>
							</div>
							<!-- Segment Details Ends-->
							<div class="clearfix"></div>
							<div class="sepertr"></div>
							<!-- LOGIN SECTION STARTS -->
							<?php 
				if($this->session->userdata('user_logged_in') == true){ ?>
				<div class="col-xs-6 nopad fulat500" style="margin-bottom:20px;">
				<input type="button" id="next_review" class="paysubmit" value="Continue">
                <br/><br/>
		        </div>
		        <?php }else{?>
	            <div class="pre_summery test col-md-5" style="">
					<div class="signdiv" id="loginsec">
						<div class="labltowr" align="center" id="guest_txt">Login</div>
						<div class="insigndiv">
							<div class="ritsform">
									<div class="ritpul">
							
							
					<div id="login_form">				
					
                         <div class="col-md-12">
                             <div class="selectft">
                                
                                <input type="text" class="form-control log_ft" id="email" name="email" placeholder="User Name" required>
                                <span id="email-error" class="error" for="email">  </span>
                            </div>
                        </div>
  
                        <div class="col-md-12">
                             <div class="selectft">
                                
                                <input type="password" class="form-control log_ft" id="password" name="password" placeholder="Password" required>
                                <span id="password-error" class="error" for="password">  </span>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <a class="btn btn_logsub mycent" value="Login"  onclick="form_submits_bookings('guest_login')"> Login </a>
                        
                    
                    </div>
                    
                   <div id="guest_from" style="display:none;">				
					
                         <div class="col-md-6">
                             <div class="selectft">
                                
                                <input type="text" class="form-control log_ft" id="mobile" name="mobile" placeholder="Mobile Number" required>
                               <span id="mobile-error" class="error" for="mobile">  </span>
                            </div>
                        </div>
  
                        <div class="col-md-6">
                             <div class="selectft">
                                
                                <input type="text" class="form-control log_ft" id="g_email" name="g_email" placeholder="Email" required>
                                <span id="g_email-error" class="error" for="email"> </span>
                                <span id="account_already" class="error" for=""> </span>
                                
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <a class="btn btn_logsub mycent" onclick="form_submits_bookings('guest_continue')" >   Continue </a>
                    </div>
                  
                  
                    <div class="orcent" style="display:block;">OR</div>
                   <div id="sug_guest" style="display:block;">  
                   <a class="btn btn_logsub mycent" class="btn btn_logsub mycent" onclick="change_guest_form('1')" > Continue As Guest </a>
                   </div>
                   <div id="sug_login" style="display:none;">
                   <a class="btn btn_logsub mycent" class="btn btn_logsub mycent" onclick="change_guest_form('2')" > Login As User </a>
                   </div>
                    
 
									
								</div>
							</div>
					
						</div>
					</div>             
				</div>
				
				<div class="col-xs-8 nopadding full_log_tab">
	  <div class="fligthsdets">
		<div class="flitab1">
				<div class="clearfix"></div>
				<div class="sepertr"></div>
				<div class="sepertr"></div>				
				</div>
				</div>
		 </div>
                </div>    
	            <?php }?> 
							<!-- LOGIN SECTION ENDS -->
						</div>
					</div>
				</div>
			</div>
			<div class="bktab2 xlbox <?=$travellers_tab_details_class?>">
				<div class="topalldesc">
					<div class="col-xs-8 nopadding celtbcel segment_seg">
						<?=$flight_segment_details['segment_abstract_details'];?>
					</div>
					<!-- Outer Summary -->
					<div class="col-xs-4 nopadding celtbcel colrcelo">
						<div class="bokkpricesml">
							<div class="travlrs">Travelers: <span class="fa fa-male"></span> <?php         
							echo $search_data['adult_config'];?> |  <span class="fa fa-child"></span> <?php echo $search_data['child_config'];?> |  <span class="infantbay"><img src="<?php  echo ASSETS; ?>assets/images/infant.png" alt="" /></span> <?php echo $search_data['infant_config'];?></div>
							<div class="totlbkamnt"> Total Amount <?php echo $currency_symbol;?> <?php echo  $total_fare; ?></div>
							<a class="fligthdets" data-toggle="collapse" data-target="#fligtdetails">Flight Details</a>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- Segment Details Starts-->
				<div class="collapse splbukdets" id="fligtdetails">
					<div class="moreflt insideagain">
						<?=$flight_segment_details['segment_full_details'];?>
					</div>
				</div>
				<!-- Segment Details Ends-->
				<div class="clearfix"></div>
				<div class="padpaspotr">
					<div class="col-xs-8 nopadding tab_pasnger">
						<div class="fligthsdets">
							<?php
								/**
								 * Collection field name 
								 */
								//Title, Firstname, Middlename, Lastname, Phoneno, Email, PaxType, LeadPassenger, Age, PassportNo, PassportIssueDate, PassportExpDate
								$total_adult_count	= is_array($search_data['adult_config']) ? array_sum($search_data['adult_config']) : intval($search_data['adult_config']);
								$total_child_count	= is_array($search_data['child_config']) ? array_sum($search_data['child_config']) : intval($search_data['child_config']);
								$total_infant_count	= is_array($search_data['infant_config']) ? array_sum($search_data['infant_config']) : intval($search_data['infant_config']);
								//------------------------------ DATEPICKER START
								$i = 1;
								$datepicker_list = array();
								if ($total_adult_count > 0) {
									for ($i=1; $i<=$total_adult_count; $i++) {
										$datepicker_list[] = array('adult-date-picker-'.$i, ADULT_DATE_PICKER);
									}
								}
								
								if ($total_child_count > 0) {
									//id should be auto picked so initialize $i to previous value of $i
									for ($i=$i; $i<=($total_child_count+$total_adult_count); $i++) {
										$datepicker_list[] = array('child-date-picker-'.$i, CHILD_DATE_PICKER);
									}
								}
								if ($total_infant_count > 0) {
									//id should be auto picked so initialize $i to previous value of $i
									for ($i=$i; $i<=($total_child_count+$total_adult_count+$total_infant_count); $i++) {
										$datepicker_list[] = array('infant-date-picker-'.$i, INFANT_DATE_PICKER);
									}
								}
								//$GLOBALS['CI']->current_page->set_datepicker($datepicker_list);
								//------------------------------ DATEPICKER END
								$total_pax_count	= $total_adult_count+$total_child_count+$total_infant_count;
								//First Adult is Primary and and Lead Pax
								$adult_enum = $child_enum = get_enum_list('title');
								$gender_enum = get_enum_list('gender');
								unset($adult_enum[MASTER_TITLE]); // Master is for child so not required
								unset($child_enum[MASTER_TITLE]); // Master is not supported in TBO list
								$adult_title_options = generate_options($adult_enum, false, true);
								$child_title_options = generate_options($child_enum, false, true);
								$gender_options	= generate_options($gender_enum);
								$nationality_options = generate_options($iso_country_list, array(INDIA_CODE));//FIXME get ISO CODE --- ISO_INDIA
								$passport_issuing_country_options = generate_options($country_list);
								if($search_data['trip_type'] == 'oneway') {
									$passport_minimum_expiry_date = date('Y-m-d', strtotime($search_data['depature']));
								} else if($search_data['trip_type'] == 'circle') {
									$passport_minimum_expiry_date = date('Y-m-d', strtotime($search_data['return']));
								} else {
									$passport_minimum_expiry_date = date('Y-m-d', strtotime(end($search_data['depature'])));
								}
								//lowest year wanted
								$cutoff = date('Y', strtotime('+20 years', strtotime($passport_minimum_expiry_date)));
								//current year
								$now = date('Y', strtotime($passport_minimum_expiry_date));
								$day_options	= generate_options(get_day_numbers());
								$month_options	= generate_options(get_month_names());
								$year_options	= generate_options(get_years($now, $cutoff));
								/**
								 * check if current print index is of adult or child by taking adult and total pax count
								 * @param number $total_pax		total pax count
								 * @param number $total_adult	total adult count
								 */
								function pax_type($pax_index, $total_adult, $total_child, $total_infant)
								{
									if ($pax_index <= $total_adult) {
										$pax_type = 'adult';
									} elseif ($pax_index <= ($total_adult+$total_child)) {
										$pax_type = 'child';
									} else {
										$pax_type = 'infant';
									}
									return $pax_type;
								}
								/**
								 * check if current print index is of adult or child by taking adult and total pax count
								 * @param number $total_pax		total pax count
								 * @param number $total_adult	total adult count
								 */
								function is_adult($pax_index, $total_adult)
								{
									return ($pax_index>$total_adult ?	false : true);
								}
								/**
								 * check if current print index is of adult or child by taking adult and total pax count
								 * @param number $total_pax		total pax count
								 * @param number $total_adult	total adult count
								 */
								function is_lead_pax($pax_count)
								{
									return ($pax_count == 1 ? true : false);
								}

									function diaplay_phonecode($phone_code,$active_data)
						{

							// debug($phone_code);exit;
							$list='';
							foreach($phone_code as $code){
							if(!empty($user_country_code)){
							if($user_country_code==$code['country_code']){
							$selected ="selected";
							}
							else {
							$selected="";
							}
							}
							else{

							if($active_data['api_country_list_fk']==$code['origin']){
							$selected ="selected";
							}
							else {
							$selected="";
							}
							}


							$list .="<option value=".$code['name']." ".$code['country_code']."  ".$selected." >".$code['name']." ".$code['country_code']."</option>";
							}   
							return $list;

						} 
								//debug($search_data); 
								?>
							<form action="<?=base_url().'index.php/flight/pre_crs_booking/'.$search_data['search_id']?>" method="POST" autocomplete="off">
								<div class="">
									<input type="hidden" required="required" name="search_id" value="<?=$search_data['search_id'];?>" />
									<?php $dynamic_params_url = serialized_data($pre_booking_params);?>
									<input type="hidden" required="required" name="token"		value="<?=$dynamic_params_url;?>" />
									<input type="hidden" name="supplier_id" value="<?=@$supplier_id?>" >
									<input type="hidden" required="required" name="token_key"	value="<?=md5($dynamic_params_url);?>" />
									<input type="hidden" required="required" name="op"			value="book_room">
									<input type="hidden" required="required" name="booking_source"		value="<?=$booking_source?>" readonly>
									<!--<input type="hidden" required="required" name="provab_auth_key" value="?=$ProvabAuthKey ?>" readonly>
								--></div>
								<div class="flitab1">
									<div class="moreflt boksectn">
										<div class="ontyp">
											<div class="labltowr arimobold">Please enter names as on passport. </div>
											<?php
												$pax_index = 1;
												$lead_pax_details = @$pax_details[0];
												if(is_logged_in_user()) {
												$traveller_class = ' user_traveller_details ';
												} else {
												$traveller_class = '';
												}
													for($pax_index=1; $pax_index <= $total_pax_count; $pax_index++) {//START FOR LOOP FOR PAX DETAILS
													$cur_pax_info = is_array($pax_details) ? array_shift($pax_details) : array();
													$pax_type = pax_type($pax_index, $total_adult_count, $total_child_count, $total_infant_count);
													?>
											<div class="pasngrinput _passenger_hiiden_inputs">
												<div class=" hidden_pax_details">
													<input type="hidden" name="passenger_type[]" value="<?=ucfirst($pax_type)?>">
													<input type="hidden" name="lead_passenger[]" value="<?=(is_lead_pax($pax_index) ? true : false)?>">
													<input type="hidden" name="gender[]" value="1" class="pax_gender">
													<input type="hidden" required="required" name="passenger_nationality[]" id="passenger-nationality-<?=$pax_index?>" value="92">
												</div>
												<div class="col-xs-1 nopadding full_dets_aps">
													<div class="adltnom"><?=ucfirst($pax_type)?><?=$mandatory_filed_marker?></div>
												</div>
												<div class="col-xs-11 nopadding full_dets_aps">
													<div class="inptalbox">
														<div class="col-xs-3 spllty">
															<div class="selectedwrap">
																<select class="mySelectBoxClass flyinputsnor name_title" name="name_title[]" required="required">
																<?php echo (is_adult($pax_index, $total_adult_count) ? $adult_title_options : $child_title_options)?>
																</select>
															</div>
														</div>
														<div class="col-xs-5 spllty">
															<input value="<?=@$cur_pax_info['first_name']?>" required="required" type="text" name="first_name[]" id="passenger-first-name-<?=$pax_index?>" class="clainput alpha <?=$traveller_class?>" maxlength="45" placeholder="Enter First Name" data-row-id="<?=($pax_index);?>"/>
														</div>
														<div class="col-xs-4 spllty">
															<input value="<?=@$cur_pax_info['last_name']?>" required="required" type="text" name="last_name[]" id="passenger-last-name-<?=$pax_index?>" class="clainput alpha" maxlength="45" placeholder="Enter Last Name" />
														</div>
														<?php if($pax_type == 'infant') {//Only For Infant?>
														<div class="col-xs-6 spllty infant_dob_div">
															<div class="col-xs-4 nopadding"><span class="fmlbl">Date of Birth <?=$mandatory_filed_marker?></span></div>
															<div class="col-xs-8 nopadding">
																<input placeholder="DOB" type="text" class="clainput"  name="date_of_birth[]" readonly="readonly" <?=(is_adult($pax_index, $total_adult_count) ? 'required="required"' : 'required="required"')?> id="<?=strtolower(pax_type($pax_index, $total_adult_count, $total_child_count, $total_infant_count))?>-date-picker-<?=$pax_index?>">
															</div>
														</div>
														<?php } else{ //Adult/Child
															if($pax_type == 'adult') {
																$static_date_of_birth = date('Y-m-d', strtotime('-30 years'));;
															} else if($pax_type == 'child') {
																$static_date_of_birth = date('Y-m-d', strtotime('-8 years'));;
															}
															?>
														<div class="adult_child_dob_div ">
															<input type="hidden" name="date_of_birth[]" value="<?=$static_date_of_birth?>">
														</div>
														<?php } ?>
														<div class="clearfix"></div>
														<!-- Passport Section Starts -->
														<div class="passport_content_div">
															<?php 
                              $is_domestic_flight=true;
                              if($is_domestic_flight == false) { //For Internatinal Travel?>
															<div class="international_passport_content_div">
																<div class="col-xs-4 spllty">
																	<span class="formlabel">Passport Number <?=$pass_mand?></span>
																	<div class="relativemask"> 
																		<input type="text" name="passenger_passport_number[]" <?=$pass_req?> id="passenger_passport_number_<?=$pax_index?>" class="clainput" maxlength="10" placeholder="Passport Number" />
																	</div>
																</div>
																<div class="col-xs-3 spllty">
																	<span class="formlabel">Issuing Country <?=$pass_mand?></span>
																	<div class="selectedwrap">
																		<select name="passenger_passport_issuing_country[]" <?=$pass_req?> id="passenger_passport_issuing_country_<?=$pax_index?>" class="mySelectBoxClass flyinputsnor">
																			<option value="INVALIDIP">Please Select</option>
																			<?=$passport_issuing_country_options?>
																		</select>
																	</div>
																</div>
																<div class="col-xs-5 spllty">
																	<span class="formlabel">Date of Expire <?=$pass_mand?></span>
																	<div class="relativemask">
																		<div class="col-xs-4 splinmar">
																			<div class="selectedwrap">
																				<select name="passenger_passport_expiry_day[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_day" data-expiry-type="day" id="passenger_passport_expiry_day_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
																					<option value="INVALIDIP">DD</option>
																					<?=$day_options;?>
																				</select>
																			</div>
																		</div>
																		<div class="col-xs-4 splinmar">
																			<div class="selectedwrap">
																				<select name="passenger_passport_expiry_month[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_month" data-expiry-type="month" id="passenger_passport_expiry_month_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
																					<option value="INVALIDIP">MM</option>
																					<?=$month_options;?>
																				</select>
																			</div>
																		</div>
																		<div class="col-xs-4 splinmar">
																			<div class="selectedwrap">
																				<select name="passenger_passport_expiry_year[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_year" data-expiry-type="year" id="passenger_passport_expiry_year_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
																					<option value="INVALIDIP">YYYY</option>
																					<?=$year_options;?>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
															<div class="pull-right text-danger " id="passport_error_msg_<?=$pax_index?>"></div>
															</div>
															<?php } else { //For Domestic Travel, Set Static Passport Data
																$passport_number = rand(1111111111,9999999999);
																$passport_issuing_country = 92;
																?>
															<div class="domestic_passport_content_div ">
																<input type="hidden" name="passenger_passport_number[]" value="<?=$passport_number?>" id="passenger_passport_number_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_issuing_country[]" value="<?=$passport_issuing_country?>" id="passenger_passport_issuing_country_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_expiry_day[]" value="<?=$static_passport_details['passenger_passport_expiry_day']?>" id="passenger_passport_expiry_day_<?=$pax_index?>">  
																<input type="hidden" name="passenger_passport_expiry_month[]" value="<?=$static_passport_details['passenger_passport_expiry_month']?>" id="passenger_passport_expiry_month_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_expiry_year[]" value="<?=$static_passport_details['passenger_passport_expiry_year']?>" id="passenger_passport_expiry_year_<?=$pax_index?>">
															</div>
															<?php }?>
														</div>
														<!-- Passport Section Ends-->
													</div>
												</div>
											</div>
											<?php
												}//END FOR LOOP FOR PAX DETAILS
												?>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="sepertr"></div>
									<div class="clearfix"></div>
									<div class="contbk">
										<div class="contcthdngs">CONTACT DETAILS</div>
										<div class="col-xs-6 nopad full_smal_forty">
											<div class="col-xs-2 nopadding">
											 
												<div class="">
													<input type="hidden" name="billing_country" value="92">
													<input type="hidden" name="billing_city" value="test">
													<input type="hidden" name="billing_zipcode" value="test">
													<input type="hidden" name="billing_address_1" value="test">
												</div>
												
											</div>
											<div class="col-xs-2 nopadding">
												<div class="sidepo">
													<select name="country_code" class="newslterinput nputbrd _numeric_only " required="required">
											<?php 
												//debug($phone_code);exit;
												echo diaplay_phonecode($phone_code,$active_data, $user_country_code); ?>
										</select> 
													
												</div>
											</div>
											<div class="col-xs-8 nopadding">
												<input value="<?=@$lead_pax_details['phone'] == 0 ? '' : @$lead_pax_details['phone'];?>" type="text" name="passenger_contact" id="passenger-contact" placeholder="Mobile Number" class="newslterinput nputbrd _numeric_only" maxlength="10" required="required">
											</div>
											<div class="clearfix"></div>
											<div class="emailperson col-xs-12 nopad full_smal_forty">
												<input value="<?=@$lead_pax_details['email']?>" type="text" maxlength="80" required="required" id="billing-email" class="newslterinput nputbrd" placeholder="Email" name="billing_email">
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="notese">Your mobile number will be used only for sending flight related communication.</div>
									</div>
									<div class="clikdiv">
										<div class="squaredThree">
											<input id="terms_cond1" type="checkbox" name="tc" checked="checked" required="required">
											<label for="terms_cond1"></label>
										</div>
										<span class="clikagre">
										
										Terms and Conditions
										</span>
									</div>
									<div class="clearfix"></div>
									<div class="loginspld">
										<div class="collogg">
											<div class="sectionbuk">
										<div class="paylater_show" >
											<ul class="radiochecks">
											<?php 
											 $this->session->userdata('user_logged_in');
											if($this->session->userdata('user_logged_in') == 0) { ?>
											<li class="li-option-2" id="commissionable_li_2" >
													<label for="s-option-1" >
														<input type="radio" id="s-option-1" value="<?=PAY_AT_BANK?>" name="payment_method" checked> 
														<label for="s-option-1" class="check"></label>
														<label for="s-option-1">PAY OFFLINE</label>
													</label>
											
												</li>
												<?php } else{ ?> 
												<li class="li-option-2" id="commissionable_li_2" >
													<label for="s-option-1" >
														<input type="radio" id="s-option-1" value="<?=PAY_NOW?>" name="payment_method" checked> 
														<label for="s-option-1" class="check"></label>
														<label for="s-option-1"><?php echo "DEPOSIT"; ?></label>
													</label>
												</li>
												<?php } ?>
											</ul>
										</div>
										</div>
											<div class="continye col-sm-3 col-xs-6">
												<button type="submit" id="flip" class="bookcont">Continue</button>
											</div>
											<div class="clearfix"></div>
											<div class="sepertr"></div>
											<div class="temsandcndtn">
												Most countries require travelers to have a passport valid for more than 3 to 6 months from the date of entry into or exit from the country. Please check the exact rules for your destination country before completing the booking.
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php if(is_logged_in_user() == true) { ?>
					<div class="col-xs-4 nopadding">
						<div class="insiefare">
							<div class="farehd arimobold">Passenger List</div>
							<div class="fredivs">
								<div class="psngrnote">
									<?php
										if(valid_array($traveller_details)) {
											$traveller_tab_content = 'You have saved passenger details in your list,on typing, passenger details will auto populate.';
										} else {
											$traveller_tab_content = 'You do not have any passenger saved in your list, start adding passenger so that you do not have to type every time. <a href="'.base_url().'index.php/user/profile?active=traveller" target="_blank">Add Now</a>';
										}
										?>
									<?=$traveller_tab_content;?>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<span class="">
<input type="hidden" id="pri_passport_min_exp" value="<?=$passport_minimum_expiry_date?>">
</span>
<?php
/*
 * Jaganath
 * Flight segment details
 * Outer summary and Inner Summary
 */
function flight_segment_details($pre_booking_params,$trip_type)
{
	$dir=air_lin_Logo_DIR;  

  foreach ($pre_booking_params as $key => $crslist) {
  	//$path=$GLOBALS ['CI']->template->domain_images('crs_airline_logo/'.$crslist['onward_path']);
  	$path=$dir.$crslist['onward_path'];  
  	//$path_b=$GLOBALS ['CI']->template->domain_images('crs_airline_logo/'.$crslist['return_path']) ;
  	if($trip_type=='oneway')
  	{
  		$inner_summary='<div class="ontyp">
   <div class="labltowr arimobold">'.substr($crslist['from_city'],-5).'to '.substr($crslist['to_city'],-5).'<strong>('.$crslist['onwards_duration'].' )</strong></div>
   <div class="allboxflt">
      <div class="col-xs-3 nopadding width_adjst">
         <div class="jetimg"><img alt="UL" src="'. $path.'"></div>
         <div class="alldiscrpo">'.$crslist['airline_name'].'<span class="sgsmal">'.$crslist['onward_code']. ' <br>'.$crslist['onwards_flight_number'].'</span></div>
      </div>
      <div class="col-xs-7 nopadding width_adjst">
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['onwards_departure_time'].'</span><span class="portnme">'.$crslist['from_city'].'</span></div>
         <div class="col-xs-2"><span class="fadr fa fa-long-arrow-right textcntr"></span></div>
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['onwards_arrival_time'].'</span><span class="portnme">'.$crslist['to_city'].'</span></div>
      </div>
      <div class="col-xs-2 nopadding width_adjst"><span class="portnme textcntr">'.$crslist['onwards_duration']. '</span><span class="portnme textcntr">Stop : 0</span></div>
   </div>
   
</div>';
$outer_summary='<div class="ontyp">
   <div class="labltowr arimobold">'.substr($crslist['from_city'],-5).'to '.substr($crslist['to_city'],-5).'<strong>('.$crslist['onwards_duration'].' )</strong></div>
   <div class="allboxflt">
      <div class="col-xs-3 nopadding width_adjst">
         <div class="jetimg"><img alt="UL" src="'. $path.'"></div>
         <div class="alldiscrpo">'.$crslist['airline_name'].'<span class="sgsmal">'.$crslist['onward_code']. ' <br>'.$crslist['onwards_flight_number'].'</span></div>
      </div>
      <div class="col-xs-7 nopadding width_adjst">
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['onwards_departure_time'].'</span><span class="portnme">'.$crslist['from_city'].'</span></div>
         <div class="col-xs-2"><span class="fadr fa fa-long-arrow-right textcntr"></span></div>
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['onwards_arrival_time'].'</span><span class="portnme">'.$crslist['to_city'].'</span></div>
      </div>
      <div class="col-xs-2 nopadding width_adjst"><span class="portnme textcntr">'.$crslist['onwards_duration']. '</span><span class="portnme textcntr">Stop : 0</span></div>
   </div>
   
</div>';

  	}
  	if($trip_type=='circle')
  	{
  		/*debug($pre_booking_params); 
  		exit("..");*/
  		$inner_summary='<div class="ontyp">
   <div class="labltowr arimobold">'.substr($crslist['from_city'],-5).'to '.substr($crslist['to_city'],-5).'<strong>('.$crslist['onwards_duration'].' )</strong></div>
   <div class="allboxflt">
      <div class="col-xs-3 nopadding width_adjst">
         <div class="jetimg"><img alt="UL" src="'. $path.'"></div>
         <div class="alldiscrpo">'.$onword_airline_name.'<span class="sgsmal">'.$crslist['onward_code']. ' <br>'.$crslist['onwards_flight_number'].'</span></div>
      </div>
      <div class="col-xs-7 nopadding width_adjst">
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['onwards_departure_time'].'</span><span class="portnme">'.$crslist['from_city'].'</span></div>
         <div class="col-xs-2"><span class="fadr fa fa-long-arrow-right textcntr"></span></div>
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['onwards_arrival_time'].'</span><span class="portnme">'.$crslist['to_city'].'</span></div>
      </div>
      <div class="col-xs-2 nopadding width_adjst"><span class="portnme textcntr">'.$crslist['onwards_duration']. '</span><span class="portnme textcntr">Stop : 0</span></div>
   </div>
   
</div>
<div class="ontyp">
   <div class="labltowr arimobold">'.substr($crslist['return_from_city'],-5).'to '.substr($crslist['return_to_city'],-5).'<strong>('.$crslist['return_duration'].' )</strong></div>
   <div class="allboxflt">
      <div class="col-xs-3 nopadding width_adjst">
         <div class="jetimg"><img alt="UL" src="'. $path_b.'"></div>
         <div class="alldiscrpo">'.$return_airline_name.'<span class="sgsmal">'.$crslist['return_code']. ' <br>'.$crslist['return_flight_number'].'</span></div>
      </div>
      <div class="col-xs-7 nopadding width_adjst">
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['return_departure_time'].'</span><span class="portnme">'.$crslist['return_from_city'].'</span></div>
         <div class="col-xs-2"><span class="fadr fa fa-long-arrow-right textcntr"></span></div>
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['return_arrival_time'].'</span><span class="portnme">'.$crslist['return_to_city'].'</span></div>
      </div>
      <div class="col-xs-2 nopadding width_adjst"><span class="portnme textcntr">'.$crslist['return_duration']. '</span><span class="portnme textcntr">Stop : 0</span></div>
   </div>
   
</div>';
$outer_summary='<div class="ontyp">
   <div class="labltowr arimobold">'.substr($crslist['from_city'],-5).'to '.substr($crslist['to_city'],-5).'<strong>('.$crslist['onwards_duration'].' )</strong></div>
   <div class="allboxflt">
      <div class="col-xs-3 nopadding width_adjst">
         <div class="jetimg"><img alt="UL" src="'. $path.'"></div>
         <div class="alldiscrpo">'.$crslist['airline_name'].'<span class="sgsmal">'.$crslist['onward_code']. ' <br>'.$crslist['onwards_flight_number'].'</span></div>
      </div>
      <div class="col-xs-7 nopadding width_adjst">
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['onwards_departure_time'].'</span><span class="portnme">'.$crslist['from_city'].'</span></div>
         <div class="col-xs-2"><span class="fadr fa fa-long-arrow-right textcntr"></span></div>
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['onwards_arrival_time'].'</span><span class="portnme">'.$crslist['to_city'].'</span></div>
      </div>
      <div class="col-xs-2 nopadding width_adjst"><span class="portnme textcntr">'.$crslist['onwards_duration']. '</span><span class="portnme textcntr">Stop : 0</span></div>
   </div>
   
</div>
<div class="ontyp">
   <div class="labltowr arimobold">'.substr($crslist['return_from_city'],-5).'to '.substr($crslist['return_to_city'],-5).'<strong>('.$crslist['return_duration'].' )</strong></div>
   <div class="allboxflt">
      <div class="col-xs-3 nopadding width_adjst">
         <div class="jetimg"><img alt="UL" src="'. $path_b.'"></div>
         <div class="alldiscrpo">'.$crslist['airline_namee'].'<span class="sgsmal">'.$crslist['return_code']. ' <br>'.$crslist['return_flight_number'].'</span></div>
      </div>
      <div class="col-xs-7 nopadding width_adjst">
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['return_departure_time'].'</span><span class="portnme">'.$crslist['return_from_city'].'</span></div>
         <div class="col-xs-2"><span class="fadr fa fa-long-arrow-right textcntr"></span></div>
         <div class="col-xs-5"><span class="airlblxl">'.$crslist['return_arrival_time'].'</span><span class="portnme">'.$crslist['return_to_city'].'</span></div>
      </div>
      <div class="col-xs-2 nopadding width_adjst"><span class="portnme textcntr">'.$crslist['return_duration']. '</span><span class="portnme textcntr">Stop : 0</span></div>
   </div>
   
</div>';
  	}
	
}

    
	return array('segment_abstract_details' => $outer_summary, 'segment_full_details' => $inner_summary);
}
function get_fare_summary($pre_booking_params,$search_data)
{
	//debug($pre_booking_params);

	 $adult_count=$search_data['adult_config'];
   	$child_count=$search_data['child_config'];
   	$infant_count=$search_data['infant_config'];
   
   //debug($pre_booking_params);
   foreach ($pre_booking_params as $key => $crslist) {

      $adult=($crslist['adult_price'] + $crslist['adult_tax'] + $crslist['adult_markup']) * $adult_count ;
      $child=($crslist['child_price'] + $crslist['child_tax'] + $crslist['child_markup']) * $child_count ;
      $infant=($crslist['infant_price'] + $crslist['infant_tax'] + $crslist['infant_markup'] ) * $infant_count ;
      $total_fare=$adult + $child + $infant;
      $fare= $crslist['adult_price'] +  $crslist['child_price'] + $crslist['infant_price'];
		/*      $tax=($crslist['adult_tax'] + $crslist['adult_markup']) + ($crslist['child_tax'] + $crslist['child_markup']) + ($crslist['infant_tax'] + $crslist['infant_markup']) ;
		*/      
		$tax_adult=($crslist['adult_tax']*$adult_count);
		$child_tax=($crslist['child_tax']*$child_count);
		$infant_tax=($crslist['infant_tax']*$infant_count);

		$markup_adult=($crslist['adult_markup']*$adult_count);
		$markup_child=($crslist['child_markup']*$child_count);
		$markup_infant=($crslist['infant_markup']*$infant_count);
		$tax=$tax_adult+$child_tax+$infant_tax;
      	$total_markup=$markup_adult+$markup_child+$markup_infant;
      $token = serialized_data($crslist['orgin']);
      $token_key = md5($token);

      if($search_data['trip_type']=="circle" && $pre_booking_params[0]['no_multiple']!="no"){
      	$total_fare=$total_fare*2;
      	$fare=$fare*2;
      	$crslist['adult_price']=$crslist['adult_price']*2;
      	$tax=$tax*2;
      }
      if(isset($adult_count) && $adult_count!='0'){  $div_adult='<div class="faresty">'.$adult_count.' Adult(s) &lrm;('.$adult_count.' X '.$crslist['adult_price'].')</div>'; } else { $div_adult ='';}
     	
      if(isset($child_count) && $child_count !='0'){  $div_child='<div class="faresty">'.$child_count.' Child(s) &lrm;('.$child_count.' X '.$crslist['child_price'].')</div>'; } else { $div_child ='';} 
      if(isset($infant_count) && $infant_count !='0'){  $div_infant='<div class="faresty">'.$infant_count.' Infonts(s) &lrm;('.$infant_count.' X '.$crslist['infant_price'].')</div>'; } else { $div_infant ='';}
	
	  if(isset($adult_count)&& $adult_count !='0'){  $div_adultprice='<div class="amnter">$'.($adult_count * $crslist['adult_price']).' </div>'; } else { $div_adultprice ='';}
      if(isset($child_count) && $child_count !='0' ){  $div_childprice='<div class="amnter">$ '.($child_count * $crslist['child_price']).' </div>';} else { $div_childprice ='';} 
      if(isset($infant_count) && $infant_count !='0' ){  $div_infantprice='<div class="amnter">$ '.($infant_count * $crslist['infant_price']).' </div>'; } else { $div_infantprice ='';}
	
      if($pre_booking_params[0]['flight_trip']=="circle"){
      	//$div_adult=$div_adult*2;
      }

	$fare_summary = '<div class="insiefare">
   <div class="farehd arimobold">Fare Summary</div>
   <div class="fredivs">
      <div class="kindrest">
         <div class="freshd">Base Fare</div>
         <div class="reptallt">
            <div class="col-xs-8 nopadding">
             '.$div_adult.'  '.$div_child.' '.$div_infant.'
              </div>
            <div class="col-xs-4 nopadding">
               '.$div_adultprice.'  '.$div_childprice.' '.$div_infantprice.'</div>
         </div>
      </div>
      <div class="kindrest">
         <div class="freshd">Taxes</div>
         <div class="reptallt">
            <div class="col-xs-8 nopadding">
               <div class="faresty">Taxes &amp; Fees</div>
            </div>
            <div class="col-xs-4 nopadding">
               <div class="amnter arimobold">$'.$tax.' </div>
            </div>
            <div class="col-xs-8 nopadding">
               <div class="faresty">Convenience Fees</div>
            </div>
            <div class="col-xs-4 nopadding">
               <div class="amnter arimobold">$ 0 </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="reptalltftr">
         <div class="col-xs-8 nopadding">
            <div class="farestybig">Grand Total</div>
         </div>
         <div class="col-xs-4 nopadding">
            <div class="amnterbig arimobold">$'.$total_fare.'  </div>
         </div>
      </div>
   </div>
</div>';
}
	return $fare_summary;
}?>
<script type="text/javascript">
  $('#stepbk1').click(function(){
		$(this).addClass('active');
		$('.bktab2, .bktab3').fadeOut(500,function(){$('.bktab1').fadeIn(500)});
	    $("#review").show();
	});
	
	$('#stepbk2,#next_review').click(function(){		 
	    $("#stepbk2").addClass('active');
		$('.bktab1, .bktab3').fadeOut(500,function(){$('.bktab2').fadeIn(500)});
	    $("#review").();
	});
	
	$('#stepbk3,#traveller_btn').click(function(){
	     $('#stepbk3').addClass('active');
		$('.bktab1, .bktab2').fadeOut(500,function(){$('.bktab3').fadeIn(500)});
	   
	}); 

</script>
<script>
	function change_guest_form(val){
	  if(val == 1){
		  $("#guest_txt").html('Guest User');
		  $("#sug_login").css('display', 'inherit');
		  $("#sug_guest").css('display', 'none');
		  $("#login_form").css('display', 'none');
		  $("#guest_from").css('display', 'inherit');
	  }else{
		   $("#guest_txt").html('Login Register User');
		  $("#sug_login").css('display', 'none');
		  $("#sug_guest").css('display', 'inherit');
		  $("#login_form").css('display', 'inherit');
		  $("#guest_from").css('display', 'none');
	  }
	}
var base_url = "<?php echo base_url(); ?>";	

function form_submits_bookings(formName) {
	if(formName == 'guest_login') {
		if($('#email').val() == ''){
		$('#email-error').html('Please enter Email Id');
		return false;
		}else{
		$('#email-error').html('');
	}

	if($('#password').val() == ''){
		$('#password-error').html('Please enter password');
		return false;
	}else{
		$('#password-error').html('');
	}

	var data_action  = base_url+'account/login';
	var data_pass = {'email' : $('#email').val(), 'password' : $('#password').val(),'users':'users'};
	}else{
		if($('#mobile').val() == ''){
			$('#mobile-error').html('Please enter Mobile Numnber');
		return false;
	}else{
		$('#mobile-error').html('');	
	}

	if($('#g_email').val() == ''){
		$('#g_email-error').html('Please enter Email Id');
		return false;
	}else{
		$('#g_email-error').html('');
	}

	var data_action  = base_url+'index.php/booking/create_guest_account';
	var data_pass = {'email' : $('#g_email').val(), 'mobile' : $('#mobile').val()};
	}

	$.ajax({
		type: "POST",
		url: data_action,
		dataType: "json",
		data : data_pass,
		success: function(data){
			if(data.status == 3) {
				$.each(data, function(key, value) {
				 $('#'+key).text(value);
				});
			}
			if(data.status == 2){
				$("#account_already").text('Email ID already registered.Login as User');
			}
			if(data.status == 1){
			     $('.center_pro').removeClass('active');
				 $("#stepbk2").addClass('active');
				 $('.bktab1, .bktab3').fadeOut(500,function(){$('.bktab2').fadeIn(500)});
		   }else{
				$('#loginLdrReg_reg').();
				$('div.reg_popuperror').html(data.msg);
				$('div.reg_popuperror').show();
			}
		}
	}); 
	return false;
	}
</script>
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script>
