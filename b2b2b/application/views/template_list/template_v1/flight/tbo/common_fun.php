<?php
/**
 * Return class based on type of page
 */
function add_special_class($col_2x_class, $col_1x_class, $domestic_round_way_flight)
{
	if ($domestic_round_way_flight) {
		return $col_2x_class;
	} else {
		return $col_1x_class;
	}
}   

function time_filter_category($time_value)
{
	$category = 1;
	$time_offset = intval(date('H', strtotime($time_value)));
	if ($time_offset < 6) {
		$category = 1;
	} elseif ($time_offset < 12) {
		$category = 2;
	} elseif ($time_offset < 18) {
		$category = 3;
	} else {
		$category = 4;
	}
	return $category;
}
/**
 * Generate Category For Stop
 */
function stop_filter_category($stop_count)
{
	$category = 1;
	switch (intval($stop_count)) {
		case 0 : $category = 1;
		break;
		case 1 : $category = 2;
		break;
		default : $category = 3;
		break;
	}
	return $category;
}