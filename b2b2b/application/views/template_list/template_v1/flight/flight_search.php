	<div id="Flight" class="tab-pane active">
							<form autocomplete="off" onSubmit="return validateform();" action="<?php echo base_url(); ?>flight/pre_flight_search" name="flight_search_form" id="flight_search_form">
								<div class="intabs">
								  <div class="waywy">
									<div class="smalway">
										<label class="wament hand-cursor active">
											<input class="hide" type="radio" name="trip_type" id="onew-trp" value="oneway" checked> One Way</label>
											<label class="wament hand-cursor">
											<input class="hide" type="radio" name="trip_type" id="rnd-trp" value="circle"> Roundtrip</label>
											<label class="wament hand-cursor"><input class="hide" type="radio" name="trip_type" id="multi-trp" value="multicity"> Multi City</label></div>
								  <div class="clearfix"></div>
								  <div class="outsideserach" id="normal">
									<div class="col-lg-6 col-md-6 col-sm-6 fiveh"> 
									  <div class="marginbotom10">
									  <span class=" formlabel">From </span>
									  <div class="relativemask "> <span class="maskimg  hfrom"></span>
										<input type="text" name="from" id="from" value="" placeholder="Airport City" class="fromflight ft" required >
										<input class="hide loc_id_holder" name="from_loc_id" type="hidden" value="">
									  </div>
									  </div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 fiveh"> 
									  <div class="marginbotom10">
									  <span class=" formlabel">To</span>
									  <div class="relativemask"> <span class="maskimg  hfrom"></span>
										<input type="text" name="to" id="to" value="" placeholder="Airport City" class="departflight ft" required>
										<input class="loc_id_holder" name="to_loc_id" type="hidden" value="">
									  </div>
									  </div>
									</div>
									<div class="col-md-12 nopad">
									  <div class="col-xs-6 fiveh"> 
										<div class="marginbotom10">
										<span class=" formlabel">Departure</span>
										<div class="relativemask"> <span class="maskimg   caln"></span>
										  <input type="text"   id="st_date" name="depature" placeholder="Depature Date" value="" class="forminput" required>
										</div>
										</div>
									  </div>
									  <div class="col-xs-6 fiveh" id="returns"> 
										<div class="marginbotom10">
										<span class=" formlabel">Return</span>
										<div class="relativemask"> <span class="maskimg caln"></span>
										  <input type="text" id="en_date" name="return" placeholder="Return Date" value="" class="forminput" required>
										</div>
										</div>
									  </div>
									</div>
							 </div>
								<div class="clearfix"></div>
								
									<div class="outsideserach" id="multi" style="display:none;">
									<div class="col-lg-6 col-md-6 col-sm-6 fiveh"> 
									  <div class="marginbotom10">
									  <span class=" formlabel">From</span>
									  <div class="relativemask"> <span class="maskimg hfrom"></span>
										<input type="text" name="from[]" id="m_from1" value="" placeholder="Airport City" class="fromflight ft" required >
										<input class="hide loc_id_holder" name="from_loc_id[]" type="hidden" value="">
									  </div>
									  </div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 fiveh"> 
									  <div class="marginbotom10">
									  <span class="  formlabel">To</span>
									  <div class="relativemask"> <span class="maskimg hfrom"></span>
										<input type="text" name="to[]" id="m_to1" value="" placeholder="Airport City" class="departflight ft" required>
										<input class="loc_id_holder" name="to_loc_id[]" type="hidden" value="">
									  </div>
									  </div>
									</div>
									<div class="col-lg-6 col-md-6 nopad">
									  <div class="col-xs-12 fiveh"> 
										<div class="marginbotom10">
										<span class="  formlabel">Departure</span>
										<div class="relativemask"> <span class="maskimg caln"></span>
										  <input type="text"   id="m_flight_datepicker1" name="depature[]" placeholder="Depature Date" value="" class="forminput" required>
										</div>
										</div>
									  </div>
								</div>
							     <div class="clearfix"></div>
							     <div id="segment_1" class="segments">
							        <div class="col-lg-6 col-md-6 col-sm-6 fiveh"> 
									  <div class="marginbotom10">
									  <span class="  formlabel">From</span>
									  <div class="relativemask"> <span class="maskimg hfrom"></span>
										<input type="text" name="from[]" id="m_from2" value="" placeholder="Airport City" class="fromflight ft" required>
										<input class="hide loc_id_holder" name="from_loc_id[]" type="hidden" value="">
									  </div>
									  </div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 fiveh"> 
									  <div class="marginbotom10">
									  <span class=" formlabel">To</span>
									  <div class="relativemask"> <span class="maskimg hfrom"></span>
										<input type="text" name="to[]" id="m_to1" value="" placeholder="Airport City" class="departflight ft" required>
										<input class="loc_id_holder" name="to_loc_id[]" type="hidden" value="">
									  </div>
									  </div>
									</div>
									
									<div class="col-lg-6 col-md-6 nopad">
									  <div class="col-xs-12 fiveh"> 
										<div class="marginbotom10">
										<span class="  formlabel">Departure</span>
										<div class="relativemask"> <span class="maskimg caln"></span>
										  <input type="text"   id="m_flight_datepicker2" name="depature[]" placeholder="Depature Date" value="" class="forminput" required>
										</div>
										</div>
									  </div>
									 
									  
								</div>
						      </div>
						      
						   
							     
							     
							      <div class="interst_clone_wrapper">
							      
							      
							      </div>
							 
							  </div>
								
								  <div class="clearfix"></div>
								
									
								 <div class="col-md-12 nopad">
									  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fiveh"> 
										<div class="marginbotom10">
										<span class=" formlabel">Adult</span>
										<div class="selectedwrap">
										  <select name="adult" id="adult" class="mySelectBoxClass flyinputsnor">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										  </select>
										</div>
										</div>
									  </div>
									  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fiveh"> 
										<div class="marginbotom10">
										<span class="  formlabel">Child<strong>(2-11 yrs)</strong></span>
										<div class="selectedwrap">
										  <select id="child" name="child" class="mySelectBoxClass flyinputsnor">
											 <option value="0">0</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										  </select>
										</div>
										</div>
									  </div>
									  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fiveh"> 
										<div class="marginbotom10">
										<span class=" formlabel">Infant<strong>(0-2 yrs)</strong></span>
										<div class="selectedwrap">
										  <select name="infant"  id="infant" class="mySelectBoxClass flyinputsnor">
											  <option value="0" >0</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										  </select>
										</div>
										</div>
									  </div>
									</div>
									<div class="clearfix"></div>
								
									<div class="col-md-8 nopad">
									  <div class="col-xs-6 fiveh"> 
										<div class="marginbotom10">
										<span class="  formlabel">Class</span>
										<div class="selectedwrap">
										  <select class="mySelectBoxClass flyinputsnor">
											<option>All</option>
											<option>Economy With Restrictions</option>
											<option>Economy Without Restrictions</option>
											<option>Economy Premium</option>
											<option>Business</option>
											<option>First</option>
										  </select>
										</div>
									  </div>
									  </div>

									  <div class="col-xs-6 fiveh"> 
										<div class="marginbotom10">
										<span class="  formlabel">Preferred Airline</span>
										<div class="selectedwrap">
										  <select class="mySelectBoxClass flyinputsnor">
											<option>All Airlines</option>
											
										  </select>
										</div>
									  </div>
									  </div>
									  </div>
									
									<div class="col-md-4 fiveh">
									  <div class="formsubmit">
										    <input type="hidden" autocomplete="off" name="carrier[]" id="carrier" value="">
									<input type="hidden" id="search_flight" name="search_flight" placeholder="Depature Date" value="search" class="forminput">
									<input type="hidden" autocomplete="off" name="v_class" id="class" value="All">
										<button class="srchbutn comncolor" style="float: right;">Search Flights <span class="srcharow"></span></button>
									  </div>
									</div>
								</div>
						   </form>
						</div>
						</div>
							
     

							
