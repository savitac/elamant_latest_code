<?php 
	$cart_iter = 0;
	foreach($cart_global as $key => $cid):
		list($module, $cid) = explode(',', $cid);
		if($module == 'Hotels'):
			$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
			$request_data = json_decode(base64_decode($cart->request));
			if($cart->cancellation_till_date >= date('Y-m-d')) {
				$paylater = true;
			}
			else {
				$paylater = false;
			}
			if($this->session->userdata('user_type') == 2):
				$net_Total[] = $cart->admin_baseprice;
				$commissionable_Total[] = $cart->agent_baseprice;
			elseif($this->session->userdata('user_type') == 4):
				$net_Total[] = $cart->sub_agent_baseprice;
				$commissionable_Total[] = $cart->total_cost;
			else:
			endif;
			$ovr_all_tax[] = $cart->service_charge + $cart->gst_charge + $cart->tax_charge;
			$hotel_request =  json_decode(base64_decode($cart->request));
		elseif($module == 'Transfer'):
			$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
			if($this->session->userdata('user_type') == 2):
				$net_Total[] = $cart->admin_base_price;
				$commissionable_Total[] = $cart->agent_baseprice;
			elseif($this->session->userdata('user_type') == 4):
				$net_Total[] = $cart->sub_agent_baseprice;
				$commissionable_Total[] = $cart->total_cost;
			else:
			endif;
			$ovr_all_tax[] = $cart->service_charge + $cart->gst_charge + $cart->tax_charge;
		endif;
	endforeach;
	$PG_Markup = 0;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Chinagap</title>
        <?php echo $this->load->view("core/load_css"); ?>
        <link href="<?php echo ASSETS; ?>assets/css/hotel_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/pre_booking.css" rel="stylesheet">
    </head>
    <body>
        <!-- Navigation -->
        <?php
            echo $this->load->view('dashboard/top');
		?>
        <!-- /Navigation -->
        <?php 
			$Acount = 0;$Fcount = 0;$Hcount = 0;$Ccount = 0;$Car_count = 0;$Car_v1_count = 0; $tCount =0;;
			$Vcount = 0;
			$Total = array();
			foreach($cart_global as $key => $cid) {
				list($module, $cid) = explode(',', $cid);
				if($module == 'Hotels'){
					$Hcount = $Hcount+1;
				}
			}
			foreach($cart_global as $key => $cid) {
				list($module, $cid) = explode(',', $cid);
				if($module == 'Hotels'){
					$cart = $this->cart_model->getBookingTemp_hotel($cid);
					//~ echo "<pre>",print_r($cart);exit;
					$Totall[] = $cart->total_cost;
					//~ $api_temp_hotel_id_key = explode("<br>", $cart->api_temp_hotel_id_key);
					//~ $cancelpolicy=$this->cart_model->getcancelpolicy($api_temp_hotel_id_key[0]);
				}else if($module == 'Transfer'){
					$transfer_cart = $this->cart_model->getBookingTemp_transfer($cid);
					$Totall[] = $transfer_cart->total_cost; 
			    }
			}	
		?>
        <div class="full onlycontent top80">
            <div class="container martopbtm">
                <div class="payment_process">
                    <div class="col-xs-4 nopad">
                        <div class="center_pro active" id="stepbk1">
                            <div class="fabols"></div>
                            <div class="center_labl">Review</div>
                        </div>
                    </div>
                    <div class="col-xs-4 nopad">
                        <div class="center_pro" id="stepbk2">
                            <div class="fabols"></div>
                            <div class="center_labl">Travellers</div>
                        </div>
                    </div>
                    <div class="col-xs-4 nopad">
                        <div class="center_pro" id="stepbk3">
                            <div class="fabols"></div>
                            <div class="center_labl">Payment</div>
                        </div>
                    </div>
                </div>
                <form name="checkout-apartment" id="checkout-apartment" autocomplete="off" data-flag="COMMISSIONABLE" onsubmit="return my_ownvalidation()" action="<?php echo ASSETS;?>booking/checkout">
					<div class="bktab1">
						<div class="paymentpage">
							<div class="col-md-4 col-sm-4 nopad sidebuki">
								<div class="cartbukdis">
									<ul class="liscartbuk">
										<li class="lostcart" id="promo_wrap">
											<?php
												if($book_temp_data[0]->promo_code == '' || $book_temp_data[0]->promo_code == NULL) {
											?>
												<div class="cartlistingbuk">
													<div class="cartitembuk">
														<div class="col-md-12">
															<div class="payblnhmxm">Promo code</div>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="cartitembuk prompform">
														<div class="col-md-8 col-xs-8">
																<div class="cartprc">
																	<div class="payblnhm singecartpricebuk ritaln">
																		<input type="text" id="promo_code" name="promo_code"  placeholder="Enter Promo"  class="promocode"/>
																		<label  id="promo_msg"></label>
																	</div>
																</div>
															</div>
															<div class="col-md-4 col-xs-4">
																<input type="button" onclick="process_promo();" value="Apply" name="apply" class="promosubmit">
															</div>
													
													</div>
													<div class="clearfix"></div>
													<div class="savemessage"></div>
												</div>
											<?php
												}
												else {
											?>
												<div class="cartlistingbuk">
													<div class="clearfix"></div>
													<div class="cartitembuk prompform">
														<div class="col-md-8 col-xs-8">
															<div class="cartprc">
																<div class="payblnhm singecartpricebuk ritaln">
																	<span>Applied promo is <strong><?php echo $book_temp_data[0]->promo_code; ?></strong></span>
																	<label id="promo_msg"></label>
																	<a href="javascript:void(0);" onclick="remove_promo()" >Remove</a>
																</div>
															</div>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="savemessage"></div>
												</div>
											<?php
												}
											?>
										</li>
									
										<li class="lostcart">
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Sub Total</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $book_temp_data[0]->site_currency; ?> <span class="sub_total"> <?php echo number_format(array_sum($Totall)-array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk discount_wrap" style="display:none;">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Discount(-)</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc discount"><?php echo $book_temp_data[0]->site_currency;?> <span class="amount promodiscount promo_discount" ></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Taxes</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $book_temp_data[0]->site_currency; ?> <span class="taxes"><?php echo number_format(array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk cc_fees" style="display:none;">
													<div class="col-md-8 celcart">
														<div class="payblnhm">CC Fees</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $book_temp_data[0]->site_currency; ?> <span class="cc_fees_value"></span></div>
														</div>
													</div>
												</div>
												
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk nomarr cc_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Credit Card Payment</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt bigclrfnt finalAmt"><span class="amount"><?php echo $book_temp_data[0]->site_currency; ?> <span class="total_cc_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk nomarr">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Total</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt bigclrfnt finalAmt"><span class="amount"><?php echo $book_temp_data[0]->site_currency; ?><span class="sub_total promo_total total_price"><?php echo  number_format($book_temp_data[0]->total_cost,2,'.', ' '); ?></span></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-md-8 col-sm-8 nopad fulbuki">
								<div class="sumry_wrap">
									<?php 
										foreach($cart_global as $key => $cid):
											list($module, $cid) = explode(',', $cid);
											if($module == 'Hotels'):
												$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
												$hotel_request =  json_decode(base64_decode($cart->request));
												//~ echo "cart data:<pre>";print_r($cart);exit;
												
												if($cart->api_id == 5) {
													//~ Star Rating 
													$star = $cart->star;
													if($star == '1EST'){
														$star = 1;  
													 }
													else if($star == '2EST'){
														$star = 2;  
													}
													else if($star == '3EST'){
														$star = 3;  
													}
													else if($star == '4EST'){
													  $star = 4;  
													}
													else if($star == '5EST'){
														$star = 5;  
													}
													else{
														$star = $star;  
													}
													//~ Thumb Image
													$thumb_image_all = explode(',',$cart->images);
													$thumb_image = $thumb_image_all[0];
													
													// Cancellation Data
													$cancel_policy = $cart->policy_description;
												}
												else {
													$star = $cart->star_rating;
													$thumb_image = $cart->thumb_image;
													$cancel_policy = $cart->cancel_policy;
												}
												 
												//~ echo "data: <pre>";print_r($hotel_request);exit;
									?>
												<!--  Hotel Summery  -->
												<div class="pre_summery">
													<div class="prebok_hding">
														<div class="detail_htlname"><?php echo $cart->hotel_name; ?></div>
														<div class="star_detail">
															<div class="stra_hotel" data-star="<?php echo $star; ?>"> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> </div>
														</div>
													</div>
													<div class="sidenamedesc">
														<div class="celhtl width20 midlbord">
															<div class="hotel_prebook"> <img alt="" src="<?php echo $thumb_image; ?>"> </div>
														</div>
														<div class="celhtl width80">
															<div class="waymensn">
																<div class="flitruo cloroutbnd">
																	<div class="detlnavi">
		<!--
																		<div class="rmtype">King Executive Room</div>
																		<div class="rminclusin">Breakfast Only</div>
																		<div cass="clearfix"></div>
		-->
																		<div class="col-xs-4 padflt widfty">
																			<span class="timlbl right"> <span class="flname"><span class="fltime">Check-in</span></span> </span>
																			<div class="clearfix"></div>
																			<span class="flitrlbl elipsetool right"><?php echo $hotel_request->hotel_checkin; ?></span> 
																		</div>
																		<div class="col-xs-4 nopad padflt widfty">
																			<div class="lyovrtime"> <span class="flect"><?php echo ($hotel_request->adult_count + $hotel_request->child_count); ?> Passenger(s)</span> <span class="flects"> <?php echo $hotel_request->rooms; ?> Room(s)</span> </div>
																		</div>
																		<div class="col-xs-4 padflt widfty">
																			<span class="timlbl left"> <span class="flname"><span class="fltime">Check-out</span> </span> </span>
																			<div class="clearfix"></div>
																			<span class="flitrlbl elipsetool"><?php echo $hotel_request->hotel_checkout; ?></span> 
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="all_oters">
														<div class="para_sentnce">
															<strong>Cancellation Policy :</strong> <?php echo $cancel_policy; ?> 
														</div>
													</div>
												</div>
												<!--  Hotel Summery End -->
									<?php
									        elseif($module == 'Transfer'):
									            $cart_transfer = $this->cart_model->getCartDataByModule($cid,$module)->row();
									            $request = json_decode(base64_decode($cart_transfer->transfer_request));
									           
									         ?>
											<div class="pre_summery">
											
											<div class="sidenamedesc">
											<div class="celhtl width20 midlbord">
											</div>
											<div class="celhtl width80">
											<div class="waymensn">
											<div class="flitruo cloroutbnd">
											  <div class="detlnavi">
												  
												<div class="col-xs-4 padflt widfty"> <span class="timlbl right"> <span class="flname"><span class="fltime"><?php echo $cart_transfer->pickup_city_code_description; ?></span></span> </span>
												  <div class="clearfix"></div>
												 </div>
												<div class="col-xs-4 nopad padflt widfty">
												  <div class="lyovrtime"> <span class="flect"><?php echo $request->no_of_travellers; ?> Passenger(s)</span>
												  <span class="flects"><?php echo date('d-m-Y', strtotime($cart_transfer->travel_start_date)); ?></span>  </div>
												</div>
												<div class="col-xs-4 padflt widfty"> <span class="timlbl left"> <span class="flname"><span class="fltime"><?php echo $cart_transfer->dropoff_city_code_description; ?></span> </span> </span>
												  <div class="clearfix"></div>
												 </div>
											  </div>
											</div>
										  </div>
										</div>
									</div>
                                      <div class="clearfix"></div>
									  <div class="all_oters">
										<div class="para_sentnce">
											<strong>Description : </strong> 
											 <?php echo $cart_transfer->item_description; ?>
										</div>
										<div class="para_sentnce">
											<strong>Cancellation Policy ; </strong> 
											 <?php if($cart_transfer->caneclation_policy != ''){
          $cancellaiton_details = json_decode($cart_transfer->caneclation_policy, true);
        if(isset($cancellaiton_details['cancellation'])){ ?>
		
			<?php for($cancel =0; $cancel < count($cancellaiton_details['cancellation']); $cancel++) { 
				
		   if($cancellaiton_details['cancellation'][$cancel]['Charge'] == 'true') { ?> 
		  
            	Cancellation from <?php echo date('dS M Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate'])); ?> onwards , will incur cancellation charge  <b><?php echo "AUD ".$cancellaiton_details['cancellation'][$cancel]['ChargeAmount']; ?></b>|
           
            <?php } else { ?>
			
				No cancellation fee (or Free cancellation) until <?php echo date('dS M Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate']))."| "; ?>
				
		   <?php } ?>
        
        <?php } } if(false) { ?>
        <strong>Amendment Policy ; </strong> 
        <?php  if(isset($cancellaiton_details['amendment'])){ ?>
		<ul class="list_popup">
	
			<?php for($cancel =0; $cancel < count($cancellaiton_details['amendment']); $cancel++) { 
				
		   if($cancellaiton_details['amendment'][$cancel]['Charge'] == 'true') { ?> 
		    <li class="listcancel">
            	Amendment from <?php echo date('d-m-Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?> onwards , will incur cancellation charge  <b><?php echo "AUD ".$cancellaiton_details['amendment'][$cancel]['ChargeAmount']; ?></b>
            </li>
            <?php } else { ?>
				<li class="listcancel">
				No Amendment fee until <?php echo date('d-m-Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?>
				</li>
		   <?php } ?>
        </ul>
        
        <?php } }  } } ?>
										</div>
									  </div>
					
            </div>
           
								<?php	endif;
										endforeach;
									?>
									<div class="col-xs-6 nopad fulat500">
										<input type="submit" id="next_review" class="paysubmit" value="Continue">
									</div>
								</div>
							</div>
						</div>
					</div>					
					<div class="bktab2" style="display:none;">
						<div class="paymentpage">
							<div class="col-md-4 col-sm-4 nopad sidebuki">
								<div class="cartbukdis">
									<ul class="liscartbuk">
										<!--For hotel traveller-->
										<?php 
											foreach($cart_global as $key => $cid):
												list($module, $cid) = explode(',', $cid);
												if($module == 'Hotels'):
													$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
													$hotel_request =  json_decode(base64_decode($cart->request));
													//~ echo "data: <pre>";print_r($hotel_request);exit;
													
													if($cart->api_id == 5) {
														//~ Star Rating 
														$star = $cart->star;
														if($star == '1EST'){
															$star = 1;  
														 }
														else if($star == '2EST'){
															$star = 2;  
														}
														else if($star == '3EST'){
															$star = 3;  
														}
														else if($star == '4EST'){
														  $star = 4;  
														}
														else if($star == '5EST'){
															$star = 5;  
														}
														else{
															$star = $star;  
														}
														//~ Thumb Image
														$thumb_image_all = explode(',',$cart->images);
														$thumb_image = $thumb_image_all[0];
													}
													else {
														$star = $cart->star_rating;
														$thumb_image = $cart->thumb_image;
													}
													
										?>
										<li class="lostcart">
											<div class="carttitlebuk"><?php echo $cart->hotel_name; ?></div>
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-3 celcart"> <a class="smalbukcrt"><img src="<?php echo $thumb_image; ?>" alt=""/></a><br>
													</div>
													<div class="col-md-8 splcrtpad celcart">
														<div class="carttitlebuk1">
															<div class="col-xs-6 nopad">
																Check In:
																<div class="cartsec_time"><?php echo $hotel_request->hotel_checkin; ?></div>
															</div>
															<div class="col-xs-6 nopad">
																Check Out:
																<div class="cartsec_time"><?php echo $hotel_request->hotel_checkout; ?></div>
															</div>
														</div>
													</div>
													<div class="col-md-1 cartfprice celcart">
														<div class="cartprc">
															<div class="singecartpricebuk"><?php echo $book_temp_data[0]->site_currency; ?> <span class="single_hotel_price" ><?php echo $cart->total_cost; ?></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<?php
												endif;
											endforeach;
										?>
										<!--For hotel traveller  End-->
										<li class="lostcart">
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Sub Total</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $book_temp_data[0]->site_currency; ?> <span class="sub_total"><?php echo number_format(array_sum($Totall) - array_sum($ovr_all_tax), 2, '.', ' ');?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk discount_wrap" style="display:none;">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Discount(-)</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc discount"><?php echo $book_temp_data[0]->site_currency;?> <span class="amount promodiscount promo_discount" ></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Taxes</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $book_temp_data[0]->site_currency; ?> <span class="taxes"><?php echo number_format(array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk cc_fees" style="display:none;">
													<div class="col-md-8 celcart">
														<div class="payblnhm">CC Fees</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $book_temp_data[0]->site_currency; ?> <span class="cc_fees_value"></span></div>
														</div>
													</div>
												</div>

											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk nomarr cc_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Credit Card Payment</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt bigclrfnt finalAmt"><span class="amount"><?php echo $book_temp_data[0]->site_currency; ?> <span class="total_cc_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk nomarr">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Total</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt bigclrfnt finalAmt"><span class="amount"><?php echo $book_temp_data[0]->site_currency; ?> <span class="total promo_total total_price"><?php echo  number_format(array_sum($Totall),2,'.',' '); ?></span></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-md-8 col-sm-8 nopad fulbuki ">
								
								<div class="col-md-12 padleftpay">
									<div class="wrappay leftboks">
										<?php 
											$cart_iter = 0;
											foreach($cart_global as $key => $cid):
												list($module, $cid) = explode(',', $cid);
												if($module == 'Hotels'):
													$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
													$Total[] = $cart->total_cost;
													$hotel_request =  json_decode(base64_decode($cart->request));
													//~ echo "data: <pre>";print_r($hotel_request);exit;
										?>
													<div class="comon_backbg">
														<h3 class="inpagehed">Traveller</h3>
														<div class="sectionbuk">
															<div class="onedept">
																<div class="evryicon"><span class="fa fa-bed"></span></div>
																<div class="pasenger_location">
																	<h3 class="inpagehedbuk"> <span class="bookingcnt"></span> <span class="aptbokname"><?php echo $cart->hotel_name; ?></span> </h3>
																	<span class="hwonum">Adult <?php echo ($hotel_request->adult_count); ?></span> <span class="hwonum">Child <?php echo ($hotel_request->child_count); ?></span> 
																</div>
																<div class="clearfix"></div>
																<div class="payrow">
																	<?php
																		for($room = 0; $room < $hotel_request->rooms; $room++):
																	?>
																		<span class="roomcntad">Room <?php echo ($room+1); ?></span>
																		<?php
																			for($ad = 0; $ad < $hotel_request->adult[$room]; $ad++):
																		?>
																				<div class="repeatprows">
																					<div class="col-md-2 downsrt set_margin">
																						<div class="lokter"> <span class="fa fa-user"></span> <span class="whoare">Adult(<?php echo $ad+1; ?>)</span> </div>
																					</div>
																					<div class="col-md-3 set_margin">
																						<div class="selectedwrap">
																							<select class="flpayinput" name="a_gender<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ad; ?>]" >
																								<option value="Mr">Mr</option>
																								<option value="Mrs">Mrs</option>
																								<option value="Miss">Miss</option>
																							</select>
																						</div>
																					</div>
																					<div class="col-md-7 nopad">
																						<div class="col-md-6 set_margin">
																							<input name="first_name<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ad; ?>]" placeholder="First Name" type="text" class="payinput" required />
																						</div>
																						<div class="col-md-6 set_margin">
																							<input name="last_name<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ad; ?>]" placeholder="Last Name" type="text" class="payinput" required />
																						</div>
																					</div>
																				</div>
																		<?php
																			endfor;
																			if($hotel_request->child[$room] > 0):
																		?>
																				<?php
																					for($ch = 0; $ch < $hotel_request->child[$room]; $ch++):
																				?>
																						<div class="repeatprows">
																							<div class="col-md-2 downsrt set_margin">
																								<div class="lokter"> <span class="fa fa-male"></span> <span class="whoare">Child (<?php echo $ch+1; ?>)</span><br/> <span class="childageback">Age - <?php echo $hotel_request->childAges[$room][$ch]; ?> yrs</span> </div>
																							</div>
																							<div class="col-md-3 set_margin">
																								<div class="selectedwrap">
																									<select class="flpayinput" name="c_gender<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ch; ?>]" >
																										<option value="Master">Master</option>
																										<option value="Miss">Miss</option>
																									</select>
																								</div>
																							</div>
																							<div class="col-md-7 nopad">
																								<div class="col-md-4 set_margin">
																									<input name="cfirst_name<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ch; ?>]" placeholder="First Name" type="text" class="payinput" required />
																								</div>
																								<div class="col-md-4 set_margin">
																									<input name="clast_name<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ch; ?>]" placeholder="Last Name" type="text" class="payinput" required />
																									<input type="hidden" value="<?php echo $hotel_request->childAges[$room][$ch]; ?>" name="childAges_room_<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ch; ?>]">
																								</div>
																								<div class="col-md-4 set_margin">
																									<input id="child_dob_<?php echo $room; ?>_<?php echo $ch; ?>" name="child_dob_<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ch; ?>]" value="" type="text" placeholder="DOB" class="payinput" readonly required >
																								</div>
																							</div>
																						</div>
																						<script>
																							// First Date Of the month 
																							//~ var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
																							// Last Date Of the Month 
																							//~ var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
																							
																							//~ Datepicker Initiation
																							var age = "<?php echo $hotel_request->childAges[$room][$ch]; ?>";
																							var currentTime = new Date();
																							var currentTime_v1 = new Date();
																							var startDateTo = new Date(currentTime.setFullYear(currentTime.getFullYear() - age));
																							var startDateFrom = new Date(currentTime.setFullYear(currentTime.getFullYear() - 1));
																							startDateFrom.setDate(startDateFrom.getDate()+1);
																							$( "#child_dob_<?php echo $room; ?>_<?php echo $ch; ?>" ).datepicker({
																								dateFormat: 'yy-mm-dd',
																								minDate: startDateFrom,
																								maxDate: startDateTo,
																								changeMonth: true,
																								changeYear: true,
																								numberOfMonths: 1
																							});
																							
																							//~ End 
																						</script>
																				<?php
																					endfor;
																				?>
																	<?php
																		endif;
																	?>
																	<div class="clearfix" ></div>
																	<?php
																		endfor;
																	?>
																</div>
															</div>
														</div>
													</div>
										<?php
													$cart_iter++;
												elseif($module == 'Transfer'):
												  $data['ttransfer_cart'] =  $ttransfer_cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
												  
												    $data['cid'] = $cid;
												    $Total[] = $ttransfer_cart->total_cost;
													$data['transfer_request'] = $transfer_request =  json_decode(base64_decode($ttransfer_cart->transfer_request));
												
													?>
													
														<div class="sectionbuk">
															<div class="onedept">
																<div class="evryicon"><span class="fa fa-car"></span></div>
																<div class="pasenger_location">
																	<h3 class="inpagehedbuk"> <span class="bookingcnt"></span> <span class="aptbokname"><?php echo $ttransfer_cart->pickup_city_code_description." - ".$ttransfer_cart->dropoff_city_code_description ; ?></span> </h3>
																	<span class="hwonum">Passengers (<?php echo ($transfer_request->no_of_travellers); ?>)</span>
																</div>
																<div class="clearfix"></div>
																<div class="payrow">
																	<?php
																		for($pass = 0; $pass < $transfer_request->no_of_travellers; $pass++): ?>
																	
																				<div class="repeatprows">
																					<div class="col-md-2 downsrt set_margin">
																						<div class="lokter"> <span class="fa fa-user"></span> <span class="whoare">Passenger(<?php echo $pass+1; ?>)</span> </div>
																					</div>
																					<div class="col-md-2 set_margin">
																						<div class="selectedwrap">
																							<select class="flpayinput" name="a_gender<?php echo $cid ?>[<?php echo $pass; ?>]" >
																								<option value="Mr">Mr</option>
																								<option value="Mrs">Mrs</option>
																								<option value="Miss">Miss</option>
																							</select>
																						</div>
																					</div>
																					<div class="col-md-4 set_margin">
																						<input name="first_name<?php echo $cid ?>[<?php echo $pass; ?>]" placeholder="First Name" type="text" class="payinput" />
																					</div>
																					<div class="col-md-4 set_margin">
																						<input name="last_name<?php echo $cid ?>[<?php echo $pass; ?>]" placeholder="Last Name" type="text" class="payinput" />
																					</div>
																				</div>
																		
																		<?php	endfor; ?>
																			
																			<div class="clearfix" ></div>
																			
																			<?php $this->load->view('booking/transfer_compination', $data); ?>
																			
																			
																			
																			
																			
																</div>
															</div>
														</div>
													
													
											<?php endif;
											endforeach;
										?>
									</div>
									<div class="clearfix"></div>
									 <?php if(false){ ?>
									<div class="comon_backbg">
										<h3 class="inpagehed">Address</h3>
										<div class="sectionbuk billingnob">
											<div class="payrow">
												<div class="col-md-2">
													<div class="paylabel">Salutation</div>
													<div class="selectedwrap">
														<select class="flpayinput " name="salutation" required>
															<option value="Mr">Mr</option>
															<option value="Mrs">Mrs</option>
															<option value="Miss">Miss</option>
														</select>
													</div>
												</div>
												<div class="col-md-5">
													<div class="paylabel">First Name</div>
													<input type="text" id="first_name" name="first_name" class="payinput" value="" required/>
												</div>
												<div class="col-md-5">
													<div class="paylabel">Last Name</div>
													<input type="text" id="last_name" name="last_name" class="payinput" value="" required/>
												</div>
											</div>
											<div class="payrow">
												<div class="col-md-6">
													<div class="paylabel">Address</div>
													<input type="text" id="street_address" name="street_address" class="payinput" value="" required/>
												</div>
												<div class="col-md-6">
													<div class="paylabel">Address2</div>
													<input type="text" id="address2" name="address2" class="payinput" value=""/>
												</div>
											</div>
											<div class="payrow">
												<div class="col-md-4">
													<div class="paylabel">Email Address</div>
													<input type="text" id="email" name="email" class="payinput" value=" " required/>
												</div>
												<div class="col-md-4">
													<div class="paylabel">Contact</div>
													<input type="text" id="mobile" name="mobile" data-mask="00000-00000" class="payinput" value="" required/>
												</div>
												<div class="col-md-4">
													<div class="paylabel">Country</div>
													<div class="selectedwrap">
														<select class="flpayinput" id="country" name="country" required>
															<?php
																echo "data: <pre>";print_r($countries);
																if(!empty($countries)):
																	foreach($countries as $country):
															?>
																		<option value="<?php echo $country->iso_code; ?>" ><?php echo $country->country_name; ?></option>
															<?php
																	endforeach;
																endif;
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="payrow">
												<div class="col-md-4">
													<div class="paylabel">City</div>
													<input type="text" id="city"  name="city" class="payinput" value="" required/>
												</div>
												<div class="col-md-4">
													<div class="paylabel">State</div>
													<input type="text" id="state" name="state" class="payinput" value=" " required/>
												</div>
												<div class="col-md-4">
													<div class="paylabel">Postal Code</div>
													<input type="text" id="zip" name="zip" class="payinput" value=" " required/>
												</div>
											</div>
										</div>
									</div>
									<?php } ?>
<!--            
									<div class="clearfix"></div>
									<div class="col-md-12 nopad">
										<div class="checkcontent">
											<div class="squaredThree">
												<input type="checkbox" value="0" name="confirm" class="filter_airline" id="squaredThree1" required>
												<label for="squaredThree1"></label>
											</div>
											<label for="squaredThree1" class="lbllbl">By booking this item, you agree to pay the total amount shown, which includes Service Fees, on the right and to the<a class="colorbl"> Terms & Condition</a>, <a href = "" class="colorbl">Cancellation Policy</a>.</label>
										</div>
									</div>
-->
									<div class="clearfix"></div>
										<div class="col-xs-6 fulat500 nopad">
											
											<!-- Hidden Data -->
											
<!--
											<input type="hidden" name="service_charge" id="service_charge" value="0" />
											<input type="hidden" name="gst_charge" id="gst_charge" value="0" />
											<input type="hidden" name="tax_charge" id="tax_charge" value="0" />
											
											<input type="hidden" name="payment_type" id="payment_type_flag" value="Commissionable" />
											<input type="hidden" name="payment_type_deduction" id="payment_type_deduction_flag" value="" />
											<input type="hidden" name="PG_Charge" id="PG_Charge" value="" />
											<input type="hidden" id="total_payable" name="total" value="<?php echo base64_encode(array_sum($Total)); ?>"/>
											<input type="hidden" id="total_PG_payble" name="total_PG_payble" value=""/>
											<input type="hidden" id="total_deposit_payble" name="total_deposit_payble" value=""/>
-->
											<input type="hidden" name="cid" id="cid" value="<?php echo $cart_global_id;?>"/>
												
											<!-- End -->
											
											<input type="submit" class="paysubmit" name="continue" id="traveller_btn" value="Continue" />
										</div>
										<div class="col-md-9 col-xs-3 fulat500 nopad"> </div>
										<div class="clear"></div>
										<div class="lastnote"> </div>
								</div>
							</div>
						</div>
					</div>
					<div class="bktab3" style="display:none;">
						<div class="paymentpage">
							
							<div class="col-md-4 col-sm-4 nopad sidebuki">
								<div class="cartbukdis">
									<ul class="liscartbuk">
										<!--For hotel traveller-->
										<?php 
											foreach($cart_global as $key => $cid):
												list($module, $cid) = explode(',', $cid);
												if($module == 'Hotels'):
													$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
													$hotel_request =  json_decode(base64_decode($cart->request));
													//~ echo "data: <pre>";print_r($hotel_request);exit;
													
													if($cart->api_id == 5) {
														//~ Star Rating 
														$star = $cart->star;
														if($star == '1EST'){
															$star = 1;  
														 }
														else if($star == '2EST'){
															$star = 2;  
														}
														else if($star == '3EST'){
															$star = 3;  
														}
														else if($star == '4EST'){
														  $star = 4;  
														}
														else if($star == '5EST'){
															$star = 5;  
														}
														else{
															$star = $star;  
														}
														//~ Thumb Image
														$thumb_image_all = explode(',',$cart->images);
														$thumb_image = $thumb_image_all[0];
													}
													else {
														$star = $cart->star_rating;
														$thumb_image = $cart->thumb_image;
													}
													
										?>
										<li class="lostcart">
											<div class="carttitlebuk"><?php echo $cart->hotel_name; ?></div>
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-3 celcart"> <a class="smalbukcrt"><img src="<?php echo $thumb_image; ?>" alt=""/></a><br>
													</div>
													<div class="col-md-8 splcrtpad celcart">
														<div class="carttitlebuk1">
															<div class="col-xs-6 nopad">
																Check In:
																<div class="cartsec_time"><?php echo $hotel_request->hotel_checkin; ?></div>
															</div>
															<div class="col-xs-6 nopad">
																Check Out:
																<div class="cartsec_time"><?php echo $hotel_request->hotel_checkout; ?></div>
															</div>
														</div>
													</div>
													<div class="col-md-1 cartfprice celcart">
														<div class="cartprc">
															<div class="singecartpricebuk"><?php echo $book_temp_data[0]->site_currency; ?> <span class="single_hotel_price" ><?php echo $cart->total_cost; ?></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<?php
												endif;
											endforeach;
										?>
										<!--For hotel traveller  End-->
										<li class="lostcart">
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Sub Total</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $book_temp_data[0]->site_currency; ?> <span class="sub_total"><?php echo number_format(array_sum($Totall)-array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk discount_wrap" style="display:none;">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Discount(-)</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc discount"><?php echo $book_temp_data[0]->site_currency;?> <span class="amount promodiscount promo_discount" ></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Taxes</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $book_temp_data[0]->site_currency; ?> <span class="taxes"><?php echo number_format(array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk cc_fees" style="display:none;">
													<div class="col-md-8 celcart">
														<div class="payblnhm">CC Fees</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $book_temp_data[0]->site_currency; ?> <span class="cc_fees_value">0.00</span></div>
														</div>
													</div>
												</div>

											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk nomarr cc_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Credit Card Payment</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt bigclrfnt finalAmt"><span class="amount"><?php echo $book_temp_data[0]->site_currency; ?> <span class="total_cc_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk nomarr">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Total</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt bigclrfnt finalAmt"><span class="amount"><?php echo $book_temp_data[0]->site_currency; ?> <span class="total promo_total total_price" ><?php echo number_format(array_sum($Totall),2,'.',' '); ?></span></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							
							<div class="col-md-8 col-sm-8 nopad fulbuki ">
							<div class="col-xs-12 padleftpay">
								<div class="payselect comon_backbg">
									<h3 class="inpagehed">Select a Payment Method</h3>
									<div class="sectionbuk">
										<div class="alldept">
											<input type="hidden" name="applied_promo_code" id="applied_promo_code" value="" />
											<input type="hidden" id="promo_price" name="promo_price"  placeholder="Enter Promo"  class="promocode"/>
											<div class="oneselects active" id="pay_later"> 
												<input type="checkbox" class="hideinpay">
												<div class="typeselctrs"><span class="fa fa-check"></span> Commissionable</div>
											</div>
											<div class="oneselects" id="pay_now">
												<input type="checkbox" class="hideinpay">
												<div class="typeselctrs"><span class="fa fa-check"></span> Net Rate</div>
											</div>
										</div>
										
										<div class="paylater_show" >
											<ul class="radiochecks">
												<li class="li-option-2" id="commissionable_li_1" >
													<input type="radio" id="f-option-1" value="DEPOSIT" name="type_of_payment" checked >
													<label for="f-option-1" class="check"></label>
													<label for="f-option-1">Deposit</label>
												</li>
												  
												<li class="li-option-2" id="commissionable_li_2" >
													<input type="radio" id="s-option-1" value="PG" name="type_of_payment">
													<label for="s-option-1" class="check"></label>
													<label for="s-option-1">Credit Card</label>
												</li>
												  
												<li class="pay_both_2" id="commissionable_li_3" >
													<input type="radio" id="t-option-1" value="BOTH" name="type_of_payment">
													<label for="t-option-1" class="check"></label>
													<label for="t-option-1">Both</label>
												</li>
												<?php
													if($paylater) {
												?>
													<li class="li-option-2" id="commissionable_li_4" >
														<input type="radio" id="l-option-1" value="PAYLATER" name="type_of_payment">
														<label for="l-option-1" class="check"></label>
														<label for="l-option-1">Pay Later</label>
													</li>
												<?php
													}
												?>
											</ul>
											<div class="col-xs-4 nopad" id="deposit-container-2" style="display:none;" >
												<input name="commissionable_both_payment" data-total-price="<?php echo array_sum($commissionable_Total); ?>" id="commissionable_both_payment" onkeyup="price_calculation('COMMISSIONABLE');" placeholder="Deposit Amount" type="text" class="payinput">
											</div>
										</div>
							
										<div class="pay_show" style="display:none;">
											<ul class="radiochecks">
												<li class="li-option-1" id="net_li_1" >
													<input type="radio" id="f-option" value="DEPOSIT" name="type_of_payment1" checked >
													<label for="f-option" class="check"></label>
													<label for="f-option">Deposit</label>
												</li>
												  
												<li class="li-option-1" id="net_li_2" >
													<input type="radio" id="s-option" value="PG" name="type_of_payment1">
													<label for="s-option" class="check"></label>
													<label for="s-option">Credit Card</label>
												</li>
												  
												<li class="pay_both_1" id="net_li_3" >
													<input type="radio" id="t-option" value="BOTH" name="type_of_payment1">
													<label for="t-option" class="check"></label>
													<label for="t-option">Both</label>
												</li>
												<?php
													if($paylater) {
												?>
													<li class="li-option-1" id="net_li_4" >
														<input type="radio" id="l-option" value="PAYLATER" name="type_of_payment1">
														<label for="l-option" class="check"></label>
														<label for="l-option">Pay Later</label>
													</li>
												<?php
													}
												?>
											</ul>
											<div class="col-xs-4 nopad" id="deposit-container-1" style="display:none;" >
												<input name="net_both_payment" data-total-price="<?php echo array_sum($net_Total); ?>" id="net_both_payment" onkeyup="price_calculation('NET');" placeholder="Deposit Amount" type="text" class="payinput">
											</div>
										</div>
						  
							  
										<div class="tabtwoout">
	<!--
											<span class="noteclick"> After clicking "Book it" you will be redirected to payment gateway. You must complete the process or the transaction will not occur. </span> 
		-->
											<div class="all_oters backblur">
												<div class="para_sentnce">
													<strong>Cancellation Policy :</strong> <?php echo $cancel_policy; ?> 
													 <?php if($cart_transfer->caneclation_policy != ''){
          $cancellaiton_details = json_decode($cart_transfer->caneclation_policy, true);
        if(isset($cancellaiton_details['cancellation'])){ ?>
		
			<?php for($cancel =0; $cancel < count($cancellaiton_details['cancellation']); $cancel++) { 
				
		   if($cancellaiton_details['cancellation'][$cancel]['Charge'] == 'true') { ?> 
		  
            	Cancellation from <?php echo date('dS M Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate'])); ?> onwards , will incur cancellation charge  <b><?php echo "AUD ".$cancellaiton_details['cancellation'][$cancel]['ChargeAmount']; ?></b>|
           
            <?php } else { ?>
			
				No cancellation fee (or Free cancellation) until <?php echo date('dS M Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate']))."| "; ?>
				
		   <?php } ?>
        
        <?php } } if(false) { ?>
        <strong>Amendment Policy ; </strong> 
        <?php  if(isset($cancellaiton_details['amendment'])){ ?>
		<ul class="list_popup">
	
			<?php for($cancel =0; $cancel < count($cancellaiton_details['amendment']); $cancel++) { 
				
		   if($cancellaiton_details['amendment'][$cancel]['Charge'] == 'true') { ?> 
		    <li class="listcancel">
            	Amendment from <?php echo date('d-m-Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?> onwards , will incur cancellation charge  <b><?php echo "AUD ".$cancellaiton_details['amendment'][$cancel]['ChargeAmount']; ?></b>
            </li>
            <?php } else { ?>
				<li class="listcancel">
				No Amendment fee until <?php echo date('d-m-Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?>
				</li>
		   <?php } ?>
        </ul>
        
        <?php } }  } } ?>
												</div>
											</div>
											<div class="col-md-12 nopad">
												<div class="checkcontent">
													<div class="squaredThree">
														<input type="checkbox" value="0" name="confirm" class="filter_airline" id="squaredThree1" >
														<label for="squaredThree1"></label>
													</div>
													<label for="squaredThree1" class="lbllbl">By proceeding further, you do agree to the booking <a href="<?php echo site_url("dashboard/termsnconditions") ?>" target="_blank">Terms &amp; Conditions</a> of TheChinaGap </label>
												</div>
											</div>
											
											<div class="clearfix"></div>
												<div class="payrowsubmt">
													<div class="col-md-3 col-xs-3 fulat500 nopad">
														<input type="submit" class="paysubmit" id="continue" name="continue"  value="Continue"/>
													</div>
												 </div>
											</div> 
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
            </div>
        </div>
        <?php
            echo $this->load->view('core/footer');
            echo $this->load->view('core/bottom_footer');
		?>
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/custom.js"></script> 
        <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
		<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
				
				price_calculation('COMMISSIONABLE');
				
            	$('.scrolltop').click(function(){
            		$("html, body").animate({ scrollTop: 0 }, 600);
            	});
				$('#stepbk1').click(function(){
					$('.center_pro').removeClass('active');
					$(this).addClass('active');
					$('.bktab2, .bktab3').fadeOut(500,function(){$('.bktab1').fadeIn(500)});
				});
				
				$('#stepbk2,#next_review').click(function(){
					$('.center_pro').removeClass('active');
					$("#stepbk2").addClass('active');
					$('.bktab1, .bktab3').fadeOut(500,function(){$('.bktab2').fadeIn(500)});
				});
				
				$('#stepbk3,#traveller_btn').click(function(){
					$('.center_pro').removeClass('active');
					$('#stepbk3').addClass('active');
					$('.bktab1, .bktab2').fadeOut(500,function(){$('.bktab3').fadeIn(500)});
				}); 
				
				$('.oneselects').click(function(){
					$('.oneselects').removeClass('active');
					$(this).addClass('active');
				});
				
				$('#pay_now').click(function(){
					$('.paylater_show').fadeOut(500, function(){
						$('.pay_show').fadeIn();
					});
					price_calculation('NET');
				});
				$('#pay_later').click(function(){
					$('.pay_show').fadeOut(500, function(){
						$('.paylater_show').fadeIn();
					});
					price_calculation('COMMISSIONABLE');
				});

				$('.pay_both_1').click(function() {
					$("#deposit-container-1").fadeIn();
				});
				$('.pay_both_2').click(function() {
					$("#deposit-container-2").fadeIn();
				});
				$(document).on('click', '.li-option-1', function() {
					$("#deposit-container-1").fadeOut();
					price_calculation('NET');
				});
				$(document).on('click', '.li-option-2', function() {
					$("#deposit-container-2").fadeOut();
					price_calculation('COMMISSIONABLE');
				});
            });
            function process_promo() {
				var module = "<?php echo $module; ?>"; 
				var cart_id = "<?php echo $cid; ?>";
				var promo_code = $("#promo_code").val();
				if(promo_code == ''){
					$('#promo_msg').html("Please enter your Promo Code");	
					$('#promo_msg').addClass('error');
					return false;
				}
				if($( "#pay_later" ).hasClass( "active" )) {
					var payment_logic = "COMMISSIONABLE";
				}
				else if($( "#pay_now" ).hasClass( "active" )) {
					var payment_logic = "NET";
				}
				else {
					var payment_logic = '';
				}
				
				var payment_type = $('input[name=type_of_payment]:checked').val();
				price_calculation(payment_logic,true);
			}
            function process_promo_old(){
				var module = "<?php echo $module; ?>"; 
				var cart_id = "<?php echo $cid; ?>";
				var promo_code = $("#promo_code").val();
				if(promo_code == ''){
					$('#promo_msg').html("Please enter your Promo Code");	
					$('#promo_msg').addClass('error');
					return false;
				}
				
				if($( "#pay_later" ).hasClass( "active" )) {
					var payment_logic = "COMMISSIONABLE";
				}
				else if($( "#pay_now" ).hasClass( "active" )) {
					var payment_logic = "NET";
				}
				else {
					var payment_logic = '';
				}
				
				var payment_type = $('input[name=type_of_payment]:checked').val();
				
				
				$.ajax({
					type: 'POST',
					url: "<?php echo ASSETS.'bookings/process_promo_code' ?>",
					dataType: "json",
					data:{'module':module, "cart_id" : cart_id, "promo_code" : promo_code},
					success: function(data){
						if(data.status == 1){
							$("#promo_wrap").html(data.promo_theme);
							$('#applied_promo_code').val(promo_code);
							var discount = data.discount.toFixed(2);
							$('#promo_price').val(discount);
							$('.promodiscount').html(discount);
							var offered_total = data.offer_amount.toFixed(2);
							$('.promo_total').html(offered_total);
							$('#promo_msg').html(data.msg);
							$('#promo_msg').addClass('success');
							$('#service_charge').val(data.service_charge);
							$('#gst_charge').val(data.gst_charge);
							$('#tax_charge').val(data.tax_charge);
							
							price_calculation(payment_logic,payment_type);
							
						}else{
							$('#promo_msg').html(data.msg);
							$('#promo_msg').addClass('error');
						}
						return false;
					}
				}); 
				
			}
			function remove_promo() {
				var module = "<?php echo $module; ?>"; 
				var cart_id = "<?php echo $cid; ?>";
				
				if($( "#pay_later" ).hasClass( "active" )) {
					var payment_logic = "COMMISSIONABLE";
				}
				else if($( "#pay_now" ).hasClass( "active" )) {
					var payment_logic = "NET";
				}
				else {
					var payment_logic = '';
				}
				var payment_type = $('input[name=type_of_payment]:checked').val();
				
				$.ajax({
					type: 'POST',
					url: "<?php echo base_url().'bookings/clear_promo' ?>",
					dataType: "json",
					data:{'module':module, "cart_id" : cart_id},
					success: function(data){
						if(data.status == 1){
							$("#promo_wrap").html(data.promo_theme);
							$('#promo_msg').html(data.msg);
							$(".discount_wrap").hide();
							price_calculation(payment_logic,payment_type);
						}
						else {
							$('#promo_msg').html(data.msg);
							$('#promo_msg').addClass('error');
						}
					}
				});
			}
            
            function my_ownvalidation(){
				
			}
			function calculate_price(obj) {
				var promo_price = 0;
				if($("#promo_price").val() != ""){
					promo_price  = $("#promo_price").val();
				}
				if(obj == "commissionable") {
					if($("#payment_type_deduction_flag").val() == 'PG') {
						var PG_Price = parseFloat("<?php echo array_sum($commissionable_Total); ?>");
						var PG_Price_Markup_val = 10;
						var only_taxes = parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());;
						var PG_Price_Markup_val_with_taxes = PG_Price_Markup_val + parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());
						PG_Price = ((PG_Price  - promo_price) + only_taxes) + PG_Price_Markup_val;
						$(".sub_total").text(PG_Price);
						$(".total").text(PG_Price);
						$(".single_hotel_price").text(PG_Price);
						$(".taxes").text(PG_Price_Markup_val_with_taxes);
						$("#total_deposit_payble").val(0);
						$("#total_payable").val(PG_Price);
						$("#total_PG_payble").val(PG_Price);
						$("#PG_Charge").val(PG_Price_Markup_val);
						$("#payment_type_flag").val('Commissionable');
					}
					else {
						var PG_Price = 0;
						var PG_Price_Markup_val_with_taxes = PG_Price + parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());
						var total_with_promo = (parseFloat("<?php echo array_sum($commissionable_Total); ?>") - promo_price) + PG_Price_Markup_val_with_taxes ;
						$(".sub_total").text(total_with_promo);
						$(".total").text(total_with_promo);
						$(".single_hotel_price").text(total_with_promo);
						$(".taxes").text(PG_Price_Markup_val_with_taxes);
						if($("#payment_type_deduction_flag").val() == 'PayLater') {
							$("#total_deposit_payble").val(0);
						}
						else {
							$("#total_deposit_payble").val(total_with_promo);
						}
						$("#total_payable").val(total_with_promo);
						$("#total_PG_payble").val(0);
						$("#PG_Charge").val(0);
						$("#payment_type_flag").val('Commissionable');
					}
				}
				else if(obj == "net") {
					if($("#payment_type_deduction_flag").val() == 'PG') {
						var PG_Price = parseFloat("<?php echo array_sum($net_Total); ?>") - promo_price;
						var PG_Price_Markup_val = 10;
						var only_taxes = parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());;
						var PG_Price_Markup_val_with_taxes = PG_Price_Markup_val + parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());
						
						PG_Price = ((PG_Price  - promo_price) + only_taxes) + PG_Price_Markup_val;
						
						$(".sub_total").text(PG_Price);
						$(".total").text(PG_Price);
						$(".single_hotel_price").text(PG_Price);
						$(".taxes").text(PG_Price_Markup_val_with_taxes);
						
						$("#total_deposit_payble").val(0);
						$("#total_payable").val(PG_Price);
						$("#total_PG_payble").val(PG_Price);
						$("#PG_Charge").val(PG_Price_Markup_val);
						$("#payment_type_flag").val('Net');
					}
					else {
						var PG_Price = 0;
						var PG_Price_Markup_val_with_taxes = PG_Price + parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());
						var total_with_promo = (parseFloat("<?php echo array_sum($net_Total); ?>") - promo_price) + PG_Price_Markup_val_with_taxes ;
						$(".sub_total").text(total_with_promo);
						$(".total").text(total_with_promo);
						$(".single_hotel_price").text(total_with_promo);
						$(".taxes").text(PG_Price_Markup_val_with_taxes);
						if($("#payment_type_deduction_flag").val() == 'PayLater') {
							$("#total_deposit_payble").val(0);
						}
						else {
							$("#total_deposit_payble").val(total_with_promo);
						}
						$("#total_payable").val(total_with_promo);
						$("#total_PG_payble").val(0);
						$("#PG_Charge").val(0);
						$("#payment_type_flag").val('Net');
					}
				}
				else {
					var total_with_promo = (parseFloat($(obj).attr('data-total-price'))) - promo_price ;
					$(obj).attr('data-total-price',total_with_promo);
					var PG_Price = (parseFloat($(obj).attr('data-total-price')) - parseFloat(promo_price)) - parseFloat($(obj).val());
					PG_Price = PG_Price.toFixed(2);
					//~ Calculating PG_Price with Markup # Todo 
					var PG_Price_Markup_val = 10;
					
					var only_taxes = parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());;
					var PG_Price_Markup_val_with_taxes = PG_Price_Markup_val + only_taxes;
					
					PG_Price = ((parseFloat(PG_Price)) + parseFloat(only_taxes)) + parseFloat(PG_Price_Markup_val);
					
					var sub_total_price = PG_Price_Markup_val + parseFloat($(obj).attr('data-total-price'));
					var total_price = PG_Price + parseFloat($(obj).val());
					
					$(".taxes").text(PG_Price_Markup_val_with_taxes);
					$(".total").text(total_price);
					$(".single_hotel_price").text(total_price);
					
					$("#total_deposit_payble").val(parseFloat($(obj).val()));
					$("#total_payable").val(total_price);
					$("#total_PG_payble").val(PG_Price);
					$("#PG_Charge").val(PG_Price_Markup_val);
				}
			}
			function price_calculation(flag,promo_flag) {
				if(flag == 'COMMISSIONABLE') {
					
					$("#checkout-apartment").attr('data-flag','COMMISSIONABLE');
					
					var payment_type = $('input[name=type_of_payment]:checked').val();
					if(payment_type == 'BOTH') {
						var deposit_payment = $('#commissionable_both_payment').val();
					}
					else {
						var deposit_payment = null;
					}
				}
				else if(flag == 'NET') {
					
					$("#checkout-apartment").attr('data-flag','NET');
					
					var payment_type = $('input[name=type_of_payment1]:checked').val();
					if(payment_type == 'BOTH') {
						var deposit_payment = $('#net_both_payment').val();
					}
					else {
						var deposit_payment = null;
					}
				}
				else {
					return false;
				}
				
				if(promo_flag == true) {
					var promo_signature = 'SET';
					var promo_code = $("#promo_code").val();
				}
				else {
					var promo_signature = 'NOTSET';
					var promo_code = '';
				}
				
				var module = "<?php echo $book_temp_data[0]->product_id; ?>"; 
				var cart_id = "<?php echo $cid; ?>";
				$.ajax({
					type: 'POST',
					url: "<?php echo ASSETS.'booking/calculate_price' ?>",
					dataType: "json",
					data:{'product_id':module, "cart_id" : cart_id, "payment_logic" : flag, "payment_type" : payment_type, "promo_signature" : promo_signature, "promo_code" : promo_code, "deposit_payment" : deposit_payment},
					success: function(data){
						if(data.status == 1){
							$('.sub_total').text(data.sub_total_cost);
							$('.single_hotel_price').text(data.sub_total_cost);
							
							if(data.promo_code == '' || data.promo_code == 'null' || data.promo_code == null) {
								$('.discount_wrap').hide();
								$('.promo_discount').text('');
								
								$('#promo_msg').html(data.msg);
								$('#promo_msg').addClass('error');
							}
							else {
								if(data.promo_status == 1) {
									$('#promo_msg').html(data.msg);
									$('#promo_msg').addClass('success');
									$("#promo_wrap").html(data.promo_theme);
								}
								else {
									$('#promo_msg').html(data.msg);
									$('#promo_msg').addClass('error');
								}
								
								$('.discount_wrap').show();
								$('.promo_discount').text(data.discount);
							}
							
							$('.taxes').text(data.total_tax);
							
							if(data.payment_type == 'PG' || data.payment_type == 'BOTH') {
								$('.cc_fees').show();
								$('.cc_fees_value').text(data.CC_fees);
								if(data.payment_type == 'BOTH') {
									$('.cc_payment_wrap').show();
									$('.total_cc_payment').text(data.transaction_amount);
								}
								else {
									$('.cc_payment_wrap').hide();
									$('.total_cc_payment').text('');
								}
							}
							else {
								$('.cc_fees_value').text('');
								$('.cc_fees').hide();
								
								$('.cc_payment_wrap').hide();
								$('.total_cc_payment').text('');
							}
							$('.total_price').text(data.total_cost);
						}
						else {
							
						}
					}
				});
			}
        </script>
    </body>
</html>
