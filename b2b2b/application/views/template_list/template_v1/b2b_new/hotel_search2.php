<div id="Hotels" class="tab-pane"> 
	<form autocomplete="off" onSubmit="return validateform();" action="<?php echo base_url().'index.php/hotel/pre_hotel_search' ?>" name="hotelSearchForm" id="hotelSearchForm">
		<div class="intabs">
			<div class="outsideserach">
				<div class="col-lg-6 col-md-6 col-sm-6  marginbotom10 fiveh">
					<span class="formlabel"><?php echo $this->TravelLights['HotelSearch']['Destination']; ?></span>
					<div class="relativemask"> <span class="maskimg hfrom"></span> 
						<input name="city" id="hotel_autocomplete" type="text" placeholder="<?php echo $this->TravelLights['HotelSearch']['CityName']; ?>" class="ft" required value="Paris (France)">
						<input type="hidden" name="hotel_destination" id="hotel_destination" value="4775">
					</div>
				</div>
				<div class="col-md-6 nopad marginbotom10">
					<div class="col-xs-4 fiveh fiftydiv">
						<span class="formlabel"><?php echo $this->TravelLights['HotelSearch']['CheckIn']; ?></span>
						<div class="relativemask"> <span class="maskimg caln"></span>
							<input id="check-in" name="hotel_checkin" type="text" placeholder="<?php echo $this->TravelLights['HotelSearch']['CheckIn']; ?>" class="forminput" readonly >
						</div>
					</div>
					<div class="col-xs-4 fiveh fiftydiv">
						<span class="formlabel"><?php echo $this->TravelLights['HotelSearch']['CheckOut']; ?></span>
						<div class="relativemask"> <span class="maskimg caln"></span>
							<input id="check-out" name="hotel_checkout" type="text" placeholder="<?php echo $this->TravelLights['HotelSearch']['CheckOut']; ?>" class="forminput" readonly >
						</div>
					</div>
					<div class="col-xs-4 fiveh fiftydiv">
						<span class="formlabel"><?php echo $this->TravelLights['HotelSearch']['Rooms']; ?></span>
						<div class="selectedwrap">
							<select name="rooms" class="mySelectBoxClass flyinputsnor" onchange="generateRACCombination()">
								<option value="1" selected="selected">1</option>
								<option value="2" >2</option>
								<option value="3" >3</option>
								<option value="4" >4</option>
							 </select>
						</div>
					</div>
				</div>
					
			
				
				<div id="room_info" class="RACContainer" >

					<div class="col-md-12 marginbotom10 nopad">
						<div class="col-xs-6 col-sm-4 fiveh fulldiv">
							<span class="formlabel notinh">&nbsp;</span>
							<div class="roomnum"> <span class="numroom"><?php echo $this->TravelLights['HotelSearch']['Room']; ?> 1</span> </div>
						</div>
						
						<div class="col-xs-6 col-sm-8 nopad fiftydiv">
							<div class="col-xs-6 fiveh fiftydiv">
							<span class="formlabel"><?php echo $this->TravelLights['HotelSearch']['Adults']; ?> <strong>(18+)</strong></span>
							<div class="selectedwrap">
								<select name="adult[]" class="mySelectBoxClass flyinputsnor" >
									<option value="1" selected="selected">1</option>
									<option value="2" >2</option>
									<option value="3" >3</option>
									<option value="4" >4</option>
								 </select>
							</div>
							</div>
							<div class="col-xs-6 fiveh fiftydiv">
							<span class="formlabel"><?php echo $this->TravelLights['HotelSearch']['Children']; ?> <strong>(0-17)</strong></span>
							<div class="selectedwrap">
								<select name="child[]" class="mySelectBoxClass flyinputsnor" onchange="generateRACCombination()">
									<option value="0" selected >0</option>
									<option value="1" >1</option>
									<option value="2" >2</option>
									<option value="3" >3</option>
									<option value="4" >4</option>
								 </select>
							</div>
							</div>
						</div>
						<div class="child_age_wrapper"></div>
					</div>
					
				
				</div>
				  
				<div class="col-md-12 nopad marginbotom10">

					<!-- <div class="col-xs-6 fiveh fiftydiv">
						<span class="formlabel"><?php echo $this->TravelLights['HotelSearch']['Nationality']; ?></span>
						<div class="selectedwrap">
							<select name="nationality" class="mySelectBoxClass flyinputsnor">
								<?php
									if(!empty($nationality_countries) and is_array($nationality_countries)):
										foreach($nationality_countries as $country_list):
								?>
										<option value="<?php echo $country_list->iso_code; ?>" ><?php echo $country_list->country_name; ?></option>
								<?php
										endforeach;
									endif;
								?>
							 </select>
						</div>
					</div> -->

					<div class="col-xs-6 fiveh fiftydiv">
						<span class="formlabel">No.of Nights</span>
						<div class="selectedwrap">
							<select class="normalsel padselct arimo" id="no_of_nights">
							<?php
								//$no_of_days = intval(get_date_difference(@$hotel_search_params['from_date'], @$hotel_search_params['to_date']));
								$no_of_days = 1;
								for ($i = 1; $i <= 10; $i++) {
									if ($i == $no_of_days) {
										$selected = 'selected="selected"';
									} else {
										$selected = '';
									}
									?>
									<option <?=$selected?>><?=$i?></option>
							<?php
								}
							?>
						</select>
						</div>
					</div>

                    <div class="col-xs-6 fiveh fiftydiv">
                    <span class="formlabel">&nbsp;</span>
					<div class="formsubmit downmrg">
						<button class="srchbutn comncolor" style="float: right;"><?php echo 'Search Hotel'; ?> <span class="srcharow"></span></button>
					</div>
				</div>
				</div>
								
				
			</div>
		</div>	
	</form>
</div>
<script>
	var WEB_URL = "<?php echo base_url(); ?>";
    function generateRACCombination() {
		var data = {};
		var data = $('#hotelSearchForm').serialize();
		$.ajax({
			type: 'POST',
			url: WEB_URL + "dashboard/roomCombination",
			async: true,
			dataType: 'json',
			data: data,
			success: function(data) {
				$(".RACContainer").html(data);
			}
		});
	}
</script>

<script>
		$.widget( "custom.catcomplete", $.ui.autocomplete, {
    _create: function() {
      this._super();
      this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
    },

    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) { 
        var li;
        if ( item.type != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category "+item.type+"'>" + item.type + "</li>" );
          currentCategory = item.type;
        }
        li = that._renderItemData( ul, item );
        if ( item.type ) {
          li.attr( "aria-label", item.type + " : " + item.label );
        }
      });
    },
    _renderItem: function( ul, item ) { 
      if(item.type == 'City'){
      return $( "<li>" )
      //.addClass(item.category)
      .attr( "data-value", item.value )     
      .append( $( "<a>" ).html('<i class="fa fa-building-o wd20"></i> <span class="ctynme">'+item.label + '</span><span class="ctycnt">'+' <span class="pull-right" style="font-weight:600;">City</span></span>') )
      .appendTo( ul );
    }else{
      return $( "<li>" )
      //.addClass(item.category)
      .attr( "data-value", item.value )     
      .append( $( "<a>" ).html('<i class="fa fa-bed wd20"></i> <span class="areanme">'+item.label + '</span><span class="ctycnt">'+' <span class="pull-right" style="font-weight:600;">Hotel</span></span>') )
      .appendTo( ul );
    }
  }
});
	$( "#hotel_autocomplete" ).catcomplete({
		delay: 0,
		minLength: 3,
		source:'<?php echo base_url();?>hotel/get_hotel_city_suggestions',
		select: function(event, ui){
		  $('#CityID').val(ui.item.CityId);
		  $('#HotelId').val(ui.item.HotelCode);
		  $('#SearchType').val(ui.item.category);
		  $('#hotel_destination').val(ui.item.id);
		}
    });
    $( "#check-in" ).datepicker({
	  minDate: 0,
	  dateFormat: 'dd-mm-yy',
	  maxDate: "+1y",
	  numberOfMonths: 2,
	  onChange : function(){
	  },
	  onClose: function( selectedDate ) {
		//~ $( "#check-out" ).datepicker( "option", "minDate", selectedDate );
		var myDate = $(this).datepicker('getDate'); 
		myDate.setDate(myDate.getDate()+1); 
		$('#check-out').datepicker('setDate', myDate);
		$( "#check-out" ).datepicker( "option", "minDate", myDate );
		$( '#check-out' ).focus();
	  }
	});
	$( "#check-out" ).datepicker({
	  minDate: 0,
	  dateFormat: 'dd-mm-yy',
	  maxDate: "+1y",
	  numberOfMonths: 2,
	  onClose: function( selectedDate ) {
		//~ $( "#check-in" ).datepicker( "option", "maxDate", selectedDate );
	  }
	});
	</script>
							
