<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Travel Lights</title>
	<?php echo $this->load->view('core/load_css'); ?>
	<!-- <link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" /> -->
	<link href="<?php echo ASSETS;?>assets/css/layerslider.css" rel="stylesheet" />
</head>
<body>

    <!-- login section -->

    <div class="log_section">

       <div class="background_login">
        <div class="loadcity"></div>
        <div class="clodnsun"></div>
        <div class="reltivefligtgo">
            <div class="flitfly"></div>
        </div>
        <div class="clearfix"></div>
        <div class="busrunning">
            <div class="runbus"></div>
            <div class="runbus2"></div>
            <div class="roadd"></div>
        </div>
    </div>

    <div class="container">
        <div class="log_mem">
            <div class="log_logo">
              <?php if($this->session->userdata('site_name') == ''){ ?>    
              <img src="<?php echo ASSETS.'assets/images/logo.png'; ?>">
              <?php } else {   ?> 
              <a class="" href="<?php echo base_url();?>"><img src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $this->session->userdata('domain_logo'); ?>"></a> 
              <?php } ?>
              <br />
              <a href="<?php echo base_url(); ?>"><b>Go Back</b></a>
          </div>
          <div class="mem_section" style="">
            <span class="mem_log_txt">
              Create Agent  Account
          </span>
          <form  id="" method="POST" action="<?php echo base_url(); ?>account/createAccount" >						

              <div class="row">
                <div class="form-group col-md-6">
                   <div class="selectft">
                    <div class="selectmask msg hide"></div>
                    <input type="text" class="form-control log_ft" id="agency_name" name="agency_name" placeholder="Agency Name" required value="">
                    <span id="agency_name-error" class="error" for="agency_name"> </span>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="selectft">
                    <div class="selectmask msg hide"></div>
                    <input type="text" class="form-control log_ft" id="agncy_fname" name="agncy_fname" placeholder="Name" required value="">
                    <span id="agncy_fname-error" class="error" for="agncy_fname"> </span>
                </div>
            </div>
        </div>


        <div class="row">
           <div class="form-group col-md-6">
               <div class="selectft">
                <div class="selectmask msg hide"></div>
                <input type="text" class="form-control log_ft" id="mobile_num" name="mobile_num" placeholder="Mobile" required value="">
                <span id="mobile_num-error" class="error" for="mobile_num"> </span>
            </div>
        </div>

        <div class="form-group col-md-6">
           <div class="selectft">
            <div class="selectmask msg"></div>
            <input type="text" class="form-control log_ft" id="email" name="email" placeholder="email" required value="">
            <span id="email-error" class="error" for="email"> </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        <div class="selectft">
            <div class="selectmask pass"></div>
            <input type="password" class="form-control log_ft" id="password" name="password" placeholder="Password" required value="">
            <span id="password-error" class="error" for="password"> </span>
        </div>
    </div>

    <div class="form-group col-md-6">
        <div class="selectft">
            <div class="selectmask pass"></div>
            <input type="password" class="form-control log_ft" id="cpassword" name="cpassword" placeholder="confirm Password" required  value="">
            <span id="cpassword-error" class="error" for="cpassword"> </span>
        </div>
    </div>
</div>

<div class="form-group">
  <div class="selectft">

      <select class="form-control" id="country" name="country">
        <option value="0" selected>country</option>
        <?php foreach($nationality_countries as $country){?>
        <option value="<?php echo $country['country_id'];?>"><?php echo $country['country_name'];?></option> 
        <?php }?>
    </select>
    <span id="country-error" class="error" for="country"> </span>
</div>
</div>

                        <!-- <div class="form-group">
                            <div class="selectft">
                                <div class="selectmask pass"></div>
                                 <input type="text" class="form-control log_ft" id="state" name="state" placeholder="state" required>
                                  <span id="state-error" class="error" for="state"> </span>
                            </div>
                        </div>-->
                        
                        
                        <div class="for_get">
                            <div class="col-sm-6 nopad">
                                <div class="squared_check"><input type="checkbox" value="tigerair" name="terms" class="filter_airline" id="squaredchk"><label for="squaredchk"></label></div>
                                <label class="remlabel_q" for="squaredchk">Terms And Conditions</label>
                                <span id="terms-error" class="error" for="terms"> </span>
                            </div>
                            <!--<div class="col-sm-6 nopad text-right for_pass">Forget Password?</div>-->
                        </div>
                        <div class="clearfix"></div>
                        <input type="submit" id="reg_validation" class="btn btn_logsub" value="Create">
                    </form>

                </div>
                <!-- <a href="#" class="reg_b">Register for B2B</a>-->
            </div>
        </div>
    </div>
    <!-- login section -->
    
    <!-- copyright section --
  <?php //echo $this->load->view('core/bottom_footer'); ?>
  <!-- copyright section -->
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/backslider.js"></script> 

  <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
  <script type="text/javascript">

     $('#reg_validation').click(function(){
        $('.error').text('');
        //alert('hello');
        if($('#agency_name').val()=="")
        {
            $('#agency_name-error').text('This field is required.');
            return false;
        }

        if($('#agncy_fname').val()=="")
        {
            $('#agncy_fname-error').text('This field is required.');
            return false;
        }

        if($('#mobile_num').val()=="")
        {
            $('#mobile_num-error').text('Mobile Number is required.');
            return false;
        }

        var x = $('#email').val();
        var atpos = x.indexOf("@");
        var dotpos = x.lastIndexOf(".");

        if(x=="")
        {
            $('#email-error').text('Email is required.');
            return false;
        }

        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
            $('#email-error').text("Not a valid e-mail address");
            return false;
        }

        if($('#password').val()=="")
        {
            $('#password-error').text('This field is required.');
            return false;
        }

        if($('#cpassword').val()=="")
        {
            $('#cpassword-error').text('This field is required.');
            return false;
        }

        if($('#password').val() != $('#cpassword').val())
        {
            $('#password-error').text('Password Mismatch');
            return false;
        }

        if($('#country').val()==0)
        {
            $('#country-error').text('Please Select A Country');
            return false;
        }

        if(!$('#squaredchk').is(':checked'))
        {
            $('#terms-error').text('Please Agree T&C');
            return false;
        }

    }) 
</script>
</body>
</html>
