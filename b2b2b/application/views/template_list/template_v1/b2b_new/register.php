<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>Travel Lights</title>
<?php echo $this->load->view('core/load_css'); ?>
<link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" />
<?php echo $this->load->view('core/header'); ?> 
</head>

<body>
<div class="clearfix"></div>

<div class="agent_login_wrap top80">
      <div class="container">
       <div class="loginsreset registadd">
            <div class="agenthed"><?php echo $this->TravelLights['Register']['AgentRegisterations']; ?></div>
            <div class="signdiv">  
              <div class="insigndiv">
                
              
                <form name="createAccount" id="createAccount" method="post" action="<?php echo base_url()."account/createAccount"; ?>" onsubmit='return validate(this)' >

                  <input type="hidden" name="user_type" id="user_type" value="2" />
                  <div class="ritpul">
                    <div class="onesetrow">
                  <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                       <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['AllowsAlphabetsOnlyMaximum']; ?>" type="text" name="surname" id="surname" placeholder="<?php echo $this->TravelLights['Register']['SurName']; ?>" class="form-control logpadding"  minlength="1" maxlength="50" required>
                        <span id="surname-error" class="error" for="surname"> </span>
                    </div>
                    </div>
                     
                    <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['MaximumLengthis50']; ?>" type="text" name="firstname" id="firstname" placeholder="<?php echo $this->TravelLights['Register']['FirstName']; ?>" class="form-control logpadding"  minlength="1" maxlength="50" required>
                        <span id="firstname-error" class="error" for="firstname"> </span>
                    </div>
                    </div>
                    
                    <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['PleaseEnterYourDesignation']; ?>"  type="text" name="position" id="position" placeholder="<?php echo $this->TravelLights['Register']['Position']; ?>" minlength="1" maxlength="50" class="form-control logpadding" required>
                        <span id="position-error" class="error" for="position"> </span>
                    </div>
                    </div>
                    </div>
                   <!--  <div class="col-xs-4">
                     <div class="rowput"> 
                      <input type="text" name="agentname" id="agentname" placeholder="Agent Name" class="form-control logpadding" minlength="2" maxlength="20"  required>
                        <span id="agentname-error" class="error" for="agentname"> </span>
                    </div> 
                    </div>-->
                    
                    <div class="onesetrow">
                    <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['AllowsBothAlphabetsand']; ?>"  type="text" name="companyname" id="companyname" placeholder="<?php echo $this->TravelLights['Register']['CompanyName']; ?>" class="form-control logpadding"  minlength="1" maxlength="50" required>
                        <span id="companyname-error" class="error" for="companyname"> </span>
                    </div>
                    </div>
                    <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['EnterAgencyTradeName']; ?>" type="text" name="agencytradingname" id="agencytradingname" placeholder="<?php echo $this->TravelLights['Register']['AgencyTradingName']; ?>" class="form-control logpadding"  minlength="1" maxlength="50" required>
                        <span id="agencytradingname-error" class="error" for="agencytradingname"> </span>
                    </div>
                    </div>
                     <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['EnterAgencyLicenseAllows']; ?>" type="text" name="agencylicenceno" id="agencylicenceno" placeholder="<?php echo $this->TravelLights['Register']['AgencyLicenseNo']; ?>" class="form-control logpadding"  minlength="1" maxlength="50" required>
                      <span id="agencylicenceno-error" class="error" for="agencylicenceno"> </span>
                    </div>
                    </div>
                   </div>
                   <div class="onesetrow">
                    <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                      <textarea  data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['EnterAddress']; ?>" name="address" id="address" placeholder="<?php echo $this->TravelLights['Register']['Address']; ?>" required class="form-control logpadding txtregister"></textarea>
                      <span id="address-error" class="error" for="address"> </span>
                    </div>
                    </div>
                    <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['Pleaseenetercity']; ?>" type="text" name="cityname" id="cityname" placeholder="<?php echo $this->TravelLights['Register']['CityName']; ?>" class="form-control logpadding"  minlength="1" maxlength="50" required>
                        <span id="cityname-error" class="error" for="cityname"> </span>
                    </div>
                    </div>
                    
                     <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput">
                      <input type="text" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['EnterPostalCodeAllows']; ?>" name="postcode" id="postcode" placeholder="<?php echo $this->TravelLights['Register']['PostalCode']; ?>" minnumber="4" maxlength="8" class="form-control logpadding"  minlength="1" maxlength="20" required>
                      <span id="postcode-error" class="error" for="postcode"> </span>
                    </div>
                    </div>
                    </div>
                    <div class="onesetrow">
                      <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                      <!-- <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['Pleaseselectcountry']; ?>" type="text" name="country" id="country" placeholder="<?php echo $this->TravelLights['Register']['SelectCountry']; ?>" class="form-control logpadding autosuggest"  minlength="1" maxlength="50" required>
                       
                        <span id="country-error" class="error" for="country"> </span> -->
                        <div class="selectedwrap">
                    <select name="country" id="country"  class="form-control logpadding flyinputsnor" required>
                     <option value=""> <?php echo $this->TravelLights['EditB2BMarkup']['SelectCountry']; ?> </option>
                     <?php /*for($country=0; $country < count($nationality_countries); $country++) { ?> 
                     <option value="<?php echo $country_list->iso_code; ?>" <?php echo ($country_list->iso_code == 'CN')?'selected':''; ?> ><?php echo $country_list->country_name; ?></option>
                    <option value ="<?php echo $nationality_countries[$country]->country_id; ?>" <?php echo ($country_list->country_id == '3')?'selected':''; ?> > <?php echo $nationality_countries[$country]->country_name; ?></option>
                    <?php }*/ ?>
                    <?php
                  if(!empty($nationality_countries) and is_array($nationality_countries)):
                    foreach($nationality_countries as $country_list):
                ?>
                    <option value="<?php echo $country_list->country_id; ?>" <?php echo ($country_list->country_id == '3')?'selected':''; ?> ><?php echo $country_list->country_name; ?></option>
                <?php
                    endforeach;
                  endif;
                ?>
                    </select>
                  </div>
                    </div>
                    </div>
                    
                     <div class="col-sm-4 col-xs-6 fullfivn">
                      <div class="rowput">
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['PleaseEnterValidCompanyNumber']; ?>" type="text" name="companyphone" id="companyphone" placeholder="<?php echo $this->TravelLights['Register']['CompanyPhone']; ?>" class="form-control logpadding"  minlength="3" maxlength="20" required>
                      <span id="companyphone-error" class="error" for="companyphone"> </span>
                    </div>
                    </div>
                    
                     <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['PleaseEnterValidEmailAddress']; ?>" type="email" name="email" id="email" placeholder="<?php echo $this->TravelLights['Register']['Email']; ?>" class="form-control logpadding email"  required>
                      <span id="email-error" class="error" for="email"> </span>
                    </div>
                    </div>

                    
                    <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['PleaseEnterNumber']; ?>" type="text" name="qq" id="qq" placeholder="<?php echo $this->TravelLights['Register']['QQ']; ?>" class="form-control logpadding"  required>
                      <span id="qq-error" class="error" for="qq"> </span>
                    </div>
                    </div>
                     <!-- <div class="onesetrow"> -->
                    <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput">
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['PleaseEnterValidWechatAccepts']; ?>" type="text" name="wechat" id="wechat" placeholder="<?php echo $this->TravelLights['Register']['WeChatID']; ?>" class="form-control logpadding"  minlength="1" maxlength="20" required>
                      <span id="wechat-error" class="error" for="wechat"> </span>
                    </div>
                    </div>
                     <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput">
                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['PleaseEntervalidContactNumber']; ?>" type="text" name="mobile" id="mobile" placeholder="<?php echo $this->TravelLights['Register']['Mobile']; ?>" class="form-control logpadding"  minlength="3" maxlength="20" required>
                      <span id="mobile-error" class="error" for="mobile"> </span>
                    </div>
                    </div>
                    <!-- </div> -->
                    </div>
                    <div class="onesetrow">
                      <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 

                      <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['PleaseEnterValidpassword']; ?>" type="password"  name="password" id="password" placeholder="<?php echo $this->TravelLights['Register']['Password']; ?>" class="form-control logpadding" minlength="8" maxlength="30" required>
                      <span id="password-error" class="error" for="password"> </span>
                    </div>
                    </div>
                    <div class="col-sm-4 col-xs-6 fullfivn">
                    <div class="rowput"> 
                       <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['PleaseEnterConfirmpassword']; ?>" type="password"  name="cpassword" id="cpassword" placeholder="<?php echo $this->TravelLights['Register']['ConfirmPasssword']; ?>" class="form-control logpadding" required>
                        <span id="cpassword-error" class="error" for="cpassword"> </span>
                    </div>
                    </div>
                    </div>
                   
                     
                    
                    
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                    
                     <div class="regissubhed"><?php echo $this->TravelLights['Register']['CompanyOffer']; ?></div>
                    <div class="checkbox-group ">
                    <ul class="morecheck">
                          <li>
                        <div class="squaredThree1">
                            <input type="checkbox" id="squaredThreet1"  class="filter_airline" name="offers[]" value="Leisure"><label for="squaredThreet1"></label></div>
                          <label for="squaredThreet1" class="remlabel"><?php echo $this->TravelLights['Register']['Leisure']; ?></label>
                         </li>
                         <li><div class="squaredThree1">
                          <input type="checkbox" id="squaredThreet2"  class="filter_airline" name="offers[]" value="Business"><label for="squaredThreet2"></label></div>
                          <label for="squaredThreet2" class="remlabel"><?php echo $this->TravelLights['Register']['BusinessTravel']; ?></label>
                         </li>
                         <li><div class="squaredThree1">
                          <input type="checkbox" id="squaredThreet3"  class="filter_airline" name="offers[]" value="MICE"><label for="squaredThreet3"></label></div>
                          <label for="squaredThreet3" class="remlabel"><?php echo $this->TravelLights['Register']['MICE']; ?></label>
                         </li>
                          <li><div class="squaredThree1">
                            <input type="checkbox" id="squaredThreet4" class="filter_airline" name="offers[]" value="ADS"><label for="squaredThreet4"></label></div>
                          <label for="squaredThreet4" class="remlabel"><?php echo $this->TravelLights['Register']['ADS']; ?></label>
                         </li>
                         <li><div class="squaredThree1">
                          <input type="checkbox" id="squaredThreet5" class="filter_airline" name="offers[]" value="Groups"><label for="squaredThreet5"></label></div>
                          <label for="squaredThreet5" class="remlabel"><?php echo $this->TravelLights['Register']['Groups']; ?></label>
                         </li>
                         <li><div class="squaredThree1">
                          <input type="checkbox" id="squaredThreet6" class="filter_airline" name="offers[]" value="FIT"><label for="squaredThreet6"></label></div>
                          <label for="squaredThreet6" class="remlabel"><?php echo $this->TravelLights['Register']['FIT']; ?></label>
                         </li>
                    </ul>
                    </div>
                    
                    <div class="regissubhed"><?php echo $this->TravelLights['Register']['CurrentPercentage']; ?> </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-sm-6 col-xs-12  padtb">
                    <div class="rowput percents"> 
                      <div class="cmnper"><?php echo $this->TravelLights['Register']['GroupTravel']; ?></div>
                        <div class="cmnper">
                        <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['PleaseEnterNumber']; ?>" type="text" name="grouptravel" id="grouptravel" placeholder="" class="form-control logpadding"  minlength="1" maxlength="2" required>
                        <span id="grouptravel-error" class="error" for="grouptravel"> </span>
                        </div>
                        <div class="cmnper">
                        <div class="paylabel">%</div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12  padtb">
                    <div class="rowput percents"> 
                      <div class="cmnper"><?php echo $this->TravelLights['Register']['FIT']; ?></div>
                        <div class="cmnper">
                        <input data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $this->TravelLights['Register']['PleaseEnterNumber']; ?>" type="text" name="fit" id="fit" placeholder="" class="form-control logpadding"  minlength="1" maxlength="2" required>
                        <span id="fit-error" class="error" for="fit"> </span>
                        </div>
                        <div class="cmnper">
                        <div class="paylabel">%</div>
                        </div>
                    </div>
                    </div>
                    <div class="clearfix"></div>
                    
                    <div class="regissubhed"><?php echo $this->TravelLights['Register']['IncreaseBusiness']; ?></div>
                    <div class="checkbox-group required">
                    <ul class="morecheck">
                      <li>
                        <div class="squaredThree1">
                        <input type="radio" id="squaredThreet8" class="filter_airline" name="bussiness" value="Yes"><label for="squaredThreet8"></label></div>
                        <label for="squaredThreet8" class="remlabel"><?php echo $this->TravelLights['Register']['Yes']; ?></label>
                        
                         </li>
                         <li><div class="squaredThree1">
                          <input type="radio" id="squaredThreet9" class="filter_airline" name="bussiness" value="No"><label for="squaredThreet9"></label></div>
                        <label for="squaredThreet9" class="remlabel"><?php echo $this->TravelLights['Register']['No']; ?></label>

                         </li>
                    </ul>
                  </div>
                    
                    
                    
                    <div class="clearfix"></div>
                    <div class="misclog align">
                    <div class="rember">
                    <div class="squaredThree1">
                      <input type="checkbox" id="squaredThree11" class="filter_airline" name="terms" value="1">
                      <label for="squaredThree11"></label>
                    </div>
                     <label for="squaredThree11" class="remlabel"><?php echo $this->TravelLights['Register']['TermsAndCondtions']; ?><a href="http://provabextranet.com/WDMA/TCG-DEMO/home/termsnconditions" target="_blank"><?php echo $this->TravelLights['Register']['Terms']; ?></a><?php echo $this->TravelLights['Register']['conditions']; ?></label>
                     <span id="terms-error" class="error" for="squaredThree11"> </span>
                     </div>
                  </div>
                  
                  <input type="submit" name="signup" id="signup" value="<?php echo $this->TravelLights['Register']['SignupNow']; ?>" class="submitlogin lfbtn">
                  </div>
                   </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
   
</div>


<div class="clearfix"></div>
<?php //echo $this->load->view('core/load_js'); ?> 
<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>
 <script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/owl.carousel.min.js"></script> 
 <script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/custom.js"></script> 
 <script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/backslider.js"></script> 
 <script type='text/javascript' src="<?php echo ASSETS; ?>assets/js/custom/jquery.validate.js"></script>
 <script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/custom/field_validate.js"></script>
 <script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/custom/custom.js"></script> 



<script type="text/javascript">
$(document).ready(function(){

$('[data-toggle="popover"]').popover();

  $('.scrolltop').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
   });
   $("#showLeft").click(function(){
       $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
      });


    $('.navbak').click(function(){
      $('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
    });
   
   
   
  
});
</script> 


<script>
$('body').on('keyup', '#password, #cpassword',function () {
   if( $('#password').val() != "" && $('#cpassword').val() != "" ){
      if ($('#password').val() == $('#cpassword').val()) {
        $('#cpassword-error').html('Valid Password').css('color', 'green');
    } else {
       $('#cpassword-error').html('').css('color', '#cd1818');
    }
       
   } else {
     $('#cpassword-error').html('');
      $('#cpassword-error').html('');
   }
    
});
$("#signup").click(function(){  
   var data_set = true;
   if(data_set){ 
          if($('#password').val() != $('#cpassword').val()){
               alert("Password is not matching.."); 
               return false;
          }
 }
});
</script>

<script>
function validate(form) { 
if(!document.createAccount.terms.checked){alert("Please check the terms and conditions");
 return false; } 


return true;
}

</script>
</body>
</html>
