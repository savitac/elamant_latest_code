<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title><?= $this->session->userdata('company_name')?></title>
<?php echo $this->load->view('core/load_css'); ?>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="
https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<link href="<?php echo ASSETS;?>assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Navigation -->

<?php echo $this->load->view('dashboard/top'); ?> 
<?php echo $this->load->view('reports/email_reply_popup');?>

<!-- /Navigation -->


<section id="main-content">
  
  <section class="wrapper">
<div class="clearfix"></div>

        <div class="main-chart">
        
        <span class="profile_head"><center>ACCOUNT TRANSACTION</center></span>
<div class="rowit">
<div class="clearfix"></div>
<div id="toggleOrdersLoader" class="lodrefrentrev" style="display: none;">
   <div class="centerload"></div>
  </div>
<div class="clearfix"></div>


<div class="row" style="overflow-x: scroll;">
<p>Export the table to CSV: <button id="btnExport" onclick="javascript:xport.toCSV('booking_list');"> Export</button>
  </p> 
        <table class="table table-striped table-bordered" id="booking_list">
        <thead>
        <tr>
        <th>Sl.No</th> 
        <th>Log Type</th> 
        <th>Last Balance</th> 
        <th>Current Balance</th> 
        <th>Amount Credited</th> 
        <th>Amount Debited</th> 
        <th>Module</th> 
        <th>App Refrence</th> 
        <th>Transaction Date</th> 
        </tr>  
        </thead>
        <tbody>
        <?php     

        if(!empty($data))
        { 
        $c=1;                 
        for($i=0;$i < count($data);$i++){
              
        ?>

        <tr> 
          <?php $symbol="$ "; ?>
        <td><?php echo $c++; ?></td>
        <td><?php echo $data[$i]->log_type; ?></td>
        <td><?php
          if($data[$i]->last_balance !='')
          {

         echo $symbol.$data[$i]->last_balance.'.00'; 
            
          }
          else
          {
            echo $data[$i]->last_balance;
          }
        ?>
          </td>
           <td><?php
          if($data[$i]->current_balance !='')
          {

         echo $symbol.$data[$i]->current_balance.'.00'; 
            
          }
          else
          {
            echo $data[$i]->current_balance;
          }
        ?>
          </td>
        <td><?php
          if($data[$i]->amount_credited !='')
          {

         echo $symbol.$data[$i]->amount_credited.'.00'; 
            
          }
          else
          {
            echo $data[$i]->amount_credited;
          }
        ?>
          </td>
         <td><?php
          if($data[$i]->amount_debited !='')
          {

         echo $symbol.$data[$i]->amount_debited.'.00'; 
            
          }
          else
          {
            echo $data[$i]->amount_debited;
          }
        ?>
          </td> 
        <td><?php echo $data[$i]->module; ?></td>
        <td><?php echo $data[$i]->app_ref; ?></td>
        <td><?php echo date($data[$i]->created_date); ?></td> 

        </tr> 
        <?php } }?>
        </tbody>
        </table>
      </div>
</div>
</div>                        
        </div>



  </section>

</section>


<!-- /.container --> 


<!-- <script type="text/javascript">
		jQuery(document).ready(function($)
		{
			var table = $("#booking_list").dataTable({
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"oTableTools": {
				},
			});
			table.columnFilter({
				"sPlaceHolder" : "head:after"
			});
		});	
    jQuery(document).ready(function($)
    {
      var table = $("#booking_listttt").dataTable({
        "sPaginationType": "bootstrap",
        "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
        "oTableTools": {
        },
      });
      table.columnFilter({
        "sPlaceHolder" : "head:after"
      });
    }); 	
	</script> -->

  <script>
  $(document).ready(function(){
   /* $('#created_datetime_from').datepicker({
      numberOfMonths: 2,
      minDate: 0,
      dateFormat: 'dd-mm-yy'
    });

    $('#created_datetime_to').datepicker({
      numberOfMonths: 2,
      minDate: 0,
      dateFormat: 'dd-mm-yy'
    });*/

    $("#created_datetime_from").datepicker({
    dateFormat: "dd-M-yy",
    numberOfMonths: 2,
  
    onSelect: function (date) {
        var date2 = $('#created_datetime_from').datepicker('getDate');
        window.setTimeout(function() {$('#created_datetime_to').focus();}, 1);
        $('#created_datetime_to').datepicker('option', 'minDate', date2);
     }
});
$('#created_datetime_to').datepicker({
    dateFormat: "dd-M-yy",
    numberOfMonths: 2,
    onSelect: function () {
        var dt1 = $('#created_datetime_from').datepicker('getDate');
        var dt2 = $('#created_datetime_to').datepicker('getDate');
        $('#created_datetime_to').datepicker('option', 'minDate', dt1);

        if (dt2 <= dt1) {
            var minDate = $('#created_datetime_to').datepicker('option', 'minDate');
            $('#created_datetime_to').datepicker('setDate', minDate);
        }
       
    }
});



  });
  </script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('.reply-button').on('click', function(e) {
      $("#email_reply_modal").modal('show');
        email = $(this).data('recipient_email');
        app_reference=$(this).data('app_refrence');
        booking_source=$(this).data('booking_src');
        status=$(this).data('status');
        $("#recipient_email").val(email);
        $("#app_reference").val(app_reference);
        $("#booking_src").val(booking_source);
        $("#status").val(status);  
    });
  }); 
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#booking_list').DataTable();
} );
</script>
<script type="text/javascript">
  var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) { 
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};
 
</script>
<?php echo  $this->load->view('core/bottom_footer'); ?> 
</body>
</html>
