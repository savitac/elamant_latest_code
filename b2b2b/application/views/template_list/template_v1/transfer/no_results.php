<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Chinagap</title>
        <?php echo $this->load->view("core/load_css"); ?>
        <link href="<?php echo ASSETS; ?>assets/css/hotel_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
    </head>
    <body>
        <!-- Navigation -->
		<?php echo $this->load->view('dashboard/top'); ?>
        <!-- /Navigation -->
        <div class="clearfix"></div>
        
        <div class="allpagewrp top80">
  <div class="newmodify">
    <div class="container">
      <div class="contentsdw">
        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 nopad">
          <div class="pad_ten">
            
            <div class="from_to_place">
              <h4 class="placename"><?php echo $request->transfer_country; ?></h4></h4>
              <h3 class="contryname"><?php echo $request->transfer_city; ?></h3>
            </div>
            
          </div>
        </div>
        
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopad">
        
        <div class="col-xs-6 nopad ">
          <div class="pad_ten">
            
            <div class="from_to_place">
              <h4 class="placename"><?php echo $this->TravelLights['TransferResult']['PickUp']; ?></h4>
              <h3 class="contryname"><?php echo  $request->pickup_code_desc; ?></h3>
            </div>
            
          </div>
          </div>
          <div class="col-xs-6 nopad ">
          <div class="pad_ten">
            
            <div class="from_to_place">
              <h4 class="placename"><?php echo $this->TravelLights['TransferResult']['DropOff']; ?></h4>
              <h3 class="contryname"><?php echo  $request->dropoff_code_desc; ?></h3>
            </div>
            
          </div>
          </div>
          
        </div>
        
        
        
        <div class="col-lg-2 hidden-md hidden-sm hidden-xs nopad">
          <div class="pad_ten">
            
            <div class="from_to_place">
              <div class="boxlabl textcentr"><?php echo date('d-m-Y',strtotime($request->start_date)); ?> </div>
              <div class="boxlabl textcentr"><strong></strong><?php echo $request->no_of_travellers;  ?><?php echo $this->TravelLights['TransferResult']['Passenger']; ?></div>
            </div>
          </div>
        </div>
        
        <div class="col-md-2 col-sm-4 col-xs-4 nopad">
          <div class="pad_ten">
            <button class="modifysrch" data-toggle="collapse" data-target="#modify"><strong><?php echo $this->TravelLights['TransferResult']['Modify']; ?></strong> <span class="down_caret"></span></button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="modify_search_wrap">
    <div class="container">
		<form autocomplete="off" onSubmit="return validateform();" action="<?php echo base_url(); ?>transfer/search" name="hotelSearchForm" id="hotelSearchForm" >
      <div id="modify" class="collapse"> 
      		<div class="insplarea">
	<div class="intabs">
		<div class="outsideserach">
			<div class="col-md-6 nopad marginbotom10">
            	<div class="col-xs-6 fiveh fiftydiv">
					<span class="formlabel"><?php echo $this->TravelLights['TransferResult']['EnterYourCountry']; ?></span>
					<div class="selectedwrap">
						<select name="transfer_country" id="transfer_country" class="mySelectBoxClass flyinputsnor" onchange="getCities(this)" required>
								<option value="" ><?php echo $this->TravelLights['TransferResult']['SelectCountry']; ?></option>
								<?php for($trans =0; $trans < count($transfer_countries); $trans++){ ?>
									<option value="<?php echo $transfer_countries[$trans]->country_code; ?>" <?php if($request->transfer_country_code == $transfer_countries[$trans]->country_code){ echo "selected"; } ?> > <?php echo $transfer_countries[$trans]->country_name; ?> </option>
								<?php } ?>
						</select>
					</div>
				</div>
                <div class="col-xs-6 fiveh fiftydiv">
					<span class="formlabel"><?php echo $this->TravelLights['TransferResult']['EnterYourCity']; ?></span>
					<div class="selectedwrap">
						<select name="transfer_city" id="transfer_city" class="mySelectBoxClass flyinputsnor" required>
					    </select>
					</div>
				</div>
            </div>
            <div class="col-md-6 nopad marginbotom10">
            	<div class="col-xs-6 fiveh fiftydiv">
					<span class="formlabel"><?php echo $this->TravelLights['TransferResult']['PickupDate']; ?></span>
					
						<div class="relativemask"> <span class="maskimg caln"></span>
							<input id="t_st_date" value="<?php echo date('d-m-Y',strtotime($request->start_date)); ?>" name="t_st_date" type="text" placeholder="Travel Date" class="forminput" readonly  required>
					     </div>
				</div>
                <div class="col-xs-6 fiveh fiftydiv">
					<span class="formlabel"><?php echo $this->TravelLights['TransferResult']['Travellers']; ?></span>
					<div class="selectedwrap">
						<select name="travellers" id="travellers" class="mySelectBoxClass flyinputsnor" >
								<option value="1" <?php if($request->no_of_travellers == 1) { echo "selected"; } ?> >1</option>
								<option value="2" <?php if($request->no_of_travellers == 2) { echo "selected"; } ?> >2</option>
								<option value="3" <?php if($request->no_of_travellers == 3) { echo "selected"; } ?> >3</option>
								<option value="4" <?php if($request->no_of_travellers == 4) { echo "selected"; } ?> >4</option>
								<option value="5" <?php if($request->no_of_travellers == 5) { echo "selected"; } ?> >5</option>
								<option value="6" <?php if($request->no_of_travellers == 6) { echo "selected"; } ?> >6</option>
								<option value="7" <?php if($request->no_of_travellers == 7) { echo "selected"; } ?> >7</option>
								<option value="8" <?php if($request->no_of_travellers == 8) { echo "selected"; } ?> >8</option>
								<option value="9" <?php if($request->no_of_travellers == 9) { echo "selected"; } ?> >9</option>
							 </select>
					</div>
				</div>
            </div>
			
            	<div class="col-md-6 marginbotom10 nopad">
						<div class="col-xs-6 fiveh fiftydiv">
                            <span class="formlabel"><?php echo $this->TravelLights['TransferResult']['Selectpickup']; ?></span>
                            <div class="selectedwrap">
							<select name="pickup_code" id="pickup_code" class="mySelectBoxClass flyinputsnor">
								<option value=""><?php echo $this->TravelLights['TransferResult']['Selectpickup']; ?></option>
								<?php for($code =0; $code < count($tranfer_list_code); $code++){ ?>
									<option value="<?php echo $tranfer_list_code[$code]->transfer_list_code; ?>" <?php if($request->pickup_code == $tranfer_list_code[$code]->transfer_list_code) { echo "selected"; } ?>> <?php echo $tranfer_list_code[$code]->english; ?>  </option>
								<?php } ?>
							 </select>
						</div>
                   </div>
                   
                   <div class="col-xs-6 fiveh fiftydiv">
                            <span class="formlabel"><?php echo $this->TravelLights['TransferResult']['SelectDropOff']; ?> </span>
                            <div class="selectedwrap">
							<select name="dropoff_code" id="dropoff_code" class="mySelectBoxClass flyinputsnor" onchange="generateRACCombination()">
								<option value=""><?php echo $this->TravelLights['TransferResult']['SelectDropOff']; ?></option> 
								<?php for($drop =0; $drop < count($tranfer_list_code); $drop++){ ?>
									<option value="<?php echo $tranfer_list_code[$drop]->transfer_list_code; ?>" <?php if($request->dropoff_code == $tranfer_list_code[$drop]->transfer_list_code) { echo "selected"; } ?>> <?php echo $tranfer_list_code[$drop]->english; ?>  </option>
								<?php } ?>
							 </select>
						</div>
                   </div>
                				
			</div>
		
			<div class="col-xs-6 fiveh">
				<div class="formsubmit">
					<button type="submit" class="srchbutn comncolor"><?php echo $this->TravelLights['TransferResult']['SearchTransfers']; ?></button>
				</div>
			</div>
			</form>
		
	</div>
			</div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="contentsec margtop">
    <div class="container">
      <div class="filtrsrch">
        <div class="col30">
          <div class="celsrch">
          	<button class="close_filter"><span class="fa fa-close"></span></button>
            <div class="boxtop">
              <div class="filtersho">
                <div class="avlhtls"><strong><span id="total_result_count">10</span></strong> <?php echo $this->TravelLights['TransferResult']['Transfersfound']; ?><span class="placenamefil"> In Bangalore </span> </div>
              </div>
            </div>
            <div class="norfilterr"> 
            <div class="resultfilt"><?php echo $this->TravelLights['TransferResult']['FilterResults']; ?></div>
             
              <div class="outbnd">
                
                <div class="rangebox">
                  <div class="ranghead collapsed" data-target="#demo" data-toggle="collapse"><?php echo $this->TravelLights['TransferResult']['Price']; ?></div>
                  <div id="demo" class="collapse">
                  	<div class="price_slider1">
                    <input type="text"  class="level" id="amount" readonly >
                    <div id="slider-range"></div>
                    </div>
                  </div>
                </div>
                <div class="rangebox">
                  <div class="ranghead collapsed" data-target="#demo2" data-toggle="collapse"><?php echo $this->TravelLights['TransferResult']['TransferType']; ?></div>
                  <div id="demo2" class="stoprow collapse">
                    <div class="boxins">
                      <ul class="locationul" id="starrat">
                      	<li><div class="squaredThree"><input type="checkbox" value="tigerair" name="" class="filter_airline" id="squaredThree01"><label for="squaredThree01"></label></div><label for="squaredThree01" class="lbllbl"><?php echo $this->TravelLights['TransferResult']['Private']; ?></label> <span class="countrgt">(20)</span></li>
                        <li><div class="squaredThree"><input type="checkbox" value="tigerair" name="" class="filter_airline" id="squaredThree02"><label for="squaredThree02"></label></div><label for="squaredThree02" class="lbllbl"><?php echo $this->TravelLights['TransferResult']['Shared']; ?></label><span class="countrgt">(300)</span></li>
                      	
                      </ul>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        <div class="col70">
          <div class="in70">
          <div class="topmisty hote_reslts">
              <div class="col-xs-12 nopad">
                <button class="filter_show"><span class="fa fa-filter"></span></button>
                <div class="insidemyt">
                  <div class="col-xs-6 nopad fullshort">
                    <ul class="sortul">
                      <li class="sortli"> <span class="sirticon fa fa-sort-amount-asc"></span> 
                      <a class="sorta des"><?php echo $this->TravelLights['TransferResult']['TransferType']; ?></a> 
                      </li>
                      <li class="sortli"> <span class="sirticon fa fa-tag"></span> <a class="sorta nobord des" ><?php echo $this->TravelLights['TransferResult']['Price']; ?></a> </li>
                      
                    </ul>
                  </div>
                  
                </div>
              </div>
            </div>
              
            <!--All Available flight result comes here -->
            
            <div class="allresult">
              <div class="hotel_map">
                <div class="map_hotel" id="map"></div>
              </div>
              <div class="hotels_results" id="transfer_result">
             
              </div>
            </div>
            <!-- End of result --> 
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
      
      
      <?php echo $this->load->view('core/footer'); ?>
	  <?php echo $this->load->view('core/bottom_footer'); ?>
	   <!--Cancellation details-->
      
        <!-- Loading Animation -->
		
		<div class="all_loading imgLoader">
			<div class="load_inner">
				<div class="relativetop">
					<div class="paraload"><?php echo $this->TravelLights['TransferResult']['Searchingforthebest']; ?> <?php echo $request->transfer_city.", ".$request->transfer_country; ?> </div>
					<div class="normal_load"></div>
					<div class="clearfix"></div>
					<div class="sckintload">
						<!--For round way add class 'round_way'-->
						<div class="ffty">
							<div class="borddo brdrit"> <span class="lblbk"><?php echo $this->TravelLights['TransferResult']['StartDate']; ?></span> </div>
						</div>
						<div class="ffty">
							<div class="borddo"> <span class="lblbk"><?php echo $this->TravelLights['TransferResult']['EndDate']; ?></span> </div>
						</div>
						<div class="clearfix"></div>
						<div class="tabledates for_hotel">
							<!--  Check in  -->
							<div class="tablecelfty">
								<div class="borddo brdrit">
									<div class="fuldate">
										<span class="bigdate"><?php echo date('d',strtotime($request->start_date)); ?></span>
										<div class="biginre"><?php echo date('M',strtotime($request->start_date)); ?><br>
											<?php echo date('Y',strtotime($request->start_date)); ?> 
										</div>
									</div>
								</div>
							</div>
							<!--  Check out  -->
							<div class="tablecelfty">
								<div class="borddo">
									<div class="fuldate">
										<span class="bigdate"><?php echo date('d',strtotime($request->start_date)); ?></span>
										<div class="biginre"> <?php echo date('M',strtotime($request->start_date)); ?><br>
											<?php echo date('Y',strtotime($request->start_date)); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="setMinPrice" value="" id="setMinPrice">
		<input type="hidden" name="setMaxPrice" value="" id="setMaxPrice">
		<input type="hidden" value="" id='session_id'>
		
		
		<!--Map view indipendent hotel-->

        <!-- End -->
        <script>var WEB_URL = "<?php echo ASSETS; ?>"</script>
        <script type="text/javascript" src="<?php echo ASSETS ?>assets/js/owl.carousel.min.js"></script> 
        <script type="text/javascript" src="<?php echo ASSETS ?>assets/js/custom.js"></script> 
        <script type="text/javascript" src="<?php echo ASSETS ?>assets/js/hotel_filter.js"></script>
        <script src="<?php echo ASSETS ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script> 
        <script type="text/javascript">
			var ASSETS	= "<?php echo ASSETS; ?>assets/";
			var WEB_URL	= "<?php echo ASSETS; ?>";
			var markers = '{}';
			var geometry = {"latitude":25.2048493,"longitude":55.2707828,"status":"OK"};
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script> 
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/map.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
				 if($('#transfer_country').val() != ''){
					 getCities1($('#transfer_country').val());
					 $("#transfer_city").val('<?php echo $transfer_request['transfer_city_code']; ?>')
				 }
				
            	$('.scrolltop').click(function(){
            		$("html, body").animate({ scrollTop: 0 }, 600);
            	 });
            	 
            	 $('[data-toggle="popover"]').popover();
            	 
            	$('.advncebtn').click(function(){
            	$(this).parent('.togleadvnce').toggleClass('open');
            });
            
            $('.alladvnce').click(function(){
            	$('.remngwd').toggleClass("chageup");
            	$('.advncedown').toggleClass("fadeinn");
            });
            
            $('.alladvnce').click(function(e){
            	e.stopPropagation();
            });
            
            $(document).click(function(){
            	$('.advncedown').removeClass("fadeinn");
            	$('.remngwd').removeClass("chageup");
            });
            	
            	
            $(document).on('click','.cmnact',function(){
				$(this).text(function (index, text) {
					return (text == 'More' ? 'Less' : 'More');
				});
				$(this).parent(".boxins").toggleClass('open')
			});
            
             /*Star rating click*/
              $('.toglefil').click(function(){
                $(this).toggleClass('active');
              });
              /*Star rating click end*/
              
              /*Map view click function*/
            	$('.map_click').click(function(){
            		$('.allresult').addClass('map_open');
            		$('.view_type').removeClass('active');
            		$(this).addClass('active');
            		
            		$(".hotels_results").niceScroll({styler:"fb",cursorcolor:"#4ECDC4", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});
            		
            		setTimeout(function(){
            			google.maps.event.trigger(map, 'resize');
            		},500);
               	 	
            	});
            	
            	$('.hotel_addressmap').click(function(){
            		setTimeout(function(){
            		initialize();
            	},200);
            	});
            	
            	$('.list_click').click(function(){
            		$('.allresult').removeClass('map_open');
            		$('.view_type').removeClass('active');
            		$(this).addClass('active');
            	});
            	
            	/*Map view click function end*/
            	
            	
            	/*Facilities tooltip*/
            	$(".tooltipv").tooltip();
            	
            
              
              
            });
        </script> 
        <script>
            //filter toggle	
            	
            	$('.filter_show').click(function(){
            		$('.filtrsrch').addClass('open');
            	});
            	
            	$('.close_filter').click(function(){
            		$('.filtrsrch').removeClass('open');
            	});
        </script>
     
        <script>
			$(document).ready(function() {
				var start = new Date().getTime(),difference;
				var a = [<?php echo $api_det; ?>];
				var i = 0;
				var k = 1;
				function nextCall() {
					if(i==0) {
						$('.imgLoader').fadeIn();
						$('body').css('overflow', 'hidden');
					}
					var x = a.length;
					if (i == a.length) {
						$('.imgLoader').fadeOut();
						$('body').css('overflow', '');
						//setclick();
						return;
					}
					$.ajax({
						dataType: 'json',
						url: '<?php echo base_url(); ?>transfer/call_api/' + a[i],
						data: { request: '<?php echo $req; ?>', sessiondata : '<?php echo $session_data; ?>'},
						beforeSend: function() {
							$('.imgLoader').fadeIn();
							$('.ppage, .pagination').hide();              
						},
						success: function(data) {
							$('.imgLoader').fadeOut();
							if (parseInt(data.total_result) == 0) {
								$('#noresult').fadeIn();
							}
						    $('#transfer_result').html(data.transfer_search_result);
							$('#total_result_count').html(data.total_result);
							$('#min_price').html(data.min_val);
							var minVal = Math.floor(parseFloat(data.min_val));
							var maxVal = Math.ceil(parseFloat(data.max_val));
							$( "#setMinPrice" ).val(minVal);
							$( "#setMaxPrice" ).val(maxVal);
							
							$("#session_id").val(data.session_id);
							$("#request_string").val(data.requeststring);
							$("#api_id").val(data.api_id);
							
							//~ $("#amount").val("<?php echo BASE_CURRENCY;?> " + minVal + " - <?php echo BASE_CURRENCY;?> " + maxVal);
							$("#slider-range").slider({
								range: true,
								min: minVal,
								max: maxVal,
								values: [minVal, maxVal],
								slide: function (event, ui) {
									$("#amount").val("<?php echo BASE_CURRENCY;?> " + ui.values[ 0 ] + " - <?php echo BASE_CURRENCY;?> " + ui.values[ 1 ]);
								},
								change: function (event, ui)
								{
									if (event.originalEvent) {
										filter();
									}
								}
							});
							
							$("#amount").val(" <?php echo BASE_CURRENCY;?> " + $("#slider-range").slider("values", 0) + " - <?php echo BASE_CURRENCY;?> " + $("#slider-range").slider("values", 1));

							var InclusionString = '';
							if( $.isArray(data.inclusion)) {
								var areaCount = 0;
								for (var a = 0; a < data.inclusion.length; a++) {
									if(data.inclusion[a] != ""){
										InclusionString += '<li>'+''+
																'<div class="squaredThree">'+''+
																	'<input type="checkbox" name="" class="filter_accommodation" id="amnts'+ a +'" name="hotel_fac_val1"  value="' + data.inclusion[a] + '">'+''+
																	'<label for="amnts'+ a +'"></label>'+''+
																'</div>'+''+
																'<label for="amnts'+ a +'" class="lbllbl">'+ data.inclusion[a] +'</label><span class="countrgt"></span>'+''+
															'</li>';
										areaCount = areaCount+1;
									}
								}
							}
							$("#accommodations").html(InclusionString);
							if(areaCount > 4) {
								var moreString = '<a class="cmnact">More</a>';
								$("#accommodations").parent().find(".cmnact").remove();
								$("#accommodations").parent().append(moreString);
							}
							
							var AmmenityString = '';
							if( $.isArray(data.ammenity)) {
								var ameIcon = new Array('IN','PA','BF','RS','SP','AS','NS','GY','DF','BC');
								var facCount = 0;
								for (var b = 0; b < data.ammenity.length; b++) {
									if(data.ammenity[b] != ""){
										var amentval = data.ammenity[b].split('@');
										if(amentval[1] != ""){
											if((ameIcon.indexOf(amentval[0])) != -1) {
												AmmenityString += '<li>'+''+
																		'<div class="squaredThree">'+''+
																			'<input type="checkbox" name="hotel_ammenity_val1" class="filter_ammenity" id="hotelamnts'+ b +'" value="' + data.ammenity[b] + '">'+''+
																			'<label for="hotelamnts'+ b +'"></label>'+''+
																		'</div>'+''+
																		'<label for="hotelamnts'+ b +'" class="lbllbl"><span class="htfiltefac '+amentval[0]+'"></span>'+amentval[1]+'</label><span class="countrgt"></span>'+''+
																	'</li>';
												facCount = facCount+1;
											}
										}
									}
								}
							}
							$("#facilities").html(AmmenityString);
							if(facCount > 4) {
								var moreString = '<a class="cmnact">More</a>';
								$("#facilities").parent().find(".cmnact").remove();
								$("#facilities").parent().append(moreString);
							}
							//Star count
							var categoryCodeString = '';
							var zero_star = 0;
							var star_rating=0;
							var one_star=0;
							var two_star=0;
							var three_star=0;
							var four_star=0;
							var five_star=0;
					   
							if( $.isArray(data.category)) {
								for (var a = 0; a < data.category.length; a++) {
									var star = data.category[a];
							
									if($.isNumeric(star[0])){
										star_rating = star[0];
									}
									else{
										star_rating=0;
									}
							
									if(star_rating == 0) 
										zero_star = zero_star + 1;
									if(star_rating == 1) 
										one_star = one_star + 1;
									if(star_rating == 2) 
										two_star = two_star + 1;
									if(star_rating == 3) 
										three_star = three_star + 1;
									if(star_rating == 4) 
										four_star = four_star + 1;
									if(star_rating == 5) 
										five_star = five_star + 1;
								}
							}
							categoryCodeString = '<li>'+''+
														'<div class="squaredThree">'+''+
															'<input type="checkbox" value="5" name="" class="filter_airline filter_depart timone toglefil filter_depart_btn" data-type="5_star" id="five_star_check" >'+''+
															'<label for="five_star_check"></label>'+''+
														'</div>'+''+
														'<label for="five_star_check" class="lbllbl">'+''+
															'<div class="stra_hotel" data-star="5">'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
															'</div>'+''+
														'</label>'+''+
														'<span class="countrgt htlcount">('+five_star+')</span>'+''+
													'</li>'+''+
													'<li>'+''+
														'<div class="squaredThree">'+''+
															'<input type="checkbox" value="4" name="" class="filter_airline filter_depart timone toglefil filter_depart_btn" data-type="4_star" id="four_star_check" >'+''+
															'<label for="four_star_check"></label>'+''+
														'</div>'+''+
														'<label for="four_star_check" class="lbllbl">'+''+
															'<div class="stra_hotel" data-star="4">'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
															'</div>'+''+
														'</label>'+''+
														'<span class="countrgt htlcount">('+four_star+')</span>'+''+
													'</li>'+''+
													'<li>'+''+
														'<div class="squaredThree">'+''+
															'<input type="checkbox" value="3" name="" class="filter_airline filter_depart timone toglefil filter_depart_btn" data-type="3_star" id="three_star_check" >'+''+
															'<label for="three_star_check"></label>'+''+
														'</div>'+''+
														'<label for="three_star_check" class="lbllbl">'+''+
															'<div class="stra_hotel" data-star="3">'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
															'</div>'+''+
														'</label>'+''+
														'<span class="countrgt htlcount">('+three_star+')</span>'+''+
													'</li>'+''+
													'<li>'+''+
														'<div class="squaredThree">'+''+
															'<input type="checkbox" value="2" name="" class="filter_airline filter_depart timone toglefil filter_depart_btn" data-type="2_star" id="two_star_check" >'+''+
															'<label for="two_star_check"></label>'+''+
														'</div>'+''+
														'<label for="two_star_check" class="lbllbl">'+''+
															'<div class="stra_hotel" data-star="2">'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
															'</div>'+''+
														'</label>'+''+
														'<span class="countrgt htlcount">('+two_star+')</span>'+''+
													'</li>'+''+
													'<li>'+''+
														'<div class="squaredThree">'+''+
															'<input type="checkbox" value="1" name="" class="filter_airline filter_depart timone toglefil filter_depart_btn" data-type="1_star" id="one_star_check" >'+''+
															'<label for="one_star_check"></label>'+''+
														'</div>'+''+
														'<label for="one_star_check" class="lbllbl">'+''+
															'<div class="stra_hotel" data-star="1">'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
															'</div>'+''+
														'</label>'+''+
														'<span class="countrgt htlcount">('+one_star+')</span>'+''+
													'</li>'+''+
													'<li>'+''+
														'<div class="squaredThree">'+''+
															'<input type="checkbox" value="0" name="" class="filter_airline filter_depart timone toglefil filter_depart_btn" data-type="0_star" id="zero_star_check" >'+''+
															'<label for="zero_star_check"></label>'+''+
														'</div>'+''+
														'<label for="zero_star_check" class="lbllbl">'+''+
															'<div class="stra_hotel" data-star="0">'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
																'<span class="fa fa-star"></span>'+''+ 
															'</div>'+''+
														'</label>'+''+
														'<span class="countrgt htlcount">('+zero_star+')</span>'+''+
													'</li>';
							$("#star_rating_nav").html(categoryCodeString);
							i++;
							nextCall();
						}
					});
				}
				nextCall();
			});
        </script>
        <script>
			var map;        
			var myCenter=new google.maps.LatLng(67.78388, -13.3334);
			var icon1 = "<?php echo ASSETS;?>assets/images/marker_out.png";
			var marker=new google.maps.Marker({
				position:myCenter,
				 icon: icon1,
			});
			function initialize() {
				var mapProp = {
					center:myCenter,
					zoom: 16,
					draggable: false,
					scrollwheel: false,
					mapTypeId:google.maps.MapTypeId.ROADMAP
				};
  
				map=new google.maps.Map(document.getElementById("map_alone"),mapProp);
				marker.setMap(map);
    
				google.maps.event.addListener(marker, 'click', function() {
      					infowindow.setContent(contentString);
					infowindow.open(map, marker);
				}); 
			};
			google.maps.event.addDomListener(window, 'load', initialize);
			google.maps.event.addDomListener(window, "resize", resizingMap());

			$('#map_view_hotel').on('show.bs.modal', function(e) { 
				var $invoker = $(e.relatedTarget);
				var latitude = $invoker[0].dataset.lat;
				var longitude = $invoker[0].dataset.long;
				var title = $invoker[0].dataset.title;
				$('.modal-title').text(title);
				var map;  
				
				var bounds = new google.maps.LatLngBounds();
				var myCenter=new google.maps.LatLng(latitude, longitude);
				var icon1 = "<?php echo ASSETS;?>assets/images/marker_out.png";
					var mapProp = {
					center:myCenter,
					zoom: 16,
					position: new google.maps.LatLng(latitude, longitude),
					mapTypeId:google.maps.MapTypeId.ROADMAP
				};
  
				map=new google.maps.Map(document.getElementById("map_alone"),mapProp);
				var marker=new google.maps.Marker({
					position:myCenter,
					icon: icon1,
					 map: map

				});
				bounds.extend(marker.position);
				//Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)

				marker.setMap(map);
				map.setCenter(bounds.getCenter());
				map.fitBounds(bounds);
				google.maps.event.addListenerOnce(map, 'idle', function() {
					google.maps.event.trigger(map, 'resize');
				});
			})
			function resizeMap() {
				if(typeof map =="undefined") return;
				setTimeout( function(){resizingMap();} , 600);
			}

			function resizingMap() { 
				if(typeof map =="undefined") return;
				var center = map.getCenter();
				google.maps.event.trigger(map, "resize");
				map.setCenter(center); 
			}
			$(document).on("click","#reset_filter",function() {
				$("#hotel_namefilter").val('');
				$('.filter_ammenity').attr('checked', false);
				$('.filter_accommodation').attr('checked', false);
	
				$(".filter_depart").prop('checked', false);
				var options = $("#slider-range").slider( 'option' );
				$("#amount").val(" <?php echo BASE_CURRENCY;?> " + options.min + " - <?php echo BASE_CURRENCY;?> " + options.max);
				$("#slider-range").slider( 'values', [ options.min, options.max ] );
				filter();
			});
			
			 function getCities(country){
				$.ajax({
				type: "POST",
				url: "<?php echo base_url().'dashboard/getTransferCities/'; ?>"+country.value,
				dataType: "json",
				success: function(data){
					$("#transfer_city").empty();
					 $("#transfer_city").append(data.option);
					return false;
				}
				});
			}
			
			 
 function getCities1(country){
	 $.ajax({
			type: "POST",
			url: "<?php echo base_url().'dashboard/getTransferCities/'; ?>"+country,
			dataType: "json",
			success: function(data){
				$("#transfer_city").empty();
				 $("#transfer_city").append(data.option);
				return false;
			}
		});
 }
        </script>
    </body>
</html>
