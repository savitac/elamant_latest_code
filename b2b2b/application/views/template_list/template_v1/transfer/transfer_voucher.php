<?php
	 $cid = $Booking[0]->cid;
	$TravelerDetails = json_decode($Booking[0]->TravelerDetails);
	
	//~ echo "data: <pre>";print_r($adult_count);exit; 
	 $travel_date = date('Y-m-d' , strtotime($Booking[0]->travel_date));
	
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo $this->TravelLights['TransferVoucher']['TransferVocher']; ?></title>
        <link href="<?php echo ASSETS; ?>assets/css/font-awesome.min.css" rel="stylesheet" />
        <link href="<?php echo ASSETS; ?>assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo ASSETS; ?>assets/css/voucher.css" rel="stylesheet" />
         <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    </head>
    <body>
        <!-- Navigation --> 
        <div class="clearfix"></div>
        <div class="top80">
            <div class="full marintopcnt contentvcr" id="voucher" class="printthis">
                <div class="container offset-0">
                    <div class="col-md-12 nopad" >
                        <table cellspacing="0" cellpadding="0" border="0" align="center" style="width:75%;margin: 10px auto; color: #444; border: 1px solid #dddddd; padding: 15px">
                            <tbody id="print_voucer">
                                <tr>
                                    <td style="padding: 20px">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table class="insideone">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="thrty first" style="width:33.33%">
                                                                       	<?php if($this->session->userdata('site_name') == ''){ ?>  
																								<div class="logovcr"> <a href="<?php echo site_url('dashboard'); ?>" ><img src="<?php echo ASSETS; ?>assets/images/logo.png" style="max-width:150px;"></a></div>
																								<?php } else { ?>
																								<div class="logovcr"> <a href="<?php echo site_url('dashboard'); ?>" ><img src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $this->session->userdata('domain_logo'); ?>" style="max-width:150px;"></a></div>	
																		<?php 	} ?>
                                                                    </td>
                                                                    <td class="thrty second" style="width:33.33%"><span class="faprnt fa fa-print" onClick="window.print()" ></span></td>
                                                                    <td class="thrty third" style="width:33.33%">
                                                                        <div class="adrssvr"> <br>
                                                                            <br>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="bighedingv" style=" color: #444444;display: block;font-size: 20px;overflow: hidden;padding: 10px 0;text-align: center;"><?php echo $this->TravelLights['TransferVoucher']['ConfirmationLetter']; ?></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height:20px;width:100%;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="border:0px;" colspan="2">
                                                        <table width="100%" cellspacing="0" cellpadding="8" border="0" align="center">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td width="50%" valign="top" align="left">
                                                                                        <table width="100%" cellspacing="1" cellpadding="7" border="0" bgcolor="#FFFFFF">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td align="left" style="color: #666;padding-bottom: 10px" colspan="2"><strong style="font-size:18px;"><?php echo $user_details[0]->user_name; ?></strong></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['BookingStatus']; ?> :</td>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $pnr_nos[0]->booking_status; ?></strong></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['VoucherNo']; ?> :</td>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo substr($Booking[0]->pnr_no, 6); ?></strong></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['BookingNo']; ?> :</td>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $Booking[0]->booking_no;?></strong></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['TheChinaGapRefNo']; ?> :</td>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $Booking[0]->pnr_no;?></strong></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['DateofIssue']; ?> :</td>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo date('d-m-Y',strtotime($Booking[0]->voucher_date));?></strong></td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td width="50%" valign="top" align="left">
                                                                                        <table width="100%" cellspacing="1" cellpadding="7" border="0" bgcolor="#FFFFFF">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td align="left" colspan="2">&nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left">&nbsp;</td>
                                                                                                    <td width="50%" align="left">&nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left">&nbsp;</td>
                                                                                                    <td align="left">&nbsp;</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                
                                                                
                                                                <tr>
                                                                    <td style="height:20px;width:100%;"></td>
                                                                </tr>
                                                                
                                                                <?php
																							if($Booking[0]->payment_type == "DEPOSIT") {
																								$total_amount = $Booking[0]->total_amount;
																								$transaction_amount = $Booking[0]->transaction_amount;
																							}
																							elseif($Booking[0]->payment_type == "CREDITCARD") {
																								$total_amount = $Booking[0]->transaction_amount;
																								$transaction_amount = $Booking[0]->transaction_amount;
																							}
																							elseif($Booking[0]->payment_type == "BOTH") {
																								$transaction_amount = 'Not Defined';
																								$total_amount = $Booking[0]->transaction_amount;
																							}
																							elseif($Booking[0]->payment_type == "PAYLATER") {
																								$total_amount = $Booking[0]->total_amount;
																								$transaction_amount = $Booking[0]->transaction_amount;
																								$due_amount = $Booking[0]->due_amount;
																							}
																							else {
																								
																							}
																						?>
																<tr>
																							<td width="50%" valign="top" align="left">
																								<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
																									<tbody>
																										<tr>
																											<td colspan="2"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;"><?php echo $this->TravelLights['TransferVoucher']['PaymentDetails']; ?></span></td>
																										</tr>
																										<tr>
																											<td width="30%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['PaymentStatus']; ?>:</td>
																											<td width="70%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $Booking[0]->payment_status ?></strong></td>
																										</tr>
																										<tr>
																											<td width="30%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['TransactionReference']; ?>:</td>
																											<td width="70%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $Booking[0]->transaction_reference ?></strong></td>
																										</tr>
																										<tr>
																											<td width="30%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['TotalAmount']; ?>:</td>
																											<td width="70%" align="left" style="font-size: 15px; line-height: 28px; color: #0e7ab1"><strong class="colored"><?php echo $Booking[0]->api_currency." ".$total_amount; ?></strong></td>
																										</tr>
																										<?php
																											if($Booking[0]->payment_type == "PAYLATER") {
																										?>
																												<tr>
																													<td width="30%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['AmountDue']; ?>:</td>
																													<td width="70%" align="left" style="font-size: 15px; line-height: 28px; color: #0e7ab1"><strong class="colored"><?php echo $Booking[0]->api_currency." ".$total_amount; ?></strong></td>
																												</tr>
																										<?php
																											}
																										?>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						
																						<?php
																							if($Booking[0]->booking_status == "CANCELLED") {
																						?>
																						<tr>
																							<td width="50%" valign="top" align="left">
																								<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
																									<tbody>
																										<tr>
																											<td colspan="2"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;"><?php echo $this->TravelLights['TransferVoucher']['CancellationDetails']; ?></span></td>
																										</tr>
																										<tr>
																											<td width="30%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['CancellationCharge']; ?>:</td>
																											<td width="70%" align="left" style="font-size: 15px; line-height: 28px; color: #0e7ab1"><strong class="colored"><?php echo $Booking[0]->api_currency." ".$Booking[0]->cancellation_amount; ?></strong></td>
																										</tr>
																										<tr>
																											<td width="30%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><?php echo $this->TravelLights['TransferVoucher']['RefundedAmound']; ?>:</td>
																											<td width="70%" align="left" style="font-size: 15px; line-height: 28px; color: #0e7ab1"><strong class="colored"><?php echo $Booking[0]->api_currency." ".$Booking[0]->refund_amount; ?></strong></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<?php
																							}
																						?>						
                                                                <tr>
                                                                    <td style="height:20px;width:100%;"></td>
                                                                </tr>
                                                                
                                                                <tr>
																
																	<?php  
																	 
																     $transfer_request =  json_decode(base64_decode($Booking[0]->transfer_request));
																     $travel_details = json_decode($Booking[0]->TravelerDetails, true);
																      $vehicle_details = json_decode($Booking[0]->vehicle_details);
																       
																	        $cid = $Booking[0]->cid;
																			$gender ="a_gender".$cid;
																			$firstname ="first_name".$cid;
																			$passenger_type ="a_type".$cid;
																			$lastname ="last_name".$cid;
																			$flight_code = "flight_code".$cid;
																			$flight_number = "flight_number".$cid;
																			$flight_code1 = "flight_code1".$cid;
																			$flight_number1 = "flight_number1".$cid;
																			$hotel_code = "hotel_code".$cid;
																			$hotel_code1 = "hotel_code1".$cid;
																			$port_code = "port_code".$cid;
																			$ship_name = "ship_name".$cid;
																			$ship_company = "ship_company".$cid;
																			$ship_name1 = "ship_name1".$cid;
																			$ship_company1 = "ship_company1".$cid;
																			$arrival_time = "arrival_time".$cid;
																			$departure_time = "departure_time".$cid;
																			$address1 = "address1".$cid;
																			$address2 = "address2".$cid;
																			$city = "city".$cid;
																			$telephone = "telephone".$cid;
																			$station_code = "station_code".$cid;
																			$train_name = "train_name".$cid;
																			$station_code1 = "station_code1".$cid;
																			$train_name1 = "train_name1".$cid;
																																 
																		 ?>
                                                                    <td bgcolor="#ffffff" align="center" style="border: 1px solid #cccccc; display: -moz-grid;overflow: auto;width: 100%;">
                                                                        <table width="100%" cellspacing="0" cellpadding="7" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
																			
																				<?php 
																				   if($Booking[0]->pickup_code == 'A') { 
																					$airports =  $this->General_Model->getTransferAirports($Booking[0]->pickup_city_code)->row();
																					$pickup_point = $airports->airport_name;
																					} elseif($Booking[0]->pickup_code == 'H') { 
																					$hotels =  $this->General_Model->getTransferHotels($Booking[0]->search_city_code, $travel_details[$hotel_code])->row();	
																					$pickup_point = $hotels->HotelName;
																				    } elseif($Booking[0]->pickup_code == 'S') { 
																					$station =  $this->General_Model->getStationsDetails($Booking[0]->pickup_city_code, $travel_details[$station_code])->row();
																					$pickup_point = $station->station_name;
																					} elseif($Booking[0]->pickup_code == 'P') { 
																					 $ports =  $this->General_Model->getPortCities($Booking[0]->pickup_city_code)->row(); 
																					 $pickup_point = $ports->city_name;
																					} ?>
																				
																				 <?php if($Booking[0]->dropoff_code == 'A') { 
																					 $airports1 =  $this->General_Model->getTransferAirports($Booking[0]->dropoff_city_code)->row();
																					  $dropoff_point = $airports1->airport_name;
																					  } elseif($Booking[0]->dropoff_code == 'H') {
																						  if(isset($travel_details[$hotel_code1])){
																							 $hotels1 =  $this->General_Model->getTransferHotels($Booking[0]->search_city_code, $travel_details[$hotel_code1])->row();  
																						  }else{
																						  $hotels1 =  $this->General_Model->getTransferHotels($Booking[0]->search_city_code, $travel_details[$hotel_code])->row();
																					      }
																					      $dropoff_point = $hotels1->HotelName;
																					  } elseif($Booking[0]->dropoff_code == 'S') {
																						  if(isset($travel_details[$station_code1])){
																							$station =  $this->General_Model->getStationsDetails($Booking[0]->dropoff_city_code, $travel_details[$station_code1])->row();
																						  }else{
																						  $station =  $this->General_Model->getStationsDetails($Booking[0]->dropoff_city_code, $travel_details[$station_code])->row();
																					      }
																						  $dropoff_point = $station->station_name;
																					  } elseif($Booking[0]->dropoff_code == 'P') { 
																					      $ports =  $this->General_Model->getPortCities($Booking[0]->dropoff_city_code)->row(); 
																					      $dropoff_point = $ports->city_name;
																					  } ?>
																				 
																				  
                                                                                <tr>
                                                                                    <td bgcolor="#f1f1f1" style="border-bottom: 1px solid #cccccc;" colspan="4">
                                                                                        <span style="display: block; font-size: 18px;padding:5px 10px"> <?php echo $pickup_point." To ".$dropoff_point;?>  </span>
                                                                                    </span>
                                                                                    </td>
                                                                                </tr>
                                                                              
                                                                                <tr>
                                                                                   <td width="25%" bgcolor="#FFFFFF" style="border-bottom:1px solid #cccccc; border-right:1px solid #cccccc; padding: 10px"><span style="font-size:16px; font-weight:bold;"><?php echo $this->TravelLights['TransferVoucher']['Pickup']; ?></span><br>
                                                                                   <?php echo $pickup_point; ?>
                                                                                     
                                                                                   </td>
                                                                                    <td width="25%" bgcolor="#FFFFFF" style="padding: 10px;border-bottom:1px solid #cccccc; border-right:1px solid #cccccc;"><span style="font-size:16px; font-weight:bold;"><?php echo $this->TravelLights['TransferVoucher']['Dropoff']; ?></span><br><?php echo $dropoff_point;?></td>
                                                                                    <td width="35%" bgcolor="#FFFFFF" style="padding: 10px" rowspan="2"><strong><?php echo $this->TravelLights['TransferVoucher']['Guests']; ?></strong> <br>
                                                                                        <br>
                                                                                        <?php echo $this->TravelLights['TransferVoucher']['Passenger']; ?> : <?php echo $transfer_request->no_of_travellers; ?>                                       <br>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="border-right:1px solid #cccccc;padding: 10px"><?php echo $this->TravelLights['TransferVoucher']['Date']; ?><br>
                                                                                       <span style="font-size:13px; font-weight:bold;"><?php echo date('d-m-Y', strtotime($Booking[0]->travel_start_date)); ?></span>
                                                                                    </td>
                                                                                    <td bgcolor="#FFFFFF" style="border-right:1px solid #cccccc; padding: 10px"><?php echo $this->TravelLights['TransferVoucher']['Duration']; ?><br>
                                                                                        <span style="font-size:13px; font-weight:bold;"><?php echo $Booking[0]->approximate_time; ?> <?php echo $this->TravelLights['TransferVoucher']['Hours']; ?></span>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                 
                                                                  <td bgcolor="#ffffff" align="center" style="border: 1px solid #cccccc; display: -moz-grid;overflow: auto;width: 100%;">
                                                                        <table width="100%" cellspacing="0" cellpadding="7" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
																			
                                                                                <tr>
                                                                                   <td width="50%" bgcolor="#FFFFFF" style="border-bottom:1px solid #cccccc; border-right:1px solid #cccccc; padding: 10px"><span style="font-size:16px; font-weight:bold;"><?php echo $this->TravelLights['TransferVoucher']['PickupDetails']; ?></span><br><?php 
                                                                                   if($Booking[0]->pickup_code == 'A') {  ?>
																					     <strong>Airport Name : </strong> <?php echo $pickup_point; ?>
																						 <br /><strong>Flight Number :</strong> <?php echo  $travel_details[$flight_number]; ?>
																						 <br /><strong>Arrival Time : </strong>  <?php echo  $travel_details[$arrival_time]; ?>
																					  <?php } elseif($Booking[0]->pickup_code == 'H') {  ?>
																					     <strong>Hotel Name : </strong> <?php echo $pickup_point; ?>
																						 <br /><strong>Address :</strong> <?php echo  $hotels->Address; ?>
																						 <br /><strong>Starting At :</strong> <?php echo  $travel_details[$arrival_time]; ?>
																						 
																					  <?php } elseif($Booking[0]->pickup_code == 'S'){ ?> 
                                                                                         <strong>Station Name : </strong> <?php echo $pickup_point; ?>
																						 <br /><strong>Train Name :</strong> <?php echo  $travel_details[$train_name]; ?>
																						 <br /><strong>Departure Time : </strong>  <?php echo  $travel_details[$arrival_time]; ?>
																				  <?php } elseif($Booking[0]->pickup_code == 'P'){ ?> 
                                                                                         <strong>Port City : </strong> <?php echo $dropoff_point; ?>
                                                                                         <br />
                                                                                         <strong>Ship Company Name : </strong> <?php  echo $travel_details[$ship_company] ; ?>
																						 <br /><strong>Ship Name :</strong>  <?php   echo $travel_details[$ship_name] ;  ?> 
																						 <br /><strong>Departure Time : </strong>  <?php echo  $travel_details[$arrival_time]; ?>
																				    <?php }?> </td>
                                                                                    <td width="50%" bgcolor="#FFFFFF" style="padding: 10px;border-bottom:1px solid #cccccc; border-right:1px solid #cccccc;"><span style="font-size:16px; font-weight:bold;"><?php echo $this->TravelLights['TransferVoucher']['DropoffDetails']; ?></span><br>
                                                                                   <?php if($Booking[0]->dropoff_code == 'H') {  ?>
																					    <strong> Hotel Name  : </strong> <?php echo $dropoff_point; ?>
																						 <br /><strong>Address : </strong><?php echo  $hotels1->Address; ?>
																				    <?php }elseif($Booking[0]->dropoff_code == 'A'){ ?> 
                                                                                         <strong>Airport Name : </strong> <?php echo $dropoff_point; ?>
																						 <br /><strong>Flight Number :</strong> <?php   if(isset($travel_details[$flight_number1])) { echo $travel_details[$flight_number1] ; } else{ echo $travel_details[$flight_number] ; } ?>
																						 <br /><strong>Departure Time : </strong>  <?php echo  $travel_details[$departure_time]; ?>
																				    <?php } elseif($Booking[0]->dropoff_code == 'S'){ ?> 
                                                                                         <strong>Station Name : </strong> <?php echo $dropoff_point; ?>
																						 <br /><strong>Train Name :</strong> <?php  if(isset($travel_details[$train_name1])) { echo $travel_details[$train_name1] ; } else{ echo $travel_details[$train_name] ; }  ?>
																						 <br /><strong>Departure Time : </strong>  <?php  echo  $travel_details[$departure_time]; ?>
																				  <?php } elseif($Booking[0]->dropoff_code == 'P'){ ?> 
                                                                                         <strong>Port City : </strong> <?php echo $dropoff_point; ?>
                                                                                         <br />
                                                                                         <strong>Ship Company Name : </strong> <?php if(isset($travel_details[$ship_company1])) { echo $travel_details[$ship_company1] ; } else{ echo $travel_details[$ship_company] ; } ?>
																						 <br /><strong>Ship Name :</strong>  <?php   if(isset($travel_details[$ship_name1])) { echo $travel_details[$ship_name1] ; } else{ echo $travel_details[$ship_name] ; } ?> 
																						 <br /><strong>Departure Time : </strong>  <?php echo  $travel_details[$departure_time]; ?>
																				    <?php }?>
                                                                                    </td>
                                                                                 </tr>
                                                                               
                                                                            </tbody>
                                                                        </table>
                                                                   </td>
                                                                </tr>
                                                                
                                                               
                                                                <tr>
                                                                    <td style="height:10px;width:100%;"></td>
                                                                </tr>
                                                                 <tr>
                                                                    <td>
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="5"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin-bottom: 15px;padding: 10px 0;"><?php echo $this->TravelLights['TransferVoucher']['VehicleDetails']; ?></span></td>
                                                                                </tr>
                                                                                	<tr style="background:#f2f2f2; border: 1px solid #eeeeee;">
																							<th valign="top" align="left" style="padding: 10px"><strong><?php echo $this->TravelLights['TransferVoucher']['VehcileName']; ?></strong></th>
																							<th valign="top" align="left" style="padding: 10px"><strong><?php echo $this->TravelLights['TransferVoucher']['MaximumLuggage']; ?></strong></th>
																							<th valign="top" align="left"  style="padding: 10px"><strong><?php echo $this->TravelLights['TransferVoucher']['MaximumPassengers']; ?></strong></th>
																					</tr>
																					<tr style="background:#ffffff; border: 1px solid #eeeeee;">
																									<td style="padding: 10px"><?php echo $vehicle_details->vehicle_name; ?></td>
																									<td  style="padding: 10px"><?php echo $vehicle_details->maximum_luggage; ?></td>
																									<td  style="padding: 10px"><?php echo $vehicle_details->maximum_passengers; ?></td>
																					</tr>
																		 </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height:10px;width:100%;"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="5"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin-bottom: 15px;padding: 10px 0;"><?php echo $this->TravelLights['TransferVoucher']['TravellerDetails']; ?></span></td>
                                                                                </tr>
                                                                                	<tr style="background:#f2f2f2; border: 1px solid #eeeeee;">
																							<th valign="top" align="left" style="padding: 10px"><strong><?php echo $this->TravelLights['TransferVoucher']['PassengerType']; ?></strong></th>
																							<th valign="top" align="left" colspan="3" style="padding: 10px"><strong><?php echo $this->TravelLights['TransferVoucher']['Name']; ?></strong></th>
																							
																					</tr>
																					<?php   for($pass =0; $pass < count($travel_details[$gender]); $pass++){ ?>
																					<tr style="background:#ffffff; border: 1px solid #eeeeee;">
																						         
																									<td style="padding: 10px"> <?php echo $travel_details[$passenger_type][$pass]; ?> </td>
																									<td  style="padding: 10px"><?php echo $travel_details[$gender][$pass]." ".$travel_details[$firstname][$pass]." ".$travel_details[$lastname][$pass]; ?></td>
																									
																					</tr>
																					<?php } ?>
																		 </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height:10px;width:100%;"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="2"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;"><?php echo $this->TravelLights['TransferVoucher']['CustomerDetails']; ?></span></td>
                                                                                </tr>
                                                                                <tr style="border: 1px solid #eee;">
                                                                                    <td width="20%" align="left" style="background:#f1f1f1;padding: 10px"><strong><?php echo $this->TravelLights['TransferVoucher']['EmailID']; ?></strong></td>
                                                                                    <td width="80%" align="left" style="background:#ffffff;padding: 10px"><?php echo $Booking[0]->billing_email;?></td>
                                                                                </tr>
                                                                                <tr style="border: 1px solid #eee;">
                                                                                    <td align="left" style="background:#f1f1f1;padding: 10px"><strong><?php echo $this->TravelLights['TransferVoucher']['MobileNumber']; ?></strong></td>
                                                                                    <td align="left" style="background:#ffffff;padding: 10px"><?php echo $Booking[0]->billing_contact_number;?></td>
                                                                                </tr>
                                                                                <tr style="border: 1px solid #eee;">
                                                                                    <td align="left" style="background:#f1f1f1;padding: 10px"><strong><?php echo $this->TravelLights['TransferVoucher']['Address']; ?></strong></td>
                                                                                    <td align="left" style="background:#ffffff;padding: 10px"><?php echo $Booking[0]->billing_address.', '.$Booking[0]->billing_city.', '.$Booking[0]->billing_state.', '.$Booking[0]->billing_zip;?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                               
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php
													if(false) {
												?>
														<tr>
															<td colspan="2">
																<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
																	<tbody>
																		<tr>
																			<td><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;"><?php echo $this->TravelLights['TransferVoucher']['AmendmentPolicy']; ?> </span></td>
																		</tr>
																		<tr>
																			<td valign="top" align="left" style="padding:10px 0;"><span style="font-size: 14px;padding: 10px 0;"><?php echo $Booking[0]->amendment_policy;?></span></td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
												<?php
													}
                                                ?>
                                                <?php if($Booking[0]->transfer_conditions != ''){ ?>
                                                <tr>
                                                    <td colspan="2">
                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                            <tbody>
                                                                <tr>
                                                                    <td><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;"><?php echo $this->TravelLights['TransferVoucher']['TransferDescription']; ?> </span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" align="left" style="padding:10px 0;"><span style="font-size: 14px;padding: 10px 0;"><?php echo $Booking[0]->transfer_conditions; ?></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                                <?php if($Booking[0]->supplier_telephone_no != '') { ?>
                                                <tr>
                                                    <td colspan="2">
                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                            <tbody>
                                                                <tr>
                                                                    <td><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;"><?php echo $this->TravelLights['TransferVoucher']['SupplierContact']; ?> </span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" align="left" style="padding:10px 0;"><span style="font-size: 14px;padding: 10px 0;"><?php echo $Booking[0]->supplier_telephone_no; ?></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                                
                                                <tr>
                                                    <td colspan="2">
                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                            <tbody>
                                                                <tr>
                                                                    <td><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;"><?php echo $this->TravelLights['TransferVoucher']['CancellationPolicy']; ?> </span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" align="left" style="padding:10px 0;">
                                                                    <?php if($Booking[0]->caneclation_policy != ''){
																					  $cancellaiton_details = json_decode($Booking[0]->caneclation_policy, true);
																					if(isset($cancellaiton_details['cancellation'])){ ?>
																					
																					<ul>
																				
																						<?php for($cancel =0; $cancel < count($cancellaiton_details['cancellation']); $cancel++) { 
																							
																					   if($cancellaiton_details['cancellation'][$cancel]['Charge'] == 'true') { ?> 
																						<li>
																							<?php echo $this->TravelLights['TransferVoucher']['Cancellationfrom']; ?> <strong><?php echo date('dS m Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate'])); ?></strong> <?php echo $this->TravelLights['TransferVoucher']['onwardswillincurcancellationcharge']; ?>  <b><?php echo "AUD ".$cancellaiton_details['cancellation'][$cancel]['ChargeAmount']; ?></b>
																						</li>
																						<?php } else { ?>
																							<li>
																							<?php echo $this->TravelLights['TransferVoucher']['Nocancellationfeeuntil']; ?>  <strong><?php echo date('dS m Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate'])); ?></strong>
																							</li>
																					   <?php } ?>
																					</ul>
																					
																					<?php } } 
																					 if(false){
																					 if(isset($cancellaiton_details['amendment'])){ ?>
																					<span class="conhead"><?php echo $this->TravelLights['TransferVoucher']['AmendmentPolicy']; ?></span>
																					<ul class="list_popup">
																				
																						<?php for($cancel =0; $cancel < count($cancellaiton_details['amendment']); $cancel++) { 
																							
																					   if($cancellaiton_details['amendment'][$cancel]['Charge'] == 'true') { ?> 
																						<li class="listcancel">
																							<?php echo $this->TravelLights['TransferVoucher']['Amendmentfrom']; ?> <?php echo date('dS m Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?> onwards , will incur cancellation charge  <b><?php echo "AUD ".$cancellaiton_details['amendment'][$cancel]['ChargeAmount']; ?></b>
																						</li>
																						<?php } else { ?>
																							<li class="listcancel">
																							<?php echo $this->TravelLights['TransferVoucher']['NoAmendmentfeeuntil']; ?> <?php echo date('dS m y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?>
																							</li>
																					   <?php } ?>
																					</ul>
																					
																					<?php } }  } } ?>
                                                                    
                                                                    
                                                                    </span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Content --> 
        <div class="clearfix"></div>
        <style>
            .leftflitmg { max-width:70px !important } 
        </style>
        <script type="text/javascript">
			//~ function printthis() {
			//~ var w = window.open('', '', 'width=800,height=600,resizeable,scrollbars');
				//~ w.document.write($("#voucher").html());
				//~ w.document.close(); // needed for chrome and safari
				//~ javascript:w.print();
				//~ w.close();
				//~ return false;
			//~ }
			//~ 
	//~ function printDiv('print_voucer') {
     //~ var printContents = document.getElementById(divName).innerHTML;
     //~ var originalContents = document.body.innerHTML;
//~ 
     //~ document.body.innerHTML = printContents;
//~ 
     //~ window.print();
//~ 
     //~ document.body.innerHTML = originalContents;
   //~ }

   function printthis() {
   var toPrint = document.getElementById('print_voucer');
        var popupWin = window.open('', '_blank', 'width=350,height=150,location=no,left=200px');
        popupWin.document.open();
        popupWin.document.write('<html><title>::Preview::</title><link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/css/voucher.css" /></head><body onload="window.print()">')
        popupWin.document.write(toPrint.innerHTML);
        popupWin.document.write('</html>');
        popupWin.document.close();
	}
		</script>
    </body>
</html>
