             <?php for($trans =0; $trans < count($transfer_result); $trans++) { ?> 
				
                <div class="rowresult"  data-price="<?php echo $transfer_result[$trans]->total_cost; ?>" data-transfer="<?php echo $transfer_result[$trans]->transfer_type_code; ?>">
                  <div class="madgrid" data-price="<?php echo $transfer_result[$trans]->total_cost; ?>" data-transfer="<?php echo $transfer_result[$trans]->transfer_type_code; ?>">
                   
                    <div class="col-xs-12 nopad">
                      <div class="sidenamedesc">
<!--
                        <div class="celhtl width32 midlbord">
                          <div class="hotel_image"> <img src="images/car.jpg" alt="" />  </div>
                        </div>
-->
                        <div class="celhtl width50">
                          <div class="waymensn">
                            <div class="flitruo_hotel">
                                <div class="clearfix"></div>
                                <span class="hotel_address elipsetool"> <span class="fa fa-map-marker"></span> <?php echo $transfer_result[$trans]->pickup_city_code_description; ?> </span>
                                <span class="hotel_address elipsetool"> <span class="fa fa-clock-o"></span> <?php echo $transfer_result[$trans]->approximate_time; ?> <?php echo $this->TravelLights['TransferResult']['Hours']; ?> </span>
                                <span class="morecontent">
                                	<?php echo $transfer_result[$trans]->item_description; ?> 
                                </span>
                               </div>
                            </div>
                          </div>
                        <div class="celhtl width18">
                          <div class="hotel_sideprice">
                            <div class="sideprice_hotel"> <?php echo $_SESSION['currency']." ".number_format($transfer_result[$trans]->total_cost* $_SESSION['currency_value'],2);  ?>  </div>
                            <div class="bookbtn_htl"> <a class="booknow" data-target="#roomshow_<?php echo $transfer_result[$trans]->transfer_temp_results; ?>" data-toggle="collapse" > <?php echo $this->TravelLights['TransferResult']['Select']; ?> </a> </div>
                          	<a data-target="#transfer_popup_<?php echo $transfer_result[$trans]->transfer_temp_results; ?>" data-toggle="modal" class="detailsflt"><?php echo $this->TravelLights['TransferResult']['ViewDetails']; ?></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                                           <div id="roomshow_<?php echo $transfer_result[$trans]->transfer_temp_results; ?>" class="shrom collapse" style="">
                        <?php $transfer_vehicles = $this->Transfer_Model->get_transfer_vehicles($transfer_result[$trans]->item_code, $transfer_result[$trans]->session_id)->result(); 
                         
                        for($vehicle =0; $vehicle < count($transfer_vehicles); $vehicle++) {
						
							 ?>
                    	
                    	<div class="innertabs">
                        	<div class="htl_rumrow">
                              <div class="hotel_list">
								  <form name="transfer_form" id="transfer_form_<?php echo $transfer_vehicles[$vehicle]->transfer_vehicle_id; ?>" method="post" action="<?php echo base_url().'transfer/add_to_cart/'.base64_encode(json_encode($transfer_vehicles[$vehicle]->transfer_vehicle_id))."/".base64_encode(json_encode($transfer_vehicles[$vehicle]->item_code)); ?>" >
                                <div class="col-sm-10 col-xs-12 nopad">
                                    <div class="in_center_htl">
                                    	<div class="col-xs-4 nopad singlecent">
                                        	<a class="hotel_hed" data-target="#roompop" data-toggle="modal"><?php echo  $transfer_vehicles[$vehicle]->vehicle_name; ?></a>
                                            <ul class="rmdetails">
                                            	<li><span class="fa fa-clock-o"></span><?php echo $transfer_vehicles[$vehicle]->approximate_time; ?> <?php echo $this->TravelLights['TransferResult']['Hours']; ?> </li>
                                              
                                            </ul>
                                      			
                                        </div>
                                        <div class="col-xs-4 nopad twocent">
                                        	<div class="rmdetails"> <?php echo $this->TravelLights['TransferResult']['Maximumluggage']; ?> : <span class="menlbl"><?php echo $transfer_vehicles[$vehicle]->maximum_luggage; ?></span> <br /> <?php echo $this->TravelLights['TransferResult']['MaximumPassengers']; ?> : <span class="menlbl"><?php echo $transfer_vehicles[$vehicle]->maximum_passengers; ?></span> <br /> <?php echo "Number of vehicles"; ?> : <span class="menlbl"><?php echo $transfer_vehicles[$vehicle]->no_of_vehicles; ?></span></div>
                                         
                                        </div>
                                        <div class="col-xs-4 nopad twocent">
                                        	<div class="sideprice"> <?php echo $_SESSION['currency']." ".number_format($transfer_vehicles[$vehicle]->total_cost* $_SESSION['currency_value'],2); ?>  </div>
                                            <div class="morerumdesc">
                                                <a data-target="#cancellation_popup_<?php echo $transfer_vehicles[$vehicle]->transfer_vehicle_id; ?>" data-toggle="modal" class="morerombtn"><?php echo $this->TravelLights['TransferResult']['CancellationPolicy']; ?></a>
                                             </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 bordrit">
                                  <div class="pricesec">
                                    
                                    <div class="bookbtn"> <a class="booknow" onclick="add_transfer_cart('transfer_form_<?php echo $transfer_vehicles[$vehicle]->transfer_vehicle_id; ?>')"><?php echo $this->TravelLights['TransferResult']['Book']; ?></a> </div>
                                  </div>
                                </div>
                                </form>
                              </div>
                            </div>
                         </div>
                         
                         <div id="cancellation_popup_<?php echo $transfer_vehicles[$vehicle]->transfer_vehicle_id; ?>" class="modal fade" role="dialog">
                           <div class="modal-dialog">
							 <div class="modal-content">
							   <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title"><?php echo $transfer_result[$trans]->pickup_city_code_description." - ".$transfer_result[$trans]->dropoff_city_code_description ; ?></h4>
								</div>
                                <div class="modal-body"> 
									  <?php if($transfer_vehicles[$vehicle]->caneclation_policy != ''){
          $cancellaiton_details = json_decode($transfer_vehicles[$vehicle]->caneclation_policy, true);
        if(isset($cancellaiton_details['cancellation'])){ ?>
		<span class="conhead"><?php echo $this->TravelLights['TransferResult']['CancellationPolicy']; ?></span>
        <ul class="list_popup">
	
			<?php for($cancel =0; $cancel < count($cancellaiton_details['cancellation']); $cancel++) { 
				
		   if($cancellaiton_details['cancellation'][$cancel]['Charge'] == 'true') { ?> 
		    <li class="listcancel">
            	<?php echo $this->TravelLights['TransferResult']['Cancellationfrom']; ?> <?php echo date('dS M Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate'])); ?> <?php echo $this->TravelLights['TransferResult']['incurcancellationcharge']; ?>  <b><?php echo $_SESSION['currency']." ".number_format($cancellaiton_details['cancellation'][$cancel]['ChargeAmount'] * $transfer_vehicles[$vehicle]->no_of_vehicles * $_SESSION['currency_value'],2); ?></b>
            </li>
            <?php } else { ?>
				<li class="listcancel">
				<?php echo $this->TravelLights['TransferResult']['Nocancellationfeeuntil']; ?> <?php echo date('dS M Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate'])); ?>
				</li>
		   <?php } ?>
        </ul>
        
        <?php } } 
         if(false){
         if(isset($cancellaiton_details['amendment'])){ ?>
		<span class="conhead"><?php echo $this->TravelLights['TransferResult']['AmendmentPolicy']; ?></span>
        <ul class="list_popup">
	
			<?php for($cancel =0; $cancel < count($cancellaiton_details['amendment']); $cancel++) { 
				
		   if($cancellaiton_details['amendment'][$cancel]['Charge'] == 'true') { ?> 
		    <li class="listcancel">
            	<?php echo $this->TravelLights['TransferResult']['Amendmentfrom']; ?> <?php echo date('d-m-Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?> <?php echo $this->TravelLights['TransferResult']['incurcancellationcharge']; ?>  <b><?php echo "AUD ".$cancellaiton_details['amendment'][$cancel]['ChargeAmount']; ?></b>
            </li>
            <?php } else { ?>
				<li class="listcancel">
				<?php echo $this->TravelLights['TransferResult']['NoAmendmentfeeuntil']; ?> <?php echo date('d-m-Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?>
				</li>
		   <?php } ?>
        </ul>
        
        <?php } }  } } ?>
      
					      </div>
                         </div> 
                         </div>
                         </div>
                        <?php } ?>
                    </div>

           
              
                  </div>
                  
       

 <div id="transfer_popup_<?php echo $transfer_result[$trans]->transfer_temp_results; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $transfer_result[$trans]->pickup_city_code_description." - ".$transfer_result[$trans]->dropoff_city_code_description ; ?></h4>
      </div>
      <div class="modal-body"> 
		  
		  <?php if($transfer_result[$trans]->item_description != '') { ?>
		  
      	<span class="conhead"><?php echo $this->TravelLights['TransferResult']['Description']; ?></span>
        <ul class="list_popup">
	        <li class="listcancel">
            	<?php echo $transfer_result[$trans]->item_description; ?>
            </li>
        </ul>
        <?php } ?>
        
	   <?php $meeting_point_details = explode("|||", $transfer_result[$trans]->meeting_point); 
		if(count($meeting_point_details) >0 ){ ?>
		<span class="conhead"><?php echo $this->TravelLights['TransferResult']['MeetingPoint']; ?> </span>
        <ul class="list_popup">
	       <?php for($meet =0; $meet < count($meeting_point_details); $meet++) { ?>
            <li class="listcancel">
            	<?php echo $meeting_point_details[$meet]; ?>
            </li>
            <?php } ?>
        </ul>
        <?php } ?>
        
         <?php $termsandcondtions = explode("|||", $transfer_result[$trans]->transfer_condition); 
		if(count($termsandcondtions) >0 ){ ?>
		<span class="conhead"><?php echo $this->TravelLights['TransferResult']['Terms&Conditons']; ?> </span>
        <ul class="list_popup">
	       <?php for($term =0; $term < count($termsandcondtions); $term++) { ?>
            <li class="listcancel">
            	<?php echo $termsandcondtions[$term]; ?>
            </li>
            <?php } ?>
        </ul>
        <?php } ?>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->TravelLights['TransferResult']['Close']; ?></button>
      </div>
    </div>

  </div>
</div>
 <div class="clearfix"></div>
     </div>
            
                
 <?php } ?>
 
 <script>
	 
 function add_transfer_cart(form_id){
	    	var action = $("#"+form_id).attr('action');
	   	
		$.ajax({
			type: "POST",
			url: action,
			data: $("#"+form_id).serialize(),
			dataType: "json",
			
			success: function(data){
				if(data.status == 1){
					window.location.href = data.redirect_url;
				}else{
					window.location.href = data.redirect_url;
				}
			}
		}); 
		return false; 
 }
 
 </script>               
 
