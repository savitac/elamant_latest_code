<?php ?>
<form id="card_details" method="POST" enctype="multipart/form-data" name="card_details" action="<?= base_url().'payment_gateway/payeesy_process'?>" >
	<input type="hidden" name="book_id" id="book_id" value="<?= json_decode(base64_decode($parent_pnr));?>">
	<input type="hidden" name="productinfo" id="productinfo" value="<?= $product;?>">
	<input type="hidden" name="txnid" id="txnid" payeezy-data="currency" value="<?= json_decode(base64_decode($parent_pnr));?>" />
	<input type="hidden" name="amount" id="amount" payeezy-data="currency" value="<?= $amount;?>" />
<section class="booking_form">

    <h5>Card Details</h5>
  

    <div class="section_bg">        
		<fieldset class="col-xs-12">
			<div class="left col-md-6 col-sm-12 col-xs-12">
				<label>Card Type<font style="color:red"> *</font>:</label>
				<select payeezy-data="card_type" name="card_type" class="mntxt form-control des_name" required> 
					<option value="mastercard">Master Card</option>
					<option value="visa">Visa</option>
					<option value="discover">Discover</option>  
					<option value="diners_club">Diners Club</option>
					<option value="jcb">JCB</option>				 
					<option value="American Express">American Express</option>
				</select>
			</div>
			<div class="right col-md-6 col-sm-12 col-xs-12">
				<img src="<?php echo base_url(); ?>cdn/provab_js/images/payicon.png" alt="Image">
			</div>
		</fieldset>  
		<fieldset class="col-xs-12">
			<div class="left col-md-6 col-sm-12 col-xs-12">
				<label>Cardholder Name <font style="color:red"> *</font>:</label>
				<input type="text" class="clainput capitalize _guest_validate_field" id="card_pname" name="cardholdername" value="" placeholder="Please enter name of cardholder" required onkeyup="chk_names(this);" >
			</div>
			<div class="right col-md-6 col-sm-12 col-xs-12">
				<small>(As it appears on your credit card)</small>
			</div>
		</fieldset>  
		<fieldset class="col-xs-12">
			<div class="left col-md-6 col-sm-12 col-xs-12">
				<label>Card Number <font style="color:red"> *</font></label>
				<input type="text" class="clainput capitalize guest_validate" name="card_number" value="" placeholder="Please enter Card Number" min="16" max="16" required>
			</div>
			<div class="right col-md-6 col-sm-12 col-xs-12">
				<small>(Pay with credit or debit card)</small>
			</div>
		</fieldset>  
		<fieldset class="col-xs-12">
			<div class="left col-md-6 col-sm-12 col-xs-12">
				<label>CVV Code <font style="color:red"> *</font>:</label>
				<input type="password" class="clainput capitalize guest_validate" id="" name="cvv_code" value="" placeholder="Please enter CVV Number" required>
			</div>
			<div class="right col-md-6 col-sm-12 col-xs-12">
				<img src="<?php echo base_url(); ?>cdn/provab_js/images/cvv.jpg" alt="Image"> <small>3 digit number from your card</small>
			</div>
		</fieldset>
		<fieldset class="col-xs-6 expry">
			<label>Expiry Date <font style="color:red"> *</font> :</label>
		<div class="row">
			<div class="col-xs-6">			
			<select payeezy-data="exp_month" class="mntxt form-control des_name" name="card_expmonth" required> <option value="01">01</option> <option value="02">02</option> <option value="03">03</option> <option value="04">04</option> <option value="05">05</option> <option value="06">06</option> <option value="07">07</option> <option value="08">08</option> <option value="09">09</option> <option value="10">10</option> <option value="11">11</option> <option value="12" selected="">12</option> </select>
			</div>
			<div class="col-xs-6">			
			<select payeezy-data="exp_year" class="mntxt form-control des_name" name="card_expyear" required> <option value="17">2017</option> <option value="18">2018</option> <option value="19">2019</option> <option value="20">2020</option> <option value="21">2021</option> <option value="22">2022</option> <option value="23">2023</option> <option value="24">2024</option> <option value="25">2025</option> <option value="26">2026</option> <option value="27">2027</option> <option value="28">2028</option> <option value="29">2029</option> <!--<option value="18">2018</option> <option value="19">2019</option> <option value="20">2020</option> <option value="21">2021</option>--> </select>
			</div>
			</div>
		</div>
		</fieldset>

</section>
<script>
	function chk_names(obj){
   		var valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ";
	    var ok = "yes";
	    var temp;
	    var validchars="";
	    var dot = 0;
	    for (var i=0; i<obj.value.length; i++) {
	        temp = obj.value.substring(i,i+1);
	        if (valid.indexOf(temp) == "-1") {
	            ok = "no";
	            break;
	        } else {
	            validchars=validchars+temp;
	        }
	    }

	    if (ok == "no") {
	        alert("Only Alpha characters are allowed!");
	        obj.value = validchars;
	        return false;
	    }
	    return true;
}

$('input._guest_validate_field').keyup(function() {
    var sAlphabetic = $(this).val();
    var alpha_filter    = /^[a-zA-Z ]+$/; 
    if(sAlphabetic != ''){
        if (alpha_filter.test(sAlphabetic)) {
            $(this).css('border', '1px solid #099A7D'); 
        }else {
            $(this).css('border', '1px solid #f52c2c');
        }
    }else{
        $(this).css('border', '1px solid #ddd');
    }
});

$('input.guest_validate').keyup(function() {
    var sAlphabetic = $(this).val();
    var alpha_number_filter     = /^[0-9 -]+$/; 
    if(sAlphabetic != ''){
        if (alpha_number_filter.test(sAlphabetic)) {
            $(this).css('border', '1px solid #099A7D'); 
        }else {
            $(this).css('border', '1px solid #f52c2c');
        }
    }else{
        $(this).css('border', '1px solid #ddd');
    }
});
</script>
