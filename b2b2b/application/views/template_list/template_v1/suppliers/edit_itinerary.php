<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo  $this->session->userdata('company_name');?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/dashboard.css"[A-Za-z]{3} rel="stylesheet">
	<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?php echo ASSETS; ?>assets/js/daterangepicker/daterangepicker-bs3.css">
</head>
<body>

<?php echo $this->load->view('dashboard/top'); ?>	

<section id="main-content">
<section class="wrapper">
<div id="package_types" class="bodyContent col-md-12">
	<div class="panel panel-default">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
					<li role="presentation" class="active"><a href="#fromList"
						aria-controls="home" role="tab" data-toggle="tab">
							<h1>Update Package Itinerary</h1>
					</a></li>
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
				</ul>
			</div>
		</div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="fromList">
					<div class="col-md-12">
						<div class='row'>
							<div class='row'>
								<div class='col-sm-12'>
									<div class='' style='margin-bottom: 0;'>
										<form class='form form-horizontal validate-form'
											style='margin-bottom: 0;'
											action="<?php echo base_url(); ?>supplier/update_itinerary/"
											method="post" enctype="multipart/form-data">
				<?php $i=0; foreach ($pack_data as $packdata){?>
                <input type="hidden" name="itinerary_id[]"
												value="<?php echo $packdata->iti_id;?>"> <input
												type="hidden" name="package_id"
												value="<?php echo $packdata->package_id;?>">
											<div class="duration_info" id="duration_info">
												<div class='form-group'>
													<label class='control-label col-sm-3' for='validation_desc'>Itinerary
														Description </label>
													<div class='col-sm-4 controls'>
														<textarea name="desc[]" class="form-control"
															data-rule-required="true" value="" cols="70" rows="3"
															placeholder="Description"><?php echo $packdata->itinerary_description;?></textarea>
													</div>
												</div>
												<div class='form-group'>
													<label class='control-label col-sm-3'
														for='validation_company'>Itinerary Image</label>
													<div class='col-sm-3 controls'>
														<input type="file" title='Image to add' class=''
															id='image' name='imagelable<?php echo $i; ?>'> <span
															id="pacmimg" style="color: #F00; display: none">Please
															Upload Itinerary Image</span> <img
															src="<?php echo ASSETS.DOMAIN_PCKG_UPLOAD_DIR.$packdata->itinerary_image; ?>"
															width="100"> <input type="hidden" name="hiddenimage[]"
															value="<?php echo $packdata->itinerary_image; ?>">
													</div>
												</div>
												<div class='form-group'>
													<label class='control-label col-sm-3' for='validation_name'>Days
													</label>
													<div class='col-sm-4 controls'>
														<input type="text" name="days[]" id="days" readonly
															value="<?php echo $packdata->day;?>"
															data-rule-required='true' class='form-control'>
													</div>
												</div>
												<div class='form-group'>
													<label class='control-label col-sm-3' for='validation_name'>Place
													</label>
													<div class='col-sm-4 controls'>
														<input type="text" name="place[]" id="Place"
															value="<?php echo $packdata->place;?>"
															data-rule-required='true' class='form-control'>
													</div>
												</div>
											</div>
											<hr>
                      <?php $i++; }?>
                       <div class='form-actions'
												style='margin-bottom: 0'>
												<div class='row'>
													<div class='col-sm-9 col-sm-offset-3'>
														<a
															href="<?php echo base_url(); ?>supplier/view_with_price">
															<button class='btn btn-primary' type='button'>
																<i class='icon-reply'></i> Go Back
															</button>
														</a>&nbsp;&nbsp;
														<button class='btn btn-primary' type='submit'>
															<i class='icon-save'></i> Update
														</button>
													</div>
												</div>
											</div>
									
									</div>
								</div>
							</div>
						</div>
					</div>
					</form>
					</section>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- PANEL BODY END -->
</div>
<!-- PANEL WRAP END -->
</div>
</section>
</section>
	
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<!-- <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script> -->
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
	<script src="<?php echo ASSETS; ?>assets/js/daterangepicker/moment.min.js"></script>
	<script src="<?php echo ASSETS; ?>assets/js/daterangepicker/daterangepicker.js"></script>
<script>
$.validator.addMethod("buga", (function(value) {
  return value === "buga";
}), "Please enter \"buga\"!");

$.validator.methods.equal = function(value, element, param) {
  return value === param;
};


$(function () {
  $('#datetimepicker2').datetimepicker({
      startDate: new Date()
  });

  $('#datetimepicker1').datetimepicker({
      startDate: new Date()
  });
});


</script>
