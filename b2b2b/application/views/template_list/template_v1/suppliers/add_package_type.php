<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo  $this->session->userdata('company_name');?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/dashboard.css"[A-Za-z]{3} rel="stylesheet">
	<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?php echo ASSETS; ?>assets/js/daterangepicker/daterangepicker-bs3.css">
</head>
<body>
	
	<?php echo $this->load->view('dashboard/top'); ?>	

<section id="main-content">
<section class="wrapper">
<div id="package_types" class="bodyContent col-md-12">
	<div class="panel panel-default">
		<!-- PANEL WRAP START -->
		<div class="panel-heading">
			<!-- PANEL HEAD START -->
			<div class="panel-title">
				<ul class="nav nav-tabs" role="tablist" id="myTab">
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
					<li role="presentation" class="active"><a href="#fromList"
						aria-controls="home" role="tab" data-toggle="tab"><h1>Add / Edit Package Type</h1></a></li>
					<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
				</ul>
			</div>
		</div>
		<!-- PANEL HEAD START -->
		<div class="panel-body">
			<!-- PANEL BODY START -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="fromList">
					<div class="col-md-12">
					<div class='row'>
					<div class='col-sm-12'>
						<div class=''>
							<div class='box-header '>
								<div class='title' id="tab1">  <?php if(isset($status)){echo $status;}?></div>
								<div class='actions'></div>
							</div>
							<div class=''>
								<form class='form form-horizontal validate-form'
									style='margin-bottom: 0;'
									action="<?php echo base_url(); ?>index.php/package/save_packages_type"
									method="post" name="frm1" enctype="multipart/form-data">
									<div class='form-group'>
										<label class='control-label col-sm-2' for='validation_name'>Package
											Type</label>
										<div class='col-sm-3 controls'>
										<input type="hidden" name="package_types_id" value="<?=@$pack_data[0]->package_types_id;?>">
											<input class='form-control' data-rule-minlength='2'
												data-rule-required='true' id='pname' name='name' value="<?=@$pack_data[0]->package_types_name;?>"
												placeholder='Tour Type' type='text'> <span id="pacname"
												style="color: #F00; display: none;">Please Select Package
												Type</span>
										</div>
									</div>
									<div class='form-actions' style='margin-bottom: 0'>
										<div class='row'>
											<div class='col-sm-9 col-sm-offset-4'>
												<a
													href="<?php echo base_url(); ?>index.php/package/view_packages_types">
													<button class='btn btn-primary' type='button'>
														<i class='icon-reply'></i> Back
													</button>
												</a>&nbsp;
												<button class='btn btn-primary' type='submit'>
													<i class='icon-save'></i> Submit
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
					</div>
				</div>
			</div>
		</div>
		<!-- PANEL BODY END -->
	</div>
	<!-- PANEL WRAP END -->
</div>
</section>
</section>
	
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
	<script src="<?php echo ASSETS; ?>assets/js/daterangepicker/moment.min.js"></script>
    <script src="<?php echo ASSETS; ?>assets/js/daterangepicker/daterangepicker.js"></script>
<script>
$.validator.addMethod("buga", (function(value) {
  return value === "buga";
}), "Please enter \"buga\"!");

$.validator.methods.equal = function(value, element, param) {
  return value === param;
};


$(function () {
  $('#datetimepicker2').datetimepicker({
      startDate: new Date()
  });

  $('#datetimepicker1').datetimepicker({
      startDate: new Date()
  });
});


    </script>