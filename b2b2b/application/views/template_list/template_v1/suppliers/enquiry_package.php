<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo  $this->session->userdata('company_name');?></title>
  <?php echo $this->load->view('core/load_css'); ?>
  <link href="<?php echo ASSETS;?>assets/css/dashboard.css"[A-Za-z]{3} rel="stylesheet">
  <link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<?php echo ASSETS; ?>assets/js/daterangepicker/daterangepicker-bs3.css">
</head>
<body>
  
  <?php echo $this->load->view('dashboard/top'); ?> 

<section id="main-content">
<section class="wrapper">
<div id="package_types" class="bodyContent col-md-12">
  <div class="panel panel-default">
    <!-- PANEL WRAP START -->
    <div class="panel-heading">
      <!-- PANEL HEAD START -->
      <div class="panel-title">
        <ul class="nav nav-tabs nav-justified" role="tablist" id="myTab">
          <!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
          <li role="presentation" class="active">
            <a href="#fromList"
              aria-controls="home" role="tab" data-toggle="tab">
              <h1>View Enquiries
              </h1>
            </a>
          </li>
          <!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
        </ul>
      </div>
    </div>
    <!-- PANEL HEAD START -->
    <div class="panel-body">
      <!-- PANEL BODY START -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="fromList">
          <div class="col-md-12">
			<div class='row'>
				<div class='col-sm-12'>
				
				 <div class='actions'>
                      <a href="<?php echo base_url(); ?>supplier/view_with_price">
                      <button class='btn btn-primary' style='margin-bottom: 5px'>
                       Go Back
                      </button>
                      </a> 
                    </div>
                    
					<div class=''
						style='margin-bottom: 0;'>
						<div class='box-content box-no-padding'>
							<div class='responsive-table'>
								<div class='scrollable-area'>
									<table
										class='data-table-column-filter table table-bordered table-striped'
										style='margin-bottom: 0;'>
										<thead>
											<tr>
												<th>S.No</th>
												<th>Name</th>
												<th>Email</th>
												<th>Contact</th>
												<th>Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
                          <?php  if(!empty($enquiries)) { $count = 1; 
                        foreach($enquiries as $key => $package) { ?>
                      <tr>
												<td><?php echo $count; ?></td>
												<td><?php echo $package->first_name; ?></td>
												<td><?php echo $package->email; ?></td>
												<td><?php echo $package->phone; ?></td>
												<!--  <td><?php echo $package->address; ?></td>  -->
												<td><?php echo $package->date; ?></td>
												<td class="center">
													<!--  <a class="btn btn-primary btn-xs has-tooltip" data-placement="top" title=""  href="<?php echo base_url(); ?>supplier/send_enq_mail/<?php echo $package->id; ?>"  data-original-title="Send mail">
                                      <i class="icon-envelope"></i>
                                    </a> --> <a
													href="<?php echo base_url(); ?>supplier/delete_enquiry/<?php echo $package->id; ?>/<?php echo $package->package_id; ?>"
													data-original-title="Delete"
													onclick="return confirm('Do you want delete this record');"
													class="btn btn-danger btn-xs has-tooltip"
													data-original-title="Delete">Delete 
												</a>

												</td>

											</tr>   
                  <?php $count++; } } else{
                  	echo '<tr><td colspan="6">No Data Found</td></tr>';
                  } ?>  
                      </tbody>
									</table>
								</div>
							</div>
		</div>
	</div>

</div>
  </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- PANEL BODY END -->
</div>
<!-- PANEL WRAP END -->
</div>
</section>
</section>
  
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <!-- <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script> -->
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
  <script src="<?php echo ASSETS; ?>assets/js/daterangepicker/moment.min.js"></script>
  <script src="<?php echo ASSETS; ?>assets/js/daterangepicker/daterangepicker.js"></script>
<script>
$.validator.addMethod("buga", (function(value) {
  return value === "buga";
}), "Please enter \"buga\"!");

$.validator.methods.equal = function(value, element, param) {
  return value === param;
};


$(function () {
  $('#datetimepicker2').datetimepicker({
      startDate: new Date()
  });

  $('#datetimepicker1').datetimepicker({
      startDate: new Date()
  });
});


    </script>
<script type="text/javascript">
        function activate(that) { window.location.href = that; }
    </script>