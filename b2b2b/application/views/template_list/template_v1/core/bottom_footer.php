<?php 
$GLOBALS['CI']->load->model('sitemanagement_model');
$domain_id=$this->session->userdata('branch_id');
$domain_pages = $GLOBALS['CI']->sitemanagement_model->get_Page_data($domain_id); 
$domain_list = $GLOBALS['CI']->sitemanagement_model->get_domain_data($domain_id); 
foreach ($domain_list as  $value) {
  $email=$value->email;
  $phone=$value->phone;
} 
?>
<div class="socialdsign">
</div>
<footer>
 <div class="fstfooter">
    <div class="container">
      <div class="reftr">
        <div class="col-xs-12 col-sm-4 nopad fulnine">
          <div class="frtbest centertio">
            <div class="footrlogo">
                <a class="logo" alt ="<?php echo $this->session->userdata('company_name'); ?>" 
                href="<?php echo base_url();?>"> <?php if($GLOBALS['CI']->session->userdata('site_name') == ''){ ?>  
        <a href="<?php echo base_url()."dashboard"; ?>"><img alt="" src="<?php echo base_url().'assets/images/logo.png'; ?>"> </a> 
        <div class="clearfix"></div>
         <?php } else { ?>  
    <a href="<?php echo base_url()."dashboard"; ?>" class="logo"><img alt="" src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $GLOBALS['CI']->session->userdata('domain_logo'); ?>"></a>   
    <?php } ?></a>      
          </div>
          <div style="color: #262b2f;font-size: small;">
            Phone: <?php echo $phone; ?>  
            <br> 
            Email: <?php echo $email; ?>   
          </div>
        </div>
      </div>
        <div class="col-xs-8 nopad fulnine">
          <div class="col-xs-4 nopad">
            <div class="frtbest">
              <h4 class="ftrhd arimo">About</h4>
              <ul>
              <?php foreach ($domain_pages as $key => $pages) { ?>
              <li class="frteli"><a href="<?php echo base_url();?>home/page_data/<?php echo $pages->id; ?>/<?php echo $pages->page_name; ?> "><?php echo $pages->page_name; ?> <br>
                  </a></li>
                  <?php
               
              } ?>
              </ul>
            </div>
          </div>
          <div class="col-xs-4 col-sm-4 nopad">
            <div class="frtbest">
              <h4 class="ftrhd arimo">Quick Links</h4>
              <ul> 
                <li class="frteli"><a id="flight_btn" href="<?php echo base_url()."home/general?view=flight";?>">Flight</a></li>
                <li class="frteli"><a id="flight_btn" href="<?php echo base_url()."home/general?view=hotel";?>">Hotel</a></li>                
              </ul>
            </div>
          </div>
          <div class="col-xs-4 nopad">
            <div class="frtbest">
              <h4 class="ftrhd arimo">Services</h4>
              <ul>
               
                <li class="frteli"><a href="<?php echo base_url().'home/login'; ?>" target="_blank" class="sub_login">Login</a></li>
                <li class="frteli"><a href="<?php echo base_url().'account/regB2BUser'; ?> " target="_blank" class="">Register</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>

<!--   <div class="btmfooter">
  <div class="container">
  <div class="org_row">
  <div class="col-md-6 col-sm-6 col-xs-12 folst">
                  <ul class="pad0 mar0">
                      <li class="list-unstyled">
                          <img src="http://bookurflight.com/b2b2b/assets/images/dinner.png" alt="" class="img-fluid">
                      </li>
                      <li class="list-unstyled">
                          <img src="http://bookurflight.com/b2b2b/assets/images/jcb.png" alt="" class="img-fluid">
                      </li>
                      <li class="list-unstyled">
                          <img src="http://bookurflight.com/b2b2b/assets/images/cc-icons.png" alt="" class="img-fluid">
                      </li>
                      <li class="list-unstyled">
                          <img src="http://bookurflight.com/b2b2b/assets/images/banktransfer.png" alt="" class="img-fluid">
                      </li>
                      <li class="list-unstyled">
                          <img src="http://bookurflight.com/b2b2b/assets/images/cash-on-delivery.png" alt="" class="img-fluid">
                      </li>
                  </ul>
              </div>      
    <div class="col-md-6 col-sm-6 col-xs-12 copyrit text-center">&copy;<?=date('Y')." ".$this->session->userdata('company_name')?> All rights reserved.</div>
  </div>
</div>
</div> -->
<div class="btmfooter">
   <div class="container">
      <div class="row">
         <div class="col-xs-12 nopad">
            <div class="col-md-5 col-xs-5 money nopad">
               <div class="moneyin">
                  <h5>Your Money is Safe with US</h5>
                  <p>You should take benefit from financial protection, when you booking with us</p>
               </div>
            </div>
            <div class="col-md-4 col-xs-4 mny1 nopad">
               <div class="acceptimg">                            
                  <img class="img-responsive" src="<?php echo ASSETS.'assets/images/payment.png'?>" alt="">                        
               </div>
            </div>
            <div class="col-md-3 col-xs-3 mny2 nopad"><p>&copy;<?=date('Y')." ".$this->session->userdata('company_name')?></p><p>All rights reserved.</p>
            </div>
         </div>
      </div>
   </div>
</div>
  <div class="copyrights">
        <div class="copyin">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 fomid">
                    <ul class="pad0 mar0 clearfix text-left">
                        <li class="list-unstyled">
                            <img src="http://bookurflight.com/b2b2b/assets/images/iata.png" alt="" class="img-fluid">
                        </li>
                        <li class="list-unstyled">
                            <img src="http://bookurflight.com/b2b2b/assets/images/arc.png" alt="" class="img-fluid">
                        </li>
                        <li class="list-unstyled">
                            <img src="http://bookurflight.com/b2b2b/assets/images/asta.png" alt="" class="img-fluid">
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 fomid">
                    <ul class="pad0 mar0 clearfix text-center logpart">
                        <li class="list-unstyled">
                            <img src="http://bookurflight.com/b2b2b/assets/images/logo.png" alt="" class="img-fluid">
                        </li>
                        <li class="list-unstyled">
                            <p>&copy;<?=date('Y')." ".$this->session->userdata('company_name')?></p><br>
                            <p>All rights reserved.</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 folst">
                    <ul class="pad0 mar0 clearfix text-right">
                        <li class="list-unstyled">
                            <img src="http://bookurflight.com/b2b2b/assets/images/dinner.png" alt="" class="img-fluid">
                        </li>
                        <li class="list-unstyled">
                            <img src="http://bookurflight.com/b2b2b/assets/images/jcb.png" alt="" class="img-fluid">
                        </li>
                        <li class="list-unstyled">
                            <img src="http://bookurflight.com/b2b2b/assets/images/cc-icons.png" alt="" class="img-fluid">
                        </li>
                        <li class="list-unstyled">
                            <img src="http://bookurflight.com/b2b2b/assets/images/banktransfer.png" alt="" class="img-fluid">
                        </li>
                        <li class="list-unstyled">
                            <img src="http://bookurflight.com/b2b2b/assets/images/cash-on-delivery.png" alt="" class="img-fluid">
                        </li>
                    </ul>
                </div>      
            </div>
        </div>
    </div>
</footer>
