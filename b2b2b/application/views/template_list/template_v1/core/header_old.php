<?php 
$domain_id=$this->session->userdata('branch_id');
$GLOBALS['CI']->load->model('sitemanagement_model');
$domain_list = $GLOBALS['CI']->sitemanagement_model->get_domain_data($domain_id); 
foreach ($domain_list as  $value) {
  $email=$value->email;
  $phone=$value->phone;
  $facebook=$value->facebook;
  $twitter=$value->twitter;
  $google=$value->google;
  $youtube=$value->youtube;
}

?>
  <link href="<?php echo ASSETS;?>assets/css/custom_b2c.css" rel="stylesheet" />
<header>
 <div class="section_top">
    <div class="container">
      <div class="topalstn">
        <div class="socila hidesocial">
          
          <div class="sidebtn flagss"><a href="<?php echo base_url().'home/login'; ?>">
            <div class="reglognorml">
              <div class="">Login</div>
            </div>
            </a>
          </div>
          <div class="sidebtn flagss"><a href="<?php echo base_url().'account/regB2BUser'; ?>">
            <div class="reglognorml">
              <div class="">Register User</div>
            </div>
            </a>
          </div>
           <div class="sidebtn flagss"><a href="<?php echo base_url().'account/createAccount'; ?>">
            <div class="reglognorml">
              <div class="">Register Agent</div>
            </div>
            </a>
          </div>
        </div>
        
        <div class="toprit">
          <div class="sectns"><a href="mailto:<?php echo @$email; ?>" class="mailadrs"><span class="fa fa-paper-plane"></span><?php echo @$email; ?></a></div>
          <div class="sectns"><a href="tel:<?php echo @$phone; ?>" class="phnumr"><i aria-hidden="true" class="fa fa-phone"></i> <span class="numhide"><?php echo @$phone; ?></span> 
            <div class="fa cliktocl fa-phone"></div>
            </a></div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  
<div class="topssec">
  <div class="container">
  
  <div class="bars_menu fa fa-bars menu_brgr"></div>
     <?php
       if($this->session->userdata('site_name') == ''){ ?>           
       <a class="logo" href="<?php echo base_url();?>"><img src="<?php echo ASSETS; ?>assets/images/logo.png" class="ful_logo"></a>
      <?php } else {  ?>
      <a class="logo" href="<?php echo base_url();?>"><img class="ful_logo" src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $this->session->userdata('domain_logo'); ?>"></a> 
     <?php } ?>
       
       <div class="menuandall">
        <div class="sepmenus">
          <ul class="exploreall">
            <li class=""><a href="<?php echo base_url();?>"><strong>Home</strong></a></li>
             <li class=""><a class="products" data-toggle="tab"  href="#Flight"><strong>Flight</strong></a></li>  
            <li class=""><a class="products" data-toggle="tab" href="#Hotels"><strong>Hotel</strong></a></li>     
          </ul>
        </div>
               <div class="ritsude">
            <div class="sidebtn flagss">
                                    <a class="topa dropdown-toggle" data-toggle="dropdown">
                                        <div class="reglognorml">
                                            <div class="flag_images">
                                                <?php
                                                $curr = get_application_currency_preference();

                                                echo '<span class="curncy_img sprte ' . strtolower($curr) . '"></span>'
                                                ?>
                                            </div>
                                            <div class="flags">
                                                <?php
                                                echo $curr;
                                                ?>
                                            </div>
                                            <b class="caret cartdown"></b>
                                        </div>
                                    </a>
                                    <ul class="dropdown-menu exploreul explorecntry logdowndiv">
                                        <?= $this->load->view('utilities/multi_currency') ?>
                                    </ul>
                                </div>
          </div>

       </div>
  </header>
  
</div>
</div>

<div class="clearfix"></div>


  <script>
//Language Convertor
var app_base_url = '<?php echo base_url(); ?>';
  function ChangeLanguage(lang){ 
    var data = {};
    data['language'] = lang;
    $.ajax({
      type: 'POST',
      url: '<?php echo ASSETS; ?>home/change_language',
      async: true,
      dataType: 'json',
      data: data,
      success: function(data) {
        window.location = "<?php echo ASSETS; ?>";
      }
    });
  }
</script>
<script type="text/javascript">
$(function(){
  $('.products').click(function(){
    var clicked=$(this).attr('href');
    if(clicked == "#Flight")
    {
      $(".flight_tab").addClass('active');
      $(".hotel_tab").removeClass('active');


    } 
    else if(clicked=="#Hotels"){ 
      $(".flight_tab").removeClass('active'); 
     $(".hotel_tab").addClass('active');  

    }
  });
}); 
</script>