<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title><?php echo $this->session->userdata('company_name')?></title>
        <?php echo $this->load->view("core/load_css"); ?>
         <link href="<?php echo ASSETS; ?>assets/css/flight_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script>
		<script src="<?php echo ASSETS; ?>assets/js/jquery-ui.min.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/general.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/css/owl.carousel.min.css"></script>
		<script src="<?php echo ASSETS; ?>assets/js/owl.carousel.min.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/flight_search.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/provablib.js"></script>
		<script src="<?php echo ASSETS; ?>assets/js/jquery.jsort.0.4.min.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/pax_count.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/datepicker.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/map.js"></script>
		<style type="text/css">
		.labladvncesearch 
		{ 
		color: #000 !important;
		line-height: 30px;    
		overflow: hidden;
		}
		</style>
    </head> 
    <body>
      <?php 
        if($this->session->userdata('user_logged_in') == 0) {  
			echo $this->load->view('core/header'); 
		} else {
			echo $this->load->view('dashboard/top'); 	 
		}
	 ?>
<br>
<section class="contentsec">
	<div class="" id="myModal_1" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Sorry for the Inconvinience !</h4>
				</div>
				<div class="modal-body">
					<font color="red"><h4 class="modal-title">This Service is
							Temporarily Unavailable !</h4></font> <br>Please Try Again Later.
				</div>
				<div class="modal-footer">
					<a class="hand-cursor" href="<?=base_url();?>">Search Again</a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php echo  $this->load->view('core/bottom_footer'); ?>
</body>