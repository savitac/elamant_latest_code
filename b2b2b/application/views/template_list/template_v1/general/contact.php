<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>Chinagap</title>
<?php echo $this->load->view('core/load_css'); ?>
<link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" />
</head>

<body>

<!-- Navigation -->
<?php
      if(isset($_SESSION['TheChinaGap']['language'])){ 
        $display_language = $_SESSION['TheChinaGap']['language'];
      } else {
        $display_language = $_SESSION['TheChinaGap']['language'] = BASE_LANGUAGE;
      } 
?>
<?php echo $this->load->view('core/header'); ?> 

<!-- /Navigation -->
<div class="clearfix"></div>

<div class="top80 static_pages">
  <div class="container">
    <div class="top_static"><?php echo $this->thechinagap['Contact']['ContactUs']; ?></div>
  </div>
  
  <div class="container">
  	<div class="col-xs-12 nopad full_tab">
      <div class="inercols_contact">
        <div class="col-xs-12 col-sm-6 nopad">
          <form name="contact" id="contact"  method="post" action="<?php echo ASSETS."home/createContact"; ?>">
            <div class="inside_form">
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="name" id="name" placeholder="<?php echo $this->thechinagap['Contact']['Name']; ?>" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="position" id="position" placeholder="<?php echo $this->thechinagap['Contact']['Position']; ?>" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="company" id="company" placeholder="<?php echo $this->thechinagap['Contact']['Company']; ?>" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="wechat" id="wechat" placeholder="<?php echo $this->thechinagap['Contact']['WeChat']; ?>" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="qq" id="qq" placeholder="<?php echo $this->thechinagap['Contact']['QQ']; ?>" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="email" class="conform" name="email" id="email" placeholder="<?php echo $this->thechinagap['Contact']['EmailID']; ?> " required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="subject" id="subject" placeholder="<?php echo $this->thechinagap['Contact']['Subject']; ?>" required>
              </div>
              <div class="col-xs-12 padmar">
                <textarea class="areatxt" name="message" id="message" placeholder="<?php echo $this->thechinagap['Contact']['Message']; ?>" required></textarea>
              </div>
              
              <div class="col-xs-12 padmar">
                <input type="submit" class="btn btn-myclr" value="<?php echo $this->thechinagap['Contact']['SubmitYourMessage']; ?>">
              </div>
            </div>
          </form>
        </div>
        <div class="col-xs-12 col-sm-6 nopad">
          <div class="set_contact">
              <?php for($contacts =0; $contacts < count($contact); $contacts++) { ?>
              <div class="row_contacts"> 
              <div class="line_desctn"><strong> <?php echo $contact[$contacts]->city; ?></strong></div>
            </div>
            <div class="row_contacts"> <span class="icon_contact fa fa-map-marker"></span>
              <div class="line_desctn">
                 <?php if($display_language=='english') $address = $contact[$contacts]->address; 
                  else $address = $contact[$contacts]->address_china; ?>
              <?php echo $address; ?>
</div>
            </div>
            
            <div class="row_contacts"> <span class="icon_contact fa fa-phone"></span>
              <div class="line_desctn"><?php echo $contact[$contacts]->contact; ?></div>
            </div>
            <?php } ?>
          
            <div class="row_contacts"> 
              <div class="line_desctn"><strong> <?php echo $this->thechinagap['Contact']['GeneralEnquiries']; ?></strong></div>
            </div>
            <div class="row_contacts"> <span class="icon_contact fa fa-envelope"></span>
              <div class="line_desctn">  info@thechinagap.com</div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>

</div>


<div class="clearfix"></div>
<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	 
	 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	 
	
});
</script> 





</body>
</html>
