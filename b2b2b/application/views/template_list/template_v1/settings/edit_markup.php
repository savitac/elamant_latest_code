    <form id="edit_markup" name="edit_markup" class="validate form-horizontal" method="post" action="<?php echo base_url().'settings/updateMarkup/'.base64_encode(json_encode($agent_markups[0]->markup_details_id)); ?>" novalidate="novalidate">   
                        <span id="markup-already-edit-available" class="error" for="markup-already-available"> </span>         
                        <div class="likrticktsec">
                            <div class="cobldo">Markup Type</div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
									  <select name="markup_type" id="markup_type1"class="payinselect mySelectBoxClassfortab hasCustomSelect"  required >
                                        <option value="GENERAL" <?php if($agent_markups[0]->markup_type == "GENERAL") { echo "selected"; } ?>>GENERAL</option>
                                        <option value="SPECIFIC" <?php if($agent_markups[0]->markup_type == "SPECIFIC") { echo "selected"; } ?>> SPECIFIC</option>
                                   </select>
                                    <span id="markup-type-error" class="error" for="user_name"> </span>
                                  </div>
                            </div>
                        </div>
                        
                        
                        <!-- <div class="likrticktsec">
                            <div class="cobldo">API</div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
									
                                <select name="api_id" value="api_id" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
									   <?php for($ap=0; $ap < count($api); $ap++) { ?> 
                                        <option value ="<?php echo $api[$ap]->api_details_id; ?>" <?php if($api[$ap]->api_details_id == $agent_markups[0]->product_details_id) { echo "selected"; } ?> > <?php echo $api[$ap]->api_name." (".$api[$ap]->product_name.")"; ?></option>
                                        <?php } ?>
                                </select>
                                 <span id="product-id-error" class="error" for="product_id"> </span>
                                 </div>
                            </div>
                        </div> -->
                        
                        
                         <div class="likrticktsec">
                            <div class="cobldo">Product</div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
								<select name="product_id" value="product_id" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
									
									   <?php for($prod=0; $prod < count($products); $prod++) { ?> 
                                        <option value ="<?php echo $products[$prod]->product_details_id; ?>" <?php if($products[$prod]->product_details_id == $agent_markups[0]->product_details_id){ echo "selected"; } ?> > <?php echo $products[$prod]->product_name; ?></option>
                                        <?php } ?>
                                </select>
                                 <span id="product-id-error" class="error" for="product_id"> </span>
                                 </div>
                            </div>
                        </div>
                        
                         
                        
                        <div class="likrticktsec" id="country1" <?php if($agent_markups[0]->markup_type == "GENERAL"){ ?> style="display:none;" <?php } ?>>
                            <div class="cobldo">Country</div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="country_code" id="country_code"   class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
									  <option value=""> Select Country </option>
									   <?php for($country=0; $country < count($nationality_countries); $country++)  { ?> 
                                        <option value ="<?php echo $nationality_countries[$country]->country_id; ?>" <?php if($agent_markups[0]->country_id == $nationality_countries[$country]->country_id){ echo "selected"; } ?> > <?php echo $nationality_countries[$country]->country_name; ?></option>
                                        <?php } ?>
                                </select>
                                <span id="country-code-error" class="error" for="country_code"> </span>
                                 </div>
                            </div>
                        </div>
                        
                       
                        
                          <!-- <div class="likrticktsec" id="expry_date1" style="display:none;">
                            <div class="cobldo">Markup Expiry Date</div>
                            <div class="coltcnt">
                            	<input type="text" class="payinput cols-sm-2" name="expiry_date" id="datepicker33"  value="<?php if($agent_markups[0]->markup_expiry_date != '0000-00-00') { echo date('d-m-Y', strtotime($agent_markups[0]->markup_expiry_date)); } ?>" placeholder="Markup End Date"   />
                            </div>
                        </div> -->
                        
                        <div class="likrticktsec">
                            <div class="cobldo">Markup value Type</div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="markup_value_type" id="markup_value_type" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
                                        <option value="Percentage" <?php if($agent_markups[0]->markup_value_type == "Percentage"){ echo "selected"; } ?>>PERCENTAGE</option>
                                        <option value="Fixed" <?php if($agent_markups[0]->markup_value_type == "Fixed"){ echo "selected"; } ?>>FIXED</option>
                                </select>
                                 <span id="markup-value-type-error" class="error" for="markup_value_type"> </span>
                                 </div>
                            </div>
                        </div>
                     
                      <div class="likrticktsec">
                            <div class="cobldo">Markup Value</div>
                            <div class="coltcnt">
                                <input type="number" class="payinput" name="markup_value" id="digits" min="0" value="<?php echo $agent_markups[0]->markup_value; ?>" id="markup_value" required />
                                <span id="markup-value-error" class="error" for="markup_value"> </span>
                            </div>
                        </div>
                        
                        <div class="likrticktsec">
                            <div class="cobldo">&nbsp;</div>
                            <div class="coltcnt">
                            <input type="submit" value="Update" onclick="form_submits('edit_markup')" class="adddtickt">
                            </div>
                        </div>
                       </form>
                       
     <script>  
   	$(document).ready(function(){
		
		<?php if($agent_markups[0]->markup_type == "SPECIFIC") { ?>
			    $("#country1").css("display", "inherit");
			    $("#date_range1").css("display", "inherit");
			    $("#expry_date1").css("display", "inherit");
		<?php } ?>
		
		
		$("#datepicker111").datepicker({
	numberOfMonths: 1,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: 1,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate()); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#datepicker222").datepicker("setDate", d1);
		$("#datepicker222").datepicker("option", "minDate", d1);
	},
	onClose: function( selectedDate ) {
		$( "#datepicker222" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker222' ).focus();
	}
});

	$("#datepicker222").datepicker({
		numberOfMonths: 1,
		minDate: '-12Y',
		dateFormat: 'dd-mm-yy'
		
	});
	
   $("#datepicker333").datepicker({
	numberOfMonths: 1,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: 1,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate()); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#datepicker444").datepicker("setDate", d1);
		$("#datepicker444").datepicker("option", "minDate", d1);
	},
	onClose: function( selectedDate ) {
		$( "#datepicker444" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker444' ).focus();
	}
	});

	$("#datepicker444").datepicker({
		numberOfMonths: 1,
		minDate: '-12Y',
		dateFormat: 'dd-mm-yy'
		
	});


		$("#markup_type1").on('change', function(){
			if($(this).val() == "SPECIFIC") {
			    $("#country1").css("display", "inherit");
			    $("#date_range1").css("display", "inherit");
			    $("#expry_date1").css("display", "inherit");
		   }else{
			    alert("asdfasdf");
			   $("#country1").css("display", "none");
			   $("#date_range1").css("display", "none");
			   $("#expry_date1").css("display", "none");
		   }
		});
		
	});   
    </script>        
