<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo  $this->session->userdata('company_name');?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/dashboard.css"[A-Za-z]{3} rel="stylesheet">
	<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?php echo ASSETS; ?>assets/js/daterangepicker/daterangepicker-bs3.css">
</head>
<body>
	
	<?php echo $this->load->view('dashboard/top'); ?>	
	<section id="main-content">
  
  <section class="wrapper">
	<div class="main-chart">

        <div class="tab_inside_profile"> 
            <span class="profile_head"><?php echo $this->TravelLights['AcencySettings']['Markup']; ?> </span>
            <div class="rowit marbotom20">
                <div class="inreprow">
                    <div class="col-md-12 nopad">
                    <div class="col-xs-2 nopad">
                        <div class="imagestep">
                            <img alt="" src="<?php echo  ASSETS.'assets/images/markup.png'; ?>">
                        </div>
                    </div>
                    <div class="col-xs-8 nopad fullsetting">
                        <h4 class="stepshed"><?php echo $this->TravelLights['AcencySettings']['MarkupManagement']; ?></h4>
                        <div class="stepspara">
                            <strong><?php echo $this->TravelLights['AcencySettings']['AddorUpdate']; ?></strong>
                        </div>
                    </div>
                    
                    <div class="col-sm-2 nopad fullsetting">
                        <a id="addMarkUp" class="enbleink"><?php echo $this->TravelLights['AcencySettings']['View']; ?></a>
                    </div>
                    
                    <div class="clearfix"></div>
                   
                    <div class="fullquestionswrp5 set_toggle">
                           	<div id="crate_agent">
                                <span class="profile_head adclr"><?php echo $this->TravelLights['AcencySettings']['MarkupList']; ?></span>
                                
                                <div class="withedrow">
                                	<div class="rowit">
                                    <a class="rgtsambtn" id="creat_click"><?php echo $this->TravelLights['AcencySettings']['AddMarkup']; ?></a>
                                    <div class="clearfix"></div>
                                    	<table id="example-markup" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
				<th><?php echo $this->TravelLights['AcencySettings']['SNo']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupType']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Product']; ?></th>
                <!-- <th><?php echo $this->TravelLights['AcencySettings']['Country']; ?></th> -->
               
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupValueType']; ?> </th>
                <th>User</th>
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupValue']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Status']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Action']; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
				<th><?php echo $this->TravelLights['AcencySettings']['SNo']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupType']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Product']; ?></th>
                <!-- <th><?php echo $this->TravelLights['AcencySettings']['Country']; ?></th> -->
                
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupValueType']; ?> </th>
                  <th>User</th>
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupValue']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Status']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Action']; ?></th>
            </tr>
        </tfoot>
        <tbody>
			<?php for($up=0; $up < count($agent_markups); $up++) { ?>
            <tr>
				 <td><?php echo $sno = $up+1; ?> </td>
				 <td><?php echo $agent_markups[$up]->markup_type; ?></td>
				 <td><?php echo $agent_markups[$up]->product_name; ?></td>
				 <!-- <td><?php echo $agent_markups[$up]->country_name; ?></td> -->
				<td><?php echo $agent_markups[$up]->markup_value_type; ?></td>
                 <td>
                     <?php 
                     if($agent_markups[$up]->user_type_id == 2)
                     {
                        echo "B2C USER";
                     }
                     elseif ($agent_markups[$up]->user_type_id == 4) 
                     {
                        echo "B2B USER";
                     }
                     else
                     {
                        echo " FOR ALL";
                     }

                      ?>
 
                 </td> 
                 <td><?php echo $agent_markups[$up]->markup_value; ?></td>
				  <td><?php  if( $agent_markups[$up]->markup_status == 'ACTIVE'){
					   echo "ACTIVE";
					   }else{
						   echo "INACTIVE";
					   } ?></td>
				  <td>
					  <?php if( $agent_markups[$up]->markup_status == 'ACTIVE'){ ?>
					  <a  class="btn btn-warning btn-xs" href="<?php echo  base_url()."settings/changeMarkupStatus/".base64_encode(json_encode($agent_markups[$up]->markup_details_id))."/".base64_encode(json_encode('INACTIVE'))."/".base64_encode(json_encode('YES')); ?>"  onclick="return confirm('<?php echo $this->TravelLights['AcencySettings']['Areyouwantinactivate']; ?>')" ><?php echo $this->TravelLights['AcencySettings']['Inactive']; ?> </a>
					  <?php }else { ?>
						   <a  class="btn btn-success btn-xs" href="<?php echo  base_url()."settings/changeMarkupStatus/".base64_encode(json_encode($agent_markups[$up]->markup_details_id))."/".base64_encode(json_encode('ACTIVE'))."/".base64_encode(json_encode('YES')); ?>" onclick="return confirm('<?php echo $this->TravelLights['AcencySettings']['Areyouwantactivate']; ?>)"> <?php echo $this->TravelLights['AcencySettings']['Active']; ?> </a>
						  <?php } ?>
<!--
						  <a class="btn btn-danger btn-xs" href="<?php echo  ASSETS."settings/deleteMarkup/".base64_encode(json_encode($agent_markups[$up]->markup_details_id)); ?>" onclick="return confirm('Are you want delete?')"> Delete </a>
-->
						  
						  <a class="btn btn-info btn-xs" 
						  onclick='editMarkup("<?php echo  base_url()."settings/editMarkup/".base64_encode(json_encode($agent_markups[$up]->markup_details_id))."/".base64_encode(json_encode('YES')); ?>")' > <?php echo $this->TravelLights['AcencySettings']['Edit']; ?> </a>
			     
			     </td>
							   
			  </tr>
            <?php } ?>
           
        </tbody>
    </table>
                                    </div>
                                </div>
                                
                                </div>
                                
                                
                     <div id="crate_agent_form" style="display:none;">
                     <span class="profile_head adclr"><?php echo $this->TravelLights['AcencySettings']['CreateAgentList']; ?></span> 
               		<div class="withedrow">
                    <div class="rowit">
                    <a class="rgtsambtn" id="back_list"><?php echo $this->TravelLights['AcencySettings']['Backtolist']; ?></a>
                    <div class="clearfix"></div>
                      <div class="addtiktble">
                     <form id="add_markup" name="add_markup" class="validate form-horizontal" method="post" action="<?php echo base_url().'settings/addMarkup'.'/'.base64_encode(json_encode('YES')); ?>" novalidate>   
                            <span style="text-align: center;" id="markup-already-available" class="error" for="markup-already-available"> </span>                 
                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['MarkupType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="markup_type" id="markup_type"class="payinselect mySelectBoxClassfortab hasCustomSelect" required >
                                        <option value="GENERAL"><?php echo $this->TravelLights['AcencySettings']['GENERAL']; ?></option>
                                        <option value="SPECIFIC"><?php echo $this->TravelLights['AcencySettings']['SPECIFIC']; ?></option>
                                </select>
                                  <span id="markup-type-error" class="error" for="markup_type"> </span>
                                 </div>
                            </div>
                        </div>
                        
                        <!--  <div class="likrticktsec" id="user_type1">
                            <div class="cobldo">User Type</div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="type_user_1" id="type_user_1"class="payinselect mySelectBoxClassfortab hasCustomSelect" required >
                                       <option value="3">B2B2B</option>
                                               <option value="">select user</option>
                                        <option value="5">B2B2C</option>
                                </select>
                                 <span id="markup-type-error" class="error" for="markup_type"> </span>
                                 </div>
                            </div>
                        </div>-->
                        
                        
                       
                       <!--   <div class="likrticktsec" id="users_list" style="display:none">
                            <div class="cobldo">Users</div>
                            <div class="coltcnt">
                                 <select name="user_list" id="user_list"class="payinselect mySelectBoxClassfortab hasCustomSelect" required >
                                       <<option value="3">B2B2B</option>
                                       <option value="">Users List</option>
                                       
                                </select>
                            </div>
                        </div>-->
                        
                        
                        <!-- <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['API']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="api_id" value="api_id" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
									   <?php for($ap=0; $ap < count($products); $ap++) { ?> 
                                        <option value ="<?php echo $products[$ap]->api_details_id; ?>" > <?php echo $products[$ap]->product_name ; ?></option>
                                        <?php } ?>
                                </select>
                                 <span id="product-id-error" class="error" for="product_id"> </span>
                                 </div>
                            </div>
                        </div> -->
                        
                         <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['Product']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="product_id" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									   <?php for($prod=0; $prod < count($products); $prod++) { ?> 
                                        <option value ="<?php echo $products[$prod]->product_details_id; ?>" > <?php echo $products[$prod]->product_name; ?></option>
                                        <?php } ?>
                                </select>
                                 <span id="product-id-error" class="error" for="product_id"> </span>
                                 </div>
                            </div>
                        </div>
                      
                    <div class="likrticktsec" id="country" style="display:none;">
                            <div class="cobldo">User Type</div>
                            <div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="user_type_id" id="country_code"   class="payinselect mySelectBoxClassfortab hasCustomSelect" >
                                       <option disabled selected value>Select</option>
                                       <option value="2">B2C Users</option>
                                       <option value="4">Sub Agents</option>
                                </select>
                                <span id="country-code-error" class="error" for="country_code"> </span>
                                 </div>
                            </div>
                        </div> 
                       
                         
                       
                          <!-- <div class="likrticktsec" id="expry_date" style="display:none;">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['ExpiryDate']; ?></div>
                            <div class="coltcnt">
                            	<input type="text" class="payinput cols-sm-2" name="expiry_date" id="datepicker2" placeholder="<?php echo $this->TravelLights['AcencySettings']['ExpiryDate']; ?>"   />
                            </div>
                        </div> -->
                        
                        
                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['MarkupValueType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="markup_value_type" id="markup_value_type" class="payinselect mySelectBoxClassfortab hasCustomSelect" required> 
                                        <option value="Percentage"><?php echo $this->TravelLights['AcencySettings']['PERCENTAGE']; ?></option>
                                        <option value="Fixed"><?php echo $this->TravelLights['AcencySettings']['FIXED']; ?></option>
                                </select>
                                <span id="markup-value-type-error" class="error" for="markup_value_type"> </span>
                                 </div>
                            </div>
                        </div>
                     
                      <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['MarkupValue']; ?></div>
                            <div class="coltcnt">
                                <input type="text" maxlength="5" class="payinput" name="markup_value" id="digits" min="0" id="markup_value"  required  />
                                <span id="markup-value-error" class="error" for="markup_value_type"> </span>
                            </div>
                        </div>
                        
                        <div class="likrticktsec">
                            <div class="cobldo">&nbsp;</div>
                            <div class="coltcnt">
                            <!-- <input type="submit" value="<?php echo $this->TravelLights['AcencySettings']['Add']; ?>" onclick="form_submits('add_markup')" class="adddtickt"> -->
                            <input type="submit" value="<?php echo $this->TravelLights['AcencySettings']['Add']; ?>" onclick="form_submits('add_markup')" class="adddtickt">
                            </div>
                        </div>
                       </form>
                      </div>
                    </div>
                </div>
                             
                           
                    </form>
                  </div> 
                  
                          <div id="edit_agent_form" style="display:none;">
				  <span class="profile_head adclr">Edit Markup</span> 
				  <div class="withedrow">
					   <div class="rowit">
					       <a class="rgtsambtn" id="back_list_edit"><?php echo $this->TravelLights['AcencySettings']['Backtolist']; ?></a>
					     <div class="clearfix"></div>
					     <div class="addtiktble" id="editmark">
							 
							 </div>
					   </div>
				 </div>
			</div>
     
     
                    </div>
        
                    </div>
                </div>
            </div>
            
      </div>
       

<!--- theme Changing  -->




<div class="tab_inside_profile"> 
    
    
	<span class="profile_head"><?php echo $this->TravelLights['AcencySettings']['Changeyourpassword']; ?></span>
    
    <div class="rowit marbotom20">
        <div class="inreprow">
            <div class="col-md-12 nopad">
        	<div class="col-xs-2 nopad">
            	<div class="imagestep">
                	<img alt="" src="<?php echo  ASSETS.'assets/images/key.png'; ?>">
                </div>
            </div>
            <div class="col-xs-7 nopad fullsetting">
                <h4 class="stepshed"><?php echo $this->TravelLights['AcencySettings']['Youcanchange']; ?></h4>
                <div class="stepspara">
                    <strong><?php echo $this->TravelLights['AcencySettings']['Updatecurrentpassword']; ?></strong>
                </div>
			</div>
            
            <div class="col-sm-3 fullsetting">
                <a id="changepaswrd" class="enbleink"><?php echo $this->TravelLights['AcencySettings']['Changeyourpassword']; ?></a>
            </div>

            <div class="clearfix"></div>
            <form name="changePwd" id="changePwd" method="post" action="<?php echo base_url()."account/changePassword"; ?>" novalidate >
            <input type="hidden" name="user_details_id" id="user_details_id" value="<?php echo $this->session->userdata('user_details_id'); ?>" required />
            <div class="fullquestionswrp3 set_toggle">
            <div class="fullquestions">
            	<h4 class="editquestions"><?php echo $this->TravelLights['AcencySettings']['Changeyourpassword']; ?></h4>
                                 <div class="rowshare">
                                    <div class="col-sm-6 fiveshare">
                                        <div class="lablshare"><?php echo $this->TravelLights['AcencySettings']['CurrentPassword']; ?></div>
                                    </div>
                                    <div class="col-sm-6 fiveshare">
										  <input type="password" name="current_password"  id="current_password" class="typecodeans notypmar" required>
                                    </div> 
                                </div>
                                 <div class="rowshare">
                                    <div class="col-sm-6 fiveshare">
                                        <div class="lablshare"><?php echo $this->TravelLights['AcencySettings']['NewPassword']; ?></div>
                                    </div>
                                    <div class="col-sm-6 fiveshare">
										<input type="password" name="password"  id="password" class="typecodeans notypmar" required>
                                     </div>
                                </div>
                                <div class="rowshare">
                                    <div class="col-sm-6 fiveshare">
                                        <div class="lablshare">Confirm New Password</div>
                                    </div>
                                    <div class="col-sm-6 fiveshare">
                                        <input type="password" name="cpassword" id="cpassword"  class="typecodeans notypmar" required>
                                    </div>
                                </div>
                <br>
                <button class="comnbutton"  type="submit"><?php echo $this->TravelLights['AcencySettings']['Update']; ?></button>
                
            </div>
            </div>
            
            </form>
           
            </div>
        </div>
    </div>

</div>


<!-- end theme Changing -->

     <!-- <div class="tab_inside_profile">
    
	<span class="profile_head">Change your Theme</span>
    
    <div class="rowit marbotom20">
        <div class="inreprow">
            <div class="col-md-12 nopad">
        	<div class="col-xs-2 nopad">
            	<div class="imagestep">
                	<img alt="" src="<?php echo  ASSETS.'assets/images/key.png'; ?>">
                </div>
            </div>
            <div class="col-xs-7 nopad fullsetting">
                <h4 class="stepshed">You can Change your theme</h4>
                <div class="stepspara">
                    <strong>Change your current theme</strong>
                </div>
			</div>
            
            <div class="col-sm-3 fullsetting">
                <a id="test" class="enbleink">change your theme</a>
            </div>

            <div class="clear"></div>
            <form name="change_theme" id="change_theme" method="post" action="<?php echo base_url()."account/change_theme"; ?>" novalidate >
            <input type="hidden" name="user_details_id" id="user_details_id" value="<?php echo $this->session->userdata('user_details_id'); ?>" required />
            <div class="theme_popup set_toggle">
            <div class="fullquestions">
            	<h4 class="">Change your theme</h4>
                                 <div class="rowshare">
                                    <div class="col-sm-6 fiveshare">
                                        <div class="lablshare"> Theme </div>
                                    </div>
                                    
                                     <div class="col-sm-6 fiveshare">
                                      <select class="form-control" id="theme_name" name="theme_name">
				                       <option value="">select</option>
                                       <option value="css_purple">purple</option>
                                      <option value="css_red">Red</option>
                                      </select>
                                    </div> 
                                </div>
                              
                          
                                    
                                </div>
                <br>
                <button class="comnbutton" id="themeclick" type="button"  >submit</button>                
            </div>
            </div>
            
            </form>
           
            </div>
        </div>
    </div>
</div> -->
<!-- white labek start -->


     


<!-- white Label end -->


		<!--<div class="tab_inside_profile"> 
        	<span class="profile_head">Change Theme</span>
    
        <div class="rowit marbotom20">
			
            <div class="inreprow">
            <div class="col-md-12 nopad">
				
        	<div class="col-xs-2 nopad">
            	<div class="imagestep">
                	<img alt="" src="<?php echo  ASSETS.'assets/images/cancel.png'; ?>">
                </div>
            </div>
               <div class="clear"></div>
            
            <div class="col-xs-8 nopad fullsetting">
                <h4 class="stepshed"> Chnage your theme</h4>
                <div class="stepspara"><strong></strong></div>
			</div>
			
			<form name="change_theme_frm" id="change_theme_frm" method="post" 
                 action="<?php echo ASSETS."account/change_theme"; ?>" 
                 novalidate >
                 <input type="hidden" name="user_details_id" id="user_details_id"
                  value="<?php echo $this->session->userdata('user_details_id'); ?>" required />
                <div class="form-group col-md-6">
                  <select class="form-control " id="theme_name" name="theme_name">
				  <option value="">select</option>
                  <option value="theme_style">Theme</option>
                  <option value="red_theme_style">Red Theme</option>
                  </select>
               </div>
			</div>
			<div class="row">  
				<div class="col-md-9"></div>
				<div class="col-md-3 fullsetting .col-sm-offset-5">
				  <button class="comnbutton"  type="submit">Change Theme</button>
			   
            </div>
            </div>
          
           
         </form>
       </div>
       </div>
       </div>
       </div>-->
  

<!--
		<div class="tab_inside_profile"> 
        	<span class="profile_head">Close Account</span>
    
        <div class="rowit marbotom20">
            <div class="inreprow">
        <div class="col-md-12 nopad">
        	<div class="col-xs-2 nopad">
            	<div class="imagestep">
                	<img alt="" src="<?php echo  ASSETS.'assets/images/cancel.png'; ?>">
                </div>
            </div>
            <div class="col-xs-8 nopad fullsetting">
                <h4 class="stepshed"> Close Account</h4>
                <div class="stepspara"><strong>You'll need verification codes</strong>
    After entering your password, you'll enter a code that you'll get via text, voice call, or our mobile app.</div>
			</div>
            
            <div class="col-sm-2 fullsetting">
                <a data-target="#cancelAccount" data-toggle="modal" class="enbleink"> Close Account</a>
            </div>
      

            <div class="clearfix"></div>
            
    		<div id="cancelAccount" class="popup_wrapper fullquestionswrp4 modal fade">
                <div class="modal-dialog">
    				<div class="modal-content">
                    <div style="display:none;" class="popuperror"></div>
                   
                    <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title">Close Account</h4>
                    </div>
                    
                    <div class="signdiv">
                        <div class="rowlistwish">
                            <strong>Closing your account will delete all your information from </strong>
                        </div>
                        
                      <div class="clear"></div>
                      <div class="downselfom">
                        <a data-dismiss="modal" id="hideCancelPopup" class="savewish colorcancel">Cancel</a>
                        <a id="startCancelProc" class="savewish colorsave">Start Closing Process</a>
                      </div>
                    </div> 
                    </div>    
                </div>
            </div>

            <div style="display:none;" class="wellme minwidth" id="cancel2stepVerify">
                <div style="display:none;" class="popuperror"></div>
                <div class="pophed"> Close Account</div>
                <div class="signdiv">
                    <div class="rowlistwish">
                        <strong>Enter Verification Code <span id="typeString"></span> </strong>
                        <input type="text"  id="twoStepCode" class="fulwish typecodeans">
                        <span id="verificationCodeErr"></span>
                    </div>
                    
                  <div class="clear"></div>
                  <div class="downselfom">
                    <a id="verifyPswd" class="savewish colorsave">Submit</a>
                  </div>
                </div>
            </div>

            <div style="display:none;" class="wellme minwidth" id="cancelLoginPswd">
                <div style="display:none;" id="popuperror_verifyOneStepPswd" class="popuperror">Password Missing.</div>
                <div class="pophed">Close Account</div>
                <div class="signdiv">
                    <div class="rowlistwish">
                        <strong>Enter Password</strong>
                        <input type="text" id="oneStepCode" class="fulwish typecodeans">
                    </div>
                    
                  <div class="clear"></div>
                  <div class="downselfom">
                    <a id="verifyOneStepPswd" class="savewish colorsave">Submit</a>
                  </div>
                </div>
            </div>
    </div>
        </div>
        </div>
        </div>
        
-->
        </div>
  </section>

</section>

	<?php //echo $this->load->view('core/bottom_footer'); ?>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
	<script src="<?php echo ASSETS; ?>assets/js/daterangepicker/moment.min.js"></script>
    <script src="<?php echo ASSETS; ?>assets/js/daterangepicker/daterangepicker.js"></script>

<script type="text/javascript">

<?php if(isset($class)) { ?> 
setTimeout(function(){
$('#addMarkUp').trigger('click'); 
}, 300);
<?php } ?>

$(document).ready(function(){
$('#example-markup').DataTable({responsive:true});	
	
	
	
	$('#addMarkUp').on("click", function(){
		

		$('.fullquestionswrp5').slideToggle(500);
				 $($.fn.dataTable.tables(true)).DataTable()
                  .columns.adjust()
                  .responsive.recalc();
	});
	
	 
	
	$('#creat_click').click(function(){
		$('#crate_agent').fadeOut(500, function(){
				$('#crate_agent_form').fadeIn();
			});
			
	});
	
	$('#back_list').click(function(){

		$('#crate_agent_form').fadeOut(500, function(){
				$('#crate_agent').fadeIn();
				
			});
			
	});
	
	
	$('#back_list_edit').click(function(){
		$('#edit_agent_form').fadeOut(500, function(){
				$('#crate_agent').fadeIn();
			});
	});
	
	
	$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
	});
	
	
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	
});

$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});

var Script = function () {

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                //$('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });




    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });




}();



</script>

<script>
	$(document).ready(function(){
		

    $("#datepicker").datepicker({
	numberOfMonths: 1,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: 1,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate()); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#datepicker1").datepicker("setDate", d1);
		$("#datepicker1").datepicker("option", "minDate", d1);
	},
	onClose: function( selectedDate ) {
		$( "#datepicker1" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker1' ).focus();
	}
});

	$("#datepicker1").datepicker({
		numberOfMonths: 1,
		minDate: '-12Y',
		dateFormat: 'dd-mm-yy'
		
	});
	
   $("#datepicker2").datepicker({
	numberOfMonths: 1,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: 1,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate()); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#datepicker3").datepicker("setDate", d1);
		$("#datepicker3").datepicker("option", "minDate", d1);
	},
	onClose: function( selectedDate ) {
		$( "#datepicker3" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker3' ).focus();
	}
	});

	$("#datepicker3").datepicker({
		numberOfMonths: 1,
		minDate: '-12Y',
		dateFormat: 'dd-mm-yy'
		
	});

		
		$("#markup_type").on('change', function(){
		   if($(this).val() == "SPECIFIC") {
			    $("#country").css("display", "inherit");
			    $("#date_range").css("display", "inherit");
			    $("#expry_date").css("display", "inherit");
		   }else{
			   $("#country").css("display", "none");
			  $("#date_range").css("display", "none");
			  $("#expry_date").css("display", "none");
		   }
		});
		
		
		
		$('#editquestion').click(function(){
			$('.fullquestionswrp').slideToggle(500);
		});
		
		$('#editprivatepub').click(function(){
			$('.fullquestionswrpshare').slideToggle(500);
		});
		
		$('#smsalert').click(function(){
			$('.fullquestionswrp2').slideToggle(500);
		});
		
		$('#changepaswrd').click(function(){
			$('.fullquestionswrp3').slideToggle(500);
		});
		
			$('#test').click(function(){
			$('.theme_popup').slideToggle(500);
		});
				$('#label_click').click(function(){
			$('.label_popup').slideToggle(500);
		});


   	});
	
	
	  function editMarkup(url){
		
		$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#crate_agent').fadeOut(500, function(){
				$('#edit_agent_form').fadeIn();
			});
				$("#edit_agent_form").css('display','inherit');
				$("#editmark").html(data.updateMarkup);
				
				return false;
			}
		});
	}
	
	
	
	$('#themeclick').click(function(){
		var url = $('#change_theme').attr('action');
		$.ajax({
			type: "POST",
			url: url,
	
			data:{'user_details_id':$("#user_details_id").val(),'theme_name':$("#theme_name").val()},
			dataType: "json",
			success: function(data){
	     	alert('theme updated successfully');
			},
			error:function(){
				alert('error');}
		});
		
	});
	
	$('#url_click').click(function(){
		var url = $('#change_url').attr('action');
		var  id = $("#user_details_id").val();
		var url_name =$("#url").val();
		$.ajax({
			type: "POST",
			url: url,
			data:{'user_details_id':id,'url_name':url_name},
			dataType: "json",
			success: function(data){
	     	alert('url updated successfully');
			},
			error:function(){
				alert('error');
				}
		});
		
	});
	
</script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/input-number.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/general.js"></script>
    <?php echo  $this->load->view('core/bottom_footer'); ?> 
</body>
</html>
