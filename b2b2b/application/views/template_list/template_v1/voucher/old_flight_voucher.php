<style>
    th,
    td {
        padding: 5px;
    }
    
    table {
        page-break-inside: auto
    }
    
    tr {
        page-break-inside: avoid;
        page-break-after: auto
    }
    
    .tbl_style {
        border-collapse: collapse;
    }
    
    .tbl_style td {
        padding: 4px;
        border: 1px solid #ccc;
    }
    
    .tbl_style th {
        padding: 4px;
        border: 1px solid #ccc;
    }
</style>
<?php
$booking_details = $data ['booking_details'] [0];
// debug($booking_details);exit;
$itinerary_details = $booking_details ['booking_itinerary_details'];

$attributes = $booking_details ['attributes'];
// debug($itinerary_details);exit;
$customer_details = $booking_details ['booking_transaction_details'] [0] ['booking_customer_details'];
$domain_details = $booking_details;
$lead_pax_details = $customer_details;
$booking_transaction_details = $booking_details ['booking_transaction_details'];
// debug($booking_transaction_details);exit;
$adult_count = 0;
$infant_count = 0;
// debug($customer_details);exit;
foreach ( $customer_details as $k => $v ) {
    if (strtolower ( $v ['passenger_type'] ) == 'infant') {
        $infant_count ++;
    } else {
        $adult_count ++;
    }
}

$Onward = '';
$return = '';
if (count ( $booking_transaction_details ) == 2) {
    $Onward = '(Onward)';
    $Return = '(Return)';
}

// generate onword and return
if ($booking_details ['is_domestic'] == true && count ( $booking_transaction_details ) == 2) {
    $onward_segment_details = array ();
    $return_segment_details = array ();
    $segment_indicator_arr = array ();
    $segment_indicator_sort = array ();
    
    foreach ( $itinerary_details as $key => $key_sort_data ) {
        $segment_indicator_sort [$key] = $key_sort_data ['origin'];
    }
    array_multisort ( $segment_indicator_sort, SORT_ASC, $itinerary_details );
    // debug($itinerary_details);exit;
    
    foreach ( $itinerary_details as $k => $sub_details ) {
        $segment_indicator_arr [] = $sub_details ['segment_indicator'];
        $count_value = array_count_values ( $segment_indicator_arr );
        
        if ($count_value [1] == 1) {
            $onward_segment_details [] = $sub_details;
        } else {
            $return_segment_details [] = $sub_details;
        }
    }
}

?>
<div class="table-responsive">
    <table style="border-collapse: collapse; background: #ffffff; font-size: 14px; margin: 0 auto; font-family: arial;border: 1px solid;" width="900" cellpadding="0" cellspacing="0" border="0">
        <tbody>
            <tr>
                <td style="border-collapse: collapse;">
                    <table width="100%" style="border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0">

                        <tr>
                            <td style="padding: 10px;">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" cellpadding="5" style="border-collapse: collapse;">
                                    <tr>
                                        <td style="font-size: 27px; line-height: 30px; width: 40%; display: block; font-weight: normal; text-align: left;float: left;padding: 10px 12px;">
                                            E TICKET </td>
                                        <td style="font-size: 27px; line-height: 30px; width: 50%; display: block; font-weight: normal; text-align: left;float: right;">
                                        <?php if($this->session->userdata('site_name') == ''){ ?>
                                            <img style="max-height: 40px;float: right;" src="<?php echo base_url().'/assets/images/logo.png'; ?>" alt="flight-logo" />
                                        <?php } else { ?>
                                            <img style="max-height: 40px;float: right;" src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $this->session->userdata('domain_logo'); ?>" alt="flight-logo" />
                                        <?php } ?>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="padding: 0px; font-size: 16px;" width="100%" cellpadding="5">
                                                <tbody>
                                                    <tr>
                                                        <td style="border: 1px solid #2B2E34;background: #EBECEC;line-height: 30px;padding: 5px 20px;"><?=@$booking_details['app_reference'];?></td>
                                                        <td style="border: 1px solid #2B2E34;background: #EBECEC;line-height: 30px;padding: 5px 20px;"><?=@$booking_transaction_details[0]['pnr']?></td>
                                                        <td style="border: 1px solid #2B2E34;background: #EBECEC;line-height: 30px;padding: 5px 20px;">R</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> 
                                            <table style="padding: 0px; font-size: 16px;" width="100%" cellpadding="5">
                                                <tbody>
                                                    <?php foreach(@$data['booking_details'][0]['booking_itinerary_details'] as $details_booking){ ?>
                                                    <tr>
                                                        <td style="border: 1px solid #2B2E34;width:33.5%;line-height: 30px;padding: 5px 20px;"><?=app_friendly_absolute_date(@$booking_details['booked_date'])?></td>
                                                        <td style="border: 1px solid #2B2E34;width:33.5%;line-height: 30px;padding: 5px 10px;"><?= strtoupper(@$details_booking['from_airport_name']); ?> &nbsp; TO &nbsp;<?= strtoupper(@$details_booking['to_airport_name']); ?></td>
                                                        <td style="border: 1px solid #2B2E34;width:33.5%;line-height: 30px;padding: 5px 20px;">

                                                            <?php
                                            switch ($data['booking_details'][0]['status']) {
                                                case 'BOOKING_CONFIRMED' :
                                                    echo 'CONFIRMED';
                                                    break;
                                                case 'BOOKING_CANCELLED' :
                                                    echo 'CANCELLED';
                                                    break;
                                                case 'BOOKING_FAILED' :
                                                    echo 'FAILED';
                                                    break;
                                                case 'BOOKING_INPROGRESS' :
                                                    echo 'INPROGRESS';
                                                    break;
                                                case 'BOOKING_INCOMPLETE' :
                                                    echo 'INCOMPLETE';
                                                    break;
                                                case 'BOOKING_HOLD' :
                                                    echo 'HOLD';
                                                    break;
                                                case 'BOOKING_PENDING' :
                                                    echo 'PENDING';
                                                    break;
                                                case 'BOOKING_ERROR' :
                                                    echo 'ERROR';
                                                    break;
                                            }
                                            
                                            ?>  

                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="100%">
                                            <table width="100%" cellpadding="5" style="padding: 0px; font-size: 15px;">
                                                <!--<tr>
                                                <td><strong>Flight</strong></td>
                                                <td><strong>AirlinePNR</strong></td>
                                                <td><strong>Departure</strong></td>
                                            </tr>-->
                                            <?php foreach(@$data['booking_details'][0]['booking_itinerary_details'] as $details_booking_flight_details){ ?>
                                                <tr>

                                                    <td style="border: 1px solid #2B2E34;width:33.5%;padding: 10px 20px;line-height: 30px; ">
                                                        <span style="width: 50%; float: left;border-right: 1px solid #2B2E34;">Flight Logo & No Details</span>
                                                        <!-- <img style="max-height: 30px" src="https://linkindian.com/extras/custom/BBkkuw3qV6TTtnKS5cVt/images/BBkkuw3qV6TTtnKS5cVtUntitled-12.png" alt="flight-logo" /> -->
                                                        <span style="padding-left: 10px;">
                                                        <?= @$details_booking_flight_details['airline_name'].' - '; ?>
                                                        <?= @$details_booking_flight_details['airline_code'].' '.@$details_booking_flight_details['flight_number']; ?>
                                                        </span>
                                                        </td>
                                                    <td style="border: 1px solid #2B2E34;width:33.5%;background: #EBECEC;padding: 10px 20px;line-height: 30px;"> <span style="width: 100%;text-align: center; float: left"><strong><?= @$details_booking_flight_details['from_airport_code']; ?></strong></span>
                                                        <span style="width: 100%;text-align: center; float: left">
                                                        <?php echo date("d M Y",strtotime($details_booking_flight_details['departure_datetime'])).", ".date("H:i",strtotime($details_booking_flight_details['departure_datetime']));?>      
                                                        </span>
                                                        <span style="width: 100%;text-align: center; float: left"><?= $details_booking_flight_details['from_airport_name']; ?></span></td>
                                                    <td style="border: 1px solid #2B2E34;width:33.5%;background: #EBECEC;padding: 10px 20px;line-height: 30px;"> <span style="width: 100%;text-align: center; float: left"><strong><?= @$details_booking_flight_details['to_airport_code']; ?></strong></span>
                                                        <span style="width: 100%;text-align: center; float: left">
                                                        <?php echo date("d M Y",strtotime($details_booking_flight_details['arrival_datetime'])).", ".date("H:i",strtotime($details_booking_flight_details['arrival_datetime']));?>  
                                                        </span>
                                                        <span style="width: 100%;text-align: center; float: left"><?= @$details_booking_flight_details['to_airport_name']; ?></span></td>
                                                </tr>
                                               <?php } ?>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr id="amounthideag" style="display: ;">
                                        <td>
                                            <table width="100%" border="0" class="tbl_style" align="center" cellpadding="0" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th style="background: #EBECEC;border: 1px solid #2B2E34;padding: 5px 20px;line-height: 30px;">SL.No</th>
                                                        <th style="background: #EBECEC;border: 1px solid #2B2E34;padding: 5px 20px;line-height: 30px;">TRAVELLERS</th>
                                                        <th style="background: #EBECEC;border: 1px solid #2B2E34;padding: 5px 20px;">PNR</th>
                                                        <th style="background: #EBECEC;border: 1px solid #2B2E34;padding: 5px 20px;">TICKET NO</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    $i = 1;
                                                    $userdata_unique = array();
                                                    foreach($data['booking_details'][0]['booking_transaction_details'][0]['booking_customer_details'] as $customer_detail){
                                                        if(!in_array($customer_detail['origin'], $userdata_unique)){
                                                    ?>
                                                    <tr>
                                                        <td style="background: #EBECEC;border: 1px solid #2B2E34;padding: 5px 20px;line-height: 30px;"><?= $i; ?></td>
                                                        <td style="background: #EBECEC;border: 1px solid #2B2E34;padding: 5px 20px;"><?= $customer_detail['title'].' '. ucfirst($customer_detail['first_name']) . ' '. $customer_detail['middle_name'].' '. $customer_detail['last_name']; ?></td>
                                                        <td style="background: #EBECEC;border: 1px solid #2B2E34;padding: 5px 20px;"><?= $data['booking_details'][0]['pnr']; ?></td>
                                                        <td style="background: #EBECEC;border: 1px solid #2B2E34;padding: 5px 20px;"><?= $customer_detail['TicketNumber']; ?></td>
                                                    </tr>
                                                    <?php 
                                                            $i++;
                                                        }
                                                        if(isset($customer_detail['origin']))
                                                        array_push($userdata_unique, $customer_detail['origin']);
                                                   
                                                     } ?> 
                                                
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr id="amounthideag" style="display: ;">
                                        <td>
                                            <table width="100%" border="0" class="tbl_style" align="center" cellpadding="0" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th style="background: #EBECEC;border: 1px solid #2B2E34;padding: 5px 20px;line-height: 30px;">FARE DETAILS</th>
                                                        <th style="background: #EBECEC;border: 1px solid #2B2E34;padding: 5px 20px;line-height: 30px;">PASSENGER INFORMATION</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="border: 1px solid #2B2E34;">
                                                            <span style="width: 100%;padding: 5px 20px;line-height: 30px; float: left;">Basic Fare: <?= @$data['booking_details'][0]['currency'].'.'.@$data['booking_details'][0]['grand_total']; ?></span>
                                                            <span style="width: 100%;padding: 5px 20px;line-height: 30px; float: left; font-size: 15px;">GST:</span>
                                                            <span style="width: 100%; padding: 5px 20px;line-height: 30px;float: left;font-size: 15px;">Pg Charges: <?= @$data['booking_details'][0]['currency'].'.'.@$data['booking_details'][0]['convinence_amount']; ?> </span>
                                                        </td>
                                                        <td style="border: 1px solid #2B2E34;">
                                                            <span style="width: 100%; padding: 5px 20px;line-height: 30px;float: left;font-size: 15px;">Email: <?=@$data['booking_details'][0]['lead_pax_email']; ?></span>
                                                            <span style="width: 100%; padding: 5px 20px;line-height: 30px;float: left;font-size: 15px;">Mob No: <?=@$data['booking_details'][0]['lead_pax_phone_number']; ?></span>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td style="border: 1px solid #2B2E34;"><span style="width: 100%;padding: 5px 20px;line-height: 30px; float: left;">ADDITIONAL SERVICES:</span></td>
                                                        <td style="border: 1px solid #2B2E34;"><span style="width: 100%;padding: 5px 20px;line-height: 30px; float: left;"></span></td>
                                                    </tr>
                                                    <tr>
                                                    <?php 
                                                        $baggage = json_decode($data['booking_details'][0]['booking_itinerary_details'][0]['attributes']);
                                                     ?>
                                                        <td style="border: 1px solid #2B2E34;"><span style="width: 100%;padding: 5px 20px;line-height: 30px; float: left;">BAGGAGE INFORMATION:</span></td>
                                                        <td style="border: 1px solid #2B2E34;"><span style="width: 100%;padding: 5px 20px;line-height: 30px; float: left;"></span></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="50%" style="padding: 10px; border: 0px solid #2B2E34; font-size: 15px; font-weight: bold;">Terms and Conditions</td>
                                    </tr>
                                    <tr>
                                        <td width="100%" style="border: 0px solid #2B2E34;">
                                            <table width="100%" cellpadding="5" style="padding: 10px 20px; font-size: 16px;">
                                                <tr>
                                                    <td>1. Use the reference number for all correspondence with us.</td>
                                                </tr>
                                                <tr>
                                                    <td>2.Use the airplane PNR for all correspondence directly with the airline.</td>
                                                </tr>
                                                <tr>
                                                    <td>3.For depature terminal please check with airline first.</td>
                                                </tr>
                                                <tr>
                                                    <td>4.You can carry a print-out of this e-ticket and present to the airlinr for check-in.</td>
                                                </tr>
                                                <tr>
                                                    <td>5.You should carry a photo identification proof while checking-in.</td>
                                                </tr>
                                                <tr>
                                                    <td>6.For any cancellation or rescheduling of tickets,contact us 4 hrs prior for domestic ans 24 hrs prior for international depatures.</td>
                                                </tr>
                                                <tr>
                                                    <td>7.If your flight gets cancelled or you cancel the ticket directly with the airlinr then please inform us to init the refund.</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p align='center'><strong>Thank you for booking with us.<br>
We wish you a pleasent journey and hopes to serve you again in the future.</strong></p>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <!-- Return Ticket --
<table id="printOption"
    onclick="document.getElementById('printOption').style.visibility = ''; print(); return true;"
    style="border-collapse: collapse; font-size: 14px; margin: 10px auto; font-family: arial;"
    width="70%" cellpadding="0" cellspacing="0" border="0">
    <tbody>
        <tr>
            <td align="center"><input
                style="background: #418bca; height: 34px; padding: 10px; border-radius: 4px; border: none; color: #fff; margin: 0 2px;"
                type="button" value="Print" /></td>
        </tr>
    </tbody>
</table>-->
</div>