<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title><?= $this->session->userdata('company_name')?></title>
<?php echo $this->load->view('core/load_css'); ?>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="<?php echo ASSETS;?>assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	
	<?php echo $this->load->view('dashboard/top'); ?>	

	<section id="main-content">
  
  <section class="wrapper">
	<div class="main-chart">

          	<div class="intabs">
                            	<div id="crate_deposit">
                                <span class="profile_head adclr"><?php echo $this->TravelLights['DepositManagementForm']['DepositManagement']; ?></span>
                                
                                <div class="withedrow">
                                	<div class="rowit">
                                    <a class="rgtsambtn" id="creat_deposit"><?php echo $this->TravelLights['DepositManagementForm']['AddDeposit']; ?></a>
                                    <div class="clearfix"></div>
                                    	<table id="depositeTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
				<th><?php echo $this->TravelLights['DepositManagementForm']['Slno']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['DepositType']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['AgentName']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['DepositedDate']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['Amount']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['CreditedAmount']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['CurrencyCode']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['Remarks']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['AdminRemarks']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['Status']; ?></th>
              
            </tr>
        </thead>
        <tfoot>
            <tr>
				<th><?php echo $this->TravelLights['DepositManagementForm']['Slno']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['DepositType']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['AgentName']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['DepositedDate']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['Amount']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['CreditedAmount']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['CurrencyCode']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['Remarks']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['AdminRemarks']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['Status']; ?></th>
               
            </tr>
        </tfoot>
        <tbody>
           <?php if($depsoite!=''){ for($a=0;$a<count($depsoite);$a++){ ?>
						<tr>
							<td><?php echo ($a+1); ?></td>
							<td><?php echo $depsoite[$a]->deposit_type; ?></td>
							<td><?php echo $depsoite[$a]->user_name; ?></td>
							<td><?php echo $depsoite[$a]->deposited_date; ?></td>
							
							 <?php $pdata = $depsoite[$a]->currencycode;
                                $TotalPrice_Curr  = 'AUD';
                                $TotalPrice =  $depsoite[$a]->amount_credit;
                            	$TotalPrice = $this->General_Model->currency_convertor($TotalPrice,$pdata,$TotalPrice_Curr);
                                ?> 
							<td><?php echo $TotalPrice; ?></td>
							<td><?php echo $depsoite[$a]->amount_credit; ?></td>
							<td><?php echo $depsoite[$a]->currencycode; ?></td>
							<td><?php echo $depsoite[$a]->remarks; ?></td>
                            <td><?php echo $depsoite[$a]->admin_remarks; ?></td>
							<td>
								<?php if($depsoite[$a]->status == "Accepted"){ ?>
									<button type="button" class="btn btn-green btn-icon icon-left action-details1"><?php echo $this->TravelLights['DepositManagementForm']['ACCEPTED']; ?></button>
								<?php }elseif($depsoite[$a]->status == "Cancelled"){ ?>
									<button type="button" class="btn btn-red btn-icon icon-left action-details1"><?php echo $this->TravelLights['DepositManagementForm']['CANCELLED']; ?></button>
								<?php }else{ ?>
										<button type="button" class="btn btn-orange btn-icon icon-left action-details1"><?php echo $this->TravelLights['DepositManagementForm']['PENDING']; ?></button>
								<?php } ?>
							</td>
							
							
						</tr>
					<?php }} ?>
           
        </tbody>
    </table>
                                    </div>
                                </div>
                                
                                </div>
                                
                                
                                <div id="crate_deposit_form" style="display:none;">
                                	<span class="profile_head adclr"><?php echo $this->TravelLights['ChangeDeposit']['AddDeposit']; ?></span> 
               						 <div class="withedrow">
                    <div class="rowit">
                    <a class="rgtsambtn" id="back_list_deposit"><?php echo $this->TravelLights['DepositManagementForm']['Back']; ?></a>
                    <div class="clearfix"></div>
                      <div class="addtiktble">
                     <form id="deposite" name="Deposit" class="validate form-horizontal"  method="post" action="<?php echo base_url().'usermanagement/addDeposit'; ?>" novalidate  >  
                                  
                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['DepositType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="deposit_type" id="deposit_type" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
                                       <option value="CASH"><?php echo $this->TravelLights['DepositManagementForm']['CASH']; ?></option>
                                        <option value="BANK"><?php echo $this->TravelLights['DepositManagementForm']['BANK']; ?></option>
                                        <option value="CHEQUE/DD"><?php echo $this->TravelLights['DepositManagementForm']['CHEQUEDD']; ?></option>
                                </select>
                                <span id="deposit-type-error" class="error" for="deposit_type"> </span>
                                 </div>
                            </div>
                        </div>
                        
                         <div class="likrticktsec hide">
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['AgentCurrencyCode']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="currencycode" id="currencycode" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
                                       <?php if($currency!=''){ for($a=0;$a<count($currency);$a++){ ?>
											<option value="<?php echo $currency[$a]->currency_code; ?>" data-iconurl=""><?php echo $currency[$a]->currency_code ." (".$currency[$a]->currency_name.")"; ?></option>
									   <?php }} ?>
                                </select>
                                <span id="currencycode-error" class="error" for="currencycode"> </span>
                                 </div>
                            </div>
                        </div>
                        
                        <div class="likrticktsec" id="deposit_date">
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['DepositedDate']; ?></div>
                            <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" name="deposit_date" id="datepicker3" placeholder="<?php echo $this->TravelLights['DepositManagementForm']['DepositDate']; ?>"   />
                            </div>
                        </div>

                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['AmountDepostied']; ?></div>
                            <div class="coltcnt">

                                  <input type="text" name="amount_credit" id="amount_credit" minlength = "1" maxlength="7"  class="payinput" required > 
                                  <span id="amount-credit-error" class="error" for="amount_credit"> </span>
                            </div>
                        </div>
                        
                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['Remarks']; ?></div>
                            <div class="coltcnt">
                                <textarea id="remarks" name="remarks" class="tikttext" required ></textarea>
                                 <span id="remarks-error" class="error" for="remarks"> </span>
                            </div>
                        </div>
                        
                        <div class="likrticktsec">
                            <div class="cobldo">&nbsp;</div>
                            <div class="coltcnt">
                            <input type="submit" value="<?php echo $this->TravelLights['DepositManagementForm']['Add']; ?>" onclick="form_submits('deposite')" class="adddtickt">
                            </div>
                        </div>
                        </form>
                      </div>
                    </div>
                </div>
              </div>
            </div>
                       
                        

        
        </div>
  </section>

</section>




<div class="clearfix"></div>
	<?php echo $this->load->view('core/bottom_footer'); ?>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
   
   $( "#datepicker" ).datepicker();
   $( "#datepicker1" ).datepicker();
   $( "#datepicker2" ).datepicker();
   $( "#datepicker3" ).datepicker({  maxDate: new Date() });
	
	$("#markup_type").on('change', function(){
		 if($(this).val() == "SPECIFIC") {
			    $("#user_t").css("display", "inherit");
			    $("#agent_n").css("display", "inherit");
			    $("#country").css("display", "inherit");
			    $("#date_range").css("display", "inherit");
			    $("#expry_date").css("display", "inherit");
		   }else{
			    $("#user_t").css("display", "none");
			    $("#agent_n").css("display", "none");
			   $("#country").css("display", "none");
			  $("#date_range").css("display", "none");
			  $("#expry_date").css("display", "none");
		   }
		});
  
    $('#agentMarkTable').DataTable({responsive: true});
    $('#depositeTable').DataTable({responsive: true});
   
	
	$('#creat_click').click(function(){
		$('#crate_agent').fadeOut(500, function(){
				$('#crate_agent_form').fadeIn();
			});
	});
	
	$('#creat_deposit').click(function(){
		$('#crate_deposit').fadeOut(500, function(){
				$('#crate_deposit_form').fadeIn();
			});
	});
	
	$('#creat_markup').click(function(){
		$('#crate_markup_list').fadeOut(500, function(){
				$('#crate_markup_form').fadeIn();
			});
	});
	
	$('#back_list').click(function(){
		$('#crate_agent_form').fadeOut(500, function(){
				$('#crate_agent').fadeIn();
			});
	});
	
	$('#back_list_deposit').click(function(){
		$('#crate_deposit_form').fadeOut(500, function(){
				$('#crate_deposit').fadeIn();
			});
	});
	
	$('#back_list_markup').click(function(){
		$('#crate_markup_form').fadeOut(500, function(){
				$('#crate_markup_list').fadeIn();
			});
	});
	
	$('#back_edit_list_markup').click(function(){
		$('#edit_markup_form').fadeOut(500, function(){
				$('#crate_markup_list').fadeIn();
			});
	});
	
	$('#back_list_edit_agent').click(function(){
		$('#edit_agent_form').fadeOut(500, function(){
				$('#crate_agent').fadeIn();
			});
	});
	
	



	
	
	
	$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
	});
	
	
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	
});




$("#owl-demobaners1").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	  $("#owl-demobaners2").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	   $("#owl-demobaners3").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });


$("#owl-demo3").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });

$("#owl-demo4").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });
	  
	  $("#owl-demo5").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });


$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});
var Script = function () {


//    sidebar dropdown menu auto scrolling





//    sidebar toggle

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                //$('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });


    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });




}();


</script>

<script>
$(document).ready(function(){
	getAgentList();
    
		$('#editquestion').click(function(){
			$('.fullquestionswrp').slideToggle(500);
		});
		
		$('#editprivatepub').click(function(){
			$('.fullquestionswrpshare').slideToggle(500);
		});
		
		$('#smsalert').click(function(){
			$('.fullquestionswrp2').slideToggle(500);
		});
		
		$('#changepaswrd').click(function(){
			$('.fullquestionswrp3').slideToggle(500);
		});

        $('#addMarkUp').click(function() {
            $('.fullquestionswrp5').slideToggle(500);
        })
			
	});
	
	function getAgentList(){
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url().'usermanagement/getAgentList'; ?>",
			dataType: "json",
			success: function(data){
				$("#agentData").html(data.agent_list);
			}
		});
	}
	
	function editAgent(url){
		$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#crate_agent').fadeOut(500, function(){
				$('#edit_agent_form').fadeIn();
			});
				$("#edit_agent_form").css('display','inherit');
				$("#edit_form").html(data.editAgent);
				
				return false;
			}
		});
	}

    function editMarkup(url){
		
		$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#crate_markup_list').fadeOut(500, function(){
				$('#edit_markup_form').fadeIn();
			});
				$("#edit_markup_form").css('display','inherit');
				$("#editmark").html(data.updateMarkup);
				
				return false;
			}
		});
	}
	</script>



</body>
</html>
