<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo  $this->session->userdata('company_name');?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet">
</head>
<body>
	<?php echo $this->load->view('dashboard/top'); ?>	
	<?php //echo $this->load->view('dashboard/left_menu'); ?>	
<section id="main-content">
	  
	  <section class="wrapper">
		

   		
  		 <div class="loginsreset thanklogin">
          	<div class="agenthed">Access Denied!</div>
            <div class="signdiv">  
              <div class="insigndiv">
                	<span class="parareserve">Further queries please contact administrator.</span>
              </div>
            </div>
          
        </div>
   


</section>
</section>

<div class="clearfix"></div>
	
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.dash_menus').click(function(){
		$('.sidebar_wrap').toggleClass('open');
	});
	
	$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
	});
	
	
	
	
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	
});




$("#owl-demobaners1").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	  $("#owl-demobaners2").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	   $("#owl-demobaners3").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });


$("#owl-demo3").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });

$("#owl-demo4").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });
	  
	  $("#owl-demo5").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });


$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});
var Script = function () {



    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                //$('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });




    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });




}();



</script>

<script>
	$(document).ready(function(){
		
		$('#editquestion').click(function(){
			$('.fullquestionswrp').slideToggle(500);
		});
		
		$('#editprivatepub').click(function(){
			$('.fullquestionswrpshare').slideToggle(500);
		});
		
		$('#smsalert').click(function(){
			$('.fullquestionswrp2').slideToggle(500);
		});
		
		$('#changepaswrd').click(function(){
			$('.fullquestionswrp3').slideToggle(500);
		});

        $('#addMarkUp').click(function() {
            $('.fullquestionswrp5').slideToggle(500);
        })
			
	});
</script>
	<script type="text/javascript">
		var check_in = "<?php echo date('d-m-Y', strtotime('1 days'));?>";
		var check_out = "<?php echo date('d-m-Y', strtotime('2 days'));?>";
		var Infant_count =0;
		var child_count =0;
		var WEB_URL = "<?php echo ASSETS; ?>";
		var ASSETS = "<?php echo ASSETS; ?>assets/";
	</script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/input-number.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/general.js"></script>
	<script>
		$.widget( "custom.catcomplete", $.ui.autocomplete, {
    _create: function() {
      this._super();
      this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
    },

    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) { 
        var li;
        if ( item.type != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category "+item.type+"'>" + item.type + "</li>" );
          currentCategory = item.type;
        }
        li = that._renderItemData( ul, item );
        if ( item.type ) {
          li.attr( "aria-label", item.type + " : " + item.label );
        }
      });
    },
    _renderItem: function( ul, item ) { 
      if(item.type == 'City'){
      return $( "<li>" )
      //.addClass(item.category)
      .attr( "data-value", item.value )     
      .append( $( "<a>" ).html('<i class="fa fa-building-o wd20"></i> <span class="ctynme">'+item.label + '</span><span class="ctycnt">'+' <span class="pull-right" style="font-weight:600;">City</span></span>') )
      .appendTo( ul );
    }else{
      return $( "<li>" )
      //.addClass(item.category)
      .attr( "data-value", item.value )     
      .append( $( "<a>" ).html('<i class="fa fa-bed wd20"></i> <span class="areanme">'+item.label + '</span><span class="ctycnt">'+' <span class="pull-right" style="font-weight:600;">Hotel</span></span>') )
      .appendTo( ul );
    }
  }
});
	$( "#hotel_autocomplete" ).catcomplete({
		delay: 0,
		minLength: 3,
		source:'<?php echo ASSETS;?>hotel/get_hotel_city_suggestions',
		select: function(event, ui){
		  $('#CityID').val(ui.item.CityId);
		  $('#HotelId').val(ui.item.HotelCode);
		  $('#SearchType').val(ui.item.category);
		}
    });
    $( "#check-in" ).datepicker({
	  minDate: 0,
	  //dateFormat: 'yy-mm-dd',
	  dateFormat: 'dd-mm-yy',
	  maxDate: "+1y",
	  numberOfMonths: 2,
	  onChange : function(){
	  },
	  onClose: function( selectedDate ) {
		$( "#check-out" ).datepicker( "option", "minDate", selectedDate );
		//var type = $("#trip_type").val();
		//console.log(type);
		$( '#check-out' ).focus();
	  }
	});
	$( "#check-out" ).datepicker({
	  minDate: 0,
	  //dateFormat: 'yy-mm-dd',
	  dateFormat: 'dd-mm-yy',
	  maxDate: "+1y",
	  numberOfMonths: 2,
	  onClose: function( selectedDate ) {
		$( "#check-in" ).datepicker( "option", "maxDate", selectedDate );
	  }
	});
	</script>
</body>
</html>
