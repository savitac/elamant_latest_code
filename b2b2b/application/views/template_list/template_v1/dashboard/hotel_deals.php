
<div class="clearfix"></div>

<!--/popular hotels-->

<div class="popularhotel">
	<div class="container">
    	<div class="staffarea">
        <div class="pagehed"><?php echo $this->thechinagap['HomeDashboard']['MostPopularHotels']; ?></div>
        <div class="clearfix"></div>
         
        <ul data-dropdown-out="fadeOutUp" data-dropdown-in="fadeInDown"  class="nav nav-tabs customdeals">
         <?php  $userid = $this->session->userdata('user_details_id');
                $cities = $this->Dashboard_Model->get_hotelcities($userid);
               foreach($cities as $cities){ ?>
            <li> <a data-toggle="tab" href="#<?php echo $cities->best_deals_id; ?>"><?php echo $cities->country; ?></a> </li>
           
          <?php } ?>   
        </ul>
          
        <div class="destslider tab-content">
        	
            <?php $userid = $this->session->userdata('user_details_id');
                  $cityid = $this->Dashboard_Model->get_hotelcities($userid); 
                  $k = 0; foreach($cityid as $cityid){ ?>
            <div id="<?php echo $cityid->best_deals_id; ?>" class="tab-pane <?php if($k == 0){ ?> active <?php } ?>">
          <div id="owl-demo4<?php echo $k; ?>" class="owl-carousel owlindex2 owl-theme">
         
           <?php $citydeals = $this->Dashboard_Model->get_hoteldeals($cityid->best_deals_id);
                foreach($citydeals as $citydeals) { 
              
                  $exp_date = $citydeals>exp_date;
                  $current_date = date("Y-m-d");
                  $date_diff = strtotime($exp_date) - $current_date;
                  $diff_in_days = floor($date_diff/86400);
                  
                  if(true){ 
				 ?>         
              <div class="item">
                <div class="outerfullfuture">
                  <div class="ourdest"><img src="<?php echo ASSETS;?>cpanel/uploads/hotels/<?php echo $citydeals->hotel_image; ?>" alt="" /> 
                 </div>
                    <div class="tripcontent">
                      
                      <div class="rgtplace"><?php echo $citydeals->hotel_name; ?></div>
                        <div class="bluplace"><?php echo $citydeals->city_name; ?></div>
                        <div class="clearfix"></div>
                        <div class="col-md-8 nopad">
                        <div data-star="<?php echo $citydeals->star; ?>" class="stra_hotel"> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> </div>
                      </div>
                        <div class="col-md-4 nopad">
                          <span class="tileprice">$ <?php echo $citydeals->price_offer; ?></span>
                        </div>
                    </div>
                    
                </div>
              </div>
              
             <?php } } ?>
            </div>
           </div>
       
         <script>
              $(document).ready(function(){
                   $("#owl-demo4<?php echo $k; ?>").owlCarousel({
                    items : 4, 
                    itemsDesktop : [1000,3],
                    itemsDesktopSmall : [900,3], 
                    itemsTablet: [768,2], 
                    itemsMobile : [479,1], 
                    navigation : true,
                    pagination : true
                  });
                });
            </script>
         <?php $k++; } ?>
       
    </div>

</div>
</div>
</div>
