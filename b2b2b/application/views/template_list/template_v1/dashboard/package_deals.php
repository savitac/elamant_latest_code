
<div class="clearfix"></div>
 <?php
      if(isset($_SESSION['TheChinaGap']['language'])){ 
        $display_language = $_SESSION['TheChinaGap']['language'];
      } else {
        $display_language = $_SESSION['TheChinaGap']['language'] = BASE_LANGUAGE;
      }
?>

<div class="perfect_holiday">
	<div class="container">
    	<div class="staffarea">
        <div class="pagehed"><?php echo $this->thechinagap['HomeDashboard']['TopPackages']; ?></div>
        <div class="clearfix"></div>
        <div class="destslider">
        	<div id="owl-demo5" class="owl-carousel owlindex2 owl-theme">
            <?php
                $userid = $this->session->userdata('user_details_id');
                 $travel = $this->Dashboard_Model->get_travel_destination($userid);
                foreach($travel as $travel_details){
                 
                  $date_diff = $travel_details->exp_date;
                  if($date_diff >= date("Y-m-d")){
                  
                    $city = '&city='.$travel_details->city;
                    $hotel_checkin = '&hotel_checkin='.date("d/m/Y",strtotime($travel_details->checkin_date));
                    
                    $hotel_checkout = '&hotel_checkout='.date("d/m/Y",strtotime($travel_details->checkout_date));
                    
                    $adults = '&adults[]='.'1';
                    $children = '&children[]='.'0';
                   
                    $rooms = '&rooms='.'1';
                    $query = $city.$hotel_checkin.$hotel_checkout.$rooms.$adults.$children;
                    $star_images = $hotel_deals->star;
                ?>
              <div class="item">
              	<div class="outerfullfuture">
                	<div class="ourdest"><img src="<?php echo ASSETS; ?>cpanel/uploads/traveldestination/<?php echo $travel_details->city_image; ?>" alt="" /> 
                 </div>
                    <div class="tripcontent">
                    	
                    	<div class="rgtplace">
                         <?php 
                          if($display_language=='english') $package_name = $travel_details->package_name; 
                          else $package_name = $travel_details->package_name_china; ?>
                        <?php echo $package_name; ?>
                      </div>
                        <div class="clearfix"></div>
                        <div class="col-md-8 nopad">
                        <div class="bluplace"><?php echo $travel_details->city; ?></div>
                        </div>
                        <div class="col-md-4 nopad">
                        	<span class="tileprice">$ <?php echo $travel_details->price; ?></span>
                        </div>
                    </div>
                    
                </div>
              </div>
              
              <?php } }?>
           </div>
        </div>
        </div>
    </div>
</div>
