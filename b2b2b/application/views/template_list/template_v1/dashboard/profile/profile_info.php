<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo $this->session->userdata('company_name'); ?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet">
	<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	
	<?php echo $this->load->view('dashboard/top'); ?>	
  <?php // $this->load->view('dashboard/left_menu'); ?>

	
<section id="main-content">
  
  <section class="wrapper">

    
        	<div id="viewpro" class="main-chart"> 

              <div class="tab_inside_profile">
              	<span class="profile_head"><?php echo $this->TravelLights['Editprofile']['Profile']; ?> </span>
              	<div class="rowit">
                <div class="fstusrp"> 
                  <img class="profile_photo" alt="" src="<?php echo ASSETS.'cpanel/uploads/agent/'.$user_info[0]->user_profile_pic; ?>"> 
                </div>
                
                <div class="adres_user">
                <div class="inside_left">
					
                	<span class="lred"><?php echo $user_info[0]->user_name; ?></span>
                    <p class=""><?php echo $user_info[0]->address; ?>,<br>  <?php echo $user_info[0]->city_name; ?>, <br> <?php echo $user_info[0]->state_name; ?><br> <?php echo $user_info[0]->zip_code; ?>  <br> <?php echo $user_info[0]->country_name; ?>.</p>
                   
				 </div>
                 
                <div class="clearfix"></div>
                
                <a class="dashbuttons extr_profile" id="edit_show"><?php echo $this->TravelLights['Editprofile']['EditProfile']; ?></a>
                
                </div>
                </div>
                
              </div>
              
            </div>
      
        <div id="edit_profil" style="display:none">
        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 right_side_dash profileplace">
      <div class="dash_inside">
        <div class="cntbl"> 
          <!-- <a class="profiledash"> 
            <img class="profile_photo" alt="" src="<?php echo ASSETS.'cpanel/uploads/agent/'.$user_info[0]->user_profile_pic; ?>">
          <div class="overlaychange"> <span class="chngers">Change Photo</span>
            
              <input type="file" value="Upload Photo" id="profilePhoto" class="rschange">
              
          </div>
          </a>  -->
<form id="myForm" action="<?php echo base_url(); ?>account/update_user_profile_image_b2b2b" method="POST" enctype="multipart/form-data" >
          <div class="profiledash">
            <img class="profile_photo" alt="" src="<?php echo ASSETS;?>cpanel/uploads/agent/<?php echo $user_info[0]->user_profile_pic; ?>" id="profile_photo">
             
            <div class="overlaychange">
                <span class="chngers">Change Image</span>
                    <input type="file" required="required" value="Upload Photo" id="profilePhoto" name="profilePhoto" class="rschange">                              
            </div>

            
        </div>
        <div class="clearfix"></div>
        <button type="submit" class="dashbuttons extr_profile">Update Profile Picture</button>
        </div>
        </form>
      </div>
    </div>
         <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 main-chart">
      	<div style="display:none;" class="editmsg"><?php echo $this->TravelLights['Editprofile']['changehassavedsuccessfully']; ?></div>
      <div class="tab_inside_profile"> 
      <span class="profile_head"><?php echo $this->TravelLights['Editprofile']['EditProfile']; ?></span>
      
       <form name="updateProfile" id="updateProfile" method="post" action="<?php echo base_url()."usermanagement/updateProfile"; ?>" novalidate >  
                <div class="rowit">
                  <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Editprofile']['AgentName']; ?>: </div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles">
                    <input type="text" name="user_name" readonly id="user_name"  value="<?php echo $user_info[0]->user_name; ?>"   class="payinput" required >
                    <span id="name-error" class="error" for="user_name"> </span>
                  </div>
                  <input type="hidden" name="address_details_id" id="address_details_id" value="<?php echo $user_info[0]->address_details_id; ?>" required>
            
                   

                  <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Editprofile']['AgentId']; ?> : </div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles">
                    <input type="text" name="agent_id" id="agent_id" value="<?php echo $user_info[0]->user_account_number; ?>"  class="payinput" readonly required> 
                    <span id="agentid-error" class="error" for="agent_id"> </span>
                  </div>
                  
                  <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Editprofile']['Address']; ?> :</div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles">
                     <input type="text" name="address" id="address" value="<?php if($user_info[0]->address != 'XXX') { echo $user_info[0]->address; } ?>"    class="payinput" required>
                     <span id="address-error" class="error" for="address"> </span>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Editprofile']['City']; ?> : </div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles">
                     <input type="text" name="city" id="city" value="<?php if($user_info[0]->city_name != 'XXX') { echo $user_info[0]->city_name; } ?>"   class="payinput" required>
                     <span id="city-error" class="error" for="city"> </span>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Editprofile']['State']; ?> : </div>
                  </div>
                 <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles">
                     <input type="text" name="state" id="state" value="<?php if($user_info[0]->state_name != 'XXX') { echo $user_info[0]->state_name; } ?>"  class="payinput" required>
                     <span id="state-error" class="error" for="state"> </span>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Editprofile']['ZipCode']; ?> : </div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles">
                   <input type="text" name="postal_code" id="postal_code" value="<?php  echo $user_info[0]->zip_code;  ?>"  class="payinput" required>
                   <span id="zip-error" class="error" for="postal_code"> </span>
                  </div>
                 
                  <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Editprofile']['Country']; ?> </div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles">
                  	<div class="selectedwrap inboxs">
                    <select name="country_id" id="country_id" class="payinput payinselect mySelectBoxClass hasCustomSelect"  required>
                        <?php for($country=0; $country < count($nationality_countries); $country++) { ?> 
                                        <option value ="<?php echo $nationality_countries[$country]->country_id; ?>" <?php if($nationality_countries[$country]->country_id ==  $user_info[0]->country_id){ echo "selected"; } ?> > <?php echo $nationality_countries[$country]->country_name; ?></option>
                                        <?php } ?>
                      </select>
                     <span id="country-error" class="error" for="name"> </span>
                      </div>
                  </div>
                  
                  <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Editprofile']['EmailAddress']; ?> :</div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles"> 
                  	<input type="text" name="email" readonly  id="email" value="<?php  echo $user_info[0]->user_email;  ?>"  class="payinput email" required>
                  	<span id="email-error" class="error" for="name"> </span>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Editprofile']['PhoneNumber']; ?></div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles">
                    <input type="text" name="mobile_no" id="mobile_no" value="<?php  echo $user_info[0]->user_cell_phone;  ?>"  class="payinput" required>
                    <span id="contact-error" class="error" for="name"> </span>
                  </div>
                   <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Editprofile']['CompanyName']; ?> : </div>
                  </div>
                 <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles">
                     <input type="text" name="company_name" id="company_name" value="<?php if($user_info[0]->company_name != 'XXX') { echo $user_info[0]->company_name; } ?>"  class="payinput" required>
                     <span id="company_name-error" class="error" for="company_name"> </span>
                  </div>
                  
                  <div class="col-lg-3 col-md-3 col-sm-12 fullprofiles">
                    <div class="prolabel"><?php echo $this->TravelLights['Register']['CompanyPhone']; ?> : </div>
                  </div>
                 <div class="col-lg-9 col-md-9 col-sm-12 margbotm15 fullprofiles">
                     <input type="text" name="companyphone" id="companyphone" value="<?php if($user_info[0]->user_home_phone != 'XXX') { echo $user_info[0]->user_home_phone; } ?>"  class="payinput" required>
                     <span id="companyphone-error" class="error" for="companyphone"> </span>
                  </div>
                 
                  <div class="col-sm-12">
                  
                    <button type="submit" class="dashbuttons extr_profile"><?php echo $this->TravelLights['Editprofile']['UpdateProfile']; ?></button>
					<a id="back_list" class="chnge_paswordd"><?php echo $this->TravelLights['Editprofile']['Cancel']; ?></a>
                   </div>
                  
                </div>
              
              
            </div>
    </div>
    
    
    
    </div>
    
    
  </section>

</section>

<?php echo $this->load->view('core/bottom_footer'); ?> 



<div class="clearfix"></div>
	<?php //echo $this->load->view('core/bottom_footer'); ?>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 


<script type="text/javascript">
    $(document).ready(function(e)
    {
      $("#profilePhoto").on('change',function(e) { 

         e.stopPropagation();
        e.preventDefault();

    var data = new FormData($('#myForm'));

     files = e.target.files;

    $.each(files, function(key, value)
    {
        data.append(key, value);
    });

    console.log(data);
    $.ajax({
        url: "<?php echo base_url();?>account/update_user_profile_image",
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR)
        {
            if(typeof data.error === 'undefined')
            {
                // Success so call function to process the form
                submitForm(event, data);
            }
            else
            {
                // Handle errors here
                console.log('ERRORS: ' + data.error);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
        }
    });

  });
});
  </script>
<script type="text/javascript">
$(document).ready(function(){

    $('#example').DataTable({responsive:true});
	
	$('#edit_show').click(function(){
		$('#viewpro').fadeOut(500, function(){
				$('#edit_profil').fadeIn();
			});
	});
	
	$('#back_list').click(function(){
		$('#edit_profil').fadeOut(500, function(){
				$('#viewpro').fadeIn();
			});
	});
	
	$('.dash_menus').click(function(){
		$('.sidebar_wrap').toggleClass('open');
	});
	
	$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
	});
	
	
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	
});

</script>


</body>
</html>
