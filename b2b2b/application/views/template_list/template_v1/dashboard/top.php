  <?php 
  if($this->session->userdata('user_details_id') == "") {
    redirect("home"); 
  } ?>
 
  <?php if($this->session->userdata('user_details_id') != ''){
    $CI =& get_instance(); 
    $CI->load->model('General_Model');
    @$user_details = $CI->General_Model->getUserDetails($this->session->userdata('user_details_id'))->row();
     @$specific_notice_details = $CI->General_Model->getnoticeDetails($this->session->userdata('user_details_id'))->row();
    if($specific_notice_details) 
    {
      $notice_details=$specific_notice_details; 
    }  
    else
    {
      @$general_notice_details = $CI->General_Model->getnoticeDetails("FOR_ALL")->row();
      $notice_details=$general_notice_details;  
    } 
  } ?>

<?php if(isset($_SESSION['currency'])){
          $this->display_currency = $_SESSION['currency'];
        } else {
          $this->display_currency = $_SESSION['currency'] = BASE_CURRENCY;
          $currency_det =$CI->General_Model->getCurrencyList(BASE_CURRENCY); 
          $_SESSION['currency_value'] = $currency_det[0]->value; 
        }
        $country_currency   = $CI->General_Model->getCurrencyList(); 
        //print_r($_SESSION);
?> 
  
<div class="topsseclogon">
 
      <div class="container-fluid module_header nopad">
        <div class="col-md-12 text-center nopad">
       <div class="alert alert-warning alert-dismissible show" style="background-color: #198C8C;color: #fff;position:relative; z-index:10" role="alert">
          <strong><?php echo $notice_details->message ?></strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
         </div>
      </div>
   <div class="clearfix"></div>
    <div class="secondtop">
      <div class="module_header">
        <button class="in_page_menu" id="showLeft">
          <span class="fa fa-bars"></span>
        </button>
          <?php if($CI->session->userdata('site_name') == ''){ ?>  
        <a href="<?php echo base_url()."dashboard"; ?>" class="logo"><img alt="<?= $this->session->userdata('company_name')?>" src="<?php echo ASSETS.'assets/images/logo.png'; ?>"> </a> 
         <?php } else {
          if($this->session->userdata('user_type')==2)
          { ?>
             <a href="<?php echo base_url()."dashboard"; ?>" class="logo"><img alt="Travel Lights" src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $CI->session->userdata('domain_logo'); ?>"></a> 
          <? }
          elseif ($this->session->userdata('user_type') == 5) {?>
             <a href="<?php echo base_url()."dashboard"; ?>" class="logo"><img alt="Travel Lights" src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $CI->session->userdata('domain_logo'); ?>"></a>   
          <?}
          else
          { ?>
            <a href="<?php echo base_url()."dashboard"; ?>" class="logo"><img alt="Travel Lights" src="<?php echo ASSETS; ?>cpanel/uploads/agent/<?php echo $CI->session->userdata('domain_logo'); ?>"></a>   
           
   
    <?php } } ?> 
         <ul data-dropdown-out="fadeOutUp" data-dropdown-in="fadeInDown" class="nav nav-pills ritsude">


   
   <?php if($CI->session->userdata('user_type') != 5){ ?>
        <li class="dropdown">
        <span class="header_top_bal">Balance
         <span class="tot_balance">
          <?php $balance = $CI->General_Model->getAgentBalance()->row(); ?>
      <?php
                  if(isset($balance->balance_credit)){
                           echo $CI->display_currency='USD'.' '. number_format($balance->balance_credit); 
                  }else { 
                                 echo "0.00";
                        } 
            ?>
       
         </span>
         </span>
        </li>
       <?php } ?>
          
        <li class="dropdown login_after" id="login_signup"> <a data-toggle="dropdown" class="topa dropdown-toggle nomargin" href="#">
          <div class="reglog caretdr">
            <div class="userimage user_profimag"><img alt="" src="<?php echo ASSETS.'cpanel/uploads/agent/'.$user_details->user_profile_pic; ?>"></div>
            <b class="fa fa-chevron-down cartdown"></b>
            <div class="userorlogin"><?php echo $user_details->user_name; ?></div>
          </div>
          </a>
          <ul class="dropdown-menu mysign1 dashmenus1">
            <li><a href="<?php echo base_url().'usermanagement/editProfile'; ?>"><i class="fa fa-user"></i> <span class="name_currency">Profile Information</span></a></li>
            <li><a href="<?php echo base_url().'dashboard'; ?>"><i class="fa fa-user"></i> <span class="name_currency">Dashboard</span></a></li>
            <li><a href="<?php echo base_url().'/booking/flightOrders'; ?>"><i class="fa fa-book"></i><span class="name_currency">Bookings</span></a></li>
              <?php if($CI->session->userdata('user_type') != 5){ ?>
            <li><a href="<?php echo base_url().'settings'; ?>"><i class="fa fa-cog"></i><span class="name_currency">Settings</span></a></li>
             <?php } ?>
            <li><a href="<?php echo base_url().'support'; ?>"><i class="fa fa-comments-o"></i><span class="name_currency">Support</span></a></li>
            <li class="log_out_li"><a href="<?php echo base_url().'account/logout'; ?>">Logout</a></li>
          </ul>
        </li>
      </ul>
              <div class="ritsude ds_crncy">
            <div class="sidebtn flagss">
                                    <a class="topa dropdown-toggle" data-toggle="dropdown">
                                        <div class="reglognorml">
                                            <div class="flag_images">
                                                <?php
                                                $curr = get_application_currency_preference();

                                                echo '<span class="curncy_img sprte ' . strtolower($curr) . '"></span>'
                                                ?>
                                            </div>
                                            <div class="flags">
                                                <?php
                                                echo $curr;
                                                ?>
                                            </div>
                                            <b class="caret cartdown"></b>
                                        </div>
                                    </a>
                                    <ul class="dropdown-menu exploreul explorecntry logdowndiv">
                                        <?= $this->load->view('utilities/multi_currency') ?>
                                    </ul>
                                </div>
          </div> 
      <div class="reglognorml">
        <div class="flags"><?php echo @$CI->display_currency; ?> <b class="fa fa-chevron-down cartdown"></b></div>
      </div>
      <ul id="nav-accordion" class="sidebar-menu">
      <?php $homepage = $this->Dashboard_Model->get_dashboardmenu(); 
               $i=0;
      foreach($homepage as $homepage){ 

        $previlege = explode(',',$homepage->user_types);
        if(in_array($this->session->userdata('user_type'), $previlege)){
        // Dynamic menu for CRS is temporary I will update this later @ajaz
      ?>
        <li class="sub-menu dcjq-parent-li <?php if($homepage->dashboard_name =='Support'|| $homepage->dashboard_name =='Agency Settings') { echo " hide ";  } ?>" > 
          <a  href="<?php echo base_url(); ?><?php echo $homepage->dashboard_url; ?>" class="dcjq-parent <?php if($i ==0) { ?> active <?php }?>">
            <i class="fa fa-<?php echo $homepage->dashboard_icon; ?>"></i>
            <span>
              <?php $dashboardname = $homepage->dashboard_name; ?>
              <?php echo $dashboardname; ?>
            </span>
          </a><!-- 
                  <?php if($i==7){?>
                  <ul class="dropdown-menu">
                                        <li>
                                            <a href="/suppliermenu">Manage Booking</a>
                                            <a href="#">Sales Report</a>
                                            <a href="#">Flight Report</a>
                                            <a href="#">Flight Crs List</a>
                                            <a href="#">Airline's List</a>
                                            <a href="#">Add New Flightt</a>
                                        </li>
                                    </ul>
                                    <?php }?> -->
        </li>
                  
        <?php $i = $i+1; } } ?>
      </ul>
      </div>
    </div>
</div>
<div class="clearfix"></div>

 
