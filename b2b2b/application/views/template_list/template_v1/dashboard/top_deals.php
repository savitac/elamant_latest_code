<div class="clearfix"></div>
<div class="perfect_holiday">
  <?php
      if(isset($_SESSION['TheChinaGap']['language'])){ 
        $display_language = $_SESSION['TheChinaGap']['language'];
      } else {
        $display_language = $_SESSION['TheChinaGap']['language'] = BASE_LANGUAGE;
      }
?>
	<div class="container">
    	<div class="staffarea">
        <div class="pagehed"><?php echo $this->TravelLights['HomeDashboard']['TodayTopDeals']; ?></div>
        <div class="clearfix"></div>
        <div class="destslider">
        	<div id="owl-demo3" class="owl-carousel owlindex2 owl-theme">
		 <?php  for($deals =0; $deals < count($top_deals); $deals++) { 
                  $exp_date = $top_deals[$deals]->exp_date;
                  $current_date = date("Y-m-d");
                  $date_diff = strtotime($exp_date) - $current_date;
                  $diff_in_days = floor($date_diff/86400);
                  
                  if($diff_in_days >= 0){ ?>
					
              <div class="item">
				
              	<div class="outerfullfuture">
                	<div class="ourdest"><img src="<?php echo base_url().'cpanel/uploads/last_minut_deals/'.$top_deals[$deals]->hotel_image; ?>" alt="" /> 
                    <div class="delas1">  <span class="rsmall">
                      <?php 
                          if($display_language=='english') $topdeal = $top_deals[$deals]->name; 
                          else $topdeal = $top_deals[$deals]->name_china; ?>
                      <?php echo $topdeal; ?>  
                    </span>
               </div>
                
                 </div>
                    <div class="tripcontent">
                    	<div class="rgtplace"><?php echo $top_deals[$deals]->hotel_name; ?></div>
                      <div class="bluplace"><?php echo $top_deals[$deals]->city_name; ?></div>
                        <div class="bluprice">
                          <?php 
                          if($display_language=='english') $topdeal_desc = $top_deals[$deals]->description; 
                          else $topdeal_desc = $top_deals[$deals]->description_china; ?>
                          <?php echo $topdeal_desc; ?>
                        </div>
                    </div>
                    
                </div>
              </div>
              
              <?php } }?>
              </div>
           </div>
        </div>
        </div>
    </div>
</div>
