<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>Chinagap</title>
<?php echo $this->load->view('core/load_css'); ?>
</head>

<body>
<?php
      if(isset($_SESSION['TravelLights']['language'])){ 
        $display_language = $_SESSION['TravelLights']['language'];
      } else {
        $display_language = $_SESSION['TravelLights']['language'] = BASE_LANGUAGE;
      } 
?>

<?php echo $this->load->view('dashboard/top'); ?> 
<div class="clearfix"></div>

<div class="top80 static_pages dashtopmargin">
  <div class="container">
    <div class="top_static">Contact Us</div>
  </div>
  
  <div class="container">
  	<div class="col-xs-12 nopad full_tab">
      <div class="inercols_contact">
        <div class="col-xs-12 col-sm-6 nopad">
          <form name="contact" id="contact"  method="post" action="<?php echo ASSETS."dashboard/createContact"; ?>">
            <div class="inside_form">
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="name" id="name" placeholder="Name" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="position" id="position" placeholder="Position" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="company" id="company" placeholder="Company" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="wechat" id="wechat" placeholder="WeChat" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="qq" id="qq" placeholder="QQ" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="email" class="conform" name="email" id="email" placeholder="Email ID" required>
              </div>
              <div class="col-xs-12 padmar">
                <input type="text" class="conform" name="subject" id="subject" placeholder="Subject" required>
              </div>
              <div class="col-xs-12 padmar">
                <textarea class="areatxt" name="message" id="message" placeholder="Message" required></textarea>
              </div>
              
              <div class="col-xs-12 padmar">
                <input type="submit" class="btn btn-myclr" value="Submit Your Message">
              </div>
            </div>
          </form>
        </div>
        <div class="col-xs-12 col-sm-6 nopad">
          <div class="set_contact">
              <?php for($contacts =0; $contacts < count($contact); $contacts++) { ?>
              <div class="row_contacts"> 
              <div class="line_desctn"><strong> <?php echo $contact[$contacts]->city; ?></strong></div>
            </div>
            <div class="row_contacts"> <span class="icon_contact fa fa-map-marker"></span>
              <div class="line_desctn">
                <?php if($display_language=='english') $address = $contact[$contacts]->address; 
                  else $address = $contact[$contacts]->address_china; ?>
              <?php echo $address; ?>
</div>
            </div>
            
            <div class="row_contacts"> <span class="icon_contact fa fa-phone"></span>
              <div class="line_desctn"><?php echo $contact[$contacts]->contact; ?></div>
            </div>
            <?php } ?>
          
            <div class="row_contacts"> 
              <div class="line_desctn"><strong> General Enquiries</strong></div>
            </div>
            <div class="row_contacts"> <span class="icon_contact fa fa-envelope"></span>
              <div class="line_desctn">  info@thechinagap.com</div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>

</div>


<div class="clearfix"></div>
<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 

<script type="text/javascript">
$(document).ready(function(){
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	 
	 
	 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	 
	 
	
});
</script> 





</body>
</html>
