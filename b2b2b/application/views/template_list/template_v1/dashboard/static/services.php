<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>Chinagap</title>
<?php echo $this->load->view('core/load_css'); ?>
</head>

<body>
<?php echo $this->load->view('dashboard/top'); ?> 
<?php
      if(isset($_SESSION['TravelLights']['language'])){ 
        $display_language = $_SESSION['TravelLights']['language'];
      } else {
        $display_language = $_SESSION['TravelLights']['language'] = BASE_LANGUAGE;
      } 
?>
<div class="clearfix"></div>

<div class="agent_login_wrap top80 dashtopmargin">
		
   		<div class="container">
  		 <div class="static_page_in">
          	<h1>
              <?php if($display_language=='english') $services_name = $services[0]->menu_name; 
                  else $services_name = $services[0]->menu_name_china; ?>
              <?php echo $services_name; ?>
            </h1>

            
    <?php for($service =0; $service < count($services); $service++) { ?>
            	<div class="repetprod">
            	<div class="col-xs-3 padrgt1">

                	<img src="<?php echo  site_url().'cpanel/uploads/header/' ?><?php echo $services[$service]->service_image; ?>" alt="" />
                </div>
                <div class="col-xs-9 nopad produconts">
              		
                  <h3>
                     <?php  if($display_language=='english') $servicesname = $services[$service]->service_name; 
                  else $servicesname = $services[$service]->service_name_china; ?>
                    <?php echo $servicesname; ?>
                  </h3>
                  <p>
                    <?php   if($display_language=='english') $service_description = $services[$service]->service_description; 
                  else $service_description = $services[$service]->service_description_china; ?>
                    <?php echo $service_description; ?>
                  </p>
                 </div>
                 </div>
                
              <?php } ?> 
                 
          </div>
          
          
          
        </div>
   
</div>


<div class="clearfix"></div>
<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 

<script type="text/javascript">
$(document).ready(function(){
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	 
	 
	 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	 
	 
	
});
</script> 





</body>
</html>
