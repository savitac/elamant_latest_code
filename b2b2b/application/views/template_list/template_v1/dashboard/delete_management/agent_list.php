
 <table id="example1" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
                            <th><?php echo $this->TravelLights['UserManagementTable']['Slno']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['Name']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['UserType']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['Email']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['AccountNo']; ?></th>
						    <th><?php echo $this->TravelLights['UserManagementTable']['MobileNo']; ?></th>
						    <th><?php echo $this->TravelLights['UserManagementTable']['CompanyName']; ?></th>
                           <th><?php echo $this->TravelLights['UserManagementTable']['Status']; ?></th>
                            <th><?php echo $this->TravelLights['UserManagementTable']['Actions']; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
            				 <th><?php echo $this->TravelLights['UserManagementTable']['Slno']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['Name']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['UserType']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['Email']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['AccountNo']; ?></th>
						    <th><?php echo $this->TravelLights['UserManagementTable']['MobileNo']; ?></th>
						    <th><?php echo $this->TravelLights['UserManagementTable']['CompanyName']; ?></th>
                           <th><?php echo $this->TravelLights['UserManagementTable']['Status']; ?></th>
                            <th><?php echo $this->TravelLights['UserManagementTable']['Actions']; ?></th>
            </tr>
        </tfoot>
        <tbody >
           <?php for($a=0;$a<count($agentusers);$a++){ ?>
						<tr>
							<td><?php echo ($a+1); ?></td>
							<td><?php echo $agentusers[$a]->user_name; ?></td>
							<td><?php echo "B2B2B"; ?></td>
							<td><?php echo $agentusers[$a]->user_email; ?></td>
							<td><?php echo $agentusers[$a]->user_account_number; ?></td>
							<td><?php echo $agentusers[$a]->user_cell_phone; ?></td>
							<td><?php echo $agentusers[$a]->company_name; ?></td>
                            <td>
								<?php if($agentusers[$a]->user_status == "ACTIVE"){  
									 echo "ACTIVE"; 
								 }else {
									  echo "INACTIVE";
								 } ?>
							</td>
							<td class="center">
								
								<a class="btn btn-primary btn-xs" onclick="editPrivilege('<?php echo  base_url()."usermanagement/getUserPrivileges/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>')"  > <?php echo $this->TravelLights['UserManagementTable']['Privilege']; ?>  </a>
							   
							   <a class="btn btn-danger btn-xs" onclick="editproduct('<?php echo  base_url()."usermanagement/getUserProducts/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>')"  > <?php echo $this->TravelLights['UserManagementTable']['ManageProducts']; ?> </a>
							
							
							 <a class="btn btn-info btn-xs" onclick="editAgent('<?php echo  base_url()."usermanagement/editAdminagent/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>')"  > <?php echo $this->TravelLights['UserManagementTable']['Edit']; ?> </a>
							 <?php if($agentusers[$a]->user_status == "ACTIVE"){ ?> 
								   <a class="btn btn-warning btn-xs" href="<?php echo  base_url()."usermanagement/inactive_b2b_users/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>" onclick="return confirm('Are you want Inactivate?')"> <?php echo $this->TravelLights['UserManagementTable']['InActive']; ?> </a>
							 <?php } else { ?>
								   <a class="btn btn-success btn-xs" href="<?php echo  base_url()."usermanagement/active_b2b_users/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>" onclick="return confirm('Are you want activate?')"> <?php echo $this->TravelLights['UserManagementTable']['Active']; ?> </a>
							 <?php } ?>	
							 <a href="<?php echo site_url()."usermanagement/depositList/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>"><button type="button" class="btn btn-red tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" data-original-title="Agent Deposit"><i class="glyphicon glyphicon-tower"></i><?php echo $this->TravelLights['UserManagementTable']['SubAgentDeposit']; ?></button></a>				
							 </td>
						</tr>
		<?php } ?>	
        </tbody>
    </table>
    <script>
    $(document).ready(function(){

    $('#example1').DataTable({responsive: true});
    
});

</script>
