    <form id="edit_markup" name="edit_markup" class="validate form-horizontal" method="post" action="<?php echo base_url().'usermanagement/updateMarkup/'.base64_encode(json_encode($agent_markups[0]->markup_details_id)); ?>" novalidate="novalidate">   
                                  
                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['MarkupType']; ?> </div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
									  <select name="markup_type" id="markup_type1" class="payinselect mySelectBoxClassfortab hasCustomSelect"  required >
                                        <option value="GENERAL" <?php if($agent_markups[0]->markup_type == "GENERAL") { echo "selected"; } ?>><?php echo $this->TravelLights['AcencySettings']['GENERAL']; ?></option>
                                        <option value="SPECIFIC" <?php if($agent_markups[0]->markup_type == "SPECIFIC") { echo "selected"; } ?>> <?php echo $this->TravelLights['AcencySettings']['SPECIFIC']; ?></option>
                                   </select>
                                    <span id="markup-type-error" class="error" for="user_name"> </span>
                                  </div>
                            </div>
                        </div>
                        
                       <!--  <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['Api']; ?>  </div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="api_id" value="api_id" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
									   <?php for($ap=0; $ap < count($api); $ap++) { ?> 
                                        <option value ="<?php echo $api[$ap]->api_details_id; ?>" <?php if($api[$ap]->api_details_id == $agent_markups[0]->api_details_id){ echo "selected"; } ?> > <?php echo $api[$ap]->api_name." (".$api[$ap]->product_name.")"; ?></option>
                                        <?php } ?>
                                </select>
                                 <span id="product-id-error" class="error" for="product_id"> </span>
                                 </div>
                            </div>
                        </div> -->
                        
                       
                        
                         <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['Product']; ?><?php //echo $agent_markups[0]->product_details_id; ?> </div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="product_id" value="product_id" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
									   <?php for($prod=0; $prod < count($products); $prod++) { ?> 
                                        <option value ="<?php echo $products[$prod]->product_details_id; ?>" <?php if($products[$prod]->product_details_id == $agent_markups[0]->product_details_id){ echo "selected"; } ?> > <?php echo $products[$prod]->product_name; ?></option>
                                        <?php } ?>
                                </select>
                                 <span id="product-id-error" class="error" for="product_id"> </span>
                                 </div>
                            </div>
                        </div>
                        
                           <div class="likrticktsec" id="user_t1" <?php if($agent_markups[0]->markup_type == "GENERAL"){ ?> style="display:none;" <?php } ?> >
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['UserType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                  <select name="user_type" id="user_type"class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value="4">B2B2B </option>
								 </select>
                              </div>
                        </div>
                        
                        
                         <div class="likrticktsec" id="agent_n" >
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['AgentName']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                  <select name="sub_agent_id" id="sub_agent_id"class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value="0">ALL </option>
									  <?php if($agentadmin_list!=''){ for($a=0;$a<count($agentadmin_list);$a++){ ?>
                                        <option value="<?php echo $agentadmin_list[$a]->user_details_id; ?>" <?php if($agent_markups[0]->user_details_id == $agentadmin_list[$a]->user_details_id){ echo "selected"; } ?> ><?php echo $agentadmin_list[$a]->user_name; ?></option>
                                       <?php  } } ?>
                                </select>
                                 <span id="markup-type-error" class="error" for="sub_agent_id"> </span>
                                 </div>
                            </div>
                        </div>
                        
                         
                        
                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['Country']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="country_code" id="country_code"  value="product_id" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									<option value=""><?php echo $this->TravelLights['EditB2BMarkup']['SelectCountry']; ?></option>
									   <?php for($country=0; $country < count($nationality_countries); $country++)  { ?> 
                                        <option value ="<?php echo $nationality_countries[$country]->country_id; ?>" <?php if($agent_markups[0]->country_id == $nationality_countries[$country]->country_id){ echo "selected"; } ?> > <?php echo $nationality_countries[$country]->country_name; ?></option>
                                        <?php } ?>
                                </select>
                                <span id="country-code-error" class="error" for="country_code"> </span>
                                 </div>
                            </div>
                        </div>
                        
                        <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['MarkupDateRange']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly value="<?php if($agent_markups[0]->markup_start_date != '0000-00-00'){ echo date('d-m-Y', strtotime($agent_markups[0]->markup_start_date)); } ?>" name="start_date" id="datepicker111" placeholder="Markup Start Date" />
                                 </div>
                               
                                 <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly name="end_date" value="<?php if($agent_markups[0]->markup_end_date != '0000-00-00'){ echo date('d-m-Y', strtotime($agent_markups[0]->markup_end_date)); } ?>" id="datepicker222" placeholder="Markup End Date"   />
                                 </div>
                                 </div>
                           
                        </div>
                        <div class="likrticktsec" id="expry_date">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['MarkupExpiryDate']; ?></div> 
                                
                                <div class="width80cnt">
                                <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly value="<?php if($agent_markups[0]->markup_expiry_date != '0000-00-00'){ echo date('d-m-Y', strtotime($agent_markups[0]->markup_expiry_date)); } ?>" name="expiry_date" id="datepicker333" placeholder="Markup Start Date" />
                                 </div>
                               
                                 <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly name="expiry_date_to" value="<?php if($agent_markups[0]->markup_expiry_date_to != '0000-00-00'){ echo date('d-m-Y', strtotime($agent_markups[0]->markup_expiry_date_to)); } ?>" id="datepicker444" placeholder="Markup End Date"   />
                                 </div>
                                 </div>
                           
                        </div>
                         <!-- <div class="likrticktsec" id="expry_date" >
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['MarkupExpiryDate']; ?></div>
                            <div class="coltcnt">
                            	<input type="text" class="payinput cols-sm-2" readonly name="expiry_date" value="<?php if($agent_markups[0]->markup_expiry_date != '0000-00-00'){ echo date('d-m-Y', strtotime($agent_markups[0]->markup_expiry_date)); } ?>" id="datepicker33" placeholder="Markup End Date"   />
                            </div>
                        </div> -->
                        
                       </div>  
                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['MarkupvalueType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="markup_value_type" id="markup_value_type" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
                                        <option value="PERCENTAGE" <?php if($agent_markups[0]->markup_value_type == "PERCENTAGE"){ echo "selected"; } ?>><?php echo $this->TravelLights['AcencySettings']['PERCENTAGE']; ?></option>
                                        <option value="FIXED" <?php if($agent_markups[0]->markup_value_type == "FIXED"){ echo "selected"; } ?>><?php echo $this->TravelLights['AcencySettings']['FIXED']; ?></option>
                                </select>
                                 <span id="markup-value-type-error" class="error" for="markup_value_type"> </span>
                                 </div>
                            </div>
                        </div>
                     
                      <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['MarkupValue']; ?></div>
                            <div class="coltcnt">
                                 <input type="text" maxlength="5" class="payinput" name="markup_value" id="digits" min="0" id="markup_value"  value="<?php echo $agent_markups[0]->markup_value; ?>"  required  />
                                <span id="markup-value-error" class="error" for="markup_value_type"> </span>
                            </div>
                        </div>
                       
                         <div class="likrticktsec">
                            <div class="cobldo">&nbsp;</div>
                            <div class="coltcnt">
                            <input type="submit" value="<?php echo $this->TravelLights['AcencySettings']['Update']; ?>" onclick="form_submits('edit_markup')" class="adddtickt">
                            </div>
                        </div>
                        
                       
                       </form>
                  
<script type="text/javascript">
$(document).ready(function(){
	$("#markup_type1").on('change', function(){
			
		 if($(this).val() == "SPECIFIC") {
			    $("#user_t1").css("display", "inherit");
			   
		   }else{
			    $("#user_t1").css("display", "none");
			 
		   }
		});
		
	$("#datepicker111").datepicker({
	numberOfMonths: 1,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: 1,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate()); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#datepicker222").datepicker("setDate", d1);
		$("#datepicker222").datepicker("option", "minDate", d1);
	},
	onClose: function( selectedDate ) {
		$( "#datepicker222" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker222' ).focus();
	}
	});
	
	$("#datepicker222").datepicker({
		numberOfMonths: 1,
		minDate: '-12Y',
		dateFormat: 'dd-mm-yy'
		
	});
	
	$("#datepicker333").datepicker({
	numberOfMonths: 1,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: 1,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate()); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#datepicker444").datepicker("setDate", d1);
		$("#datepicker444").datepicker("option", "minDate", d1);
	},
	onClose: function( selectedDate ) {
		$( "#datepicker444" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker444' ).focus();
	}
	});
	
	$("#datepicker444").datepicker({
		numberOfMonths: 1,
		minDate: '-12Y',
		dateFormat: 'dd-mm-yy'
		
	});
	
	
	
});
