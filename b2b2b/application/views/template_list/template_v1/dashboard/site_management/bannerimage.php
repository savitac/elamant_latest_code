
 <table id="example-staff" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
            <th>Image</th>
            <th>Banner Message</th>
            <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
            <th>Image</th>
            <th>Banner Message</th>
            <th>Action</th>
            </tr>
        </tfoot>
        <tbody >
           <?php for($a=0;$a<count($banner);$a++){ 
           	$path="assets/domain/".$banner[$a]->banner;
           ?>
						<tr>
							<td><img src="<?php echo ASSETS;?><?php echo  $path; ?>" alt="Smiley face" height="42" width="42"></td>
                            <td><?php echo $banner[$a]->msg; ?></td>
							
							<td class="center">
							 
								   <a class="btn btn-warning btn-xs" href="<?php echo  base_url()."site_management/inactive_banner/".base64_encode(json_encode($banner[$a]->id)); ?>"> Delete </a>
							 
							
							 </td>
						</tr>
		<?php } ?>	
        </tbody>
    </table>
    <script>
    $(document).ready(function(){

    $('#example-staff').DataTable({responsive:true});

});




</script>
