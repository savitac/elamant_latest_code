   
      	<table id="transfer_table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
                <th><?php echo $this->TravelLights['BookingTable']['Slno']; ?></th>  
                <th><?php echo $this->TravelLights['BookingTable']['Actions']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['TravellerName']; ?></th>   
                <th><?php echo $this->TravelLights['BookingTable']['BookingDate']; ?></th>   
                <th><?php echo $this->TravelLights['BookingTable']['TravelDate']; ?></th>   
                <th><?php echo $this->TravelLights['BookingTable']['PNRNo']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['BookingNo']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['BookingStatus']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['PaymentStatus']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['PickUp']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['DropOff']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['Currency']; ?></th> 
                <th><?php echo $this->TravelLights['BookingTable']['TotalAmount']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['NetRate']; ?></th> 
                <th><?php echo $this->TravelLights['BookingTable']['MyMarkup']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['MyBasePrice']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['ServiceCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['GSTCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['TaxCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['DiscountApplied']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['PromoCode']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['Discount']; ?></th>
                <?php if($this->session->userdata('user_type') == 2){ ?>
                <th><?php echo $this->TravelLights['BookingTable']['MySubAgentMarkup']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['MySubAgentPrice']; ?></th>
                <?php } ?>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><?php echo $this->TravelLights['BookingTable']['Slno']; ?></th>  
                <th><?php echo $this->TravelLights['BookingTable']['Actions']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['TravellerName']; ?></th>   
                <th><?php echo $this->TravelLights['BookingTable']['BookingDate']; ?></th>   
                <th><?php echo $this->TravelLights['BookingTable']['TravelDate']; ?></th>   
                <th><?php echo $this->TravelLights['BookingTable']['PNRNo']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['BookingNo']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['BookingStatus']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['PaymentStatus']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['PickUp']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['DropOff']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['Currency']; ?></th> 
                <th><?php echo $this->TravelLights['BookingTable']['TotalAmount']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['NetRate']; ?></th> 
                <th><?php echo $this->TravelLights['BookingTable']['MyMarkup']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['MyBasePrice']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['ServiceCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['GSTCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['TaxCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['DiscountApplied']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['PromoCode']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['Discount']; ?></th>
                <?php if($this->session->userdata('user_type') == 2){ ?>
                <th><?php echo $this->TravelLights['BookingTable']['MySubAgentMarkup']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['MySubAgentPrice']; ?></th>
                <?php } ?>
            </tr>
        </tfoot>
        <tbody>
          <?php $c=1;  
          for ($i=0; $i < count($transfer_orders); $i++){
              ?>
            <tr>
                <td><?php echo $c++; ?></td>
                <td>
					<a href="<?php echo site_url(); ?>bookings/view_bookings/<?php  echo base64_encode(json_encode($transfer_orders[$i]->parent_pnr_no)); ?>"> <i class="entypo-ticket" title="Voucher"></i><?php echo $this->TravelLights['BookingTable']['ViewVoucher']; ?></a>
					<?php if($transfer_orders[$i]->booking_status == 'CONFIRMED' || $transfer_orders[$i]->booking_status == "PENDING") { ?>
						<a href="<?php echo base_url();?>bookings/cancel/<?php echo $transfer_orders[$i]->product_name;?>/<?php echo base64_encode(base64_encode($transfer_orders[$i]->pnr_no)); ?>" onclick="return confirm('<?php echo $this->TravelLights['BookingTable']['Areyouwant']; ?>')" >
							<i class='entypo-cancel-squared' title="Cancel PNR"><?php echo $this->TravelLights['BookingTable']['Cancel']; ?></i> 
						</a>
					<?php } ?>
					<?php
						if($transfer_orders[$i]->booking_status == 'CONFIRMED' && $transfer_orders[$i]->payment_type == 'PAYLATER') {
					?>
							<a href="<?php echo base_url(); ?>payment/payment_view/<?php  echo base64_encode(json_encode($transfer_orders[$i]->pnr_no)); ?>/<?php echo $transfer_orders[$i]->product_id;?>">Make Payment</a>
					<?php
						}
					?>
				</td>
                <td><?php echo $transfer_orders[$i]->leadpax; ?></td>
                <td><?php echo date('d-m-Y',strtotime($transfer_orders[$i]->voucher_date)); ?></td>
                <td><?php echo date('d-m-Y', strtotime($transfer_orders[$i]->travel_date)); ?></td>
                <td><?php echo $transfer_orders[$i]->pnr_no; ?></td>
                <td><?php echo $transfer_orders[$i]->booking_no; ?></td>
              
                <td><?php echo $transfer_orders[$i]->booking_status; ?></td>
                <td><?php echo $transfer_orders[$i]->payment_status; ?></td>
                <td><?php echo $transfer_orders[$i]->pickup_code_description; ?></td>
                <td><?php echo $transfer_orders[$i]->dropoff_code_description; ?></td>
                <td><?php echo BASE_CURRENCY; ?></td>
                <td><?php echo $transfer_orders[$i]->total_amount; ?></td>
                 <?php if($this->session->userdata('user_type') == 2){ ?>   
                <td><?php echo $transfer_orders[$i]->admin_baseprice; ?></td>
                 <td><?php echo $transfer_orders[$i]->agent_markup; ?></td>
                <td><?php echo $transfer_orders[$i]->agent_base_amount; ?></td>
                <?php }else{ ?>
				  <td><?php echo $transfer_orders[$i]->agent_base_amount; ?></td>
				 <td><?php echo $transfer_orders[$i]->my_b2b2b_markup; ?></td>
                <td><?php echo $transfer_orders[$i]->b2b2b_baseprice; ?></td>	
				<?php } ?>
                <td><?php echo $transfer_orders[$i]->service_charge; ?></td>
                 <td><?php echo $transfer_orders[$i]->gst_charge; ?></td>
                 <td><?php echo $transfer_orders[$i]->tax_charge; ?></td>
                 <?php if ($transfer_orders[$i]->promo_id == 0) { ?>
                  <?php  $promoid = 'No' ?>
                <?php }else { ?>
                <?php  $promoid = 'Yes' ?>
                <?php } ?>
                 <td><?php echo $promoid; ?></td>
                 <td><?php echo $transfer_orders[$i]->promo_id; ?></td>
                 <td><?php echo $transfer_orders[$i]->discount; ?></td>
                 <?php if($this->session->userdata('user_type') == 2){ ?>
                <td><?php echo $transfer_orders[$i]->my_agent_Markup; ?></td>
                <td><?php echo $transfer_orders[$i]->sub_agent_baseprice; ?></td>
                <?php } ?>
                
            </tr>
           <?php } ?>
        </tbody>
    </table>
    
    <script>
    $(document).ready(function(){
      $('#transfer_table').DataTable({responsive: true});
    });
      
    </script>
