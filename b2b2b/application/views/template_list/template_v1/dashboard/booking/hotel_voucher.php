<?php
	//~ echo "data: <pre>";print_r(json_decode(base64_decode($Booking[0]->cart_data)));exit;
	//~ echo "data: <pre>";print_r($Booking);exit;
	$cid = $Booking[0]->cid;
	$TravelerDetails = json_decode($Booking[0]->TravelerDetails);
	$TravelerDetails = json_decode(json_encode($TravelerDetails), true);
	$roomcount = count($TravelerDetails['a_gender'.$cid]);

	$adult_count = 0;
	$child_count = 0;
	
	for ($i=0 ;$i< $roomcount ;$i++) { 
		$adultcnt = count($TravelerDetails['a_gender'.$cid][$i]);
		$childcnt = count(@$TravelerDetails['c_gender'.$cid][$i]);
		unset($room);
		for ($j=0 ;$j< $adultcnt ;$j++) { 
			$room['adult'][] = $TravelerDetails['a_gender'.$cid][$i][$j].' '.$TravelerDetails['first_name'.$cid][$i][$j].' '.$TravelerDetails['last_name'.$cid][$i][$j];
			$adult_count++;
		}
		for ($k=0 ;$k< $childcnt ;$k++) { 
			if($TravelerDetails['c_gender'.$cid][$i][$k]){
				$room['child'][] = $TravelerDetails['c_gender'.$cid][$i][$k].' '.$TravelerDetails['cfirst_name'.$cid][$i][$k].' '.$TravelerDetails['clast_name'.$cid][$i][$k];
				$child_count++;
			}  
		} 
		$roomdata[$i] = $room; 
	}
	//~ echo "data: <pre>";print_r($adult_count);exit; 
	$check_in = date('Y-m-d' , strtotime($Booking[0]->checkin));
	$check_out = date('Y-m-d' , strtotime($Booking[0]->checkout));
	$checkin_date = strtotime($check_in);
	$checkout_date = strtotime($check_out);
	$absDateDiff = abs($checkout_date - $checkin_date);
	$nights = floor($absDateDiff/(60*60*24));
?>
<?php
	if($Booking[0]->api_id == 5):
		$star = $Booking[0]->star;
		if($star == '1EST'){
			$star = 1;  
		 }
		else if($star == '2EST'){
			$star = 2;  
		}
		else if($star == '3EST'){
			$star = 3;  
		}
		else if($star == '4EST'){
		  $star = 4;  
		}
		else if($star == '5EST'){
			$star = 5;  
		}
		else{
			$star = $star;  
		}
	else:
		$star = $Booking[0]->star;
	endif;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Hotel Voucher</title>
        <link href="<?php echo ASSETS; ?>assets/css/font-awesome.min.css" rel="stylesheet" />
        <link href="<?php echo ASSETS; ?>assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo ASSETS; ?>assets/css/voucher.css" rel="stylesheet" />
         <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    </head>
    <body>
        <!-- Navigation --> 
        <div class="clearfix"></div>
        <div class="top80">
            <div class="full marintopcnt contentvcr" id="voucher" class="printthis">
                <div class="container offset-0">
                    <div class="col-md-12 nopad">
                        <table class="cell-container" cellspacing="0" cellpadding="0" border="0" align="center" style="width:75%;margin: 10px auto; color: #444; border: 1px solid #dddddd; padding: 15px">
                            <tbody>
                                <tr>
                                    <td style="padding: 20px">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table class="insideone">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="thrty first" style="width:33.33%">
                                                                        <div class="logovcr"> <a href="<?php echo ASSETS."dashboard"; ?>" ><img alt="" src="<?php echo ASSETS; ?>assets/images/logo.png" style="max-width:150px;"></a> </div>
                                                                    </td>
                                                                    <td class="thrty second" style="width:33.33%"><span class="faprnt fa fa-print" onClick="window.print()" ></span></td>
                                                                    <td class="thrty third" style="width:33.33%">
                                                                        <div class="adrssvr"> <br>
                                                                            <br>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
														<?php
															if($pnr_nos[0]->booking_status == 'CONFIRMED') {
														?>
																<div class="bighedingv" style=" color: #444444;display: block;font-size: 20px;overflow: hidden;padding: 10px 0;text-align: center;">Confirmation</div>
														<?php
															}
															elseif($pnr_nos[0]->booking_status == 'CANCELLED') {
														?>
																<div class="bighedingv" style=" color: #444444;display: block;font-size: 20px;overflow: hidden;padding: 10px 0;text-align: center;">Cancellation Confirmation</div>
														<?php	
															}
															else {
																
															}
														?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height:20px;width:100%;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="border:0px;" colspan="2">
                                                        <table width="100%" cellspacing="0" cellpadding="8" border="0" align="center">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td width="50%" valign="top" align="left">
                                                                                        <table width="100%" cellspacing="1" cellpadding="7" border="0" bgcolor="#FFFFFF">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td align="left" style="color: #666;padding-bottom: 10px" colspan="2"><strong style="font-size:18px;"><?php echo $user_details[0]->user_name; ?></strong></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Booking Status :</td>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $pnr_nos[0]->booking_status; ?></strong></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Voucher No :</td>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo substr($Booking[0]->pnr_no, 6); ?></strong></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Booking No :</td>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $Booking[0]->booking_no;?></strong></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666">The China Gap Ref No :</td>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $Booking[0]->pnr_no;?></strong></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Booking Date :</td>
                                                                                                    <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo date('d-m-Y',strtotime($Booking[0]->voucher_date));?></strong></td>
                                                                                                </tr>
																								
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td width="50%" valign="top" align="left">
                                                                                        <table width="100%" cellspacing="1" cellpadding="7" border="0" bgcolor="#FFFFFF">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td align="left" colspan="2">&nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="50%" align="left">&nbsp;</td>
                                                                                                    <td width="50%" align="left">&nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left">&nbsp;</td>
                                                                                                    <td align="left">&nbsp;</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                
                                                                <?php
																	if($Booking[0]->payment_type == "DEPOSIT") {
																		$total_amount = $Booking[0]->total_amount;
																		$transaction_amount = $Booking[0]->transaction_amount;
																	}
																	elseif($Booking[0]->payment_type == "PAYMENT") {
																		$total_amount = $Booking[0]->total_PG_payble;
																		$transaction_amount = $Booking[0]->transaction_amount;
																	}
																	elseif($Booking[0]->payment_type == "BOTH") {
																		$transaction_amount = 'Not Defined';
																		$total_amount = 'Not Defined';
																	}
																	elseif($Booking[0]->payment_type == "PAYLATER") {
																		$transaction_amount = $Booking[0]->transaction_amount;
																		$due_amount = $Booking[0]->due_amount;
																	}
																	else {
																		
																	}
                                                                ?>
                                                                
                                                                <tr>
                                                                    <td width="50%" valign="top" align="left">
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="2"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;">Payment Details</span></td>
                                                                                </tr>
                                                                                <tr>
																					<td width="30%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Payment Status:</td>
																					<td width="70%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $Booking[0]->payment_status ?></strong></td>
																				</tr>
																				<tr>
																					<td width="30%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Total Amount:</td>
																					<td width="70%" align="left" style="font-size: 15px; line-height: 28px; color: #0e7ab1"><strong><?php echo $Booking[0]->api_currency." ".$total_amount; ?></strong></td>
																				</tr>
																				<?php
																					if($Booking[0]->payment_type == "PAYLATER") {
																				?>
																						<tr>
																							<td width="30%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Amount Due:</td>
																							<td width="70%" align="left" style="font-size: 15px; line-height: 28px; color: #0e7ab1"><strong><?php echo $due_amount; ?></strong></td>
																						</tr>
																				<?php
																					}
																				?>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                    
																	
																	
                                                                </tr>
                                                                <tr>
                                                                    <td style="height:20px;width:100%;"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td bgcolor="#ffffff" align="center" style="border: 1px solid #cccccc;overflow: auto;width: 100%;">
                                                                        <table width="100%" cellspacing="0" cellpadding="7" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td bgcolor="#f1f1f1" style="border-bottom: 1px solid #cccccc;" colspan="4">
                                                                                        <span style="display: block; font-size: 18px;padding:5px 10px"> <?php echo $Booking[0]->hotel_name;?>  </span>
                                                                                        <span style="display: block; font-size: 13px;padding:5px 10px; ">
                                                                                            <div data-star="<?php echo $star;?>" class="stra_hotel">
                                                                                                <span class="fa fa-star"></span>
                                                                                                <span class="fa fa-star"></span>
                                                                                                <span class="fa fa-star"></span>
                                                                                                <span class="fa fa-star"></span>
                                                                                                <span class="fa fa-star"></span>
                                                                                            </div>
                                                                                            <?php echo (!empty(@($Booking[0]->hotel_address)))?$Booking[0]->hotel_address:'';?>                                           
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="25%" style="border-right: 1px solid #cccccc; padding: 10px" rowspan="2">
                                                                                        <span style="width:100%"> <img width="100px" height="100px" src="<?php echo $Booking[0]->image;?>"> </span>
                                                                                        <br>
                                                                                        <span style="font-size: 13px;">Room type: <strong><?php echo $Booking[0]->room_type;?><br></strong></span>
                                                                                    </td>
                                                                                    <td width="25%" bgcolor="#FFFFFF" style="border-bottom:1px solid #cccccc; border-right:1px solid #cccccc; padding: 10px"><span style="font-size:16px; font-weight:bold;">Check-in</span><br><?php echo $check_in;?></td>
                                                                                    <td width="25%" bgcolor="#FFFFFF" style="padding: 10px;border-bottom:1px solid #cccccc; border-right:1px solid #cccccc;"><span style="font-size:16px; font-weight:bold;">Check-out</span><br><?php echo $check_out;?></td>
                                                                                    <td width="25%" bgcolor="#FFFFFF" style="padding: 10px" rowspan="2"><strong>Guests</strong> <br>
                                                                                        <br>
                                                                                        Adult : <?php echo $adult_count; ?>                                       <br>
                                                                                        Children : <?php echo $child_count; ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="border-right:1px solid #cccccc;padding: 10px">No of night(s)<br>
                                                                                        <span style="font-size:13px; font-weight:bold;"><?php echo $nights; ?></span>
                                                                                    </td>
                                                                                    <td bgcolor="#FFFFFF" style="border-right:1px solid #cccccc; padding: 10px">Room(s)<br>
                                                                                        <span style="font-size:13px; font-weight:bold;"><?php echo count($roomdata); ?></span>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="5"><span style="display: block;font-size: 14px;padding: 10px 0 0;"><strong>Room Inclusions:</strong> <?php echo $Booking[0]->inclusion; ?></span></td>
                                                                                </tr>
																			</tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td style="height:10px;width:100%;"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height:10px;width:100%;"></td>
                                                                </tr>
                                                                
                                                                <?php
																	if($Booking[0]->booking_res != '') {
																		$booking_res = json_decode($Booking[0]->booking_res);
																		if($Booking[0]->api_id == 1):
																			$RemarksArray = json_decode($booking_res->Remarks);
																			for($remark_iter = 0; $remark_iter < count($RemarksArray); $remark_iter++) {
																				$Remarks[] = $RemarksArray[$remark_iter]->text;
																			}
																			$Remarks = implode('<br>',$Remarks);
																		elseif($Booking[0]->api_id == 5):
																			$Remarks = $booking_res->Remarks;
																		else:
																		endif;
																		
																		if($Remarks != '') {
																		
																?>
																			<tr>
																				<td colspan="2">
																					<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
																						<tbody>
																							<tr>
																								<td><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;"><?php echo "Special Information"; ?> </span></td>
																							</tr>
																							<tr>
																								<td valign="top" align="left" style="padding:10px 0;"><span style="font-size: 14px;padding: 10px 0;"><?php echo $Remarks;?></span></td>
																							</tr>
																						</tbody>
																					</table>
																				</td>
																			</tr>
																<?php
																		}
																	}
																?>
                                                                
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="5"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin-bottom: 15px;padding: 10px 0;">Traveller Details</span></td>
                                                                                </tr>
                                                                                <?php  
																					for($t=0; $t<count($roomdata); $t++){
																				?>
																						<tr>
																							<th valign="top" align="left" colspan="4" style="padding: 20px 0; font-size:16px; color:#333;"> Room <?php echo $t+1;?> </th>
																						</tr>
																						<tr style="background:#f2f2f2; border: 1px solid #eeeeee;">
																							<th valign="top" align="left" style="padding: 10px"><strong>Passenger Type</strong></th>
																							<th valign="top" align="left" colspan="3" style="padding: 10px"><strong>Name</strong></th>
																						</tr>
																						<?php 
																							for($u = 0;$u<count($roomdata[$t]['adult']);$u++){
																						?>
																								<tr style="background:#ffffff; border: 1px solid #eeeeee;">
																									<td style="padding: 10px">ADULT</td>
																									<td colspan="3" style="padding: 10px"><?php echo $roomdata[$t]['adult'][$u]; ?></td>
																								</tr>
																						<?php
																							}
																						?>
																						<?php 
																							if($child_count > 0) {
																								for($u = 0;$u<count(@$roomdata[$t]['child']);$u++){
																							?>
																									<tr style="background:#ffffff; border: 1px solid #eeeeee;">
																										<td style="padding: 10px">CHILD</td>
																										<td colspan="3" style="padding: 10px"><?php echo $roomdata[$t]['child'][$u]; ?></td>
																									</tr>
																						<?php
																								}
																							}
																						?>
																				<?php
																					}
																				?>
                                                                                
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td style="height:10px;width:100%;"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="2"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;">Customer Details</span></td>
                                                                                </tr>
                                                                                <tr style="border: 1px solid #eee;">
                                                                                    <td width="20%" align="left" style="background:#f1f1f1;padding: 10px"><strong>Email ID</strong></td>
                                                                                    <td width="80%" align="left" style="background:#ffffff;padding: 10px"><?php echo $Booking[0]->billing_email;?></td>
                                                                                </tr>
                                                                                <tr style="border: 1px solid #eee;">
                                                                                    <td align="left" style="background:#f1f1f1;padding: 10px"><strong>Mobile Number</strong></td>
                                                                                    <td align="left" style="background:#ffffff;padding: 10px"><?php echo $Booking[0]->billing_contact_number;?></td>
                                                                                </tr>
                                                                                <tr style="border: 1px solid #eee;">
                                                                                    <td align="left" style="background:#f1f1f1;padding: 10px"><strong>Address</strong></td>
                                                                                    <td align="left" style="background:#ffffff;padding: 10px"><?php echo $Booking[0]->billing_address.', '.$Booking[0]->billing_city.', '.$Booking[0]->billing_state.', '.$Booking[0]->billing_zip;?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="2"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;">Hotel Details</span></td>
                                                                                </tr>
                                                                                <tr style="border: 1px solid #eee;">
                                                                                    <td width="20%" align="left" style="background:#f1f1f1;padding: 10px"><strong>Name</strong></td>
                                                                                    <td width="80%" align="left" style="background:#ffffff;padding: 10px"><?php echo $Booking[0]->hotel_name;?></td>
                                                                                </tr>
                                                                                <tr style="border: 1px solid #eee;">
                                                                                    <td align="left" style="background:#f1f1f1;padding: 10px"><strong>Destination</strong></td>
                                                                                    <td align="left" style="background:#ffffff;padding: 10px"><?php echo $Booking[0]->city;?></td>
                                                                                </tr>
                                                                                <?php
																					if(!empty(@($Booking[0]->hotel_address)) and @($Booking[0]->hotel_address) != null) {
                                                                                ?>
																					<tr style="border: 1px solid #eee;">
																						<td align="left" style="background:#f1f1f1;padding: 10px"><strong>Address</strong></td>
																						<td align="left" style="background:#ffffff;padding: 10px"><?php echo $Booking[0]->hotel_address;?> </td>
																					</tr>
                                                                                <?php
																					}
                                                                                ?>
                                                                                <?php
																					if(!empty(@($Booking[0]->hotel_contact)) and @($Booking[0]->hotel_contact) != null) {
                                                                                ?>
																					<tr style="border: 1px solid #eee;">
																						<td align="left" style="background:#f1f1f1;padding: 10px"><strong>Contact No</strong></td>
																						<td align="left" style="background:#ffffff;padding: 10px">
																							PhoneHotel : <?php echo $Booking[0]->hotel_contact;?>
																							<br>
																						</td>
																					</tr>
                                                                                <?php
																					}
                                                                                ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php
													if(false) {
												?>
														<tr>
															<td colspan="2">
																<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
																	<tbody>
																		<tr>
																			<td><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;">Amendment Policy </span></td>
																		</tr>
																		<tr>
																			<td valign="top" align="left" style="padding:10px 0;"><span style="font-size: 14px;padding: 10px 0;"><?php echo $Booking[0]->amendment_policy;?></span></td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
												<?php
													}
                                                ?>
                                                <tr>
                                                    <td colspan="2">
                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                                            <tbody>
                                                                <tr>
                                                                    <td><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;">Cancellation Policy </span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" align="left" style="padding:10px 0;"><span style="font-size: 14px;padding: 10px 0;"><?php echo $Booking[0]->cancel_policy;?></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Content --> 
        <div class="clearfix"></div>
        <style>
            .leftflitmg { max-width:70px !important } 
        </style>
        <script type="text/javascript">
			function printthis() {
				var w = window.open('', '', 'width=800,height=600,resizeable,scrollbars');
					w.document.write($("#voucher").html());
					w.document.close(); // needed for chrome and safari
					javascript:w.print();
					w.close();
					return false;
				}
		</script>
    </body>
</html>
