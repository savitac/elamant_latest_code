<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo  $this->session->userdata('company_name');?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet">
	<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	
	<?php echo $this->load->view('dashboard/top'); ?>	


<section id="main-content">
  
  <section class="wrapper">
	<div class="main-chart">

        <div class="tabbable customtab">
            
                    <ul class="nav nav-tabs">
                        <li class="active"> <a data-toggle="tab" href="#sub_agent"> <?php echo $this->TravelLights['roleManagement']['RolesList']; ?> </a> </li>
                        <li class=""> <a data-toggle="tab" href="#depositmanage"> <?php echo $this->TravelLights['roleManagement']['StaffList']; ?> </a> </li>
                     </ul>
                    
                    <div class="tab-content">
                        
                        <div id="sub_agent" class="tab-pane active">
                        	<div class="intabs">
                            	<div id="crate_agent">
                                <span class="profile_head adclr"><?php echo $this->TravelLights['roleManagement']['RolesList']; ?> </span>
                                
                                <div class="withedrow">
                                	<div class="rowit">
                                    <a class="rgtsambtn" id="creat_click"><?php echo $this->TravelLights['roleManagement']['AddNewRole']; ?></a>
                                    <div class="clearfix"></div>
                                    <?php $this->load->view('dashboard/staffmanagement/roles_list'); ?>
                               </div>
                                </div>
                                
                                </div>
                     <div id="crate_agent_form" style="display:none;">
						 
                       <span class="profile_head adclr"><?php echo $this->TravelLights['roleManagement']['AddNewRole']; ?></span> 
               		  <div class="withedrow">
                    <div class="rowit">
                    <a class="rgtsambtn" id="back_list"><?php echo $this->TravelLights['StaffManagementForm']['Backtolist']; ?></a>
                    <div class="clearfix"></div>
                      <div class="addtiktble">
						  <?php $this->load->view('dashboard/staffmanagement/add_roles'); ?> 
                        </div>
                    </div>
                   </div>
                   
             
             </div>
             
                             
                  <div id="crate_privilege_form" style="display:none;">
                        <span class="profile_head adclr"><?php echo $this->TravelLights['StaffManagementForm']['ManagePrivileges']; ?></span> 
               						 <div class="withedrow">
                    <div class="rowit">
                    <a class="rgtsambtn" id="back_list_2"><?php echo $this->TravelLights['StaffManagementForm']['Backtolist']; ?></a>
                    <div class="clearfix"></div>
                      <div class="addtiktble" id="agent_prvilege">
                        
                      </div>
                    </div>
                </div>
                </div>
                
                
             </div>
                        </div>
                       
                        <div id="depositmanage" class="tab-pane ">
                          	<div class="intabs">
                            	<div id="creat_staff">
                                <span class="profile_head adclr"><?php echo $this->TravelLights['StaffManagementForm']['StaffList']; ?></span>
                                
                                <div class="withedrow">
                                	<div class="rowit">
                                    <a class="rgtsambtn" id="creat_staff_btn"><?php echo $this->TravelLights['StaffManagementForm']['CreateStaff']; ?></a>
                                    <div class="clearfix"></div>
                                <?php $this->load->view('dashboard/staffmanagement/staff_list'); ?>    
                           </div>
                        </div>
                                
                                </div>
                                
                                
                                <div id="crate_staff_form" style="display:none;">
                                	<span class="profile_head adclr"><?php echo $this->TravelLights['StaffManagementForm']['CreateRoles']; ?></span> 
               						 <div class="withedrow">
                    <div class="rowit">
                    <a class="rgtsambtn" id="back_list_1"><?php echo $this->TravelLights['StaffManagementForm']['Backtolist']; ?></a>
                    <div class="clearfix"></div>
                      <div class="addtiktble">
                        <?php $this->load->view('dashboard/staffmanagement/add_staff'); ?>    
                      </div>
                    </div>
                </div>
                </div>
                
                
                
                
                <div id="edit_agent_form" style="display:none;">
                     <div class="withedrow">
                    <div class="rowit">
                    <a class="rgtsambtn" id="back_list_edit_agent"><?php echo $this->TravelLights['StaffManagementForm']['Backtolist']; ?></a>
                    <div class="clearfix"></div>
                      <div class="addtiktble">
						  <div  id="edit_form">
						
						
						 </div>
						
					</div>
					</div>
					</div>
					</div>
       
               
               
               
                        <div id="crate_product_form" style="display:none;">
                        <span class="profile_head adclr"><?php echo $this->TravelLights['StaffManagementForm']['ManageProducts']; ?></span> 
               						 <div class="withedrow">
                    <div class="rowit">
                    <a class="rgtsambtn" id="back_list_3"><?php echo $this->TravelLights['StaffManagementForm']['Backtolist']; ?></a>
                    <div class="clearfix"></div>
                      <div class="addtiktble" id="agent_products">
                        
                      </div>
                    </div>
                </div>
                </div>
       
       
            </div>
        </div>
                        
                        
                  </div>
                  
            </div>
        
        </div>
  </section>

</section>




<div class="clearfix"></div>
	<?php echo $this->load->view('core/bottom_footer'); ?>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
   
   $( "#datepicker" ).datepicker();
	$( "#datepicker1" ).datepicker();
	$( "#datepicker2" ).datepicker();
	
	$("#markup_type").on('change', function(){
		 if($(this).val() == "SPECIFIC") {
			    $("#user_t").css("display", "inherit");
			    $("#agent_n").css("display", "inherit");
			    $("#country").css("display", "inherit");
			    $("#date_range").css("display", "inherit");
			    $("#expry_date").css("display", "inherit");
		   }else{
			    $("#user_t").css("display", "none");
			    $("#agent_n").css("display", "none");
			   $("#country").css("display", "none");
			  $("#date_range").css("display", "none");
			  $("#expry_date").css("display", "none");
		   }
		});
  
    
   
   
	
	$('#creat_click').click(function(){
		$('#crate_agent').fadeOut(500, function(){
				$('#crate_agent_form').fadeIn();
			});
	});
	
	$('#creat_staff_btn').click(function(){
		$('#creat_staff').fadeOut(500, function(){
				$('#crate_staff_form').fadeIn();
			});
	});
	
	$('#back_list').click(function(){
		$('#crate_agent_form').fadeOut(500, function(){
				$('#crate_agent').fadeIn();
			});
	});
	
	$('#back_list_1').click(function(){
		$('#crate_staff_form').fadeOut(500, function(){
				$('#creat_staff').fadeIn();
			});
	});  
	
	$('#back_list_2').click(function(){
		$('#crate_privilege_form').fadeOut(500, function(){
				$('#crate_agent').fadeIn();
			});
	});  
	
	$('#back_list_3').click(function(){
		$('#crate_product_form').fadeOut(500, function(){
				$('#creat_staff').fadeIn();
			});
	}); 
	
	
	
	
	$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
	});
	
	
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	
});




$("#owl-demobaners1").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	  $("#owl-demobaners2").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	   $("#owl-demobaners3").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });


$("#owl-demo3").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });

$("#owl-demo4").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });
	  
	  $("#owl-demo5").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });


$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});
var Script = function () {


//    sidebar dropdown menu auto scrolling





//    sidebar toggle

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                //$('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });


    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });




}();


</script>

<script>
$(document).ready(function(){
	getAgentList();
    
		$('#editquestion').click(function(){
			$('.fullquestionswrp').slideToggle(500);
		});
		
		$('#editprivatepub').click(function(){
			$('.fullquestionswrpshare').slideToggle(500);
		});
		
		$('#smsalert').click(function(){
			$('.fullquestionswrp2').slideToggle(500);
		});
		
		$('#changepaswrd').click(function(){
			$('.fullquestionswrp3').slideToggle(500);
		});

        $('#addMarkUp').click(function() {
            $('.fullquestionswrp5').slideToggle(500);
        })
			
	});
	
	function getAgentList(){
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url().'usermanagement/getAgentList'; ?>",
			dataType: "json",
			success: function(data){
				$("#agentData").html(data.agent_list);
			}
		});
	}
	
	function editAgent(url){
		$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#crate_agent').fadeOut(500, function(){
				$('#edit_agent_form').fadeIn();
			});
				$("#edit_agent_form").css('display','inherit');
				$("#edit_form").html(data.editAgent);
				
				return false;
			}
		});
	}

    function editMarkup(url){
		
		$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#crate_markup_list').fadeOut(500, function(){
				$('#edit_markup_form').fadeIn();
			});
				$("#edit_markup_form").css('display','inherit');
				$("#editmark").html(data.updateMarkup);
				
				return false;
			}
		});
	}
	
	function editPrivilege(url){

	$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#crate_agent').fadeOut(500, function(){
				$('#crate_privilege_form').fadeIn();
			});
				$("#crate_privilege_form").css('display','inherit');
				$("#agent_prvilege").html(data.privilege_template);
				
				return false;
			}
		});
}

function editproduct(url){

	$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#creat_staff').fadeOut(500, function(){
				$('#crate_product_form').fadeIn();
			});
				$("#crate_product_form").css('display','inherit');
				$("#agent_products").html(data.product_template);
				
				return false;
			}
		});
}



$("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                  .columns.adjust()
                  .responsive.recalc();
            });


	</script>



</body>
</html>
