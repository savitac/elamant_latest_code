
 <table id="example-staff" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
                            <th><?php echo $this->TravelLights['StaffManagementForm']['SNo']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Name']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Role']?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['UserType']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Email']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Accountnumber']; ?></th>
						    <th><?php echo $this->TravelLights['StaffManagementForm']['MobileNumber']; ?></th>
						    <th><?php echo $this->TravelLights['StaffManagementForm']['CompanyName']; ?></th>
                            <th><?php echo $this->TravelLights['StaffManagementForm']['Status']; ?></th>
                            <th><?php echo $this->TravelLights['StaffManagementForm']['Action']; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
               				<th><?php echo $this->TravelLights['StaffManagementForm']['SNo']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Name']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Role']?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['UserType']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Email']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Accountnumber']; ?></th>
						    <th><?php echo $this->TravelLights['StaffManagementForm']['MobileNumber']; ?></th>
						    <th><?php echo $this->TravelLights['StaffManagementForm']['CompanyName']; ?></th>
                            <th><?php echo $this->TravelLights['StaffManagementForm']['Status']; ?></th>
                            <th><?php echo $this->TravelLights['StaffManagementForm']['Action']; ?></th>
            </tr>
        </tfoot>
        <tbody >
           <?php for($a=0;$a<count($agentadmin_list);$a++){ ?>
						<tr>
							<td><?php echo ($a+1); ?></td>
							<td><?php echo $agentadmin_list[$a]->user_name; ?></td>
							<td><?php echo $agentadmin_list[$a]->agent_roles_name; ?></td>
							<td><?php echo "B2B"; ?></td>
							<td><?php echo $agentadmin_list[$a]->user_email; ?></td>
							<td><?php echo $agentadmin_list[$a]->user_account_number; ?></td>
							<td><?php echo $agentadmin_list[$a]->user_cell_phone; ?></td>
							<td><?php echo $agentadmin_list[$a]->company_name; ?></td>
                            <td>
								<?php if($agentadmin_list[$a]->user_status == "ACTIVE"){  
									 echo $this->TravelLights['roleManagement']['Active'];
								 } else {
									  echo $this->TravelLights['roleManagement']['InActive'];
								 } ?>
							</td>
							<td class="center">
							 <?php if($agentadmin_list[$a]->user_status == "ACTIVE"){ ?> 
								   <a class="btn btn-warning btn-xs" href="<?php echo  base_url()."usermanagement/inactive_b2b_users/".base64_encode(json_encode($agentadmin_list[$a]->user_details_id)); ?>" onclick="return confirm('Are you want Inactivate?')"> <?php echo $this->TravelLights['roleManagement']['InActive']; ?> </a>
							 <?php } else { ?>
								   <a class="btn btn-success btn-xs" href="<?php echo  base_url()."usermanagement/active_b2b_users/".base64_encode(json_encode($agentadmin_list[$a]->user_details_id)); ?>" onclick="return confirm('Are you want activate?')"> <?php echo $this->TravelLights['roleManagement']['Active']; ?> </a>
							 <?php } ?>	
							  
							  <a class="btn btn-danger btn-xs" onclick="editproduct('<?php echo  base_url()."staffmanagement/getUserProducts/".base64_encode(json_encode($agentadmin_list[$a]->user_details_id)); ?>')"  > <?php echo $this->TravelLights['StaffManagementForm']['ManageProducts']; ?>  </a>
							
							 </td>
						</tr>
		<?php } ?>	
        </tbody>
    </table>
    <script>
    $(document).ready(function(){

    $('#example-staff').DataTable({responsive:true});
    
});




</script>
