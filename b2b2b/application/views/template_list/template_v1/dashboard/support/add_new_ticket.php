<div class="intabs">
                                <div id="suportcloseticket">
                                	<span class="profile_head adclr"><?php echo $this->TravelLights['Support']['Inbox']; ?></span>
                                    <div class="withedrow">
                                    <div class="rowit">
                                  <table id="support_ticket_list_closed" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                      <tr class="sortablehed">
                                        <th><?php echo $this->TravelLights['Support']['No']; ?></th>
                                        <th><?php echo $this->TravelLights['Support']['TicketNo']; ?></th>
                                        <th><?php echo $this->TravelLights['Support']['Subject']; ?></th>
                                        <th><?php echo $this->TravelLights['Support']['LastUpdatedBy']; ?></th>
                                        <th><?php echo $this->TravelLights['Support']['LastUpdatedOn']; ?></th>
                                        <th><?php echo $this->TravelLights['Support']['Action']; ?></th>
                                      </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                        <th><?php echo $this->TravelLights['Support']['No']; ?></th>
                                        <th><?php echo $this->TravelLights['Support']['TicketNo']; ?></th>
                                        <th><?php echo $this->TravelLights['Support']['Subject']; ?></th>
                                        <th><?php echo $this->TravelLights['Support']['LastUpdatedBy']; ?></th>
                                        <th><?php echo $this->TravelLights['Support']['LastUpdatedOn']; ?></th>
                                        <th><?php echo $this->TravelLights['Support']['Action']; ?></th>
                                      </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php if($support_close != ""){
                                      $i = 1;
                                      foreach ($support_close as $key => $value) {
                                    ?>
                                      <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value->support_ticket_id; ?></td>
                                        <td><?php echo $value->subject; ?></td>
                                        <td><?php echo $value->last_updated_by; ?></td>
                                        <td><?php echo date('d\ M\ Y h:i',strtotime($value->last_update_time)); ?></td>
                                        <td><ul class="list-inline action tdaction">
                                            <li> <a class="btn btnred"  data-toggle="tooltip" data-placement="top" title="Delete Ticket" href="<?php echo base_url().'support/delete_ticket/'.$value->support_ticket_id ?>"><i class="fa fa-times"></i></a> </li>
                                          </ul></td>
                                      </tr>
                                      <?php $i++; } } else { ?>
                                      <tr>
                                        <td colspan="6"><?php echo $this->TravelLights['Support']['NoDataAvailable']; ?></td>
                                      </tr>
                                      <?php } ?>
                                    </tbody>
                                  </table>
                               </div>
                                  </div>
                                  
                                </div>
                              </div>
<script>
$(document).ready(function(){
$('#support_ticket_list_closed').DataTable({responsive:true});  
});
</script>
