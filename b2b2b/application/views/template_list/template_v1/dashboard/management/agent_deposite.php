   	<div class="intabs">
                            	<div id="crate_deposit">
                                <span class="profile_head adclr"><?php echo $this->TravelLights['UserManagement']['DepositManagement']; ?></span>
                                
                                <div class="withedrow">
                                	<div class="rowit">
                                    <a class="rgtsambtn" id="creat_deposit"><?php echo $this->TravelLights['UserManagement']['AddAgentDeposite']; ?></a>
                                    <div class="clearfix"></div>
                                    	<table id="depositeTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
				<th> <?php echo $this->TravelLights['DepositManagementForm']['Slno']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['DepositeType']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['AgentName']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['DepositedDate']; ?></th>
                <!-- <th><?php// echo $this->TravelLights['DepositManagementForm']['Amount']; ?></th> -->
                <th><?php echo $this->TravelLights['DepositManagementForm']['CreditedAmount']; ?></th>
                <!-- <th><?php// echo $this->TravelLights['DepositManagementForm']['CurrencyCode']; ?></th> -->
                <th><?php echo $this->TravelLights['DepositManagementForm']['Remarks']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['AdminRemarks']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['Status']; ?></th>
              
            </tr>
        </thead>
        <tfoot>
            <tr>
				<th> <?php echo $this->TravelLights['DepositManagementForm']['Slno']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['DepositeType']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['AgentName']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['DepositedDate']; ?></th>
                <!-- <th><?php// echo $this->TravelLights['DepositManagementForm']['Amount']; ?> --></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['CreditedAmount']; ?></th>
                <!-- <th><?php// echo $this->TravelLights['DepositManagementForm']['CurrencyCode']; ?></th> -->
                <th><?php echo $this->TravelLights['DepositManagementForm']['Remarks']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['AdminRemarks']; ?></th>
                <th><?php echo $this->TravelLights['DepositManagementForm']['Status']; ?></th>
               
            </tr>
        </tfoot>
        <tbody>
           <?php if($depsoite!=''){ for($a=0;$a<count($depsoite);$a++){ ?>
						<tr>
							<td><?php echo ($a+1); ?></td>
							<td><?php echo $depsoite[$a]->deposit_type; ?></td>
							<td><?php echo $depsoite[$a]->user_name; ?></td>
							<td><?php echo $depsoite[$a]->deposited_date; ?></td>
							
							 <?php $pdata = $depsoite[$a]->currencycode;
                                $TotalPrice_Curr  = 'AUD';
                                $TotalPrice =  $depsoite[$a]->amount_credit;
                            	$TotalPrice = $this->General_Model->currency_convertor($TotalPrice,$pdata,$TotalPrice_Curr);
                                ?> 
							<td><?php echo $TotalPrice; ?></td>
							<!-- <td><?php// echo $depsoite[$a]->amount_credit; ?></td> -->
							<!-- <td><?php// echo $depsoite[$a]->currencycode; ?></td> -->
							<td><?php echo $depsoite[$a]->remarks; ?></td>
                            <td><?php echo $depsoite[$a]->admin_remarks; ?></td>
							<td>
								<?php if($depsoite[$a]->status == "Accepted"){ ?>
									<button type="button" class="btn btn-green btn-icon icon-left action-details1"><?php echo $this->TravelLights['DepositManagementForm']['ACCEPTED']; ?></button>
								<?php }elseif($depsoite[$a]->status == "Cancelled"){ ?>
									<button type="button" class="btn btn-red btn-icon icon-left action-details1"><?php echo $this->TravelLights['DepositManagementForm']['CANCELLED']; ?></button>
								<?php }else{ ?>
										<button type="button" class="btn btn-orange btn-icon icon-left action-details1"><?php echo $this->TravelLights['DepositManagementForm']['PENDING']; ?></button>
								<?php } ?>
							</td>
							
							
						</tr>
					<?php }} ?>
           
        </tbody>
    </table>
                                    </div>
                                </div>
                                
                                </div>
                                
                                
                                <div id="crate_deposit_form" style="display:none;">
                                	<span class="profile_head adclr"><?php echo $this->TravelLights['DepositManagementForm']['AddDeposit']; ?></span> 
               						 <div class="withedrow">
                    <div class="rowit">
                    <a class="rgtsambtn" id="back_list_deposit"><?php echo $this->TravelLights['DepositManagementForm']['Back']; ?></a>
                    <div class="clearfix"></div>
                      <div class="addtiktble">
                     <form id="deposite" name="Deposit" class="validate form-horizontal"  method="post" action="<?php echo base_url().'usermanagement/addAgentDeposit'; ?>" novalidate  >  
                        <div class="likrticktsec"><?php //."/".base64_encode(json_encode('YES')) ?>
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['SubAgents']; ?></div>
                            <div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="subagents" id="subagents" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
                                       <?php if($agentusers!=''){ for($a=0;$a<count($agentusers);$a++){ ?>
                                            <option value="<?php echo $agentusers[$a]->user_details_id; ?>" data-iconurl=""><?php echo $agentusers[$a]->user_email ." (".$agentusers[$a]->user_name.")"; ?></option>
                                       <?php }} ?>
                                </select>
                                <span id="subagents-error" class="error" for="subagents"> </span>
                                 </div>
                            </div>
                        </div>        
                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['DepositType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="deposit_type" id="deposit_type" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
                                       <option value="CASH"><?php echo $this->TravelLights['DepositManagementForm']['CASH']; ?></option>
                                        <option value="BANK"><?php echo $this->TravelLights['DepositManagementForm']['BANK']; ?></option>
                                        <option value="CHEQUE/DD"><?php echo $this->TravelLights['DepositManagementForm']['CHEQUEDD']; ?></option>
                                </select>
                                <span id="deposit-type-error" class="error" for="deposit_type"> </span>
                                 </div>
                            </div>
                        </div>
                        
                         <div class="likrticktsec hide">
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['AgentCurrencyCode']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="currencycode" id="currencycode" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
                                       <?php if($currency!=''){ for($a=0;$a<count($currency);$a++){ ?>
											<option value="<?php echo $currency[$a]->currency_code; ?>" data-iconurl=""><?php echo $currency[$a]->currency_code ." (".$currency[$a]->currency_name.")"; ?></option>
									   <?php }} ?>
                                </select>
                                <span id="currencycode-error" class="error" for="currencycode"> </span>
                                 </div>
                            </div>
                        </div>
                        
                        <div class="likrticktsec" id="deposit_date">
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['DepositedDate']; ?></div>
                            <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly  name="deposit_date" id="datepicker3" placeholder="Deposit Date"   />
                            </div>
                        </div>

                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['AmountDepostied']; ?></div>
                            <div class="coltcnt">

                                  <input type="text" name="amount_credit" id="amount_credit" maxlength="8" class="payinput" required > 
                                  <span id="amount-credit-error" class="error" for="amount_credit"> </span>
                            </div>
                        </div>
                        
                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['DepositManagementForm']['Remarks']; ?></div>
                            <div class="coltcnt">
                                <textarea id="remarks" name="remarks" class="tikttext" required ></textarea>
                                 <span id="remarks-error" class="error" for="remarks"> </span>
                            </div>
                        </div>
                        
                        <div class="likrticktsec">
                            <div class="cobldo">&nbsp;</div>
                            <div class="coltcnt">
                            <input type="submit" value="Add" onclick="form_submits('deposite')" class="adddtickt">
                            </div>
                        </div>
                        </form>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            
            
        <script>
        $("#datepicker3").datepicker({
        numberOfMonths: 1,
        dateFormat: 'dd-mm-yy'

        });    
        </script> 
                        
