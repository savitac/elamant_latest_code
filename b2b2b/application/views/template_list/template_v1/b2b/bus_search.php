<div id="Bus" class="tab-pane <?=@$bus_in_view?>"> 
    <form autocomplete="on" name="bus" id="bus_form" action="<?php echo base_url();?>index.php/bus/pre_bus_search" method="get" class="activeForm oneway_frm" style="">
	<div class="tabspl forbusonly">
    	<div class="tabrow bus_search">
			<div class="col-md-8 col-sm-6 col-xs-12 nopad">
				<div class="col-xs-6 padfive full_smal_tab">
					<div class="lablform">From</div>
					<div class="plcetogo locatiomarker ">
                    <span class="maskimg  hfrom"></span>
						<input type="text" required=""  value="" placeholder="Type Departure City" id="bus-station-from" class="ft normalinput bus-station auto-focus form-control b-r-0 valid_class bus-station-from" name="bus_station_from" autocomplete="on">
						<input class="hide loc_id_holder" name="from_station_id" type="hidden" value="<?=@$bus_search_params['from_station_id']?>" >
					</div>
				</div>
				<div class="col-xs-6 padfive full_smal_tab">
					<div class="lablform">To</div>
					<div class="plcetogo locatiomarker ">
                    <span class="maskimg  hfrom"></span>
						<input type="text" required="" value="" placeholder="Type Destination City" id="bus-station-to" class="ft normalinput bus-station bus-station auto-focus form-control b-r-0 valid_class bus-station-to ui-autocomplete-input" name="bus_station_to" autocomplete="on">
						<input class="hide loc_id_holder" name="to_station_id" type="hidden" value="<?=@$bus_search_params['to_station_id']?>" >
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="alert-box" id="bus-alert-box">
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 nopad">
				<div class="col-xs-6 padfive">
					<div class="lablform">Date of Journey</div>
					<div class="plcetogo datemark ">
                    <span class="maskimg   caln"></span>
						<input type="text" class="forminput auto-focus hand-cursor form-control b-r-0" id="bus-date-1" placeholder="dd-mm-yy" value="05-07-2017" name="bus_date_1" required>
					</div>
				</div>
				<div class="col-xs-6 padfive">
					<div class="lablform">&nbsp;</div>
					<div class="searchsbmtfot">
						<input type="submit" id="bus-form-submit" class="searchsbmt" value="search" />
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script>
$("#bus-date-1").datepicker({
    numberOfMonths: 1,
    minDate: 0,
    dateFormat: 'dd-mm-yy'
});

</script>
<script type="text/javascript">
	
</script>
</div>