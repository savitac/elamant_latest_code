 <?php 
$domain_id=$this->session->userdata('branch_id');
$GLOBALS['CI']->load->model('sitemanagement_model');  
$domain_list = $GLOBALS['CI']->sitemanagement_model->get_domain_data($domain_id); 
foreach ($domain_list as  $value) {
  $email=$value->email;
  $phone=$value->phone;
  $facebook=$value->facebook;
  $twitter=$value->twitter;
  $google=$value->google;
  $youtube=$value->youtube;
}

?>   
<?php 

if($view=='flight')
{
  $flight_view['view']="active";
}
elseif($view=='hotel')
{
  $hotel_view['in_view']="active";       
}
elseif($view=='bus')
{
  $bus_view['bus_in_view']="active";       
}
elseif($view=='holiday')
{
  $holiday_data['holiday_view']="active";       
}
elseif ($view=='activity') {
 $activity_view['activity_view']="active";   
} 
elseif ($view=='') {
 $flight_view['view']="active";   
}  
 ?> 
 <!--7P8mrBnrDr9dubVVRhYt -->
 <div class="totopp">  
  <div class="searchtabs">
    <div class="container">
           
      <div class="col-lg-12 col-xs-12 nopad cum_mrg mainsearch">
          <div class="tabbable customtab">
               <!--  <ul class="nav nav-tabs nav-tabs-responsive tabstab">
                    <li  class="flight_tab <?php// echo @$flight_view['view']; ?>"> <a data-toggle="tab" href="#Flight"> <span class="text">  
                    <span class="sprte iconcmn icnhnflight"></span>Flights</span> </a> 
                    </li>
                    <li class="hotel_tab <?php// echo @$hotel_view['in_view']; ?> next"> <a data-toggle="tab" href="#Hotels"> <span class="text"><span class="sprte iconcmn icnhtl"></span>Hotels</span> </a> </li> 
                </ul>  -->
                <ul class="nav nav-tabs nav-tabs-responsive hide_3">
                    
                      <?php  for($prod = 0; $prod < count($products); $prod++) { ?>
                       
                          <li <?php if($prod == 0) { ?> class="active" <?php }elseif($prod == 1){ ?> class="next" <?php } ?> > <a data-toggle="tab" href="#<?php echo $products[$prod]->product_name; ?>"> <span class="text"><span class="<?php echo $products[$prod]->product_icon; ?>"></span>
                            <?php 
                               $product_name = $products[$prod]->product_name; 
                             ?>
                            <?php echo $product_name; ?>
                          </span> </a> </li>
                        
                        <?php  } ?> 
                    </ul> 
             <div class="tab-content">  
                <?php $this->load->view('b2b/flight_search',@$flight_view);?> 
                <?php $this->load->view('b2b/hotel_search',@$hotel_view); ?>  
                <?php $this->load->view('b2b/holiday_search',@$holiday_data);?> 
                <?php $this->load->view('b2b/domestic_search');?> 
                <?php $this->load->view('b2b/bus_search',@$bus_view);?>
                <?php $this->load->view('b2b/activity_search',@$activity_view);?> 
             </div>
          </div>
       </div>  
    </div>
  </div>
</div>
  
<?php if($content!=''){ $divClass = '';for($c = 0 ; $c < count($content); $c++) { 
   if($c == 0){ if(count($content) % 2 == 0){ $divClass = "blockstwo"; }else{ $divClass = "blocksone"; } } 
   if($c != 0){ if($divClass == "blockstwo"){ $divClass = "blocksone"; }else{ $divClass = "blockstwo"; }}   
   if($c == (count($content)- 1)){ $divClass = "blocksone"; } ?>

<?php }} ?> 
<div class="clearfix"></div>

<div class="section htlPckg">
    <div class="container-fluid nopad">
        <!--<div class="description text-center ">
            <h1>Explore Our Latest Hotel Packages</h1> </div>-->

<div class="pagehdwrap"><h2 class="pagehding1">Top Hotel <span class="head_clr">Destinations</span></h2><div class="pagebtm"></div></div>


        <div style="margin-bottom:0px; display:inline-block;"></div>
        <div ng-controller="top_hotel_ctrl">
        <div class="grid image-box style10" ng-repeat="x in hotel_data"> 
           <div class="col-sm-4 col-xs-12 nopad htd-wrap">
               <div class="effect-marley figure">
                  <img class="lazy" src="<?= ASSETS;?>assets/domain/{{x.image}}" alt="{{x.destination}}" style="display: block;">
                  <div class="figcaption">
 
                     <h3 class="clasdstntion">{{x.destination}}</h3>
                     <p>(246 Hotels)</p>
                     <input type="hidden" class="top-des-val hand-cursor"
                value="<?=hotel_suggestion_value("{{x.destination}}","{{x.country}}")?>"><a href="#">{{x.destination}}</a> 
                  </div>
               </div>
              </div>
             </div>  
            </div>
        </div>
    </div>



<div class="clearfix"></div>

      <!-- Top Airliners -->
      <div class="topAirlineOut">
        <div class="container" ng-controller="top_airline">
            <div class="pagehdwrap"><h2 class="pagehding1">Top <span class="head_clr">Airlines</span></h2><div class="pagebtm"></div></div>
            <div class="line"></div>
            <div class="airlinecosmic" ng-controller="top_airline">
              <div id="TopAirLine" class="owl-carousel owl-theme">
                <?php //debug($airlines);
                foreach($airlines as $_v)
                { ?>
                <div class="item"> 
                  <div class="airlinepart" style="line-height: 100px !important;">
                    <img src="<?php echo ASSETS.'assets/domain/'.$_v->airlines_image;?>" alt="">    
                  </div>
                </div>
                  
               <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
<div class="ychoose">
    <div class="container">
        <div class="pagehdwrap">
            <h2 class="pagehding1">Why Book <span class="head_clr">With us</span></h2>
            <div class="pagebtm"></div>
        </div>
        <div class="allys">
            <div class="col-xs-6 col-sm-3 nopad full_mobile">
                <div class="threey">
                    <div class="sprte apritopty sppricegu"></div>
                    <div class="dismany">
                        <div class="hedsprite">Huge Savings<br /></div>
                    </div>
                    <p>Our offered price is proven in the market to convert well, making money for YOU!  



 </p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 nopad full_mobile">
                <div class="threey">
                    <div class="sprte apritopty spsatis"></div>
                    <div class="dismany">
                        <div class="hedsprite">Biggest Selection of Services</div>
                    </div>
                    <p>From flights to luxury hotels to rental cars, we have something for everyone! </p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 nopad full_mobile">
                <div class="threey">
                    <div class="sprte apritopty spsupprt"></div>
                    <div class="dismany">
                        <div class="hedsprite">Easy to Use </div>
                    </div>
                    <p>You don't have to be expert to get our website up and running - it's intuitive and easy to learn. </p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 nopad full_mobile">
                <div class="threey">
                    <div class="sprte apritopty spprt"></div>
                    <div class="dismany">
                        <div class="hedsprite">Help is On Hand</div>
                    </div>
                    <p>A dedicated support team is available to help you with any problems you might have.</p>
                </div>
            </div>
        </div>
    </div>
</div>

        <div class="clearfix"></div>
        <!-- signup -->
        <div class="footertop">
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-xs-12 footertop1 nopad">
                <div class="col-md-6 col-xs-6 footertoplft fwidth">
                  <h3 class="">Sign up</h3>
                  <p class="">Register With Us....</p>
                  <div class="signfomup">
                    <div class="formbtmns">
                      <input type="text" name="email" id="exampleInputEmail2" class="form-control ft_subscribe" value="" required="required" placeholder="Enter Your Email...">
                    </div>
                    <button type="button" class="btn btn_sub subsbtm" onclick="check_newsletter()">Subscribe</button>
                                        <div id="alert-success"></div>
                                        <div class="msgNewsLetterSubsc12" style="display:none;"><h4 style="color:red">Please Enter your Email Id</h4></div>
                                        <div class="succNewsLetterSubsc" style="display:none;"><h4 style="color:green">Thanks for Subscription</h4></div>
                                        <div class="msgNewsLetterSubsc" style="display:none;"><h4 style="color:green">Email Already exists</h4></div>
                                        
                  </div>
                </div>
                <div class="col-md-6 col-xs-6 footertoprgt fwidth">
                  <h3>Follow us</h3>
                  <p>Lorem Ipsum dolor sit amet consectetur.</p>
                  <ul class="list-inline social1">
                    <li class="face_book"><a href="<?php echo $facebook; ?>"><i class="fa fa-facebook"></i></a></li>
                    <li class="twit"><a href="<?php echo @$twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                    <li class="plus"><a href="mailto:<?php echo @$email; ?>"><i class="fa fa-google-plus"></i></a></li> 
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- //GOOGLE ANALYTIC CODE DIFFER FOR EVERY DOMAIN-->
        <?php
        if(!empty(@$site_details->google_analytics_code)) 
        {
          echo html_entity_decode($site_details->google_analytics_code);
        } 
        ?>
<script>
$(document).ready(function(){    
    $(function($) {
    var check_in = db_date(7);    
    var check_out = db_date(10);    
    $('.htd-wrap').on('click', function(e) {
        e.preventDefault();
        var curr_destination = $('.top-des-val', this).val();
        $('#hotel_destination_search_name').val(curr_destination);
        $('#hotel_checkin').val(check_in);
        $('#hotel_checkout').val(check_out);
        $('#hotel_search').submit()
    });
}); 
});</script> 
<script>

     $("#holidaySlider").owlCarousel({
          autoPlay: 3000, //Set AutoPlay to 3 seconds
          items : 5,
          itemsDesktop : [1199,5],
          itemsDesktopSmall : [979,3],
          navigation:true,
          pagination:false
      });
      $("#TopAirLine").owlCarousel({
        items:5,
        loop:true,
        margin:10,
        autoplay:true,
        navigation: true,
        pagination: false,
        autoplayTimeout:1000,
        autoplayHoverPause:true
    });
    </script>
<script type="text/javascript">
 var angular_hotel_data = <?=json_encode(@$top_hotel_destination);?>      
 var angular_airline_data = <?= json_encode($airlines);?> 
</script> 