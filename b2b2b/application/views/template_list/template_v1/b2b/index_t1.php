<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Travel Lights</title>
	<?php echo $this->load->view('core/load_css'); ?>
    <link href="<?php echo ASSETS;?>assets/css/index_t1.css" rel="stylesheet">
	 <link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" /> 
</head>
<body>
	
    <?php 
      $this->session->userdata('user_details_id');
    if($this->session->userdata('user_details_id') == '') { 
	echo $this->load->view('core/header'); 
	} else {
	echo $this->load->view('dashboard/top'); 	 
	 } ?>
	
	<div class="allpagewrp top80">
    	<div class="col-md-5">
        	<span id= "account-error" class="error" for=""> </span>
                <div class="mem_section">
                    <span class="mem_log_txt">
                        Member Login
                    </span>
                   
                    <form  id="login" name="login" method="POST" action="<?php echo base_url(); ?>account/login" role="form" novalidate>
                        <div class="form-group">
                            <div class="selectft">
                                <div class="selectmask msg"></div>
                                <input type="text" class="form-control log_ft" id="email" name="email" placeholder="Email" required>
                                <span id="email-error" class="error" for="email"> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="selectft">
                                <div class="selectmask pass"></div>
                                 <input type="password" class="form-control log_ft" id="password" name="password" placeholder="Password" required>
                                  <span id="password-error" class="error" for="password"> </span>
                            </div>
                           
                        </div>
                      <?php if($this->session->userdata('domain_id') != '') {  ?> 
                        <div class="form-group">
                            <div class="selectft">
                            	<ul class="newradi">
                                	<li>
                                    	<div class="squaredThree1"><input type="radio" name="users" id="userss" value="admin" checked="checked"/>
                                        <label for="userss"></label>
                                        </div>
                                		<label class="remlabel_q" for="userss">Admin</label>
                                    </li>
                                     <?php $access_array = explode(",", $this->session->userdata('required_access') );
                                       if(in_array(5, $access_array)) { ?>
                                    <li>
                                    	<div class="squaredThree1"><input type="radio" name="users" id="userss1" value="users" />
                                        <label for="userss1"></label>
                                        </div>
                                		<label class="remlabel_q" for="userss1">Users</label>
                                    </li>
                                    <?php } ?>
                                     <?php if(in_array(4, $access_array)) { ?>
                                    <li>
                                    	<div class="squaredThree1"><input type="radio" name="users" id="userss2" value="agent" />
                                        <label for="userss2"></label>
                                        </div>
                                		<label class="remlabel_q" for="userss2">Agents</label>
                                    </li>
                                    <?php } ?>
                                </ul>
                            
                             </div>
                        </div>
					<?php  } ?>
                        <div class="for_get">
                            <div class="col-sm-6 nopad">
                                <div class="squared_check"><input type="checkbox" value="tigerair" name="" class="filter_airline" id="squaredchk"><label for="squaredchk"></label></div>
                                <label class="remlabel_q" for="squaredchk">Stay signed in</label>
                            </div>
                            <div class="col-sm-6 nopad text-right for_pass">Forget Password?</div>
                        </div>
                        <div class="clearfix"></div>
                        <button type="submit" class="btn btn_logsub">SIGN IN</button>
                    </form>

                </div>
               
        </div>
    	<div class="col-md-7">
	   <div class="captngrp">
          <div class="bigcaption" id="big1"></div>
          <div class="smalcaptn" id="desc"></div>
          <div class="smalcaptn" id="prce"></div>
        </div> 
        </div>
         <div class="clearfix"></div>
<!-- Why book with us -->
<section>
   <div class="book_us">
      <div class="container">
      		<div class="hedthew">Why Choose Us?</div>
         <div class="col-sm-3 text-center">
            <div class="whyimg"><img src="<?php echo ASSETS; ?>/assets/images/bookwithus_1.png"></div>
            <span class="txt_bookus">Easy Bookings</span>
         </div>
         <div class="col-sm-3 text-center">
            <div class="whyimg"><img src="<?php echo ASSETS; ?>/assets/images/bookwithus_2.png"></div>
            <span class="txt_bookus"> Best Prices and Deals Packages</span>
         </div>
         <div class="col-sm-3 text-center">
            <div class="whyimg"><img src="<?php echo ASSETS; ?>/assets/images/bookwithus_3.png"></div>
            <span class="txt_bookus">100% secure payment</span>
         </div>
         <div class="col-sm-3 text-center">
            <div class="whyimg"><img src="<?php echo ASSETS; ?>/assets/images/bookwithus_4.png"></div>
            <span class="txt_bookus">Customer Satisfaction</span>
         </div>
      </div>
   </div>
</section>
<!-- Why book with us -->

<div class="Budget">
         <div class="container">
            <div class="hedthew">Our Latest News</div>
            <p class="budp">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
</p>
         </div>
      </div>
		   
	</div>
	<?php echo $this->load->view('core/bottom_footer'); ?>

	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/backslider.js"></script> 
	
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/general.js"></script> 
    
	 
		<script type="text/javascript">
		$(document).ready(function(){
		$('.scrolltop').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
		 });
		 
		 
		 
		 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	

	});
	</script> 


<script type="text/javascript">
$(document).ready(function(){
	 
	 //homepage slide show
$(function($){
	
				var url = '<?php echo base_url();?>assets/images/';
				
				
				$.supersized({
				
					// Functionality
					slide_interval          :   5000,		// Length between transitions
					transition              :   1, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed		:	700,		// Speed of transition
															   
					// Components							
					slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
					slides 					:  	[			// Slideshow Images
												{image : url+'/slide2.jpg' ,title : '  Start Your Online' ,description:'travel agency today', price:'<a class="regbtn" href="#" title="">REGISTER FOR B2B</a>'},
												{image : url+'/slide3.jpg' ,title : '  Start Your Online' ,description:'travel agency today', price:'<a class="regbtn" href="#" title="">REGISTER FOR B2B</a>'},
												]
					
				});
		    });

//homepage slide show end

});
</script> 
</body>
</html>
