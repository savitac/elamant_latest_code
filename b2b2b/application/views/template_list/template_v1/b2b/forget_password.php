<?php 
/**
 * Dinesh Kumar Behera
 */
$CI=&get_instance();
$CI->load->library('session');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?=$this->session->userdata('company_name')?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<!-- <link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" /> -->
	<link href="<?php echo ASSETS;?>assets/css/layerslider.css" rel="stylesheet" />
</head>
<body>
    <!-- login section -->
    <div class="log_section">
	
	<div class="background_login">
    <div class="loadcity"></div>
    <div class="clodnsun"></div>
    <div class="reltivefligtgo">
        <div class="flitfly"></div>
    </div>
    <div class="clearfix"></div>
    <div class="busrunning">
        <div class="runbus"></div>
        <div class="runbus2"></div>
        <div class="roadd"></div>
    </div>
</div>
        <div class="container">
            <div class="log_mem">
                <div class="log_logo">
					<?php if($this->session->userdata('site_name') == ''){ ?>    
                    <img src="<?php echo ASSETS.'assets/images/logo.png'; ?>">
                    <?php } else {   ?> 
						<a class="" href="<?php echo base_url();?>"><img src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $this->session->userdata('domain_logo'); ?>" width="150px" height="75px" ></a> 
				    <?php } ?>
                </div>
                <span id= "account-error" class="error" for=""> </span>
                <div class="mem_section">
                    <span class="mem_log_txt">
                        Enter Your Email 
                    </span>
                    <?php
                        if($this->session->flashdata('forget_pass_msg')){
                            $data = $this->session->flashdata('forget_pass_msg');
                            if($data['status'] == 1){ ?>
                                <p class="success" style="color:green; text-align: center;"> <?php echo $data['msg'] ?> </p>
                            <?php }else{ ?>
                                <p class="failure" style="color:red; text-align: center;"> <?php echo $data['msg'] ?> </p>
                            <?php }
                        }
                        
                    ?>
                    <form method="post" role="form" id="form_forgot_password"  action="<?php echo base_url() . 'account/send_password_reset_link'; ?>" onsubmit="send_admin_password_reset_link($(this), event);return false">
                        <div class="form-group">
                            <div class="selectft">
                                <div class="selectmask msg"></div>
                                <input type="text" class="form-control log_ft" id="email" name="email" placeholder="Email" required>
                                <span id="email-error" class="error" for="email"> </span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <button type="submit" class="btn btn_logsub">Send Email Verification</button>
                    </form>

                </div>
                <a href="<?php echo base_url().'home/login'; ?>" class="reg_b">go back</a>
            </div>
        </div>
    </div>
    <!-- login section -->
    
    <!-- copyright section -->
  <?php echo $this->load->view('core/bottom_footer'); ?>
    <!-- copyright section -->
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/backslider.js"></script> 
	
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
</body>
</html>
