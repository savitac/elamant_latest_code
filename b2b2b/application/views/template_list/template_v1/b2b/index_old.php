
 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?=$this->session->userdata('company_name')?></title>
	<?php echo $this->load->view('core/load_css'); ?>
    <link href="<?php echo ASSETS;?>assets/css/index.css" rel="stylesheet"> 
	<link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" />
	<script src="<?php echo ASSETS;?>assets/js/ion.rangeSlider.js"></script>    
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/backslider.js"></script> 
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/pax_count.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/datepicker.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/bootstrap.min.js"></script> 
	<script src="<?php echo ASSETS;?>assets/js/bootstrap.min.js"></script>
	<script>

	 $("#holidaySlider").owlCarousel({
	      autoPlay: 3000, //Set AutoPlay to 3 seconds
	      items : 3,
	      itemsDesktop : [1199,3],
	      itemsDesktopSmall : [979,3],
		  navigation:true,
		  pagination:false
	  });
	</script>	
    <script type="text/javascript">
		$('.movetop').click(function(){
		$('html, body').animate({scrollTop: $('#scrolldown').offset().top }, 'slow');
	});
		
		$("#owl-demo3").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : true
      });
	  
	  $("#owl-demo4").owlCarousel({
		items : 5, 
		itemsDesktop : [1000,4],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
		autoPlay:true,
        navigation : false,
		pagination : false,
		
      });
		
	$(document).ready(function(){
	$('.scrolltop').click(function(){
	$("html, body").animate({ scrollTop: 0 }, 600);
	});

	$("#showLeft").click(function(){
	 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
	});

	$('.navbak').click(function(){
	$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
	});

	});

	$(function($){

	var url = '<?php echo ASSETS; ?>assets/domain/';

	$.supersized({
		slide_interval          :   5000,		// Length between transitions
		transition              :   1, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
		transition_speed		:	700,		// Speed of transition
												   
		// Components							
		slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
		slides 					:  	[	
								<?php 
								foreach ($banner as  $ban) {
								$path=$ban->banner;?>
								{image : url+'<?php echo $path; ?>' ,title : ' ' ,description:'<?php echo $ban->msg; ?>', price:''},
								<?php } ?>

								]
		
	});
	});
	</script> 
</head>
<body>
    <?php 
        if($this->session->userdata('user_logged_in') == 0) { 
       	echo $this->load->view('core/header');  
		} else {
			echo $this->load->view('dashboard/top'); 	 
		}
	 ?> 
	<div class="allpagewrp top80">
		<?php echo $this->load->view('b2b/offer',@$holiday_data,@$airlines,@$products);  ?>	  	   
	</div>
	<?php echo $this->load->view('core/bottom_footer'); ?>
	
	<!-- <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/javascript.js"></script> -->
	<!-- <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery-1.11.0.js"></script>-->
</body>
</html>
