<?php if(isset($flight_search_params['adult_config']) == false || intval($flight_search_params['adult_config']) < 1) {
	$flight_search_params['adult_config'] = 1;
} 
$airline_list = $GLOBALS['CI']->db_cache_api->get_airline_code_list_home();  
?>	

  
	<div id="Flight" class="tab-pane <?php echo $view ?>">
		<form autocomplete="off" onSubmit="return validateform();" action="<?php echo base_url(); ?>flight/pre_flight_search" name="flight_search_form" id="flight_search_form"> 
			<div class="intabs">
			   <div class="waywy">
				<div class="smalway">
					<label class="wament hand-cursor <?php echo $view ?>">
						<input class="hide" type="radio" name="trip_type" id="onew-trp" value="oneway" checked> One Way</label>
						<label class="wament hand-cursor">
						<input class="hide" type="radio" name="trip_type" id="rnd-trp" value="circle"> Roundtrip</label>
						<label class="wament hand-cursor"><input class="hide" type="radio" name="trip_type" id="multi-trp" value="multicity"> Multi City</label></div>
				</div>    			
			  <div class="clearfix"></div>
			  <div class="outsideserach col-md-9 nopad" id="normal">
				<div class="col-lg-4 col-md-6 col-sm-6 fiveh"> 
				  <div class="marginbotom10">
				  <span class=" formlabel">From </span>
				  <div class="relativemask "> <span class="maskimg  hfrom"></span>
					<input type="text" name="from" id="from" value="<?php echo @$onw_rndw_segment_search_params['from'] ?>" placeholder="Airport City" class="fromflight ft" required >
					<input class="hide loc_id_holder" name="from_loc_id" type="hidden" value="<?=@$onw_rndw_segment_search_params['from_loc_id']?>">
				  </div>
				  </div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 fiveh"> 
				  <div class="marginbotom10">
				  <span class=" formlabel">To</span>
				  <div class="relativemask"> <span class="maskimg  hfrom"></span>
					<input type="text" name="to" id="to" value="<?php echo @$onw_rndw_segment_search_params['to'] ?>" placeholder="Airport City" class="arrivalflight ft" required>   
					<input class="loc_id_holder" name="to_loc_id" type="hidden" value="<?=@$onw_rndw_segment_search_params['to_loc_id']?>">
				  </div>
				  </div>
				</div>
				
				<div class="col-md-4 nopad">
				  <div class="col-xs-6 fiveh"> 
					<div class="marginbotom10">
					<span class=" formlabel">Departure</span>
					<div class="relativemask"> <span class="maskimg caln"></span>
					  <input type="text"   id="st_date" name="depature" placeholder="Select Date" value="<?php echo @$onw_rndw_segment_search_params['depature'] ?>" class="forminput" required>
					</div>
					</div> 
				  </div>
				  <div class="col-xs-6 fiveh" id="returns"> 
					<div class="marginbotom10">
					<span class=" formlabel">Return</span>
					<div class="relativemask"> <span class="maskimg caln"></span>
					  <input type="text" id="en_date" name="return" placeholder="Select Date" value="<?php echo @$onw_rndw_segment_search_params['return'] ?>" class="forminput" required>
					</div>
					</div>
				  </div>
				</div>
		</div> 
		<?php echo $this->load->view('share/flight_multi_way_search_1'); ?>
	    <div class="interst_clone_wrapper"></div>
 	<div class="col-xs-3 nopad" >
 <div class="col-xs-6 fiveh">
					<div class="lablform">&nbsp;</div>
					<div class="totlall">
						<span class="remngwd"><span class="total_pax_count">1</span> Traveller(s)</span>
						<div class="roomcount pax_count_div">
							<div class="inallsn">
								<div class="oneroom fltravlr">
									<div class="roomrow">
										<div class="celroe col-xs-4">Adults
											<span class="agemns">(12+)</span>
										</div>
										<div class="celroe col-xs-8">
											<div class="input-group countmore pax-count-wrapper adult_count_div"> <span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="minus" data-field="adult"> <span class="glyphicon glyphicon-minus"></span> </button>
												</span>
												<input type="text" id="OWT_adult" name="adult" class="form-control input-number centertext valid_class pax_count_value" value="<?=(int)@$flight_search_params['adult_config']?>" min="1" max="9" readonly>
												<span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="adult"> <span class="glyphicon glyphicon-plus"></span> </button>
												</span> 
											</div>
										</div>
									</div>
									<div class="roomrow">
										<div class="celroe col-xs-4">Children
											<span class="agemns">(2-11)</span>
										</div>
										<div class="celroe col-xs-8">
											<div class="input-group countmore pax-count-wrapper child_count_div"> <span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="minus" data-field="child"> <span class="glyphicon glyphicon-minus"></span> </button>
												</span>
												<input type="text" id="OWT_child" name="child" class="form-control input-number centertext pax_count_value" value="<?=(int)@$flight_search_params['child_config']?>" min="0" max="9" readonly>
												<span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="child"> <span class="glyphicon glyphicon-plus"></span> </button>
												</span> 
											</div>
										</div>
									</div>
									<div class="roomrow">
										<div class="celroe col-xs-4">Infants
											<span class="agemns">(0-2)</span>
										</div>
										<div class="celroe col-xs-8">
											<div class="input-group countmore pax-count-wrapper infant_count_div"> <span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="minus" data-field="infant"> <span class="glyphicon glyphicon-minus"></span> </button>
												</span>
												<input type="text" id="OWT_infant" name="infant" class="form-control input-number centertext pax_count_value" value="<?=(int)@$flight_search_params['infant_config']?>" min="0" max="9" readonly>
												<span class="input-group-btn">
						search_flight						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="infant"> <span class="glyphicon glyphicon-plus"></span> </button>
												</span> 
											</div>
										</div>
									</div>
									<!-- Infant Error Message-->
									<div class="roomrow">
										<div class="celroe col-xs-8">
										<div class="alert-wrapper hide">
										<div role="alert" class="alert alert-error">
											<span class="alert-content"></span>
										</div>
										</div>
										</div>
									</div>
									<!-- Infant Error Message-->
								</div>
							</div>
						</div>
					</div>
				</div>
<!-- 				<?php 
//Preferred Airlines
		if(isset($flight_search_params['carrier'][0]) == true && empty($flight_search_params['carrier'][0]) == false) {
			$choosen_airline_name = $airline_list[$flight_search_params['carrier'][0]];
		} else {
			$choosen_airline_name = ''.get_legend('Preferred Airline').'';
		}
		$preferred_airlines = '<a class="adscrla choose_preferred_airline" data-airline_code="">All</a>';
		foreach($airline_list as $airline_list_k => $airline_list_v) {
			$preferred_airlines .= '<a class="adscrla choose_preferred_airline" data-airline_code="'.$airline_list_k.'">'.$airline_list_v.'</a>';
		}
 ?> -->
				<div class="col-md-6 nopad">
				   
				<div class="col-md-12 fiveh">
					<div class="formsubmit">  
						<input type="hidden" autocomplete="off" name="carrier[]" id="carrier" value="">
						<input type="hidden"   id="search_flight" name="search_flight" placeholder="Depature Date" value="search" class="forminput">
						<!-- <input type="hidden" autocomplete="off" name="v_class" id="class" value="All"> -->
						<!--<button class="srchbutn comncolor" style="float: right;">Search Flights <span class="srcharow"></span></button>-->

						<div class="searchsbmtfot">
						<input type="submit" class="searchsbmt" value="search" />
					</div>
					</div>
				</div>
				  <div class="col-md-12 col-xs-12 padfive">
			<button style="display:none" class="add_city_btn" id="add_city"> <span class="fa fa-plus"></span> Add City</button>
			</div>
				  </div>  
				 </div>
				 			<div class="clearfix"></div>
			<div class="alert-box" id="flight-alert-box"></div>
			<div class="clearfix"></div>
			<div class="csearch_flightol-xs-10 nopad">
			<div class="togleadvnce">
				<div class="advncebtn">
					<div class="labladvnce">Advanced options</div>
				</div>
				<div class="advsncerdch">
						<div class="col-xs-3 nopad"> 
						<div class="marginbotom10">
						<!-- <span class="formlabel">Class</span> -->
						<div class="selectedwrap alladvnce">
						  <select class="mySelectBoxClass flyinputsnor" name="v_class" >
							<!-- <option>All</option>
							<option>Economy With </option>
							<option>Economy Without Restrictions</option>
							<option>Economy Premium</option>
							<option>Business</option>
							<option>First</option> -->
							<option>All</option>
							<option value="Economy">Economy</option>
							<option value="PremiumEconomy">Premium Economy</option>
							<option value="Business">Business</option>
							<option value="PremiumFusines">Premium Business</option>
							<option value="First">First</option>
						  </select>
						</div>
					  </div>
					  </div>
					<div class="col-xs-3 nopad">
					   <div class="marginbotom10">
					      <div class="alladvnce pr_air">
					         <span class="remngwd" id="choosen_preferred_airline">Preferred Airline</span>
					         <input type="hidden" autocomplete="off" name="carrier[]" id="carrier" value="">
					         <div class="advncedown spladvnce preferred_airlines_advance_div">
					            <div class="inallsnnw">
					               <div class="scroladvc">
					                  <a class="adscrla choose_preferred_airline" data-airline_code="">All</a> 
					            	 <?php

										foreach($airline_list['data'] as $airline_list_k => $airline_list_v) { ?>
										<a class="adscrla choose_preferred_airline" data-airline_code="<?= $airline_list_v['code'] ?>"><?= $airline_list_v['name']."- "; ?><?= $airline_list_v['code'] ?>
					                  </a> 
					                  <?php } ?>
					               </div>
					            </div>
					         </div>
					      </div>
					   </div>
					</div>
				</div>
			</div>
		</div>

				 </div>
	   </form> 
	</div>

     
<?php echo $this->load->view('dashboard/flight_search_prob'); ?>

