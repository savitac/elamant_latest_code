<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php $this->session->userdata('company_name');?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/layerslider.css" rel="stylesheet" />
</head>
<body>
    <!-- login section -->
    <div class="log_section">
		<div class="background_login">
    <div class="loadcity"></div>
    <div class="clodnsun"></div>
    <div class="reltivefligtgo">
        <div class="flitfly"></div>
    </div>
    <div class="clearfix"></div>
    <div class="busrunning">
        <div class="runbus"></div>
        <div class="runbus2"></div>
        <div class="roadd"></div>
    </div>
</div>
        <div class="container">
            <div class="log_mem">

            <?php 
                if($this->session->flashdata("regi_success")) { ?>

<div class="alert alert-success alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
<center><strong ><?=$this->session->flashdata("regi_success")?></strong></center>
  
</div>

                <?php }
            ?>
                <div class="log_logo">
                <!-- <?php# echo $this->session->userdata('user_logged_in')?> -->
                    <?php if($this->session->userdata('site_name') == ''){ ?>    
                    <img src="<?php echo ASSETS.'assets/images/logo.png'; ?>">
                    <?php } else {   ?> 
                        <a class="" href="<?php echo base_url();?>"><img src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $this->session->userdata('domain_logo'); ?>"></a> 
                    <?php } ?>
                </div>
                <span id= "account-error" class="error" for=""> </span>
                <div class="mem_section">
                    <span class="mem_log_txt">
                        Member Login
                    </span>
                   
                    <form  id="login" name="login" method="POST" action="<?php echo base_url(); ?>index.php/account/login" role="form" novalidate>
                        <div class="form-group">
                            <div class="selectft">
                                <div class="selectmask msg"></div>
                                <input type="text" class="form-control log_ft" id="email" name="email" placeholder="Email" required>
                                <span id="email-error" class="error" for="email"> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="selectft">
                                <div class="selectmask pass"></div>
                                 <input type="password" class="form-control log_ft" id="password" name="password" placeholder="Password" required>
                                  <span id="password-error" class="error" for="password"> </span>
                            </div>
                           
                        </div>
                      <?php 
                     /* echo $this->session->userdata('domain_id');die();*/
                      if($this->session->userdata('domain_id') != '') {  ?> 
                        <div class="form-group">
                            <div class="selectft">
                            	<ul class="newradi">
                                     <?php $access_array = explode(",", $this->session->userdata('required_access') );
                                       if(in_array(5, $access_array)) { ?>
                                    <li>
                                    	<div class="squaredThree1"><input type="radio" name="users" id="userss1" value="users" />
                                        <label for="userss1"></label>
                                        </div>
                                		<label class="remlabel_q" for="userss1">Users</label>
                                    </li>
                                    <?php } ?>
                                     <?php if(in_array(4, $access_array)) { ?>
                                    <li>
                                    	<div class="squaredThree1"><input type="radio" name="users" id="userss2" value="agent" />
                                        <label for="userss2"></label>
                                        </div>
                                		<label class="remlabel_q" for="userss2">Agents</label>
                                    </li>
                                    <?php } ?>
                                    <li>
                                        <div class="squaredThree1"><input type="radio" name="users" id="userss" value="admin" checked="checked"/>
                                        <label for="userss"></label>
                                        </div>
                                        <label class="remlabel_q" for="userss">Admin</label>
                                    </li>
                                </ul>
                            
                             </div>
                        </div>
					<?php  } ?>
                        <div class="for_get">
                            <div class="col-sm-6 nopad">
                                <div class="squared_check"><input type="checkbox" value="tigerair" name="" class="filter_airline" id="squaredchk"><label for="squaredchk"></label></div>
                                <label class="remlabel_q" for="squaredchk">Stay signed in</label>
                            </div>
                            <div class="col-sm-6 nopad text-right for_pass"><a href="<?php echo base_url(); ?>home/forget_password">Forget Password?</a></div>
                        </div>
                        <div class="clearfix"></div>
                        <button type="submit" class="btn btn_logsub">SIGN IN</button>
                    </form>

                </div>
                <a href="<?php echo base_url().'account/createAccount'; ?>" class="reg_b">Create Account</a>
            </div>
        </div>
    </div>
    <!-- login section -->
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/backslider.js"></script> 
	
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
</body>
</html>
