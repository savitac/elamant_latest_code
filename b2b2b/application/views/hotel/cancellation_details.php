<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title><?= $this->session->userdata('company_name')?></title>
<?php echo $this->load->view('core/load_css'); ?>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="
https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<link href="<?php echo ASSETS;?>assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Navigation -->

<?php echo $this->load->view('dashboard/top'); ?> 
<?php echo $this->load->view('reports/email_reply_popup');?>
<?php 
$booking_details = $data ['booking_details'] [0];
extract($booking_details);
if(isset($_GET['error_msg']) == true && empty($_GET['error_msg']) == false) {
	$status_message = trim($_GET['error_msg']);
} else if($status == 'BOOKING_CANCELLED' || intval($cancellation_details[0]['ChangeRequestStatus']) >= 1) {
	$status_message = 'Your Cancellation Request has been sent, <br />our Representatives will process further';
} else {
	$status_message = 'Some thing went wrong Please Try Again !!!';
}
?>
<div class="content-wrapper dashboard_section">
	<div class="container">
	<div class="staffareadash">
		<!-- <?php //echo $GLOBALS['CI']->template->isolated_view('share/profile_navigator_tab') ?> -->
		<div class="bakrd_color">
		
<div class="search-result">
	<div class="container">
		<div class="confir_can">
			<div class="can_msg"><?=$status_message?></div>
			<div class="col-xs-12 nopad">
				<div class="marg_cans">
					<div class="bookng_iddis">Booking ID
					<span class="down_can"><?=$app_reference?></span></div>
				</div>
			</div>
			<!--
			<div class="col-xs-6 nopad">
				<div class="marg_cans">
					<div class="bookng_iddis">Refund Amount 
					<span class="down_can"><span class="fa fa-rupee"></span>2,456</span></div>
				</div>
			</div>
			 -->
		</div>
		
		
	</div>
</div>
  </div>
		
	</div>
</div>
</div>