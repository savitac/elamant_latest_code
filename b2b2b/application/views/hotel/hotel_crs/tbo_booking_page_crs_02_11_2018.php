<style type="text/css">
	.discount_total_pay_user, span.discount_currency_only {
    font-weight: 500;
    color: #f58830;
    font-size: 18px;
}
</style>
<?php
$language= $this->session->userdata['language'];
$module_value = md5('hotel'); 
$currency_symbol = $this->currency->get_currency_symbol($pre_booking_params['default_currency']);

$CI = &get_instance (); 
$template_images = $GLOBALS ['CI']->template->template_images ();
$mandatory_filed_marker = '<sup class="text-danger">*</sup>';
$hotel_checkin_date = hotel_check_in_out_dates ( $search_data ['from_date'] );
$hotel_checkin_date = explode ( '|', $hotel_checkin_date );
$hotel_checkout_date = hotel_check_in_out_dates ( $search_data ['to_date'] );
$hotel_checkout_date = explode ( '|', $hotel_checkout_date );
if (is_logged_in_user ()) {
	$review_active_class = ' success ';
	$review_tab_details_class = '';
	$review_tab_class = ' inactive_review_tab_marker ';
	$travellers_active_class = ' active ';
	$travellers_tab_details_class = ' gohel ';
	$travellers_tab_class = ' travellers_tab_marker ';
} else {
	$review_active_class = ' active ';
	$review_tab_details_class = ' gohel ';
	$review_tab_class = ' review_tab_marker ';
	$travellers_active_class = '';
	$travellers_tab_details_class = '';
	$travellers_tab_class = ' inactive_travellers_tab_marker ';
}
$passport_issuing_country = INDIA_CODE;
$temp_passport_expiry_date = date ( 'Y-m-d', strtotime ( '+5 years' ) );
$static_passport_details = array ();
$static_passport_details ['passenger_passport_expiry_day'] = date ( 'd', strtotime ( $temp_passport_expiry_date ) );
$static_passport_details ['passenger_passport_expiry_month'] = date ( 'm', strtotime ( $temp_passport_expiry_date ) );
$static_passport_details ['passenger_passport_expiry_year'] = date ( 'Y', strtotime ( $temp_passport_expiry_date ) );
//$hotel_total_price = roundoff_number ( $pre_booking_params['price'] );

//Added by abi
// debug($total_price);exit;
$hotel_total_price = get_converted_currency_value ( $currency_obj->force_currency_conversion ($total_price) );

/**
 * ******************************* Convenience Fees ********************************
 */
$subtotal = $hotel_total_price;
$pre_booking_params ['convenience_fees'] = '0';
$hotel_total_price = roundoff_number ( $pre_booking_params ['convenience_fees'] + $hotel_total_price );

$RoomTypeName = $pre_booking_params['room_type_name'];
/**
 * ******************************* Convenience Fees ********************************
 */
Js_Loader::$js [] = array (
		'src' => $GLOBALS ['CI']->template->template_js_dir ( 'provablib.js' ),
		'defer' => 'defer' 
);
$ret_data = room_price_details($pre_booking_params, $search_data, $currency_obj,$hotel_data);	
$cancellation_policy = isset($pre_booking_params['CancellationDetails'][0])?$pre_booking_params['CancellationDetails'][0]->cancellation_description:"Non-Refundable";

$total_adult_count	= array_sum($search_data['adult_config']);
$total_child_count	= array_sum($search_data['child_config']);
$no_adult='No of Adults';
	$no_child = 'No of Childs';
	if($total_adult_count==1){
		$no_adult='No of Adult';			      			
	}
	if($total_child_count==1 || $total_child_count ==0){
		$no_child = 'No of Child';
	}
?>
<style>
	.topssec::after{display:none;}
</style>
<div class="fldealsec">
	<div class="container">
		<div class="tabcontnue">
			<div class="col-xs-4 nopadding">
				<div class="rondsts <?=$review_active_class?>">
					<a class="taba core_review_tab <?=$review_tab_class?>" id="stepbk1">
						<div class="iconstatus fa fa-eye"></div>
						<div class="stausline">Review</div>
					</a>
				</div>
			</div>
			<div class="col-xs-4 nopadding">
				<div class="rondsts <?=$travellers_active_class?>">
					<a class="taba core_travellers_tab <?=$travellers_tab_class?>"
						id="stepbk2">
						<div class="iconstatus fa fa-group"></div>
						<div class="stausline">Travellers</div>
					</a>
				</div>
			</div>
			<div class="col-xs-4 nopadding">
				<div class="rondsts">
					<a class="taba" id="stepbk3">
						<div class="iconstatus fa fa-money"></div>
						<div class="stausline">Payments</div>
					</a>
				</div>
			</div>
		</div>

	</div>
</div>
<?php 
$roomdetail = $this->hotels_model->get_room_details($price_data->hotel_details_id,$price_data->hotel_room_type_id); 
$roomdetail = $roomdetail->row();
$hotelimage = base_url().'supervision/uploads/hotel_images'.'/'.$hotel_data->thumb_image;

?>
<div class="clearfix"></div>
<div class="alldownsectn">
	<div class="container">
  <div class="ovrgo">
	<div class="bktab1 xlbox <?=$review_tab_details_class?>">
		<div class="col-xs-8 toprom nopad">
		  <div class="col-xs-12 nopad full_room_buk">
			<div class="bookcol">
			  <div class="hotelistrowhtl">
				<div class="col-md-4 nopad xcel">
				  <div class="imagehotel">
				  		<img alt="HotelImage" src="<?=$hotelimage?>">
				  </div>
				</div>
				<div class="col-md-12 padall10 xcel">
				  <div class="hotelhed"><?=$hotel_data->hotel_name; ?>
                     
				  </div>
				  <div class="clearfix"></div>
				  <div class="bokratinghotl rating-no">				   
					  <?=print_star_rating($hotel_data->star_rating)?>
				  </div>
				  <div class="clearfix"></div>
				  <div class="mensionspl"><?=$hotel_data->hotel_address; ?>
				  </div>
				  <div class="sckint">
				  <div class="ffty">
					<div class="borddo brdrit"> <span class="lblbk_book">
					<span class="fa fa-calendar"></span>
					Check-in</span>
					  <div class="fuldate_book"> <span class="bigdate_book"><?=$hotel_checkin_date[0]?></span>
						<div class="biginre_book"> <?=$hotel_checkin_date[1]?><br>
						  <?=$hotel_checkin_date[2]?> </div>
					  </div>
					</div>
				  </div>
				  <div class="ffty">
					<div class="borddo"> <span class="lblbk_book"> <span class="fa fa-calendar"></span> Check-out</span>
					  <div class="fuldate_book"> <span class="bigdate_book"><?=$hotel_checkout_date[0]?></span>
						<div class="biginre_book"> <?=$hotel_checkout_date[1]?><br>
						  <?=$hotel_checkout_date[2]?> </div>
					  </div>
					</div>
				  </div>
				  <div class="clearfix"></div>
				  <div class="nigthcunt">Night(s) <?=$search_data['no_of_nights']?>,Room(s) <?=$search_data['room_count']?></div>
				</div>
				</div>
			  </div>
			</div>
		  </div>
		  
	<div class="col-xs-12 nopadding full_log_tab">
	  <div class="fligthsdets">
		<div class="flitab1">
				<div class="clearfix"></div>
				<div class="sepertr"></div>
				<div class="sepertr"></div>
				<!-- LOGIN SECTION STARTS -->
				<?php if(is_logged_in_user() == false) { ?>
				<div class="loginspld">
					<div class="logininwrap">
					<div class="signinhde">
						Sign in now to Book Online
					</div>
					<div class="newloginsectn">
						<div class="col-xs-7 celoty nopad">
							<div class="insidechs">
								<div class="mailenter">
									<input type="text" name="booking_user_name" id="booking_user_name"  placeholder="Your mail id" class="newslterinput nputbrd _guest_validate" maxlength="80">
								</div>  
								<div class="noteinote">Your booking details will be sent to this email address.</div>
								<div class="clearfix"></div>
								<div class="havealrdy">
									<div class="squaredThree">
									  <input id="alreadyacnt" type="checkbox" name="check" value="None">
									  <label for="alreadyacnt"></label>
									</div>
									<label for="alreadyacnt" class="haveacntd">I have an Account</label>
								</div>
								<div class="clearfix"></div>
								<div class="twotogle">
								<div class="cntgust">
									<div class="phoneumber">
										<div class="col-xs-3 nopadding">
												<!-- <input type="text" placeholder="+91" class="newslterinput nputbrd"> -->
											<select name="" class="newslterinput nputbrd _numeric_only" id="before_country_code"  required>
												<?php 
												//debug($phone_code);exit;
												echo diaplay_phonecode($phone_code,$active_data, $user_country_code); ?>
											</select> 
										</div>
										<div class="col-xs-1 nopadding"><div class="sidepo">-</div></div>
										<div class="col-xs-8 nopadding">
											<input type="text" id="booking_user_mobile" placeholder="Mobile Number" class="newslterinput nputbrd _numeric_only _guest_validate" maxlength="10">
										</div>
										<div class="clearfix"></div>
										<div class="noteinote">We'll use this number to send possible update alerts.</div>
									</div>
									<div class="clearfix"></div>
									<div class="continye col-xs-8 nopad">
										<button class="bookcont" id="continue_as_guest">Book as Guest</button>
									</div>
								</div>
								<div class="alrdyacnt">
									<div class="col-xs-12 nopad">
										 <div class="relativemask"> 
											<input type="password" name="booking_user_password" id="booking_user_password" class="clainput" placeholder="<?php echo $this->lang->line('password');?>" />
										 </div>
										 <div class="clearfix"></div>
										 <a class="frgotpaswrd"><?php echo $this->lang->line('forgot_password');?></a>
										 <div style="" class="hide alert alert-danger"></div>
									</div>
									
									<div id="book_login_auth_loading_image" style="display: none">
										<?=$book_login_auth_loading_image?>
									</div>
														
									<div class="clearfix"></div>
									<div class="continye col-xs-8 nopad">
										<button class="bookcont" id="continue_as_user">Proceed to Book</button>
									</div>
								</div>
								</div>
							</div>
						</div>
						 <?php $no_social=no_social(); if($no_social != 0) {?>
						<div class="col-xs-2 celoty nopad linetopbtm">
								<div class="orround"><?php echo $this->lang->line('book_or');?></div>
						</div>
						<?php } ?>
						<div class="col-xs-5 celoty">
							<div class="insidechs booklogin">
								<div class="leftpul">
								<?php 
									$social_login1 = 'facebook';
									$social1 = is_active_social_login($social_login1);
									if($social1){
										$GLOBALS['CI']->load->library('social_network/facebook');
										echo $GLOBALS['CI']->facebook->login_button ();
									} 
									$social_login2 = 'twitter';
									$social2 = is_active_social_login($social_login2);
									if($social2){
									?>
									<a class="logspecify tweetcolor"><span class="fa fa-twitter"></span>
									<div class="mensionsoc">Login with Twitter</div>
									</a>
								<?php } 
									$social_login3 = 'googleplus';
									$social3= is_active_social_login($social_login3);
									if($social3){
										$GLOBALS['CI']->load->library('social_network/google');
										echo $GLOBALS['CI']->google->login_button ();
									} ?>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			<?php } ?>
				<!-- LOGIN SECTION ENDS -->
				</div>
				</div>
		 </div>
		  <div class="col-xs-4 nopad full_room_buk hide">
			<div class="sckint">
			  <div class="ffty">
				<div class="borddo brdrit"> <span class="lblbk_book">
				<span class="fa fa-calendar"></span>
				<?php echo $this->lang->line('check_in');?></span>
				  <div class="fuldate_book"> <span class="bigdate_book"><?=$hotel_checkin_date[0]?></span>
					<div class="biginre_book"> <?=$hotel_checkin_date[1]?><br>
					  <?=$hotel_checkin_date[2]?> </div>
				  </div>
				</div>
			  </div>
			  <div class="ffty">
				<div class="borddo"> <span class="lblbk_book"> <span class="fa fa-calendar"></span> <?php echo $this->lang->line('check_out');?></span>
				  <div class="fuldate_book"> <span class="bigdate_book"><?=$hotel_checkout_date[0]?></span>
					<div class="biginre_book"> <?=$hotel_checkout_date[1]?><br>
					  <?=$hotel_checkout_date[2]?> </div>
				  </div>
				</div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="nigthcunt">Night(s)<?=$search_data['no_of_nights']?>, Room(s)<?=$search_data['room_count']?></div>
			</div>
		  </div>
		</div>
		<div class="col-xs-4 full_room_buk rhttbepa">
		   <div id="" style="">
		 	<table class="table table-condensed tblemd">
			 	<tbody>
			 	  <tr class="rmdtls">
			   		 <th colspan="2">Room Details</th>
			      </tr>
			      <tr>
			         <th>Room Type</th>
			        <td><?=$RoomTypeName?></td>
			      </tr>
			      <tr class="aminitdv">
			      <th>Board Type</th>
			       <td>
			        	
			        	<?php if($Boardingdetails):?>			         		
		         		<?php  
		         			$am_arr = array();
		         			foreach ($Boardingdetails as $b_key => $b_value) {
		         				$am_arr[]=$b_value;
		         			}
		         			echo implode(",",$am_arr);
			         		
		         		?>
			        	
		         	<?php else:?>
		         			<span>Room Only</span>
			        	<?php endif;?>
			        </td>
			      </tr>
			      <tr>
			       <?php
				         $total_pax = array_sum($search_data['adult_config'])+array_sum($search_data['child_config']);
				        ?>

			        <th>No of Guest</th>
			        <td><?=$total_pax?></td>
			      </tr>
			        <tr>

			        <th><?=$no_adult?></th>
			        <td><?=array_sum($search_data['adult_config'])?></td>
			      </tr>
			      <tr>
			        <th><?=$no_child?></th>
			        <td><?=array_sum($search_data['child_config'])?></td>
			      </tr>
			      
				 <?php if($LastCancellationDate):?>
				      <tr class="frecanpy">
				        <th>Free Cancellation till:<br/><a  href="#" data-target="#roomCancelModal"  data-toggle="modal" >View Cancellation Policy</a></th>
				        <td><?=local_month_date($LastCancellationDate)?></td>
				        
				      </tr>
				  <?php else:?>
				  		<tr class="frecanpy">
				        <th>Cancellation Policy:<br/><a  href="#" data-target="#roomCancelModal"  data-toggle="modal" >View Cancellation Policy</a></th>
				        <td>Non-Refundable</td>
				      </tr>
			 	 <?php endif;?>
			 
			      <tr>
			     <tr>
			        <th>Total Price</th>
			        <td><?=$this->currency->get_currency_symbol($pre_booking_params['default_currency'])?> <?=($ret_data['grand_total'])?></td>
			      </tr>
			      <tr class="texdiv">
			        <th>Taxes & Service fee</th>
			        <td><?=$this->currency->get_currency_symbol($pre_booking_params['default_currency'])?> <?=$ret_data['tax_total']?></td>
			      </tr>
			      <tr class="grd_tol">
			        <th>Grand Total</th>
			        <td><?=$this->currency->get_currency_symbol($pre_booking_params['default_currency'])?> <?=($ret_data['grand_total'])?></td>
			      </tr>
			    </tbody>
			  </table>
		   </div>
		  </div>
		<div class="clearfix"></div>
		<div class="clearfix"></div>
	</div>
	<div class="bktab2 xlbox <?=$travellers_tab_details_class?>">
	  <div class="col-xs-8 nopad">
		<div class="col-xs-12 topalldesc">
			<div class="col-xs-12 nopad">
				<div class="bookcol">
			  <div class="hotelistrowhtl">
				<div class="col-md-4 nopad xcel">
				  <div class="imagehotel">
					  		<img alt="HotelImage" src="<?=$hotelimage?>">
				</div>
				</div>
				<div class="col-md-8 padall10 xcel">
				  <div class="hotelhed">
				  	<?=$hotel_data->hotel_name; ?>
				  </div>
				  <div class="clearfix"></div>
				  <div class="bokratinghotl rating-no">				   
					   <?=print_star_rating($hotel_data->star_rating)?>
				  </div>
				  <div class="clearfix"></div>
				  <div class="mensionspl">
				  	<?=$hotel_data->hotel_address; ?>
				   </div>
				  <div class="bokkpricesml">
					<div class="travlrs"><span class="travlrsnms">Travelers:</span><span class="fa fa-male"></span> <?=array_sum($search_data['adult_config'])?><span class="fa fa-child"></span> <?=array_sum($search_data['child_config'])?></div>
					<div class="totlbkamnt grandtotal"> <span class="ttlamtdvot">Total Amount</span><?=$this->currency->get_currency_symbol($pre_booking_params['default_currency'])?> <?=($hotel_total_price)?>/-</div>
					</div>
				</div>
			  </div>
			</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12 padpaspotr">
		<div class="col-xs-12 nopadding">
		<div class="fligthsdets">
		<?php
/**
 * Collection field name 
 */
//Title, Firstname, Middlename, Lastname, Phoneno, Email, PaxType, LeadPassenger, Age, PassportNo, PassportIssueDate, PassportExpDate
$total_adult_count	= array_sum($search_data['adult_config']);
$total_child_count	= array_sum($search_data['child_config']);

//------------------------------ DATEPICKER START
$i = 1;
$datepicker_list = array();
if ($total_adult_count > 0) {
	for ($i=1; $i<=$total_adult_count; $i++) {
		$datepicker_list[] = array('adult-date-picker-'.$i, ADULT_DATE_PICKER);
	}
}

if ($total_child_count > 0) {
	for ($i=$i; $i<=($total_child_count+$total_adult_count); $i++) {
		$datepicker_list[] = array('child-date-picker-'.$i, CHILD_DATE_PICKER);
	}
}
$GLOBALS['CI']->current_page->set_datepicker($datepicker_list);
//------------------------------ DATEPICKER END
$total_pax_count	= $total_adult_count+$total_child_count;
//First Adult is Primary and and Lead Pax
$adult_enum = $child_enum = get_enum_list('title');
$gender_enum = get_enum_list('gender');
unset($adult_enum[MASTER_TITLE]); // Master is for child so not required
unset($child_enum[MASTER_TITLE]); // Master is not supported in TBO list
unset($adult_enum[MISS_TITLE]); // Miss is not supported in GRN list
unset($child_enum[MISS_TITLE]);
unset($child_enum[C_MRS_TITLE]);
unset($adult_enum[A_MASTER]);
$adult_title_options = generate_options($adult_enum, false, true);
$child_title_options = generate_options($child_enum, false, true);
$gender_options	= generate_options($gender_enum);
$nationality_options = generate_options($iso_country_list, array(INDIA_CODE));//FIXME get ISO CODE --- ISO_INDIA
$passport_issuing_country_options = generate_options($country_list);
//lowest year wanted
$cutoff = date('Y', strtotime('+20 years'));
//current year
$now = date('Y');
$day_options	= generate_options(get_day_numbers());
$month_options	= generate_options(get_month_names());
$year_options	= generate_options(get_years($now, $cutoff));
/**
 * check if current print index is of adult or child by taking adult and total pax count
 * @param number $total_pax		total pax count
 * @param number $total_adult	total adult count
 */
function is_adult($total_pax, $total_adult)
{
	return ($total_pax>$total_adult ?	false : true);
}

/**
 * check if current print index is of adult or child by taking adult and total pax count
 * @param number $total_pax		total pax count
 * @param number $total_adult	total adult count
 */
function is_lead_pax($pax_count)
{
	return ($pax_count == 1 ? true : false);
}
$lead_pax_details = @$pax_details[0];
 ?>
		<form action="<?=base_url().'index.php/hotels/pre_bookings/'.$search_data['search_id']?>" method="POST" autocomplete="off">
		<div class="hide">
			<?php $dynamic_params_url = serialized_data($pre_booking_params);?>
			<input type="hidden" required="required" name="token"		value="<?=$dynamic_params_url;?>" />
			<input type="hidden" required="required" name="token_key"	value="<?=md5($dynamic_params_url);?>" />
			<input type="hidden" required="required" name="op"			value="book_flight">
			<input type="hidden" required="required" name="booking_source"		value="<?=$booking_source?>" readonly>
			<input type="hidden" required="required" name="promo_code_discount_val" id="promo_code_discount_val" value="0.00" readonly>
			<input type="hidden" required="required" name="promo_code" id="promocode_val" value="" readonly>
		</div>
			 <div class="flitab1">
			<div class="moreflt boksectn">
					<div class="ontyp">
						<div class="labltowr arimobold"><?php echo $this->lang->line('please_enter_customer_name');?></div>

<?php
	$child_age = @$search_data['child_age'];
	if(is_logged_in_user()) {
		$traveller_class = ' user_traveller_details ';
	} else {
		$traveller_class = '';
	}
	for($pax_index=1; $pax_index <= $total_pax_count; $pax_index++) {//START FOR LOOP FOR PAX DETAILS
	$cur_pax_info = is_array($pax_details) ? array_shift($pax_details) : array();
?>
	<div class="pasngrinput _passenger_hiiden_inputs">
		<div class="hide hidden_pax_details">
		<?php
		if(is_adult($pax_index, $total_adult_count) == true) {
			 $static_date_of_birth = date('Y-m-d', strtotime('-30 years'));;
			 } else {//child
			 	$static_date_of_birth = date('Y-m-d', strtotime('-'.intval(array_shift($child_age)).' years'));;
			 }
			 $passport_number = rand(1111111111,9999999999);
		  ?>
			<input type="hidden" name="passenger_type[]" value="<?=(is_adult($pax_index, $total_adult_count) ? 1 : 2)?>">
			<input type="hidden" name="lead_passenger[]" value="<?=(is_lead_pax($pax_index) ? true : false)?>">
			<input type="hidden" name="date_of_birth[]" value="<?=$static_date_of_birth?>">
			<input type="hidden" name="gender[]" value="1" class="pax_gender">
			<input type="hidden" required="required" name="passenger_nationality[]" id="passenger-nationality-<?=$pax_index?>" value="92">
			<!-- Static Passport Details -->
			<input type="hidden" name="passenger_passport_number[]" value="<?=$passport_number?>" id="passenger_passport_number_<?=$pax_index?>">
			<input type="hidden" name="passenger_passport_issuing_country[]" value="<?=$passport_issuing_country?>" id="passenger_passport_issuing_country_<?=$pax_index?>">
			<input type="hidden" name="passenger_passport_expiry_day[]" value="<?=$static_passport_details['passenger_passport_expiry_day']?>" id="passenger_passport_expiry_day_<?=$pax_index?>">
			<input type="hidden" name="passenger_passport_expiry_month[]" value="<?=$static_passport_details['passenger_passport_expiry_month']?>" id="passenger_passport_expiry_month_<?=$pax_index?>">
			<input type="hidden" name="passenger_passport_expiry_year[]" value="<?=$static_passport_details['passenger_passport_expiry_year']?>" id="passenger_passport_expiry_year_<?=$pax_index?>">
		</div>
		<div class="col-xs-2 nopadding">
		    <div class="adltnom"><?=(is_adult($pax_index, $total_adult_count) ? "Adult" : "Child")?> <?=(is_lead_pax($pax_index) ? $GLOBALS['CI']->lang->line('lead_pax') : '')?></div>
		 </div>
		 <div class="col-xs-10 nopadding">
		 <div class="inptalbox">
			<div class="col-xs-3 spllty">
			<div class="selectedwrap">
			<select class="mySelectBoxClass flyinputsnor name_title" name="name_title[]" required>
			<?php echo (is_adult($pax_index, $total_adult_count) ? $adult_title_options : $child_title_options)?>
			</select>
			</div>
			</div>
			<div class="col-xs-4 spllty">
				  <input value="<?=@$cur_pax_info['first_name']?>" required="required" type="text" name="first_name[]" id="passenger-first-name-<?=$pax_index?>" class="clainput alpha_space <?=$traveller_class?>"  minlength="2" maxlength="45" placeholder="<?php echo $this->lang->line('enter_first_name');?>" data-row-id="<?=($pax_index);?>"/>
				  <input type="hidden" class="hide"  maxlength="45" name="middle_name[]">
			</div>
			<div class="col-xs-4 spllty">
			 	<input value="<?=@$cur_pax_info['last_name']?>" required="required" type="text" name="last_name[]" id="passenger-last-name-<?=$pax_index?>" class="clainput alpha_space last_name" minlength="2" maxlength="45" placeholder="<?php echo $this->lang->line('enter_last_name');?>" />
			 </div>
		</div>
		</div>
	</div>
<?php
}//END FOR LOOP FOR PAX DETAILS
?>
	</div>
</div>
			<div class="ontyp">
				<div class="kindrest">
					<div class="cartlistingbuk">
						<div class="cartitembuk">
							<div class="col-md-12">
								<div class="payblnhmxm">Have an e-coupon or a deal-code ? (Optional)
<!-- <?php //echo $this->lang->line('have_promocode'); ?> --></div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="cartitembuk prompform">
							<form name="promocode" id="promocode" novalidate>
		                    <div class="col-md-4 col-xs-6 nopadding_right">
		                      <div class="cartprc">
		                        <div class="payblnhm singecartpricebuk ritaln">
		                    	 <input type="text" placeholder="Please Enter Promo code<?php //echo $this->lang->line('please_enter_promo_code'); ?>" name="code" id="code" class="promocode" aria-required="true" />
		                          <input type="hidden" name="module_type" id="module_type" class="promocode" value="<?=@$module_value;?>" />
		                          <input type="hidden" name="total_amount_val" id="total_amount_val" class="promocode" value="<?=@$subtotal;?>" />
		                          <input type="hidden" name="convenience_fee" id="convenience_fee" class="promocode" value="<?=@$convenience_fees;?>" />
		                          <input type="hidden" name="currency_symbol" id="currency_symbol" value="<?=@$currency_symbol;?>" />
		                          <input type="hidden" name="currency" id="currency" value="<?=@$pre_booking_params['default_currency'];?>" />
		                         
		                          <p class="error_promocode text-danger"></p>                     
		                        </div>
		                      </div>
		                    </div>
		                    <div class="col-md-2 col-xs-3 nopadding_left">
		                      <input type="button" value="Apply<?php //echo $this->lang->line('prebook_apply'); ?>" name="apply" id="apply" class="promosubmit">
		                    </div>
		                  </form>
						</div>
						<div class="loading hide" id="loading"><img src="<?php echo $GLOBALS['CI']->template->template_images('loader_v3.gif')?>"></div>
						<div class="clearfix"></div>
						<div class="savemessage"></div>
					</div>
				</div>
				</div>
				<div class="clearfix"></div>
				<div class="contbk">
					<div class="contcthdngs">Contact Details</div>
					<div class="hide">
					<input type="hidden" name="billing_country" value="92">
					<input type="hidden" name="billing_city" value="test">
					<input type="hidden" name="billing_zipcode" value="test">
					<input type="hidden" name="billing_address_1" value="test">
					</div>
					<div class="col-xs-8 nopad">
					<div class="col-xs-12 nopadding">
					<select name="country_code" class="newslterinput nputbrd _numeric_only " id="after_country_code" required>
					<?php echo diaplay_phonecode($phone_code,$active_data, $user_country_code); ?>
										</select> 
					</div>
					<div class="col-xs-12"><div class="sidepo">-</div></div>
					<div class="col-xs-12 nopadding">
					<input value="<?=@$lead_pax_details['phone'] == 0 ? '' : @$lead_pax_details['phone'];?>" type="text" name="passenger_contact" id="passenger-contact" placeholder="<?php echo $this->lang->line('mobile_number');?>" class="newslterinput nputbrd _numeric_only" maxlength="10" required="required">
					</div>
					<div class="clearfix"></div>
					<div class="emailperson col-xs-12 nopad xyz">
					<input value="<?=@$lead_pax_details['email']?>" type="text" maxlength="80" required="required" id="billing-email" class="newslterinput nputbrd" placeholder="<?php echo $this->lang->line('enter_your_email');?>" name="billing_email">
					</div>
					</div>
					<div class="clearfix"></div>
					<div class="notese"><?php echo $this->lang->line('mobile_no_used_for_communication');?></div>
				</div>
				<div class="clikdiv">
					 <div class="squaredThree">
					 <input id="terms_cond1" type="checkbox" name="tc" required="required">
					 <label for="terms_cond1"></label>
					 </div>
					 <span class="clikagre">
					 	Terms & Conditions
					 </span>
				</div>
				<div class="clearfix"></div>
				<div class="loginspld">
					<div class="collogg">
						<?php
						//If single payment option then hide selection and select by default
						if (count($active_payment_options) == 1) {
							$payment_option_visibility = 'hide';
							$default_payment_option = 'checked="checked"';
						} else {
							$payment_option_visibility = 'show';
							$default_payment_option = '';
						}
						?>
						<div class="row <?=$payment_option_visibility?>">
							<?php if (in_array(PAY_NOW, $active_payment_options)) {?>
								<div class="col-md-3">
									<div class="form-group">
										<label for="payment-mode-<?=PAY_NOW?>">
											<input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_NOW?>" id="payment-mode-<?=PAY_NOW?>" class="form-control b-r-0" placeholder="Payment Mode">
											Pay Now
										</label>
									</div>
								</div>
							<?php } ?>
							<?php if (in_array(PAY_AT_BANK, $active_payment_options)) {?>
								<div class="col-md-3">
									<div class="form-group">
										<label for="payment-mode-<?=PAY_AT_BANK?>">
											<input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_AT_BANK?>" id="payment-mode-<?=PAY_AT_BANK?>" class="form-control b-r-0" placeholder="Payment Mode">
											Pay At Bank
										</label>
									</div>
								</div>
							<?php } ?>
							</div>
						<div class="continye col-xs-3">
							<button id="flip" class="bookcont" type="submit">Continue</button>
						</div>
						<div class="clearfix"></div>
						<div class="sepertr"></div>
						
						<div class="temsandcndtn">
						<?php echo $this->lang->line('most_countries_require');?>
						</div>
					</div>
				</div>
			</div>
			</form>
			</div>
		</div>
	 	<?php if(is_logged_in_user() == true) { ?>
			<div class="col-xs-4 nopadding">
				<div class="insiefare">
					<div class="farehd arimobold"><?php echo $this->lang->line('passenger_list');?></div>
					<div class="fredivs">
						<div class="psngrnote">
							<?php
								if(valid_array($traveller_details)) {
									$traveller_tab_content = $this->lang->line('saved_passenger_details');
								} else {
									$traveller_tab_content = $this->lang->line('no_saved_passenger_details').' <a href="'.base_url().'index.php/user/profile?active=traveller" target="_blank">Add Now</a>';
								}
							?>
							<?=$traveller_tab_content;?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		</div>
		</div>
		<div class="col-xs-4 rhttbepa">
			   <div id="" style="">
			 	<table class="table table-condensed tblemd">
			 	<tbody>
			 	  <tr class="rmdtls">
			        <th>Room Details</th>
			        <td></td>
			      </tr>
			      <tr>
			        <th>Room Type</th>
			        <td><?=$RoomTypeName?></td>
			      </tr>
			     <tr class="aminitdv">
			      <th>Board Type</th>
			       <td>
			        	
			        	<?php if($Boardingdetails):?>			         		
		         		<?php  
		         			$am_arr = array();
		         			foreach ($Boardingdetails as $b_key => $b_value) {
		         				$am_arr[]=$b_value;
		         			}
		         			echo implode(",",$am_arr);
			         		
		         		?>
			        	
		         	<?php else:?>
		         			<span>Room Only</span>
			        	<?php endif;?>
			        </td>
			      </tr>
			      <tr>
			       <?php
				         $total_pax = array_sum($search_data['adult_config'])+array_sum($search_data['child_config']);
				        ?>

			        <th>No of Guest</th>
			        <td><?=$total_pax?></td>
			      </tr>
			       <tr>

			        <th><?=$no_adult?></th>
			        <td><?=array_sum($search_data['adult_config'])?></td>
			      </tr>
			      <tr>
			        <th><?=$no_child?></th>
			        <td><?=array_sum($search_data['child_config'])?></td>
			      </tr>
			      <?php if($LastCancellationDate):?>
				  <tr class="frecanpy">
				        <th>Free Cancellation till:<br/><a  href="#" data-target="#roomCancelModal"  data-toggle="modal" >View Cancellation Policy</a></th>
				        <td><?=local_month_date($LastCancellationDate)?></td>
				        
				      </tr>
				       <?php else:?>
				  		<tr class="frecanpy">
				        <th>Cancellation Policy:<br/><a  href="#" data-target="#roomCancelModal"  data-toggle="modal" >View Cancellation Policy</a></th>
				        <td>Non-Refundable</td>
				      </tr>
			 	 <?php endif;?>
			       <tr>
			        <th>Total Price</th>
			        <td><?=$this->currency->get_currency_symbol($pre_booking_params['default_currency'])?> <?=($ret_data['grand_total'])?></td>
			      </tr>
			      <tr class="texdiv">
			        <th>Taxes & Service fee</th>
			        <td><?=$this->currency->get_currency_symbol($pre_booking_params['default_currency'])?> <?=$ret_data['tax_total']?></td>
			      </tr>
			        <tr class="hide promo_code_discount texdiv">
                          <td>Promo Code Discount</td>
                          <td><?=$this->currency->get_currency_symbol($pre_booking_params['default_currency'])?> <span class="discount_amount"></span></td>
                        </tr>
			      <tr class="grd_tol">
			        <th>Grand Total</th>
			        <td class="discount_total"><?=$this->currency->get_currency_symbol($pre_booking_params['default_currency'])?> <?=($ret_data['grand_total'])?></td>
			      </tr>
			    </tbody>
			  </table>
			   </div>
		</div>
	</div>
 </div>
	</div>
</div>
<span class="hide">
	<input type="hidden" id="pri_journey_date" value='<?=date('Y-m-d',strtotime($search_data['from_date']))?>'>
</span>
<div class="modal fade bs-example-modal-lg" id="roomCancelModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('cancellation_policy'); ?></h5>
				
				<div class="imghtltrpadv hide">
				  <img src="" id="trip_adv_img">
				</div>
			</div>
			<div class="modal-body">
				<p class="policy_text"><?php echo $cancellation_policy; ?></p>
		
			</div>
			<div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
    $(document).on('scroll', function(){
        if ($('#slidebarscr')[0].offsetTop < $(document).scrollTop()){
        	var top = $(document).scrollTop();
        	var height = $(window).height();
        	if((top >= 150) || (top >= 285))  // || (height > 300))
        	{
        		$("#slidebarscr").css({position: "fixed", top:0});  	
        	}   
        	else 
        	{
        		$("#slidebarscr").css({position: "",top:0});  	
        	}                     
        }
    }); 

    $("#apply").click(function(){
	// console.log($("#code").val());console.log($("#module_type").val());console.log($("#total_amount_val").val());console.log($("#convenience_fee").val());console.log($("#email").val());
	var base_url = '<?php echo base_url()?>';
    $.ajax({
        type: "POST",
        url: base_url+'index.php/management/promocode',
         data: {promocode: $("#code").val(), moduletype: $("#module_type").val(), total_amount_val: $("#total_amount_val").val(), convenience_fee: $("#convenience_fee").val()},
         //dataType: "text",  
         dataType:'json',
         cache:false,
         success: 
              function(data){
				// console.log(data);
				if(data.status == 1){
					$(".promo_code_discount").removeClass('hide');
					$(".discount_amount").html(data.value);
					$(".discount_total").html(data.total_amount_val);
					$(".error_promocode").hide();
					$("#promocode_val").val(data.promocode);
					$("#total_amount_payment").val(data.total_amount_val);
					$("#prompform").hide();
				
				}else{
					$(".promo_code_discount").addClass('hide');
					$(".error_promocode").html(data.error_msg);
				}
            
              }
          });
     return false;
 });
 
});
</script>
<!-- <script type="text/javascript">
	$(document).ready(function(){
    $(document).on('scroll', function(){
        if ($('#nxtbarslider')[0].offsetTop < $(document).scrollTop()){
        	var top = $(document).scrollTop();
        	var height = $(window).height();
        	//alert(top);
        	if((top >= 243) || (height <300))
        	{
        		$("#nxtbarslider").css({position: "fixed", top:0});  	
        	}   
        	else if ((top <243) || (height < 300))
        	{
        		$("#nxtbarslider").css({position: "", top:0});  	
        	}         
            
        }
    });  
});
</script> -->

<?php
function room_price_details($pre_booking_params, $search_data, $currency_obj,$hotel_data) {
$data = array();
	$token = $search_data['room_count'];
	$adult_config = $search_data ['adult_config'];
	$child_config = $search_data ['child_config'];
	$no_of_nights = $search_data ['no_of_nights'];
	$default_currency = $currency_obj->get_currency_symbol ( $pre_booking_params ['default_currency'] );
	$CancellationPolicy = $pre_booking_params ['CancellationPolicy'];
	$grand_total = 0;
	$tax_total = 0;
	//foreach ( $token as $token_k => $token_v ) {
		$room_number = $search_data['room_count'];
		$adult_count = array_shift ( $adult_config );
		if (valid_array ( $child_config )) {
			$child_count = array_shift ( $child_config );
		} else {
			$child_count = 0;
		}
		
		//$RoomPrice = $pre_booking_params ['price'];

		//Added by Abi
		$RoomPrice = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $pre_booking_params ['price']) );
		$admin_markup = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $pre_booking_params ['admin_markup']) );
		
		$data['default_currency']=$default_currency;
		$data['per_night_price'] = ($RoomPrice / $no_of_nights);
		$data['adult_count']=$adult_count;
		$data['child_count']=$child_count;
		$data['total_guest']=$adult_count+$child_count;
		$data['CancellationPolicy']=$CancellationPolicy;
		$data['no_of_nights']= $no_of_nights;
		$grand_total += ($RoomPrice + $admin_markup);
		$data['tax_total'] = '0';
		$data['grand_total']=ceil($grand_total);
	    return $data;
}

function diaplay_phonecode($phone_code,$active_data, $user_country_code)
{
	
	$list='';
	foreach($phone_code as $code){
	if(!empty($user_country_code)){
		if($user_country_code==$code['country_code']){
			$selected ="selected";
		}
		else {
			$selected="";
		}
	}
	else{
		
		if($active_data['api_country_list_fk']==$code['origin']){
			$selected ="selected";
		}
		else {
			$selected="";
		}
	}
	
	
		$list .="<option value=".$code['country_code']."  ".$selected." >".$code['name']." ".$code['country_code']."</option>";
	}   
	 return $list;
	
}
?>
<?php
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/hotel_booking.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/booking_script.js'), 'defer' => 'defer');