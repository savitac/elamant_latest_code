<?php
$template_images = $GLOBALS['CI']->template->template_images();
$mini_loading_image = '<div class="text-center loader-image">Please Wait</div>';
$search_id = intval($attr['search_id']);
//debug($raw_hotel_list); exit;
if($view_type == 'list'){
	$class = 'item list-group-item rowresult1 r-r-i';
}else{
	$class = 'item rowresult1 r-r-i col-xs-4 grid-group-item';
}
//debug($raw_hotel_list); exit;
foreach ($raw_hotel_list as $hk => $hd) {
	$trip_result = array();
	
	$hd['accomodation_type'] = 'Hotel';

	//$trip_result = $this->hotels_model->get_trip_adv($hd['HotelCode']);
	$trip_rating = 0;
	$trip_message = 'Terrible';

	
	$trip_result = $this->hotels_model->get_trip_adv_not_exists();
	$trip_hotel_code = $trip_result['result']['hotel_code'];
	$trip_result['status'] = $trip_result['status'];
	$trip = json_decode($trip_result['result']['tri_adv_hotel'],true);
	$trip_location_id = $trip['location_id'];
	$trip_rate = $trip['rating_image_url'];
	$trip_web_url = $trip['web_url'];
	$trip_rating = $trip['rating'];
	$trip_num_reviews = $trip['num_reviews'];


	switch (round($trip_rating)) {
		case 1:
			$trip_message = 'Terrible';
			break;
		case 2:
			$trip_message = 'Poor';
			break;
		case 3:
			$trip_message = 'Average';
			break;
		case 4:
			$trip_message = 'Very Good';
			break;
		case 5:
			$trip_message = 'Excellent';
			break;
		default:
			$trip_message = 'Terrible';
			break;
	}

	$tripadvisor_rating = 0;
	if(!empty($hd['pic'])){
		$img = $hd['pic'];
	}
	else{
		$img = NO_IMG;
	}
	//$img = (array_key_exists('img', $hd) == true) ? $hd['img']: NO_IMG;
?>
<div class="<?php echo $class; ?>">
<div class="col-xs-12 nopad">
	<div class="madgrid">
			<div class="sidenamedesc">
				<div class="celhtl width22 midlbord">
					<div class="hotel_image">
						<img alt="<?=$hd['hotel_name']?>" data-src="<?=$img?>" class="lazy lazy_loader h-img" onerror="javascript:this.src='<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>'">
						<a data-lat="<?=$hd['latitude']?>" data-lon="<?=$hd['longitude']?>"  data-img="<?=$img?>" data-target="#map_view_hotel" data-toggle="modal" class="hotel_location fa fa-map-marker hide"></a>
					</div>
				</div>
				<div class="celhtl width60">
					<div class="waymensn">
						<div class="flitruo_hotel ">
							<div class="hoteldist col-xs-8 nopad">
								<?php //echo "Refundable".$hd['free_cancellation_date']?>
								<span class="hotel_name h-name"><?=$hd['hotel_name'].'-<span class="a-t" data-cstr="'.@$hd['accomodation_cstr'].'">'.(ucwords(strtolower(@$hd['accomodation_type']))).'</span>'?></span>
								<div class="clearfix"></div>
								<span class="h-sr hide"><?php echo $hd['star_rating']?></span>
								<span class="hotel_address elipsetool detal_htladrs"><?=ucfirst(strtolower(rtrim($hd['hotel_address'],',')));?></span>

								<?php 

								$acc_type = $hd['accomodation_type'];
								$star_rating = $hd['star_rating'];

								?>
								<div class="stra_hotel" data-star="<?=intval($star_rating)?>">
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div>
								
								<span class="h-loc hide"><?= ucfirst(strtolower($hd['request']['data']['location']));?></span>
								<span class="deal-status hide" data-deal="0"></span>
								<div class="col-md-12 col-xs-12 nopad">
								<div class="side_amnties">
									<ul class="hotel_fac">
									<?php
									// debug($hd['hotel_amenities_name']);
									if(empty($hd['hotel_amenities_name']) == false && valid_array($hd['hotel_amenities_name'])) {
										foreach($hd['hotel_amenities_name'] as $fk=>$fv) {
											echo '<li data-cstr="'.$fv['icon'].'" data-class="'.$fv['icon'].'" class="'.$fv['icon'].' tooltipv h-f" title="'.ucwords(strtolower($fv['amenities_name'])).'"></li>';
										}
									} else {
										echo '<li class="hide " title="NA"></li>';
									}
									?>
									</ul>
									<input type="hidden" class="m-f-l" value="<?=@$hd['facility_cstr']?>">
								</div>
								</div>

								<?php 
								if($hd['promotions']['status']){
								?>
								<div class="htl_offr"><i class="fa fa-gift" aria-hidden="true"></i><b><?php echo $hd['promotions']['name']; ?> </b><b class="ht_amt"><?php echo $hd['promotions']['remark']; ?></b></div>

								<?php
								} 
								?>	

								<?php 
								if($hd['offers']['status']){
								?>
								<br>
								<div class="htl_offr"><i class="fa fa-gift" aria-hidden="true"></i><b><?php echo $hd['offers']['name']; ?> </b><b class="ht_amt"><?php echo $hd['currency'];?><?php echo str_replace('-', "", $hd['offers']['amount']); ?></b></div>

								<?php
								} 
								?>
								<span class="h-chain hide"><?= ucfirst(strtolower($hd['hotel_chain']));?></span>
							</div>

							<?php 
							if(($trip_result['status'] == true) && !empty($trip_rating)){ 
								?>
							<div class="superb col-xs-4">
							<div class="reviews-box resp-module">
							   <div class="cont">
							      <div class="guest-reviews" style="display: block;">
							         <div class="guest-reviews-badge guest-rating-excellent"><?php echo $trip_message; ?> <span class="guest-rating-value"><strong><?= $trip_rating;?> / 5</strong></span></div>
							          <div class="guest-reviews-link"><a href="<?php echo $trip_web_url; ?>" target="_blank"><span class="small-view"><?= $trip_num_reviews;?> <?php echo $this->lang->line('hotel_reviews');?></span><span class="full-view" style="display: block;"><?= $trip_num_reviews;?> www.tripadvisor.com guest reviews</span></a></div> 
							      </div>
							      <div class="ta-reviews" style="display: block;">
							         <div class="logo-wrap">
							         <img class="blk" style="margin-right: 10px; margin-top: 10px;" src="<?=base_url()?>extras/system/template_list/template_v1/images/trip_advisor/<?php echo $trip_rating?>-37331-5.svg"></div>
							         <a href="<?php echo $trip_web_url; ?>" target="_blank">
							         <div class="text-wrap"><span class="ta-total-reviews"><?= $trip_num_reviews;?> <?php echo $this->lang->line('hotel_reviews');?></span></div>
							         </a>
							         <!-- <img class="ta-tracking-pixel" src="<?=$trip_rate; ?>" alt=""> -->
							      </div>
							   </div>
							</div>
							</div>
							<?php 
								}
							?>
						</div>
					</div>
				</div>
				<div class="celhtl width18">
					<div class="hotel_sideprice">
					<!-- <p>Avg Per Night</p> -->
						<div class="sideprice_hotel">
							<span class="currency_symbol"><?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?></span> <span><?=number_format($hd['total_price'],2)?></span>
							<span class="h-p hide"><?php echo number_format($hd['total_price'],2); ?></span>
							<span class="prce_per"><?php echo $this->lang->line('total_price');?></span>
						</div>
						
						<div class="bookbtn_htl"> <input type="button" value="<?php echo $this->lang->line('hotel_details');?>" class="booknow" onclick="window.open('<?=base_url()?>index.php/hotels/hotel_details/<?=$search_id?>?booking_source=<?=$booking_source?>&hotel_id=<?=$hd['HotelCode']?>')" />
							 </div>
						<span class="hide" style="color:#0B9FD1"><?php echo $this->lang->line('hotel_non_refundable');?></span>
					</div>

					<!-- <div class="ribbon-green">Offer</div> -->

				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$(".tooltipv").tooltip();
});
</script>

<?php
}


