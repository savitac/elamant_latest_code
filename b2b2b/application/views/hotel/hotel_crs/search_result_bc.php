<?php
//error_reporting(E_ALL);
$template_images = $GLOBALS['CI']->template->template_images();
$mini_loading_image = '<div class="text-center loader-image">Please Wait</div>';
$search_id = intval($attr['search_id']);
//debug($raw_hotel_list); exit;
if($view_type == 'list'){
	$class = 'item list-group-item rowresult1 r-r-i';
}else{
	$class = 'item rowresult1 r-r-i col-xs-4 grid-group-item';
}
$booking_source = $booking_source;
//debug($raw_hotel_list); exit;

foreach ($raw_hotel_list as $hk => $hd) { 

	$trip_result = array();
	// debug($hd['facility_search']);
	// echo "------";
	$trip_result = $this->hotel_model->get_trip_adv($hd['hotel_code']);
	$trip_rating = 0;
	$trip_message = 'Terrible';

	if(($trip_result['status'] == 1)){
		$trip_hotel_code = $trip_result['result']['hotel_code'];
		$trip = json_decode($trip_result['result']['tri_adv_hotel'],true);
		$trip_location_id = $trip['location_id'];
		$trip_rate = $trip['rating_image_url'];
		$trip_web_url = $trip['web_url'];
		$trip_rating = $trip['rating'];
		$trip_num_reviews = $trip['num_reviews'];
	}else{
		$trip_result = $this->hotel_model->get_trip_adv_not_exists();
		$trip_hotel_code = $trip_result['result']['hotel_code'];
		$trip_result['status'] = $trip_result['status'];
		$trip = json_decode($trip_result['result']['tri_adv_hotel'],true);
		$trip_location_id = $trip['location_id'];
		$trip_rate = $trip['rating_image_url'];
		$trip_web_url = $trip['web_url'];
		$trip_rating = $trip['rating'];
		$trip_num_reviews = $trip['num_reviews'];
	}


	switch (round($trip_rating)) {
		case 1:
			$trip_message = 'Terrible';
			break;
		case 2:
			$trip_message = 'Poor';
			break;
		case 3:
			$trip_message = 'Average';
			break;
		case 4:
			$trip_message = 'Very Good';
			break;
		case 5:
			$trip_message = 'Excellent';
			break;
		default:
			$trip_message = 'Terrible';
			break;
	}

	$tripadvisor_rating = 0;
	$img_url = site_url().'supervision/uploads/hotel_images/';
	//debug($hd['api_base_url']);exit;
	if(!empty($hd['pic'])){
		$img = $hd['pic'];
	}
	else{
		$img = NO_IMG;
	}
	//debug($hd);
	//$img = (array_key_exists('img', $hd) == true) ? $hd['img']: NO_IMG;
?>

<div class="<?php echo $class; ?>">
<div class="col-xs-12 nopad">
	<div class="madgrid">
			<div class="sidenamedesc">
			<span class="api_base_url hide"></span>
				<div class="celhtl width22 midlbord">
					<div class="hotel_image">
						<img alt="<?=$hd['hotel_name']?>" data-src="<?=$img?>" class="lazy lazy_loader h-img" onerror="javascript:this.src='<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>'">
						<a data-lat="<?=$hd['latitude']?>" data-lon="<?=$hd['longitude']?>"  data-img="<?=$img?>" data-target="#map_view_hotel" data-toggle="modal" class="hotel_location fa fa-map-marker hide"></a>
					</div>
				</div>
				<div class="celhtl width60">
					<div class="waymensn">
						<div class="flitruo_hotel ">
							<div class="hoteldist col-xs-8 nopad">
								<a href="<?=base_url()?>index.php/hotels/hotel_details/<?=$search_id?>?booking_source=<?=$booking_source?>&hotel_id=<?=$hd['HotelCode']?>" target="_blank" ><span class="hotel_name h-name" id="hotel-name"><?=$hd['hotel_name'].' <span class="a-t" ></span>'?></span></a>

								<span class="map-hotel-name"><?=ucwords($hd['hotel_name']);?></span>

								<div class="clearfix"></div>
								<?php //echo "lat".$hd['latitude']?>
								<?php //echo "lon".$hd['longitude']?>
								<span class="h-sr hide"><?php echo $hd['star_rating']?></span>
								<div data-star="<?=$hd['star_rating']?>" class="stra_hotel hide">
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div> 
								<div class="col-md-12 col-xs-12 nopad hide">
								<div class="side_amnties">
									<ul class="hotel_fac">
									<?php

									if(empty($hd['hotel_amenities_name']) == false && valid_array($hd['hotel_amenities_name'])) {
										// debug($hd['hotel_amenities_name']);exit;
										//echo "---";
										//$ame_count = count($hd['amenities']['category_type']);
										foreach ($hd['hotel_amenities_name'] as $ame_1 => $value_1)
										{
											echo '<span class="h-amenities hide">'. ucfirst ( strtolower ($value_1)).'</span>';

											echo '<li data-cstr="'.$hd['hotel_amenities_name'][$ame_1]['icon'].'" data-class="'.$hd['hotel_amenities_name'][$ame_1]['icon'].'" class="'.$hd['hotel_amenities_name'][$ame_1]['icon'].' tooltipv h-f" title="'.ucwords(strtolower($value_1)).'"></li>';
										}
									} else {
										echo '<li class="hide " title="Not Available">Not Available</li>';
									}
									
									?>
									</ul>
								</div>
								</div>
								<span class="hotel_address elipsetool detal_htladrs"><?=ucfirst(strtolower(rtrim($hd['hotel_address'],',')));?>
									<?php if($hd['place'] != '') { //echo $hd['destination'].'&nbsp;&nbsp;'; 
								}
								if(isset($hd['zone_code']) && $hd['zone_code'] !=''){ echo 'Pincode : '.$hd['zone_code']; } ?>
								
								</span>

								<div data-star="<?=$hd['star_rating']?>" class="stra_hotel">
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div> 

								<div class="clearfix"></div>

								<?php //echo '<pre>'; print_r($hd['amenities']);
									
										
								 ?>
								 <div class="col-md-12 col-xs-12 nopad">
								<div class="side_amnties">
									<ul class="hotel_fac">
									<?php
									$amenitie = array();
									$amenitie_name = array();
									$amenitie_name_str = array();
									//debug($hd);exit;
									//debug($hd['amenities']['icon_class']);
									if(empty($hd['hotel_amenities_name']) == false && valid_array($hd['hotel_amenities_name'])) {
										$ame_count = count($hd['hotel_amenities_name']);
										// debug($hd['amenities']['category_type']);
										// debug($hd['amenities']['icon_class']);

										foreach ($hd['hotel_amenities_name'] as $ame => $value)
										{	
											//debug($value);exit;
											// echo '<span class="h-amenities hide">'. ucfirst ( strtolower ($hd['amenities']['category_type'][$ame])).'</span>';
											$amenitie_name_str[] = ucfirst(strtolower($value['amenities_name']));
											echo '<li data-cstr="'.$value['amenities_name'].'" data-class="'.$value['amenities_name'].'" class="'.$value['icon'].' tooltipv h-f" title="'.ucwords(strtolower($value['amenities_name'])).'"></li>';

											$amenitie_name [] = ucwords(strtolower($value['amenities_name']));
											
											$amenitie [] = $value['icon'];

										}
									} else {
										echo '<li class="hide " title="Not Available">Not Available</li>';
									}
									if($amenitie_name&&$amenitie&&$amenitie_name_str){
										$amenitie_name = implode(",",$amenitie_name);
										$amenitie = implode(",",$amenitie);
										$amenitie_name_str = implode(",",$amenitie_name_str);
									}else{
										$amenitie_name='';
										$amenitie='';
										$amenitie_name_str='';
									}
									?>
									</ul>
									<span class="ame hide"><?php echo $amenitie;?></span>
									<span class="ame_name hide"><?php echo $amenitie_name; ?></span>
									<span class="ame_name_str hide"><?php echo  $amenitie_name_str?></span>
									<input type="hidden" class="m-f-l" value="<?=@$hd['facility_cstr']?>">
								</div>
								</div>

							</div>
								
								<span class="h-loc hide"></span>
								<span class="h-property hide"></span>

								<span class="h-chain hide"></span>
								<span class="deal-status hide" data-deal="0"></span>
								

								<?php 
								//if($hd['promotions']['status']){
								?>
								<div class="htl_offr hide"><i class="fa fa-gift" aria-hidden="true"></i><b><?php //echo $hd['promotions']['name']; ?> </b><b class="ht_amt"><?php //echo $hd['promotions']['remark']; ?></b></div>

								<?php
								//} 
								?>	

								<?php 
								//if($hd['offers']['status']){
								?>
								<div class="htl_offr hide"><i class="fa fa-gift" aria-hidden="true"></i><b><?php //echo $hd['offers']['name']; ?> </b><b class="ht_amt"><?php //echo $hd['currency'];?><?php //echo str_replace('-', "", $hd['offers']['amount']); ?></b></div>

								<?php
								//} 
								?>

							</div>

							<?php 
							if(($trip_result['status'] == true) && !empty($trip_rating)){ 
								?>
							<div class="superb col-xs-4 hide">
							<div class="reviews-box resp-module">
							   <div class="cont">
							      <div class="guest-reviews">
							         <div class="guest-reviews-badge guest-rating-excellent"><?php echo $trip_message; ?> <span class="guest-rating-value"><strong><?= $trip_rating;?> / 5</strong></span></div>
							         <div class="guest-reviews-link"><a href="<?php echo $trip_web_url; ?>" target="_blank"><span class="small-view"><?= $trip_num_reviews;?> reviews</span><span class="full-view"><?= $trip_num_reviews;?> www.tripadvisor.com guest reviews</span></a></div>
							      </div>
							      <div class="ta-reviews">
							         <div class="logo-wrap">
							         <img class="blk" style="margin-right: 10px; margin-top: 10px;" src="<?=base_url()?>extras/system/template_list/template_v1/images/trip_advisor/<?php echo $trip_rating?>-37331-5.svg"></div>
							         <!-- <a href="<?php echo $trip_web_url; ?>"> -->
							         <div class="text-wrap"><span class="ta-total-reviews"><?= $trip_num_reviews;?> reviews</span></div>
							         <!-- </a> -->
							         <!-- <img class="ta-tracking-pixel" src="<?=$trip_rate; ?>" alt=""> -->
							      </div>
							   </div>
							</div>
							</div>
							<?php 
								}
							?>
						</div>
					</div>
				
				<div class="celhtl width18">
					<div class="hotel_sideprice">
					<!-- <p>Avg Per Night</p> -->
						<div class="sideprice_hotel">
							<span class="currency_symbol"><?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?></span> <span><?=number_format($hd['total_price'],2)?></span>
							<span class="h-p hide"><?php echo number_format($hd['total_price'],2); ?></span>
							<span class="prce_per hide">Price per Night</span>
						</div>
						
						<div class="bookbtn_htl"> <a href="<?=base_url()?>index.php/hotels/hotel_details/<?=$search_id?>?booking_source=<?=$booking_source?>&hotel_id=<?=$hd['HotelCode']?>" target="_blank" class="booknow">Details</a> </div>

						<div class="clearfix"></div>
						<?php if(isset($hd['rooms']['post_pay'])):?>
							<?php 
								//echo var_dump($hd['rooms']['post_pay'][0]);
							?>
						<span class="pay_at">
							<?php if($hd['rooms']['post_pay'][0]=='true') { echo ' Pay At Hotel Available'; } else { echo ''; } ?>
								
						</span>
					<?php endif;?>
					</div>

					<!-- <div class="ribbon-green">Offer</div> -->

				</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$(".tooltipv").tooltip();
});
</script>

<?php
}


