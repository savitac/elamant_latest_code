<?php
error_reporting(E_ALL);
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('page_resource/hotel_result.css'), 'media' => 'screen');
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('page_resource/hotel_detail.css'), 'media' => 'screen');
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('page_resource/animation.css'), 'media' => 'screen');
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('owl.carousel.min.css'), 'media' => 'screen');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/flight_session_expiry_script.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('owl.carousel.min.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('custom_1.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => '//code.jquery.com/ui/1.11.4/jquery-ui.js', 'defer' => 'defer');

Js_Loader::$js [] = array (
		'src' => $GLOBALS ['CI']->template->template_js_dir ( 'page_resource/pax_count.js' ),
		'defer' => 'defer' 
);
// debug($rooms_lists);exit;
$sanitized_data ['Images'] = $hotel_details ['hotel_images'];
$pscheckin = date('d M Y',strtotime($hotel_search_params['from_date']));
$pscheckout = date('d M Y',strtotime($hotel_search_params['to_date']));
$template_images	= $GLOBALS['CI']->template->template_images();
//debug($country_code);exit;
//******* Search Params *******//
$search_id			= $hotel_search_params['search_id'];
$number_of_nigths	= $hotel_search_params['no_of_nights'];
$number_of_rooms	= $hotel_search_params['room_count'];
$number_of_adults	= array_sum($hotel_search_params['adult_config']);
$number_of_childs	= array_sum($hotel_search_params['child_config']);
//******* Dynamic Data *******//
//debug($hotel_details); exit;
$hotel_code				= $hotel_details['hotel_code'];
$hotel_name				= $hotel_details['hotel_name'];
$hotel_description = $hotel_details['hotel_info'];
$star_rating			= $hotel_details['star_rating'];
//$copy_right				= $hotel_details['hotel_copy_right'];
$destination			= @$hotel_details['destination'];
$check_in				= $hotel_search_params['from_date'];
$check_out				= $hotel_search_params['to_date'];
$latitude				= $hotel_details['latitude'];
$longitude				= $hotel_details['longitude'];
$min_rate				= $rooms_lists[0]['total_price'];
$minRate 				= @$hotel_details['minRate'];
$max_rate				= @$hotel_details['maxRate'];
$currency				= $currency_obj->to_currency;
$room_list				= $rooms_lists;
$print_format_room_list	= @$rooms_lists;
// debug($print_format_room_list);
// exit;

//******* Static Data *******//FIXME: Get Hotel Static Data from Local Database
$hotel_image			= explode(",",$hotel_details['hotel_images']);
$img_url 				= site_url().'supervision/';
$mediumImage_baseUrl	= 'uploads/hotel_images/';
$image_url =$img_url.$mediumImage_baseUrl;
//debug($image_url);exit;

//debug($hotel_image);exit;
//$smallImage_baseUrl		= @$hotel_details['small_image_baseUrl'];

/*if(!empty($hotel_image))
{		
	//echo $hotel_image;
	$img = $img_url.$mediumImage_baseUrl.$hotel_image;
	//$pathinfo =pathinfo($hotel_image);	
	//debug($pathinfo);
	// $dirname = 	$pathinfo['dirname'];
	// $base_name = $pathinfo['filename'];
	// $extension = $pathinfo['extension'];
	// $new_file_name = substr($base_name, 0, -3);
	// //echo $new_file_name;
 //    $new_url  = $img_url.$mediumImage_baseUrl.$dirname.'/'.$new_file_name.'.'.$extension;      
 //    //exit;
 //   // echo $new_url;
	// $file_header = @get_headers($new_url);
	// if($file_header  || $file_header [0] !='HTTP/1.1 404 Not Found'){
	// 	$img = $new_url;
	// }
}
else{
	$img = NO_IMG;
}*/
//exit;
//debug($hotel_image);exit;
/*$file_header = @get_headers($pre_booking_params['HotelImage']);
$image_found=1;
if(!$file_header  || $file_header [0] =='HTTP/1.1 404 Not Found'){
	$image_found=0;

}*/


//$hotelContact			= $hotel_details['phone_no'];
$hotelContact = explode(",",$hotel_details['phone_number']);

$new_hotel_address 		= @$hotel_details['hotel_address'];
$acc_type				= @$hotel_details['accomodation_type_code'];
$postal					= @$hotel_detail['zone_code'];
$hotel_ameneties		= '';
$hotelEmail				= @$hotel_details['email'];
//******* cached Token Details *******//
$Token					= @$hotel_details['Token'];
$TokenKey				= @$hotel_details['TokenKey'];


$trip_rating = 0;
$trip_message = 'Terrible';

if($acc_type == 'APTHOTEL'){ 
		$acc_type = "HOTEL APARTMENT";
	}else if($acc_type == 'APART'){
		$acc_type = "APARTMENT";
	}else{
		$acc_type = $acc_type;
	}

// trip Advisor
/*$trip_result = $this->hotel_model->get_trip_adv($params['trip_hotel_code']);

if(($trip_result['status'] == 1)){
	$trip = json_decode($trip_result['result']['tri_adv_hotel'],true);
	$trip_location_id = $trip['location_id'];
	$trip_rate = $trip['rating_image_url'];
	$trip_web_url = $trip['web_url'];
	
	$trip_num_reviews = $trip['num_reviews'];
}*/
//debug($hotel_details);exit;
$trip_rating = @$hotel_details['trip_adv_ratings'];
$trip_web_url = @$hotel_details['trip_adv_img'];
$trip_review_url = @$hotel_details['trip_new_rate_url'];
$trip_num_reviews = @$hotel_details['trip_num_reviews'];
switch (round($trip_rating)) {
	case 1:
		$trip_message = 'Terrible';
		break;
	case 2:
		$trip_message = 'Poor';
		break;
	case 3:
		$trip_message = 'Average';
		break;
	case 4:
		$trip_message = 'Very Good';
		break;
	case 5:
		$trip_message = 'Excellent';
		break;
	default:
		$trip_message = 'Terrible';
		break;
}

?>

<script type="text/javascript">
	/*
		session time out variables defined
	*/
	var  search_session_expiry = "<?php echo $GLOBALS ['CI']->config->item ( 'flight_search_session_expiry_period' ); ?>";
	var search_session_alert_expiry = "<?php echo $GLOBALS ['CI']->config->item ( 'flight_search_session_expiry_alert_period' ); ?>";
	var search_hash = "<?php echo @$session_expiry_details['search_hash']; ?>";
	var start_time = "<?php echo @$session_expiry_details['session_start_time']; ?>";
	var session_time_out_function_call = 1; 
</script>

<style type="text/css">
	.hotel_fac li { margin:0px; }
</style>
<style type="text/css">
	
	

.outerfullfuture{ background: #fff none repeat scroll 0 0;display: block;margin: 0px;box-shadow: 0 0 0 0;}

.ourdest{display: block;min-height: 298px;max-height: 298px;max-width: 100%;overflow: hidden;position: relative;}
.ourdest img{width: 100%;min-height: 298px;max-height: 298px;}
.destplace{bottom:0px;color:#fff;font-size:14px;left:0;padding:5px 15px;position:absolute;right:0;z-index:1;background:rgba(0, 0, 0, 0.7) none repeat scroll 0 0;}
.cntdes1{color: #fff;font-weight: 200;padding: 1px 0;font-size: 15px;}
.price_ht strong{color: #06b5f1;font-weight: 400;}
.star{color:#ff9800;}
.surbtm1{float:left;}
.destbtm2{float: right;width: 30%;margin: 15px 10px;}
.bk_btn{
	background: #fdb912 none repeat scroll 0 0;
    border: 1px solid #fdb912;
    color: #000;
    display: block;
    font-size: 15px;
    overflow: hidden;
    padding: 7px 0;
    text-align: center;
    width: 100%;
    border-radius: 10px;text-transform: uppercase;}
.tooltip { 
	width: auto !important;
 float: left; background: none !important; 
 border-radius: 3px;
} 
.tooltip.left {
 padding: 0px !important;
  }
.tooltip-inner{padding: 2px 7px !important;background: #333 !important;max-width: 100% !important;}
.tooltip-inner .table {margin-bottom: 0px !important;background: #333 !important;}
.tooltip.left .tooltip-arrow {right: -5px !important;border-left-color: #333;}
.tooltip.in {opacity: 1 !important;}
.str_ratng, .detail_htlname {float: left;}		
.str_ratng {line-height: 31px;}
</style>

<!-- Header Carousel -->
<?php

$data ['result'] = $hotel_search_params;
$mini_loading_image = '<div class="text-center loader-image"><img src="' . $GLOBALS ['CI']->template->template_images ( 'loader_v3.gif' ) . '" alt="Loading........"/></div>';
$loading_image = '<div class="text-center loader-image"><img src="' . $GLOBALS ['CI']->template->template_images ( 'htl_load.gif' ) . '" alt="Loading........"/></div>';
$template_images = $GLOBALS ['CI']->template->template_images ();
echo $GLOBALS ['CI']->template->isolated_view ( 'hotel/search_panel_summary' );
?>
<div class="fldealsec">
  <div class="container">
    <div class="tabcontnue">
      <div class="breadli nopadding">
        <div class="rondsts success">
          <a class="taba core_review_tab <?php //echo $strQuat_2; ?>review_tab_marker" id="stepbk1">
            <!-- <div class="iconstatus fa fa-list-alt"></div> -->
            <div class="iconstatus fa fa-list"></div>
            <div class="stausline">Search Results</div>
            
          </a>
        </div>
      </div>

      <div class="breadli nopadding">
        <div class="rondsts active">
          <a class="taba core_review_tab <?php //echo $strQuat_2; ?>review_tab_marker" id="stepbk1">
            <!-- <div class="iconstatus fa fa-list-alt"></div> -->
            <div class="iconstatus fa fa-bed"></div>
            <div class="stausline">Select Rooms</div>
            
          </a>
        </div>
      </div>

      <div class="breadli nopadding">
        <div class="rondsts">
          <a class="taba core_review_tab <?php //echo $strQuat_2; ?>review_tab_marker" id="stepbk1">
            <!-- <div class="iconstatus fa fa-list-alt"></div> -->
            <div class="iconstatus fa fa-group"></div>
            <div class="stausline">Guest Details</div>
            
          </a>
        </div>
      </div>

      <div class="breadli nopadding">
        <div class="rondsts">
          <a class="taba core_travellers_tab <?php //echo $strPay_2; ?>travellers_tab_marker"
            id="stepbk2">
            <!-- <div class="iconstatus fa fa-money"></div> -->
            <div class="iconstatus fa fa-money"></div>
            <div class="stausline">Payment Method</div>
            
          </a>
        </div>
      </div>
      <div class="breadli nopadding">
        <div class="rondsts <?php //echo $strConf_1; ?>">
          <a class="taba" id="stepbk3">
            <!-- <div class="iconstatus fa fa-check"></div> -->
            <div class="iconstatus fa fa-check"></div>
            <div class="stausline">Confirmation</div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div> 

<div class="allpagewrp top80">
	<div class="newmodify">
		<div class="container">
			<div class="contentsdw">
				<div class="col-md-5 col-sm-8 col-xs-9 nopad">
					<div class="detail_htlname"><?=$hotel_name?> <?php echo ucfirst(strtolower($acc_type)); ?></div>
					<?php //if(!empty($acc_type == 'HOTEL')){?>
					<div class="star_detail">
						<div class="stra_hotel" data-star="<?=intval($star_rating)?>">
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div>
					</div>
					<?php //} else{ ?>
						<!-- <div class="str_ratng"><?php //echo $star_rating; ?></div> -->
					<?php // } ?>
					<div class="clearfix"></div>
					<div class="detal_htladrs"><?=strtolower($new_hotel_address)?></div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-3 nopad">
					<a class="view_map_dets location-map " href="<?php echo base_url().'index.php/hotels/map?lat='.$latitude.'&lon='.$longitude.'&hn='.urlencode($hotel_name).'&sr='.intval($star_rating).'&c='.urlencode($destination).'&img='.urlencode($image_url.$hotel_image[0])	?>" target="map_box_frame">
					<strong>View Map</strong>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="dets_section">
	<div class="container">
		<div class="col-md-8 col-xs-12 padR30 padr30m">
			<div class="leftslider">
				<div id="sync1" class="owl-carousel detowl">
					<?php

						if(valid_array($sanitized_data) == true) {
							$hotel_imgs_count = 0;
							$hotel_image =  explode(',', $sanitized_data ['Images']);
							foreach($hotel_image as $hmKey => $img) { ?>
									<div class="bighotl bigimg">
			                        <img src="<?= $image_url . $img;?>" onerror="javascript:this.src='<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>'" alt="" />
			                        </div>

						<?php 	}
						}else { ?>

							?><img src="<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>"/>
						<?php 
						}
						?>
				</div>
				<div id="sync2" class="owl-carousel syncslide" style="margin-top: 10px;">
					<?php 
						if(valid_array($sanitized_data) == true) {
							$hotel_imgs_count = 0;
							$hotel_image =  explode(',', $sanitized_data ['Images']);
							foreach($hotel_image as $hmKey => $image) {
								?>
								<div class="item" style="padding-right:5px;">
									<div class="bighotl"><img src="<?=$image_url . $image;?>" height="60" onerror="javascript:this.src='<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>'"  /></div>
								</div>
							<?php }
						}else{ ?>	
							<img src="<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>"/>
						<?php }
					?>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 nopad">
			<div class="inside_detsy">
				<div class="dets_hotel">
					<div class="col-md-6 col-sm-6 col-xs-6 nopad">
					<span class="hotel_address elipsetool">Check In: <span class="trvlr"><?= $pscheckin;?></span></span>
			    </div>
			    
			    <div class="col-md-6 col-sm-6 col-xs-6 nopad">		
					<span class="hotel_address elipsetool">Check Out: <span class="trvlr"><?= $pscheckout;?></span></span>
			    </div>	

			    <div class="col-md-6 col-sm-6 col-xs-6 nopadding_left">		
					<span class="hotel_address elipsetool">Adult(s): <span class="trvlr"><?= array_sum($hotel_search_params['adult_config']);?></span></span>
					<?php if(array_sum($hotel_search_params['child_config']) > 0){?>
					<span class="hotel_address elipsetool">No. of Children : <span class="trvlr"><?= array_sum($hotel_search_params['child_config']);?></span></span>
					<?php 
					$i = 1; 

					foreach($hotel_search_params['child_age'] as $age){ ?>
					<span class="hotel_address elipsetool">Child <?php echo $i; ?> : <span class="trvlr"><?php echo $age; ?> Yrs </span></span>
			    	<?php $i++; } } ?>
			    </div>	
			   <div class="clearfix"></div>
			   <?php if(isset($hotel_details['hotel_amenities'])):?> 
			   <div class="side_amnties">
                 	<ul class="hotel_fac">
                 		<?php
                 			//$hotel_details['hotel_amenities_name'] = explode(',',$hotel_details['hotel_amenities']);
                 			//debug($hotel_details['hotel_amenities_name']);exit; 
                 			foreach($hotel_details['hotel_amenities_name'] as $h_akey=>$h_avalue):?>
                 			<li class="<?= $h_avalue['icon'].' tooltipv h-f' ?>" data-toggle="tooltip" title="<?=$h_avalue['amenities_name']?>"></li>
                 		<?php endforeach;?>
                 	</ul>
                </div>
            <?php endif;?>
					<div class="side_amnties">
							<ul class="hotel_fac">
					<?php
                 			//$hotel_details['hotel_amenities_name'] = explode(',',$hotel_details['hotel_amenities']);
                 			//debug($hotel_details['hotel_amenities_name']);exit; 
                 			foreach($hotel_details['hotel_amenities_name'] as $h_akey=>$h_avalue):?>
                 			<li class="<?= $h_avalue['icon'].' tooltipv h-f' ?>" data-toggle="tooltip" title="<?=$h_avalue['amenities_name']?>"></li>
                 		<?php endforeach;?>
                 	</ul>
                </div>
            <?php endif;?>
					<a style="line-height: 30px;" href="#facility" data-toggle="tab" onclick="facilitypsw()">See More</a>
							</ul>
					</div>
				</div>
				<div class="clearfix"></div>

				<!-- <div class="trp_adv">
				<h3>Exceptional 5/5</h3>
				<img class="blk" style="margin-right: 10px; margin-top: 10px;" src="<?=$GLOBALS['CI']->template->template_images('trp_adnisor.png'); ?>">
				<p class="rvw">12 Reviews</p>	
				</div> -->

				<?php 
				if($trip_rating){ 
					?>
				<!-- <div class="superb col-xs-4"> -->
				<!-- <div class="reviews-box resp-module"> -->
				   <!-- <div class="cont"> -->
				      <div class="guest-reviews">
				         <div class="guest-reviews-badge guest-rating-excellent"><?php echo $trip_message; ?> <span class="guest-rating-value"><strong><?= $trip_rating;?> / 5</strong></span></div>
				         <div class="guest-reviews-link"><a href="<?php echo $trip_web_url; ?>" target="_blank"><span class="small-view"><?= $trip_num_reviews;?> reviews</span><span class="full-view"><?= $trip_num_reviews;?> www.tripadvisor.com guest reviews</span></a></div>
				      </div>
				      <div class="ta-reviews">
				         <div class="logo-wrap">
				         <img class="blk" style="margin-right: 10px; margin-top: 10px;" src="<?=base_url()?>extras/system/template_list/template_v1/images/trip_advisor/<?php echo $trip_rating?>-37331-5.svg"></div>
				         <!-- <a href="<?php echo $trip_web_url; ?>"> -->
				         <div class="text-wrap"><a style="line-height: 30px;" href="#tripadvisor" onclick="tripadvisorw()" data-toggle="tab"><span class="ta-total-reviews"><?= $trip_num_reviews;?> reviews</span></a></div>
				         <!-- </a> -->
				         <!-- <img class="ta-tracking-pixel" src="<?=$trip_rate; ?>" alt=""> -->
				      </div>
				   <!-- </div> -->
				<!-- </div> -->
				<!-- </div> -->
				<?php 
					}
				?>

				<div class="clearfix"></div>
				<div class="price_strts">
					<div class="price_froms">starting from<strong><?=$currency?> <?=$min_rate?></strong></div>
				</div>
				<div class="clearfix"></div>
				<a class="room_select">Select Room</a>
			</div>
		</div>
		<?php $contactStr = '';
			if(isset($hotelContact) && valid_array($hotelContact)) {
				
				foreach($hotelContact as $cKey => $contactarr) {

					$contactStr = str_replace("-Switch","",$contactarr);

					//$contactStr = ($contactStr != '') ? $contactStr . ',' .$contactarr['contactN'] : $contactarr['contactN'];
				}
			}
			?>
		
	</div>
</div>
<div class="clearfix"></div>
<div class="down_hotel">
	<div class="container">
		<div class="col-md-8 col-xs-12 nopad">
			<div class="shdoww">
				<div class="hotel_detailtab">
				<div class="details_litab">
					<ul class="nav nav-tabs htl_bigtab">
						<li class="" id='facilitypsh'>
							<a href="#htldets" data-toggle="tab"><span class="fa fa-info-circle"></span>Hotel Details</a>
						</li>
						<li class="active" id='facilitypsr'>
							<a href="#rooms" data-toggle="tab" id="room-list"><span class="fa fa-building-o"></span>Rooms</a>
						</li>
						<li id='facilitypsw'>
							<a href="#facility" data-toggle="tab"><span class="fa fa-check-square-o"></span>Amenities</a>
						</li>
						<?php
							$policy_class="hide";
							if(isset($hotel_details['hotel_notice']) && $hotel_details['hotel_notice']!=""){
								$policy_class="";
							}
						?>
						<li id='hotelpolicysw' class="<?=$policy_class?>">
							<a href="#hotelpolicy" data-toggle="tab"><span class="fa fa-info"></span>Hotel Policy</a>
						</li>
						<?php
							$trip_class="hide";
							if(isset($trip_review_url) && $trip_review_url!=''){
								$trip_class='';
							}
						?>
						<li id='tripadvisorw' class="<?=$trip_class?>"> 
							<a href="#tripadvisor" data-toggle="tab"><span class="fa fa-tripadvisor"></span>Tripadvisor</a>
						</li>

						<li id='facilitypost' class="hide">
							<a href="#post" data-toggle="tab"><span class="fa fa-comment"></span>Post a Review</a>
						</li>
<!--						<li>-->
<!--							<a href="#policy" data-toggle="tab"><span class="fa fa-list-alt"></span>Hotel Policy</a>-->
<!--						</li>-->
					</ul>
					</div>
					<div class="clearfix"></div>
					<div class="tab-content clearfix">

						<!-- Hotel Detail-->
						<div class="tab-pane" id="htldets">
							<div class="innertabs">
                            <div class="dets_hotels">
                            <strong><?=$hotel_name?></strong>
								<div class="comenhtlsum">  <?php echo $hotel_description;?></div>
							</div>
                            </div>
						</div>
						<!-- Hotel Detail End--> 
						<div class="clearfix"></div>
						<!-- Rooms Summry-->
						<div class="tab-pane active" id="rooms">
							<div class="innertabs">
								<?php
								$rm_cnt = 0;
								//debug($print_format_room_list);
								$room_info = $print_format_room_list;
								//$room_info_count = count($room_list);
								$room_rate = $base_fare = 0;
								foreach($room_info as $rl => $room_list) {
									//debug($room_list['cancel_policy']);
									/*$total_adult = $rl_v['max_adults'];
									$total_child = $rl_v['max_child'];*/
									$room_desc = $room_list['room_description'];
									$room_code = $room_list['hotel_room_type_id'];

									$base_fare = $room_list['total_price'];
									$tax_fare = @$room_list['tax_fare'];
									$dis_fare = @$room_list['discount_fare'];								
									$room_rate = $room_list['total_price'];
									$total_price = ($base_fare + $tax_fare) - $dis_fare;
									$booking_code = @$room_list['room_rate']['booking_code'];

									//debug($room_list);exit;
								?>

								<div class="htl_rumrow">
								   <div class="hotel_list">
								      <div class="col-md-9 col-sm-9 col-xs-8 fullfives nopad">
								         <div class="col-md-12 col-xs-12 nopad">
								            <!--<div class="col-md-2 nopad">--><!--<div class="imagehotel hotel_image"><img src="" alt="" /></div>--><!--</div>-->
								            <div class="col-md-9 col-sm-9 col-xs-7 nopad">
								               <div class="in_center_htl">
								                  <div class="hotel_hed"><?php echo $room_list['room_type_name']; ?></div>
								                  <div class="hotel_sub_hed"><?php $room_includes = @$room_list['room_rate']['inclusions']; 
								                  //debug($room_includes);
								                 
								                  if(@$room_list['room_amenity']['amenities']['name'][$rl]) {
								                  	echo '<span>Inclusion In the Room</span>';	
								                  }
								                  
								                  /*echo '<ul>';
								                  if((count($room_includes) > 0) && (valid_array($room_includes)))
								                  {
								                  	for($inc=0; $inc < count($room_includes); $inc++)
								                  	{
								                  		echo '<li><span class="fa fa-check-square-o"></span>'.$room_includes[$inc].'</li>';		
								                  	}									                  	
								                  }
								                  else
								                  {
								                  	if($room_includes != '')
								                  	{
								                  		echo '<li><span class="fa fa-check-square-o"></span>'.$room_includes.'</li>';
								                  	}
								                  	else
								                  	{
								                  		echo '<li>Not Available</li>';	
								                  	}								                  	
								                  }
								                  echo '</ul>';*/

								                   ?></div>
								                  <!--<div class="mensionspl"> Extra Bed Available : <span class="menlbl">Crib</span> </div> 
								                     -->
								                     <!-- <?php if($room_list['room_amenity']): ?>

								                     <div class="side_amnties">
									                 	<ul class="hotel_fac">
									                 		<?php foreach($room_list['room_amenity'] as $a_key=>$a_value):?>

									                 		<li class="<?=$a_value['icon']?>" data-toggle="tooltip" title="<?=$a_value['amenities_name']?>"></li>

									                 	<?php endforeach;?>
									                 	</ul>
									                </div>
									            <?php endif;?> -->
									            	<?php if(@$room_list['room_amenity']):?>
								                     <div class="hotel_sub_hed">
								                     	<a class="all_amints" data-toggle="collapse" data-target="#demo_<?=$rl?>">Other Amenities</a>
								                     	<div id="demo_<?=$rl?>" class="col-md-12 nopad shw_aminities collapse">
								                     		<ul>
								                     		<?php foreach($room_list['room_amenity'] as $o_key=>$o_value):?>
							                     			
									                     		<li><span class="fa fa-check-square-o"></span><?=$o_value['amenities_name']?></li>               
									                     	<?php endforeach; ?>
									                     	</ul> 	
								                     	</div>
								                     	
								                     </div>
								                    <?php endif;?>
								                  <div class="mensionspl"> </div>
								                  <div class="clearfix"></div>
								                  <div class="morerumdesc"><a class="morerombtn" data-toggle="modal" data-target="#cancellation_policy<?=$rl?>"><span class="fa fa-ban"></span>Cancellation Policy</a>
								                  </div>
								                  <div class="morerumdesc prom"></div>
								                  <div class="morerumdesc"></div>
								                  <div class="ht_refnd hide"><span class="fa fa-ticket fldetail"></span> </div>
								               </div>
								            </div>
								            <div class="col-md-3 col-sm-3 col-xs-3 nopad hide">
								               <div class="pers hide">
								                  <h5 class="htl_rm">Rooms  : <?php echo $number_of_rooms; ?></h5>
								                  <?php 
								                  //debug($hotel_details['formated_room_list']);
								                  $adult_cnt = $child_cnt = 0;
								                  for($frl=0; $frl < count($room_list); $frl++)
								                  {
								                  	if($hotel_details['formated_room_list'][$frl]['room_rate']['max_adults'] != '')
								                  	{
								                  		$adult_cnt = $hotel_details['formated_room_list'][$frl]['room_rate']['max_adults'];	
								                  	}
								                  	if($hotel_details['formated_room_list'][$frl]['room_rate']['max_child'] != '')
								                  	{
								                  		$child_cnt = $hotel_details['formated_room_list'][$frl]['room_rate']['max_child'];	
								                  	}
								                  }
								                  
								                 ?>
								                 <i class="fa fa-male"></i>
								                 <?php echo $adult_cnt;  ?>
								                 <i class="fa fa-child"></i>
								                 <?php echo $child_cnt; ?>
								               </div>
								            </div>
								            <div class="col-md-3 col-sm-3 col-xs-3 nopad">
								               <div class="sideprice"><?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?> <?php echo number_format($room_rate,2); ?> <span class="avgper">Total</span> </div>
								            </div>
								         </div>
								         <!--Cancellation details Modal Starts-->
								         <div id="cancellation_policy<?=$rl?>" class="modal fade" role="dialog">
								            <div class="modal-dialog">
								               <div class="modal-content">
								                  <div class="modal-header">
								                     <button type="button" class="close" data-dismiss="modal">×</button>
								                     <h4 class="modal-title">Cancellation Policy</h4>
								                  </div>
								                  <div class="modal-body">
								                     <ul class="list_popup">
								                        <li class="listcancel">
								                        	<?php 
								                        		$cancel_policy = '';
								                        		if($room_list['cancel_policy']){
								                        			$cancel_policy = $room_list['cancel_policy'];
								                        		}else{
								                        			$cancel_policy = 'This booking is non-refundable.';
								                        		}
								                        	?>
								                        	<?php echo $cancel_policy;?>
								                        </li>
								                     </ul>
								                  </div>
								                  <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
								               </div>
								            </div>
								         </div>
								         <!--Cancellation details Modal Ends-->
								      </div>
								      <div class="col-md-3 col-sm-3 col-xs-4 fullfives">
								         <div class="col-md-12 col-xs-12 nopad bordrit">
								            <div class="pricesec">
								               <div class="bookbtn">
								               <?php
								               	$booking_url = $GLOBALS['CI']->hotel_lib->booking_url($hotel_search_params['search_id']);
								               	//debug($hotel_search_params);exit;
								               ?>
								                  <form action="<?php echo 	$booking_url; ?>" id="room_details_form" method="post">
								                     <div class="">
								                     <input type="hidden" name="HotelCode" value="<?= $room_list['hotel_details_id'] ?>">
								                     <input type="hidden" name="ResultIndex" value="<?= @$params['ResultIndex'] ?>">
								                     <input type="hidden" name="booking_source" value="<?= $active_booking_source; ?>">
								                     <input type="hidden" name="search_id"		value="<?= $search_id ?>">
								                     <input type="hidden" name="TraceId" value="<?= @$params['TraceId'] ?>">
								                     <input type="hidden" name="op" value="block_room">
								                     <input type="hidden" name="HotelName"		value="<?= $hotel_details['hotel_name'] ?>" >
								                     <input type="hidden" name="StarRating"		value="<?= $hotel_details['star_rating'] ?>">
								                     <input type="hidden" name="HotelImage"		value="<?= $hotel_details['thumb_image'] ?>">
								                     <input type="hidden" name="HotelAddress"		value="<?= $hotel_details['hotel_address'] ?>">
								                     <input type="hidden" name="room_id"	value ="<?= $room_list['hotel_room_type_id'] ?>">
								                     <input type="hidden" name="season_id"	value ="<?= $room_list['seasons_details_id'] ?>">
								                     <input type="hidden" name="room_type_name"	value ="<?= $room_list['room_type_name'] ?>">
								                     <input type="hidden" name="price"	value ="<?= get_converted_currency_value ( $currency_obj->force_currency_conversion ( $room_list['total_price']) ) ?>">
								                     <input type="hidden" name="CancellationPolicy[]"	value ="<?= @$room_list['price_data']['room_rate_cancellation_policy'] ?>">

								                     <input type="hidden" name="token" value="<?php echo @$hotel_details['Token']; ?>">
								                     <!-- <input type="hidden" name="token_key" value="<?php echo $hotel_details['TokenKey']; ?>"> -->
								                    <!--  <input type="hidden" name="rateKey[]" value="<?php echo $booking_code; ?>"> -->
								                    <!--  <input type="hidden" name="rooms[]" value="<?php echo $hotel_search_params['room_count']; ?>"> -->
								                     <input type="hidden" name="adults[]" value="<?php echo implode(',',$hotel_search_params['adult_config']); ?>">
								                     <input type="hidden" name="childs[]" value="<?php echo implode(',',$hotel_search_params['child_config']); ?>"></div>

								                     <?php if($room_list['cancel_policy']):?>
								                     	<input type="hidden" name="cancel_policy[]" value="<?php echo $room_list['cancel_policy']; ?>">
								                     <?php else:?>
														<input type="hidden" name="cancel_policy[]" value="This booking is non-refundable">
								                     <?php endif;?>
								                     <input type="hidden" name="hotel_policy" value="<?php echo $hotel_details['hotel_notice']?>">
								                     <input type="submit" class="booknow" value="Book">
								                  </form>
								               </div>
								            </div>
								         </div>
								         <div class="col-md-12 col-xs-12 nopad bordrit hide">
								            <div class="col-md-12 col-xs-12 nopad">
								               <div class="col-md-12 col-xs-12 sideprice"> <?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?> <?php echo number_format($room_rate,2); ?> <span class="avgper">Total</span> </div>
								            </div>
								         </div>
								      </div>
								   </div>
								</div>
								<?php } ?>
							</div>
						</div>
						<!-- Rooms End--> 
						<!-- Facilities-->
						<div class="tab-pane" id="facility">
							<div class="innertabs">
                            <div class="dets_hotels">
								<div class="col-xs-12 nopad">
									<ul class="checklist">
										<?php 
											//debug($room_list['room_amenity']);exit;
											if(valid_array($room_list['room_amenity']) == true) {
												$amen_cat = count($room_list['room_amenity']);
												for($ame=0; $ame < $amen_cat; $ame++) {
													if($room_list['room_amenity'][$ame] != '')
													{
													?>				
													<li> <span class="fa fa-check-square-o"></span>
													<strong><?php echo $room_list['room_amenity'][$ame]['amenities_name'];?></strong>
													<!-- <ul>
														<?php 
														if($hotel_details['hotel_amenities'][$ame] !=''){

															if(valid_array($hotel_details['amenities']['value'][$ame])){
															$amen_val = count($hotel_details['amenities']['value'][$ame]);
															for($s_ame=0; $s_ame < $amen_val; $s_ame++) {  ?>
															<li> <span class=""></span><?php echo $hotel_details['amenities']['value'][$ame][$s_ame];?></li>
													<?php } }else{ echo "<li><span class=''></span>".$hotel_details['amenities']['value'][$ame]."</li>";} } ?>
													</ul> -->
													</li>
										<?php	
													}													
												}
											}
											?>
									</ul>
								</div>
								<p class="mar_t20">Facilities marked with <strong class="text-danger">*</strong> are available at additional cost</p>
							</div>
                            </div>
						</div>
						<div class="tab-pane" id="hotelpolicy">
								<div class="innertabs">
		                            <div class="dets_hotels">
										<div class="col-xs-12 nopad">
											<?php
												if(isset($hotel_details['hotel_notice']) && $hotel_details['hotel_notice']){
													echo $hotel_details['hotel_notice'];
												}else{
													echo "No Policy Found";
												}
											?>
											
										</div>
									</div>
								</div>
						</div>
						
						<div class="tab-pane" id="tripadvisor">
								<div class="innertabs">
		                            <div class="dets_hotels">
										<div class="col-xs-12 nopad">										
											<iframe src="<?php echo $trip_review_url?>" width="100%" height="100%"></iframe>
										</div>
									</div>
								</div>
						</div>
						<div class="tab-pane" id="post">
							<div class="innertabs">
	                            <div class="dets_hotels">
									<?php echo $this->template->isolated_view('hotel/post_review'); ?>
								</div>
                            </div>
						</div>

						<!-- Facilities End--> 
						<!-- Reviews-->
<!--						<div class="tab-pane" id="policy">-->
<!--							<div class="innertabs">-->
<!--								<ul class="list_detail_tab">-->
<!--									<li class="listcancel">-->
<!--										Lorem Ipsum is simply dummy text of the printing and typesetting industry.-->
<!--									</li>-->
<!--									<li class="listcancel">-->
<!--										Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.-->
<!--									</li>-->
<!--									<li class="listcancel">-->
<!--										It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.-->
<!--									</li>-->
<!--								</ul>-->
<!--							</div>-->
<!--						</div>-->
						<!-- Reviews End--> 
					</div>
				</div>
			</div>
		</div>
		<?php
			$hote_count = count($search_hotel_list['HotelSearchResult']);
		?>
		<?php if($hote_count>=4):?>
		<div class="col-md-4 col-sm-6 col-xs-12 nopad">
			<div class="contact_hotel">
				<h3 class="head_hotel">Similar Hotels</h3>

			<!-- 	<img class="pull-left" src="<?=$GLOBALS['CI']->template->template_images('similar_hotel.jpg'); ?>"> -->

			<div id="carousel-example-vertical" class="carousel vertical slide" data-interval="false">
		      <div class="carousel-inner" role="listbox">
		     <?php 
		      	$count_x=0;
				$max_range = $min_rate + ((20/100)*$min_rate);
				$min_range = $min_rate - ((20/100)*$min_rate);	
				//debug($search_hotel_list['HotelSearchResult']);exit;
				foreach( $search_hotel_list['HotelSearchResult'] as $similar_hotel) 
				{	
					
					$nex_similar_hotel = next($search_hotel_list['HotelSearchResult']);
					//echo 'Similar Hotel '.$similar_hotel['hotel_code'].'<br>';				
					if($count_x<=15 && ($star_rating==$similar_hotel['star_rating'] &&  $similar_hotel['hotel_code']!=$hotel_code) || ( $similar_hotel['total_price'] >= $min_range && $max_range >= $similar_hotel['total_price']) && !empty($similar_hotel['image']) && ($hotel_code !=$nex_similar_hotel['hotel_code']))
					{
						if($similar_hotel['hotel_code']!=$hotel_code && $nex_similar_hotel['hotel_code'] !=$hotel_code ){
						// echo $similar_hotel['hotel_code'].',';
							if($count_x==0)
						{
							$active="active";
							}else{
							$active="";
						}
		      	?>
		        <div class="item <?=$active?>">
		        	<div class="outerfullfuture">
		        		<div class="ourdest">
		        			<?php 
								if(!empty($similar_hotel['image'])) { $img = $img_url.$mediumImage_baseUrl.$similar_hotel['image']; ?>
								<img src="<?=$img?>" alt="<?=$similar_hotel['hotel_name']?>" onerror="javascript:this.src='<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>'" />
								<?php }else{ ?>
									<img src="<?=$template_images;?>no_image.png" alt="<?=$similar_hotel['name']?>" />
								<?php } 
							?>		        			
		        			<span class="destplace">
								<div class="surbtm1">
									<div class="cntdes1"><?=$similar_hotel['hotel_name']?></div>
									<div class="cntdes1 price_ht"><strong><?=$currency.' '.$similar_hotel['total_price']?> </strong></div> 
									<div class="stra_hotel"  data-star="<?=$similar_hotel['star_rating']?>">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span> 
									</div>
								</div>
								<div class="destbtm2">
									<a href="<?=base_url().'hotel/hotel_details/'.$search_id.'?booking_source='.$_REQUEST['booking_source'].'&hotel_id='.$similar_hotel['hotel_code']?>" class="bk_btn">Book</a>
								</div>
							</span>
		        		</div>
		        	</div>
		        	<div class="clearfix"></div>
		        	<?php if($nex_similar_hotel):?>
		        	<div class="outerfullfuture">
		        		<div class="ourdest">
		        			<?php 
								if(!empty($nex_similar_hotel['image'])) { $img = $img_url.$mediumImage_baseUrl.$nex_similar_hotel['image']; ?>
								<img src="<?=$img?>" alt="<?=$nex_similar_hotel['hotel_name']?>" >
								<?php }else{ ?>
									<img src="<?=$template_images;?>no_image.png" alt="<?=$nex_similar_hotel['name']?>" >
								<?php } 
							?>	

		        			<img src="<?=$template_images;?>no_image.png">
		        			<span class="destplace">
								<div class="surbtm1">
									<div class="cntdes1"><?=$nex_similar_hotel['hotel_name']?></div>
									<div class="cntdes1 price_ht"><strong><?=$currency.' '.$nex_similar_hotel['total_price']?>  </strong></div> 
									<div class="stra_hotel"  data-star="<?=$nex_similar_hotel['star_rating']?>">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span> 
									</div>
								</div>
								<div class="destbtm2">
									<a href="<?=base_url().'hotel/hotel_details/'.$search_id.'?booking_source='.$_REQUEST['booking_source'].'&hotel_id='.$nex_similar_hotel['hotel_code']?>" class="bk_btn">Book</a>
								</div>
							</span>
		        		</div>
		        	</div>
		        <?php endif;?>
		        </div>
		  	 <?php
						$count_x++;
						if($count_x == 20) break;
						}
					}					
				}?>
		       
<!--
		        <?php
				$count_x=0;
				$max_range = $min_rate + ((20/100)*$min_rate);
				$min_range = $min_rate - ((20/100)*$min_rate);
				//debug($search_hotel_list); 
				foreach ($search_hotel_list['HotelSearchResult'] as $similar_hotel) 
				{	

					//echo 'Similar Hotel '.$similar_hotel['hotel_code'].'<br>';				
					if($count_x<=5 && ($star_rating==$similar_hotel['star_rating'] &&  $similar_hotel['hotel_code']!=$hotel_code) || ( $similar_hotel['total_price'] >= $min_range && $max_range >= $similar_hotel['total_price']) && !empty($similar_hotel['image']))
					{
						if($similar_hotel['hotel_code']!=$hotel_code){
						// echo $similar_hotel['hotel_code'].',';
							if($count_x==0)
						{
							$active="active";
						}else{
						$active="";
					}
						?>

						<div class="item <?=$active?>">
							<div class="outerfullfuture">
								<div class="ourdest">
									<?php 
									if(!empty($similar_hotel['image'])) { $img = $img_url.$mediumImage_baseUrl.$similar_hotel['image']; ?>
									<img src="<?=$img?>" alt="<?=$similar_hotel['hotel_name']?>" onerror="javascript:this.src='<?php echo $GLOBALS['CI']->template->template_images('no_image.png'); ?>'" />
									<?php }else{ ?>
										<img src="<?=$template_images;?>no_image.png" alt="<?=$similar_hotel['name']?>" />
									<?php } ?>
									<span class="destplace">
										<div class="surbtm1">
											<div class="cntdes1"><?=$similar_hotel['hotel_name']?></div>
											<div class="cntdes1 price_ht"><strong><?=$currency.' '.$similar_hotel['total_price']?> </strong></div> 
											<div class="stra_hotel" data-star="<?=$star_rating?>">
												<span class="fa fa-star"></span>
												<span class="fa fa-star"></span>
												<span class="fa fa-star"></span>
												<span class="fa fa-star"></span>
												<span class="fa fa-star"></span> 
											</div>
										</div>
										<div class="destbtm2">
											<a href="<?=base_url().'hotel/hotel_details/'.$search_id.'?booking_source='.$_REQUEST['booking_source'].'&hotel_id='.$similar_hotel['hotel_code']?>" class="bk_btn">Book</a>
										</div>
									</span>
								</div>
							</div>
						</div>
						<?php
						$count_x++;
						if($count_x == 20) break;
						}
					}					
				}
				?>-->
		        
		      </div>
		  
		      <!-- Controls -->
		      <a class="up carousel-control" href="#carousel-example-vertical" role="button" data-slide="prev">
		        <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
		        <span class="sr-only">Previous</span>
		      </a>
		      <a class="down carousel-control" href="#carousel-example-vertical" role="button" data-slide="next">
		        <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
		        <span class="sr-only">Next</span>
		      </a>
		    </div>
			<?php endif;?>
	           <div class="clearfix"></div>
				<div class="inside_contact hide">
					<div class="clearfix"></div>
					<?php if(isset($hotelEmail) && !empty($hotelEmail)) {
						?>
					<div class="row_contact">
						<span class="fa fa-envelope"></span>
						<strong><?php echo $hotelEmail; ?></strong>
					</div>
					<?php 
						}
						if(isset($terminal) && !empty($terminal)) {
						?>
					<div class="row_terminal">
						<span class="fa fa-plane"></span>
						Nearest Terminal is <?php echo $terminal;?> KM.
					</div>
					<?php
						}?>
					<div class="clearfix"></div>
					<div class="map_contact">
						<div class="row_contact">
							<span class="fa fa-map-marker"></span>
							<strong><?php echo $hotel_address . $postal;?></strong>
						</div>
					</div>
				</div>
			</div>
		</div>

		
		<div class="col-md-4 col-xs-12  nopad nearby-hotels" id="nearby-hotels">
					<?php $contactStr = '';
			if(isset($hotelContact) && valid_array($hotelContact)) {
				
				foreach($hotelContact as $cKey => $contactarr) {
					$contactStr = str_replace("-Switch","",$contactarr);
					//$contactStr = ($contactStr != '') ? $contactStr . ',' .$contactarr['contactN'] : $contactarr['contactN'];
				}
			}
			?>
		<div class="col-xs-12 nopad dealday">
			<div class="contact_hotel">
				<?php /* ?>
				<!-- <h3 class="head_hotel">Deal of the day</h3> -->
			<!-- 	<img class="pull-left" src="<?=$GLOBALS['CI']->template->template_images('similar_hotel.jpg'); ?>"> -->


			<div id="owl_demo_2" class="owl-carousel owlindex3 owl-theme">
				<?php
				$count_x=0;
				$max_range = $min_rate + ((20/100)*$min_rate);
				$min_range = $min_rate - ((20/100)*$min_rate);
				foreach ($search_hotel_list as $similar_hotel) 
				{					
					if($count_x<=5 && ($star_rating==$similar_hotel['star_rating'] &&  $similar_hotel['hotel_code']!=$hotel_code) || ( $similar_hotel['price'] >= $min_range && $max_range >= $similar_hotel['price']))
					{
						// echo $similar_hotel['hotel_code'].',';
						?>
						<div class="item">
							<div class="outerfullfuture">
								<div class="ourdest">
									<img src="<?=$similar_hotel['img']?>" alt="<?=$similar_hotel['name']?>" />
									<span class="destplace">
										<div class="surbtm1">
											<div class="cntdes1"><?=$similar_hotel['name']?></div>
											<div class="cntdes1 price_ht"><strong><?=$similar_hotel['price'].' '.$currency?> </strong>Avg / Night</div> 
											<div class="stra_hotel" data-star="<?=$star_rating?>">
												<span class="fa fa-star"></span>
												<span class="fa fa-star"></span>
												<span class="fa fa-star"></span>
												<span class="fa fa-star"></span>
												<span class="fa fa-star"></span> 
											</div>
										</div>
										<div class="destbtm2">
											<a href="<?=base_url().'hotel/hotel_details/'.$search_id.'?booking_source='.$_REQUEST['booking_source'].'&hotel_id='.$similar_hotel['hotel_code']?>" class="bk_btn">Book</a>
										</div>
									</span>
								</div>
							</div>
						</div>
						<?php
						$count_x++;
					}					
				}
				?>
			</div>
			<?php */ ?>
	           <div class="clearfix"></div>
				<div class="inside_contact hide">
					<div class="clearfix"></div>
					<?php if(isset($hotelEmail) && !empty($hotelEmail)) {
						?>
					<div class="row_contact">
						<span class="fa fa-envelope"></span>
						<strong><?php echo $hotelEmail; ?></strong>
					</div>
					<?php 
						}
						if(isset($terminal) && !empty($terminal)) {
						?>
					<div class="row_terminal">
						<span class="fa fa-plane"></span>
						Nearest Terminal is <?php echo $terminal;?> KM.
					</div>
					<?php
						}?>
					<div class="clearfix"></div>
					<div class="map_contact">
						<div class="row_contact">
							<span class="fa fa-map-marker"></span>
							<strong><?php echo $hotel_address . $postal;?></strong>
						</div>
					</div>
				</div>
			</div>
		</div>


			<!-- <div class="contact_hotel">
				<h3 class="head_hotel">Deal of the day</h3>
				<img class="pull-left" src="<?=$GLOBALS['CI']->template->template_images('similar_hotel.jpg'); ?>">
			</div> -->
			<div class="clearfix"></div>

			<?php 

			if(isset($advertisement['link']) && $advertisement['link']){
				?>
				<a href="<?=$advertisement['link']?>" target="<?=$advertisement['target']?>">
				<?php
			}
			if(isset($advertisement['slider_image']) && $advertisement['slider_image']){
			?>
			<img class="pull-left" src="<?=$GLOBALS['CI']->template->domain_images($advertisement['slider_image']); ?>">
				<?php 
			}
			if( isset($advertisement['slider_image']) && $advertisement['link']){
				?>
				</a>
				<?php
			}
			?>
			<?php
				/*if(isset($hotel_details['hotel_nearby']) && COUNT($hotel_details['hotel_nearby']) > 0 && !empty($hotel_details['hotel_nearby'])) {
				?>
			<div class="contact_hotel">
				<h3 class="head_hotel">You may also like</h3>
				<div class="umay_also">
					<?php 
						foreach($hotel_details['hotel_nearby'] as $nKey => $nearby) {
							$imagePath_near = '';
							$imagePath_near = isset($nearby->image_path) && !empty($nearby->image_path) ? $smallImage_baseUrl . $nearby->image_path : base_url() . '/extras/system/template_list/template_v1/images/no_image.png';
							?>
					<a class="rowlikes" target=_blank href="<?=base_url()?>index.php/hotel/hotel_details/<?=$search_id?>?booking_source=<?=$active_booking_source?>&hotel_id=<?=$nearby->hotel_code?>">
						<div class="hotlealso"> <img src="<?=$imagePath_near;?>" alt="" /></div>
						<div class="alsodets">
							<div class="alsohed"><?php echo $nearby->hotel_name; ?></div>
							<div class="address-suggested"><?php if(isset($nearby->address) && !empty($nearby->address))echo $nearby->address;?></div>
						</div>
					</a>
					<?php
						}
						?>
				</div>
			</div>
			<?php
				}*/
				?>
		</div>
	</div>
</div>
<!-- Page Content -->
<!-- Hotel Location Map Starts -->
<div class="modal fade bs-example-modal-lg" id="map-box-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Hotel Location Map</h4>
			</div>
			<div class="modal-body">
				<iframe src="" id="map-box-frame" name="map_box_frame" style="height: 500px;width: 850px;">
				</iframe>
			</div>
		</div>
	</div>
</div>
<!-- Hotel Location Map Ends -->

<?php echo $GLOBALS['CI']->template->isolated_view('share/flight_session_expiry_popup');?>

<script type="text/javascript">
$(document).ready(function(){

$("#owl_demo_1").owlCarousel({
    items : 1, 
    itemsDesktop : [1000,1],
    itemsDesktopSmall : [900,1], 
    itemsTablet: [768,1], 
    itemsMobile : [479,1], 
    navigation : true,
    pagination : false
});		
$("#owl_demo_2").owlCarousel({
    items : 1, 
    itemsDesktop : [1000,1],
    itemsDesktopSmall : [900,1], 
    itemsTablet: [768,2], 
    itemsMobile : [479,1], 
    navigation : true,
    pagination : false
});
  
	
	$(".tooltipv").tooltip();
	
	$(".provabslideshow").provabslideshow({
            mobile: true
        });
	
	
	$(document).on('click', '.location-map', function() {
		$('#map-box-modal').modal();
	});
	$('.room_select').click(function(){
		$('#room-list').trigger('click');
		var topmove = $('.room_select').offset().top;
		$('html, body').animate({scrollTop: topmove}, 500);
	});
	var sync1 = $("#sync1");
	var sync2 = $("#sync2");
	sync1.owlCarousel({
		singleItem : true,
		slideSpeed : 1000,
		navigation: true,
		pagination:false,
		afterAction : syncPosition,
		responsiveRefreshRate : 200,
	});

	sync2.owlCarousel({
		items : 6,
		itemsDesktop	: [1199,4],
		itemsDesktopSmall	 : [979,4],
		itemsTablet	 : [768,4],
		itemsMobile	 : [479,2],
		pagination:false,
		responsiveRefreshRate : 100,
		afterInit : function(el){
		el.find(".owl-item").eq(0).addClass("synced");
		}
	});

	function syncPosition(el){
		var current = this.currentItem;
		$("#sync2")
		.find(".owl-item")
		.removeClass("synced")
		.eq(current)
		.addClass("synced")
		if($("#sync2").data("owlCarousel") !== undefined){
		center(current)
		}

	}

	$("#sync2").on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).data("owlItem");
		sync1.trigger("owl.goTo",number);
	});

	function center(number){
		var sync2visible = sync2.data("owlCarousel").owl.visibleItems;

		var num = number;
		var found = false;
		for(var i in sync2visible){
		if(num === sync2visible[i]){
			var found = true;
		}
		}

		if(found===false){
		if(num>sync2visible[sync2visible.length-1]){
			sync2.trigger("owl.goTo", num - sync2visible.length+2)
		}else{
			if(num - 1 === -1){
			num = 0;
			}
			sync2.trigger("owl.goTo", num);
		}
		} else if(num === sync2visible[sync2visible.length-1]){
		sync2.trigger("owl.goTo", sync2visible[1])
		} else if(num === sync2visible[0]){
		sync2.trigger("owl.goTo", num-1)
		}
	}
});

/*function facilitypsw() {
	window.setTimeout( function () { top(); }, 2000 );
	$('#facilitypsw').addClass('active');
	$('#facilitypsr').removeClass('active');
	$('#facilitypsh').removeClass('active');
	$('#facilitypost').removeClass('active');
	// body...
}*/
function facilitypsw() {
    $('html,body').animate({
        scrollTop: $(".shdoww").offset().top},
        'slow');
	$('#facilitypsw').addClass('active');
	$('#facilitypsr').removeClass('active');
	$('#facilitypsh').removeClass('active');
	$('#facilitypost').removeClass('active');
	$('#hotelpolicysw').removeClass('active');
	$("#tripadvisorw").removeClass('active');
}
function tripadvisorw(){
	$('html,body').animate({
        scrollTop: $(".shdoww").offset().top},
        'slow');
	$('#facilitypsw').removeClass('active');
	$('#facilitypsr').removeClass('active');
	$('#facilitypsh').removeClass('active');
	$('#facilitypost').removeClass('active');
	$('#hotelpolicysw').removeClass('active');
	$("#tripadvisorw").addClass('active');
}
</script>

<script type="text/javascript">
	$('.stra_hotel .fa').click(function() {
    $(this).toggleClass("active");
});
</script>
<script>
	$(document).ready(function(){
	    // $(".all_amints").click(function(){
	    //     $(".shw_aminities").toggle();
	    // });
	});
</script>
<script>

function showMore(obj) {
	var id = obj.id;
	var className = '.room-details-facilities-more' + id;
	var morelink = '.more-room-facilities' + id;
	if($(className).hasClass('hide')) {
		console.log(morelink);
		$(className).removeClass('hide');
		$(morelink).html('Show Less');
	}else {
		console.log(morelink);
		$(className).addClass('hide');
		$(morelink).html('Show More');
	}
}
</script>