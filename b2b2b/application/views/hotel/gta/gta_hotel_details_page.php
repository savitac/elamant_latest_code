<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title></title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo ASSETS;?>assets/css/log_in.css" rel="stylesheet" type="text/css">
		<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
		<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" />
		<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" />
		<link href="<?php echo ASSETS;?>assets/css/theme_style.css" rel="stylesheet" /-->
		<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="<?php echo ASSETS;?>assets/gta/hotel_result.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ASSETS;?>assets/gta/hotel_detail.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ASSETS;?>assets/gta/hotel_result.css">
		<script src="<?php echo ASSETS;?>assets/js/jquery-1.11.0.js"></script>
		<script src="<?php echo ASSETS;?>assets/js/jquery_ui.js"></script>
		<script src="<?php echo ASSETS;?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo ASSETS;?>assets/js/general.js"></script>
		<script src="<?php echo ASSETS;?>assets/gta/owl.carousel.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script>
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/map.js"></script>
    </head>
    <body>
    	  <?php 
        if($this->session->userdata('user_logged_in') == 0) {  
			echo $this->load->view('core/header'); 
		} else {
			echo $this->load->view('dashboard/top'); 	 
		}

	 ?>
<script>
$(document).ready(function() {
	loadNearbyHotels();
	var app_base_url = "<?php echo base_url(); ?>";
	function loadNearbyHotels() {
		$.ajax({
			type: 'GET',
			url: app_base_url+'index.php/ajax/nearby_hotels/?booking_source=<?=$_REQUEST['booking_source']?>&latitude=<?=$hotel_details['latitude'] ?>&longitude=<?=$hotel_details['longitude'] ?>&hotel_code=<?=$hotel_details["hotel_code"]?>&search_id=<?=$hotel_search_params["search_id"]?>&op=load',
			//dataType: 'json',
			success: function(res) {
				if(res.status == true) {
					$('#nearby-hotels').append(res.data);
				}
			}
		});
	}

	
	
});
function showMore(obj) {
	var id = obj.id;
	var className = '.room-details-facilities-more' + id;
	var morelink = '.more-room-facilities' + id;
	if($(className).hasClass('hide')) {
		console.log(morelink);
		$(className).removeClass('hide');
		$(morelink).html('Show Less');
	}else {
		console.log(morelink);
		$(className).addClass('hide');
		$(morelink).html('Show More');
	}
}
</script>
<?php
//debug($hotel_details); exit("  main result");
//debug(array_sum($hotel_search_params['adult_config']));
$pscheckin = date('d M Y',strtotime($hotel_search_params['from_date']));
$pscheckout = date('d M Y',strtotime($hotel_search_params['to_date']));
/*$template_images	= $GLOBALS['CI']->template->template_images();*/
//******* Search Params *******//
$search_id			= $hotel_search_params['search_id'];
$number_of_nigths	= $hotel_search_params['no_of_nights'];
$number_of_rooms	= $hotel_search_params['room_count'];
$number_of_adults	= array_sum($hotel_search_params['adult_config']);
$number_of_childs	= array_sum($hotel_search_params['child_config']);
//******* Dynamic Data *******//
$hotel_code				= $hotel_details['hotel_code'];
$hotel_name				= $hotel_details['hotel_name'];
$star_rating			= intval($hotel_details['star_rating']);
$destination			= $hotel_details['destination'];
$check_in				= $hotel_details['checkIn'];
$check_out				= $hotel_details['checkOut'];
$latitude				= $hotel_details['latitude'];
$longitude				= $hotel_details['longitude'];
$min_rate				= $hotel_details['minRate']; //$hotel_details['minRate'];
$max_rate				= $hotel_details['maxRate'];
$currency				= $hotel_details['currency'];
$room_list				= $hotel_details['rooms'];
$print_format_room_list	= $hotel_details['formated_room_list'];
$terminal				= @$hotel_details['hotel_distance'];
//******* Static Data *******//FIXME: Get Hotel Static Data from Local Database
$hotel_image			= @$hotel_details['thumbnails'];
$mediumImage_baseUrl	= @$hotel_details['medium_image_baseUrl'];
$smallImage_baseUrl		= @$hotel_details['small_image_baseUrl'];
$hotelMapImg			= ($hotel_image[0] != '') ? $hotel_image[0] : '';
$hotelContact			= $hotel_details['contact'];
$hotel_address			= @$hotel_details['address'];
$postal					= @$hotel_detail['postal'];
$hotel_ameneties		= '';
$contact_details		= @$hotel_details['hotel_static_contact_num'];
$hotel_description		= @$hotel_details['description'];
$hotel_facilities		= @$hotel_details['facilities'];
$hotel_policies			= '';
$hotelEmail				= @$hotel_details['email'];
//******* cached Token Details *******//
$Token					= @$hotel_details['Token'];
$TokenKey				= @$hotel_details['TokenKey'];
 
?>


<script type="text/javascript">
	/*
		session time out variables defined
	*/
	var  search_session_expiry = "<?php echo $GLOBALS ['CI']->config->item ( 'flight_search_session_expiry_period' ); ?>";
	var search_session_alert_expiry = "<?php echo $GLOBALS ['CI']->config->item ( 'flight_search_session_expiry_alert_period' ); ?>";
	var search_hash = "<?php echo $session_expiry_details['search_hash']; ?>";
	var start_time = "<?php echo $session_expiry_details['session_start_time']; ?>";
	var session_time_out_function_call = 1; 
</script>

<style type="text/css">
	
	

.outerfullfuture{ background: #fff none repeat scroll 0 0;
    display: block;
    margin: 0px;
box-shadow: 0 0 0 0}

.ourdest {
   display: block;
    min-height: 298px;
    max-height: 298px;
    max-width: 100%;
    overflow: hidden;
    position: relative;
}
.ourdest img{
    width: 100%;
 min-height: 298px;
    max-height: 298px;}
.destplace {
      bottom: 0px;
    color: #fff;
    font-size: 14px;
    left: 0;
    padding: 5px 15px;
    position: absolute;
    right: 0;
    z-index: 1;
    background: rgba(0, 0, 0, 0.7) none repeat scroll 0 0;
  
}
.cntdes1{
	    color: #fff;
    font-weight: 200;
    padding: 1px 0;
    font-size: 15px;
}

.price_ht strong{
	color: #06b5f1;
    font-weight: 400;
}
.star{
	color:#ff9800;
}
.surbtm1{
	float:left;
}
.destbtm2{
	float: right;
    width: 30%;
        margin: 15px 3%;
}
.bk_btn{

	background: #fdb912 none repeat scroll 0 0;
    border: 1px solid #fdb912;
    color: #000;
    display: block;
    font-size: 15px;
    overflow: hidden;
    padding: 7px 0;
    text-align: center;
    width: 100%;
    border-radius: 3px;
    text-transform: uppercase;
}
.tooltip { width: auto !important; float: left; background: none !important; border-radius: 3px;} 
.tooltip.left { padding: 0px !important; }
.tooltip-inner { padding: 2px 7px !important; background: #333 !important; max-width: 100% !important; }
.tooltip-inner .table { margin-bottom: 0px !important; background: #333 !important; }
.tooltip.left .tooltip-arrow { right: -5px !important; border-left-color: #333; }
.tooltip.in { opacity: 1 !important; }
</style>


<div class="fldealsec">
  <div class="container">
    <div class="tabcontnue">
      <div class="breadli nopadding">
        <div class="rondsts success">
          <a class="taba core_review_tab <?php echo @$strQuat_2; ?>review_tab_marker" id="stepbk1">
            <!-- <div class="iconstatus fa fa-list-alt"></div> -->
            <div class="iconstatus"><div class="stausline">Search Results</div></div>
            
          </a>
        </div>
      </div>

      <div class="breadli nopadding">
        <div class="rondsts active">
          <a class="taba core_review_tab <?php echo @$strQuat_2; ?>review_tab_marker" id="stepbk1">
            <!-- <div class="iconstatus fa fa-list-alt"></div> -->
            <div class="iconstatus"><div class="stausline">Select Rooms</div></div>
            
          </a>
        </div>
      </div>

      <div class="breadli nopadding">
        <div class="rondsts">
          <a class="taba core_review_tab <?php echo @$strQuat_2; ?>review_tab_marker" id="stepbk1">
            <!-- <div class="iconstatus fa fa-list-alt"></div> -->
            <div class="iconstatus"><div class="stausline">Guest Details</div></div>
            
          </a>
        </div>
      </div>

      <div class="breadli nopadding">
        <div class="rondsts">
          <a class="taba core_travellers_tab <?php echo @$strPay_2; ?>travellers_tab_marker"
            id="stepbk2">
            <!-- <div class="iconstatus fa fa-money"></div> -->
            <div class="iconstatus"><div class="stausline">Payment Method</div></div>
            
          </a>
        </div>
      </div>
      <div class="breadli nopadding">
        <div class="rondsts <?php echo @$strConf_1; ?>">
          <a class="taba" id="stepbk3">
            <!-- <div class="iconstatus fa fa-check"></div> -->
            <div class="iconstatus"><div class="stausline">Confirmation</div></div>
            
          </a>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Header Carousel -->
<div class="allpagewrp top80">
	<div class="newmodify">
		<div class="container">
			<div class="contentsdw">
				<div class="col-sm-5 col-xs-8 nopad">
					<div class="detail_htlname"><?=$hotel_name?> - Hotel</div>
					<div class="star_detail">
						<div class="stra_hotel" data-star="<?=$star_rating?>">
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="detal_htladrs"><?=$hotel_address?></div>
				</div>
				<div class="col-md-4 col-sm-3 col-xs-4 nopad">
					<!--
						<a class="view_map_dets">
						   	<span class="fa fa-map-marker"></span>
						       View map
						   </a>-->
					<a class="view_map_dets location-map" href="<?php echo base_url().'index.php/hotel/map?lat='.$latitude.'&lon='.$longitude.'&hn='.urlencode($hotel_name).'&sr='.intval($star_rating).'&c='.urlencode($destination).'&img='.urlencode($hotelMapImg)	?>" target="map_box_frame">
					<strong>View Map</strong>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="dets_section">
	<div class="container">
		<div class="col-md-4 col-xs-12 nopad">
			<div class="leftslider">
				<div id="sync1" class="owl-carousel detowl">
					<?php 
						if(isset($hotel_image) && valid_array($hotel_image)) {
							foreach($hotel_image as $hmKey => $image) {
								?>

								<a href="<?=$image;?>" class="item provabslideshow" data-gallery="gallery">
									<div class="bighotl">
			                        <img src="<?=$image;?>" alt="" onerror="" />
			                        </div>
								</a>


								<?php 
							}
						}else {
						?><img src=""/><?php 
						}
					?>
				</div>
				<div id="sync2" class="owl-carousel syncslide" style="margin-top: 10px;">
					<?php 
						if(isset($hotel_image) && valid_array($hotel_image)) {
							foreach($hotel_image as $hmKey => $image) {
								?> 

								<div class="item" style="padding-right:5px;">
									<div class="bighotl"><img src="<?=$image;?>" alt="" height="60" onerror=""/></div>
								</div>

								<?php 
							}
						}
						?>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 nopad">
			<div class="inside_detsy">
				<div class="dets_hotel">
				<div class="col-md-6 nopad">
					<span class="hotel_address elipsetool">Check In: <?= $pscheckin;?></span>
			    </div>
			    
			    <div class="col-md-6 nopad">		
					<span class="hotel_address elipsetool">Check Out: <?= $pscheckout;?></span>
			    </div>	

			    <div class="col-md-6 nopadding_left">		
					<span class="hotel_address elipsetool">Adult(s): <?= array_sum($hotel_search_params['adult_config']);?></span>
					<?php if(array_sum($hotel_search_params['child_config']) > 0){?>
					<span class="hotel_address elipsetool">No. of Children : <?= array_sum($hotel_search_params['child_config']);?></span>
					<?php 
					$i = 1; 

					foreach($hotel_search_params['child_age'] as $age){ ?>
					<span class="hotel_address elipsetool">Child <?php echo $i; ?> : <?php echo $age; ?> Yrs </span>
			    	<?php $i++; } } ?>
			    </div>		
					<div class="clearfix"></div>
					<div class="side_amnties" style="margin:0px;">
						<ul class="hotel_fac">
						<?php 
						if(isset($hotel_facilities) && COUNT($hotel_facilities) > 0) {
							$facilities_strArr[] = array();
							$aCnt = 1;
							foreach($hotel_facilities as $aKey => $aminities) {
								if(isset($aminities['icon_class']) && (empty($facilities_strArr) || (valid_array($facilities_strArr) && !in_array($aminities['icon_class'],$facilities_strArr)))) {
						?>
								<li class="<?php echo $aminities['icon_class']; ?> tooltipv h-f" title="<?php echo $aminities['name'];?>"></li>
						<?php
									$facilities_strArr[] = $aminities['icon_class'];
									if($aCnt == 5) {break;}
									$aCnt++;
								}
							}
						}
						?> 
	
						<a style="line-height: 40px;" href="#facility" data-toggle="tab" onclick="facilitypsw()">See More</a>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="price_strts">
					<div class="price_froms">starting from<strong><?=$currency?> <?=number_format($min_rate,2)?></strong></div>
				</div>
				<div class="clearfix"></div>
				<a class="room_select">Select Room</a>
			</div>
		</div>
		<?php $contactStr = '';
			if(isset($hotelContact) && valid_array($hotelContact)) {
				
				foreach($hotelContact as $cKey => $contactarr) {
					$contactStr = ($contactStr != '') ? $contactStr . ',' .$contactarr['contactN'] : $contactarr['contactN'];
				}
			}
			?>
			<div class="col-md-4 col-sm-6 col-xs-12 nopad">
				<div class="contact_hotel">
					
					<h3 class="head_hotel">Similar Hotels</h3>
					<div id="owl_demo_1" class="owl-carousel owlindex3 owl-theme">
						<?php
						$count_x=0;
						$max_range = $min_rate + ((20/100)*$min_rate);
						$min_range = $min_rate - ((20/100)*$min_rate);
						foreach ($search_hotel_list as $similar_hotel) 
						{					
							if($count_x<=5 && ($star_rating==$similar_hotel['star_rating'] &&  $similar_hotel['hotel_code']!=$hotel_code) || ( $similar_hotel['price'] >= $min_range && $max_range >= $similar_hotel['price']))
							{
								if($similar_hotel['hotel_code']!=$hotel_code){
								?>
								<div class="item">
									<div class="outerfullfuture">
										<div class="ourdest">
											<?php if(array_key_exists('img', $similar_hotel) && $similar_hotel['img']!=''){ ?>
											<img src="<?=$similar_hotel['img']?>" alt="<?=$similar_hotel['name']?>" onerror=""/>
											<?php }else{ ?>
												<img src="<?=NO_IMG?>" alt="<?=$similar_hotel['name']?>" />
											<?php } ?>
											<span class="destplace">
												<div class="surbtm1">
													<div class="cntdes1"><?=$similar_hotel['name']?></div>
													<div class="cntdes1 price_ht"><strong><?=$currency.' '.number_format($similar_hotel['price'],2)?> </strong></div> 
													<div class="stra_hotel" data-star="<?=$star_rating?>">
														<span class="fa fa-star"></span>
														<span class="fa fa-star"></span>
														<span class="fa fa-star"></span>
														<span class="fa fa-star"></span>
														<span class="fa fa-star"></span> 
													</div>
												</div>
												<div class="destbtm2">
													<a href="<?=base_url().'hotel/hotel_details/'.$search_id.'?booking_source='.$_REQUEST['booking_source'].'&hotel_id='.$similar_hotel['hotel_code']?>" class="bk_btn">Book</a>
												</div>
											</span>
										</div>
									</div>
								</div>
								<?php
								$count_x++;
								}
							}					
						}
						?>
					</div>		
			<!-- <div id="owl_demo_1" class="owl-carousel owlindex3 owl-theme">
	              <div class="item">
	                <div class="outerfullfuture">
	                  <div class="ourdest"><img src="http://images.gta-travel.com/HH/Images/RS/SIN/SIN-LIN-21.jpg" alt="" />
	                  <span class="destplace">
	                  	<div class="surbtm1">
	                  			<div class="cntdes1">Hotel Amaragua</div>
                                <div class="cntdes1 price_ht"><strong>$36 - $160 </strong>avg / night</div> 
                                <div class="cntdes1 star">
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span> 
	                            </div>
                        </div>
                        <div class="destbtm2">
                               <a class="bk_btn">Book</a>
                        </div>
	                  </span>


	                  </div>
	                  
	                </div>
	              </div>
	             <div class="item">
	                <div class="outerfullfuture">
	                  <div class="ourdest"><img src="http://images.gta-travel.com/HH/Images/RS/SIN/SIN-LIN-21.jpg" alt="" />
	                  <span class="destplace">
	                  	<div class="surbtm1">
	                  			<div class="cntdes1">Hotel Amaragua</div>
                                <div class="cntdes1 price_ht"><strong>$36 - $160 </strong>avg / night</div> 
                                <div class="cntdes1 star">
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span> 
	                            </div>
                        </div>
                        <div class="destbtm2">
                               <a class="bk_btn">Book</a>
                        </div>
	                  </span>


	                  </div>
	                  
	                </div>
	              </div>
	              <div class="item">
	                <div class="outerfullfuture">
	                  <div class="ourdest"><img src="http://images.gta-travel.com/HH/Images/RS/SIN/SIN-LIN-21.jpg" alt="" />
	                  <span class="destplace">
	                  	<div class="surbtm1">
	                  			<div class="cntdes1">Hotel Amaragua</div>
                                <div class="cntdes1 price_ht"><strong>$36 - $160 </strong>avg / night</div> 
                                <div class="cntdes1 star">
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span>
	                                  <span class="fa fa-star"></span> 
	                            </div>
                        </div>
                        <div class="destbtm2">
                               <a class="bk_btn">Book</a>
                        </div>
	                  </span>


	                  </div>
	                  
	                </div>
	              </div>
	           </div> -->
	           <div class="clearfix"></div>
				<div class="inside_contact hide">
					<div class="clearfix"></div>
					<?php if(isset($hotelEmail) && !empty($hotelEmail)) {
						?>
					<div class="row_contact">
						<span class="fa fa-envelope"></span>
						<strong><?php echo $hotelEmail; ?></strong>
					</div>
					<?php 
						}
						if(isset($terminal) && !empty($terminal)) {
						?>
					<div class="row_terminal">
						<span class="fa fa-plane"></span>
						Nearest Terminal is <?php echo $terminal;?> KM.
					</div>
					<?php
						}?>
					<div class="clearfix"></div>
					<div class="map_contact">
						<div class="row_contact">
							<span class="fa fa-map-marker"></span>
							<strong><?php echo $hotel_address . $postal;?></strong>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="down_hotel">
	<div class="container">
		<div class="col-md-8 col-xs-12 nopad">
			<div class="shdoww">
				<div class="hotel_detailtab">
					<ul class="nav nav-tabs htl_bigtab">
						<li class="" id='facilitypsh'>
							<a href="#htldets" data-toggle="tab"><span class="fa fa-info-circle"></span>Hotel Details</a>
						</li>
						<li class="active" id='facilitypsr'>
							<a href="#rooms" data-toggle="tab" id="room-list"><span class="fa fa-building-o"></span>Rooms</a>
						</li>
						<li id='facilitypsw'>
							<a href="#facility" data-toggle="tab"><span class="fa fa-check-square-o"></span>Amenities</a>
						</li>

						<li id='facilitypost'>
							<a href="#post" data-toggle="tab"><span class="fa fa-comment"></span>Post a Review</a>
						</li>
<!--						<li>-->
<!--							<a href="#policy" data-toggle="tab"><span class="fa fa-list-alt"></span>Hotel Policy</a>-->
<!--						</li>-->
					</ul>
					<div class="clearfix"></div>
					<div class="tab-content clearfix">
						<!-- Hotel Detail-->
						<div class="tab-pane" id="htldets">
							<div class="innertabs">
                            <div class="dets_hotels">
                            <strong><?=$hotel_name?></strong>
								<div class="comenhtlsum"> <?php echo $hotel_description;?></div>
							</div>
                            </div>
						</div>
						<!-- Hotel Detail End--> 
						<div class="clearfix"></div>
						<!-- Rooms Summry-->
						<div class="tab-pane active" id="rooms">
							<div class="innertabs">
								<?php
								$rm_cnt = 0;
								$rate_key_array = array();
								foreach($print_format_room_list as $rl_k => $rl_v) {//debug($rl_v['rate_key']);exit;
									$room_price = $rl_v['net'];
									$total_adult = $rl_v['adults'];
									$total_child = $rl_v['children'];
									$total_room = $rl_v['room'];
									$rate_key_array[]= $rl_v['rate_key'];
								?>
								<div class="htl_rumrow">
									<div class="hotel_list">
										<div class="col-md-9 col-sm-9 col-xs-8 fullfives">
											<div class="col-md-12 col-xs-12 nopad">
												<div class="col-md-5 col-xs-5 nopad">
													<div class="in_center_htl">
														<div class="hotel_hed"><?=@$rl_v['name']?></div>
														<div class="hotel_sub_hed"><?=@$rl_v['boardName']?></div>
														<div class="clearfix"></div>
														<div class="morerumdesc">
															<a class="morerombtn" data-toggle="modal" data-target="#cancellation_policy<?=$rm_cnt?>"><span class="fa fa-ban"></span>Cancellation Policy</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-xs-4 nopad">
													<div class="pers">
														<?php
														echo str_repeat('<i class="fa fa-male"></i>', intval(@$rl_v['adults']));
														echo str_repeat('<i class="fa fa-child"></i>', intval(@$rl_v['children']));
														?>
													</div>
												</div>

													<div class="col-md-3 col-xs-3 nopad">
													<div class="sideprice"><span class="avgper">Total</span>  <?=$currency?> <?=number_format($room_price,2)?> </div>
													</div>
											</div>
										</div>

										<div class="col-md-3 col-sm-3 col-xs-4 fullfives">
											<div class="col-md-12 nopad bordrit">
												<div class="pricesec">
													
													<div class="bookbtn">
														<form action="<?=base_url()?>index.php/hotel/booking/<?=$search_id?>" id="room_details_form" method="post">
															<div class="hide">
																<input type="hidden" name="token" value="<?=$Token?>" />
																<input type="hidden" name="token_key" value="<?=$TokenKey?>" />
																<input type="hidden" name="booking_source" value="<?=$active_booking_source?>" />
																<?php //if(valid_array($rate_key_array)) {
																	//foreach($rate_key_array as $rat_key => $key_rate) {
																		?><input type="hidden" name="rateKey[]" value="<?=$rl_v['rate_key']?>"><?php
																	//}
																//}?>
																<input type="hidden" name="rooms[]" value="<?=$total_room?>">
																<input type="hidden" name="adults[]" value="<?=$total_adult?>">
																<input type="hidden" name="childs[]" value="<?=$total_child?>">
															</div>
															<input type="submit" class="booknow" value="Book">
														</form>
													</div>
												</div>
											</div>
										</div>
										<!--Cancellation details Modal Starts-->
										<div id="cancellation_policy<?=$rm_cnt?>" class="modal fade" role="dialog">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Cancellation Policy</h4>
													</div>
													<div class="modal-body">
														<ul class="list_popup">
															<?php
															
															if (isset($rl_v['cancellationPolicies']) == true and valid_array($rl_v['cancellationPolicies']) == true) {
																foreach($rl_v['cancellationPolicies'] as $c_k => $c_v) {
																	?>
																	<li class="listcancel">
																		Cancellation Made After <?=app_friendly_absolute_date(date('Y-m-d H:i:s', strtotime($c_v['from'])))?> Would Charge 
																		<?=$c_v['amount']?> <?=$currency?>
																	</li>
																<?php }
															} else {
																echo '<li>No Cancellation Policy Found</li>';
															}
															?>
														</ul>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
											</div>
										</div>
										<!--Cancellation details Modal Ends-->
										</div>
									</div>
									<?php
									$rm_cnt++; 
									} ?>
							</div>
						</div>
						<!-- Rooms End--> 
						<!-- Facilities-->
						<div class="tab-pane" id="facility">
							<div class="innertabs">
                            <div class="dets_hotels">
								<div class="col-xs-12 nopad">
									<ul class="checklist">
										<?php 
											if(valid_array($hotel_facilities) == true) {
												foreach($hotel_facilities as $hfKey => $facilities) {
													$additional_cost = '';
													if (isset($facilities['additional_cost']) && $facilities['additional_cost'] == true) {
														$additional_cost = '<sup><strong class="text-danger">*</strong></sup>';
													}
													?>
													<li> <span class="fa fa-check-square-o"></span><?php echo $facilities['name'].$additional_cost;?></li>
										<?php
												}
											}
											?>
									</ul>
								</div>
								<p class="mar_t20">Facilities marked with <strong class="text-danger">*</strong> are available at additional cost</p>
							</div>
                            </div>
						</div>

						<div class="tab-pane" id="post">
							<div class="innertabs">
	                            <div class="dets_hotels">
							 
								</div>
                            </div>
						</div>
						<!-- Facilities End--> 
						<!-- Reviews-->
<!--						<div class="tab-pane" id="policy">-->
<!--							<div class="innertabs">-->
<!--								<ul class="list_detail_tab">-->
<!--									<li class="listcancel">-->
<!--										Lorem Ipsum is simply dummy text of the printing and typesetting industry.-->
<!--									</li>-->
<!--									<li class="listcancel">-->
<!--										Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.-->
<!--									</li>-->
<!--									<li class="listcancel">-->
<!--										It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.-->
<!--									</li>-->
<!--								</ul>-->
<!--							</div>-->
<!--						</div>-->
						<!-- Reviews End--> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Content -->
<!-- Hotel Location Map Starts -->
<div class="modal fade bs-example-modal-lg" id="map-box-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Hotel Location Map</h4>
			</div>
			<div class="modal-body">
				<iframe src="" id="map-box-frame" name="map_box_frame" style="height: 500px;width: 850px;">
				</iframe>
			</div>
		</div>
	</div>
</div>
<!-- Hotel Location Map Ends -->
<?php echo $this->load->view('core/bottom_footer');?> 
<script type="text/javascript">

$(document).ready(function(){
	$("#owl_demo_1").owlCarousel({
    items : 1, 
    itemsDesktop : [1000,1],
    itemsDesktopSmall : [900,1], 
    itemsTablet: [768,1], 
    itemsMobile : [479,1], 
    navigation : true,
    pagination : false
});
  	var sync1 = $("#sync1");
	var sync2 = $("#sync2");
	sync1.owlCarousel({
		singleItem : true,
		slideSpeed : 1000,
		navigation: true,
		pagination:false,
		afterAction : syncPosition,
		responsiveRefreshRate : 200,
	});

	sync2.owlCarousel({
		items : 4,
		itemsDesktop	: [1199,4],
		itemsDesktopSmall	 : [979,4],
		itemsTablet	 : [768,4],
		itemsMobile	 : [479,2],
		pagination:false,
		responsiveRefreshRate : 100,
		afterInit : function(el){
		el.find(".owl-item").eq(0).addClass("synced");
		}
	});
	$(".tooltipv").tooltip();
	
	$(".provabslideshow").provabslideshow({
            mobile: true
        });
	
	
	$(document).on('click', '.location-map', function() {
		$('#map-box-modal').modal();
	});
	$('.room_select').click(function(){
		$('#room-list').trigger('click');
		var topmove = $('.room_select').offset().top;
		$('html, body').animate({scrollTop: topmove}, 500);
	});

	function syncPosition(el){
		var current = this.currentItem;
		$("#sync2")
		.find(".owl-item")
		.removeClass("synced")
		.eq(current)
		.addClass("synced")
		if($("#sync2").data("owlCarousel") !== undefined){
		center(current)
		}

	}

	$("#sync2").on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).data("owlItem");
		sync1.trigger("owl.goTo",number);
	});

	function center(number){
		var sync2visible = sync2.data("owlCarousel").owl.visibleItems;

		var num = number;
		var found = false;
		for(var i in sync2visible){
		if(num === sync2visible[i]){
			var found = true;
		}
		}

		if(found===false){
		if(num>sync2visible[sync2visible.length-1]){
			sync2.trigger("owl.goTo", num - sync2visible.length+2)
		}else{
			if(num - 1 === -1){
			num = 0;
			}
			sync2.trigger("owl.goTo", num);
		}
		} else if(num === sync2visible[sync2visible.length-1]){
		sync2.trigger("owl.goTo", sync2visible[1])
		} else if(num === sync2visible[0]){
		sync2.trigger("owl.goTo", num-1)
		}
	}
});
function facilitypsw() {
    $('html,body').animate({
        scrollTop: $(".shdoww").offset().top},
        'slow');
	$('#facilitypsw').addClass('active');
	$('#facilitypsr').removeClass('active');
	$('#facilitypsh').removeClass('active');
	$('#facilitypost').removeClass('active');
}
</script>