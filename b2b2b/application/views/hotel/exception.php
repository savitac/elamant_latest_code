<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title></title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo ASSETS;?>assets/css/log_in.css" rel="stylesheet" type="text/css">
		<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
		<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" />
		<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" />
		<link href="<?php echo ASSETS;?>assets/css/theme_style.css" rel="stylesheet" /-->
		<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" />
		<link href="<?php echo ASSETS;?>assets/css/ion.rangeSlider.css" rel="stylesheet" />
		<link href="<?php echo ASSETS;?>assets/css/ion.rangeSlider.skinModern.css" rel="stylesheet" />
		<script src="<?php echo ASSETS;?>assets/js/jquery-1.11.0.js"></script>
		<script src="<?php echo ASSETS;?>assets/js/jquery_ui.js"></script>
		<script src="<?php echo ASSETS;?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo ASSETS;?>assets/js/ion.rangeSlider.js"></script>
		<script src="<?php echo ASSETS;?>assets/js/pax_count.js"></script>
		<script src="<?php echo ASSETS;?>assets/js/jquery.jsort.0.4.min.js"></script>
		<script src="<?php echo ASSETS;?>assets/js/jquery.lazy.min.js"></script>
		
        <?php //echo $this->load->view("core/load_css"); ?>
        <link href="<?php echo ASSETS; ?>assets/css/flight_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script>
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/map.js"></script>
    </head>
    <body>
<?php echo $this->load->view('core/header'); ?>
<div class="jumbotron text-center">
<h1>OOOOPS!!!!!!!</h1>
<p class="text-danger">Hotel Booking Engine Could Not Process Your Request
!!!</p>
<p class="text-danger">Please Try Again To Process Your New Request</p>
<?php
if (isset($eid) == true and strlen($eid) > 0) {
	?>
	<p class="text-primary">You Can Use <strong><?=$eid?></strong> Reference Number To Talk To Our Customer Support</p>
	<?php
}
?>
<!-- <p><a class="btn btn-primary btn-lg" href="<?php echo base_url()?>index.php/general/index/hotel/?default_view=<?php echo META_ACCOMODATION_COURSE; ?>"
	role="button">Click Here To Start New Search</a></p> -->
	<p><a class="btn btn-primary btn-lg" href="<?php echo base_url()?>"
	role="button">Click Here To Start New Search</a></p>
</div>
<?php
if (isset($log_ip_info) and $log_ip_info == true and isset($eid) == true) {
?>
<script>
$(document).ready(function() {
	$.getJSON("http://ip-api.com/json", function(json) {
		$.post(app_base_url+"index.php/ajax/log_event_ip_info/<?=$eid?>", json);
	});
});
</script>
<?php
}
?>
 <?php echo $this->load->view('core/bottom_footer'); ?>