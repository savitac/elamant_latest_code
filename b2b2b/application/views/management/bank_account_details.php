<?php
if( isset($_GET['eid']) == TRUE OR validation_errors() != FALSE || (isset($_GET['op']) == true && $_GET['op'] == 'add')) {			
	$tab1="active";
	$tab2="";			
} else {
	$tab2="active";
	$tab1="";
}

if(isset($form_data) && valid_array($form_data)){
	
	$acc_name = $form_data['en_account_name'];
	$acc_number =$form_data['account_number'];
	$bank_name =$form_data['en_bank_name'];
	$branch_name =$form_data['en_branch_name'];
	$ifsc =$form_data['ifsc_code'];
	$pan =$form_data['pan_number'];
	$origin=$form_data['origin'];
	$bank_icon =$form_data['bank_icon'];

	if($form_data['status'] == 1){
		$active = "checked";
		$inactive ="";
	}else{
		$active = "";
		$inactive ="checked";
	}
	
}else{
	$acc_name = "";
	$acc_number ="";
	$bank_name ="";
	$branch_name ="";
	$ifsc ="";
	$pan ="";
	$bank_icon ="";
	$active = "";
	$inactive ="checked";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title><?= $this->session->userdata('company_name')?></title>
<?php echo $this->load->view('core/load_css'); ?>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="
https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<link href="<?php echo ASSETS;?>assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Navigation -->

<?php echo $this->load->view('dashboard/top'); ?> 
<!-- /Navigation -->


<section id="main-content">
  
  <section class="wrapper">
<div class="clearfix"></div>
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<!-- HTML BEGIN -->
<div id="general_user" class="bodyContent">
<div id="bank_details"
	class="bodyContent col-md-12">
<div class="panel panel-default"><!-- PANEL WRAP START -->
<div class="panel-heading"><!-- PANEL HEAD START -->
<div class="panel-title">
<ul class="nav nav-tabs" role="tablist" id="myTab">
	<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE START-->
	<li role="presentation" class="<?php echo $tab1;?>"><a href="#fromList"
		aria-controls="home" role="tab" data-toggle="tab">Add/Update Bank Account Details
	 <span class="fa fa-pencil"></span></a></li>
	<li role="presentation" class="<?php echo $tab2;?>"><a href="#tabList"
		aria-controls="home" role="tab" data-toggle="tab"> Bank Account Details 
	<span class="fa fa-book"></span></a></li>
	<!-- INCLUDE TAB FOR ALL THE DETAILS ON THE PAGE END -->
</ul>
</div>
</div>
<!-- PANEL HEAD START -->
<div class="panel-body"><!-- PANEL BODY START -->
<div class="tab-content">
<div role="tabpanel" class="clearfix tab-pane <?php echo $tab1;?>" id="fromList">
<div class="">
<?php
/** Generating Form**/
/*if( isset($_GET['eid']) == false || empty($_GET['eid']) == true ) {
     
     echo $this->current_page->generate_form('bank_account_details',$form_data);
     } else {
     
     echo $this->current_page->generate_form('bank_account_details_edit',$form_data);
     }*/
?>
<div class="">
   <form name="bank_account_details" autocomplete="off" action="" method="POST" enctype="multipart/form-data" id="bank_account_details" role="form" class="form-horizontal">
      <input type="hidden" name="FID" value="pxfksgzLhJpW4pSIfcFnSZOQhEhWWmtaEBRH4LGqTP51qkxvkoCYO5sIJqm6rHD/e4Q57LjMUvIFaIuLxoyTWw==">
      <fieldset form="bank_account_details">
         <legend class="form_legend">Account Details</legend>
         <input name="origin" type="hidden" id="origin" class=" origin hiddenIp" required="" value="<?=$origin?>">
         <div class="form-group">
            <label class="col-sm-3 control-label" for="en_account_name" form="bank_account_details">Account Name<span class="text-danger">*</span></label>
            <div class="col-sm-6"><input name="en_account_name" required="" type="text" placeholder="Account Name" value="<?=$acc_name;?>" class=" en_account_name form-control" id="en_account_name" data-container="body" data-toggle="popover" data-original-title="" data-placement="bottom" data-trigger="hover focus" data-content="Account Name"></div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label" for="account_number" form="bank_account_details">Account Number<span class="text-danger">*</span></label>
            <div class="col-sm-6"><input minlength="16" maxlength="16" name="account_number" value="<?=$acc_number;?>" required="" type="text" placeholder="Account Number" class=" numeric account_number form-control" id="account_number" data-container="body" data-toggle="popover" data-original-title="" data-placement="bottom" data-trigger="hover focus" data-content="Account Number"></div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label" for="ifsc_code" form="bank_account_details">IFSC Code<span class="text-danger">*</span></label>
            <div class="col-sm-6"><input minlength="11" maxlength="11" name="ifsc_code" value="<?=$ifsc;?>" required="" type="text" placeholder="IFSC Code" class=" ifsc_code form-control" id="ifsc_code" data-container="body" data-toggle="popover" data-original-title="" data-placement="bottom" data-trigger="hover focus" data-content="IFSC Code"></div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label" for="pan_number" form="bank_account_details">PAN<span class="text-danger">*</span></label>
            <div class="col-sm-6"><input name="pan_number" required="" type="text" value="<?=$pan;?>" placeholder="PAN" minlength="10" maxlength="10" class=" pan_number form-control" id="pan_number" data-container="body" data-toggle="popover" data-original-title="" data-placement="bottom" data-trigger="hover focus" data-content="PAN"></div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label" for="en_bank_name" form="bank_account_details">Bank Name<span class="text-danger">*</span></label>
            <div class="col-sm-6"><input name="en_bank_name" required="" type="text" value="<?=$bank_name;?>" placeholder="Bank Name" class=" en_bank_name form-control" id="en_bank_name" data-container="body" data-toggle="popover" data-original-title="" data-placement="bottom" data-trigger="hover focus" data-content="Bank Name"></div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label" for="en_branch_name" form="bank_account_details">Branch Name<span class="text-danger">*</span></label>
            <div class="col-sm-6"><input name="en_branch_name" required="" type="text" value="<?=$branch_name;?>" placeholder="Branch Name" class=" en_branch_name form-control" id="en_branch_name" data-container="body" data-toggle="popover" data-original-title="" data-placement="bottom" data-trigger="hover focus" data-content="Branch Name"></div>
         </div>
        <?php if(!isset($bank_icon)){ ?>
         <div class="form-group">
            <label class="col-sm-3 control-label" for="bank_icon" form="bank_account_details">Bank Logo</label>
            <div class="col-sm-6"><a class="file-input-wrapper btn  bank_icon bank_icon">Browse<input name="bank_icon" value="<?=$bank_icon;?>" accept="image/x-png,image/gif,image/jpeg" type="file" placeholder="Bank Logo" class=" bank_icon bank_icon" value="" id="bank_icon" data-container="body" data-toggle="popover" data-original-title="" data-placement="bottom" data-trigger="hover focus" data-content="Bank Logo"></a></div>
         </div>
       <?php } ?>
         <div class="form-group"><label class="col-sm-3 control-label" for="status" form="bank_account_details">Status<span class="text-danger">*
         </span></label>
         <label class="radio-inline" for="bank_account_detailsstatus0">  
         <input required="" <?=$inactive;?> dt="" class=" status radioIp" type="radio" name="status" id="bank_account_detailsstatus0" value="0">Inactive</label>
         <label class="radio-inline" for="bank_account_detailsstatus1">  <input required="" dt="" class=" status radioIp" type="radio" <?=$active;?> name="status" id="bank_account_detailsstatus1" value="1">Active</label></div>
      </fieldset>
      <div class="form-group">
         <div class="col-sm-8 col-sm-offset-4"> <button type="submit" id="bank_account_details_submit" class=" btn btn-success ">Submit</button> <button type="reset" id="bank_account_details_reset" class=" btn btn-warning ">Reset</button></div>
      </div>
   </form>
</div>
</div>
</div>
<!-- Table List -->
<div role="tabpanel" class="tab-pane clearfix <?php echo $tab2;?>" id="tabList">
<div class="col-md-12">
<?php
echo get_table($table_data);
?>
</div>
</div>

</div>
</div>
<!-- PANEL BODY END --></div>
<!-- PANEL WRAP END --></div>


  </section>

</section>

<!-- HTML END -->

<?php
function get_table($table_data='')
{
	$table = '
<div class="table-responsive col-md-12"><table class="table table-hover table-striped table-bordered table-condensed">';
	$table .= '<tr>
		<th><i class="fa fa-sort-numeric-asc"></i> Sno</th>
		<th>Bank Logo</th>
		<th>Account Name</th>
		<th>Account Number</th>
		<th>Bank Name</th>
		<th>Branch Name</th>
		<th>IFSC Code</th>
		<th>PAN</th>
		<th>Created on</th>
		<th>Status</th>
		<th>Action</th>
		</tr>';		

	if (valid_array($table_data) == true) {
		$segment_3 = $GLOBALS['CI']->uri->segment(3);
		$current_record = (empty($segment_3) ? 0 : $segment_3);
		foreach ($table_data as $k => $v) {			
			$table .= '<tr>
			<td>'.(++$current_record).'</td>
			<td><img height="75px" width="75px" src="'.ASSETS.$GLOBALS ['CI']->template->domain_images('bank_logo/'.$v['bank_icon']).'" alt="Bank Logo"></td>
			<td>'.$v['en_account_name'].'</td>
			<td>'.$v['account_number'].'</td>
			<td>'.$v['en_bank_name'].'</td>
			<td>'.$v['en_branch_name'].'</td>
			<td>'.$v['ifsc_code'].'</td>
			<td>'.$v['pan_number'].'</td>
			<td>'.app_friendly_date($v['created_datetime']).'</td>
			<td>'.get_status_label($v['status']).'</td>
			<td>'.get_edit_button($v['origin']).'</td>			
</tr>';
		}
	} else {
		$table .= '<tr><td colspan="9">'.get_app_message('AL005').'</td></tr>';
	}
	$table .= '</table></div>';
	return $table;
}

function get_edit_button($id)
{
	return ' <a role="button" href="'.base_url().'management/bank_account_details?eid='.$id.'" class="btn btn-primary fa fa-pencil">
		'.get_app_message('AL0041').'</a>
		';
}

function get_status_label($status)
{
	if (intval($status) == ACTIVE) {
		return '<a class="btn btn-success fa fa-hand-point-right">'.get_enum_list('status', ACTIVE).'</span>
	<a role="button" href="" class="hide">'.get_app_message('AL0021').'</a>';
	} else {
		return '<span class="btn btn-danger fa fa-hand-point-right">'.get_enum_list('status', INACTIVE).'</span>
		<a role="button" href="" class="hide">'.get_app_message('AL0020').'</a>';
	}
}
?>
<?php echo  $this->load->view('core/bottom_footer'); ?> 
</body>
<style>.err{color: red;}</style>
<script type="text/javascript">
	  $('#bank_account_details_submit').on('click', function() {

	  	if($("#en_account_name").val()==""){
	  		$(".err").remove();
	  		$("#en_account_name").after("<span class='err'>Please Enter Account Name<span>");
	  		return false;
	  	}

	  	if($("#account_number").val()==""){
	  		$(".err").remove();
	  		$("#account_number").after("<span class='err'>Please Enter Account Number<span>");
	  		return false;
	  	}

	  	if($("#ifsc_code").val()==""){
	  		$(".err").remove();
	  		$("#ifsc_code").after("<span class='err'>Please Enter IFSC Code<span>");
	  		return false;
	  	}

	  	if($("#pan_number").val()==""){
	  		$(".err").remove();
	  		$("#pan_number").after("<span class='err'>Please Enter PAN Number<span>");
	  		return false;
	  	}

	  	if($("#en_bank_name").val()==""){
	  		$(".err").remove();
	  		$("#en_bank_name").after("<span class='err'>Please Enter Bank Name<span>");
	  		return false;
	  	}

	  	if($("#en_branch_name").val()==""){
	  		$(".err").remove();
	  		$("#en_branch_name").after("<span class='err'>Please Enter Branch Name<span>");
	  		return false;
	  	}

	  	if($("#bank_icon").val()==""){
	  		$(".err").remove();
	  		$("#bank_icon").after("<span class='err'>Upload Bank Logo<span>");
	  		return false;
	  	}


	  });
</script>
</html>