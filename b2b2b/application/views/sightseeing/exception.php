<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title><?php echo $this->session->userdata('company_name')?></title>
        <?php echo $this->load->view("core/load_css"); ?>
         <link href="<?php echo ASSETS; ?>assets/css/flight_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
        <link href="<?php echo ASSETS.SYSTEM_TEMPLATE_LIST; ?>/template_v3/css/front_end.css" rel="stylesheet">
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script>  
    <!-- <script src="<?php echo ASSETS; ?>assets/js/owl.carousel.min.js"></script>  -->
    <!-- <script src="<?php echo ASSETS; ?>assets/js/provablib.js"></script>
    <script src="<?php echo ASSETS; ?>assets/js/jquery.jsort.0.4.min.js"></script> -->
    <script src="<?php echo ASSETS; ?>assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo ASSETS; ?>assets/js/general.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS.SYSTEM_TEMPLATE_LIST; ?>/template_v3/javascript/javascript.js"></script>
    <script src="<?php echo ASSETS.SYSTEM_TEMPLATE_LIST; ?>/template_v1/javascript/page_resource/datepicker.js"></script>
    <script src="<?php echo ASSETS.SYSTEM_TEMPLATE_LIST; ?>/template_v3/javascript/page_resource/sightseeing_booking.js"></script> 
    <script src="<?php echo ASSETS.SYSTEM_TEMPLATE_LIST; ?>/template_v1/javascript/page_resource/booking_script.js"></script>
    <style type="text/css">
    .labladvncesearch 
    { 
    color: #000 !important;
    line-height: 30px;    
    overflow: hidden;
    }
    </style>
    </head> 
    <body>
      <?php 
        if($this->session->userdata('user_logged_in') == 0) {  
       echo $this->load->view('core/header'); 
    } else {
       echo $this->load->view('dashboard/top');    
    }

if (isset($exception) == true) {
	 ?>
<div class="err_mge">
  <div class="container">
     <div class="err_out">
       <div class="err">
        <h2>Error</h2>
        <h4>
        <?php 
            if($exception['op']=='booking_exception'){
               echo "Some Problem Occured. Please Try Again";
            }else{
              echo $exception['op'];
            }
        ?>
        </h4>             
       </div>
       <div class="ref_num">
        	<p>Ref: <?=$exception['exception_id']?></p>
       </div>
       <div class="confirm_btn">
       		<a href="<?php echo base_url()?>index.php/general/index/activities/?default_view=<?php echo META_SIGHTSEEING_COURSE?>" class="btn" role="button">OK</a>
       </div>
     </div>
  </div>
</div>

<?php } 

if (isset($log_ip_info) and $log_ip_info == true and isset($exception) == true) {
	// echo 'herre I am';
	// debug($exception);exit;
?>
<script>
$(document).ready(function() {
	$.getJSON("http://ip-api.com/json", function(json) {
		$.post(app_base_url+"index.php/ajax/log_event_ip_info/<?=$exception1->xception_id?>", json);
	});
});
</script>
<?php
}
?>

