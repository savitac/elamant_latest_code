<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edgse">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo $this->session->userdata('company_name');?></title>
        <?php echo $this->load->view("core/load_css"); ?> 
	   <link href="<?php echo ASSETS; ?>assets/css/flight_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script>
		<script src="<?php echo ASSETS; ?>assets/js/jquery-ui.min.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/general.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/css/owl.carousel.min.css"></script>
		<script src="<?php echo ASSETS; ?>assets/js/owl.carousel.min.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/provablib.js"></script>
		<script src="<?php echo ASSETS; ?>assets/js/jquery.jsort.0.4.min.js"></script>
        <script src="<?php echo ASSETS; ?>assets/js/bus_search_result.js"></script>
		<script src="<?php echo ASSETS; ?>assets/js/bus_search.js"></script>   
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/pax_count.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/map.js"></script>
    </head> 
    <body>
     <?php 
        if($this->session->userdata('user_logged_in') == '') { 
			echo $this->load->view('core/header'); 
		} else {
			echo $this->load->view('dashboard/top'); 	 
		}
	 ?>
	
<?php
	foreach ($active_booking_source as $t_k => $t_v) {
		$active_source[] = $t_v['source_id'];
	}
		$active_source = json_encode($active_source);
		$data['result'] = $bus_search_params;

?>

<script>
var app_base_url = "<?php echo base_url(); ?>";	
var load_busses = function(){
	$.ajax({
		type: 'GET',
		url: app_base_url+'index.php/ajax/bus_list?booking_source=<?=$active_booking_source[0]['source_id']?>&search_id=<?=$bus_search_params['search_id']?>&op=load',
		async: true,
		cache: true,
		dataType: 'json',
		success: function(res) {
			var dui;
			var r = res;
			dui = setInterval(function(){
			if (typeof(process_result_update) != "undefined" && $.isFunction(process_result_update) == true) {
				clearInterval(dui);
				process_result_update(r);
			}
			}, 1);
		}
	});
}
//Load buss from active source
load_busses();
</script>
<span class="hide">
	<input type="hidden" id="pri_active_source" value='<?=($active_source)?>' >
	<input type="hidden" id="pri_search_id" value="<?=$bus_search_params['search_id']?>" >
	<input type="hidden" id="pri_slider_currency" value="Rs " >
</span>
<?php
$mini_loading_image = '<div class="text-center loader-image"><img src="'.ASSETS.'assets/images/loader_v3.gif" alt="Loading........"/></div>';
$loading_image = '<div class="text-center loader-image"><img src="'.ASSETS.'assets/images/loader_v1.gif" alt="Loading........"/></div>';
$flight_o_direction_icon = '<img src="'.ASSETS.'assets/images/icons/flight-search-result-up-icon.png" alt="Flight Search Result Up Icon">';

echo $GLOBALS['CI']->load->view('bus/search_panel_summary',true);
?>
<section class="search-result">
	<div class="container" id="page-parent">
	<?php echo $GLOBALS['CI']->load->view('share/loader/bus_result_pre_loader',$data,true);?>
		<div class="resultalls open">
		
			<div class="coleft">
			<div class="flteboxwrp">
				<div class="filtersho">
					<div class="avlhtls">
					<strong id="total_records"></strong>
					</div>
				</div>
				<div class="fltrboxin">
					<div class="celsrch">
						<div class="row_busses">
							<a class="pull-right" id="reset_filters">RESET ALL</a>
						</div>
						<div class="">
							<div class="rangebox">
								<button class="collapsebtn" type="button" data-toggle="collapse" data-target="#price-refine">Price</button>
								
								<div class="collapse in price-refine">
									<div class="price_slider1">
									<div id="core_min_max_slider_values" class="hide">
										<input type="hiden" id="core_minimum_range_value" value="">
										<input type="hiden" id="core_maximum_range_value" value="">
									</div>
										<p id="amount" class="level"></p>
										<div id="slider-range-1" class="" aria-disabled="false"></div>
										<?=$mini_loading_image;?>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="septor"></div>
							
							<div class="rangebox">
								<button class="collapsebtn" type="button" data-toggle="collapse" data-target="#bustype-refine">Bus Type</button>
								<div class="collapse in bustype-refine">
									<?=$mini_loading_image;?>
								</div>
							</div>
							
							<div class="clearfix"></div>
							<div class="septor"></div>
								<div class="rangebox">
									<button type="button" class="collapsebtn" data-toggle="collapse"
										data-target="#collapse503">Departure Time</button>
									<div class="collapse in" id="collapse503">
										<div id="departureTimeWrapper" class="boxins marret">
											<a
												class="timone toglefil time-wrapper">
												<input type="checkbox"
													value="1" class="time-category hidecheck">
												<div class="starin">
													<div class="flitsprt mng1"></div>
													<span class="htlcount">5-9AM</span>
												</div>
											</a>
											<a class="timone toglefil time-wrapper">
												<input
													type="checkbox" value="2" class="time-category hidecheck">
												<div class="starin">
													<div class="flitsprt mng2"></div>
													<span class="htlcount">9-5PM</span>
												</div>
											</a>
											<a class="timone toglefil time-wrapper">
												<input
													type="checkbox" value="3" class="time-category hidecheck">
												<div class="starin">
													<div class="flitsprt mng3"></div>
													<span class="htlcount">5-9PM</span>
												</div>
											</a>
											<a class="timone toglefil time-wrapper">
												<input
													type="checkbox" value="4" class="hidecheck time-category">
												<div class="starin">
													<div class="flitsprt mng4"></div>
													<span class="htlcount">9PM-5AM</span>
												</div>
											</a>
										</div>
									</div>
								</div>
								
								<div class="rangebox">
									<button type="button" class="collapsebtn" data-toggle="collapse" data-target="#collapse504">Arrival Time</button>
									<div class="collapse in" id="collapse504">
										<div id="arrivalTimeWrapper" class="boxins marret">
											<a class="timone toglefil time-wrapper">
												<input type="checkbox" value="1" class="time-category hidecheck">
												<div class="starin">
													<div class="flitsprt mng1"></div>
													<span class="htlcount">5-9AM</span>
												</div>
											</a>
											<a class="timone toglefil time-wrapper">
												<input type="checkbox" value="2" class="time-category hidecheck">
												<div class="starin">
													<div class="flitsprt mng2"></div>
													<span class="htlcount">9-5PM</span>
												</div>
											</a>
											<a class="timone toglefil time-wrapper">
												<input type="checkbox" value="3" class="time-category hidecheck">
												<div class="starin">
													<div class="flitsprt mng3"></div>
													<span class="htlcount">5-9PM</span>
												</div>
											</a>
											<a class="timone toglefil time-wrapper">
												<input type="checkbox" value="4" class="time-category hidecheck">
												<div class="starin">
													<div class="flitsprt mng4"></div>
													<span class="htlcount">9PM-5AM</span>
												</div>
											</a>
										</div>
									</div>
								</div>

							<div class="clearfix"></div>
							<div class="septor"></div>
							
							<div class="rangebox">
								<button class="collapsebtn" type="button" data-toggle="collapse" data-target="#travel-refine">Operators</button>
								<div class="collapse in travel-refine">
									<?=$mini_loading_image;?>
								</div>
							</div>
							
							<div class="col-lg-12" style="display:none">
								<button type="button" class="btn btn-t btn-block b-r-0 reset-page-loader">Reset Filters</button>
							</div>
						</div>
					</div>
				</div>
				
			 </div>	
			 </div>
			<div class="colrit">
				<div class="insidebosc">
				<div class="filter_tab fa fa-filter"></div>
				<div class="filterforall" id="top-sort-list-wrapper">
						<div class="topmisty bus_filter" id="top-sort-list-1">
							<div class="col-xs-12 nopad divinsidefltr">
								<div class="insidemyt">
									<ul class="sortul bus_sorting">
										<li class="sortli oprtrli"><a class="sorta travel-l-2-h loader asc"><i class="fa fa-bus"></i> <strong>Operators</strong></a><a
											class="sorta travel-h-2-l hide loader des"><i class="fa fa-bus"></i> <strong>Operators</strong></a></li>
										<li class="sortli deprtli"><a class="sorta departure-l-2-h loader asc"><i class="fa fa-calendar"></i> <strong>Depart</strong></a><a
											class="sorta departure-h-2-l hide loader des"><i class="fa fa-calendar"></i> <strong>Depart</strong></a></li>
										<li class="sortli durli"><a class="sorta seat-l-2-h loader asc"><i class="fa fa-clock-o"></i> <strong>Duration</strong></a><a
											class="sorta seat-h-2-l hide loader des"><i class="fa fa-clock-o"></i> <strong>Duration</strong></a></li>
										<li class="sortli arrivli"><a class="sorta arrival-l-2-h loader asc"><i class="fa fa-calendar"></i> <strong>Arrive</strong></a><a
											class="sorta arrival-h-2-l hide loader des"><i class="fa fa-calendar"></i>	<strong>Arrive</strong></a></li>
										<li class="sortli priceli"><a class="sorta price-l-2-h loader asc"><i class="fa fa-tag"></i> <strong>Price</strong></a><a
											class="sorta price-h-2-l hide loader des"><i class="fa fa-tag"></i> <strong>Price</strong></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<?php echo $loading_image;?>
					<div id="bus_search" class="">
					</div>
					<div class="" id="empty_bus_search_result" style="display:none">
						<div class="noresultfnd">
							<div class="imagenofnd"><img src="<?php echo ASSETS.'assets/images/empty.jpg'?>" alt="Empty" /></div>
							<div class="lablfnd">No Result Found!!!</div>
						</div>
					</div>
					<hr class="hr-10">
					<div class="clearfix">
						<div class="pull-right">
							<span class="h5">Showing: <?php echo $mini_loading_image?><span class="visible-row-record-count">...</span> of <span class="total-row-record-count">...</span> Busses</span>
						</div>
					</div>
					<hr class="hr-10 invisible">
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade bs-example-modal-lg" id="bus-info-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Bus Details</h4>
			</div>
			<div class="modal-body">
				<?=$mini_loading_image?>
				<div id="bus-info-modal-content">
				</div>
			</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
		</div>
	</div>
	<div class="modal fade bs-example-modal-lg" id="bus-boarding-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Bus Details</h4>
			</div>
			<div class="modal-body">
			<?=$mini_loading_image?>
				<div id="bus-boarding-modal-content-data"></div>
			</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
		</div>
	</div>
	<div id="empty-search-result" class="jumbotron container" style="display:none">
		<h1><i class="fa fa-bus"></i> Oops!</h1>
		<p>No tickets were found for this route today.</p>
		<p>
		Search results change daily based on availability.If you have an urgent requirement, please get in touch with our call center using the contact details mentioned on the home page. They will assist you to the best of their ability.
		</p>
	</div>
</section>

<?php echo  $this->load->view('core/bottom_footer'); ?> 