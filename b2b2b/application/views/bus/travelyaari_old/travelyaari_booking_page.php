<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo  $this->session->userdata('company_name');?></title>
        <?php echo $this->load->view("core/load_css"); ?>
        <link href="<?php echo ASSETS; ?>assets/css/index.css" rel="stylesheet">
		<link href="<?php echo ASSETS; ?>assets/css/pre_booking.css" rel="stylesheet">
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery-1.11.0.js">	
		</script>
    </head>
    <body>
     <?php 
     if($this->session->userdata('user_logged_in') == false) { 
			echo $this->load->view('core/header'); 
		} else {
			echo $this->load->view('dashboard/top'); 	 
		}
	 ?>
<?php
$template_images = ASSETS .'assets/images/';
$CUR_Route = ($details ['Route']);
$bus_seats = $details ['Layout'] ['SeatDetails'] ['clsSeat'];
$bus_pickup = $details ['Pickup'] ['clsPickup'];
$CUR_CancellationCharges = $details ['CancellationCharges'] ['clsCancellationCharge'];
$seat_attr ['markup_price_summary'] = $markup_total_fare;
$seat_attr ['total_price_summary'] = $total_fare;
$seat_attr ['domain_deduction_fare'] = $domain_total_fare;
$seat_attr ['default_currency'] = $default_currency;
$seat_count = count ( $pre_booking_params ['seat'] );
$pax_title_options = generate_options ( $pax_title_enum, array (
		MR_TITLE 
), true );
$gender_options = generate_options ( $gender_enum, array (
		1 
) );

$mandatory_filed_marker = '<sup class="text-danger">*</sup>';
if (is_logged_in_user ()) {
	$review_active_class = ' success ';
	$review_tab_details_class = '';
	$review_tab_class = ' inactive_review_tab_marker ';
	$travellers_active_class = ' active ';
	$travellers_tab_details_class = ' gohel ';
	$travellers_tab_class = ' travellers_tab_marker ';
} else {
	$review_active_class = ' active ';
	$review_tab_details_class = ' gohel ';
	$review_tab_class = ' review_tab_marker ';
	$travellers_active_class = '';
	$travellers_tab_details_class = '';
	$travellers_tab_class = ' inactive_travellers_tab_marker ';
}

?>

<style>
.topssec::after {
	display: none;
}
</style> 
<div class="fldealsec">
	<div class="container">
		<div class="tabcontnue">
			<div class="col-xs-4 nopadding">
				<div class="rondsts <?=$review_active_class?>">
					<a class="taba core_review_tab <?=$review_tab_class?>" id="stepbk1">
						<div class="iconstatus fa fa-eye"></div>
						<div class="stausline">Review</div>
					</a>
				</div>
			</div>
			<div class="col-xs-4 nopadding">
				<div class="rondsts <?=$travellers_active_class?>">
					<a class="taba core_travellers_tab <?=$travellers_tab_class?>"
						id="stepbk2">
						<div class="iconstatus fa fa-group"></div>
						<div class="stausline">Travellers</div>
					</a>
				</div>
			</div>
			<div class="col-xs-4 nopadding">
				<div class="rondsts">
					<a class="taba" id="stepbk3">
						<div class="iconstatus fa fa-money"></div>
						<div class="stausline">Payments</div>
					</a>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="clearfix"></div>
<div class="alldownsectn">
<div class="container">
		<div class="ovrgo">
			<div class="bktab1 xlbox <?=$review_tab_details_class?>">
				<!-- Fare Summary  Starts-->
					<div class="col-xs-4 nopadding rit_summery">
						<div class="insiefare">
							<div class="farehd arimobold">Fare Summary</div>
							<div class="fredivs">
								<div class="kindrest">
									<div class="reptallt">
										<div class="col-xs-8 nopadding">
											<div class="faresty freshd">Total Seat(s)</div>
										</div>
										<div class="col-xs-4 nopadding">
											<div class="amnter freshd"><?=$seat_count?></div>
										</div>
									</div>
								</div>
							   <div class="clearfix"></div>
								<div class="reptalltftr">
									<div class="col-xs-8 nopadding">
										<div class="farestybig">Grand Total</div>
									</div>
									<div class="col-xs-4 nopadding">
										<div class="amnterbig arimobold"><?=$default_currency_symbol?> <?=roundoff_number($markup_total_fare)?> </div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Fare Summary  Ends-->
				<div class="col-xs-8 nopadding full_summery_tab">
					<div class="fligthsdets only_bus_book">
						<div class="flitab1">
<?php //debug($CUR_Route);exit;?>
							<div class="moreflt boksectn">
							  <div class="ontyp">
								<div class="labltowr arimobold"><?=ucfirst($CUR_Route['FromCityName'])?> to <?=ucfirst($CUR_Route['ToCityName'])?><strong>(<?=get_time_duration_label(calculate_duration($CUR_Route['DepartureTime'], $CUR_Route['ArrivalTime']))?>)</strong></div>
								<div class="allboxflt">
								  <div class="col-xs-3 nopadding full_fiftys">
									<div class="alldiscrpo"><?=$CUR_Route['CompanyName']?>
									<div class="sgsmalbus"><strong>Pickup :</strong>
									<div class="pikuplokndt">
										<?php
										$pickup = $bus_pickup[$pre_booking_params['pickup_id']]; 
										echo $pickup['PickupName'];
										$pickup_string = ($pickup['PickupName'].' - '.get_time($pickup['PickupTime']));
										$pickup_string .= ', Address : '.$pickup['Address'].', Landmark : '.$pickup['Landmark'].', Phone : '.$pickup['Phone'];
										?>
										<span class="pikuptm">
										<?php echo get_time($pickup['PickupTime']);	?>
										</span>
									</div>
									</div>
									
									</div>
								  </div>
								  <div class="col-xs-7 nopadding qurter_wdth">
									<div class="col-xs-5">
										<span class="airlblxl"><?=local_date($CUR_Route['DepartureTime'])." ".get_time($CUR_Route['DepTime'])?></span>
										<span class="portnme"><?=ucfirst($CUR_Route['FromCityName'])?></span>	
									</div>
									<div class="col-xs-2">
										<span class="fadr fa fa-long-arrow-right textcntr"></span>
									</div>
									<div class="col-xs-5">
										<span class="airlblxl"><?=local_date($CUR_Route['ArrivalTime'])." ".get_time($CUR_Route['ArrTime'])?></span>
										<span class="portnme"><?=ucfirst($CUR_Route['ToCityName'])?></span></div>
									</div>
									<div class="col-xs-2 nopadding smal_width_hr"> 
									<span class="portnme textcntr"><?=get_time_duration_label(calculate_duration($CUR_Route['DepartureTime'], $CUR_Route['ArrivalTime']))?></span> 
									<span data-stop-number="0" class="portnme textcntr">Seat(<?=$seat_count?>) : <?=implode(',', $pre_booking_params['seat'])?></span> 
								</div>
								</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="sepertr"></div>
							<!-- LOGIN SECTION STARTS -->
									<?php 
				if($this->session->userdata('user_logged_in') == true){ ?>
				<div class="col-xs-6 nopad fulat500" style="margin-bottom:20px;">
				<input type="button" id="next_review" class="paysubmit" value="Continue">
                <br/><br/>
		        </div>
		        <?php }else{?>
	            <div class="pre_summery test col-md-5" style="">
					<div class="signdiv" id="loginsec">
						<div class="labltowr" align="center" id="guest_txt">Login</div>
						<div class="insigndiv">
							<div class="ritsform">
									<div class="ritpul">
							
							
					<div id="login_form">				
					
                         <div class="col-md-12">
                             <div class="selectft">
                                
                                <input type="text" class="form-control log_ft" id="email" name="email" placeholder="User Name" required>
                                <span id="email-error" class="error" for="email">  </span>
                            </div>
                        </div>
  
                        <div class="col-md-12">
                             <div class="selectft">
                                
                                <input type="password" class="form-control log_ft" id="password" name="password" placeholder="Password" required>
                                <span id="password-error" class="error" for="password">  </span>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <a class="btn btn_logsub mycent" value="Login"  onclick="form_submits_bookings('guest_login')"> Login </a>
                        
                    
                    </div>
                    
                   <div id="guest_from" style="display:none;">				
					
                         <div class="col-md-6">
                             <div class="selectft">
                                
                                <input type="text" class="form-control log_ft" id="mobile" name="mobile" placeholder="Mobile Number" required>
                               <span id="mobile-error" class="error" for="mobile">  </span>
                            </div>
                        </div>
  
                        <div class="col-md-6">
                             <div class="selectft">
                                
                                <input type="text" class="form-control log_ft" id="g_email" name="g_email" placeholder="Email" required>
                                <span id="g_email-error" class="error" for="email"> </span>
                                <span id="account_already" class="error" for=""> </span>
                                
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <a class="btn btn_logsub mycent" onclick="form_submits_bookings('guest_continue')" >   Continue </a>
                    </div>
                  
                  
                    <div class="orcent" style="display:block;">OR</div>
                   <div id="sug_guest" style="display:block;">  
                   <a class="btn btn_logsub mycent" class="btn btn_logsub mycent" onclick="change_guest_form('1')" > Continue As Guest </a>
                   </div>
                   <div id="sug_login" style="display:none;">
                   <a class="btn btn_logsub mycent" class="btn btn_logsub mycent" onclick="change_guest_form('2')" > Login As User </a>
                   </div>
                    
 
									
								</div>
							</div>
					
						</div>
					</div>             
				</div>
				
				<div class="col-xs-8 nopadding full_log_tab">
	  <div class="fligthsdets">
		<div class="flitab1">
				<div class="clearfix"></div>
				<div class="sepertr"></div>
				<div class="sepertr"></div>				
				</div>
				</div>
		 </div>
                </div>    
	            <?php }?> 
							<!-- LOGIN SECTION ENDS -->
						</div>
					</div>
				</div>
			</div>
			<div class="bktab2 xlbox <?=$travellers_tab_details_class?>">
				<div class="clearfix"></div>
				<!-- Segment Details Starts-->
				<div class="collapse splbukdets" id="fligtdetails">
					<div class="moreflt insideagain">
			</div>
				</div>
				<!-- Segment Details Ends-->
				<div class="clearfix"></div>
				<div class="padpaspotr">
					<!-- Fare Summary  Starts-->
					<div class="col-xs-4 nopadding rit_summery">
						<div class="insiefare">
							<div class="farehd arimobold">Fare Summary</div>
							<div class="fredivs">
								<div class="kindrest">
									<div class="reptallt">
										<div class="col-xs-8 nopadding">
											<div class="faresty freshd">Total Seat(s)</div>
										</div>
										<div class="col-xs-4 nopadding">
											<div class="amnter freshd"><?=$seat_count?></div>
										</div>
									</div>
								</div>
							   <div class="clearfix"></div>
								<div class="reptalltftr">
									<div class="col-xs-8 nopadding">
										<div class="farestybig">Grand Total</div>
									</div>
									<div class="col-xs-4 nopadding">
										<div class="amnterbig arimobold"><?=$default_currency_symbol?> <?=roundoff_number($markup_total_fare)?> </div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Fare Summary  Ends-->
					<div class="col-xs-8 nopadding full_summery_tab">
						<div class="fligthsdets only_bus_book">
			<form action="<?=base_url().'index.php/bus/pre_booking/'.$search_data['search_id']?>" method="POST" autocomplete="off" id="pre-booking-form">
			 <div class="flitab1">
				<div class="moreflt boksectn">
					<div class="ontyp">
					  <div class="labltowr arimobold"><?=ucfirst($CUR_Route['FromCityName'])?> to <?=ucfirst($CUR_Route['ToCityName'])?><strong>(<?=get_time_duration_label(calculate_duration($CUR_Route['DepartureTime'], $CUR_Route['ArrivalTime']))?>)</strong></div>
								<div class="allboxflt">
								<div class="col-xs-3 nopadding full_fiftys">
									<div class="alldiscrpo"><?=$CUR_Route['CompanyName']?>
									<div class="sgsmalbus"><strong>Pickup :</strong>
									<div class="pikuplokndt">
										<?php $pickup = $bus_pickup[$pre_booking_params['pickup_id']]; 
										echo $pickup['PickupName']; ?>
										<span class="pikuptm">
										<?php echo get_time($pickup['PickupTime']);	?>
										</span>
									</div>
									</div>
									
									</div>
								</div>
								<div class="col-xs-7 nopadding qurter_wdth">
									<div class="col-xs-5">
										<span class="airlblxl"><?=local_date($CUR_Route['DepartureTime'])." ".get_time($CUR_Route['DepTime'])?></span>
										<span class="portnme"><?=ucfirst($CUR_Route['FromCityName'])?></span>	
									</div>
									<div class="col-xs-2">
										<span class="fadr fa fa-long-arrow-right textcntr"></span>
									</div>
									<div class="col-xs-5">
										<span class="airlblxl"><?=local_date($CUR_Route['ArrivalTime'])." ".get_time($CUR_Route['ArrTime'])?></span>
										<span class="portnme"><?=ucfirst($CUR_Route['ToCityName'])?></span></div>
								</div>
								<div class="col-xs-2 nopadding smal_width_hr"> 
									<span class="portnme textcntr"><?=get_time_duration_label(calculate_duration($CUR_Route['DepartureTime'], $CUR_Route['ArrivalTime']))?></span> 
									<span data-stop-number="0" class="portnme textcntr">Seat(<?=$seat_count?>) : <?=implode(',', $pre_booking_params['seat'])?></span> 
								</div>
								</div><br>
						<div class="labltowr arimobold">Please enter Name(s) of the Passenger(s). </div>
						<!-- template_v1 code -->
						<div class="pasngrinput_enter">
								<div class="col-xs-3 nopad">
									<span class="labl_pasnger">Seat Details</span>
								</div>
								<div class="col-xs-9 nopad">
								<div class="col-xs-3 nopad">
									<span class="labl_pasnger">
										Title <sup class="text-danger">*</sup>
									</span>
								</div>
								<div class="col-xs-5 nopad">
									<span class="labl_pasnger">
										Name <sup class="text-danger">*</sup>
									</span>
								</div>
								<div class="col-md-4 nopad">
									<span class="labl_pasnger">
										Age <sup class="text-danger">*</sup>
									</span>
								</div>
								</div>
							</div>
							<?php $i = 0;
							$datepicker_list = array();
							$lead_pax_details = @$pax_details[0];
							if(is_logged_in_user()) {
								$traveller_class = ' user_traveller_details ';
							} else {
								$traveller_class = '';
							}
							for($i=0; $i<$seat_count; $i++) {
								$cur_pax_info = is_array($pax_details) ? array_shift($pax_details) : array();
								$i_seat = $bus_seats[$pre_booking_params['seat'][$i]];
								$attr['Fare']		= $i_seat['Fare'];
								$attr['API_Raw_Fare']		= $i_seat['API_Raw_Fare'];
								$attr['Markup_Fare']= $i_seat['Markup_Fare'];
							?>
							<div class="pasngrinput_secnrews _passenger_hiiden_inputs">
							<div class="hide hidden_pax_details">
								<input type="hidden" name="gender[]" value="1" class="pax_gender">
							</div>
								<div class="col-xs-3 nopad">
								<div class="pad_psger">
									<?php
									$i_seat_title = array();
									if ($i_seat['IsAC'] != 'false') {
										$attr['IsAcSeat']	= false;
										$i_seat_title[] = 'NON_AC';
									} else {
										$attr['IsAcSeat']	= true;
										$i_seat_title[] = 'AC';
									}
									if (intval($i_seat['Deck']) == 1) {
										$i_seat_title[] = $i_seat['SeatNo'].' Lower Deck';
									} else {
										$i_seat_title[] = $i_seat['SeatNo'].' Upper Deck';
									}
									$i_seat_title[] = ($i_seat['Gender'] != 'F' ? '' : 'Reserved For Ladies');
									if ($i_seat['IsSleeper'] == 'true') {
										$i_seat_type = 'sleeper-A.png';
										$attr['SeatType']	= 'sleeper';
									} else {
										$attr['SeatType']	= 'seat';
										$i_seat_type = 'seat-A.png';
									}
									$i_seat_title[] = ' @ '.$domain_currency_obj->get_currency_symbol($pre_booking_params['default_currency']).' '.$i_seat['Markup_Fare'];
									//echo '<img src="'.$template_images.'seats/'.$i_seat_type.'" title="'.implode(' ', $i_seat_title).'" class="hand-cursor">';
									?> 
									<span class="seat_number"> Seat <strong><?=$i_seat['SeatNo']?></strong></span>
									</div>
								</div>
										<div class="col-xs-9 nopad flling_name">
										<div class="col-xs-3 nopad">
										<div class="pad_psger">
											<div class="selectedwrap">
												<select class="name_title flyinputsnor " required name="pax_title[]">
												<?=$pax_title_options?>
												</select>
											</div>
										</div>
										</div>
										<div class="col-xs-5 nopad">
										<div class="pad_psger">
												<input value="<?=@$cur_pax_info['first_name']?>" type="text" maxlength="45" id="contact-name"  name="contact_name[]" class="clainput  alpha_space <?=$traveller_class?>" required placeholder="Name" data-row-id="<?=($i);?>">
										</div>
										</div>
										<div class="col-xs-4 nopad">
										<div class="pad_psger">
											<div class="selectedwrap">
												<select class="age flyinputsnor" name="age[]" id="age-<?=($i);?>" required>
												<option value="INVALIDIP">Age</option>
												<?php echo generate_options(numeric_dropdown(array('size' => 99))); ?>
												</select>
											</div>
										</div>
										</div>
										</div>
							</div>
							<?php
							$seat_attr['seats'][$i_seat['SeatNo']] = $attr;
							}
							?>
							<div class="hide">
							<?php 
							/**
							 * Data for booking
							 */
							$dynamic_params_url['RouteScheduleId'] = $pre_booking_params['route_schedule_id'];
							$dynamic_params_url['JourneyDate'] = $pre_booking_params['journey_date'];
							$dynamic_params_url['PickUpID'] = $pre_booking_params['pickup_id'];
							$dynamic_params_url['seat_attr'] = $seat_attr;
							$dynamic_params_url['DepartureTime'] = $CUR_Route['DepartureTime'];
							$dynamic_params_url['ArrivalTime'] = $CUR_Route['ArrivalTime'];
							$dynamic_params_url['departure_from'] = $CUR_Route['FromCityName'];
							$dynamic_params_url['arrival_to'] = $CUR_Route['ToCityName'];
							$dynamic_params_url['boarding_from'] = $pickup_string;//
							$dynamic_params_url['dropping_to'] = '';//
							$dynamic_params_url['bus_type'] = $CUR_Route['BusLabel'];
							$dynamic_params_url['operator'] = $CUR_Route['CompanyName'];
							$dynamic_params_url['CommPCT'] = $CUR_Route['CommPCT'];
							$dynamic_params_url['CommAmount'] = $CUR_Route['CommAmount'];
							$dynamic_params_url = serialized_data($dynamic_params_url);
							?>
							<input type="hidden" required="required" name="token"		value="<?=$dynamic_params_url;?>" />
							<input type="hidden" required="required" name="token_key"	value="<?=md5($dynamic_params_url);?>" />
							<input type="hidden" required="ResultToken" name="ResultToken"	value="<?=$pre_booking_params['ResultToken'];?>" />
							<input type="hidden" required="required" name="op"			value="book_bus">
							<input type="hidden" required="required" name="booking_source"	value="<?=$pre_booking_params['booking_source']?>" >
							<!-- FIXME -->
			 			</div>
						<!-- template_v1 code -->
				   </div>
				</div>
				<div class="clearfix"></div>
				<div class="sepertr"></div>
				<div class="clearfix"></div>
				<div class="contbk">
					<div class="contcthdngs">CONTACT DETAILS</div>
					<div class="col-xs-6 nopad full_smal_forty">
					<div class="col-xs-2 nopadding">
					<!-- <input type="text" placeholder="+91" class="newslterinput nputbrd" readonly> -->
					<select class="newslterinput nputbrd _numeric_only " >
						<?php echo diaplay_phonecode($phone_code,$active_data); ?>
					</select> 
					</div>
					<div class="col-xs-2"><div class="sidepo">-</div></div>
					<div class="col-xs-8 nopadding">
					<input value="<?=@$lead_pax_details['phone'] == 0 ? '' : @$lead_pax_details['phone'];?>" type="text" name="passenger_contact" id="passenger-contact" placeholder="Mobile Number" class="newslterinput nputbrd _numeric_only numeric" maxlength="10" required="required">
					<input type="hidden" name="alternate_contact" value="">
					</div>
					<div class="clearfix"></div>
					<div class="emailperson col-xs-12 nopad full_smal_forty">
					<input value="<?=@$lead_pax_details['email']?>" type="text" maxlength="80" required="required" id="billing-email" class="newslterinput nputbrd" placeholder="Email" name="billing_email">
					</div>
					</div>
					<div class="clearfix"></div>
					<div class="notese">Your mobile number will be used only for sending Bus related communication.</div>
				</div>
					<div class="col-xs-12 padleftpay">
								<div class="payselect comon_backbg">
									<h3 class="inpagehed" style="text-align: left !important;">Select Payment Method</h3>
									<div class="sectionbuk">
										
										<div class="paylater_show" >
											<ul class="radiochecks">
												<li class="li-option-2" id="commissionable_li_1" >
													<label for="f-option-1" >
														<input type="radio" id="f-option-1" value="CCAVENUE" name="type_of_payment" checked > 
														<label for="f-option-1" class="check"></label>
														<label for="f-option-1"><?php echo "CCAVENUE"; ?></label>
													</label>
												</li> 
											
											<!-- 	<li class="li-option-2" id="commissionable_li_2" >
													<label for="s-option-1" >
														<input type="radio" id="s-option-1" value="PAYPAL" name="type_of_payment">
														<label for="s-option-1" class="check"></label>
														<label for="s-option-1"><?php# echo "Paypal"; ?></label>
													</label>
													
													
												</li> -->
											
												
											</ul>
											
										</div>
										   
										</div>
										
										<div class="col-md-12 nopad" id="paypal_pro" style="display:none;">
											<?php echo $this->load->view('booking/paypal_payment'); ?>	
									    </div>
											
											<div class="clearfix"></div>
						
										
									</div>
								</div>
				<div class="clikdiv">
					<div class="squaredThree">
					<input id="terms_cond1" type="checkbox" name="tc" checked="checked" required="required">
					<label for="terms_cond1"></label>
					</div>
					<span class="clikagre" id="clikagre">
						Terms and Conditions
					</span>
				</div>
				<div class="clearfix"></div>
				<div class="loginspld">
					<div class="collogg">
						<?php
						//If single payment option then hide selection and select by default
						if (count($active_payment_options) == 1) {
							$payment_option_visibility = 'hide';
							$default_payment_option = 'checked="checked"';
						} else {
							$payment_option_visibility = 'show';
							$default_payment_option = '';
						}
						
						?>
						<div class="row <?=$payment_option_visibility?>">
							<?php if (in_array(PAY_NOW, $active_payment_options)) {?>
								<div class="col-md-3">
									<div class="form-group">
										<label for="payment-mode-<?=PAY_NOW?>">
											<input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_NOW?>" id="payment-mode-<?=PAY_NOW?>" class="form-control b-r-0" placeholder="Payment Mode">
											Pay Now
										</label>
									</div>
								</div>
							<?php } ?>
							<?php if (in_array(PAY_AT_BANK, $active_payment_options)) {?>
								<div class="col-md-3">
									<div class="form-group">
										<label for="payment-mode-<?=PAY_AT_BANK?>">
											<input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_AT_BANK?>" id="payment-mode-<?=PAY_AT_BANK?>" class="form-control b-r-0" placeholder="Payment Mode">
											Pay At Bank
										</label>
									</div>
								</div>
							<?php } ?>
							</div>
						<div class="continye col-sm-3 col-xs-6 nopad">
							<button type="submit" id="flip" class="bookcont continue_booking_button" name="bus">Continue</button>
						</div>
						<div class="clearfix"></div>
						<div class="sepertr"></div>
						<div class="temsandcndtn">
						Most operators require travelers to have an ID valid for more than 3 to 6 months from the date of entry into or exit. Please check the exact rules for your destination before completing the booking.
						</div>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
			<?php if(is_logged_in_user() == true) { ?>
			<div class="col-xs-4 nopadding">
				<div class="insiefare">
					<div class="farehd arimobold">Passenger List</div>
					<div class="fredivs">
						<div class="psngrnote">
							<?php
								if(valid_array($traveller_details)) {
									$traveller_tab_content = 'You have saved passenger details in your list,on typing, passenger details will auto populate.';
								} else {
									$traveller_tab_content = 'You do not have any passenger saved in your list, start adding passenger so that you do not have to type every time. <a href="'.base_url().'index.php/user/profile?active=traveller" target="_blank">Add Now</a>';
								}
							?>
							<?=$traveller_tab_content;?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
 <script type="text/javascript">
  $('#stepbk1').click(function(){
		$(this).addClass('active');
		$('.bktab2, .bktab3').fadeOut(500,function(){$('.bktab1').fadeIn(500)});
	    $("#review").show();
	});
	
	$('#stepbk2,#next_review').click(function(){		 
	    $("#stepbk2").addClass('active');
		$('.bktab1, .bktab3').fadeOut(500,function(){$('.bktab2').fadeIn(500)});
	    $("#review").hide();
	});
	
	$('#stepbk3,#traveller_btn').click(function(){
	     $('#stepbk3').addClass('active');
		$('.bktab1, .bktab2').fadeOut(500,function(){$('.bktab3').fadeIn(500)});
	   
	}); 

</script>
<script>
	function change_guest_form(val){
	  if(val == 1){
		  $("#guest_txt").html('Guest User');
		  $("#sug_login").css('display', 'inherit');
		  $("#sug_guest").css('display', 'none');
		  $("#login_form").css('display', 'none');
		  $("#guest_from").css('display', 'inherit');
	  }else{
		   $("#guest_txt").html('Login Register User');
		  $("#sug_login").css('display', 'none');
		  $("#sug_guest").css('display', 'inherit');
		  $("#login_form").css('display', 'inherit');
		  $("#guest_from").css('display', 'none');
	  }
	}
var base_url = "<?php echo base_url(); ?>";	

function form_submits_bookings(formName) {
	if(formName == 'guest_login') {
		if($('#email').val() == ''){
		$('#email-error').html('Please enter Email Id');
		return false;
		}else{
		$('#email-error').html('');
	}

	if($('#password').val() == ''){
		$('#password-error').html('Please enter password');
		return false;
	}else{
		$('#password-error').html('');
	}

	var data_action  = base_url+'account/login';
	var data_pass = {'email' : $('#email').val(), 'password' : $('#password').val(),'users':'users'};
	}else{
		if($('#mobile').val() == ''){
			$('#mobile-error').html('Please enter Mobile Numnber');
		return false;
	}else{
		$('#mobile-error').html('');	
	}

	if($('#g_email').val() == ''){
		$('#g_email-error').html('Please enter Email Id');
		return false;
	}else{
		$('#g_email-error').html('');
	}

	var data_action  = base_url+'booking/create_guest_account';
	var data_pass = {'email' : $('#g_email').val(), 'mobile' : $('#mobile').val()};
	}

	$.ajax({
		type: "POST",
		url: data_action,
		dataType: "json",
		data : data_pass,
		success: function(data){
			if(data.status == 3) {
				$.each(data, function(key, value) {
				 $('#'+key).text(value);
				});
			}
			if(data.status == 2){
				$("#account_already").text('Email ID already registered.Login as User');
			}
			if(data.status == 1){
			     $('.center_pro').removeClass('active');
				 $("#stepbk2").addClass('active');
				 $('.bktab1, .bktab3').fadeOut(500,function(){$('.bktab2').fadeIn(500)});
		   }else{
				$('#loginLdrReg_reg').hide();
				$('div.reg_popuperror').html(data.msg);
				$('div.reg_popuperror').show();
			}
		}
	}); 
	return false;
	}
</script>
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script>

<?php echo $GLOBALS['CI']->load->view('share/passenger_confirm_popup');?>

<?php 
function diaplay_phonecode($phone_code,$active_data)
	{
		$list='';
		foreach($phone_code as $code){
		if($active_data['api_country_list_fk']==$code['origin']){
				$selected ="selected";
			}
			else {
				$selected="";
			}
		
			$list .="<option value=".$code['country_code']."  ".$selected." >".$code['country_code']."</option>";
		}
		 return $list;
		
	}
	?>
<?php echo  $this->load->view('core/bottom_footer'); ?> 