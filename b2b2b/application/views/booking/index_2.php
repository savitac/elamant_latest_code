<?php if(isset($_SESSION['currency'])){
          $this->display_currency = $_SESSION['currency'];
        } else {
          $this->display_currency = $_SESSION['currency'] = BASE_CURRENCY;
          $_SESSION['currency_value'] = '1'; 
        }
        ?>
<?php 
	$cart_iter = 0;
	foreach($cart_global as $key => $cid):
		list($module, $cid) = explode(',', $cid);
		if($module == 'Hotels'):
			$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
			$request_data = json_decode(base64_decode($cart->request));
			if($cart->cancellation_till_date >= date('Y-m-d')) {
				$paylater = true;
			}
			else {
				$paylater = false;
			}
			if($this->session->userdata('user_type') == 2):
				$net_Total[] = $cart->admin_baseprice;
				$commissionable_Total[] = $cart->agent_baseprice;

			elseif($this->session->userdata('user_type') == 4):
				$net_Total[] = $cart->sub_agent_baseprice;
				$commissionable_Total[] = $cart->total_cost;
			else:
			endif;
			$ovr_all_tax[] = $cart->service_charge + $cart->gst_charge + $cart->tax_charge;
			$hotel_request =  json_decode(base64_decode($cart->request));
		elseif($module == 'Transfer'):
			$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
			if($cart->cancellation_till_date >= date('Y-m-d')) {
				$paylater = true;
			}
			else {
				$paylater = false;
			}
			if($this->session->userdata('user_type') == 2):
				$net_Total[] = $cart->admin_base_price;
				$commissionable_Total[] = $cart->agent_baseprice;
			elseif($this->session->userdata('user_type') == 4):
				$net_Total[] = $cart->sub_agent_baseprice;
				$commissionable_Total[] = $cart->total_cost;
			else:
			endif;
			$ovr_all_tax[] = $cart->service_charge + $cart->gst_charge + $cart->tax_charge;
		endif;
	endforeach;
	$PG_Markup = 0;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Chinagap</title>
        <?php echo $this->load->view("core/load_css"); ?>
        <link href="<?php echo ASSETS; ?>assets/css/hotel_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/pre_booking.css" rel="stylesheet">
    </head>
    <body>
        <!-- Navigation -->
        <?php
            echo $this->load->view('dashboard/top');
		?>
        <!-- /Navigation -->
        <?php 
			$Acount = 0;$Fcount = 0;$Hcount = 0;$Ccount = 0;$Car_count = 0;$Car_v1_count = 0; $tCount =0;;
			$Vcount = 0;
			$Total = array();
			foreach($cart_global as $key => $cid) {
				list($module, $cid) = explode(',', $cid);
				if($module == 'Hotels'){
					$Hcount = $Hcount+1;
				}
			}
			foreach($cart_global as $key => $cid) {
				list($module, $cid) = explode(',', $cid);
				if($module == 'Hotels'){
					$cart = $this->cart_model->getBookingTemp_hotel($cid);
					//~ echo "<pre>",print_r($cart);exit;
					$Totall[] = $cart->total_cost;
					//~ $api_temp_hotel_id_key = explode("<br>", $cart->api_temp_hotel_id_key);
					//~ $cancelpolicy=$this->cart_model->getcancelpolicy($api_temp_hotel_id_key[0]);
				}else if($module == 'Transfer'){
					$transfer_cart = $this->cart_model->getBookingTemp_transfer($cid);
					$Totall[] = $transfer_cart->total_cost; 
			    }
			}	
		?>
        <div class="full onlycontent top80">
            <div class="container martopbtm">
                <div class="payment_process">
                    <div class="col-xs-4 nopad">
                        <div class="center_pro active" id="stepbk1">
                            <div class="fabols"></div>
                            <div class="center_labl"><?php echo $this->TravelLights['HotelResult']['Review']; ?></div>
                        </div>
                    </div>
                    <div class="col-xs-4 nopad">
                        <div class="center_pro" id="stepbk2">
                            <div class="fabols"></div>
                            <div class="center_labl"><?php echo $this->TravelLights['HotelResult']['Travellers']; ?></div>
                        </div>
                    </div>
                    <div class="col-xs-4 nopad">
                        <div class="center_pro" id="stepbk3">
                            <div class="fabols"></div>
                            <div class="center_labl"><?php echo $this->TravelLights['HotelResult']['Payment']; ?></div>
                        </div>
                    </div>
                </div>
                <form name="checkout-apartment" id="checkout-apartment" autocomplete="off" data-flag="COMMISSIONABLE" onsubmit="return my_ownvalidation();" action="<?php echo ASSETS;?>booking/checkout">
					<div class="bktab1">
						<div class="paymentpage">
							<div class="col-md-4 col-sm-4 nopad sidebuki">
								<div class="cartbukdis">
									<ul class="liscartbuk">
										<li class="lostcart" id="promo_wrap">
											<?php
												if($book_temp_data[0]->promo_code == '' || $book_temp_data[0]->promo_code == NULL) {
											?>
												<div class="cartlistingbuk">
													<div class="cartitembuk">
														<div class="col-md-12">
															<div class="payblnhmxm"><?php echo $this->TravelLights['HotelResult']['Promocode']; ?></div>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="cartitembuk prompform">
														<div class="col-md-8 col-xs-8">
																<div class="cartprc">
																	<div class="payblnhm singecartpricebuk ritaln">
																		<input type="text" id="promo_code" name="promo_code"  placeholder="<?php echo $this->TravelLights['HotelResult']['EnterPromo']; ?>"  class="promocode"/>
																		<label  id="promo_msg"></label>
																	</div>
																</div>
															</div>
															<div class="col-md-4 col-xs-4">
																<input type="button" onclick="process_promo();" value="<?php echo $this->TravelLights['HotelResult']['Apply']; ?>" name="apply" class="promosubmit">
															</div>
													
													</div>
													<div class="clearfix"></div>
													<div class="savemessage"></div>
												</div>
											<?php
												}
												else {
											?>
												<div class="cartlistingbuk">
													<div class="clearfix"></div>
													<div class="cartitembuk prompform">
														<div class="col-md-8 col-xs-8">
															<div class="cartprc">
																<div class="payblnhm singecartpricebuk ritaln">
																	<span><?php echo $this->TravelLights['HotelResult']['Appliedpromois']; ?> <strong><?php echo $book_temp_data[0]->promo_code; ?></strong></span>
																	<label id="promo_msg"></label>
																	<a href="javascript:void(0);" onclick="remove_promo()" ><?php echo $this->TravelLights['HotelResult']['Remove']; ?></a>
																</div>
															</div>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="savemessage"></div>
												</div>
											<?php
												}
											?>
										</li>
									
										<li class="lostcart">
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['SubTotal']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $this->display_currency; //echo $book_temp_data[0]->site_currency; ?> <span class="sub_total"> <?php echo number_format(array_sum($Totall)-array_sum($ovr_all_tax) * $_SESSION['currency_value'],2); //echo number_format(array_sum($Totall)-array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk discount_wrap" style="display:none;">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['Discount']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc discount"><?php echo "-".$this->display_currency; //echo "- ".$book_temp_data[0]->site_currency;?> <span class="amount promodiscount promo_discount" ></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk tax_fees" style="display:none;">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['Taxes']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $this->display_currency; //echo $book_temp_data[0]->site_currency; ?> <span class="taxes"><?php echo number_format(array_sum($ovr_all_tax) * $_SESSION['currency_value'],2);//number_format(array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk cc_fees" style="display:none;">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['CreditCardTransactionFees']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency; ?> <span class="cc_fees_value"></span></div>
														</div>
													</div>
												</div>
												
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk deposit_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['DepositPayment']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc finalAmt"><span class="amount"><?php echo $this->display_currency; //$book_temp_data[0]->site_currency; ?> <span class="total_deposit_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk cc_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['CreditCardPayment']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc finalAmt"><span class="amount"><?php echo $this->display_currency; //$book_temp_data[0]->site_currency; ?> <span class="total_cc_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['Total']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc finalAmt"><span class="amount"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency; ?> <span class="sub_total promo_total total_price"><?php echo  number_format($book_temp_data[0]->total_cost * $_SESSION['currency_value'],2); //number_format($book_temp_data[0]->total_cost,2,'.', ' '); ?></span></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-md-8 col-sm-8 nopad fulbuki">
								<div class="sumry_wrap">
									<?php 
										foreach($cart_global as $key => $cid):
											list($module, $cid) = explode(',', $cid);
											if($module == 'Hotels'):
												$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
												$hotel_request =  json_decode(base64_decode($cart->request));
												//~ echo "cart data:<pre>";print_r($cart);exit;
												
												if($cart->api_id == 5) {
													//~ Star Rating 
													$star = $cart->star;
													if($star == '1EST'){
														$star = 1;  
													 }
													else if($star == '2EST'){
														$star = 2;  
													}
													else if($star == '3EST'){
														$star = 3;  
													}
													else if($star == '4EST'){
													  $star = 4;  
													}
													else if($star == '5EST'){
														$star = 5;  
													}
													else{
														$star = $star;  
													}
													//~ Thumb Image
													$thumb_image_all = explode(',',$cart->images);
													$thumb_image = $thumb_image_all[0];
													
													// Cancellation Data
													$cancel_policy = $cart->policy_description;
												}
												else {
													$star = $cart->star_rating;
													$thumb_image = $cart->thumb_image;
													$cancel_policy = $cart->cancel_policy;
												}
												$special_notes = $cart->comment;
												 
												//~ echo "data: <pre>";print_r($hotel_request);exit;
									?>
												<!--  Hotel Summery  -->
												<div class="pre_summery">
													<div class="prebok_hding">
														<div class="detail_htlname"><?php echo $cart->hotel_name; ?></div>
														<div class="star_detail">
															<div class="stra_hotel" data-star="<?php echo $star; ?>"> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> </div>
														</div>
													</div>
													<div class="sidenamedesc">
														<div class="celhtl width20 midlbord">
															<div class="hotel_prebook"> <img alt="" src="<?php echo $thumb_image; ?>"> </div>
														</div>
														<div class="celhtl width80">
															<div class="waymensn">
																<div class="flitruo cloroutbnd">
																	<div class="detlnavi">
		<!--
																		<div class="rmtype">King Executive Room</div>
																		<div class="rminclusin">Breakfast Only</div>
																		<div cass="clearfix"></div>
		-->
																		<div class="col-xs-4 padflt widfty">
																			<span class="timlbl right"> <span class="flname"><span class="fltime"><?php echo $this->TravelLights['HotelResult']['Checkin']; ?></span></span> </span>
																			<div class="clearfix"></div>
																			<span class="flitrlbl elipsetool right"><?php echo $hotel_request->hotel_checkin; ?></span> 
																		</div>
																		<div class="col-xs-4 nopad padflt widfty">
																			<div class="lyovrtime"> <span class="flect"><?php echo ($hotel_request->adult_count + $hotel_request->child_count); ?><?php echo $this->TravelLights['HotelResult']['Passenger']; ?></span> <span class="flects"> <?php echo $hotel_request->rooms; ?> <?php echo $this->TravelLights['HotelResult']['Room']; ?></span> </div>
																		</div>
																		<div class="col-xs-4 padflt widfty">
																			<span class="timlbl left"> <span class="flname"><span class="fltime"><?php echo $this->TravelLights['HotelResult']['Checkout']; ?></span> </span> </span>
																			<div class="clearfix"></div>
																			<span class="flitrlbl elipsetool"><?php echo $hotel_request->hotel_checkout; ?></span> 
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="all_oters backblur1">
														<div class="para_sentnce">
															<strong><?php echo $this->TravelLights['HotelResult']['CancellationPolicy']; ?> :</strong><span  class="cancellation"> <?php echo $cancel_policy; ?> </span>
														</div>
													</div>
													<?php
														if($special_notes != '') {
													?>
														<div class="clearfix"></div>
														<div class="all_oters backblur1">
															<div class="para_sentnce">
																<strong><?php echo "Special Information"; ?> :</strong><span  class="special_notes"> <?php echo $special_notes; ?> </span>
															</div>
														</div>
													<?php
														}
													?>
												</div>
												<!--  Hotel Summery End -->
									<?php
									        elseif($module == 'Transfer'):
									            $cart_transfer = $this->cart_model->getCartDataByModule($cid,$module)->row();
									            $request = json_decode(base64_decode($cart_transfer->transfer_request));
									           
									         ?>
											<div class="pre_summery">
											
											<div class="sidenamedesc">
											<div class="celhtl width20 midlbord">
											</div>
											<div class="celhtl width80">
											<div class="waymensn">
											<div class="flitruo cloroutbnd">
											  <div class="detlnavi">
												  
												<div class="col-xs-4 padflt widfty"> <span class="timlbl right"> <span class="flname"><span class="fltime"><?php echo $cart_transfer->pickup_city_code_description; ?></span></span> </span>
												  <div class="clearfix"></div>
												 </div>
												<div class="col-xs-4 nopad padflt widfty">
												  <div class="lyovrtime"> <span class="flect"><?php echo $request->no_of_travellers; ?> <?php echo $this->TravelLights['HotelResult']['Passenger']; ?></span>
												  <span class="flects"><?php echo date('d-m-Y', strtotime($cart_transfer->travel_start_date)); ?></span>  </div>
												</div>
												<div class="col-xs-4 padflt widfty"> <span class="timlbl left"> <span class="flname"><span class="fltime"><?php echo $cart_transfer->dropoff_city_code_description; ?></span> </span> </span>
												  <div class="clearfix"></div>
												 </div>
											  </div>
											</div>
										  </div>
										</div>
									</div>
                                      <div class="clearfix"></div>
									  <div class="all_oters">
										<div class="para_sentnce">
											<strong><?php echo $this->TravelLights['HotelResult']['Description']; ?> : </strong> 
											 <?php echo $cart_transfer->item_description; ?>
										</div>
										<div class="para_sentnce">
											<strong><?php echo $this->TravelLights['HotelResult']['CancellationPolicy']; ?> : </strong> 
											<span class='cancellation'><?php if($cart_transfer->caneclation_policy != ''){
          $cancellaiton_details = json_decode($cart_transfer->caneclation_policy, true);
        if(isset($cancellaiton_details['cancellation'])){ ?>
		
			<?php for($cancel =0; $cancel < count($cancellaiton_details['cancellation']); $cancel++) { 
				
		   if($cancellaiton_details['cancellation'][$cancel]['Charge'] == 'true') { ?> 
		    
            	<?php echo $this->TravelLights['HotelResult']['Cancellationfrom']; ?> <?php echo date('dS M Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate'])); ?> <?php echo $this->TravelLights['HotelResult']['onwardswillincurcancellationcharge']; ?>  <b><?php echo "AUD ".$cancellaiton_details['cancellation'][$cancel]['ChargeAmount']; ?></b>|
           
            <?php } else { ?>
				
				<?php echo $this->TravelLights['HotelResult']['Nocancellationfeeuntil']; ?> <?php echo date('dS M Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate'])); ?>|
				
		   <?php } ?>
       
        
        <?php } } if(false) { ?>
        <strong><?php echo $this->TravelLights['HotelResult']['AmendmentPolicy']; ?> ; </strong> 
        <?php  if(isset($cancellaiton_details['amendment'])){ ?>
		<ul class="list_popup">
	
			<?php for($cancel =0; $cancel < count($cancellaiton_details['amendment']); $cancel++) { 
				
		   if($cancellaiton_details['amendment'][$cancel]['Charge'] == 'true') { ?> 
		    <li class="listcancel">
            	<?php echo $this->TravelLights['HotelResult']['Amendmentfrom']; ?> <?php echo date('d-m-Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?> <?php echo $this->TravelLights['HotelResult']['onwardswillincurcancellationcharge']; ?>  <b><?php echo "AUD ".$cancellaiton_details['amendment'][$cancel]['ChargeAmount']; ?></b>
            </li>
            <?php } else { ?>
				<li class="listcancel">
				<?php echo $this->TravelLights['HotelResult']['NoAmendmentfeeuntil']; ?> <?php echo date('d-m-Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?>
				</li>
		   <?php } ?>
        </ul>
        
        <?php } }  } } ?>
        </span> 
										</div>
									  </div>
					
            </div>
           
								<?php	endif;
										endforeach;
									?>
									<div class="col-xs-6 nopad fulat500">
										<input type="button" id="next_review" class="paysubmit" value="Continue">
									</div>
								</div>
							</div>
						</div>
					</div>					
					<div class="bktab2" style="display:none;">
						<div class="paymentpage">
							<div class="col-md-4 col-sm-4 nopad sidebuki">
								<div class="cartbukdis">
									<ul class="liscartbuk">
										<!--For hotel traveller-->
										<?php 
											foreach($cart_global as $key => $cid):
												list($module, $cid) = explode(',', $cid);
												if($module == 'Hotels'):
													$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
													$hotel_request =  json_decode(base64_decode($cart->request));
													//~ echo "data: <pre>";print_r($hotel_request);exit;
													
													if($cart->api_id == 5) {
														//~ Star Rating 
														$star = $cart->star;
														if($star == '1EST'){
															$star = 1;  
														 }
														else if($star == '2EST'){
															$star = 2;  
														}
														else if($star == '3EST'){
															$star = 3;  
														}
														else if($star == '4EST'){
														  $star = 4;  
														}
														else if($star == '5EST'){
															$star = 5;  
														}
														else{
															$star = $star;  
														}
														//~ Thumb Image
														$thumb_image_all = explode(',',$cart->images);
														$thumb_image = $thumb_image_all[0];
													}
													else {
														$star = $cart->star_rating;
														$thumb_image = $cart->thumb_image;
													}
													
										?>
										<li class="lostcart">
											<div class="carttitlebuk"><?php echo $cart->hotel_name; ?></div>
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-3 celcart"> <a class="smalbukcrt"><img src="<?php echo $thumb_image; ?>" alt=""/></a><br>
													</div>
													<div class="col-md-8 splcrtpad celcart">
														<div class="carttitlebuk1">
															<div class="col-xs-6 nopad">
																<?php echo $this->TravelLights['HotelResult']['CheckIn']; ?>:
																<div class="cartsec_time"><?php echo $hotel_request->hotel_checkin; ?></div>
															</div>
															<div class="col-xs-6 nopad">
																<?php echo $this->TravelLights['HotelResult']['CheckOut']; ?>:
																<div class="cartsec_time"><?php echo $hotel_request->hotel_checkout; ?></div>
															</div>
														</div>
													</div>
													<div class="col-md-1 cartfprice celcart">
														<div class="cartprc">
															<div class="singecartpricebuk"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency; ?> <span class="single_hotel_price" ><?php echo number_format($cart->total_cost * $_SESSION['currency_value'],2); //$cart->total_cost; ?></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<?php
												endif;
											endforeach;
										?>
										<!--For hotel traveller  End-->
										<li class="lostcart">
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['SubTotal']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $this->display_currency; //$book_temp_data[0]->site_currency; ?> <span class="sub_total"><?php echo number_format(array_sum($Totall) - array_sum($ovr_all_tax) * $_SESSION['currency_value'],2); //number_format(array_sum($Totall) - array_sum($ovr_all_tax), 2, '.', ' ');?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk discount_wrap" style="display:none;">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['Discount']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc discount"><?php echo "-".$this->display_currency; //echo "- ".$book_temp_data[0]->site_currency;?> <span class="amount promodiscount promo_discount" ></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk tax_fees" style="display:none;">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['Taxes']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $this->display_currency; //$book_temp_data[0]->site_currency; ?> <span class="taxes"><?php echo number_format(array_sum($ovr_all_tax) * $_SESSION['currency_value'],2); //number_format(array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk cc_fees" style="display:none;">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['CreditCardTransactionFees']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php $this->display_currency;//echo $book_temp_data[0]->site_currency; ?> <span class="cc_fees_value"></span></div>
														</div>
													</div>
												</div>

											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk deposit_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['DepositPayment']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc finalAmt"><span class="amount"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency; ?> <span class="total_deposit_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk cc_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['CreditCardPayment']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc finalAmt"><span class="amount"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency; ?> <span class="total_cc_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['Total']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc finalAmt"><span class="amount"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency; ?> <span class="total promo_total total_price"><?php echo  number_format(array_sum($Totall) * $_SESSION['currency_value'],2);//number_format(array_sum($Totall),2,'.',' '); ?></span></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-md-8 col-sm-8 nopad fulbuki ">
								
								<div class="col-md-12 padleftpay">
									<div class="wrappay leftboks">
										<?php 
											$cart_iter = 0;
											foreach($cart_global as $key => $cid):
												list($module, $cid) = explode(',', $cid);
												if($module == 'Hotels'):
													$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
													$Total[] = $cart->total_cost;
													$hotel_request =  json_decode(base64_decode($cart->request));
													//~ echo "data: <pre>";print_r($hotel_request);exit;
										?>
													<div class="comon_backbg">
														<h3 class="inpagehed"><?php echo $this->TravelLights['HotelResult']['Traveller']; ?></h3>
														<div class="sectionbuk">
															<div class="onedept">
																<div class="evryicon"><span class="fa fa-bed"></span></div>
																<div class="pasenger_location">
																	<h3 class="inpagehedbuk"> <span class="bookingcnt"></span> <span class="aptbokname"><?php echo $cart->hotel_name; ?></span> </h3>
																	<span class="hwonum"><?php echo $this->TravelLights['HotelResult']['Adult']; ?> <?php echo ($hotel_request->adult_count); ?></span> <span class="hwonum"><?php echo $this->TravelLights['HotelResult']['Child']; ?> <?php echo ($hotel_request->child_count); ?></span> 
																</div>
																<div class="clearfix"></div>
																<div class="payrow">
																	<?php
																		for($room = 0; $room < $hotel_request->rooms; $room++):
																	?>
																		<span class="roomcntad">Room <?php echo ($room+1); ?></span>
																		<?php
																			for($ad = 0; $ad < $hotel_request->adult[$room]; $ad++):
																		?>
																				<div class="repeatprows">
																					<div class="col-md-2 downsrt set_margin">
																						<div class="lokter"> <span class="fa fa-user"></span> <span class="whoare"><?php echo $this->TravelLights['HotelResult']['Adult']; ?>(<?php echo $ad+1; ?>)</span> </div>
																					</div>
																					<div class="col-md-3 set_margin">
																						<div class="selectedwrap">
																							<select class="flpayinput" name="a_gender<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ad; ?>]" >
																								<option value="Mr">Mr</option>
																								<option value="Mrs">Mrs</option>
																								<option value="Ms">Ms</option>
																								<option value="Miss">Miss</option>
																								<option value="Dr">Dr</option>
																								<option value="Prof">Prof</option>
																							</select>
																						</div>
																					</div>
																					<div class="col-md-7 nopad">
																						<div class="col-md-6 set_margin">
																							<input name="first_name<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ad; ?>]" class="first_name payinput" placeholder="<?php echo $this->TravelLights['HotelResult']['FirstName']; ?>" type="text" />
																						</div>
																						<div class="col-md-6 set_margin">
																							<input name="last_name<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ad; ?>]" class="last_name payinput" placeholder="<?php echo $this->TravelLights['HotelResult']['LastName']; ?>" type="text" />
																						</div>
																					</div>
																				</div>
																		<?php
																			endfor;
																			if($hotel_request->child[$room] > 0):
																		?>
																				<?php
																					for($ch = 0; $ch < $hotel_request->child[$room]; $ch++):
																				?>
																						<div class="repeatprows">
																							<div class="col-md-2 downsrt set_margin">
																								<div class="lokter"> <span class="fa fa-male"></span> <span class="whoare"><?php echo $this->TravelLights['HotelResult']['Child']; ?> (<?php echo $ch+1; ?>)</span><br/> <span class="childageback"><?php echo $this->TravelLights['HotelResult']['Age']; ?> - <?php echo $hotel_request->childAges[$room][$ch]; ?> <?php echo $this->TravelLights['HotelResult']['yrs']; ?></span> </div>
																							</div>
																							<div class="col-md-3 set_margin">
																								<div class="selectedwrap">
																									<select class="flpayinput" name="c_gender<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ch; ?>]" >
																										<option value="Master"><?php echo $this->TravelLights['HotelResult']['Master']; ?></option>
																										<option value="Miss"><?php echo $this->TravelLights['HotelResult']['Miss']; ?></option>
																									</select>
																								</div>
																							</div>
																							<div class="col-md-7 nopad">
																								<div class="col-md-4 set_margin">
																									<input name="cfirst_name<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ch; ?>]" class="c_first_name payinput" placeholder="<?php echo $this->TravelLights['HotelResult']['FirstName']; ?>" type="text" required />
																								</div>
																								<div class="col-md-4 set_margin">
																									<input name="clast_name<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ch; ?>]" class="c_last_name payinput" placeholder="<?php echo $this->TravelLights['HotelResult']['LastName']; ?>" type="text" />
																									<input type="hidden" value="<?php echo $hotel_request->childAges[$room][$ch]; ?>" name="childAges_room_<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ch; ?>]">
																								</div>
																								<div class="col-md-4 set_margin">
																									<input id="child_dob_<?php echo $room; ?>_<?php echo $ch; ?>" class="child_dob payinput" name="child_dob_<?php echo $cid ?>[<?php echo $room; ?>][<?php echo $ch; ?>]" value="" type="text" placeholder="<?php echo $this->TravelLights['HotelResult']['DOB']; ?>" readonly >
																								</div>
																							</div>
																						</div>
																						<script>
																							// First Date Of the month 
																							//~ var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
																							// Last Date Of the Month 
																							//~ var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
																							
																							//~ Datepicker Initiation
																							var age = "<?php echo $hotel_request->childAges[$room][$ch]; ?>";
																							var currentTime = new Date();
																							var currentTime_v1 = new Date();
																							var startDateTo = new Date(currentTime.setFullYear(currentTime.getFullYear() - age));
																							var startDateFrom = new Date(currentTime.setFullYear(currentTime.getFullYear() - 1));
																							startDateFrom.setDate(startDateFrom.getDate()+1);
																							$( "#child_dob_<?php echo $room; ?>_<?php echo $ch; ?>" ).datepicker({
																								dateFormat: 'yy-mm-dd',
																								minDate: startDateFrom,
																								maxDate: startDateTo,
																								changeMonth: true,
																								changeYear: true,
																								numberOfMonths: 1
																							});
																							
																							//~ End 
																						</script>
																				<?php
																					endfor;
																				?>
																	<?php
																		endif;
																	?>
																	<div class="clearfix" ></div>
																	<?php
																		endfor;
																	?>
																</div>
															</div>
														</div>
													</div>
										<?php
													$cart_iter++;
												elseif($module == 'Transfer'):
												  $data['ttransfer_cart'] =  $ttransfer_cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
												  
												    $data['cid'] = $cid;
												    $Total[] = $ttransfer_cart->total_cost;
													$data['transfer_request'] = $transfer_request =  json_decode(base64_decode($ttransfer_cart->transfer_request));
												
													?>
													
														<div class="sectionbuk">
															<div class="onedept">
																<div class="evryicon"><span class="fa fa-car"></span></div>
																<div class="pasenger_location">
																	<h3 class="inpagehedbuk"> <span class="bookingcnt"></span> <span class="aptbokname"><?php echo $ttransfer_cart->pickup_city_code_description." - ".$ttransfer_cart->dropoff_city_code_description ; ?></span> </h3>
																	<span class="hwonum"><?php echo $this->TravelLights['HotelResult']['Passenger']; ?> (<?php echo ($transfer_request->no_of_travellers); ?>)</span>
																</div>
																<div class="clearfix"></div>
																<div class="payrow">
																	<?php
																		for($pass = 0; $pass < $transfer_request->no_of_travellers; $pass++): ?>
																	
																				<div class="repeatprows">
																					<div class="col-md-2 downsrt set_margin">
																						<div class="lokter"> <span class="fa fa-user"></span> <span class="whoare"><?php echo $this->TravelLights['HotelResult']['Passenger']; ?>(<?php echo $pass+1; ?>)</span> </div>
																					</div>
																					
																					<div class="col-md-2 set_margin">
																						<div class="selectedwrap">
																							<select class="flpayinput" name="a_type<?php echo $cid ?>[<?php echo $pass; ?>]" onchange="change_div(this,'<?php echo $pass; ?>')">
																							    <option value="Adult"><?php echo $this->TravelLights['HotelResult']['Adults']; ?></option>
																								<option value="Child"><?php echo $this->TravelLights['HotelResult']['Child']; ?></option>
																							</select>
																						</div>
																					</div>
																					
																					<div class="col-md-2 set_margin"  id="adults<?php echo $pass; ?>">
																						<div class="selectedwrap">
																							<select class="flpayinput" name="a_gender<?php echo $cid ?>[<?php echo $pass; ?>]" >
																								<option value="Mr">Mr</option>
																								<option value="Mrs">Mrs</option>
																								<option value="Miss"><?php echo $this->TravelLights['HotelResult']['Miss']; ?></option>
																							</select>
																						</div>
																					</div>
																					
																					<div class="col-md-2 set_margin"  id="childs<?php echo $pass; ?>" style="display:none;">
																						<div class="selectedwrap">
																							<select class="flpayinput" name="c_gender<?php echo $cid ?>[<?php echo $pass; ?>]" >
																								<option value="Master"><?php echo $this->TravelLights['HotelResult']['Master']; ?></option>
																								<option value="Miss"><?php echo $this->TravelLights['HotelResult']['Miss']; ?></option>
																							</select>
																						</div>
																					</div>
																					
																					<div class="col-md-3 set_margin">
																						<input name="first_name<?php echo $cid ?>[<?php echo $pass; ?>]" id="first_name<?php echo $cid ?>[<?php echo $pass; ?>]" placeholder="<?php echo $this->TravelLights['HotelResult']['FirstName']; ?>" type="text" class="payinput" required />
																					</div>
																					<div class="col-md-3 set_margin">
																						<input name="last_name<?php echo $cid ?>[<?php echo $pass; ?>]" id="last_name<?php echo $cid ?>[<?php echo $pass; ?>]" placeholder="<?php echo $this->TravelLights['HotelResult']['LastName']; ?>" type="text" class="payinput" required />
																					</div>
																				</div>
																		
																		<?php	endfor; ?>
																			
																			<div class="clearfix" ></div>
																			
																			<?php $this->load->view('booking/transfer_compination', $data); ?>
																			
																			
																</div>
															</div>
														</div>
													
													
											<?php endif;
											endforeach;
										?>
									</div>
									<div class="clearfix"></div>
									 <?php if(false){ ?>
									<div class="comon_backbg">
										<h3 class="inpagehed"><?php echo $this->TravelLights['HotelResult']['Address']; ?></h3>
										<div class="sectionbuk billingnob">
											<div class="payrow">
												<div class="col-md-2">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['Salutation']; ?></div>
													<div class="selectedwrap">
														<select class="flpayinput " name="salutation" required>
															<option value="Mr">Mr</option>
															<option value="Mrs">Mrs</option>
															<option value="Miss">Miss</option>
														</select>
													</div>
												</div>
												<div class="col-md-5">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['FirstName']; ?></div>
													<input type="text" id="first_name" name="first_name" class="payinput" value="" required/>
												</div>
												<div class="col-md-5">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['LastName']; ?></div>
													<input type="text" id="last_name" name="last_name" class="payinput" value="" required/>
												</div>
											</div>
											<div class="payrow">
												<div class="col-md-6">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['Address']; ?></div>
													<input type="text" id="street_address" name="street_address" class="payinput" value="" required/>
												</div>
												<div class="col-md-6">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['Address']; ?>2</div>
													<input type="text" id="address2" name="address2" class="payinput" value=""/>
												</div>
											</div>
											<div class="payrow">
												<div class="col-md-4">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['EmailAddress']; ?></div>
													<input type="text" id="email" name="email" class="payinput" value=" " required/>
												</div>
												<div class="col-md-4">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['Contact']; ?></div>
													<input type="text" id="mobile" name="mobile" data-mask="00000-00000" class="payinput" value="" required/>
												</div>
												<div class="col-md-4">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['Country']; ?></div>
													<div class="selectedwrap">
														<select class="flpayinput" id="country" name="country" required>
															<?php
																echo "data: <pre>";print_r($countries);
																if(!empty($countries)):
																	foreach($countries as $country):
															?>
																		<option value="<?php echo $country->iso_code; ?>" ><?php echo $country->country_name; ?></option>
															<?php
																	endforeach;
																endif;
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="payrow">
												<div class="col-md-4">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['City']; ?></div>
													<input type="text" id="city"  name="city" class="payinput" value="" required/>
												</div>
												<div class="col-md-4">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['State']; ?></div>
													<input type="text" id="state" name="state" class="payinput" value=" " required/>
												</div>
												<div class="col-md-4">
													<div class="paylabel"><?php echo $this->TravelLights['HotelResult']['PostalCode']; ?></div>
													<input type="text" id="zip" name="zip" class="payinput" value=" " required/>
												</div>
											</div>
										</div>
									</div>
									<?php } ?>
<!--            
									<div class="clearfix"></div>
									<div class="col-md-12 nopad">
										<div class="checkcontent">
											<div class="squaredThree">
												<input type="checkbox" value="0" name="confirm" class="filter_airline" id="squaredThree1" required>
												<label for="squaredThree1"></label>
											</div>
											<label for="squaredThree1" class="lbllbl">By booking this item, you agree to pay the total amount shown, which includes Service Fees, on the right and to the<a class="colorbl"> Terms & Condition</a>, <a href = "" class="colorbl">Cancellation Policy</a>.</label>
										</div>
									</div>
-->
									<div class="clearfix"></div>
										<div class="col-xs-6 fulat500 nopad">
											
											<!-- Hidden Data -->
											
<!--
											<input type="hidden" name="service_charge" id="service_charge" value="0" />
											<input type="hidden" name="gst_charge" id="gst_charge" value="0" />
											<input type="hidden" name="tax_charge" id="tax_charge" value="0" />
											
											<input type="hidden" name="payment_type" id="payment_type_flag" value="Commissionable" />
											<input type="hidden" name="payment_type_deduction" id="payment_type_deduction_flag" value="" />
											<input type="hidden" name="PG_Charge" id="PG_Charge" value="" />
											<input type="hidden" id="total_payable" name="total" value="<?php echo base64_encode(array_sum($Total)); ?>"/>
											<input type="hidden" id="total_PG_payble" name="total_PG_payble" value=""/>
											<input type="hidden" id="total_deposit_payble" name="total_deposit_payble" value=""/>
-->
											<input type="hidden" name="cid" id="cid" value="<?php echo $cart_global_id;?>"/>
												
											<!-- End -->
											
											<input type="button" class="paysubmit" name="continue" id="traveller_btn" value="<?php echo $this->TravelLights['HotelResult']['Continue']; ?>" />
										</div>
										<div class="col-md-9 col-xs-3 fulat500 nopad"> </div>
										<div class="clear"></div>
										<div class="lastnote"> </div>
								</div>
							</div>
						</div>
					</div>
					<div class="bktab3" style="display:none;">
						<div class="paymentpage">
							
							<div class="col-md-4 col-sm-4 nopad sidebuki">
								<div class="cartbukdis">
									<ul class="liscartbuk">
										<!--For hotel traveller-->
										<?php 
											foreach($cart_global as $key => $cid):
												list($module, $cid) = explode(',', $cid);
												if($module == 'Hotels'):
													$cart = $this->cart_model->getCartDataByModule($cid,$module)->row();
													$hotel_request =  json_decode(base64_decode($cart->request));
													//~ echo "data: <pre>";print_r($hotel_request);exit;
													
													if($cart->api_id == 5) {
														//~ Star Rating 
														$star = $cart->star;
														if($star == '1EST'){
															$star = 1;  
														 }
														else if($star == '2EST'){
															$star = 2;  
														}
														else if($star == '3EST'){
															$star = 3;  
														}
														else if($star == '4EST'){
														  $star = 4;  
														}
														else if($star == '5EST'){
															$star = 5;  
														}
														else{
															$star = $star;  
														}
														//~ Thumb Image
														$thumb_image_all = explode(',',$cart->images);
														$thumb_image = $thumb_image_all[0];
													}
													else {
														$star = $cart->star_rating;
														$thumb_image = $cart->thumb_image;
													}
													
										?>
										<li class="lostcart">
											<div class="carttitlebuk"><?php echo $cart->hotel_name; ?></div>
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-3 celcart"> <a class="smalbukcrt"><img src="<?php echo $thumb_image; ?>" alt=""/></a><br>
													</div>
													<div class="col-md-8 splcrtpad celcart">
														<div class="carttitlebuk1">
															<div class="col-xs-6 nopad">
																<?php echo $this->TravelLights['HotelResult']['CheckIn']; ?>:
																<div class="cartsec_time"><?php echo $hotel_request->hotel_checkin; ?></div>
															</div>
															<div class="col-xs-6 nopad">
																<?php echo $this->TravelLights['HotelResult']['CheckOut']; ?>:
																<div class="cartsec_time"><?php echo $hotel_request->hotel_checkout; ?></div>
															</div>
														</div>
													</div>
													<div class="col-md-1 cartfprice celcart">
														<div class="cartprc">
															<div class="singecartpricebuk"><?php  echo $this->display_currency; //$book_temp_data[0]->site_currency; ?> <span class="single_hotel_price" ><?php echo $cart->total_cost; ?></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<?php
												endif;
											endforeach;
										?>
										<!--For hotel traveller  End-->
										<li class="lostcart">
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['SubTotal']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency; ?> <span class="sub_total"><?php echo number_format(array_sum($Totall)-array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk discount_wrap" style="display:none;">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['Discount']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc discount"><?php echo "- ".$this->display_currency;//$book_temp_data[0]->site_currency; //echo "- ".$book_temp_data[0]->site_currency;?> <span class="amount promodiscount promo_discount" ></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk tax_fees" style="display:none;">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['Taxes']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency;//$book_temp_data[0]->site_currency; ?> <span class="taxes"><?php echo number_format(array_sum($ovr_all_tax) * $_SESSION['currency_value'],2);//number_format(array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk cc_fees" style="display:none;">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['CreditCardTransactionFees']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $this->display_currency; //$book_temp_data[0]->site_currency; ?> <span class="cc_fees_value">0.00</span></div>
														</div>
													</div>
												</div>

											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk deposit_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['DepositPayment']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc finalAmt"><span class="amount"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency; ?> <span class="total_deposit_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk cc_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['CreditCardPayment']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc finalAmt"><span class="amount"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency; ?> <span class="total_cc_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-5 celcart">
														<div class="payblnhm"><?php echo $this->TravelLights['HotelResult']['Total']; ?></div>
													</div>
													<div class="col-md-7 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc finalAmt"><span class="amount"><?php echo $this->display_currency;//$book_temp_data[0]->site_currency; ?> <span class="total promo_total total_price" ><?php echo number_format(array_sum($Totall),2,'.',' '); ?></span></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							
							<div class="col-md-8 col-sm-8 nopad fulbuki ">
							<div class="col-xs-12 padleftpay">
								<div class="payselect comon_backbg">
									<h3 class="inpagehed"><?php echo $this->TravelLights['HotelResult']['SelectaPaymentMethod']; ?></h3>
									<div class="sectionbuk">
										<div class="alldept">
											<input type="hidden" name="applied_promo_code" id="applied_promo_code" value="" />
											<input type="hidden" id="promo_price" name="promo_price"  placeholder="<?php echo $this->TravelLights['HotelResult']['EnterPromo']; ?>"  class="promocode"/>
											<div class="oneselects active" id="pay_later"> 
												<input type="checkbox" class="hideinpay">
												<div class="typeselctrs"><span class="fa fa-check"></span> <?php echo $this->TravelLights['HotelResult']['Commissionable']; ?></div>
											</div>
											<div class="oneselects" id="pay_now">
												<input type="checkbox" class="hideinpay">
												<div class="typeselctrs"><span class="fa fa-check"></span>  <?php echo $this->TravelLights['HotelResult']['NetRate']; ?></div>
											</div>
										</div>
										
										<div class="paylater_show" >
											<ul class="radiochecks">
												<li class="li-option-2" id="commissionable_li_1" >
													<label for="f-option-1" >
														<input type="radio" id="f-option-1" value="DEPOSIT" name="type_of_payment" checked >
														<label for="f-option-1" class="check"></label>
														<label for="f-option-1"><?php echo $this->TravelLights['HotelResult']['Deposit']; ?></label>
													</label>
												</li>
												  
												<li class="li-option-2" id="commissionable_li_2" >
													<label for="s-option-1" >
														<input type="radio" id="s-option-1" value="PG" name="type_of_payment">
														<label for="s-option-1" class="check"></label>
														<label for="s-option-1"><?php echo $this->TravelLights['HotelResult']['CreditCard']; ?></label>
													</label>
												</li>
												  
												<li class="pay_both_2" id="commissionable_li_3" >
													<label for="t-option-1" >
														<input type="radio" id="t-option-1" value="BOTH" name="type_of_payment">
														<label for="t-option-1" class="check"></label>
														<label for="t-option-1"><?php echo $this->TravelLights['HotelResult']['Both']; ?></label>
													</label>
												</li>
												<?php
													if($paylater) {
												?>
													<li class="li-option-2" id="commissionable_li_4" >
														<label for="l-option-1" >
															<input type="radio" id="l-option-1" value="PAYLATER" name="type_of_payment">
															<label for="l-option-1" class="check"></label>
															<label for="l-option-1"><?php echo $this->TravelLights['HotelResult']['PayLater']; ?></label>
														</label>
													</li>
												<?php
													}
												?>
											</ul>
											<div class="col-xs-4 nopad" id="deposit-container-2" style="display:none;" >
												<input name="commissionable_both_payment" data-total-price="<?php echo array_sum($commissionable_Total); ?>" id="commissionable_both_payment" onkeypress="return isNumber(event)" onkeyup="price_calculation('COMMISSIONABLE');" placeholder="<?php echo $this->TravelLights['HotelResult']['DepositAmount']; ?>" type="text" class="payinput">
											</div>
										</div>
							
										<div class="pay_show" style="display:none;">
											<ul class="radiochecks">
												<li class="li-option-1" id="net_li_1" >
													<label for="f-option" >
														<input type="radio" id="f-option" value="DEPOSIT" name="type_of_payment1" checked >
														<label for="f-option" class="check"></label>
														<label for="f-option"><?php echo $this->TravelLights['HotelResult']['Deposit']; ?></label>
													</label>
												</li>
												  
												<li class="li-option-1" id="net_li_2" >
													<label for="s-option" >
														<input type="radio" id="s-option" value="PG" name="type_of_payment1">
														<label for="s-option" class="check"></label>
														<label for="s-option"><?php echo $this->TravelLights['HotelResult']['CreditCard']; ?></label>
													</label>
												</li>
												  
												<li class="pay_both_1" id="net_li_3" >
													<label for="t-option" >
														<input type="radio" id="t-option" value="BOTH" name="type_of_payment1">
														<label for="t-option" class="check"></label>
														<label for="t-option"><?php echo $this->TravelLights['HotelResult']['Both']; ?></label>
													</label>
												</li>
												<?php
													if($paylater) {
												?>
													<li class="li-option-1" id="net_li_4" >
														<label for="l-option" >
															<input type="radio" id="l-option" value="PAYLATER" name="type_of_payment1">
															<label for="l-option" class="check"></label>
															<label for="l-option"><?php echo $this->TravelLights['HotelResult']['PayLater']; ?></label>
														</label>
													</li>
												<?php
													}
												?>
											</ul>
											<div class="col-xs-4 nopad" id="deposit-container-1" style="display:none;" >
												<input name="net_both_payment" data-total-price="<?php echo array_sum($net_Total); ?>" id="net_both_payment" onkeypress="return isNumber(event)" onkeyup="price_calculation('NET');" placeholder="<?php echo $this->TravelLights['HotelResult']['DepositAmount']; ?>" type="text" class="payinput">
											</div>
										</div>
						  
							  
										<div class="tabtwoout">
	<!--
											<span class="noteclick"> After clicking "Book it" you will be redirected to payment gateway. You must complete the process or the transaction will not occur. </span> 
		-->
		
											<div class="all_oters nb-contents"></div>
		
											<div class="all_oters backblur">
												<div class="para_sentnce">
													<strong><?php echo $this->TravelLights['HotelResult']['CancellationPolicy']; ?> :</strong> <span class="cancellation"> <?php echo $cancel_policy; ?> 
												
													 <?php if($cart_transfer->caneclation_policy != ''){
          $cancellaiton_details = json_decode($cart_transfer->caneclation_policy, true);
        if(isset($cancellaiton_details['cancellation'])){ ?>
		
			<?php for($cancel =0; $cancel < count($cancellaiton_details['cancellation']); $cancel++) { 
				
		   if($cancellaiton_details['cancellation'][$cancel]['Charge'] == 'true') { ?> 
		    
            	<?php echo $this->TravelLights['HotelResult']['Cancellationfrom']; ?> <?php echo date('dS M Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate'])); ?> <?php echo $this->TravelLights['HotelResult']['onwardswillincurcancellationcharge']; ?>  <b><?php echo "AUD ".$cancellaiton_details['cancellation'][$cancel]['ChargeAmount']; ?></b>|
           
            <?php } else { ?>
				
				<?php echo $this->TravelLights['HotelResult']['Nocancellationfeeuntil']; ?> <?php echo date('dS M Y', strtotime($cancellaiton_details['cancellation'][$cancel]['FromDate'])); ?>|
				
		   <?php } ?>
       
        
        <?php } } if(false) { ?>
        <strong><?php echo $this->TravelLights['HotelResult']['AmendmentPolicy']; ?> ; </strong> 
        <?php  if(isset($cancellaiton_details['amendment'])){ ?>
		<ul class="list_popup">
	
			<?php for($cancel =0; $cancel < count($cancellaiton_details['amendment']); $cancel++) { 
				
		   if($cancellaiton_details['amendment'][$cancel]['Charge'] == 'true') { ?> 
		    <li class="listcancel">
            	<?php echo $this->TravelLights['HotelResult']['Amendmentfrom']; ?> <?php echo date('d-m-Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?> <?php echo $this->TravelLights['HotelResult']['onwardswillincurcancellationcharge']; ?>  <b><?php echo "AUD ".$cancellaiton_details['amendment'][$cancel]['ChargeAmount']; ?></b>
            </li>
            <?php } else { ?>
				<li class="listcancel">
				<?php echo $this->TravelLights['HotelResult']['NoAmendmentfeeuntil']; ?> <?php echo date('d-m-Y', strtotime($cancellaiton_details['amendment'][$cancel]['FromDate'])); ?>
				</li>
		   <?php } ?>
        </ul>
        
        <?php } }  } } ?>
        </span>
												</div>
											</div>
											
											<?php
												if($special_notes != '') {
											?>
												<div class="clearfix"></div>
												<div class="all_oters backblur">
													<div class="para_sentnce">
														<strong><?php echo $this->TravelLights['HotelResult']['SpecialInformation']; ?> :</strong><span  class="special_notes"> <?php echo $special_notes; ?> </span>
													</div>
												</div>
											<?php
												}
											?>
											<div class="col-md-12 nopad">
												<div class="checkcontent">
													<div class="squaredThree">
														<input type="checkbox" value="0" name="confirm" class="filter_airline" id="squaredThree1" >
														<label for="squaredThree1"></label>
													</div>
													<label for="squaredThree1" id="indicator" class="lbllbl"><?php echo $this->TravelLights['HotelResult']['Byproceedingfurther']; ?> <a href="<?php echo site_url("dashboard/termsnconditions") ?>" target="_blank"><?php echo $this->TravelLights['HotelResult']['TermsConditions']; ?></a> <?php echo $this->TravelLights['HotelResult']['ofTheChinaGap']; ?> </label>
												</div>
											</div>
											
											<div class="clearfix"></div>
												<div class="payrowsubmt">
													<div class="col-md-3 col-xs-3 fulat500 nopad">
														<input type="submit" class="paysubmit" id="checkout_continue" name="continue"  value="<?php echo $this->TravelLights['HotelResult']['Continue']; ?>"/>
													</div>
												 </div>
											</div> 
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
            </div>
        </div>
        
        <div class="carttoloadr"><strong><?php echo $this->TravelLights['HotelResult']['Confirmingyourhotel']; ?>...</strong></div>
        
        <?php
            echo $this->load->view('core/footer');
            echo $this->load->view('core/bottom_footer');
		?>
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/custom.js"></script> 
        <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
		<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script>
        <script type="text/javascript">
			
			function my_ownvalidation(){}
			
            $(document).ready(function(){
				
				
				
				price_calculation('COMMISSIONABLE');
				
				function checkValidation(key) {
					
					if(key == 1) {
						$("#currencies").css('display','');
						$("#currency_drop").removeClass('open');
						return true;
					}
					else if(key == 2) {
						$("#currencies").css('display','none');
						return true;
					}
					else if(key == 3) {
						$("#currencies").css('display','none');
						
						var flagStorage = [];
						$(".first_name,.last_name").each(function() {
							var response = validateName($(this).val());
							if(response.status == true) {
								if($(this).next().hasClass('error')) {
									$(this).next().remove(".error");
								}
								flagStorage.push(true);
							}
							else {
								if($(this).next().hasClass('error')) {
									$(this).next().remove(".error");
								}
								var error_contents = "<span class='error'>"+response.message+"</span>"
								$(this).after(error_contents);
								flagStorage.push(false);
							}
						});
						$(".c_first_name,.c_last_name").each(function() {
							var response = validateName($(this).val());
							if(response.status == true) {
								if($(this).next().hasClass('error')) {
									$(this).next().remove(".error");
								}
								flagStorage.push(true);
							}
							else {
								if($(this).next().hasClass('error')) {
									$(this).next().remove(".error");
								}
								var error_contents = "<span class='error'>"+response.message+"</span>"
								$(this).after(error_contents);
								flagStorage.push(false);
							}
						});
						$(".child_dob").each(function() {
							var response = isEmpty($(this).val());
							if(response.status == true) {
								if($(this).next().hasClass('error')) {
									$(this).next().remove(".error");
								}
								flagStorage.push(true);
							}
							else {
								if($(this).next().hasClass('error')) {
									$(this).next().remove(".error");
								}
								var error_contents = "<span class='error'>"+response.message+"</span>"
								$(this).after(error_contents);
								flagStorage.push(false);
							}
						});
						if($.inArray(false,flagStorage) === -1) {
							return true;
						}
						else {
							return false;
						}
					}
					else {
						return true;
					}
				}
				function validateName(val) {
					var regex = "^[a-zA-Z\s ]+$";
					if(val.match(regex)) {
						var response = {
										'status': true,
										'message': '',
									};
					}
					else {
						var response = {
										'status': false,
										'message': 'This field is required!',
									};
					}
					return response;
				}
				function isEmpty(val) {
					if(val.trim() != '') {
						var response = {
										'status': true,
										'message': '',
									};
					}
					else {
						var response = {
										'status': false,
										'message': 'This field is required!',
									};
					}
					return response;
				}
				function validateCheckbox(id) {
					if (!jQuery("#"+id).is(":checked")) {
						var response = {
										'status': false,
										'message': 'Before you proceed, do agree to our terms &amp; conditions.',
									};
					}
					else {
						var response = {
										'status': true,
										'message': '',
									};
					}
					return response;
				}
				
            	$('.scrolltop').click(function(){
            		$("html, body").animate({ scrollTop: 0 }, 600);
            	});
				$('#stepbk1').click(function(){
					if(checkValidation(1)) {
						$('.center_pro').removeClass('active');
						$(this).addClass('active');
						$('.bktab2, .bktab3').fadeOut(500,function(){$('.bktab1').fadeIn(500)});
					}
					else {
						
					}
					
				});
				
				$('#stepbk2,#next_review').click(function(){
					if(checkValidation(2)) {
						$('.center_pro').removeClass('active');
						$("#stepbk2").addClass('active');
						$('.bktab1, .bktab3').fadeOut(500,function(){$('.bktab2').fadeIn(500)});
					}
					else {
						
					}
				});
				
				$('#stepbk3,#traveller_btn').click(function(){
					if(checkValidation(3)) {
						$('.center_pro').removeClass('active');
						$('#stepbk3').addClass('active');
						$('.bktab1, .bktab2').fadeOut(500,function(){$('.bktab3').fadeIn(500)});
					}
				}); 
				
				$('.oneselects').click(function(){
					$('.oneselects').removeClass('active');
					$(this).addClass('active');
				});
				
				$('#pay_now').click(function(){
					$('.paylater_show').fadeOut(500, function(){
						$('.pay_show').fadeIn();
					});
					price_calculation('NET');
				});
				$('#pay_later').click(function(){
					$('.pay_show').fadeOut(500, function(){
						$('.paylater_show').fadeIn();
					});
					price_calculation('COMMISSIONABLE');
				});

				$(document).on('click', '.pay_both_1', function() {
					$("#deposit-container-1").fadeIn();
					$(".nb-contents").html('');
					var nb_contents = '<div class="para_sentnce">'+
											'<strong>NB: </strong>'+
											'<span class="">2.63 percentage of transaction fees will be applicable on total amount once you proceed with this booking.</span>'+
										'</div>';
					$(".nb-contents").append(nb_contents);
					price_calculation('NET');
				});
				$(document).on('click', '.pay_both_2', function() {
					$("#deposit-container-2").fadeIn();
					$(".nb-contents").html('');
					var nb_contents = '<div class="para_sentnce">'+
											'<strong>NB: </strong>'+
											'<span class="">2.63 percentage of transaction fees will be applicable on total amount once you proceed with this booking.</span>'+
										'</div>';
					$(".nb-contents").append(nb_contents);
					price_calculation('COMMISSIONABLE');
				});
				$(document).on('click', '.li-option-1', function() {
					$("#deposit-container-1").fadeOut();
					price_calculation('NET');
				});
				$(document).on('click', '.li-option-2', function() {
					$("#deposit-container-2").fadeOut();
					price_calculation('COMMISSIONABLE');
				});
            });
            function process_promo() {
				var module = "<?php echo $module; ?>"; 
				var cart_id = "<?php echo $cid; ?>";
				var promo_code = $("#promo_code").val();
				if(promo_code == ''){
					$('#promo_msg').html("Please enter your Promo Code");	
					$('#promo_msg').addClass('error');
					return false;
				}
				if($( "#pay_later" ).hasClass( "active" )) {
					var payment_logic = "COMMISSIONABLE";
				}
				else if($( "#pay_now" ).hasClass( "active" )) {
					var payment_logic = "NET";
				}
				else {
					var payment_logic = '';
				}
				
				var payment_type = $('input[name=type_of_payment]:checked').val();
				price_calculation(payment_logic,true);
			}
            function process_promo_old(){
				var module = "<?php echo $module; ?>"; 
				var cart_id = "<?php echo $cid; ?>";
				var promo_code = $("#promo_code").val();
				if(promo_code == ''){
					$('#promo_msg').html("Please enter your Promo Code");	
					$('#promo_msg').addClass('error');
					return false;
				}
				
				if($( "#pay_later" ).hasClass( "active" )) {
					var payment_logic = "COMMISSIONABLE";
				}
				else if($( "#pay_now" ).hasClass( "active" )) {
					var payment_logic = "NET";
				}
				else {
					var payment_logic = '';
				}
				
				var payment_type = $('input[name=type_of_payment]:checked').val();
				
				
				$.ajax({
					type: 'POST',
					url: "<?php echo ASSETS.'bookings/process_promo_code' ?>",
					dataType: "json",
					data:{'module':module, "cart_id" : cart_id, "promo_code" : promo_code},
					success: function(data){
						if(data.status == 1){
							$("#promo_wrap").html(data.promo_theme);
							$('#applied_promo_code').val(promo_code);
							var discount = data.discount.toFixed(2);
							$('#promo_price').val(discount);
							$('.promodiscount').html(discount);
							var offered_total = data.offer_amount.toFixed(2);
							$('.promo_total').html(offered_total);
							$('#promo_msg').html(data.msg);
							$('#promo_msg').addClass('success');
							$('#service_charge').val(data.service_charge);
							$('#gst_charge').val(data.gst_charge);
							$('#tax_charge').val(data.tax_charge);
							price_calculation(payment_logic,payment_type);
							
						}else{
							$('#promo_msg').html(data.msg);
							$('#promo_msg').addClass('error');
						}
						return false;
					}
				}); 
				
			}
			function remove_promo() {
				var module = "<?php echo $module; ?>"; 
				var cart_id = "<?php echo $cid; ?>";
				
				if($( "#pay_later" ).hasClass( "active" )) {
					var payment_logic = "COMMISSIONABLE";
				}
				else if($( "#pay_now" ).hasClass( "active" )) {
					var payment_logic = "NET";
				}
				else {
					var payment_logic = '';
				}
				var payment_type = $('input[name=type_of_payment]:checked').val();
				
				$.ajax({
					type: 'POST',
					url: "<?php echo ASSETS.'bookings/clear_promo' ?>",
					dataType: "json",
					data:{'module':module, "cart_id" : cart_id},
					success: function(data){
						if(data.status == 1){
							$("#promo_wrap").html(data.promo_theme);
							$('#promo_msg').html(data.msg);
							$(".discount_wrap").hide();
							price_calculation(payment_logic,payment_type);
						}
						else {
							$('#promo_msg').html(data.msg);
							$('#promo_msg').addClass('error');
						}
					}
				});
			}
            
			function calculate_price(obj) {
				var promo_price = 0;
				if($("#promo_price").val() != ""){
					promo_price  = $("#promo_price").val();
				}
				if(obj == "commissionable") {
					if($("#payment_type_deduction_flag").val() == 'PG') {
						var PG_Price = parseFloat("<?php echo array_sum($commissionable_Total); ?>");
						var PG_Price_Markup_val = 10;
						var only_taxes = parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());;
						var PG_Price_Markup_val_with_taxes = PG_Price_Markup_val + parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());
						PG_Price = ((PG_Price  - promo_price) + only_taxes) + PG_Price_Markup_val;
						$(".sub_total").text(PG_Price);
						$(".total").text(PG_Price);
						$(".single_hotel_price").text(PG_Price);
						$(".taxes").text(PG_Price_Markup_val_with_taxes);
						$("#total_deposit_payble").val(0);
						$("#total_payable").val(PG_Price);
						$("#total_PG_payble").val(PG_Price);
						$("#PG_Charge").val(PG_Price_Markup_val);
						$("#payment_type_flag").val('Commissionable');
					}
					else {
						var PG_Price = 0;
						var PG_Price_Markup_val_with_taxes = PG_Price + parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());
						var total_with_promo = (parseFloat("<?php echo array_sum($commissionable_Total); ?>") - promo_price) + PG_Price_Markup_val_with_taxes ;
						$(".sub_total").text(total_with_promo);
						$(".total").text(total_with_promo);
						$(".single_hotel_price").text(total_with_promo);
						$(".taxes").text(PG_Price_Markup_val_with_taxes);
						if($("#payment_type_deduction_flag").val() == 'PayLater') {
							$("#total_deposit_payble").val(0);
						}
						else {
							$("#total_deposit_payble").val(total_with_promo);
						}
						$("#total_payable").val(total_with_promo);
						$("#total_PG_payble").val(0);
						$("#PG_Charge").val(0);
						$("#payment_type_flag").val('Commissionable');
					}
				}
				else if(obj == "net") {
					if($("#payment_type_deduction_flag").val() == 'PG') {
						var PG_Price = parseFloat("<?php echo array_sum($net_Total); ?>") - promo_price;
						var PG_Price_Markup_val = 10;
						var only_taxes = parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());;
						var PG_Price_Markup_val_with_taxes = PG_Price_Markup_val + parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());
						
						PG_Price = ((PG_Price  - promo_price) + only_taxes) + PG_Price_Markup_val;
						
						$(".sub_total").text(PG_Price);
						$(".total").text(PG_Price);
						$(".single_hotel_price").text(PG_Price);
						$(".taxes").text(PG_Price_Markup_val_with_taxes);
						
						$("#total_deposit_payble").val(0);
						$("#total_payable").val(PG_Price);
						$("#total_PG_payble").val(PG_Price);
						$("#PG_Charge").val(PG_Price_Markup_val);
						$("#payment_type_flag").val('Net');
					}
					else {
						var PG_Price = 0;
						var PG_Price_Markup_val_with_taxes = PG_Price + parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());
						var total_with_promo = (parseFloat("<?php echo array_sum($net_Total); ?>") - promo_price) + PG_Price_Markup_val_with_taxes ;
						$(".sub_total").text(total_with_promo);
						$(".total").text(total_with_promo);
						$(".single_hotel_price").text(total_with_promo);
						$(".taxes").text(PG_Price_Markup_val_with_taxes);
						if($("#payment_type_deduction_flag").val() == 'PayLater') {
							$("#total_deposit_payble").val(0);
						}
						else {
							$("#total_deposit_payble").val(total_with_promo);
						}
						$("#total_payable").val(total_with_promo);
						$("#total_PG_payble").val(0);
						$("#PG_Charge").val(0);
						$("#payment_type_flag").val('Net');
					}
				}
				else {
					var total_with_promo = (parseFloat($(obj).attr('data-total-price'))) - promo_price ;
					$(obj).attr('data-total-price',total_with_promo);
					var PG_Price = (parseFloat($(obj).attr('data-total-price')) - parseFloat(promo_price)) - parseFloat($(obj).val());
					PG_Price = PG_Price.toFixed(2);
					//~ Calculating PG_Price with Markup # Todo 
					var PG_Price_Markup_val = 10;
					
					var only_taxes = parseFloat($("#service_charge").val()) + parseFloat($("#gst_charge").val()) + parseFloat($("#tax_charge").val());;
					var PG_Price_Markup_val_with_taxes = PG_Price_Markup_val + only_taxes;
					
					PG_Price = ((parseFloat(PG_Price)) + parseFloat(only_taxes)) + parseFloat(PG_Price_Markup_val);
					
					var sub_total_price = PG_Price_Markup_val + parseFloat($(obj).attr('data-total-price'));
					var total_price = PG_Price + parseFloat($(obj).val());
					
					$(".taxes").text(PG_Price_Markup_val_with_taxes);
					$(".total").text(total_price);
					$(".single_hotel_price").text(total_price);
					
					$("#total_deposit_payble").val(parseFloat($(obj).val()));
					$("#total_payable").val(total_price);
					$("#total_PG_payble").val(PG_Price);
					$("#PG_Charge").val(PG_Price_Markup_val);
				}
			}
			function isNumber(evt) {
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				return true;
			}
			$('#commissionable_both_payment, #net_both_payment').bind("cut copy paste",function(e) {
				e.preventDefault();
			});
			function price_calculation(flag,promo_flag) {
				if(flag == 'COMMISSIONABLE') {
					
					$("#checkout-apartment").attr('data-flag','COMMISSIONABLE');
					
					var payment_type = $('input[name=type_of_payment]:checked').val();
					if(payment_type == 'BOTH') {
						var deposit_payment = $('#commissionable_both_payment').val();
						if(deposit_payment != '') {}
						else {
							deposit_payment = 0;
						}
					}
					else {
						var deposit_payment = null;
					}
				}
				else if(flag == 'NET') {
					
					$("#checkout-apartment").attr('data-flag','NET');
					
					var payment_type = $('input[name=type_of_payment1]:checked').val();
					if(payment_type == 'BOTH') {
						var deposit_payment = $('#net_both_payment').val();
						if(deposit_payment != '') {}
						else {
							deposit_payment = 0;
						}
					}
					else {
						var deposit_payment = null;
					}
				}
				else {
					return false;
				}
				
				if(promo_flag == true) {
					var promo_signature = 'SET';
					var promo_code = $("#promo_code").val();
				}
				else {
					var promo_signature = 'NOTSET';
					var promo_code = '';
				}
				
				var module = "<?php echo $book_temp_data[0]->product_id; ?>"; 
				var cart_id = "<?php echo $cid; ?>";
				$.ajax({
					type: 'POST',
					url: "<?php echo ASSETS.'booking/calculate_price' ?>",
					dataType: "json",
					data:{'product_id':module, "cart_id" : cart_id, "payment_logic" : flag, "payment_type" : payment_type, "promo_signature" : promo_signature, "promo_code" : promo_code, "deposit_payment" : deposit_payment},
					success: function(data){
						if(data.status == 1){
							$('.sub_total').text(data.sub_total_cost);
							$('.single_hotel_price').text(data.sub_total_cost);
							$('.cancellation').html(data.cancellation_txt);
							if(data.promo_code == '' || data.promo_code == 'null' || data.promo_code == null) {
								$('.discount_wrap').hide();
								$('.promo_discount').text('');
								
								$('#promo_msg').html(data.msg);
								$('#promo_msg').addClass('error');
							}
							else {
								if(data.promo_status == 1) {
									$('#promo_msg').html(data.msg);
									$('#promo_msg').addClass('success');
									$("#promo_wrap").html(data.promo_theme);
									
									$('.discount_wrap').show();
									$('.promo_discount').text(data.discount);
								}
								else {
									$('#promo_msg').html(data.msg);
									$('#promo_msg').addClass('error');
									
									$('.discount_wrap').hide();
									$('.promo_discount').text('0.00');
								}
							}
							if(data.total_tax > 0) {
								$('.taxes').text(data.total_tax);
								$('.tax_fees').show();
							}
							else {
								$('.tax_fees').hide();
							}
							
							
							if(data.payment_type == 'PG' || data.payment_type == 'BOTH') {
								$('.cc_fees').show();
								$('.cc_fees_value').text(data.CC_fees);
								if(data.payment_type == 'BOTH') {
									$('.cc_payment_wrap').show();
									$('.total_cc_payment').text(data.transaction_amount);
									
									$('.deposit_payment_wrap').show();
									$('.total_deposit_payment').text(data.deposit_amount);
								}
								else {
									$('.cc_payment_wrap').hide();
									$('.total_cc_payment').text('');
									
									$('.deposit_payment_wrap').hide();
									$('.total_deposit_payment').text('');
								}
								$(".nb-contents").html('');
								var nb_contents = '<div class="para_sentnce">'+
														'<strong>NB: </strong>'+
														'<span class="">2.63 percentage of transaction fees will be applicable on total amount once you proceed with this booking.</span>'+
													'</div>';
								$(".nb-contents").append(nb_contents);
							}
							else {
								
								$(".nb-contents").html('');
								
								$('.cc_fees_value').text('');
								$('.cc_fees').hide();
								
								$('.cc_payment_wrap').hide();
								$('.total_cc_payment').text('');
								
								$('.deposit_payment_wrap').hide();
								$('.total_deposit_payment').text('');
							}
							$('.total_price').text(data.total_cost);
						}
						else {
							
						}
					}
				});
			}
			
			function change_div(tht, div_id){
				 if(tht.value == 'Adult'){
					 $("#adults"+div_id).css("display","inherit");
					 $("#childs"+div_id).css("display","none");
				 }else{
					  $("#adults"+div_id).css("display","none");
					 $("#childs"+div_id).css("display","inherit");
				 }
			}
        </script>
    </body>
</html>
