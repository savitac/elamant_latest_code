<!--
<body onload="document.payment.submit()">
	<form name="payment" method="post" action="<?php echo $payment_url; ?>">
		<input type="text" name="user_SessionId" value="<?php echo $user_SessionId; ?>">
		<input type="text" name="vpc_AccessCode" value="<?php echo $vpc_AccessCode; ?>">
		<input type="text" name="vpc_Amount" value="<?php echo $vpc_Amount; ?>">
		<input type="text" name="vpc_Command" value="<?php echo $vpc_Command; ?>">
		<input type="text" name="vpc_Currency" value="<?php echo $vpc_Currency; ?>">
		<input type="text" name="vpc_MerchTxnRef" value="<?php echo $vpc_MerchTxnRef; ?>">
		<input type="text" name="vpc_Merchant" value="<?php echo $vpc_Merchant; ?>">
		<input type="text" name="vpc_OrderInfo" value="<?php echo $vpc_OrderInfo; ?>">
		<input type="text" name="vpc_ReturnURL" value="<?php echo $vpc_ReturnURL; ?>">
		<input type="text" name="vpc_Version" value="<?php echo $vpc_Version; ?>">
		<input type="text" name="vpc_SecureHash" value="<?php echo $secureHash; ?>">
		<input type="text" name="vpc_SecureHashType" value="SHA256">
	</form>
</body>
-->

<?php
session_start();
$paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_id='khaled@travellights.net'; // Business email ID
?>

<body onload="do_submit();">
<form action='<?php echo $paypal_url; ?>' method='post'  name="payment_form">
<input type='text' name='business' value='<?php echo $paypal_id; ?>'>
<input type='text' name='cmd' value='_xclick'>
<input type='text' name='item_name' value='Hotel'>
<input type='text' name='item_number' value='<?php echo $parent_pnr; ?>'>
<input type='text' name='amount' value='<?php echo $paypalAmount; ?>'>
<input type='text' name='no_shipping' value='1'>
<input type='text' name='currency_code' value='USD'>
<input type='text' name='cancel_return' value="<?php echo $_SERVER['HTTP_REFERER']; ?>">
<input type='text' name='return' value="<?php echo $return; ?>">
</form> 
<script type="text/javascript">
			function do_submit() {
				document.payment_form.submit();
			}
		</script>
</body>


