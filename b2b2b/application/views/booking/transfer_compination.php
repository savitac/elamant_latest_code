
		    <!-- Port to Airport -->
			<?php if($transfer_request->pickup_code == 'P' && $transfer_request->dropoff_code == 'A') { ?>
		        
		         <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Port City</div>
						<div class="selectedwrap">
							<?php $ports =  $this->General_Model->getPortCities($ttransfer_cart->pickup_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="port_code<?php echo $cid ?>" >
								<?php for($port =0; $port < count($ports); $port++) { ?>
									<option value="<?php echo $ports[$port]->city_code; ?>"> <?php echo $ports[$port]->city_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6 set_margin">
						<div class="paylabel">Ship Name</div>
						<input name="ship_name<?php echo $cid ?>" placeholder="Ship Name" type="text" class="payinput" required />
					</div>
			</div>
			
			<div class="repeatprows">
				    <div class="col-md-6 set_margin">
						<div class="paylabel">Ship Company Name</div>
						<input name="ship_company<?php echo $cid ?>" placeholder="Ship Company Name" type="text" class="payinput" required />
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			</div>
			
			
			<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Airport</div>
						<div class="selectedwrap">
							<?php $airports =  $this->General_Model->getTransferAirports($ttransfer_cart->dropoff_city_code)->result();  ?>
							
							<select class="flpayinput" name="flight_code<?php echo $cid ?>" >
								<?php for($air =0; $air < count($airports); $air++) { ?>
									<option value="<?php echo $airports[$air]->iata_code; ?>"> <?php echo $airports[$air]->airport_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Flight Number</div>
						<input name="flight_number<?php echo $cid ?>" placeholder="Flight Number" type="text" class="payinput" required />
					</div>
					
			</div>
			 
			 
				<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										  
										for($m=0; $m<46;){  
										  if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m; ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			</div>
			  
			<?php } ?>
			
		
			 <!-- Port to Accomodation -->
			<?php if($transfer_request->pickup_code == 'P' && $transfer_request->dropoff_code == 'H') { ?>
				<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Port City</div>
						<div class="selectedwrap">
							<?php $ports =  $this->General_Model->getPortCities($ttransfer_cart->pickup_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="port_code<?php echo $cid ?>" >
								<?php for($port =0; $port < count($ports); $port++) { ?>
									<option value="<?php echo $ports[$port]->city_code; ?>"> <?php echo $ports[$port]->city_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6 set_margin">
						<div class="paylabel">Ship Name</div>
						<input name="ship_name<?php echo $cid ?>" placeholder="Ship Name" type="text" class="payinput"  required />
					</div>
			</div>
			
			<div class="repeatprows">
				    <div class="col-md-6 set_margin">
						<div class="paylabel">Ship Company Name</div>
						<input name="ship_company<?php echo $cid ?>" placeholder="Ship Company Name" type="text" class="payinput" required />
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			 <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
			 </div>
			</div>
				
			<?php } ?>
			
			
			 <!-- Port to Station -->
			<?php if($transfer_request->pickup_code == 'P' && $transfer_request->dropoff_code == 'S') { ?>
				<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Port City</div>
						<div class="selectedwrap">
							<?php $ports =  $this->General_Model->getPortCities($ttransfer_cart->pickup_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="port_code<?php echo $cid ?>" >
								<?php for($port =0; $port < count($ports); $port++) { ?>
									<option value="<?php echo $ports[$port]->city_code; ?>"> <?php echo $ports[$port]->city_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6 set_margin">
						<div class="paylabel">Ship Name</div>
						<input name="ship_name<?php echo $cid ?>" placeholder="Ship Name" type="text" class="payinput" />
					</div>
			</div>
			
			<div class="repeatprows">
				    <div class="col-md-6 set_margin">
						<div class="paylabel">Ship Company Name</div>
						<input name="ship_company<?php echo $cid ?>" placeholder="Ship Company Name" type="text" class="payinput" required />
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Station Name</div>
						<div class="selectedwrap">
							<?php $stations =  $this->General_Model->getStationsDetails($ttransfer_cart->dropoff_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="station_code<?php echo $cid ?>" >
								<?php for($station =0; $station < count($stations); $station++) { ?>
									<option value="<?php echo $stations[$station]->station_code; ?>"> <?php echo $stations[$station]->station_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					  
			     <div class="col-md-6 set_margin">
						<div class="paylabel">Train Name</div>
						<input name="train_name<?php echo $cid ?>" id="train_name<?php echo $cid ?>" placeholder="Train Name" type="text" class="payinput"  required />
				   </div>
				   
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
			</div>
				
			<?php } ?>
			
			
			 <!-- Port to Port -->
			<?php if($transfer_request->pickup_code == 'P' && $transfer_request->dropoff_code == 'P') { ?>
				<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Pickup Port City</div>
						<div class="selectedwrap">
							<?php $ports =  $this->General_Model->getPortCities($ttransfer_cart->pickup_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="port_code<?php echo $cid ?>" >
								<?php for($port =0; $port < count($ports); $port++) { ?>
									<option value="<?php echo $ports[$port]->city_code; ?>"> <?php echo $ports[$port]->city_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6 set_margin">
						<div class="paylabel">Ship Name</div>
						<input name="ship_name<?php echo $cid ?>" placeholder="Ship Name" type="text" class="payinput" required />
					</div>
			</div>
			
			<div class="repeatprows">
				    <div class="col-md-6 set_margin">
						<div class="paylabel">Ship Company Name</div>
						<input name="ship_company<?php echo $cid ?>" placeholder="Ship Company Name" type="text" class="payinput" required />
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
			
				<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Dropoff Port City</div>
						<div class="selectedwrap">
							<?php $ports =  $this->General_Model->getPortCities($ttransfer_cart->dropoff_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="port_code1<?php echo $cid ?>" >
								<?php for($port =0; $port < count($ports); $port++) { ?>
									<option value="<?php echo $ports[$port]->city_code; ?>"> <?php echo $ports[$port]->city_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6 set_margin">
						<div class="paylabel">Ship Name</div>
						<input name="ship_name1<?php echo $cid ?>" placeholder="Ship Name" type="text" class="payinput"  required />
					</div>
			</div>
			
			<div class="repeatprows">
				    <div class="col-md-6 set_margin">
						<div class="paylabel">Ship Company Name</div>
						<input name="ship_company1<?php echo $cid ?>" placeholder="Ship Company Name" type="text" class="payinput" required />
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
				
			<?php } ?>
			
			
			 <!-- AirPort to Accomodation -->
			<?php if($transfer_request->pickup_code == 'A' && $transfer_request->dropoff_code == 'H') { ?>
			<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Airport</div>
						<div class="selectedwrap">
							 
							<?php  $airports =  $this->General_Model->getTransferAirports($ttransfer_cart->pickup_city_code)->result();  ?>
							
							<select class="flpayinput" name="flight_code<?php echo $cid ?>" >
								<?php for($air =0; $air < count($airports); $air++) { ?>
									<option value="<?php echo $airports[$air]->iata_code; ?>"> <?php echo $airports[$air]->airport_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Flight Number</div>
						<input name="flight_number<?php echo $cid ?>" placeholder="Flight Number" type="text" class="payinput" required />
					</div>
					
			</div>
			
			
			
			<div class="repeatprows">
				
				<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Hotel Name</div>
						<div class="selectedwrap">
							<?php  $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
			</div>
			<?php } ?>
			
			 <!-- AirPort to Port -->
			<?php if($transfer_request->pickup_code == 'A' && $transfer_request->dropoff_code == 'P') { ?>
		        
		        
		        <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Airport</div>
						<div class="selectedwrap">
							<?php $airports =  $this->General_Model->getTransferAirports($ttransfer_cart->pickup_city_code)->result();  ?>
							
							<select class="flpayinput" name="flight_code<?php echo $cid ?>" >
								<?php for($air =0; $air < count($airports); $air++) { ?>
									<option value="<?php echo $airports[$air]->iata_code; ?>"> <?php echo $airports[$air]->airport_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Flight Number</div>
						<input name="flight_number<?php echo $cid ?>" placeholder="Flight Number" type="text" class="payinput" required />
					</div>
					
			</div>
			 
			 
				<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput"  name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										  
										for($m=0; $m<46;){  
										  if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m; ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			</div>
			
		         <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Port City</div>
						<div class="selectedwrap">
							<?php $ports =  $this->General_Model->getPortCities($ttransfer_cart->dropoff_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="port_code<?php echo $cid ?>" >
								<?php for($port =0; $port < count($ports); $port++) { ?>
									<option value="<?php echo $ports[$port]->city_code; ?>"> <?php echo $ports[$port]->city_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6 set_margin">
						<div class="paylabel">Ship Name</div>
						<input name="ship_name<?php echo $cid ?>" placeholder="Ship Name" type="text" class="payinput"  required />
					</div>
			</div>
			
			<div class="repeatprows">
				    <div class="col-md-6 set_margin">
						<div class="paylabel">Ship Company Name</div>
						<input name="ship_company<?php echo $cid ?>" placeholder="Ship Company Name" type="text" class="payinput" required />
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			</div>
			<?php } ?>
		
			 <!-- AirPort to Station -->
			<?php if($transfer_request->pickup_code == 'A' && $transfer_request->dropoff_code == 'S') { ?>
		        
		        
		        <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Airport</div>
						<div class="selectedwrap">
							<?php $airports =  $this->General_Model->getTransferAirports($ttransfer_cart->pickup_city_code)->result();  ?>
							
							<select class="flpayinput" name="flight_code<?php echo $cid ?>" >
								<?php for($air =0; $air < count($airports); $air++) { ?>
									<option value="<?php echo $airports[$air]->iata_code; ?>"> <?php echo $airports[$air]->airport_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Flight Number</div>
						<input name="flight_number<?php echo $cid ?>" placeholder="Flight Number" type="text" class="payinput" required />
					</div>
					
			</div>
			 
				<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput"  name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										  
										for($m=0; $m<46;){  
										  if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m; ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			</div>
			
			<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Station Name</div>
						<div class="selectedwrap">
							<?php $stations =  $this->General_Model->getStationsDetails($ttransfer_cart->dropoff_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="station_code<?php echo $cid ?>" >
								<?php for($station =0; $station < count($stations); $station++) { ?>
									<option value="<?php echo $stations[$station]->station_code; ?>"> <?php echo $stations[$station]->station_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					  
			     <div class="col-md-6 set_margin">
						<div class="paylabel">Train Name</div>
						<input name="train_name<?php echo $cid ?>" id="train_name<?php echo $cid ?>" placeholder="Train Name" type="text" class="payinput" required />
				   </div>
				   
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
		
		     <?php } ?>
			
			<!-- AirPort to Airport -->
			<?php if($transfer_request->pickup_code == 'A' && $transfer_request->dropoff_code == 'A') { ?>
		        <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Airport</div>
						<div class="selectedwrap">
							<?php $airports =  $this->General_Model->getTransferAirports($ttransfer_cart->pickup_city_code)->result();  ?>
							
							<select class="flpayinput" name="flight_code<?php echo $cid ?>" >
								<?php for($air =0; $air < count($airports); $air++) { ?>
									<option value="<?php echo $airports[$air]->iata_code; ?>"> <?php echo $airports[$air]->airport_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Flight Number</div>
						<input name="flight_number<?php echo $cid ?>" placeholder="Flight Number" type="text" class="payinput" required />
					</div>
					
			</div>
			 
			 <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput"  name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										  
										for($m=0; $m<46;){  
										  if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m; ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			</div>
			
			
			        <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Airport</div>
						<div class="selectedwrap">
							<?php $airports =  $this->General_Model->getTransferAirports($ttransfer_cart->dropoff_city_code)->result();  ?>
							
							<select class="flpayinput" name="flight_code1<?php echo $cid ?>" >
								<?php for($air =0; $air < count($airports); $air++) { ?>
									<option value="<?php echo $airports[$air]->iata_code; ?>"> <?php echo $airports[$air]->airport_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Flight Number</div>
						<input name="flight_number1<?php echo $cid ?>" placeholder="Flight Number" type="text" class="payinput" required />
					</div>
					
			</div>
			 
			 
				<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput"  name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										  
										for($m=0; $m<46;){  
										  if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m; ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			</div>
		

		     <?php } ?>
		     
			 <!-- Accomodation to AirPort -->
			<?php if($transfer_request->pickup_code == 'H' && $transfer_request->dropoff_code == 'A') { ?>
				
		   <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
			
					<div class="col-md-6 set_margin">
						<div class="paylabel">Starting Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput"  name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										  
										for($m=0; $m<46;){  
										  if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m; ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			
			</div>
				<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Airport</div>
						<div class="selectedwrap">
							<?php $airports =  $this->General_Model->getTransferAirports($ttransfer_cart->dropoff_city_code)->result();  ?>
							<select class="flpayinput" name="flight_code<?php echo $cid ?>" >
								<?php for($air =0; $air < count($airports); $air++) { ?>
									<option value="<?php echo $airports[$air]->iata_code; ?>"> <?php echo $airports[$air]->airport_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Flight Number</div>
						<input name="flight_number<?php echo $cid ?>" placeholder="Flight Number" type="text" class="payinput" required />
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			</div>
			
			
			<?php } ?>
			
			 <!-- Accomodation to Port -->
			<?php if($transfer_request->pickup_code == 'H' && $transfer_request->dropoff_code == 'P') { ?>
				
		   <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
			
					<div class="col-md-6 set_margin">
						<div class="paylabel">Starting Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput"  name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										  
										for($m=0; $m<46;){  
										  if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m; ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			
			</div>
			
			
				  <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Port City</div>
						<div class="selectedwrap">
							<?php $ports =  $this->General_Model->getPortCities($transfer_request->dropoff_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="port_code<?php echo $cid ?>" >
								<?php for($port =0; $port < count($ports); $port++) { ?>
									<option value="<?php echo $ports[$port]->city_code; ?>"> <?php echo $ports[$port]->city_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6 set_margin">
						<div class="paylabel">Ship Name</div>
						<input name="ship_name<?php echo $cid ?>" placeholder="Ship Name" type="text" class="payinput" required />
				   </div>
			</div>
			
			<div class="repeatprows">
				    <div class="col-md-6 set_margin">
						<div class="paylabel">Ship Company Name</div>
						<input name="ship_company<?php echo $cid ?>" placeholder="Ship Company Name" type="text" class="payinput" required />
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			</div>
			
			<?php } ?>
			
				 <!-- Accomodation to Staton -->
			<?php if($transfer_request->pickup_code == 'H' && $transfer_request->dropoff_code == 'S') { ?>
				
		   <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
			
					<div class="col-md-6 set_margin">
						<div class="paylabel">Starting Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput"  name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										  
										for($m=0; $m<46;){  
										  if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m; ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			
			</div>
			
			
			<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Station Name</div>
						<div class="selectedwrap">
							<?php $stations =  $this->General_Model->getStationsDetails($ttransfer_cart->dropoff_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="station_code<?php echo $cid ?>" >
								<?php for($station =0; $station < count($stations); $station++) { ?>
									<option value="<?php echo $stations[$station]->station_code; ?>"> <?php echo $stations[$station]->station_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					  
			     <div class="col-md-6 set_margin">
						<div class="paylabel">Train Name</div>
						<input name="train_name<?php echo $cid ?>" id="train_name<?php echo $cid ?>" placeholder="Train Name" type="text" class="payinput" required />
				   </div>
				   
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
		<?php } ?>
		
			 <!-- Accomodation to Accomodation -->
			<?php if($transfer_request->pickup_code == 'H' && $transfer_request->dropoff_code == 'H') { ?>
				
		   <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
			
					<div class="col-md-6 set_margin">
						<div class="paylabel">Starting Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput"  name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										  
										for($m=0; $m<46;){  
										  if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m; ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			
			</div>
			
		
		     <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code1<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
			
			</div>
		
		<?php } ?>

			 <!-- Station to Accomodation -->
			<?php if($transfer_request->pickup_code == 'S' && $transfer_request->dropoff_code == 'H') { ?>
				
			  <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Station Name</div>
						<div class="selectedwrap">
							<?php $stations =  $this->General_Model->getStationsDetails($transfer_request->transfer_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="station_code<?php echo $cid ?>" >
								<?php for($station =0; $station < count($stations); $station++) { ?>
									<option value="<?php echo $stations[$station]->station_code; ?>"> <?php echo $stations[$station]->station_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					  
			     <div class="col-md-6 set_margin">
						<div class="paylabel">Train Name</div>
						<input name="train_name<?php echo $cid ?>" id="train_name<?php echo $cid ?>" placeholder="Train Name" type="text" class="payinput" required />
				   </div>
				   
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
			<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" id="hotel_code<?php echo $cid ?>" name="hotel_code<?php echo $cid ?>" onchange="getHotelAddress(this.value, '<?php echo $transfer_request->transfer_city_code; ?>')" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
			     
			     <div class="col-md-6 set_margin">
						<div class="paylabel">Address Line 1</div>
						<input name="address1<?php echo $cid ?>" id="address1<?php echo $cid ?>" placeholder="Address Line 1" type="text" class="payinput" required />
				   </div>
				   
				   <div class="col-md-6 set_margin">
						<div class="paylabel">Address Line 2</div>
						<input name="address2<?php echo $cid ?>" id="address2<?php echo $cid ?>" placeholder="Address Line 2" type="text" class="payinput"  required />
				   </div>
				   
				   <div class="col-md-6 set_margin">
						<div class="paylabel">City</div>
						<input name="city<?php echo $cid ?>" id="city<?php echo $cid ?>" placeholder="City" type="text" class="payinput" required />
				   </div>
				   
				   <div class="col-md-6 set_margin">
						<div class="paylabel">Telephone</div>
						<input name="telephone<?php echo $cid ?>" id="telephone<?php echo $cid ?>" placeholder="Telephone" type="text" class="payinput" required />
				   </div>
					
			</div>
		
	    <?php } ?>
	    
	     <!-- Station to Airport -->
			<?php if($transfer_request->pickup_code == 'S' && $transfer_request->dropoff_code == 'A') { ?>
				
			  <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Station Name</div>
						<div class="selectedwrap">
							<?php $stations =  $this->General_Model->getStationsDetails($transfer_request->transfer_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="station_code<?php echo $cid ?>" >
								<?php for($station =0; $station < count($stations); $station++) { ?>
									<option value="<?php echo $stations[$station]->station_code; ?>"> <?php echo $stations[$station]->station_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					  
			     <div class="col-md-6 set_margin">
						<div class="paylabel">Train Name</div>
						<input name="train_name<?php echo $cid ?>" id="train_name<?php echo $cid ?>" placeholder="Train Name" type="text" class="payinput" required />
				   </div>
				   
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
			
				<div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Airport</div>
						<div class="selectedwrap">
							<?php $airports =  $this->General_Model->getTransferAirports($transfer_request->transfer_city_code)->result();  ?>
							
							<select class="flpayinput" name="flight_code<?php echo $cid ?>" >
								<?php for($air =0; $air < count($airports); $air++) { ?>
									<option value="<?php echo $airports[$air]->iata_code; ?>"> <?php echo $airports[$air]->airport_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Flight Number</div>
						<input name="flight_number<?php echo $cid ?>" placeholder="Flight Number" type="text" class="payinput" required />
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
		  <?php } ?>
		  
		    <!-- Station to Port -->
			<?php if($transfer_request->pickup_code == 'S' && $transfer_request->dropoff_code == 'P') { ?>
				
			  <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Station Name</div>
						<div class="selectedwrap">
							<?php $stations =  $this->General_Model->getStationsDetails($transfer_request->transfer_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="station_code<?php echo $cid ?>" >
								<?php for($station =0; $station < count($stations); $station++) { ?>
									<option value="<?php echo $stations[$station]->station_code; ?>"> <?php echo $stations[$station]->station_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					  
			     <div class="col-md-6 set_margin">
						<div class="paylabel">Train Name</div>
						<input name="train_name<?php echo $cid ?>" id="train_name<?php echo $cid ?>" placeholder="Train Name" type="text" class="payinput" required />
				   </div>
				   
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
			
				  <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Port City</div>
						<div class="selectedwrap">
							<?php $ports =  $this->General_Model->getPortCities($transfer_request->transfer_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="port_code<?php echo $cid ?>" >
								<?php for($port =0; $port < count($ports); $port++) { ?>
									<option value="<?php echo $ports[$port]->city_code; ?>"> <?php echo $ports[$port]->city_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6 set_margin">
						<div class="paylabel">Ship Name</div>
						<input name="ship_name<?php echo $cid ?>" placeholder="Ship Name" type="text" class="payinput" required />
				   </div>
			</div>
			
			<div class="repeatprows">
				    <div class="col-md-6 set_margin">
						<div class="paylabel">Ship Company Name</div>
						<input name="ship_company<?php echo $cid ?>" placeholder="Ship Company Name" type="text" class="payinput" required />
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
					
			</div>
			<?php } ?>
			
			
			 <!-- Station to Station -->
			<?php if($transfer_request->pickup_code == 'S' && $transfer_request->dropoff_code == 'S') { ?>
				
			  <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Station Name</div>
						<div class="selectedwrap">
							<?php $stations =  $this->General_Model->getStationsDetails($transfer_request->transfer_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="station_code<?php echo $cid ?>" >
								<?php for($station =0; $station < count($stations); $station++) { ?>
									<option value="<?php echo $stations[$station]->station_code; ?>"> <?php echo $stations[$station]->station_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					  
			     <div class="col-md-6 set_margin">
						<div class="paylabel">Train Name</div>
						<input name="train_name<?php echo $cid ?>" id="train_name<?php echo $cid ?>" placeholder="Train Name" type="text" class="payinput" required />
				   </div>
				   
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Arrival Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
			
				  <div class="repeatprows">
					<div class="col-md-6 set_margin">
						<div class="paylabel">Station Name</div>
						<div class="selectedwrap">
							<?php $stations =  $this->General_Model->getStationsDetails($transfer_request->transfer_city_code)->result(); 
							 ?>
							<select class="flpayinput" name="station_code1<?php echo $cid ?>" >
								<?php for($station =0; $station < count($stations); $station++) { ?>
									<option value="<?php echo $stations[$station]->station_code; ?>"> <?php echo $stations[$station]->station_name; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					  
			     <div class="col-md-6 set_margin">
						<div class="paylabel">Train Name</div>
						<input name="train_name1<?php echo $cid ?>" id="train_name1<?php echo $cid ?>" placeholder="Train Name" type="text" class="payinput" required />
				   </div>
				   
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Departure Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="departure_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h <= 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<=9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			</div>
			<?php } ?>
			
			 <!-- Other to Accomodation -->
			<?php if($transfer_request->pickup_code == 'O' && $transfer_request->dropoff_code == 'H') { ?>
			<div class="repeatprows">
				
					<div class="col-md-6 set_margin">
						<div class="paylabel">Pickup Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Pickup Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			 </div>
				
	
			
			<div class="repeatprows">
		
					<div class="col-md-6 set_margin">
						<div class="paylabel">Deopoff Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code1<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
			</div>
			<?php } ?>
			
				 <!-- Other to Airport -->
			<?php if($transfer_request->pickup_code == 'O' && $transfer_request->dropoff_code == 'H') { ?>
			<div class="repeatprows">
				
					<div class="col-md-6 set_margin">
						<div class="paylabel">Pickup Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-md-6 set_margin">
						<div class="paylabel">Pickup Time</div>
						<div class="selectedwrap">
							
							<select class="flpayinput" name="arrival_time<?php echo $cid ?>" >
								      <?php for ($h=0; $h<24; $h++){
										for($m=0; $m<46;){  
										   if($h < 9) $h1 = '0'.$h ; else $h1 = $h ;
										  if($m<9) $m = '0'.$m; else  $m = $m; 
										  if($h==0 && $m==0) $m = $m+15;  $tme= $h.':'.$m;
										  ?>
										<option value="<?php echo $h1.'.'.$m; ?>" ><?php echo $h1.':'.$m; ?></option>
									  <?php $m=$m+15; } } ?>
							</select>
						</div>
					</div>
			 </div>
				
	
			
			<div class="repeatprows">
		
					<div class="col-md-6 set_margin">
						<div class="paylabel">Deopoff Hotel Name</div>
						<div class="selectedwrap">
							<?php $hotels =  $this->General_Model->getTransferHotels($transfer_request->transfer_city_code)->result(); ?>
							<select class="flpayinput" name="hotel_code1<?php echo $cid ?>" >
								<?php for($hot =0; $hot < count($hotels); $hot++) { ?>
									<option value="<?php echo $hotels[$hot]->HotelCode; ?>"> <?php echo $hotels[$hot]->HotelName; ?> </option> 
								<?php } ?>
							</select>
						</div>
					</div>
			</div>
			<?php } ?>
			
			
<script>
	  
	  var city_code =  "<?php echo $transfer_request->transfer_city_code; ?>";
	 $(document).ready(function(){
		 var hotelcode = "hotel_code<?php echo $cid ?>";
		 if($("#"+hotelcode).val() !=''){
			 getHotelAddress($("#hotel_code<?php echo $cid ?>").val(), city_code)
		 }
	 });
	
	
 function getHotelAddress(hotel_code, city_code){
	  $.ajax({
			type: "POST",
			url: "<?php echo base_url().'transfer/get_hotel_address/'; ?>"+hotel_code+"/"+city_code,
			dataType: "json",
			success: function(data){
				$('#address1<?php echo $cid; ?>').val('');
				$('#address2<?php echo $cid; ?>').val('');
				$('#city<?php echo $cid; ?>').val('');
				$('#telephone<?php echo $cid; ?>').val('');
			    $('#address1<?php echo $cid; ?>').val(data.address1);
				$('#address2<?php echo $cid; ?>').val(data.address2);
				$('#city<?php echo $cid; ?>').val(data.city);
				$('#telephone<?php echo $cid; ?>').val(data.telephone);
			
				return false;
			}
		});
 } 

</script>
