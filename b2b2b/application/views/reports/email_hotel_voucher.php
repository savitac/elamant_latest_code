<!-- Enquiry Mail Reply  starts-->
<div class="modal fade" id="email_reply_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">  
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-envelope-o"></i>
			Enquiry Reply
		</h4>
      </div>
      <div class="modal-body">	
		<form action="<?php echo site_url(); ?>index.php/voucher/hotel_email/" method="POST" id="send_voucher">
		To: <input type="email" name="email" id="recipient_email" class="form-control" required="required" placeholder="Enter Email">
		<input type="hidden" name="app_reference" id="app_reference">
		<input type="hidden" name="booking_source" id="booking_src">
		<input type="hidden" name="status" id="status">
		<br>		<div class="col-md-4">
		<br>
		<input type="submit" value="SEND" class="btn btn-success">
		</div>
		<div class="col-md-8">
			<strong id="email_reply_error_message" class="text-danger"></strong>
		</div>
		</form>
		</div> 
      </div>
    </div>
  </div>
</div>
<!-- Enquiry Mail Reply  ends-->