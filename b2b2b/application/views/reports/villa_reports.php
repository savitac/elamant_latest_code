<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title><?= $this->session->userdata('company_name')?></title>
<?php echo $this->load->view('core/load_css'); ?>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="
https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<link href="<?php echo ASSETS;?>assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Navigation -->

<?php echo $this->load->view('dashboard/top'); ?> 
<?php echo $this->load->view('reports/email_hotel_voucher');?>

<!-- /Navigation -->

<section id="main-content"> 
  <section class="wrapper">
    <div class="main-chart">
      <div class="rowit">
        <div class="top_booking_info">
          <a class="col-sm-1 box_main" href="<?php echo base_url().'booking/flightOrders'; ?>">
          Flight Report
          </a>

          <a class="col-sm-2 box_main" href="<?php echo base_url().'booking/hotelOrders'; ?>">Hotel Report</a>

          <a class="col-sm-2 box_main" href="<?php echo base_url().'booking/hotelcrsOrders'; ?>">
          HotelCRS Report
          </a>

          <a class="col-sm-2 box_main">
          VILLA REPORT 
          </a>

          <a class="col-sm-2 box_main" href="<?php echo base_url().'booking/activityOrders'; ?>">
          Activity Report 
          </a>

          <a class="col-sm-2 box_main" href="<?php echo base_url().'booking/busOrders'; ?>">
          Bus Report 
          </a>

          <a class="col-sm-1 box_main" href="<?php echo base_url().'booking/transferOrders'; ?>">
          Transfer Report 
          </a>      
        </div>
      </div>                        
    </div>

<div class="clearfix"></div>

    <div class="main-chart">
      <span class="profile_head"><center>VILLA REPORT</center></span>
        <div class="rowit">
          <div class="top_booking_info">
            <div style="overflow">
                <form name="resetpwd" style="margin:0;" id="refineSearch" method="GET" >
                    <div class="col-md-4 padding10">
                        <div class="ritpul"> 
                            <div class="rowput">
                               <label>Application Reference</label>
                              <input type="text" class="form-control" name="app_reference" value="<?php echo @$_GET['app_reference']; ?>" placeholder="Application Reference">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding10">
                      <div class="ritpul"> 
                        <div class="rowput">
                          <label>
                              Booked From Date
                              </label>
                              <input type="text" readonly id="created_datetime_from" class="form-control" name="created_datetime_from" value="<?=@$_GET['created_datetime_from']?>" placeholder="Request Date">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 padding10">
                      <div class="ritpul"> 
                        <div class="rowput">
                          <label>
                              Booked To Date
                              </label>
                              <input type="text" readonly id="created_datetime_to" class="form-control disable-date-auto-update" name="created_datetime_to" value="<?=@$_GET['created_datetime_to']?>" placeholder="Request Date">
                        </div>
                      </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col-md-4 padding10">
                      <div class="ritpul"> 
                        <div class="rowput">
                          <label>Booking Status</label>
                          <select class="form-control logpadding" name="status" id="bookingstatus" value="<?=@$_GET['status']?>">
                            <option value=""> Select Booking Status</option>
                            <option value="CONFIRMED">CONFIRMED</option>
                            <option value="BOOKING_INPROGRESS">PROCESS</option>
                            <option value="PENDING">PENDING</option> 
                            <option value="CANCELLED">CANCELLED</option>
                            <option value="FAILED">FAILED</option>
                          </select> 
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 padding10">
                      <div class="ritpul"> 
                        <div class="rowput">
                          <label>PNR</label>
                         <input type="text" class="form-control" name="pnr" value="<?=@$_GET['pnr']?>" placeholder="PNR">
                        </div>
                      </div>
                    </div>
                   <!--  <div class="col-md-4 padding10">
                      <div class="ritpul"> 
                        <div class="rowput">
                          Email
                          <input class="form-control" id="email" type="text" name="email" placeholder="Enter Email"/>
                        </div>
                      </div>
                    </div> -->
                    <!--User Type-->  
                    <!--Test-->
                    <div class="col-md-4 padding10">
                      <div class="ritpul"> 
                        <div class="rowput">
                          <div class="clearfix"></div>
                          <br />
                          <button type="submit" class="btn btn-primary">Search</button> 
                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                          <a href="<?php echo base_url().'index.php/booking/hotelOrders? '?>" id="clear-filter" class="btn btn-primary">Clear Filter</a>
                        </div>
                      </div>
                    </div> 
                </form>
            </div>
          </div>

          <div class="clearfix"></div>
<div id="toggleOrdersLoader" class="lodrefrentrev" style="display: none;">
   <div class="centerload"></div>
</div>
<div class="clearfix"></div>

  <div class="row" style="overflow-x: scroll;">
    <p>Export the table to CSV: <button id="btnExport" onclick="javascript:xport.toCSV('booking_list');"> Export</button>
    </p> 
      <table class="table table-bordered datatable" id="booking_list">
        <thead>
          <tr>
            <th>Sl.No </th> 
            <th>Reference No</th> 
            <th>Name</th> 
            <th>Email</th>
            <th>Contact.no</th> 
            <th>Booking Date</th>       
            <th>Currency</th>
            <th>Total Fare</th>
            <th>Admin Markup</th>
            <th>PNR No</th>
            <th>Booking Status</th>      
            <th>Actions</th>               
          </tr>  
        </thead>
        <tbody>
          <?php                       
            if(!empty($orders))
            {     
              $c=1;   
              for ($i=0; $i < count($orders); $i++){ 
                  $total_price = $orders[$i]['total_room_price'] + $orders[$i]['admin_markup'];
                  $request = json_decode($orders[$i]['request'],true);
                  $booking_source = $request['booking_source'];
                  //debug($orders);die;               
              ?>
              <tr>
                <td><?php echo $c++; ?></td>
                <td><?php echo $orders[$i]['ref_id']; ?></td>
                <td><?php echo $orders[$i]['contact_fname'] .' '. $orders[$i]['contact_sur_name']; ?></td>
                <td><?php echo $orders[$i]['contact_email']; ?></td>
                <td><?php echo $orders[$i]['contact_mobile_number']; ?></td>
                <td><?php echo $orders[$i]['book_date']; ?></td>    
                <td><?php echo $orders[$i]['payment_currency']; ?></td> 
                <td><?php echo $total_price; ?></td>  
                <td><?php echo $orders[$i]['admin_markup']; ?></td>
                <td><?php echo $orders[$i]['pnr_no']; ?></td>
                <td><?php echo $orders[$i]['booking_status']; ?></td>
                <td>

                  <?php 
                  $app_reference = $orders[$i]['parent_pnr'];
                  $booking_source = $booking_source;
                  $status = $orders[$i]['booking_status'];
                  ?>

                  <a href=" <?php echo site_url(); ?>index.php/voucher/hotels/<?php echo $app_reference.'/'.$booking_source.'/'.$status; ?>/show_voucher" target="_blank" class="btn btn-blue btn-sm btn-icon icon-left"><i class="glyphicon glyphicon-list-alt"></i>View Voucher</a>
                  <a data-recipient_email="<?php echo $orders[$i]['contact_email']; ?>" data-app_refrence="<?php echo $app_reference;?>"   
                  data-status="<?php echo $status;?>" data-booking_src="<?php echo $booking_source ?>" data-url="<?php echo site_url(); ?>index.php/voucher/hotelcrs_email/villa" class="btn btn-green btn-sm btn-icon icon-left reply-button"><i class="glyphicon glyphicon-send"></i>Send Voucher</a>

                  <?php if (!empty($orders[$i]['payment_res'])) { ?>
                    <a target = '_blank' href="<?php echo site_url(); ?>booking/view_payment_response/<?php  echo base64_encode(json_encode($orders[$i]->parent_pnr)); ?>" class="btn btn-blue btn-sm btn-icon icon-left cancelBooking"><i class="glyphicon glyphicon-list-alt"></i>Payment Response</a>
                  <?php }?>
                </td>  
              </tr> 
            <?php
              }
            } else { ?>
              <tr>
                <td colspan="15" align="center"> <b>Sorry! No Result Found.</b> </td>
              </tr>
            <?php } ?>
        </tbody>
      </table>
  </div>
</div>
</div>                        
</div>



  </section>

</section>


<!-- /.container --> 


<!-- <script type="text/javascript">
    jQuery(document).ready(function($)
    {
      var table = $("#booking_list").dataTable({
        "sPaginationType": "bootstrap",
        "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
        "oTableTools": {
        },
      });
      table.columnFilter({
        "sPlaceHolder" : "head:after"
      });
    }); 
    jQuery(document).ready(function($)
    {
      var table = $("#booking_listttt").dataTable({
        "sPaginationType": "bootstrap",
        "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
        "oTableTools": {
        },
      });
      table.columnFilter({
        "sPlaceHolder" : "head:after"
      });
    });   
  </script> -->
<script type="text/javascript">
  $(document).ready(function(){
    $('.reply-button').on('click', function(e) {
      $("#email_reply_modal").modal('show');
        email = $(this).data('recipient_email');
        app_reference=$(this).data('app_refrence');
        booking_source=$(this).data('booking_src');
        status=$(this).data('status');
        url=$(this).data('url');
        $("#recipient_email").val(email);
        $("#app_reference").val(app_reference);
        $("#status").val(status);  
        $("#booking_src").val(booking_source);
        $('#send_voucher').attr('action', url);
    }); 
  }); 
</script>

<script>
  $(document).ready(function(){
    $('#created_datetime_from').datepicker({
      numberOfMonths: 2,
      dateFormat: 'dd-mm-yy'
    });

    $('#created_datetime_to').datepicker({
      numberOfMonths: 2,
      dateFormat: 'dd-mm-yy'
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#booking_list').DataTable();
} );
</script>

<script type="text/javascript">
  var xport = {
  _fallbacktoCSV: true,  
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) { 
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
 }; 
</script>
<?php echo  $this->load->view('core/bottom_footer'); ?>
</body>
</html>
