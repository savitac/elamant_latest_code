<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Elaman Travel</title>
    <link href=images/fav-icon.png rel="shortcut icon" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap&subset=devanagari,latin-ext" rel="stylesheet">
    <script src="https://kit.fontawesome.com/059a8d99db.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/homepage/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/homepage/css/bootstrap-select.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/homepage/css/bootstrap-theme.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/homepage/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/homepage/css/jquery.datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/homepage/css/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/homepage/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/homepage/css/master.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/homepage/css/footer.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>assets/homepage/css/slick.css">

    <!-- end theme style -->
</head>
<style type="text/css">

.removeRooms{}

.placeholder-setttings{
    width: 100%;
    border: 1px solid #fff;
    height: 50px;
    padding-left: 12px;
    border-radius: 0px;
    color: #000;
    font-size: 18px !important;
}

.placeholder-setttings::placeholder{
            font-size: 18px !important;
        }

        .airport_list{
        margin-left: 1px;
        background: black;
        padding: 5px;
        margin-top: -5px;
        border-radius: 2px;
        position: absolute;
        color: #FFF;
        opacity: 1;
        display: none;
    }

    .airport_list li{
        border-bottom: 1px solid #F5f5f5;
        list-style: none;
        cursor: pointer;
        width:100%;
        min-width: 16rem;
    }
    .airport_list p{
        color:white !important;
    }

    .airport_list2{
        background: black;
        padding: 5px;
        margin-top: -5px;
        border-radius: 2px;
        position: absolute;
        color: #FFF;
        opacity: 1;
        display: none;
    }
    .airport_list2 li{
        border-bottom: 1px solid #F5f5f5;
        list-style: none;
        cursor: pointer;
        width:100%;
        min-width: 16rem;
    }
    .airport_list2 p{
        color:white !important;
    }
 
    </style>
<body class="travel_home">
    <div class="se-pre-con0"></div>

    <div class="header-section">

        <div class="row">

            <div class="left-logo">
                <img src="<?php echo ASSETS; ?>assets/homepage/images/Logo.png">
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">

                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">

                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">FLIGHTS <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">HOTELS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CAR RENTALS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">HOLIDAYS</a>
                        </li>
                        <!-- <li>
                            <p>Loyalty Credit : 8000</p>
                        </li> -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								My Account
							  </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><a class="dropdown-item" href="#">Submenu</a></li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </nav>

        </div>

    </div>
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="first-slide" src="<?php echo ASSETS; ?>assets/homepage/images/banner01.jpg" alt="First slide">
            </div>
            <div class="item">
                <img class="second-slide" src="<?php echo ASSETS; ?>assets/homepage/images/banner02.jpg" alt="Second slide">
            </div>
            <div class="item">
                <img class="third-slide" src="<?php echo ASSETS; ?>assets/homepage/images/banner03.jpg" alt="Third slide">
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>

    <div class="search-filter-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-6 pl0 pr0 search-filter-opt">
                    <div id="content_wrapper">
                        <div class="clearfix"></div>
                        <!-- Home first slider start -->
                        <div class="slider_tab_main">

                            <!-- Home first slider End -->
                            <div class="home_tabs_search">
                                <div class="full_width slider_form_main">

                                    <div class="row">
                                        <div class="col-lg-12 pl0 pr0 main-tabs-sec">
                                            <div class="slider_tabs">
                                                <div class="wsa_tab">
                                                    <ul>
                                                        <li>
                                                            <a href="#flight_search"><img class="gray-c" src="<?php echo ASSETS; ?>assets/homepage/images/flight.png">
                                                                <img class="red-c" src="<?php echo ASSETS; ?>assets/homepage/images/flight02.png">
                                                                <span>Flights</span></a>
                                                        </li>

                                                        <li>
                                                            <a href="#hotel_search"><img class="gray-c" src="<?php echo ASSETS; ?>assets/homepage/images/hotel.png">
                                                                <img class="red-c" src="<?php echo ASSETS; ?>assets/homepage/images/hotel02.png">
                                                                <span>Hotels</span></a>
                                                        </li>
                                                        <li>
                                                            <a href="#transfer_search"><img class="gray-c" src="<?php echo ASSETS; ?>assets/homepage/images/car.png">
                                                                <img class="red-c" src="<?php echo ASSETS; ?>assets/homepage/images/car02.png">
                                                                <span>Car Rentals</span></a>
                                                        </li>
                                                        <li>
                                                            <a href="#cruise"><img class="gray-c" src="<?php echo ASSETS; ?>assets/homepage/images/vacation.png">
                                                                <img class="red-c" src="<?php echo ASSETS; ?>assets/homepage/images/vacation02.png">
                                                                <span>Holiday</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- slider_tab_main end -->

                    </div>
                    <!--content body end-->

                </div>

                <div class="full_width slider_content_wrap">

                    <!-- first content_start -->
                    <div id="flight_search" class="main_content_form">

                        <!-- tab_search form start -->
                        
                        <div class="radio-section-grp" id="trip_mode">
                            <input type="radio" name="trip_type" id="one-way" value="oneway" checked> 
                            <label for="one-way">One way</label>
                            <input type="radio" name="trip_type" id="round-trip" value="circle"> 
                            <label for="round-trip">Round Trip</label>
                            <input type="radio" name="trip_type" id="multi-city" value="multicity"> 
                            <label for="multi-city">Multi city</label>
                        </div>
                        <!-- oneway form start  -->
                        <form id="onewayFlightSearch" autocomplete="off" action="<?=base_url('flight/pre_flight_search'); ?>" name="flight_search_form" class="ng-pristine ng-valid"> 
                            <input type="hidden" name="trip_type"  value="oneway" checked> 

                            <div class="pull_left destination_field">
                                <label>From</label>
                                <input required class="placeholder-setttings" type="text" placeholder="Departure" list="browsers" id="departureInput" name="from" onkeyup="getDepartureAirportCity(this);">
                                <input type="hidden" name="from_loc_id" id="departureCityId">
                                <ul id="airportCityList" class="airport_list"></ul>
                                <p id="departureName">Departure City</p>
                            </div>
                            <div class="arrow-changer">
                                <div class="arrow-change-image">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/arrow.png" alt="" width="28" />
                                </div>
                            </div>
                            <div class="pull_left destination_field">
                                <label>To</label>
                                <input required  class="placeholder-setttings" name="to" type="text" placeholder="Destination" id="destinationInput" onkeyup="getDestinationAirportCity(this);">
                                <input  type="hidden" name="to_loc_id" id="destinationCityId">
                                <ul id="airportCityList_2" class="airport_list"></ul>
                                <p id="destinationName">Destination City</p>
                            </div>
                            <div class="pull_left check_in_field">
                                <label>Depature <i class="fa fa-caret-down"></i></label>
                                <input type="text" required  name="depature" class="placeholder-setttings" id="datepicker" placeholder="<?=date('d-m-Y'); ?>">
                                <!-- <p id="departureDay">Thursday</p> -->

                            </div>
                            
                            <div class="pull_left room_select_field">
                                <label>Travellers & Class</label>
                                <div class="totlall"> <span class="remngwd"><span id="change_travellercount_oneway_display">1</span> Traveller(s)</span>
                                    <p id="oneWayFlightClassName">Economy</p>
                                    <div class="roomcount">
                                        <div class="inallsn">
                                            <div class="oneroom fltravlr">

                                                <div class="roomrow">
                                                    <div class="celroe col-xs-8">Adults
                                                        <span class="agemns">Ages (12+)</span></div>
                                                    <div class="celroe col-xs-4">
                                                        <div class="input-group countmore"> <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="adult"> <span class="glyphicon glyphicon-minus"></span> </button>
                                                            </span>
                                                            <input  type="text"  class="form-control input-number centertext" value="1" min="1" max="9" onchange="change_travellercount_oneway()" name="adult" id="adult">
                                                            <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="adult"> <span class="glyphicon glyphicon-plus"></span> </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="roomrow">
                                                    <div class="celroe col-xs-8">Children
                                                        <span class="agemns">Ages (2-17)</span></div>
                                                    <div class="celroe col-xs-4">
                                                        <div class="input-group countmore"> <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="child"> <span class="glyphicon glyphicon-minus"></span> </button>
                                                            </span>
                                                            <input  type="text" class="form-control input-number centertext" value="0" min="0" onchange="change_travellercount_oneway()" max="9" name="child" id="child">
                                                            <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="child"> <span class="glyphicon glyphicon-plus"></span> </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="roomrow">
                                                    <div class="celroe col-xs-8">Infants
                                                        <span class="agemns">Ages under(0-2)</span></div>
                                                    <div class="celroe col-xs-4">
                                                        <div class="input-group countmore"> <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="infant"> <span class="glyphicon glyphicon-minus"></span> </button>
                                                            </span>
                                                            <input  type="text" class="form-control input-number centertext" value="0" min="0" onchange="change_travellercount_oneway()" max="9" id="infant" name="infant">
                                                            <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="infant"> <span class="glyphicon glyphicon-plus"></span> </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="fly-checkbox">

                                            <select class="" name ="carrierRoundTrip[]">
                                                <option value="">All</option>
                                                <option value="SU">AEROFLOT AIRLINE- SU	</option>
                                                <option value="G9">Air Arabia- G9</option>
                                                <option value="I5">AIR ASIA- I5</option>
                                                <option value="KC">AIR ASTANA- KC</option>
                                                <option value="AC">AIR CANADA- AC</option>
                                                <option value="CA">AIR CHINA- CA </option>
                                                <option value="AF">AIR FRANCE- AF</option>
                                                <option value="AI">AIR INDIA- AI</option>
                                                <option value="IX">AIR INDIA EXPRESS- IX</option>
                                                <option value="MK">AIR MAURITIUS- MK</option>
                                                <option value="UK">Air Vistara- UK</option>
                                                <option value="LB">AirCosta- LB</option>
                                                <option value="OP">AirPegasus- OP</option>
                                                <option value="AZ">ALITALIA- AZ</option>
                                                <option value="AA">AMERICAN AIRLINES- AA</option>
                                                <option value="OZ">ASIANA AIRLINES- OZ</option>
                                                <option value="OS">AUSTRIAN AIRWAYS- OS</option>
                                                <option value="BG">BIMAN BANGLADESH- BG</option>
                                                <option value="BA">BRITISH AIRWAYS- BA</option>
                                                <option value="BD">BRITISH MIDLAND- BD</option>
                                                <option value="SN">BRUSSELS AIRLINES- SN</option>
                                                <option value="CX">CATHAY PACIFIC- CX</option>
                                                <option value="CI">CHINA AIRLINES- CI</option>
                                                <option value="MU">CHINA EASTERN AIRLINES- MU</option>
                                                <option value="CZ">CHINA SOUTHERN- CZ</option>
                                                <option value="CO">CONTINENTAL AIRWAYS- CO</option>
                                                <option value="CY">CYPRUS AIRWAYS- CY</option>
                                                <option value="DL">DELTA AIRLINES- DL</option>
                                                <option value="KB">DRUK AIR- KB</option>
                                                <option value="MS">EGYPT AIR- MS</option>
                                                <option value="LY">ELAL ISRAEL AIRWAYS- LY</option>
                                                <option value="EK">EMIRATES AIRLINES- EK</option>
                                                <option value="ET">ETHIOPIAN AIRLINES- ET</option>
                                                <option value="EY">ETIHAD AIRWAYS- EY</option>
                                                <option value="BR">EVA AIR- BR</option>
                                                <option value="AY">FINNAIR- AY</option>
                                                <option value="GA">GARUDA AIRLINES- GA</option>
                                                <option value="Z5">GMG Airlines- Z5</option>
                                                <option value="G8">GoAir- G8</option>
                                                <option value="GF">GULF AIR- GF</option>
                                                <option value="IC">INDIAN AIRLINES- IC</option>
                                                <option value="6E">IndiGo- 6E</option>
                                                <option value="IR">IRAN AIR- IR</option>
                                                <option value="JL">JAPAN AIRLINES- JL	</option>
                                                <option value="J9">JAZEERA AIRWAYS- J9</option>
                                                <option value="9W">JET AIRWAYS- 9W</option>
                                                <option value="S2">JETLITE- S2</option>
                                                <option value="KQ">KENYA AIRWAYS- KQ</option>
                                                <option value="IT">KINGFISHER AIRLINES- IT</option>
                                                <option value="KL">KLM NORTHWEST- KL</option>
                                                <option value="KU">KUWAIT AIRWAYS- KU</option>
                                                <option value="LH">LUFTHANSA- LH</option>
                                                <option value="W5">MAHAN AIR- W5</option>
                                                <option value="MH">MALAYSIAN AIRLINE- MH</option>
                                                <option value="CE">NATIONWIDE- CE</option>
                                                <option value="NW">NORTHWEST AIRLINE- NW</option>
                                                <option value="WY">OMAN AIR- WY</option>
                                                <option value="OTH">Other Airlines- OTH</option>
                                                <option value="PK">PAKISTAN AIRLINES- PK</option>
                                                <option value="QF">QANTAS AIRWAYS- QF</option>
                                                <option value="QR">QATAR AIRWAYS- QR</option>
                                                <option value="RJ">ROYAL JORDANIAN- RJ</option>
                                                <option value="RA">ROYAL NEPAL- RA</option>
                                                <option value="SV">SAUDI ARABIAN- SV</option>
                                                <option value="MI">SILK AIR- MI</option>
                                                <option value="SQ">SINGAPORE AIRLINES- SQ</option>
                                                <option value="SA">SOUTH AFRICAN- SA</option>
                                                <option value="SG">SpiceJet- SG</option>
                                                <option value="UL">SRILANKAN AIRLINES- UL</option>
                                                <option value="LX">SWISS AIR- LX</option>
                                                <option value="RB">SYRIAN AIR- RB	</option>
                                                <option value="TG">THAI AIRWAYS- TG</option>
                                                <option value="2T">TruJet- 2T</option>
                                                <option value="TK">TURKISH AIRLINES- TK</option>
                                                <option value="UA">UNITED AIRLINES- UA</option>
                                                <option value="US">US AIRWAYS- US</option>
                                                <option value="HY">UZBEKISTAN AIRWAYS- HY</option>
                                                <option value="VN">VIETNAM AIRWAYS- VN</option>
                                                <option value="VS">VIRGIN ATLANTIC- VS</option>
                                                <option value="IY">YEMENA AIRWAYS- IY</option>
                                            </select>
                                            <input type="hidden"   id="search_flight" name="search_flight" placeholder="Depature Date" value="search" class="forminput">
                                            <select class="" required name="v_class" id="oneWayFlightClass" onchange="oneWayflightClassSelection(this)">
                                                <option value="Economy">Economy</option>
                                                <option value="Premium Economy">Premium Economy</option>
                                                <option value="Business">Business</option>
                                                <option value="Premium Business">Premium Business</option>
                                                <option value="First">First</option>
                                            </select>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <i class="fa fa-caret-down"></i>
                            </div>
                            <div class="flight-box-section">
                                <div class="circle_icon">
                                    <button class="fas fa-arrow-right small_circle" type="submit"> </button>
                                </div>
                            </div> 
                        </form>

                        <!-- oneway form end -->
                        <!--Roundtrip form Start -->
                        <form id="roundtripFlightSearch" autocomplete="off" action="<?=base_url('flight/pre_flight_search'); ?>" name="flight_search_form"  class="ng-pristine ng-valid"> 
                            <input type="hidden" name="trip_type" id="round-trip" value="circle">
                            <div class="pull_left destination_field">
                                <label>From</label>
                                <input required class="placeholder-setttings" type="text" placeholder="Departure" list="browsers" id="roundTripDepartureInput" name="from" onkeyup="getRoundTripDepartureAirportCity(this);">
                                <input type="hidden" name="from_loc_id" id="roundTripDepartureCityId">
                                <ul id="roundTripAirportCityList" class="airport_list"></ul>
                                <p id="roundTripDepartureName">Departure City</p>
                            </div>
                            <div class="arrow-changer">
                                <div class="arrow-change-image">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/arrow.png" alt="" width="28" />
                                </div>
                            </div>
                            <div class="pull_left destination_field">
                                <label>To</label>
                                <input  required class="placeholder-setttings" name="to" type="text" placeholder="Destination" id="roundTripDestinationInput" onkeyup="getRoundTripDestinationAirportCity(this);">
                                <input type="hidden" required name="to_loc_id" id="roundTripDestinationCityId">
                                <ul id="roundTripAirportCityList2" class="airport_list"></ul>
                                <p id="roundTripDestinationName">Destination City</p>
                            </div>
                            <div class="pull_left check_in_field">
                                <label>Depature <i class="fa fa-caret-down"></i></label>
                                <input type="text" required  name="depature" class="placeholder-setttings" id="datepicker01" placeholder="<?=date('d-m-Y'); ?>">
                                <!-- <p id="departureDay">Thursday</p> -->

                            </div>

                            <div class="pull_left check_in_field" id="return">
                                <label>Return <i class="fa fa-caret-down"></i></label>
                                <input type="text" required name="return"  class="placeholder-setttings" id="datepicker02" placeholder="<?=date('d-m-Y', strtotime('+1 day')); ?>" >
                                <!-- <p id="returnDay">Monday</p> -->

                            </div>
                            
                            <div class="pull_left room_select_field">
                                <label>Travellers & Class</label>
                                <div class="totlall"> <span class="remngwd"><span id="change_travellercount_roundtrip_display">1</span> Traveller(s)</span>
                                    <p id="roundTripflightClassName">Economy</p>
                                    <div class="roomcount">
                                        <div class="inallsn">
                                            <div class="oneroom fltravlr">

                                                <div class="roomrow">
                                                    <div class="celroe col-xs-8">Adults
                                                        <span class="agemns">Ages (12+)</span></div>
                                                    <div class="celroe col-xs-4">
                                                        <div class="input-group countmore"> <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="adult"> <span class="glyphicon glyphicon-minus"></span> </button>
                                                            </span>
                                                            <input  type="text"  class="form-control input-number centertext" value="1" min="1" max="9" onchange="change_travellercount_roundtrip()" name="adult" id="adult">
                                                            <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="adult"> <span class="glyphicon glyphicon-plus"></span> </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="roomrow">
                                                    <div class="celroe col-xs-8">Children
                                                        <span class="agemns">Ages (2-17)</span></div>
                                                    <div class="celroe col-xs-4">
                                                        <div class="input-group countmore"> <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="child"> <span class="glyphicon glyphicon-minus"></span> </button>
                                                            </span>
                                                            <input  type="text" class="form-control input-number centertext" value="0" min="0" onchange="change_travellercount_roundtrip()" max="9" name="child" id="child">
                                                            <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="child"> <span class="glyphicon glyphicon-plus"></span> </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="roomrow">
                                                    <div class="celroe col-xs-8">Infants
                                                        <span class="agemns">Ages under(0-2)</span></div>
                                                    <div class="celroe col-xs-4">
                                                        <div class="input-group countmore"> <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="infant"> <span class="glyphicon glyphicon-minus"></span> </button>
                                                            </span>
                                                            <input  type="text" class="form-control input-number centertext" value="0" min="0" onchange="change_travellercount_roundtrip()" max="9" id="infant" name="infant">
                                                            <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="infant"> <span class="glyphicon glyphicon-plus"></span> </button>
                                                            </smultiCityflightClassNamepan>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="fly-checkbox">

                                            <select class="" name ="carrier[]">
                                                <option value="">All</option>
                                                <option value="SU">AEROFLOT AIRLINE- SU	</option>
                                                <option value="G9">Air Arabia- G9</option>
                                                <option value="I5">AIR ASIA- I5</option>
                                                <option value="KC">AIR ASTANA- KC</option>
                                                <option value="AC">AIR CANADA- AC</option>
                                                <option value="CA">AIR CHINA- CA </option>
                                                <option value="AF">AIR FRANCE- AF</option>
                                                <option value="AI">AIR INDIA- AI</option>
                                                <option value="IX">AIR INDIA EXPRESS- IX</option>
                                                <option value="MK">AIR MAURITIUS- MK</option>
                                                <option value="UK">Air Vistara- UK</option>
                                                <option value="LB">AirCosta- LB</option>
                                                <option value="OP">AirPegasus- OP</option>
                                                <option value="AZ">ALITALIA- AZ</option>
                                                <option value="AA">AMERICAN AIRLINES- AA</option>
                                                <option value="OZ">ASIANA AIRLINES- OZ</option>
                                                <option value="OS">AUSTRIAN AIRWAYS- OS</option>
                                                <option value="BG">BIMAN BANGLADESH- BG</option>
                                                <option value="BA">BRITISH AIRWAYS- BA</option>
                                                <option value="BD">BRITISH MIDLAND- BD</option>
                                                <option value="SN">BRUSSELS AIRLINES- SN</option>
                                                <option value="CX">CATHAY PACIFIC- CX</option>
                                                <option value="CI">CHINA AIRLINES- CI</option>
                                                <option value="MU">CHINA EASTERN AIRLINES- MU</option>
                                                <option value="CZ">CHINA SOUTHERN- CZ</option>
                                                <option value="CO">CONTINENTAL AIRWAYS- CO</option>
                                                <option value="CY">CYPRUS AIRWAYS- CY</option>
                                                <option value="DL">DELTA AIRLINES- DL</option>
                                                <option value="KB">DRUK AIR- KB</option>
                                                <option value="MS">EGYPT AIR- MS</option>
                                                <option value="LY">ELAL ISRAEL AIRWAYS- LY</option>
                                                <option value="EK">EMIRATES AIRLINES- EK</option>
                                                <option value="ET">ETHIOPIAN AIRLINES- ET</option>
                                                <option value="EY">ETIHAD AIRWAYS- EY</option>
                                                <option value="BR">EVA AIR- BR</option>
                                                <option value="AY">FINNAIR- AY</option>
                                                <option value="GA">GARUDA AIRLINES- GA</option>
                                                <option value="Z5">GMG Airlines- Z5</option>
                                                <option value="G8">GoAir- G8</option>
                                                <option value="GF">GULF AIR- GF</option>
                                                <option value="IC">INDIAN AIRLINES- IC</option>
                                                <option value="6E">IndiGo- 6E</option>
                                                <option value="IR">IRAN AIR- IR</option>
                                                <option value="JL">JAPAN AIRLINES- JL	</option>
                                                <option value="J9">JAZEERA AIRWAYS- J9</option>
                                                <option value="9W">JET AIRWAYS- 9W</option>
                                                <option value="S2">JETLITE- S2</option>
                                                <option value="KQ">KENYA AIRWAYS- KQ</option>
                                                <option value="IT">KINGFISHER AIRLINES- IT</option>
                                                <option value="KL">KLM NORTHWEST- KL</option>
                                                <option value="KU">KUWAIT AIRWAYS- KU</option>
                                                <option value="LH">LUFTHANSA- LH</option>
                                                <option value="W5">MAHAN AIR- W5</option>
                                                <option value="MH">MALAYSIAN AIRLINE- MH</option>
                                                <option value="CE">NATIONWIDE- CE</option>
                                                <option value="NW">NORTHWEST AIRLINE- NW</option>
                                                <option value="WY">OMAN AIR- WY</option>
                                                <option value="OTH">Other Airlines- OTH</option>
                                                <option value="PK">PAKISTAN AIRLINES- PK</option>
                                                <option value="QF">QANTAS AIRWAYS- QF</option>
                                                <option value="QR">QATAR AIRWAYS- QR</option>
                                                <option value="RJ">ROYAL JORDANIAN- RJ</option>
                                                <option value="RA">ROYAL NEPAL- RA</option>
                                                <option value="SV">SAUDI ARABIAN- SV</option>
                                                <option value="MI">SILK AIR- MI</option>
                                                <option value="SQ">SINGAPORE AIRLINES- SQ</option>
                                                <option value="SA">SOUTH AFRICAN- SA</option>
                                                <option value="SG">SpiceJet- SG</option>
                                                <option value="UL">SRILANKAN AIRLINES- UL</option>
                                                <option value="LX">SWISS AIR- LX</option>
                                                <option value="RB">SYRIAN AIR- RB	</option>
                                                <option value="TG">THAI AIRWAYS- TG</option>
                                                <option value="2T">TruJet- 2T</option>
                                                <option value="TK">TURKISH AIRLINES- TK</option>
                                                <option value="UA">UNITED AIRLINES- UA</option>
                                                <option value="US">US AIRWAYS- US</option>
                                                <option value="HY">UZBEKISTAN AIRWAYS- HY</option>
                                                <option value="VN">VIETNAM AIRWAYS- VN</option>
                                                <option value="VS">VIRGIN ATLANTIC- VS</option>
                                                <option value="IY">YEMENA AIRWAYS- IY</option>
                                            </select>
                                            <input type="hidden"   id="search_flight" name="search_flight" placeholder="Depature Date" value="search" class="forminput">
                                            <select class="" required name="v_class" id="roundTripFlightClass" onchange="roundTripFlightClassSelection(this)">
                                                <option value="Economy">Economy</option>
                                                <option value="Premium Economy">Premium Economy</option>
                                                <option value="Business">Business</option>
                                                <option value="Premium Business">Premium Business</option>
                                                <option value="First">First</option>
                                            </select>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <i class="fa fa-caret-down"></i>
                            </div>

                            <div class="flight-box-section">
                                <div class="circle_icon">
                                    <button class="fas fa-arrow-right small_circle" type="submit"> </button>
                                </div>
                            </div> 
                        </form>

                         <!--Roundtrip form End -->

                         <!--Multi City form Start -->

<form autocomplete="off" action="<?=base_url('flight/pre_flight_search'); ?>" name="flight_search_form" id="multicityFlightSearch" class="ng-pristine ng-valid"> 
                            <input type="hidden" name="trip_type" id="multi-city" value="multicity">
                            <div class="pull_left destination_field">
                                <label>From</label>
                                <input required  class="placeholder-setttings" type="text" placeholder="Departure" list="browsers" id="multiCityDepartureInput" name="from[]" onkeyup="getMultiCityDepartureAirportCity(this); ">
                                <input type="hidden" name="from_loc_id[]" id="multiCityDepartureCityId">
                                <ul id="multiCityAirportCityList" class="airport_list"></ul>
                                <p id="multiCityDepartureName">Departure City</p>
                            </div>
                            <div class="arrow-changer">
                                <div class="arrow-change-image">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/arrow.png" alt="" width="28" />
                                </div>
                            </div>
                            <div class="pull_left destination_field">
                                <label>To</label>
                                <input required class="placeholder-setttings" name="to[]" type="text" placeholder="Destination" id="multiCityDestinationInput" onkeyup="getMultiCityDestinationAirportCity(this);">
                                <input type="hidden" name="to_loc_id[]" id="multiCityDestinationCityId">
                                <ul id="multiCityAirportCityList2" class="airport_list"></ul>
                                <p id="multiCityDestinationName">Destination City</p>
                            </div>
                            <div class="pull_left check_in_field">
                                <label>Depature <i class="fa fa-caret-down"></i></label>
                                <input type="text" required name="depature[]" class="placeholder-setttings" id="datepicker03" placeholder="<?=date('d-m-Y'); ?>">
                                <!-- <p id="departureDay">Thursday</p> -->
                            </div>
                            
                            <div class="pull_left room_select_field">
                                <label>Travellers & Class</label>
                                <div class="totlall"> <span class="remngwd"><span id="change_travellercount_multicity_display">1</span> Traveller(s)</span>
                                    <p id="multiCityFlightClassName">Economy</p>
                                    <div class="roomcount">
                                        <div class="inallsn">
                                            <div class="oneroom fltravlr">

                                                <div class="roomrow">
                                                    <div class="celroe col-xs-8">Adults
                                                        <span class="agemns">Ages (12+)</span></div>
                                                    <div class="celroe col-xs-4">
                                                        <div class="input-group countmore"> <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="adult"> <span class="glyphicon glyphicon-minus"></span> </button>
                                                            </span>
                                                            <input  type="text"  class="form-control input-number centertext" value="1" min="1" max="9" onchange="change_travellercount_multicity()" name="adult" id="adult">
                                                            <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="adult"> <span class="glyphicon glyphicon-plus"></span> </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="roomrow">
                                                    <div class="celroe col-xs-8">Children
                                                        <span class="agemns">Ages (2-17)</span></div>
                                                    <div class="celroe col-xs-4">
                                                        <div class="input-group countmore"> <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="child"> <span class="glyphicon glyphicon-minus"></span> </button>
                                                            </span>
                                                            <input  type="text" class="form-control input-number centertext" value="0" min="0" onchange="change_travellercount_multicity()" max="9" name="child" id="child">
                                                            <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="child"> <span class="glyphicon glyphicon-plus"></span> </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="roomrow">
                                                    <div class="celroe col-xs-8">Infants
                                                        <span class="agemns">Ages under(0-2)</span></div>
                                                    <div class="celroe col-xs-4">
                                                        <div class="input-group countmore"> <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="infant"> <span class="glyphicon glyphicon-minus"></span> </button>
                                                            </span>
                                                            <input  type="text" class="form-control input-number centertext" value="0" min="0" onchange="change_travellercount_multicity()" max="9" id="infant" name="infant">
                                                            <span class="input-group-btn">
						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="infant"> <span class="glyphicon glyphicon-plus"></span> </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="fly-checkbox">

                                            <select class="" name ="carrierMulticity[]">
                                                <option value="">All</option>
                                                <option value="SU">AEROFLOT AIRLINE- SU	</option>
                                                <option value="G9">Air Arabia- G9</option>
                                                <option value="I5">AIR ASIA- I5</option>
                                                <option value="KC">AIR ASTANA- KC</option>
                                                <option value="AC">AIR CANADA- AC</option>
                                                <option value="CA">AIR CHINA- CA </option>
                                                <option value="AF">AIR FRANCE- AF</option>
                                                <option value="AI">AIR INDIA- AI</option>
                                                <option value="IX">AIR INDIA EXPRESS- IX</option>
                                                <option value="MK">AIR MAURITIUS- MK</option>
                                                <option value="UK">Air Vistara- UK</option>
                                                <option value="LB">AirCosta- LB</option>
                                                <option value="OP">AirPegasus- OP</option>
                                                <option value="AZ">ALITALIA- AZ</option>
                                                <option value="AA">AMERICAN AIRLINES- AA</option>
                                                <option value="OZ">ASIANA AIRLINES- OZ</option>
                                                <option value="OS">AUSTRIAN AIRWAYS- OS</option>
                                                <option value="BG">BIMAN BANGLADESH- BG</option>
                                                <option value="BA">BRITISH AIRWAYS- BA</option>
                                                <option value="BD">BRITISH MIDLAND- BD</option>
                                                <option value="SN">BRUSSELS AIRLINES- SN</option>
                                                <option value="CX">CATHAY PACIFIC- CX</option>
                                                <option value="CI">CHINA AIRLINES- CI</option>
                                                <option value="MU">CHINA EASTERN AIRLINES- MU</option>
                                                <option value="CZ">CHINA SOUTHERN- CZ</option>
                                                <option value="CO">CONTINENTAL AIRWAYS- CO</option>
                                                <option value="CY">CYPRUS AIRWAYS- CY</option>
                                                <option value="DL">DELTA AIRLINES- DL</option>
                                                <option value="KB">DRUK AIR- KB</option>
                                                <option value="MS">EGYPT AIR- MS</option>
                                                <option value="LY">ELAL ISRAEL AIRWAYS- LY</option>
                                                <option value="EK">EMIRATES AIRLINES- EK</option>
                                                <option value="ET">ETHIOPIAN AIRLINES- ET</option>
                                                <option value="EY">ETIHAD AIRWAYS- EY</option>
                                                <option value="BR">EVA AIR- BR</option>
                                                <option value="AY">FINNAIR- AY</option>
                                                <option value="GA">GARUDA AIRLINES- GA</option>
                                                <option value="Z5">GMG Airlines- Z5</option>
                                                <option value="G8">GoAir- G8</option>
                                                <option value="GF">GULF AIR- GF</option>
                                                <option value="IC">INDIAN AIRLINES- IC</option>
                                                <option value="6E">IndiGo- 6E</option>
                                                <option value="IR">IRAN AIR- IR</option>
                                                <option value="JL">JAPAN AIRLINES- JL	</option>
                                                <option value="J9">JAZEERA AIRWAYS- J9</option>
                                                <option value="9W">JET AIRWAYS- 9W</option>
                                                <option value="S2">JETLITE- S2</option>
                                                <option value="KQ">KENYA AIRWAYS- KQ</option>
                                                <option value="IT">KINGFISHER AIRLINES- IT</option>
                                                <option value="KL">KLM NORTHWEST- KL</option>
                                                <option value="KU">KUWAIT AIRWAYS- KU</option>
                                                <option value="LH">LUFTHANSA- LH</option>
                                                <option value="W5">MAHAN AIR- W5</option>
                                                <option value="MH">MALAYSIAN AIRLINE- MH</option>
                                                <option value="CE">NATIONWIDE- CE</option>
                                                <option value="NW">NORTHWEST AIRLINE- NW</option>
                                                <option value="WY">OMAN AIR- WY</option>
                                                <option value="OTH">Other Airlines- OTH</option>
                                                <option value="PK">PAKISTAN AIRLINES- PK</option>
                                                <option value="QF">QANmultiCityFlightClassSelectionTAS AIRWAYS- QF</option>
                                                <option value="QR">QATAR AIRWAYS- QR</option>
                                                <option value="RJ">ROYAL JORDANIAN- RJ</option>
                                                <option value="RA">ROYAL NEPAL- RA</option>
                                                <option value="SV">SAUDI ARABIAN- SV</option>
                                                <option value="MI">SILK AIR- MI</option>
                                                <option value="SQ">SINGAPORE AIRLINES- SQ</option>
                                                <option value="SA">SOUTH AFRICAN- SA</option>
                                                <option value="SG">SpiceJet- SG</option>
                                                <option value="UL">SRILANKAN AIRLINES- UL</option>
                                                <option value="LX">SWISS AIR- LX</option>
                                                <option value="RB">SYRIAN AIR- RB	</option>
                                                <option value="TG">THAI AIRWAYS- TG</option>
                                                <option value="2T">TruJet- 2T</option>
                                                <option value="TK">TURKISH AIRLINES- TK</option>
                                                <option value="UA">UNITED AIRLINES- UA</option>
                                                <option value="US">US AIRWAYS- US</option>
                                                <option value="HY">UZBEKISTAN AIRWAYS- HY</option>
                                                <option value="VN">VIETNAM AIRWAYS- VN</option>
                                                <option value="VS">VIRGIN ATLANTIC- VS</option>
                                                <option value="IY">YEMENA AIRWAYS- IY</option>
                                            </select>
                                            <input type="hidden"   id="search_flight" name="search_flight" placeholder="Depature Date" value="search" class="forminput">
                                            <select class="" name="v_class" id="multiCityflightClass" onchange="multiCityFlightClassSelection(this)" required>
                                                <option value="Economy">Economy</option>
                                                <option value="Premium Economy">Premium Economy</option>
                                                <option value="Business">Business</option>
                                                <option value="Premium Business">Premium Business</option>
                                                <option value="First">First</option>
                                            </select>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <i class="fa fa-caret-down"></i>
                            </div>

                            <div class="multi-add" id="multiCity"> 
                                
                                <!-- <div class="multi-box">
                                    <div class="pull_left destination_field">
                                        <label>From</label>
                                        <input type="hidden" name="from_loc_id[]" id="multidepartureCityId">
                                        <input  class="placeholder-setttings" type="text" placeholder="Departure" list="browsers" id="multidepartureInput" name="from[]" onkeyup="getDepartureAirportCity(this);">
                                        <ul id="airportCityList" class="airport_list"></ul>
                                        <p id="departureName">Departure City</p>
                                    </div>
                                
                                    <div class="pull_left destination_field">
                                        <label>To</label>
                                        <input type="hidden" name="to_loc_id[]" id="destinationCityId">
                                        <input  class="placeholder-setttings" name="to[]" type="text" placeholder="Destination" id="multidestinationInput" onkeyup="getDestinationAirportCity(this);">
                                        <ul id="airportCityList" class="airport_list"></ul>
                                        <p id="destinationName">Destination City</p>
                                    </div>
                                    <div class="pull_left check_in_field">
                                        <label>Depature <i class="fa fa-caret-down"></i></label>
                                        <input type="text"  name="depature[]" class="placeholder-setttings" id="datepicker" placeholder="<?//=date('d-m-Y'); ?>">
                                    </div>
                                </div>  -->


                                <!-- <div class="multi-box">
                                    <div class="pull_left destination_field">
                                        <label>From</label>
                                        <input  class="placeholder-setttings" type="text" placeholder="Departure" list="browsers" id="departureInput" name="from[]" onkeyup="getDepartureAirportCity(this);">
                                        <input type="hidden" name="from_loc_id[]" id="departureCityId">
                                        <ul id="airportCityList" class="airport_list"></ul>
                                        <p id="deapertureNeme">Departure City</p>
                                    </div>
                                    
                                    <div class="pull_left destination_field">
                                        <label>To</label>
                                        <input  class="placeholder-setttings" name="to[]" type="text" placeholder="Destination" id="destinationInput" onkeyup="getDestinationAirportCity(this);">
                                        <input type="hidden" name="to_loc_id[]" id="destinationCityId">
                                        <ul id="airportCityList_2" class="airport_list2"></ul>
                                        <p id="destinationNeme">Destination City</p>
                                    </div>
                                    <div class="pull_left check_in_field">
                                        <label>Depature <i class="fa fa-caret-down"></i></label>
                                        <input type="text"  name="depature[]" class="placeholder-setttings" id="datepicker" placeholder="<?//=date('d-m-Y'); ?>">
                                        <div class="close-multy-box">
                                        <i class="fa fa-times-circle"></i>
                                        </div>

                                    </div>
                                </div> -->

                                <a id="addCity" href="#"><i class="fa fa-plus"></i> Add City</a>

                        </div>

                            <div class="flight-box-section">
                                <div class="circle_icon">
                                    <button class="fas fa-arrow-right small_circle" type="submit"> </button>
                                </div>
                            </div> 
                        </form>
                        
                        <!--Multi City form End -->

                        
                        <!-- tab_search form End -->
                    </div>
                    <!-- first content_end -->

                    <div id="hotel_search" class="main_content_form car-form-main">
                        <!-- tab_search form start -->
<form action="<?=base_url('hotel/pre_hotel_search'); ?>">
    <div class="pull_left destination_field">
        <label>City/Hotel/Area/Building</label>
        <input type="hidden" name="hotel_destination" id="hotel_destination">
        <input  class="placeholder-setttings" type="text" name="city" placeholder="Hotel" list="browsers" id="cityInput" onkeyup="getHotelCityList(this);">
        <ul id="hotelCityList" class="airport_list"></ul>
        <p id="cityName">City</p>
    </div>
    <div class="pull_left check_in_field">
        <label>Check In</label>
        <input type="text" class="hotel_checkin" id="datepicker10" name="hotel_checkin" placeholder="Check-In Date" required>
        <i class="fa fa-calendar"></i>
    </div>
    <div class="pull_left check_in_field">
        <label>Check Out</label>
        <input type="text" class="hotel_checkout" id="datepicker11" name="hotel_checkout" placeholder="Check-Out Date" required>
        <i class="fa fa-calendar"></i>
    </div>
    <div class="pull_left check_in_field hidden">
        <label>Rooms</label>
        <input type="hidden" id="rooms" name="rooms" placeholder="29 Jul" value="1" required>
        <i class="fa fa-calendar"></i>
    </div>
    <div class="pull_left room_select_field" style="border-right: 1px solid #3ca7be;">
        <label>No.of Nights</label>
        <select class="form-control selectpicker" name="" required>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
        </select>
        <i class="fa fa-caret-down"></i>
    </div>
    <div class="pull_left room_select_field">
        <label>Room & Guests</label>
        <div class="totlall"> 
            <span class="remngwd">
                <span id="change_hoteltravellercount_display" class="hotelTravelerCount">1 Adults, 0 Childs, 1 Rooms</span> 
            </span>
            <div class="roomcount">
                <div class="inallsn" id="" >
                    <div class="oneroom fltravlr">
                        <h3>Room 1</h3>
                        <div class="roomrow">
                            <div class="celroe col-xs-8">Adults
                                <span class="agemns">Ages (12+)</span>
                            </div>
                            <div class="celroe col-xs-4">
                                <div class="input-group countmore"> 
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number" onclick="update_count_passanger('adult', 'minus', 'adult-onetype'); ">
                                            <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>
                                    <input type="text" class="form-control input-number centertext" value="2" min="1" max="10" name="adult[]" id="adult-onetype">
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" onclick="update_count_passanger('adult', 'plus', 'adult-onetype'); "> 
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="roomrow">
                            <div class="celroe col-xs-8">Children
                                <span class="agemns">Ages (2-17)</span>
                            </div>
                            <div class="celroe col-xs-4">
                                <div class="input-group countmore">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number" onclick="update_count_passanger('child', 'minus', 'child-onetype'); "><span class="glyphicon glyphicon-minus"></span></button>
                                    </span>
                                    <input type="text" class="form-control input-number centertext" value="0" min="0" max="10" name="child[]" id="child-onetype">
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-default btn-number" onclick="update_count_passanger('child', 'plus', 'child-onetype'); "> <span class="glyphicon glyphicon-plus"></span> </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="addRooms">+ Add other room</a>
                </div>
            </div>
        </div>
        <i class="fa fa-caret-down"></i>
        <div class="flight-box-section">
            <div class="circle_icon">
                <button class="fas fa-arrow-right small_circle" type="submit"> </button>
            </div>
        </div>
    </div>
</form>
                        <!-- tab_search form End -->

                        <!-- <div class="advanced-search-option option-heading">
	   <a href="#">Advanced Search</a>
	</div> -->
                        <div class="option-content is-hidden">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Car type</label>
                                                <div class="select_box">
                                                    <select class="form-control">
                                                        <option value="">Select</option>
                                                        <option selected="selected">Hatchback1</option>
                                                        <option>Hatchback1</option>
                                                        <option>Hatchback1</option>
                                                        <option>Hatchback1</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Rental for company</label>
                                                <div class="select_box">
                                                    <select class="form-control">
                                                        <option value="">Select</option>
                                                        <option selected="selected">select from the list</option>
                                                        <option>Hatchback1</option>
                                                        <option>Hatchback1</option>
                                                        <option>Hatchback1</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Car Preference</label>
                                                <div class="select_box">
                                                    <select class="form-control">
                                                        <option value="">Select</option>
                                                        <option selected="selected">AC</option>
                                                        <option>NON-AC</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Discount code</label>
                                                <div class="select_box">
                                                    <select class="form-control">
                                                        <option value="">Select</option>
                                                        <option selected="selected">Corporate</option>
                                                        <option>Corporate1</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">&nbsp;</label>
                                                <div class="select_box">
                                                    <select class="form-control">
                                                        <option value="">Select</option>
                                                        <option selected="selected">Automatic Trans</option>
                                                        <option>Automatic Trans1</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">&nbsp;</label>
                                                <div class="select-box-input">
                                                    <input type="text" class="form-control" placeholder="Enter Code">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div id="transfer_search" class="main_content_form car-form-main">

<!-- <style type="text/css">
    #removeRoom2{}
    .removeRoom2{}
</style>
            -->             <!-- <div class="row">
								 <label class="col-md-12 el-switch" >
								 <input type="checkbox" id="toggleElement" type="checkbox" name="toggle" onchange="toggleStatus()">
								 <span class="el-switch-style"></span>
								 <span class="margin-r">Same drop off location</span>
								 </label>
							  </div> -->
                        <!-- tab_search form start -->
                        <form action="<?=base_url('general/pre_transferv1_search'); ?>">
                        
                            <div class="pull_left destination_field-car">
                                <label>Pick Up</label>
                                <!-- <input type="hidden" placeholder="Location" name="from" id="transferLocation" required> -->
                                <input required class="placeholder-setttings" type="text" placeholder="Departure" list="browsers" id="locationInput" name="from" onkeyup="getTransferLocation(this);">
                                <input type="hidden" placeholder="Location" name="destination_id" id="transferLocationId" required>
                                <ul id="transferLocationList" class="airport_list"></ul>
                                <p id="locationName">Location</p>

                                <input type="hidden" name="from_date" id="from_date" >
                                <input type="hidden" name="to_date" id="to_date" >
                            </div>
                            <!-- <div class="arrow-changer-for-car">
							<div class="arrow-change-image-for-car">
							   <img src="<?php echo ASSETS; ?>assets/homepage/images/arrow.png" alt="" width="28" />
							</div>
						 </div>
						 <div class="pull_left destination_field-car" id="elementsToOperateOn">
							<label>Drop Off</label>
							<input type="text" placeholder="Arrival">
						 </div>
						 <div class="pull_left check_in_field-car">
							<label>Pickup Date & Time</label>
							<input type="text"  id="datetimepicker" placeholder="20 Jul">
							<i class="fa fa-calendar"></i>
						 </div>
						 <div class="pull_left check_in_field-car" style="border:0;">
							<label>Return Date & Time</label>
							<input type="text" id="datetimepicker1" placeholder="29 Jul">
							<i class="fa fa-calendar"></i>
						 </div> -->
                            <!-- <div class="pull_left destination_field">
							<label>Travellers & Class <i class="pull-right fa fa-caret-down"></i></label>
							<input type="text" placeholder="0 Traveller">
							</div> -->
                            <!-- <div class="pull_left room_select_field">
							<label>adults</label>
							<select  class="form-control selectpicker" data-live-search="true" id="search_adults" >
									 <div class="row form-group form_text">
										<label class="col-md-3 control-label">Adult:</label>
										<div class="col-md-9 adultbox">
										<span class="adulticon"></span>
										<input id="after1" class="form-control" type="number" value="1" min="1" max="20" />
										</div>
									 </div>
							</select>
							<i class="fa fa-caret-down"></i>
							</div> -->
                            <!-- <div class="pull_left room_select_field ">
							<label>kids</label>
							<select class="form-control selectpicker" data-live-search="true" id="search_kids">
							   <option value="1">01</option>
							   <option value="2">02</option>
							   <option value="3">03</option>
							   <option value="4">04</option>
							</select>
							<i class="fa fa-caret-down"></i> 
							</div> -->
                            <!-- <div class="pull_left submit_field">
							<input type="search" placeholder="SEARCH" class="search_tabs">
							<button class="btn tab_search" type="submit"> <i class="glyphicon glyphicon-search"></i></button>
							</div> -->
                            <div class="flight-box-section">
                                <div class="circle_icon">
                                    <!-- <a href="car-serch-result.html" class="fas fa-arrow-right small_circle" tabindex="0"></a> -->
                                    <button class="fas fa-arrow-right small_circle" type="submit"> </button>
                                </div>
                            </div>
                        </form>
                        <!-- tab_search form End -->
                        <!-- <div class="clearfix">&nbsp;</div> -->
                        <!-- <div class="advanced-search-option option-heading">
	   <a href="#">Advanced Search</a>
	</div> -->
                        <div class="option-content is-hidden">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Car type</label>
                                                <div class="select_box">
                                                    <select class="form-control">
                                                        <option value="">Select</option>
                                                        <option selected="selected">Hatchback1</option>
                                                        <option>Hatchback1</option>
                                                        <option>Hatchback1</option>
                                                        <option>Hatchback1</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Rental for company</label>
                                                <div class="select_box">
                                                    <select class="form-control">
                                                        <option value="">Select</option>
                                                        <option selected="selected">select from the list</option>
                                                        <option>Hatchback1</option>
                                                        <option>Hatchback1</option>
                                                        <option>Hatchback1</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Car Preference</label>
                                                <div class="select_box">
                                                    <select class="form-control">
                                                        <option value="">Select</option>
                                                        <option selected="selected">AC</option>
                                                        <option>NON-AC</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Discount code</label>
                                                <div class="select_box">
                                                    <select class="form-control">
                                                        <option value="">Select</option>
                                                        <option selected="selected">Corporate</option>
                                                        <option>Corporate1</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">&nbsp;</label>
                                                <div class="select_box">
                                                    <select class="form-control">
                                                        <option value="">Select</option>
                                                        <option selected="selected">Automatic Trans</option>
                                                        <option>Automatic Trans1</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">&nbsp;</label>
                                                <div class="select-box-input">
                                                    <input type="text" class="form-control" placeholder="Enter Code">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div id="cruise" class="main_content_form">
                        <!-- tab_search form start -->
                        <form>
                            <div class="pull_left destination_field">
                                <label>Country</label>
                                <select  class="form-control selectpicker">
										  <option value="1">01</option>
										  <option value="2">02</option>
										  <option value="3">03</option>
										  <option value="4">04</option>
									  </select>
									  <i class="fa fa-caret-down"></i>
                            </div>
                            <div class="pull_left destination_field">
                                <label>Package Type</label>
                                <select  class="form-control selectpicker">
										  <option value="1">01</option>
										  <option value="2">02</option>
										  <option value="3">03</option>
										  <option value="4">04</option>
									  </select>
									  <i class="fa fa-caret-down"></i>
                            </div>
                            <div class="pull_left destination_field">
                                <label>Duration</label>
                                <select  class="form-control selectpicker">
										  <option value="1">01</option>
										  <option value="2">02</option>
										  <option value="3">03</option>
										  <option value="4">04</option>
									  </select>
									  <i class="fa fa-caret-down"></i>
                            </div>
                          
                            <div class="pull_left destination_field">
                                <label>Budget</label>
                                <select  class="form-control selectpicker">
										  <option value="1">01</option>
										  <option value="2">02</option>
										  <option value="3">03</option>
										  <option value="4">04</option>
									  </select>
									  <i class="fa fa-caret-down"></i>
                        
                                <div class="flight-box-section">
                                    <div class="circle_icon">
                                        <a href="inner.html" class="fas fa-arrow-right small_circle" tabindex="0"></a>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="pull_left room_select_field">
									  <label>rooms</label>
									  <select  class="form-control selectpicker">
										  <option value="1">01</option>
										  <option value="2">02</option>
										  <option value="3">03</option>
										  <option value="4">04</option>
									  </select>
									  <i class="fa fa-caret-down"></i>
								  </div>
								  <div class="pull_left room_select_field">
									  <label>adults</label>
									  <select  class="form-control selectpicker" data-live-search="true" id="search_adults4" >
										  <option value="1">01</option>
										  <option value="2">02</option>
										  <option value="3">03</option>
										  <option value="4">04</option>
									  </select>
									  <i class="fa fa-caret-down"></i>
								  </div> -->
                            <!-- <div class="pull_left room_select_field">
									  <label>kids</label>
									  <select class="form-control selectpicker" data-live-search="true" id="search_kids4">
										  <option value="1">01</option>
										  <option value="2">02</option>
										  <option value="3">03</option>
										  <option value="4">04</option>
									  </select>
									  <i class="fa fa-caret-down"></i> 

								  </div> -->
                            <!-- <div class="pull_left submit_field">
									  <input type="search" placeholder="SEARCH" class="search_tabs">
									  <button class="btn tab_search" type="submit"> <i class="glyphicon glyphicon-search"></i></button>
								  </div> -->
                        </form>
                        <!-- tab_search form End -->
                    </div>
                </div>
            </div>

        </div>

    </div>

    <!-- top deals start -->

    <div class="full_width travelite_world_section">
        <div class="container">
            <div class="row">

                <div class="heading_team">
                    <h3>Popular Flight Destination</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur </p>
                </div>

                <div class="full_width rectangle_wrapper">

                    <section class="regular slider">
                        <div>
                            <!-- first ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/holiday.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>Cape Town</h2>
                                        <h5>Starting From</h5>
                                        <h4>$ 160</h4>
                                        <p>
                                            <input type="checkbox" id="c1" name="cb">
                                            <label for="c1">Show Hotels</label>
                                        </p>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>

                                    <!-- <i class="fa fa-arrow-right small_circle"></i> -->
                                </div>
                            </div>
                            <!-- first ractangle End -->
                        </div>
                        <div>
                            <!-- second ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/hotels.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>Cape Town</h2>
                                        <h5>Starting From</h5>
                                        <h4>$ 160</h4>
                                        <p>
                                            <input type="checkbox" id="c2" name="cb">
                                            <label for="c2">Show Hotels</label>
                                        </p>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>
                                </div>
                            </div>
                            <!-- second ractangle End -->
                        </div>
                        <div>
                            <!-- thired ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/return-trip.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>Cape Town</h2>
                                        <h5>Starting From</h5>
                                        <h4>$ 160</h4>
                                        <p>
                                            <input type="checkbox" id="c3" name="cb">
                                            <label for="c3">Show Hotels</label>
                                        </p>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>
                                </div>
                            </div>
                            <!-- thired ractangle End -->
                        </div>
                        <div>
                            <!-- fourth ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/international-flight.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>Cape Town</h2>
                                        <h5>Starting From</h5>
                                        <h4>$ 160</h4>
                                        <p>
                                            <input type="checkbox" id="c4" name="cb">
                                            <label for="c4">Show Hotels</label>
                                        </p>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>
                                </div>
                            </div>
                            <!-- fourth ractangle End -->
                        </div>
                        <div>
                            <!-- fourth ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/international-flight.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>Cape Town</h2>
                                        <h5>Starting From</h5>
                                        <h4>$ 160</h4>
                                        <p>
                                            <input type="checkbox" id="c5" name="cb">
                                            <label for="c5">Show Hotels</label>
                                        </p>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>
                                </div>
                            </div>
                            <!-- fourth ractangle End -->
                        </div>
                        <div>
                            <!-- fourth ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/international-flight.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>Cape Town</h2>
                                        <h5>Starting From</h5>
                                        <h4>$ 160</h4>
                                        <p>
                                            <input type="checkbox" id="c6" name="cb">
                                            <label for="c6">Show Hotels</label>
                                        </p>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>
                                </div>
                            </div>
                            <!-- fourth ractangle End -->
                        </div>
                    </section>

                </div>

            </div>
        </div>
    </div>

    <!-- top deals start -->

    <div class="full_width travelite_world_section">
        <div class="container">
            <div class="row">

                <div class="heading_team">
                    <h3>Top Hotel Destinations</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur </p>
                </div>

                <div class="full_width rectangle_wrapper">

                    <section class="regular slider hotel-slider">
                        <div>
                            <!-- first ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/holiday.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>PARIS 5 <i class="fa fa-star" aria-hidden="true"></i></h2>
                                        <ul>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list04.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list03.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list02.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list01.png"></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>

                                    <!-- <i class="fa fa-arrow-right small_circle"></i> -->
                                </div>
                            </div>
                            <!-- first ractangle End -->
                        </div>
                        <div>
                            <!-- second ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/hotels.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>PARIS 5 <i class="fa fa-star" aria-hidden="true"></i></h2>
                                        <ul>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list04.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list03.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list02.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list01.png"></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>
                                </div>
                            </div>
                            <!-- second ractangle End -->
                        </div>
                        <div>
                            <!-- thired ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/return-trip.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>PARIS 5 <i class="fa fa-star" aria-hidden="true"></i></h2>
                                        <ul>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list04.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list03.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list02.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list01.png"></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>
                                </div>
                            </div>
                            <!-- thired ractangle End -->
                        </div>
                        <div>
                            <!-- fourth ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/international-flight.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>PARIS 5 <i class="fa fa-star" aria-hidden="true"></i></h2>
                                        <ul>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list04.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list03.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list02.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list01.png"></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>
                                </div>
                            </div>
                            <!-- fourth ractangle End -->
                        </div>
                        <div>
                            <!-- fourth ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/international-flight.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>PARIS 5 <i class="fa fa-star" aria-hidden="true"></i></h2>
                                        <ul>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list04.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list03.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list02.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list01.png"></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>
                                </div>
                            </div>
                            <!-- fourth ractangle End -->
                        </div>
                        <div>
                            <!-- fourth ractangle start -->
                            <div class="col_25 ractangle_box_cover">
                                <div class="full_width ractangle_inner">
                                    <img src="<?php echo ASSETS; ?>assets/homepage/images/international-flight.png" alt="house">
                                    <div class="inner_ovelay">
                                        <h2>PARIS 5 <i class="fa fa-star" aria-hidden="true"></i></h2>
                                        <ul>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list04.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list03.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list02.png"></li>
                                            <li><img src="<?php echo ASSETS; ?>assets/homepage/images/h-list01.png"></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="circle_icon">
                                    <a href="#" class="fas fa-arrow-right small_circle"></a>
                                </div>
                            </div>
                            <!-- fourth ractangle End -->
                        </div>
                    </section>

                </div>

            </div>
        </div>
    </div>

    <!-- top deals end -->

    <!-- honeymoon package section start -->
    <div class="full_width travelite_package_section">
        <div class="container">
            <div class="row">
                <div class="heading_team">
                    <h3>Top Holiday Destinations</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur </p>
                </div>
                <div class="control-pad holiday-sec">

                    <section class="lazy slider" data-sizes="50vw">
                        <div>
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                `
                                <div class="full_width">
                                    <div class="hnmn_packages">
                                        <div class="hnmn_pack_thumb">
                                            <img src="<?php echo ASSETS; ?>assets/homepage/images/hol-01.jpg">

                                        </div>

                                        <div class="hnmn_pack_content">
                                            <h4 class="package_title">Hamburg</h4>
                                            <span>Germany</span>
                                            <p class="day-night">4 Days 3 Nights</p>
                                            <p class="start-rs">Starting at
                                                <br>
                                                <a href="#" class="black_btn all_pack_btn">$ 450</a></p>

                                        </div>
                                    </div>
                                    <div class="circle_icon">
                                        <a href="#" class="fas fa-arrow-right small_circle"></a>
                                    </div>

                                </div>

                            </div>
                            <div id="half-width" class="half-width col-lg-7 col-md-7 col-sm-12">
                                <div class="left_side_packages_part">

                                    <div class="full_width ">
                                        <div class="hnmn_packages">
                                            <div class="hnmn_pack_thumb">
                                                <img src="<?php echo ASSETS; ?>assets/homepage/images/hol-03.jpg">

                                            </div>

                                            <div class="hnmn_pack_content">
                                                <h4 class="package_title">Whistler</h4>
                                                <span>Germany</span>
                                                <p class="day-night">4 Days 3 Nights</p>
                                                <p class="start-rs">Starting at
                                                    <br>
                                                    <a href="#" class="black_btn all_pack_btn">$ 450</a></p>

                                            </div>
                                        </div>
                                        <div class="circle_icon">
                                            <a href="#" class="fas fa-arrow-right small_circle"></a>
                                        </div>
                                    </div>

                                    <div class="full_width">
                                        <div class="hnmn_packages">
                                            <div class="hnmn_pack_thumb">
                                                <img src="<?php echo ASSETS; ?>assets/homepage/images/hol-04.jpg">

                                            </div>

                                            <div class="hnmn_pack_content">
                                                <h4 class="package_title">Whistler</h4>
                                                <span>Germany</span>
                                                <p class="day-night">4 Days 3 Nights</p>
                                                <p class="start-rs">Starting at
                                                    <br>
                                                    <a href="#" class="black_btn all_pack_btn">$ 450</a></p>

                                            </div>
                                        </div>
                                        <div class="circle_icon">
                                            <a href="#" class="fas fa-arrow-right small_circle"></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                `
                                <div class="full_width">
                                    <div class="hnmn_packages">
                                        <div class="hnmn_pack_thumb">
                                            <img src="<?php echo ASSETS; ?>assets/homepage/images/hol-01.jpg">

                                        </div>

                                        <div class="hnmn_pack_content">
                                            <h4 class="package_title">Hamburg</h4>
                                            <span>Germany</span>
                                            <p class="day-night">4 Days 3 Nights</p>
                                            <p class="start-rs">Starting at
                                                <br>
                                                <a href="#" class="black_btn all_pack_btn">$ 450</a></p>

                                        </div>
                                    </div>
                                    <div class="circle_icon">
                                        <a href="#" class="fas fa-arrow-right small_circle"></a>
                                    </div>

                                </div>

                            </div>
                            <div id="half-width" class="half-width col-lg-7 col-md-7 col-sm-12">
                                <div class="left_side_packages_part">

                                    <div class="full_width ">
                                        <div class="hnmn_packages">
                                            <div class="hnmn_pack_thumb">
                                                <img src="<?php echo ASSETS; ?>assets/homepage/images/hol-03.jpg">

                                            </div>

                                            <div class="hnmn_pack_content">
                                                <h4 class="package_title">Whistler</h4>
                                                <span>Germany</span>
                                                <p class="day-night">4 Days 3 Nights</p>
                                                <p class="start-rs">Starting at
                                                    <br>
                                                    <a href="#" class="black_btn all_pack_btn">$ 450</a></p>

                                            </div>
                                        </div>
                                        <div class="circle_icon">
                                            <a href="#" class="fas fa-arrow-right small_circle"></a>
                                        </div>
                                    </div>

                                    <div class="full_width">
                                        <div class="hnmn_packages">
                                            <div class="hnmn_pack_thumb">
                                                <img src="<?php echo ASSETS; ?>assets/homepage/images/hol-04.jpg">

                                            </div>

                                            <div class="hnmn_pack_content">
                                                <h4 class="package_title">Whistler</h4>
                                                <span>Germany</span>
                                                <p class="day-night">4 Days 3 Nights</p>
                                                <p class="start-rs">Starting at
                                                    <br>
                                                    <a href="#" class="black_btn all_pack_btn">$ 450</a></p>

                                            </div>
                                        </div>
                                        <div class="circle_icon">
                                            <a href="#" class="fas fa-arrow-right small_circle"></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </section>

                </div>
            </div>
        </div>

        <div class="full_width travelite_world_section">
            <div class="container">
                <div class="row">

                    <div class="heading_team">
                        <h3>Tour Style</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur </p>
                    </div>

                    <div class="full_width rectangle_wrapper style-sec">

                        <ul>
                            <li>
                                <img src="<?php echo ASSETS; ?>assets/homepage/images/s-list01.png">
                                <p>THEME PARKS</p>
                            </li>
                            <li>
                                <img src="<?php echo ASSETS; ?>assets/homepage/images/s-list02.png">
                                <p>SPA TOURS</p>
                            </li>
                            <li>
                                <img src="<?php echo ASSETS; ?>assets/homepage/images/s-list03.png">
                                <p>WATER SPORTS</p>
                            </li>
                            <li>
                                <img src="<?php echo ASSETS; ?>assets/homepage/images/s-list04.png">
                                <p>AIR, HELICOPTER & BALLOON TOURS</p>
                            </li>
                            <li>
                                <img src="<?php echo ASSETS; ?>assets/homepage/images/s-list05.png">
                                <p>FAMILY FRIENDLY</p>
                            </li>
                            <li>
                                <img src="<?php echo ASSETS; ?>assets/homepage/images/s-list06.png">
                                <p>DAY TRIPS & EXCURSIONS
                                </p>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="full_width travelite_world_section partner-sec">
            <div class="container">
                <div class="row">
                    <ul>

                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-01.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-02.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-03.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-04.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-05.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-06.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-07.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-08.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-09.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-10.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-11.png"></li>
                        <li><img src="<?php echo ASSETS; ?>assets/homepage/images/p-list-12.png"></li>

                    </ul>

                </div>
            </div>
        </div>
        <!-- honeymoon package section start -->

        <!--footer section start -->
        <footer id="footer_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <aside class="widget widget_links">
                            <h4 class="widget-title">POPULAR HOTELS</h4>
                            <ul>
                                <li><a href="#">PHUKET</a></li>
                                <li><a href="#">SPLIT </a></li>
                                <li><a href="#">MONTEGO BAY</a></li>
                                <li><a href="#">LONDON </a></li>
                                <li><a href="#">PARIS </a></li>
                                <li><a href="#">JOHANNESBURG </a></li>
                                <li><a href="#">BARCELONA </a></li>
                                <li><a href="#">BALI </a></li>
                            </ul>
                        </aside>
                    </div>

                    <div class="col-md-3">
                        <aside class="widget widget_links">
                            <h4 class="widget-title">THINGS TO DO</h4>
                            <ul>
                                <li><a href="#">BANGKOK </a></li>
                                <li><a href="#">ROME </a></li>
                                <li><a href="#">SINGAPORE</a></li>
                                <li><a href="#">TOKYO </a></li>
                                <li><a href="#">ISTANBUL </a></li>
                                <li><a href="#">CAPE TOWN </a></li>
                                <li><a href="#">HAMBURG </a></li>
                                <li><a href="#">CASABLANCA </a></li>
                            </ul>
                        </aside>
                    </div>

                    <div class="col-md-3">

                        <aside class="widget widget_links">
                            <h4 class="widget-title">TRAVELO NEWS</h4>
                            <ul>
                                <li><a href="#">Cheap hotels in Miami7</a></li>
                                <li><a href="#">TRAVEL BOOKS WORTH <br>
				READING RIGHT NOW</a></li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-md-3">

                        <aside class="widget payment_method">
                            <h4 class="widget-title">Follow Us</h4>
                            <a href="#"><img src="<?php echo ASSETS; ?>assets/homepage/images/insta.png" alt="insta" /></a>
                            <a href="#"><img src="<?php echo ASSETS; ?>assets/homepage/images/fb.png" alt="insta" /></a>
                            <a href="#"><img src="<?php echo ASSETS; ?>assets/homepage/images/twitter.png" alt="insta" /></a>
                        </aside>
                    </div>
                </div>
            </div>
        </footer>
        <!--footer section end -->
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center"> <span>COPYRIGHT 2019 ELAMANT, LLC | ALL RIGHTS RESERVED.</span> </div>
                </div>
            </div>
        </div>
    </div>

    <!--content body start-->

    <!--Page main section end-->
    <!--main js file start-->

    <!-- <script type="text/javascript" src="js/modernizr.custom.js"></script> -->
    <script type="text/javascript" src="<?php echo ASSETS; ?>assets/homepage/js/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>assets/homepage/js/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>assets/homepage/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>assets/homepage/js/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>assets/homepage/js/jquery.checkradios.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>assets/homepage/js/bootstrap-number-input.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>assets/homepage/js/plugin/datetimepicker/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>assets/homepage/js/plugin/parallax/jquery.parallax-1.1.3.js"></script>

    </script>
    <!-- pricefilter -->
    <script src="<?php echo ASSETS; ?>assets/homepage/js/plugin/jquery-ui/jquery-ui.js"></script>
    <!-- pricefilter-->
    <script type="text/javascript" src="<?php echo ASSETS; ?>assets/homepage/js/custom.js"></script>
    <!--main js file end-->
    <!-- CheckRadios Usage Examples -->

    <script src="<?php echo ASSETS; ?>assets/homepage/js/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(document).on('ready', function() {

            $(".regular").slick({
                dots: true,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1
            });
            $(".lazy").slick({
                dots: true,
                lazyLoad: 'ondemand', // ondemand progressive anticipated
                infinite: true
            });
        });
    </script>
    <script>
        $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
            }
            var $subMenu = $(this).next(".dropdown-menu");
            $subMenu.toggleClass('show');

            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
                $('.dropdown-submenu .show').removeClass("show");
            });

            return false;
        });
    </script>

    <script>
        //paste this code under head tag or in a seperate js file.
        // Wait for window load
        $(window).load(function() {
            // Animate loader off screen
            $(".se-pre-con").delay(1800).slideUp(500).fadeOut(400);
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.checkradios').checkradios();
        });
    </script>

    <script type="text/javascript">
        // Remember set you events before call bootstrapSwitch or they will fire after bootstrapSwitch's events
        $('#after1').bootstrapNumber();
        $('#after2').bootstrapNumber();
        $('#after3').bootstrapNumber();
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            var winht = $(window).height();
            var minht = winht - ($('.topssec').outerHeight());
            $('.minheight').css({
                'min-height': minht
            });

            $('.advncebtn').click(function() {
                $(this).parent('.togleadvnce').toggleClass('open');
            });

            $('.totlall').click(function() {
                $('.roomcount').toggleClass("fadeinn");

            });

            $('.totlall, .roomcount').click(function(e) {
                e.stopPropagation();
            });

            $(document).click(function() {
                $('.roomcount').removeClass("fadeinn");
            });

            $('.alladvnce').click(function() {
                $('.advncedown').removeClass("fadeinn");
                $(this).children('.advncedown').toggleClass("fadeinn");
            });

            $('.alladvnce, .advncedown').click(function(e) {
                e.stopPropagation();
            });

            $(document).click(function() {
                $('.advncedown').removeClass("fadeinn");
            });

            $('.btn-number').click(function(e) {
                e.preventDefault();

                fieldName = $(this).attr('data-field');
                type = $(this).attr('data-type');
                var input = $("input[name='" + fieldName + "']");
                var currentVal = parseInt(input.val());
                if (!isNaN(currentVal)) {
                    if (type == 'minus') {

                        if (currentVal > input.attr('min')) {
                            input.val(currentVal - 1).change();
                        }
                        if (parseInt(input.val()) == input.attr('min')) {
                            $(this).attr('disabled', true);
                        }

                    } else if (type == 'plus') {

                        if (currentVal < input.attr('max')) {
                            input.val(currentVal + 1).change();
                        }
                        if (parseInt(input.val()) == input.attr('max')) {
                            $(this).attr('disabled', true);
                        }

                    }
                } else {
                    input.val(0);
                }
            });
            $('.input-number').focusin(function() {
                $(this).data('oldValue', $(this).val());
            });
            $('.input-number').change(function() {

                minValue = parseInt($(this).attr('min'));
                maxValue = parseInt($(this).attr('max'));
                valueCurrent = parseInt($(this).val());

                name = $(this).attr('name');
                if (valueCurrent >= minValue) {
                    $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
                } else {
                    alert('Sorry, the minimum value was reached');
                    $(this).val($(this).data('oldValue'));
                }
                if (valueCurrent <= maxValue) {
                    $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
                } else {
                    alert('Sorry, the maximum value was reached');
                    $(this).val($(this).data('oldValue'));
                }

            });
            $(".input-number").keydown(function(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            handleStatusChanged();
        });

        function handleStatusChanged() {
            $('#toggleElement').on('change', function() {
                toggleStatus();hotelCityList
            });
        }

        function toggleStatus() {
            if ($('#toggleElement').is(':checked')) {
                $('#elementsToOperateOn :input').attr('disabled', true);
                $('#elementsToOperateOn').addClass('disabled_sec');
            } else {
                $('#elementsToOperateOn :input').removeAttr('disabled');
                $('#elementsToOperateOn').removeClass('disabled_sec');
            }
        }
    </script>
    <script type="text/javascript">
    // form rondtrip start
    function getRoundTripDepartureAirportCity(e){
            
            //console.log("getDepartureAirportCity");
            //console.log($(e).val());

            var enter_city = $(e).val();
            $.ajax({
                url: WEB_URL + 'index.php/ajax/get_airport_code_list?term='+enter_city,
                type: 'GET',
                success:function(resposne){
                    //console.log("RESPONSE");
                    //console.log(resposne);
                    var html = '';
                        if(resposne != null && resposne){
                        $.each(resposne, function (index, data) {
                            html += '<li data-code="'+data.code+'" airport_city_id="'+data.id+'" airport_city_name="'+data.value+'" onclick="selectRoundTripAirportCity(this);"><p>'+data.value+'</p></li>'; 
                        });
                        $('#roundTripAirportCityList').show();
                        $('#roundTripAirportCityList').html(html);
                        }
                        else{
                        $('#roundTripAirportCityList').html('<li>Airport not found</li>');
                        }
                }
            })

        }
        jQuery(function($) { // DOM ready and $ alias in scope

            /**
             * Option dropdowns. Slide toggle
             */
            $(".option-heading").on('click', function() {
                $(this).toggleClass('is-active').next(".option-content").stop().slideToggle(500);
            });

        });

        jQuery('#datetimepicker').datetimepicker();
        jQuery('#datetimepicker1').datetimepicker();

        var WEB_URL = "<?php echo base_url(); ?>";

        
        function getHotelCityList(e){
            //console.log("getHotelCityList");
            //console.log($(e).val());
            var city = $(e).val();
            $.ajax({
                url: WEB_URL + 'index.php/ajax/get_hotel_city_list?term='+city,
                type:'GET',
                success:function(response){
                   // console.log(response);
                    var html = '';
                            if(response != null && response){
                                $.each(response, function (index, data) {
                                    html += '<li data-code="'+data.code+'" hotel_city_id="'+data.id+'" hotel_city_name="'+data.label+'" onclick="selectHotelCity(this);"><p>'+data.label+'</p></li>';  
                                    //alert(html);
                                    //console.log(html);
                                });
                                $('#hotelCityList').show();
                                $('#hotelCityList').html(html);
                            }
                            else{
                            $('#hotelCityList').html('<li>city not found</li>');
                            }
                },
                error: function(error) {
                    console.log(error);
                    //alert('failed');
                }
            });
        }
        // form oneway start
        function getDepartureAirportCity(e){
            
            //console.log("getDepartureAirportCity");
            //console.log($(e).val());

            var enter_city = $(e).val();
            $.ajax({
                url: WEB_URL + 'index.php/ajax/get_airport_code_list?term='+enter_city,
                type: 'GET',
                success:function(resposne){
                    //console.log("RESPONSE");
                    //console.log(resposne);
                    var html = '';
                        if(resposne != null && resposne){
                        $.each(resposne, function (index, data) {
                            html += '<li data-code="'+data.code+'" airport_city_id="'+data.id+'" airport_city_name="'+data.value+'" onclick="selectAirportCity(this);"><p>'+data.value+'</p></li>'; 
                        });
                        $('#airportCityList').show();
                        $('#airportCityList').html(html);
                        }
                        else{
                        $('#airportCityList').html('<li>Airport not found</li>');
                        }
                }
            })

        }

        function  getDestinationAirportCity(e){
            
            //console.log("getDepartureAirportCity");
            //console.log($(e).val());

            var enter_city = $(e).val();
            $.ajax({
                url: WEB_URL + 'index.php/ajax/get_airport_code_list?term='+enter_city,
                type: 'GET',
                success:function(resposne){
                   // console.log("RESPONSE");
                    //console.log(resposne);
                    var html = '';
                        if(resposne != null && resposne){
                        $.each(resposne, function (index, data) {
                            html += '<li data-code="'+data.code+'" airport_city_id="'+data.id+'" airport_city_name="'+data.value+'" onclick="selectAirportCity2(this);"><p>'+data.value+'</p></li>'; 
                        });
                        $('#airportCityList_2').show();
                        $('#airportCityList_2').html(html);
                        }
                        else{
                        $('#airportCityList_2').html('<li>Airport not found</li>');
                        }
                }
            })


        }
        // form oneway end 

        function  getRoundTripDestinationAirportCity(e){
            //console.log("getDepartureAirportCity");
            //console.log($(e).val());
            var enter_city = $(e).val();
            $.ajax({
                url: WEB_URL + 'index.php/ajax/get_airport_code_list?term='+enter_city,
                type: 'GET',
                success:function(resposne){
                   // console.log("RESPONSE");
                    //console.log(resposne);
                    var html = '';
                        if(resposne != null && resposne){
                        $.each(resposne, function (index, data) {
                            html += '<li data-code="'+data.code+'" airport_city_id="'+data.id+'" airport_city_name="'+data.value+'" onclick="selectRoundTripAirportCity2(this);"><p>'+data.value+'</p></li>'; 
                        });
                        $('#roundTripAirportCityList2').show();
                        $('#roundTripAirportCityList2').html(html);
                        }
                        else{
                        $('#roundTripAirportCityList2').html('<li>Airport not found</li>');
                        }
                }
            })
        }

        // form roundtrip end

        // form multicity start
        function getMultiCityDepartureAirportCity(e){
            
            //console.log("getDepartureAirportCity");
            //console.log($(e).val());

            var enter_city = $(e).val();
            $.ajax({
                url: WEB_URL + 'index.php/ajax/get_airport_code_list?term='+enter_city,
                type: 'GET',
                success:function(resposne){
                    //console.log("RESPONSE");
                    //console.log(resposne);
                    var html = '';
                        if(resposne != null && resposne){
                        $.each(resposne, function (index, data) {
                            html += '<li data-code="'+data.code+'" airport_city_id="'+data.id+'" airport_city_name="'+data.value+'" onclick="selectMultiCityAirportCity(this);"><p>'+data.value+'</p></li>'; 
                        });
                        $('#multiCityAirportCityList').show();
                        $('#multiCityAirportCityList').html(html);
                        }
                        else{
                        $('#multiCityAirportCityList').html('<li>Airport not found</li>');
                        }
                }
            })

        }

        function  getMultiCityDestinationAirportCity(e){
            //console.log("getDepartureAirportCity");
            //console.log($(e).val());
            var enter_city = $(e).val();
            $.ajax({
                url: WEB_URL + 'index.php/ajax/get_airport_code_list?term='+enter_city,
                type: 'GET',
                success:function(resposne){
                   // console.log("RESPONSE");
                    //console.log(resposne);
                    var html = '';
                        if(resposne != null && resposne){
                        $.each(resposne, function (index, data) {
                            html += '<li data-code="'+data.code+'" airport_city_id="'+data.id+'" airport_city_name="'+data.value+'" onclick="selectMultiCityAirportCity2(this);"><p>'+data.value+'</p></li>'; 
                        });
                        $('#multiCityAirportCityList2').show();
                        $('#multiCityAirportCityList2').html(html);
                        }
                        else{
                        $('#multiCityAirportCityList2').html('<li>Airport not found</li>');
                        }
                }
            })
        }
        // multicity repeater start

        function getMultiCityDepartureAirportCity2(e){
            //console.log("getDepartureAirportCity");
            //console.log($(e).val());

            var enter_city = $(e).val();
            let data_val = $(e).attr('data-deptval');
            //alert(data_val); return;
            $.ajax({
                url: WEB_URL + 'index.php/ajax/get_airport_code_list?term='+enter_city,
                type: 'GET',
                success:function(resposne){
                    //console.log("RESPONSE");
                    //console.log(resposne);
                    var html = '';
                        if(resposne != null && resposne){
                        $.each(resposne, function (index, data) {
                            html += '<li data-val="'+data_val+'" class="multiCityDepart" data-code="'+data.code+'" airport_city_id="'+data.id+'" airport_city_name="'+data.value+'"><p>'+data.value+'</p></li>'; 
                        });
                        $('#multiCityAirportCityList'+data_val).show();
                        $('#multiCityAirportCityList'+data_val).html(html);
                        }
                        else{
                        $('#multiCityAirportCityList'+data_val).html('<li>Airport not found</li>');
                        }
                }
            })
        }

        $(document).on('click', '.multiCityDepart',  function(e) {
            let val = $(this).attr('data-val');
            
            $('#multiCityDepartureCityId'+val).val($(this).attr('airport_city_id'));
            $('#multiCityDepartureInput'+val).val($(this).attr('airport_city_name'));
            $('#multiCityDepartureName'+val).text($(this).attr('airport_city_name'));
            $('#multiCityAirportCityList'+val).hide();
            
            
        });

        $(document).on('click', '.multiCityDest',  function(e) {
            let val = $(this).attr('data-val');
            
            $('#multiCityDestinationCityId2'+val).val($(this).attr('airport_city_id'));
            $('#multiCityDestinationInput2'+val).val($(this).attr('airport_city_name'));
            $('#multiCityDestinationName2'+val).text($(this).attr('airport_city_name'));
            $('#multiCityAirportCityList2'+val).hide();

            val++;
            $('#multiCityDepartureCityId'+val).val($(this).attr('airport_city_id'));
            $('#multiCityDepartureInput'+val).val($(this).attr('airport_city_name'));
            $('#multiCityDepartureName'+val).text($(this).attr('airport_city_name'));
            $('#multiCityAirportCityList'+val).hide();
            
        });

        function  getMultiCityDestinationAirportCity2(e){
            //console.log("getDeparalert(enter_city);tureAirportCity");
            //console.log($(e).val());
            var enter_city = $(e).val();
            let data_val   = $(e).attr('data-destval');
            //alert(data_val); return;
            $.ajax({
                url: WEB_URL + 'index.php/ajax/get_airport_code_list?term='+enter_city,
                type: 'GET',
                success:function(resposne){
                   // console.log("RESPONSE");
                   // console.log(resposne);
                    var html = '';
                        if(resposne != null && resposne){
                        $.each(resposne, function (index, data) {
                            html += '<li data-val="'+data_val+'" class="multiCityDest" data-code="'+data.code+'" airport_city_id="'+data.id+'" airport_city_name="'+data.value+'" ><p>'+data.value+'</p></li>'; 
                        });
                        //console.log("append val");
                       // console.log(html);
                        $('#multiCityAirportCityList2'+data_val).show();
                        $('#multiCityAirportCityList2'+data_val).html(html);
                        }
                        else{
                        $('#multiCityAirportCityList2'+data_val).html('<li>Airport not found</li>');
                        }
                }
            })
        }
        
       // multicity repeater end

       // form multicity end
        
        function getTransferLocation(e){
            //console.log("getTransferLocation");
            //console.log($(e).val());
           // console.log(WEB_URL);return;
            var loc =$(e).val();
            $.ajax({
                url: WEB_URL + 'index.php/ajax/get_sightseen_city_list?term='+loc,
                success:function(response){
                    //console.log(response);
                    var html = '';
                        if(response != null && response){
                            $.each(response, function (index, data) {
                                html += '<li data-code="'+data.code+'" transfer_city_id="'+data.id+'" transfer_city_name="'+data.label+'" onclick="selectTransferCity(this);"><p>'+data.label+'</p></li>'; 
                            });
                            $('#transferLocationList').show();
                            $('#transferLocationList').html(html);
                            //console.log(html);
                        }
                        else{
                        $('#transferLocationList').html('<li>Airport not found</li>');
                        }
                },
                error:function(error){
                    console.log('error');
                }

            });
        }
        
        
//flight class selection append to placeholder for oneway transfer
function oneWayflightClassSelection(){
    let class_val = document.getElementById("oneWayFlightClass").value;
    $('#oneWayFlightClassName').text(class_val);
}

// //flight class selection append to placeholder for oneway roundtrip
function roundTripFlightClassSelection(){
    let class_val = document.getElementById("roundTripFlightClass").value;
    $('#roundTripflightClassName').text(class_val);
}

//flight class selection append to placeholder for oneway multicity
function multiCityFlightClassSelection(){
    let class_val = document.getElementById("multiCityflightClass").value;
    $('#multiCityFlightClassName').text(class_val);
}
// for oneway start
function selectAirportCity(e){
    //console.log("TTTTTTT");
    $('#departureCityId').val($(e).attr('airport_city_id'));
    $('#departureInput').val($(e).attr('airport_city_name'));
    $('#departureName').text($(e).attr('airport_city_name'));
    $('#airportCityList').hide();
}


function selectAirportCity2(e){
//console.log("TTTTTTT");
    $('#destinationCityId').val($(e).attr('airport_city_id'));
    $('#destinationInput').val($(e).attr('airport_city_name'));
    $('#destinationName').text($(e).attr('airport_city_name'));
    $('#airportCityList_2').hide();
}
// for oneway end
// for roundtrip start
function selectRoundTripAirportCity(e){
    //console.log("TTTTTTT");
    $('#roundTripDepartureCityId').val($(e).attr('airport_city_id'));
    $('#roundTripDepartureInput').val($(e).attr('airport_city_name'));
    $('#roundTripDepartureName').text($(e).attr('airport_city_name'));
    $('#roundTripAirportCityList').hide();
}
function selectRoundTripAirportCity2(e){
//console.log("TTTTTTT");
    $('#roundTripDestinationCityId').val($(e).attr('airport_city_id'));
    $('#roundTripDestinationInput').val($(e).attr('airport_city_name'));
    $('#roundTripDestinationName').text($(e).attr('airport_city_name'));
    $('#roundTripAirportCityList2').hide();
}
// for roundtrip end 
// for multicity start
function selectMultiCityAirportCity(e){
    //console.log("TTTTTTT");
    $('#multiCityDepartureCityId').val($(e).attr('airport_city_id'));
    $('#multiCityDepartureInput').val($(e).attr('airport_city_name'));
    $('#multiCityDepartureName').text($(e).attr('airport_city_name'));
    $('#multiCityAirportCityList').hide();
}
function selectMultiCityAirportCity2(e){
//console.log("TTTTTTT");
    $('#multiCityDestinationCityId').val($(e).attr('airport_city_id'));
    $('#multiCityDestinationInput').val($(e).attr('airport_city_name'));
    $('#multiCityDestinationName').text($(e).attr('airport_city_name'));
    $('#multiCityAirportCityList2').hide();
   
    $('#multiCityDepartureCityId0').val($(e).attr('airport_city_id'));
    $('#multiCityDepartureInput0').val($(e).attr('airport_city_name'));
    $('#multiCityDepartureName0').text($(e).attr('airport_city_name'));
    $('#multiCityAirportCityList0').hide();
}

// for multicity end 
function selectHotelCity(e){
//console.log("TTTTTTT");
//   $('#hotel_destination_search_name').val($(e).attr('hotel_city_name'));
    $('#hotel_destination').val($(e).attr('hotel_city_id'))
    $('#cityName').text($(e).attr('hotel_city_name'));
    $('#cityInput').val($(e).attr('hotel_city_name'));
    $('#hotelCityList').hide();
}

function selectTransferCity(e){
    $('#transferLocation').val($(e).attr('transfer_city_id'));
    $('#locationName').text($(e).attr('transfer_city_name'));
    $('#locationInput').val($(e).attr('transfer_city_name'));
    $('#transferLocationId').val($(e).attr('transfer_city_id'));
    $('#transferLocationList').hide();
}
    </script>

   
<script type="text/javascript">

    // for oneway flight start
    function change_travellercount_oneway() {
        let total = parseInt($('#adult').val()) + parseInt($('#child').val()) + parseInt($('#infant').val());
        $('#change_travellercount_oneway_display').text(total);
    }

    // for oneway flight end 

     // for roundtrip start
    function change_travellercount_roundtrip() {
        let total = parseInt($('#adult').val()) + parseInt($('#child').val()) + parseInt($('#infant').val());
        $('#change_travellercount_roundtrip_display').text(total);
    }
    // for roundtrip end 

    // for multicity start
    function change_travellercount_multicity() {
        let total = parseInt($('#adult').val()) + parseInt($('#child').val()) + parseInt($('#infant').val());
        $('#change_travellercount_multicity_display').text(total);
    }
    // for multicity end 


    function change_hoteltravellercount(){

        let totalCount = parseInt($('#adult').val()) + parseInt($('#child').val()) + parseInt($('#infant').val
        ());
        $('#change_hoteltravellercount_here').text(totalCount);
    }
</script>

<!-- Form Validation  & flight tab change hide & show -->
<script>
        (function($) {
            $( document ).ready(function() {
                var counter = 0;
                var count = 0;
                var date_count =4;
                //$("#onewayFlightSearch").removeClass("hidden");
                $("#roundtripFlightSearch").addClass("hidden");
                $("#multicityFlightSearch").addClass("hidden");

                var val = $("input[name='trip_type']:checked").val();
                if(val == 'oneway'){
                   $("#roundtripFlightSearch").addClass("hidden");
                   $("#multicityFlightSearch").addClass("hidden");
                }
                $('#trip_mode').change(function(){
                    var val = $("input[name='trip_type']:checked").val();
                    if( val == 'oneway'){
                        $("#onewayFlightSearch").removeClass("hidden");
                        $("#roundtripFlightSearch").addClass("hidden");
                        $("#multicityFlightSearch").addClass("hidden");
                    } else if(val == 'circle'){//    alert(val);
                       $("#roundtripFlightSearch").removeClass("hidden");
                       $("#onewayFlightSearch").addClass("hidden");
                       $("#multicityFlightSearch").addClass("hidden");

                    } 
                    else if(val == 'multicity'){
                       //alert(count);
                       $("#multicityFlightSearch").removeClass("hidden");
                       $("#onewayFlightSearch").addClass("hidden");
                       $("#roundtripFlightSearch").addClass("hidden");

                       let html = '<div class="multi-box" id="multi_city_row">';
                        html +='<div class="pull_left destination_field">';
                        html +='<label>From</label>';
                        html +='<input  class="multiCityDeparture" type="text" placeholder="Departure" list="browsers" id="multiCityDepartureInput'+count+'" data-deptval="'+count+'"  name="from[]" onkeyup="getMultiCityDepartureAirportCity2(this)">';
                        html +='<input type="hidden" name="from_loc_id[]" id="multiCityDepartureCityId'+count+'">';
                        html +='<ul id="multiCityAirportCityList'+count+'" class="airport_list"></ul>';
                        html +='<p id="multiCityDepartureName'+count+'">Departure City</p>';
                        html +='</div>';
                        html +='<div class="pull_left destination_field">';
                        html +='<label>To</label>';
                        html +='<input  class="placeholder-setttings" name="to[]" type="text" placeholder="Destination" id="multiCityDestinationInput2'+count+'" data-destval="'+count+'" onkeyup="getMultiCityDestinationAirportCity2(this);">';
                        html +='<input type="hidden" name="to_loc_id[]" id="multiCityDestinationCityId2'+count+'">';
                        html +='<ul id="multiCityAirportCityList2'+count+'" class="airport_list2"></ul>';
                        html +='<p id="multiCityDestinationName2'+count+'">Destination City</p>';
                        html +='</div>';
                        html +='<div class="pull_left check_in_field">';
                        html +='<label>Depature <i class="fa fa-caret-down"></i></label>';
                        html +='<input type="text"  name="depature[]" class="placeholder-setttings" id="datepicker0'+date_count+'" placeholder="<?=date('d-m-Y'); ?>">';
                        html +='</div>';
                        html +='</div>';

                        $(".multi-add").append(html);
                        date_count++;
                        
                        $("#datepicker, #datepicker01, #datepicker02, #datepicker03, #datepicker04, #datepicker05, #datepicker06, #datepicker07, #datepicker08, #datepicker09").datepicker({
                            numberOfMonths: 2,
                            //dateFormat: 'd-m-y'
                            dateFormat: 'dd-mm-yy'
                        });
                    }
                });
                 
                // multiple city appending on click
            
                $("#addCity").on('click', function(){
                    count ++;
                    //alert(count);
                    let html ='<div class="multi-box" id="multi_city_row'+count+'">';
                        html +='<div class="pull_left destination_field">';
                        html +='<label>From</label>';
                        html +='<input  class="placeholder-setttings" type="text" placeholder="Departure" list="browsers" id="multiCityDepartureInput'+count+'" data-deptval="'+count+'" name="from[]" onkeyup="getMultiCityDepartureAirportCity2(this)">';
                        html +='<input type="hidden" name="from_loc_id[]" id="multiCityDepartureCityId'+count+'">';
                        html +='<ul id="multiCityAirportCityList'+count+'" class="airport_list"></ul>';
                        html +='<p id="multiCityDepartureName'+count+'">Departure City</p>';
                        html +='</div>';
                        html +='<div class="pull_left destination_field">';
                        html +='<label>To</label>';
                        html +='<input  class="placeholder-setttings" name="to[]" type="text" placeholder="Destination" id="multiCityDestinationInput2'+count+'"  data-destval="'+count+'"  onkeyup="getMultiCityDestinationAirportCity2(this);">';
                        html +='<input type="hidden" name="to_loc_id[]" id="multiCityDestinationCityId2'+count+'">';
                        html +='<ul id="multiCityAirportCityList2'+count+'" class="airport_list2"></ul>';
                        html +='<p id="multiCityDestinationName2'+count+'">Destination City</p>';
                        html +='</div>';
                        html +='<div class="pull_left check_in_field">';
                        html +='<label>Depature <i class="fa fa-caret-down"></i></label>';
                        html +='<input type="text"  name="depature[]" class="placeholder-setttings" id="datepicker0'+date_count+'" placeholder="<?=date('d-m-Y'); ?>">';
                        html +='<div class="close-multy-box" id="remove_multicity" data-value="'+count+'">';
                        html +='<i class="fa fa-times-circle"></i>';
                        html +='</div>';
                        html +='</div>';
                        html +='</div>';
                        
                        if(counter < 3){
                            counter++;
                            $(".multi-add").append(html);
                            date_count++;
                        } else {
                            alert('Sorry, Unable to add more,the maximum number was reached !');
                        }  
                        $("#datepicker, #datepicker01, #datepicker02, #datepicker03, #datepicker04, #datepicker05, #datepicker06, #datepicker07, #datepicker08, #datepicker09").datepicker({
                            numberOfMonths: 2,
                            //dateFormat: 'd-m-y'
                            dateFormat: 'dd-mm-yy'
                        });
                });

                $(document).on('click', '.close-multy-box',  function(events) {
                    counter--;
                    let counter_val = $(this).data('value');
                    $('#multi_city_row'+counter_val).remove();
                });

                // hotel room & passengers count 
                var roomCounter = 1;
                $('.hotelTravelerCount').one('click', function(){
                    $('.hotelTravelerCount').text('1 Adults, 0 Childs, 1 Rooms ');
                    let roomHtml  ='<div class="oneroom fltravlr">';
                        roomHtml +='<h3>Room '+roomCounter+'</h3>';
                        roomHtml +='<div class="roomrow">';
                        roomHtml +='<div class="celroe col-xs-8">Adults';
                        roomHtml +='<span class="agemns">Ages (12+)</span>';
                        roomHtml +='</div>';
                        roomHtml +='<div class="celroe col-xs-4">';
                        roomHtml +='<div class="input-group countmore"> <span class="input-group-btn">';
                        roomHtml +='<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="adult"><span class="glyphicon glyphicon-minus"></span></button>';
                        roomHtml +='</span>';
                        roomHtml +='<input type="text" class="form-control input-number centertext" onchange="change_hoteltravellercount()" value="1" min="1" max="10" name="adult[]" id="adult">';
                        roomHtml +='<span class="input-group-btn">';
                        roomHtml +='<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="adult"> <span class="glyphicon glyphicon-plus"></span></button>';
                        roomHtml +='</span>';
                        roomHtml +='</div>';
                        roomHtml +='</div>';
                        roomHtml +='</div>';
                        roomHtml +='<div class="roomrow">';
                        roomHtml +='<div class="celroe col-xs-8">Children';
                        roomHtml +='<span class="agemns">Ages (2-17)</span>';
                        roomHtml +='</div>';
                        roomHtml +='<div class="celroe col-xs-4">';
                        roomHtml +='<div class="input-group countmore"><span class="input-group-btn">';
                        roomHtml +='<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="child"><span class="glyphicon glyphicon-minus"></span></button>';
                        roomHtml +='</span>';
                        roomHtml +='<input type="text" class="form-control input-number centertext"onchange="change_hoteltravellercount()" value="1" min="0" max="10" name="child[]" id="child"><span class="input-group-btn">';
                        roomHtml +='<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="child"> <span class="glyphicon glyphicon-plus"></span> </button>';
                        roomHtml +='</span>';
                        roomHtml +='</div>';
                        roomHtml +='</div>';
                        roomHtml +='</div>';
                        roomHtml +='</div>';
                        //roomHtml +='<a id="addRoom" href="#" class="addRooms">+ Add other room</a>';
                        // let roomHtml ='<h2>room block</h2>';
                    $('#hotelRoomBlk').append(roomHtml);
                    roomCounter++;
                });

                $('.addRooms').on('click', function(){ 
                
                    let roomHtml='<a id="removeRoom'+roomCounter+'" data-val="'+roomCounter+'" class="removeRooms">- Remove room</a>'; 
                        roomHtml +='<div class="oneroom fltravlr">';
                        roomHtml +='<h3>Room '+roomCounter+'</h3>';
                        roomHtml +='<div class="roomrow">';
                        roomHtml +='<div class="celroe col-xs-8">Adults';
                        roomHtml +='<span class="agemns">Ages (12+)</span>';
                        roomHtml +='</div>';
                        roomHtml +='<div class="celroe col-xs-4">';
                        roomHtml +='<div class="input-group countmore"> <span class="input-group-btn">';
                        roomHtml +='<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="adult"><span class="glyphicon glyphicon-minus"></span></button>';
                        roomHtml +='</span>';
                        roomHtml +='<input type="text" class="form-control input-number centertext" onchange="change_hoteltravellercount()" value="1" min="1" max="10" name="adult[]" id="adult">';
                        roomHtml +='<span class="input-group-btn">';
                        roomHtml +='<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="adult"> <span class="glyphicon glyphicon-plus"></span></button>';
                        roomHtml +='</span>';
                        roomHtml +='</div>';
                        roomHtml +='</div>';
                        roomHtml +='</div>';
                        roomHtml +='<div class="roomrow">';
                        roomHtml +='<div class="celroe col-xs-8">Children';
                        roomHtml +='<span class="agemns">Ages (2-17)</span>';
                        roomHtml +='</div>';
                        roomHtml +='<div class="celroe col-xs-4">';
                        roomHtml +='<div class="input-group countmore"><span class="input-group-btn">';
                        roomHtml +='<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="child"><span class="glyphicon glyphicon-minus"></span></button>';
                        roomHtml +='</span>';
                        roomHtml +='<input type="text" class="form-control input-number centertext"onchange="change_hoteltravellercount()" value="1" min="0" max="10" name="child[]" id="child"><span class="input-group-btn">';
                        roomHtml +='<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="child"> <span class="glyphicon glyphicon-plus"></span> </button>';
                        roomHtml +='</span>';
                        roomHtml +='</div>';
                        roomHtml +='</div>';
                        roomHtml +='</div>';
                        roomHtml +="</div>";
                        //roomHtml +='<a id="removeRoom'+roomCounter+'" href="#">- Remove room</a>';
                        if(roomCounter < 4){
                            $('#hotelRoomBlk').append(roomHtml);
                            roomCounter++;
                        } else{
                            alert('Sorry, Unable to add more,the maximum number was reached !');
                        }
                });
                
                
                //$(document).on('click', '.removeRooms',  function(events) {
                /*$('.removeRooms').on('click', function(){
                    alert('dsfhjhsa');
                    console.log('removehdsfhjaswedh');
                });
*/
                


            });

                $(document).on('click', '.removeRooms',  function(events) {
                    // counter--;
                    // let counter_val = $(this).data('value');
                    // $('#multi_city_row'+counter_val).remove();
                    alert();
                });

        })(jQuery);


        // $( document ).ready(function() {
        //    // $(document).on('click', '.removeRooms',  function(events) {
        //     $('.removeRooms').on('click', function(){
        //       alert('remove');
        //     });
        // });
        // hotel checkin date 
        // $('.hotel_checkin').datepicker({
        //     dateFormat: "dd-mm-yy",
        //     minDate: new Date(),
        //     onSelect: function(date) {
        //         var checkin = date;
        //         var date = new Date();
        //         //alert(date);
        //         // var nextDayDate = new Date();
        //         // nextDayDate.setDate(checkin.getDate() + 1);
                
        //         //  alert(nextDayDate);
        //         $('.hotel_checkout').val(nextDayDate);
        //     }
        // });
        
    </script>
    <script>
        $("#datepicker, #datepicker01, #datepicker02, #datepicker03, #datepicker04, #datepicker05, #datepicker06, #datepicker07, #datepicker08, #datepicker09,#datepicker10, #datepicker11").datepicker({
            numberOfMonths: 2,
            //dateFormat: 'd-m-y'
            dateFormat: 'dd-mm-yy'
        });

        $('#datepicker').datepicker({
            numberOfMonths: 1,
            //dateFormat: "dd mm y",
            dateFormat: "dd-mm-yy",
            minDate: new Date()
            // showOn: 'focus'
        });
    </script>

    <script type="text/javascript">
        function update_count_passanger(type, count_type, id) {
            var cont = $('#'+id).val();
            if(count_type == 'minus')
                cont = parseInt(cont) - 1;
            else
                cont = parseInt(cont) + 1;
            if(type == 'adult'){
                if(cont <= 0){
                    $('#'+id).val(0);
                }if(cont >= 10){
                    $('#'+id).val(10);
                }else{
                    $('#'+id).val(cont);
                }
            }else{
                if(cont <= 0){
                    $('#'+id).val(0);
                }if(cont >= 2){
                    $('#'+id).val(2);
                }else{
                    $('#'+id).val(cont);
                }
            }
            
        }
    </script>
</body>

</html>