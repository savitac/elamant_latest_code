<div class="totopp">
	   	<div class="col-lg-8 nopad cum_mrg">
	      <div class="tabbable customtab">
	         <ul class="nav nav-tabs nav-tabs-responsive">
	            <li class="active"> <a data-toggle="tab" href="#Flight"> <span class="text"><span class="spanfa sprite faflight"></span>Flights</span> </a> </li>
	            <li class="next"> <a data-toggle="tab" href="#Hotels"> <span class="text"><span class="spanfa sprite fahotel"></span>Hotels</span> </a> </li>
	            <li> <a data-toggle="tab" href="#Villa"> <span class="text"><span class="spanfa sprite faflight"></span>Villa</span> </a> </li>
	            <li class="next"> <a data-toggle="tab" href="#Bus"> <span class="text"><span class="spanfa sprite fabus"></span>Bus</span> </a> </li>
	         </ul>
	         <div class="tab-content">
	              <?php $this->load->view('b2b/flight_search'); ?>
	              <?php $this->load->view('b2b/hotel_search'); ?>
	             
	         </div>
	      </div>
    <div class="col-lg-3 right_side_dash">
      <div class="dash_inside">
      	<div id="owl-demobaners1" class="owl-carousel owlindex2 owl-theme">
        	<div class="item">
            	<div class="bannersrgt1">
                    <img src="<?php echo ASSETS?>assets/images/banner1.jpg" alt="" />
                </div>
            </div>
            <div class="item">
            	<div class="bannersrgt1">
                    <img src="<?php echo ASSETS?>assets/images/banner1.jpg" alt="" />
                </div>
            </div>
        </div>
      </div>
    </div>
   </div>
</div>
<!--search_tab end-->
<div class="clearfix"></div>
 <?php if($content!=''){ $divClass = '';for($c = 0 ; $c < count($content); $c++) { 
	if($c == 0){ if(count($content) % 2 == 0){ $divClass = "blockstwo"; }else{ $divClass = "blocksone"; } } 
	if($c != 0){ if($divClass == "blockstwo"){ $divClass = "blocksone"; }else{ $divClass = "blockstwo"; }}   
	if($c == (count($content)- 1)){ $divClass = "blocksone"; } ?>
	<?php
      if(isset($_SESSION['TheChinaGap']['language'])){ 
        $display_language = $_SESSION['TheChinaGap']['language'];
      } else {
        $display_language = $_SESSION['TheChinaGap']['language'] = BASE_LANGUAGE;
      } 
?>
	     <div class="<?php echo $divClass; ?> chcnt">
			 <div class="container">
				<div class="outblock">			
					<?php if(($c+1) % 2 == 0) { ?>	
                    	<div class="col-xs-6 centerchange imagingblockright">
							<div class="blocksimg">
								<img src="<?php echo ASSETS.'cpanel/uploads/home/'.$content[$c]->content_image; ?>" alt="" />
							</div>
						</div>	
						<div class="col-xs-6 centerchange">
							<div class="blockhesd">
								 <?php 
						          if($display_language=='english') $content_title = $content[$c]->content_title; 
						          else $content_title = $content[$c]->content_title_china; ?>
								<?php  echo $content_title; ?>
								 </div>
								  <?php 
						          if($display_language=='english') $content_description = $content[$c]->content_description; 
						          else $content_description = $content[$c]->content_description_china; ?>
							<?php echo $content_description; ?>
							<a class="lmoreblock"><?php echo $this->TravelLights['Login']['LearnMore']; ?></a>
						</div>
						
					<?php }else{ ?>
						<div class="col-xs-6 centerchange imagingblockleft">
							<div class="blocksimg">
								<img src="<?php echo ASSETS.'cpanel/uploads/home/'.$content[$c]->content_image; ?>" alt="" />
							</div>
						</div>
						<div class="col-xs-6 centerchange">
							<div class="blockhesd">
								<?php 
						          if($display_language=='english') $content_title = $content[$c]->content_title; 
						          else $content_title = $content[$c]->content_title_china; ?>
								<?php  echo $content_title; ?> 
							</div>
							<?php //echo '<pre>'; print_r($content[$c]->content_description); ?>
							<?php 
						          if($display_language=='english') $content_description = $content[$c]->content_description; 
						          else $content_description = $content[$c]->content_description_china; ?>
                            <?php echo $content_description; ?>
							<a class="lmoreblock"><?php echo $this->TravelLights['Login']['LearnMore']; ?></a>
						</div>
					<?php } ?>	
				</div>
			</div>
		</div>	
	  <?php }} ?> 

<div class="clearfix"></div>
    <section>
        <div class="book_us">
            <div class="container">
                <div class="col-sm-3 text-center">
                    <img src="<?php echo ASSETS; ?>/assets/images/bookwithus_img1.png">
                    <span class="txt_bookus">Easy Bookings</span>
                </div>
                <div class="col-sm-3 text-center">
                    <img src="<?php echo ASSETS; ?>/assets/images/bookwithus_img2.png">
                    <span class="txt_bookus"> Best Prices and Deals Packages</span>
                </div>
                <div class="col-sm-3 text-center">
                    <img src="<?php echo ASSETS; ?>/assets/images/bookwithus_img3.png">
                    <span class="txt_bookus">100% secure payment</span>
                </div>
                <div class="col-sm-3 text-center">
                    <img src="<?php echo ASSETS; ?>/assets/images/bookwithus_img4.png">
                    <span class="txt_bookus">Customer Satisfaction</span>
                </div>
            </div>
        </div>
    </section>