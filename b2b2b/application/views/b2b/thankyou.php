<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?= $this->session->userdata('company_name'); ?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" />
	<link href="<?php echo ASSETS;?>assets/css/index.css" rel="stylesheet" />
</head>
<body>
	<?php echo $this->load->view('core/header'); ?>	
	<div class="agent_login_wrap top80">
   		<div class="container">
  		 <div class="loginsreset thanklogin">
          	<div class="agenthed"><?php echo $this->TravelLights['Register']['ThankForChoosing']; ?> </div>
            <div class="signdiv">  
              <div class="insigndiv">
                	<span class="parareserve">Thankyou For Registration.</span>
                	<span class="parareserve">Please Check email or proceed for Login</span> 
              </div>
            </div>
          </div>
        </div>
   
</div>

	<?php echo $this->load->view('core/bottom_footer'); ?>

	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/backslider.js"></script> 
	
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
	 
		<script type="text/javascript">
		$(document).ready(function(){
		$('.scrolltop').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
		 });
		 
		 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
		 
		$(function($){
		
					var url = '<?php echo ASSETS;?>assets/images/';
					
					
					$.supersized({
					
						// Functionality
						slide_interval          :   5000,		// Length between transitions
						transition              :   1, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
						transition_speed		:	700,		// Speed of transition
																   
						// Components							
						slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
						slides 					:  	[			// Slideshow <?php echo ASSETS;?>assets/images/
													{image : url+'/slide1.jpg' ,title : '  instantly bookable' ,description:'access a huge range of', price:'competitively-priced global travel products'},
													{image : url+'/slide2.jpg' ,title : 'FIT QUOTES IN 5 MINUTES' ,description:'create'},
													{image : url+'/slide3.jpg' ,title : 'COMPETE EFFECTIVELY FOR FIT' ,description:'help travel agencies'}
													]
						
					});
				});

	});
	</script> 

</body>
</html>
