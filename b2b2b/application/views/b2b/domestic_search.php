<?php if(isset($flight_search_params['adult_config']) == false || intval($flight_search_params['adult_config']) < 1) {
	$flight_search_params['adult_config'] = 1;
} 
?>
	<div id="Domestic" class="tab-pane">
		<form autocomplete="off" onSubmit="return validateform();" action="<?php echo base_url(); ?>index.php/flight/pre_crs_flight_search" name="flight_search_form" id="flight_search_form">
			<div class="intabs">
			  <div class="waywy">
				<div class="smalway">
					<label class="wament hand-cursor active">
						<input class="hide1" type="radio" name="trip_type" id="onew-trp_demostic" value="oneway" checked> One Way</label>
						<label class="wament hand-cursor">
						<input class="hide1" type="radio" name="trip_type" id="rnd-trp_demostic" value="circle"> Round Way</label>
						<label class="wament hand-cursor hide"><input class="hide" type="radio" name="trip_type" id="multi-trp" value="multicity"> Multi City</label></div>
						
			  <div class="clearfix"></div>
			  <div class="outsideserach" id="normal">
				<div class="col-lg-6 col-md-6 col-sm-6 fiveh"> 
				  <div class="marginbotom10">
				  <span class=" formlabel">From </span>
				  <div class="relativemask "> <span class="maskimg  hfrom"></span>
					<input type="text" name="from" id="from" value="" placeholder="Airport City" class="fromflight_demo ft" required >
					<input class="hide loc_id_holder" name="from_loc_id" type="hidden" value="">
				  </div>
				  </div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 fiveh"> 
				  <div class="marginbotom10">
				  <span class=" formlabel">To</span>
				  <div class="relativemask"> <span class="maskimg  hfrom"></span>
					<input type="text" name="to" id="to" value="" placeholder="Airport City" class="departflight_demo ft" required>
					<input class="loc_id_holder" name="to_loc_id" type="hidden" value="">
				  </div>
				  </div>
				</div>
				
				<div class="col-md-12 nopad">
				  <div class="col-xs-6 fiveh"> 
					<div class="marginbotom10">
					<span class=" formlabel">Departure</span>
					<div class="relativemask"> <span class="maskimg   caln"></span>
					  <input type="text"   id="st_date_domestic" name="depature" placeholder="Depature Date" value="" class="forminput" required>
					</div>
					</div>
				  </div>
				  <div class="col-xs-6 fiveh" id="returns"> 
					<div class="marginbotom10">
					<span class=" formlabel">Return</span>
					<div class="relativemask"> <span class="maskimg caln"></span>
					  <input type="text" id="en_date_domestic" name="return" placeholder="Return Date" value="" class="forminput" required>
					</div>
					</div>
				  </div>
				</div>
		
		<div class="clearfix"></div></div></div>
	    <div class="interst_clone_wrapper"></div>
		<div class="clearfix"></div>
 <div class="col-xs-6 padfive">
					<div class="lablform">&nbsp;</div>
					<div class="totlall">
						<span class="remngwd"><span class="total_pax_count">1</span> Traveller(s)</span>
						<div class="roomcount pax_count_div">
							<div class="inallsn">
								<div class="oneroom fltravlr">
									<div class="roomrow">
										<div class="celroe col-xs-4">Adults
											<span class="agemns">(12+)</span>
										</div>
										<div class="celroe col-xs-8">
											<div class="input-group countmore pax-count-wrapper adult_count_div"> <span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="minus" data-field="adult"> <span class="glyphicon glyphicon-minus"></span> </button>
												</span>
												<input type="text" id="OWT_adult" name="adult" class="form-control input-number centertext valid_class pax_count_value" value="<?=(int)@$flight_search_params['adult_config']?>" min="1" max="9" readonly>
												<span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="adult"> <span class="glyphicon glyphicon-plus"></span> </button>
												</span> 
											</div>
										</div>
									</div>
									<div class="roomrow">
										<div class="celroe col-xs-4">Children
											<span class="agemns">(2-11)</span>
										</div>
										<div class="celroe col-xs-8">
											<div class="input-group countmore pax-count-wrapper child_count_div"> <span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="minus" data-field="child"> <span class="glyphicon glyphicon-minus"></span> </button>
												</span>
												<input type="text" id="OWT_child" name="child" class="form-control input-number centertext pax_count_value" value="<?=(int)@$flight_search_params['child_config']?>" min="0" max="9" readonly>
												<span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="child"> <span class="glyphicon glyphicon-plus"></span> </button>
												</span> 
											</div>
										</div>
									</div>
									<div class="roomrow">
										<div class="celroe col-xs-4">Infants
											<span class="agemns">(0-2)</span>
										</div>
										<div class="celroe col-xs-8">
											<div class="input-group countmore pax-count-wrapper infant_count_div"> <span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number" data-type="minus" data-field="infant"> <span class="glyphicon glyphicon-minus"></span> </button>
												</span>
												<input type="text" id="OWT_infant" name="infant" class="form-control input-number centertext pax_count_value" value="<?=(int)@$flight_search_params['infant_config']?>" min="0" max="9" readonly>
												<span class="input-group-btn">
						search_flight						<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="infant"> <span class="glyphicon glyphicon-plus"></span> </button>
												</span> 
											</div>
										</div>
									</div>
									<!-- Infant Error Message-->
									<div class="roomrow">
										<div class="celroe col-xs-8">
										<div class="alert-wrapper hide">
										<div role="alert" class="alert alert-error">
											<span class="alert-content"></span>
										</div>
										</div>
										</div>
									</div>
									<!-- Infant Error Message-->
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 nopad">
				  <div class="col-xs-12 fiveh"> 
					<div class="marginbotom10">
					<span class="formlabel">Class</span>
					<div class="selectedwrap">
					  <select class="mySelectBoxClass flyinputsnor">
						<option>All</option>
						<option>Economy With Restrictions</option>
						<option>Economy Without Restrictions</option>
						<option>Economy Premium</option>
						<option>Business</option>
						<option>First</option>
					  </select>
					</div>
				  </div>
				  </div>
				
					
			<div class="col-xs-6 fiveh" style="display: none;"> 
					<div class="marginbotom10">
					<span class="formlabel">Preferred Airline</span>
					<div class="selectedwrap">
					  <select class="mySelectBoxClass flyinputsnor">
						<option>All Airlines</option>
						
					  </select>
					</div>
				  </div>
				  </div>
				  </div> 
				  <div class="col-md-6 fiveh">
					<div class="formsubmit">  
						<input type="hidden" autocomplete="off" name="carrier[]" id="carrier" value="">
						<input type="hidden"   id="search_flight" name="search_flight" placeholder="Depature Date" value="search" class="forminput">
						<input type="hidden" autocomplete="off" name="v_class" id="class" value="All">
						
						<div class="searchsbmtfot">
						<input type="submit" class="searchsbmt" value="search" />
					</div>

						
					</div>
				</div>
				 </div>
	   </form> 
	</div>

     
<?php //echo $this->load->view('dashboard/flight_search_prob_crs'); ?>
  
<?php //echo $this->load->view('dashboard/flight_search_prob'); ?>

   
<script>
	$(document).ready(function($) {

		//$("#onew-trp_demostic").();
		$("#en_date_domestic").datepicker("option","disabled",true);
		$("#rnd-trp_demostic").click(function(event) {
			$("#en_date_domestic").datepicker("option","disabled",false);
		});
		$("#onew-trp_demostic").click(function(event) {
			$("#en_date_domestic").datepicker("option","disabled",true);
		});
	});
	 
</script>