<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>Chinagap</title>
<?php echo $this->load->view('core/load_css'); ?>
  <link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" />
</head>

<body>

<!-- Navigation -->

<?php echo $this->load->view('core/header'); ?> 
<?php
      if(isset($_SESSION['TheChinaGap']['language'])){ 
        $display_language = $_SESSION['TheChinaGap']['language'];
      } else {
        $display_language = $_SESSION['TheChinaGap']['language'] = BASE_LANGUAGE;
      } 
?>

<!-- /Navigation -->
<div class="clearfix"></div>

<div class="agent_login_wrap top80">
		
   		<div class="container">
  		 <div class="static_page_in">
          	<h1><?php 
          if($display_language=='english') $productname = $products[0]->menu_name; 
          else $productname = $products[0]->menu_name_china; ?>
              <?php echo $productname; ?></h1>

            
    <?php for($product =0; $product < count($products); $product++) { ?>
            	<div class="repetprod">
            	<div class="col-xs-3 padrgt1">

                	<img src="<?php echo  site_url().'cpanel/uploads/header/' ?><?php echo $products[$product]->product_image; ?>" alt="" />
                </div>
                <div class="col-xs-9 nopad produconts">
              		
                  <h3>
                    <?php 
                          if($display_language=='english') $product_name = $products[$product]->product_name; 
                          else $product_name = $products[$product]->product_name_china; ?>
                    <?php echo $product_name; ?></h3>
                  <p>
                    <?php 
                          if($display_language=='english') $product_description = $products[$product]->product_description; 
                          else $product_description = $products[$product]->product_description_china; ?>
                    <?php echo $product_description; ?></p>
                 </div>
                 </div>
                
              <?php } ?> 
                 
          </div>
          
          
          
        </div>
   
</div>


<div class="clearfix"></div>
<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>



<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 



<script type="text/javascript">
$(document).ready(function(){
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	 
	
});
</script> 





</body>
</html>
