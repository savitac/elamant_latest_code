
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>Chinagap</title>
<?php echo $this->load->view('core/load_css'); ?>

</head>

<body>
<?php
      if(isset($_SESSION['TravelLights']['language'])){ 
        $display_language = $_SESSION['TravelLights']['language'];
      } else {
        $display_language = $_SESSION['TravelLights']['language'] = BASE_LANGUAGE;
      } 
?>

<?php echo $this->load->view('dashboard/top'); ?> 

<div class="clearfix"></div>

<div class="agent_login_wrap top80 dashtopmargin">
		
   		<div class="container">
  		 <div class="static_page_in">
          	<h1>Carrers</h1>
            	<div class="repetprod">
            	
                <div class="col-xs-12 nopad produconts">
              		<?php for($carrers =0; $carrers < count($carrer); $carrers++) { ?>
                  
                  <p>
                    <?php if($display_language=='english') $carrer_description = $carrer[0]->carrer_description; 
                  else $carrer_description = $carrer[0]->carrer_description_china; ?>
                    <?php echo $carrer_description; ?>
                  </p>
                    <?php } ?>
                 </div>
                 </div>
                 
          </div>
          
          
          
        </div>
   
</div>


<div class="clearfix"></div>
<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 

<script type="text/javascript">
$(document).ready(function(){
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	 
	 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	 
	 
	 
	
});
</script> 





</body>
</html>
