<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>Chinagap</title>
<?php echo $this->load->view('core/load_css'); ?>
<link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" />
</head>

<body>

<!-- Navigation -->

<?php echo $this->load->view('core/header'); ?> 
<!-- /Navigation -->
<div class="clearfix"></div>
<?php
      if(isset($_SESSION['TheChinaGap']['language'])){ 
        $display_language = $_SESSION['TheChinaGap']['language'];
      } else {
        $display_language = $_SESSION['TheChinaGap']['language'] = BASE_LANGUAGE;
      } 
?>
<div class="agent_login_wrap top80">
		
   		<div class="container">
  		 <div class="static_page_in">
          	<h1>Terms & Conditions</h1>
            	<div class="repetprod">
            	
                <div class="col-xs-12 nopad produconts">
              		<?php for($term =0; $term < count($terms); $term++) { ?>
                  
                  <p>
                     <?php if($display_language=='english') $termsconditions_description = $terms[0]->termsconditions_description; 
                  else $termsconditions_description = $terms[0]->termsconditions_description_china; ?>
                    <?php echo $termsconditions_description; ?></p>
                    <?php } ?>
                 </div>
                 </div>
                 
          </div>
          
          
          
        </div>
   
</div>


<div class="clearfix"></div>
<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	 
	 
	 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	
});
</script> 





</body>
</html>
