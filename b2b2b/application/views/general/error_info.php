<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo $title; ?></title>
		<link href="<?php echo ASSETS;?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<style>
		   .a-server {
			  width: 60%;
			  margin: 1em auto;
			  text-align: center;
			  padding: 1em;
		   }
		   .a-server .p-err-sorry {
			  color: gray;
		   }
		   .a-server .p-err {
			  color: indianred;
		   }
		   .a-server .center-responsive {
			  width: 30%;
			  margin: 1em auto;
		   }
		   .a-server nav {
			  margin: 1em auto;
		   }
		   .a-server ul {
			  list-style: none;
			  margin: 0;
			  padding: 0;
		   }
		   .a-server ul li {
			  display: inline;
		   }
		   .a-server .btn-as {
			  padding: 1em 3em;
			  margin: 1em .3em;
			  color: white;
			  border: 0;
			  outline: 0;
		   }
		   .btn-lime {
			  background: #98c000;
		   }
		   .btn-red {
			  background: #ea2e49;
		   }
		   .btn-gray {
			  background: #3d4c53;
		   }
		   .btn-cyan {
			  background: #0cdbe8;
		   }
		   .cancel_search {
				display: block;
				font-size: 16px;
				margin: 15px auto;
				overflow: hidden;
				text-decoration:none;
			}
			
		</style>
	</head>
	<body>
		<div class="a-server">
			<img src="<?php echo $image_url; ?>" alt="sorry" class="center-responsive">
			<p class="p-err-sorry"><?php echo $caption; ?></p>
			<p class="p-err"><?php echo $message; ?></p>  
			<?php
				if(isset($back_to_home_flag)) {
					if($back_to_home_flag) {
			?>
						<a class="cancel_search" href="<?php echo site_url('dashboard'); ?>">
							<span class="fa fa-long-arrow-left"></span>
							Back to home
						</a>
			<?php
					}
					else {
					
					}
				}
				else {
			?>
				<a class="cancel_search" href="<?php echo site_url('dashboard'); ?>">
					<span class="fa fa-long-arrow-left"></span>
					Back to home
				</a>
			<?php
				}
			?>
			   
		</div>
	</body>
</html>
