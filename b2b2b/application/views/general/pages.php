<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title><?php echo $this->session->userdata('site_name'); ?></title>
<?php echo $this->load->view('core/load_css'); ?>
  <link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" />
</head>
<style type="text/css">
 .produconts ul li {
  list-style: inside;
}
</style>
<body>

<!-- Navigation -->

<?php echo $this->load->view('core/header'); ?> 
<?php
      if(isset($_SESSION['TheChinaGap']['language'])){ 
        $display_language = $_SESSION['TheChinaGap']['language'];
      } else {
        $display_language = $_SESSION['TheChinaGap']['language'] = BASE_LANGUAGE;
      } 
?>
<?php foreach ($pages as $key => $page) {

  $page_title=$page->page_title;
  $page_name=$page->page_name;
  $page_data=$page->page_data;
  # code...
} ?>

<!-- /Navigation -->
<div class="clearfix"></div>

<div class="agent_login_wrap top80">
		
   		<div class="container">
  		 <div class="static_page_in">
          	<h1><?php echo $page_name; ?></h1>

            
    
            	<div class="repetprod">
                <div class="col-xs-12 nopad produconts">
              		
                  
                  <p>
                    <?php echo $page_data; ?></p>
                 </div>
                 </div>
                
              
                 
          </div>
          
          
          
        </div>
   
</div>


<div class="clearfix"></div>

<?php echo $this->load->view('core/bottom_footer'); ?>



<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 



<script type="text/javascript">
$(document).ready(function(){
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	 
	
});
</script> 





</body>
</html>
