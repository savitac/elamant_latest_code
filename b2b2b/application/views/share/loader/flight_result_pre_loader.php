

<div class="all_loading imgLoader loader-image">
	<div class="load_inner">
		<div class="loadcity hide"></div>
		
		  <div class="loader-wrap">
  <svg class="loader-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 116.031 116.031" style="enable-background:new 0 0 116.031 116.031;" xml:space="preserve" >
  <rect x="0" y="0" style="display:none;fill:#055D84;" width="116.031" height="116.031"/>
  <g id="BG">
    <path style="opacity:0.2;fill-rule:evenodd;clip-rule:evenodd;fill:#f52007;" d="M58.639,0.019
      c32.031,0.344,57.718,26.589,57.374,58.62c-0.344,32.031-26.589,57.718-58.62,57.374c-32.031-0.344-57.718-26.589-57.374-58.62
      C0.363,25.362,26.608-0.325,58.639,0.019z"/>
    <path style="fill-rule:evenodd;clip-rule:evenodd;fill:#f5f5f5;" d="M58.542,9.019c27.06,0.291,48.761,22.463,48.471,49.524
      c-0.291,27.06-22.463,48.761-49.524,48.471C30.429,106.722,8.728,84.55,9.019,57.489C9.309,30.429,31.482,8.728,58.542,9.019z"/>
  </g>
  <g id="ICON-GROUP">
    <defs>
		<path id="SVGID_1_" d="M40.909,103.933C15.55,94.485,2.651,66.268,12.099,40.909c9.448-25.359,37.665-38.258,63.024-28.81
			c25.359,9.448,38.258,37.665,28.81,63.024C94.485,100.482,66.268,113.38,40.909,103.933z"/>
	</defs>
	<clipPath id="SVGID_2_">
		<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
	</clipPath>
	<g id="ICONS" style="clip-path:url(#SVGID_2_);">
    <g>
      <animateTransform 
                        id="down" 
                        attributeName="transform" 
                        attributeType="XML" 
                        type="translate" 
                        from="0,0"
                        to="0, -95"
                        dur="0.475s" 
                        calcMode="spline"
                        keySplines=".25,.1,.25,1" 
                        keyTimes="0;1"
                        begin="0s; next.end"
                         /> 
        <animateTransform 
                        id="left"
                        attributeName="transform" 
                        attributeType="XML" 
                        type="translate" 
                        from="0, -95"
                        to="-95, -95"
                        dur="0.475s" 
                        calcMode="spline"
                        keySplines=".25,.1,.25,1" 
                        keyTimes="0;1"
                        begin="0s; down.end"/> 
              <animateTransform 
                        id="next"
                        attributeName="transform" 
                        attributeType="XML" 
                        type="translate" 
                        from="-95, -95"
                        to="-194, -95"
                        dur="0.475s" 
                        calcMode="spline"
                        keySplines=".25,.1,.25,1" 
                        keyTimes="0;1"
                        begin="0s; left.end"/> 
      <g id="ICON-PAINT">
        <path style="opacity:1.0;fill:#f52007;enable-background:new    ;" d="M50.621,63.108l-9.538-0.317c-3.192,0-5.788,2.471-5.788,5.508s2.596,5.508,5.81,5.508l9.59-0.32L61.438,93.41h5.583
  c0.938,0,1.7-0.762,1.7-1.701v-0.094l-5.381-18.557l9.304-0.379c0.733,0,1.445-0.094,2.124-0.279l3.774,6.41h3.616
  c0.796-0.002,1.445-0.65,1.447-1.447v-0.107l-2.817-8.615v-0.822l2.816-8.615v-0.106c0-0.388-0.151-0.751-0.423-1.023
  c-0.273-0.273-0.636-0.424-1.023-0.424l-3.615-0.001l-3.844,6.529c-0.659-0.173-1.342-0.26-2.027-0.26l-9.37-0.381l5.421-18.693
  V44.75c0-0.939-0.762-1.7-1.7-1.7h-5.583L50.621,63.108z M67.021,44.395c0.171,0,0.314,0.121,0.348,0.284l-5.838,20.132
  l11.112,0.452c0.75,0,1.473,0.116,2.15,0.348l0.519,0.176l4-6.793l2.846,0.001c0.034,0,0.058,0.016,0.071,0.03
  c0.005,0.004,0.01,0.009,0.014,0.016l-2.799,8.564v1.25l2.799,8.564c-0.019,0.028-0.05,0.047-0.086,0.047l-2.846-0.001l-3.935-6.681
  l-0.521,0.183c-0.692,0.243-1.436,0.367-2.238,0.368L61.57,71.785l5.799,19.996c-0.034,0.162-0.177,0.285-0.348,0.285h-4.78
  L51.482,72.114l-10.4,0.349c-2.45,0-4.442-1.869-4.442-4.164s1.993-4.164,4.42-4.164l10.35,0.346l10.833-20.086L67.021,44.395
  L67.021,44.395z"/>       
      </g>
      <g id="ICON-PAINT_1_">
        <path style="opacity:1.0;fill:#f52007;enable-background:new    ;" d="M245.163,158.633l-9.538-0.316c-3.192,0-5.788,2.471-5.788,5.508s2.596,5.508,5.811,5.508l9.59-0.32l10.742,19.924h5.583
  c0.938,0,1.7-0.762,1.7-1.701v-0.094l-5.382-18.557l9.304-0.379c0.733,0,1.445-0.094,2.125-0.279l3.773,6.41h3.615
  c0.797-0.002,1.445-0.65,1.447-1.447v-0.107l-2.816-8.615v-0.822l2.815-8.615v-0.107c0-0.387-0.151-0.75-0.423-1.023
  c-0.273-0.273-0.637-0.424-1.023-0.424h-3.615l-3.844,6.529c-0.658-0.174-1.342-0.26-2.027-0.26l-9.369-0.381l5.42-18.693v-0.094
  c0-0.939-0.762-1.701-1.699-1.701h-5.583L245.163,158.633z M261.563,139.92c0.171,0,0.314,0.121,0.348,0.285l-5.838,20.131
  l11.111,0.453c0.75,0,1.473,0.115,2.15,0.348l0.518,0.176l4-6.793h2.846c0.035,0,0.059,0.016,0.072,0.031
  c0.004,0.004,0.01,0.008,0.014,0.016l-2.799,8.564v1.25l2.799,8.564c-0.02,0.027-0.051,0.047-0.086,0.047l-2.846-0.002l-3.936-6.68
  l-0.521,0.182c-0.691,0.244-1.436,0.367-2.238,0.369l-11.045,0.449l5.799,19.996c-0.033,0.162-0.178,0.285-0.348,0.285h-4.781
  l-10.758-19.953l-10.4,0.35c-2.449,0-4.442-1.869-4.442-4.164s1.993-4.164,4.42-4.164l10.35,0.346l10.833-20.086H261.563
  L261.563,139.92z"/>
      </g>
      <g id="ICON-ALBUM_1_">
        <path style="opacity:1.0;fill:#f52007;enable-background:new    ;" d="M39.603,148.783l-9.538-0.316c-3.192,0-5.788,2.471-5.788,5.508s2.596,5.508,5.81,5.508l9.59-0.32l10.743,19.924h5.583
  c0.938,0,1.699-0.762,1.699-1.701v-0.094l-5.381-18.557l9.304-0.379c0.733,0,1.445-0.094,2.124-0.279l3.774,6.41h3.615
  c0.797-0.002,1.445-0.65,1.447-1.447v-0.107l-2.817-8.615v-0.822l2.816-8.615v-0.107c0-0.387-0.151-0.75-0.424-1.023
  s-0.636-0.424-1.022-0.424h-3.615l-3.844,6.529c-0.659-0.174-1.343-0.26-2.027-0.26l-9.37-0.381l5.421-18.693v-0.094
  c0-0.939-0.762-1.701-1.699-1.701H50.42L39.603,148.783z M56.002,130.07c0.172,0,0.314,0.121,0.349,0.285l-5.839,20.131
  l11.112,0.453c0.749,0,1.473,0.115,2.149,0.348l0.519,0.176l4-6.793h2.846c0.034,0,0.059,0.016,0.071,0.031
  c0.005,0.004,0.01,0.008,0.015,0.016l-2.8,8.564v1.25l2.8,8.564c-0.02,0.027-0.051,0.047-0.086,0.047l-2.846-0.002l-3.936-6.68
  l-0.521,0.182c-0.692,0.244-1.436,0.367-2.238,0.369l-11.046,0.449l5.8,19.996c-0.034,0.162-0.178,0.285-0.349,0.285h-4.78
  l-10.759-19.953l-10.399,0.35c-2.45,0-4.442-1.869-4.442-4.164s1.992-4.164,4.42-4.164l10.35,0.346l10.832-20.086H56.002
  L56.002,130.07z"/>        
      </g>
      <g id="ICON-DATE_1_">
        <path style="opacity:1.0;fill:#f52007;enable-background:new    ;" d="M147.186,152.262l-9.538-0.316c-3.192,0-5.788,2.471-5.788,5.508s2.596,5.508,5.811,5.508l9.59-0.32l10.742,19.924h5.583
  c0.938,0,1.7-0.762,1.7-1.701v-0.094l-5.382-18.557l9.304-0.379c0.733,0,1.445-0.094,2.125-0.279l3.773,6.41h3.615
  c0.797-0.002,1.445-0.65,1.447-1.447v-0.107l-2.816-8.615v-0.822l2.815-8.615v-0.107c0-0.387-0.151-0.75-0.423-1.023
  c-0.273-0.273-0.637-0.424-1.023-0.424h-3.615l-3.844,6.529c-0.658-0.174-1.342-0.26-2.027-0.26l-9.369-0.381l5.42-18.693v-0.094
  c0-0.939-0.762-1.701-1.699-1.701h-5.583L147.186,152.262z M163.586,133.549c0.171,0,0.314,0.121,0.348,0.285l-5.838,20.131
  l11.111,0.453c0.75,0,1.473,0.115,2.15,0.348l0.518,0.176l4-6.793h2.846c0.035,0,0.059,0.016,0.072,0.031
  c0.004,0.004,0.01,0.008,0.014,0.016l-2.799,8.564v1.25l2.799,8.564c-0.02,0.027-0.051,0.047-0.086,0.047l-2.846-0.002l-3.936-6.68
  l-0.521,0.182c-0.691,0.244-1.436,0.367-2.238,0.369l-11.045,0.449l5.799,19.996c-0.033,0.162-0.178,0.285-0.348,0.285h-4.781
  l-10.758-19.953l-10.4,0.35c-2.449,0-4.442-1.869-4.442-4.164s1.993-4.164,4.42-4.164l10.35,0.346l10.833-20.086H163.586
  L163.586,133.549z"/>        
      </g>
      </g>
    </g>

  </g> 
  <g id="RING-GROUP" >
      <animateTransform attributeName="transform" 
                        attributeType="XML" 
                        type="rotate" 
                        from="0 58.02 58.02" 
                        to="360 58.02 58.02" 
                        dur="1.75s" 
                        repeatCount="indefinite"
                        calcMode="spline"
                        keySplines="0.4, 0, 0.2, 1" 
                        keyTimes="0;1"/> 
    
      <path style="opacity:0.5;fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#f52007;stroke-width:9;stroke-linecap:round;stroke-miterlimit:10;" d="
      M32.557,105.36C15.569,96.206,4.093,78.183,4.273,57.545c0.152-17.39,8.547-32.779,21.444-42.49"/>

      <path style="opacity:0.5;fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#f52007;stroke-width:9;stroke-linecap:round;stroke-miterlimit:10;" d="
      M58.481,4.743c1.013,0.009,2.019,0.046,3.017,0.11 M86.788,102.859 M61.498,4.853c28.004,1.81,50.038,25.219,49.79,53.628
      c-0.163,18.656-9.897,34.989-24.5,44.378"/>
  </g>
  </svg>
</div>
		
		
		
		
		
		
		
		
		<div class="result-pre-loader-wrapper result-pre-loader">
			<div class="result-pre-loader-container">
				<div class="paraload">Searching for the best fares !</div>
		        <div class="normal_load hide"></div>
		        <div class="clearfix"></div>
				<div class="sckintload">
				<?php if($trip_details['trip_type']== "oneway" ){ ?>
					<div class="ffty">
						<div class="pull-left hide">
							<i class="icn ldep_f_icn"><img alt="" src="<?php ASSETS.'assets/images/dep_l.png'; ?>"></i>
						</div>
						<div class="borddo brdrit">
							<span class="lblbk"><?php echo ucfirst($trip_details['from']); ?></span >
						</div>
					</div>
					
					<div class="ffty">
						<div class="pull-right hide">
							<i class="icn larr_f_icn"><img src="<?php echo ASSETS.'assets/images/arr_l.png'; ?>"></i>
						</div>
						<div class="borddo">
							<span class="lblbk"><?php echo ucfirst($trip_details['to']); ?></span >
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="tabledates">
                    <div class="tablecelfty">
								<div class="borddo brdrit">
									<div class="fuldate">
										<span class="bigdate"><?php echo date("d",strtotime($trip_details['depature']));?></span>
										<div class="biginre"><?php echo date("M",strtotime($trip_details['depature']));?><br>
											<?php echo date("Y",strtotime($trip_details['depature']));?>
										</div>
									</div>
								</div>
							</div>
					
				</div>
				<?php } else {   // mullitrip case     ?>
			<div class="ffty">
				       
						<div class="pull-left hide">
							<i class="icn ldep_f_icn"><img alt="" src="<?php ASSETS.'assets/images/dep_l.png'; ?>"></i>
						</div>
						<div class="borddo brdrit">
							 <?php if($trip_details['trip_type']== "circle"){ ?>
							<span class="lblbk"><?php echo ucfirst($trip_details['from']); ?></span >
							 <?php } else { ?>
								<span class="lblbk"><?php echo ucfirst($trip_details['from'][0]); ?></span > 
						     <?php } ?>
						</div>
					</div>
					
					<div class="ffty">
						<div class="pull-right hide">
							<i class="icn larr_f_icn"><img src="<?php echo ASSETS.'assets/images/arr_l.png'; ?>"></i>
						</div>
						<div class="borddo">
							 <?php if($trip_details['trip_type']== "circle"){ ?>
							<span class="lblbk"><?php echo ucfirst($trip_details['to']); ?></span >
							<?php } else { ?>
								<span class="lblbk"><?php echo ucfirst($trip_details['to'][count($trip_details['to'])-1]); ?></span > 
						     <?php } ?>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="tabledates">
                    <div class="tablecelfty">
						
								<div class="borddo brdrit">
									<?php if($trip_details['trip_type']== "circle"){ ?>
									<div class="halfdate">
										<span class="bigdate"><?php echo date("d",strtotime($trip_details['depature']));?></span>
										<div class="biginre"><?php echo date("M",strtotime($trip_details['depature']));?><br>
											<?php echo date("Y",strtotime($trip_details['depature']));?>
										</div>
									</div>
									
									<div class="halfdate">
										<span class="bigdate"><?php echo date("d",strtotime($trip_details['return']));?></span>
										<div class="biginre"><?php echo date("M",strtotime($trip_details['return']));?><br>
											<?php echo date("Y",strtotime($trip_details['return']));?>
										</div>
									</div>


									<?php }
									else if ($trip_details['trip_type']== "multicity") {
										?> 
										<div style="margin-top: 10px;">
										<span class=""><?php echo date("d",strtotime(current($trip_details['depature'])));?></span>
										<div class=""><?php echo date("M",strtotime(current($trip_details['depature'])));?><br>
											<?php echo date("Y",strtotime(current($trip_details['depature'])));?>
										</div>
										<?php
										?><span><?php echo @$trip_details['trip_type_label']; ?></span>
									</div>
										<?php 									}
									 else{ ?>
										<div class="halfdate">
										<span class="bigdate"><?php echo date("d",strtotime(current($trip_details['depature'])));?></span>
										<div class="biginre"><?php echo date("M",strtotime(current($trip_details['depature'])));?><br>
											<?php echo date("Y",strtotime(current($trip_details['depature'])));?>
										</div>
									</div>
									
									<div class="halfdate">
										<span class="bigdate"><?php echo date("d",strtotime(end($trip_details['return'])));?></span>
										<div class="biginre"><?php echo date("M",strtotime(end($trip_details['return'])));?><br>
											<?php echo date("Y",strtotime(end($trip_details['return'])));?>
										</div>
									</div>
										
								    <?php } ?>
									
								</div>
								
							</div>
					
				</div>
				<?php } // multi trip case?>
				
				</div>
				<div class="progress flghtloas">
			<div class="progress-bar progress-bar-striped active" id="progressbar" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">Please Wait...</div>
			</div>
				
			</div>
		</div>
	</div>
</div>
