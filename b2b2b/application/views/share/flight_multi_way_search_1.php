	<?php
$max_multicity_segments = 5;
$multicity_segment_count = (intval(count(@$multicity_segment_search_params['from_loc'])) > 0 ? count($multicity_segment_search_params['from_loc']) : 2);
$multi_flight_datepicker = array();
for($i=1;$i<=$max_multicity_segments; $i++){
	$multi_flight_datepicker[$i] = array('m_flight_datepicker'.$i, FUTURE_DATE_DISABLED_MONTH);
}

?>
<span class="hide">
	<input type="hidden" id="max_multicity_segments" value="<?=$max_multicity_segments?>">
	<input type="hidden" id="multicity_segment_count" value="<?=$multicity_segment_count?>">
</span>

<div id="multi" class="col-md-9 nopad" style="display: none">

	<?php for($multi_k=1;$multi_k<=$max_multicity_segments; $multi_k++){ 
			if (intval($multi_k) > $multicity_segment_count) {
				$segment_visibility = 'display:none';
			} else {
				$segment_visibility = '';
			}
	?>
			<div class="multi_city_container" id="multi_city_container_<?=$multi_k?>" style="<?=$segment_visibility?>">
				<div class="col-md-8 padfive placerows">
				<div class="col-xs-6 padfive"> 
				 <?php if($multi_k == 1) { ?>
					<div class="lablform">From</div>
                    <?php } ?>
					<div class="plcetogo deprtures sidebord"> <span class="maskimg  hfrom"></span>
						<input type="text" autocomplete="off" name="from[]" class="m_depcity normalinput ft valid_class fromflight form-control b-r-0" id="m_from<?=$multi_k?>" placeholder="Type Departure City" value="<?php echo @$multicity_segment_search_params['from'][$multi_k-1]?>" />
						<input class="hide loc_id_holder" name="from_loc_id[]" type="hidden" value="<?=@$multicity_segment_search_params['from_loc_id'][$multi_k-1]?>" >
					</div>
				</div>
				<div class="col-xs-6 padfive">
                <?php if($multi_k == 1) { ?>
					<div class="lablform">To</div>
                    <?php } ?>
					<div class="plcetogo destinatios sidebord"> <span class="maskimg  hfrom"></span>
						<input type="text" autocomplete="off" name="to[]"  class="m_arrcity normalinput ft valid_class departflight form-control b-r-0" id="m_to<?=$multi_k?>" placeholder="Type Destination City" value="<?php echo @$multicity_segment_search_params['to'][$multi_k-1] ?>"/>
						<input class="hide loc_id_holder" name="to_loc_id[]" type="hidden" value="<?=@$multicity_segment_search_params['to_loc_id'][$multi_k-1]?>" >
					</div>
				</div>
				</div>
				<div class="col-md-4 nopad secndates">
					<div class="col-xs-12 padfive">
	                    <?php if($multi_k == 1) { ?>
							<div class="lablform">Departure</div>
                        <?php } ?>
						<div class="plcetogo datemark sidebord"> <span class="maskimg caln"></span>
							<input type="text" name="depature[]" class="m_depature_date normalinput ft hand-cursor form-control b-r-0 disable-date-auto-update" id="m_flight_datepicker<?=$multi_k?>" placeholder="Select Date" value="<?php echo @$multicity_segment_search_params['depature'][$multi_k-1] ?>" readonly required/>
						</div>
					</div> 
                    <?php if($multi_k > 2) { ?>
	                    <button class="city_close_btn remove_city"> <span class="fa fa-times"></span></button>
					<?php } ?>
                    
				</div>
                
			</div>
	<?php }?>
</div>
<script type="text/javascript">$(document).ready(function() {
futureDatepickerMonthDisabled("flight_datepicker1");
futureDatepickerMonthDisabled("flight_datepicker2");
futureDatepickerMonthDisabled("m_flight_datepicker1");
futureDatepickerMonthDisabled("m_flight_datepicker2");
futureDatepickerMonthDisabled("m_flight_datepicker3");
futureDatepickerMonthDisabled("m_flight_datepicker4");
futureDatepickerMonthDisabled("m_flight_datepicker5");
futureDatepickerMonthDisabled("hotel_checkin");
futureDatepickerMonthDisabled("hotel_checkout");
});</script> 