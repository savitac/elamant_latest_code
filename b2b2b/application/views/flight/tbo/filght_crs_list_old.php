<?php 
/*print_r($flights);
exit("1");*/
   $adult_count=$search_params['data']['adult_config']; 
   $child_count=$search_params['data']['child_config'];
   $infant_count=$search_params['data']['infant_config'];
   $trip_type=$search_params['data']['trip_type'];
   $org_loc=$search_params['data']['from'];
   ?>
<?php foreach ($flights as $key => $crslist) {
      $crslist['adult_price']; 
      $adult=($crslist['adult_price'] + $crslist['adult_tax'] + $crslist['adult_markup']) * $adult_count ; 
      $child=($crslist['child_price'] + $crslist['child_tax'] + $crslist['child_markup']) * $child_count ; 
      $infant=($crslist['infant_price'] + $crslist['infant_tax'] + $crslist['infant_markup'] ) * $infant_count ; 
      $total_fare= $adult + $child + $infant; 
      $fare= ($crslist['adult_price'] * $adult_count) + ($crslist['child_price'] * $child_count ) + ( $crslist['infant_price'] * $infant_count );
      
      $tax=($crslist['adult_tax']  * $adult_count) +
                 ($crslist['child_tax']  * $child_count) +
                 ($crslist['infant_tax']  * $infant_count) ;

      $markup=($crslist['adult_markup']  * $adult_count) +
                 ($crslist['child_markup']  * $child_count) +
                 ($crslist['infant_markup']  * $infant_count) ;
      
      $net_fare=(($crslist['adult_price'] + $crslist['adult_tax']) * $adult_count) +
                 (($crslist['child_price'] + $crslist['child_tax']) * $child_count) +
                 (($crslist['infant_price'] + $crslist['infant_tax']) * $infant_count) ;

      $total_amount=$net_fare + $markup;

     /* if($crslist['no_multiple']=="no_multiplaction"){
         exit("..");
      }*/

      // no_multiple 
      if($trip_type=='circle' && $crslist['no_multiple']=="") {
          $total_amount= $total_amount*2;
           $net_fare= $net_fare*2;
           $fare=$fare*2;
           $tax=$tax*2;
      }
      $token = serialized_data($crslist['orgin']);
      $token_key = md5($token);
      $aviablity=$crslist['left_seats'];
      
      if($crslist['no_multiple']!=""){
         // changes on sep-6-2017;
         $total_amount_discount=($crslist['round_trip_dicount']/100)*$total_amount;
         $total_amount=($total_amount-$total_amount_discount);
         $crslist['orgin']="";
         $total_bagage="not_required";
         $onword_bagnge=($adult_count * $crslist['adult_bagage_onword']) + ($child_count * $crslist['child_bagage_onword']);
         $return_bagnge=($adult_count * $crslist['adult_bagage_retrun']) + ($child_count * $crslist['child_bagage_retrun']);
      }else{
        
         $total_bagage=($adult_count * $crslist['adult_bagage']) + ($child_count * $crslist['child_bagage']);
      }

      //$path=$GLOBALS ['CI']->template->domain_images('crs_airline_logo/'.$crslist['onward_path']);
      $path="";
       ?>
   <?php 
   // timing  
  // 
   //debug($flights);
   if($trip_type=="oneway" && $flights[$key]['flight_trip']=="circle"){
	   if($org_loc==$crslist['return_from_city']){
		$crslist['onward_code']=$crslist['onward_code'];
		$crslist['onwards_departure_time']=$crslist['return_departure_time'];
		$crslist['from_city']=$crslist['return_from_city'];
		$crslist['to_city']=$crslist['return_to_city'];
		$crslist['onwards_arrival_time']=$crslist['return_arrival_time'];
		$crslist['onwards_duration']=$crslist['return_duration'];
	}
   }

   if($trip_type=="circle"){
      
      if($org_loc==$crslist['return_from_city']){
         $crslist['onward_code']=$crslist['onward_code'];
         $crslist['onwards_departure_time']=$crslist['return_departure_time'];
         $crslist['from_city']=$crslist['return_from_city'];
         $crslist['to_city']=$crslist['return_to_city'];
         $crslist['onwards_arrival_time']=$crslist['return_arrival_time'];
         $crslist['onwards_duration']=$crslist['return_duration'];
   }
   }

   
   ?>
   <div class="rowresult p-0 r-r-i t-w-i-1"> 
   <div class="madgrid">
   <div class="f-s-d-w col-xs-8 nopad wayeght full_same">
    <?php if($trip_type=='oneway') { ?>
      <div class="allsegments outer-segment-0">
         <div class="quarter_wdth nopad col-xs-3">
            <div class="fligthsmll"><img class="airline-logo" alt="" src="<?=$path?>"></div>
            <div class="m-b-0 text-center">
               <div class="a-n airlinename" data-code="<?php echo $crslist['onward_code']; ?>"><?php echo $crslist['airline_name']; ?></div>
               <strong> <?php echo $crslist['onward_code']; echo $crslist['onwards_flight_number']; ?></strong>
            </div>
         </div>
         <div class="col-xs-3 nopad quarter_wdth">
            <div class="insidesame">
               <span class="fdtv hide"><?php echo date('Hi', strtotime($crslist['onwards_departure_time'])) ?></span>
               <div class="f-d-t bigtimef"><?php echo $crslist['onwards_departure_time']; ?></div>
               <div class="from-loc smalairport"><?php echo substr($crslist['from_city'],0,-5); ?></div>
               <span class="dep_dt hide" data-category="2" data-datetime="<?php echo strtotime($crslist['onwards_departure_time']); ?>"></span>
            </div>
         </div>
         <div class="col-md-1 p-tb-10 hide">
            <div class="arocl fa fa-long-arrow-right"></div>
         </div>
         <div class="col-xs-3 nopad quarter_wdth">
            <div class="insidesame">
               <span class="fatv hide"><?php echo date('Hi', strtotime($crslist['onwards_arrival_time'])) ?></span>
               <div class="f-a-t bigtimef"><?php echo $crslist['onwards_arrival_time']; ?></div>
               <div class="to-loc smalairport"><?php echo substr($crslist['to_city'],0,-5).""; ?></div>
               <span class="arr_dt hide" data-category="3" data-datetime="<?php echo strtotime($crslist['onwards_arrival_time']); ?>"></span>
            </div>
         </div>
         <div class="smal_udayp nopad col-xs-3">
            <span class="f-d hide"><?php echo $crslist['onwards_duration']?></span>
            <div class="insidesame">
               <div class="durtntime"><?php echo $crslist['onwards_duration']?></div>
               <div class="stop-value smalairport">Stop:0</div>
               <div class="stop-value smalairport">Bagage:<?php echo $total_bagage; ?></div>
            </div>         </div> 
      </div>
      <?php } ?>
      <?php if($trip_type=='circle') { ?>

      <div class="allsegments outer-segment-0">
         <div class="quarter_wdth nopad col-xs-3">
            <div class="fligthsmll"><img class="airline-logo" alt="this" src="<?=SYSTEM_IMAGE_DIR.'airline_logo/' . $crslist['onward_code']?>.gif"></div>
            <div class="m-b-0 text-center">
               <div class="a-n airlinename" data-code="<?php echo $crslist['onward_code']; ?>"><?php echo $crslist['airline_name']; ?></div>
               <strong> <?php echo $crslist['onward_code']; echo $crslist['onwards_flight_number']; ?></strong>
            </div>
         </div>
         <div class="col-xs-3 nopad quarter_wdth">
            <div class="insidesame">
               <span class="fdtv hide"><?php echo date('Hi', strtotime($crslist['onwards_departure_time'])) ?></span>
               <div class="f-d-t bigtimef"><?php echo $crslist['onwards_departure_time']; ?></div>
               <div class="from-loc smalairport"><?php echo substr($crslist['from_city'],0,-5); ?></div>
               <span class="dep_dt hide" data-category="2" data-datetime="<?php echo strtotime($crslist['onwards_departure_time']); ?>"></span>
            </div>
         </div>
         <div class="col-md-1 p-tb-10 hide">
            <div class="arocl fa fa-long-arrow-right"></div>
         </div>
         <div class="col-xs-3 nopad quarter_wdth">
            <div class="insidesame">
               <span class="fatv hide"><?php echo date('Hi', strtotime($crslist['onwards_arrival_time'])) ?></span>
               <div class="f-a-t bigtimef"><?php echo $crslist['onwards_arrival_time']; ?></div>
               <div class="to-loc smalairport"><?php echo substr($crslist['to_city'],0,-5); ?></div>
               <span class="arr_dt hide" data-category="3" data-datetime="<?php echo strtotime($crslist['onwards_arrival_time']); ?>"></span>
            </div>
         </div>
         <div class="smal_udayp nopad col-xs-3">
            <span class="f-d hide"><?php echo $crslist['onwards_duration']?></span>
            <div class="insidesame">
               <div class="durtntime"><?php echo $crslist['onwards_duration']?></div>
               <div class="stop-value smalairport">Stop:0</div>
               <div class="stop-value smalairport">Bagage:<?php 
               if($total_bagage=="not_required"){echo $onword_bagnge; }else{echo $total_bagage;} ?></div>
            </div>         </div> 
      </div>
     
     
      <div class="allsegments outer-segment-1">
         <div class="quarter_wdth nopad col-xs-3">
            <div class="fligthsmll"><img class="airline-logo" alt="" src="<?=$path_b?>"></div>
            <div class="m-b-0 text-center">
               <div class="a-n airlinename" data-code="<?php echo $crslist['retuen_code']; ?>"><?php echo $crslist['airline_namee']; ?></div>
               <strong> <?php echo $crslist['retuen_code']; echo $crslist['return_flight_number']; ?></strong>
            </div>
         </div>
         <div class="col-xs-3 nopad quarter_wdth">
            <div class="insidesame">
               <span class="fdtv hide"><?php echo date('Hi', strtotime($crslist['return_departure_time'])) ?></span>
               <div class="f-d-t bigtimef"><?php echo $crslist['return_departure_time']; ?></div>
               <div class="from-loc smalairport"><?php echo substr($crslist['return_from_city'],0,-5); ?></div>
               <span class="dep_dt hide" data-category="2" data-datetime="<?php echo strtotime($crslist['return_departure_time']); ?>"></span>
            </div>
         </div>
         <div class="col-md-1 p-tb-10 hide">
            <div class="arocl fa fa-long-arrow-right"></div>
         </div>
         <div class="col-xs-3 nopad quarter_wdth">
            <div class="insidesame">
               <span class="fatv hide"><?php echo date('Hi', strtotime($crslist['return_arrival_time'])) ?></span>
               <div class="f-a-t bigtimef"><?php echo $crslist['return_arrival_time']; ?></div>
               <div class="to-loc smalairport"><?php echo substr($crslist['return_to_city'],0,-5); ?></div>
               <span class="arr_dt hide" data-category="3" data-datetime="<?php echo strtotime($crslist['return_arrival_time']); ?>"></span>
            </div>
         </div>
         <div class="smal_udayp nopad col-xs-3">
            <span class="f-d hide"><?php echo $crslist['return_duration']?></span>
            <div class="insidesame">
               <div class="durtntime"><?php echo $crslist['return_duration']?></div>
               <div class="stop-value smalairport">Stop:0</div>
               <div class="stop-value smalairport">Bagage :<?php if($total_bagage=="not_required"){echo $return_bagnge; }else{echo $total_bagage;}?></div>
            </div>         </div> 
      </div>

      <?php } ?>
   </div>
   <div class="col-xs-4 nopad wayfour full_same">
      <span class="hide stp" data-stp="0" data-category="1"></span>
      <div class="priceanbook">
         <div class="col-xs-6 nopad wayprice">
            <div class="insidesame">
               <div class="priceflights"><strong>$ <?php $curr = get_application_currency_preference();
                                                       ?></strong><span class="f-p"></span><?php echo $total_amount ?></span></div>
                                          <div style="display:none" class="snf_hnf net-fare-tag" title="">$ <?php echo $net_fare ?> </div>
                           <span class="hide price" data-price="<?php echo $total_amount ?>" data-currency="<?php echo $net_fare ?>"></span>
                           
               
            </div>
         </div>
         <div class="col-xs-6 nopad waybook">
            <div class="form-wrapper bookbtlfrt">
               <form method="POST" id="form-id-113" action="<?php echo base_url(); ?>index.php/flight/booking/<?php echo $search_id ?>" 
               class="book-form-wrapper"><input type="hidden" name="is_domestic" class="" value="1">
               <input type="hidden" name="token[]" class="token data-access-key" value="<?=@$token?>">
               <input type="hidden" name="token_key[]" class="token_key" value="<?=@$token_key?>">
                <input type="hidden" name="search_access_key[]" class="search-access-key" 
                value="<?php if($crslist['orgin']!=""){echo $crslist['orgin'];}else{ echo $crslist['orgin_onword'].",".$crslist['orgin_return'];} ?>">
               <input type="hidden" name="is_lcc[]" class="is-lcc" value="">
               <input type="hidden" name="promotional_plan_type[]" class="promotional-plan-type" value="">
               <input type="hidden" name="booking_type" class="booking-type" value="process_fare_quote">
               <input type="hidden" name="booking_source" class="booking-source" value="<?php echo $booking_source ?>">
               <?php if($aviablity !='0')
               { ?>
                   <button class="b-btn bookallbtn" type="submit">Book Now</button>
                   <br/><br/><br/>
                   <div data-val="1" class="n-r n-r-t">Availability : <?php echo $aviablity; ?> </div>
                 </form>
             <?php   } ?>
               
            </div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="mrinfrmtn"><a class="detailsflt iti-btn" data-id="fdp_<?php echo $crslist['orgin']?>">Flight Details</a></div>
   <div class="propopum" id="fdp_<?php echo $crslist['orgin']?>">
      <div class="comn_close_pop closepopup">X</div>
      <div class="p_i_w">
         <div class="popuphed">
            <div class="hdngpops"><?php echo substr($crslist['from_city'],-5); ?> <span class="fa fa-exchange"></span> <?php echo substr($crslist['to_city'],-5); ?> </div>
         </div>
         <div class="popconyent">
            <div class="contfare">
               <ul role="tablist" class="nav nav-tabs flittwifil">
                  <li class="active" data-role="presentation"><a data-toggle="tab" data-role="tab" href="#iti_det_<?php echo $crslist['orgin']?>">Itinerary</a></li>
                  <li data-role="presentation"><a data-toggle="tab" data-form-id="form-id-<?php echo $crslist['orgin']?>" class="iti-fare-btn" data-role="tab" href="#fare_det_<?php echo $crslist['orgin']?>">Fare Details</a></li>
               </ul>
               <div class="tab-content">
                  <div id="fare_det_<?php echo $crslist['orgin']?>" class="tab-pane i-i-f-s-t">
                     <div class="text-center loader-image"><?php echo $crslist['fare_rules']; ?></div>
                     <div class="i-s-s-c tabmarg"></div>
                  </div>
                  <div id="iti_det_<?php echo $crslist['orgin']?>" class="tab-pane active i-i-s-t ">
                     <div class="tabmarg">
                        <div class="alltwobnd">
                           <div class="col-xs-7 nopad full_wher">
                           <?php if($trip_type=='oneway'){?>
                              <div class="inboundiv seg-0">
                                 <div class="hedtowr"><?php echo substr($crslist['from_city'],0,-5); ?> to <?php echo substr($crslist['to_city'],0,-5); ?><strong>(25h 30m )</strong></div>
                                 <div class="flitone">
                                    <div class="col-xs-3 nopad5">
                                       <div class="imagesmflt"><img alt="UL icon" src="<?=$path?>"></div>
                                       <div class="flitsmdets"><?php echo $crslist['airline_name']; ?><strong><?php echo $crslist['onward_code']; echo $crslist['onwards_flight_number']; ?></strong></div>
                                    </div>
                                    <div class="col-xs-7 nopad5">
                                       <div class="col-xs-5 nopad5">
                                          <div class="dateone"><?php echo $crslist['onwards_departure_time']; ?></div>
                                          <div class="termnl"><?php echo substr($crslist['from_city'],0,-5); ?></div>
                                       </div>
                                       <div class="col-xs-2 nopad">
                                          <div class="arocl fa fa-long-arrow-right"></div>
                                       </div>
                                       <div class="col-xs-5 nopad5">
                                          <div class="dateone"><?php echo $crslist['onwards_arrival_time']; ?></div>
                                          <div class="termnl"><?php echo substr($crslist['to_city'],0,-5); ?></div>
                                       </div>
                                    </div>
                                    <div class="col-xs-2 nopad5">
                                       <div class="ritstop">
                                          <div class="termnl"><?php echo $crslist['onwards_duration']?> </div>
                                          <div class="termnl1">Stop : 0</div>
                                       </div>
                                    </div>
                                 </div>
                                 
                              </div>
                              <?php } if($trip_type=='circle'){ ?>
                              <div class="inboundiv seg-0">
                                 <div class="hedtowr"><?php echo substr($crslist['return_from_city'],0,-5); ?> to <?php echo substr($crslist['return_to_city'],0,-5); ?><strong>(25h 30m )</strong></div>
                                 <div class="flitone">
                                    <div class="col-xs-3 nopad5">
                                       <div class="imagesmflt"><img alt="UL icon" src="<?=$path_b?>"></div>
                                       <div class="flitsmdets"><?php echo $crslist['airline_namee']; ?><strong><?php echo $crslist['retuen_code']; echo $crslist['return_flight_number']; ?></strong></div>
                                    </div>
                                    <div class="col-xs-7 nopad5">
                                       <div class="col-xs-5 nopad5">
                                          <div class="dateone"><?php echo $crslist['return_departure_time']; ?></div>
                                          <div class="termnl"><?php echo substr($crslist['return_from_city'],0,-5); ?></div>
                                       </div>
                                       <div class="col-xs-2 nopad">
                                          <div class="arocl fa fa-long-arrow-right"></div>
                                       </div>
                                       <div class="col-xs-5 nopad5">
                                          <div class="dateone"><?php echo $crslist['return_arrival_time']; ?></div>
                                          <div class="termnl"><?php echo substr($crslist['return_to_city'],0,-5); ?></div>
                                       </div>
                                    </div>
                                    <div class="col-xs-2 nopad5">
                                       <div class="ritstop">
                                          <div class="termnl"><?php echo $crslist['return_duration']?> </div>
                                          <div class="termnl1">Stop : 0</div>
                                       </div>
                                    </div>
                                 </div>
                                 
                              </div>

                             <?php  } ?>
                           </div>
                           <div class="col-xs-5 nopad full_wher">
                              <div class="inboundiv sidefare">
                                 <h4 class="farehdng">Total Fare Breakup</h4>
                                 <div class="inboundivinr">
                                    <div class="rowfare">
                                       <div class="col-xs-8 nopad"><span class="infolbl">Total Base Fare</span></div>
                                       <div class="col-xs-4 nopad"><span class="pricelbl">$ <?php echo $fare ?></span></div>
                                    </div>
                                    <div class="rowfare">
                                       <div class="col-xs-8 nopad"><span class="infolbl">Taxes &amp; Fees</span></div>
                                       <div class="col-xs-4 nopad"><span class="pricelbl">$ <?php echo $tax ?> + <?php echo $markup ?> </span></div>
                                    </div>
                                    <div class="rowfare grandtl">
                                       <div class="col-xs-8 nopad"><span class="infolbl">Grand Total</span></div>
                                       <div class="col-xs-4 nopad"><span class="pricelbl">$ <?php echo $total_amount ?></span></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="popfooter">
            <div class="futrcnt"><button class="norpopbtn closepopup">Close</button>  </div>
         </div>
      </div>
   </div>
</div>
</div>
<?php } ?>


