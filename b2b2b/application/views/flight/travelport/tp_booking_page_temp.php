<style type="text/css">
	
/*flight_booking*/
.full.onlycontent{background:none;}
.removeseting{ display:none;}
.repeatprows {
    float: left;
    margin: 5px 0;
    width: 100%;
}
.postnew{background: #f9f9f9 none repeat scroll 0 0;
    border-radius: 3px;
    display: block;
    overflow: hidden;
    padding: 15px 0; font-size:14px;}
.comon_backbg{background: #fff none repeat scroll 0 0;
    border: 1px solid #e6e6e6;
    border-radius: 4px;
    box-shadow: 0 0 5px #e6e6e6;
    float: left;
    margin: 0 0 20px;
    overflow: hidden;
    width: 100%;}
.leftboks{
    float: left;
    width: 100%;}
.flightset{display: block;
    overflow: hidden;}
.frto{ color: #666666;
    font-size: 15px;}
.fseting.fa{color: #666666;
    display: block;
    font-size: 18px;
    text-align: center;}
.durpla{color: #666666;
    font-size: 13px;
    margin: 5px 0;}
.dutarionsf{ border: 1px solid #eeeeee;
    display: block;
    margin: 0 10px;
    text-align: center;}
.witebackgrnd {
     background: #ffffff none repeat scroll 0 0;
    border: 1px solid #dddddd;
    margin: 25px 0;
    padding: 0 15px;
}
.lostcart{ background: #fff none repeat scroll 0 0;
    border: 1px solid #e6e6e6;
    border-radius: 3px;
    box-shadow: 0 0 5px #e6e6e6;
    float: left;
    font-size: 14px;
    margin-bottom: 15px;
    overflow: hidden;
    width: 100%;}
.fordetailpage {
    display: block;
    margin-bottom: 20px;
    overflow: hidden;
}
.set_margin {
    margin: 5px 0;
}
.sidebuki {
    float: right;
}
.cartbukdis {
    display: block;
    float: left;
    width: 100%;
}
.cartlistingbuk {
    float: left;
    width: 100%;
}
.carttitlebuk1{ float:left; width:100%;}
.celcart {
    display: table-cell;
    float: none;
    vertical-align: middle;
}
.smalbukcrt {
    float: left;
	overflow:hidden;
    width: 60px;
}
.smalbukcrt  img{width:100%;}
.splcrtpad {
    padding: 10px 0;
}
.carttitlebuk {
    background: #f6f6f6 none repeat scroll 0 0;
    color: #333;
    display: block;
    float: left;
    font-size: 16px;
    overflow: hidden;
    padding: 5px 10px;
    width: 100%;
}
.cartsec {
      color: #666;
    display: block;
    float: left;
    font-size: 12px;
    overflow: hidden;
	font-family: roboto;
}
.singecartpricebuk {
    color: #333333;
	font-family: roboto;
}

.cartitembuk {
    display: table;
    position: relative;
    width: 100%;
}
.payblnhmxm {
    display: block;
    overflow: hidden;
    padding: 10px 0;
}
.ritaln {
    text-align: right;
}
.payblnhm {
    display: block;
    overflow: hidden;
}
.promocode {
   border: 1px solid #dddddd;
    border-radius: 3px;
    display: block;
    font-size: 14px;
    height: 50px;
    overflow: hidden;
    padding: 0 10px;
    width: 100%;
}
.promosubmit {
    background: #f88c3e none repeat scroll 0 0;
    border: 1px solid #f88c3e;
    border-radius: 3px;
    color: #ffffff;
    display: table;
    height: 50px;
    margin: 0 auto;
    padding: 7px;
    text-align: center;
    width: 100%; font-size:14px;
}
.savemessage {
    color: #666666;
    display: block;
    overflow: hidden;
    padding: 5px 25px 15px;
}
.cartcntamnt.normalprc {
    color: #333333;
}
.cartlistingbuk.nomarr {
    margin: -11px 0 0;
}
.cartcntamnt.bigclrfnt {
    color: #07253f;
    font-size: 20px;
}
.padleftpay {
    padding: 0 20px 0 0;
}
.inpagehed {
   background: #424a5d none repeat scroll 0 0;
    color: #fff;
    display: block;
    font-size: 22px;
    font-weight: normal;
    margin: 0;
    overflow: hidden;
    padding: 10px 15px;
}
.sectionbuk{padding: 15px;}
.collapsebtn2.bukcolsp::after {
    background: rgba(0, 0, 0, 0) url("../../images/stip.png") no-repeat scroll 0 bottom;
    bottom: -9px;
    content: "";
    height: 9px;
    left: 70px;
    position: absolute;
    width: 25px;
    z-index: 100;
}

.collapsebtn2 {
    background: #ffffff none repeat scroll 0 0;
    border: 0 solid #000000;
    color: #15262f;
    font-size: 14px;
    font-weight: 700;
    height: 32px;
    padding-left: 20px;
    padding-right: 20px;
    text-align: left;
    width: 100%;
}
.collapsebtn2.bukcolsp {
     border-bottom: 1px dotted #ddd;
    color: #444;
    font-size: 20px;
    font-weight: normal;
    height: auto;
    margin: 10px 0 30px;
    padding: 10px 0;
    position: relative;
}
.collapsebtn2.bukcolsp span.collapsearrow {
    background: rgba(0, 0, 0, 0) url("../../images/card.png") no-repeat scroll 0 0;
    height: 28px;
    margin: 0;
    width: 28px;
}
.editbuk {
    background: #f88c3e none repeat scroll 0 0;
    border: 1px solid #f88c3e;
    border-radius: 2px;
    color: #ffffff;
    float: right;
    font-size: 14px;
    margin-right: 10px;
    padding: 3px 15px;
}
.onedept {
    display: block;
    overflow: hidden;
}
.evryicon{background: #424a5d none repeat scroll 0 0;
    border-radius: 30px;
    color: #fff;
    float: left;
    height: 60px;
    line-height: 68px;
    margin-right: 10px;
    text-align: center;
    width: 60px;}
.evryicon .fa{font-size: 26px;}
.pasenger_location{display: block;
    float: left;
    margin-bottom: 20px;
    overflow: hidden;}
.payinput{border: 1px solid #dddddd;
    border-radius: 3px;
    color: #333333;
    display: block;
    font-size: 14px;
    height: 50px;
    overflow: hidden;
    padding: 10px;
    width: 100%;}
.inpagehedbuk {
    color: #444444;
    display: table;
    font-size: 20px;
    font-weight: normal;
    margin: 0 0 5px;
    overflow: hidden;
    padding: 5px 10px;
}
.hwonum{background: #e9e9e9 none repeat scroll 0 0;
    border-radius: 3px;
    float: left;
    font-size: 14px;
    margin: 0 5px 0 0;
    padding: 5px 10px;}
.bookingcnt {
    color: #333333;
}
.payrow {
     border-bottom: 1px solid #e6e6e6;
    display: block;
    font-size: 14px;
    margin: 0 -15px 20px;
    overflow: hidden;
    padding-bottom: 10px;
}
.payrow:last-child{border:0;}
.billingnob .payrow{border-bottom: none;}
.lokter{
    text-align: center;
    width: 100%;}
.whoare{color: #444;
    font-size: 12px;}
.lokter .fa{background: #f88c3e none repeat scroll 0 0;
    border-radius: 100%;
    color: #fff;
    display: block;
    font-size: 22px;
    height: 40px;
    line-height: 40px;
    margin: 0 auto;
    overflow: hidden;
    width: 40px;}
.lokter .fa-male{background: #f96800 none repeat scroll 0 0;}
.lokter .fa-child{background: #fcae05 none repeat scroll 0 0;}
.paylabel {
    color: #666666;
    display: block;
    margin-bottom: 5px;
    overflow: hidden;
}
.cartcntamnt {
    color: #ffffff;
    display: block;
    overflow: hidden;
    padding: 10px;
	font-family: roboto;
}
.flpayinput {
    border: 1px solid #dddddd;
    border-radius: 3px;
    color: #333333;
    display: block;
    font-size: 14px;
    height: 50px;
    overflow: hidden;
    padding: 10px;
    width: 100%;
}
.selectedwrap {
    position: relative;
}
.selectedwrap::after {
    background: #ffffff none repeat scroll 0 0;
    bottom: 1px;
    color: #666666;
    content: "";
    cursor: pointer;
    font-family: "FontAwesome";
    font-size: 22px;
    height: 48px;
    line-height: 48px;
    pointer-events: none;
    position: absolute;
    right: 1px;
    text-align: center;
    top: 1px;
    width: 40px;
}
.noteclick {
    color: #555555;
    display: block;
    font-size: 13px;
    overflow: hidden;
    padding: 10px;
}
.featurette-divider3 {
    margin: 30px 0;
}

.checkcontent {
    color: #666666;
    display: block;
    font-size: 14px;
    line-height: 22px;
    overflow: hidden;
}
.colorbl {
    color: #0082be;
}
.payrowsubmt {
    float: left;
    padding: 20px 0;
    width: 100%;
}
.paysubmit {
    background: #f88c3e none repeat scroll 0 0;
    border: 0 none;
    border-radius: 3px;
    color: #ffffff;
    float: left;
    font-size: 14px;
    padding: 15px 20px;
    text-align: center;
    width: 70%;
}
.verifycod {
    color: #666666;
    display: block;
    margin-top: 10px;
    overflow: hidden;
}
.lastnote {
    color: #666666;
    display: block;
    margin-top: 20px;
    overflow: hidden;
}
.popuofixissue{position: absolute;
    background: rgba(0,0,0,0.7);
    top: 0;
    right: 0;
    bottom: 0;
    z-index: 10000;
    left: 0; display:none; transition:all 400ms ease-in-out 0s;}
.popbighed {
	background: none repeat scroll 0 0 #f6f6f6;
	color: #666;
	display: block;
	font-size: 18px;
	overflow: hidden;
	padding: 10px;
}
.paymentpage{display: block;
    padding: 10px 0; float:left; width:100%;}

.paybilling_section{ float: left;
    width: 100%; margin-bottom:20px;}
.addnewbill{background: #d3d3d3 none repeat scroll 0 0;
    border-radius: 3px;
	cursor:pointer;
    float: left;
    height: 80px;
	position:relative;
    text-align: center;
    width: 100%;}
.addnewbill .add_new_fa{background: #008a00 none repeat scroll 0 0;
    border-radius: 30px;
    color: #fff;
    display: none;
    height: 24px;
    line-height: 24px;
    position: absolute;
    right: 5px;
    text-align: center;
    top: 5px;
    width: 24px;}


.addnewbill.active {
    border: 1px solid #008a00;
}
.addnewbill.active .add_new_fa {
    display:block;
}

.addnewbill .fa.fasplus{display: block;
    font-size: 20px;
    margin-top: 20px;
    overflow: hidden; color:#444;}
.plus_add{display: block;
    font-size: 14px;
    overflow: hidden;}
.Addingbill{padding: 0 25px;}
.ouraddrss{ background: #ededed none repeat scroll 0 0;
    border-radius: 3px;
    display: block;
    font-size: 14px;
    margin: 0 10px;
    overflow: hidden;
    padding: 10px 15px; position:relative; border:1px solid #ededed;}
.addname{color: #444;
    display: block;
    overflow: hidden;}
.addaddress{color: #444;}	
.owlindex3 .owl-controls .owl-buttons .owl-prev, .owlindex3 .owl-controls .owl-buttons .owl-next {
     border-radius: 0;
    bottom: 0;
    margin: 0;
    padding: 0;
    position: absolute;
    text-indent: -99999px;
    top: 0;
    width: 35px;
    z-index: 10;
}
.owlindex3 .owl-controls .owl-buttons .owl-prev {
    background:url(../../images/prev.png) no-repeat center center;; left: -25px;}
.owlindex3 .owl-controls .owl-buttons .owl-next {
   background:url(../../images/next.png) no-repeat center center;right: -25px;}
.owlindex3 .owl-controls.clickable{ margin-top:0px;}
.item.active .ouraddrss{ border:1px solid #008a00;}
.ouraddrss .fa{ display:none}
.item.active .ouraddrss .fa{background: #008a00 none repeat scroll 0 0;
    border-radius: 30px;
    color: #fff;
    display: block;
    height: 24px;
    line-height: 24px;
    position: absolute;
    right: 5px;
    text-align: center;
    top: 5px;
    width: 24px;} 

.payment_process::after {
    background: #ccc none repeat scroll 0 0;
    content: "";
    height: 2px;
    left: 17%;
    position: absolute;
    right: 17%;
    top: 9px;
}
.payment_process{display: block;
    margin: 30px 0;
    overflow: hidden;
    position: relative;}
.center_pro {
    display: block;
    overflow: hidden;
    text-align: center;
}
.fabols{background: #ccc none repeat scroll 0 0;
    border: 4px solid #fff;
    border-radius: 100%;
    display: block;
    height: 20px;
    margin: 0 auto 10px;
    overflow: hidden;
    position: relative;
    width: 20px;
    z-index: 10;}
.center_labl{color: #333;
    display: block;
    font-size: 13px;
    overflow: hidden;
    text-align: center;
    text-transform: uppercase;}
.center_pro.active .fabols{background:#eb8c48;}

.sumry_wrap{float: left;
    padding-right: 20px;
    width: 100%;}
.pre_summery{background:#fff; border: 1px solid #e6e6e6;
    border-radius: 3px;
    box-shadow: 0 0 5px #e6e6e6;
    display: block;
    margin: 0 0 20px;
    overflow: hidden;}
.prebok_hding{border-bottom: 1px dashed #ddd;
    color: #333;
    display: block;
    font-size: 18px;
    overflow: hidden;
    padding: 10px 15px;}
.pre_summery .flname {
    font-size: 15px;
}
.prebok_hding .fa{margin:0 10px; color:#999;}

.signing_detis{}
.wrp_pre{ float: left;
    padding: 15px;
    width: 100%;}
.pre_put{border: 1px solid #ddd;
    border-radius: 3px;
    color: #333;
    display: block;
    font-size: 15px;
    height: 50px;
    overflow: hidden;
    padding: 10px;
    width: 100%;}
.sentmail_id{color: #666;
    display: block;
    font-size: 13px;
    margin: 5px 0 0;
    overflow: hidden;}
.have_account{color: #333;
    display: block;
    font-size: 14px;
    font-weight: normal;
    margin: 3px 0 0;
    overflow: hidden;}
.pre_forgot{display: block;
    font-size: 13px;
    margin: 5px 0 0;
    overflow: hidden;}
.prebok_hding.spl_sigin{background: #424a5d none repeat scroll 0 0;
    border-bottom: medium none;
    color: #fff;}
.mob_hi {
    display: block;
    margin: 12px 0 0;
    overflow: hidden;
    text-align: center;
}
.sign_twosec{display:block;overflow:hidden;}
#i_have_account{display:none;}
.pre_summery .popuperror{margin:0;}

.confirm_mfg{display: block;
    font-size: 20px;
    margin: 0 0 15px;
    overflow: hidden;
    text-align: center;}
.confrm_smmry {border: 1px solid #e6e6e6;
    border-radius: 3px;
    box-shadow: 0 0 5px #e6e6e6;
    display: block;
    margin: 0 0 20px;
    overflow: hidden;
    padding: 10px;}
.btn_comnbtns:hover{color:#fff;}
.cartsec_time{color: #666;
    display: block;
    font-family: roboto;
    font-size: 12px;
    overflow: hidden;}
.cartsec_more{border-top: 1px dashed #ddd;
    color: #666;
    display: block;
    font-size: 12px;
    margin: 5px 0 0;
    overflow: hidden;}
.pad_summery_sec{display: block;
    overflow: hidden;
    padding: 10px;}

/* Hotel booking*/


.detail_htlname{color: #07253f;
    float: left;
    font-size: 22px;
    font-weight: 500;
    margin-right: 10px;}
.star_detail{float: left;}
.stra_hotel{display: block;
    margin: 5px 0;
    overflow: hidden;}
.stra_hotel .fa{color: #91a9b1;font-size: 13px;margin: 5px 2px;}
.stra_hotel[data-star="5"] .fa:nth-child(1), 
.stra_hotel[data-star="5"] .fa:nth-child(2), 
.stra_hotel[data-star="5"] .fa:nth-child(3), 
.stra_hotel[data-star="5"] .fa:nth-child(4), 
.stra_hotel[data-star="5"] .fa:nth-child(5),
.stra_hotel[data-star="4"] .fa:nth-child(1), 
.stra_hotel[data-star="4"] .fa:nth-child(2), 
.stra_hotel[data-star="4"] .fa:nth-child(3), 
.stra_hotel[data-star="4"] .fa:nth-child(4),
.stra_hotel[data-star="3"] .fa:nth-child(1), 
.stra_hotel[data-star="3"] .fa:nth-child(2), 
.stra_hotel[data-star="3"] .fa:nth-child(3),
.stra_hotel[data-star="2"] .fa:nth-child(1), 
.stra_hotel[data-star="2"] .fa:nth-child(2),
.stra_hotel[data-star="1"] .fa:nth-child(1){ color:#f88c3e;}	

.hotel_prebook{display: block;
    height: 100px;
    margin: 10px 15px;
    overflow: hidden;}
.hotel_prebook img{min-height: 100px;
    width: 100%;}
.splalert{padding:30px 0;}


.all_oters{display: block;
    overflow: hidden;
    padding: 0 15px;}
.para_sentnce{color: #666;
    display: block;
    font-size: 13px;
    margin: 15px 0;
    overflow: hidden;}
.para_sentnce strong{}
.detlnavi {
    display: block;
    overflow: hidden;
}
.detlnavi_activity {
    display: block;
    overflow: hidden;
    padding: 10px 10px 10px 0;
}
.flitruo {
    display: block;
    margin: 10px 0;
    overflow: hidden;
    padding: 10px 0;
}

.pasngerall{display: block;
    overflow: hidden;
    padding: 0 0 10px;
    text-align: left;}
.width20 {
    width: 20%;
}
.width80 {
    width: 80%;
}





/*transfer_payment*/
.botomparts {
    display: block;
    margin: 0 15px;
    overflow: hidden;
    transition: all 400ms ease-in-out 0s;
}
.relatdays::after {
    border-left: 5px dotted #dddddd;
    border-radius: 5px;
    bottom: 0;
    content: "";
    left: 19px;
    position: absolute;
    top: 0;
    width: 0;
    z-index: 0;
}
.relatdays {
     display: block;
    margin: 20px 15px;
    padding-left: 50px;
    position: relative;
    transition: all 400ms ease-in-out 0s;
}
.backdot {
    display: block;
    min-height: 45px;
    position: relative;
    transition: all 400ms ease-in-out 0s;
    z-index: 1;
}
.backdot .absicon {
   background: #ffffff none repeat scroll 0 0;
    border: 2px solid #379b0c;
    border-radius: 30px;
    color: #fcae05;
    font-size: 20px;
    height: 44px;
    left: -50px;
    line-height: 39px;
    position: absolute;
    text-align: center;
    top: 0;
    width: 44px;
}
.synplace {
    color: #666666;
    display: block;
    font-size: 14px;
    overflow: hidden;
    padding: 10px 15px;
    position: relative;
    transition: all 400ms ease-in-out 0s;
}
.synplace strong, .synplace1 strong {
     color: #07253f;
    display: block;
    font-size: 16px;
    font-weight: normal;
    margin-bottom: 5px;
    overflow: hidden;
}
.disfind {
    display: block;
    min-height: 65px;
    overflow: hidden;
    padding-top: 20px;
    transition: all 400ms ease-in-out 0s;
}
.disfind .fa {
    color: #91a9b1;
    font-size: 14px;
    margin-right: 5px;
    transition: all 400ms ease-in-out 0s;
}
.distancar {
    color: #666666;
    font-size: 16px;
    transition: all 400ms ease-in-out 0s;
}
.synplace .classtr{color: #9c9c9c;
    display: block;
    margin-top: 5px;
    overflow: hidden;}
.backdot.connectiontr{}
.backdot.connectiontr::after {
    border-left: 1px dashed #0088ce;
    bottom: 0;
    content: "";
    left: -28px;
    position: absolute;
    top: 0;
    z-index: -1;
}
.connectiontr .absicon.fa {
    background: #0088ce none repeat scroll 0 0;
    color: #fff;
    font-size: 14px;
    height: 25px;
    left: -40px;
    line-height: 25px;
    top: 33px;
    width: 25px;
}
.connectiontr .disfind .fa{color: #0088ce;}
.connectiontr .distancar{color: #0088ce;}
.backdot:last-child .absicon {
    bottom: 0;
    top: auto;
	border:2px solid #f45104;
}
.backdot:last-child .synplace{ padding-bottom:0;}
.paytransfer .payrow{border-bottom: medium none;
    margin: 0;
    padding: 10px 0;}
.paytransfer .payrow b{border-right: 1px solid #eee;
    color: #666666;
    float: left;
    font-weight: normal;
    margin-right: 5px;
    padding: 10px;
    width: 20%;}
.tabletransfer .payrow > span {
    display: block;
    float: left;
    padding: 10px;
}

.ft{box-shadow: none;}
.nameoftransfer{display: block;
    overflow: hidden;
    padding: 15px;}
.lafname{ color: #333333;
    float: left;
    font-size: 14px;
    padding: 10px 0;
    text-transform: uppercase;}
.typetrip{ background: #eeeeee none repeat scroll 0 0;
    border-bottom: 1px solid #dddddd;
    color: #333333;
    font-size: 14px;
    padding: 5px 15px;}
.tabletransfer{background: #f6f6f6 none repeat scroll 0 0;
    border-top: 1px solid #eee;}
.tabletransfer .payrow {
     border-bottom: 1px solid #eee;
    padding: 0;
}
.tabletransfer .payrow:last-child{ border-bottom:none;}
.set_trips{border: 1px solid #ddd;
    border-radius: 3px;
    display: block;
    margin: 0 0 15px;
    overflow: hidden;}

.total_tranfer{color: #f88c3e;
    display: block;
    font-size: 20px;
    overflow: hidden;
    padding: 10px 0;}
.total_tranfer b{font-weight: normal;
    margin-right: 5px;}
.total_tranfer span{font-family: roboto;
    font-size: 24px;}
.paysubmit.modi_back{background: #666 none repeat scroll 0 0;}
.downbulet {
    margin: 15px 0 0;
}
.downbulet > li {
    color: #666666;
    font-size: 14px;
    list-style-position: inside;
    list-style-type: disc !important;
}
.final-booking-wrapper {
    display: block;
    float: left;
    overflow: hidden;
    width: 65%;
}
#final-booking-summary {
    float: left;
    text-align: right;
    width: 70%;
}
.finalclass{font-size: 18px;
    margin: 0;
    padding: 10px 15px;}
.finalclass span, .finalclass strong{color: #f88c3e;}
.finalclass strong{ font-family: roboto;
    font-size: 24px;
    font-weight: 500;}
#final-booking-wrapper .paysubmit{float: left;
    margin: 0 0 20px;
    width: 30%;}
.actprebook{display: block;
    overflow: hidden;
    padding: 0 15px 15px;}
.checkcontent .lbllbl{white-space: normal;}
.international_passport_content_div .formlabel{color:#666;}

@media screen and (max-width: 991px) {
.set_margin{margin:10px 0;}
.spllty.set_margin{float: left;
    width: 100%;}
.payrow .col-md-4{margin: 10px 0;}
.billingnob .payrow {
    border-bottom: medium none;
    margin: 0 -15px;
    padding: 0;
}
}

@media screen and (max-width: 768px) {
.sidebuki{float:none;}
.sumry_wrap, .padleftpay{padding:0;}

}

.invalid-ip {
		border: 1px solid #bf7070 !important;
	}

</style>
<?php
//debug($pre_booking_params['token']['total_price']);exit;
//debug($flight_details);exit;
$change_panelty = @$flight_details['change_panelty'][0];
$cncel_panelty = @$flight_details['cncel_panelty'][0];
$total_price = $flight_details['total_price'];
$currency = $flight_details['total_price_curr'];
$base_price = $flight_details['base_price'];
$taxes = $flight_details['taxes'];
$lead_pax_details = $pax_details[0];
$dob_dp_placeholder = 'dd/mm/yy';

$pre_booking_params['token']['total_price'] = $total_price;
$pre_booking_params['token']['currency'] = $currency;

$template_images = $GLOBALS['CI']->template->template_images();
$mandatory_filed_marker = '<sup class="text-danger">*</sup>';

$pax_default_country = (isset($lead_pax_details['country_code']) ? $lead_pax_details['country_code'] : INDIA_CODE);
$nationality_options = generate_options($iso_country_list, array($pax_default_country));//FIXME get ISO CODE --- ISO_INDIA

Js_Loader::$js[]	= array('src' => $GLOBALS['CI']->template->template_js_dir('provablib.js'), 'defer' => 'defer');
Js_Loader::$css[]	= array('href' => $GLOBALS['CI']->template->template_css_dir('page_resource/pre_booking.css'));
Js_Loader::$css[]	= array('href' => $GLOBALS['CI']->template->template_css_dir('page_resource/flight_result.css'));
Js_Loader::$js[]	= array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/booking_script.js'), 'defer' => 'defer');
Js_Loader::$js[]	= array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/flight_booking.js'), 'defer' => 'defer');

$flights_array = $flight_details['flights'];
//First Adult is Primary and and Lead Pax
$adult_enum = $child_enum = $form_params['title'];
$gender_enum = $form_params['gender'];

unset($adult_enum[MASTER_TITLE]); // Master is for child so not required
unset($child_enum[MASTER_TITLE]); // Master is not supported in TBO list
$title_options = generate_options($adult_enum, false, true);
$gender_options	= generate_options($gender_enum);
if(is_logged_in_user()) {
        $review_active_class = ' success ';
        $review_tab_details_class = '';
        $review_tab_class = ' inactive_review_tab_marker ';
        $travellers_active_class = ' active ';
        $travellers_tab_details_class = ' gohel ';
        $travellers_tab_class = ' travellers_tab_marker ';
    } else {
        $review_active_class = ' active ';
        $review_tab_details_class = ' gohel ';
        $review_tab_class = ' review_tab_marker ';
        $travellers_active_class = '';
        $travellers_tab_details_class = '';
        $travellers_tab_class = ' inactive_travellers_tab_marker ';
    }

?>

<div class="full onlycontent top80">
	<div class="container martopbtm">
		<div class="fldealsec">
            <div class="container">
                <div class="tabcontnue">
                    <div class="col-xs-4 nopadding">
                        <div class="rondsts <?=$review_active_class?>">
                            <a class="taba core_review_tab <?=$review_tab_class?>" id="stepbk1">
                                <div class="iconstatus fa fa-eye"></div>
                                <div class="stausline">Review</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-4 nopadding">
                        <div class="rondsts <?=$travellers_active_class?>">
                            <a class="taba core_travellers_tab <?=$travellers_tab_class?>" id="stepbk2">
                                <div class="iconstatus fa fa-group"></div>
                                <div class="stausline">Travelers</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-4 nopadding">
                        <div class="rondsts">
                            <a class="taba" id="stepbk3">
                                <div class="iconstatus fa fa-money"></div>
                                <div class="stausline">Payments</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="paymentpage">
			<div class="col-md-4 col-sm-4 nopad sidebuki">
				<div class="cartbukdis">
					<ul class="liscartbuk">
<!--						<li class="lostcart">-->
<!--							<div class="cartlistingbuk">-->
<!--								<div class="cartitembuk">-->
<!--									<div class="col-md-12">-->
<!--										<div class="payblnhmxm">Promo code</div>-->
<!--									</div>-->
<!--								</div>-->
<!--								<div class="clearfix"></div>-->
<!--								<div class="cartitembuk prompform">-->
<!--									<form action="" name="promocode" id="promocode" novalidate="novalidate">-->
<!--										<div class="col-md-8 col-xs-8">-->
<!--											<div class="cartprc">-->
<!--												<div class="payblnhm singecartpricebuk ritaln">-->
<!--													<input type="text" required="" placeholder="Enter Promo" name="code" id="code" class="promocode" aria-required="true" />-->
<!--												</div>-->
<!--											</div>-->
<!--										</div>-->
<!--										<div class="col-md-4 col-xs-4">-->
<!--											<input type="submit" value="Apply" name="apply" class="promosubmit">-->
<!--										</div>-->
<!--									</form>-->
<!--								</div>-->
<!--								<div class="clearfix"></div>-->
<!--								<div class="savemessage"></div>-->
<!--							</div>-->
<!--						</li>-->
						
						<li class="lostcart" id="booking-fare-summary">
							<div class="cartlistingbuk">
								<div class="cartitembuk">
									<div class="col-md-5 celcart">
										<div class="payblnhm">Total Fare</div>
									</div>
									<div class="col-md-7 celcart">
										<div class="cartprc">
											<div class="ritaln cartcntamnt normalprc"><?=$currency?> <?=$total_price?></div>
										</div>
									</div>
								</div>
								<div class="cartitembuk">
									<div class="col-md-5 celcart">
										<div class="payblnhm">Base Fare</div>
									</div>
									<div class="col-md-7 celcart">
										<div class="cartprc">
											<div class="ritaln cartcntamnt normalprc discount"><?=$currency?> <span class="amount"><?=$base_price?></span></div>
										</div>
									</div>
								</div>
								<div class="cartitembuk">
									<div class="col-md-5 celcart">
										<div class="payblnhm">Tax</div>
									</div>
									<div class="col-md-7 celcart">
										<div class="cartprc">
											<div class="ritaln cartcntamnt normalprc discount"><?=$currency?> <span class="amount"><?=$taxes?></span></div>
										</div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
							<div class="cartlistingbuk nomarr">
								<div class="cartitembuk">
									<div class="col-md-6 celcart">
										<div class="payblnhm">Total</div>
									</div>
									<div class="col-md-6 celcart">
										<div class="cartprc">
											<div class="ritaln cartcntamnt bigclrfnt finalAmt"><?=$currency?> <span class="amount"><?=$total_price?></span></div>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 nopad fulbuki">
				<div class="sumry_wrap">
					<!--  Flight Summery  -->
					<div class="pre_summery">
                    <?php //debug($search_data['trip_type']) ;
                          if ($search_data['trip_type'] == 'multicity') {
                         for ($pi=0; $pi < count($search_data['from_city']); $pi++) {
                    ?>
                              <div class="prebok_hding">
                                <?=$search_data['from_city'][$pi]?><span class="fa fa-exchange"></span> <?=$search_data['to_city'][$pi]?>
                            </div>
                    <?php } } else { ?>
                              <div class="prebok_hding">
                                <?=$search_data['from_city']?><span class="fa fa-exchange"></span> <?=$search_data['to_city']?>
                            </div>
                    <?php }
                          
                    ?>
						<!-- <div class="prebok_hding">
							<?=$search_data['from_city']?><span class="fa fa-exchange"></span> <?=$search_data['to_city']?>
						</div> -->
						<div class="pad_summery_sec">
						<?php
							foreach ($flights_array as $k => $v) {
							?>
							<div class="sidenamedesc">
								<div class="celhtl width20 midlbord">
									<div class="fligthsmll"> <img alt="" src="<?=SYSTEM_IMAGE_DIR . '/airline_logo/'.$v['carrier']?>.gif"> </div>
									<div class="airlinename"><?=$v['carrier']?> - <?=@$v['flight_number']?></div>
								</div>
								<div class="celhtl width80">
									<div class="waymensn">
										<div class="flitruo cloroutbnd">
											<div class="detlnavi">
												<div class="col-xs-4 padflt widfty">
													<span class="timlbl right"> <span class="flname"><span class="sprite reflone"></span><?=$v['origin']?><span class="fltime"><?=$v['departure_time']?></span></span> </span>
													<div class="clearfix"></div>
													<span class="flitrlbl elipsetool"><?=$v['departure_date']?> </span>
													<div class="rndplace"><?=$v['origin_city']?></div>
												</div>
												<div class="col-xs-4 nopad padflt widfty">
													<div class="lyovrtime">
														<span class="flect"> <span class="sprite retime"></span><?=@$v['duration']?></span>
														<div class="instops ">
															<!--if one stop add class  'morestop' - more than one, also add class 'plusone'-->
															<a class="stopone">
															<label class="rounds"></label>
															</a> <a class="stopone">
															<label class="rounds oneonly"></label>
															<label class="rounds oneplus"></label>
															</a> <a class="stopone">
															<label class="rounds"></label>
															</a> 
														</div>
<!--														<span class="flects"> <?=$k?> stop</span> -->
													</div>
												</div>
												<div class="col-xs-4 padflt widfty">
													<span class="timlbl left"> <span class="flname"><span class="sprite refltwo"></span><?=$v['destination']?><span class="fltime"><?=$v['arrival_time']?></span> </span> </span>
													<div class="clearfix"></div>
													<span class="flitrlbl elipsetool"><?=$v['arrival_date']?></span>
													<div class="rndplace"><?=$v['destination_city']?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<?php
								if (isset($flight_details[$k+1]) == true) {?>
									<div class="layoverdiv hide">
										<div class="centovr"> <span class="fa fa-plane"></span> 
											Change of planes at <?=$flight_details[$k+1]['origine']?> Airport | 
											<span class="fa fa-clock-o"></span> Connection Time : 5h 40m 
										</div>
									</div>
								<?php
								}
							}?>
						</div>
					</div>
					<!--  Flight Summery Ends Here -->
					<div class="clearfix"></div>
					<?php if(is_logged_in_user() == false) { ?>
					<div class="bktab1 pre_summery user-login-guest ">
						<!-- Enable Later  hide-->
						<div class="prebok_hding spl_sigin"> Sign in now to Book Online </div>
						<div class="signing_detis">
							<form name="login" id="login_prebook" novalidate="novalidate">
								<div class="col-md-6 nopad">
									<div class="wrp_pre">
										<input type="text" required="" placeholder="Email Address" name="booking_user_name" id="booking_user_name" class="_guest_validate form-control logpadding pre_put" aria-required="true" maxlength="80">
										<span class="sentmail_id">Your booking details will be sent to this email address.</span> 
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="wrp_pre">
									<div class="squaredThree">
										<input type="checkbox" id="ihave" class="filter_airline" name="confirm" value="0">
										<label for="ihave"></label>
									</div>
									<label class="have_account" for="ihave">I have an Account</label>
								</div>
								<div class="clearfix"></div>
								<div class="sign_twosec">
									<div id="i_have_account" class="section_sign">
										<div class="col-md-6 nopad">
											<div class="wrp_pre">
												<input type="password" placeholder="Password" id="booking_user_password" name="booking_user_password" class="form-control logpadding pre_put">
												<a id="forgtpsw" class="fadeandscale_close fadeandscaleforget_open forgtpsw pre_forgot">Forgot password?</a>
											</div>
											<div class="clearfix"></div>
											<div class="alert-danger"></div>
											<div class="wrp_pre">
												<input type="submit" value="Continue" id="continue_as_user" name="continue" class="paysubmit">
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div id="con_as_guest" class="section_sign">
										<div class="col-md-6 nopad">
											<div class="wrp_pre">
												<div class="col-xs-2 nopad">
													<input type="text" placeholder="+41" class="pre_put form-control" required="" name="pn_country_code" aria-required="true" >
												</div>
												<div class="col-xs-1 nopad">
													<div class="mob_hi">-</div>
												</div>
												<div class="col-xs-9 nopad">
													<input type="text" id="booking_user_mobile" placeholder="Mobile Number" required="" class="_numeric_only _guest_validate form-control pre_put" maxlength="10" aria-required="true">
												</div>
												<div class="clearfix"></div>
												<div class="sentmail_id">We'll use this number to send
													possible update alerts.
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="wrp_pre">
												<input type="button" value="Continue as guest" id="continue_as_guest" name="continue_c" class="paysubmit">
											</div>
										</div>
									</div>
								</div>
								<div class="wrp_pre">
									Don't have an account? 
									<a class="fadeandscale_close fadeandscaleregbooking_open">Sign up</a>							
								</div>
							</form>
						</div>
						<div style="display:none" class="signing_detis_confirm">
							<div class="col-md-6 nopad">
								<div class="wrp_pre"> <span id="user_logs_status" class="sentmail_id"></span> </div>
							</div>
							<div class="clearfix"></div>
							<form action="">
								<div class="sign_twosec">
									<div class="section_sign">
										<div class="col-md-6 nopad">
											<div class="wrp_pre">
												<input type="submit" value="Continue" id="continue_v" name="continue_v" class="paysubmit">
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								
							</form>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			
			<div <?=(is_logged_in_user() == true ? '' : 'style="display:none"')?> class="bktab2 col-md-8 col-sm-8 nopad fulbuki ">
				<form action="<?=base_url().'index.php/flight/payment/'.$search_data['search_id']?>" method="POST" autocomplete="off" id="checkout-passenger">
					<div class="hide">
						<input type="hidden" required="required" name="search_id"		value="<?=$search_data['search_id'];?>" />
						<?php $dynamic_params_url = serialized_data($pre_booking_params);?>
						<input type="hidden" required="required" name="token"		value="<?=$dynamic_params_url;?>" />
						<input type="hidden" required="required" name="token_key"	value="<?=md5($dynamic_params_url);?>" />
						<input type="hidden" required="required" name="op"	value="book_flight">
						<input type="hidden" required="required" name="booking_source"	value="<?=$booking_source?>" readonly>
					</div>
					<div class="col-md-12 padleftpay">
						<div class="wrappay leftboks">
							<div class="comon_backbg">
								<h3 class="inpagehed">Traveller</h3>
								<div class="sectionbuk">
									<div id="collapse102" class="collapse in">
										<div class="onedept">
											<div class="evryicon"><span class="fa fa-plane"></span></div>
											<div class="pasenger_location">
												<?php //debug($search_data['trip_type']) ;
                                                  if ($search_data['trip_type'] == 'multicity') {
                                                 for ($pi=0; $pi < count($search_data['from_city']); $pi++) {
                                            ?>
                                                      <h3 class="inpagehedbuk"> 
                                                    <span class="bookingcnt">1.</span> <span class="aptbokname"><?=$search_data['from_city'][$pi]?> - <?=$search_data['to_city'][$pi]?></span>
                                                </h3>
                                            <?php } } else { ?>
                                                      <h3 class="inpagehedbuk"> 
                                                    <span class="bookingcnt">1.</span> <span class="aptbokname"><?=$search_data['from_city']?> - <?=$search_data['to_city']?></span>
                                                </h3>
                                            <?php }
                                                  
                                            ?>
												<span class="hwonum">Adult <?=$search_data['adult_config']?></span> 
												<span class="hwonum">Child <?=$search_data['child_config']?></span> 
												<span class="hwonum">Infant <?=$search_data['infant_config']?></span>
											</div>
											<div class="clearfix"></div><?php 
											$adult_dp_view = $adult_dp_val = '';
											$pass_no_val = '';
											$pass_exp_dp_val = '';
											$pass_dp_placeholder = 'Passport Expiry Date';
											for ($pax_index=1; $pax_index<=$search_data['total_passenger']; $pax_index++) {
												$cur_pax_info = is_array($pax_details) ? array_shift($pax_details) : array();
												?><div class="payrow">
													<div class="repeatprows">
														<div class="col-md-1 downsrt set_margin">
															<div class="lokter"><?php 
																$pax_type = pax_type($pax_index, $search_data['adult_config'], $search_data['child_config'], $search_data['infant_config']);
																$dp_type = '';
																$dp_wrapper = '';
																$dp_date_val = '';
																switch ($pax_type) {
																	case 'adult' :
																		echo '<span class="fa fa-user"></span>';
																		$dp_id = 'adult-date-picker-'.$pax_index;
																		$dp_type = ADULT_DATE_PICKER;
																		
																		break;
																	case 'child' :
																		echo '<span class="fa fa-male"></span>';
																		$dp_id = 'child-date-picker-'.$pax_index;
																		$dp_type = CHILD_DATE_PICKER;
														
																		break;
																	case 'infant' :
																		echo '<span class="fa fa-child"></span>';
																		$dp_id = 'infant-date-picker-'.$pax_index;
																		$dp_type = INFANT_DATE_PICKER;
																		
																		break;
																}
																$datepicker_list[] = array($dp_id, $dp_type);
															?><span class="whoare"><?=(ucfirst($pax_type))?>(<?=$pax_index?>)</span> </div>
															</div>
															<div class="col-md-2 set_margin">
																<div class="selectedwrap">
																	<select name="title[]" class="flpayinput">
																		<?=$title_options?>
																	</select>
																</div>
															</div>
															<div class="col-md-3 set_margin">
																<input value="<?=@$cur_pax_info['first_name']?>" required="required" type="text" name="first_name[]" id="passenger-first-name-<?=$pax_index?>" class="payinput clainput alpha <?=(is_logged_in_user() ? 'user_traveller_details' : '')?>" maxlength="45" placeholder="First Name" data-row-id="<?=($pax_index);?>"/>
															</div>
															<div class="col-md-3 set_margin">
																<input value="<?=@$cur_pax_info['last_name']?>" required="required" type="text" name="last_name[]" id="passenger-last-name-<?=$pax_index?>" class="payinput clainput alpha" maxlength="45" placeholder="Last Name" />
															</div>
															<div class="col-md-3 set_margin <?=$dp_wrapper?>">
																<input data-format="dd/mm/yy" value="<?=$dp_date_val?>" placeholder="<?=$dob_dp_placeholder?>" type="text" class="payinput adt"  name="date_of_birth[]" readonly="readonly" <?=(is_adult($pax_index, $search_data['adult_config']) ? 'required="required"' : 'required="required"')?> id="<?=$dp_id?>">
															</div>
														</div>
														<div class="international_passport_content_div">
															<div class="col-xs-4 spllty">
																<span class="formlabel">Passport Number <sup class="text-danger">*</sup></span>
																<div class="relativemask"> 
																	<input value="<?=$pass_no_val?>" type="text" name="passenger_passport_number[]" required="required" id="passenger_passport_number_<?=$pax_index?>" class="payinput clainput" maxlength="10" placeholder="Passport Number" />
																</div>
															</div>
															<div class="col-xs-3 spllty">
																<span class="formlabel">Issuing Country <sup class="text-danger">*</sup></span>
																<div class="selectedwrap">
																	<select name="passenger_passport_issuing_country[]" required="required" id="passenger_passport_issuing_country_<?=$pax_index?>" class="mySelectBoxClass flyinputsnor">
																		<option value="INVALIDIP">Please Select</option>
																		<?=generate_options($country_list, array($pass_issue_val))?>
																	</select>
																</div>
															</div>
															<?php
															$dp_id = 'passport'.$pax_index;
															$datepicker_list[] = array($dp_id, FUTURE_DATE);
															?>
															<div class="col-xs-5 spllty">
																<span class="formlabel">Date of Expire <sup class="text-danger">*</sup></span>
																<input value="<?=$pass_exp_dp_val?>" placeholder="<?=$pass_dp_placeholder?>" data-format="dd/mm/yy" type="text" id="<?=$dp_id?>" name="passenger_passport_expiry_date[]" class="payinput">
															</div>
														</div>
														<div class="hide hidden_pax_details">
															<input type="hidden" name="passenger_type[]" value="<?=ucfirst($pax_type)?>">
															<input type="hidden" name="lead_passenger[]" value="<?=(is_lead_pax($pax_index) ? true : false)?>">
															<input type="hidden" name="gender[]" value="1" class="pax_gender">
															<input type="hidden" required="required" name="passenger_nationality[]" id="passenger-nationality-<?=$pax_index?>" value="92">
														</div>
												</div><?php 
											}?>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="clearfix"></div>
						
						<div class="comon_backbg">
							<h3 class="inpagehed">Address</h3>
							<div class="sectionbuk billingnob">
								<?php
								$option_view = '';
								$address = @$lead_pax_details['address'];
								if (isset($trans_mand['BillingAddress']) == false) {
									$option_view = 'hide';
									if (empty($address)) {
										$address = 'Address';
									}
								}
								?>
								<div class="payrow">
									<div class="col-md-4">
										<div class="paylabel">Street</div>
										<input value="" type="text" maxlength="80" required="required" id="street" class="payinput newslterinput nputbrd" name="street">
									</div>
									<div class="col-md-4">
										<div class="paylabel">City</div>
										<input type="text" required="" value="" class="payinput" name="city" id="billing_city">
									</div>
									<div class="col-md-4">
										<div class="paylabel">State</div>
										<input type="text" required="" value="" class="payinput" name="state" id="billing_state">
									</div>
									
								</div>
								<div class="payrow">
									<div class="col-md-4">
										<div class="paylabel">Country</div>
										<select required="" name="billing_country" id="country" class="flpayinput">
											<?=$nationality_options?>
										</select>
									</div>
									<div class="col-md-4">
										<div class="paylabel">Contact</div>
										<input value="<?=@$lead_pax_details['phone'] == 0 ? '' : @$lead_pax_details['phone'];?>" type="text" name="billing_passenger_contact" id="passenger-contact" placeholder="Mobile Number" class="payinput newslterinput nputbrd _numeric_only" maxlength="10" required="required">
									</div>
									<div class="col-md-4">
										<div class="paylabel">Postal Code</div>
										<input type="text" required="" value="<?=@$post_code?>" class="payinput" name="postal_code" id="billing_postal_code">
									</div>
								</div>
								<div class="payrow">
									<div class="col-md-4">
										<div class="paylabel">Email Address</div>
										<input value="<?=@$lead_pax_details['email']?>" type="text" maxlength="80" required="required" id="billing-email" class="payinput newslterinput nputbrd" placeholder="Email" name="billing_email">
									</div>
								</div>
								<span class="noteclick"> After clicking "Book it" you will be redirected to payment gateway. You must complete the process or the transaction will not occur. </span> 
							</div>
						</div>
						
						<div class="clearfix"></div>
						
						<div class="col-md-12 nopad">
							<div class="checkcontent">
								<div class="squaredThree">
									<input type="checkbox" required="" id="terms_cond1" class="filter_airline "   name="tc" value="0">
									<label for="terms_cond1"></label>
								</div>
								<label class="lbllbl" for="terms_cond1">By booking this item, you agree to pay the total amount shown, which includes Service Fees, on the right and to the<a class="colorbl"> Terms &amp; Condition</a>, <a class="colorbl" href="">Cancellation Policy</a>.</label>
							</div>
						</div>
						
						<div class="clearfix"></div>
						
						<div class="payrowsubmt">
							<div class="col-md-3 col-xs-3 fulat500 nopad">
							<?php
								//If single payment option then hide selection and select by default
								if (count($active_payment_options) == 1) {
									$payment_option_visibility = 'hide';
									$default_payment_option = 'checked="checked"';
								} else {
									$payment_option_visibility = 'show';
									$default_payment_option = '';
								}
								
								?>
							<div class="row <?=$payment_option_visibility?>">
								<?php if (in_array(PAY_NOW, $active_payment_options)) {?>
								<div class="col-md-3">
									<div class="form-group">
										<label for="payment-mode-<?=PAY_NOW?>">
										<input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_NOW?>" id="payment-mode-<?=PAY_NOW?>" class="form-control b-r-0" placeholder="Payment Mode">
										Pay Now
										</label>
									</div>
								</div>
								<?php } ?>
								<?php if (in_array(PAY_AT_BANK, $active_payment_options)) {?>
								<div class="col-md-3">
									<div class="form-group">
										<label for="payment-mode-<?=PAY_AT_BANK?>">
										<input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_AT_BANK?>" id="payment-mode-<?=PAY_AT_BANK?>" class="form-control b-r-0" placeholder="Payment Mode">
										Pay At Bank
										</label>
									</div>
								</div>
								<?php } ?>
							</div>
								<input type="submit" value="Continue" id="continue_to_travelport_booking" name="continue" class="paysubmit">
							</div>
							<div class="col-md-9 col-xs-3 fulat500 nopad"> </div>
							<div class="clear"></div>
							<div class="lastnote"> </div>
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div>
</div>
<?php 
$GLOBALS['CI']->current_page->set_datepicker($datepicker_list);
/**
 * check if current print index is of adult or child by taking adult and total pax count
 * @param number $total_pax		total pax count
 * @param number $total_adult	total adult count
 */
function pax_type($pax_index, $total_adult, $total_child, $total_infant)
{
	if ($pax_index <= $total_adult) {
		$pax_type = 'adult';
	} elseif ($pax_index <= ($total_adult+$total_child)) {
		$pax_type = 'child';
	} else {
		$pax_type = 'infant';
	}
	return $pax_type;
}

/**
 * check if current print index is of adult or child by taking adult and total pax count
 * @param number $total_pax		total pax count
 * @param number $total_adult	total adult count
 */
function is_adult($pax_index, $total_adult)
{
	return ($pax_index>$total_adult ?	false : true);
}

/**
 * check if current print index is of adult or child by taking adult and total pax count
 * @param number $total_pax		total pax count
 * @param number $total_adult	total adult count
 */
function is_lead_pax($pax_count)
{
	return ($pax_count == 1 ? true : false);
}

?>