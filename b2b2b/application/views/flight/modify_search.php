<?php 
	//~ echo "data: <pre>";print_r($req); 
?>
<div class="newmodify">
	<div class="container">
		<div class="contentsdw">
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-8 nopad">
				<div class="pad_ten">
					<div class="from_to_place">
						<h4 class="placename">Multi City</h4>
						<h3 class="contryname">Bangalore (BLR) to Goa (GOI)</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xs-6 hidden-sm hidden-xs nopad">
				<div class="col-xs-6 nopad">
					<div class="pad_ten">
						<div class="from_to_place">
							<div class="boxlabl">Departure</div>
							<div class="datein"> <span class="calinn">
							Tue, 15 Nov</span> </div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 nopad ">
					<div class="pad_ten">
						<div class="from_to_place">
							<div class="boxlabl">Return</div>
							<div class="datein"> <span class="calinn">-- </span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-2 hidden-md hidden-sm hidden-xs nopad">
				<div class="pad_ten">
					<div class="from_to_place">
						<div class="boxlabl textcentr"><strong></div>
						<div class="boxlabl textcentr"><strong></div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-4 nopad">
				<div class="pad_ten">
					<button class="modifysrch" data-toggle="collapse" data-target="#modify"><strong><?php echo $this->thechinagap['HotelSearch']['Modify']; ?></strong> <span class="down_caret"></span></button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="modify_search_wrap">
	<form autocomplete="off" onSubmit="return validateform();" action="<?php echo ASSETS; ?>hotel/search" name="hotelSearchForm" id="hotelSearchForm">
		<div class="container">
			<div id="modify" class="collapse">
				<div class="insplarea">
					<div class="intabs">
						<div class="outsideserach">
							<div class="col-lg-6 col-md-6 col-sm-12 fiveh">
                            	<div class="marginbotom10">
								<span class="formlabel"><?php echo $this->thechinagap['HotelSearch']['Destination']; ?></span>
								<div class="relativemask"> <span class="maskimg hfrom"></span> 
									<input name="city" value="<?php echo $req->city; ?>" id="hotel_autocomplete" type="text" placeholder="<?php echo $this->thechinagap['HotelSearch']['CityName']; ?>" class="ft normalinput form-control" required >
								</div>
                                </div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 nopad">
                            	<div class="marginbotom10">
								<div class="col-lg-5 col-md-5 col-xs-4 fiveh fiftydiv">
									<span class="formlabel"><?php echo $this->thechinagap['HotelSearch']['CheckIn']; ?></span>
									<div class="relativemask"> <span class="maskimg caln"></span>
										<input id="check-in" name="checkin_date" value="<?php echo date('d-m-Y',strtotime($req->hotel_checkin)); ?>" type="text" placeholder="<?php echo $this->thechinagap['HotelSearch']['CheckIn']; ?>" class="forminput" readonly required >
									</div>
								</div>
								<div class="col-lg-5 col-md-5 col-xs-4 fiveh fiftydiv">
									<span class="formlabel"><?php echo $this->thechinagap['HotelSearch']['CheckOut']; ?></span>
									<div class="relativemask"> <span class="maskimg caln"></span>
										<input id="check-out" name="checkout_date" value="<?php echo date('d-m-Y',strtotime($req->hotel_checkout)); ?>" type="text" placeholder="<?php echo $this->thechinagap['HotelSearch']['CheckOut']; ?>" class="forminput" readonly required >
									</div>
								</div>
								<div class="col-lg-2 col-md-2 col-xs-4 fiveh fiftydiv">
									<span class="formlabel"><?php echo $this->thechinagap['HotelSearch']['Rooms']; ?></span>
									<div class="selectedwrap">
										<select name="rooms" class="mySelectBoxClass flyinputsnor" onchange="generateRACCombination()">
											<option <?php echo ($req->rooms == 1)?'selected':''; ?> >1</option>
											<option <?php echo ($req->rooms == 2)?'selected':''; ?> >2</option>
											<option <?php echo ($req->rooms == 3)?'selected':''; ?> >3</option>
											<option <?php echo ($req->rooms == 4)?'selected':''; ?> >4</option>
										</select>
									</div>
								</div>
                                </div>
							</div>
							<div id="room_info" class="RACContainer" >
								<?php
									if(!empty($req->rooms)):
										for($room_iter = 0; $room_iter < $req->rooms; $room_iter++ ):
								?>
											<div class="col-md-6 marginbotom10 nopad">
												<div class="col-xs-3 fiveh fulldiv">
													<span class="formlabel notinh">&nbsp;</span>
													<div class="roomnum"> <span class="numroom"><?php echo $this->thechinagap['HotelSearch']['Room']; ?> <?php echo $room_iter+1; ?></span></div>
												</div>
												<div class="col-xs-3 nopad fiftydiv">
													<div class="col-xs-6 fiveh fiftydiv">
														<span class="formlabel"><?php echo $this->thechinagap['HotelSearch']['Adult']; ?> <strong>(18+)</strong></span>
														<div class="selectedwrap">
															<select name="adult[]" class="mySelectBoxClass flyinputsnor" >
																<?php 
																	for($ad = 1; $ad < 5; $ad++ ):
																?>
																		<option value="<?php echo $ad; ?>" <?php echo ($ad == $req->adult[$room_iter])?'selected':''; ?> ><?php echo $ad; ?></option>
																<?php
																	endfor;
																?>
															 </select>
														</div>
													</div>
													<div class="col-xs-6 fiveh fiftydiv">
														<span class="formlabel"><?php echo $this->thechinagap['HotelSearch']['Child']; ?> <strong>(0-17)</strong></span>
														<div class="selectedwrap">
															<select name="child[]" class="mySelectBoxClass flyinputsnor" onchange="generateRACCombination()">
																<?php 
																	for($ch = 0; $ch < 5; $ch++ ):
																?>
																		<option value="<?php echo $ch; ?>" <?php echo ($ch == $req->child[$room_iter])?'selected':''; ?> ><?php echo $ch; ?></option>
																<?php
																	endfor;
																?>
															 </select>
														</div>
													</div>
												</div>
												
												<div class="child_age_wrapper">
												
												<?php
													if($req->child_count > 0 && $req->child[$room_iter] > 0):
												?>
														<div class="col-xs-6 nopad fiftydiv">
												<?php
															for($child_iter = 0; $child_iter < count($req->childAges[$room_iter]); $child_iter++):
													?>
																<div class="col-xs-3 fiveh fiftydiv" >
																	<span class="formlabel">Child <?php echo $child_iter+1; ?> <strong>(Age)</strong></span>
																	<div class="selectedwrap">
																		<select name="child_<?php echo $room_iter; ?>_Age[]" class="mySelectBoxClass flyinputsnor">
																			<?php 
																				for($ch_age = 0; $ch_age < 18; $ch_age++ ):
																			?>
																					<option value="<?php echo $ch_age; ?>" <?php echo ($ch_age == $req->childAges[$room_iter][$child_iter])?'selected':''; ?> ><?php echo $ch_age; ?></option>
																			<?php
																				endfor;
																			?>
																		 </select>
																	</div>
																</div>	
													<?php
																endfor;
													?>
																</div>
													<?php
													endif;
												?>
												
												</div>
											</div>
											<div class="clearfix"></div>
								<?php
										endfor;
									endif;
								?>
							</div>
							<div class="col-md-12 nopad marginbotom10">
								<div class="col-xs-4 fiveh fiftydiv">
									<span class="formlabel"><?php echo $this->thechinagap['HotelSearch']['Nationality']; ?></span>
									<div class="selectedwrap">
										<select name="nationality" class="mySelectBoxClass flyinputsnor">
											<?php
												if(!empty($nationality_countries) and is_array($nationality_countries)):
													foreach($nationality_countries as $country_list):
											?>
													<option value="<?php echo $country_list->iso_code; ?>" <?php echo ($country_list->iso_code == $req->nationality)?'selected':''; ?> ><?php echo $country_list->country_name; ?></option>
											<?php
													endforeach;
												endif;
											?>
										 </select>
									</div>
								</div>
                                <div class="col-xs-6 fiveh fiftydiv">
                                <span class="formlabel">&nbsp;</span>
								<div class="formsubmit downmrg">
									<button class="srchbutn comncolor"><?php echo $this->thechinagap['HotelSearch']['SearchHotels']; ?> <span class="srcharow"></span></button>
								</div>
							</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="clearfix"></div>

<script>
    function generateRACCombination() {
		var data = {};
		var data = $('#hotelSearchForm').serialize();
		$.ajax({
			type: 'POST',
			url: "<?php echo ASSETS; ?>dashboard/roomCombination",
			async: true,
			dataType: 'json',
			data: data,
			success: function(data) {
				$(".RACContainer").html(data);
			}
		});
	}
</script>
<script>
		$.widget( "custom.catcomplete", $.ui.autocomplete, {
    _create: function() {
      this._super();
      this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
    },

    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) { 
        var li;
        if ( item.type != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category "+item.type+"'>" + item.type + "</li>" );
          currentCategory = item.type;
        }
        li = that._renderItemData( ul, item );
        if ( item.type ) {
          li.attr( "aria-label", item.type + " : " + item.label );
        }
      });
    },
    _renderItem: function( ul, item ) { 
      if(item.type == 'City'){
      return $( "<li>" )
      //.addClass(item.category)
      .attr( "data-value", item.value )     
      .append( $( "<a>" ).html('<i class="fa fa-building-o wd20"></i> <span class="ctynme">'+item.label + '</span><span class="ctycnt">'+' <span class="pull-right" style="font-weight:600;">City</span></span>') )
      .appendTo( ul );
    }else{
      return $( "<li>" )
      //.addClass(item.category)
      .attr( "data-value", item.value )     
      .append( $( "<a>" ).html('<i class="fa fa-bed wd20"></i> <span class="areanme">'+item.label + '</span><span class="ctycnt">'+' <span class="pull-right" style="font-weight:600;">Hotel</span></span>') )
      .appendTo( ul );
    }
  }
});
	$( "#hotel_autocomplete" ).catcomplete({
		delay: 0,
		minLength: 3,
		source:'<?php echo ASSETS;?>hotel/get_hotel_city_suggestions',
		select: function(event, ui){
		  $('#CityID').val(ui.item.CityId);
		  $('#HotelId').val(ui.item.HotelCode);
		  $('#SearchType').val(ui.item.category);
		}
    });
    $( "#check-in" ).datepicker({
	  minDate: 0,
	  dateFormat: 'dd-mm-yy',
	  maxDate: "+1y",
	  numberOfMonths: 2,
	  onChange : function(){
	  },
	  onClose: function( selectedDate ) {
		//~ $( "#check-out" ).datepicker( "option", "minDate", selectedDate );
		var myDate = $(this).datepicker('getDate'); 
		myDate.setDate(myDate.getDate()+1); 
		$('#check-out').datepicker('setDate', myDate);
		$( "#check-out" ).datepicker( "option", "minDate", myDate );
		$( '#check-out' ).focus();
	  }
	});
	$( "#check-out" ).datepicker({
	  minDate: 0,
	  dateFormat: 'dd-mm-yy',
	  maxDate: "+1y",
	  numberOfMonths: 2,
	  onClose: function( selectedDate ) {
		//~ $( "#check-in" ).datepicker( "option", "maxDate", selectedDate );
	  }
	});
	</script>
