<style>
th,td{padding:5px;}
</style>

<?php
$CI =& get_instance();
$booking_details = $data ['booking_details'] [0];
$itinerary_details = $booking_details ['booking_itinerary_details'];
$attributes = $booking_details ['attributes'];
$customer_details = $booking_details ['booking_transaction_details'] [0] ['booking_customer_details'];
$domain_details = $booking_details;
$lead_pax_details = $customer_details;
$booking_transaction_details = $booking_details ['booking_transaction_details'];
$adult_count = 0;
$infant_count = 0;

foreach ($customer_details as $k => $v) {
	if (strtolower($v['passenger_type']) == 'infant') {
		$infant_count++;
	} else {
		$adult_count++;
	}
}

                            
                        
		                              $Onward = '';
		                              $return = '';
		                    if(count($booking_transaction_details) == 2) {
		                                 $Onward = '(Onward)';                             
		                                 $Return =  '(Return)';		                             
	                               }       
                           
	                        //generate onword and return        
                           if($booking_details['is_domestic'] == true && count($booking_transaction_details) == 2)
                           {
			                              $onward_segment_details = array();
			                           	  $return_segment_details = array();
			                           	  $segment_indicator_arr = array();
			                           	  $segment_indicator_sort= array();
			                           	  
			                           	  foreach ($itinerary_details as $key => $key_sort_data) {
			                           	  	$segment_indicator_sort[$key]  = $key_sort_data['origin'];
			                           	  	
			                           	  }
			                               array_multisort($segment_indicator_sort, SORT_ASC,$itinerary_details);
			                           	   //debug($itinerary_details);exit;
			                           	  
			                           	 foreach($itinerary_details as $k=>$sub_details ){
			                           	 	    $segment_indicator_arr[] = $sub_details['segment_indicator'];
			                           	 	    $count_value = array_count_values($segment_indicator_arr);
			                           	 		
			                           	 		if($count_value[1] == 1){
			                           	 	      $onward_segment_details[] =$sub_details;
			                           	 		}else{
			                           	 			$return_segment_details[] = $sub_details;
			                           	 		}
			                           	 	}
			                           	   
			                           	}
                           	
                                              
			                    ?>
			  <?php ob_start(); ?>                  
<table style="border-collapse: collapse; background: #ffffff;font-size: 12pt; margin: 0 auto; font-family: arial;" width="100%" cellpadding="0" cellspacing="0" border="0">
<tbody>
	<tr> 
	  <td style="border-collapse: collapse; padding:10px 20px 20px" >
	      <table width="100%" style="border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0">
	      	<tr><td style="font-size:15pt; line-height:30px; width:100%; display:block; font-weight:600; text-align:center">E-Ticket<?php echo $Onward;?></td></tr>
	       <tr>
					<td>
			       <table width="100%" style="border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0">
			       <tr>
					<td style="padding: 10px; width:65%">
						<?php
			             if($this->session->userdata('site_name') == ''){ ?>
			             <img style="width:150px;" src="<?php echo base_url(); ?>assets/images/logo.png">        
			            <?php } else {
			            if($this->session->userdata('user_type')==2)
			            { ?>
			              <img style="width:150px;" src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $CI->session->userdata('domain_logo'); ?>">
			            <? }
			            elseif ($this->session->userdata('user_type') == 5) {?>
			               <img style="width:150px;" src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $CI->session->userdata('domain_logo'); ?>"> 
			            <? }
			            else
			            { ?>
			               <img style="width:150px;" src="<?php echo ASSETS; ?>cpanel/uploads/agent/<?php echo $CI->session->userdata('domain_logo'); ?>"> 
			            <? } } ?>
					</td>
					<td style="padding: 10px; width:35%">
                    	<table width="100%" style="border-collapse: collapse;text-align: right; line-height:15px;" cellpadding="0" cellspacing="0" border="0">
                    		
                    		<tr><td style="font-size:12pt;"><span style="width:100%; float:left"><?php echo @$data['address'];?></span>
                    		</td></tr>
                         </table></td>
				</tr>
				</table>
				</td>
			</tr>
			<tr>
			 <td style="padding: 10px;">
			  <table cellpadding="5" cellspacing="0" border="0" width="100%" style="border-collapse: collapse;">
							<tr>
								<td width="100%" style="padding: 10px;border: 1px solid #cccccc; font-size: 11pt; font-weight: bold;">Reservation Lookup</td>
								
							</tr>
							<tr>
								<td style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 11pt;">
										<tr>
											<td><strong>Booking Reference</strong></td>
											<td><strong>Booking ID</strong></td>
											<td><strong>PNR</strong></td>
											<td><strong>Booking Date</strong></td>
											<td><strong>Status</strong></td>
										</tr>
										<tr>
										 
											<td><?=@$booking_details['app_reference']?></td>
											<td><?=@$booking_transaction_details[0]['book_id']?></td>
											<td><?=@$booking_transaction_details[0]['pnr']?></td>
											<td><?=app_friendly_absolute_date(@$booking_details['booked_date'])?></td>
											<td><strong class="<?php echo booking_status_label( $booking_details['status']);?>" style=" font-size:11pt;">
											<?php 
											switch($booking_details['status']){
												case 'BOOKING_CONFIRMED': echo 'CONFIRMED';break;
												case 'BOOKING_CANCELLED': echo 'CANCELLED';break;
												case 'BOOKING_FAILED': echo 'FAILED';break;
												case 'BOOKING_INPROGRESS': echo 'INPROGRESS';break;
												case 'BOOKING_INCOMPLETE': echo 'INCOMPLETE';break;
												case 'BOOKING_HOLD': echo 'HOLD';break;
												case 'BOOKING_PENDING': echo 'PENDING';break;
												case 'BOOKING_ERROR': echo 'ERROR';break;
												
											}
											
																				
											?>
											</strong></td>
										</tr>
									</table>
								</td>
							</tr>
							 <tr><td>&nbsp;</td></tr>
							 <?php if($admin_details)
							 { ?>
							 	<tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 11pt; font-weight: bold;">Please Contact administrator for confirm your booking .Contact No - <?= $admin_details->user_cell_phone ?> OR Email at  <?= $admin_details->user_email;?>   
								</td>
								
							</tr><?php } ?>
							<tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 11pt; font-weight: bold;">Journey Information</td>
								
							</tr>
							<tr>
								<td  width="100%" style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 11pt;">
										<tr>
											<td><strong>Flight</strong></td>
											<td><strong>Departure</strong></td>
											<td><strong>Arrival</strong></td>
											<td><strong>Journey Time</strong></td>
										</tr>
										<?php 
						if(isset($booking_transaction_details) && $booking_transaction_details != ""){
                          
							if($booking_details['is_domestic'] == true && count($booking_transaction_details) == 2){
								$itinerary_details = array();
								$itinerary_details = $onward_segment_details;
							}
								foreach($itinerary_details as $segment_details_k => $segment_details_v){
								
							
							  $itinerary_details_attributes = json_decode($segment_details_v['attributes']);
                              @$airline_craft = $itinerary_details_attributes->craft;
                              @$airline_terminal_origin = $itinerary_details_attributes->ws_val->OriginDetails->Terminal;
                              @$airline_terminal_destination = $itinerary_details_attributes->ws_val->DestinationDetails->Terminal;
                              $origin_terminal='';
                              $destination_terminal='';
                              if($airline_terminal_origin != ''){
                              	$origin_terminal = 'Terminal '.$airline_terminal_origin;
                              }
                              if($airline_terminal_destination != ''){
                              	$destination_terminal = 'Terminal '.$airline_terminal_destination;
                              }

                              if(valid_array($segment_details_v) == true) { ?>
                                     <tr>
                               
									 <td>
									 	<?php 
									 	$tr=json_decode($segment_details_v['attributes'],true); 
									 	$airline_logo= ASSETS.'assets/images/airline_logo/'.$segment_details_v['airline_code'].'.gif';  
									 	 ?>
                                        <img style="max-height: 100px" src="<?= $airline_logo?>" alt="flight-logo" />
                                        <br> 
                                        <span style="width:100%; float:left"><?=@$segment_details_v['airline_name']?></span>
                                        <span style="width:100%; float:left; font-size:13px; font-weight:bold"><?php echo $segment_details_v['airline_code'].' '.$segment_details_v['flight_number'];?></span></td> 
                                        <td style="line-height:16px">
                                        <span style="width:100%; float:left; font-size:13px; font-weight:bold"> <?=@$segment_details_v['from_airport_name'] ?>(<?=@$segment_details_v['from_airport_code']?>)</span>
                                        <span style="width:100%; float:left"><?php echo $origin_terminal;?></span>
                                        <span style="width:100%; float:left; font-weight:bold"> <?php echo date("d M Y",strtotime($segment_details_v['departure_datetime'])).", ".date("H:i",strtotime($segment_details_v['departure_datetime']));?></span></td>
                                        <td style="line-height:16px">
                                        <span style="width:100%; float:left; font-size:13px; font-weight:bold"> <?=@$segment_details_v['to_airport_name']?>(<?=@$segment_details_v['to_airport_code']?>)</span>
                                        <span style="width:100%; float:left"> <?php echo $destination_terminal;?></span>
                                        <span style="width:100%; float:left; font-weight:bold">  <?php echo date("d M Y",strtotime($segment_details_v['arrival_datetime'])).", ".date("H:i",strtotime($segment_details_v['arrival_datetime']));?></span></td>
                                        <td><!-- <span style="width:100%; float:left">Non-Stop</span> -->
                                        <span style="width:100%; float:left"><?php echo $segment_details_v['total_duration'];?></span>
                                        </td>
										</tr>
									 <?php }
                              }
                              }?>		
									</table>
									</td>
							</tr>
							   <tr>
                            <td>&nbsp;</td></tr>
							<tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 11pt; font-weight: bold;">Travelers Information</td>
								
							</tr>
							<tr>
								<td style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 11pt;">
									    <tr>
											<td><strong>Passenger Name</strong></td>
											<td><strong>Ticket No</strong></td>
											<td><strong>Ticket ID</strong></td>
											<td><strong>Status</strong></td>
										</tr>
										 <?php 
									
									    $booking_transaction_details_value = $booking_transaction_details[0];
									
									 	if(isset($booking_transaction_details_value['booking_customer_details'])){
									 		foreach($booking_transaction_details_value['booking_customer_details'] as $cus_k => $cus_v){
									 	
									 ?>
									 <tr>
										<?php if (strtolower($cus_v['passenger_type']) == 'infant') { ?>
		                                   <td><?php echo $cus_v['first_name'].'  '.$cus_v['last_name'];?>(Infant)</td>
		                                 <?php }else{?>
		                                 <td><?php echo $cus_v['title'].'.'.$cus_v['first_name'].'  '.$cus_v['last_name'];?></td>
		                                 <?php } ?>
										 <td><?=@$cus_v['TicketNumber'];?></td>
										<td><?=@$cus_v['TicketId']?></td>
										<td>
										<strong class="<?php echo booking_status_label($cus_v['status'])?>">
										<?php 
										switch($cus_v['status']){
												case 'BOOKING_CONFIRMED': echo 'CONFIRMED';break;
												case 'BOOKING_CANCELLED': echo 'CANCELLED';break;
												case 'BOOKING_FAILED': echo 'FAILED';break;
												case 'BOOKING_INPROGRESS': echo 'INPROGRESS';break;
												case 'BOOKING_INCOMPLETE': echo 'INCOMPLETE';break;
												case 'BOOKING_HOLD': echo 'HOLD';break;
												case 'BOOKING_PENDING': echo 'PENDING';break;
												case 'BOOKING_ERROR': echo 'ERROR';break;
												
											}
											?>
										</strong>
										</td>
										</tr>
										<?php } }?>
									</table>
						       </td>
					 </tr>
					  <tr><td>&nbsp;</td></tr>
						<tr><td>&nbsp;</td></tr>
							<tr>
								<td width="100%" style="padding: 10px;border: 1px solid #cccccc; font-size: 11pt; font-weight: bold;">Terms and Conditions</td>
							</tr>
					  <tr>
								<td  width="100%" style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px 20px;font-size: 11pt;">
										<tr>
                                                    <td>1. YOUR BOOKING IS CONFIRMED AND YOU ARE NOT REQUIRED TO CONTACT THE AIRLINE TO RECONFIRM THE SAME.</td>
                                                </tr>
                                                <tr>
                                                    <td>2. DEAR CUSTOMER PLEASE CHECK***** VISA REQUIREMENTS*****.</td>
                                                </tr>
                                                <tr>
                                                    <td>3. THE TICKET REFUND WILL BE APPLICABLE ON AIRLINE POLICY & CHARGES.</td>
                                                </tr>
                                                <tr>
                                                    <td>4. USD 7$ ONLY AGENCY FEE ** + AIRLINE PENALTIES APPLY.</td>
                                                </tr>
                                                <tr>
                                                    <td>5. CONTACT RESPECTIVE AIRLINE PRIOR TO DEPARTURE REGARDING THE SPECIFIC LUGGAGE ALLOWANCES.</td>
                                                </tr>
                                                <tr>
                                                    <td>6. CANCELLATION AFTER PURCHASE MUST BE 48HRS PRIOR TO DEPARTURE TO AVOID NO SHOW.</td>
                                                </tr>
                                                <tr>
                                                    <td>7. PLEASE BE ADVISED NO SHOW TICKETS WILL HAVE ZERO REFUND.</td>
                                                </tr>
                                                <tr>
                                                    <td>8. FOR ALL DOMESTIC FLIGHTS WITHIN THE USA PLEASE CHECKIN 2 HOURS PRIOR WHILE FOR ALL INTERNATIONAL FLIGHTS 3 HOURS PRIOR.</td>
                                                </tr>
                                                <tr>
                                                    <td>9. PLEASE OBTAIN PROPER VISAS TO TRANSIT AND DESTINATION COUNTRIES.</td>
                                                </tr>
                                                <tr>
                                                    <td>10. PLEASE VERIFY CHANGE AND CANCELLATION PENALTIES.</td>
                                                </tr>
                                                <tr>
                                                    <td>11. CANCELLATION OF FLIGHTS MUST BE MADE ATLEAST 24 HOURS PRIOR TO DEPARTURE TO AVOID ANY NO SHOW FEES BY THE AIRLINES.</td>
                                                </tr>
                                                <tr>
                                                    <td>12. ANY CANCELLATION WITHIN 24 HOURS IS SUBJECT TO NO SHOW FEES.</td>
                                                </tr>
                                                <tr>  
                                                    <td>
                                                        <p align='center'><strong>Thank you for booking with us.<br>
We wish you a pleasent journey and hopes to serve you again in the future.</strong></p>
                                                    </td>
                                                </tr> 
									</table>
								</td>
							</tr>								
			  </table>				
			 </td>
			</tr> 
	      </table>
	 </td>
	</tr>  
</tbody>
</table>


<?php if(count($booking_transaction_details) == 2) {?>

<table style="border-collapse: collapse; background: #ffffff;font-size: 12pt; margin: 0 auto; font-family: arial;" width="100%" cellpadding="0" cellspacing="0" border="0">
<tbody>
	<tr> 
	  <td style="border-collapse: collapse; padding:10px 20px 20px" >
	      <table width="100%" style="border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0">
	      	<tr><td style="font-size:15pt; line-height:30px; width:100%; display:block; font-weight:600; text-align:center">E-Ticket<?php echo $Return;?></td></tr>
	       <tr>
					<td>
			       <table width="100%" style="border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0">
			       <tr>
					<td style="padding: 10px; width:65%"><</td>
					<td style="padding: 10px; width:35%">
                    	<table width="100%" style="border-collapse: collapse;text-align: right; line-height:15px;" cellpadding="0" cellspacing="0" border="0">
                    		
                    		<tr><td style="font-size:12pt;"><span style="width:100%; float:left"><?php echo $data['address'];?></span>
                    		</td></tr>
                         </table></td>
				</tr>
				</table>
				</td>
			</tr>
			<tr>
			 <td style="padding: 10px;">
			  <table cellpadding="5" cellspacing="0" border="0" width="100%" style="border-collapse: collapse;">
							<tr>
								<td width="100%" style="padding: 10px;border: 1px solid #cccccc; font-size: 11pt; font-weight: bold;">Reservation Lookup</td>
								
							</tr>
							<tr>
								<td style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 11pt;">
										<tr>
											<td><strong>Booking Reference</strong></td>
											<td><strong>Booking ID</strong></td>
											<td><strong>PNR</strong></td>
											<td><strong>Booking Date</strong></td>
											<td><strong>Status</strong></td>
										</tr>
										<tr>
										 
											<td><?=@$booking_details['app_reference']?></td>
											<td><?=@$booking_transaction_details[1]['book_id']?></td>
											<td><?=@$booking_transaction_details[1]['pnr']?></td>
											<td><?=app_friendly_absolute_date(@$booking_details['booked_date'])?></td>
											<td><strong class="<?php echo booking_status_label( $booking_details['status']);?>" style=" font-size:14px;color:#28b700">
											<?php 
											switch($booking_details['status']){
												case 'BOOKING_CONFIRMED': echo 'CONFIRMED';break;
												case 'BOOKING_CANCELLED': echo 'CANCELLED';break;
												case 'BOOKING_FAILED': echo 'FAILED';break;
												case 'BOOKING_INPROGRESS': echo 'INPROGRESS';break;
												case 'BOOKING_INCOMPLETE': echo 'INCOMPLETE';break;
												case 'BOOKING_HOLD': echo 'HOLD';break;
												case 'BOOKING_PENDING': echo 'PENDING';break;
												case 'BOOKING_ERROR': echo 'ERROR';break;
												
											}
											
																				
											?>
											</strong></td>
										</tr>
									</table>
								</td>
							</tr>
							 <tr><td>&nbsp;</td></tr>
							<tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 11pt; font-weight: bold;">Journey Information</td>
								
							</tr>
							<tr>
								<td  width="100%" style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 11pt;">
										<tr>
											<td><strong>Flight</strong></td>
											<td><strong>Departure</strong></td>
											<td><strong>Arrival</strong></td>
											<td><strong>Journey Time</strong></td>
										</tr>
										<?php 
						if(isset($booking_transaction_details) && $booking_transaction_details != ""){
                          
							if($booking_details['is_domestic'] == true && count($booking_transaction_details) == 2){
								$itinerary_details = array();
								$itinerary_details = $onward_segment_details;
							}
								foreach($return_segment_details as $segment_details_k => $segment_details_v){
								
							
							  $itinerary_details_attributes = json_decode($segment_details_v['attributes']);
                              $airline_craft = $itinerary_details_attributes->craft;
                              $airline_terminal_origin = $itinerary_details_attributes->ws_val->OriginDetails->Terminal;
                              $airline_terminal_destination = $itinerary_details_attributes->ws_val->DestinationDetails->Terminal;
                              $origin_terminal='';
                              $destination_terminal='';
                              if($airline_terminal_origin != ''){
                              	$origin_terminal = 'Terminal '.$airline_terminal_origin;
                              }
                              if($airline_terminal_destination != ''){
                              	$destination_terminal = 'Terminal '.$airline_terminal_destination;
                              }
                              
                              if(valid_array($segment_details_v) == true) { ?>
                                     <tr>
                               
									 <td><img style="max-height:30px" src="<?=ASSETS.'airline_logo/'.$segment_details_v['airline_code'].'.gif'?>" alt="flight-logo" />
                                        <span style="width:100%; float:left"><?=@$segment_details_v['airline_name']?></span>
                                        <span style="width:100%; float:left; font-size:13px; font-weight:bold"><?php echo $airline_craft;?></span></td>
                                        <td style="line-height:16px">
                                        <span style="width:100%; float:left; font-size:13px; font-weight:bold"> <?=@$segment_details_v['from_airport_name'] ?>(<?=@$segment_details_v['from_airport_code']?>)</span>
                                        <span style="width:100%; float:left"><?php echo $origin_terminal;?></span>
                                        <span style="width:100%; float:left; font-weight:bold"> <?php echo date("d M Y",strtotime($segment_details_v['departure_datetime'])).", ".date("H:i",strtotime($segment_details_v['departure_datetime']));?></span></td>
                                        <td style="line-height:16px">
                                        <span style="width:100%; float:left; font-size:13px; font-weight:bold"> <?=@$segment_details_v['to_airport_name']?>(<?=@$segment_details_v['to_airport_code']?>)</span>
                                        <span style="width:100%; float:left"> <?php echo $destination_terminal;?></span>
                                        <span style="width:100%; float:left; font-weight:bold">  <?php echo date("d M Y",strtotime($segment_details_v['arrival_datetime'])).", ".date("H:i",strtotime($segment_details_v['arrival_datetime']));?></span></td>
                                        <td><!-- <span style="width:100%; float:left">Non-Stop</span> -->
                                        <span style="width:100%; float:left"><?php echo $segment_details_v['total_duration'];?></span>
                                        </td>
										</tr>
									 <?php }
                              }
                              }?>		
									</table>
									</td>
							</tr>
							   <tr>
                            <td>&nbsp;</td></tr>
							<tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 11pt; font-weight: bold;">Travelers Information</td>
								
							</tr>
							<tr>
								<td style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 11pt;">
									    <tr>
											<td><strong>Passenger Name</strong></td>
											<td><strong>Ticket No</strong></td>
											<td><strong>Ticket ID</strong></td>
											<td><strong>Status</strong></td>
										</tr>
										 <?php 
									
									    $booking_transaction_details_value = $booking_transaction_details[1];
									
									 	if(isset($booking_transaction_details_value['booking_customer_details'])){
									 		foreach($booking_transaction_details_value['booking_customer_details'] as $cus_k => $cus_v){
									 	
									 ?>
									 <tr>
										<?php if (strtolower($cus_v['passenger_type']) == 'infant') { ?>
		                                   <td><?php echo $cus_v['first_name'].'  '.$cus_v['last_name'];?>(Infant)</td>
		                                 <?php }else{?>
		                                 <td><?php echo $cus_v['title'].'.'.$cus_v['first_name'].'  '.$cus_v['last_name'];?></td>
		                                 <?php } ?>
										 <td><?=@$cus_v['TicketNumber'];?></td>
										<td><?=@$cus_v['TicketId']?></td>
										<td>
										<strong class="<?php echo booking_status_label($cus_v['status'])?>">
										<?php 
										switch($cus_v['status']){
												case 'BOOKING_CONFIRMED': echo 'CONFIRMED';break;
												case 'BOOKING_CANCELLED': echo 'CANCELLED';break;
												case 'BOOKING_FAILED': echo 'FAILED';break;
												case 'BOOKING_INPROGRESS': echo 'INPROGRESS';break;
												case 'BOOKING_INCOMPLETE': echo 'INCOMPLETE';break;
												case 'BOOKING_HOLD': echo 'HOLD';break;
												case 'BOOKING_PENDING': echo 'PENDING';break;
												case 'BOOKING_ERROR': echo 'ERROR';break;
												
											}
											?>
										</strong>
										</td>
										</tr>
										<?php } }?>
									</table>
						       </td>
					 </tr>
					  <tr><td>&nbsp;</td></tr>
					  <tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 11pt; font-weight: bold;">Price Summary</td>
								
							</tr>
						<tr>
								<td style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 11pt;">
									<tr>
											<td><strong>Base Fare</strong></td>
											<!-- <td><strong>Baggage Fare</strong></td>
											<td><strong>Meals Fare</strong></td>
											<td><strong>Service Fee</strong></td> 
											<td><strong>Discount</strong></td>-->
											<td><strong>Total Fare</strong></td>
										</tr>
									 <tr>
											<td><?=@$booking_details['currency']?>  <?=@$booking_details['grand_total']?></td>
											 <!--<td>0</td>
											<td>0</td>
											<td>375</td>
											<td>0</td>-->
											<td><?=@$booking_details['currency']?>  <?=@$booking_details['grand_total']?></td>
										</tr>
											
									</table>
								 </td>
							</tr>
						<tr><td>&nbsp;</td></tr>
							<tr>
								<td width="100%" style="padding: 10px;border: 1px solid #cccccc; font-size: 11pt; font-weight: bold;">Terms and Conditions</td>
							</tr>
				<tr>
                                        <td width="100%" style="border: 0px solid #2B2E34;">
                                            <table width="100%" cellpadding="5" style="padding: 10px 20px; font-size: 16px;">
                                                <tr>
                                                    <td>1. YOUR BOOKING IS CONFIRMED AND YOU ARE NOT REQUIRED TO CONTACT THE AIRLINE TO RECONFIRM THE SAME.</td>
                                                </tr>
                                                <tr>
                                                    <td>2. DEAR CUSTOMER PLEASE CHECK***** VISA REQUIREMENTS*****.</td>
                                                </tr>
                                                <tr>
                                                    <td>3. THE TICKET REFUND WILL BE APPLICABLE ON AIRLINE POLICY & CHARGES.</td>
                                                </tr>
                                                <tr>
                                                    <td>4. USD 7$ ONLY AGENCY FEE ** + AIRLINE PENALTIES APPLY.</td>
                                                </tr>
                                                <tr>
                                                    <td>5. CONTACT RESPECTIVE AIRLINE PRIOR TO DEPARTURE REGARDING THE SPECIFIC LUGGAGE ALLOWANCES.</td>
                                                </tr>
                                                <tr>
                                                    <td>6. CANCELLATION AFTER PURCHASE MUST BE 48HRS PRIOR TO DEPARTURE TO AVOID NO SHOW.</td>
                                                </tr>
                                                <tr>
                                                    <td>7. PLEASE BE ADVISED NO SHOW TICKETS WILL HAVE ZERO REFUND.</td>
                                                </tr>
                                                <tr>
                                                    <td>8. FOR ALL DOMESTIC FLIGHTS WITHIN THE USA PLEASE CHECKIN 2 HOURS PRIOR WHILE FOR ALL INTERNATIONAL FLIGHTS 3 HOURS PRIOR.</td>
                                                </tr>
                                                <tr>
                                                    <td>9. PLEASE OBTAIN PROPER VISAS TO TRANSIT AND DESTINATION COUNTRIES.</td>
                                                </tr>
                                                <tr>
                                                    <td>10. PLEASE VERIFY CHANGE AND CANCELLATION PENALTIES.</td>
                                                </tr>
                                                <tr>
                                                    <td>11. CANCELLATION OF FLIGHTS MUST BE MADE ATLEAST 24 HOURS PRIOR TO DEPARTURE TO AVOID ANY NO SHOW FEES BY THE AIRLINES.</td>
                                                </tr>
                                                <tr>
                                                    <td>12. ANY CANCELLATION WITHIN 24 HOURS IS SUBJECT TO NO SHOW FEES.</td>
                                                </tr>
                                                <tr>  
                                                    <td>
                                                        <p align='center'><strong>Thank you for booking with us.<br>
We wish you a pleasent journey and hopes to serve you again in the future.</strong></p>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>          								
			  </table>				
			 </td>
			</tr> 
	      </table>
	 </td>
	</tr>  
</tbody>
</table>
<?php } ?>	

		                    