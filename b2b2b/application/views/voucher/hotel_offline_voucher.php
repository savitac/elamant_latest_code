<style>
th,td{padding:5px;}
</style>
<?php
$booking_details = $data['booking_details'][0];
/*print_r($booking_details);
exit;*/
$itinerary_details = $booking_details['itinerary_details'][0];
$attributes = $booking_details['attributes'];
$customer_details = $booking_details['customer_details'][0];
$domain_details = $booking_details;
$lead_pax_details = $booking_details['customer_details'];
//debug($temp_booking);die(" khjv");  
?> 
<div class="table-responsive">
<table style="border-collapse: collapse; background: #ffffff;font-size: 14px; margin: 0 auto; font-family: arial;" width="100%" cellpadding="0" cellspacing="0" border="0">
<tbody>
	<tr>
		<td style="border-collapse: collapse; padding:10px 20px 20px" >
			<table width="100%" style="border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0">
				
				<tr>
					<td style="padding: 10px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse: collapse;">
						<tr><td style="font-size:22px; line-height:30px; width:100%; display:block; font-weight:600; text-align:center">E-Ticket</td></tr>
				<tr>
					<td>
			<table width="100%" style="border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0"><tr>
					<!-- <td style="padding: 10px;width:65%;"><img style="max-height:56px;" src=""></td> -->
					 <?php if($this->session->userdata('site_name') == ''){ ?>
                                            <img style="max-height: 60px;float: left;" src="<?php echo base_url().'/assets/images/logo.png'; ?>" alt="flight-logo" />
                                        <?php } else { ?>
                                            <img style="max-height: 60px;float: left;" src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $this->session->userdata('domain_logo'); ?>" alt="flight-logo" />
                                        <?php } ?>
					<td style="padding: 10px;width:35%">
                    	<table width="100%" style="border-collapse: collapse;text-align: right; line-height:15px;" cellpadding="0" cellspacing="0" border="0">
                    		
                    		<tr>
                    		<td style="font-size:14px;"><span style="width:100%; float:left"><?php echo $data['address'];?></span>
                    		</td>
                    		</tr>
                         </table></td>
				</tr></table></td></tr>
				<tr>
						<td  width="100%" style="border: 1px solid #cccccc;background-color: #3d9970;">
						<table width="100%" cellpadding="5" style="padding: 0px 20px;font-size: 13px;">
						<tr>
						<td style="
						color: #fff;
						text-align: center;
						">1.Please Contact Admin to confirm your hotel booking at Email :- <?php echo $admin_details->domain_user_name;  ?> or Phone no :-  <?php echo ($admin_details->user_cell_phone);  ?>.</td>
						</tr>
						</table>
						</td>
							</tr>
							<tr>
								<td width="50%" style="padding: 10px;border: 1px solid #cccccc; font-size: 14px; font-weight: bold;">Hotel Booking Lookup</td>
							</tr>
							<tr>
								<td style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 13px;">
										<tr>
											<td><strong>Booking Reference</strong></td>
											<td><strong>Booking Date</strong></td>
											<td><strong>Status</strong></td>
										</tr>
										<tr>
											<td><?php echo $temp_booking['book_id']; ?></td>  
											<td><?php echo date("d M Y",strtotime($booking_details['created_datetime'])); ?></td>
											
                                           <td>
                                           <strong class="<?php echo booking_status_label( $booking_details['status']);?>" style="font-size:14px;background-color: #ea2f24;padding: 5px;color: #fff;">
											PENDING 
											</strong></td>
                                           
										</tr>
									</table>
								</td>
							</tr>
                            <tr><td>&nbsp;</td></tr>
							<tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 14px; font-weight: bold;">Hotel Information</td>
								
							</tr>
							<tr>
								<td  width="100%" style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 13px;">
										<tr>
											<td width="10%"><strong>Hotel Name</strong></td>
											<td width="19%"><strong>Hotel Address</strong></td>
											<!-- <td><strong>Phone</strong></td> -->
											<td><strong>Check-In</strong></td>
											<td><strong>Check-Out</strong></td>
											<td><strong>No of Room's</strong></td>
											<td><strong>Room Type</strong></td>
											<td><strong>No Of PAX</strong></td>
										</tr>
										<tr>
                                        <td>
                                        <?php echo $temp_booking['book_attributes']['token']['HotelName'];?>
                                        </td>
                                        <td style="line-height:16px">
                                        <?php echo $temp_booking['book_attributes']['token']['HotelAddress'];  ?> 
                                        </td>
                                       
                                        <td><span style="width:100%; float:left"> <?=@date("d M Y",strtotime($itinerary_details['check_in']))?></span></td>
                                        <td><span style="width:100%; float:left"> 	<?=@date("d M Y",strtotime($itinerary_details['check_out']))?></span></td>
                                        <td align="center"><?php echo $temp_booking['book_attributes']['token']['RoomTypeName'] ?></span></td> 
                                        <td> - </td>
                                        <td align="center"><?php echo count($temp_booking['book_attributes']['passenger_type']); ?></td>
                                        
										</tr>
									</table> 
								</td>
							</tr>
                            <tr>
                            <td>&nbsp;</td></tr>
							<tr>
								<td style="padding: 10px;border: 1px solid #cccccc; font-size: 13px; font-weight: bold;">Contact Information</td>
								
							</tr> 
							<tr>
								<td style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px;font-size: 13px;">
										<tr>
											
											<td><strong>Mobile</strong></td>
											<td><strong>Email</strong></td>
										</tr>
										<tr>
										 
                                            <td><?php echo $temp_booking['book_attributes']['passenger_contact'];?></td>
                                            <td><?php echo $temp_booking['book_attributes']['billing_email'];?></td>
                                           
                                             
                                         </tr>   
									</table>
								</td>
								<td></td>
							</tr>
                            <tr><td>&nbsp;</td></tr>
						                           
                            <tr><td>&nbsp;</td></tr>
							<tr>
								<td width="50%" style="padding: 10px;border: 1px solid #cccccc; font-size: 13px; font-weight: bold;">Terms and Conditions</td>
							</tr>
							<tr>
								<td  width="100%" style="border: 1px solid #cccccc;">
									<table width="100%" cellpadding="5" style="padding: 10px 20px;font-size: 13px;">
										<tr>
                                        <td>1.Please Contact Admin to confirm your hotel booking at Email :- <?php echo $admin_details->domain_user_name;  ?> or Phone no :-  <?php echo ($admin_details->user_cell_phone);  ?>.</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table> 
		</td>
	</tr></tbody>
</table>
<table id="printOption" style="border-collapse: collapse;font-size: 14px; margin: 10px auto; font-family: arial;" width="70%" cellpadding="0" cellspacing="0" border="0">
<tbody>
	<tr>
    <td align="center" onclick="document.getElementById('printOption').style.visibility = ''; print(); return true;"><input style="background:#418bca; height:34px; padding:10px; border-radius:4px; border:none; color:#fff; margin:0 2px;" type="button" value="Print" />
    	<td align="center">
    		<a style="background:#418bca; height:34px; padding:10px; border-radius:4px; border:none; color:#fff; margin:0 2px;text-decoration:none;" href="<?php echo base_url().'dashboard'; ?>" target="_blank" >
    		Home</a>
    	</td>
    
    </tr>
</tbody></table> 
</div>