<link href="<?php echo ASSETS;?>assets/css/flight-ram.css" rel="stylesheet" />
<section id="main-content">
     <section class="wrapper">
     <div class="container">
		<div class="<?php if($this->session->userdata('user_type') != 5){ ?> col-lg-12 <?php } else { ?> col-lg-12 <?php } ?> main-chart">
		<!--search_tab-->
		 <div class="totopp">
			  <div class="col-lg-12 nopad">
				<div class="tabbable customtab">
				  
				  	<ul class="nav nav-tabs nav-tabs-responsive hide_3">
				  	
					  <?php  for($prod = 0; $prod < count($products); $prod++) { 
					  	//debug($product_name);?>
					   
					      <li <?php if($prod == 0) { ?> class="active" <?php }elseif($prod == 1){ ?> class="next" <?php } ?> > 
					      	<a 
					      		data-toggle="tab" 
					      		href="#<?php echo str_replace(' ', '_',$products[$prod]->product_name); ?>"> 
					      			<span class="text">
				      					<span class="<?php echo $products[$prod]->product_icon; ?>"></span>
					      	<?php 
					      	//str_replace(' ', '_', $products[$prod]->product_name)
					           $product_name = $products[$prod]->product_name; 
					         ?>
					      	<?=($product_name !='Transfer')?$product_name:'Car Rentals'; ?>
					      </span> </a> </li>
					    
					    <?php  } ?> 
				  	</ul>
							  
					<div class="tab-content">	
					<div class="tab-pane" id="flight">
						<h3>Book Domestic &amp; International Flight Tickets</h3>
						<form action="" method="POST" role="form">
							<div class="col-md-12 nopad marginbotom10">
								<div class="col-md-6 nopad fiveh">
									<span class="formlabel">From</span>
									<div class="relativemask"> <span class="maskimg ffrom"></span> 
										<input name="city" id="flight" type="text" placeholder="Type departure city" class="ft ui-autocomplete-input" required="" autocomplete="off">
									</div>
								</div>
								<div class="col-md-6 nopad fiveh">
									<span class="formlabel">To</span>
									<div class="relativemask"> <span class="maskimg fto"></span> 
										<input name="city" id="flight" type="text" placeholder="Type departure city" class="ft ui-autocomplete-input" required="" autocomplete="off">
									</div>
								</div>
							</div>
							<div class="col-md-12 nopad marginbotom10">
								<div class="col-md-6 nopad fiveh">
									<div class="col-md-6 nopad fiveh">
										<span class="formlabel">Departure</span>
										<div class="relativemask"> <span class="maskimg caln"></span> 
											<input name="city" id="flight" type="text" placeholder="Select Date" class="ft ui-autocomplete-input" required="" autocomplete="off">
										</div>
									</div>
									<div class="col-md-6 nopad fiveh">
										<span class="formlabel">Return</span>
										<div class="relativemask"> <span class="maskimg caln"></span> 
											<input name="city" id="flight" type="text" placeholder="Select Date" class="ft ui-autocomplete-input" required="" autocomplete="off">
										</div>
									</div>
								</div>
							
							
								<div class="col-md-6 nopad fiveh">
									<div class="col-md-4 nopad fiveh">
										<span class="formlabel">Adult (12+ Yrs)</span>
										<div class="selectedwrap">
											<select class="mySelectBoxClass flyinputsnor">
												<option>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</div>
									</div>
									<div class="col-md-4 nopad fiveh">
										<span class="formlabel">Adult (12+ Yrs)</span>
										<div class="selectedwrap">
											<select class="mySelectBoxClass flyinputsnor">
												<option>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</div>
									</div>
									<div class="col-md-4 nopad fiveh">
										<span class="formlabel">Adult (12+ Yrs)</span>
										<div class="selectedwrap">
											<select class="mySelectBoxClass flyinputsnor">
												<option>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 nopad marginbotom10">
								<div class="col-md-6 nopad fiveh">
									<div class="col-md-6 nopad fiveh">
										<span class="formlabel">Class</span>
										<div class="selectedwrap">
											<select class="mySelectBoxClass flyinputsnor">
												<option>Select class</option>
												<option>2</option>
											</select>
										</div>
									</div>
									<div class="col-md-6 nopad fiveh">
										<span class="formlabel">Prefered airline</span>
										<div class="selectedwrap">
											<select class="mySelectBoxClass flyinputsnor">
												<option>Select class</option>
												<option>2</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6 nopad fiveh">
									<span class="formlabel">&nbsp;</span>
									<button class="srchbutn comncolor">Search Flight <span class="srcharow"></span></button>		
								</div>
							</div>
						</form>
					</div>	
					<?php 
					//error_reporting(E_ALL);
					//debug($products);die;
					//for($i =0; $i<count($products); $i++){ 
					foreach($products as $key => $listOne) {
						// debug($listOne->product_view); ?>
						<div class="tab-pane <?php if($key==0){ ?> active <?php } ?>" id="<?php echo str_replace(' ', '_', $listOne->product_name); ?>"><?php $this->load->view('dashboard/'.$listOne->product_view); ?></div>
				    <?php } ?>	
				    </div>
				   
				
					  </div>
					  
				<div class="clearfix"></div>
				<div class="booking_n_cancl_sec">
   <div class="col-xs-12 nopad">
      <div class="service_sec_agent clearfix">
         <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="serv_ico"><span><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span></div>
            <h3>Best Price Guarantee</h3>
            <p>Find our lowest price to target global, guaranteed</p>
         </div>
         <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="serv_ico"><span><i class="fa fa-calendar-plus-o" aria-hidden="true"></i></span></div>
            <h3>Easy Booking</h3>
            <p>Search, select and save - the quickest way to reserve your Journey.</p>
         </div>
         <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="serv_ico"><span><i class="fa fa-clock-o" aria-hidden="true"></i></span></div>
            <h3>24/7 Customer Care</h3>
            <p>Get excellent service and first deals by calling +1 571 789 99 30</p>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="col-xs-12 nopad">
      <div class="col-md-12 nopad">
         <div class="row orgn_row">
            <div class="col-md-6 col-sm-6 col-xs-12">
               <a href="" class="">
                  <span class="info-box">
                     <span class="info-box-icon bg-blue"> <i class="fa fa-plane"></i> </span> 
                     <div class="info-box-content">
                        <span class="info-box-text">
                           <span class="badge"><?= count($flight_booking) ?></span> Flight Booking <!-- <span class="bkng_cnt">(0%)</span> -->
                        </span>
                     </div>
                     <!-- /.info-box-content --> 
                  </span>
               </a>
               <!-- /.info-box --> 
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
               <a href="" class="">
                  <span class="info-box">
                     <span class="info-box-icon bg-green"> <i class="fa fa-bed"></i> </span> 
                     <div class="info-box-content">
                        <span class="info-box-text">
                           <span class="badge"><?= count($hotel_booking); ?></span> Hotel Booking <!-- <span class="bkng_cnt">(0%)</span> -->
                        </span>
                     </div>
                     <!-- /.info-box-content --> 
                  </span>
               </a>
               <!-- /.info-box --> 
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
               <a href="" class="">
                  <span class="info-box">
                     <span class="info-box-icon bg-blue"> <i class="fa fa-plane"></i> </span> 
                     <div class="info-box-content">
                        <span class="info-box-text">
                           <span class="badge"><?= $flight_booking_confirm ?></span> Flight Confirmed <!-- <span class="bkng_cnt">(0%)</span> -->
                        </span>
                     </div>
                     <!-- /.info-box-content --> 
                  </span>
               </a>
               <!-- /.info-box --> 
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
               <a href="" class="">
                  <span class="info-box">
                     <span class="info-box-icon bg-green"> <i class="fa fa-bed"></i> </span> 
                     <div class="info-box-content">
                        <span class="info-box-text">
                           <span class="badge"><?= $hotel_booking_confirm ?></span> Hotel Confirmed <!-- <span class="bkng_cnt">(0%)</span> -->
                        </span>
                     </div>
                     <!-- /.info-box-content --> 
                  </span>
               </a>
               <!-- /.info-box --> 
            </div>
         </div>
      </div>
      <div class="col-md-12 nopad mTop20">
         <div class="col-md-6 padL0">
            <div class="widget green">
               <div class="widget-title">
                  <h4><i class="icon-bell"></i> Recent Flight Bookings </h4>
               </div>
               <div class="widget-body" style="height: 350px; display: block;">
                  <div class="slimScrollDiv" style="position: relative; overflow: auto; width: auto; height: 320px;">
                     <ul class="item-list scroller padding" style="overflow: hidden; width: auto; height: 1000px;" data-always-visible="1"> 
                        <?php foreach ($flight_booking as $key => $value) { 
                           $date=date("Y-m-d",strtotime($value['created_datetime']));  
                           ?>
                           <li>
                           <span class="label label-success"><i class="fa fa-plane"></i></span> <span> <span style="color:GREEN"><?php echo $value['app_reference'] ?></span> <strong>-Flight</strong> <?php echo $value['status'] ?> </span>  
                           <div class="pull-right"> <span class="small italic "><?= $date?></span> </div>
                        </li>
                       <?php } ?>
                       
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-6 padR0">
            <div class="widget green">
               <div class="widget-title">
                  <h4><i class="icon-bell"></i> Recent Hotel Bookings </h4>
               </div>
               <div class="widget-body" style="height: 350px; display: block;">
                  <div class="slimScrollDiv" style="position: relative; overflow: auto; width: auto; height: 320px;">
                     <ul class="item-list scroller padding" style="overflow: hidden; width: auto; height: 1000px;" data-always-visible="1">
                        <?php foreach ($hotel_booking as $key => $value) { 
                           $date=date("Y-m-d",strtotime($value['created_datetime']));  
                           ?> 
                           <li>
                           <span class="label label-success"><i class="fa fa-plane"></i></span> <span> <span style="color:GREEN"><?php echo $value['app_reference'] ?></span> <strong>-Hotel </strong> <?php echo $value['status'] ?> </span>  
                           <div class="pull-right"> <span class="small italic "><?= $date?></span> </div>
                        </li>
                       <?php } ?>
                       
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div></div>
</div>
</div>
</div>
</section>
</section>

<?php echo  $this->load->view('core/bottom_footer'); ?>
<div class="clearfix"></div>
		<?php  if($this->session->userdata('user_type') != 5){ ?>
		
		 <?php } ?>
		
	<!--search_tab end-->

