<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>Chinagap</title>
<?php echo $this->load->view('core/load_css'); ?>

 
</head>

<body>
<?php
      if(isset($_SESSION['TravelLights']['language'])){ 
        $display_language = $_SESSION['TravelLights']['language'];
      } else {
        $display_language = $_SESSION['TravelLights']['language'] = BASE_LANGUAGE;
      } 
?>

<?php echo $this->load->view('dashboard/top'); ?> 

<div class="clearfix"></div>
<?php //echo '<pre>'; print_r($aboutus); ?>
<div class="agent_login_wrap top80 dashtopmargin">
		<div class="aboutback"><img alt="" src="<?php echo  site_url().'cpanel/uploads/header/' ?><?php echo $aboutus[0]->service_images; ?>"> <div class="container"> <h1><?php echo $aboutus[0]->menu_name; ?></h1></div> </div>
   		<div class="container">
  		 <div class="static_page_in">
          	<br>
              <p>
                 <?php if($display_language=='english') $about_description = $aboutus[0]->about_description; 
                  else $about_description = $aboutus[0]->about_description_china; ?>
                <?php echo $about_description;  ?>
              </p>
          </div>
          
          <div class="partner_page">
          	<h1><?php echo $aboutus[0]->sub_aboutus; ?></h1>
            <?php $loop = $this->Dashboard_Model->getHeaderManageAboutus($aboutus[0]->headerabout_id);
             for($loops =0; $loops < count($loop); $loops++) {?>
			<div class="repeatchoose">
            <div class="left_wy">
            	<img src="<?php echo  site_url().'cpanel/uploads/header/' ?><?php echo $loop[$loops]->service_image; ?>" alt="" />
            </div>
            <div class="rgt_wy">
            	<h3>
                 <?php if($display_language=='english') $aboutmanage_name = $loop[$loops]->aboutmanage_name; 
                  else $aboutmanage_name = $loop[$loops]->aboutmanage_name_china; ?>
                <?php echo $aboutmanage_name; ?>
              </h3>
              <?php if($display_language=='english') $aboutmanage_description = $loop[$loops]->aboutmanage_description; 
                  else $aboutmanage_description = $loop[$loops]->aboutmanage_description_china; ?>
                <p><?php echo $aboutmanage_description; ?></p>
            </div>
            </div>
            <?php } ?>
            <div class="clearfix"></div>
            
          </div>
          
        </div>
   
</div>


<div class="clearfix"></div>
<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script> 

<script type="text/javascript">
$(document).ready(function(){
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	 
	 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	 
	 
	 
	
});
</script> 





</body>
</html>
