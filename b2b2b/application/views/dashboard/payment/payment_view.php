<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>China Gap</title>
<?php echo $this->load->view('core/load_css'); ?>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>

<link href="<?php echo ASSETS;?>assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS; ?>assets/css/pre_booking.css" rel="stylesheet">
</head>
<body>
<!-- Navigation -->

<?php echo $this->load->view('dashboard/top'); ?> 
<?php echo $this->load->view('dashboard/left_menu'); ?>




<!-- /Navigation -->





<section id="main-content">
  
  <section class="wrapper">
	<div class="main-chart">
		
        <div id="BookingList">
        <span class="profile_head">Make Payment</span>

<div class="rowit">
	 


<div class="top_booking_info">




<?php 
	//~ echo "data: <pre>";print_r($booking_details);exit;
	$total_tax = $booking_details[0]->service_charge + $booking_details[0]->gst_charge + $booking_details[0]->tax_charge;
	$discount = $booking_details[0]->discount;
	if($booking_details[0]->product_id == 1):
		if($this->session->userdata('user_type') == 2):
			$net_Total[] = ($booking_details[0]->admin_baseprice + $total_tax) - $discount;
			$commissionable_Total[] = ($booking_details[0]->agent_base_amount + $total_tax) - $discount;
		elseif($this->session->userdata('user_type') == 4):
			$net_Total[] = ($booking_details[0]->sub_agent_baseprice + $total_tax) - $discount;
			$commissionable_Total[] = ($booking_details[0]->total_cost + $total_tax) - $discount;
		else:
		endif;
	elseif($booking_details[0]->product_id == 19):
		if($this->session->userdata('user_type') == 2):
			$net_Total[] = $booking_details[0]->admin_baseprice;
			$commissionable_Total[] = $booking_details[0]->agent_base_amount;
		elseif($this->session->userdata('user_type') == 4):
			$net_Total[] = $booking_details[0]->sub_agent_baseprice;
			$commissionable_Total[] = $booking_details[0]->total_cost;
		else:
		endif;
	endif;

	$PG_Markup = 0;
?>






					<form name="payment" id="payment" method="post" autocomplete="off" onsubmit="return my_ownvalidation()" action="<?php echo ASSETS;?>payment/do_payment/<?php echo base64_encode(json_encode($booking_details[0]->parent_pnr_no)); ?>/<?php echo $booking_details[0]->product_id; ?>">
						
						<input type="hidden" name="payment_logic" id="payment_logic" />
						<input type="hidden" name="payment_type" id="payment_type" />
						<input type="hidden" name="deposit_payment" id="deposit_payment" />
						
						<div class="paymentpage">
							<div class="col-md-4 col-sm-4 nopad sidebuki">
								<div class="cartbukdis">
									<ul class="liscartbuk">
										<li class="lostcart">
											<div class="cartlistingbuk">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Sub Total</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $booking_details[0]->api_currency; ?> <span class="sub_total"><?php echo number_format($booking_details[0]->total_amount, 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk discount_wrap" style="display:none;">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Discount(-)</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc discount"><?php echo $booking_details[0]->api_currency; ?> <span class="amount promodiscount promo_discount" ></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Taxes</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $booking_details[0]->api_currency; ?> <span class="taxes"><?php echo number_format(array_sum($ovr_all_tax), 2, '.', ' '); ?></span></div>
														</div>
													</div>
												</div>
												
												<div class="cartitembuk cc_fees" style="display:none;">
													<div class="col-md-8 celcart">
														<div class="payblnhm">CC Fees</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt normalprc"><?php echo $booking_details[0]->api_currency; ?> <span class="cc_fees_value">0.00</span></div>
														</div>
													</div>
												</div>

											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk nomarr deposit_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Deposit Payment</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt bigclrfnt finalAmt"><span class="amount"><?php echo $booking_details[0]->api_currency; ?> <span class="total_deposit_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk nomarr cc_payment_wrap" style="display:none;">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Credit Card Payment</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt bigclrfnt finalAmt"><span class="amount"><?php echo $booking_details[0]->api_currency; ?> <span class="total_cc_payment" ></span></span></div>
														</div>
													</div>
												</div>
											</div>
											<div class="clear"></div>
											
											<div class="cartlistingbuk nomarr">
												<div class="cartitembuk">
													<div class="col-md-8 celcart">
														<div class="payblnhm">Total</div>
													</div>
													<div class="col-md-4 celcart">
														<div class="cartprc">
															<div class="ritaln cartcntamnt bigclrfnt finalAmt"><span class="amount"><?php echo $booking_details[0]->api_currency; ?> <span class="total promo_total total_price" ><?php echo number_format(array_sum($Totall),2,'.',' '); ?></span></span></div>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>

							
							<div class="col-md-8 col-sm-8 nopad fulbuki ">
							<div class="col-xs-12 padleftpay">
								<div class="payselect comon_backbg">
									<h3 class="inpagehed">Select a Payment Method</h3>
									<div class="sectionbuk">
										<div class="alldept">
											<div class="oneselects active" id="pay_later"> 
												<input type="checkbox" class="hideinpay">
												<div class="typeselctrs"><span class="fa fa-check"></span> Commissionable</div>
											</div>
											<div class="oneselects" id="pay_now">
												<input type="checkbox" class="hideinpay">
												<div class="typeselctrs"><span class="fa fa-check"></span> Net Rate</div>
											</div>
										</div>
										
										<div class="paylater_show" >
											<ul class="radiochecks">
												<li class="li-option-2" id="commissionable_li_1" >
													<input type="radio" id="f-option-1" value="DEPOSIT" name="type_of_payment" checked >
													<label for="f-option-1" class="check"></label>
													<label for="f-option-1">Deposit</label>
												</li>
												  
												<li class="li-option-2" id="commissionable_li_2" >
													<input type="radio" id="s-option-1" value="PG" name="type_of_payment">
													<label for="s-option-1" class="check"></label>
													<label for="s-option-1">Credit Card</label>
												</li>
												  
												<li class="pay_both_2" id="commissionable_li_3" >
													<input type="radio" id="t-option-1" value="BOTH" name="type_of_payment">
													<label for="t-option-1" class="check"></label>
													<label for="t-option-1">Both</label>
												</li>
											</ul>
											<div class="col-xs-4 nopad" id="deposit-container-2" style="display:none;" >
												<input name="commissionable_both_payment" data-total-price="<?php echo array_sum($commissionable_Total); ?>" id="commissionable_both_payment" onkeyup="price_calculation('COMMISSIONABLE');" placeholder="Deposit Amount" type="text" class="payinput">
											</div>
										</div>
							
										<div class="pay_show" style="display:none;">
											<ul class="radiochecks">
												<li class="li-option-1" id="net_li_1" >
													<input type="radio" id="f-option" value="DEPOSIT" name="type_of_payment1" checked >
													<label for="f-option" class="check"></label>
													<label for="f-option">Deposit</label>
												</li>
												  
												<li class="li-option-1" id="net_li_2" >
													<input type="radio" id="s-option" value="PG" name="type_of_payment1">
													<label for="s-option" class="check"></label>
													<label for="s-option">Credit Card</label>
												</li>
												  
												<li class="pay_both_1" id="net_li_3" >
													<input type="radio" id="t-option" value="BOTH" name="type_of_payment1">
													<label for="t-option" class="check"></label>
													<label for="t-option">Both</label>
												</li>
											</ul>
											<div class="col-xs-4 nopad" id="deposit-container-1" style="display:none;" >
												<input name="net_both_payment" data-total-price="<?php echo array_sum($net_Total); ?>" id="net_both_payment" onkeyup="price_calculation('NET');" placeholder="Deposit Amount" type="text" class="payinput">
											</div>
										</div>
						  
							  
										<div class="tabtwoout">
<!--
											<div class="all_oters backblur">
												<div class="para_sentnce">
													<strong>Cancellation Policy :</strong> <?php echo $cancel_policy; ?> 
												</div>
											</div>
											<div class="col-md-12 nopad">
												<div class="checkcontent">
													<div class="squaredThree">
														<input type="checkbox" value="0" name="confirm" class="filter_airline" id="squaredThree1" >
														<label for="squaredThree1"></label>
													</div>
													<label for="squaredThree1" class="lbllbl">By proceeding further, you do agree to the booking <a href="<?php echo site_url("dashboard/termsnconditions") ?>" target="_blank">Terms &amp; Conditions</a> of TheChinaGap </label>
												</div>
											</div>
-->
											
											<div class="clearfix"></div>
												<div class="payrowsubmt">
													<div class="col-md-3 col-xs-3 fulat500 nopad">
														<input type="submit" class="paysubmit" id="continue" name="continue"  value="Continue"/>
													</div>
												 </div>
											</div> 
										</div>
										
									</div>
								</div>
							</div>
						</div>
					




























        
</div>

</div>
</div>
                
        </div>
  </section>

</section>






 

  



<!-- /.container --> 

<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>


 
<script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>

  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 



<script>
	$(document).ready(function() {
		
		price_calculation('COMMISSIONABLE');
		
		$('.oneselects').click(function(){
			$('.oneselects').removeClass('active');
			$(this).addClass('active');
		});

		$('#pay_now').click(function(){
			$('.paylater_show').fadeOut(500, function(){
				$('.pay_show').fadeIn();
			});
			price_calculation('NET');
		});
		$('#pay_later').click(function(){
			$('.pay_show').fadeOut(500, function(){
				$('.paylater_show').fadeIn();
			});
			price_calculation('COMMISSIONABLE');
		});

		$('.pay_both_1').click(function() {
			$("#deposit-container-1").fadeIn();
		});
		$('.pay_both_2').click(function() {
			$("#deposit-container-2").fadeIn();
		});
		$(document).on('click', '.li-option-1', function() {
			$("#deposit-container-1").fadeOut();
			price_calculation('NET');
		});
		$(document).on('click', '.li-option-2', function() {
			$("#deposit-container-2").fadeOut();
			price_calculation('COMMISSIONABLE');
		});
		
	});
	
	$("#payment").validate({
	submitHandler: function() {
		var action = $("#payment").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#payment").serialize(),
			dataType: "json",
			beforeSend: function(){
		        $('.lodrefrentwhole').show();
		    },
			success: function(data){
				if(data.status == 1){
					setTimeout(function(){ $('div.lodrefrentwhole').hide();}, 2000);
					window.location.href = data.voucher_url; 	  
				}else if(data.status == -1){
					window.location.href = data.signup_login;
				}else if(data.status == -2){
					$('div.lodrefrentwhole').hide();
					//~ inform();
					alert('You have insufficient balance!');
					console.log('There is no enough amount in your account');
				}else if(data.status == 555){
					console.log('Gate');
					window.location.href = data.GateURL;
				}
			}
		});
		return false; 
	}
});

	function price_calculation(flag,promo_flag) {
		if(flag == 'COMMISSIONABLE') {
			
			//~ $("#checkout-apartment").attr('data-flag','COMMISSIONABLE');
			
			var payment_type = $('input[name=type_of_payment]:checked').val();
			if(payment_type == 'BOTH') {
				var deposit_payment = $('#commissionable_both_payment').val();
			}
			else {
				var deposit_payment = null;
			}
		}
		else if(flag == 'NET') {
			
			//~ $("#checkout-apartment").attr('data-flag','NET');
			
			var payment_type = $('input[name=type_of_payment1]:checked').val();
			if(payment_type == 'BOTH') {
				var deposit_payment = $('#net_both_payment').val();
			}
			else {
				var deposit_payment = null;
			}
		}
		else {
			return false;
		}
		
		if(promo_flag == true) {
			var promo_signature = 'SET';
			var promo_code = $("#promo_code").val();
		}
		else {
			var promo_signature = 'NOTSET';
			var promo_code = '';
		}
		
		var module = "<?php echo $booking_details[0]->product_id; ?>"; 
		var cart_id = "<?php echo $booking_details[0]->pnr_no; ?>";
		$.ajax({
			type: 'POST',
			url: "<?php echo ASSETS.'payment/calculate_price' ?>",
			dataType: "json",
			data:{'product_id':module, "cart_id" : cart_id, "payment_logic" : flag, "payment_type" : payment_type, "promo_signature" : promo_signature, "promo_code" : promo_code, "deposit_payment" : deposit_payment},
			success: function(data){
				if(data.status == 1){
					$('#payment_logic').val(flag);
					$('#payment_type').val(payment_type);
					
					$('.sub_total').text(data.sub_total_cost);
					$('.single_hotel_price').text(data.sub_total_cost);
					
					if(data.promo_code == '' || data.promo_code == 'null' || data.promo_code == null) {
						$('.discount_wrap').hide();
						$('.promo_discount').text('');
						
						$('#promo_msg').html(data.msg);
						$('#promo_msg').addClass('error');
					}
					else {
						if(data.promo_status == 1) {
							$('#promo_msg').html(data.msg);
							$('#promo_msg').addClass('success');
							$("#promo_wrap").html(data.promo_theme);
						}
						else {
							$('#promo_msg').html(data.msg);
							$('#promo_msg').addClass('error');
						}
						
						$('.discount_wrap').show();
						$('.promo_discount').text(data.discount);
					}
					
					$('.taxes').text(data.total_tax);
					
					if(data.payment_type == 'PG' || data.payment_type == 'BOTH') {
						$('.cc_fees').show();
						$('.cc_fees_value').text(data.CC_fees);
						if(data.payment_type == 'BOTH') {
							$('.cc_payment_wrap').show();
							$('.total_cc_payment').text(data.transaction_amount);
							
							$('.deposit_payment_wrap').show();
							$('.total_deposit_payment').text(data.deposit_amount);
							
							$('#deposit_payment').val(data.deposit_amount);
						}
						else {
							$('.cc_payment_wrap').hide();
							$('.total_cc_payment').text('');
							
							$('.deposit_payment_wrap').hide();
							$('.total_deposit_payment').text('');
						}
					}
					else {
						$('.cc_fees_value').text('');
						$('.cc_fees').hide();
						
						$('.cc_payment_wrap').hide();
						$('.total_cc_payment').text('');
						
						$('.deposit_payment_wrap').hide();
						$('.total_deposit_payment').text('');
					}
					$('.total_price').text(data.total_cost);
				}
				else {
					
				}
			}
		});
	}

</script>



<script type="text/javascript">
$(document).ready(function(){
	
  get_booking_hotel_records();
	
   $( "#datepicker11" ).datepicker({
	   onClose: function( selectedDate ) {
		$( "#datepicker22" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker22' ).focus();
	}
	   });
   $( "#datepicker22" ).datepicker();
   $( "#datepicker3" ).datepicker({  maxDate: new Date() });
   
  
    $("#by_user").on("change", function(){
		 if($(this).val() == 'By Staff'){
			 $('#b2b2b').css('display', 'none');
			 $('#staff').css('display', 'inherit');
		 }else if($(this).val() == 'BySubAgents'){
			  $('#b2b2b').css('display', 'inherit');
			 $('#staff').css('display', 'none');
		 }
		
	});
	  
	  
	  $("#by_type").on("change", function(){
		 if($(this).val() == 'Booking'){
			 $('#payment_status').css('display', 'none');
			 $('#booking_status').css('display', 'inherit');
		 }else if($(this).val() == 'payment'){
			  $('#payment_status').css('display', 'inherit');
			 $('#booking_status').css('display', 'none');
		 }
		
	});
  
	
	$('#hotel_books').click(function(){
		$('#BookingList').fadeOut(500, function(){
				$('#hotel_show').fadeIn();
			});
	}); 
	
	 
	
	$('#transfer_books').click(function(){
		$('#BookingList').fadeOut(500, function(){
				$('#transfer_show').fadeIn();
				get_transfer_list();
			});
	}); 
	
	$('#back_list').click(function(){
		$('#hotel_show').fadeOut(500, function(){
				$('#BookingList').fadeIn();
			});
	});
	
	
	
	$('.dash_menus').click(function(){
		$('.sidebar_wrap').toggleClass('open');
	});
	
	$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
	});
	
	
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	
});




$("#owl-demobaners1").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	  $("#owl-demobaners2").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	   $("#owl-demobaners3").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });


$("#owl-demo3").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });

$("#owl-demo4").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });
	  
	  $("#owl-demo5").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });


$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});
var Script = function () {


//    sidebar dropdown menu auto scrolling





//    sidebar toggle

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                //$('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });
	
/*	$('.fa-bars').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });*/




// widget tools

    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });




}();


//Prevent mousewheel on hover of sidebar
/*$( '.top_dash' ).bind( 'mousewheel DOMMouseScroll', function ( e ) {
    var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;

    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
    e.preventDefault();
});*/


</script>

<script>
$(document).ready(function(){
		
		$('#editquestion').click(function(){
			$('.fullquestionswrp').slideToggle(500);
		});
		
		$('#editprivatepub').click(function(){
			$('.fullquestionswrpshare').slideToggle(500);
		});
		
		$('#smsalert').click(function(){
			$('.fullquestionswrp2').slideToggle(500);
		});
		
		$('#changepaswrd').click(function(){
			$('.fullquestionswrp3').slideToggle(500);
		});

        $('#addMarkUp').click(function() {
            $('.fullquestionswrp5').slideToggle(500);
        })
			
	});
	

	</script>
	<script >
		
		 
	  function get_booking_hotel_filter_result(){
		  
		   if($("#reference_type").val() != ''){
			 
			 if($("#reference_no").val() == ''){
				 $("#reference-no-error").html("Please enter the reference number");
				 return false;
			 }else{
				  $("#reference-no-error").html("");
			 }
		  }
		  
		  if($("#reference_no").val() != ''){
			 
			 if($("#reference_type").val() == ''){
				 $("#reference-type-error").html("Please enter the reference type");
				 return false;
			 }else{
				  $("#reference-type-error").html("");
			 }
		  }
		 
		
		 if($("#by_booking").val() != ''){
			 
			 if($("#datepicker11").val() == ''){
				 $("#start-type-error").html("Please select start date");
				 return false;
			 }else{
				 $("#start-type-error").html("");
			 }
			 
			 if($("#datepicker22").val() == ''){
				 $("#end-type-error").html("Please select end date");
				 return false;
			 }else{
				 $("#end-type-error").html("");
			 }
		 }
		 
		 if($("#datepicker11").val() != '' || $("#datepicker22").val() != '' ){
			 if($("#by_booking").val() == ''){
				 $("#date-type-error").html("Please select date type");
				 return false;
			 }else{
				 $("#date-type-error").html(""); 
			 }
			 
		 }
		 
		 if($("#datepicker11").val() != '' || $("#datepicker22").val() != '' ){
			 if($("#by_booking").val() == ''){
				 $("#date-type-error").html("Please select date type");
				 return false;
			 }else{
				 $("#date-type-error").html(""); 
			 }
			 
		 }
		 
		 if($("#staff_id").val() != 0  ){
			 if($("#by_user").val() == ''){
				   $("#user-type-error").html("Please select user type");
			 }else{
				 $("#user-type-error").html("");
			 }
		 }
		 
		 if($("#by_type").val() != 0 ){
			 if($("#status").val() == ''){
				   $("#by-status-error").html("Please select status");
			 }else{
				 $("#by-status-error").html("");
			 }
		 }
		 
		 if($("#status").val() != 0 ){
			 if($("#by_type").val() == ''){
				   $("#by-type-error").html("Please select status type");
			 }else{
				 $("#by-type-error").html("");
			 }
		 }
		
		 $.ajax({
			type: "POST",
			url: $("#bookins_hotel").attr('action'),
			data: $("#bookins_hotel").serialize(),
			dataType: "json",
			success: function(data){
			  	$("#booking_hotel_record").html(data.hotel_bookings); 
				return false;
			}
		});
		 
	 }
	 
		
	 function  get_booking_hotel_records(){
		$.ajax({
			type: 'POST',
			url: "<?php echo ASSETS.'bookings/get_hotel_bookings' ?>",
			dataType: "json",
			success: function(data){
					$("#booking_hotel_record").html(data.hotel_bookings); 
				return false;
			}
		}); 
     }
     
     function get_transfer_list(){
		 $.ajax({
			type: 'POST',
			url: "<?php echo ASSETS.'bookings/get_transfer_bookings' ?>",
			dataType: "json",
			success: function(data){
					$("#booking_transfer_record").html(data.transfer_bookings); 
				return false;
			}
		}); 
	 }
     
 
	 
	</script>


  
</body>
</html>
