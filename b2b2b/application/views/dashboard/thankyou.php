<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>Chinagap</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/jquery_ui.css" rel="stylesheet" type="text/css">
<link href="css/owl.carousel.css" rel="stylesheet" />
<link href="css/animation.css" rel="stylesheet" />
<link href="css/core.css" rel="stylesheet" />
<link href="css/index.css" rel="stylesheet" />
<script src="js/jquery-1.11.0.js"></script>
<script src="js/jquery_ui.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>

<body>

<!-- Navigation -->

<nav id="cbp-spmenu-s1" class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">
<div class="navbak fa fa-long-arrow-left"></div>
    <ul class="page_menu">
    	<li><a href=""><span class="fa fa-home"></span> Home</a></li>
        <li><a href=""><span class="fa fa-plane"></span> Flights</a></li>
    	<li><a href=""><span class="fa fa-bed"></span> Hotels</a></li>
    	<li><a href=""><span class="fa fa-car"></span> Cars</a></li>
    	<li><a href=""><span class="fa fa-train"></span> Rail</a></li>
    	<li><a href=""><span class="fa fa-suitcase"></span> Packages</a></li>
    	<li><a href=""><span class="fa fa-flag"></span> Activities</a></li>
        <li><a href=""><span class="fa fa-exchange"></span> Transfers</a></li>
        <li><a href=""><span class="fa fa-ship"></span> Cruise</a></li>
    </ul>
</nav>

<div class="topssec">
  <div class="container">
  
  <button class="in_page_menu" id="showLeft">
  	<span class="fa fa-bars"></span>
  </button>
  
  <a href="index.html" class="logo"><img alt="Chinagap" src="images/logo.png"> </a> 
  <div class="null_effective">
  <ul id="simnavrit" data-dropdown-out="fadeOutUp" data-dropdown-in="fadeInDown" class="nav nav-pills ritsude">
    
    <li class="dropdown sidebtn flagss csupport"> <a class="topa" href="#">
      <div class="reglognorml">
        <div class="flags">Products</div>
      </div>
      </a> 
    </li>
    
    <li class="dropdown sidebtn flagss csupport"> <a class="topa" href="#">
      <div class="reglognorml">
        <div class="flags">Services </div>                                       
      </div>
      </a> 
    </li>
    
    <li class="dropdown sidebtn flagss csupport"> <a class="topa" href="#">
      <div class="reglognorml">
        <div class="flags">Customers</div>
      </div>
      </a> 
    </li>
    
    <li class="dropdown sidebtn flagss csupport"> <a class="topa" href="#">
      <div class="reglognorml">
        <div class="flags">About Us</div>
      </div>
      </a> 
    </li>
    
    <li class="dropdown sidebtn flagss csupport"> <a class="topa" href="#">
      <div class="reglognorml">
        <div class="flags">Contact</div>
      </div>
      </a> 
    </li>
      
    
    <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle topa" href="#">
      <div class="reglognorml">
        <div class="flags"> <img alt="" src="images/eng.png"><b class="fa fa-chevron-down cartdown"></b> </div>
      </div>
      </a>
      <div class="dropdown-menu mysign1">
      <ul class="">
        <li><a href="#"><span class="curncy_img sprte gbp"></span><span class="name_currency"> English</span></a></li>
      </ul>
      </div>
    </li>
    
    
      </ul>
  </div>
</div>
</div>

<!-- /Navigation -->


 

  <div class="footerbotm">
    <div class="container"> 
    	<span class="copurit">2016 © TravelLights.  All Rights Reserved. </span> 
    	<ul class="rgtlinks">
        	<li><a>ICP 15045570 </a></li>
            <li><a>Careers </a></li>
            <li><a>Terms</a></li>
            <li><a class="scrolltop fa fa-chevron-up"></a></li>
        </ul>
    </div>
  </div>







<script type="text/javascript" src="js/owl.carousel.min.js"></script> 
<script type="text/javascript" src="js/custom.js"></script> 



<script type="text/javascript">
$(document).ready(function(){
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	 
	 $("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});
	 
	
});
</script> 





</body>
</html>
