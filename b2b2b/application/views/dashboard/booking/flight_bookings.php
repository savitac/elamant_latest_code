<?php ?>
 <table id="flight_table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
			   <th>Sl No</th>
			   <th>Actions</th>
               <th>Traveller Name</th>
               <th>Booking Date</th>
               <th>Travel Date</th>
               <th>PNRNo</th>
               <th>Booking Number</th>
               <th>Booking Status</th>
               <th>Currency </th>
               <th>Total Fare</th>
               <th>Net Rate</th>
               <th>Discount</th>
		       <th>Admin Commission</th>
               <th>Agent Commission</th>
               <th>Admin tds</th>
               <th>Agent tds</th>
               <th>Admin markup</th>
               <th>Agent markup</th>
               <th>Email </th>
               <th>Phone </th>
               <th>Trip Type</th>
               <th>Airline Name</th>
               <th> Airline Code</th>
               <th> Flight Number</th>
               <th>Fare Class</th>
               <th>Booking Source</th>
               
               <!--  <th>Service Charge</th>
               <th>GST Charge</th>
               <th>Tax Charge</th>
               <th>Discount Appiled</th>
               <th>promo Code</th>
               <?php if($this->session->userdata('user_type')==2){ ?>
               <th>My Agent Markup</th>
               <th>MY Sub Agent Markup</th>
		       <?php }?>-->
            </tr>
        </thead>
        
        <tbody>
	     <?php 
	       $array_details = $total_records['data'];
           $count = count($array_details['booking_details']);
          for($i=0 ; $i<$count; $i++){ ?>
            <tr>
                    <td><?php    echo $i+1;?></td>
                    <td>
					    <a href="#"><i class="entypo-ticket" title="Voucher"></i>view vowcher</a>
					    <?php if($array_details['booking_details'][$i]['status'] == 'CONFIRMED' || 
					          $array_details['booking_details'][$i]['status'] == "PENDING") 
					        {
				        ?>
						<a href="#" onclick="return confirm('<?php echo $this->TravelLights['BookingTable']['Areyouwant']; ?>')" >
						<i class='entypo-cancel-squared' title="Cancel PNR">Cancel</i> 
						</a>
					    <?php } ?>
					  
                    </td>
                    <td><?php    echo $array_details['booking_customer_details'][$i]['first_name'];?></td>
                    <td><?php    echo date("d-m-Y",strtotime($array_details['booking_details'][$i]['created_datetime'])); ?></td>
                    <td><?php    echo date("d-m-Y",strtotime($array_details['booking_details'][$i]['journey_start']));?></td>
                    <td><?php    echo $array_details['booking_transaction_details'][$i]['pnr'];?></td>
                    <td><?php    echo $array_details['booking_transaction_details'][$i]['book_id'];?></td>
                    <td><?php    
                               if($array_details['booking_details'][$i]['status'] == "BOOKING_INPROGRESS"){
								   echo "INPROGRESS";
							   }else{
								   echo $array_details['booking_details'][$i]['status']; 
							   }
                 
                        ?>
                    </td>
                    <td><?php   echo $array_details['booking_details'][$i]['currency'];?></td>
                    <td><?php   echo $array_details['booking_transaction_details'][$i]['total_fare'];?></td>
                    <td><?php   echo $array_details['booking_transaction_details'][$i]['total_fare'];?></td>
                    <td><?php   echo $array_details['booking_details'][$i]['discount'];?></td>
                    <td><?php   echo $array_details['booking_transaction_details'][$i]['admin_commission'];?></td>
                    <td><?php   echo $array_details['booking_transaction_details'][$i]['agent_commission'];?></td>
                    <td><?php   echo $array_details['booking_transaction_details'][$i]['admin_commission'];?></td>
                    <td><?php   echo $array_details['booking_transaction_details'][$i]['admin_tds'];?></td>
                    <td><?php   echo $array_details['booking_transaction_details'][$i]['admin_markup'];?></td>
                    <td><?php   echo $array_details['booking_transaction_details'][$i]['agent_markup'];?></td>                
                    <td><?php   echo $array_details['booking_details'][$i]['email'];?></td>
                    <td><?php   echo $array_details['booking_details'][$i]['phone'];?></td>
                    <td><?php   echo $array_details['booking_details'][$i]['trip_type'];?></td>
                    <td><?php   echo $array_details['booking_itinerary_details'][$i]['airline_name'];?></td>
                    <td><?php   echo $array_details['booking_itinerary_details'][$i]['airline_code'];?></td>
                    <td><?php   echo $array_details['booking_itinerary_details'][$i]['flight_number'];?></td>
                    <td><?php   echo $array_details['booking_itinerary_details'][$i]['fare_class'];?></td>
                    <td><?php   echo $array_details['booking_details'][$i]['booking_source'];?></td>
                   
                    <!-- <td>Service Charge</td>
                    <td>GST Charge</td>
                    <td>Tax Charge</td>
                    <td>Discount Appiled</td>
                    <td>promo Code</td>
                    <?php if($this->session->userdata('user_type') == 2){ ?>
                    <td>My Agent Markup</td>
                    <td>MY Sub Agent Markup</td>
                    <?php } ?>-->
                   
            </tr>
           <?php } ?>
        </tbody>
    </table>
    
    <script>
    $(document).ready(function(){
      $('#flight_table').DataTable({responsive: true});
    });
      
    </script>


