   
<table id="example-hotelbook" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
                <th><?php echo $this->TravelLights['BookingTable']['Slno']; ?></th>  
                <th><?php echo $this->TravelLights['BookingTable']['Actions']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['TravellerName']; ?></th>   
                <th><?php echo $this->TravelLights['BookingTable']['BookingDate']; ?></th>   
                <th><?php echo $this->TravelLights['BookingTable']['PNRNo']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['BookingNo']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['CheckIn']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['CheckOut']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['BookingStatus']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['PaymentStatus']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['Currency']; ?></th> 
                <th><?php echo $this->TravelLights['BookingTable']['CurrencyExchange']; ?></th> 
                <th><?php echo $this->TravelLights['BookingTable']['TotalAmount']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['NetRate']; ?></th> 
                <th><?php echo $this->TravelLights['BookingTable']['MyMarkup']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['MyBasePrice']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['ServiceCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['GSTCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['TaxCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['DiscountApplied']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['PromoCode']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['Discount']; ?></th>
                <?php if($this->session->userdata('user_type') == 2){ ?>
                <th><?php echo $this->TravelLights['BookingTable']['MySubAgentMarkup']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['MySubAgentPrice']; ?></th>
                <?php } ?>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><?php echo $this->TravelLights['BookingTable']['Slno']; ?></th>  
                <th><?php echo $this->TravelLights['BookingTable']['Actions']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['TravellerName']; ?></th>   
                <th><?php echo $this->TravelLights['BookingTable']['BookingDate']; ?></th>   
                <th><?php echo $this->TravelLights['BookingTable']['PNRNo']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['BookingNo']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['CheckIn']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['CheckOut']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['BookingStatus']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['PaymentStatus']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['Currency']; ?></th> 
                <th><?php echo $this->TravelLights['BookingTable']['CurrencyExchange']; ?></th> 
                <th><?php echo $this->TravelLights['BookingTable']['TotalAmount']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['NetRate']; ?></th> 
                <th><?php echo $this->TravelLights['BookingTable']['MyMarkup']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['MyBasePrice']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['ServiceCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['GSTCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['TaxCharge']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['DiscountApplied']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['PromoCode']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['Discount']; ?></th>
                <?php if($this->session->userdata('user_type') == 2){ ?>
                <th><?php echo $this->TravelLights['BookingTable']['MySubAgentMarkup']; ?></th>
                <th><?php echo $this->TravelLights['BookingTable']['MySubAgentPrice']; ?></th>
              
                <?php } ?>
            </tr>
        </tfoot>
        <tbody>
          <?php $c=1;  
          for ($i=0; $i < count($hotel_orders); $i++){
			   
              ?>
            <tr>
                <td><?php echo $c++; ?></td>
                <td>
					<a href="<?php echo site_url(); ?>bookings/view_bookings/<?php  echo base64_encode(json_encode($hotel_orders[$i]->parent_pnr_no)); ?>"> 
					<i class="entypo-ticket" title="Voucher"></i>
					<?php echo $this->TravelLights['BookingTable']['ViewVoucher']; ?>
					</a>
					
					<?php if($hotel_orders[$i]->booking_status == 'CONFIRMED' || $hotel_orders[$i]->booking_status == "PENDING") { ?>
						<a href="<?php echo ASSETS;?>bookings/cancel/<?php echo $hotel_orders[$i]->product_name;?>/<?php echo base64_encode(base64_encode($hotel_orders[$i]->pnr_no)); ?>" onclick="return confirm('<?php echo $this->TravelLights['BookingTable']['Areyouwant']; ?>')" >
							<i class='entypo-cancel-squared' title="Cancel PNR"><?php echo $this->TravelLights['BookingTable']['Cancel']; ?></i> 
						</a>
					<?php } ?>
					<?php
						if($hotel_orders[$i]->booking_status == 'CONFIRMED' && $hotel_orders[$i]->payment_type == 'PAYLATER') {
					?>
							<a href="<?php echo ASSETS; ?>payment/payment_view/<?php  echo base64_encode(json_encode($hotel_orders[$i]->pnr_no)); ?>/<?php echo $hotel_orders[$i]->product_id;?>">Make Payment</a>
					<?php
						}
					?>
				</td>
                <td><?php echo $hotel_orders[$i]->leadpax; ?></td>
                <td><?php echo $hotel_orders[$i]->voucher_date; ?></td>
                <td><?php echo $hotel_orders[$i]->pnr_no; ?></td>
                <td><?php echo $hotel_orders[$i]->booking_no; ?></td>
                <td><?php echo date('d-m-Y', strtotime($hotel_orders[$i]->checkin)); ?></td>
                <td><?php echo date('d-m-Y', strtotime($hotel_orders[$i]->checkout)); ?></td>
                <td><?php echo $hotel_orders[$i]->booking_status; ?></td>
                <td><?php echo $hotel_orders[$i]->payment_status; ?></td>
                <td><?php echo $hotel_orders[$i]->booking_currency; ?></td>
                <td><?php echo $hotel_orders[$i]->currency_exchange_rate; ?></td>
                <td><?php echo number_format(($hotel_orders[$i]->total_amount * $hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                 <?php if($this->session->userdata('user_type') == 2){ ?>   
                <td><?php echo number_format(($hotel_orders[$i]->admin_baseprice * $hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                 <td><?php echo number_format(($hotel_orders[$i]->agent_markup *$hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                <td><?php echo  number_format(($hotel_orders[$i]->agent_base_amount*$hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                <?php }else{ ?>
				  <td><?php echo number_format(($hotel_orders[$i]->agent_base_amount*$hotel_orders[$i]->currency_exchange_rate),2); ?></td>
				 <td><?php echo number_format(($hotel_orders[$i]->my_b2b2b_markup*$hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                <td><?php echo number_format(($hotel_orders[$i]->b2b2b_baseprice*$hotel_orders[$i]->currency_exchange_rate),2); ?></td>	
				<?php } ?>
                <td><?php echo number_format(($hotel_orders[$i]->service_charge *$hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                 <td><?php echo number_format(($hotel_orders[$i]->gst_charge *$hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                 <td><?php echo  number_format(($hotel_orders[$i]->tax_charge *$hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                 <?php if ($hotel_orders[$i]->promo_code != '' || $hotel_orders[$i]->promo_code != NULL) { ?>
                  <?php  $promoid = 'YES' ?>
                <?php }else { ?>
                <?php  $promoid = 'NO' ?>
                <?php } ?>
                 <td><?php echo $promoid; ?></td>
                 <td><?php echo $hotel_orders[$i]->promo_code; ?></td>
                 <td><?php echo number_format(($hotel_orders[$i]->discount*$hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                 <?php if($this->session->userdata('user_type') == 2){ ?>
                <td><?php echo number_format(($hotel_orders[$i]->my_agent_Markup *$hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                <td><?php echo number_format(($hotel_orders[$i]->sub_agent_baseprice*$hotel_orders[$i]->currency_exchange_rate),2); ?></td>
                <?php } ?>
                
            </tr>
           <?php } ?>
        </tbody>
    </table>
    
    
    
    <script>
   
	 $(document).ready(function(){
      $('#example-hotelbook').DataTable({responsive: true});
    });
      
    </script>
