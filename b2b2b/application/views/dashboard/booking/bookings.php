<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title><?= $this->session->userdata('company_name')?></title>
<?php echo $this->load->view('core/load_css'); ?>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>

<link href="<?php echo ASSETS;?>assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Navigation -->

<?php echo $this->load->view('dashboard/top'); ?>  
<!-- /Navigation -->





<section id="main-content">
  
  <section class="wrapper">
	<div class="main-chart">
		
        <div id="BookingList">
        <span class="profile_head"><?php echo $this->TravelLights['Bookings']['MyBookings']; ?></span>

<div class="rowit">
	 


<div class="top_booking_info">

        <a id="flight_books" class="col-sm-3 box_main" style="display:none">
          <div class="hover_box">
            <div class="box_in"> <span class="fa fa-plane"></span>
              <h3>4</h3>
            </div>
            <span class="bkd_all"><strong><?php echo $this->TravelLights['Bookings']['Flights']; ?></strong><?php echo $this->TravelLights['Bookings']['booked']; ?></span> </div>
        </a>
        
        <a id="hotel_books" class="col-sm-3 box_main">
          <div class="hover_box">
            <div class="box_in"> <span class="fa fa-bed"></span>
              <h3><?php echo count($hotel_orders); ?></h3>
            </div>
            <span class="bkd_all"><strong>Hotels</strong> <?php echo $this->TravelLights['Bookings']['booked']; ?></span> </div>
        </a>
        
        
        
        <a class="col-sm-3 box_main" style="display:none">
          <div class="hover_box">
            <div class="box_in"> <span class="fa fa-suitcase"></span>
              <h3>0</h3>
            </div>
            <span class="bkd_all"><strong>Packages</strong> <?php echo $this->TravelLights['Bookings']['booked']; ?></span> </div>
        </a>
        
        
        <a class="col-sm-3 box_main" style="display:none">
          <div class="hover_box">
            <div class="box_in"> <span class="fa fa-car"></span>
              <h3>0</h3>
            </div>
            <span class="bkd_all"><strong>Cars</strong> <?php echo $this->TravelLights['Bookings']['booked']; ?></span> </div>
        </a>
        
        
        
        <a class="col-sm-3 box_main" style="display:none">
          <div class="hover_box">
            <div class="box_in"> <span class="fa fa-flag"></span>
              <h3>0</h3>
            </div>
            <span class="bkd_all"><strong>Activities</strong> <?php echo $this->TravelLights['Bookings']['booked']; ?></span> </div>
        </a>
        
        
        <a class="col-sm-3 box_main" style="display:none">
          <div class="hover_box">
            <div class="box_in"> <span class="fa fa-ship"></span>
              <h3>0</h3>
            </div>
            <span class="bkd_all"><strong>Cruise</strong> <?php echo $this->TravelLights['Bookings']['booked']; ?></span> </div>
        </a>
        
        <a id="transfer_books" class="col-sm-3 box_main">
          <div class="hover_box">
            <div class="box_in"> <span class="fa fa-exchange"></span>
              <h3><?php echo count($transfer_orders); ?></h3>
            </div>
            <span class="bkd_all"><strong><?php echo $this->TravelLights['Bookings']['Transfers']; ?></strong> <?php echo $this->TravelLights['Bookings']['booked']; ?></span> </div>
        </a>
        
        
        <!-- flight boooks new start-->
        
          <a id="flights_books" class="col-sm-3 box_main">
          <div class="hover_box">
            <div class="box_in"> <span class="fa fa-plane"></span>
              <h3><?php echo $flight_book_count; ?></h3>
            </div>
            <span class="bkd_all"><strong>Flights</strong> <?php echo $this->TravelLights['Bookings']['booked']; ?></span> </div>
           </a>
        
        <!-- flight books end-->
   
      </div>

</div>
</div>

                              <div id="hotel_show" style="display:none;">
                                <span class="profile_head"><?php echo $this->TravelLights['Bookings']['HotelBookingsList']; ?></span>
                                
                                <div class="withedrow">
                                	<div class="rowit">
                                    <a class="rgtsambtn" id="back_list"><?php echo $this->TravelLights['Bookings']['Backtolist']; ?></a>
                                
                                    <div class="clearfix"></div>
                                    
                                         
                                  <div class="addtiktble">
									  <form id="bookins_hotel" name="bookins_hotel" class="validate form-horizontal"  method="post" action="<?php echo base_url().'bookings/get_hotel_bookings'; ?>" novalidate  >  
									  
									  
									  
									    <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['ByReference']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="reference_type" id="reference_type" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['SelectReferenceType']; ?></option>
                                       <option value="pnr_no"><?php echo $this->TravelLights['Bookings']['PNRNo']; ?></option>
                                        <option value="booking_no"><?php echo $this->TravelLights['Bookings']['BookingNo']; ?></option>
                                 </select>
                                 </div>
                                 <span id="reference-type-error" class="error" for="by_booking"> </span>
                                 </div>
                               
                                 <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2"  name="reference_no" value="" id="reference_no" placeholder="<?php echo $this->TravelLights['Bookings']['ReferenceNo']; ?>"   />
                                <span id="reference-no-error" class="error" for="reference_no"> </span>
                                 </div>
                                 </div>
                           
                        </div>
					
						 <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['DateType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="by_booking" id="by_booking" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['SelectDate']; ?></option>
                                       <option value="booking_date"><?php echo $this->TravelLights['Bookings']['BookingDate']; ?></option>
                                        <option value="checkin_date"><?php echo $this->TravelLights['Bookings']['Checkin']; ?></option>
                                        <option value="checkout_date"><?php echo $this->TravelLights['Bookings']['Checkout']; ?></option>
                                </select>
                                 <span id="date-type-error" class="error" for="by_booking"> </span>
                              </div>
                             
                            </div>
                        </div>
                        
                        <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['DateRange']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly value="" name="start_date" id="datepicker11" placeholder="<?php echo $this->TravelLights['Bookings']['StartDate']; ?>" />
                                 <span id="start-type-error" class="error" for="by_booking"> </span>
                                 </div>
                               
                                 <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly name="end_date" value="" id="datepicker22" placeholder="<?php echo $this->TravelLights['Bookings']['EndDate']; ?>"   />
                                <span id="end-type-error" class="error" for="by_booking"> </span>
                                 </div>
                                 </div>
                           
                        </div>
                        
                        
                         <div class="likrticktsec" <?php if($this->session->userdata('user_type') == 4){ ?> style="display:none";  <?php } ?> >
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['ByUsers']; ?> </div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="by_user" id="by_user" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['SelectUserType']; ?></option>
                                       <option value="ByStaff"><?php echo $this->TravelLights['Bookings']['ByStaff']; ?></option>
                                        <option value="BySubAgents"><?php echo $this->TravelLights['Bookings']['BySubAgents']; ?></option>
                                </select>
                                </div>
                                <span id="user-type-error" class="error" for="by_user"> </span>
                                 </div>
                               
                                 <div class="coltcnt" id="b2b2b" style="display:none;">
                                 <div class="selectedwrap inboxs">
                                 <select name="sub_agent_id" id="sub_agent_id"class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value="0"><?php echo $this->TravelLights['Bookings']['ALL']; ?> </option>
									  <?php if($agentadmin_list!=''){ for($a=0;$a<count($agentadmin_list);$a++){ ?>
                                        <option value="<?php echo $agentadmin_list[$a]->user_details_id; ?>"><?php echo $agentadmin_list[$a]->user_name."(".$agentadmin_list[$a]->company_name.")"; ?></option>
                                       <?php  } } ?>
                                </select>
                                </div>
                                 <span id="sub-type-error" class="error" for="by_user"> </span>
                                </div>
                                
                                <div class="coltcnt" id="staff" >
                                <div class="selectedwrap inboxs">
                                 <select name="staff_id" id="staff_id"class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value="0"><?php echo $this->TravelLights['Bookings']['ALL']; ?> </option>
									  <?php if($agentadmin_list!=''){ for($a=0;$a<count($agentadmin_list);$a++){ ?>
                                        <option value="<?php echo $agentstaff_list[$a]->user_details_id; ?>"><?php echo $agentstaff_list[$a]->user_name; ?></option>
                                       <?php  } } ?>
                                </select>
                                </div>
                                <span id="staff-type-error" class="error" for="by_user"> </span>
                                </div>
                                
                                 </div>
                           
                        </div>
                        
                        
                         <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['ByStatus']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="by_type" id="by_type" class="payinselect mySelectBoxClassfortab hasCustomSelect">
                                       <option value=""><?php echo $this->TravelLights['Bookings']['SelectByStatus']; ?></option>
                                       <option value="Booking"><?php echo $this->TravelLights['Bookings']['ByBooking']; ?></option>
                                        <option value="payment"><?php echo $this->TravelLights['Bookings']['ByPayment']; ?></option>
                                </select>
                                </div>
                                <span id="by-type-error" class="error" for="by_user"> </span>
                                 </div>
                               
                                 <div class="coltcnt" id="booking_status">
                                 <div class="selectedwrap inboxs">
                                <select name="status" id="status" class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value=""><?php echo $this->TravelLights['Bookings']['SelectStatus']; ?></option>
                                       <option value="CONFIRMED"><?php echo $this->TravelLights['Bookings']['CONFIRMED']; ?></option>
                                        <option value="INCOMPLETE"><?php echo $this->TravelLights['Bookings']['PROCESS']; ?></option>
                                        <option value="HOLD"><?php echo $this->TravelLights['Bookings']['HOLD']; ?></option>
                                        <option value="FAILED"><?php echo $this->TravelLights['Bookings']['CANCELLED']; ?></option>
                                </select>
                                </div>
                                 <span id="by-status-error" class="error" for="by_user"> </span>
                                </div>
                                
                                <div class="coltcnt" id="payment_status" style="display:none">
                                <div class="selectedwrap inboxs">
                                <select name="pay_status" id="pay_status" class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value=""><?php echo $this->TravelLights['Bookings']['SelectStatus']; ?></option>
                                       <option value="SUCCESS"><?php echo $this->TravelLights['Bookings']['SUCCESS']; ?></option>
                                       <option value="PENDING"><?php echo $this->TravelLights['Bookings']['PENDING']; ?></option>
                                        <option value="INCOMPLETE"><?php echo $this->TravelLights['Bookings']['FAILED']; ?></option>
                                </select>
                                </div>
                                 <span id="by-status-error" class="error" for="by_user"> </span>
                                </div>
                                
                                 </div>
                           
                        </div>
                        
                        
                       
                         <div class="likrticktsec">
                            <div class="cobldo">&nbsp;</div>
                            <div class="coltcnt">
                            <input type="button" value="<?php echo $this->TravelLights['Bookings']['Search']; ?>" onclick="get_booking_hotel_filter_result()" class="adddtickt">
                            </div>
                        </div>
									  </form>
								 </div>     
                                    
                                     <br /> <br />
                                  <div id="booking_hotel_record" >
                             
                                  </div>
                                  
                                 
                                    </div>
                                </div>
                                
                                </div>
                                
                                
                                
                    <!----flight div start---->
                    
                    <div id="flight_show" style="display:none;">
                                <span class="profile_head">Flight Booking List</span>
                                
                                <div class="withedrow">
                                	<div class="rowit">
                                    <a class="rgtsambtn" id="flight_back_list"><?php echo $this->TravelLights['Bookings']['Backtolist']; ?></a>
                                
                                    <div class="clearfix"></div>
                                    
                                         
                                  <div class="addtiktble">
									<!--  <form id="bookins_hotel" name="bookins_hotel" class="validate form-horizontal"  method="post" action="<?php echo base_url().'bookings/get_hotel_bookings'; ?>" novalidate  >  
									    <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['ByReference']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="reference_type" id="reference_type" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['SelectReferenceType']; ?></option>
                                       <option value="pnr_no"><?php echo $this->TravelLights['Bookings']['PNRNo']; ?></option>
                                        <option value="booking_no"><?php echo $this->TravelLights['Bookings']['BookingNo']; ?></option>
                                 </select>
                                 </div>
                                 <span id="reference-type-error" class="error" for="by_booking"> </span>
                                 </div>
                               
                                 <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2"  name="reference_no" value="" id="reference_no" placeholder="<?php echo $this->TravelLights['Bookings']['ReferenceNo']; ?>"   />
                                <span id="reference-no-error" class="error" for="reference_no"> </span>
                                 </div>
                                 </div>
                           
                        </div>
					
						 <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['DateType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="by_booking" id="by_booking" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['SelectDate']; ?></option>
                                       <option value="booking_date"><?php echo $this->TravelLights['Bookings']['BookingDate']; ?></option>
                                        <option value="checkin_date"><?php echo $this->TravelLights['Bookings']['Checkin']; ?></option>
                                        <option value="checkout_date"><?php echo $this->TravelLights['Bookings']['Checkout']; ?></option>
                                </select>
                                 <span id="date-type-error" class="error" for="by_booking"> </span>
                              </div>
                             
                            </div>
                        </div>
                        
                        <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['DateRange']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly value="" name="start_date" id="datepicker11" placeholder="<?php echo $this->TravelLights['Bookings']['StartDate']; ?>" />
                                 <span id="start-type-error" class="error" for="by_booking"> </span>
                                 </div>
                               
                                 <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly name="end_date" value="" id="datepicker22" placeholder="<?php echo $this->TravelLights['Bookings']['EndDate']; ?>"   />
                                <span id="end-type-error" class="error" for="by_booking"> </span>
                                 </div>
                                 </div>
                           
                        </div>
                        
                        
                         <div class="likrticktsec" <?php if($this->session->userdata('user_type') == 4){ ?> style="display:none";  <?php } ?> >
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['ByUsers']; ?> </div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="by_user" id="by_user" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['SelectUserType']; ?></option>
                                       <option value="ByStaff"><?php echo $this->TravelLights['Bookings']['ByStaff']; ?></option>
                                        <option value="BySubAgents"><?php echo $this->TravelLights['Bookings']['BySubAgents']; ?></option>
                                </select>
                                </div>
                                <span id="user-type-error" class="error" for="by_user"> </span>
                                 </div>
                               
                                 <div class="coltcnt" id="b2b2b" style="display:none;">
                                 <div class="selectedwrap inboxs">
                                 <select name="sub_agent_id" id="sub_agent_id"class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value="0"><?php echo $this->TravelLights['Bookings']['ALL']; ?> </option>
									  <?php if($agentadmin_list!=''){ for($a=0;$a<count($agentadmin_list);$a++){ ?>
                                        <option value="<?php echo $agentadmin_list[$a]->user_details_id; ?>"><?php echo $agentadmin_list[$a]->user_name."(".$agentadmin_list[$a]->company_name.")"; ?></option>
                                       <?php  } } ?>
                                </select>
                                </div>
                                 <span id="sub-type-error" class="error" for="by_user"> </span>
                                </div>
                                
                                <div class="coltcnt" id="staff" >
                                <div class="selectedwrap inboxs">
                                 <select name="staff_id" id="staff_id"class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value="0"><?php echo $this->TravelLights['Bookings']['ALL']; ?> </option>
									  <?php if($agentadmin_list!=''){ for($a=0;$a<count($agentadmin_list);$a++){ ?>
                                        <option value="<?php echo $agentstaff_list[$a]->user_details_id; ?>"><?php echo $agentstaff_list[$a]->user_name; ?></option>
                                       <?php  } } ?>
                                </select>
                                </div>
                                <span id="staff-type-error" class="error" for="by_user"> </span>
                                </div>
                                
                                 </div>
                           
                        </div>
                        
                        
                         <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['ByStatus']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="by_type" id="by_type" class="payinselect mySelectBoxClassfortab hasCustomSelect">
                                       <option value=""><?php echo $this->TravelLights['Bookings']['SelectByStatus']; ?></option>
                                       <option value="Booking"><?php echo $this->TravelLights['Bookings']['ByBooking']; ?></option>
                                        <option value="payment"><?php echo $this->TravelLights['Bookings']['ByPayment']; ?></option>
                                </select>
                                </div>
                                <span id="by-type-error" class="error" for="by_user"> </span>
                                 </div>
                               
                                 <div class="coltcnt" id="booking_status">
                                 <div class="selectedwrap inboxs">
                                <select name="status" id="status" class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value=""><?php echo $this->TravelLights['Bookings']['SelectStatus']; ?></option>
                                       <option value="CONFIRMED"><?php echo $this->TravelLights['Bookings']['CONFIRMED']; ?></option>
                                        <option value="INCOMPLETE"><?php echo $this->TravelLights['Bookings']['PROCESS']; ?></option>
                                        <option value="HOLD"><?php echo $this->TravelLights['Bookings']['HOLD']; ?></option>
                                        <option value="FAILED"><?php echo $this->TravelLights['Bookings']['CANCELLED']; ?></option>
                                </select>
                                </div>
                                 <span id="by-status-error" class="error" for="by_user"> </span>
                                </div>
                                
                                <div class="coltcnt" id="payment_status" style="display:none">
                                <div class="selectedwrap inboxs">
                                <select name="pay_status" id="pay_status" class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value=""><?php echo $this->TravelLights['Bookings']['SelectStatus']; ?></option>
                                       <option value="SUCCESS"><?php echo $this->TravelLights['Bookings']['SUCCESS']; ?></option>
                                       <option value="PENDING"><?php echo $this->TravelLights['Bookings']['PENDING']; ?></option>
                                        <option value="INCOMPLETE"><?php echo $this->TravelLights['Bookings']['FAILED']; ?></option>
                                </select>
                                </div>
                                 <span id="by-status-error" class="error" for="by_user"> </span>
                                </div>
                                
                                 </div>
                           
                        </div>
                        
                        
                       
                         <div class="likrticktsec">
                            <div class="cobldo">&nbsp;</div>
                            <div class="coltcnt">
                            <input type="button" value="<?php echo $this->TravelLights['Bookings']['Search']; ?>" onclick="get_booking_hotel_filter_result()" class="adddtickt">
                            </div>
                        </div>
                        
                       	  
									  
					  </form>-->
				    </div>     
                             <br /> <br />
                             
                        <div id="booking_flight_record" >Flight List </div>
                                  
                                 </div>
                                </div>
                                </div>
                    
                    <!--flight div end-->            
                                

                               <div id="transfer_show" style="display:none;">
                                <span class="profile_head"><?php echo $this->TravelLights['Bookings']['TransferBookingsList']; ?></span>
                                
                                <div class="withedrow">
                                	<div class="rowit">
                                    <a class="rgtsambtn" id="trans_back_list"><?php echo $this->TravelLights['Bookings']['Backtolist']; ?></a>
                                
                                    <div class="clearfix"></div>
                                    
                                  

                                  <div class="addtiktble">
									  <form id="bookins_transfer" name="bookins_transfer" class="validate form-horizontal"  method="post" action="<?php echo base_url().'bookings/get_transfer_bookings'; ?>" novalidate  >  
									   <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['ByReference']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="t_reference_type" id="t_reference_type" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['SelectReferenceType']; ?></option>
                                       <option value="pnr_no"><?php echo $this->TravelLights['Bookings']['PNRNo']; ?></option>
                                        <option value="booking_no"><?php echo $this->TravelLights['Bookings']['BookingNo']; ?></option>
                                 </select>
                                 </div>
                                 <span id="t_reference-type-error" class="error" for="by_booking"> </span>
                                 </div>
                               
                                 <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2"  name="t_reference_no" value="" id="t_reference_no" placeholder="<?php echo $this->TravelLights['Bookings']['ReferenceNo']; ?>"   />
                                <span id="t_reference-no-error" class="error" for="reference_no"> </span>
                                 </div>
                                 </div>
                           
                        </div>
					
						 <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['DateType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="t_by_booking" id="t_by_booking" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['SelectDate']; ?></option>
                                       <option value="booking_date"><?php echo $this->TravelLights['Bookings']['BookingDate']; ?></option>
                                        <option value="travel_date"><?php echo $this->TravelLights['Bookings']['TravelDate']; ?></option>
                                </select>
                                 <span id="t_date-type-error" class="error" for="by_booking"> </span>
                              </div>
                             
                            </div>
                        </div>
                        
                        <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['DateRange']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly value="" name="t_start_date" id="datepicker111" placeholder="<?php echo $this->TravelLights['Bookings']['StartDate']; ?>" />
                                 <span id="t_start-type-error" class="error" for="by_booking"> </span>
                                 </div>
                               
                                 <div class="coltcnt">
                                <input type="text" class="payinput cols-sm-2" readonly name="t_end_date" value="" id="datepicker222" placeholder="<?php echo $this->TravelLights['Bookings']['EndDate']; ?>"   />
                                <span id="t_end-type-error" class="error" for="by_booking"> </span>
                                 </div>
                                 </div>
                           
                        </div>
                        
                        
                         <div class="likrticktsec" <?php if($this->session->userdata('user_type') == 4){ ?> style="display:none";  <?php } ?> >
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['ByUsers']; ?> </div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="t_by_user" id="t_by_user" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['SelectUserType']; ?> </option>
                                       <option value="ByStaff"><?php echo $this->TravelLights['Bookings']['ByStaff']; ?></option>
                                        <option value="BySubAgents"><?php echo $this->TravelLights['Bookings']['BySubAgents']; ?> </option>
                                </select>
                                </div>
                                <span id="t_user-type-error" class="error" for="by_user"> </span>
                                 </div>
                               
                                 <div class="coltcnt" id="t_b2b2b" style="display:none;">
                                 <div class="selectedwrap inboxs">
                                 <select name="t_sub_agent_id" id="t_sub_agent_id"class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value="0"><?php echo $this->TravelLights['Bookings']['ALL']; ?>  </option>
									  <?php if($agentadmin_list!=''){ for($a=0;$a<count($agentadmin_list);$a++){ ?>
                                        <option value="<?php echo $agentadmin_list[$a]->user_details_id; ?>"><?php echo $agentadmin_list[$a]->user_name."(".$agentadmin_list[$a]->company_name.")"; ?></option>
                                       <?php  } } ?>
                                </select>
                                </div>
                                 <span id="t_sub-type-error" class="error" for="by_user"> </span>
                                </div>
                                
                                <div class="coltcnt" id="t_staff" >
                                <div class="selectedwrap inboxs">
                                 <select name="t_staff_id" id="t_staff_id"class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value="0"><?php echo $this->TravelLights['Bookings']['ALL']; ?> </option>
									  <?php if($agentadmin_list!=''){ for($a=0;$a<count($agentadmin_list);$a++){ ?>
                                        <option value="<?php echo $agentstaff_list[$a]->user_details_id; ?>"><?php echo $agentstaff_list[$a]->user_name; ?></option>
                                       <?php  } } ?>
                                </select>
                                </div>
                                <span id="t_staff-type-error" class="error" for="by_user"> </span>
                                </div>
                                
                                 </div>
                           
                        </div>
                        
                        
                         <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['ByStatus']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="t_by_type" id="t_by_type" class="payinselect mySelectBoxClassfortab hasCustomSelect">
                                       <option value=""><?php echo $this->TravelLights['Bookings']['SelectByStatus']; ?></option>
                                       <option value="Booking"><?php echo $this->TravelLights['Bookings']['ByBooking']; ?></option>
                                        <option value="payment"><?php echo $this->TravelLights['Bookings']['ByPayment']; ?></option>
                                </select>
                                </div>
                                <span id="t_by-type-error" class="error" for="by_user"> </span>
                                 </div>
                               
                                 <div class="coltcnt" id="booking_status">
                                 <div class="selectedwrap inboxs">
                                <select name="t_status" id="t_status" class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value=""><?php echo $this->TravelLights['Bookings']['SelectStatus']; ?></option>
                                       <option value="CONFIRMED"><?php echo $this->TravelLights['Bookings']['CONFIRMED']; ?></option>
                                       <option value="PENDING CONFIRMATION"><?php echo $this->TravelLights['Bookings']['PENDINGCONFIRMATION']; ?></option>
                                        <option value="INCOMPLETE"><?php echo $this->TravelLights['Bookings']['PROCESS']; ?></option>
                                        <option value="HOLD"><?php echo $this->TravelLights['Bookings']['HOLD']; ?></option>
                                        <option value="CANCELLED"><?php echo $this->TravelLights['Bookings']['CANCELLED']; ?></option>
                                        <option value="FAILED"><?php echo $this->TravelLights['Bookings']['FAILED']; ?></option>
                                </select>
                                </div>
                                 <span id="t_by-status-error" class="error" for="by_user"> </span>
                                </div>
                                
                                <div class="coltcnt" id="payment_status" style="display:none">
                                <div class="selectedwrap inboxs">
                                <select name="t_pay_status" id="t_pay_status" class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value=""><?php echo $this->TravelLights['Bookings']['SelectStatus']; ?></option>
                                       <option value="SUCCESS"><?php echo $this->TravelLights['Bookings']['SUCCESS']; ?></option>
                                       <option value="PENDING"><?php echo $this->TravelLights['Bookings']['PENDING']; ?></option>
                                        <option value="INCOMPLETE"><?php echo $this->TravelLights['Bookings']['FAILED']; ?></option>
                                </select>
                                </div>
                                 <span id="t_by-status-error" class="error" for="by_user"> </span>
                                </div>
                                
                                 </div>
                           
                        </div>
                        
                              <div class="likrticktsec"  >
                            <div class="cobldo"><?php echo $this->TravelLights['Bookings']['Transfers']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <div class="selectedwrap inboxs">
                                <select name="pickup" id="pickup" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['PickUp']; ?></option>
                                      <?php for($code =0; $code < count($tranfer_list_code); $code++){ ?>
										<option value="<?php echo $tranfer_list_code[$code]->transfer_list_code; ?>"> <?php echo $tranfer_list_code[$code]->english; ?>  </option>
								     <?php } ?> 
                                </select>
                                </div>
                                
                                 </div>
                                 <div class="coltcnt">
                                  <div class="selectedwrap inboxs">
                                <select name="dropoff" id="dropoff" class="payinselect mySelectBoxClassfortab hasCustomSelect">
									 <option value=""><?php echo $this->TravelLights['Bookings']['DropOff']; ?></option>
                                      <?php for($drop =0; $drop < count($tranfer_list_code); $drop++){ ?>
									<option value="<?php echo $tranfer_list_code[$drop]->transfer_list_code; ?>"> <?php echo $tranfer_list_code[$drop]->english; ?>  </option>
								<?php } ?>
                                </select>
                                </div>
                                 <div class="likrticktsec">
                            <div class="cobldo">&nbsp;</div>
                            <div class="coltcnt">
                            <input type="button" value="<?php echo $this->TravelLights['Bookings']['Search']; ?>" onclick="get_booking_transfer_filter_result()" class="adddtickt">
                            </div>
                        </div>
                                
                                 </div>
                          
                        
                       	  
                        </div>
                        
                       
									  </form>
								 </div>     
                                  
                                     <br /> <br />
                                  <div id="booking_transfer_record" >
                                  
                                  </div>
                                  
                                 
                                    </div>
                                </div>
                                
                                </div>
                                
                                
                                
                              
                                
        </div>
  </section>

</section>


<!-- /.container --> 

<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>

<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
 <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>

  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
    
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 




<script type="text/javascript">
$(document).ready(function(){
	
  //get_booking_hotel_records();
	
   $( "#datepicker11,#datepicker111" ).datepicker({
	   onClose: function( selectedDate ) {
		$( "#datepicker22" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker22' ).focus();
		
		$( "#datepicker222" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker222' ).focus();
	}
	   });
	   
   $( "#datepicker22, #datepicker222" ).datepicker();
   $( "#datepicker3" ).datepicker({  maxDate: new Date() });
   
  
    $("#by_user").on("change", function(){
		 if($(this).val() == 'ByStaff'){
			 $('#b2b2b').css('display', 'none');
			 $('#staff').css('display', 'inherit');
		 }else if($(this).val() == 'BySubAgents'){
			  $('#b2b2b').css('display', 'inherit');
			 $('#staff').css('display', 'none');
		 }
		
	});
	
	$("#t_by_user").on("change", function(){
		 if($(this).val() == 'ByStaff'){
			 $('#t_b2b2b').css('display', 'none');
			 $('#t_staff').css('display', 'inherit');
		 }else if($(this).val() == 'BySubAgents'){
			  $('#t_b2b2b').css('display', 'inherit');
			 $('#t_staff').css('display', 'none');
		 }
		
	});
	  
	  
	  $("#by_type").on("change", function(){
		 if($(this).val() == 'Booking'){
			 $('#payment_status').css('display', 'none');
			 $('#booking_status').css('display', 'inherit');
		 }else if($(this).val() == 'payment'){
			  $('#payment_status').css('display', 'inherit');
			 $('#booking_status').css('display', 'none');
		 }
		
	});
  
	
	$('#hotel_books').click(function(){
		$('#BookingList').fadeOut(500, function(){
				$('#hotel_show').fadeIn();
				get_booking_hotel_records();
			});
	}); 
	
    $('#flights_books').click(function(){
	
             $('#BookingList').fadeOut(500, function(){
				$('#flight_show').fadeIn();
				get_flight_list();
			});;
	});
	
	$('#transfer_books').click(function(){
		$('#BookingList').fadeOut(500, function(){
				$('#transfer_show').fadeIn();
				$("#booking_transfer_record").css("display","inherit");
				get_transfer_list();
			});
	}); 
	
 
	
	$('#back_list').click(function(){
		$('#hotel_show').fadeOut(500, function(){
				$('#BookingList').fadeIn();
			});
	});
	
	
	$('#flight_back_list').click(function(){
		$('#flight_show').fadeOut(500, function(){
				$('#BookingList').fadeIn();
			});
	});
	
	
	
	$('#trans_back_list').click(function(){
		$('#transfer_show').fadeOut(500, function(){
				$('#BookingList').fadeIn();
				$("#booking_transfer_record").css("display","none");
			});
	});

	
	$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
	});
	
	
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	
});




$("#owl-demobaners1").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	  $("#owl-demobaners2").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	   $("#owl-demobaners3").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });


$("#owl-demo3").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });

$("#owl-demo4").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });
	  
	  $("#owl-demo5").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });


$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});
var Script = function () {


//    sidebar dropdown menu auto scrolling





//    sidebar toggle

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                //$('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });
	
/*	$('.fa-bars').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });*/




// widget tools

    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });




}();


//Prevent mousewheel on hover of sidebar
/*$( '.top_dash' ).bind( 'mousewheel DOMMouseScroll', function ( e ) {
    var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;

    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
    e.preventDefault();
});*/


</script>

<script>
$(document).ready(function(){
		
		$('#editquestion').click(function(){
			$('.fullquestionswrp').slideToggle(500);
		});
		
		$('#editprivatepub').click(function(){
			$('.fullquestionswrpshare').slideToggle(500);
		});
		
		$('#smsalert').click(function(){
			$('.fullquestionswrp2').slideToggle(500);
		});
		
		$('#changepaswrd').click(function(){
			$('.fullquestionswrp3').slideToggle(500);
		});

        $('#addMarkUp').click(function() {
            $('.fullquestionswrp5').slideToggle(500);
        })
			
	});
	

	</script>
	<script >
		
		 
	  function get_booking_hotel_filter_result(){
		  
		   if($("#reference_type").val() != ''){
			 
			 if($("#reference_no").val() == ''){
				 $("#reference-no-error").html("Please enter the reference number");
				 return false;
			 }else{
				  $("#reference-no-error").html("");
			 }
		  }
		  
		  if($("#reference_no").val() != ''){
			 
			 if($("#reference_type").val() == ''){
				 $("#reference-type-error").html("Please enter the reference type");
				 return false;
			 }else{
				  $("#reference-type-error").html("");
			 }
		  }
		 
		
		 if($("#by_booking").val() != ''){
			 
			 if($("#datepicker11").val() == ''){
				 $("#start-type-error").html("Please select start date");
				 return false;
			 }else{
				 $("#start-type-error").html("");
			 }
			 
			 if($("#datepicker22").val() == ''){
				 $("#end-type-error").html("Please select end date");
				 return false;
			 }else{
				 $("#end-type-error").html("");
			 }
		 }
		 
		 if($("#datepicker11").val() != '' || $("#datepicker22").val() != '' ){
			 if($("#by_booking").val() == ''){
				 $("#date-type-error").html("Please select date type");
				 return false;
			 }else{
				 $("#date-type-error").html(""); 
			 }
			 
		 }
		 
		 if($("#datepicker11").val() != '' || $("#datepicker22").val() != '' ){
			 if($("#by_booking").val() == ''){
				 $("#date-type-error").html("Please select date type");
				 return false;
			 }else{
				 $("#date-type-error").html(""); 
			 }
			 
		 }
		 
		 if($("#staff_id").val() != 0  ){
			 if($("#by_user").val() == ''){
				   $("#user-type-error").html("Please select user type");
			 }else{
				 $("#user-type-error").html("");
			 }
		 }
		 
		 if($("#by_type").val() != 0 ){
			 if($("#status").val() == ''){
				   $("#by-status-error").html("Please select status");
			 }else{
				 $("#by-status-error").html("");
			 }
		 }
		 
		 if($("#status").val() != 0 ){
			 if($("#by_type").val() == ''){
				   $("#by-type-error").html("Please select status type");
			 }else{
				 $("#by-type-error").html("");
			 }
		 }
		
		 $.ajax({
			type: "POST",
			url: $("#bookins_hotel").attr('action'),
			data: $("#bookins_hotel").serialize(),
			dataType: "json",
			success: function(data){
			  	$("#booking_hotel_record").html(data.hotel_bookings); 
				return false;
			}
		});
		 
	 }
	 
	 
	 function get_booking_transfer_filter_result(){
		  
		   if($("#t_reference_type").val() != ''){
			 
			 if($("#t_reference_no").val() == ''){
				 $("#t_reference-no-error").html("Please enter the reference number");
				 return false;
			 }else{
				  $("#t_reference-no-error").html("");
			 }
		  }
		  
		  if($("#t_reference_no").val() != ''){
			 
			 if($("#t_reference_type").val() == ''){
				 $("#t_reference-type-error").html("Please enter the reference type");
				 return false;
			 }else{
				  $("#t_reference-type-error").html("");
			 }
		  }
		 
		
		 if($("#t_by_booking").val() != ''){
			 
			 if($("#datepicker111").val() == ''){
				 $("#t_start-type-error").html("Please select start date");
				 return false;
			 }else{
				 $("#t_start-type-error").html("");
			 }
			 
			 if($("#datepicker222").val() == ''){
				 $("#t_end-type-error").html("Please select end date");
				 return false;
			 }else{
				 $("#t_end-type-error").html("");
			 }
		 }
		 
		 if($("#datepicker111").val() != '' || $("#datepicker222").val() != '' ){
			 if($("#t_by_booking").val() == ''){
				 $("#t_date-type-error").html("Please select date type");
				 return false;
			 }else{
				 $("#t_date-type-error").html(""); 
			 }
			 
		 }
		 
		 if($("#datepicker111").val() != '' || $("#datepicker222").val() != '' ){
			 if($("#t_by_booking").val() == ''){
				 $("#t_date-type-error").html("Please select date type");
				 return false;
			 }else{
				 $("#t_date-type-error").html(""); 
			 }
			 
		 }
		 
		 if($("#t_staff_id").val() != 0  ){
			 if($("#by_user").val() == ''){
				   $("#t_user-type-error").html("Please select user type");
			 }else{
				 $("#t_user-type-error").html("");
			 }
		 }
		 
		 if($("#t_by_type").val() != 0 ){
			 if($("#status").val() == ''){
				   $("#t_by-status-error").html("Please select status");
			 }else{
				 $("#t_by-status-error").html("");
			 }
		 }
		 
		 if($("#t_status").val() != 0 ){
			 if($("#t_by_type").val() == ''){
				   $("#t_by-type-error").html("Please select status type");
			 }else{
				 $("#bt_y-type-error").html("");
			 }
		 }
		
		 $.ajax({
			type: "POST",
			url: $("#bookins_transfer").attr('action'),
			data: $("#bookins_transfer").serialize(),
			dataType: "json",
			success: function(data){
			  	$("#booking_transfer_record").html(data.transfer_bookings); 
				return false;
			}
		});
		 
	 }
	 
		
	 function  get_booking_hotel_records(){
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().'bookings/get_hotel_bookings' ?>",
			dataType: "json",
			success: function(data){
					$("#booking_hotel_record").html(data.hotel_bookings); 
				return false;
			}
		}); 
     }
     
     function get_transfer_list(){
		 $.ajax({
			type: 'POST',
			url: "<?php echo base_url().'bookings/get_transfer_bookings' ?>",
			dataType: "json",
			success: function(data){
					$("#booking_transfer_record").html(data.transfer_bookings); 
				return false;
			}
		}); 
	 }
	 
	  function get_flight_list(){
		 $.ajax({
			type: 'POST',
			url: "<?php echo base_url().'bookings/get_flight_bookings' ?>",
			dataType: "json",
			success: function(data){
				
		     	$("#booking_flight_record").html(data.transfer_bookings); 
					return false;
			},
			error:function(){
				alert('error');
			}
		}); 
	 }
     
 
	 
	</script>


  
</body>
</html>
