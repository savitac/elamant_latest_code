<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="">
<meta name="author" content="">
<title>Hotel Vocher</title>
<?php echo $this->load->view('core/load_css'); ?>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>

<link href="<?php echo ASSETS;?>assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>assets/css/jquery_ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/animation.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSETS;?>assets/css/voucher.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Navigation -->

<?php echo $this->load->view('dashboard/top'); ?> 
<?php echo $this->load->view('dashboard/left_menu'); ?>

</head>
<body>
<!-- Navigation --> 


<section id="main-content">
  
  <section class="wrapper">
  <div class="main-chart">


  <div class="full marintopcnt whitebg">
    
      <div class="col-md-12 nopad">
          <table cellspacing="0" cellpadding="0" border="0" align="center" style="width:100%; color: #444; border: 1px solid #dddddd; padding: 15px; background:#fff;"> 
            <tbody>
              <tr>
              <td style="padding: 20px">
                
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                    <tr>
                        <td><table class="insideone">
                            <tbody><tr>
                              <td class="thrty first" style="width:33.33%"><div class="logovcr"> <img alt="" src="<?php echo ASSETS;?>assets/images/logo.png" style="max-width:150px;"> </div></td>
                              <td class="thrty second" style="width:33.33%"><span class="faprnt fa fa-print"  ></span></td>
                              <td class="thrty third" style="width:33.33%"><div class="adrssvr"> <br>
                                  <br>
                                </div></td>
                            </tr>
                          </tbody></table></td>
                      </tr>
                      <tr>
                        <td><div class="bighedingv" style=" color: #444444;display: block;font-size: 20px;overflow: hidden;padding: 10px 0;text-align: center;">Confirmation Letter</div></td>
                      </tr>
                      <tr>
                        <td style="height:20px;width:100%;"></td>
                      </tr>
                    <tr>
                      <td style="border:0px;" colspan="2"><table width="100%" cellspacing="0" cellpadding="8" border="0" align="center">
                          <tbody>
                           
                            <tr>
                              <td align="center"><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                  <tbody>
                                    <tr>
                                      <td width="50%" valign="top" align="left"><table width="100%" cellspacing="1" cellpadding="7" border="0" bgcolor="#FFFFFF">
                                          <tbody>
                                            <tr>
                                              <td align="left" style="color: #666;padding-bottom: 10px" colspan="2"><strong style="font-size:18px;"> <?php echo $orders->leadpax;?></strong></td>
                                            </tr>
                                            <tr>
                                              <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Booking Status  :</td>
                                              <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $orders->booking_status; ?></strong></td>
                                            </tr>
                                            <tr>
                                              <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Pnr No  :</td>
                                              <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $orders->pnr_no; ?></strong></td>
                                            </tr>
                                             <tr>
                                              <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Booking Reference  :</td>
                                              <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $orders->parent_pnr_no; ?></strong></td>
                                            </tr>
                                            <tr>
                                              <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666">Your Reference  :</td>
                                              <td width="50%" align="left" style="font-size: 15px; line-height: 28px; color: #666"><strong><?php echo $orders->pnr_no; ?></strong></td>
                                            </tr>
                                          </tbody>
                                        </table></td>
                                      <td width="50%" valign="top" align="left"><table width="100%" cellspacing="1" cellpadding="7" border="0" bgcolor="#FFFFFF">
                                          <tbody>
                                            <tr>
                                              <td align="left" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td width="50%" align="left">&nbsp;</td>
                                              <td width="50%" align="left">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="left">&nbsp;</td>
                                              <td align="left">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr>
                              <td style="height:20px;width:100%;"></td>
                            </tr>
                            <?php //echo '<pre>'; print_r($passanger); exit();?>
                            <tr>
                              <td bgcolor="#ffffff" align="center" style="border: 1px solid #cccccc; display: -moz-grid;overflow: auto;width: 100%;"><table width="100%" cellspacing="0" cellpadding="7" border="0" bgcolor="#FFFFFF">
                                  <tbody>
                                    <tr>
                                       <td bgcolor="#f1f1f1" style="border-bottom: 1px solid #cccccc;" colspan="4">
                                          <span style="display: block; font-size: 18px;padding:5px 10px"> <?php echo $passanger[0]->hotel_name.", ".$passanger[0]->city;?>  </span>
                                          <span style="display: block; font-size: 13px;padding:5px 10px; "> 
                                              <?php echo $passanger[0]->star;?> - <?php echo $passanger[0]->star;?> STARS
                                              <div data-star="<?php echo $booking->star; ?>" class="stra_hotel">
                                                    
                                                    
                                                    <img src="http://192.168.0.129/Kouaou/assets/theme_dark/images/four.png">
                                            </div>
                                            <?php echo $passanger[0]->hotel_address;?>, <?php echo $passanger[0]->city;?> <br /> Tel: <?php 
                                            if($booking->hotel_contact != "") {
                                                $contact = $booking->hotel_contact;
                                                $contact = explode('|', $contact);
                                                $contact = implode(', ', $contact);
                                                echo $contact;
                                            } else {
                                                echo "Not Available";
                                            }
                                        ?>
                                        </span>
                                        </td>
                                    </tr>
                                    <tr>
                                      <td width="15%" style="border-right: 1px solid #cccccc; padding: 10px" rowspan="2">
                                          <span style="width:100%"> <img width="170px" src="http://photos.hotelbeds.com/giata/08/089250/089250a_hb_w_003.jpg"> </span>
                                          <br><br>
                                          <span style="font-size: 13px;">Room type: <strong><?php echo $hotel_data->room_type;?> <?php echo ' '.$sb_string; ?><br></strong></span>
                                        </td>
                                      <td width="25%" bgcolor="#FFFFFF" style="border-bottom:1px solid #cccccc; border-right:1px solid #cccccc; padding: 10px"><span style="font-size:16px; font-weight:bold;">Check-in</span><br><?php echo date('D, dS M Y', strtotime($hotel_data->checkin));?></td>
                                      <td width="25%" bgcolor="#FFFFFF" style="padding: 10px;border-bottom:1px solid #cccccc; border-right:1px solid #cccccc;"><span style="font-size:16px; font-weight:bold;">Check-out</span><br><?php echo date('D, dS M Y', strtotime($hotel_data->checkout));?></td>
                                      <td width="35%" bgcolor="#FFFFFF" style="padding: 10px" rowspan="2"><strong>Guests</strong> <br>
                                        <br>
                                       Adult : <?php echo $hotel_data->adult; ?>                                      <br>
                                       Children : <?php echo $hotel_data->child; ?></td>
                                    </tr>
                                    <tr><?php //echo $hotel_data->room_data; ?>
                                      <td bgcolor="#FFFFFF" style="border-right:1px solid #cccccc;padding: 10px">No of night(s)<br>
                                        <span style="font-size:13px; font-weight:bold;">1</span></td>
                                      <td bgcolor="#FFFFFF" style="border-right:1px solid #cccccc; padding: 10px">Room(s)<br>
                                        <span style="font-size:13px; font-weight:bold;"><?php echo $hotel_data->room_count; ?></span></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr>
                              <td style="height:10px;width:100%;"></td>
                            </tr>     
                            <tr>
                              <td style="height:10px;width:100%;"></td>
                            </tr>    
                                                        <tr>
                              <td><table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                  <tbody>
                                    <tr>
                                      <td colspan="5"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin-bottom: 15px;padding: 10px 0;">Traveller Details</span></td>
                                    </tr>
                                    
                                    <tr>
                                        <th valign="top" align="left" colspan="4" style="padding: 20px 0; font-size:16px; color:#333;"> Room 1 ( <?php echo $hotel_data->hotel_name; ?>- ROOM ONLY ) </th>
                                      </tr>
                                      <tr style="background:#f2f2f2; border: 1px solid #eeeeee;">
                                        <th valign="top" align="left" style="padding: 10px"><strong>Passenger Type</strong></th>
                                        <th valign="top" align="left" style="padding: 10px"><strong>First Name </strong></th>
                                        <th valign="top" align="left" style="padding: 10px"><strong>Last Name </strong></th>
                                        <th valign="top" align="left" style="padding: 10px"><strong>Age </strong></th>
                                      </tr>
                                      <?php 
                                      $name = json_decode($hotel_data->TravelerDetails);
                                       
                                       ?>
                                      <tr style="background:#ffffff; border: 1px solid #eeeeee;">
                                        <td style="padding: 10px">ADULT</td>
                                        <td style="padding: 10px"><?php echo $name->first_name; ?></td>
                                        <td style="padding: 10px"><?php echo $name->last_name; ?></td>
                                        <td style="padding: 10px"><?php echo  ""; ?></td>
                                       <!--  <td style="padding: 10px">30</td> -->
                                      </tr>
                                      <!-- <tr style="background:#ffffff; border: 1px solid #eeeeee;">
                                        <td style="padding: 10px">ADULT</td>
                                        <td style="padding: 10px">HGF</td>
                                        <td style="padding: 10px">FSDR</td>
                                        <td style="padding: 10px">30</td>
                                      </tr>
                                      <tr style="background:#ffffff; border: 1px solid #eeeeee;">
                                        <td style="padding: 10px">CHILD</td>
                                        <td style="padding: 10px">EW</td>
                                        <td style="padding: 10px">FSD</td>
                                        <td style="padding: 10px">14</td>
                                      </tr> -->
                                    
                                    
                                                                     </tbody>
                                </table></td>
                            </tr>
                            <tr>
                              <td style="height:10px;width:100%;"></td>
                            </tr>
                            <tr>
                              <td><table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                  <tbody>
                                    <tr>
                                      <td colspan="2"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;">Customer Details</span></td>
                                    </tr>
                                    <tr style="border: 1px solid #eee;">
                                      <td width="20%" align="left" style="background:#f1f1f1;padding: 10px"><strong>Email ID</strong></td>
                                      <td width="80%" align="left" style="background:#ffffff;padding: 10px"><?php echo $orders->billing_email; ?> </td>
                                    </tr>
                                    <tr style="border: 1px solid #eee;">
                                      <td align="left" style="background:#f1f1f1;padding: 10px"><strong>Mobile Number</strong></td>
                                      <td align="left" style="background:#ffffff;padding: 10px"><?php echo $orders->billing_contact_number; ?></td>
                                    </tr>
                                    <tr style="border: 1px solid #eee;">
                                      <td align="left" style="background:#f1f1f1;padding: 10px"><strong>Address</strong></td>
                                      <td align="left" style="background:#ffffff;padding: 10px"><?php echo $orders->billing_address; ?> </td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr>
                              <td><table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                                  <tbody>
                                    <tr>
                                      <td colspan="2"><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;">Hotel Details</span></td>
                                    </tr>
                                    <tr style="border: 1px solid #eee;">
                                      <td width="20%" align="left" style="background:#f1f1f1;padding: 10px"><strong>Name</strong></td>
                                      <td width="80%" align="left" style="background:#ffffff;padding: 10px"><?php echo $hotel_data->hotel_name; ?></td>
                                    </tr>
                                    <tr style="border: 1px solid #eee;">
                                      <td align="left" style="background:#f1f1f1;padding: 10px"><strong>Destination</strong></td>
                                      <td align="left" style="background:#ffffff;padding: 10px"><?php echo $hotel_data->city; ?></td>
                                    </tr>
                                    <tr style="border: 1px solid #eee;">
                                      <td align="left" style="background:#f1f1f1;padding: 10px"><strong>Address</strong></td>
                                      <td align="left" style="background:#ffffff;padding: 10px"><?php echo $hotel_data->hotel_address; ?> </td>
                                    </tr>
                                   
                                  </tbody>
                                </table></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                
                    <tr>
                      <td colspan="2"><table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                          <tbody>
                            <tr>
                              <td><span style="border-bottom: 1px solid #dddddd;display: block;font-size: 18px;margin: 15px 0;padding: 10px 0;">Cancellation Policy </span></td>
                            </tr>
                            <tr>
                              <td valign="top" align="left" style="padding:10px 0;"><span style="font-size: 14px;padding: 10px 0;"> <?php echo $passanger[0]->cancel_policy; ?></span></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                 
                  </tbody>
                </table>
              
              </td>
              </tr>
            </tbody>
          </table>
          </div>
      </div>
    
  </div>

  </section>

</section>
<!-- Page Content --> 




<!-- Script to Activate the Carousel -->
<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>
<script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>

  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 

 
<script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>

  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
<style>
.leftflitmg { max-width:70px !important } 
</style>



</body>
</html>
