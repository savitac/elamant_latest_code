<div class="table-responsive">
 <table id="example1" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
                            <th>Sno</th>
							<th>Trip Type</th>
							<th>From</th>
							<th>To</th>						
						    <th>Start</th>
						    <th>Return</th>
						    <th>Class</th>
                            <th>Status</th>
                            <th>Alloted</th>
                           	<th>Avail</th>
                           	<th>Booked</th>
                           	<th>Hold Seats</th>
                           	<th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
            				    <th>Sno</th>
							<th>Trip Type</th>
							<th>From</th>
							<th>To</th>						
						    <th>Start</th>
						    <th>Return</th>
						    <th>Class</th>
                            <th>Status</th>
                            <th>Alloted</th>
                           	<th>Avail</th>
                           	<th>Booked</th>
                           	<th>Hold Seats</th>
                           	<th>Action</th>
            </tr>
        </tfoot>
        <tbody >
           <?php for($a=0;$a<count($flight_details);$a++){ ?>
						<tr>
							<td><?php echo ($a+1); ?></td>
							<td><?php echo $flight_details[$a]->flight_trip; ?></td>
							<td><?php echo $flight_details[$a]->from_city; ?></td>
							<td><?php echo $flight_details[$a]->to_city; ?></td>
							<td><?php echo $flight_details[$a]->jourany_date; ?></td>
							<td><?php echo $flight_details[$a]->retuen_date; ?></td>
							<td><?php echo $flight_details[$a]->class; ?></td>
							<td><?php echo $flight_details[$a]->status; ?></td>
							<td><?php echo $flight_details[$a]->alotted; ?></td>
							<td><?php echo $flight_details[$a]->no_of_seats; ?></td>
							<td><?php echo $flight_details[$a]->booked_seats; ?></td>
							<td><?php echo $flight_details[$a]->hold_seats; ?></td>
                          	<td class="center">
								
								<a class="btn btn-primary btn-xs" onclick="editPrivilege('<?php echo  base_url()."usermanagement/getUserPrivileges/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>')"  > <?php echo $this->TravelLights['UserManagementTable']['Privilege']; ?>  </a>
							   
							   <a class="btn btn-danger btn-xs" onclick="editproduct('<?php echo  base_url()."usermanagement/getUserProducts/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>')"  > <?php echo $this->TravelLights['UserManagementTable']['ManageProducts']; ?> </a>
							
							
							 <a class="btn btn-info btn-xs" onclick="editAgent('<?php echo  base_url()."usermanagement/editAdminagent/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>')"  > <?php echo $this->TravelLights['UserManagementTable']['Edit']; ?> </a>
							 <?php if($agentusers[$a]->user_status == "ACTIVE"){ ?> 
								   <a class="btn btn-warning btn-xs" href="<?php echo  base_url()."usermanagement/inactive_b2b_users/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>" onclick="return confirm('Are you want Inactivate?')"> <?php echo $this->TravelLights['UserManagementTable']['InActive']; ?> </a>
							 <?php } else { ?>
								   <a class="btn btn-success btn-xs" href="<?php echo  base_url()."usermanagement/active_b2b_users/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>" onclick="return confirm('Are you want activate?')"> <?php echo $this->TravelLights['UserManagementTable']['Active']; ?> </a>
							 <?php } ?>	
							 <a href="<?php echo site_url()."usermanagement/depositList/".base64_encode(json_encode($agentusers[$a]->user_details_id)); ?>"><button type="button" class="btn btn-red tooltip-primary btn-sm" data-placement="top" data-toggle="tooltip" data-original-title="Agent Deposit"><i class="glyphicon glyphicon-tower"></i><?php echo $this->TravelLights['UserManagementTable']['SubAgentDeposit']; ?></button></a>				
							 </td>
						</tr>
		<?php } ?>	
        </tbody>
    </table>
    </div>
    <script>
    $(document).ready(function(){

    $('#example1').DataTable({responsive: true});
    
});

</script>
