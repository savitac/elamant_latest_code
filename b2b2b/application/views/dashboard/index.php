<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?= $this->session->userdata('company_name')?></title> 
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet">
	<link href="<?php echo ASSETS;?>assets/css/index.css" rel="stylesheet"> 
	<link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" />
	<script src="<?php echo ASSETS;?>assets/js/ion.rangeSlider.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
	<!-- <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  -->
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/backslider.js"></script> 
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/pax_count.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/datepicker.js"></script> 
</head> 
<body>
	<?php echo $this->load->view('dashboard/top'); ?>	
	<?php //echo $this->load->view('dashboard/left_menu'); ?>	
	<?php echo $this->load->view('dashboard/dashboard'); ?>

	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>

<script type="text/javascript">


(function($) {
	

  'use strict';

  $(document).on('show.bs.tab', '.nav-tabs-responsive [data-toggle="tab"]', function(e) {
    var $target = $(e.target);
    var $tabs = $target.closest('.nav-tabs-responsive');
    var $current = $target.closest('li');
    var $parent = $current.closest('li.dropdown');
		$current = $parent.length > 0 ? $parent : $current;
    var $next = $current.next();
    var $prev = $current.prev();
    var updateDropdownMenu = function($el, position){
      $el
      	.find('.dropdown-menu')
        .removeClass('pull-xs-left pull-xs-center pull-xs-right')
      	.addClass( 'pull-xs-' + position );
    };

    $tabs.find('>li').removeClass('next prev');
    $prev.addClass('prev');
    $next.addClass('next');
    
    updateDropdownMenu( $prev, 'left' );
    updateDropdownMenu( $current, 'center' );
    updateDropdownMenu( $next, 'right' );
  });

})(jQuery);


$(document).ready(function(){
	
	
	$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
	});
	
	 
	
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	
});




$("#owl-demobaners1").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	  $("#owl-demobaners2").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });
	  
	   $("#owl-demobaners3").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
      });


$("#owl-demo3").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });

$("#owl-demo4").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });
	  
	  $("#owl-demo5").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
      });


$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});
var Script = function () {



    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                //$('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });




    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });




}();



</script>

<script>
	$(document).ready(function(){
		
		$('#editquestion').click(function(){
			$('.fullquestionswrp').slideToggle(500);
		});
		
		$('#editprivatepub').click(function(){
			$('.fullquestionswrpshare').slideToggle(500);
		});
		
		$('#smsalert').click(function(){
			$('.fullquestionswrp2').slideToggle(500);
		});
		
		$('#changepaswrd').click(function(){
			$('.fullquestionswrp3').slideToggle(500);
		});

        $('#addMarkUp').click(function() {
            $('.fullquestionswrp5').slideToggle(500);
        })
		
		
		
		
			
	});
</script>


	<script type="text/javascript">
		var check_in = "<?php echo date('d-m-Y', strtotime('1 days'));?>";
		var check_out = "<?php echo date('d-m-Y', strtotime('2 days'));?>";
		var Infant_count =0;
		var child_count =0;
		var WEB_URL = "<?php echo ASSETS; ?>";
		var ASSETS = "<?php echo ASSETS; ?>assets/";
	</script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/input-number.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/general.js"></script>
	<?php echo $this->load->view('core/load_js'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#supersized-loader').addClass('hide');
	});
</script>
   
</body>
</html>
