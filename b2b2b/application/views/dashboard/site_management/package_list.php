
 <table id="package-list" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
                            <th>Image</th>
                            <th>Name</th>
							
                            <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
               				<th>Image</th>
                            <th>Name</th>
                            
                            <th>Action</th>
            </tr>
        </tfoot>
        <tbody >
           <?php for($a=0;$a<count($packages);$a++){ 
           	$path="assets/domain/".$packages[$a]->package_image;
           ?>
						<tr>
							<td><img src="<?php echo ASSETS;?><?php echo  $path; ?>" alt="Smiley face" height="42" width="42"></td>

                            <td><?php echo $packages[$a]->package_name; ?></td>
							
							<td class="center">
							 
								   <a class="btn btn-warning btn-xs" href="<?php echo  base_url()."site_management/inactive_banner/".base64_encode(json_encode($packages[$a]->id)); ?>" onclick="return confirm('Are you want Inactivate?')"> Delete </a>
							 
							
							 </td>
						</tr>
		<?php } ?>	
        </tbody>
    </table>
    <script>
    $(document).ready(function(){

        

    $('#package-list').DataTable({responsive:true});
    
});




</script>
