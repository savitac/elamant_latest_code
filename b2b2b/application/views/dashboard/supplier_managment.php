<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?= $this->session->userdata('company_name');?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet">
	<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	
	<?php echo $this->load->view('dashboard/top'); ?>	
     <?php $this->load->view('dashboard/left_menu'); ?>

<section id="main-content">
  
  <section class="wrapper">
	<div class="main-chart">
        <div class="tabbable customtab">
         <ul class="nav nav-tabs">
			      <li class="active"> <a data-toggle="tab" href="#create_flight">Create Flight </a> </li>
			      <!-- <li class=""> <a data-toggle="tab" href="#crs_flights">CRS Flights</a> </li>
			                        <li class=""> <a data-toggle="tab" href="#airlinelist">Airline List</a> </li>
			                        <li class=""> <a data-toggle="tab" href="#supplier_manage">Manage Booking </a> </li>
			                        <li class=""> <a data-toggle="tab" href="#supplier_sales_reports">Sales Reports</a> </li>
			                        <li class=""> <a data-toggle="tab" href="#supplier_flight_reports">Flight Report</a> </li> -->
                 
          </ul>     
          <div class="tab-content">
						 <div id="create_flight" class="tab-pane active"> 
                             <?php $this->load->view('dashboard/suppliers/create_flight'); ?>
                        </div>
			           <!-- <div id="crs_flights" class="tab-pane"> 
                             <?php $this->load->view('dashboard/suppliers/crs_flights'); ?>
                        </div>

                        <div id="airlinelist" class="tab-pane">
							<?php $this->load->view('dashboard/suppliers/airlinelist'); ?>
                        </div>
                        <div id="supplier_manage" class="tab-pane"> 
                             <?php $this->load->view('dashboard/suppliers/supplier_manage'); ?>
                        </div>
                       
                        <div id="supplier_sales_reports" class="tab-pane"> 
                             <?php $this->load->view('dashboard/suppliers/supplier_sales_reports'); ?>
                        </div>  
                        <div id="supplier_flight_reports" class="tab-pane"> 
                             <?php $this->load->view('dashboard/suppliers/supplier_flight_reports'); ?>
                        </div> -->
             </div>
                  
            </div>
        </div>
  </section>
</section>
<div class="clearfix"></div> 
<?php //echo $this->load->view('core/bottom_footer'); ?>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 
<script type="text/javascript">

<?php if(isset($class)) { ?> 
setTimeout(function(){
$('#b2bmarkup').trigger('click'); 
}, 300);
<?php } ?>

$(document).ready(function(){
   
  
   //~ $( "#datepicker" ).datepicker();
   //~ $( "#datepicker1" ).datepicker();
   //~ $( "#datepicker2" ).datepicker();
   //~ $( "#datepicker4" ).datepicker();
    //~ $( "#datepicker11" ).datepicker();
   //~ $( "#datepicker22" ).datepicker();
   //~ $( "#datepicker33" ).datepicker();
   //~ $( "#datepicker44" ).datepicker();
   //~ $( "#datepicker3" ).datepicker({  maxDate: new Date() });
   //~ 

           
$("#user_type").on('change',function(){
	if($(this).val()== 4){
			$("#company_show_hide").css('display','block');
		} else if($(this).val()== 5){
			$("#company_show_hide").css('display','none');
		}
});
   
$("#markup_type").on('change', function(){
	if($(this).val() == "SPECIFIC") {
		 $("#user_t").css("display", "inherit");
		 $("#agent_n").css("display", "inherit");
		 $("#country").css("display", "inherit");
		 $("#date_range").css("display", "inherit");
		 $("#expry_date").css("display", "inherit");
	}else{
		$("#user_t").css("display", "none");
		$("#agent_n").css("display", "none");
		$("#country").css("display", "none");
		$("#date_range").css("display", "none");
		$("#expry_date").css("display", "none");
	}
});
		
$('#creat_click').click(function(){
	$('#crate_agent').fadeOut(500, function(){
			$('#crate_agent_form').fadeIn();
	});
});
	
	
		
$('#creat_click_user').click(function(){
		//$('#crate_agent_user').fadeOut(500, function(){
			//	$('#crate_agent_form_user').fadeIn();
		//	});
});
	
	
$('#creat_deposit').click(function(){
	$('#crate_deposit').fadeOut(500, function(){
			$('#crate_deposit_form').fadeIn();
	});
});
	
$('#creat_markup').click(function(){
	$('#crate_markup_list').fadeOut(500, function(){
		$('#crate_markup_form').fadeIn();
	});
});
	
$('#back_list').click(function(){
	$('#crate_agent_form').fadeOut(500, function(){
			$('#crate_agent').fadeIn();
	});
});
	
$('#back_list_user').click(function(){
		$('#crate_agent_form_user').fadeOut(500, function(){
			$('#crate_agent_user').fadeIn();
		});
});
	
$('#back_list_deposit').click(function(){
	$('#crate_deposit_form').fadeOut(500, function(){
	     $('#crate_deposit').fadeIn();
	});
});
	
$('#back_list_markup').click(function(){
	$('#crate_markup_form').fadeOut(500, function(){
	     $('#crate_markup_list').fadeIn();
	 });
});
	
$('#back_edit_list_markup').click(function(){
	$('#edit_markup_form').fadeOut(500, function(){
	    $('#crate_markup_list').fadeIn();
	});
});
	
$('#back_list_edit_agent').click(function(){
	$('#edit_agent_form').fadeOut(500, function(){
			$('#crate_agent').fadeIn();
		});
});
	
	
$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
});
	
	
$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
});

$("#owl-demobaners1").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
 });
	  
$("#owl-demobaners2").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
});
	  
$("#owl-demobaners3").owlCarousel({
		items : 1, 
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1], 
		itemsTablet: [768,1], 
		itemsMobile : [479,1], 
        navigation : false,
		pagination : false,
		autoplay : true,
});


$("#owl-demo3").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
});

$("#owl-demo4").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
});
	  
$("#owl-demo5").owlCarousel({
		items : 4, 
		itemsDesktop : [1000,3],
		itemsDesktopSmall : [900,3], 
		itemsTablet: [768,2], 
		itemsMobile : [479,1], 
        navigation : true,
		pagination : true
});


$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});
var Script = function () {

//    sidebar dropdown menu auto scrolling

//    sidebar toggle

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                //$('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });


    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });




}();


</script>

<script>
$(document).ready(function(){
	get_crs_flights();
	getuserList();
    //get_list_b2b2b();
    
		$('#editquestion').click(function(){
			$('.fullquestionswrp').slideToggle(500);
		});
		
		$('#editprivatepub').click(function(){
			$('.fullquestionswrpshare').slideToggle(500);
		});
		
		$('#smsalert').click(function(){
			$('.fullquestionswrp2').slideToggle(500);
		});
		
		$('#changepaswrd').click(function(){
			$('.fullquestionswrp3').slideToggle(500);
		});

        $('#addMarkUp').click(function() {
            $('.fullquestionswrp5').slideToggle(500);
        })
			
	});
	
	function get_crs_flights(){
		// calling suppliermenu/crs_flights
		$.ajax({
			type: "POST",
			url: "<?php echo base_url().'index.php/suppliermenu/crs_flights'; ?>",
			dataType: "json",
			success: function(data){
				$("#crs_flightsData").html(data.agent_list);
			}
		});
	}
	
	
	//getting usertype 5 list
	function getuserList(){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url().'usermanagement/getuserList'; ?>",
			dataType: "json",
			success: function(data){
				//lert(data);
				$("#agentData_user").html(data.agent_list);
			}
		});
	}
	
	
	
	function editAgent(url){
		
		$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#crate_agent').fadeOut(500, function(){
				$('#edit_agent_form').fadeIn();
			});
				$("#edit_agent_form").css('display','inherit');
				$("#edit_form").html(data.editAgent);
				
				return false;
			}
		});
	}

	
	function editUser(url){
		$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				    $('#agentData_user').fadeOut(500, function(){
					$('span#user_head_line').text('Update User');
					$('#back_list_edit_agent_uuu').css('display','block');
				    $('#edit_user_form').fadeIn();
			});
			$('#user_edit_div').html(data.editAgent);
				
				return false;
			}
		});
	}
	
	$('#back_list_edit_agent_uuu').click(function(){
		$('#edit_user_form').fadeOut(500, function(){
				$('span#user_head_line').text('Users List');
				$('#agentData_user').fadeIn();
			});
	});
	

    function editMarkup(url){
		
		$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#crate_markup_list').fadeOut(500, function(){
				$('#edit_markup_form').fadeIn();
			});
				$("#edit_markup_form").css('display','inherit');
				$("#editmark").html(data.updateMarkup);
				
				return false;
			}
		});
	}
	
	
	function editPrivilege(url){

	$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#crate_agent').fadeOut(500, function(){
				$('#crate_privilege_form').fadeIn();
			});
				$("#crate_privilege_form").css('display','inherit');
				$("#agent_prvilege").html(data.privilege_template);
				
				return false;
			}
		});
}

function editproduct(url){

	$.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			success: function(data){
				$('#crate_agent').fadeOut(500, function(){
				$('#crate_product_form').fadeIn();
			});
				$("#crate_product_form").css('display','inherit');
				$("#agent_products").html(data.product_template);
				
				return false;
			}
		});
}

    $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                  .columns.adjust()
                  .responsive.recalc();
            });

	</script>



</body>
</html>
