
                      
 <table id="example-roll" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
                            <th><?php echo $this->TravelLights['StaffManagementForm']['SNo']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['RoleName']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Status']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Action']; ?></th>
						
						
            </tr>
        </thead>
        <tfoot>
            <tr>
                            <th><?php echo $this->TravelLights['StaffManagementForm']['SNo']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['RoleName']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Status']; ?></th>
							<th><?php echo $this->TravelLights['StaffManagementForm']['Action']; ?></th>
						
            </tr>
        </tfoot>
        <tbody >
           <?php   for($a=0;$a<count($roles);$a++){ ?>
						<tr>
							<td><?php echo ($a+1); ?></td>
							<td><?php echo $roles[$a]->agent_roles_name; ?></td>
							<td><?php  if($roles[$a]->role_status == 1) { echo $this->TravelLights['roleManagement']['Active']; }else { echo $this->TravelLights['roleManagement']['InActive']; } ?></td>
							
							
							<td><?php  if($roles[$a]->role_status == 1) { ?>
								 <a class="btn btn-warning btn-xs" href="<?php echo  base_url()."staffmanagement/inactive_roles/".base64_encode(json_encode($roles[$a]->agent_roles_id)); ?>" onclick="return confirm('Are you want Inactivate?')"><?php echo $this->TravelLights['roleManagement']['InActive']; ?> </a>
								
								<?php }else {  ?>
									 <a class="btn btn-success btn-xs" href="<?php echo  base_url()."staffmanagement/active_rolese/".base64_encode(json_encode($roles[$a]->agent_roles_id)); ?>" onclick="return confirm('Are you want activate?')"> <?php echo $this->TravelLights['roleManagement']['Active']; ?> </a>
									
									<?php  } ?>
									
									<a class="btn btn-primary btn-xs" onclick="editPrivilege('<?php echo  base_url()."staffmanagement/getUserPrivileges/".base64_encode(json_encode($roles[$a]->agent_roles_id)); ?>')"  > <?php echo $this->TravelLights['StaffManagementForm']['ManagePrivileges']; ?>  </a>
									
<!--
									<a class="btn btn-info btn-xs" onclick="editAgent('<?php echo  ASSETS."staffmanagement/edit_roles/".base64_encode(json_encode($roles[$a]->agent_roles_id)); ?>')"  > Edit </a>
									
-->
<!--
									<a class="btn btn-default btn-xs" onclick="editAgent('<?php echo  ASSETS."staffmanagement/edit_roles/".base64_encode(json_encode($roles[$a]->agent_roles_id)); ?>')"  > Privileges </a>
-->
						</td>
						
						</tr>
		<?php } ?>	
        </tbody>
    </table>
    <script>
    $(document).ready(function(){

    $('#example-roll').DataTable({responsive:true});
    
});



</script>
