   	<div class="intabs">
                            	<div id="crate_markup_list">
                                <span class="profile_head adclr"><?php echo $this->TravelLights['UserManagement']['AgentMarkupManagement']; ?></span>
                                
                                <div class="withedrow">
                                	<div class="rowit">
                                    <a class="rgtsambtn" id="creat_markup"><?php echo $this->TravelLights['UserManagement']['AddMarkup']; ?></a>
                                    <div class="clearfix"></div>
    
      <table id="example-data" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
				<th><?php echo $this->TravelLights['AcencySettings']['SNo']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupType']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Product']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['SubAgents']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Country']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['StartDate']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['EndDate']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['ExpiryDate']; ?></th>
                <th>Expiry Date End</th>
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupValueType']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupValue']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Status']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Action']; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
				        <th><?php echo $this->TravelLights['AcencySettings']['SNo']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupType']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Product']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['SubAgents']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Country']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['StartDate']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['EndDate']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['ExpiryDate']; ?></th>
                <th>Expiry Date End</th>
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupValueType']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['MarkupValue']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Status']; ?></th>
                <th><?php echo $this->TravelLights['AcencySettings']['Action']; ?></th>
            </tr>
        </tfoot>
        <tbody>
			<?php for($up=0; $up < count($agent_markups); $up++) { ?>
            <tr><?php //echo '<pre>'; print_r($agent_markups[$up]); ?>
				 <td><?php echo $sno = $up+1; ?> </td>
				 <td><?php echo $agent_markups[$up]->markup_type; ?></td>
				 <td><?php echo $agent_markups[$up]->product_name; ?></td>
				 <td><?php echo $agent_markups[$up]->user_name; ?></td>
				 <td><?php echo $agent_markups[$up]->country_name; ?></td>
				 <td><?php if($agent_markups[$up]->markup_start_date != '0000-00-00') { echo date('d-m-Y', strtotime($agent_markups[$up]->markup_start_date)); } ?></td>
				 <td><?php if($agent_markups[$up]->markup_end_date != '0000-00-00') { echo date('d-m-Y', strtotime($agent_markups[$up]->markup_end_date)); } ?></td>
				 <td><?php if($agent_markups[$up]->markup_expiry_date != '0000-00-00') { echo date('d-m-Y', strtotime($agent_markups[$up]->markup_expiry_date)); } ?></td>
         <td><?php if($agent_markups[$up]->markup_expiry_date_to != '0000-00-00') { echo date('d-m-Y', strtotime($agent_markups[$up]->markup_expiry_date_to)); } ?></td>
				 <td><?php echo $agent_markups[$up]->markup_value_type; ?></td>
				 <td><?php echo $agent_markups[$up]->markup_value; ?></td>
				  <td><?php if( $agent_markups[$up]->markup_status == 'ACTIVE'){
					   echo "ACTIVE";
					   }else{
						   echo "INACTIVE";
					   } ?></td>
				  <td>
            <?php //echo '<pre>'; print_r($agent_markups); ?>
					  <?php if( $agent_markups[$up]->markup_status == 'ACTIVE'){ ?>
					  <a  class="btn btn-warning btn-xs" href="<?php echo  base_url()."usermanagement/changeMarkupStatus/".base64_encode(json_encode($agent_markups[$up]->markup_details_id))."/".base64_encode(json_encode('INACTIVE'))."/".base64_encode(json_encode('YES')); ?>" onclick="return confirm('<?php echo $this->TravelLights['AcencySettings']['Areyouwantinactivate']; ?>')"> <?php echo $this->TravelLights['AcencySettings']['Inactive']; ?> </a>
					  <?php }else { ?>
						    <a  class="btn btn-success btn-xs" href="<?php echo  base_url()."usermanagement/changeMarkupStatus/".base64_encode(json_encode($agent_markups[$up]->markup_details_id))."/".base64_encode(json_encode('ACTIVE'))."/".base64_encode(json_encode('YES')); ?>" onclick="return confirm('<?php echo $this->TravelLights['AcencySettings']['Areyouwantactivate']; ?>')"> <?php echo $this->TravelLights['AcencySettings']['Active']; ?> </a>
						  <?php } ?>
<!--
						  <a class="btn btn-danger btn-xs" href="<?php echo  base_url()."usermanagement/deleteMarkup/".base64_encode(json_encode($agent_markups[$up]->markup_details_id)); ?>" onclick="return confirm('Are you want Delete?')"> Delete </a>
						  
-->
						  <a class="btn btn-info btn-xs" onclick='editMarkup("<?php echo  base_url()."usermanagement/editMarkup/".base64_encode(json_encode($agent_markups[$up]->markup_details_id)); ?>")'><?php echo $this->TravelLights['AcencySettings']['Edit']; ?>  </a>
			    
			     </td>
							   
			  </tr>
            <?php } ?>
           
        </tbody>
    </table>
             
                                    </div>
                                </div>
                                
                                </div>
                                
                                
                                <div id="crate_markup_form" style="display:none;">
                                	<span class="profile_head adclr"><?php echo $this->TravelLights['AcencySettings']['AddMarkup']; ?></span> 
               						 <div class="withedrow">
                    <div class="rowit">
                    <a class="rgtsambtn" id="back_list_markup"><?php echo $this->TravelLights['AcencySettings']['Backtolist']; ?></a>
                    <div class="clearfix"></div>
                      <div class="addtiktble">
                      <form id="add_markup" name="add_markup" class="validate form-horizontal" method="post" action="<?php echo base_url().'usermanagement/addMarkup'; ?>" novalidate>   
                       <span id="markup-already-available" class="error" for="markup-already-available"> </span>                 
                        <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['MarkupType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
									  <select name="markup_type" id="markup_type"class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
                                        <option value="GENERAL"><?php echo $this->TravelLights['AcencySettings']['GENERAL']; ?></option>
                                        <option value="SPECIFIC"><?php echo $this->TravelLights['AcencySettings']['SPECIFIC']; ?></option>
                                   </select>
                                    <span id="markup-type-error" class="error" for="markup_type"> </span>
                                  </div>
                            </div>
                        </div>
                        
                         <!-- <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['Api']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="api_id" value="api_id" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
									   <?php for($ap=0; $ap < count($api); $ap++) { ?> 
                                        <option value ="<?php echo $api[$ap]->api_details_id; ?>" > <?php echo $api[$ap]->api_name." (".$api[$ap]->product_name.")"; ?></option>
                                        <?php } ?>
                                </select>
                                 <span id="product-id-error" class="error" for="product_id"> </span>
                                 </div>
                            </div>
                        </div> -->
                       
                         <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['Product']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="product_id" value="product_id" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
									   <?php for($prod=0; $prod < count($products); $prod++) { ?> 
                                        <option value ="<?php echo $products[$prod]->product_details_id; ?>" > <?php echo $products[$prod]->product_name; ?></option>
                                        <?php } ?>
                                </select>
                                 <span id="product-id-error" class="error" for="product_id"> </span>
                                 </div>
                            </div>
                        </div>

                       
                          <div class="likrticktsec" id="user_t"  style="display:none;">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['UserType']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                  <select name="user_type" id="user_type"class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value=""><?php echo $this->TravelLights['EditB2BMarkup']['SelectUserType']; ?> </option>
									  <option value="4">B2B2B </option>
									  
                                </select>
                              </div>
                        </div>
                        
                        
                         <div class="likrticktsec" id="agent_n"  style="display:none;">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['AgentName']; ?></div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                  <select name="sub_agent_id" id="sub_agent_id"class="payinselect mySelectBoxClassfortab hasCustomSelect" >
									  <option value="0"><?php echo $this->TravelLights['EditB2BMarkup']['ALL']; ?> </option>
									  <?php if($agentadmin_list!=''){ for($a=0;$a<count($agentadmin_list);$a++){ ?>
                                        <option value="<?php echo $agentadmin_list[$a]->user_details_id; ?>"><?php echo $agentadmin_list[$a]->user_name; ?></option>
                                       <?php  } } ?>
                                </select>
                                 <span id="markup-type-error" class="error" for="sub_agent_id"> </span>
                                 </div>
                            </div>
                        </div>
                        
                        
                         <div class="likrticktsec" id="country">
                            <div class="cobldo"><?php echo $this->TravelLights['AcencySettings']['Country']; ?> </div>
                            <div class="coltcnt">
                            	<div class="selectedwrap inboxs">
                                <select name="country_code" id="country_code"  class="payinselect mySelectBoxClassfortab hasCustomSelect">
									   <option value=""> <?php echo $this->TravelLights['EditB2BMarkup']['SelectCountry']; ?> </option>
									   <?php for($country=0; $country < count($nationality_countries); $country++) { ?> 
                                        <option value ="<?php echo $nationality_countries[$country]->country_id; ?>" > <?php echo $nationality_countries[$country]->country_name; ?></option>
                                        <?php } ?>
                                </select>
                                 </div>
                            </div>
                        </div>
                        
                         <div class="likrticktsec" id="date_range">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['MarkupDateRange']; ?></div> 
								
								<div class="width80cnt">
                            	<div class="coltcnt">
                                <input type="text" readonly class="payinput cols-sm-2" name="start_date" id="datepicker11" placeholder="Markup Start Date" />
                                 </div>
                               
                                 <div class="coltcnt">
                                <input type="text" readonly class="payinput cols-sm-2" name="end_date" id="datepicker22" placeholder="Markup End Date"   />
                                 </div>
                                 </div>
                           
                        </div>
                       <div class="likrticktsec" id="expry_date">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['MarkupExpiryDate']; ?></div> 
                
                <div class="width80cnt">
                              <div class="coltcnt">
                                <input type="text" readonly class="payinput cols-sm-2" name="expiry_date" id="datepicker33" placeholder="Markup Start Date" />
                                 </div>
                               
                                 <div class="coltcnt">
                                <input type="text" readonly class="payinput cols-sm-2" name="expiry_date_to" id="datepicker44" placeholder="Markup End Date"   />
                                 </div>
                                 </div>
                           
                        </div>
                         <!-- <div class="likrticktsec" id="expry_date" style="display:none;">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['MarkupExpiryDate']; ?></div>
                            <div class="coltcnt">
                            	<input type="text" class="payinput cols-sm-2" readonly name="expiry_date" id="datepicker2" placeholder="<?php echo $this->TravelLights['EditB2BMarkup']['MarkupExpiryDate']; ?>"   />
                            </div>
                        </div> -->
                    
                        
                     
                     
                    </div>

                      <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['MarkupvalueType']; ?></div>
                            <div class="coltcnt">
                              <div class="selectedwrap inboxs">
                                <select name="markup_value_type" id="markup_value_type" class="payinselect mySelectBoxClassfortab hasCustomSelect" required>
                                        <option value="Percentage"><?php echo $this->TravelLights['AcencySettings']['PERCENTAGE']; ?></option>
                                        <option value="Fixed"><?php echo $this->TravelLights['AcencySettings']['FIXED']; ?></option>
                                </select>
                                 <span id="markup-type-error" class="error" for="markup_value_type"> </span>
                                 </div>
                            </div>
                        </div>
                    
                    
                    <div class="likrticktsec">
                            <div class="cobldo"><?php echo $this->TravelLights['EditB2BMarkup']['MarkupValue']; ?></div>
                            <div class="coltcnt">
                                <input type="number" class="payinput" name="markup_value" id="markup_value" min="0" id="markup_value"  required />
                                 <span id="markup-value-error" class="error" for="number"> </span>
                            </div>
                        </div>
                  
                     <div class="likrticktsec">
                            <div class="cobldo">&nbsp;</div>
                            <div class="coltcnt">
                            <input type="submit" value="<?php echo $this->TravelLights['AcencySettings']['Add']; ?>"  onclick="form_submits('add_markup')"  class="adddtickt">
                            </div>
                        </div>
                       </form>   
                    </div>
                </div>
              </div>
              
                      
           
            </div>
            
             <div id="edit_markup_form" style="display:none;">
                              <span class="profile_head adclr"><?php echo $this->TravelLights['EditB2BMarkup']['EditMarkup']; ?></span> 
               						 <div class="withedrow">
                    <div class="rowit">
                    <a class="rgtsambtn" id="back_edit_list_markup"><?php echo $this->TravelLights['AcencySettings']['Backtolist']; ?></a>
                    <div class="clearfix"></div>
                      <div class="addtiktble">
						  <div id="editmark">
						  
						  </div>
						  
                    </div>
                    </div>
                </div>
              </div>
              
<script>
$(document).ready(function(){
    $('#example-data').DataTable({responsive: true});
    
    
    $("#datepicker11").datepicker({
	numberOfMonths: 1,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: 1,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate()); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#datepicker22").datepicker("setDate", d1);
		$("#datepicker22").datepicker("option", "minDate", d1);
	},
	onClose: function( selectedDate ) {
		$( "#datepicker22" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker22' ).focus();
	}
	});
	
	$("#datepicker22").datepicker({
		numberOfMonths: 1,
		minDate: '-12Y',
		dateFormat: 'dd-mm-yy'
		
	});
	
	$("#datepicker33").datepicker({
	numberOfMonths: 1,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: 1,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate()); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#datepicker44").datepicker("setDate", d1);
		$("#datepicker44").datepicker("option", "minDate", d1);
	},
	onClose: function( selectedDate ) {
		$( "#datepicker44" ).datepicker( "option", "minDate", selectedDate );
		$( '#datepicker44' ).focus();
	}
	});
	
	$("#datepicker44").datepicker({
		numberOfMonths: 1,
		minDate: '-12Y',
		dateFormat: 'dd-mm-yy'
		
	});
  
});

</script>
           
             

