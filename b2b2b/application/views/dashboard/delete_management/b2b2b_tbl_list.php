
 <table id="example4" class="table table-striped table-bordered dt-responsive nowrap example" cellspacing="0" width="100%">
        <thead>
            <tr class="sortablehed">
                            <th><?php echo $this->TravelLights['UserManagementTable']['Slno']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['Name']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['UserType']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['Email']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['AccountNo']; ?></th>
						    <th><?php echo $this->TravelLights['UserManagementTable']['MobileNo']; ?></th>
						    <th><?php echo $this->TravelLights['UserManagementTable']['CompanyName']; ?></th>
                           <th><?php echo $this->TravelLights['UserManagementTable']['Status']; ?></th>
                            <th><?php echo $this->TravelLights['UserManagementTable']['Actions']; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
            				 <th><?php echo $this->TravelLights['UserManagementTable']['Slno']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['Name']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['UserType']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['Email']; ?></th>
							<th><?php echo $this->TravelLights['UserManagementTable']['AccountNo']; ?></th>
						    <th><?php echo $this->TravelLights['UserManagementTable']['MobileNo']; ?></th>
						    <th><?php echo $this->TravelLights['UserManagementTable']['CompanyName']; ?></th>
                           <th><?php echo $this->TravelLights['UserManagementTable']['Status']; ?></th>
                            <th><?php echo $this->TravelLights['UserManagementTable']['Actions']; ?></th>
            </tr>
        </tfoot>
        <tbody >
           <?php for($a=0;$a<count($users);$a++){ ?>
						<tr>
							<td><?php echo ($a+1); ?></td>
							<td><?php echo $users[$a]->user_name; ?></td>
							<td><?php echo "B2B2B"; ?></td>
							<td><?php echo $users[$a]->user_email; ?></td>
							<td><?php echo $users[$a]->user_account_number; ?></td>
							<td><?php echo $users[$a]->user_cell_phone; ?></td>
							<td><?php echo $users[$a]->company_name; ?></td>
                            <td>
								<?php if($users[$a]->user_status == "ACTIVE"){  
									 echo "ACTIVE"; 
								 }else {
									  echo "INACTIVE";
								 } ?>
							</td>
							<td class="center">
							 <a class="btn btn-info btn-xs" onclick="editAgent('<?php echo  base_url()."usermanagement/editAdminagent/".base64_encode(json_encode($users[$a]->user_details_id)); ?>')"  > <?php echo $this->TravelLights['UserManagementTable']['Edit']; ?> </a>
							 <?php if($users[$a]->user_status == "ACTIVE"){ ?> 
								   <a class="btn btn-warning btn-xs" href="<?php echo  base_url()."usermanagement/inactive_b2b_users/".base64_encode(json_encode($users[$a]->user_details_id)); ?>" onclick="return confirm('Are you want Inactivate?')"> <?php echo $this->TravelLights['UserManagementTable']['InActive']; ?> </a>
							 <?php } else { ?>
								   <a class="btn btn-success btn-xs" href="<?php echo  base_url()."usermanagement/active_b2b_users/".base64_encode(json_encode($users[$a]->user_details_id)); ?>" onclick="return confirm('Are you want activate?')"> <?php echo $this->TravelLights['UserManagementTable']['Active']; ?> </a>
							 <?php } ?>	
							
							 </td>
						</tr>
		<?php } ?>	
        </tbody>
    </table>
    <script>
    $(document).ready(function(){

    $('#example4').DataTable({responsive: true});
    
});

</script>
