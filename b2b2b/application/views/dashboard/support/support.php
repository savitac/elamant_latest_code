<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?= $this->session->userdata('company_name')?></title>
  <?php echo $this->load->view('core/load_css'); ?>
  <link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet">
  <link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
  
  <?php echo $this->load->view('dashboard/top'); ?> 


  
<section id="main-content">
  
  <section class="wrapper">
  <div class="main-chart">

        <div class="tabbable customtab padtopmore">
            
                    <ul class="nav nav-tabs">
                        <li class="active"> <a class="forrestab" data-toggle="tab" href="#sub_agent"><span class="fa fa-ticket" data-toggle="tooltip" data-placement="bottom" title="Inbox"></span><strong><?php echo $this->TravelLights['Support']['Inbox']; ?></strong></a> </li>
                        <li class=""> <a class="forrestab" data-toggle="tab" href="#depositmanage"><span class="fa fa-share-square-o" data-toggle="tooltip" data-placement="bottom" title="Send Tickets"></span><strong><?php echo $this->TravelLights['Support']['SendTickets']; ?></strong> </a> </li>
                        <li class=""> <a class="forrestab" data-toggle="tab" href="#b2bmarkup"><span class="fa fa-file-text-o" data-toggle="tooltip" data-placement="bottom" title="Closed Tickets"></span><strong><?php echo $this->TravelLights['Support']['ClosedTickets']; ?></strong> </a> </li>
                        <li class=""> <a class="forrestab" data-toggle="tab" href="#addticket"><span class="fa fa-plus-square" data-toggle="tooltip" data-placement="bottom" title="Add New Ticket"></span><strong><?php echo $this->TravelLights['Support']['AddNewTicket']; ?></strong> </a> </li>
                    </ul>
                    
                    <div class="tab-content">
                        
                        <div id="sub_agent" class="tab-pane active">
                           <?php $this->load->view('dashboard/support/inbox'); ?>
                        </div>
                        <div id="depositmanage" class="tab-pane ">
                        <?php $this->load->view('dashboard/support/send_ticket'); ?>
                            
                        </div>
                        <div id="b2bmarkup" class="tab-pane ">
                        <?php $this->load->view('dashboard/support/add_new_ticket'); ?>
                        </div>
                         <div class="clearfix"></div>
                        <div id="addticket" class="tab-pane "> 

                            <div class="intabs">
                                <div id="suportaddnew">
                                  <span class="profile_head adclr"><?php echo $this->TravelLights['Support']['AddNewTicket']; ?></span>
                                    <div class="withedrow">
                                    <div class="rowit">
                                    
                                    <div class="clearfix"></div>
                                    <div class="addtiktble">
                                <?=form_open('support/add_new_ticket',array('id' => 'supportadd','enctype' => 'multipart/form-data','class' => 'validate form-horizontal'))?>
                                  
                                  <div class="likrticktsec">
                                    <div class="cobldo"><?php echo $this->TravelLights['Support']['Subject']; ?></div>
                                    <div class="coltcnt">
                                        <div class="selectedwrap inboxs">
                                        <select class="payinselect mySelectBoxClassfortab hasCustomSelect" id="sub" name="sub" required>
                                                <option value=""><?php echo $this->TravelLights['Support']['SelectSubject']; ?></option>
                                      <?php print_r($support_ticket_subject); ?>
                                      <?php foreach ($support_ticket_subject as $key => $value) {?>
                                      <option value="<?php echo $value->support_ticket_subject_value ?>"><?php echo $value->support_ticket_subject_value ?></option>
                                      <?php } ?>
                                        </select>
                                         </div>
                                    </div>
                                </div>
                                
                                
                                 <div class="likrticktsec">
                                    <div class="cobldo"><?php echo $this->TravelLights['Support']['Attachment']; ?></div>
                                    <div class="coltcnt">
        
                                        <input type="file"  class="payinput" name="file_name"> 
                                    </div>
                                </div>  
                                  
                                   <div class="likrticktsec">
                                        <div class="cobldo"><?php echo $this->TravelLights['Support']['Message']; ?></div>
                                        <div class="coltcnt">
                                            <textarea class="tikttext" name="message" data-max="1500" rows='6' required></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="likrticktsec">
                                        <div class="cobldo">&nbsp;</div>
                                        <div class="coltcnt">
                                        <input type="submit" value="<?php echo $this->TravelLights['Support']['Add']; ?>" class="adddtickt">
                                        </div>
                                    </div>
                                  
                                 
                                </form>
                                </div>
                              </div>
                              </div>
                              </div>
                              </div>
                        </div>
                        
                  </div>
                  
            </div>
        
        </div>
  </section>

</section>
<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>



<div class="clearfix"></div>
  <?php //echo $this->load->view('core/bottom_footer'); ?>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 

    
<script type="text/javascript">
$(document).ready(function(){

      //$('#support_ticket_list_closed').DataTable();    

    //$('#support_ticket_list_sent').DataTable();
  
  //$('#support_ticket_list_inbox').DataTable();
$('[data-toggle="tooltip"]').tooltip();
  
  $('#edit_show').click(function(){
    $('#viewpro').fadeOut(500, function(){
        $('#edit_profil').fadeIn();
      });
  });
  
  
  
  
  
  
  $('#back_list').click(function(){
    $('#edit_profil').fadeOut(500, function(){
        $('#viewpro').fadeIn();
      });
  });
  
  $('.dash_menus').click(function(){
    $('.sidebar_wrap').toggleClass('open');
  });
  
  $('.wament').click(function(){
    $('.wament').removeClass('active');
    $(this).addClass('active');
  });
  
  
  $('.scrolltop').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
   });
  
});



$("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                  .columns.adjust()
                  .responsive.recalc();
       });

</script>


</body>
</html>
