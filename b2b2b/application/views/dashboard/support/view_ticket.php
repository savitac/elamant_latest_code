<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?= $this->session->userdata('company_name')?></title>
	<?php echo $this->load->view('core/load_css'); ?>
	<link href="<?php echo ASSETS;?>assets/css/dashboard.css" rel="stylesheet">
	<link href="<?php echo ASSETS;?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS;?>assets/css/responsive-dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body> 
	<?php echo $this->load->view('dashboard/top'); ?>	
<section id="main-content">
  
  <section class="wrapper">
	<div class="main-chart">
    
    
    <div id="crate_agent">
    <span class="profile_head"><?php echo $this->TravelLights['Support']['Message']; ?></span>
    
    <div class="withedrow">
        <div class="rowit">
        <!-- <a id="back_ticket_list" class="rgtsambtn">Back to list</a> -->
        <div class="clearfix"></div>
        	
            
            <?php //echo "<pre/>";print_r($ticket);die; ?>
            <?php foreach ($ticket as $key => $value) { ?>
            <div class="chatrow">
            	<div class="chaterimage">
               	 <img alt="" src="<?php echo $user_img_url ?>">
                </div>
              <div class="chaterdetail">
              
              <div class="insidechat">
                <?php $id = $this->session->userdata("user_details_id");
                    $agent = $this->Account_Model->get_user_details($id); ?>
                    
              <?php $user_namex = ($value->last_updated_by != "Admin") ? $agent['user_info'][0]->agent_name : "ADMIN"; ?>
              <?php //echo '<pre>'; print_r($user_namex); ?>
			  <?php $user_img_url = ($value->last_updated_by == "B2B")? $this->session->userdata('b2b_photo'): ASSETS.'assets/images/logo.png' 

              ?>
                <div class="chatername"><?php //echo strtoupper($user_namex) ?></div>
                <div class="chattime">
                    <span class="fa fa-clock-o"></span>
                    <?php //echo  date('Y-m-d H:i:s'); ?>
                    <?php echo $this->Support_Model->calculate_time_ago($value->last_update_time); ?>
                </div>
                <div class="chatermsg"> <?php echo $value->message;?> </div>
                <?php if($value->file_path != ''){

                  $file =   strtr(base64_encode($value->file_path),array('+' => '.','=' => '-','/' => '~'));

                  $a = explode('support', $value->file_path);

                  $name1 = substr($a[1],3);
                  if($name1 != ""){
                ?>
                <div class="chatattech"><p><a  href="<?php echo base_url(); ?>support/download_file/<?php echo $file; ?>"> <?php echo $this->TravelLights['Support']['DownloadTheAttchment']; ?> : <?php echo $name1; ?></a></p> </div>
                <?php } ?>
                <?php } ?>
                 
              </div>
              
             
              </div>
            </div>
            <?php } ?>
            
            <div class="clearfix"></div>
            <form enctype="multipart/form-data" id="support_msg" class="validate form-horizontal" method="post" action="<?php echo base_url().'support/reply_ticket/'.$id ?>">
            <input type="hidden" name="subject" value="<?php echo $ticketrow->subject; ?>">
            <input type="hidden" name="domain" value="<?php echo $ticketrow->domain; ?>">
            <input type="hidden" name="user_type" value="<?php echo $ticketrow->user_type; ?>">
            <input type="hidden" name="user_id" value="<?php echo $ticketrow->user_id; ?>">
            <input type="hidden" name="support_ticket_id" value="<?php echo $ticketrow->support_ticket_id; ?>">
            <div class="likrticktsec">
            <div class="cobldo"><?php echo $this->TravelLights['Support']['Attachment']; ?></div>
            <div class="coltcnt">
            <input class="payinput" type="file" name="file_name">
            </div>
            </div>
            <div class="likrticktsec">
            <div class="cobldo"><?php echo $this->TravelLights['Support']['Message']; ?></div>
            <div class="coltcnt">
            <textarea class="tikttext" data-max="150" rows='6' name="textcounter" required></textarea>
            </div>
            </div>
            <div class="likrticktsec">
            <div class="cobldo"> </div>
            <div class="coltcnt">
            <input class="adddtickt" type="submit" value="<?php echo $this->TravelLights['Support']['Reply']; ?>">
            </div>
            </div>
            </form>
            
            
            
          </div>
            
        </div>
        </div>
        </div>
        
    </div>  
        
  </section>

</section>






<div class="clearfix"></div>
<?php //echo $this->load->view('core/footer'); ?>
	<?php //echo $this->load->view('core/bottom_footer'); ?>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/owl.carousel.min.js"></script> 
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.bootstrap.min.js"></script> 
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/responsive.bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom.js"></script>  
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script type='text/javascript' src="<?php echo ASSETS;?>assets/js/custom/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/field_validate.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/custom/custom.js"></script> 


    
<script type="text/javascript">
$(document).ready(function(){

    $('#example').DataTable({responsive:true});
	
	$('#edit_show').click(function(){
		$('#viewpro').fadeOut(500, function(){
				$('#edit_profil').fadeIn();
			});
	});
	
	$('#back_list').click(function(){
		$('#edit_profil').fadeOut(500, function(){
				$('#viewpro').fadeIn();
			});
	});
	
	$('.dash_menus').click(function(){
		$('.sidebar_wrap').toggleClass('open');
	});
	
	$('.wament').click(function(){
		$('.wament').removeClass('active');
		$(this).addClass('active');
	});
	
	
	$('.scrolltop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
	 });
	
});

</script>


</body>
</html>
