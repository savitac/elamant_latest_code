<div id="car" class="tab-pane "> 
 
	<form autocomplete="off" onSubmit="return validateform();" action="<?php echo base_url(); ?>transfer/search" name="hotelSearchForm" id="hotelSearchForm" >
		<div class="intabs">
			<div class="outsideserach">
				<div class="col-md-6 nopad marginbotom10">
					<div class="col-xs-6 fiveh fiftydiv">
						<span class="formlabel"><?php echo $this->TravelLights['TransferSearch']['EnterYourCountry']; ?></span>
						<div class="selectedwrap">
							<select name="transfer_country" id="transfer_country" class="mySelectBoxClass flyinputsnor" onchange="getCities(this)" required>
								<option value="" ><?php echo $this->TravelLights['TransferSearch']['SelectCountry']; ?></option>
								<?php for($trans =0; $trans < count($transfer_countries); $trans++){ ?>
									<option value="<?php echo $transfer_countries[$trans]->country_code; ?>" > <?php echo $transfer_countries[$trans]->country_name; ?> </option>
								<?php } ?>
							 </select>
						</div>
				</div>
					<div class="col-xs-6 fiveh fiftydiv">
						<span class="formlabel"><?php echo $this->TravelLights['TransferSearch']['EnterYourCity']; ?></span>
					  		<div class="selectedwrap">
							<select name="transfer_city" id="transfer_city" class="mySelectBoxClass flyinputsnor" required>
							 </select>
						</div>
				    </div>
				</div>
				
					<div class="col-md-6 nopad marginbotom10">
					<div class="col-xs-6 fiveh fiftydiv">
						<span class="formlabel"><?php echo $this->TravelLights['TransferSearch']['PickupDate']; ?></span>
							<div class="relativemask"> <span class="maskimg caln"></span>
							<input id="t_st_date" name="t_st_date" type="text" placeholder="<?php echo $this->TravelLights['TransferSearch']['TravelDate']; ?>" class="forminput" readonly  required>
						</div>
					
				</div>
					<div class="col-xs-6 fiveh fiftydiv">
						<span class="formlabel"><?php echo $this->TravelLights['TransferSearch']['Travellers']; ?></span>
					  		<div class="selectedwrap">
							<select name="travellers" id="travellers" class="mySelectBoxClass flyinputsnor" >
								<option value="1" selected="selected">1</option>
								<option value="2" >2</option>
								<option value="3" >3</option>
								<option value="4" >4</option>
								<option value="5" >5</option>
								<option value="6" >6</option>
								<option value="7" >7</option>
								<option value="8" >8</option>
								<option value="9" >9</option>
							 </select>
						</div>
				    </div>
				</div>
				
				<div class="col-md-6 nopad marginbotom10">
					<div class="col-xs-6 fiveh fiftydiv">
						<span class="formlabel"><?php echo $this->TravelLights['TransferSearch']['SelectPickup']; ?></span>
						<div class="selectedwrap">
							<select name="pickup_code" id="pickup_code" class="mySelectBoxClass flyinputsnor">
								<option value=""><?php echo $this->TravelLights['TransferSearch']['SelectPickup']; ?></option>
								<?php for($code =0; $code < count($tranfer_list_code); $code++){ ?>
									<option value="<?php echo $tranfer_list_code[$code]->transfer_list_code; ?>"> <?php echo $tranfer_list_code[$code]->english; ?>  </option>
								<?php } ?>
							 </select>
						</div>
				</div>
					<div class="col-xs-6 fiveh fiftydiv">
						<span class="formlabel"><?php echo $this->TravelLights['TransferSearch']['SelectDropoff']; ?></span>
					  		<div class="selectedwrap">
							<select name="dropoff_code" id="dropoff_code" class="mySelectBoxClass flyinputsnor" onchange="generateRACCombination()">
								<option value=""><?php echo $this->TravelLights['TransferSearch']['SelectDropoff']; ?> </option>
								<?php for($drop =0; $drop < count($tranfer_list_code); $drop++){ ?>
									<option value="<?php echo $tranfer_list_code[$drop]->transfer_list_code; ?>"> <?php echo $tranfer_list_code[$drop]->english; ?>  </option>
								<?php } ?>
							 </select>
						</div>
				    </div>
				</div>
					
				<div class="col-md-12 marginbotom10 nopad">
				<div class="col-xs-6 fiveh fiftydiv">
					<div class="formsubmit downmrg">
						<button class="srchbutn comncolor"><?php echo $this->TravelLights['TransferSearch']['SearchTransfers']; ?> <span class="srcharow"></span></button>
					</div>
				</div>
                </div>
			</div>
		</div>	
	</form>
</div>

<script>
 function getCities(country){
	 $.ajax({
			type: "POST",
			url: "<?php echo ASSETS.'dashboard/getTransferCities/'; ?>"+country.value,
			dataType: "json",
			success: function(data){
				$("#transfer_city").empty();
				 $("#transfer_city").append(data.option);
				return false;
			}
		});
 }

</script>

