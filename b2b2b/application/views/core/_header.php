<?php 
$domain_id=$this->session->userdata('branch_id');
$GLOBALS['CI']->load->model('sitemanagement_model');
$domain_list = $GLOBALS['CI']->sitemanagement_model->get_domain_data($domain_id); 
 
foreach ($domain_list as  $value) {
  $email=$value->email;
  $phone=$value->phone;
  $facebook=$value->facebook;
  $twitter=$value->twitter;
  $google=$value->google;
  $youtube=$value->youtube;
}

?>
<header>
 <div class="section_top">
    <div class="container">
      <div class="topalstn">
        <div class="socila hidesocial">
          
          <div class="sidebtn flagss"><a href="<?php echo base_url().'home/login'; ?>">
            <div class="reglognorml">
              <div class="flags">Login</div>
            </div>
            </a>
          </div>
          <div class="sidebtn flagss"><a href="<?php echo base_url().'account/regB2BUser'; ?>">
            <div class="reglognorml">
              <div class="flags">Register user</div>
            </div>
            </a>
          </div>
           <div class="sidebtn flagss"><a href="<?php echo base_url().'account/createAccount'; ?>">
            <div class="reglognorml">
              <div class="flags">Register Agent</div>
            </div>
            </a>
          </div>
          
          <a href="https://<?php echo @$facebook; ?>"><i class="fa fa-facebook"></i></a>
          <a href="https://<?php echo @$twitter; ?>"><i class="fa fa-twitter"></i></a>
          <a href="https://<?php echo @$google; ?>"> <i class="fa fa-google-plus"></i></a>
          <a href="https://<?php echo @$youtube; ?>"><i class="fa fa-youtube"></i></a>
          
        </div>
        
        <div class="toprit">
          <div class="sectns"><a href="mailto:<?php echo @$email; ?>" class="mailadrs"><span class="fa fa-paper-plane"></span><?php echo @$email; ?></a></div>
          <div class="sectns"><a href="tel:<?php echo @$phone; ?>" class="phnumr"><i aria-hidden="true" class="fa fa-phone"></i> <span class="numhide"><?php echo @$phone; ?></span> 
            <div class="fa cliktocl fa-phone"></div>
            </a></div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  
<div class="topssec">
  <div class="container">
  
  <div class="bars_menu fa fa-bars menu_brgr"></div>
     <?php
       if($this->session->userdata('site_name') == ''){ ?>           
       <a class="logo" href="<?php echo base_url();?>"><img src="<?php echo ASSETS; ?>assets/images/logo.png" class="ful_logo"></a>
      <?php } else {  ?>
      <a class="logo" href="<?php echo base_url();?>"><img class="ful_logo" src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $this->session->userdata('domain_logo'); ?>"></a> 
     <?php } ?>
       
       <div class="menuandall">
        <div class="sepmenus">
          <ul class="exploreall">
            <li class=""><a href=""><strong>Home</strong></a></li>
            <li class=""><a href=""><strong>About Us</strong></a></li>
            <li class=""><a href=""><strong>Flight</strong></a></li>
            <li class=""><a href=""><strong>Hotel</strong></a></li>
            <li class=""><a href=""><strong>Bus</strong></a></li>
           <!--  <li class=""><a class="movetop" href="#"><strong>Package</strong></a></li> -->
            <li class=""><a href=""><strong>Contact Us</strong></a></li>
          </ul>
        </div>
       </div>
  </header>
  
</div>
</div>

<div class="clearfix"></div>


  <script>
//Language Convertor
var app_base_url = '<?php echo base_url(); ?>';
  function ChangeLanguage(lang){ 
    var data = {};
    data['language'] = lang;
    $.ajax({
      type: 'POST',
      url: '<?php echo ASSETS; ?>home/change_language',
      async: true,
      dataType: 'json',
      data: data,
      success: function(data) {
        window.location = "<?php echo ASSETS; ?>";
      }
    });
  }
</script>
