<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Travel Lights</title>
        <?php echo $this->load->view("core/load_css"); ?>
        <link href="<?php echo ASSETS; ?>assets/css/transfer_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
    </head>
    <body>
        <!-- Navigation -->
		<?php   if($this->session->userdata('domain_id') == ''){
			 echo $this->load->view('dashboard/top'); 
	     }else{
		   echo $this->load->view('core/header');	 
		 } ?>
        <!-- /Navigation -->
        <div class="clearfix"></div>
        
  <div class="allpagewrp top80">
  <div class="newmodify">
    <div class="container">
      <div class="contentsdw">
        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 nopad">
          <div class="pad_ten">
            
            <div class="from_to_place">
              <h4 class="placename"><?php echo $request->transfer_country; ?></h4></h4>
              <h3 class="contryname"><?php echo $request->transfer_city; ?></h3>
            </div>
            
          </div>
        </div>
        
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopad">
        
        <div class="col-xs-6 nopad ">
          <div class="pad_ten">
            
            <div class="from_to_place">
              <h4 class="placename"><?php echo $this->TravelLights['TransferResult']['PickUp']; ?></h4>
              <h3 class="contryname"><?php echo  $request->pickup_code_desc; ?></h3>
            </div>
            
          </div>
          </div>
          <div class="col-xs-6 nopad ">
          <div class="pad_ten">
            
            <div class="from_to_place">
              <h4 class="placename"><?php echo $this->TravelLights['TransferResult']['DropOff']; ?></h4>
              <h3 class="contryname"><?php echo  $request->dropoff_code_desc; ?></h3>
            </div>
            
          </div>
          </div>
          
        </div>
        
        
        
        <div class="col-lg-2 hidden-md hidden-sm hidden-xs nopad">
          <div class="pad_ten">
            
            <div class="from_to_place">
              <div class="boxlabl textcentr"><?php echo date('d-m-Y',strtotime($request->start_date)); ?> </div>
              <div class="boxlabl textcentr"><strong><?php echo $request->no_of_travellers;  ?></strong><?php echo $this->TravelLights['TransferResult']['Passenger']; ?></div>
            </div>
          </div>
        </div>
        
        <div class="col-md-2 col-sm-4 col-xs-4 nopad">
          <div class="pad_ten">
            <button class="modifysrch" data-toggle="collapse" data-target="#modify"><strong><?php echo $this->TravelLights['TransferResult']['Modify']; ?> </strong> <span class="down_caret"></span></button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="modify_search_wrap">
    <div class="container">
		<form autocomplete="off" onSubmit="return validateform();" action="<?php echo base_url(); ?>transfer/search" name="hotelSearchForm" id="hotelSearchForm" >
      <div id="modify" class="collapse"> 
      		<div class="insplarea">
	<div class="intabs">
		<div class="outsideserach">
			<div class="col-md-6 nopad marginbotom10">
            	<div class="col-xs-6 fiveh fiftydiv">
					<span class="formlabel"><?php echo $this->TravelLights['TransferResult']['EnterYourCountry']; ?></span>
					<div class="selectedwrap">
						<select name="transfer_country" id="transfer_country" class="mySelectBoxClass flyinputsnor" onchange="getCities(this)" required>
								<option value="" ><?php echo $this->TravelLights['TransferResult']['SelectCountry']; ?></option>
								<?php for($trans =0; $trans < count($transfer_countries); $trans++){ ?>
									<option value="<?php echo $transfer_countries[$trans]->country_code; ?>" <?php if($request->transfer_country_code == $transfer_countries[$trans]->country_code){ echo "selected"; } ?> > <?php echo $transfer_countries[$trans]->country_name; ?> </option>
								<?php } ?>
						</select>
					</div>
				</div>
                <div class="col-xs-6 fiveh fiftydiv">
					<span class="formlabel"><?php echo $this->TravelLights['TransferResult']['EnterYourCity']; ?></span>
					<div class="selectedwrap">
						<select name="transfer_city" id="transfer_city" class="mySelectBoxClass flyinputsnor" required>
					    </select>
					</div>
				</div>
            </div>
            <div class="col-md-6 nopad marginbotom10">
            	<div class="col-xs-6 fiveh fiftydiv">
					<span class="formlabel"><?php echo $this->TravelLights['TransferResult']['PickupDate']; ?></span>
					
						<div class="relativemask"> <span class="maskimg caln"></span>
							<input id="t_st_date" value="<?php echo date('d-m-Y',strtotime($request->start_date)); ?>" name="t_st_date" type="text" placeholder="<?php echo $this->TravelLights['TransferSearch']['TravelDate']; ?>" class="forminput" readonly  required>
					     </div>
				</div>
                <div class="col-xs-6 fiveh fiftydiv">
					<span class="formlabel"><?php echo $this->TravelLights['TransferSearch']['Travellers']; ?></span>
					<div class="selectedwrap">
						<select name="travellers" id="travellers" class="mySelectBoxClass flyinputsnor" >
								<option value="1" <?php if($request->no_of_travellers == 1) { echo "selected"; } ?> >1</option>
								<option value="2" <?php if($request->no_of_travellers == 2) { echo "selected"; } ?> >2</option>
								<option value="3" <?php if($request->no_of_travellers == 3) { echo "selected"; } ?> >3</option>
								<option value="4" <?php if($request->no_of_travellers == 4) { echo "selected"; } ?> >4</option>
								<option value="5" <?php if($request->no_of_travellers == 5) { echo "selected"; } ?> >5</option>
								<option value="6" <?php if($request->no_of_travellers == 6) { echo "selected"; } ?> >6</option>
								<option value="7" <?php if($request->no_of_travellers == 7) { echo "selected"; } ?> >7</option>
								<option value="8" <?php if($request->no_of_travellers == 8) { echo "selected"; } ?> >8</option>
								<option value="9" <?php if($request->no_of_travellers == 9) { echo "selected"; } ?> >9</option>
							 </select>
					</div>
				</div>
            </div>
			
            	<div class="col-md-6 marginbotom10 nopad">
						<div class="col-xs-6 fiveh fiftydiv">
                            <span class="formlabel"><?php echo $this->TravelLights['TransferSearch']['SelectPickup']; ?></span>
                            <div class="selectedwrap">
							<select name="pickup_code" id="pickup_code" class="mySelectBoxClass flyinputsnor">
								<option value=""><?php echo $this->TravelLights['TransferSearch']['SelectPickup']; ?></option>
								<?php for($code =0; $code < count($tranfer_list_code); $code++){ ?>
									<option value="<?php echo $tranfer_list_code[$code]->transfer_list_code; ?>" <?php if($request->pickup_code == $tranfer_list_code[$code]->transfer_list_code) { echo "selected"; } ?>> <?php echo $tranfer_list_code[$code]->english; ?>  </option>
								<?php } ?>
							 </select>
						</div>
                   </div>
                   
                   <div class="col-xs-6 fiveh fiftydiv">
                            <span class="formlabel"><?php echo $this->TravelLights['TransferSearch']['SelectDropoff']; ?> </span>
                            <div class="selectedwrap">
							<select name="dropoff_code" id="dropoff_code" class="mySelectBoxClass flyinputsnor" onchange="generateRACCombination()">
								<option value=""><?php echo $this->TravelLights['TransferSearch']['SelectDropoff']; ?></option> 
								<?php for($drop =0; $drop < count($tranfer_list_code); $drop++){ ?>
									<option value="<?php echo $tranfer_list_code[$drop]->transfer_list_code; ?>" <?php if($request->dropoff_code == $tranfer_list_code[$drop]->transfer_list_code) { echo "selected"; } ?>> <?php echo $tranfer_list_code[$drop]->english; ?>  </option>
								<?php } ?>
							 </select>
						</div>
                   </div>
                				
			</div>
			<div class="col-md-12 marginbotom10 nopad">
			<div class="col-xs-6 fiveh fiftydiv">
				<div class="formsubmit downmrg">
					<button type="submit" class="srchbutn comncolor"><?php echo $this->TravelLights['TransferSearch']['SearchTransfers']; ?> </button>
				</div>
			</div>
            </div>
			</form>
		
	</div>
			</div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="contentsec">
    <div class="container">
      <div class="filtrsrch">
        <div class="col30">
          <div class="celsrch">
          	<button class="close_filter"><span class="fa fa-close"></span></button>
            <div class="boxtop">
              <div class="filtersho">
                <div class="avlhtls"><strong><span id="total_result_count">0</span></strong> <?php echo $this->TravelLights['TransferResult']['Transfersfound']; ?><span class="placenamefil">  <?php echo $this->TravelLights['TransferResult']['In']; ?>  <?php echo ucfirst($request->transfer_city).", ".ucfirst($request->transfer_country); ?> </span> </div>
              </div>
            </div>
            <div class="norfilterr"> 
            <div class="resultfilt"> <?php echo $this->TravelLights['TransferResult']['FilterResults']; ?></div>
             
              <div class="outbnd">
                
                <div class="rangebox">
                  <div class="ranghead collapsed" data-target="#demo" data-toggle="collapse"> <?php echo $this->TravelLights['TransferResult']['Price']; ?></div>
                  <div id="demo" class="collapse">
                  	<div class="price_slider1">
                    <input type="text"  class="level" id="amount" readonly >
                    <div id="slider-range"></div>
                    </div>
                  </div>
                </div>
                <div class="rangebox">
                  <div class="ranghead collapsed" data-target="#demo2" data-toggle="collapse"> <?php echo $this->TravelLights['TransferResult']['TransferType']; ?></div>
                  <div id="demo2" class="stoprow collapse">
                    <div class="boxins">
						<input type="hidden" name="min_price" id="min_price" value="" />
						<input type="hidden" name="max_price" id="max_price" value="" />
                      <ul class="locationul" id="starrat">
                     
                      </ul>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        <div class="col70">
          <div class="in70">
          <div class="topmisty hote_reslts">
              <div class="col-xs-12 nopad">
                <button class="filter_show"><span class="fa fa-filter"></span></button>
                <div class="insidemyt">
                  <div class="col-xs-6 nopad fullshort">
                    <ul class="sortul">
                      <li class="sortli"> <span class="sirticon fa fa-sort-amount-asc"></span> 
                      <a class="sorta des" id="sort1" data-order-by="type" data-order='desc' rel="data-transfer" onclick="filter_sortings('sort1');" > <?php echo $this->TravelLights['TransferResult']['TransferType']; ?></a> 
                      </li>
                      <li class="sortli" data-order-by="price" > <span class="sirticon fa fa-tag"></span>
                      <a class="sorta nobord des" id="sort2" data-order-by="price" data-order='asc' rel="data-price" onclick="filter_sortings('sort2');"> <?php echo $this->TravelLights['TransferResult']['Price']; ?></a> </li>
                      
                    </ul>
                  </div>
                  
                </div>
              </div>
            </div>
              
            <!--All Available flight result comes here -->
            
            <div class="allresult">
              <div class="hotel_map">
                <div class="map_hotel" id="map"></div>
              </div>
              <div class="hotels_results" id="transfer_result">
             
              </div>
            </div>
            <!-- End of result --> 
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
      
      
      <?php echo $this->load->view('core/footer'); ?>
	  <?php echo $this->load->view('core/bottom_footer'); ?>
	   <!--Cancellation details-->
      
        <!-- Loading Animation -->
		
		<div class="all_loading imgLoader"> 
			  
			<div class="load_inner">
				<div class="relativetop">
					<div class="paraload"> <?php echo $this->TravelLights['TransferResult']['Searchingforthebest']; ?> <?php echo ucfirst($request->transfer_city).", ".ucfirst($request->transfer_country); ?> </div>
					<div class="normal_load">
					<?php if($this->session->userdata('site_name') == ''){ ?>  
						<img style="max-height: 56px" src="<?php echo base_url().'/assets/images/loader.gif'; ?>">
					<?php } else { ?>
					     <img style="max-height: 56px" src="<?php echo ASSETS; ?>cpanel/uploads/domain/<?php echo $this->session->userdata('domain_logo'); ?>" width="100px" height="100px">	
					<?php 	} ?>
					
					</div>
					<div class="clearfix"></div>
					<div class="sckintload">
						<!--For round way add class 'round_way'-->
						<div class="ffty">
							<div class="borddo brdrit"> <span class="lblbk"><?php echo $this->TravelLights['TransferResult']['StartDate']; ?></span> </div>
						</div>
						<div class="ffty">
							<div class="borddo"> <span class="lblbk"><?php echo $this->TravelLights['TransferResult']['EndDate']; ?></span> </div>
						</div>
						<div class="clearfix"></div>
						<div class="tabledates for_hotel">
							<!--  Check in  -->
							<div class="tablecelfty">
								<div class="borddo brdrit">
									<div class="fuldate">
										<span class="bigdate"><?php echo date('d',strtotime($request->start_date)); ?></span>
										<div class="biginre"><?php echo date('M',strtotime($request->start_date)); ?><br>
											<?php echo date('Y',strtotime($request->start_date)); ?> 
										</div>
									</div>
								</div>
							</div>
							<!--  Check out  -->
							<div class="tablecelfty">
								<div class="borddo">
									<div class="fuldate">
										<span class="bigdate"><?php echo date('d',strtotime($request->start_date)); ?></span>
										<div class="biginre"> <?php echo date('M',strtotime($request->start_date)); ?><br>
											<?php echo date('Y',strtotime($request->start_date)); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="setMinPrice" value="" id="setMinPrice">
		<input type="hidden" name="setMaxPrice" value="" id="setMaxPrice">
		<input type="hidden" value="" id='session_id'>
		
		
		<!--Map view indipendent hotel-->

        <!-- End -->
        <script>var WEB_URL = "<?php echo ASSETS; ?>"</script>
        <script type="text/javascript" src="<?php echo ASSETS ?>assets/js/owl.carousel.min.js"></script> 
        <script type="text/javascript" src="<?php echo ASSETS ?>assets/js/custom.js"></script> 
        <script type="text/javascript" src="<?php echo ASSETS ?>assets/js/hotel_filter.js"></script>
        <script src="<?php echo ASSETS ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script> 
        <script src="<?php echo ASSETS ?>assets/js/filter.js" type="text/javascript"></script> 
        <script src="<?php echo ASSETS ?>assets/js/custom/transfer.js" type="text/javascript"></script> 
        <script type="text/javascript">
			var ASSETS	= "<?php echo ASSETS; ?>assets/";
			var WEB_URL	= "<?php echo ASSETS; ?>";
			var markers = '{}';
			var geometry = {"latitude":25.2048493,"longitude":55.2707828,"status":"OK"};
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script> 
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/map.js"></script>
        <script type="text/javascript">
			
            $(document).ready(function(){
				$("#t_st_date").datepicker({
						numberOfMonths: 1,
						minDate: 0,
						dateFormat: 'dd-mm-yy',
						maxDate: "+1y",
						numberOfMonths: 1,
						onSelect: function(dateStr) {
							var d1 = $(this).datepicker("getDate");
							d1.setDate(d1.getDate()); // change to + 1 if necessary
							var d2 = $(this).datepicker("getDate");
							d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
							$("#en_date").datepicker("setDate", d1);
							$("#en_date").datepicker("option", "minDate", d1);
						},
						onClose: function( selectedDate ) {
							$( "#en_date" ).datepicker( "option", "minDate", selectedDate );
							$( '#en_date' ).focus();
						}
					});
				 if($('#transfer_country').val() != ''){
					 getCities1($('#transfer_country').val());
					
					
				 }
				
            	$('.scrolltop').click(function(){
            		$("html, body").animate({ scrollTop: 0 }, 600);
            	 });
            	 
            	 $('[data-toggle="popover"]').popover();
            	 
            	$('.advncebtn').click(function(){
            	$(this).parent('.togleadvnce').toggleClass('open');
            });
            
            $('.alladvnce').click(function(){
            	$('.remngwd').toggleClass("chageup");
            	$('.advncedown').toggleClass("fadeinn");
            });
            
            $('.alladvnce').click(function(e){
            	e.stopPropagation();
            });
            
            $(document).click(function(){
            	$('.advncedown').removeClass("fadeinn");
            	$('.remngwd').removeClass("chageup");
            });
            	
            	
            $(document).on('click','.cmnact',function(){
				$(this).text(function (index, text) {
					return (text == 'More' ? 'Less' : 'More');
				});
				$(this).parent(".boxins").toggleClass('open')
			});
            
             /*Star rating click*/
              $('.toglefil').click(function(){
                $(this).toggleClass('active');
              });
              /*Star rating click end*/
              
              /*Map view click function*/
            	$('.map_click').click(function(){
            		$('.allresult').addClass('map_open');
            		$('.view_type').removeClass('active');
            		$(this).addClass('active');
            		
            		$(".hotels_results").niceScroll({styler:"fb",cursorcolor:"#4ECDC4", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});
            		
            		setTimeout(function(){
            			google.maps.event.trigger(map, 'resize');
            		},500);
               	 	
            	});
            	
            	$('.hotel_addressmap').click(function(){
            		setTimeout(function(){
            		initialize();
            	},200);
            	});
            	
            	$('.list_click').click(function(){
            		$('.allresult').removeClass('map_open');
            		$('.view_type').removeClass('active');
            		$(this).addClass('active');
            	});
            	
            	$(".tooltipv").tooltip();
            	
            
              
              
            });
        </script> 
        <script>
               $('.filter_show').click(function(){
            		$('.filtrsrch').addClass('open');
            	});
            	
            	$('.close_filter').click(function(){
            		$('.filtrsrch').removeClass('open');
            	});
        </script>
        
        <script>
         
        </script>
     
        <script>
			$(document).ready(function() {
				
				var start = new Date().getTime(),difference;
				var a = [<?php echo $api_det; ?>];
				var i = 0;
				var k = 1;
				function nextCall() {
					if(i==0) {
						$('.imgLoader').fadeIn();
						$('body').css('overflow', 'hidden');
					}
					var x = a.length;
					if (i == a.length) {
						$('.imgLoader').fadeOut();
						$('body').css('overflow', '');
						//setclick();
						return;
					}
					$.ajax({
						dataType: 'json',
						url: '<?php echo base_url(); ?>transfer/call_api/' + a[i],
						data: { request: '<?php echo $req; ?>', sessiondata : '<?php echo $session_data; ?>'},
						beforeSend: function() {
							$('.imgLoader').fadeIn();
							$('.ppage, .pagination').hide();              
						},
						success: function(data) {
								$('.imgLoader').fadeOut();
							if(data.status == 1){
							
							if (parseInt(data.total_result) == 0) {
								$('#noresult').fadeIn();
							}
						    $('#transfer_result').html(data.transfer_search_result);
							$('#total_result_count').html(data.total_result);
							$('#min_price').val(data.min_val);
							$('#max_price').val(data.max_val);
							var minVal = Math.floor(parseFloat(data.min_val));
							var maxVal = Math.ceil(parseFloat(data.max_val));
							$( "#setMinPrice" ).val(minVal);
							$( "#setMaxPrice" ).val(maxVal);
							$("#session_id").val(data.session_id);
							$("#request_string").val(data.requeststring);
							$("#api_id").val(data.api_id);
							$("#slider-range").slider({
								range: true,
								min: minVal,
								max: maxVal,
								values: [minVal, maxVal],
								slide: function (event, ui) {
									$("#amount").val("<?php echo BASE_CURRENCY;?> " + ui.values[ 0 ] + " - <?php echo BASE_CURRENCY;?> " + ui.values[ 1 ]);
								},
								change: function (event, ui)
								{
								 $("#min_price").val(ui.values[ 0 ]);
								 $("#max_price").val(ui.values[ 1 ]);
								filter_result();	
								}
							});
							
							$("#amount").val(" <?php echo BASE_CURRENCY;?> " + $("#slider-range").slider("values", 0) + " - <?php echo BASE_CURRENCY;?> " + $("#slider-range").slider("values", 1));

							var transferType = '';
							var facCount = 0;
							if( $.isArray(data.trans_types)) {
								transfer_type = '';
								 $.each(data.trans_types, function(key, values) {
									  $.each(values, function(key1, value) {
										  transfers = value.split('|||');
										   transfer_type +='<li>'+''+
										                '<div class="squaredThree">'+''+
																			'<input type="checkbox"    name="transfer_type" class="filter_transfer" id="transfer_type'+ transfers[0] +'" value='+transfers[0]+' onclick="filter_result();">'+''+
																			'<label for="transfer_type'+ transfers[0] +'"></label>'+''+
																		'</div>'+''+
																		'<label for="transfer_type'+ transfers[0] +'" class="lbllbl">'+transfers[1]+'</label><span class="countrgt"></span>'+''+
																	'</li>';
								    	facCount = facCount+1;
									 });
								}); 
							   $("#starrat").html(transfer_type);
						    }
						    i++;
							nextCall();
						}else{
							alert("Transfer API is not activated. Please contact administrator");
							return false;
						}
						}
					});
				}
				nextCall();
				filter_result();
			});
			
						 function getCities(country){
				$.ajax({
				type: "POST",
				url: "<?php echo base_url().'dashboard/getTransferCities/'; ?>"+country.value,
				dataType: "json",
				success: function(data){
					$("#transfer_city").empty();
					 $("#transfer_city").append(data.option);
					return false;
				}
				});
			}
			
			 
 function getCities1(country){
	 $.ajax({
			type: "POST",
			url: "<?php echo base_url().'dashboard/getTransferCities/'; ?>"+country,
			dataType: "json",
			success: function(data){
				$("#transfer_city").empty();
				 $("#transfer_city").append(data.option);
				   $("#transfer_city").val('<?php echo $request->transfer_city_code; ?>');
				return false;
			}
		});
 }
 
 var filterTransfer = [];
			
			function filter_sortings(obj){
				var $order = $("#"+obj).attr("data-order");
				$("#"+obj).addClass('active');
				$("#"+obj).toggleClass('des ase');
				if($("#"+obj).attr("data-order") == 'asc'){
				 $("#"+obj).attr("data-order", "desc");
			    }else{
				 $("#"+obj).attr("data-order", "asc");	
				}
				var $order_by = $("#"+obj).attr("rel");
				sort_results($order,$order_by) 
				 
			}
			
        </script>
     </body>
</html>
