<?php   ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title><?php echo $this->session->userdata('company_name')?></title>
        <?php echo $this->load->view("core/load_css"); ?>
         <link href="<?php echo ASSETS; ?>assets/css/flight_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script>
		<script src="<?php echo ASSETS; ?>assets/js/jquery-ui.min.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/general.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/css/owl.carousel.min.css"></script>
		<script src="<?php echo ASSETS; ?>assets/js/owl.carousel.min.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/flight_search.js"></script> 
		<script src="<?php echo ASSETS; ?>assets/js/provablib.js"></script>
		<script src="<?php echo ASSETS; ?>assets/js/jquery.jsort.0.4.min.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/pax_count.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>assets/js/datepicker.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/map.js"></script>

    </head>    
    <body>
      <?php 
        if($this->session->userdata('user_logged_in') == 0) {  
			echo $this->load->view('core/header'); 
		} else {
			echo $this->load->view('dashboard/top'); 	 
		}
	 ?>

<script>
var app_base_url = "<?php echo base_url(); ?>";
</script>
<div class="fltbook prbkng"> 
<div class="container" id="flights_pre_booking"> 
<div class="row">
<div class="col-sm-12 col-md-8">
<form id="card_details" method="POST" enctype="multipart/form-data" name="card_details" action="<?= base_url().'payment_gateway/payeesy_process'?>" >
	<input type="hidden" name="book_id" id="book_id" value="<?= $book_id;?>">
	<input type="hidden" name="productinfo" id="productinfo" value="<?= $productinfo;?>">
	<input type="hidden" name="txnid" id="txnid" payeezy-data="currency" value="<?= $book_id;?>" />
<section class="booking_form">  
 
    <h5>Card Details</h5>
  

    <div class="section_bg">        
		<fieldset class="col-xs-12">
			<div class="left col-md-3 col-sm-12 col-xs-12">
				<label>Card Type<font style="color:red"> *</font>:</label>
				<select payeezy-data="card_type" name="card_type" class="mntxt form-control des_name" required> 
					<option value="mastercard">Master Card</option>
					<option value="visa">Visa</option>
					<option value="discover">Discover</option>  
					<option value="diners_club">Diners Club</option>
					<option value="jcb">JCB</option>				 
					<option value="American Express">American Express</option>
				</select>
			</div>
			<div class="right col-md-6 col-sm-12 col-xs-12">
				<img src="<?php echo ASSETS; ?>assets/images/payicon.png" alt="Image">
			</div>
		</fieldset>  
		<fieldset class="col-xs-12">
			<div class="left col-md-5 col-sm-12 col-xs-12">
				<label>Cardholder Name <font style="color:red"> *</font>:</label>
				<input type="text" class="clainput capitalize _guest_validate_field" id="card_pname" name="cardholdername" value="" placeholder="Please enter name of cardholder" required onkeyup="chk_names(this);" >
			</div>
			<div class="right col-md-6 col-sm-12 col-xs-12">
				<small>(As it appears on your credit card)</small>
			</div>
		</fieldset>  
		<fieldset class="col-xs-12">
			<div class="left col-md-5 col-sm-12 col-xs-12">
				<label>Card Number <font style="color:red"> *</font></label>
				<input type="text" class="clainput capitalize guest_validate" name="card_number" value="" placeholder="Please enter Card Number" min="16" max="16" required>
			</div>
			<div class="right col-md-6 col-sm-12 col-xs-12">
				<small>(Pay with credit or debit card)</small>
			</div>
		</fieldset>  
		<fieldset class="col-xs-12">
			<div class="left col-md-3 col-sm-12 col-xs-12">
				<label>CVV Code <font style="color:red"> *</font>:</label>
				<input type="password" class="clainput capitalize guest_validate" id="" name="cvv_code" value="" placeholder="Please enter CVV" required>
			</div>
			<div class="right col-md-6 col-sm-12 col-xs-12">
				<img src="<?php echo ASSETS; ?>assets/images/cvv.jpg" alt="Image"> <small>3 digit number from your card</small>
			</div>  
		</fieldset>
		<fieldset class="col-xs-12">
			<div class="left col-md-3 col-sm-12 col-xs-12">
			<div class="col-xs-12 expry nopad">
			<label>Expiry Date <font style="color:red"> *</font> :</label>
			<div class="row">
			<div class="col-xs-6 nopadR">			
			<select payeezy-data="exp_month" class="mntxt form-control des_name" name="card_expmonth" required> <option value="01">01</option> <option value="02">02</option> <option value="03">03</option> <option value="04">04</option> <option value="05">05</option> <option value="06">06</option> <option value="07">07</option> <option value="08">08</option> <option value="09">09</option> <option value="10">10</option> <option value="11">11</option> <option value="12" selected="">12</option> </select>
			</div>
			<div class="col-xs-6 nopadL">			
			<select payeezy-data="exp_year" class="mntxt form-control des_name" name="card_expyear" required> <option value="17">2017</option> <option value="18">2018</option> <option value="19">2019</option> <option value="20">2020</option> <option value="21">2021</option> <option value="22">2022</option> <option value="23">2023</option> <option value="24">2024</option> <option value="25">2025</option> <option value="26">2026</option> <option value="27">2027</option> <option value="28">2028</option> <option value="29">2029</option> <!--<option value="18">2018</option> <option value="19">2019</option> <option value="20">2020</option> <option value="21">2021</option>--> </select>
			</div>
			</div>
			</div>
			</div>
		</fieldset>
		</div>
		</section>
		<section class="booking_form billing"> <h5><!-- Contact Details --> Billing &amp; Contact Information</h5>
		<div class="section_bg">
		<fieldset class="col-md-4 col-xs-12">
			<div class="left col-md-12 col-sm-12 col-xs-12">
				<label>CITY <font style="color:red"> *</font>:</label>
				<input type="text" class="clainput capitalize guest_validate" id="" name="contact_city" value="" placeholder="Please enter CITY Name" required>
			</div>
		</fieldset>
		<fieldset class="col-md-4 col-xs-12">
			<div class="left col-md-12 col-sm-12 col-xs-12">
				<label>STATE <font style="color:red"> *</font>:</label>
				<input type="text" class="clainput capitalize guest_validate" id="" name="contact_state" value="" placeholder="Please enter STATE Name" required>
			</div>
		</fieldset>
		<fieldset class="col-md-4 col-xs-12">
			<div class="left col-md-12 col-sm-12 col-xs-12">
				<label>COUNTRY <font style="color:red"> *</font>:</label>
				<input type="text" class="clainput capitalize guest_validate" id="" name="contact_state" value="" placeholder="Please enter COUNTRY Name" required>
			</div>
		</fieldset>
		<fieldset class="col-md-4 col-xs-12">
			<div class="left col-md-12 col-sm-12 col-xs-12">
				<label>ZIPCODE <font style="color:red"> *</font>:</label>
				<input type="text" class="clainput capitalize guest_validate" id="" name="contact_state" value="" placeholder="Please enter ZIPCODE" required>
			</div>
		</fieldset>  
		<fieldset class="col-md-4 col-xs-12">
			<div class="left col-md-12 col-sm-12 col-xs-12">
				<label>NATIONALITY <font style="color:red"> *</font>:</label>
				<input type="text" class="clainput capitalize guest_validate" id="contact_nationality" name="contact_nationality" value="" placeholder="Please enter NATIONALITY" required>  
			</div> 
		</fieldset>
		<fieldset class="col-md-4 col-xs-12">
			<div class="left col-md-12 col-sm-12 col-xs-12">
				<label>EMAIL <font style="color:red"> *</font>:</label>
				<input type="text" class="clainput capitalize guest_validate" id="contact_email" name="contact_email" value="" placeholder="Please enter EMAIL" required>  
			</div>
		</fieldset>
		<fieldset class="col-md-4 col-xs-12">
			<div class="left col-md-12 col-sm-12 col-xs-12">
				<label>CODE <font style="color:red"> *</font>:</label>
				<input type="text" class="clainput capitalize guest_validate" id="telephone_code" name="telephone_code" value="" placeholder="Please enter CODE" required>  
			</div>
		</fieldset> 
		 <fieldset class="col-md-4 col-xs-12">
			<div class="left col-md-12 col-sm-12 col-xs-12">
				<label>PHONE NO <font style="color:red"> *</font>:</label>
				<input type="text" class="clainput capitalize guest_validate" id="phone_number" name="phone_number" value="" placeholder="Please enter PHONE NO" required>  
			</div>
		</fieldset> 
		<div class="clearfix"></div>
<input type="SUBMIT" class="btn blue make_payment" value="Confirm Booking">
</div>
</section>
</form>
</div>
<div class="col-xs-12 col-md-4 advr">
<a href="#"><img src="<?php echo ASSETS; ?>assets/images/ad1.jpg" alt="advertisement" /></a>
<a href="#"><img src="<?php echo ASSETS; ?>assets/images/ad2.jpg" alt="advertisement" /></a>
</div>
</div>
</div>
</div>
<script>
	function chk_names(obj){
   		var valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ";
	    var ok = "yes";
	    var temp;
	    var validchars="";
	    var dot = 0;
	    for (var i=0; i<obj.value.length; i++) {
	        temp = obj.value.substring(i,i+1);
	        if (valid.indexOf(temp) == "-1") {
	            ok = "no";
	            break;
	        } else {
	            validchars=validchars+temp;
	        }
	    }

	    if (ok == "no") {
	        alert("Only Alpha characters are allowed!");
	        obj.value = validchars;
	        return false;
	    }
	    return true;
}

$('input._guest_validate_field').keyup(function() {
    var sAlphabetic = $(this).val();
    var alpha_filter    = /^[a-zA-Z ]+$/; 
    if(sAlphabetic != ''){
        if (alpha_filter.test(sAlphabetic)) {
            $(this).css('border', '1px solid #099A7D'); 
        }else {
            $(this).css('border', '1px solid #f52c2c');
        }
    }else{
        $(this).css('border', '1px solid #ddd');
    }
});

$('input.guest_validate').keyup(function() {
    var sAlphabetic = $(this).val();
    var alpha_number_filter     = /^[0-9 -]+$/; 
    if(sAlphabetic != ''){
        if (alpha_number_filter.test(sAlphabetic)) {
            $(this).css('border', '1px solid #099A7D'); 
        }else {
            $(this).css('border', '1px solid #f52c2c');
        }
    }else{
        $(this).css('border', '1px solid #ddd');
    }
});
</script>

<?php echo  $this->load->view('core/bottom_footer'); ?>
 
</body>   