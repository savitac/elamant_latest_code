<?php   ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title><?php echo $this->session->userdata('company_name')?></title>
        <?php echo $this->load->view("core/load_css"); ?>
         <link href="<?php echo ASSETS; ?>assets/css/flight_result.css" rel="stylesheet">
        <link href="<?php echo ASSETS; ?>assets/css/load.css" rel="stylesheet">
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather"></script>
        <script src="<?php echo ASSETS; ?>assets/js/jquery-ui.min.js"></script> 
        <script src="<?php echo ASSETS; ?>assets/js/general.js"></script> 
        <script src="<?php echo ASSETS; ?>assets/css/owl.carousel.min.css"></script>
        <script src="<?php echo ASSETS; ?>assets/js/owl.carousel.min.js"></script> 
        <script src="<?php echo ASSETS; ?>assets/js/flight_search.js"></script> 
        <script src="<?php echo ASSETS; ?>assets/js/provablib.js"></script>
        <script src="<?php echo ASSETS; ?>assets/js/jquery.jsort.0.4.min.js"></script>
        <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/pax_count.js"></script>
        <script type="text/javascript" src="<?php echo ASSETS;?>assets/js/datepicker.js"></script>
        <script type="text/javascript" src="<?php echo ASSETS; ?>assets/js/map.js"></script>

    </head>    
    <body>
      <?php 
        if($this->session->userdata('user_logged_in') == 0) {  
            echo $this->load->view('core/header'); 
        } else {
            echo $this->load->view('dashboard/top');     
        }
     ?>

<script>
var app_base_url = "<?php echo base_url(); ?>";
</script>
<div class="fltbook trns">
    <!-- <div class="transan container text-center" id="">
        <img src="<?php echo base_url(); ?>cdn/provab_js/images/cancelled.jpg" />
        <p>Your Transaction Is Failed</p>
    </div> -->
    <div class="transan" id="">
        <img src="<?php echo ASSETS; ?>assets/images/cancelled.png" />
        <h3 class="text-uppercase">Transaction Failed</h3> 
        <p>Your transaction is failed due to some reasons</p>
        <a href="<?php echo base_url(); ?>">Home</a>
    </div>
</div>
                
<?php echo  $this->load->view('core/bottom_footer'); ?>
 
</body>     
