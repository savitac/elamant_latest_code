<?php
ini_set('memory_limit', '-1');
/**
 * Provab XML Class
 *
 * Handle XML Details
 *
 * @package	Provab
 * @subpackage	provab
 * @category	Libraries
 * @author		Arjun J<arjun.provab@gmail.com>
 * @link		http://www.provab.com
 */
class Api_Interface {
	/**
	 *
	 * @param array $query_details - array having details of query
	 */
	public function __construct()
	{

	}

	/**
	 * Get Domain Balance for Admin
	 */
	function rest_service($method, $params=array()) {
		$CI=&get_instance();
		//$system = 'live';
		$system = $CI->config->item('external_service_system');

		$username = $CI->config->item($system.'_username');
		$password = $CI->config->item($system.'_password');
		$params = array('domain_key'=> get_domain_key(), 'username'=> $username, 'password'=> $password, 'system'=>$system);

		$params['domain_id'] = @$CI->entity_domain_id;
		$url = $CI->config->item('external_service');

		$ch = curl_init($url.$method);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_ENCODING, "gzip");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$res = curl_exec($ch);
		curl_close($ch);
		return  $res;
	}

	/**
	 * get response from server for the request
	 *
	 * @param $request 	   request which has to be processed
	 * @param $url	   	   url to which the request has to be sent
	 * @param $soap_action
	 *
	 * @return xml response
	 */
	public function get_json_response($url, $request=array(), $header_details){

//debug($url);debug($request);debug($header_details);exit;
		$header=array(
			'Content-Type:application/json',
			'Accept-Encoding:gzip, deflate',
			'x-Username:'.$header_details['UserName'],//Remove password later, sending basic/digest auth
			'x-DomainKey:'.$header_details['DomainKey'],
			'x-system:'.$header_details['system'],
			'x-Password:'.$header_details['Password']//Remove password later, sending basic/digest auth
		);
		//debug($header_details);
		//echo $url;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_ENCODING, "gzip");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	
		$res = curl_exec($ch);
		// //debug($res);exit;
		// //echo $url;
  //    	if($_SERVER['REMOTE_ADDR']=='192.168.0.75'){
  //    		//debug($res);exit;
  //    	}elseif ($_SERVER['REMOTE_ADDR']=='192.168.0.199') {
  //    		//bala ip
  //    	}
  //    	elseif ($_SERVER['REMOTE_ADDR']=='192.168.0.77') {
  //    		debug($res);exit;
  //    	}
  //    	elseif($_SERVER['REMOTE_ADDR']=='::1'){
		// 	//debug($res);exit;
  //    	}
		
		$res = json_decode($res, true);
		curl_close($ch);
		return $res;
	}


	// public function get_json_response($url, $request = array(), $header_details) {
  		
 //        $header = array(
 //            'Content-Type:application/json',
          
 //            'Accept-Encoding:gzip, deflate',
 //            'x-Username:' . $header_details['UserName'], //Remove password later, sending basic/digest auth
 //            'x-DomainKey:' . $header_details['DomainKey'],
 //            'x-system:' . $header_details['system'],
 //            'x-Password:' . $header_details['Password']//Remove password later, sending basic/digest auth
 //        );
        
 //  		//print_r($url);die;
 //        $ch = curl_init($url);
 //        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
 //        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
 //        curl_setopt($ch, CURLOPT_POST, 1);
 //        curl_setopt($ch, CURLOPT_ENCODING, "gzip");

 //        //curl_setopt($ch, CURLOPT_COOKIEJAR, $_COOKIE);

 //        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
 //        if(isset($_COOKIE['AWSELB'])){
 //            curl_setopt($ch, CURLOPT_COOKIE, 'AWSELB='.$_COOKIE['AWSELB']);
 //        }
 //        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        
 //        //enable headers
 //        curl_setopt($ch, CURLOPT_HEADER, 1);

 //        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
 //        $res = curl_exec($ch);  

 //        list($header, $body) = explode("\r\n\r\n", $res, 2);

 //        // set cookie 
 //        if(isset($header) and !empty($header)){
 //            preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
 //            $cookies = array();
 //            foreach($matches[1] as $key =>  $item) {
 //                $cookies[$key] =  $item.';';
 //            }
 //            foreach ($cookies as $key => $value) {
 //                $cookies_val = explode("=", $value);
 //                if($cookies_val[0]=='AWSELB'){
 //                    setcookie($cookies_val[0],$cookies_val[1], time() + (86400 * 30), "/");  
 //                }
 //            }
 //        }

 //        $res = json_decode($body, true);   
 //        curl_close($ch);
 //        return $res;
 //    }
	/**
	 * get response from server for the request
	 *
	 * @param $request 	   request which has to be processed
	 * @param $url	   	   url to which the request has to be sent
	 * @param $soap_action
	 *
	 * @return xml response
	 */
	public function delete_get_json_response($url, $request=array(), $header_details){
		$header=array(
			'Content-Type:application/json',
			'Accept-Encoding:gzip, deflate',
			'x-Username:'.$header_details['UserName'],
			'x-DomainKey:'.$header_details['DomainKey'],
			'x-system:'.$header_details['system'],
			'x-Password:'.$header_details['Password']
		);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_ENCODING, "gzip");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	
		$res = curl_exec($ch);
		echo 'reponse';
		debug($res);exit;
		$res = json_decode($res, true);
		curl_close($ch);
		return $res;
	}

	/**
	 * Get xml response from URL for the request
	 * @param string $url
	 * @param xml	 $request
	 */
	public function get_xml_response($url, $request, $convert_to_array=true)
	{
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'Accept-Encoding:gzip, deflate',));
		curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "$request");

		$xml = curl_exec($ch);
		
		if ($convert_to_array) {
			$data = Converter::createArray($xml);
		} else {
			$data = $xml;
		}
		//debug($data); exit;
		return $data;
	}

	public function objectToArray($d)
	{
		if (is_object($d)) {
			$d = get_object_vars($d);
		}

		if (is_array($d)) {
			return array_map(array($this, 'objectToArray'), $d);
		}
		else {
			return $d;
		}
	}

	public function get_object_response($request_type, $request, $header_details)
	{
		
		//echo $request_type; exit;
		/* debug($request_type);
		debug($request);
		debug($header_details);exit; */
		$header = $header_details['header'];
		$credintials = $header_details['credintials'];
		
		//debug($header_details); exit;
		
		$_header[] = new SoapHeader("http://provab.com/soap/", 'AuthenticationData', $header, "");
		$client = new SoapClient(NULL, array('location' => $credintials['URL'],
                  'uri' => 'http://provab.com/soap/','trace' => 1, 'exceptions' => 0));
		try {
			$result = $client->provab_api($request_type, $request, $_header);
			//debug(unserialize(base64_decode($result->GetFareQuoteResult->ProvabAuthKey)));
		} catch(Exception $err) {
			echo "<pre>"; print_r($err->getMessage());
		}
		//print_r($client->__getLastResponse());
		//echo "<pre>"; print_r($result); exit;

		return $result;
	}
}
