<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once '../b2b2b/application/libraries/Common_Api_Grind.php'; 
/**
 *
 * @package    Provab
 * @subpackage API
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */

class Provab_private extends Common_Api_Grind {

	protected $ClientId;
	protected $UserName;
	protected $Password;
	protected $system;			//test/live   -   System to which we have to connect in web service
	protected $Url;
	private $service_url;
	private $TokenId;//	Token ID that needs to be echoed back in every subsequent request
	protected $ins_token_file;
	private $CI;
	private $commission = array();
	var $master_search_data;
	var $search_hash;//search

	public function __construct()
	{
		parent::__construct();
		$this->CI = &get_instance();
		$this->CI->load->library('Api_Interface');
		$this->CI->load->model('flight_model');
		$this->CI->load->model('private_management_model'); 
		$this->set_api_credentials();
	}
	private function set_api_credentials()
	{

		$flight_engine_system = $this->CI->config->item('flight_engine_system');
		$this->system = $flight_engine_system;
		$this->UserName = $this->CI->config->item($flight_engine_system.'_username');
		$this->Password =  $this->CI->config->item($flight_engine_system.'_password');
		$this->Url = $this->CI->config->item('flight_url');
		$this->ClientId = $this->CI->config->item('domain_key');
		//$this->UserName = 'test';
		//$this->Password = 'password'; // miles@123 for b2b

	}
	function credentials($service)
	{
		switch ($service) {
			case 'Search':
				$this->service_url = $this->Url . 'Search';
				break;
			case 'FareRule':
				$this->service_url = $this->Url . 'FareRule';
				break;
			case 'UpdateFareQuote':
				$this->service_url = $this->Url . 'UpdateFareQuote';
				break;
			case 'ExtraServices':
				$this->service_url = $this->Url . 'ExtraServices';
				break;
			case 'HoldTicket':
				$this->service_url = $this->Url . 'HoldTicket';
				break;
			case 'CommitBooking':
				$this->service_url = $this->Url . 'CommitBooking';
				break;
			case 'IssueHoldTicket':
				$this->service_url = $this->Url . 'IssueHoldTicket';
				break;
			case 'CancelBooking':
				$this->service_url = $this->Url . 'CancelBooking';
				break;
			case 'GetCalendarFare':
				$this->service_url = $this->Url . 'GetCalendarFare';
				break;
			case 'UpdateCalendarFareOfDay':
				$this->service_url = $this->Url . 'UpdateCalendarFareOfDay';
				break;
			case 'BookingDetails':
					$this->service_url = $this->Url . 'BookingDetails';
					break;
			case 'TicketRefundDetails':
				$this->service_url = $this->Url . 'TicketRefundDetails';
				break;
		}
	}
	/*
	 *
	 *Convert Object To Array
	 *
	 */
	public function objectToArray($d)
	{
		if (is_object($d)) {
			$d = get_object_vars($d);
		}

		if (is_array($d)) {
			return array_map(array($this, 'objectToArray'), $d);
		}
		else {
			return $d;
		}
	}
	/**
	 *  Arjun J Gowda
	 *
	 * TBO auth token will be returned
	 */
	public function get_authenticate_token()
	{
		return $GLOBALS['CI']->session->userdata('tb_auth_token');
	}
	/**
	 * request Header
	 */
	private function get_header()
	{
		$response['UserName']=$this->UserName;
		$response['Password']=$this->Password;
		$response['DomainKey']=$this->DomainKey;
		$response['system']=$this->system;
		return $response;
	}

	/**
	 *get Flight search request details
	 *@param array $search_params data to be used while searching of flight
	 */
	function flight_search_request($search_params)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		/** Request to be formed for search **/
		$this->credentials('Search');
		$request_params = array();
		//Converting to an array
		$search_params['from'] = (is_array($search_params['from']) ? $search_params['from'] : array($search_params['from']));
		$search_params['to'] = (is_array($search_params['to']) ? $search_params['to'] : array($search_params['to']));
		$search_params['depature'] = (is_array($search_params['depature']) ? $search_params['depature'] : array($search_params['depature']));
		$search_params['return'] = (is_array($search_params['return']) ? $search_params['return'] : array($search_params['return']));
		$segments = array();
		for($i=0; $i<count($search_params['from']); $i++){
			$segments[$i]['Origin'] = $search_params['from'][$i];
			$segments[$i]['Destination'] = $search_params['to'][$i];
			$segments[$i]['DepartureDate'] = $search_params['depature'][$i];
			if($search_params['type'] == 'Return') {
				$segments[$i]['ReturnDate'] = $search_params['return'][$i];
			}
		}
		$request_params['AdultCount'] =			$search_params['adult'];
		$request_params['ChildCount'] = 		$search_params['child'];
		$request_params['InfantCount'] =		$search_params['infant'];
		$request_params['JourneyType'] = 		$search_params['type'];
		$request_params['PreferredAirlines'] = 	array($search_params['carrier']);
		$request_params['CabinClass'] = 		$search_params['v_class'];
		
		$request_params['Segments'] = $segments;
		$response['data']['request'] = json_encode($request_params);
		$response['data']['service_url'] = $this->service_url;			
		return $response;
	}

	/**
	 * get fare rules request
	 * @param $data_key		data to be used in the result index - comes from search result
	 * @param $search_key	session id of the search  -  session identifies each search
	 */
	function fare_details_request($data_key, $search_key)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$request_params = array();
		$this->credentials('FareRule');
		if (empty($data_key) == false) {
			$request_params['ResultToken'] = $search_key;
		} else {
			$response['status']	= FAILURE_STATUS;
		}
		$response['data']['request']		= json_encode($request_params);
		$response['data']['service_url']		= $this->service_url;
		return $response;
	}

	/**
	 * get fare quote request
	 * @param $data_key		data to be used in the result index - comes from search result
	 * @param $search_key	session id of the search  -  session identifies each search
	 */
	function fare_quote_request($data_key, $search_key)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$request_params = array();
		$this->credentials('UpdateFareQuote');
		if (empty($data_key) == false) {
			$request_params['ResultToken'] = $search_key;
		} else {
			$response['status']	= FAILURE_STATUS;
		}
		$response['data']['request']= json_encode($request_params);
		$response['data']['service_url']= $this->service_url;
		return $response;
	}
	/**
	 * extra service request
	 * @param $data_key		data to be used in the result index - comes from search result
	 * @param $search_key	session id of the search  -  session identifies each search
	 */
	function extra_services_request($data_key, $search_key)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$request_params = array();
		$this->credentials('ExtraServices');
		if (empty($data_key) == false) {
			$request_params['ResultToken'] = $search_key;
		} else {
			$response['status']	= FAILURE_STATUS;
		}
		$response['data']['request']= json_encode($request_params);
		$response['data']['service_url']= $this->service_url;
		return $response;
	}
	/**
	 * Create Booking Request
	 * @param array $booking_params
	 */
	private function commit_booking_request($booking_params, $app_reference)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$request_params = array();
		$this->credentials('CommitBooking');
		$request_params['AppReference'] = trim($app_reference);
		$request_params['SequenceNumber'] = $booking_params['SequenceNumber'];
		$request_params['ResultToken'] = $booking_params['ProvabAuthKey'];
		$request_params['Passengers'] = $booking_params['Passenger'];
		$response['data']['request']		= json_encode($request_params);
		$response['data']['service_url']		= $this->service_url;
		return $response;
	}
	/**
	 * Hold Booking Request
	 * @param array $booking_params
	 */
	private function hold_booking_request($booking_params, $app_reference)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$request_params = array();
		$this->credentials('HoldTicket');
		$request_params['AppReference'] = trim($app_reference);
		$request_params['SequenceNumber'] = $booking_params['SequenceNumber'];
		$request_params['ResultToken'] = $booking_params['ProvabAuthKey'];
		$request_params['Passengers'] = $booking_params['Passenger'];
		$response['data']['request']		= json_encode($request_params);
		$response['data']['service_url']		= $this->service_url;
		return $response;
	}
	/**
	 * Jaganath- Cancellation Request
	 * Request Format For CancelBooking Method
	 * @param $cancell_request_params
	 */
	function cancel_booking_request($cancell_request_params, $app_reference)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$this->credentials('CancelBooking');
		$request_params = array();
		$request_params['AppReference'] = $app_reference;
		$request_params['SequenceNumber'] = $cancell_request_params['SequenceNumber'];
		$request_params['BookingId'] = $cancell_request_params['BookingId'];
		$request_params['PNR'] = 		$cancell_request_params['PNR'];
		$request_params['TicketId'] = $cancell_request_params['TicketId'];
		$request_params['IsFullBookingCancel'] = $cancell_request_params['IsFullBookingCancel'];
		$response['data']['request']		= json_encode($request_params);
		$response['data']['service_url']		= $this->service_url;
		return $response;
	}
	/**
	 * Jaganath
	 * Request For getting cancellation Refund details
	 * @param $request_data
	 */
	function ticket_refund_details_request($request_data)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$this->credentials('TicketRefundDetails');
		$request_params = array();
		$request_params['AppReference'] = 		$request_data['AppReference'];
		$request_params['SequenceNumber'] = 	$request_data['SequenceNumber'];
		$request_params['BookingId'] = 			$request_data['BookingId'];
		$request_params['PNR'] = 				$request_data['PNR'];
		$request_params['TicketId'] = 			$request_data['TicketId'];
		$request_params['ChangeRequestId'] =	$request_data['ChangeRequestId'];
		
		$response['data']['request']		= json_encode($request_params);
		$response['data']['service_url']		= $this->service_url;
		return $response;
	}
	/**
	 * Get Booking Details Request
	 * @param string $book_id
	 * @param string $pnr
	 * @param string $booking_source
	 */
	function booking_details_request($book_id, $pnr)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$request_params = array();
		$this->credentials('GetBookingDetails');
		$request_params['BookingId']= $book_id;
		$request_params['PNR']= $pnr;
		$response['data']['request']		= json_encode($request_params);
		$response['data']['service_url']		= $this->service_url;
		return $response;
	}

	//****************************************************************************
	/**
	* Fare calendar request
	* @param array $search_params
	*/
	function calendar_fare_request($search_params)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$request_params = array();
		$this->credentials('GetCalendarFare');
		//Segments
		$segments = array();
		$segments['Origin'] = $search_params['from'];
		$segments['Destination'] = $search_params['to'];
		$segments['CabinClass'] = $search_params['cabin'];
		$segments['DepartureDate'] = $search_params['depature'];

		$request_params['JourneyType'] = $search_params['trip_type'];
		$request_params['Segments'] = $segments;
		$request_params['PreferredAirlines'] = $search_params['carrier'];
		$response['data']['request']		= json_encode($request_params);
		$response['data']['service_url']		= $this->service_url;
		return $response;
	}

	/**
	 * Day Fare Request
	 * @param $search_params
	 */
	function day_fare_request($search_params)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$request_params = array();
		//$search_params['adult']
		$this->credentials('UpdateCalendarFareOfDay');
		//Segments
		$segments = array();
		$segments['Origin'] = $search_params['from'];
		$segments['Destination'] = $search_params['to'];
		$segments['CabinClass'] = $search_params['cabin'];
		$segments['DepartureDate'] = $search_params['depature'];

		$request_params['JourneyType'] = $search_params['trip_type'];
		$request_params['Segments'] = $segments;
		$request_params['PreferredAirlines'] = $search_params['carrier'];
		$response['data']['request']		= json_encode($request_params);
		$response['data']['service_url']		= $this->service_url;
		return $response;
	}

	/**
	 * Calendar Fare
	 * @param $search_params
	 */
	function get_fare_list($search_params)
	{
		$response['data'] = array();
		$response['status'] = true;
		$header_info = $this->get_header();
		//get request
		$search_request = $this->calendar_fare_request($search_params);
		//get data
		if ($search_request['status']) {
			//$this->CI->custom_db->generate_static_response(json_encode($search_request['data']));
			$search_response = $this->CI->api_interface->get_json_response($search_request['data']['service_url'], $search_request['data']['request'], $header_info);
			//$this->CI->custom_db->generate_static_response(json_encode($search_response));
			//$search_response = $GLOBALS['CI']->flight_model->get_static_response(526);
			if ($this->valid_api_response($search_response)) {
				$response['data'] = $search_response['GetCalendarFare'];
			} else {
				$response['status'] = false;
			}
		} else {
			$response['status'] = false;
		}

		return $response;
	}

	/**
	 * Calendar Day Fare
	 */
	function get_day_fare($search_params)
	{
		$response['data'] = array();
		$response['status'] = true;
		$header_info = $this->get_header();
		//get request
		$search_request = $this->day_fare_request($search_params);
		//get data
		if ($search_request['status']) {
			//$this->CI->custom_db->generate_static_response(json_encode($search_request['data']));
			$search_response = $this->CI->api_interface->get_json_response($search_request['data']['service_url'], $search_request['data']['request'], $header_info);
			//$this->CI->custom_db->generate_static_response(json_encode($search_response));
			//$search_response = $GLOBALS['CI']->flight_model->get_static_response(526);
			if ($this->valid_api_response($search_response)) {
				$response['data'] = $search_response['UpdateCalendarFareOfDay'];
			} else {
				$response['status'] = false;
			}
		} else {
			$response['status'] = false;
		}

		return $response;
	}

	/**
	 * Format for generic view
	 * @param array $raw_fare_list
	 */
	function format_cheap_fare_list($raw_fare_list, $strict_format=false)
	{

		$fare_list = array();
		$response['status'] = SUCCESS_STATUS;
		$response['data'] = $fare_list;
		$response['msg'] = '';
		$GetCalendarFareResult = $raw_fare_list['CalendarFareDetails'];
		if (valid_array($GetCalendarFareResult) == true) {
			$LowestFareOfDayInMonth = $GetCalendarFareResult;
			foreach ($LowestFareOfDayInMonth as $k => $day_fare) {
				if (valid_array($day_fare) == true) {
					$fare_list_obj['airline_code'] = $day_fare['AirlineCode'];
					$fare_list_obj['airline_icon'] = SYSTEM_IMAGE_DIR.'airline_logo/'.$day_fare['AirlineCode'].'.gif';
					$fare_list_obj['airline_name'] = $day_fare['AirlineName'];

					$fare_list_obj['departure_date'] = local_date($day_fare['DepartureDate']);
					$fare_list_obj['departure_time'] = local_time($day_fare['DepartureDate']);
					$fare_list_obj['departure'] = $day_fare['DepartureDate'];
					$fare_list_obj['BaseFare'] = $day_fare['BaseFare'];//Base Fare
					$fare_list_obj['tax'] = $day_fare['Tax']+$day_fare['FuelSurcharge'];
				} else {
					$fare_list_obj = false;
				}
				if (valid_array($day_fare) == true) {
					$fare_list[db_current_datetime(add_days_to_date(0, $day_fare['DepartureDate']))] = $fare_list_obj;
				}
			}
			$response['data'] = $fare_list;
		} else {
			$response['status'] = FAILURE_STATUS;
		}
		return $response;
	}

	/**
	 * Format for generic view
	 * @param array $raw_fare_list
	 */
	function format_day_fare_list($raw_fare_list)
	{
		$fare_list = array();
		$response['status'] = SUCCESS_STATUS;
		$response['data'] = $fare_list;
		$response['msg'] = '';
		$UpdateCalendarFareOfDayResult = $raw_fare_list['CalendarFareDetails'];
		if (valid_array($UpdateCalendarFareOfDayResult) == true) {
			$CheapestFareOfDay = $UpdateCalendarFareOfDayResult;
			foreach ($CheapestFareOfDay as $k => $day_fare) {
				if (valid_array($day_fare) == true) {
					$fare_list_obj['airline_code'] = $day_fare['AirlineCode'];
					$fare_list_obj['airline_icon'] = SYSTEM_IMAGE_DIR.'airline_logo/'.$day_fare['AirlineCode'].'.gif';
					$fare_list_obj['airline_name'] = $day_fare['AirlineName'];

					$fare_list_obj['departure_date'] = local_date($day_fare['DepartureDate']);
					$fare_list_obj['departure_time'] = local_time($day_fare['DepartureDate']);
					$fare_list_obj['departure'] = $day_fare['DepartureDate'];
					$fare_list_obj['BaseFare'] = $day_fare['BaseFare'];//Base Fare
					$fare_list_obj['tax'] = $day_fare['Tax']+$day_fare['FuelSurcharge'];
				} else {
					$fare_list_obj = false;

				}
				$fare_list[db_current_datetime(add_days_to_date($k, $day_fare['DepartureDate']))] = $fare_list_obj;
			}
			$response['data'] = $fare_list;
		} else {
			$response['status'] = FAILURE_STATUS;
		}
		return $response;
	}
	//****************************************************************************

	/**
	 * get search result from tbo
	 * @param number $search_id unique id which identifies search details
	 */
	function get_flight_list($search_id='')
	{
		$this->CI->load->driver('cache');
		$response['data'] = array();
		$response['status'] = true;
		$search_data = $this->search_data($search_id);
		$header_info = $this->get_header();
		//generate unique searchid string to enable caching
		$cache_search = $this->CI->config->item('cache_flight_search');
		$search_hash = $this->search_hash;
		if ($cache_search) {  
			$cache_contents = $this->CI->cache->file->get($search_hash);
		}
		if ($search_data['status'] == true) {

			if ($cache_search === false || ($cache_search === true && empty($cache_contents) == true)){
				//get request
				$search_request = $this->flight_search_request($search_data['data']); 
				#print_r($search_request);die();
				//get data
				if ($search_request['status']) {
					$search_response = $this->CI->api_interface->get_json_response($search_request['data']['service_url'], $search_request['data']['request'], $header_info);
					#print_r($search_response);die();
					$this->CI->custom_db->generate_static_response(json_encode($search_response));
					
					//$search_response = $this->CI->flight_model->get_static_response(336);//336=>oneway;=>domestic roundway;=> Multicity
					
					if ($this->valid_api_response($search_response)) {
						$response['data'] = $search_response;
						$response['search_hash'] = $search_hash;
						$response['from_cache'] = false;
						if ($cache_search) {
							$cache_exp = $this->CI->config->item('cache_flight_search_ttl');
							$this->CI->cache->file->save($search_hash, $response['data'], $cache_exp);
						}
					} else {
						$response['status'] = false;
					}
				} else {
					$response['status'] = false;
				}
			} else {
				//read from cache
				$response['data'] = $cache_contents;
				$response['search_hash'] = $search_hash;
				$response['from_cache'] = true;
			}
		} else {
			$response['status'] = false;
		}
		return $response;
	}

	/**
	 * Get Fare Details based on fare key
	 * @param array	 $data_row			 data row of the result
	 * @param string $search_session_key search session key
	 */
	function get_fare_details($data_row, $search_session_key)
	{
		$response['data'] = array();
		$response['status'] = FAILURE_STATUS;
		$api_request = $this->fare_details_request($data_row, $search_session_key);
		//get data
		if ($api_request['status']) {
			$header_info = $this->get_header();
			$api_response = $this->CI->api_interface->get_json_response($api_request['data']['service_url'], $api_request['data']['request'], $header_info);
			
			//$this->CI->custom_db->generate_static_response(json_encode($api_response));
			//$api_response = $this->CI->flight_model->get_static_response(35);
			if ($this->valid_api_response($api_response)) {
				$response['data'] = $api_response['FareRule']['FareRuleDetail'];
				$response['status'] = SUCCESS_STATUS;
			}
		}
		return $response;
	}

	/**
	 * Get Fare Quote Details
	 * @param array $flight_booking_details
	 */
	function fare_quote_details($flight_booking_details)
	{	
		$response['status'] = SUCCESS_STATUS; // update
		extract($flight_booking_details);
		$unique_search_access_key = array_unique($flight_booking_details['search_access_key']);
		if (count($unique_search_access_key) == 1) {
			//single request - all search except domestic round way uses this
			if (count($flight_booking_details['search_access_key']) == 1) {
				$tmp_token = $this->run_fare_quote(array($flight_booking_details['token'][0]), $flight_booking_details['search_access_key'][0]);
				
				$this->update_fare_quote_details($flight_booking_details, 0, $tmp_token['data'], $tmp_token['status'], $response);
			} elseif (count($flight_booking_details['search_access_key']) == 2) {
				//(domestic and round)
				//---Merge both and send single key
				echo 'Under Construction - Arjun';
				exit;
			}
		} else {
			//multiple request - domestic round way uses this T1 - R1, T2 - R2
			foreach ($flight_booking_details['token'] as $___k => $___v) {
				if ($response['status'] == SUCCESS_STATUS) {
					//If LCC THEN RUN ELSE JUST UPDATE SAME VALUE IF NEEDED
					$tmp_token = $this->run_fare_quote(array($___v), $flight_booking_details['search_access_key'][$___k]);
					$this->update_fare_quote_details($flight_booking_details, $___k, $tmp_token['data'], $tmp_token['status'], $response);

				}
			}

		}

		//Update response with the data returned - $flight_booking_details
		$response['data']	= $flight_booking_details;

		if (count($unique_search_access_key) != 1) {
			/*foreach($response['token'] as $k=>$v){

			$response['data']['token'][$k]['ProvabAuthKey']=$v['ProvabAuthKey'];
			$response['ProvabAuthKey']="return";   // remove this for testing
			}*/
			unset($response['token']);
		}

		return $response;
	}
	public function get_extra_services($flight_booking_details)
	{
		echo 'Under working: get_extra_services';exit;
		$response['status'] = FAILURE_STATUS;
		$extra_services = array();
		extract($flight_booking_details);
		$unique_search_access_key = array_unique($flight_booking_details['search_access_key']);
		if (count($unique_search_access_key) == 1) {
			//single request - all search except domestic round way uses this
			if (count($flight_booking_details['search_access_key']) == 1) {
				$extra_services = $this->run_extra_services(array($flight_booking_details['token'][0]), $flight_booking_details['search_access_key'][0]);
			}
		} else {
			//multiple request - domestic round way
			foreach ($flight_booking_details['token'] as $___k => $___v) {
				if ($response['status'] == SUCCESS_STATUS) {
					$extra_services = $this->run_extra_services(array($___v), $flight_booking_details['search_access_key'][$___k]);
					echo 'Roundway Extra services: Under working';
					debug($extra_services);exit;
				}
			}

		}
		
		//Update response with the data returned - $flight_booking_details
		$response['data']	= $flight_booking_details;

		if (count($unique_search_access_key) != 1) {
			unset($response['token']);
		}
		return $response;
	}
	/**
	 * Check if ticketed
	 * @param array $api_response
	 * @return boolean|string
	 */
	private function format_ticket_response($api_response)
	{
		$response['status'] = BOOKING_FAILED;//Master Booking status
		$response['data'] = array();
		$response['message'] = '';
		$ticket_details = array();
		if (valid_array($api_response) == true) {
			foreach ($api_response as $k => $v) {
				if($v['status'] == SUCCESS_STATUS) {
					$ticket_details[$k]['data'] = $v['data'];
					$ticket_details[$k]['status'] = SUCCESS_STATUS;
					$response['status'] = SUCCESS_STATUS;//DONT CHANGE(single ticket can be successfull)
				} else {
					$ticket_details[$k]['data'] = $v['data'];
					$ticket_details[$k]['status'] = $v['status'];
				}
			}
			$response['data']['TicketDetails'] = $ticket_details;
		}
		return $response;
	}

	/**
	 *
	 * @param array	 $flight_booking_details	flight booking details passed with reference
	 * @param number $index 					index to be updated
	 * @param array	 $new_quote_details			new details to be updated to index
	 */
	function update_fare_quote_details(& $flight_booking_details, $index, $new_quote_details, $process_quote_request, & $response)
	{

		if ($process_quote_request != FAILURE_STATUS) {
			$flight_booking_details['token'][$index]		= $new_quote_details;
			$flight_booking_details['token_key'][$index]	= serialized_data($flight_booking_details['token'][$index]);
		} else {
			$response['status'] = FAILURE_STATUS;
		}
	}

	/**
	 * @param array $data_key
	 * @param string $search_access_key
	 */
	private function run_fare_quote($data_key, $search_access_key)
	{
		$response['data'] = array();
		$response['status'] = FAILURE_STATUS;
		$api_request = $this->fare_quote_request($data_key, $search_access_key);
		
		if ($api_request['status']) {
			$header_info = $this->get_header();
			$api_response = $this->CI->api_interface->get_json_response($api_request['data']['service_url'], $api_request['data']['request'], $header_info);
			//$this->CI->custom_db->generate_static_response(json_encode($api_response));
			//$api_response = $this->CI->flight_model->get_static_response(616);//38//668//1022
			if ($this->valid_api_response($api_response)) {
				$response['data'] = $api_response['UpdateFareQuote']['FareQuoteDetails']['JourneyList'];
				$response['status'] = SUCCESS_STATUS;
			}
		}
		return $response;
	}
	/**
	 * @param array $data_key
	 * @param string $search_access_key
	 */
	private function run_extra_services($data_key, $search_access_key)
	{
		$response['data'] = array();
		$response['status'] = FAILURE_STATUS;
		$api_request = $this->extra_services_request($data_key, $search_access_key);
		//get data
		if ($api_request['status']) {
			$header_info = $this->get_header();
			
			//$api_response = $this->CI->api_interface->get_json_response($api_request['data']['service_url'], $api_request['data']['request'], $header_info);
			//$this->CI->custom_db->generate_static_response(json_encode($api_response));//later
			
			$api_response = $this->CI->flight_model->get_static_response(902);//901
			if ($this->valid_api_response($api_response)) {
				$response['data']['ExtraServiceDetails'] = $this->extra_services_in_preferred_currency($api_response['ExtraServices']['ExtraServiceDetails']);
				$response['status'] = SUCCESS_STATUS;
			}
		}
		return $response;
	}
	/**
	 * Wrapper - 1 for booking
	 * Arjun J Gowda
	 *Process Booking
	 * @param array $booking_params
	 */
	public function process_booking($book_id, $booking_params)
	{
		//Adding SequenceNumber
		foreach($booking_params['token']['token'] as $k => $v) {
			$booking_params['token']['token'][$k]['SequenceNumber'] = $k;
		}
		$response['status'] = SUCCESS_STATUS;
		$wrapper_token = $booking_params['token'];
		$book_response = array();
		$book_response = $this->book_flight($book_id, $booking_params);
		if ($book_response['status'] == FAILURE_STATUS) {
			$response['status'] = FAILURE_STATUS;
			$response['message'] = $book_response['message'];
		} else {
			$ticket_response = $book_response;
			$response['status'] = $ticket_response['status'];
		}
		//Extracting Response
		$response['data']['ticket']['TicketDetails'] = @$ticket_response['data'];
		$response['data']['book_id'] = $book_id;
		$response['data']['booking_params'] = $booking_params;
		
		return $response;
	}
	/**
	 * Do Booking of Flight
	 * @param $book_id
	 * @param $booking_params
	 */
	function book_flight($book_id, $booking_params)
	{
		$response['status'] = FAILURE_STATUS;
		$booking_response = array();
		$token_wrapper = $booking_params['token'];
		$op = $booking_params['op'];
		$passenger = $this->extract_passenger_info($booking_params);
		//check ONE WAY - Domestic / Intl & ROUND WAY - Intl - Run Once
		$unique_search_access_key = array_unique($token_wrapper['search_access_key']);
			
		if (count($unique_search_access_key) == 1) { // Single session is one request
			if (count($token_wrapper['search_access_key']) == 1) {
				
				if(isset($booking_params['ticket_method']) &&  $booking_params['ticket_method'] === 'hold_ticket'){//HOLD TICKET
					$tmp_res = $this->run_hold_booking($op, $book_id, $token_wrapper['token'][0], $passenger, $token_wrapper['search_access_key'][0]);
				} else {//DIRECT TICKETING
					$tmp_res = $this->run_commit_booking($op, $book_id, $token_wrapper['token'][0], $passenger, $token_wrapper['search_access_key'][0]);
				}
				

				if ($this->valid_flight_booking_status($tmp_res['status']) == true) {
					$booking_response[] = $tmp_res['data'];
					$response['status'] = $tmp_res['status'];
				} else {
					$response['message'] = $tmp_res['message'];
					$response['status'] = FAILURE_STATUS;
				}
			}
		} else { // multiple request is two request
			//Domestic Round - Run Twice
			foreach ($token_wrapper['token'] as $___k => $___v) {
				$tmp_resp = $this->run_commit_booking($op, $book_id, $___v, $passenger, $token_wrapper['search_access_key'][$___k]);
				if ($this->valid_flight_booking_status($tmp_resp['status']) == true) {
					
					$booking_response[$___k] = $tmp_resp['data'];
					
					if($response['status'] != BOOKING_CONFIRMED){
						$response['status'] = $tmp_resp['status'];
					}
				} else {
					$booking_response[$___k]['Status'] = $tmp_resp['status'];
					$booking_response[$___k]['Message'] = $tmp_resp['message'];
					$response['message'] = @$booking_response[$___k]['message'];
					if ($this->valid_flight_booking_status($response['status']) == false) {//Even if one booking is Hold/Success, return the status as Hold/Success
						$response['status'] = FAILURE_STATUS;
					}
					break;
				}
			}
		}
		$response['data'] = $booking_response;
		return $response;
	}
	/**
	 * Booking status is valid or not
	 * @param unknown_type $booking_status
	 */
	private function valid_flight_booking_status($booking_status)
	{
		if(in_array($booking_status, array(BOOKING_CONFIRMED, BOOKING_HOLD)) == true){
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Book Flight
	 * @param $book_id			temporary book id used to make payment :p
	 * @param $booking_params	all the booking data wrapped in array
	 */
	function run_commit_booking($op, $book_id, $token, $passenger, $search_access_key)
	{
		$response['data'] = array();
		$response['status'] = FAILURE_STATUS;
		$response['message'] = '';
		$SequenceNumber = $token['SequenceNumber'];
		$booking_params['Passenger']			= $this->WSPassenger($passenger);
		//Prova Auth key
		$booking_params['ProvabAuthKey']		= $token['ProvabAuthKey'];
		$booking_params['SequenceNumber']		= $SequenceNumber;
		$api_request = $this->commit_booking_request($booking_params, $book_id);
		//get data
		if ($api_request['status']) {
			$header_info = $this->get_header();
			
			$this->CI->custom_db->generate_static_response(json_encode($api_request['data']['request']));
			$api_response = $this->CI->api_interface->get_json_response($api_request['data']['service_url'], $api_request['data']['request'], $header_info);
			$this->CI->custom_db->generate_static_response(json_encode($api_response));
			
			//$static_id = 	378;
			//$api_response = $this->CI->flight_model->get_static_response($static_id);//378
			
			if ($this->valid_commit_booking_response($api_response) == true) {
				$api_response['CommitBooking']['BookingDetails']['Price'] = $this->convert_bookingdata_to_application_currency($api_response['CommitBooking']['BookingDetails']['Price']);
				$response['data'] = $api_response;
				$response['status'] = $api_response['Status'];
			} else {
				$response['message'] = @$api_response['Message'];
				$response['status'] = FAILURE_STATUS;
			}
		}
		/** PROVAB LOGGER **/
		$GLOBALS['CI']->private_management_model->provab_xml_logger('Commit Booking', $book_id, 'flight', json_encode($api_request['data']), json_encode($api_response));
		return $response;
	}
	/**
	 * Hold Ticket
	 * @param $book_id			temporary book id used to make payment :p
	 * @param $booking_params	all the booking data wrapped in array
	 */
	function run_hold_booking($op, $book_id, $token, $passenger, $search_access_key)
	{
		$response['data'] = array();
		$response['status'] = FAILURE_STATUS;
		$response['message'] = '';
		$SequenceNumber = $token['SequenceNumber'];
		$booking_params['Passenger']			= $this->WSPassenger($passenger);
		//Prova Auth key
		$booking_params['ProvabAuthKey']		= $token['ProvabAuthKey'];
		$booking_params['SequenceNumber']		= $SequenceNumber;
		$api_request = $this->hold_booking_request($booking_params, $book_id);
		//get data
		if ($api_request['status']) {
			$header_info = $this->get_header();
			
			$this->CI->custom_db->generate_static_response(json_encode($api_request['data']['request']));
			
			$api_response = $this->CI->api_interface->get_json_response($api_request['data']['service_url'], $api_request['data']['request'], $header_info);
			$this->CI->custom_db->generate_static_response(json_encode($api_response));
			
			/*$static_id = 	1198;
			$api_response = $this->CI->flight_model->get_static_response($static_id);//378*/
			
			if ($this->valid_commit_booking_response($api_response) == true) {
				$api_response['CommitBooking'] = $api_response['HoldTicket'];
				unset($api_response['HoldTicket']);
				$api_response['CommitBooking']['BookingDetails']['Price'] = $this->convert_bookingdata_to_application_currency($api_response['CommitBooking']['BookingDetails']['Price']);
				
				$response['data'] = $api_response;
				$response['status'] = $api_response['Status'];
			} else {
				$response['message'] = @$api_response['Message'];
				$response['status'] = FAILURE_STATUS;
			}
		}
		/** PROVAB LOGGER **/
		$GLOBALS['CI']->private_management_model->provab_xml_logger('Hold Booking', $book_id, 'flight', json_encode($api_request['data']), json_encode($api_response));
		return $response;
	}
	
	/**
	 * Forms a group based on passenger origin and transaction_fk
	 * @param $booking_details
	 * @param $passenger_origin
	 */
	function group_cancellation_passenger_ticket_id($booking_details, $passenger_origin)
	{
		$booking_details = $booking_details['booking_details'][0];
		$booking_transaction_details = $booking_details['booking_transaction_details'];
		$indexed_passenger_ticket_id = array();
		$indexed_passenger_origin = array();
		foreach ($booking_transaction_details as $tk => $tv){
			$booking_customer_details = $tv['booking_customer_details'];
			foreach ($booking_customer_details as $ck => $cv){
				if(in_array($cv['origin'], $passenger_origin) == true){
					$indexed_passenger_ticket_id[$tv['origin']][$ck] = (int)$cv['TicketId'];//Ticket Ids
					$indexed_passenger_origin[$tv['origin']][$ck] = $cv['origin'];//Passenger Origin
				}
			}
			if(isset($indexed_passenger_ticket_id[$tv['origin']])){
				$indexed_passenger_ticket_id[$tv['origin']] = array_values($indexed_passenger_ticket_id[$tv['origin']]);
				$indexed_passenger_origin[$tv['origin']] = array_values($indexed_passenger_origin[$tv['origin']]);
			}
		}
		return array('passenger_origin' => $indexed_passenger_origin, 'passenger_ticket_id' => $indexed_passenger_ticket_id);
	}
	/**
	 * Jaganath
	 * Flight Booking Cancel
	 * @param $master_booking_details
	 * @param $passenger_origin => $passenger_origin indexed with Transaction Origin
	 * @param $passenger_ticket_id => Ticket Ids indexed with Transaction Origin
	 */
	function cancel_booking($master_booking_details, $passenger_origin, $passenger_ticket_id)
	{
		$response['data'] = array();
		$response['status'] = FAILURE_STATUS;
		$response['message'] = '';
		$booking_details = $master_booking_details['booking_details']['0'];
		$app_reference = $booking_details['app_reference'];
		$booking_transaction_details =  $booking_details['booking_transaction_details'];
		$passenger_origins = array();//Change Request IDs
		foreach($booking_transaction_details as $transaction_details_k => $transaction_details_v) {
			$transaction_origin = $transaction_details_v['origin'];
			if(isset($passenger_ticket_id[$transaction_origin]) == true && valid_array($passenger_ticket_id[$transaction_origin]) == true){
				//If Ticket Ids exists for the Transaction, then run the cancel request for Requested Pax Tickets
				$pax_ticket_ids = $passenger_ticket_id[$transaction_origin];
				$pax_count = count($transaction_details_v['booking_customer_details']);
				$pax_cancel_count = count($pax_ticket_ids);
				if($pax_count == $pax_cancel_count){
					$IsFullBookingCancel = true;
				} else {
					$IsFullBookingCancel = false;
				}
				$api_booking_id = trim($transaction_details_v['book_id']);
				$pnr = trim($transaction_details_v['pnr']);
				$app_reference = trim($transaction_details_v['app_reference']);
				$cancell_request_params['SequenceNumber'] = (int)$transaction_details_v['sequence_number'];
				$cancell_request_params['BookingId'] = $api_booking_id;
				$cancell_request_params['PNR'] = $pnr;
				$cancell_request_params['TicketId'] = $pax_ticket_ids;
				$cancell_request_params['IsFullBookingCancel'] = $IsFullBookingCancel;
				$cancel_booking_request = $this->cancel_booking_request($cancell_request_params, $app_reference);
				if ($cancel_booking_request['status']) {
					$header_info = $this->get_header();
					$send_change_response = $this->CI->api_interface->get_json_response($cancel_booking_request['data']['service_url'], $cancel_booking_request['data']['request'], $header_info);
					$this->CI->custom_db->generate_static_response(json_encode($send_change_response));
					
					//$send_change_response = $this->CI->flight_model->get_static_response(536);//493=>success;492=>failed
					
					if(isset($send_change_response['Status']) == true && $send_change_response['Status'] == SUCCESS_STATUS) {
						$passenger_origins[$transaction_origin] = $passenger_origin[$transaction_origin];
					}
				}
			}
		}
		if(valid_array($passenger_origins) == true) {
			$response['status'] = SUCCESS_STATUS;
			$this->update_ticket_cancellation_status($app_reference, $passenger_origins);
		}
		return $response;
	}
	/**
	 * Jaganath
	 * Update the Cancelled Ticket Status
	 * @param unknown_type $app_reference
	 * @param unknown_type $passenger_ticket_id
	 * @param unknown_type $ChangeRequestIds
	 */
	public function update_ticket_cancellation_status($app_reference, $passenger_ticket_origins)
	{
		$booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference);
		$current_module = $GLOBALS['CI']->config->item('current_module');
		$GLOBALS['CI']->load->library('booking_data_formatter');
		$booking_details = $GLOBALS['CI']->booking_data_formatter->format_flight_booking_data($booking_details, $current_module);
		$booking_details = $booking_details['data']['booking_details']['0'];
		$booking_transaction_details =  $booking_details['booking_transaction_details'];
		foreach($booking_transaction_details as $transaction_details_k => $transaction_details_v) {
			$transaction_origin = $transaction_details_v['origin'];
			if(isset($passenger_ticket_origins[$transaction_origin]) == true && valid_array($passenger_ticket_origins[$transaction_origin]) == true){
				//If Ticket Ids exists for the Transaction, then run the get cancel status request
				$api_booking_id = trim($transaction_details_v['book_id']);
				$pnr = trim($transaction_details_v['pnr']);
				$app_reference = trim($transaction_details_v['app_reference']);
				$sequence_number = (int)$transaction_details_v['sequence_number'];
				
				$passenger_origins = array_values($passenger_ticket_origins[$transaction_origin]);
				foreach ($passenger_origins as $tick_k => $tick_v){
					$pax_origin = array_shift($passenger_origins);
					//Update Cancellation Status
					$booking_status = 'BOOKING_CANCELLED';
					$passenger_update_data = array();
					$passenger_update_data['status'] = $booking_status;
					$passenger_update_condition = array();
					$passenger_update_condition['origin'] = $pax_origin;
					$this->CI->custom_db->update_record('flight_booking_passenger_details', $passenger_update_data, $passenger_update_condition);
					echo $this->CI->db->last_query();echo '<br/>';
				}
				$GLOBALS['CI']->flight_model->update_flight_booking_transaction_cancel_status($transaction_origin);
			}
		}//End of Transaction Loop
		//Update Master Booking Status
		$GLOBALS['CI']->flight_model->update_flight_booking_cancel_status($app_reference);
	}
	/**
	 * Jaganath
	 * API request for getting Ticket cancellation status
	 * @param unknown_type $request_params
	 * @param unknown_type $app_reference
	 */
	public function get_supplier_ticket_refund_details($request_params)
	{
		$response['data'] = array();
		$response['status'] = SUCCESS_STATUS;
		$response['messge'] = '';
		$api_request = $this->ticket_refund_details_request($request_params);
		if ($api_request['status'] == true) {
			$header_info = $this->get_header();
			$api_response = $this->CI->api_interface->get_json_response($api_request['data']['service_url'], $api_request['data']['request'], $header_info);
			//$this->CI->custom_db->generate_static_response(json_encode($api_response));
			if ($this->valid_api_response($api_response) == true) {
				$response['data'] = $api_response['TicketRefundDetails'];
				$response['status'] = SUCCESS_STATUS;
			} else {
				$response['message'] = @$api_response['Message'];
				$status = empty($api_response['Status']) == true ? FAILURE_STATUS : $api_response['Status'];
				$response['status'] = $status;
			}
		}
		return $response;
	}
	/**
	 * Formates Passenger Info for Booking
	 * @param unknown_type $passenger
	 * @param unknown_type $passenger_token
	 */
	private function WSPassenger($passenger)
	{
		$tmp_passenger = array();
		$total_pax_count = count($passenger['passenger_type']);
		$i = 0;
		for ($i=0; $i<$total_pax_count; $i++)
		{
			$tmp_passenger[$i]['IsLeadPax'] = $passenger['lead_passenger'][$i];
			$tmp_passenger[$i]['Title'] = $passenger['name_title'][$i];
			$tmp_passenger[$i]['FirstName'] = ((strlen($passenger['first_name'][$i])<2) ? str_repeat($passenger['first_name'][$i], 2) : $passenger['first_name'][$i]);
			$tmp_passenger[$i]['LastName'] = ((strlen($passenger['last_name'][$i])<2)   ? str_repeat($passenger['last_name'][$i], 2)  : $passenger['last_name'][$i]);
			$tmp_passenger[$i]['PaxType'] = $passenger['passenger_type'][$i];
			$tmp_passenger[$i]['Gender'] = $passenger['gender'][$i];
			$tmp_passenger[$i]['DateOfBirth'] = date('Y-m-d', strtotime($passenger['date_of_birth'][$i]));

			if (empty($passenger['passport_number'][$i]) == false and empty($passenger['passport_expiry_date'][$i]) == false) {
				$tmp_passenger[$i]['PassportNumber'] = $passenger['passport_number'][$i];
				$tmp_passenger[$i]['PassportExpiry'] = $passenger['passport_expiry_date'][$i];
			} else {
				$tmp_passenger[$i]['PassportNumber'] = '';
				$tmp_passenger[$i]['PassportExpiry'] = null;
			}

			$tmp_passenger[$i]['CountryCode'] = $passenger['passenger_nationality'][$i];
			$tmp_passenger[$i]['CountryName'] = $passenger['billing_country_name'];
			$tmp_passenger[$i]['ContactNo'] = $passenger['passenger_contact'];
			$tmp_passenger[$i]['City'] = $passenger['billing_city'];
			$tmp_passenger[$i]['PinCode'] = $passenger['billing_zipcode'];
				
			$tmp_passenger[$i]['AddressLine1'] = $passenger['billing_address_1'];
			$tmp_passenger[$i]['AddressLine2'] = $passenger['billing_address_1'];
			$tmp_passenger[$i]['Email'] = $passenger['billing_email'];
			
			
			//Baggage
			if(isset($passenger['baggage'][0][$i]) == true && empty($passenger['baggage'][0][$i]) == false){
				$tmp_passenger[$i]['BaggageId'][0] = $passenger['baggage'][0][$i];
			}
			if(isset($passenger['baggage'][1][$i]) == true && empty($passenger['baggage'][1][$i]) == false){
				$tmp_passenger[$i]['BaggageId'][1] = $passenger['baggage'][1][$i];
			}
		}
		return $tmp_passenger;
	}
	/**
	 * Get Booking Details
	 * @param array $booking_details
	 */
	function extract_booking_details($booking_details=array())
	{
		if(valid_array($booking_details) == true && $booking_details['Status'] == SUCCESS_STATUS) {
			$data['pnr'] = $booking_details['Book']['BookingDetails']['PNR'];
			$data['booking_id'] = $booking_details['Book']['BookingDetails']['BookingId'];
			return $data;
		}
	}

	/**
	 * get only passenger info from booking form
	 * @param $booking_params
	 */
	private function extract_passenger_info($booking_params)
	{
		$country_list = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'iso_country_code'));
		//$city_list = $GLOBALS['CI']->db_cache_api->get_city_list();
		$passenger['lead_passenger']		= $booking_params['lead_passenger'];
		foreach ($booking_params['name_title'] as $__k => $__v) {
			$passenger['name_title'][$__k]	= @get_enum_list('title', $__v);
		}
		$passenger['first_name']			= $booking_params['first_name'];
		//$passenger['middle_name']			= $booking_params['middle_name'];
		$passenger['last_name']				= $booking_params['last_name'];
		$passenger['date_of_birth']			= $booking_params['date_of_birth'];
		foreach ($booking_params['passenger_type'] as $__k => $__v) {
			$passenger['passenger_type'][$__k]		= $this->pax_type($__v);
		}
		foreach ($booking_params['gender'] as $__k => $__v) {
			$gender		= (isset($__v) ? get_enum_list('gender', $__v) : '');
			$passenger['gender'][$__k] = $this->gender_type($gender);
		}
		foreach ($booking_params['passenger_nationality'] as $__k => $__v) {
			$passenger['passenger_nationality'][$__k]	= (isset($country_list[$__v]) ? $country_list[$__v] : '');
		}

		foreach ($booking_params['passenger_passport_issuing_country'] as $__k => $__v) {
			$passenger['passenger_passport_issuing_country'][$__k]	= (isset($country_list[$__v]) ? $country_list[$__v] : '');
		}
		//$passenger['passport_number'] = $booking_params['passenger_passport_number'];
		$passenger['passport_number'] = preg_replace('/\s+/', '', $booking_params['passenger_passport_number']);
		
		
		foreach ($passenger['passport_number'] as $__k => $__v) {
			if (empty($__v) == false) {
				//FIXME
				$pass_date = strtotime($booking_params['passenger_passport_expiry_year'][$__k].'-'.$booking_params['passenger_passport_expiry_month'][$__k].'-'.$booking_params['passenger_passport_expiry_day'][$__k]);
				$passenger['passport_expiry_date'][$__k]	= date('Y-m-d', $pass_date);
			} else {
				$passenger['passport_expiry_date'][$__k]	= '';
			}
		}
		
		//Baggage
		if(isset($booking_params['onward_baggage']) == true && empty($booking_params['onward_baggage']) == false){
			foreach ($booking_params['onward_baggage'] as $__k => $__v) {
				$passenger['baggage'][0][$__k] = $__v;
			}
		}
		if(isset($booking_params['return_baggage']) == true && empty($booking_params['return_baggage']) == false){
			foreach ($booking_params['return_baggage'] as $__k => $__v) {
				$passenger['baggage'][1][$__k] = $__v;
			}
		}
		$passenger['billing_country'] = 'INDIA';//FIXME: Make it Dynamic;
		/*$passenger['billing_country'] = $country_list[$booking_params['billing_country']];*/
		$passenger['billing_country_name'] = 'India';//FIXME: Make it Dynamic
		//$passenger['billing_city'] = $city_list[$booking_params['billing_city']];
		$passenger['billing_city'] = $booking_params['billing_city'];
		$passenger['billing_zipcode'] = $booking_params['billing_zipcode'];
		/*$passenger['billing_email'] = $booking_params['billing_email'];*/
		$passenger['billing_email'] = 'admin@daltab.com';//FIXME: Make it Dynamic
		$passenger['billing_address_1'] = $booking_params['billing_address_1'];
		/*$passenger['passenger_contact'] = $booking_params['passenger_contact'];*/
		$passenger['passenger_contact'] = '9875563210';//FIXME: Make it Dynamic
		return $passenger;
	}

	private function pax_type($pax_type)
	{
		switch (strtoupper($pax_type))
		{
			case 'ADULT' : $pax_type = "1";
			break;
			case 'CHILD' : $pax_type = "2";
			break;
			case 'INFANT' : $pax_type = "3";
			break;
		}
		return $pax_type;
	}
	private function gender_type($pax_type)
	{
		switch (strtoupper($pax_type))
		{
			case 'MALE' : $pax_type = "1";
			break;
			case 'FEMALE' : $pax_type = "2";
		}
		return $pax_type;
	}

	private function tbo_source_enum($source)
	{
		switch($source)
		{
			case 'WorldSpan' : $source = 0;
			break;
			case 'Abacus' : $source = 1;
			break;
			case 'SpiceJet' : $source = 2;
			break;
			case 'Amadeus' : $source = 3;
			break;
			case 'Galileo' : $source = 4;
			break;
			case 'Indigo' : $source = 5;
			break;
			case 'Paramount' : $source = 6;
			break;
			case 'AirDeccan' : $source = 7;
			break;
			case 'MDLR' : $source = 8;
			break;
			case 'GoAir' : $source = 9;
			break;
		}
		return  $source;
	}
	/**
	 * TBO SOurce Name
	 * @param unknown_type $source
	 */
	private function get_tbo_source_name($source)
	{
		switch($source)
		{
			case 0 : $source = 'WorldSpan';
			break;
			case  1 : $source = 'Abacus';
			break;
			case  3: $source = 'SpiceJet';
			break;
			case  4 : $source = 'Amadeus';
			break;
			case 5 : $source = 'Galileo';
			break;
			case 6 : $source = 'Indigo';
			break;
			/*case 6 : $source = 'Paramount';
			 break;*/
			/*case 7 : $source = 'AirDeccan';
			 break;*/
			/*case  8 : $source = 'MDLR';
			 break;*/
			case 10: $source = 'GoAir';
			break;
			case 19 : $source = 'AirAsia';
			break;
			case 13 : $source = 'AirArabia';
			break;
			case 17 : $source = 'FlyDubai';
			break;
			case 14 : $source = 'AirIndiaExpress';
			break;
			case 46 : $source = 'AirCosta';
			break;
			case 48 : $source = 'BhutanAirlines';
			break;
			case 49 : $source = 'AirPegasus';
			break;
			case 50 : $source = 'TruJet';
			break;
		}
		return  $source;
	}


	/**
	 * check the response is valid or not
	 * @param array $api_response  response to be validated
	 */
	function valid_api_response($api_response)
	{
		if (empty($api_response) == false && valid_array($api_response) == true and isset($api_response['Status']) == true and $api_response['Status'] == SUCCESS_STATUS) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Validates Commit Booking Response
	 * @param unknown_type $api_response
	 */
	private function valid_commit_booking_response($api_response)
	{
		if (empty($api_response) == false && valid_array($api_response) == true and isset($api_response['Status']) == true and in_array($api_response['Status'], array(SUCCESS_STATUS, BOOKING_CONFIRMED, BOOKING_HOLD))) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * convert search params to TBO format
	 */
	public function search_data($search_id)
	{
		$response['status'] = true;
		$response['data'] = array();
		if (empty($this->master_search_data) == true and valid_array($this->master_search_data) == false) {
			$is_roundtrip  = false;
			$is_multicity  = false;
			$clean_search_details = $this->CI->flight_model->get_safe_search_data($search_id);
			if ($clean_search_details['status'] == true) {
				$response['status'] = true;
				$response['data'] = $clean_search_details['data'];
				$response['data']['from_city'] = $clean_search_details['data']['from'];
				$response['data']['to_city'] = $clean_search_details['data']['to'];
				
				switch($clean_search_details['data']['trip_type']){
					case 'oneway':
						$response['data']['type'] = 'OneWay';
						$response['data']['depature'] = date("Y-m-d", strtotime($clean_search_details['data']['depature'])) . 'T00:00:00';
						$response['data']['return'] = date("Y-m-d", strtotime($clean_search_details['data']['depature'])) . 'T00:00:00';
						$response['data']['from'] = substr(chop(substr($clean_search_details['data']['from'], -5), ')'), -3);
						$response['data']['to'] = substr(chop(substr($clean_search_details['data']['to'], -5), ')'), -3);
						break;
					case 'circle':
						$response['data']['type'] = 'Return';
						$response['data']['depature'] = date("Y-m-d", strtotime($clean_search_details['data']['depature'])) . 'T00:00:00';
						$response['data']['return'] = date("Y-m-d", strtotime($clean_search_details['data']['return'])) . 'T00:00:00';
						$response['data']['from'] = substr(chop(substr($clean_search_details['data']['from'], -5), ')'), -3);
						$response['data']['to'] = substr(chop(substr($clean_search_details['data']['to'], -5), ')'), -3);
						$is_roundtrip = true;
						break;
					case 'multicity':
						$response['data']['type'] = 'Multicity';
						$is_multicity = true;
						for($i=0; $i<count($clean_search_details['data']['depature']); $i++) {
							$response['data']['depature'][$i] = date("Y-m-d", strtotime($clean_search_details['data']['depature'][$i])) . 'T00:00:00';
							$response['data']['return'][$i] = date("Y-m-d", strtotime($clean_search_details['data']['depature'][$i])) . 'T00:00:00';
							$response['data']['from'][$i] = substr(chop(substr($clean_search_details['data']['from'][$i], -5), ')'), -3);
							$response['data']['to'][$i] = substr(chop(substr($clean_search_details['data']['to'][$i], -5), ')'), -3);
						}
						break;

					default : $response['data']['type'] = 'OneWay';
				}
				$response['data']['adult'] = $clean_search_details['data']['adult_config'];
				$response['data']['child'] = $clean_search_details['data']['child_config'];
				$response['data']['infant'] = $clean_search_details['data']['infant_config'];
				$response['data']['total_passenger'] = intval($clean_search_details['data']['adult_config']+$clean_search_details['data']['child_config']+$clean_search_details['data']['infant_config']);
				$response['data']['v_class'] = $clean_search_details['data']['v_class'];
				$response['data']['carrier'] = implode($clean_search_details['data']['carrier']);
				
				$response['data']['is_roundtrip'] = $is_roundtrip;
				$response['data']['is_multicity'] = $is_multicity;
				
				$this->master_search_data = $response['data'];
			} else {
				$response['status'] = false;
			}
		} else {
			$response['data'] = $this->master_search_data;
		}
		$this->search_hash = md5(serialized_data($response['data']));
		return $response;
	}

	/**
	 * Search Data for day fare
	 * @param unknown_type $par
	 */
	function calendar_day_fare_safe_search_data($params)
	{

		$response['status'] = true;
		$response['data'] = array();
		//Origin
		if (isset($params['from'])) {
			$response['data']['from'] = $params['from'];
		} else {
			$response['status'] = false;
		}

		if (isset($params['to'])) {
			$response['data']['to'] = $params['to'];
		} else {
			$response['status'] = false;
		}

		if (isset($params['depature'])) {
			if (strtotime($params['depature']) < time() ) {
				$response['data']['depature'] = date('Y-m-d');
			} else {
				$response['data']['depature'] = date('Y-m-d', strtotime($params['depature']));
			}
		} else {
			$response['status'] = false;
		}

		if (isset($params['session_id'])) {
			$response['data']['session_id'] = $params['session_id'];
		} else {
			$response['status'] = false;
		}
		return $response;
	}

	/**
	 * Search data for fare search result
	 * @param array $search_data
	 */
	function calendar_safe_search_data($search_data)
	{
		$safe_data = array();
		//Origin
		if (isset($search_data['from']) == true and empty($search_data['from']) == false) {
			$safe_data['from'] = $search_data['from'];
		} else {
			$safe_data['from'] = 'DEL';
		}

		//Destination
		if (isset($search_data['to']) == true and empty($search_data['to']) == false) {
			$safe_data['to'] = $search_data['to'];
		} else {
			$safe_data['to'] = 'BOM';
		}

		//PreferredCarrier
		if (isset($search_data['carrier']) == true and empty($search_data['carrier']) == false) {
			$safe_data['carrier'] = implode(',', $search_data['carrier']);
		} else {
			$safe_data['carrier'] = '';
		}

		//AdultCount
		if (isset($search_data['adult']) == true and empty($search_data['adult']) == false and intval($search_data['adult']) > 0) {
			$safe_data['adult'] = intval($search_data['adult']);
		} else {
			$safe_data['adult'] = 1;
		}

		//DepartureDate
		if (isset($search_data['depature']) == true and empty($search_data['depature']) == false) {
			if (strtotime($search_data['depature']) < time() ) {
				$safe_data['depature'] = date('Y-m-d');
			} else {
				$safe_data['depature'] = date('Y-m-d', strtotime($search_data['depature']));
			}
		} else {
			$safe_data['depature'] = date('Y-m-d');
		}
		//Type
		$safe_data['trip_type'] = 'OneWay';
		//CabinClass
		$safe_data['cabin'] = 'Economy';
		//ReturnDate
		$safe_data['return'] = '';
		//PromotionalPlanType
		$safe_data['PromotionalPlanType'] = 'Normal';
		return $safe_data;

	}

	/**
	 * Check and tell if flight response is round way and domestic
	 */
	function is_domestic_round_way_flight($flight_search_result)
	{
		if ($flight_search_result['Search']['SearchResult']['IsDomestic'] == true and $flight_search_result['Search']['SearchResult']['RoundTrip'] == true) {
			return true;
		}
	}

	private function way_multiplier($way_type, $domestic, $search_id=0)
	{
		$way_count = 0;
		if($way_type == 'multicity'){
			$search_data = $this->search_data($search_id);
			$way_count = intval(count($search_data['data']['from']));
		}
		else if ($way_type == 'circle') {
			$way_count = 1; 
		}
		else if ($way_type == 'oneway' || $domestic == true) {
			$way_count = 1;
		}
		else {
			$way_count = 1;
		}
		return $way_count;
	}

	/**
	 * Makrup for search result
	 * @param array $price_summary
	 * @param object $currency_obj
	 * @param boolean $level_one_markup
	 * @param boolean $current_domain_markup
	 * @param number $search_id
	 */
	function update_search_markup_currency(& $price_summary, & $currency_obj, $level_one_markup=false, $current_domain_markup=true, $search_id=0, $specific_markup_config = array())
	{
		if (intval($search_id) > 0) {
			$search_data = $this->search_data($search_id);
		}

		$total_pax = intval($this->master_search_data['adult_config'] + $this->master_search_data['child_config'] + $this->master_search_data['infant_config']);
		$trip_type = $this->master_search_data['trip_type'];

		$way_count = $this->way_multiplier($this->master_search_data['trip_type'], $this->master_search_data['is_domestic'], $search_id);
		$multiplier = ($total_pax*$way_count);

		return $this->update_markup_currency($price_summary, $currency_obj, $level_one_markup, $current_domain_markup, $multiplier, $specific_markup_config);
	}

	/**
	 *Markup pax wise
	 */
	function update_pax_markup_currency(& $price_summary, & $currency_obj, $level_one_markup=false, $current_domain_markup=true, $pax_count=0, $search_id=0)
	{
		if (intval($search_id) > 0) {
			$search_data = $this->search_data($search_id);
		}

		if (intval($pax_count) > 0) {
			$total_pax = intval($pax_count);
		} else {
			$total_pax = intval($this->master_search_data['adult_config'] + $this->master_search_data['child_config'] + $this->master_search_data['infant_config']);
		}

		$way_count = $this->way_multiplier($this->master_search_data['trip_type'], $this->master_search_data['is_domestic']);

		$multiplier = ($total_pax*$way_count);
		return $this->update_markup_currency( $price_summary, $currency_obj, $level_one_markup, $current_domain_markup, $multiplier);
	}
	/**
	 * update markup currency and return summary
	 */
	function update_markup_currency(& $price_summary, & $currency_obj, $level_one_markup=false, $current_domain_markup=true, $multiplier=1, $specific_markup_config = array())
	{
		$markup_list = array('OfferedFare');
		$markup_summary = array();
		foreach ($price_summary as $__k => $__v) {
			if (is_numeric($__v) == true) {
				$ref_cur = $currency_obj->force_currency_conversion($__v);	//Passing Value By Reference so dont remove it!!!
				$price_summary[$__k] = $ref_cur['default_value'];			//If you dont understand then go and study "Passing value by reference"

				if (in_array($__k, $markup_list)) {
					$temp_price = $currency_obj->get_currency($__v, true, $level_one_markup, $current_domain_markup, $multiplier, $specific_markup_config);
				} elseif (is_array($__v) == false) {
					$temp_price = $currency_obj->force_currency_conversion($__v);
				} else {
					$temp_price['default_value'] = $__v;
				}
				$markup_summary[$__k] = $temp_price['default_value'];
			}
		}
		
		//Markup
		//PublishedFare
		$Markup = 0;
		$price_summary['_Markup'] = 0;
		if (isset($markup_summary['OfferedFare'])) {
			$Markup = $markup_summary['OfferedFare'] - $price_summary['OfferedFare'];
			$markup_summary['PublishedFare'] = $markup_summary['PublishedFare'] + $Markup;
		}
		$markup_summary['_Markup'] = $Markup;
		return $markup_summary;
	}
	/**
	 * Jaganath
	 * Update Netfare tag for the response
	 */
	function update_net_fare($token)
	{
		$net_price_summary = array();
		$net_fare_tags = array('ServiceTax', 'AdditionalTxnFee', 'AgentCommission', 'TdsOnCommission', 'IncentiveEarned', 'TdsOnIncentive', 'PublishedFare', 'AirTransFee', 'Discount', 'OtherCharges', 'FuelSurcharge', 'TransactionFee', 'ReverseHandlingCharge', 'OfferedFare', 'AgentServiceCharge', 'AgentConvienceCharges');
		foreach($token as $k => $v) {
			$fare = $v['Fare'];
			foreach($fare as $fare_k => $fare_v) {
				if(in_array($fare_k, $net_fare_tags)) {
					if(isset($net_price_summary[$fare_k]) == true) {
						$net_price_summary[$fare_k] += $fare_v;
					} else {
						$net_price_summary[$fare_k] = $fare_v;
					}
				}
			}
		}
		$net_price_summary['TotalCommission'] = ($net_price_summary['PublishedFare']-$net_price_summary['OfferedFare']);
		return $net_price_summary;
	}
	/**
	 *Tax price is the price for which markup should not be added
	 */
	function tax_service_sum($markup_price_summary, $api_price_summary, $retain_commission=false)
	{

		//AirlineTransFee - Not Available
		//sum of tax and service ;

		if ($retain_commission == true) {
			$commission = 0;
			$commission_tds = 0;
		} else {
			$commission = $markup_price_summary['AgentCommission'];
			$commission_tds = $markup_price_summary['AgentTdsOnCommision'];
		}
		$markup_price = 0;
		$markup_price = $markup_price_summary['OfferedFare'] - $api_price_summary['OfferedFare'];
		return ((floatval($markup_price + @$markup_price_summary['AdditionalTxnFee'])+floatval(@$markup_price_summary['Tax'])+floatval(@$markup_price_summary['OtherCharges'])+floatval(@$markup_price_summary['ServiceTax'])) - $commission + $commission_tds);
	}

	/**
	 * calculate and return total price details
	 */
	function total_price($price_summary, $retain_commission=false, $currency_obj = '')
	{

		$com = 0;
		$com_tds = 0;
		if ($retain_commission == false) {
			$com = 0;
			$com_tds += floatval($currency_obj->calculate_tds($price_summary['AgentCommission']));
			$com_tds += floatval($currency_obj->calculate_tds(@$price_summary['PLBEarned']));
			$com_tds += floatval($currency_obj->calculate_tds(@$price_summary['IncentiveEarned']));
		} else {
			$com += floatval(@$price_summary['AgentCommission']);
			$com += floatval(@$price_summary['PLBEarned']);
			$com += floatval(@$price_summary['IncentiveEarned']);
			$com_tds = 0;
		}
		return (floatval(@$price_summary['OfferedFare'])+$com+$com_tds);
	}

	/**
	 *
	 * @param array $api_price_details
	 * @param array $admin_price_details
	 * @param array $agent_price_details
	 * @return number
	 */
	function b2b_price_details($api_price_details, $admin_price_details, $agent_price_details, $currency_obj)
	{
		$total_price['BaseFare']	= $api_price_details['BaseFare'];
		$total_price['_CustomerBuying']	= $agent_price_details['PublishedFare'];
		$total_price['_AgentBuying']	= $admin_price_details['OfferedFare'];
		$total_price['_AdminBuying']	= $api_price_details['OfferedFare'];
		$total_price['_AgentMarkup']	= $total_price['_Markup'] = $agent_price_details['OfferedFare'] - $admin_price_details['OfferedFare'];
		$total_price['_AdminMarkup']	= ($total_price['_AgentBuying'] - $total_price['_AdminBuying']);
		$total_price['_Commission']		= round($agent_price_details['PublishedFare'] - $agent_price_details['OfferedFare'], 3);
		$total_price['_tdsCommission']	= $currency_obj->calculate_tds($total_price['_Commission']);//Includes TDS ON PLB AND COMMISSION
		$total_price['_AgentEarning']	= $total_price['_Commission']+$total_price['_Markup'] - $total_price['_tdsCommission'];
		$total_price['_TaxSum']			= $agent_price_details['PublishedFare'] - $agent_price_details['BaseFare'];
		$total_price['_BaseFare']		= $agent_price_details['BaseFare'];
		$total_price['_TotalPayable']	= $total_price['_AgentBuying']+$total_price['_tdsCommission'];
		return $total_price;
	}

	/**
	 * Update Commission details
	 */
	function get_commission(& $__trip_flight, & $currency_obj)
	{
		$this->commission = $currency_obj->get_commission();
		if (valid_array($this->commission) == true && intval($this->commission['admin_commission_list']['value']) > 0) {
			//update commission
			//$bus_row = array(); Preserving Row data before calculation
			$core_agent_commision = ($__trip_flight['FareDetails']['PublishedFare']-$__trip_flight['FareDetails']['OfferedFare']);
			$com = $this->calculate_commission($core_agent_commision);
			$this->set_b2b_comm_tag($__trip_flight['FareDetails'], $com, $currency_obj);
		} else {
			//update commission
			$this->set_b2b_comm_tag($__trip_flight['FareDetails'], 0, $currency_obj);
		}
	}

	/**
	 * Add custom commission tag for b2b only
	 * @param array		s$v
	 * @param number	$b2b_com
	 */
	function set_b2b_comm_tag(& $v, $b2b_com=0, $currency_obj)
	{
		$v['ORG_AgentCommission'] = $v['AgentCommission'];
		$v['ORG_TdsOnCommission'] = $v['AgentTdsOnCommision'];
		$v['ORG_OfferedFare'] = $v['OfferedFare'];
		
		//$admin_com = $v['AgentCommission'] - $b2b_com;
		$core_agent_commision = ($v['PublishedFare']-$v['OfferedFare']);
		$admin_com = $core_agent_commision - $b2b_com;
		
		$v['OfferedFare'] = $v['OfferedFare']+$admin_com;
		$v['AgentCommission'] = $b2b_com;
		$v['TdsOnCommission'] = $currency_obj->calculate_tds($core_agent_commision);
	}

	/**
	 *
	 */
	private function calculate_commission($agent_com)
	{
		$agent_com_row = $this->commission['admin_commission_list'];
		$agent_com_row['value_type'] ='percentage';
		$b2b_comm = 0;
		if ($agent_com_row['value_type'] == 'percentage') {
			//%
			$b2b_comm = ($agent_com/100)*$agent_com_row['value'];
		} else {
			//plus
			$b2b_comm = ($agent_com-$agent_com_row['value']);
		}
		return number_format($b2b_comm, 2, '.', '');
	} 
	/**
	 * return booking form
	 */
	function booking_form($isDomestic, $token='', $token_key='', $search_access_key='', $promotional_plan_type='', $booking_source=PROVAB_FLIGHT_BOOKING_SOURCE)
	{
		$booking_form = '';

		$booking_form .= '<input type="hidden" name="is_domestic" class="" value="'.$isDomestic.'">';
		$booking_form .= '<input type="hidden" name="token[]" class="token data-access-key" value="'.$token.'">';
		$booking_form .= '<input type="hidden" name="token_key[]" class="token_key" value="'.$token_key.'">';
		$booking_form .= '<input type="hidden" name="search_access_key[]" class="search-access-key" value="'.$search_access_key.'">';
		$booking_form .= '<input type="hidden" name="promotional_plan_type[]" class="promotional-plan-type" value="'.$promotional_plan_type.'">';
		
		if (empty($booking_source) == false) {
			$booking_form .= '<input type="hidden" name="booking_source" class="booking-source" value="'.$booking_source.'">';
		}
		return $booking_form;
	}

	/**
	 * booking_url to be used
	 */
	function booking_url($search_id)
	{
		return base_url().'index.php/flight/booking/'.intval($search_id);
	}
	/**
	 * combine and get tbo booking form
	 */
	function get_form_content($form_1, $form_2)
	{
		$booking_form = '';
		$lcc = (($form_1['is_lcc[]'] == true || $form_2['is_lcc[]'] == true) ? true : false);
		//booking_type - decide it based on f1 is_lcc and f2 is_lcc
		$booking_type = $this->get_booking_type($lcc);
		$booking_form .= $this->booking_form(true, $form_1['token[]'], $form_1['token_key[]'], $form_1['search_access_key[]']);
		$booking_form .= $this->booking_form(true, $form_2['token[]'], $form_2['token_key[]'], $form_2['search_access_key[]']);
		return $booking_form;
	}

	/**
	 * Return booking type
	 */
	function get_booking_type($is_lcc)
	{
		if ($is_lcc) {
			return LCC_BOOKING;
		} else {
			return NON_LCC_BOOKING;
		}
	}

	/**
	 * Return unserialized data
	 * @param array $token		serialized data having token
	 * @param array $token_key	serialized data having token key
	 */
	public function unserialized_token($token, $token_key)
	{
		$response['data'] = array();
		$response['status'] = true;
		foreach($token as $___k => $___v) {
			$tmp_tkn = $this->read_token($___v);
			if ($tmp_tkn != false) {
				$response['data']['token'][$___k] = $tmp_tkn;
				$response['data']['token_key'] = $token_key[$___k];
			} else {
				$response['data']['token'][$___k] = false;
			}

			if ($response['status'] == true) {
				if ($response['data']['token'][$___k] == false) {
					$response['status'] = false;
				}
			}
		}
		return $response;
	}
	/**
	 * Jaganath
	 * Converts API data currency to preferred currency
	 * @param unknown_type $search_result
	 * @param unknown_type $currency_obj
	 */
	public function search_data_in_preferred_currency($search_result, $currency_obj)
	{
		#echo "<pre>";print_r($search_result);die('IN Provab_private lib'); 
		$flights = $search_result['JourneyList'];
		$flight_list = array();
		foreach($flights as $fk => $fv){
			foreach($fv as $list_k => $list_v){
				$flight_list[$fk][$list_k] = $list_v;
				$flight_list[$fk][$list_k]['FareDetails'] = $this->preferred_currency_fare_object($list_v['Price'], $currency_obj);
				$flight_list[$fk][$list_k]['PassengerFareBreakdown'] = $this->preferred_currency_paxwise_breakup_object($list_v['Price']['PassengerBreakup'], $currency_obj);
			}
		}
		$search_result['JourneyList'] = $flight_list;
		return $search_result;
	}
	/**
	 * Converts API data currency to preferred currency
	 * Jaganath
	 * @param unknown_type $search_result
	 * @param unknown_type $currency_obj
	 */
	public function farequote_data_in_preferred_currency($fare_quote_details, $currency_obj)
	{
		$flight_quote = $fare_quote_details['data']['token'];
		$flight_quote_data = array();
		foreach($flight_quote as $fk => $fv){
			$flight_quote_data[$fk] = $fv;
			$flight_quote_data[$fk]['FareDetails'] = $this->preferred_currency_fare_object($fv['Price'], $currency_obj);
			$flight_quote_data[$fk]['PassengerFareBreakdown'] = $this->preferred_currency_paxwise_breakup_object($fv['Price']['PassengerBreakup'], $currency_obj);
			unset($flight_quote_data[$fk]['Price']);
		}
		$fare_quote_details['data']['token'] = $flight_quote_data;
		return $fare_quote_details;
	}
	/**
	 * Converts Extra services Price details to preferred currency
	 * @param unknown_type $extra_services
	 */
	private function extra_services_in_preferred_currency($extra_services)
	{
		$currency_obj = new Currency(array('module_type' => 'flight','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		
		//Baggage
		if(isset($extra_services['Baggage']) == true && valid_array($extra_services['Baggage']) == true){
			foreach ($extra_services['Baggage'] as $bag_k => $bag_v){
				foreach ($bag_v as $bd_k => $bd_v){
					//Convert the Price to prefeerd currency
					$Price = $extra_services['Baggage'][$bag_k][$bd_k]['Price'];
					$Price = get_converted_currency_value($currency_obj->force_currency_conversion($Price));
					$extra_services['Baggage'][$bag_k][$bd_k]['Price'] = $Price;
				}
			}
		}
		
		return $extra_services;
	}
	/**
	 * Jaganath
	 * Converts Display currency to application currency
	 * @param unknown_type $fare_details
	 * @param unknown_type $currency_obj
	 * @param unknown_type $module
	 */
	public function convert_token_to_application_currency($token, $currency_obj, $module)
	{
		$token_details = $token;
		$token = array();
		$application_default_currency = admin_base_currency();
		foreach($token_details as $tk => $tv) {
			$token[$tk] = $tv;
			$temp_fare_details = $tv['FareDetails'];
			//Fare Details
			$FareDetails = array();
			if($module == 'b2c') {
			$PriceDetails = $temp_fare_details[$module.'_PriceDetails'];
			
			$FareDetails['b2c_PriceDetails']['BaseFare'] = 			get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['BaseFare']));
			$FareDetails['b2c_PriceDetails']['TotalTax'] = 			get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['TotalTax']));
			$FareDetails['b2c_PriceDetails']['TotalFare'] = 		get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['TotalFare']));
			$FareDetails['b2c_PriceDetails']['Currency'] = 			$application_default_currency;
			$FareDetails['b2c_PriceDetails']['CurrencySymbol'] =	$currency_obj->get_currency_symbol($currency_obj->to_currency);
			} else if($module == 'b2b') {
				$PriceDetails = $temp_fare_details[$module.'_PriceDetails'];
				
				$FareDetails['b2b_PriceDetails']['BaseFare'] = 			get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['BaseFare']));
				$FareDetails['b2b_PriceDetails']['_CustomerBuying'] =	get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_CustomerBuying']));
				$FareDetails['b2b_PriceDetails']['_AgentBuying'] = 		get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AgentBuying']));
				$FareDetails['b2b_PriceDetails']['_AdminBuying'] = 		get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AdminBuying']));
				$FareDetails['b2b_PriceDetails']['_Markup'] = 			get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_Markup']));
				$FareDetails['b2b_PriceDetails']['_AgentMarkup'] = 		get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AgentMarkup']));
				$FareDetails['b2b_PriceDetails']['_AdminMarkup'] = 		get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AdminMarkup']));
				$FareDetails['b2b_PriceDetails']['_Commission'] = 		get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_Commission']));
				$FareDetails['b2b_PriceDetails']['_tdsCommission'] = 	get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_tdsCommission']));
				$FareDetails['b2b_PriceDetails']['_AgentEarning'] = 	get_converted_currency_val