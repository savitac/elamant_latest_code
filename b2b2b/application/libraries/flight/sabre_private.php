<?php
error_reporting(-1); 
require_once 'Common_api_flight.php';
class Sabre_private extends Common_Api_Flight {
  var $master_search_data;
  var $search_hash;
  protected $source_code = SABRE_FLIGHT_BOOKING_SOURCE;
  protected $token;
  //$_GLOBALS['BinarySecurityToken'];
  function __construct() {
     /*if ($_SERVER ['REMOTE_ADDR'] != '192.168.0.26') {debug($_SERVER);exit;
     exit ();
     }*/
     //die('sabre');
    parent::__construct ( META_AIRLINE_COURSE, $this->source_code );
    $this->CI = &get_instance ();
    $GLOBALS ['CI']->load->library ( 'Api_Interface' );
    $GLOBALS ['CI']->load->library ( 'converter' ); 
    $GLOBALS ['CI']->load->library ( 'ArrayToXML' );
    $GLOBALS ['CI']->load->model ( 'flight_model' );
    $GLOBALS ['CI']->load->model ( 'db_cache_api' );
    $GLOBALS ['CI']->load->helper ( 'url' );
    //$GLOBALS ['CI']->load->library ( 'ArrayToXML' );
    $this->CI->load->library('xml_to_array');
    // $this->set_api_credentials ();
    // error_reporting ( E_ALL );
  }
  function update_markup_currency(& $price_summary, & $currency_obj) {
  }
  
  /**
   * booking_url to be used
   */
  function booking_url($search_id)
  {
    return base_url().'index.php/flight/booking/'.intval($search_id);
  }

  public function getAirPrice_tp($value, $value1)
  {
    //debug($value1);die;
     $data['format_airprice']['BasePrice'] = $value1[0]['price']['total_breakup']['api_total_fare'];
    $data['format_airprice']['Taxes'] = $value1[0]['price']['total_breakup']['api_total_tax'];
    $data['format_airprice']['TotalPrice'] = $value1[0]['price']['api_total_display_fare'];
    return $data;
  }
  function process_booking($book_id, $temp_booking, $customer_payment_details="") {
    //debug($temp_booking);die('process_booking token');
    $ptempbooking = $temp_booking['seat_information'];
    $temp_booking = $temp_booking ['book_attributes'];
    $clean_search_details = $this->CI->flight_model->get_safe_search_data ( $temp_booking ['search_id'] );
    $clean_search_details = $clean_search_details ['data'];
    
    $response ['data'] = array ();
     $psdxdata['status'] = 0;
      $psdxdata['msg'] = 'booking failed';
    $b_source = $this->source_code;
    $air_cde = '';
    
    /*-----------------------------------Domestic round trip changes------------------------------------------------------------- */
    
    if(valid_array($temp_booking['token'])) { 
    //  debug($temp_booking);die('process_booking token');
      $SessionCreateRQ_RS = $this->SessionCreateRQ($book_id,'BOOK');
      //echo "seess";exit();
      $mypdata = $this->parse_session_create_response($SessionCreateRQ_RS);
      //debug($temp_booking);die('process_booking token');
      $TravelItineraryAddInfoRQ_RS = $this->TravelItineraryAddInfoRQ($book_id,$temp_booking,'TravelItineraryAddInfoLLSRQ');
      $TravelItineraryAddInfoRS = $TravelItineraryAddInfoRQ_RS['TravelItineraryAddInfoRS'];
      if (!empty($TravelItineraryAddInfoRS) && substr($TravelItineraryAddInfoRS, 0, 5) == "<?xml") {
        $response = $this->CI->xml_to_array->XmlToArray($TravelItineraryAddInfoRS);
        $TravelItineraryAddInfoStatus = $response['soap-env:Body']['TravelItineraryAddInfoRS']['stl:ApplicationResults']['@attributes']['status'];//debug($TravelItineraryAddInfoStatus);die;
        if ($TravelItineraryAddInfoStatus == 'Complete') {
          $OTA_AddRemarkRQ_RS = $this->AddRemarkRQ($book_id,$temp_booking, $customer_payment_details, 'AddRemarkLLSRQ');
          $OTA_AirBookRQ_RS   = $this->OTA_AirBookRQ($book_id,$temp_booking,'EnhancedAirBookRQ');
          $OTA_AirBookRS    = $OTA_AirBookRQ_RS['OTA_AirBookRS'];
          //debug($OTA_AirBookRS);die('OTA_AirBookRS');
            $ReserveAirseatRQ_RS = $this->ReserveAirSeatLLSRQ($book_id,$temp_booking, $ptempbooking);
                          $ReserveAirseatRS    = $ReserveAirseatRQ_RS['AirSeat_RS'];
          if (!empty($OTA_AirBookRS) && substr($OTA_AirBookRS, 0, 5) == "<?xml") { 
            $response = $this->CI->xml_to_array->XmlToArray($OTA_AirBookRS);
                  $OTA_AirBookStatus = $response['soap-env:Body']['EnhancedAirBookRS']['stl:ApplicationResults']['@attributes']['status'];
                  /*debug($OTA_AirBookStatus);die('OTA_AirBookStatus');*/
                  if ($OTA_AirBookStatus == 'Complete') {
                    $OTA_AirPriceRQ_RS  = $this->OTA_AirPriceRQ($book_id,$temp_booking,'OTA_AirPriceLLSRQ');
                    $OTA_AirPriceRS   = $OTA_AirPriceRQ_RS['OTA_AirPriceRS'];
                    if (!empty($OTA_AirPriceRS) && substr($OTA_AirPriceRS, 0, 5) == "<?xml") {
                      $response = $this->CI->xml_to_array->XmlToArray($OTA_AirPriceRS);
                      $OTA_AirPriceStatus = $response['soap-env:Body']['OTA_AirPriceRS']['stl:ApplicationResults']['@attributes']['status'];
                      if ($OTA_AirPriceStatus == 'Complete') {
                       // debug($temp_booking);die;
                        $ps_service = 0;
                        for ($zs=0; $zs < count($temp_booking['passenger_passport_issuing_country']); $zs++) { 

                         // echo "----".$temp_booking['passenger_passport_number']."---".$temp_booking['ADT_MEAL']."---".$temp_booking['ADT_SPECIAL']."---";exit();
                                if (($temp_booking['passenger_passport_number'][$zs] != "") || ($temp_booking['ADT_MEAL'][$zs] != "") || ($temp_booking['ADT_SPECIAL'][$zs] != "")) {
                                 $ps_service = 1;
                                }
                        }
                        $SpecialServiceRS = "";
                        $Special_status = false;
                        if ($ps_service == 0) {
                         $Special_status = true;
                        }
                          $SpecialServiceRQ_RS = $this->SpecialServiceRQ($book_id,$temp_booking,'SpecialServiceLLSRQ');
                           $SpecialServiceRS   = $SpecialServiceRQ_RS['SpecialServiceRS'];
                           //echo $ps_service.'pankaj<br/>';
                          // debug($SpecialServiceRS);die;
                        if ((!empty($SpecialServiceRS) && substr($SpecialServiceRS, 0, 5) == "<?xml") || ($Special_status)) {
                          $SpecialServiceStatus = 'Complete';
                          if ($ps_service == 1) {
                             $response = $this->CI->xml_to_array->XmlToArray($SpecialServiceRS);
                             $SpecialServiceStatus = $response['soap-env:Body']['SpecialServiceRS']['stl:ApplicationResults']['@attributes']['status'];
                          }
                          //debug($SpecialServiceStatus);die;
                          if (($SpecialServiceStatus == 'Complete') || ($Special_status == true)) {
                            $EndTransactionRQ_RS  = $this->EndTransactionRQ($book_id,$temp_booking,'EndTransactionLLSRQ');
                            $EndTransactionRS     = $EndTransactionRQ_RS['EndTransactionRS'];
                            if (!empty($EndTransactionRS) && substr($EndTransactionRS, 0, 5) == "<?xml") {
                               $response = $this->CI->xml_to_array->XmlToArray($EndTransactionRS);
                               $EndTransactionStatus = $response['soap-env:Body']['EndTransactionRS']['stl:ApplicationResults']['@attributes']['status'];
                               if ($EndTransactionStatus == 'Complete') {
                                $ItineraryRef = $response['soap-env:Body']['EndTransactionRS']['ItineraryRef']['@attributes']['ID'];  
                                                             


                                  $expl = explode("-", $book_id);
                                  $webdata['app_reference_gds'] = $expl[0]."-".$ItineraryRef."-".$expl[1]."-".$expl[2];                               
                                  $update_booking = array(
                                          'pnr'    => $ItineraryRef,
                                          'status' => "BOOKING_CONFIRMED",                                          'app_reference' => $book_id,
                                          'payment_status' => 'paid'
                                         );
                                $QueuePlaceRQ_RS = $this->QueuePlaceRQ($book_id, $ItineraryRef,'QueuePlaceLLSRQ');
                                $this->CI->flight_model->update_booking_global($book_id, $update_booking); 
                                $this->CI->flight_model->update_booking_pglobal($book_id, $webdata);   
                               }
                            }
                            $this->SessionCloseRQ($book_id,'BOOK','SessionCloseRQ'); 
                            $psdxdata['status'] = 1;
                            $psdxdata['data']['msg'] = 'booking confirmed';

    //return $psdxdata;
                           // debug($psdxdata);exit('psdxdata');
                            
                          }
                        }
                       }
                    }
                  }
          }
        }
        //debug($response);die;
      }
      //exit('psdxdata');
      //return $psdxdata;
      
    }
    //die('eee');
    
    
    
    return $psdxdata;
  }
   /* traveller booking request start here*/
  function TravelItineraryAddInfoRQ_old($book_id, $traveler_details1, $Action = 'TravelItineraryAddInfoLLSRQ'){
  $search_data = $this->search_data($traveler_details1['search_id']);
 //debug($traveler_details1);die;
    $traveler_details   = $traveler_details1;
   //echo "<pre/>";print_r($traveler_details1['book_attributes']);exit('TravelItineraryAddInfoRQ');
    $travellers     = $frequent_traveler = $PassengerType = '';$a = 1;$c = 0;$i = 0;
    for($aaa=0;$aaa < count($traveler_details['first_name']);$aaa++) {       
      $pmytitle = '';
      $pfirst_name = "";
      if ($traveler_details['passenger_type'][$aaa] == 'Adult') {
        if ($traveler_details['gender'][$aaa] == 1) {
          $pmytitle = 'Mr';
        } else{
          $pmytitle = 'Miss';
        }
        // debug($traveler_details);die;
        $pfirst_name = $traveler_details['first_name'][$aaa];
        if($traveler_details['middle_name'][$aaa] != ""){
           $pfirst_name .= " ".$traveler_details['middle_name'][$aaa];
        }
        $GivenNameXMLADT = "<GivenName>".$pfirst_name."</GivenName>";
      $travellers .= "<PersonName NameNumber='".$a.".1' PassengerType='ADT' NameReference='".$pmytitle."'>
                ".$GivenNameXMLADT."
                <Surname>".$traveler_details['last_name'][$aaa]."</Surname>
              </PersonName>"; 
      }
      if ($traveler_details['passenger_type'][$aaa] == 'Child') {
         $pmytitle = '';
        if ($traveler_details['gender'][$aaa] == 1) {
          $pmytitle = 'Mr';
        } else{
          $pmytitle = 'Miss';
        }
        $pfirst_name = $traveler_details['first_name'][$aaa];
        if($traveler_details['middle_name'][$aaa] != ""){
           $pfirst_name .= " ".$traveler_details['middle_name'][$aaa];
        }
            $GivenNameXMLCHD = "<GivenName>".$pfirst_name."</GivenName>";
          
          
          $travellers .= "<PersonName NameNumber='".$a.".1' PassengerType='CNN' NameReference='".$pmytitle."'>
                    ".$GivenNameXMLCHD."
                    <Surname>".$traveler_details['last_name'][$aaa]."</Surname>
                  </PersonName>";
      }
      if ($traveler_details['passenger_type'][$aaa] == 'Infant') {
         $pmytitle = '';
        if ($traveler_details['gender'][$aaa] == 1) {
          $pmytitle = 'Mr';
        } else{
          $pmytitle = 'Miss';
        }
        $pfirst_name = $traveler_details['first_name'][$aaa];
        if($traveler_details['middle_name'][$aaa] != ""){
           $pfirst_name .= " ".$traveler_details['middle_name'][$aaa];
        }
        $GivenNameXMLINF = "<GivenName>".$pfirst_name."</GivenName>";
      $travellers .= "<PersonName Infant='true' NameNumber='".$a.".1' PassengerType='INF' NameReference='".$pmytitle."'>
                ".$GivenNameXMLINF."
                <Surname>".$traveler_details['last_name'][$aaa]."</Surname>
              </PersonName>"; 

      }
     
    $a++;       
  }
    
    $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z");   
    $TravelItineraryAddInfoRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service eb:type='OTA'>Air</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$pxdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$pxdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                      <TravelItineraryAddInfoRQ Version='2.0.2' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
                        <AgencyInfo>
                             <Address>
                                <AddressLine>innovative</AddressLine>
                                <CityName>Covina</CityName>
                                <CountryCode>USA</CountryCode>
                                <PostalCode>91723</PostalCode>
                                <StateCountyProv StateCode='CA'/>
                                <StreetNmbr>654 Shoppers Lane</StreetNmbr>                              
                            </Address>
                            <Ticketing TicketType='7TAW'/>
                        </AgencyInfo>
                        <CustomerInfo>
                            <ContactNumbers>
                                <ContactNumber Phone='8951009100' PhoneUseType='A'/>
                            </ContactNumbers>
                            <Email Address='pankajprovab212@gmail.com' Type='CC'/>
                            ".$travellers."
                        </CustomerInfo>
                    </TravelItineraryAddInfoRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";
             
    // echo $TravelItineraryAddInfoRQ;exit;
    $TravelItineraryAddInfoRS = $this->flight_processRequest($TravelItineraryAddInfoRQ); 
    // echo $TravelItineraryAddInfoRS;exit;
    $TravelItineraryAddInfoRQ_RS = array(
      'TravelItineraryAddInfoRQ' => $TravelItineraryAddInfoRQ,
      'TravelItineraryAddInfoRS' => $TravelItineraryAddInfoRS
    );  
    
    $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$book_id."/TravelItineraryAddInfoRQ.xml";
    $fp = fopen($path,"wb");fwrite($fp,$TravelItineraryAddInfoRQ);fclose($fp);
    
    $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$book_id."/TravelItineraryAddInfoRS.xml";
    $fp = fopen($path,"wb");fwrite($fp,$TravelItineraryAddInfoRS);fclose($fp);    
    return $TravelItineraryAddInfoRQ_RS;
}

function TravelItineraryAddInfoRQ($BinarySecurityToken, $search_id, $search_data1, $book_params, $Action = 'TravelItineraryAddInfoLLSRQ'){

  /*debug($BinarySecurityToken);
  debug($search_id);
  debug($search_data1);
  debug($book_params);die;*/
  $strSabrePath = "all_xml_logs/flight/sabre";
  $credencials    = $this->set_api_credentials();
    $search_data    = $search_data1;
    $segment_data     = $book_params;
    // $traveler_details   = json_decode(base64_decode($traveler_details1));
  // debug($segment_data);die;
    $travellers     = $frequent_traveler = '';$a = 1;$c = 0;$i = 0;

    for ($pax=0; $pax < count($segment_data['passenger_type']); $pax++) { 

        if($segment_data['middle_name'][$pax] == ''){
          $GivenNameXMLADT = "<GivenName>".$segment_data['first_name'][$pax]."</GivenName>";
        }else{
          $GivenNameXMLADT = "<GivenName>".$segment_data['first_name'][$pax]." ".$segment_data['middle_name'][$pax]."</GivenName>";
        }

        if($segment_data['passenger_type'][$pax] == 'Adult'){
          $PassengerType='ADT';
          $infantTrue = "";
        }
        if($segment_data['passenger_type'][$pax] == 'Child'){
          $PassengerType='CNN';
          $infantTrue = "";
        }
        if($segment_data['passenger_type'][$pax] == 'Infant'){
          $PassengerType='INF';
          $infantTrue = "Infant='true'";
        }
        if($segment_data['name_title'][$pax] == 1){
          $nameTile = 'Mr';
        }
        if($segment_data['name_title'][$pax] == 2){
          $nameTile = 'Ms';
        }
        if($segment_data['name_title'][$pax] == 3){
          $nameTile = 'Miss';
        }
        if($segment_data['name_title'][$pax] == 5){
          $nameTile = 'Mrs';
        }
        $paxCount = $pax+1;
        $travellers .= "<PersonName ".$infantTrue." NameNumber='".$paxCount.".1' PassengerType='".$PassengerType."' NameReference='".$nameTile."'>
              ".$GivenNameXMLADT."
              <Surname>".$segment_data['last_name'][$pax]."</Surname>
            </PersonName>"; 

    }

    /*for($aaa=0;$aaa < count($traveler_details->saladult);$aaa++) {       
    if($traveler_details->mnameadult[$aaa] == ''){
      $GivenNameXMLADT = "<GivenName>".$traveler_details->fnameadult[$aaa]."</GivenName>";
    }else{
      $GivenNameXMLADT = "<GivenName>".$traveler_details->fnameadult[$aaa]." ".$traveler_details->mnameadult[$aaa]."</GivenName>";
    }
    if ($traveler_details->airlineadult[$aaa]!='') {    
       if ($traveler_details->ftnumberadult[$aaa]!='') {    
         $frequent_traveler .= '<CustLoyalty MembershipID="'.$traveler_details->ftnumberadult[$aaa].'" NameNumber="'.$a.'.1" ProgramID="'.$traveler_details->airlineadult[$aaa].'" TravelingCarrierCode="'.$traveler_details->airlineadult[$aaa].'" />'; 
       }
    }
    $travellers .= "<PersonName NameNumber='".$a.".1' PassengerType='ADT' NameReference='".$traveler_details->saladult[$aaa]."'>
              ".$GivenNameXMLADT."
              <Surname>".$traveler_details->lnameadult[$aaa]."</Surname>
            </PersonName>"; 
     
    $a++;       
  }*/
    /*if(isset($traveler_details->salchild)){
        $c = $a;
        for($ccc=0;$ccc < count($traveler_details->salchild);$ccc++) {  
      if($traveler_details->mnamechild[$ccc] == ''){
        $GivenNameXMLCHD = "<GivenName>".$traveler_details->fnamechild[$ccc]."</GivenName>";
      }else{
        $GivenNameXMLCHD = "<GivenName>".$traveler_details->fnamechild[$ccc]." ".$traveler_details->mnamechild[$ccc]."</GivenName>";
      }
      if ($traveler_details->airlinechild[$aaa]!='') {    
         if ($traveler_details->ftnumberchild[$aaa]!='') {    
           $frequent_traveler .= '<CustLoyalty MembershipID="'.$traveler_details->ftnumberchild[$aaa].'" NameNumber="'.$a.'.1" ProgramID="'.$traveler_details->airlinechild[$aaa].'" TravelingCarrierCode="'.$traveler_details->airlinechild[$aaa].'" />';
         }
      }
      $travellers .= "<PersonName NameNumber='".$a.".1' PassengerType='CNN' NameReference='".$traveler_details->salchild[$ccc]."'>
                ".$GivenNameXMLCHD."
                <Surname>".$traveler_details->lnamechild[$ccc]."</Surname>
              </PersonName>"; 
       $a++; $c++;    
    }
    }*/
    
    /*if(isset($traveler_details->salinfant)){
        $i = $a+$c;
        for($iii=0;$iii < count($traveler_details->salinfant);$iii++) {   
      if($traveler_details->mnameinfant[$iii] == ''){
        $GivenNameXMLINF = "<GivenName>".$traveler_details->fnameinfant[$iii]."</GivenName>";
      }else{
        $GivenNameXMLINF = "<GivenName>".$traveler_details->fnameinfant[$iii]." ".$traveler_details->mnameinfant[$iii]."</GivenName>";
      }
      if ($traveler_details->airlineinfant[$aaa]!='') {   
         if ($traveler_details->ftnumberinfant[$aaa]!='') {   
           $frequent_traveler .= '<CustLoyalty MembershipID="'.$traveler_details->ftnumberinfant[$aaa].'" NameNumber="'.$a.'.1" ProgramID="'.$traveler_details->airlineinfant[$aaa].'" TravelingCarrierCode="'.$traveler_details->airlineinfant[$aaa].'" />';
         }
      }
      $travellers .= "<PersonName Infant='true' NameNumber='".$a.".1' PassengerType='INF' NameReference='".$traveler_details->salinfant[$iii]."'>
                ".$GivenNameXMLINF."
                <Surname>".$traveler_details->lnameinfant[$iii]."</Surname>
              </PersonName>"; 
        $a++; $i++;  
    }
  }   */
  $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
  $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z"); 
    $TravelItineraryAddInfoRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service eb:type='OTA'>Air</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$pxdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$pxdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                      <TravelItineraryAddInfoRQ Version='2.0.2' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
                        <AgencyInfo>
                             <Address>
                                <AddressLine>bookurflight</AddressLine>
                                <CityName>virginia</CityName>
                                <CountryCode>USA</CountryCode>
                                <PostalCode>22030-6331</PostalCode>
                                <StateCountyProv StateCode='VA'/>
                                <StreetNmbr>5408 SHAHER LA FAIRFAX</StreetNmbr>                              
                            </Address>
                            <Ticketing TicketType='7TAW'/>
                        </AgencyInfo>
                        <CustomerInfo>
                            <ContactNumbers>
                                <ContactNumber Phone='".trim($segment_data['passenger_contact'])."' PhoneUseType='A'/>
                            </ContactNumbers>
                            ".$frequent_traveler."
                            <Email Address='".trim($segment_data['billing_email'])."' Type='CC'/>
                            ".$travellers."
                        </CustomerInfo>
                    </TravelItineraryAddInfoRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";
      $TravelItineraryAddInfoRS = $this->flight_processRequest($TravelItineraryAddInfoRQ, $this->system); 
            // echo "<pre>";print_r($TravelItineraryAddInfoRS);die; 
  
  $TravelItineraryAddInfoRQ_RS = array(
    'TravelItineraryAddInfoRQ' => $TravelItineraryAddInfoRQ,
    'TravelItineraryAddInfoRS' => $TravelItineraryAddInfoRS
  );  
    
  // $path = FCPATH. $strSabrePath . "/booking_logs/".$search_id."/TravelItineraryAddInfoRQ.xml";
  // $fp = fopen($path,"wb");fwrite($fp,$TravelItineraryAddInfoRQ);fclose($fp);
  
  // $path = FCPATH. $strSabrePath . "/booking_logs/".$search_id."/TravelItineraryAddInfoRS.xml";
  // $fp = fopen($path,"wb");fwrite($fp,$TravelItineraryAddInfoRS);fclose($fp);  
   // echo "<pre>";print_r($TravelItineraryAddInfoRS);die;  
  return $TravelItineraryAddInfoRQ_RS;
}

  /*traveller booking request ends here*/
  /*for adding payment to sabre start here*/
function AddRemarkRQ($book_id,$traveler_details1, $customer_payment_details, $Action = 'AddRemarkLLSRQ'){
    //$credencials = set_credencials_flight();
    //$search_data = $this->search_data($search_data1['book_attributes']['search_id']);
    //debug($traveler_details1['book_attributes']);exit;
    $search_id = $traveler_details1;
    //debug($customer_payment_details);exit;
    $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z");  
    $traveler_details   = $search_id;
    //$traveler_details  = json_decode(base64_decode($traveler_details1));
    /*$PaymentDetails = '<FOP_Remark>
             <CC_Info>
              <PaymentCard Code="'.$search_id->CardType.'" ExpireDate="2017-07" Number="4111111111111111" />
            </CC_Info>
          </FOP_Remark>';*/
          //for dynamic card
    $PaymentDetails = '<FOP_Remark>
             <CC_Info>
              <PaymentCard Code="'.$customer_payment_details["card_type"].'" ExpireDate="'.$customer_payment_details["card_expyear"].'-'.$customer_payment_details["card_expmonth"].'" Number="'.$customer_payment_details["card_number"].'" />
            </CC_Info>
          </FOP_Remark>';
          /*$PaymentDetails = '';*/
  $GeneralReamrk = '<Remark Type="General">
              <Text>Internal Code - '.$customer_payment_details["cvv_code"].'</Text>
            </Remark>';
  /*$GeneralReamrk = '<Remark Type="General">
              <Text>Internal Code - 123</Text>
            </Remark>';*/
 //$GeneralReamrk = '';
            
  /*$BillingAddress = '<Remark Type="Client Address">
              <Text>'.$search_id["first_name"][0].$search_id["last_name"][0]."".$search_id["billing_city"]."".$search_id["billing_country"]."".$search_id["billing_zipcode"].'</Text>
              </Remark>';*/
  $BillingAddress = '<Remark Type="Client Address">
              <Text>'.$search_id["address2"]."".$search_id["billing_address_1"].'</Text>
              </Remark>';
              // <Text>'.$traveler_details->card_contact_first_name." ".$traveler_details->card_contact_middle_name." ".$traveler_details->card_contact_last_name.", ".$traveler_details->card_contact_city.", ".$traveler_details->card_contact_state.", ".$traveler_details->card_contact_nationality." - ".$traveler_details->card_contact_zipcode." Email: ".$traveler_details->card_contact_email.'</Text>
   $AddRemarkRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service eb:type='OTA'>Air</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$pxdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$pxdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                      <AddRemarkRQ xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' ReturnHostCommand='false' Version='2.1.0'>
              <RemarkInfo>
              ".$PaymentDetails."
              ".$GeneralReamrk."
              ".$BillingAddress."
              </RemarkInfo>
                     </AddRemarkRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";
              // echo $AddRemarkRQ;//die;
    $AddRemarkRS = $this->flight_processRequest($AddRemarkRQ); 
  // echo $AddRemarkRS;die('$AddRemarkRS');
    $AddRemarkRQ_RS = array(
            'AddRemarkRQ' => $AddRemarkRQ,
            'AddRemarkRS' => $AddRemarkRS
          );      
    $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$book_id."/AddRemarkRQ.xml";
  $fp = fopen($path,"wb");fwrite($fp,$AddRemarkRQ);fclose($fp);
  
  $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$book_id."/AddRemarkRS.xml";
  $fp = fopen($path,"wb");fwrite($fp,$AddRemarkRS);fclose($fp);  
    return $AddRemarkRQ_RS;
}
public function search_data_my($value)
{
  return $clean_search_details = $this->CI->flight_model->get_safe_search_data_my ( $value );
}

function OTA_AirBookRQ($BinarySecurityToken, $search_id, $search_data1, $book_params, $Action = 'EnhancedAirBookRQ'){
  $strSabrePath = "all_xml_logs/flight/sabre";
    $search_data  = $search_data1;
    $segment_data   = $book_params['token']['token'][0]['SegmentDetails'];


    $data = json_decode(base64_decode ( $book_params['flights_data'] ));
      
    // if ($data !== false) {
    //   $data = @unserialize ( $data );
    // }

    // $flights_data = unserialized($book_params['flights_data']);
    // debug($book_params);
    // debug($segment_data);die;

    $Segments = '';
    for($j=0;$j< count($segment_data); $j++){
    for($ss=0;$ss< count($segment_data[$j]); $ss++){
      if(!empty($segment_data[$j][$ss]['Group'])){
      if($segment_data[$j][$ss]['Group']=="O")
        $MarriageGrp_status="false";
      else if($segment_data[$j][$ss]['Group']=="I")
        $MarriageGrp_status="true";
      }else{
        $MarriageGrp_status="false";
      }
      $Segments .= "<FlightSegment DepartureDateTime='".$segment_data[$j][$ss]['OriginDetails']['DateTime']."' ArrivalDateTime='".$segment_data[$j][$ss]['DestinationDetails']['DateTime']."' FlightNumber='".$segment_data[$j][$ss]['flight_number']."' NumberInParty='".($search_data['data']['adult_config'] + $search_data['data']['child_config'])."' ResBookDesigCode='".$segment_data[$j][$ss]['is_leg']."' Status='NN'>
               <DestinationLocation LocationCode='".$segment_data[$j][$ss]['DestinationDetails']['AirportCode']."'/>
               <Equipment AirEquipType='".$segment_data[$j][$ss]['flight_number']."'/>
               <MarketingAirline Code='".$segment_data[$j][$ss]['operator_code']."' FlightNumber='".$segment_data[$j][$ss]['AirlineDetails']['FlightNumber']."'/>
               <MarriageGrp Ind='".$MarriageGrp_status."'/>
               <OperatingAirline Code='".$segment_data[$j][$ss]['operator_code']."'/>
               <OriginLocation LocationCode='".$segment_data[$j][$ss]['OriginDetails']['AirportCode']."'/>
              </FlightSegment>";
    }
  }
  $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
  $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z"); 
  $OTA_AirBookRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service>EnhancedAirBookRQ</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$pxdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$pxdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                    <EnhancedAirBookRQ Version='2.3.0' ReturnHostCommand='true' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://webservices.sabre.com/sabreXML/2011/10 http://webservices.sabre.com/wsdl/swso/EnhancedAirBook2.3.0RQ.xsd'>
                      <OTA_AirBookRQ HaltOnError='true'>
                        <HaltOnStatus Code='NN'/>
                        <HaltOnStatus Code='UC'/>
                        <HaltOnStatus Code='NO'/>
                        <HaltOnStatus Code='US'/>
                        <HaltOnStatus Code='LL'/>
                        <OriginDestinationInformation>
                          " . $Segments . "
                        </OriginDestinationInformation>
                        <RedisplayReservation NumAttempts='10' WaitInterval='3000'/>
                      </OTA_AirBookRQ>
                      <PostProcessing HaltOnError='true' IgnoreAfter='false' RedisplayReservation='true'/>
                      <PreProcessing HaltOnError='true' IgnoreBefore='false'/>  
                    </EnhancedAirBookRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";

              // echo "<pre>";print_r($OTA_AirBookRQ);die;
    $OTA_AirBookRS = $this->flight_processRequest($OTA_AirBookRQ, $this->system);  
     // echo "<pre>";print_r($OTA_AirBookRS);die;
    $OTA_AirBookRQ_RS = array(
              'OTA_AirBookRQ' => $OTA_AirBookRQ,
              'OTA_AirBookRS' => $OTA_AirBookRS
            );
    // echo "<pre>";print_r($OTA_AirBookRQ_RS);die;
  //   $path = FCPATH. $strSabrePath . "/booking_logs/".$search_id."/OTA_AirBookRQ.xml";
  // $fp = fopen($path,"wb");fwrite($fp,$OTA_AirBookRQ);fclose($fp);
  
  // $path = FCPATH. $strSabrePath . "/booking_logs/".$search_id."/OTA_AirBookRS.xml";
  // $fp = fopen($path,"wb");fwrite($fp,$OTA_AirBookRS);fclose($fp); 
  return $OTA_AirBookRQ_RS;
}

  /*for adding payment to sabre ends here*/
function OTA_AirBookRQ_Old($search_id, $traveler_details1, $Action = 'EnhancedAirBookRQ'){
  $search_data['data'] = $this->search_data_my($traveler_details1['search_id']);
  // debug($search_data);exit('OTA_AirBookRQ');
   // $credencials  = set_credencials_flight();
    //$search_data  = json_decode(base64_decode($search_data1));
  $padult_count = isset($search_data['data']['adult']) ? $search_data['data']['adult'] : $search_data['data']['data']['adult_config'];
  $pchild_count = isset($search_data['data']['child']) ? $search_data['data']['child'] : $search_data['data']['data']['child_config'];
  //echo ($padult_count + $pchild_count);die;
    $segment_data   = $traveler_details1['token'];
    $Segments = '';
    for($j=0;$j< count($segment_data); $j++){
    for($ss=0;$ss< count($segment_data[$j]['OriginLocation']); $ss++){
      if($segment_data[$j]['MarriageGrp'][$ss]=="O")
        $MarriageGrp_status="false";
      else if($segment_data[$j]['MarriageGrp'][$ss]=="I")
        $MarriageGrp_status="true";
      $Segments .= "<FlightSegment DepartureDateTime='".$segment_data[$j]['DepartureDateTime_r'][$ss]."' ArrivalDateTime='".$segment_data[$j]['ArrivalDateTime_r'][$ss]."' FlightNumber='".$segment_data[$j]['FlighvgtNumber_no'][$ss]."' NumberInParty='".($padult_count + $pchild_count)."' ResBookDesigCode='".$segment_data[$j]['ResBookDesigCode'][$ss]."' Status='NN'>
               <DestinationLocation LocationCode='".$segment_data[$j]['DestinationLocation'][$ss]."'/>
               <Equipment AirEquipType='".$segment_data[$j]['Equipment'][$ss]."'/>
               <MarketingAirline Code='".$segment_data[$j]['MarketingAirline'][$ss]."' FlightNumber='".$segment_data[$j]['FlighvgtNumber_no'][$ss]."'/>
               <MarriageGrp Ind='".$MarriageGrp_status."'/>
               <OperatingAirline Code='".$segment_data[$j]['OperatingAirline'][$ss]."'/>
               <OriginLocation LocationCode='".$segment_data[$j]['OriginLocation'][$ss]."'/>
              </FlightSegment>";
    }
  }
    $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z"); 
  $OTA_AirBookRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id ."</eb:ConversationId>
                          <eb:Service>EnhancedAirBookRQ</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id ."</eb:MessageId>
                              <eb:Timestamp>".$pxdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$pxdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                    <EnhancedAirBookRQ Version='2.3.0' ReturnHostCommand='true' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://webservices.sabre.com/sabreXML/2011/10 http://webservices.sabre.com/wsdl/swso/EnhancedAirBook2.3.0RQ.xsd'>
                      <OTA_AirBookRQ HaltOnError='true'>
                        <HaltOnStatus Code='NN'/>
                        <HaltOnStatus Code='UC'/>
                        <HaltOnStatus Code='NO'/>
                        <HaltOnStatus Code='US'/>
                        <HaltOnStatus Code='LL'/>
                        <OriginDestinationInformation>
                          " . $Segments . "
                        </OriginDestinationInformation>
                        <RedisplayReservation NumAttempts='10' WaitInterval='5000'/>
                      </OTA_AirBookRQ>
                      <PostProcessing HaltOnError='true' IgnoreAfter='false' RedisplayReservation='true'/>
                      <PreProcessing HaltOnError='true' IgnoreBefore='false'/>  
                    </EnhancedAirBookRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";
              //debug($OTA_AirBookRQ);exit('OTA_AirBookRQ');
    $OTA_AirBookRS = $this->flight_processRequest($OTA_AirBookRQ);   
   // echo "<br/><pre/>OTA_AirBookRS";print_r($OTA_AirBookRS);//die;
    $OTA_AirBookRQ_RS = array(
              'OTA_AirBookRQ' => $OTA_AirBookRQ,
              'OTA_AirBookRS' => $OTA_AirBookRS
            );
    $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/OTA_AirBookRQ.xml";
  $fp = fopen($path,"wb");fwrite($fp,$OTA_AirBookRQ);fclose($fp);
  
  $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/OTA_AirBookRS.xml";
  $fp = fopen($path,"wb");fwrite($fp,$OTA_AirBookRS);fclose($fp); 
  return $OTA_AirBookRQ_RS;
}
function ReserveAirSeatLLSRQ($search_id, $search_data1, $selected_seats){
  $arrSelectedSeates = array();
  foreach($selected_seats as $sKey=>$sVal){
    if (strpos($sKey, 'hidjrnysegment') !== false) {
        $arrSelectedSeates[$sKey] = $sVal;
    }
  }

  //debug($arrSelectedSeates);
  //debug($search_data1);exit('ReserveAirSeatLLSRQ');
   /* $search_data    = json_decode(base64_decode($search_data1),true);
    $segment_data   = json_decode(base64_decode($segment_data1),true);
    $selected_seats = json_decode($selected_seats1,true);*/
    $arrSelvals = array();
    $s=1;
    foreach($arrSelectedSeates as $sKey=>$sVal){
        $strVals = explode(",",$sVal);
        $px = 1;
        for($p=0; $p<count($strVals); $p++){
            $strPaxSeg = $px.".1";// 1.1 = pax.segment
            //$strPaxSeg = $s.".".$px;// 1.1 = segment.pax
            $arrSelvals[$s][$strPaxSeg] = str_replace(" ","",$strVals[$p]);
            $px++;
        }
        $s++;
    }
    $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z"); 
    //debug($arrSelvals);exit();
    for($seg=1; $seg<=count($arrSelvals); $seg++){
        $arrSegmentDtls = $arrSelvals[$seg];
        foreach($arrSegmentDtls as $segKey=>$segVal){
          if($segVal !=""){
             $AirSeatRQ = "<?xml version='1.0' encoding='utf-8'?>
                      <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                          <soap-env:Header>
                              <eb:MessageHeader
                                  xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                                  <eb:From>
                                      <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                                  </eb:From>
                                  <eb:To>
                                      <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                                  </eb:To>
                                  <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                                  <eb:Service eb:type='OTA'>Air</eb:Service>
                                  <eb:Action>AirSeatLLSRQ</eb:Action>
                                  <eb:CPAID>".$this->ipcc."</eb:CPAID>
                                  <eb:MessageData>
                                      <eb:MessageId>".$this->message_id."</eb:MessageId>
                                      <eb:Timestamp>".$pxdata['timestamp']."</eb:Timestamp>
                                      <eb:TimeToLive>".$pxdata['timetolive']."</eb:TimeToLive>
                                  </eb:MessageData>
                              </eb:MessageHeader>
                              <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                                  <wsse:UsernameToken>
                                      <wsse:Username>".$this->UserName."</wsse:Username>
                                      <wsse:Password>".$this->Password."</wsse:Password>
                                      <Organization>".$this->ipcc."</Organization>
                                      <Domain>Default</Domain>
                                  </wsse:UsernameToken>
                                  <wsse:BinarySecurityToken>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                              </wsse:Security>
                          </soap-env:Header>
                          <soap-env:Body>
                              <AirSeatRQ xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' ReturnHostCommand='false' Version='2.0.0'>
                                  <Seats>
                                      <Seat NameNumber='".$segKey."' Number='".$segVal."' SegmentNumber='".$seg."'/>
                                  </Seats>
                              </AirSeatRQ>
                          </soap-env:Body>
                      </soap-env:Envelope>";
                      //echo $AirSeatRQ;
                      //echo "<br/>";
                      if(true){
                $AirSeatRS = $this->flight_processRequest($AirSeatRQ);  
                $fileName = "AddSeat_".str_replace(".","_",$segKey)."_".$seg."_";
                $AirSeatRQ_RS = array(
                          'AirSeatRQ' => $AirSeatRQ,
                          'AirSeatRS' => $AirSeatRS
                        );
                $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/".$fileName.$search_id."RQ.xml";
                $fp = fopen($path,"wb");fwrite($fp,$AirSeatRQ);fclose($fp);
              
                $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/".$fileName.$search_id."RS.xml";
                $fp = fopen($path,"wb");fwrite($fp,$AirSeatRS);fclose($fp);
              }
          }
        }
    }
    //exit();
    return $AirSeatRQ_RS;
  }

function OTA_AirPriceRQ($BinarySecurityToken, $search_id, $search_data1, $book_params, $Action = 'OTA_AirPriceLLSRQ'){
  $strSabrePath = "all_xml_logs/flight/sabre";
    // $credencials  = set_credencials_flight();
    $search_data  = $search_data1;  
    $adult_patch  = $child_patch = $infant_patch = '';

    // debug($search_data);die;
    $adultCount = $search_data['data']['adult_config'];
    $childCount = $search_data['data']['child_config'];
    $infantCount = $search_data['data']['infant_config'];

    if($adultCount > 0){
        $adult_patch = "<PassengerType Code='ADT' Quantity='".$adultCount."' Force='true'/>";
    }
    if($childCount > 0){
        $child_patch = "<PassengerType Code='CNN' Quantity='".$childCount."' Force='true'/>";
    }
    if($infantCount > 0){
        $infant_patch = "<PassengerType Code='INF' Quantity='".$infantCount."' Force='true'/>";
    }
  $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
  $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z"); 
  $OTA_AirPriceRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service>OTA_AirPriceLLSRQ</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$pxdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$pxdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                    <OTA_AirPriceRQ Version='2.8.0' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' ReturnHostCommand='true'>
                      <PriceRequestInformation Retain='true'>
                        <OptionalQualifiers>
                          <PricingQualifiers>
                            ".$adult_patch.$child_patch.$infant_patch."
                          </PricingQualifiers>
                        </OptionalQualifiers>
                      </PriceRequestInformation>
                    </OTA_AirPriceRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";

    $OTA_AirPriceRS = $this->flight_processRequest($OTA_AirPriceRQ, $this->system);   
    // debug($OTA_AirPriceRS);die;
    $OTA_AirPriceRQ_RS = array(
                'OTA_AirPriceRQ' => $OTA_AirPriceRQ,
                'OTA_AirPriceRS' => $OTA_AirPriceRS
              );
  //   $path = FCPATH. $strSabrePath . "/booking_logs/".$search_id."/OTA_AirPriceRQ.xml";
  // $fp = fopen($path,"wb");fwrite($fp,$OTA_AirPriceRQ);fclose($fp);
  
  // $path = FCPATH. $strSabrePath . "/booking_logs/".$search_id."/OTA_AirPriceRS.xml";
  // $fp = fopen($path,"wb");fwrite($fp,$OTA_AirPriceRS);fclose($fp); 
    return $OTA_AirPriceRQ_RS;
}

function OTA_AirPriceRQ_Old($search_data1,$traveler_details1, $Action = 'OTA_AirPriceLLSRQ'){
    //$credencials  = set_credencials_flight();
  $search_data['data'] = $this->search_data_my($traveler_details1['search_id']);
  
    $traveler_details   = $search_data['data'];
    //echo "<pre/>";print_r($traveler_details);exit;
    //$search_data  = json_decode(base64_decode($search_data1));  
    $adult_patch  = $child_patch = $infant_patch = '';
    if($traveler_details['adult'] > 0){
        $adult_patch = "<PassengerType Code='ADT' Quantity='".$traveler_details['adult']."' Force='true'/>";
    }
    if($traveler_details['child'] > 0){
        $child_patch = "<PassengerType Code='CNN' Quantity='".$traveler_details['child']."' Force='true'/>";
    }
    if($traveler_details['infant'] > 0){
        $infant_patch = "<PassengerType Code='INF' Quantity='".$traveler_details['infant']."' Force='true'/>";
    }
   $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z");  
  $OTA_AirPriceRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service>OTA_AirPriceLLSRQ</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$pxdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$pxdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                    <OTA_AirPriceRQ Version='2.8.0' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' ReturnHostCommand='true'>
                      <PriceRequestInformation Retain='true'>
                        <OptionalQualifiers>
                          <PricingQualifiers>
                            ".$adult_patch.$child_patch.$infant_patch."
                          </PricingQualifiers>
                        </OptionalQualifiers>
                      </PriceRequestInformation>
                    </OTA_AirPriceRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";
              // debug($OTA_AirPriceRQ);
    $OTA_AirPriceRS = $this->flight_processRequest($OTA_AirPriceRQ); 
     // debug($OTA_AirPriceRS);exit('OTA_AirPriceRS');  
    $OTA_AirPriceRQ_RS = array(
                'OTA_AirPriceRQ' => $OTA_AirPriceRQ,
                'OTA_AirPriceRS' => $OTA_AirPriceRS
              );
    $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_data1."/OTA_AirPriceRQ.xml";
  $fp = fopen($path,"wb");fwrite($fp,$OTA_AirPriceRQ);fclose($fp);
  
  $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_data1."/OTA_AirPriceRS.xml";
  $fp = fopen($path,"wb");fwrite($fp,$OTA_AirPriceRS);fclose($fp); 
    return $OTA_AirPriceRQ_RS;
}

function SpecialServiceRQ($BinarySecurityToken, $search_id, $search_data1, $book_params, $Action = 'SpecialServiceLLSRQ'){
    $strSabrePath = "all_xml_logs/flight/sabre";
    // $credencials    = set_credencials_flight();
    $search_data      = $search_data1;
    $segment_data     = $book_params['token']['token'][0]['SegmentDetails'];
    $traveler_details = $book_params;

    // debug($segment_data);die;
  $travellers1    = $travellers   = $PassengerType = '';$a = 1;$c = 0;$i = 0;
  //echo "<pre>";print_r($traveler_details);exit();
  // echo '<pre/>';print_r($traveler_details->contact_nationality);exit;
    $Segments = ''; $MarketingAirlineCode = array(); $ac = 0;
    //Sajith
  /*for($j=0;$j< count($segment_data); $j++){
    for($ss=0;$ss< count($segment_data[$j]['OriginLocation']); $ss++){
      $MarketingAirlineCode[$ac++] = $segment_data[$j]->MarketingAirline[$ss];
    }
  }*/
  
  /*$AirlineHostedTrue = '';$AirlineHostedFalse = '';
    if(in_array("AA",$MarketingAirlineCode)){ $AirlineHostedTrue = "Available";
    for($AAA = 0; $AAA < count($MarketingAirlineCode);$AAA++){
      if($MarketingAirlineCode[$AAA] !='AA'){ $AirlineHostedFalse = "Available"; break; }
    }
  }else{ $AirlineHostedFalse = "Available"; }*/
  
  
  /*for($aaa=0;$aaa < count($traveler_details['saladult']);$aaa++) {       
    $adultDOB = strtoupper(date("dMy", strtotime($traveler_details['dobadult'][$aaa])));
    if($traveler_details->mnameadult[$aaa] == ''){
      $adultText    = 'P/'.$traveler_details['contact_nationality'].'/123456789/'.$traveler_details['contact_nationality'].'/'.$adultDOB.'/'.($traveler_details['genderadult'][$aaa]=='Mr'?'M':'F').'/30MAY30/'.$traveler_details['lnameadult'][$aaa].'/'.$traveler_details['fnameadult'][$aaa].'/H-'.$a .'.'.'1';
      $adultTextName  = $traveler_details['lnameadult'][$aaa].' '.$traveler_details['fnameadult'][$aaa];
      $GivenNameXMLADT = "<GivenName>".$traveler_details['fnameadult'][$aaa]."</GivenName>";
    }else{
      $adultText    = 'P/'.$traveler_details['contact_nationality'].'/123456789/'.$traveler_details['contact_nationality'].'/'.$adultDOB.'/'.($traveler_details['genderadult'][$aaa]=='Mr'?'M':'F').'/30MAY30/'.$traveler_details['lnameadult'][$aaa].'/'.$traveler_details['fnameadult'][$aaa]." ".$traveler_details['mnameadult'][$aaa].'/H-'.$a .'.'.'1';
      $adultTextName  = $traveler_details['lnameadult'][$aaa].' '.$traveler_details['fnameadult'][$aaa]." ".$traveler_details['mnameadult'][$aaa];
      $GivenNameXMLADT = "<GivenName>".$traveler_details['fnameadult'][$aaa]." ".$traveler_details['mnameadult'][$aaa]."</GivenName>";
    }
    if($AirlineHostedFalse !=''){ 
      $travellers .= '<Service SSR_Code="DOCS">
                  <PersonName/>
                  <Text>'.$adultText.'</Text>
                  <VendorPrefs><Airline Hosted="false"/></VendorPrefs>
                </Service>';
    }
    if($AirlineHostedTrue !=''){
      $travellers .= '<Service SSR_Code="DOCS">
                <PersonName/>
                <Text>'.$adultText.'</Text>
                <VendorPrefs><Airline Hosted="true"/></VendorPrefs>
              </Service>';
    }
    $travellers1 .= '<SecureFlight SegmentNumber="A">
              <PersonName DateOfBirth="'.$traveler_details['dobadult'][$aaa].'" Gender="'.($traveler_details['saladult'][$aaa]=='Mr'?'M':'F').'" NameNumber="'.$a .'.1" >
              '.$GivenNameXMLADT.'
              <Surname>'.$traveler_details['lnameadult'][$aaa].'</Surname>
              </PersonName>
            </SecureFlight>';
    
              
    if(isset($traveler_details['salinfant'])){  
      $infant = $a;  
      for($inf=0; $inf < count($traveler_details['salinfant']); $inf++){

      $infantDOB        = strtoupper(date("dMy", strtotime($traveler_details['dobinfant'][$inf])));
      if($traveler_details->mnameinfant[$inf] == ''){
        $infText      = $traveler_details['lnameinfant'][$inf].'/'.$traveler_details['fnameinfant'][$inf].'/'.$infantDOB.'';
      }else{
        $infText      = $traveler_details['lnameinfant'][$inf].'/'.$traveler_details['fnameinfant'][$inf]." ".$traveler_details['mnameinfant'][$inf].'/'.$infantDOB.'';
      }
      if($AirlineHostedFalse !=''){ 
        $travellers .= '<Service SSR_Code="INFT">
              <PersonName NameNumber="'.$a . '.1" />
               <Text>'.$infText.'</Text>
               <VendorPrefs><Airline Hosted="false"/></VendorPrefs>
              </Service>';
      }
      if($AirlineHostedTrue !=''){
        $travellers .= '<Service SSR_Code="INFT">
              <PersonName NameNumber="'.$a . '.1" />
               <Text>'.$infText.'</Text>
               <VendorPrefs><Airline Hosted="true"/></VendorPrefs>
              </Service>';
      }

      $travellers.='<Service SegmentNumber="A" SSR_Code="FOID">
                      <PersonName NameNumber="'.$a .'.1"/>
                      <Text>'.'PP'.$traveler_details['contact_nationality'].'123456789'.'</Text>
                    </Service>';
      
      $infant++;
      }
    }
     $a++;       
  }*/

  // debug($traveler_details);die;
    if($traveler_details['passenger_type']){
        // $c = $a;
        for($ccc=0;$ccc < count($traveler_details['first_name']);$ccc++) {  
      $childDOB = strtoupper(date("dMy", strtotime($traveler_details['date_of_birth'][$ccc])));

      /*if($traveler_details['mnamechild'][$aaa] == ''){
        $childText = 'P/'.$traveler_details['contact_nationality'].'/123456789/'.$traveler_details['contact_nationality'].'/'.$childDOB.'/'.($traveler_details['genderchild'][$ccc]=='Mr'?'M':'F').'/30MAY30/'.$traveler_details['lnameadult'][$ccc].'/'.$traveler_details['fnamechild'][$ccc].'/H-'.$a .'.'.'1';
        $childTextName = $traveler_details['lnamechild'][$ccc].' '.$traveler_details['fnamechild'][$ccc];
        $GivenNameXMLCHD = "<GivenName>".$traveler_details['fnamechild'][$ccc]."</GivenName>";
      }else{
        $childText = 'P/'.$traveler_details['contact_nationality'].'/123456789/'.$traveler_details['contact_nationality'].'/'.$childDOB.'/'.($traveler_details['genderchild'][$ccc]=='Mr'?'M':'F').'/30MAY30/'.$traveler_details['lnamechild'][$ccc].'/'.$traveler_details['fnamechild'][$ccc]." ".$traveler_details['mnamechild'][$ccc].'/H-'.$a .'.'.'1';
        $childTextName = $traveler_details['lnamechild'][$ccc].' '.$traveler_details['fnamechild'][$ccc]." ".$traveler_details['mnamechild'][$ccc];
        $GivenNameXMLCHD = "<GivenName>".$traveler_details['fnamechild'][$ccc]." ".$traveler_details['mnamechild'][$ccc]."</GivenName>";
      }*/

      $traveler_details['contact_nationality'] = 'IN';
      if($traveler_details['gender'] == 1){
        $traveler_details['gender'][$ccc] = 'M';
      }else{
        $traveler_details['gender'][$ccc] = 'F';
      }

      $d = $ccc + 1;
      $childText = 'P/'.$traveler_details['contact_nationality'].'/123456789/'.$traveler_details['contact_nationality'].'/'.$childDOB.'/'.($traveler_details['gender'][$ccc]).'/30MAY30/'.$traveler_details['last_name'][$ccc].'/'.$traveler_details['first_name'][$ccc]." ".$traveler_details['middle_name'][$ccc].'/H-'.$d .'.'.'1';
      $childTextName = $traveler_details['last_name'][$ccc].' '.$traveler_details['first_name'][$ccc]." ".$traveler_details['middle_name'][$ccc];
      $GivenNameXMLCHD = "<GivenName>".$traveler_details['first_name'][$ccc]." ".$traveler_details['middle_name'][$ccc]."</GivenName>";

      $AirlineHostedFalse = "Available";
      $AirlineHostedTrue = "";

      if($AirlineHostedFalse !=''){ 
        $travellers .= '<Service SSR_Code="DOCS">
                  <PersonName/>
                  <Text>'.$childText.'</Text>
                  <VendorPrefs><Airline Hosted="false"/></VendorPrefs>
                </Service>';
      }
              
      if($AirlineHostedTrue !=''){
        $travellers .= '<Service SSR_Code="DOCS">
                <PersonName/>
                <Text>'.$childText.'</Text>
                <VendorPrefs><Airline Hosted="true"/></VendorPrefs>
              </Service>';
      }

      $travellers1 .= '<SecureFlight SegmentNumber="A">
                <PersonName DateOfBirth="'.$traveler_details['date_of_birth'][$ccc].'" Gender="'.($traveler_details['gender'][$ccc]).'" NameNumber="'.$d .'.1" >
                '.$GivenNameXMLCHD.'
                <Surname>'.$traveler_details['last_name'][$ccc].'</Surname>
                </PersonName>
              </SecureFlight>';
      $travellers.='<Service SegmentNumber="A" SSR_Code="FOID">
                      <PersonName NameNumber="'.$d .'.1"/>
                      <Text>'.'PP'.$traveler_details['contact_nationality'].'123456789'.'</Text>
                    </Service>';
       //$a++; $c++;    
    }
    }

  $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
  $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z");
    // debug($travellers);die;
  $SpecialServiceRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service>SpecialServiceLLSRQ</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$pxdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$pxdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                    <SpecialServiceRQ Version='2.0.2' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' ReturnHostCommand='true'>
                      <SpecialServiceInfo>
                        ".$travellers."
                      </SpecialServiceInfo>
                    </SpecialServiceRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";
  
    $SpecialServiceRS = $this->flight_processRequest($SpecialServiceRQ, $this->system);    
    // echo '<pre/>';print_r($SpecialServiceRS);exit;
    $SpecialServiceRQ_RS = array(
                  'SpecialServiceRQ' => $SpecialServiceRQ,
                  'SpecialServiceRS' => $SpecialServiceRS
                );  
  //   $path = FCPATH. $strSabrePath . "/booking_logs/".$search_id."/SpecialServiceRQ.xml";
  // $fp = fopen($path,"wb");fwrite($fp,$SpecialServiceRQ);fclose($fp);
  
  // $path = FCPATH. $strSabrePath . "/booking_logs/".$search_id."/SpecialServiceRS.xml";
  // $fp = fopen($path,"wb");fwrite($fp,$SpecialServiceRS);fclose($fp);
    return $SpecialServiceRQ_RS;
}

function SpecialServiceRQ_old($search_id, $traveler_details1, $Action = 'SpecialServiceLLSRQ'){
   // $credencials    = set_credencials_flight();
  $search_data['data'] = $this->search_data_my($traveler_details1['search_id']);
  //echo 'search_data';debug($search_data);
  /*echo 'segment_data1';debug($traveler_details1['search_id']);exit('SpecialServiceRQ');*/
   // $search_data    = json_decode(base64_decode($search_data1));
  $pmysearch_dataccccc['data'] = $this->search_data_my($traveler_details1['search_id']);
  //echo 'segment_data1';debug($pmysearch_dataccccc['data']);exit('SpecialServiceRQ');
    $segment_data     = $traveler_details1['token'];
    $search_data1     = $traveler_details1;
    // debug($segment_data);exit;
    //$traveler_details   = json_decode(base64_decode($traveler_details1));
  $travellers1    = $travellers   = $PassengerType = '';$a = 1;$c = 0;$i = 0;
  // echo '<pre/>';print_r($traveler_details->contact_nationality);exit;
    $Segments = ''; $MarketingAirlineCode = array(); $ac = 0;
    for($j=0;$j< count($segment_data); $j++){
    for($ss=0;$ss< count($segment_data[$j]['OriginLocation']); $ss++){
      $MarketingAirlineCode[$ac++] = $segment_data[$j]['MarketingAirline'][$ss];
    }
  }
   // debug($MarketingAirlineCode);exit;
  $AirlineHostedTrue = '';$AirlineHostedFalse = '';$psAirlineHostedTrue = '';
    if(in_array("AA",$MarketingAirlineCode)){ $AirlineHostedTrue = "Available";
    $psAirlineHostedTrue = "true";
    for($AAA = 0; $AAA < count($MarketingAirlineCode);$AAA++){
      if($MarketingAirlineCode[$AAA] !='AA'){ $AirlineHostedFalse = "Available";
      $psAirlineHostedTrue = "false";
       break; }
    }
  }else{ $AirlineHostedFalse = "Available";
      $psAirlineHostedTrue = "false";
   }
  //debug($search_data1);die;
   $pcounter = 0;
  for($aaa=0;$aaa < count($search_data1['first_name']);$aaa++) {  
  if ($search_data1['passenger_type'][$aaa] == "Adult") { 
    //debug($search_data1);die;
    $search_data1['passenger_passport_issuing_country'][$aaa] = ($search_data1['passenger_passport_issuing_country'][$aaa] == '92') ? 'IN' : 'CA';
    $exp_pass_date = $search_data1['passenger_passport_expiry_day'][$aaa].'-'.$search_data1['passenger_passport_expiry_month'][$aaa].'-'.$search_data1['passenger_passport_expiry_year'][$aaa];
    $exp_passport_date =  strtoupper(date("dMy", strtotime($exp_pass_date)));
    $adultDOB = strtoupper(date("dMy", strtotime($search_data1['date_of_birth'][$aaa])));
    //$adultText = '';
      $adultText    = 'P/'.$search_data1['passenger_passport_issuing_country'][$aaa].'/'.$search_data1['passenger_passport_number'][$aaa].'/'.$search_data1['passenger_passport_issuing_country'][$aaa].'/'.$adultDOB.'/'.($search_data1['gender'][$aaa]=='1'?'M':'F').'/'.$exp_passport_date.'/'.$search_data1['last_name'][$aaa].'/'.$search_data1['first_name'][$aaa].'/H-'.$a .'.'.'1';
      $adultTextName  = $search_data1['last_name'][$aaa].' '.$search_data1['first_name'][$aaa];
      $GivenNameXMLADT = "<GivenName>".$search_data1['first_name'][$aaa]."</GivenName>";
    if ($search_data1['passenger_passport_expiry_day'][$aaa] != "INVALIDIP") {
              if($AirlineHostedFalse !=''){ 
                $travellers .= '<Service SSR_Code="DOCS">
                            <PersonName/>
                            <Text>'.$adultText.'</Text>
                            <VendorPrefs><Airline Hosted="'.$psAirlineHostedTrue.'"/></VendorPrefs>
                          </Service>';
              }
              if($AirlineHostedTrue !=''){
                $travellers .= '<Service SSR_Code="DOCS">
                          <PersonName/>
                          <Text>'.$adultText.'</Text>
                          <VendorPrefs><Airline Hosted="'.$psAirlineHostedTrue.'"/></VendorPrefs>
                        </Service>';
              }
  }
    if ($search_data1['ADT_MEAL'][$aaa] != '') {
      $travellers .= '<Service SegmentNumber="1" SSR_Code="'.$search_data1['ADT_MEAL'][$aaa].'">
          <PersonName NameNumber="'.$a .'.1"/>
          <VendorPrefs>
            <Airline Hosted="'.$psAirlineHostedTrue.'"/>
          </VendorPrefs>
        </Service>';
    }
    if ($search_data1['ADT_SPECIAL'][$aaa] != '') {
      $travellers .= ' <Service SSR_Code="'.$search_data1['ADT_SPECIAL'][$aaa].'">
          <PersonName NameNumber="'.$a .'.1"/>
          <VendorPrefs>
            <Airline Hosted="'.$psAirlineHostedTrue.'"/>
          </VendorPrefs>
        </Service>';
    }
    $travellers1 .= '<SecureFlight SegmentNumber="A">
              <PersonName DateOfBirth="'.$search_data1['date_of_birth'][$aaa].'" Gender="'.($search_data1['gender'][$aaa]=='1'?'M':'F').'" NameNumber="'.$a .'.1" >
              '.$GivenNameXMLADT.'
              <Surname>'.$search_data1['last_name'][$aaa].'</Surname>
              </PersonName>
            </SecureFlight>';
     $a++; 
     $pcounter++;
      
      }

     
    // $pcounter =   $a;  
  }
  $inf_count = 0;
 // echo $pmysearch_dataccccc['data']['adult'] + 1; die;
  $pcounter = $pmysearch_dataccccc['data']['adult'] + 1;
   // echo $pcounter; die;
   for($aaa=0;$aaa < count($search_data1['first_name']);$aaa++) {  
    $inf_count++;
     if ($search_data1['passenger_type'][$aaa] == "Child") { 
    $search_data1['passenger_passport_issuing_country'][$aaa] = ($search_data1['passenger_passport_issuing_country'][$aaa] == '92') ? 'IN' : 'CA';
    $exp_pass_date = $search_data1['passenger_passport_expiry_day'][$aaa].'-'.$search_data1['passenger_passport_expiry_month'][$aaa].'-'.$search_data1['passenger_passport_expiry_year'][$aaa];
    $exp_passport_date =  strtoupper(date("dMy", strtotime($exp_pass_date)));
    $adultDOB = strtoupper(date("dMy", strtotime($search_data1['date_of_birth'][$aaa])));
    //$adultText = '';
      $adultText    = 'P/'.$search_data1['passenger_passport_issuing_country'][$aaa].'/'.$search_data1['passenger_passport_number'][$aaa].'/'.$search_data1['passenger_passport_issuing_country'][$aaa].'/'.$adultDOB.'/'.($search_data1['gender'][$aaa]=='1'?'M':'F').'/'.$exp_passport_date.'/'.$search_data1['last_name'][$aaa].'/'.$search_data1['first_name'][$aaa].'/H-'.$pcounter .'.'.'1';
      $adultTextName  = $search_data1['last_name'][$aaa].' '.$search_data1['first_name'][$aaa];
      $GivenNameXMLADT = "<GivenName>".$search_data1['first_name'][$aaa]."</GivenName>";
   if ($search_data1['passenger_passport_expiry_day'][$aaa] != "INVALIDIP") { 
    if($AirlineHostedFalse !=''){ 
      $travellers .= '<Service SSR_Code="DOCS">
                  <PersonName/>
                  <Text>'.$adultText.'</Text>
                  <VendorPrefs><Airline Hosted="'.$psAirlineHostedTrue.'"/></VendorPrefs>
                </Service>';
    }
    if($AirlineHostedTrue !=''){
      $travellers .= '<Service SSR_Code="DOCS">
                <PersonName/>
                <Text>'.$adultText.'</Text>
                <VendorPrefs><Airline Hosted="'.$psAirlineHostedTrue.'"/></VendorPrefs>
              </Service>';
    }
  }
    if ($search_data1['ADT_MEAL'][$aaa] != '') {
      $travellers .= '<Service SegmentNumber="1" SSR_Code="'.$search_data1['ADT_MEAL'][$aaa].'">
          <PersonName NameNumber="'.$pcounter .'.1"/>
          <VendorPrefs>
            <Airline Hosted="'.$psAirlineHostedTrue.'"/>
          </VendorPrefs>
        </Service>';
    }
    if ($search_data1['ADT_SPECIAL'][$aaa] != '') {
      $travellers .= ' <Service SSR_Code="'.$search_data1['ADT_SPECIAL'][$aaa].'">
          <PersonName NameNumber="'.$pcounter .'.1"/>
          <VendorPrefs>
            <Airline Hosted="'.$psAirlineHostedTrue.'"/>
          </VendorPrefs>
        </Service>';
    }
    $travellers1 .= '<SecureFlight SegmentNumber="A">
              <PersonName DateOfBirth="'.$search_data1['date_of_birth'][$aaa].'" Gender="'.($search_data1['gender'][$aaa]=='1'?'M':'F').'" NameNumber="'.$pcounter .'.1" >
              '.$GivenNameXMLADT.'
              <Surname>'.$search_data1['last_name'][$aaa].'</Surname>
              </PersonName>
            </SecureFlight>';
            //$inf_count++;
            $pcounter++;   
    
      }       

    
     $a++;    
  }
  $inf_count = 1;
  for($aaa=0;$aaa < count($search_data1['first_name']);$aaa++) {  
    if($search_data1['passenger_type'][$aaa] == "Infant"){          
      $infantDOB        = strtoupper(date("dMy", strtotime($search_data1['date_of_birth'][$aaa])));
        $infText      = $search_data1['last_name'][$aaa].'/'.$search_data1['first_name'][$aaa].'/'.$infantDOB.'';
      
      if($AirlineHostedFalse !=''){ 
        $travellers .= '<Service SSR_Code="INFT">
              <PersonName NameNumber="'.$inf_count . '.1" />
               <Text>'.$infText.'</Text>
               <VendorPrefs><Airline Hosted="'.$psAirlineHostedTrue.'"/></VendorPrefs>
              </Service>';
      }
      if($AirlineHostedTrue !=''){
        $travellers .= '<Service SSR_Code="INFT">
              <PersonName NameNumber="'.$inf_count . '.1" />
               <Text>'.$infText.'</Text>
               <VendorPrefs><Airline Hosted="'.$psAirlineHostedTrue.'"/></VendorPrefs>
              </Service>';
      }
      if ($search_data1['ADT_MEAL'][$aaa] != '') {
      $travellers .= '<Service SegmentNumber="1" SSR_Code="'.$search_data1['ADT_MEAL'][$aaa].'">
          <PersonName NameNumber="'.$inf_count .'.1"/>
          <VendorPrefs>
            <Airline Hosted="'.$psAirlineHostedTrue.'"/>
          </VendorPrefs>
        </Service>';
    }
    if ($search_data1['ADT_SPECIAL'][$aaa] != '') {
      $travellers .= ' <Service SSR_Code="'.$search_data1['ADT_SPECIAL'][$aaa].'">
          <PersonName NameNumber="'.$inf_count .'.1"/>
          <VendorPrefs>
            <Airline Hosted="'.$psAirlineHostedTrue.'"/>
          </VendorPrefs>
        </Service>';
    }
      $inf_count++;
      $a++;   
      $pcounter++;
    }
    
    // $pcounter =   $a;  
  }
    
    $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z"); 
  $SpecialServiceRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service>SpecialServiceLLSRQ</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$pxdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$pxdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                    <SpecialServiceRQ Version='2.0.2' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' ReturnHostCommand='true'>
                      <SpecialServiceInfo>
                        ".$travellers."
                      </SpecialServiceInfo>
                    </SpecialServiceRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";
  //echo '<pre/>';print_r($SpecialServiceRQ);//exit('SpecialServiceRS');
    $SpecialServiceRS = $this->flight_processRequest($SpecialServiceRQ);    
    //  echo '<pre/>';print_r($SpecialServiceRS);exit('SpecialServiceRS');
    $SpecialServiceRQ_RS = array(
                  'SpecialServiceRQ' => $SpecialServiceRQ,
                  'SpecialServiceRS' => $SpecialServiceRS
                );  
    $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/SpecialServiceRQ.xml";
  $fp = fopen($path,"wb");fwrite($fp,$SpecialServiceRQ);fclose($fp);
  
  $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/SpecialServiceRS.xml";
  $fp = fopen($path,"wb");fwrite($fp,$SpecialServiceRS);fclose($fp);
    return $SpecialServiceRQ_RS;
}
function QueuePlaceRQ($search_id, $PNR_NUMBER, $Action = 'QueuePlaceLLSRQ'){
    //$credencials = set_credencials_flight();
  $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z"); 
    $QueuePlaceRQ = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:eb="http://www.ebxml.org/namespaces/messageHeader" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
          <SOAP-ENV:Header>
            <eb:MessageHeader SOAP-ENV:mustUnderstand="1" eb:version="1.0">
              <eb:From>
             <eb:PartyId eb:type="urn:x12.org.IO5:01">'.$this->sabre_myemail.'</eb:PartyId>
              </eb:From>
              <eb:To>
                <eb:PartyId type="urn:x12.org:IO5:01">webservices.sabre.com</eb:PartyId>
              </eb:To>
              <eb:CPAId>'.$this->ipcc.'</eb:CPAId>
              <eb:ConversationId>'.$this->conversation_id.'</eb:ConversationId>
              <eb:Service>QueuePlaceLLSRQ</eb:Service>
              <eb:Action>QueuePlaceLLSRQ</eb:Action>
              <eb:MessageData>
                <eb:MessageId>mid:'.$this->message_id.'</eb:MessageId>
                <eb:Timestamp>'.$pxdata["timestamp"].'</eb:Timestamp>
                <eb:TimeToLive>'.$pxdata["timetolive"].'</eb:TimeToLive>
                <eb:Timeout>40</eb:Timeout>
              </eb:MessageData>
            </eb:MessageHeader>
            <wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/12/secext" xmlns:wsu="http://schemas.xmlsoap.org/ws/2002/12/utility">
              <wsse:BinarySecurityToken valueType="String" EncodingType="wsse:Base64Binary">' . $this->BinarySecurityToken . '</wsse:BinarySecurityToken>
            </wsse:Security>
          </SOAP-ENV:Header>
            <SOAP-ENV:Body>
             <QueuePlaceRQ xmlns="http://webservices.sabre.com/sabreXML/2011/10" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ReturnHostCommand="false" TimeStamp="2014-09-07T09:30:00-06:00" Version="2.0.4">
         <QueueInfo>
          <QueueIdentifier Number="100" PrefatoryInstructionCode="11" PseudoCityCode="M8F2"/>
          <UniqueID ID="'.$PNR_NUMBER.'"/>
        </QueueInfo>
       </QueuePlaceRQ>
      </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>';
    // echo '<pre/>';print_r($QueuePlaceRQ);
    $QueuePlaceRS = $this->flight_processRequest($QueuePlaceRQ); 
    $QueuePlaceRQ_RS = array(
                'QueuePlaceRQ' => $QueuePlaceRQ,
                'QueuePlaceRS' => $QueuePlaceRS
              );
    $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/QueuePlaceRQ.xml";
  $fp = fopen($path,"wb");fwrite($fp,$QueuePlaceRQ);fclose($fp);
    
  $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/QueuePlaceRS.xml";
  $fp = fopen($path,"wb");fwrite($fp,$QueuePlaceRS);fclose($fp);   
    return $QueuePlaceRQ_RS;
}


function EndTransactionRQ($BinarySecurityToken, $search_id, $search_data1, $book_params, $Action = 'EndTransactionLLSRQ'){
    $strSabrePath = "all_xml_logs/flight/sabre";
    // $credencials = set_credencials_flight();
    $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z"); 
    $EndTransactionRQ = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:eb="http://www.ebxml.org/namespaces/messageHeader" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
          <SOAP-ENV:Header>
            <eb:MessageHeader SOAP-ENV:mustUnderstand="1" eb:version="1.0">
              <eb:From>
             <eb:PartyId eb:type="urn:x12.org.IO5:01">'.$this->sabre_myemail.'</eb:PartyId>
              </eb:From>
              <eb:To>
                <eb:PartyId type="urn:x12.org:IO5:01">webservices.sabre.com</eb:PartyId>
              </eb:To>
              <eb:CPAId>'.$this->ipcc.'</eb:CPAId>
              <eb:ConversationId>'.$this->conversation_id.'</eb:ConversationId>
              <eb:Service>EndTransactionLLSRQ</eb:Service>
              <eb:Action>EndTransactionLLSRQ</eb:Action>
              <eb:MessageData>
                <eb:MessageId>mid:'.$this->message_id.'</eb:MessageId>
                <eb:Timestamp>'.$pxdata['timestamp'].'</eb:Timestamp>
                <eb:TimeToLive>'.$pxdata['timetolive'].'</eb:TimeToLive>
                <eb:Timeout>40</eb:Timeout>
              </eb:MessageData>
            </eb:MessageHeader>
            <wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/12/secext" xmlns:wsu="http://schemas.xmlsoap.org/ws/2002/12/utility">
              <wsse:BinarySecurityToken valueType="String" EncodingType="wsse:Base64Binary">' . $BinarySecurityToken . '</wsse:BinarySecurityToken>
            </wsse:Security>
          </SOAP-ENV:Header>
            <SOAP-ENV:Body>
             <EndTransactionRQ Version="2.0.1" xmlns="http://webservices.sabre.com/sabreXML/2011/10" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <EndTransaction Ind="true">
            </EndTransaction>
          <Source ReceivedFrom="bookurflight"/>
        </EndTransactionRQ>
      </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>';

    // debug($EndTransactionRQ);die;
    $EndTransactionRS = $this->flight_processRequest($EndTransactionRQ, $this->system); 
    $EndTransactionRQ_RS = array(
                'EndTransactionRQ' => $EndTransactionRQ,
                'EndTransactionRS' => $EndTransactionRS
              );
  //   $path = FCPATH. $strSabrePath . "/booking_logs/".$search_id."/EndTransactionRQ.xml";
  // $fp = fopen($path,"wb");fwrite($fp,$EndTransactionRQ);fclose($fp);
  
  // $path = FCPATH. $strSabrePath . "/booking_logs/".$search_id."/EndTransactionRS.xml";
  // $fp = fopen($path,"wb");fwrite($fp,$EndTransactionRS);fclose($fp);    
    return $EndTransactionRQ_RS;
}


function EndTransactionRQOld($search_id, $traveler_details1, $Action = 'EndTransactionLLSRQ'){
    //$credencials = set_credencials_flight();
    $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z"); 
    $EndTransactionRQ = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:eb="http://www.ebxml.org/namespaces/messageHeader" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
          <SOAP-ENV:Header>
            <eb:MessageHeader SOAP-ENV:mustUnderstand="1" eb:version="1.0">
              <eb:From>
             <eb:PartyId eb:type="urn:x12.org.IO5:01">'.$this->sabre_myemail.'</eb:PartyId>
              </eb:From>
              <eb:To>
                <eb:PartyId type="urn:x12.org:IO5:01">webservices.sabre.com</eb:PartyId>
              </eb:To>
              <eb:CPAId>'.$this->ipcc.'</eb:CPAId>
              <eb:ConversationId>'.$this->conversation_id.'</eb:ConversationId>
              <eb:Service>EndTransactionLLSRQ</eb:Service>
              <eb:Action>EndTransactionLLSRQ</eb:Action>
              <eb:MessageData>
                <eb:MessageId>mid:'.$this->message_id.'</eb:MessageId>
                <eb:Timestamp>'.$pxdata["timestamp"].'</eb:Timestamp>
                <eb:TimeToLive>'.$pxdata["timetolive"].'</eb:TimeToLive>
                <eb:Timeout>40</eb:Timeout>
              </eb:MessageData>
            </eb:MessageHeader>
            <wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/12/secext" xmlns:wsu="http://schemas.xmlsoap.org/ws/2002/12/utility">
              <wsse:BinarySecurityToken valueType="String" EncodingType="wsse:Base64Binary">' . $this->BinarySecurityToken . '</wsse:BinarySecurityToken>
            </wsse:Security>
          </SOAP-ENV:Header>
            <SOAP-ENV:Body>
             <EndTransactionRQ Version="2.0.1" xmlns="http://webservices.sabre.com/sabreXML/2011/10" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <EndTransaction Ind="true">
            </EndTransaction>
          <Source ReceivedFrom="Airliners"/>
        </EndTransactionRQ>
      </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>';
    $EndTransactionRS = $this->flight_processRequest($EndTransactionRQ); 
   // debug($EndTransactionRS);exit('EndTransactionRS');
    $EndTransactionRQ_RS = array(
                'EndTransactionRQ' => $EndTransactionRQ,
                'EndTransactionRS' => $EndTransactionRS
              );
    $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/EndTransactionRQ.xml";
  $fp = fopen($path,"wb");fwrite($fp,$EndTransactionRQ);fclose($fp);
  
  $path = "..b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/EndTransactionRS.xml";
  $fp = fopen($path,"wb");fwrite($fp,$EndTransactionRS);fclose($fp);    
    return $EndTransactionRQ_RS;
}

function EndTransactionRQFinal($BinarySecurityToken, $Name, $pnr,  $Action = 'EndTransactionLLSRQ'){
    //$credencials = set_credencials_flight();
    $pxdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $pxdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z"); 
    $EndTransactionRQ = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:eb="http://www.ebxml.org/namespaces/messageHeader" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
          <SOAP-ENV:Header>
            <eb:MessageHeader SOAP-ENV:mustUnderstand="1" eb:version="1.0">
              <eb:From>
                <eb:PartyId eb:type="urn:x12.org.IO5:01">'.$this->sabre_myemail.'</eb:PartyId>
              </eb:From>
              <eb:To>
                <eb:PartyId type="urn:x12.org:IO5:01">webservices.sabre.com</eb:PartyId>
              </eb:To>
              <eb:CPAId>'.$this->ipcc.'</eb:CPAId>
              <eb:ConversationId>'.$this->conversation_id.'</eb:ConversationId>
              <eb:Service>EndTransactionLLSRQ</eb:Service>
              <eb:Action>EndTransactionLLSRQ</eb:Action>
              <eb:MessageData>
                <eb:MessageId>mid:'.$this->message_id.'</eb:MessageId>
                <eb:Timestamp>'.$pxdata["timestamp"].'</eb:Timestamp>
                <eb:TimeToLive>'.$pxdata["timetolive"].'</eb:TimeToLive>
                <eb:Timeout>40</eb:Timeout>
              </eb:MessageData>
            </eb:MessageHeader>
            <wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/12/secext" xmlns:wsu="http://schemas.xmlsoap.org/ws/2002/12/utility">
              <wsse:BinarySecurityToken valueType="String" EncodingType="wsse:Base64Binary">' . $this->BinarySecurityToken . '</wsse:BinarySecurityToken>
            </wsse:Security>
          </SOAP-ENV:Header>
            <SOAP-ENV:Body>
             <EndTransactionRQ Version="2.0.1" xmlns="http://webservices.sabre.com/sabreXML/2011/10" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <EndTransaction Ind="true">
            </EndTransaction>
          <Source ReceivedFrom="' . $Name . '"/>
        </EndTransactionRQ>
      </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>';
    $EndTransactionRS = $this->flight_processRequest($EndTransactionRQ); 
    $EndTransactionRQ_RS = array(
                'EndTransactionRQ' => $EndTransactionRQ,
                'EndTransactionRS' => $EndTransactionRS
              );
    $XmlReqFileName = 'EndTransactionRQ'.$pnr; $XmlResFileName = 'EndTransactionRS'.$pnr;
    $path   = "..b2b2b/xml_logs/Flight/sabre/cancel_logs/".$XmlReqFileName.".xml";
  $fp   = fopen($path,"wb");fwrite($fp,$EndTransactionRQ);fclose($fp);
  
  $path   = "..b2b2b/xml_logs/Flight/sabre/cancel_logs/".$XmlResFileName.".xml";
  $fp   = fopen($path,"wb");fwrite($fp,$EndTransactionRS);fclose($fp);  
    return $EndTransactionRQ_RS;   
}
function EnhancedSeatMapRQ($BinarySecurityToken, $segment_data1,$request=''){
    //debug($segment_data1);die('EnhancedSeatMapRQ');
    $this->set_api_credentials();
    $appCurency = get_application_default_currency();
    $psegment_data = $segment_data1['booking_data']['booking_itinerary_details'];
    $psearch_data  = $segment_data1['search_data'];
    //debug($psearch_data);die;
    //$segment_data   = json_decode(base64_decode($segment_data1));
    $myseatmapresponse = array();
    foreach ($psegment_data as $key => $segment_data) {
      //debug($segment_data);die('segment_data');
      //debug(date('Y-m-d',strtotime($segment_data['departure_datetime'])));die('segment_data');
      $tdsdept = date('Y-m-d',strtotime($segment_data['departure_datetime']));
      $tdsarrival = date('Y-m-d',strtotime($segment_data['arrival_datetime']));
      $economyCode = 'Y'; $SegmentType = '';
      $economy = $segment_data["cabin_class"];
      if ($economy == 'Economy') {
        $economyCode = 'Y'; $KeepSameCabin = '';
      }else{
        $KeepSameCabin = '<JumpCabinLogic Disabled="true"/><KeepSameCabin Enabled="true"/>';
      }
      /*if ($economy == 'Economy') {
        $economyCode = 'Y';
      }*/
      if ($economy == 'PremiumEconomy') {
        $economyCode = 'S';
      }
      if ($economy == 'Business') {
        $economyCode = 'C';
      }
      if ($economy == 'PremiumBusiness') {
        $economyCode = 'J';
      }
      if ($economy == 'First') {
        $economyCode = 'F';
      }
      if ($economy == 'PremiumFirst') {
        $economyCode = 'P';
      }
      $pxdata['timestamp']   = gmdate("Y-m-d\TH-i-s\Z");
      $pxdata['timetolive']  = gmdate("Y-m-d\TH-i-s\Z"); 
      $Air_SeatmapRQ = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:eb="http://www.ebxml.org/namespaces/messageHeader" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
                   <SOAP-ENV:Header>
                    <eb:MessageHeader
                                xmlns:eb="http://www.ebxml.org/namespaces/messageHeader">
                                <eb:From>
                                    <eb:PartyId eb:type="urn:x12.org.IO5:01">"'.$this->sabre_myemail.'"</eb:PartyId>
                                </eb:From>
                                <eb:To>
                                    <eb:PartyId eb:type="urn:x12.org.IO5:01">webservices.sabre.com</eb:PartyId>
                                </eb:To>
                                <eb:ConversationId>"'.$this->conversation_id.'"</eb:ConversationId>
                                <eb:Service eb:type="OTA">Air</eb:Service>
                                <eb:Action>EnhancedSeatMapRQ</eb:Action>
                                <eb:CPAID>'.$this->ipcc.'</eb:CPAID>
                                <eb:MessageData>
                                    <eb:MessageId>"'.$this->message_id.'"</eb:MessageId>
                                    <eb:Timestamp>"'.$pxdata['timestamp'].'"</eb:Timestamp>
                                    <eb:TimeToLive>"'.$pxdata['timetolive'].'"</eb:TimeToLive>
                                </eb:MessageData>
                            </eb:MessageHeader>
                    <wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/12/secext" xmlns:wsu="http://schemas.xmlsoap.org/ws/2002/12/utility">
                     <wsse:BinarySecurityToken valueType="String" EncodingType="wsse:Base64Binary">' . $BinarySecurityToken . '</wsse:BinarySecurityToken>
                    </wsse:Security>
                   </SOAP-ENV:Header>
                   <SOAP-ENV:Body>
                    <EnhancedSeatMapRQ xmlns="http://stl.sabre.com/Merchandising/v4">
                      <SeatMapQueryEnhanced>
                        <RequestType>Payload</RequestType>
                        <Flight destination="'.$segment_data["to_airport_code"].'" origin="'.$segment_data["from_airport_code"].'">
                          <DepartureDate>'.$tdsdept.'</DepartureDate>
                          <Operating carrier="'.$segment_data["airline_code"].'">'.$segment_data["flight_number"].'</Operating>
                          <Marketing carrier="'.$segment_data["airline_code"].'">'.$segment_data["flight_number"].'</Marketing>
                          <ArrivalDate>'.$tdsarrival.'</ArrivalDate>
                        </Flight>
                        <CabinDefinition><RBD>'.$economyCode.'</RBD></CabinDefinition>
                        <Currency>'.$appCurency.'</Currency>
                        <POS><PCC>'.$this->ipcc.'</PCC></POS>
                        <JourneyData>
                          <JourneyFlight>
                            <Flight destination="'.$segment_data["to_airport_code"].'" origin="'.$segment_data["from_airport_code"].'">
                              <DepartureDate>'.$tdsdept.'</DepartureDate>
                              <Operating carrier="'.$segment_data["airline_code"].'">'.$segment_data["flight_number"].'</Operating>
                              <Marketing carrier="'.$segment_data["airline_code"].'">'.$segment_data["flight_number"].'</Marketing>
                              <ArrivalDate>'.$tdsarrival.'</ArrivalDate>
                            </Flight> 
                          </JourneyFlight>
                        </JourneyData>
                    </SeatMapQueryEnhanced>
                  </EnhancedSeatMapRQ>
                  </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>';
                //echo $Air_SeatmapRQ;//die;
        $strFlt_data = $segment_data["from_airport_code"]."_".$segment_data["to_airport_code"]."_".$segment_data["airline_code"]."_".$segment_data["flight_number"];
        $fp = fopen("xml_logs/Flight/sabre/search_logs/Air_SeatmapRQ_".$psearch_data['search_id']."_".$strFlt_data.".xml", 'wb');
        fwrite($fp, $Air_SeatmapRQ); fclose($fp);

        $Air_SeatmapRS = $this->flight_processRequest($Air_SeatmapRQ);
        $IMAP_AirSeatMapLLSRQ_RS = array('Air_SeatmapRQ' => $Air_SeatmapRQ,'Air_SeatmapRS' => $Air_SeatmapRS);
        $myseatmapresponse[$key]  = $this->seatmapformating($Air_SeatmapRS, 0, $key);
        
        $fp = fopen("xml_logs/Flight/sabre/search_logs/Air_SeatmapRS_".$psearch_data['search_id']."_".$strFlt_data.".xml", 'wb');
        fwrite($fp, $Air_SeatmapRS); fclose($fp);
     }  
    // echo '<pre/>';print_r($segment_data);exit;

 /* $CI =& get_instance();
    $CI->load->model('Flight_Model');
    $CI->Flight_Model->update_flight_aireat_response($search_data[0]->search_parameter_details_id, $segment_data[$type]->FlightDetailsID,$XmlReqFileName,$XmlResFileName);*/
  //debug($myseatmapresponse);exit;  
  return $myseatmapresponse;
}
function xml2array_seat($contents, $get_attributes = 1) {
        if (!$contents) return array();
        if (!function_exists('xml_parser_create')) { return array(); }
        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, $contents, $xml_values);
        xml_parser_free($parser);
        if (!$xml_values)
            return;
    $xml_array = array(); $parents = array(); $opened_tags = array(); $arr = array();
    $current = &$xml_array;
    foreach ($xml_values as $data) {
            unset($attributes, $value);
            extract($data);
      $result = '';
            if ($get_attributes) {
                $result = array();
                if (isset($value))
                    $result['value'] = $value;
        if (isset($attributes)) {
                    foreach ($attributes as $attr => $val) {
                        if ($get_attributes == 1)
                            $result['attr'][$attr] = $val;
                    }
                }
            } elseif (isset($value)) {
                $result = $value;
            }
      if ($type == "open") {
                $parent[$level - 1] = &$current;

                if (!is_array($current) or ( !in_array($tag, array_keys($current)))) { 
                    $current[$tag] = $result;
                    $current = &$current[$tag];
                } else { 
                    if (isset($current[$tag][0])) {
                        array_push($current[$tag], $result);
                    } else {
                        $current[$tag] = array($current[$tag], $result);
                    }
                    $last = count($current[$tag]) - 1;
                    $current = &$current[$tag][$last];
                }
            } elseif ($type == "complete") {
                if (!isset($current[$tag])) {
                    $current[$tag] = $result;
                } else { 
                    if ((is_array($current[$tag]) and $get_attributes == 0)
                            or ( isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
                        array_push($current[$tag], $result);
                    } else {
                        $current[$tag] = array($current[$tag], $result);
                    }
                }
            } elseif ($type == 'close') { 
                $current = &$parent[$level - 1];
            }
        }
        return($xml_array);
    }
public function seatmapformating($Air_SeatmapRS, $type, $segment){
    $SeatmapRS    = $this->xml2array_seat($Air_SeatmapRS);
    if (isset($SeatmapRS['soap-env:Envelope']['soap-env:Body']['ns6:EnhancedSeatMapRS']['ns4:ApplicationResults']['attr']['status'])){
        $air_map_status = $SeatmapRS['soap-env:Envelope']['soap-env:Body']['ns6:EnhancedSeatMapRS']['ns4:ApplicationResults']['attr']['status'];
        if($air_map_status == "Complete"){
          $air_map_data = $SeatmapRS['soap-env:Envelope']['soap-env:Body']['ns6:EnhancedSeatMapRS']['ns6:SeatMap']; 
        }else{
          $air_map_data = "";
        }
    }else if(isset($SeatmapRS['soap-env:Envelope']['soap-env:Body']['EnhancedSeatMapRS']['ns3:ApplicationResults']['attr']['status'])){
      $air_map_status = $SeatmapRS['soap-env:Envelope']['soap-env:Body']['EnhancedSeatMapRS']['ns3:ApplicationResults']['attr']['status'];
      if($air_map_status == "Complete"){
        $air_map_data = $SeatmapRS['soap-env:Envelope']['soap-env:Body']['EnhancedSeatMapRS']['SeatMap']; 
      }else{
        $air_map_data = "";
      }
    }else{
      $air_map_data = "";
    }
    // echo '<pre/>';print_r($air_map_data);exit;
    if($air_map_data !=''){
      if(isset($air_map_data['ns6:Cabin'])){
        $CabinDetails1 = $air_map_data['ns6:Cabin'];
      }else{
        $CabinDetails1 = $air_map_data['Cabin'];
      }
      if(isset($CabinDetails1[0]))
        $CabinDetails = $CabinDetails1;
      else  
        $CabinDetails[0] = $CabinDetails1;
      
      foreach($CabinDetails as $c => $cabin){
        $airSeatMapDeatils['cabin']['firstRow'][$c]         = $cabin['attr']['firstRow'];
        $airSeatMapDeatils['cabin']['lastRow'][$c]          = $cabin['attr']['lastRow'];
        $airSeatMapDeatils['cabin']['classLocation'][$c]      = $cabin['attr']['classLocation'];
        $airSeatMapDeatils['cabin']['seatOccupationDefault'][$c]  = $cabin['attr']['seatOccupationDefault'];
        if(isset($cabin['ns6:Column'])){
          $cabinColumnDetails = $cabin['ns6:Column'];
        }else{
          $cabinColumnDetails = $cabin['Column'];
        }
        // echo '<pre/>';print_r($cabinColumnDetails);exit;
        foreach($cabinColumnDetails as $cd => $ColumnDetails){
          if(isset($ColumnDetails['ns6:Column']['value'])){
            $airSeatMapDeatils['cabin']['seatColumn'][$cd]       = $ColumnDetails['ns6:Column']['value'];
            $airSeatMapDeatils['cabin']['columnCharacteristic'][$cd] = $ColumnDetails['ns6:Characteristics']['value'];
          }else{
            $airSeatMapDeatils['cabin']['seatColumn'][$cd]       = $ColumnDetails['Column']['value'];
            $airSeatMapDeatils['cabin']['columnCharacteristic'][$cd] = $ColumnDetails['Characteristics']['value'];
          }
        }
                
        if(isset($cabin['ns6:Row'])){
          $row_details = $cabin['ns6:Row'];
        }else{
          $row_details = $cabin['Row'];
        }
        //debug($row_details);exit();
        foreach($row_details as $r => $row){
          if(isset($row['ns6:RowNumber']['value'])){
            $airSeatMapDeatils['row'][$r]['seatRowNumber'] = $row['ns6:RowNumber']['value'];
          }else{
            $airSeatMapDeatils['row'][$r]['seatRowNumber'] = $row['RowNumber']['value'];
          }
          if(isset($row['rowDetails']['rowCharacteristicsDetails']))
            $airSeatMapDeatils['row'][$r]['rowCharacteristicsDetails'] = $row['rowDetails']['rowCharacteristicsDetails']['rowCharacteristic'];
          if(isset($row['ns6:Seat'])){
            $rowDetails = $row['ns6:Seat'];
            foreach($rowDetails as $s => $seat){
              $airSeatMapDeatils['row'][$r]['seatColumn'][$s] = $seat['ns6:Number']['value'];
              if(isset($seat['ns6:Limitations']['ns6:Detail']['value'])){
                $airSeatMapDeatils['row'][$r]['Limitations'][$s] = $seat['ns6:Limitations']['ns6:Detail']['value'];
              }
              
              if(isset($seat['ns6:Facilities']['ns6:Detail']['value'])){
                $airSeatMapDeatils['row'][$r]['Facilities'][$s] = $seat['ns6:Facilities']['ns6:Detail']['value'];
              }
              if(isset($seat['ns6:Price']['ns6:TotalAmount']['value'])){
                $airSeatMapDeatils['row'][$r]['TotalAmount'][$s] = $seat['ns6:Price']['ns6:TotalAmount']['value'];
              }else{
                $airSeatMapDeatils['row'][$r]['TotalAmount'][$s] = 0;
              }
              
              if(isset($seat['attr']['occupiedInd'])){
                $airSeatMapDeatils['row'][$r]['occupiedInd'][$s]          = $seat['attr']['occupiedInd'];
                $airSeatMapDeatils['row'][$r]['inoperativeInd'][$s]       = $seat['attr']['inoperativeInd'];
                $airSeatMapDeatils['row'][$r]['premiumInd'][$s]           = $seat['attr']['premiumInd'];
                $airSeatMapDeatils['row'][$r]['chargeableInd'][$s]        = $seat['attr']['chargeableInd'];
                $airSeatMapDeatils['row'][$r]['exitRowInd'][$s]           = $seat['attr']['exitRowInd'];
                $airSeatMapDeatils['row'][$r]['restrictedReclineInd'][$s] = $seat['attr']['restrictedReclineInd'];
                $airSeatMapDeatils['row'][$r]['noInfantInd'][$s]          = $seat['attr']['noInfantInd'];
              }
            }
          }else if(isset($row['Seat'])){
            $rowDetails = $row['Seat'];
            foreach($rowDetails as $s => $seat){
              $airSeatMapDeatils['row'][$r]['seatColumn'][$s] = $seat['Number']['value'];
              if(isset($seat['Limitations']['Detail']['value'])){
                $airSeatMapDeatils['row'][$r]['Limitations'][$s] = $seat['Limitations']['Detail']['value'];
              }
              
              if(isset($seat['Facilities']['Detail']['value'])){
                $airSeatMapDeatils['row'][$r]['Facilities'][$s] = $seat['Facilities']['Detail']['value'];
              }
              if(isset($seat['Price']['TotalAmount']['value'])){
                $airSeatMapDeatils['row'][$r]['TotalAmount'][$s] = $seat['Price']['TotalAmount']['value'];
              }else{
                $airSeatMapDeatils['row'][$r]['TotalAmount'][$s] = 0;
              }
              
              if(isset($seat['attr']['occupiedInd'])){
                $airSeatMapDeatils['row'][$r]['occupiedInd'][$s]          = $seat['attr']['occupiedInd'];
                $airSeatMapDeatils['row'][$r]['inoperativeInd'][$s]       = $seat['attr']['inoperativeInd'];
                $airSeatMapDeatils['row'][$r]['premiumInd'][$s]           = $seat['attr']['premiumInd'];
                $airSeatMapDeatils['row'][$r]['chargeableInd'][$s]        = $seat['attr']['chargeableInd'];
                $airSeatMapDeatils['row'][$r]['exitRowInd'][$s]           = $seat['attr']['exitRowInd'];
                $airSeatMapDeatils['row'][$r]['restrictedReclineInd'][$s] = $seat['attr']['restrictedReclineInd'];
                $airSeatMapDeatils['row'][$r]['noInfantInd'][$s]          = $seat['attr']['noInfantInd'];
              }
            }
          }
        } 
        //echo '<pre/>';print_r($airSeatMapDeatils);exit;              
      }
    }
    $data['journey']        = $type;
    $data['segment']        = $segment;
    $data['airSeatMapDeatils']    = $airSeatMapDeatils;
    return $data;
}
  /**
   * set API credentials
   */
  private function set_api_credentials() {
    $engine_system  = $this->CI->config->item ( 'flight_engine_system' );
    $this->system   = $engine_system;
    $this->details  = $this->CI->config->item ( 'sabre_flight_live');
    $this->Url      = $this->details ['api_url'];
    $this->UserName = $this->details ['username'];
    $this->Password = $this->details ['password'];
    $this->ipcc     = $this->details ['ipcc'];
    $this->sabre_myemail     = $this->details ['sabre_email'];
    $this->conversation_id  = time().$this->sabre_myemail;
    $this->message_id     = "mid:".time().$this->sabre_myemail;
  }
  
  /**
   * request Header
   */
  private function get_header() {
    // Vipassana centre
    $header = array (
        'Accept: text/xml',
        'Accept-Encoding: gzip, deflate',
        'Content-Type: text/xml; charset=utf-8' 
    );
    return array (
        'header' => $header 
    );
  }
  
  /**
   *
   * @param int $search_id          
   */
  public function search_data($search_id) {
    $response ['status'] = true;
    $response ['data'] = array ();
    if (empty ( $this->master_search_data ) == true and valid_array ( $this->master_search_data ) == false) {
      $clean_search_details = $this->CI->flight_model->get_safe_search_data ( $search_id );
      //debug($search_id);die;
      if ($clean_search_details ['status'] == true) {
        $response ['status'] = true;
        $response ['data'] = $clean_search_details ['data'];
        // 28/12/2014 00:00:00 - date format
        if ($clean_search_details ['data'] ['trip_type'] == 'multicity') {
          $response ['data'] ['from'] = $clean_search_details ['data'] ['from'];
          $response ['data'] ['to'] = $clean_search_details ['data'] ['to'];
          $response ['data'] ['from_city'] = $clean_search_details ['data'] ['from'];
          $response ['data'] ['to_city'] = $clean_search_details ['data'] ['to'];
          $response ['data'] ['depature'] = $clean_search_details ['data'] ['depature'];
          $response ['data'] ['return'] = $clean_search_details ['data'] ['depature'];
        } else {
          $response ['data'] ['from'] = substr ( chop ( substr ( $clean_search_details ['data'] ['from'], - 5 ), ')' ), - 3 );
          $response ['data'] ['to'] = substr ( chop ( substr ( $clean_search_details ['data'] ['to'], - 5 ), ')' ), - 3 );
          $response ['data'] ['from_city'] = $clean_search_details ['data'] ['from'];
          $response ['data'] ['to_city'] = $clean_search_details ['data'] ['to'];
          $response ['data'] ['depature'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['depature'] ) ) . 'T00:00:00';
          $response ['data'] ['return'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['depature'] ) ) . 'T00:00:00';
        }
        
        switch ($clean_search_details ['data'] ['trip_type']) {
          
          case 'oneway' :
            $response ['data'] ['type'] = 'OneWay';
            break;
          
          case 'circle' :
            $response ['data'] ['type'] = 'Return';
            $response ['data'] ['return'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['return'] ) ) . 'T00:00:00';
            break;
          case 'multicity' :
            $response ['data'] ['type'] = 'MultiCity';
            break;
          case 'gdsspecial' :
            $response ['data'] ['type'] = 'GDS Special';
            $response ['data'] ['return'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['return'] ) ) . 'T00:00:00';
            break;
          
          default :
            $response ['data'] ['type'] = 'OneWay';
        }
        //debug($clean_search_details);exit();
        $response ['data'] ['adult'] = isset($clean_search_details ['data'] ['adult_config'])?$clean_search_details ['data'] ['adult_config']:$clean_search_details ['data'] ['adult'];
        $response ['data'] ['child'] = isset($clean_search_details ['data'] ['child_config'])?$clean_search_details ['data'] ['child_config']:$clean_search_details ['data'] ['child'];
        $response ['data'] ['infant'] = isset($clean_search_details ['data'] ['infant_config'])?$clean_search_details ['data'] ['infant_config']:$clean_search_details ['data'] ['infant'];
        $response ['data'] ['adult_config'] = isset($clean_search_details ['data'] ['adult_config'])?$clean_search_details ['data'] ['adult_config']:$clean_search_details ['data'] ['adult'];
        $response ['data'] ['child_config'] = isset($clean_search_details ['data'] ['child_config'])?$clean_search_details ['data'] ['child_config']:$clean_search_details ['data'] ['child'];
        $response ['data'] ['infant_config'] = isset($clean_search_details ['data'] ['infant_config'])?$clean_search_details ['data'] ['infant_config']:$clean_search_details ['data'] ['infant'];
        
        $response ['data'] ['v_class'] = $clean_search_details ['data'] ['v_class'];
        $response ['data'] ['carrier'] = implode ( $clean_search_details ['data'] ['carrier'] );
        $this->master_search_data = $response ['data'];
      } else {
        $response ['status'] = false;
      }
    } else {
      $response ['data'] = $this->master_search_data;
    }
    $this->search_hash = md5 ( serialized_data ( $response ['data'] ) );
    return $response;
  }
  
  /**
   * flight search request
   *
   * @param $search_id unique
   *          id which identifies search details
   */
  function get_flight_list($search_id, $currency_obj) {
    
    $this->CI->load->driver ( 'cache' );
    $response ['data']    = array ();
    $response ['status']  = SUCCESS_STATUS;
    
    /* get search criteria based on search id */
    $search_data = $this->search_data ( $search_id );
    $header_info = $this->get_header ();
    // generate unique searchid string to enable caching
    $cache_search = $this->CI->config->item ( 'cache_flight_search' );
    $search_hash  = $this->search_hash;
    //die($search_data ['status']);

    /*if ($cache_search) {
      $cache_contents = $this->CI->cache->file->get ( $search_hash );
    }*/
  
    //debug($cache_contents);die;

    if ($search_data ['status'] == SUCCESS_STATUS) {
      if ($cache_search == false || ($cache_search === true && empty ( $cache_contents ) == true)) {
        //debug($search_data);die('if');
        //Advance search
        if(false) {
          
          die(" til here case 1");
          $search_avilability = $this->flight_availability_details($search_data ['data']);
          if($search_avilability['status'] == SUCCESS_STATUS) {
            $response ['data']   = $search_avilability['data'];
            $response ['status'] = SUCCESS_STATUS;
          }
        } else {
          //die('else');
          // Flight search request
          $flight_search_request = $this->BargainFinderMaxRQ ($search_id, $search_data);
          if ($flight_search_request['status'] = SUCCESS_STATUS) {
            // error_reporting(E_ALL);
            try {
              unset($flight_search_request['status']);
              if (count ($flight_search_request) > 0) {
                //die('valid_search_result');
                $key = 0;
                
                $response  = $flight_search_request;
                //$response ['data'] ['flight_data_list'] ['journey_list'] = $flight_search_request;
                if ($response ['data']) {
                  $response ['status'] = SUCCESS_STATUS;
                } else {
                  $response ['status'] = FAILURE_STATUS;
                }
              } else {
                $response ['status'] = FAILURE_STATUS;
              }
            } // catch exception
            catch ( Exception $e ) {
              $response ['status'] = FAILURE_STATUS;
            }
              
            if (isset ( $flight_search_request ['data'] ['return'] ) && ! empty ( $flight_search_request ['data'] ['return'] )) {
              $this->CI->custom_db->generate_static_response ( $flight_search_request ['data'] ['return'], 'sabre search request return', 'galileo' );
          
              try {
                $return_search_response = $this->process_request ( $flight_search_request ['data'] ['return'], $flight_search_request ['data'] ['url'], $flight_search_request ['data'] ['soap_action'] );
                $return_search_response_array = Converter::createArray ( $return_search_response );
                if ($this->valid_search_result ( $return_search_response_array ) == TRUE) {
                  $key = 1;
                  // SAVE response in 'test' table
                  $this->CI->custom_db->generate_static_response ($return_search_response, 'sabre search response return', 'galileo' );
                  $clean_format_data = $this->format_search_data_response ( $return_search_response_array, $search_data ['data'], $key );
                  $response ['data'] ['flight_data_list'] ['journey_list'] [1] = $clean_format_data ['data'];
                }
              } catch ( Exception $e ) {
              }
            }
          }
        }
        // Cache
        if ($response ['status'] == SUCCESS_STATUS) {
          if ($cache_search) {
            $cache_exp = $this->CI->config->item ( 'cache_flight_search_ttl' );
            $this->CI->cache->file->save ( $search_hash, $response ['data'], $cache_exp );
          }
        }
      } else {
        $response ['data'] = $cache_contents;
      }
    } else {
      $response ['status'] = FAILURE_STATUS;
    }
    //debug($response);exit;
    return $response; 
  }
  
  /**
   * Advance search get price based on class for specific segment
   * 
   * @param char $class
   * @param array $temp_booking
   * @param string $search_id
   * 
   * @return array
   */
  
    /**
     * return booking form
     */
    function booking_form($isDomestic, $token='', $token_key='', $search_access_key='', $is_lcc='', $booking_type='', $promotional_plan_type='', $cur_ProvabAuthKey='', $booking_source=SABRE_FLIGHT_BOOKING_SOURCE)
    {
        $booking_form = '';

        $booking_form .= '<input type="hidden" name="is_domestic" class="" value="'.$isDomestic.'">';
        $booking_form .= '<input type="hidden" name="token[]" class="token data-access-key" value="'.$token.'">';
        $booking_form .= '<input type="hidden" name="token_key[]" class="token_key" value="'.$token_key.'">';
        $booking_form .= '<input type="hidden" name="search_access_key[]" class="search-access-key" value="'.$search_access_key.'">';
        $booking_form .= '<input type="hidden" name="is_lcc[]" class="is-lcc" value="'.$is_lcc.'">';
        $booking_form .= '<input type="hidden" name="promotional_plan_type[]" class="promotional-plan-type" value="'.$promotional_plan_type.'">';
        //$booking_form .= '<input type="hidden" name="provab-auth-key[]" class="provab-auth-key" value="'.$cur_ProvabAuthKey.'">'; since data is cached no need to carry this
        if (empty($booking_type) == false) {
            $booking_form .= '<input type="hidden" name="booking_type" class="booking-type" value="'.$booking_type.'">';
        }
        if (empty($booking_source) == false) {
            $booking_form .= '<input type="hidden" name="booking_source" class="booking-source" value="'.$booking_source.'">';
        }
        return $booking_form;
    }
  
  /*for creating session for sabre start here*/
  function SessionCreateRQ($search_id, $type = 'SEARCH', $Action = 'SessionCreateRQ'){ 
    $this->set_api_credentials();
    $psdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $psdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z");
    $SessionCreateRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope
                  xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices3.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service eb:type='SabreXML'>Session</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$psdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$psdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security
                          xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                      <SessionCreateRQ>
                          <POS>
                              <Source PseudoCityCode='".$this->ipcc."' />
                          </POS>
                      </SessionCreateRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";
    //echo $SessionCreateRQ;exit;
    $SessionCreateRS = $this->flight_processRequest($SessionCreateRQ);
    //debug($SessionCreateRS);exit('SessionCreateRS');
    $SessionCreateRQ_RS = array(
      'SessionCreateRQ' => $SessionCreateRQ,
      'SessionCreateRS' => $SessionCreateRS
    );  

    $mypdata = $this->parse_session_create_response($SessionCreateRQ_RS);
   // $SessionCreateRQ_RS['psbinarytoken'] = $mypdata;
  if($type == "SEARCH"){
    $path = "../b2b2b/xml_logs/Flight/sabre/search_logs/SessionCreateRQ_".$search_id.".xml";
    $fp = fopen($path,"wb");fwrite($fp,$SessionCreateRQ);fclose($fp);

    $path = "../b2b2b/xml_logs/Flight/sabre/search_logs/SessionCreateRS_".$search_id.".xml";
    $fp = fopen($path,"wb");fwrite($fp,$SessionCreateRS);fclose($fp);
  }else{
    $folder_path = "../b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id; 
    if (!file_exists($folder_path)){
       mkdir("../b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id, 0777);
    }       
    $path = "../b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/SessionCreateRQ.xml";
    $fp = fopen($path,"wb");fwrite($fp,$SessionCreateRQ);fclose($fp);
    
    $path = "../b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/SessionCreateRS.xml";
    $fp = fopen($path,"wb");fwrite($fp,$SessionCreateRS);fclose($fp);
  }
    return $SessionCreateRQ_RS;
}
  /*for creating session for sabre end here*/
  /*for parsing session response start*/
  function parse_session_create_response($SessionCreateRQ_RS){  
    //error_reporting(E_ALL);
        $SessionCreateRS    = $SessionCreateRQ_RS['SessionCreateRS'];
        $response         = $this->CI->xml_to_array->XmlToArray($SessionCreateRS);
        //debug($SessionCreateRS);die('response');
        $BinarySecurityToken  = array();
    if(isset($response['soap-env:Header']['eb:MessageHeader'])){
      $BinarySecurityToken['ConversationId']      = $response['soap-env:Header']['eb:MessageHeader']['eb:ConversationId'];
      $BinarySecurityToken['BinarySecurityToken']   = $response['soap-env:Header']['wsse:Security']['wsse:BinarySecurityToken']['@content'];
        }
        $_GLOBALS['BinarySecurityToken'] = $this->BinarySecurityToken = $BinarySecurityToken['BinarySecurityToken'];
       // echo $this->BinarySecurityToken;die('parse_session_create_response');
        //return $BinarySecurityToken;
  }
  /*for parsing session response ends*/
  /*for closing session start here*/
  function SessionCloseRQ($search_id, $type = 'SEARCH',$Action = 'SessionCloseRQ'){
    //$credencials = set_credencials_flight();
    $psdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $psdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z");
    $SessionCloseRQ = "<?xml version='1.0' encoding='utf-8'?>
                        <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                            <soap-env:Header>
                                <eb:MessageHeader xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                                    <eb:From>
                                        <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                                    </eb:From>
                                    <eb:To>
                                        <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices3.sabre.com</eb:PartyId>
                                    </eb:To>
                                    <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                                    <eb:Service>SessionCloseRQ</eb:Service>
                                    <eb:Action>".$Action."</eb:Action>
                                    <eb:CPAID>".$this->ipcc."</eb:CPAID>
                                    <eb:MessageData>
                                        <eb:MessageId>".$this->message_id."</eb:MessageId>
                                        <eb:Timestamp>".$psdata['timestamp']."</eb:Timestamp>
                                        <eb:TimeToLive>".$psdata['timetolive']."</eb:TimeToLive>
                                    </eb:MessageData>
                                </eb:MessageHeader>
                                <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                                  <wsse:BinarySecurityToken valueType='String' EncodingType='wsse:Base64Binary'>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                                </wsse:Security>
                            </soap-env:Header>
                            <soap-env:Body>
                                <SessionCloseRQ>
                                    <POS>
                                        <Source PseudoCityCode='".$this->ipcc."' />
                                    </POS>
                                </SessionCloseRQ>
                            </soap-env:Body>
                        </soap-env:Envelope>";
    $SessionCloseRS = $this->flight_processRequest($SessionCloseRQ);
    $SessionCloseRQ_RS = array(
                  'SessionCloseRQ' => $SessionCloseRQ,
                  'SessionCloseRS' => $SessionCloseRS
                );
    if($type == "SEARCH"){}else{
            
    $path = "../b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/SessionCloseRQ.xml";
    $fp = fopen($path,"wb");fwrite($fp,$SessionCloseRQ);fclose($fp);
    
    $path = "../b2b2b/xml_logs/Flight/sabre/booking_logs/".$search_id."/SessionCloseRS.xml";
    $fp = fopen($path,"wb");fwrite($fp,$SessionCloseRS);fclose($fp);
  }
  return $SessionCloseRQ_RS;
}
  /*for closing session ends here*/

  /*for getting flight list start here*/
function BargainFinderMaxRQ($search_id, $search_data){    
    //echo 'search_data:<pre/>';print_r($search_data);exit;
    $this->SessionCreateRQ($search_id, 'SEARCH', 'SessionCreateRQ');
    $strSabrePath = "xml_logs/Flight/sabre";
    if($search_data['data']['trip_type'] == 'circle'){
      $MultiTicket = '<MultiTicket DisplayPolicy="SCHS"/>';
    }else{
      $MultiTicket = '';
    }
  
    //if($search_data->flexible == '1'){
  if(false){
    $Action         = 'BargainFinderMax_ADRQ';
    $RequestType      = 'AD3';
    $field_xml_request    = "xml_request_flexible";
    $field_xml_response   = "xml_response_flexible";    
  }else{
    $Action         = 'BargainFinderMaxRQ';
    $RequestType      = '50ITINS';
    $field_xml_request    = "xml_request";
    $field_xml_response   = "xml_response";   
  }
  //debug($search_data['data']['carrier']);die;
  if(($search_data['data']['carrier'] !='') && ($search_data['data']['carrier'] !='all')){
    $airline = "<IncludeVendorPref Code='".$search_data['data']['carrier']."'/>";
  }else{
    $airline = "";
  }
  $economyCode = 'Y'; $SegmentType = '';
  $economy = $search_data['data']['v_class'];
  if ($economy == 'All') {
    $economy = 'Economy';
    $economyCode = 'Y'; $KeepSameCabin = '';
  }else{
    $KeepSameCabin = '<JumpCabinLogic Disabled="true"/><KeepSameCabin Enabled="true"/>';
  }
  if ($economy == 'Economy') {
    $economyCode = 'Y';
  }
  if ($economy == 'PremiumEconomy') {
    $economyCode = 'S';
  }
  if ($economy == 'Business') {
    $economyCode = 'C';
  }
  if ($economy == 'PremiumBusiness') {
    $economyCode = 'J';
  }
  if ($economy == 'First') {
    $economyCode = 'F';
  }
  if ($economy == 'PremiumFirst') {
    $economyCode = 'P';
  }
  /*pending from here start*/
  //if($search_data->departure_time !=''){
  if(false){
    $DepartureWindow =  '<DepartureWindow>'.$search_data->departure_time.'</DepartureWindow>';
  }else{
    $DepartureWindow =  '';
  }
  
  //if($search_data->arrival_time !=''){
   if(false){
    $ArrivalWindow =  '<ArrivalWindow>'.$search_data->arrival_time.'</ArrivalWindow>';
  }else{
    $ArrivalWindow =  '';
  }
  
  //if($search_data->max_stop !=''){
  if(false){
    $MaxStopsQuantity =  'MaxStopsQuantity="'.$search_data->max_stop.'"';
  }else{
    $MaxStopsQuantity =  '';
  }
  //if($search_data->connection_location !=''){
  if(false){
    $ConnectionLocations  = explode(",",$search_data->connection_location);
    $ConnectionLocation =  '<ConnectionLocations>
                <ConnectionLocation LocationCode="'.trim($ConnectionLocations[1]).'" />
              </ConnectionLocations>';
  }else{
    $ConnectionLocation =  '';
  }
  
  $SegmentType= '';// $SegmentType = '<SegmentType Code="O" />';  
  //$credencials = set_credencials_flight();
    $oneway = '';$roundtrip = '';$multicity = ''; $i = "2";
    $DepPreferredTime     = $search_data['data']['depature'];
  //$OriginLocation     = explode(",",$search_data->departure_city);
  //$DestinationLocation  = explode(",",$search_data->arrival_city);
    //debug($search_data['data']['trip_type']);exit;
   if ($search_data['data']['trip_type'] != 'multicity') {
      $OriginLocation[1]     = (($search_data['data']['from_loc'] == "YMX") || $search_data['data']['from_loc'] == "YMQ")? "YUL" : $search_data['data']['from_loc'];
    $DestinationLocation[1]  = (($search_data['data']['to_loc'] == "YMX") || $search_data['data']['to_loc'] == "YMQ")? "YUL" : $search_data['data']['to_loc'];
    $oneway =  "<OriginDestinationInformation RPH='1'>
        <DepartureDateTime>".$DepPreferredTime."</DepartureDateTime>
        ".$DepartureWindow."
        ".$ArrivalWindow."
        <OriginLocation LocationCode='".trim($OriginLocation[1])."'/>
        <DestinationLocation LocationCode='".trim($DestinationLocation[1])."'/>   
        ".$ConnectionLocation."
        <TPA_Extensions>
          ".$SegmentType."
          ".$airline."
        </TPA_Extensions>
       </OriginDestinationInformation>"; 
    } 
  if($search_data['data']['trip_type'] == 'circle'){
    //debug($search_data);die;
    $DepPreferredTime     = $search_data['data']['return'];
    $OriginLocation[1]     = (($search_data['data']['from_loc'] == "YMX") || ($search_data['data']['from_loc'] == "YMQ")) ? "YUL" : $search_data['data']['from_loc'];
    $DestinationLocation[1]  = (($search_data['data']['to_loc'] == "YMX") || ($search_data['data']['to_loc'] == "YMQ")) ? "YUL" : $search_data['data']['to_loc'];
   // $DepPreferredTime     = $search_data['data']['depature'];
    $roundtrip =  "<OriginDestinationInformation RPH='2'>
                        <DepartureDateTime>".$DepPreferredTime."</DepartureDateTime>
                        ".$DepartureWindow."
            ".$ArrivalWindow."
            <OriginLocation LocationCode='".trim($DestinationLocation[1])."'/>
            <DestinationLocation LocationCode='".trim($OriginLocation[1])."'/>                      
            ".$ConnectionLocation."
            <TPA_Extensions>
              ".$SegmentType."
              ".$airline."
            </TPA_Extensions>
                     </OriginDestinationInformation>";
                          
    }
   // debug($search_data);exit();
    if($search_data['data']['trip_type'] == 'multicity'){
      //debug($search_data);die;
    /*$multi_from_city = (($search_data['data']['from_loc'] !="YMX") || ($search_data['data']['from_loc'] !="YMQ")) ? "YUL" : $search_data['data']['from_loc'];
    $multi_to_city = (($search_data['data']['to_loc'] !="YMX") || ($search_data['data']['to_loc'] !="YMQ")) ? "YUL" : $search_data['data']['to_loc'];
    $multi_fcheckin = $search_data['data']['depature'];*/
    $multi_from_city = $search_data['data']['from_loc'];
    $multi_to_city = $search_data['data']['to_loc'];
    $multi_fcheckin = $search_data['data']['depature'];
    //debug($multi_from_city);
    //debug(date('Y-m-d',strtotime($multi_fcheckin[0])));exit;
    $i = 1;
      foreach ($multi_from_city as $key => $value) {
        //debug($value);exit;
        $OriginLocation[1]     =  (($value =="YMX") || ($value =="YMQ")) ? "YUL" : $value;
        $DestinationLocation[1]  = (($multi_to_city[$key] =="YMX") || ($multi_to_city[$key] =="YMQ")) ? "YUL" : $multi_to_city[$key];
        $DepPreferredTime = date('Y-m-d',strtotime($multi_fcheckin[$key]));
        $multicity .=  "<OriginDestinationInformation RPH='".$i."'>
                           <DepartureDateTime>".$DepPreferredTime."T00:00:00</DepartureDateTime>
                           ".$DepartureWindow."
              ".$ArrivalWindow."
              <OriginLocation LocationCode='".trim($OriginLocation[1])."'/>
              <DestinationLocation LocationCode='".trim($DestinationLocation[1])."'/>    
              ".$ConnectionLocation."
              <TPA_Extensions>
                ".$SegmentType."
                ".$airline."
              </TPA_Extensions>
                       </OriginDestinationInformation>";        
        $i++;
      }
     
    }
   // echo $multicity;die;
    /*pending from here end*/
    //Passenger Details Start -> done till here
    $adult_patch_public = $adult_patch = $child_patch = $infant_patch = $cchild_patch = $iinfant_patch = '';
    //debug($search_data);exit();
    if($search_data['data']['adult_config'] > 0){
        /*$adult_patch = "<PassengerTypeQuantity Code='ADT' Quantity='".$search_data['data']['adult_config']."'/>";*/
        /*only for private fare*/
        $adult_patch = "<PassengerTypeQuantity Code='JCB' Quantity='".$search_data['data']['adult_config']."'/>";
        $adult_patch_public = "<PassengerTypeQuantity Code='ADT' Quantity='".$search_data['data']['adult_config']."'/>";
    }
    if($search_data['data']['child_config'] > 0){
        $child_patch = "<PassengerTypeQuantity Code='JNN' Quantity='".$search_data['data']['child_config']."'/>";
        $cchild_patch = "<PassengerTypeQuantity Code='CNN' Quantity='".$search_data['data']['child_config']."'/>";
    }
    if($search_data['data']['infant_config'] > 0){
        $infant_patch = "<PassengerTypeQuantity Code='JNF' Quantity='".$search_data['data']['infant_config']."'/>";
        $iinfant_patch = "<PassengerTypeQuantity Code='INF' Quantity='".$search_data['data']['infant_config']."'/>";
    }
  $nonstop='';
  $psdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
    $psdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z");
    
    $BargainFinderMaxRQ_publish = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                            <SOAP-ENV:Header>
                              <m:MessageHeader xmlns:m="http://www.ebxml.org/namespaces/messageHeader">
                                <m:From>
                                  <m:PartyId type="urn:x12.org:IO5:01">'.$this->sabre_myemail.'</m:PartyId>
                                </m:From>
                                <m:To>
                                  <m:PartyId type="urn:x12.org:IO5:01">webservices.sabre.com</m:PartyId>
                                </m:To>
                                <m:CPAId>'.$this->ipcc.'</m:CPAId>
                                <m:ConversationId>'.$this->conversation_id.'</m:ConversationId>
                                <m:Service m:type="OTA">'.$Action.'</m:Service>
                                <m:Action>'.$Action.'</m:Action>
                                <m:MessageData>
                                  <m:MessageId>'.$this->message_id.'</m:MessageId>
                                  <m:Timestamp>'.$psdata['timestamp'].'</m:Timestamp>
                                  <m:TimeToLive>'.$psdata['timetolive'].'</m:TimeToLive>
                                </m:MessageData>
                                <m:DuplicateElimination/>
                                <m:Description>Bargain Finder Max Service</m:Description>
                              </m:MessageHeader>
                              <wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/12/secext">
                                <wsse:BinarySecurityToken valueType="String" EncodingType="wsse:Base64Binary">'.$this->BinarySecurityToken.'</wsse:BinarySecurityToken>
                              </wsse:Security>
                            </SOAP-ENV:Header>
                            <SOAP-ENV:Body>
                              <OTA_AirLowFareSearchRQ ResponseType="OTA" ResponseVersion="3.0.0" Version="3.0.0" AvailableFlightsOnly="true" xmlns="http://www.opentravel.org/OTA/2003/05">
                                  <POS>
                                    <Source PseudoCityCode="'.$this->ipcc.'">
                                      <RequestorID ID="1" Type="1">
                                        <CompanyName Code="TN">TN</CompanyName>
                                      </RequestorID>
                                    </Source>
                                  </POS>
                                  '.$oneway.'
                                  '.$roundtrip.'
                                  '.$multicity.'
                                  <TravelPreferences '.$MaxStopsQuantity.'>
                  '.$nonstop.'
                  <CabinPref PreferLevel="Preferred" Cabin="'.$economyCode.'"/>

                  <TPA_Extensions>
                                      <LongConnectTime Min="5" Max="1439" Enable="true"/>
                                      <LongConnectPoints Min="1" Max="3"/>
                                      '.$KeepSameCabin.'
                                    </TPA_Extensions>
                                  </TravelPreferences>
                                  <TravelerInfoSummary>
                                    <SeatsRequested>'.($search_data['data']['adult_config']+$search_data['data']['child_config']).'</SeatsRequested>
                                    <AirTravelerAvail>
                                      '.$adult_patch_public.'
                                      '.$cchild_patch.'
                                       '.$iinfant_patch.'                                    
                                    </AirTravelerAvail>
                                  </TravelerInfoSummary>
                                  <TPA_Extensions>
                                    <IntelliSellTransaction>
                                      <RequestType Name="'.$RequestType.'"/>
                                    </IntelliSellTransaction>
                                    '.$MultiTicket.'
                                  </TPA_Extensions>
                                </OTA_AirLowFareSearchRQ>
                            </SOAP-ENV:Body>
                          </SOAP-ENV:Envelope>';
    //echo '<pre/>';print_r($BargainFinderMaxRQ_publish);exit;    
    $XmlReqFileName = $Action.'_Search_RQ_'.$search_id; 
    $XmlResFileName = $Action.'_Search_RS_'.$search_id;
    if(true){
        $fp = fopen("../b2b2b/xml_logs/Flight/sabre/search_logs/".$XmlReqFileName.'.xml', 'a+');
        fwrite($fp, $BargainFinderMaxRQ_publish);
        fclose($fp); 

        $BargainFinderMaxRS = $this->flight_processRequest($BargainFinderMaxRQ_publish);
        
        //debug($BargainFinderMaxRS);exit();
        $fp = fopen("../b2b2b/xml_logs/Flight/sabre/search_logs/".$XmlResFileName.'.xml', 'a+');
        fwrite($fp, $BargainFinderMaxRS);
        fclose($fp); 
    }else{
        $BargainFinderMaxRS = file_get_contents("../b2b2b/xml_logs/Flight/sabre/search_logs/".$XmlResFileName.'.xml');      
    }


    $BargainFinderMaxRQ_RS = array(
          'BargainFinderMaxRQ' => $BargainFinderMaxRQ_publish,
          'BargainFinderMaxRS' => $BargainFinderMaxRS,
          //'BargainFinderMaxRQ_private' => $BargainFinderMaxRQ_private,
          //'BargainFinderMaxRS_private' => $BargainFinderMaxRS_publish,
          'BinarySecurityToken' => $this->BinarySecurityToken
        );
    //debug($BargainFinderMaxRQ_RS);die;
    $parsed_formattted_data['status'] = SUCCESS_STATUS;
    $BargainFinderMaxRS_publish ="";
    // $parsed_formattted_data = $this->parse_flight_search_response($BargainFinderMaxRS, $BargainFinderMaxRS_publish,'normal');
    $parsed_formattted_data = $this->parse_flight_search_response($BargainFinderMaxRS,'normal',$search_data);
    //debug($parsed_formattted_data);die;
    $sci = &get_instance();
    $sci->session->set_userdata(array('security_token' => $this->BinarySecurityToken));

    $parsed_formattted_data['BinarySecurityToken'] = $this->BinarySecurityToken;
    $parsed_formattted_data['booking_url']         = $this->booking_url($search_id);
    return $parsed_formattted_data;
}
/*for getting flight list end here*/
/*for parsing search result start here*/
function parse_flight_search_response($SearchResponse,$type,$search_data){
    error_reporting(E_ALL);
    $CI = & get_instance();
    $CI->load->model('Flight_Model');
    $arrSearchData = $this->xml2array($SearchResponse);
    $arrAirlines = $CI->Flight_Model->get_all_airline();
    $arrAirlineNames = array();
    $arrAirlineIDs = array();
    for($a=0; $a<count($arrAirlines); $a++){
        $arrAirlineNames[$arrAirlines[$a]->code] = $arrAirlines[$a]->name;
        $arrAirlineIDs[$arrAirlines[$a]->code]   = $arrAirlines[$a]->origin;
    }

    $arrAirports = $CI->Flight_Model->get_all_airports(); 
    $arrAirportNames = array();
    for($a=0; $a<count($arrAirports); $a++){
        $arrAirportNames[$arrAirports[$a]->airport_code] = $arrAirports[$a]->airport_city;
    }

    $arrEconomy['Y'] = 'Economy';
    $arrEconomy['S'] = 'PremiumEconomy';
    $arrEconomy['C'] = 'Business';
    $arrEconomy['J'] = 'PremiumBusiness';
    $arrEconomy['F'] = 'First';
    $arrEconomy['P'] = 'PremiumFirst';
    /*$get_currency_symbol = ($this->CI->session->userdata('currency') != '') ? $this->CI->session->userdata('currency') : get_application_default_currency();
    // debug($get_currency_symbol);die;
    $current_currency_symbol = $currency_obj->get_currency_symbol($get_currency_symbol);
    //$converted_currency_rate = 1;
    $converted_currency_rate = $currency_obj->getConversionRate(false);*/

    $arrJrnySmry = array();
    //$strTotalPass = $search_data['data']['adult_config']+$search_data['data']['child_config']+$search_data['data']['infant_config'];
    $arrJrnySmry['Origin']                    = $search_data['data']['from'];
    $arrJrnySmry['Destination']               = $search_data['data']['to'];
    $arrJrnySmry['IsDomestic']                = $search_data['data']['is_domestic'];
    $arrJrnySmry['RoundTrip']                 = ($search_data['data']['trip_type'] == 'circle')?true:false;
    $arrJrnySmry['MultiCity']                 = ($search_data['data']['trip_type'] == 'multicity')?true:false; 
    $arrJrnySmry['PassengerConfig']['Adult']  = $search_data['data']['adult_config'];
    $arrJrnySmry['PassengerConfig']['Child']  = $search_data['data']['child_config'];
    $arrJrnySmry['PassengerConfig']['Infant'] = $search_data['data']['infant_config'];
    $arrJrnySmry['PassengerConfig']['TotalPassenger'] = array_sum($arrJrnySmry['PassengerConfig']);
    $arrJrnySmry['IsDomesticRoundway']        = $search_data['data']['is_domestic'];

    $flight_result['data']['JourneySummary']  = $arrJrnySmry;

    $strTripType = $search_data['data']['trip_type'];
    $arrFltResult = array();
    $currency_obj = new Currency(array('module_type' => 'flight','from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
    $currency_symbol = $currency_obj->get_currency_symbol($currency_obj->to_currency); 

    $converted_currency_rate = $currency_obj->getConversionRate(TRUE,$currency_obj->to_currency,get_application_default_currency ());
    if(!isset($arrSearchData['SOAP-ENV:Envelope']['SOAP-ENV:Body']['SOAP-ENV:Fault']['faultcode'])){
        $priced_Itinarys = $arrSearchData['SOAP-ENV:Envelope']['SOAP-ENV:Body']['OTA_AirLowFareSearchRS']['PricedItineraries']['PricedItinerary'];
        if(!isset($priced_Itinarys[0])){
            $pricedItinarys[0] = $priced_Itinarys;
        }else{
            $pricedItinarys = $priced_Itinarys;
        }

        // debug($pricedItinarys);die;
    $baggage_info= @$arrSearchData['SOAP-ENV:Envelope']['SOAP-ENV:Body']['OTA_AirLowFareSearchRS']['PricedItineraries']['PricedItinerary'][0]['AirItineraryPricingInfo']['PTC_FareBreakdowns']['PTC_FareBreakdown']['PassengerFare']['TPA_Extensions']['BaggageInformationList']['BaggageInformation']['Allowance']['attr'];   

        for($p=0; $p<count($pricedItinarys); $p++){
            $strToken = base64_encode(serialize($pricedItinarys[$p]));
            $arrFltResult[$p] = array();
            $arrOrigin_Dest = $pricedItinarys[$p]['AirItinerary']['OriginDestinationOptions']['OriginDestinationOption'];
            // debug($arrOrigin_Dest);exit();

            if(!isset($arrOrigin_Dest[0])){
                $arrOriginDest[0] = $arrOrigin_Dest;
            }else{
                $arrOriginDest = $arrOrigin_Dest;
            }
            //debug($arrOriginDest);exit();
            /*if(!isset($arrOriginDest[1])){
              debug($arrOriginDest);exit();
            }*/
            $arrSegmentDtls     = array();
            $arrSegmentSummary  = array();
            $trpInd = 0;
            // debug($arrOriginDest);exit();
            for($o=0; $o<count($arrOriginDest); $o++){
                $trpInd++;
                $origin_Dest = $arrOriginDest[$o]['FlightSegment'];
                // debug($arrOriginDest);exit();
                if(!isset($origin_Dest[0])){
                    $originDest[0] = $origin_Dest;
                }else{
                    $originDest = $origin_Dest;
                }
                // debug($originDest);exit();
                $segInd = 0;
                $arrSegmentSummary[$o]['journey_number'] = $o;
                $arrAirlineSegmentData      = array();
                $arrOriginSegmentData       = array();
                $arrDestinationSegmentData  = array();
                $strOpAirlineCode           = "";
                $strOpAirlineName           = "";
                $strFltNumber               = "";
                for($or=0; $or<count($originDest); $or++){
                    $strJryNumber = "onward";
                    if($strTripType == "circle" && $o > 0){
                        $strJryNumber = "return";
                    }

                    // debug($originDest);die;
                    $segInd++;
                    $arrSegmentDtls[$o][$or]['Baggage']           = $baggage_info['Weight'].$baggage_info['Unit'];
                    $arrSegmentDtls[$o][$or]['Baggage_weight_unit']           = $baggage_info['Unit'];
                    $arrSegmentDtls[$o][$or]['CabinBaggage']      = "";
                    $arrSegmentDtls[$o][$or]['AvailableSeats']    = "";
                    $arrSegmentDtls[$o][$or]['TripIndicator']     = $trpInd; 
                    $arrSegmentDtls[$o][$or]['SegmentIndicator']  = $segInd; 
                    $arrSegmentDtls[$o][$or]['journey_number']    = $strJryNumber;

                    $strAirlineCode = $originDest[$or]['OperatingAirline']['attr']['Code'];
                    @$strAirlineName = isset($originDest[$or]['OperatingAirline']['attr']['CompanyShortName'])?$originDest[$or]['OperatingAirline']['attr']['CompanyShortName']:$arrAirlineNames[$strAirlineCode];
                    $arrSegmentAirlineData['AirlineCode']       = $strAirlineCode;
                    // $arrSegmentAirlineData['AirlineName']       = $arrAirlineNames[$strAirlineCode];
                    $arrSegmentAirlineData['AirlineName']       = $strAirlineName;
                    $arrSegmentAirlineData['FlightNumber']      = $originDest[$or]['OperatingAirline']['attr']['FlightNumber'];
                    $arrSegmentAirlineData['FareClass']         = "";
                    $arrSegmentAirlineData['OperatingCarrier']  = $strAirlineCode;
                    $arrSegmentAirlineData['FareClassLabel']    = "";
                    $arrSegmentDtls[$o][$or]['AirlineDetails']  = $arrSegmentAirlineData;

                    $arroriginDetails['AirportCode']    = $originDest[$or]['DepartureAirport']['attr']['LocationCode'];
                    $arroriginDetails['AirportName']    = $arrAirportNames[$originDest[$or]['DepartureAirport']['attr']['LocationCode']];
                      $arroriginDetails['CityName']    = $arrAirportNames[$originDest[$or]['DepartureAirport']['attr']['LocationCode']];
                    $arroriginDetails['FlightNumber']   = $originDest[$or]['OperatingAirline']['attr']['FlightNumber'];
                    $arroriginDetails['Terminal']       = isset($originDest[$or]['DepartureAirport']['attr']['TerminalID'])?$originDest[$or]['DepartureAirport']['attr']['TerminalID']:"";  
                    $arroriginDetails['CityCode']       = $originDest[$or]['DepartureAirport']['attr']['LocationCode'];
                    $arroriginDetails['CountryCode']    = ""; 
                    $arroriginDetails['CountryName']    = "";
                    $strDepTime_date = date('d M Y',strtotime($originDest[$or]['attr']['DepartureDateTime']));
                    $strDepTime_time = date('H:i',strtotime($originDest[$or]['attr']['DepartureDateTime']));
                    $arroriginDetails['DepartureTime']  = $strDepTime_date;
                    $arroriginDetails['_DepartureTime'] = $strDepTime_time;
                    $arroriginDetails['_DateTime'] = $strDepTime_time;
                    $arroriginDetails['DateTime'] = $originDest[$or]['attr']['DepartureDateTime'];
                    $arroriginDetails['fdtv'] = "";
                    if($or == 0){
                        $arrAirlineSegmentData = $arrSegmentAirlineData;
                        $arrOriginSegmentData  = $arroriginDetails;
                        $strOpAirlineCode      = $strAirlineCode;
                        $strOpAirlineName      = $strAirlineName;
                        $strFltNumber          = $originDest[$or]['OperatingAirline']['attr']['FlightNumber'];
                    }                    
                    $arrSegmentDtls[$o][$or]['OriginDetails']   = $arroriginDetails;

                    $arrDestDetails['AirportCode']    = $originDest[$or]['ArrivalAirport']['attr']['LocationCode'];
                    $arrDestDetails['AirportName']    = $arrAirportNames[$originDest[$or]['ArrivalAirport']['attr']['LocationCode']];
                    $arrDestDetails['CityName']    = $arrAirportNames[$originDest[$or]['ArrivalAirport']['attr']['LocationCode']];
                    $arrDestDetails['FlightNumber']   = $originDest[$or]['OperatingAirline']['attr']['FlightNumber'];
                    $arrDestDetails['Terminal']       = isset($originDest[$or]['ArrivalAirport']['attr']['TerminalID'])?$originDest[$or]['ArrivalAirport']['attr']['TerminalID']:""; 
                    $arrDestDetails['CityCode']       = $originDest[$or]['ArrivalAirport']['attr']['LocationCode'];
                    $arrDestDetails['CountryCode']    = ""; 
                    $arrDestDetails['CountryName']    = "";
                    $strArrTime_date = date('d M Y',strtotime($originDest[$or]['attr']['ArrivalDateTime']));
                    $strArrTime_time = date('H:i',strtotime($originDest[$or]['attr']['ArrivalDateTime']));
                    $arrDestDetails['ArrivalTime']    = $strArrTime_date;
                    $arrDestDetails['_ArrivalTime']   = $strArrTime_time;
                    $arrDestDetails['_DateTime']   = $strArrTime_time;
                    $arrDestDetails['DateTime']   = $originDest[$or]['attr']['ArrivalDateTime'];
                    $arrDestDetails['fdtv']           = "";
                    if($or == count($originDest)-1){
                      $arrDestinationSegmentData = $arrDestDetails;
                    }
                    $arrSegmentDtls[$o][$or]['DestinationDetails'] = $arrDestDetails;

                    $strSegmentElapseTime = $originDest[$or]['attr']['ElapsedTime'];
                    $strSegmentHrsMins    = $this->getSegmentElapseHrsMins($strSegmentElapseTime); 
                    $strSegmentSecs       = $strSegmentElapseTime*60;
                    $arrSegmentDtls[$o][$or]['SegmentDuration']   = $strSegmentHrsMins;
                    $arrSegmentDtls[$o][$or]['Status']            = "";
                    $arrSegmentDtls[$o][$or]['Craft']             = $originDest[$or]['Equipment']['attr']['AirEquipType'];
                    $arrSegmentDtls[$o][$or]['IsETicketEligible'] = "";
                    $arrSegmentDtls[$o][$or]['duration_seconds']  = $strSegmentSecs;
                    $arrSegmentDtls[$o][$or]['duration']          = $strSegmentHrsMins;
                    $arrSegmentDtls[$o][$or]['Group']             = $originDest[$or]['MarriageGrp']['value'];
                    $arrSegmentDtls[$o][$or]['operator_code']     = $strAirlineCode;
                    $arrSegmentDtls[$o][$or]['operator_name']     = $strAirlineName;
                    $arrSegmentDtls[$o][$or]['flight_number']     = $originDest[$or]['OperatingAirline']['attr']['FlightNumber'];
                    $arrSegmentDtls[$o][$or]['cabin_class']       = "";
                    $arrSegmentDtls[$o][$or]['is_leg']            = $originDest[$or]['attr']['ResBookDesigCode'];
                }
                $strTotalElapsedTime = $arrOriginDest[$o]['attr']['ElapsedTime'];
                $arrSegmentSummary[$o]['AirlineDetails']      = $arrAirlineSegmentData;
                $arrSegmentSummary[$o]['OriginDetails']       = $arrOriginSegmentData;
                $arrSegmentSummary[$o]['DestinationDetails']  = $arrDestinationSegmentData;
                $arrSegmentSummary[$o]['TotalStops']          = $or-1; 
                $arrSegmentSummary[$o]['TotalDuaration']      = ($strTotalElapsedTime*60);
                $arrSegmentSummary[$o]['TotalDuarationLabel'] = $this->getSegmentElapseHrsMins($strTotalElapsedTime);
                $arrSegmentSummary[$o]['operator_code']       = $strOpAirlineCode;
                $arrSegmentSummary[$o]['display_operator_code'] = '';
                $arrSegmentSummary[$o]['operator_name']       = $strOpAirlineName;
                $arrSegmentSummary[$o]['flight_number']       = $strFltNumber;
                $arrSegmentSummary[$o]['cabin_class']         = 'economy';
                $arrSegmentSummary[$o]['fare_class']          = '';
                $arrSegmentSummary[$o]['no_of_stops']         = $or;
                $arrSegmentSummary[$o]['is_leg']              = ($or>0)?1:0;
                $arrSegmentSummary[$o]['cabin_bag']           = '';
                $arrSegmentSummary[$o]['duration_seconds']    = ($strTotalElapsedTime*60);
                $arrSegmentSummary[$o]['duration']            = $this->getSegmentElapseHrsMins($strTotalElapsedTime);
            }

            $arrItineryPricing = $pricedItinarys[$p]['AirItineraryPricingInfo']['ItinTotalFare'];
            $arrPassBreakDown  = $pricedItinarys[$p]['AirItineraryPricingInfo']['PTC_FareBreakdowns']['PTC_FareBreakdown'];
            //debug($arrPassBreakDown);exit();

            $arrB2CPriceDtls = array();
            $strCurrency = $arrItineryPricing['EquivFare']['attr']['CurrencyCode'];
            $strToINR    = $converted_currency_rate;

            $b2c_price = $arrItineryPricing['TotalFare']['attr']['Amount']*$strToINR;
            $CI =& get_instance();
            $CI->load->library('master_currency');
            $markup_with_price = $CI->master_currency->get_currency($b2c_price);
            $markup = round($markup_with_price['default_value'] - $b2c_price);


            $arrB2CPriceDtls['BaseFare']                     = $arrItineryPricing['EquivFare']['attr']['Amount']*$strToINR;
            $arrB2CPriceDtls['TotalTax']                     = $arrItineryPricing['Taxes']['Tax']['attr']['Amount']*$strToINR + @$markup;
            $arrB2CPriceDtls['TotalFare']                    = $arrItineryPricing['TotalFare']['attr']['Amount']*$strToINR + @$markup;

            $arrB2CPriceDtls['Markup']                    = $markup;
            $arrB2CPriceDtls['PublishedFare']             = $arrItineryPricing['TotalFare']['attr']['Amount']*$strToINR;
             $arrB2CPriceDtls['PublishedTax']             = $arrItineryPricing['Taxes']['Tax']['attr']['Amount']*$strToINR;



            $arrB2CPriceDtls['CurrencySymbol']               = $strCurrency;
            $arrB2CPriceDtls['CurrencySymbol']               = $currency_symbol;
            $arrB2CPriceDtls['Admin_Markup']['type']         = 0;
            $arrB2CPriceDtls['Admin_Markup']['currency_val'] = "";
            $arrB2CPriceDtls['Admin_Markup']['markup_type']  = "";
            $arrB2CPriceDtls['Admin_Markup']['markup_val']   = "";
            $arrB2CPriceDtls['Admin_Markup']['total_pax']    = "";
            $arrFareDetails['b2c_PriceDetails']      = $arrB2CPriceDtls;

            $arrApiPriceDtls = array();
            $strXMLCurrency  = $arrItineryPricing['EquivFare']['attr']['CurrencyCode'];
            $strApiCurrency  = "INR";
            $arrApiPriceDtls['Currency']             = $strApiCurrency;            
            $arrApiPriceDtls['BaseFare']             = $arrItineryPricing['EquivFare']['attr']['Amount']*$strToINR;
            $arrApiPriceDtls['Tax']                  = $arrItineryPricing['Taxes']['Tax']['attr']['Amount']*$strToINR;
            $arrApiPriceDtls['YQTax']                = 0;
            $arrApiPriceDtls['AdditionalTxnFeeOfrd'] = 0;
            $arrApiPriceDtls['AdditionalTxnFeePub']  = 0;
            $arrApiPriceDtls['OtherCharges']         = 0;
            $arrApiPriceDtls['Discount']             = 0;
            $arrApiPriceDtls['PublishedFare']        = $arrItineryPricing['TotalFare']['attr']['Amount']*$strToINR;
            $arrApiPriceDtls['AgentCommission']      = 0;
            $arrApiPriceDtls['PLBEarned']            = 0;
            $arrApiPriceDtls['IncentiveEarned']      = 0;
            $arrApiPriceDtls['OfferedFare']          = $arrItineryPricing['TotalFare']['attr']['Amount']*$strToINR;
            $arrApiPriceDtls['TdsOnCommission']      = 0;
            $arrApiPriceDtls['TdsOnPLB']             = 0;
            $arrApiPriceDtls['TdsOnIncentive']       = 0;
            $arrApiPriceDtls['ServiceFee']           = 0;
            $arrApiPriceDtls['TotalBaggageCharges']  = 0;
            $arrApiPriceDtls['TotalMealCharges']     = 0;
            $arrApiPriceDtls['TotalSeatCharges']     = 0;
            $arrFareDetails['api_PriceDetails'] = $arrApiPriceDtls;
            $arrTaxes = array();
            
            if(!isset($arrPassBreakDown[0])){
                $arrPassBreak_Down[0] = $arrPassBreakDown;
            }else{
                $arrPassBreak_Down = $arrPassBreakDown;
            }

            $arrPTCBreakDown = array();
            $strRefundable   = false; 
            for($ps=0; $ps<count($arrPassBreak_Down);$ps++){
                if($ps == 0){
                    $strRefundable = $arrPassBreak_Down[$ps]['Endorsements']['attr']['NonRefundableIndicator'];
                }
                $arrPTCBreakDown[$ps]['PassengerType']  = $arrPassBreak_Down[$ps]['PassengerTypeQuantity']['attr']['Code'];
                $arrPTCBreakDown[$ps]['Count']          = $arrPassBreak_Down[$ps]['PassengerTypeQuantity']['attr']['Quantity'];
                $arrPTCBreakDown[$ps]['BaseFare']       = $arrPassBreak_Down[$ps]['PassengerFare']['EquivFare']['attr']['Amount']*$strToINR;
                $arrPTCBreakDown[$ps]['TaxFare']        = $arrPassBreak_Down[$ps]['PassengerFare']['Taxes']['TotalTax']['attr']['Amount']*$strToINR;
                $arrPTCBreakDown[$ps]['TotalFare']      = $arrPassBreak_Down[$ps]['PassengerFare']['TotalFare']['attr']['Amount']*$strToINR;
            }
            //debug($arrPassBreak_Down);exit();

            $arrFltResult[$p]['ProvabAuthKey']      = "";
            $arrFltResult[$p]['ResultIndex']        = "";
            $arrFltResult[$p]['Source']             = "";
            $arrFltResult[$p]['IsLCC']              = "";
            $arrFltResult[$p]['AirlineRemark']      = "";
            $arrFltResult[$p]['FareDetails']        = $arrFareDetails;
            $arrFltResult[$p]['SegmentDetails']     = $arrSegmentDtls;
            $arrFltResult[$p]['SegmentSummary']     = $arrSegmentSummary;
            //$arrFltResult[$p]['taxes']              = $arrTaxes;
            //$arrFltResult[$p]['Is_Refundable']       = $strRefundable;
            $arrFltResult[$p]['IsRefundable']       = ($strRefundable === true)?1:0;
            $arrFltResult[$p]['Attr']        = 
            array(
              'IsRefundable' => ($strRefundable === true)?1:0,
              'AirlineRemark' =>"",
            );
            $arrFltResult[$p]['Token']              = $strToken;
            $arrFltResult[$p]['TokenKey']           = md5($strToken);
            $arrFltResult[$p]['booking_source']     = SABRE_FLIGHT_BOOKING_SOURCE;
            $arrFltResult[$p]['PassengerFareBreakdown'] = $arrPTCBreakDown;            
        }
    }
    $flight_result['data']['Flights'][0]      = $arrFltResult;
    return $flight_result;
}

function getSegmentElapseHrsMins($strData){
    $strTime = $strData/60;
    $strHRS  = floor($strTime);
    $strMins = $strData*1-($strHRS*60);
    $retData = $strHRS." Hrs ".$strMins." Mins";
    return $retData;
}

function parse_flight_search_response_SEP20($SearchResponse,$type,$search_data){
      error_reporting(0);
      $CI = & get_instance();
      $CI->load->model('Flight_Model');
        $array = $this->xml2array($SearchResponse);
        //$publisharray = $this->xml2array($SearchResponse_publish);

      $arrAirlines = $CI->Flight_Model->get_all_airline();
      $arrAirlineNames = array();
      $arrAirlineIDs = array();
      for($a=0; $a<count($arrAirlines); $a++){
        $arrAirlineNames[$arrAirlines[$a]->code] = $arrAirlines[$a]->name;
        $arrAirlineIDs[$arrAirlines[$a]->code]   = $arrAirlines[$a]->origin;
      }

      $arrAirports = $CI->Flight_Model->get_all_airports(); 
      $arrAirportNames = array();
      for($a=0; $a<count($arrAirports); $a++){
        $arrAirportNames[$arrAirports[$a]->airport_code] = $arrAirports[$a]->airport_city;
      }

      $arrEconomy['Y'] = 'Economy';
      $arrEconomy['S'] = 'PremiumEconomy';
      $arrEconomy['C'] = 'Business';
      $arrEconomy['J'] = 'PremiumBusiness';
      $arrEconomy['F'] = 'First';
      $arrEconomy['P'] = 'PremiumFirst';


      //debug($array);exit;
      //$currency_obj = new Currency(array('module_type' => 'flight','from' => 'CAD', 'to' => 'CAD')); 
      $currency_obj = new Currency(array('module_type' => 'flight','from' => "CAD", 'to' => get_application_display_currency_preference())); 
      // debug($currency_obj);die;
   
      // debug($this->CI->session->userdata('currency'));die;
      // $get_currency_symbol = 'CAD';
      $get_currency_symbol = ($this->CI->session->userdata('currency') != '') ? $this->CI->session->userdata('currency') : get_application_default_currency();
      // debug($get_currency_symbol);die;
      $current_currency_symbol = $currency_obj->get_currency_symbol($get_currency_symbol);
      //$converted_currency_rate = 1;
      $converted_currency_rate = $currency_obj->getConversionRate(false);
      // debug($current_currency_symbol);die('111');
      $tax    = '';
      $res    = '';      
      $k      = 0;

      $flight_result      = '';
      $DepartureDateTime  = ''; 
      $ArrivalDateTime    = ''; 
      $respublisharray    = '';
      if (isset($array['SOAP-ENV:Envelope']['SOAP-ENV:Body']['OTA_AirLowFareSearchRS']['PricedItineraries'])) {
      //debug($array['SOAP-ENV:Envelope']['SOAP-ENV:Body']['OTA_AirLowFareSearchRS']['PricedItineraries']['PricedItinerary']);die;
      $res = $array['SOAP-ENV:Envelope']['SOAP-ENV:Body']['OTA_AirLowFareSearchRS']['PricedItineraries']['PricedItinerary'];

      // debug($res);die;
      $stopover_airport = '';
      if (isset($res)) {
        if (isset($res['attr']['SequenceNumber']) && $res['attr']['SequenceNumber']==1) { 
          $res_reassign = $res;
          unset($res);
          $res[0]=$res_reassign;
        }
        $flight_result['status'] = FAILURE_STATUS;
        if (!isset($res[0])) {
          echo 'errors';
        } else {
          $flight_result['status'] = SUCCESS_STATUS;
        //  debug($res);die('ll;l');
          for ($i = 0; $i < count($res); $i++) { // No Of Flights
            $result = array();
            $resultrespublish = array();
            $orgindestination = array();
            // debug($res[$i]);die;
            $result = $res[$i]['AirItinerary']['OriginDestinationOptions']['OriginDestinationOption'];
            if(false){
              $resultrespublish = $respublisharray[$i]['AirItinerary']['OriginDestinationOptions']['OriginDestinationOption'];
            }
            if (isset($result[0]))
              $orgindestination = $result;
            else
              $orgindestination[0] = $result;
          //  debug($orgindestination);die;
            for ($d = 0; $d < count($orgindestination); $d++) { 
              //$pitInTotalFare = array();
              //debug($orgindestination[$d]);die('$orgindestination[$d]');
              $publishfare_flightcode = (isset($orgindestination[$d]['FlightSegment'][0]['OperatingAirline']['attr']['Code'])) ? $orgindestination[$d]['FlightSegment'][0]['OperatingAirline']['attr']['Code'] : $orgindestination[$d]['FlightSegment']['OperatingAirline']['attr']['Code'];
              $publishfare_flightFlightNumber = (isset($orgindestination[$d]['FlightSegment'][0]['OperatingAirline']['attr']['FlightNumber'])) ? $orgindestination[$d]['FlightSegment'][0]['OperatingAirline']['attr']['FlightNumber'] : $orgindestination[$d]['FlightSegment']['OperatingAirline']['attr']['FlightNumber'];
              $publishfare_flightcode1 = (isset($orgindestination[$d]['FlightSegment'][1]['OperatingAirline']['attr']['Code'])) ? $orgindestination[$d]['FlightSegment'][1]['OperatingAirline']['attr']['Code'] : $orgindestination[$d]['FlightSegment']['OperatingAirline']['attr']['Code'];
              $publishfare_flightFlightNumber1 = (isset($orgindestination[$d]['FlightSegment'][1]['OperatingAirline']['attr']['FlightNumber'])) ? $orgindestination[$d]['FlightSegment'][1]['OperatingAirline']['attr']['FlightNumber'] : $orgindestination[$d]['FlightSegment']['OperatingAirline']['attr']['FlightNumber'];

                $PrivateFare_flightcode = '';
                $PrivateFare_flightFlightNumber = '';
                //debug($publisharray['SOAP-ENV:Envelope']['SOAP-ENV:Body']['OTA_AirLowFareSearchRS']['PricedItineraries']['PricedItinerary']);die;

              //}
             // die;
              //echo "pankaj".$i; debug($publisharray['SOAP-ENV:Envelope']['SOAP-ENV:Body']['OTA_AirLowFareSearchRS']['PricedItineraries']['PricedItinerary']);die;

                // echo "<br/>pankaj publish";debug(@$Privatefarefarebasic);die;
               if(isset($res[$i]['AirItineraryPricingInfo']['PTC_FareBreakdowns']['PTC_FareBreakdown']['FareBasisCodes']['FareBasisCode'][0])){
               $publicfarefarebasic = $res[$i]['AirItineraryPricingInfo']['PTC_FareBreakdowns']['PTC_FareBreakdown']['FareBasisCodes']['FareBasisCode'];
             }else{

               $publicfarefarebasic[0] = $res[$i]['AirItineraryPricingInfo']['PTC_FareBreakdowns']['PTC_FareBreakdown']['FareBasisCodes']['FareBasisCode'];
             }
             //  $itInTotalFare = $res[$i]['AirItineraryPricingInfo']['ItinTotalFare'];
               $itInTotalFare = $res[$i]['AirItineraryPricingInfo']['ItinTotalFare'];
              
              //echo "pankaj".$i; debug($pitInTotalFare);die;
              //$itInTotalFare
              $flight_result[$i][$d]['FlightDetailsID'] = $i;
              $flight_result[$i][$d]['ResultType'] = $type;
              $flight_result[$i][$d]['BaseFare'] = $itInTotalFare['BaseFare']['attr']['Amount'];
              $flight_result[$i][$d]['TotalTax'] = ceil(($itInTotalFare['TotalFare']['attr']['Amount'] - $itInTotalFare['EquivFare']['attr']['Amount']) * $converted_currency_rate);
              $flight_result[$i][$d]['TotalFare_org'] = ceil($itInTotalFare['TotalFare']['attr']['Amount'] * $converted_currency_rate);
              /*formating according to neptune structure start here*/
              $flight_result[$i][$d]['price']['api_currency'] = $current_currency_symbol;
              $flight_result[$i][$d]['price']['api_total_display_fare'] = ceil($itInTotalFare['TotalFare']['attr']['Amount'] * $converted_currency_rate);
              $flight_result[$i][$d]['price']['api_total_display_fare_withoutmarkup'] = ceil($itInTotalFare['TotalFare']['attr']['Amount'] * $converted_currency_rate);

              $flight_result[$i][$d]['price']['total_breakup']['api_total_fare'] = ceil($itInTotalFare['EquivFare']['attr']['Amount'] * $converted_currency_rate);

              $flight_result[$i][$d]['price']['total_breakup']['api_total_tax'] = ceil(($itInTotalFare['TotalFare']['attr']['Amount'] - $itInTotalFare['EquivFare']['attr']['Amount']) * $converted_currency_rate);
              $flight_result[$i][$d]['price']['pax_breakup'] = array();

              $flight_result[$i][$d]['fare'][$d]['api_currency'] = $current_currency_symbol;
              $flight_result[$i][$d]['fare'][$d]['api_total_display_fare'] = ceil($itInTotalFare['TotalFare']['attr']['Amount'] * $converted_currency_rate);


              for ($zz=0; $zz < count($publicfarefarebasic); $zz++) { 
                 $flight_result[$i][$d]['fare'][$d]['publicFareBasisCodes'][$zz]['publicFareBasisCodes_value'] = $publicfarefarebasic[$zz]['value'];
                 $flight_result[$i][$d]['fare'][$d]['publicFareBasisCodes'][$zz]['publicFareBasisCodes_DepartureAirportCode'] = $publicfarefarebasic[$zz]['attr']['DepartureAirportCode'];
                 $flight_result[$i][$d]['fare'][$d]['publicFareBasisCodes'][$zz]['publicFareBasisCodes_ArrivalAirportCode'] = $publicfarefarebasic[$zz]['attr']['ArrivalAirportCode'];
                 //debug($Privatefarefarebasic);die;

              }
              $flight_result[$i][$d]['fare'][$d]['api_total_display_fare_withoutmarkup'] = ceil($itInTotalFare['TotalFare']['attr']['Amount'] * $converted_currency_rate);
              $flight_result[$i][$d]['fare'][$d]['total_breakup']['api_total_fare'] = ceil($itInTotalFare['EquivFare']['attr']['Amount'] * $converted_currency_rate);

              $flight_result[$i][$d]['fare'][$d]['total_breakup']['api_total_tax'] = ceil(($itInTotalFare['TotalFare']['attr']['Amount'] - $itInTotalFare['EquivFare']['attr']['Amount']) * $converted_currency_rate);
              $flight_result[$i][$d]['fare'][$d]['pax_breakup'] = array();
              /*formating according to neptune structure ends here*/
              $flight_result[$i][$d]['BaseFare_CurrencyCode'] = $current_currency_symbol;
              
              $flight_result[$i][$d]['EquivFare'] = ceil($itInTotalFare['EquivFare']['attr']['Amount'] * $converted_currency_rate);
              $flight_result[$i][$d]['EquivFare_CurrencyCode'] = $current_currency_symbol;
              
              
              $flight_result[$i][$d]['TotalFare'] = $itInTotalFare['TotalFare']['attr']['Amount'];
              $flight_result[$i][$d]['TotalFare_CurrencyCode'] = $converted_currency_rate;
              $amount = array();
              $amount = $flight_result[$i][$d]['BaseFare'];
              $flight_result[$i][$d]['amount'] = $amount;
              $flight_result[$i][$d]['admin_markup'] = '';
              $flight_result[$i][$d]['payment_amount'] = '';
              $flight_result[$i][$d]['agent_markup'] = '';

              $taxes1 = array();
              $taxes = array();
              $taxes1 = $itInTotalFare['Taxes']['Tax'];
              if (isset($taxes[0])) {
                $taxes = $taxes1;
              } else {
                $taxes[0] = $taxes1;
              }
              for ($t = 0; $t < count($taxes); $t++) {
                $tax[$t]['Amount'] = $taxes[$t]['attr']['Amount'];
                $tax[$t]['TaxCode'] = $taxes[$t]['attr']['TaxCode'];
                $tax[$t]['Tax_CurrencyCode'] = $taxes[$t]['attr']['CurrencyCode'];
              }
              $fight_details1 = array();
              $fight_details = array();
              $flight_result[$i][$d]['ElapsedTime'] = '';
              $fight_details1 = $orgindestination[$d]['FlightSegment'];
              $count_flight = count($fight_details);

              if (isset($fight_details1[0])) {
                $fight_details = $fight_details1;
              } else {
                $fight_details[0] = $fight_details1;
              }
              for ($p = 0; $p < count($fight_details); $p++) { // No Of Segmemnts
                $OriginLocation = $fight_details[$p]['DepartureAirport']['attr']['LocationCode'];
                $DestinationLocation = $fight_details[$p]['ArrivalAirport']['attr']['LocationCode'];
                $flight_result[$i][$d]['OriginLocation'][$p] = $fight_details[$p]['DepartureAirport']['attr']['LocationCode'];
                $flight_result[$i][$d]['DestinationLocation'][$p] = $fight_details[$p]['ArrivalAirport']['attr']['LocationCode'];
                $flight_result[$i][$d]['FlighvgtNumber_no'][$p] = $fight_details[$p]['attr']['FlightNumber'];
                $flight_result[$i][$d]['ResBookDesigCode'][$p] = $fight_details[$p]['attr']['ResBookDesigCode'];
                $flight_result[$i][$d]['StopQuantity'][$p] = $fight_details[$p]['attr']['StopQuantity'];
                $flight_result[$i][$d]['ElapsedTime']+= $fight_details[$p]['attr']['ElapsedTime'];
                $flight_result[$i][$d]['StopOver'][$p] = $fight_details[$p]['attr']['ElapsedTime'];

                $time = $fight_details[$p]['attr']['ElapsedTime'];
                $hours = floor($time / 60);
                $minutes = ($time % 60);
                $duration = $hours . "h " . $minutes . "m";
                $flight_result[$i][$d]['Elapsed_Duration'][$p] = $duration;

                $flight_result[$i][$d]['Equipment'][$p] = $fight_details[$p]['Equipment']['attr']['AirEquipType'];
                $flight_result[$i][$d]['MarketingAirline'][$p] = $fight_details[$p]['MarketingAirline']['attr']['Code'];
                $flight_result[$i][$d]['MarriageGrp'][$p] = $fight_details[$p]['MarriageGrp']['value'];
                $flight_result[$i][$d]['OperatingAirline'][$p] = $fight_details[$p]['OperatingAirline']['attr']['Code'];
                $flight_result[$i][$d]['DepartureTimeZone'][$p] = $fight_details[$p]['DepartureTimeZone']['attr']['GMTOffset'];
                $flight_result[$i][$d]['ArrivalTimeZone'][$p] = $fight_details[$p]['ArrivalTimeZone']['attr']['GMTOffset'];
                $flight_result[$i][$d]['stops'] = ((count($fight_details)) - 1);
                $airline_name = $fight_details[$p]['MarketingAirline']['attr']['Code'];


                $flight_result[$i][$d]['Airline_name'][$p] = $arrAirlineNames[$airline_name];
                //$markup_details = $this->Flight_Model->get_markup_details($airline_details_id);
                if(false){
                    $markup_details = "";
                    if($markup_details!=''){
                      $markup_value = $flight_result[$i][$d]['markup_value']    = $markup_details->markup_value;
                      $markup_fixed = $flight_result[$i][$d]['markup_fixed']    = $markup_details->markup_fixed;
                      if($markup_value != ''){
                        $flight_result[$i][$d]['admin_markup']  = $admin_markup = (($amount + $markup_fixed) * $markup_value) / 100 + $markup_fixed;
                      }else{
                        $flight_result[$i][$d]['admin_markup']  = $admin_markup = (($markup_fixed));
                      }
                      $flight_result[$i][$d]['TotalFare']     = $flight_result[$i][$d]['amount']  = $flight_result[$i][$d]['TotalTax'] + $amount + $admin_markup;
                    }else{
                      $markup_value = $markup_fixed = '';
                    }
                }else{
                  $markup_value = $markup_fixed = 0;
                }
                // /echo "<pre/>";print_r($admin_markup);exit;
                $flight_result[$i][$d]['StopQuantity'][$p] = $fight_details[$p]['attr']['StopQuantity'];
                $flight_result[$i][$d]['eTicket'][$p] = $fight_details[$p]['TPA_Extensions']['eTicket']['attr']['Ind'];

                 //$origin_name = $this->CI->flight_model->get_airport_cityname($OriginLocation);
                 //$destination_name = $this->CI->flight_model->get_airport_cityname($DestinationLocation);

                 $origin_name = $arrAirportNames[$OriginLocation];
                 $destination_name = $arrAirportNames[$DestinationLocation];
              
                if ($destination_name != '') {
                    $disp_name1  = $origin_name. '(' . $OriginLocation . ')' . ' - ' . $destination_name. '(' . $DestinationLocation . ')';                 
                    $disp_name[] = $disp_name1;
                } else {
                    $disp_name[] = '';$disp_name1 = '';
                 }
                $pankaj_origin = $flight_result[$i][$d]['Origin'][$p]   = $origin_name;
                $mytrip_trip = 'onward';
                if ($d == 1) {
                  $mytrip_trip = 'return';
                }
                
                
                $airline_details_id = $arrAirlineIDs[$airline_name];
                
                /*rest formatting ends here*/
                /*summary formatting ends here*/
                $flight_result[$i][$d]['Destination'][$p] = $destination_name;
                $flight_result[$i][$d]['disp_name'][$p] = $disp_name1;
                $flight_result[$i][$d]['Equipment'][$p] = $fight_details[$p]['Equipment']['attr']['AirEquipType'];

                $DepartureDateTime_r = $fight_details[$p]['attr']['DepartureDateTime'];
                $flight_result[$i][$d]['DepartureDateTime_r'][$p] = $fight_details[$p]['attr']['DepartureDateTime'];

                $DepartureDateTime_rp[] = $DepartureDateTime_r;
                $DepartureDateTime_r_1 = explode('T', $DepartureDateTime_r);
                $DepartureDateTime_r_2 = explode(':', $DepartureDateTime_r_1[1]);
                $flight_result[$i][$d]['dateOfDeparture'][$p] = $DepartureDateTime_r_1[0];
                $flight_result[$i][$d]['timeOfDeparture'][$p] = $DepartureDateTime_r_2[0] . ":" . $DepartureDateTime_r_2[1];
                $flight_result[$i][$d]['timeOfDeparture'][$p] = $DepartureDateTime_r_2[0] . $DepartureDateTime_r_2[1];
                $DepartureDateTime[].=$DepartureDateTime_r_2[0] . ":" . $DepartureDateTime_r_2[1];
                $DepartureDateTimediff = $DepartureDateTime_r_2[0] . ":" . $DepartureDateTime_r_2[1];

                $ArrivalDateTime_r = $fight_details[$p]['attr']['ArrivalDateTime'];
                if(isset($fight_details[$p]['ArrivalAirport']['attr']['TerminalID'])){
                  $flight_result[$i][$d]['TerminalID'][$p]      = $fight_details[$p]['ArrivalAirport']['attr']['TerminalID'];
                }else{
                  $flight_result[$i][$d]['TerminalID'][$p]      = '';
                }
                
                if(isset($fight_details[$p]['DepartureAirport']['attr']['TerminalID'])){
                  $flight_result[$i][$d]['TerminalID'][$p]      = $fight_details[$p]['DepartureAirport']['attr']['TerminalID'];
                }else{
                  $flight_result[$i][$d]['TerminalID'][$p]      = '';
                }

                $flight_result[$i][$d]['ArrivalDateTime_r'][$p] = $fight_details[$p]['attr']['ArrivalDateTime'];
                $ArrivalDateTime_rp[] = $ArrivalDateTime_r;
                $ArrivalDateTime_r_1 = explode('T', $ArrivalDateTime_r);
                $ArrivalDateTime_r_2 = explode(':', $ArrivalDateTime_r_1[1]);
                $flight_result[$i][$d]['dateOfArrival'][$p] = $ArrivalDateTime_r_1[0];
                $flight_result[$i][$d]['timeOfArrival'][$p] = $ArrivalDateTime_r_2[0] . ":" . $ArrivalDateTime_r_2[1];
                $flight_result[$i][$d]['timeOfArrival'][$p] = $ArrivalDateTime_r_2[0] . $ArrivalDateTime_r_2[1];
                $ArrivalDateTime[].=$ArrivalDateTime_r_2[0] . ":" . $ArrivalDateTime_r_2[1];
                $ArrivalDateTimediff = $ArrivalDateTime_r_2[0] . ":" . $ArrivalDateTime_r_2[1];
                $flight_result[$i][$d]['final_duration'] = $this->calculate_time_zone_duration($flight_result[$i][$d]['DepartureDateTime_r'][0],$flight_result[$i][$d]['ArrivalDateTime_r'][$p],$flight_result[$i][$d]['DepartureTimeZone'][0],$flight_result[$i][$d]['ArrivalTimeZone'][$p]);
                $flight_result[$i][$d]['segment_duration'][$p] = $this->calculate_time_zone_duration($flight_result[$i][$d]['DepartureDateTime_r'][$p],$flight_result[$i][$d]['ArrivalDateTime_r'][$p],$flight_result[$i][$d]['DepartureTimeZone'][$p],$flight_result[$i][$d]['ArrivalTimeZone'][$p]);
                
              if($p != '0' && ($p+1) <= count($fight_details)){
                list($date, $time) = explode('T', $flight_result[$i][$d]['DepartureDateTime_r'][($p)]);
                $DepartureDateTime_l = $date." ".$time; //Exploding T and adding space
                list($date, $time) = explode('T', $flight_result[$i][$d]['ArrivalDateTime_r'][($p-1)]);
                $ArrivalDateTime_l = $date." ".$time; //Exploding T and adding space
                // echo $ArrivalDateTime_l. " ".$DepartureDateTime_l;exit;
                $flight_result[$i][$d]['layover_duration'][$p] = $this->calculate_time_zone_duration($ArrivalDateTime_l,$DepartureDateTime_l,$flight_result[$i][$d]['ArrivalTimeZone'][($p-1)],$flight_result[$i][$d]['DepartureTimeZone'][($p)]);
              }
                // echo '<pre/>';print_r($flight_result);exit;
                $flight_result[$i][$d]['tax'] = $tax;

                //Extra Features 
                $PTC_Fare1 = array();
                $PTC_Fare = array();
                if (isset($res[$i]['TPA_Extensions']['AdditionalFares']['AirItineraryPricingInfo'])) {
                //debug($res[$i]['TPA_Extensions']['AdditionalFares']['AirItineraryPricingInfo']['PTC_FareBreakdowns']['PTC_FareBreakdown']);die('inside');
                $PTC_Fare1 = $res[$i]['TPA_Extensions']['AdditionalFares']['AirItineraryPricingInfo']['PTC_FareBreakdowns']['PTC_FareBreakdown'];
                if (isset($PTC_Fare1[0])) {
                  $PTC_Fare = $PTC_Fare1;
                } else {
                  $PTC_Fare[0] = $PTC_Fare1;
                }
                // echo '<pre/>';print_r($PTC_Fare);exit;
                $PTC_Fare_count = count($PTC_Fare);
                for ($pi = 0; $pi < $PTC_Fare_count; $pi++) {
                  $flight_result[$i][$d]['PCode'][$pi] = $PTC_Fare[$pi]['PassengerTypeQuantity']['attr']['Code'];
                  $flight_result[$i][$d]['PQuantity'][$pi] = $PTC_Fare[$pi]['PassengerTypeQuantity']['attr']['Quantity'];
                  $flight_result[$i][$d]['PEquivFare_org'][$pi] = $PTC_Fare[$pi]['PassengerFare']['EquivFare']['attr']['Amount'];
                  $flight_result[$i][$d]['PTaxFare'][$pi] = (($PTC_Fare[$pi]['PassengerFare']['TotalFare']['attr']['Amount']) - ($PTC_Fare[$pi]['PassengerFare']['EquivFare']['attr']['Amount']));
                  $flight_result[$i][$d]['PTotalFare_org'][$pi] = ($PTC_Fare[$pi]['PassengerFare']['TotalFare']['attr']['Amount']);
                  if($markup_value != ''){
                    $adminMarkup = (((($PTC_Fare[$pi]['PassengerFare']['EquivFare']['attr']['Amount'])) + $markup_fixed) * $markup_value) / 100  + $markup_fixed;
                  }else{
                    $adminMarkup = $markup_fixed;
                  }
                  $flight_result[$i][$d]['PEquivFare'][$pi] = ($PTC_Fare[$pi]['PassengerFare']['EquivFare']['attr']['Amount']  + $adminMarkup);
                  $flight_result[$i][$d]['PTotalFare'][$pi] = (($flight_result[$i][$d]['PEquivFare'][$pi]) + ($flight_result[$i][$d]['PTaxFare'][$pi]));
                  
                  $flight_result[$i][$d]['PEquivFare_CurrencyCode'][$pi] = $PTC_Fare[$pi]['PassengerFare']['EquivFare']['attr']['CurrencyCode'];
                  $flight_result[$i][$d]['PTotalFare_CurrencyCode'][$pi] = $PTC_Fare[$pi]['PassengerFare']['TotalFare']['attr']['CurrencyCode'];
                  
                  $BaggageInformation1 = array(); $BaggageInformation = array();
                  $BaggageInformation1 = @$PTC_Fare[$pi]['PassengerFare']['TPA_Extensions']['BaggageInformationList']['BaggageInformation'];
                  
                  if(isset($BaggageInformation1[0])){
                    $BaggageInformation = $BaggageInformation1;
                  }else{
                    $BaggageInformation[0] = $BaggageInformation1;
                  }
                  
                  // echo '<pre/>';print_r($BaggageInformation);exit;
                  foreach($BaggageInformation as $key => $baggage_info){
                    $Segment1 = array(); $Segment = array();                    
                    $Segment1 = $baggage_info['Segment'];
                    if(isset($Segment1[0])){
                      $Segment = $Segment1;
                    }else{
                      $Segment[0] = $Segment1;
                    }
                    foreach($Segment as $key1 => $segment_info){
                      $flight_result[$i][$d]['SegmentId'][$pi][$key1] = $key1;
                    }
                    if(isset($baggage_info['Allowance']['attr']['Pieces'])){
                      $flight_result[$i][$d]['Weight_Allowance'][$pi] = $baggage_info['Allowance']['attr']['Pieces'] ." - Pieces";
                    }else {
                      $flight_result[$i][$d]['Weight_Allowance'][$pi] = $baggage_info['Allowance']['attr']['Weight'].'-'.$baggage_info['Allowance']['attr']['Unit'];
                    }
                  }                 
                  $flight_result[$i][$d]['nonRefundable'][$pi] = $PTC_Fare[$pi]['Endorsements']['attr']['NonRefundableIndicator'];
                  if (isset($PTC_Fare[$pi]['PassengerFare']['Taxes'])) {
                    $ptaxes = $PTC_Fare[$pi]['PassengerFare']['Taxes']['Tax'];

                    if (isset($ptaxes[0])) {
                      $ptaxes = $ptaxes;
                    } else {
                      $ptaxes[0] = $ptaxes;
                    }
                    for ($t = 0; $t < count($ptaxes); $t++) {
                      $ptax[$t]['Amount'] = $ptaxes[$t]['attr']['Amount'];
                      $ptax[$t]['TaxCode'] = $ptaxes[$t]['attr']['TaxCode'];
                      $ptax[$t]['Tax_CurrencyCode'] = $ptaxes[$t]['attr']['CurrencyCode'];
                    }
                  } else
                    $ptax = '';
                  $flight_result[$i][$d]['PTax'] = $ptax;
                }
             } else{
                $PTC_Fare1 = $res[$i]['AirItineraryPricingInfo']['PTC_FareBreakdowns']['PTC_FareBreakdown'];
                if (isset($PTC_Fare1[0])) {
                  $PTC_Fare = $PTC_Fare1;
                } else {
                  $PTC_Fare[0] = $PTC_Fare1;
                }
                // echo '<pre/>';print_r($PTC_Fare);exit;
                $PTC_Fare_count = count($PTC_Fare);
                for ($pi = 0; $pi < $PTC_Fare_count; $pi++) {
                  $flight_result[$i][$d]['PCode'][$pi] = $PTC_Fare[$pi]['PassengerTypeQuantity']['attr']['Code'];
                  $flight_result[$i][$d]['PQuantity'][$pi] = $PTC_Fare[$pi]['PassengerTypeQuantity']['attr']['Quantity'];
                  $flight_result[$i][$d]['PEquivFare_org'][$pi] = $PTC_Fare[$pi]['PassengerFare']['EquivFare']['attr']['Amount'];
                  $flight_result[$i][$d]['PTaxFare'][$pi] = (($PTC_Fare[$pi]['PassengerFare']['TotalFare']['attr']['Amount']) - ($PTC_Fare[$pi]['PassengerFare']['EquivFare']['attr']['Amount']));
                  $flight_result[$i][$d]['PTotalFare_org'][$pi] = ($PTC_Fare[$pi]['PassengerFare']['TotalFare']['attr']['Amount']);
                  if($markup_value != ''){
                    $adminMarkup = (((($PTC_Fare[$pi]['PassengerFare']['EquivFare']['attr']['Amount'])) + $markup_fixed) * $markup_value) / 100  + $markup_fixed;
                  }else{
                    $adminMarkup = $markup_fixed;
                  }
                  $flight_result[$i][$d]['PEquivFare'][$pi] = ($PTC_Fare[$pi]['PassengerFare']['EquivFare']['attr']['Amount']  + $adminMarkup);
                  $flight_result[$i][$d]['PTotalFare'][$pi] = (($flight_result[$i][$d]['PEquivFare'][$pi]) + ($flight_result[$i][$d]['PTaxFare'][$pi]));
                  
                  $flight_result[$i][$d]['PEquivFare_CurrencyCode'][$pi] = $PTC_Fare[$pi]['PassengerFare']['EquivFare']['attr']['CurrencyCode'];
                  $flight_result[$i][$d]['PTotalFare_CurrencyCode'][$pi] = $PTC_Fare[$pi]['PassengerFare']['TotalFare']['attr']['CurrencyCode'];
                  
                  $BaggageInformation1 = array(); $BaggageInformation = array();
                  $BaggageInformation1 = @$PTC_Fare[$pi]['PassengerFare']['TPA_Extensions']['BaggageInformationList']['BaggageInformation'];
                  
                  if(isset($BaggageInformation1[0])){
                    $BaggageInformation = $BaggageInformation1;
                  }else{
                    $BaggageInformation[0] = $BaggageInformation1;
                  }
                  
                  // echo '<pre/>';print_r($BaggageInformation);exit;
                  foreach($BaggageInformation as $key => $baggage_info){
                    $Segment1 = array(); $Segment = array();                    
                    $Segment1 = $baggage_info['Segment'];
                    if(isset($Segment1[0])){
                      $Segment = $Segment1;
                    }else{
                      $Segment[0] = $Segment1;
                    }
                    foreach($Segment as $key1 => $segment_info){
                      $flight_result[$i][$d]['SegmentId'][$pi][$key1] = $key1;
                    }
                    if(isset($baggage_info['Allowance']['attr']['Pieces'])){
                      $flight_result[$i][$d]['Weight_Allowance'][$pi] = $baggage_info['Allowance']['attr']['Pieces'] ." - Pieces";
                    }else {
                      $flight_result[$i][$d]['Weight_Allowance'][$pi] = $baggage_info['Allowance']['attr']['Weight'].'-'.$baggage_info['Allowance']['attr']['Unit'];
                  }
                  }                 
                  $flight_result[$i][$d]['nonRefundable'][$pi] = $PTC_Fare[$pi]['Endorsements']['attr']['NonRefundableIndicator'];
                  if (isset($PTC_Fare[$pi]['PassengerFare']['Taxes'])) {
                    $ptaxes = $PTC_Fare[$pi]['PassengerFare']['Taxes']['Tax'];

                    if (isset($ptaxes[0])) {
                      $ptaxes = $ptaxes;
                    } else {
                      $ptaxes[0] = $ptaxes;
                    }
                    for ($t = 0; $t < count($ptaxes); $t++) {
                      $ptax[$t]['Amount'] = $ptaxes[$t]['attr']['Amount'];
                      $ptax[$t]['TaxCode'] = $ptaxes[$t]['attr']['TaxCode'];
                      $ptax[$t]['Tax_CurrencyCode'] = $ptaxes[$t]['attr']['CurrencyCode'];
                    }
                  } else
                    $ptax = '';
                  $flight_result[$i][$d]['PTax'] = $ptax;
                }
             }
              
                $FareInfos = array();
                $FrInfo = array();
                $FrInfo = (isset($res[$i]['TPA_Extensions']['AdditionalFares']['AirItineraryPricingInfo']['PTC_FareBreakdowns']['PTC_FareBreakdown']['FareInfos']['FareInfo'])) ? $res[$i]['TPA_Extensions']['AdditionalFares']['AirItineraryPricingInfo']['PTC_FareBreakdowns']['PTC_FareBreakdown']['FareInfos']['FareInfo'] : $res[$i]['AirItineraryPricingInfo']['FareInfos']['FareInfo'];
                if (isset($FrInfo[0]))
                  $FareInfos = $FrInfo;
                else
                  $FareInfos[0] = $FrInfo;

                $FareInfos_count = count($FareInfos);
                for ($fi = 0; $fi < $FareInfos_count; $fi++) {
                  $flight_result[$i][$d]['SeatsRemaining'][$fi] = $FareInfos[$fi]['TPA_Extensions']['SeatsRemaining']['attr']['Number'];
                  //$flight_result[$i][$d]['flight_details']['details'][$i][$d]['SeatsRemaining'] = $FareInfos[$fi]['TPA_Extensions']['SeatsRemaining']['attr']['Number'];
                  //$flight_result[$i][$d]['flight_details']['summary'][$i]['SeatsRemaining'] = $FareInfos[$fi]['TPA_Extensions']['SeatsRemaining']['attr']['Number'];
                  $economyCode = $flight_result[$i][$d]['Cabin'][$fi] = $FareInfos[$fi]['TPA_Extensions']['Cabin']['attr']['Cabin'];


                  $economy = isset($arrEconomy[$economyCode])?isset($arrEconomy[$economyCode]):"Economy";
                  if (isset($FareInfos[$fi]['TPA_Extensions']['Meal']['attr']['Code'])) {
                    $flight_result[$i][$d]['Meal'][$fi] = $FareInfos[$fi]['TPA_Extensions']['Meal']['attr']['Code'];
                  } else {
                    $flight_result[$i][$d]['Meal'][$fi] = '';
                  }
                }
              }
            }
           // debug($flight_result[$i]);die;  
            $flight_result[$i][0]['token']    = serialized_data($flight_result[$i]);
            $flight_result[$i][0]['token_key']  = md5 ( $flight_result[$i][0]['token']);           
          }  //die;
        }
      }
    }
// echo "flight_result<pre/>";print_r($flight_result);exit;
    return $flight_result;
    }


    /*for time zone calculation start here*/
    function calculate_time_zone_duration($ddate,$adate,$dep_zone,$arv_zone){
    $ddate = str_replace("T"," ",$ddate); $adate = str_replace("T"," ",$adate);
    //echo $arv_zone.'<br/>'.$dep_zone;
    $Change_clock=($arv_zone)-($dep_zone);
    if(!is_int($Change_clock)){
     // debug($Change_clock);exit;
      $Changeclock  = explode(".", $Change_clock);
      //debug($Changeclock);exit;
      $Changeclock0 = $Changeclock[0];
      if (count($Changeclock) > 1) {
        if($Changeclock0 > 0){
        $Changeclock1   = ($Changeclock[1]*6);
      }else{
        $Changeclock1   = (-1 * $Changeclock[1]*6);
      }
      } else {
        if($Changeclock0 > 0){
        $Changeclock1   = ($Changeclock[0]*6);
      }else{
        $Changeclock1   = (-1 * $Changeclock[0]*6);
      }
      }
      
    }else{
      $Changeclock0 = $Change_clock;
      $Changeclock1 = 0;
    }   
    // echo $ddate." ".$dep_zone." ".$adate." ".$arv_zone." ".$Changeclock0." ".$Changeclock1."<br/>";
    $date_a   = new DateTime($ddate);
    $date_b   = new DateTime($adate);
    $interval   = date_diff($date_a, $date_b);
    $hour     = $interval->format('%h');
    $min    = $interval->format('%i');
    $day1   = $interval->format('%d');
    $dur_in_min = ((($hour * 60) + $min) - (($Changeclock0 * 60) + $Changeclock1));
    $hour     = FLOOR($dur_in_min / 60);
    $min    = $dur_in_min%60;
    // echo '<pre/>';print_r($interval);
    if($hour<0){  $hour=((24)+($hour)); $day1     -= 1;
    }else{
      $day1     += floor(((($hour * 60) + $min) / 1440));
    }
    if($min<0){ $min=((60)+($min)); }
    $hours    = floor((($hour * 60) + $min) / 60);
    $minutes  = ((($hour * 60) + $min) % 60);
    
    if($hours>24){ $hours=($hours % 24);}
    if ($day1 > 0)
      $dur=$day1."d ".$hours."h ".$minutes."m";
    else
      $dur=$hours."h ".$minutes."m";
    return $dur;
        
  }
  function xml2array($contents, $get_attributes = 1) {
        if (!$contents) return array();
        if (!function_exists('xml_parser_create')) { return array(); }
        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, $contents, $xml_values);
        xml_parser_free($parser);
        if (!$xml_values)
            return;
    $xml_array = array(); $parents = array(); $opened_tags = array(); $arr = array();
    $current = &$xml_array;
    foreach ($xml_values as $data) {
            unset($attributes, $value);
            extract($data);
      $result = '';
            if ($get_attributes) {
                $result = array();
                if (isset($value))
                    $result['value'] = $value;
        if (isset($attributes)) {
                    foreach ($attributes as $attr => $val) {
                        if ($get_attributes == 1)
                            $result['attr'][$attr] = $val;
                    }
                }
            } elseif (isset($value)) {
                $result = $value;
            }
      if ($type == "open") {
                $parent[$level - 1] = &$current;

                if (!is_array($current) or ( !in_array($tag, array_keys($current)))) { 
                    $current[$tag] = $result;
                    $current = &$current[$tag];
                } else { 
                    if (isset($current[$tag][0])) {
                        array_push($current[$tag], $result);
                    } else {
                        $current[$tag] = array($current[$tag], $result);
                    }
                    $last = count($current[$tag]) - 1;
                    $current = &$current[$tag][$last];
                }
            } elseif ($type == "complete") {
                if (!isset($current[$tag])) {
                    $current[$tag] = $result;
                } else { 
                    if ((is_array($current[$tag]) and $get_attributes == 0)
                            or ( isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
                        array_push($current[$tag], $result);
                    } else {
                        $current[$tag] = array($current[$tag], $result);
                    }
                }
            } elseif ($type == 'close') { 
                $current = &$parent[$level - 1];
            }
        }
        return($xml_array);
    }
/*for parsing search result end here*/
  
  /**
   * process soap API request
   *
   * @param string $request         
   */
  
  function flight_processRequest($XML){
    //debug($this->Url);die;
   $httpHeader = array( 'Content-Type: text/xml; charset="utf-8"',);
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $this->Url); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
    curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
    curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
    curl_setopt($ch, CURLOPT_POST, TRUE); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
   // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    //curl_setopt($ch, CURLOPT_SSLVERSION, 4); 
    curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
    $response = curl_exec($ch);
    //debug($response);exit;
    return $response;
  }
  public function format_neptune_structure($flight_list)
  {
    $pflight_list = $flight_list['data']['flight_data_list']['journey_list'][0];
  //debug($pflight_list);die('format_neptune_structure');
    //debug($pflight_list);die;
    for ($flight_count=0; $flight_count < count($pflight_list) ; $flight_count++) { 
      /*$xpid = 0;
      if (isset($pflight_list[$flight_count][1])) {
        $xpid = 1;
      }*/
     // debug(count($pflight_list[$flight_count]));//die;
       //debug($pflight_list[$flight_count]);die;
     // debug(count($pflight_list[$flight_count]));echo '<pre>TEST';
      if (isset($pflight_list[$flight_count])) {
        $xpid = 0;
      for ($dx=0; $dx < count($pflight_list[$flight_count]); $dx++) { 
        
        $xpid = $dx;
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['origin']['loc'] = $pflight_list[$flight_count][$xpid]['OriginLocation'][0];
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['origin']['city'] = $pflight_list[$flight_count][$xpid]['Origin'][0];
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['origin']['datetime'] = $pflight_list[$flight_count][$xpid]['DepartureDateTime_r'][0];
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['origin']['date'] = date('d M Y',strtotime($pflight_list[$flight_count][$xpid]['DepartureDateTime_r'][0]));
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['origin']['time'] = date('H:i',strtotime($pflight_list[$flight_count][$xpid]['DepartureDateTime_r'][0]));
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['origin']['fdtv'] = strtotime($pflight_list[$flight_count][$xpid]['DepartureDateTime_r'][0]);
      $mycount = count($pflight_list[$flight_count][$xpid]['Origin']) - 1;
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['destination']['loc'] = $pflight_list[$flight_count][$xpid]['DestinationLocation'][$mycount];
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['destination']['city'] = $pflight_list[$flight_count][$xpid]['Destination'][$mycount];
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['destination']['datetime'] = $pflight_list[$flight_count][$xpid]['ArrivalDateTime_r'][$mycount];
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['destination']['date'] = date('d M Y', strtotime($pflight_list[$flight_count][$xpid]['ArrivalDateTime_r'][$mycount]));
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['destination']['time'] = date('H:i', strtotime($pflight_list[$flight_count][$xpid]['ArrivalDateTime_r'][$mycount]));
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['destination']['fatv'] = strtotime($pflight_list[$flight_count][$xpid]['ArrivalDateTime_r'][$mycount]);
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['operator_code'] = $pflight_list[$flight_count][$xpid]['MarketingAirline'][0];
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['display_operator_code'] = $pflight_list[$flight_count][$xpid]['MarketingAirline'][0];
      $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['operator_name'] = $pflight_list[$flight_count][$xpid]['Airline_name'][0];
      $economy = 'Economy';
      if ($pflight_list[$flight_count][$xpid]['Cabin'][0] == 'Y') {
        $economy = 'Economy';
        }
        if ($pflight_list[$flight_count][$xpid]['Cabin'][0] == 'S') {
          $economy = 'PremiumEconomy';
        }
        if ($pflight_list[$flight_count][$xpid]['Cabin'][0] == 'C') {
          $economy = 'Business';
        }
        if ($pflight_list[$flight_count][$xpid]['Cabin'][0] == 'J') {
          $economy = 'PremiumBusiness';
        }
        if ($pflight_list[$flight_count][$xpid]['Cabin'][0] == 'F') {
          $economy = 'First';
        }
        if ($pflight_list[$flight_count][$xpid]['Cabin'][0] == 'P') {
          $economy = 'PremiumFirst';
        }
        $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['cabin_class'] = $economy;
        $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['flight_number'] = $pflight_list[$flight_count][$xpid]['FlighvgtNumber_no'][0];
        $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['no_of_stops'] = $pflight_list[$flight_count][$xpid]['stops'];
        $total_journey_duration = $pflight_list[$flight_count][$xpid]['final_duration'];
        $explode_day = explode('d', $total_journey_duration);
        $explode_hours = 0;
        $explode_hours1 = 0;
        $explode_day_cal = 0;
        if (count($explode_day) > 1) {
          $explode_day_cal = $explode_day[0];
          $explode_hours = explode('h', $explode_day[1]);
          $explode_hours1 = explode('m', $explode_hours[1]);
        } else {
          $explode_hours = explode('h', $explode_day[0]);
          $explode_hours1 = explode('m', $explode_hours[1]);
        }
        
       // $explode_hours = explode('h', $total_journey_duration);
        //$explode_hours1 = explode('m', $explode_hours[1]);
        $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['duration_seconds'] = (($explode_day_cal * 24 * 60)+($explode_hours[0] * 60) + $explode_hours1[0]);
        $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['duration'] = $total_journey_duration;
        $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['Meal'] = $pflight_list[$flight_count][$xpid]['Meal'][0];
        $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['Meal_description'] = $this->get_sabre_meal($pflight_list[$flight_count][$xpid]['Meal'][0]);
        $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['SeatsRemaining'] = $pflight_list[$flight_count][$xpid]['SeatsRemaining'][0];
        $pflight_list[$flight_count][0]['flight_details']['summary'][$xpid]['Weight_Allowance'] = $pflight_list[$flight_count][$xpid]['Weight_Allowance'][0];
      /*flight details start here*/
      
      } 
      $xspid = 0;
      for ($tript_count=0; $tript_count < count($pflight_list[$flight_count]); $tript_count++) { 
        
          $xspid = $tript_count;
        for ($mydetail_count=0; $mydetail_count < count($pflight_list[$flight_count][$tript_count]['Origin']); $mydetail_count++) { 
          $mypstrip_tpy = ($tript_count == 1) ? 'return' : 'onward';
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['journey_number'] = $mypstrip_tpy;
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['origin']['loc'] = $pflight_list[$flight_count][$tript_count]['OriginLocation'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['origin']['city'] = $pflight_list[$flight_count][$tript_count]['Origin'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['origin']['datetime'] = $pflight_list[$flight_count][$tript_count]['DepartureDateTime_r'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['origin']['date'] = date('d M Y', strtotime($pflight_list[$flight_count][$tript_count]['DepartureDateTime_r'][$mydetail_count]));
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['origin']['time'] = date('H:i', strtotime($pflight_list[$flight_count][$tript_count]['DepartureDateTime_r'][$mydetail_count]));
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['origin']['fdtv'] = strtotime($pflight_list[$flight_count][$tript_count]['DepartureDateTime_r'][$mydetail_count]);
          /*for destination start here*/
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['destination']['loc'] = $pflight_list[$flight_count][$tript_count]['DestinationLocation'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['destination']['city'] = $pflight_list[$flight_count][$tript_count]['Destination'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['destination']['datetime'] = $pflight_list[$flight_count][$tript_count]['ArrivalDateTime_r'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['destination']['date'] = date('d M Y', strtotime($pflight_list[$flight_count][$tript_count]['ArrivalDateTime_r'][$mydetail_count]));
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['destination']['time'] = date('H:i', strtotime($pflight_list[$flight_count][$tript_count]['ArrivalDateTime_r'][$mydetail_count]));
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['destination']['fdtv'] = strtotime($pflight_list[$flight_count][$tript_count]['ArrivalDateTime_r'][$mydetail_count]);
          /*for destination ends here*/
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['duration'] = $pflight_list[$flight_count][$tript_count]['segment_duration'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['layover_duration'] = @$pflight_list[$flight_count][$tript_count]['layover_duration'][1];
          $segp_duration = $pflight_list[$flight_count][$tript_count]['segment_duration'][$mydetail_count];
          $seg_explode = explode('h', $segp_duration);
          $seg_explode1 = explode('m', $seg_explode[1]);
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['duration_seconds'] = (($seg_explode[0] * 60) + $seg_explode1[0]);
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['operator_code'] = $pflight_list[$flight_count][$tript_count]['MarketingAirline'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['operator_name'] = $pflight_list[$flight_count][$tript_count]['Airline_name'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['flight_number'] = $pflight_list[$flight_count][$tript_count]['FlighvgtNumber_no'][$mydetail_count];
          $seg_class = $pflight_list[$flight_count][$tript_count]['Cabin'][$mydetail_count];
          $economy = 'Economy';
            if ($seg_class == 'Y') {
              $economy = 'Economy';
              }
              if ($seg_class == 'S') {
                $economy = 'PremiumEconomy';
              }
              if ($seg_class == 'C') {
                $economy = 'Business';
              }
              if ($seg_class == 'J') {
                $economy = 'PremiumBusiness';
              }
              if ($seg_class == 'F') {
                $economy = 'First';
              }
              if ($seg_class == 'P') {
                $economy = 'PremiumFirst';
              }
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['cabin_class'] = $economy;
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['class']['name'] = $economy;
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['class']['description'] = '';
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['booking_code'] =  $pflight_list[$flight_count][$tript_count]['ResBookDesigCode'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['SeatsRemaining'] =  $pflight_list[$flight_count][$tript_count]['SeatsRemaining'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['Meal'] =  $pflight_list[$flight_count][$tript_count]['Meal'][$mydetail_count];
          $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['Meal_description'] =  $this->get_sabre_meal($pflight_list[$flight_count][$tript_count]['Meal'][$mydetail_count]);
          if (isset($pflight_list[$flight_count][$tript_count]['Weight_Allowance'][$mydetail_count])) {
            $pflight_list[$flight_count][0]['flight_details']['details'][$xspid][$mydetail_count]['Weight_Allowance'] =  $pflight_list[$flight_count][$tript_count]['Weight_Allowance'][$mydetail_count];
          }
          

        }
        
      }

      }
      
      
      
      /*flight details ends here*/
    }
  //exit;
    //debug($pflight_list);exit('format_neptune_structure1');
    $psflight_list['data']['flight_data_list']['journey_list'][0] = $pflight_list;
    return $psflight_list;
  }
  function update_payment_record($book_id, $payment) {
    $cond ['app_reference'] = $book_id;
    
    $data ['customer_payment_details'] = $payment;
    $CI = & get_instance ();
    
    $CI->custom_db->update_record ( 'flight_booking_transaction_details', $data, $cond );
  } 
  public function get_sabre_meal($value)
  {
    $meal_desc = '';
    //echo $value;die;
    switch ($value) {
      case 'S':
        $meal_desc="SNACK";
        break;
      case 'L':
        $meal_desc="LUNCH";
        break;
      case 'B':
        $meal_desc="BREAK FAST";
        break;
      case 'D':
        $meal_desc="DINNER";
        break;
      case 'R':
        $meal_desc="Refreshment";
        break;
      case 'M':
        $meal_desc="MEALS";
        break;
      
      default:
        $meal_desc="No Meals Information Avaiable";
        break;
    }
  }
  public function group_cancellation_passenger_ticket_id($app_reference, $value)
  { 
    $data=$app_reference['booking_details'][0];  
    $app_reference=$data['app_reference'];
    $value=$data['pnr'];
   //echo "sabre".$value;die;
   $SessionCreateRQ_RS = $this->SessionCreateRQ_c($value);
   $mypdata = $this->parse_session_create_response($SessionCreateRQ_RS);
   $TravelItineraryReadRQ_RS   = $this->TravelItineraryReadRQ_c($value,"TravelItineraryReadLLSRQ");
            $OTA_CancelRQ_RS      = $this->OTA_CancelRQ($value, "OTA_CancelLLSRQ");
            $OTA_EndTransactionRQ_RS  = $this->EndTransactionRQFinal($BinarySecurityToken,'Airliners',$value, $app_reference);
            $this->SessionCloseRQ_c($value);
            if(isset($OTA_CancelRQ_RS['OTA_CancelRS'])){
              $response   = $this->CI->xml_to_array->XmlToArray($OTA_CancelRQ_RS['OTA_CancelRS']);
             //debug($response['soap-env:Body']['OTA_CancelRS']['stl:ApplicationResults']['@attributes']['status']);exit();
              if ($response['soap-env:Body']['OTA_CancelRS']['stl:ApplicationResults']['@attributes']['status'] == "Complete") {
                $this->CI->flight_model->update_ticket_cancellation_details($app_reference);
                return true;
              }
              return false;
              }   
  }
  function TravelItineraryReadRQ_c($pnr, $Action = 'TravelItineraryReadLLSRQ'){
    error_reporting(0);
  $psdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
        $psdata['timetolive']   = gmdate("Y-m-d\TH-i-s\Z");

    $TravelItineraryReadRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service>TravelItineraryReadLLSRQ</eb:Service>
                          <eb:Action>".$Action."</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$psdata['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$psdata['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                    <TravelItineraryReadRQ Version='2.2.0' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
                      <MessagingDetails>
                       <Transaction Code='PNR'/>
                      </MessagingDetails>
                      <UniqueID ID='" . $pnr . "'/>
                    </TravelItineraryReadRQ>                    
                  </soap-env:Body>
              </soap-env:Envelope>";
   // header("Content-type: text/xml");print_r($TravelItineraryReadRQ);die;
    $TravelItineraryReadRS = $this->flight_processRequest($TravelItineraryReadRQ);    
   // header("Content-type: text/xml");print_r($TravelItineraryReadRS);die;
    //$res = SessionCloseRQ($BinarySecurityToken);
    $TravelItineraryReadRQ_RS = array(
      'TravelItineraryReadRQ' => $TravelItineraryReadRQ,
      'TravelItineraryReadRS' => $TravelItineraryReadRS
    );
    $date = date('Ymd_His');
    //$path = $_SERVER['DOCUMENT_ROOT'] . "/HorizonTravel/logs.sabre/TravelItineraryReadRQ_".$date.".xml";
    $path = "/cancel_logs/".$parent_pnr."/TravelItineraryReadRQ_".$date.".xml";      
    $fp = fopen($path,"wb");fwrite($fp,$TravelItineraryReadRQ);fclose($fp);

    // $path = $_SERVER['DOCUMENT_ROOT'] . "/HorizonTravel/logs.sabre/TravelItineraryReadRS_".$date.".xml";
    $path = "/cancel_logs/".$parent_pnr."/TravelItineraryReadRS_".$date.".xml";          
    $fp = fopen($path,"wb");fwrite($fp,$TravelItineraryReadRS);fclose($fp);
    return $TravelItineraryReadRQ_RS;
}
  public function TravelItineraryReadRQ($pnr, $Action = 'TravelItineraryReadLLSRQ'){
    error_reporting(0);
        $psdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
        $psdata['timetolive']   = gmdate("Y-m-d\TH-i-s\Z");
      $TravelItineraryReadRQ = "<?xml version='1.0' encoding='utf-8'?>
                  <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                      <soap-env:Header>
                          <eb:MessageHeader xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                              <eb:From>
                                  <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                              </eb:From>
                              <eb:To>
                                  <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                              </eb:To>
                              <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                              <eb:Service>TravelItineraryReadLLSRQ</eb:Service>
                              <eb:Action>".$Action."</eb:Action>
                              <eb:CPAID>".$this->ipcc."</eb:CPAID>
                              <eb:MessageData>
                                  <eb:MessageId>".$this->message_id."</eb:MessageId>
                                  <eb:Timestamp>".$psdata['timestamp']."</eb:Timestamp>
                                  <eb:TimeToLive>".$psdata['timetolive']."</eb:TimeToLive>
                              </eb:MessageData>
                          </eb:MessageHeader>
                          <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                              <wsse:UsernameToken>
                                  <wsse:Username>".$this->UserName."</wsse:Username>
                                  <wsse:Password>".$this->Password."</wsse:Password>
                                  <Organization>".$this->ipcc."</Organization>
                                  <Domain>Default</Domain>
                              </wsse:UsernameToken>
                              <wsse:BinarySecurityToken>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                          </wsse:Security>
                      </soap-env:Header>
                      <soap-env:Body>
                        <TravelItineraryReadRQ Version='2.2.0' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
                          <MessagingDetails>
                           <Transaction Code='PNR'/>
                          </MessagingDetails>
                          <UniqueID ID='" . $pnr . "'/>
                        </TravelItineraryReadRQ>                    
                      </soap-env:Body>
                  </soap-env:Envelope>";
               //   echo $TravelItineraryReadRQ;die;
        $TravelItineraryReadRS = $this->flight_processRequest($TravelItineraryReadRQ);    
        $TravelItineraryReadRQ_RS = array(
                        'TravelItineraryReadRQ' => $TravelItineraryReadRQ,
                        'TravelItineraryReadRS' => $TravelItineraryReadRS
                      );   
        $XmlReqFileName = 'TravelItineraryReadRQ'.$pnr; $XmlResFileName = 'TravelItineraryReadRS'.$pnr;
        $path   = "/cancel_logs/".$XmlReqFileName.".xml";
      $fp   = fopen($path,"wb");fwrite($fp,$TravelItineraryReadRQ);fclose($fp);
      
      $path   =  "/cancel_logs/".$XmlResFileName.".xml";
      $fp   = fopen($path,"wb");fwrite($fp,$TravelItineraryReadRS);fclose($fp);  
        return $TravelItineraryReadRQ_RS;
    }
    public function OTA_CancelRQ($parent_pnr, $Action = 'OTA_CancelLLSRQ'){
      error_reporting(0);
      $psdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
      $psdata['timetolive']   = gmdate("Y-m-d\TH-i-s\Z");
       $OTA_CancelRQ = "<?xml version='1.0' encoding='utf-8'?>
                  <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                      <soap-env:Header>
                          <eb:MessageHeader xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                              <eb:From>
                                  <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                              </eb:From>
                              <eb:To>
                                  <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                              </eb:To>
                              <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                              <eb:Service>OTA_CancelLLSRQ</eb:Service>
                              <eb:Action>".$Action."</eb:Action>
                              <eb:CPAID>".$this->ipcc."</eb:CPAID>
                              <eb:MessageData>
                                  <eb:MessageId>".$this->message_id."</eb:MessageId>
                                  <eb:Timestamp>".$psdata['timestamp']."</eb:Timestamp>
                                  <eb:TimeToLive>".$psdata['timetolive']."</eb:TimeToLive>
                              </eb:MessageData>
                          </eb:MessageHeader>
                          <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                              <wsse:UsernameToken>
                                  <wsse:Username>".$this->UserName."</wsse:Username>
                                  <wsse:Password>".$this->Password."</wsse:Password>
                                  <Organization>".$this->ipcc."</Organization>
                                  <Domain>Default</Domain>
                              </wsse:UsernameToken>
                              <wsse:BinarySecurityToken>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                          </wsse:Security>
                      </soap-env:Header>
                      <soap-env:Body>
                        <OTA_CancelRQ Version='2.0.0' xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
                          <Segment Type='air'/>
                        </OTA_CancelRQ>                                        
                      </soap-env:Body>
                  </soap-env:Envelope>";
                //  echo $OTA_CancelRQ;//die;
        $OTA_CancelRS = $this->flight_processRequest($OTA_CancelRQ);  
        //echo $OTA_CancelRS;die;  
        $OTA_CancelRQ_RS = array(
          'OTA_CancelRQ' => $OTA_CancelRQ,
          'OTA_CancelRS' => $OTA_CancelRS
        );
        $final_de = date('Ymd_His')."_".rand(1,10000);
        $XmlReqFileName = 'OTA_CancelRQ'.$final_de; $XmlResFileName = 'OTA_CancelRS'.$final_de;
        
        $path = "/cancel_logs/".$parent_pnr."/".$XmlReqFileName.".xml";
      $fp = fopen($path,"wb");fwrite($fp,$OTA_CancelRQ);fclose($fp);
      
      $path = "/cancel_logs/".$parent_pnr."/".$XmlResFileName.".xml";
      $fp = fopen($path,"wb");fwrite($fp,$OTA_CancelRS);fclose($fp);    
        return $OTA_CancelRQ_RS;
    }
    public function SessionCreateRQ_c($parent_pnr,$Action = 'SessionCreateRQ'){ 
      error_reporting(0);
      $this->set_api_credentials();
      $psdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
      $psdata['timetolive']   = gmdate("Y-m-d\TH-i-s\Z");
      $SessionCreateRQ = "<?xml version='1.0' encoding='utf-8'?>
                <soap-env:Envelope
                    xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                    <soap-env:Header>
                        <eb:MessageHeader
                            xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                            <eb:From>
                                <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                            </eb:From>
                            <eb:To>
                                <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices3.sabre.com</eb:PartyId>
                            </eb:To>
                            <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                            <eb:Service eb:type='SabreXML'>Session</eb:Service>
                            <eb:Action>".$Action."</eb:Action>
                            <eb:CPAID>".$this->ipcc."</eb:CPAID>
                            <eb:MessageData>
                                <eb:MessageId>".$this->message_id."</eb:MessageId>
                                <eb:Timestamp>".$psdata['timestamp']."</eb:Timestamp>
                                <eb:TimeToLive>".$psdata['timetolive']."</eb:TimeToLive>
                            </eb:MessageData>
                        </eb:MessageHeader>
                        <wsse:Security
                            xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                            <wsse:UsernameToken>
                                <wsse:Username>".$this->UserName."</wsse:Username>
                                <wsse:Password>".$this->Password."</wsse:Password>
                                <Organization>".$this->ipcc."</Organization>
                                <Domain>Default</Domain>
                            </wsse:UsernameToken>
                        </wsse:Security>
                    </soap-env:Header>
                    <soap-env:Body>
                        <SessionCreateRQ>
                            <POS>
                                <Source PseudoCityCode='".$this->ipcc."' />
                            </POS>
                        </SessionCreateRQ>
                    </soap-env:Body>
                </soap-env:Envelope>";
      $SessionCreateRS = $this->flight_processRequest($SessionCreateRQ); 
      $SessionCreateRQ_RS = array(
        'SessionCreateRQ' => $SessionCreateRQ,
        'SessionCreateRS' => $SessionCreateRS
      ); 
       //debug($SessionCreateRQ_RS);die;
        $folder_path = "/cancel_logs/".$parent_pnr; 
        if (!file_exists($folder_path)){
           mkdir("/cancel_logs/".$parent_pnr, 0777);
        }       
        $path = "/cancel_logs/".$parent_pnr."/SessionCreateRQ.xml";
        $fp = fopen($path,"wb");fwrite($fp,$SessionCreateRQ);fclose($fp);
        
        $path = "/cancel_logs/".$parent_pnr."/SessionCreateRS.xml";
        $fp = fopen($path,"wb");fwrite($fp,$SessionCreateRS);fclose($fp);
      return $SessionCreateRQ_RS;
  }
  public function SessionCloseRQ_c($search_id, $type = 'SEARCH',$Action = 'SessionCloseRQ'){
    error_reporting(0);
    $psdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
      $psdata['timetolive']   = gmdate("Y-m-d\TH-i-s\Z");
    $SessionCloseRQ = "<?xml version='1.0' encoding='utf-8'?>
                        <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                            <soap-env:Header>
                                <eb:MessageHeader xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                                    <eb:From>
                                        <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                                    </eb:From>
                                    <eb:To>
                                        <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices3.sabre.com</eb:PartyId>
                                    </eb:To>
                                    <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                                    <eb:Service>SessionCloseRQ</eb:Service>
                                    <eb:Action>".$Action."</eb:Action>
                                    <eb:CPAID>".$this->ipcc."</eb:CPAID>
                                    <eb:MessageData>
                                        <eb:MessageId>".$this->message_id."</eb:MessageId>
                                        <eb:Timestamp>".$psdata['timestamp']."</eb:Timestamp>
                                        <eb:TimeToLive>".$psdata['timetolive']."</eb:TimeToLive>
                                    </eb:MessageData>
                                </eb:MessageHeader>
                                <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                                  <wsse:BinarySecurityToken valueType='String' EncodingType='wsse:Base64Binary'>".$this->BinarySecurityToken."</wsse:BinarySecurityToken>
                                </wsse:Security>
                            </soap-env:Header>
                            <soap-env:Body>
                                <SessionCloseRQ>
                                    <POS>
                                        <Source PseudoCityCode='".$this->ipcc."' />
                                    </POS>
                                </SessionCloseRQ>
                            </soap-env:Body>
                        </soap-env:Envelope>";
    $SessionCloseRS = $this->flight_processRequest($SessionCloseRQ);
    $SessionCloseRQ_RS = array(
                  'SessionCloseRQ' => $SessionCloseRQ,
                  'SessionCloseRS' => $SessionCloseRS
                );
    if($type == "SEARCH"){}else{
            
    $path = "/cancel_logs/".$search_id."/SessionCloseRQ.xml";
    $fp = fopen($path,"wb");fwrite($fp,$SessionCloseRQ);fclose($fp);
    
    $path = "/cancel_logs/".$search_id."/SessionCloseRS.xml";
    $fp = fopen($path,"wb");fwrite($fp,$SessionCloseRS);fclose($fp);
  }
  return $SessionCloseRQ_RS;
}

  public function reschedule($flight_data)
  {
    //echo'sabre';debug($flight_data);
    $SessionCreateRQ_RS = $this->SessionCreateRQ($book_id,'BOOK');
   // debug($SessionCreateRQ_RS);die;
      $mypdata = $this->parse_session_create_response($SessionCreateRQ_RS);
     // debug($mypdata);die;
      foreach ($flight_data['booking_itinerary_details'] as $key => $value) {
       //  echo'sabre';debug($value);die;
        //echo'sabre';debug(date("m-dTH:i",strtotime($value["departure_datetime"])));die;
                  $requrst = '<OTA_AirScheduleRQ Version="2.1.0" xmlns="http://webservices.sabre.com/sabreXML/2011/10" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                  <OriginDestinationInformation>
                <FlightSegment DepartureDateTime="'.date("m-d",strtotime($value["departure_datetime"])).'T'.date("H:i",strtotime($value["departure_datetime"])).'">
                    <DestinationLocation LocationCode="'.$value["to_airport_code"].'"/>
                  <OriginLocation LocationCode="'.$value["from_airport_code"].'"/>
                 </FlightSegment>
            </OriginDestinationInformation>
          </OTA_AirScheduleRQ>';
          $psdata['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
          $psdata['timetolive']     = gmdate("Y-m-d\TH-i-s\Z");
          $main_request = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                            <SOAP-ENV:Header>
                              <m:MessageHeader xmlns:m="http://www.ebxml.org/namespaces/messageHeader">
                                <m:From>
                                  <m:PartyId type="urn:x12.org:IO5:01">'.$this->sabre_myemail.'</m:PartyId>
                                </m:From>
                                <m:To>
                                  <m:PartyId type="urn:x12.org:IO5:01">webservices.sabre.com</m:PartyId>
                                </m:To>
                                <m:CPAId>'.$this->ipcc.'</m:CPAId>
                                <m:ConversationId>'.$this->conversation_id.'</m:ConversationId>
                                <m:Service m:type="OTA">OTA_AirScheduleLLSRQ</m:Service>
                                <m:Action>OTA_AirScheduleLLSRQ</m:Action>
                                <m:MessageData>
                                  <m:MessageId>'.$this->message_id.'</m:MessageId>
                                  <m:Timestamp>'.$psdata['timestamp'].'</m:Timestamp>
                                  <m:TimeToLive>'.$psdata['timetolive'].'</m:TimeToLive>
                                </m:MessageData>
                                <m:DuplicateElimination/>
                                <m:Description>OTA_AirScheduleLLSRQ</m:Description>
                              </m:MessageHeader>
                              <wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/12/secext">
                                <wsse:BinarySecurityToken valueType="String" EncodingType="wsse:Base64Binary">'.$this->BinarySecurityToken.'</wsse:BinarySecurityToken>
                              </wsse:Security>
                            </SOAP-ENV:Header>
                            <SOAP-ENV:Body>
                             '.$requrst.'
                            </SOAP-ENV:Body>
                          </SOAP-ENV:Envelope>';
                        //  echo $main_request;//die;
          $response[$key] = $this->flight_processRequest($main_request);//die;
          //echo $response[$key];die;
          $formatted_response[$key] = $this->CI->xml_to_array->XmlToArray($response[$key]);
          //debug($formatted_response[$key]);die;
      }
      return $formatted_response;
  }
  function get_fare_details($params){
      $sci = &get_instance();
      $BinarySecurityToken = $sci->session->userdata('security_token');
      $search_id           = $params['search_id'];
      $segData             = $params['data_access_key']['AirItinerary'];
      $origin_Dest         = $segData['OriginDestinationOptions']['OriginDestinationOption'];
      if(!isset($origin_Dest[0])){
          $originDest[0] = $origin_Dest;
      }else{
          $originDest   = $origin_Dest;
      }
      $arrFlight_Segment = $originDest[0]['FlightSegment'];
      if(!isset($arrFlight_Segment[0])){
          $arrFlightSegment[0] = $arrFlight_Segment;
          $strCnt = 0;
      }else{
          $arrFlightSegment = $arrFlight_Segment;
          $strCnt = count($arrFlightSegment)-1;
      }
      $segmentData['OriginLocation']      = $arrFlightSegment[0]['DepartureAirport']['attr']['LocationCode'];
      $segmentData['DestinationLocation'] = $arrFlightSegment[$strCnt]['ArrivalAirport']['attr']['LocationCode'];
      $segmentData['dateOfDeparture']     = $arrFlightSegment[0]['attr']['DepartureDateTime'];
      $segmentData['MarketingAirline']    = $arrFlightSegment[0]['MarketingAirline']['attr']['Code'];
      //debug($segmentData);exit();

      $arrRules = $this->FareRuleRQ($segmentData, $BinarySecurityToken, $search_id, $Action = 'FareLLSRQ');
      $arrRetData['status'] = SUCCESS_STATUS;
      $arrRetData['resdata']= $arrRules;
      return $arrRetData;
  }
public function FareRuleRQ($segment_data1, $BinarySecurityToken, $search_id, $Action = 'FareLLSRQ'){
 
    $segment_data       = $segment_data1;
    $this->set_api_credentials();
    /*$this->system   = $engine_system;
    $this->details  = $this->CI->config->item ( 'sabre_flight_' . $engine_system );
    $this->Url      = $this->details ['api_url'];
    $this->UserName = $this->details ['username'];
    $this->Password = $this->details ['password'];
    $this->ipcc     = $this->details ['ipcc'];
    $this->sabre_myemail     = $this->details ['sabre_email'];
    $this->conversation_id  = time().$this->sabre_myemail;
    $this->message_id     = "mid:".time().$this->sabre_myemail;
    debug($segment_data);exit('sabre_lib');*/
    $DepartureAirportCode   = $segment_data['OriginLocation'];
    $ArrivalAirportCode   = $segment_data['DestinationLocation'];
    $DepartureDate      = date('m-d',strtotime($segment_data['dateOfDeparture']));
    $MarketingAirlineCode   = $segment_data['MarketingAirline'];
    $credencials['timestamp']      = gmdate("Y-m-d\TH-i-s\Z");
          $credencials['timetolive']     = gmdate("Y-m-d\TH-i-s\Z");
    // echo '<pre/>';print_r($segment_data);exit;
    $FareRuleRQ = "<?xml version='1.0' encoding='utf-8'?>
              <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap-env:Header>
                      <eb:MessageHeader
                          xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                          <eb:From>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                          </eb:From>
                          <eb:To>
                              <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                          </eb:To>
                          <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                          <eb:Service eb:type='OTA'>FareLLSRQ</eb:Service>
                          <eb:Action>FareLLSRQ</eb:Action>
                          <eb:CPAID>".$this->ipcc."</eb:CPAID>
                          <eb:MessageData>
                              <eb:MessageId>".$this->message_id."</eb:MessageId>
                              <eb:Timestamp>".$credencials['timestamp']."</eb:Timestamp>
                              <eb:TimeToLive>".$credencials['timetolive']."</eb:TimeToLive>
                          </eb:MessageData>
                      </eb:MessageHeader>
                      <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                          <wsse:UsernameToken>
                              <wsse:Username>".$this->UserName."</wsse:Username>
                              <wsse:Password>".$this->Password."</wsse:Password>
                              <Organization>".$this->ipcc."</Organization>
                              <Domain>Default</Domain>
                          </wsse:UsernameToken>
                          <wsse:BinarySecurityToken>".$BinarySecurityToken."</wsse:BinarySecurityToken>
                      </wsse:Security>
                  </soap-env:Header>
                  <soap-env:Body>
                      <FareRQ xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' Version='2.0.0'>
                        <OptionalQualifiers>
                           <FlightQualifiers>
                <VendorPrefs>
                  <Airline Code='".$MarketingAirlineCode."'/>
                </VendorPrefs>
              </FlightQualifiers>
                          <TimeQualifiers>
                            <TravelDateOptions Start='".$DepartureDate."'/>
                          </TimeQualifiers>
                        </OptionalQualifiers>
                        <OriginDestinationInformation>
                          <FlightSegment>
                             <DestinationLocation LocationCode='".$ArrivalAirportCode."'/>                         
                             <OriginLocation LocationCode='".$DepartureAirportCode."'/>
                          </FlightSegment>
                        </OriginDestinationInformation>
                      </FareRQ>
                  </soap-env:Body>
              </soap-env:Envelope>";  
            //  echo $FareRuleRQ;die;
        $fp = fopen("../b2b2b/xml_logs/Flight/sabre/search_logs/FareRuleRQ_".$search_id."xml", 'a+');
        fwrite($fp, $FareRuleRQ);
        fclose($fp); 
        $FareRuleRS   = $this->flight_processRequest($FareRuleRQ);   
        $FareRuleRQ_RS  = array(
              'FareRuleRQ' => $FareRuleRQ,
              'FareRuleRS' => $FareRuleRS
            );

        $fp = fopen("../b2b2b/xml_logs/Flight/sabre/search_logs/FareRuleRS_".$search_id.".xml", 'a+');
        fwrite($fp, $FareRuleRS); 
        fclose($fp);

   // debug($FareRuleRS);die;
    $Rules = array();
    if (!empty($FareRuleRS) && substr($FareRuleRS, 0, 5) == "<?xml") {
    $doc = new DOMDOcument;
    $doc->loadxml($FareRuleRS);
    $FareBasis = $doc->getElementsByTagName("FareBasis");
    foreach ($FareBasis as $fare) {
      $FareBasisCode = $fare->getAttribute('Code');
      $OTA_AirRulesRQ = "<?xml version='1.0' encoding='utf-8'?>
            <soap-env:Envelope xmlns:soap-env='http://schemas.xmlsoap.org/soap/envelope/'>
              <soap-env:Header>
                <eb:MessageHeader
                  xmlns:eb='http://www.ebxml.org/namespaces/messageHeader'>
                  <eb:From>
                    <eb:PartyId eb:type='urn:x12.org.IO5:01'>".$this->sabre_myemail."</eb:PartyId>
                  </eb:From>
                  <eb:To>
                    <eb:PartyId eb:type='urn:x12.org.IO5:01'>webservices.sabre.com</eb:PartyId>
                  </eb:To>
                  <eb:ConversationId>".$this->conversation_id."</eb:ConversationId>
                  <eb:Service eb:type='OTA'>OTA_AirRulesLLSRQ</eb:Service>
                  <eb:Action>OTA_AirRulesLLSRQ</eb:Action>
                  <eb:CPAID>".$this->ipcc."</eb:CPAID>
                  <eb:MessageData>
                    <eb:MessageId>".$this->message_id."</eb:MessageId>
                    <eb:Timestamp>".$credencials['timestamp']."</eb:Timestamp>
                    <eb:TimeToLive>".$credencials['timetolive']."</eb:TimeToLive>
                  </eb:MessageData>
                </eb:MessageHeader>
                <wsse:Security xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>
                  <wsse:UsernameToken>
                    <wsse:Username>".$this->UserName."</wsse:Username>
                    <wsse:Password>".$this->Password."</wsse:Password>
                    <Organization>".$this->ipcc."</Organization>
                    <Domain>Default</Domain>
                  </wsse:UsernameToken>
                  <wsse:BinarySecurityToken>".$BinarySecurityToken."</wsse:BinarySecurityToken>
                </wsse:Security>
              </soap-env:Header>
              <soap-env:Body>
                <OTA_AirRulesRQ xmlns='http://webservices.sabre.com/sabreXML/2011/10' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' ReturnHostCommand='true' Version='2.2.0'>
                <OriginDestinationInformation>
                  <FlightSegment DepartureDateTime='" . $DepartureDate . "'>
                    <DestinationLocation LocationCode='".$ArrivalAirportCode."'/>
                    <MarketingCarrier Code='" . $MarketingAirlineCode . "'/>
                    <OriginLocation LocationCode='".$DepartureAirportCode."'/>
                  </FlightSegment>
                </OriginDestinationInformation>
                <RuleReqInfo>
                  <FareBasis Code='" . $FareBasisCode ."'/>
                </RuleReqInfo>
                </OTA_AirRulesRQ>
              </soap-env:Body>
            </soap-env:Envelope>";
      
        $fp = fopen("..b2b2b/xml_logs/Flight/sabre/search_logs/OTA_AirRulesRQ".$search_id.".xml", 'a+');
        fwrite($fp, $OTA_AirRulesRQ);
        fclose($fp);

      $OTA_AirRulesRS   = $this->flight_processRequest($OTA_AirRulesRQ);   
      $OTA_AirRulesRQ_RS  = array(
                  'OTA_AirRulesRQ' => $OTA_AirRulesRQ,
                  'OTA_AirRulesRS' => $OTA_AirRulesRS
                );
        $fp = fopen("..b2b2b/xml_logs/Flight/sabre/search_logs/OTA_AirRulesRS_".$search_id."xml", 'a+');
        fwrite($fp, $OTA_AirRulesRS);
        fclose($fp);

      if (!empty($OTA_AirRulesRS) && substr($OTA_AirRulesRS, 0, 5) == "<?xml") {
        $doc = new DOMDOcument;
        $doc->loadxml($OTA_AirRulesRS);
        $Paragraphs = $doc->getElementsByTagName("Paragraph");
        $i=0;
        if($Paragraphs->length > 0) {
          foreach ($Paragraphs as $Paragraph) {         
            $Title        = $Paragraph->getAttribute('Title');            
            $Text         = $Paragraph->getElementsByTagName("Text");
            $Text         = $Text->item(0)->nodeValue;
            $Rules[$i]['Title'] = $Title;
            $Rules[$i]['Text']  = $Text;
            $i++;
          }
        }else {
          $Rules = array();
        }
        }else{
        $Rules = array();
        }
      if(isset($Rules[0]) && $Rules[0] != ''){ break; }
    }
    }else{
      $Rules = array();
    }    
    return $Rules;
}

    public function read_token($token_key)
    {
        //debug($token_key); die;
        //$token_key = explode(DB_SAFE_SEPARATOR, unserialized_data($token_key));
        $token_key = explode(DB_SAFE_SEPARATOR, ($token_key));
        if (valid_array($token_key) == true) {
            $file = DOMAIN_TMP_UPLOAD_DIR.$token_key[0].'.json';//File name
            //debug($file); die;
            //$index = $token_key[1]; // access key
            $index = $token_key[0]; // access key
            if (file_exists($file) == true) {
                $token_content = file_get_contents($file);
                
                if (empty($token_content) == false) {
                    $token = json_decode($token_content, true);
                    if (valid_array($token) == true && isset($token[$index]) == true) {
                        return $token[$index];
                    } else {
                        return false;
                        echo 'Token data not found';
                        exit;
                    }
                } else {
                    return false;
                    echo 'Invalid File access';
                    exit;
                }
            } else {
                return false;
                echo 'Invalid Token access';
                exit;
            }
        } else {
            return false;
            echo 'Invalid Token passed';
            exit;
        }
    }

    /**
     * Return unserialized data
     * @param array $token      serialized data having token
     * @param array $token_key  serialized data having token key
     */
    public function unserialized_token($token, $token_key)
    {
        $response['data'] = array();
        $response['status'] = true;
        foreach($token as $___k => $___v) {
            $tmp_tkn = $this->read_token($___v);
            if ($tmp_tkn != false) {
                $response['data']['token'][$___k] = $tmp_tkn;
                $response['data']['token_key'] = $token_key[$___k];
            } else {
                $response['data']['token'][$___k] = false;
            }

            if ($response['status'] == true) {
                if ($response['data']['token'][$___k] == false) {
                    $response['status'] = false;
                }
            }
        }

        return $response;
    }

    public function preferred_currency_fare_details_tp($fare_details, $module='', $currency_obj, $default_currency = '')
    {
        //echo "currency_obj ";debug($currency_obj);//exit;
        $FareDetails = array();
    
        if($module == 'b2b') 
        {
            $FareDetails['b2b_PriceDetails']['BaseFare']           = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['BaseFare']));
            $FareDetails['b2b_PriceDetails']['_CustomerBuying']    = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_CustomerBuying']));
            $FareDetails['b2b_PriceDetails']['_AgentBuying']       = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_AgentBuying']));
            $FareDetails['b2b_PriceDetails']['_AdminBuying']       = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_AdminBuying']));
            $FareDetails['b2b_PriceDetails']['_Markup']            = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_Markup']));
            $FareDetails['b2b_PriceDetails']['_AgentMarkup']       = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_AgentMarkup']));
            $FareDetails['b2b_PriceDetails']['_AdminMarkup']       = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_AdminMarkup']));
            $FareDetails['b2b_PriceDetails']['_Commission']        = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_Commission']));
            $FareDetails['b2b_PriceDetails']['_tdsCommission']     = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_tdsCommission']));
            $FareDetails['b2b_PriceDetails']['_AgentEarning']      = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_AgentEarning']));
            $FareDetails['b2b_PriceDetails']['_TaxSum']            = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_TaxSum']));
            $FareDetails['b2b_PriceDetails']['_BaseFare']          = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_BaseFare']));
            $FareDetails['b2b_PriceDetails']['_TotalPayable']      = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_TotalPayable']));
            $FareDetails['b2b_PriceDetails']['Currency']    = get_application_currency_preference();
            $FareDetails['b2b_PriceDetails']['CurrencySymbol']  = $currency_obj->get_currency_symbol($currency_obj->to_currency);
        }
        else{

            $FareDetails['b2c_PriceDetails']['BaseFare'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2c_PriceDetails']['BaseFare']));
            $FareDetails['b2c_PriceDetails']['TotalTax'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2c_PriceDetails']['TotalTax']));
            $FareDetails['b2c_PriceDetails']['TotalFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2c_PriceDetails']['TotalFare']));
            $FareDetails['b2c_PriceDetails']['Currency'] =    get_application_currency_preference();
            $FareDetails['b2c_PriceDetails']['CurrencySymbol'] =    $currency_obj->get_currency_symbol($currency_obj->to_currency);
            $FareDetails['b2c_PriceDetails']['Admin_Markup'] =    $fare_details['b2c_PriceDetails']['Admin_Markup'];
        }

        $FareDetails['api_PriceDetails']['Currency'] =    get_application_currency_preference();
        $FareDetails['api_PriceDetails']['BaseFare'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['BaseFare']));
        $FareDetails['api_PriceDetails']['Tax'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['Tax']));
        $FareDetails['api_PriceDetails']['PublishedFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['PublishedFare']));

        $FareDetails['api_PriceDetails']['YQTax'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['YQTax']));
        $FareDetails['api_PriceDetails']['AdditionalTxnFeeOfrd'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['AdditionalTxnFeeOfrd']));
        $FareDetails['api_PriceDetails']['AdditionalTxnFeePub'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['AdditionalTxnFeePub']));
        $FareDetails['api_PriceDetails']['OtherCharges'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['OtherCharges']));
        $FareDetails['api_PriceDetails']['AgentCommission'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['AgentCommission']));
        $FareDetails['api_PriceDetails']['PLBEarned'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['PLBEarned']));
        $FareDetails['api_PriceDetails']['IncentiveEarned'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['IncentiveEarned']));
        $FareDetails['api_PriceDetails']['OfferedFare'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['OfferedFare']));
        $FareDetails['api_PriceDetails']['TdsOnCommission'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['TdsOnCommission']));
        $FareDetails['api_PriceDetails']['TdsOnPLB'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['TdsOnPLB']));
        $FareDetails['api_PriceDetails']['TdsOnIncentive'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['TdsOnIncentive']));
         $FareDetails['api_PriceDetails']['ServiceFee'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['ServiceFee']));

       // echo "FareDetails_new ";debug($FareDetails);
        return $FareDetails;
    }

    public function convert_token_to_application_currency($token, $currency_obj, $module)
    {
        $token_details = $token;
        // debug($token);die;
        $token = array();
        $application_default_currency = admin_base_currency();
        foreach($token_details as $tk => $tv) {
            $token[$tk] = $tv;
            $temp_fare_details = $tv['FareDetails'];
            //Fare Details
            $FareDetails = array();
            if($module == 'b2c') {
            $PriceDetails = $temp_fare_details[$module.'_PriceDetails'];
            
            $FareDetails['b2c_PriceDetails']['BaseFare'] =          get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['BaseFare']));
            $FareDetails['b2c_PriceDetails']['TotalTax'] =          get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['TotalTax']));
            $FareDetails['b2c_PriceDetails']['TotalFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['TotalFare']));
            $FareDetails['b2c_PriceDetails']['Currency'] =          $application_default_currency;
            $FareDetails['b2c_PriceDetails']['CurrencySymbol'] =    $currency_obj->get_currency_symbol($currency_obj->to_currency);
            } else if($module == 'b2b') {
                $PriceDetails = $temp_fare_details[$module.'_PriceDetails'];
                
                $FareDetails['b2b_PriceDetails']['BaseFare'] =          get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['BaseFare']));
                $FareDetails['b2b_PriceDetails']['_CustomerBuying'] =   get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_CustomerBuying']));
                $FareDetails['b2b_PriceDetails']['_AgentBuying'] =      get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AgentBuying']));
                $FareDetails['b2b_PriceDetails']['_AdminBuying'] =      get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AdminBuying']));
                $FareDetails['b2b_PriceDetails']['_Markup'] =           get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_Markup']));
                $FareDetails['b2b_PriceDetails']['_AgentMarkup'] =      get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AgentMarkup']));
                $FareDetails['b2b_PriceDetails']['_AdminMarkup'] =      get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AdminMarkup']));
                $FareDetails['b2b_PriceDetails']['_Commission'] =       get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_Commission']));
                $FareDetails['b2b_PriceDetails']['_tdsCommission'] =    get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_tdsCommission']));
                $FareDetails['b2b_PriceDetails']['_AgentEarning'] =     get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AgentEarning']));
                $FareDetails['b2b_PriceDetails']['_TaxSum'] =           get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_TaxSum']));
                $FareDetails['b2b_PriceDetails']['_BaseFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_BaseFare']));
                $FareDetails['b2b_PriceDetails']['_TotalPayable'] =     get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_TotalPayable']));
                $FareDetails['b2b_PriceDetails']['Currency'] =          $application_default_currency;
                $FareDetails['b2b_PriceDetails']['CurrencySymbol'] =    $currency_obj->get_currency_symbol($currency_obj->to_currency);
            }
            $FareDetails['api_PriceDetails'] = $this->preferred_currency_fare_object($temp_fare_details['api_PriceDetails'], $currency_obj, $application_default_currency);
            $token[$tk]['FareDetails'] = $FareDetails;
            //Passenger Breakdown
            $token[$tk]['PassengerFareBreakdown'] = $this->preferred_currency_paxwise_breakup_object($tv['PassengerFareBreakdown'], $currency_obj);
        }
        return $token;
    }

    /**
     * Jaganath
     * Passenger Fare Breakdown details
     * Converts the API Currency to Preferred Currency
     * @param unknown_type $FareDetails
     */
    public function preferred_currency_paxwise_breakup_object($fare_details, $currency_obj)
    {
        $PassengerFareBreakdown = array();
        foreach($fare_details as $k => $v) {
            $PassengerFareBreakdown[$k] = $v;
            $PassengerFareBreakdown[$k]['BaseFare'] = get_converted_currency_value($currency_obj->force_currency_conversion($v['BaseFare']));
        }
        return $PassengerFareBreakdown;
    }

    /**
     * Jaganath
     * Fare Details
     * Converts the API Currency to Preferred Currency
     * @param unknown_type $FareDetails
     */
    private function preferred_currency_fare_object($fare_details, $currency_obj, $default_currency = '')
    {
        
        $FareDetails = array();
        $FareDetails['Currency'] =              empty($default_currency) == false ? $default_currency : get_application_currency_preference();
        $FareDetails['BaseFare'] =              get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['BaseFare']));
        $FareDetails['Tax'] =                   get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['Tax']));
        $FareDetails['YQTax'] =                 get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['YQTax']));
        $FareDetails['AdditionalTxnFeeOfrd'] =  get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['AdditionalTxnFeeOfrd']));
        $FareDetails['AdditionalTxnFeePub'] =   get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['AdditionalTxnFeePub']));
        $FareDetails['OtherCharges'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['OtherCharges']));
        if(!empty($fare_details['Discount'])){
          $FareDetails['Discount'] =              get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['Discount']));
        }
        $FareDetails['PublishedFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['PublishedFare']));
        $FareDetails['AgentCommission'] =       get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['AgentCommission']));
        $FareDetails['PLBEarned'] =             get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['PLBEarned']));
        $FareDetails['IncentiveEarned'] =       get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['IncentiveEarned']));
        $FareDetails['OfferedFare'] =           get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['OfferedFare']));
        $FareDetails['TdsOnCommission'] =       get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['TdsOnCommission']));
        $FareDetails['TdsOnPLB'] =              get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['TdsOnPLB']));
        $FareDetails['TdsOnIncentive'] =        get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['TdsOnIncentive']));
        $FareDetails['ServiceFee'] =            get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['ServiceFee']));
        return $FareDetails;
    }

    function save_booking($app_booking_id, $book_params, $currency_obj, $module='b2c')
    {   

        // echo 'book_params';debug($book_params);
        // echo 'currency_obj';debug($currency_obj);
        // echo 'module';debug($module);die;
        //Need to return following data as this is needed to save the booking fare in the transaction details
        $response['fare'] = $response['domain_markup'] = $response['level_one_markup'] = 0;
        $book_total_fare = array();
        $book_domain_markup = array();
        $book_level_one_markup = array();
        $master_transaction_status = 'BOOKING_INPROGRESS';
        $master_search_id = $book_params['search_id'];

        $domain_origin = get_domain_auth_id();
        $app_reference = $app_booking_id;
        $booking_source = $book_params['booking_source'];

        //PASSENGER DATA UPDATE
        $total_pax_count = count($book_params['passenger_type']);
        $pax_count = $total_pax_count;
        
        //PREFERRED TRANSACTION CURRENCY AND CURRENCY CONVERSION RATE 
        $transaction_currency = get_application_currency_preference();
        $application_currency = admin_base_currency();
        $currency_conversion_rate = $currency_obj->transaction_currency_conversion_rate();
        //********************** only for calculation
        $safe_search_data = $this->search_data($master_search_id);
        $safe_search_data = $safe_search_data['data'];
        $safe_search_data['is_domestic_one_way_flight'] = false;
        $from_to_trip_type = $safe_search_data['trip_type'];
        if(strtolower($from_to_trip_type) == 'multicity') {
            $from_loc = $safe_search_data['from'][0];
            $to_loc = end($safe_search_data['to']);
            $journey_from = $safe_search_data['from_city'][0];
            $journey_to = end($safe_search_data['to_city']);
        } else {
            $from_loc = $safe_search_data['from'];
            $to_loc = $safe_search_data['to'];
            $journey_from = isset($safe_search_data['from_city'])?$safe_search_data['from_city']:$safe_search_data['from'];
            $journey_to = isset($safe_search_data['to_city'])?$safe_search_data['to_city']:$safe_search_data['to'];
        }
        //debug($safe_search_data);
        //echo $journey_from;exit();
        $safe_search_data['is_domestic_one_way_flight'] = $GLOBALS['CI']->flight_model->is_domestic_flight($from_loc, $to_loc);
        if ($safe_search_data['is_domestic_one_way_flight'] == false && strtolower($from_to_trip_type) == 'return') {
            $multiplier = $pax_count * 2;//Multiply with 2 for international round way
        } else if(strtolower($from_to_trip_type) == 'multicity'){
            $multiplier = $pax_count * count($safe_search_data['from']);
        } else {
            $multiplier = $pax_count;
        }
        //********************* only for calculation
        $token = $book_params['token']['token'];
        // debug($token);die;
        $master_booking_source = array();
        $currency = $currency_obj->to_currency;
        $deduction_cur_obj  = clone $currency_obj;
        //Storing Flight Details - Every Segment can repeate also
        foreach ($token as $token_index => $token_value) {

            $segment_details = $token_value['SegmentDetails'];
            //debug($segment_details);die;
            $segment_summary = $token_value['SegmentSummary'];
            $Fare = $token_value['FareDetails']['api_PriceDetails'];
            $tmp_domain_markup = 0;
            $tmp_level_one_markup = 0;
            $itinerary_price    = $Fare['BaseFare'];
            //Calculation is different for b2b and b2c
            //Specific Markup Config
            $specific_markup_config = array();
            $specific_markup_config = $this->get_airline_specific_markup_config($segment_details);//Get the Airline code for setting airline-wise markup
            // debug($specific_markup_config);die;

            $admin_commission = 0;
            $agent_commission = 0;
            $admin_tds = 0;
            $agent_tds = 0;
            $core_agent_commision = ($Fare['PublishedFare']-$Fare['OfferedFare']);
            $commissionable_fare = $Fare['PublishedFare'];
            if ($module == 'b2c') {             
                $trans_total_fare = $this->total_price($Fare, false, $currency_obj);
                $markup_total_fare  = $currency_obj->get_currency($trans_total_fare, true, false, true, $multiplier, $specific_markup_config);
                $ded_total_fare     = $deduction_cur_obj->get_currency($trans_total_fare, true, true, false, $multiplier, $specific_markup_config);
                $admin_markup = roundoff_number($markup_total_fare['default_value']-$ded_total_fare['default_value']);
                $agent_markup = roundoff_number($ded_total_fare['default_value']-$trans_total_fare);
                $admin_commission = $core_agent_commision;
                $agent_commission = 0;
                
            } else {
                //B2B Calculation
                //Markup
                $trans_total_fare = $Fare['PublishedFare'];
                $markup_total_fare  = $currency_obj->get_currency($trans_total_fare, true, true, true, $multiplier, $specific_markup_config);
                $ded_total_fare     = $deduction_cur_obj->get_currency($trans_total_fare, true, false, true, $multiplier, $specific_markup_config);
                $admin_markup = abs($markup_total_fare['default_value']-$ded_total_fare['default_value']);
                $agent_markup = roundoff_number($ded_total_fare['default_value']-$trans_total_fare);
                //Commission
                $this->commission = $currency_obj->get_commission();
                $AgentCommission = $this->calculate_commission($core_agent_commision);
                $admin_commission = roundoff_number($core_agent_commision-$AgentCommission);//calculate here
                $agent_commission = roundoff_number($AgentCommission);
            }
            //TDS Calculation
            ## $admin_tds = $currency_obj->calculate_tds($admin_commission); // commented by bhola
            $agent_tds = $currency_obj->calculate_tds($agent_commission);

            //**************Ticketing For Each Token START
            //Following Variables are used to save Transaction and Pax Ticket Details
            $pnr = '';
            $book_id = '';
            $source = '';
            $ref_id = '';
            $transaction_status = 0;
            $GetBookingResult = array();
            $transaction_description = '';
            $getbooking_StatusCode = '';
            $getbooking_Description = '';
            $getbooking_Category = '';
            $WSTicket = array();
            $WSFareRule = array();
            //Saving Flight Transaction Details
            $tranaction_attributes = array();
            $pnr = '';
            $book_id = '';
            $source = 'SABRE'; 
            $ref_id = '';
            $transaction_status = $master_transaction_status;
            $transaction_description = '';
            //Get Booking Details
            $getbooking_status_details = '';
            $getbooking_StatusCode = '';
            $getbooking_Description = '';
            $getbooking_Category = '';
            $tranaction_attributes['Fare'] = $Fare;
            $sequence_number = $token_index;
            //Transaction Log Details
            $ticket_trans_status_group[] = $transaction_status;
            $book_total_fare[]  = $trans_total_fare;
            $book_domain_markup[]   = $admin_markup;
            $book_level_one_markup[] = $agent_markup;
            //Need individual transaction price details
            //SAVE Transaction Details
            $transaction_insert_id = $GLOBALS['CI']->flight_model->save_flight_booking_transaction_details(
            $app_reference, $transaction_status, $transaction_description, $pnr, $book_id, $source, $ref_id,
            json_encode($tranaction_attributes), $sequence_number, $currency, $commissionable_fare, $admin_markup, $agent_markup,
            $admin_commission, $agent_commission,
            $getbooking_StatusCode, $getbooking_Description, $getbooking_Category,
            $admin_tds, $agent_tds
            );
            $transaction_insert_id = $transaction_insert_id['insert_id'];
            //Saving Passenger Details
            $i = 0;
            for ($i=0; $i<$total_pax_count; $i++)
            {
                $passenger_type = $book_params['passenger_type'][$i];
                $is_lead = $book_params['lead_passenger'][$i];
                $title = get_enum_list('title', $book_params['name_title'][$i]);
                $first_name = $book_params['first_name'][$i];
                $middle_name = $book_params['middle_name'][$i];
                $last_name = $book_params['last_name'][$i];
                $date_of_birth = $book_params['date_of_birth'][$i];
                $gender = get_enum_list('gender', $book_params['gender'][$i]);

                $passenger_nationality_id = intval($book_params['passenger_nationality'][$i]);
                $passport_issuing_country_id = intval($book_params['passenger_passport_issuing_country'][$i]);
                $passenger_nationality = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passenger_nationality_id));
                $passport_issuing_country = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passport_issuing_country_id));

                $passenger_nationality = isset($passenger_nationality[$passenger_nationality_id]) ? $passenger_nationality[$passenger_nationality_id] : '';
                $passport_issuing_country = isset($passport_issuing_country[$passport_issuing_country_id]) ? $passport_issuing_country[$passport_issuing_country_id] : '';

                $passport_number = $book_params['passenger_passport_number'][$i];
                $passport_expiry_date = $book_params['passenger_passport_expiry_year'][$i].'-'.$book_params['passenger_passport_expiry_month'][$i].'-'.$book_params['passenger_passport_expiry_day'][$i];
                //$status = 'BOOKING_CONFIRMED';//Check it
                $status = $master_transaction_status;
                $passenger_attributes = array();
                $flight_booking_transaction_details_fk = $transaction_insert_id;//Adding Transaction Details Origin
                //SAVE Pax Details
                $pax_insert_id = $GLOBALS['CI']->flight_model->save_flight_booking_passenger_details(
                $app_reference, $passenger_type, $is_lead, $title, $first_name, $middle_name, $last_name, $date_of_birth,
                $gender, $passenger_nationality, $passport_number, $passport_issuing_country, $passport_expiry_date, $status,
                json_encode($passenger_attributes), $flight_booking_transaction_details_fk);
                //Save passenger ticket information     
                $passenger_ticket_info = $GLOBALS['CI']->flight_model->save_passenger_ticket_info($pax_insert_id['insert_id']);

            }//Adding Pax Details Ends 
                
            //Saving Segment Details
            foreach($segment_details as $seg_k => $seg_v) {
                foreach($seg_v as $ws_key => $ws_val) {
                    $FareRestriction = '';
                    $FareBasisCode = '';
                    $FareRuleDetail = '';
                    $airline_pnr = '';
                    $AirlineDetails = $ws_val['AirlineDetails'];
                    $OriginDetails = $ws_val['OriginDetails'];
                    $DestinationDetails = $ws_val['DestinationDetails'];
                    $segment_indicator = $ws_val['SegmentIndicator'];
                    $airline_code = $AirlineDetails['AirlineCode'];
                    $airline_name = $AirlineDetails['AirlineName'];
                    $flight_number = $AirlineDetails['FlightNumber'];
                    $fare_class = $AirlineDetails['FareClass'];
                    $from_airport_code = $OriginDetails['AirportCode'];
                    $from_airport_name = $OriginDetails['AirportName'];
                    $to_airport_code = $DestinationDetails['AirportCode'];
                    $to_airport_name = $DestinationDetails['AirportName'];
                    //$departure_datetime = date('Y-m-d H:i:s', strtotime($OriginDetails['DepartureTime']));
                    //$arrival_datetime = date('Y-m-d H:i:s', strtotime($DestinationDetails['ArrivalTime']));
                     $departure_datetime = date('Y-m-d H:i:s', strtotime($OriginDetails['DateTime']));
                    $arrival_datetime = date('Y-m-d H:i:s', strtotime($DestinationDetails['DateTime']));     
                    $iti_status = $ws_val['Status'];
                    $operating_carrier = $AirlineDetails['OperatingCarrier'];
                    $attributes = array('craft' => $ws_val['Craft'], 'ws_val' => $ws_val);
                    //SAVE ITINERARY
                    $GLOBALS['CI']->flight_model->save_flight_booking_itinerary_details(
                    $app_reference, $segment_indicator, $airline_code, $airline_name, $flight_number, $fare_class, $from_airport_code, $from_airport_name,
                    $to_airport_code, $to_airport_name, $departure_datetime, $arrival_datetime, $iti_status, $operating_carrier, json_encode($attributes),
                    $FareRestriction, $FareBasisCode, $FareRuleDetail, $airline_pnr);
                }
            }//End Of Segments Loop
        }//End Of Token Loop

        //Save Master Booking Details
        $book_total_fare = array_sum($book_total_fare);
        $book_domain_markup = array_sum($book_domain_markup);
        $book_level_one_markup = array_sum($book_level_one_markup);

        $phone = $book_params['passenger_contact'];
        $alternate_number = '';
        $email = $book_params['billing_email'];
        $start = $token[0];
        $end = end($token);
        // debug($segment_summary);exit;
        $journey_start = $segment_summary[0]['OriginDetails']['DepartureTime'];
        $journey_start = date('Y-m-d H:i:s', strtotime($journey_start));
        $journey_end = end($segment_summary);
        $journey_end = $journey_end['DestinationDetails']['ArrivalTime'];
        $journey_end = date('Y-m-d H:i:s', strtotime($journey_end));
        $payment_mode = $book_params['payment_method'];
        $created_by_id = intval(@$GLOBALS['CI']->entity_user_id);

        $passenger_country_id = intval($book_params['billing_country']);
        //$passenger_city_id = intval($book_params['billing_city']);
        $passenger_country = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passenger_country_id));
        //$passenger_city = $GLOBALS['CI']->db_cache_api->get_city_list(array('k' => 'origin', 'v' => 'destination'), array('origin' => $passenger_city_id));

        $passenger_country = isset($passenger_country[$passenger_country_id]) ? $passenger_country[$passenger_country_id] : '';
        //$passenger_city = isset($passenger_city[$passenger_city_id]) ? $passenger_city[$passenger_city_id] : '';
        $passenger_city = $book_params['billing_city'];

        $attributes = array('country' => $passenger_country, 'city' => $passenger_city, 'zipcode' => $book_params['billing_zipcode'], 'address' =>  $book_params['billing_address_1']);
        $flight_booking_status = $master_transaction_status;
        //SAVE Booking Details 
        $domain_id = $GLOBALS['CI']->session->userdata('domain_id');
        $branch_id = $GLOBALS['CI']->session->userdata('branch_id');
        $user_details_id = $GLOBALS['CI']->session->userdata('user_details_id');
        $user_type =  $GLOBALS['CI']->session->userdata('user_type'); 
        // debug($module);die;
        $GLOBALS['CI']->flight_model->save_flight_booking_details(
        $domain_origin, $flight_booking_status, $app_reference, $booking_source, $phone, $alternate_number, $email,
        $journey_start, $journey_end, $journey_from, $journey_to, $payment_mode, json_encode($attributes), $created_by_id,
        $from_loc, $to_loc, $from_to_trip_type, $transaction_currency, $currency_conversion_rate, $domain_id, $branch_id, $user_details_id, $user_type
        );

        /************** Update Convinence Fees And Other Details Start ******************/
        //Convinence_fees to be stored and discount
        $convinence = 0;
        $discount = 0;
        $convinence_value = 0;
        $convinence_type = 0;
        $convinence_type = 0;
        if ($module == 'b2c') {         
            $total_transaction_amount = $book_total_fare+$book_domain_markup;
            $convinence = $currency_obj->convenience_fees($total_transaction_amount, $master_search_id);
            
            $convinence_row = $currency_obj->get_convenience_fees();
            $convinence_value = $convinence_row['value'];
            $convinence_type = $convinence_row['type'];
            $convinence_per_pax = $convinence_row['per_pax'];
        } elseif ($module == 'b2b') {
            $discount = 0;
            $convinence_per_pax = 0;
        }
        $GLOBALS['CI']->load->model('transaction');
        //SAVE Convinience and Discount Details
        $GLOBALS['CI']->transaction->update_convinence_discount_details('flight_booking_details', $app_reference, $discount, $convinence, $convinence_value, $convinence_type, $convinence_per_pax);
        /************** Update Convinence Fees And Other Details End ******************/

        /**
         * Data to be returned after transaction is saved completely
         */
        $response['fare'] = $book_total_fare;
        $response['admin_markup'] = $book_domain_markup;
        $response['agent_markup'] = $book_level_one_markup;
        $response['convinence'] = $convinence;
        $response['discount'] = $discount;

        $response['status'] = $flight_booking_status;
        $response['status_description'] = $transaction_description;
        $response['name'] = $first_name;
        $response['phone'] = $phone;

        // debug($response);die;

        return $response;
    }

    public function get_airline_specific_markup_config($segment_details)
    {
        $specific_markup_config = array();
        // debug($segment_details);die;
        $airline_code = $segment_details[0][0]['AirlineDetails']['AirlineCode'];
        $category = 'airline_wise';
        $specific_markup_config[] = array('category' => $category, 'ref_id' => $airline_code);
        return $specific_markup_config;
    }

    public function get_airline_specific_markup_configfinal($segment_details)
    {
        $specific_markup_config = array();
        // debug($segment_details);die;
        $airline_code = $segment_details[0]['AirlineDetails']['AirlineCode'];
        $category = 'airline_wise';
        $specific_markup_config[] = array('category' => $category, 'ref_id' => $airline_code);
        return $specific_markup_config;
    }

    function total_price($price_summary, $retain_commission=false, $currency_obj = '')
    {
        
        $com = 0;
        $com_tds = 0;
        if ($retain_commission == false) {
            $com = 0;
            $com_tds += floatval($currency_obj->calculate_tds($price_summary['AgentCommission']));
            $com_tds += floatval($currency_obj->calculate_tds(@$price_summary['PLBEarned']));
            $com_tds += floatval($currency_obj->calculate_tds(@$price_summary['IncentiveEarned']));
        } else {
            $com += floatval(@$price_summary['AgentCommission']);
            $com += floatval(@$price_summary['PLBEarned']);
            $com += floatval(@$price_summary['IncentiveEarned']);
            $com_tds = 0;
        }
        return (floatval(@$price_summary['OfferedFare'])+$com+$com_tds);
    }

    private function calculate_commission($agent_com)
    {
        $agent_com_row = $this->commission['admin_commission_list'];
        $b2b_comm = 0;
        if ($agent_com_row['value_type'] == 'percentage') {
            //%
            $b2b_comm = ($agent_com/100)*$agent_com_row['value'];
        } else {
            //plus
            $b2b_comm = ($agent_com-$agent_com_row['value']);
        }
        return number_format($b2b_comm, 2, '.', '');
    }

    /**
   * Merges Flight Segment Details and Fare Details
   */
  public function merge_flight_segment_fare_details($flight_details)
  {
    $flight_pre_booking_summery = array();
    $PassengerFareBreakdown = array();
    $SegmentDetails = array();
    $SegmentSummary = array();
    $FareDetails = $this->merge_fare_details($flight_details);
    $PassengerFareBreakdown = $this->merge_passenger_fare_break_down($flight_details);
    $SegmentDetails = $this->merge_segment_details($flight_details);
    $SegmentSummary = $this->merge_segment_summary($flight_details);
    if(!empty($flight_details[0]['HoldTicket'])){
      $flight_details[0]['HoldTicket'] = $flight_details[0]['HoldTicket'];
    }else{
      $flight_details[0]['HoldTicket'] = 0;
    }
    // debug($flight_details);die;
    $flight_pre_booking_summery['FareDetails'] = $FareDetails;
    $flight_pre_booking_summery['PassengerFareBreakdown'] = $PassengerFareBreakdown;
    $flight_pre_booking_summery['SegmentDetails'] = $SegmentDetails;
    $flight_pre_booking_summery['SegmentSummary'] = $SegmentSummary;
    $flight_pre_booking_summery['HoldTicket'] = $flight_details[0]['HoldTicket'];
    return $flight_pre_booking_summery;
  }

  /**
   * Merges Fare Details
   * @param unknown_type $flight_details
   */
  public function merge_fare_details($flight_details)
  {
    $FareDetails = array();
    $temp_fare_details = group_array_column($flight_details, 'FareDetails');
    $APIPriceDetails = array_merge_numeric_values(group_array_column($temp_fare_details, 'api_PriceDetails'));
    if(isset($temp_fare_details[0]['b2c_PriceDetails']) == true) {//B2C
      $B2CPriceDetails = array_merge_numeric_values(group_array_column($temp_fare_details, 'b2c_PriceDetails'));
      $FareDetails['b2c_PriceDetails'] = $B2CPriceDetails;
    } elseif (isset($temp_fare_details[0]['b2b_PriceDetails']) == true) {//B2B
      $B2BPriceDetails = array_merge_numeric_values(group_array_column($temp_fare_details, 'b2b_PriceDetails'));
      $FareDetails['b2b_PriceDetails'] = $B2BPriceDetails;
    }
    $FareDetails['api_PriceDetails'] = $APIPriceDetails;
    return $FareDetails;
  }

  /**
   * Merge Passenger Breakdown details
   */
  public function merge_passenger_fare_break_down($flight_details)
  {
    $PassengerFareBreakdown = array();
    $tmp_fare_breakdown = group_array_column($flight_details, 'PassengerFareBreakdown');
    // debug($tmp_fare_breakdown);die;
    foreach($tmp_fare_breakdown as $k => $v) {
      foreach($v as $pax_k => $pax_v) {
        $pax_type = $pax_k;
        if(isset($PassengerFareBreakdown[$pax_type]) == false) {
          $PassengerFareBreakdown[$pax_type]['PassengerType'] = $pax_type;
          $PassengerFareBreakdown[$pax_type]['Count'] = $pax_v['Count'];
          $PassengerFareBreakdown[$pax_type]['BaseFare'] = $pax_v['BaseFare'];
        } else {
          $PassengerFareBreakdown[$pax_type]['BaseFare'] += $pax_v['BaseFare'];
        }
      }
    }
    return $PassengerFareBreakdown;
  }

  /**
   * Merges Flight Segment Details
   * @param unknown_type $flight_details
   */
  public function merge_segment_details($flight_details)
  {
    $SegmentDetails = array();
    foreach($flight_details as $k => $v){
      $SegmentDetails = array_merge($SegmentDetails, $v['SegmentDetails']);
    }
    return $SegmentDetails;
  }

  /**
   * Merges Flight Segment Summery
   * @param unknown_type $flight_details
   */
  public function merge_segment_summary($flight_details)
  {
    $SegmentSummary = array();
    foreach($flight_details as $k => $v){
      $SegmentSummary = array_merge($SegmentSummary, $v['SegmentSummary']);
    }
    return $SegmentSummary;
  }

  /**
   * Updates the Booking Details:Status, Price and Ticket Details
   */
  public function update_booking_details($book_id, $book_params, $ticket_details, $module='b2c',$currency_obj)
  {
    $response = array();
    $book_total_fare = array();
    $book_domain_markup = array();
    $book_level_one_markup = array();
    // debug($ticket_details);die;
    $app_reference = $book_id;
    $master_search_id = $book_params['search_id'];
    $ticket_details = array(
        'PNR' => $ticket_details['pnr_no'],
        'TicketDetails' => $book_params,
        'master_booking_status' => $ticket_details['status'],
        );
    // debug($ticket_details);die;
    //Setting Master Booking Status
    $master_transaction_status = $this->status_code_value($ticket_details['master_booking_status']);
    // debug($master_transaction_status);die;
    if(isset($ticket_details['TicketDetails']) == true && valid_array($ticket_details['TicketDetails']) == true){
      $ticket_details = $ticket_details;
    } else {
      $ticket_details = array();
    }
    // debug($ticket_details);die;
    $saved_booking_data = $GLOBALS['CI']->flight_model->get_booking_details($book_id);
    if($saved_booking_data['status'] == false) {
      $response['status'] = BOOKING_ERROR;
      $response['msg'] = 'No Data Found';
      return $response;
    }
    
    //Extracting the Saved data
    $s_master_data = $saved_booking_data['data']['booking_details'][0];
    $s_booking_itinerary_details = $saved_booking_data['data']['booking_itinerary_details'];
    $s_booking_transaction_details = $saved_booking_data['data']['booking_transaction_details'];
    $s_booking_customer_details = $saved_booking_data['data']['booking_customer_details'];
    $first_name = $s_booking_customer_details[0]['first_name'];
    $phone = $s_master_data['phone'];
    $current_master_booking_status = $s_master_data['status'];
    //Extracting the Origins
    $transaction_origins = group_array_column($s_booking_transaction_details, 'origin');
    $passenger_origins = group_array_column($s_booking_customer_details, 'origin');
    $itinerary_origins = group_array_column($s_booking_itinerary_details, 'origin');
    //Indexing the data with origin
    $indexed_transaction_details = array();
    foreach($s_booking_transaction_details as $s_tk => $s_tv){
      $indexed_transaction_details[$s_tv['origin']] = $s_tv;
    }
    //1.Update : flight_booking_details
    $flight_master_booking_status = $master_transaction_status;
    $GLOBALS['CI']->custom_db->update_record('flight_booking_details', array('status' => $master_transaction_status), array('app_reference' => $app_reference));

    $total_pax_count = count($book_params['passenger_type']);
    $pax_count = $total_pax_count;
    //********************** only for calculation
    $safe_search_data = $this->search_data($master_search_id);
    $safe_search_data = $safe_search_data['data'];
    $from_loc = $safe_search_data['from'];
    $to_loc = $safe_search_data['to'];
    $safe_search_data['is_domestic_one_way_flight'] = false;
    $from_to_trip_type = $safe_search_data['trip_type'];
    
    $safe_search_data['is_domestic_one_way_flight'] = $GLOBALS['CI']->flight_model->is_domestic_flight($from_loc, $to_loc);
    if ($safe_search_data['is_domestic_one_way_flight'] == false && strtolower($from_to_trip_type) == 'circle') {
      $multiplier = $pax_count * 2;//Multiply with 2 for international round way
    } else if(strtolower($from_to_trip_type) == 'multicity'){
      $multiplier = $pax_count * count($safe_search_data['from']);
    } else {
      $multiplier = $pax_count;
    }
    //********************* only for calculation
    // echo '5130';debug($currency_obj);die;
    // $currency_obj   = $book_params['currency_obj'];
    $currency = $currency_obj->to_currency;
    $deduction_cur_obj  = clone $currency_obj;
    //PREFERRED TRANSACTION CURRENCY AND CURRENCY CONVERSION RATE 
    $transaction_currency = get_application_currency_preference();
    $application_currency = admin_base_currency();
    $currency_conversion_rate = $currency_obj->transaction_currency_conversion_rate();
    
    if(valid_array($ticket_details) == true) {
      //Ticket Loop Starts
      // echo '5140';debug($ticket_details);die;
      // $ticket_value = $ticket_details;
      // foreach ($ticket_details as $ticket_index => $ticket_value) {
      // debug($transaction_origins);die;
        $transaction_details_origin = intval($transaction_origins[0]);
        // echo '5144';debug($s_booking_customer_details);die;
        if ($this->valid_flight_booking_status($ticket_details['master_booking_status']) == true) {//IF Ticket is HOLD/CONFIRMED
          $status = $this->status_code_value($ticket_details['master_booking_status']);
          $ticket_value = $ticket_details['PNR'];
          
          $api_booking_id = $book_id;
          $pnr = $ticket_details['PNR'];
          $Fare = $ticket_details['TicketDetails']['token']['token'][0]['FareDetails'];
          $PassengerFareBreakdown = $ticket_details['TicketDetails']['token']['token'][0]['PassengerFareBreakdown'];
          $segment_details = $ticket_details['TicketDetails']['token']['token'][0]['SegmentSummary'];
          $passenger_details = $s_booking_customer_details;
          // debug($ticket_details);die;
          $tmp_domain_markup = 0;
          $tmp_level_one_markup = 0;
          // echo '5159';debug($Fare);die;
          $itinerary_price  = $Fare['b2c_PriceDetails']['BaseFare'];
          //Calculation is different for b2b and b2c
          //Specific Markup Config
          $specific_markup_config = array();
          $specific_markup_config = $this->get_airline_specific_markup_configfinal($segment_details);//Get the Airline code for setting airline-wise markup
          
          $final_booking_price_details = $this->get_final_booking_price_details($Fare['api_PriceDetails'], $multiplier, $specific_markup_config, $currency_obj, $deduction_cur_obj, $module);
          // debug($final_booking_price_details);die;
          $commissionable_fare = $final_booking_price_details['commissionable_fare'];
          $trans_total_fare = $final_booking_price_details['trans_total_fare'];
          $admin_markup = $final_booking_price_details['admin_markup'];
          $agent_markup = $final_booking_price_details['agent_markup'];
          $admin_commission = $final_booking_price_details['admin_commission'];
          $agent_commission = $final_booking_price_details['agent_commission'];
          $admin_tds = $final_booking_price_details['admin_tds'];
          $agent_tds = $final_booking_price_details['agent_tds'];
          
          
          //2.Update : flight_booking_transaction_details
          $update_transaction_condition = array();
          $update_transaction_data = array();
          $update_transaction_condition['origin'] = $transaction_details_origin;
          $update_transaction_data['pnr'] = $pnr;
          $update_transaction_data['book_id'] = $api_booking_id;
          $update_transaction_data['status'] = $status;
          $update_transaction_data['total_fare'] = $commissionable_fare;
          $update_transaction_data['admin_commission'] = $admin_commission;
          $update_transaction_data['agent_commission'] = $agent_commission;
          $update_transaction_data['admin_tds'] = $admin_tds;
          $update_transaction_data['agent_tds'] = $agent_tds;
          $update_transaction_data['admin_markup'] = $admin_markup;
          $update_transaction_data['agent_markup'] = $agent_markup;
          //For Transaction Log
          $book_total_fare[]  = $trans_total_fare;
          $book_domain_markup[] = $admin_markup;
          $book_level_one_markup[] = $agent_markup;
          
          $GLOBALS['CI']->custom_db->update_record('flight_booking_transaction_details', $update_transaction_data, $update_transaction_condition);
  
          //3.Update: flight_booking_passenger_details
          $update_passenger_condition = array();
          $update_passenger_data = array();
          $update_passenger_condition['flight_booking_transaction_details_fk'] = $transaction_details_origin;
          $update_passenger_data['status'] = $master_transaction_status;
          $GLOBALS['CI']->custom_db->update_record('flight_booking_passenger_details', $update_passenger_data, $update_passenger_condition);
  
          //4.Update Ticket details to flight_passenger_ticket_info
          $single_pax_fare_breakup = $this->get_single_pax_fare_breakup($PassengerFareBreakdown);
          // debug($passenger_details);die;
          foreach($passenger_details as $pax_k => $pax_v){
            $passenger_fk = intval(array_shift($passenger_origins));
            $TicketId = $pax_v['origin'];
            $TicketNumber = $pax_v['TicketNumber'];
            $IssueDate = '';
            $Fare = $pax_v['Fare'];
            $SegmentAdditionalInfo = '';
            $ValidatingAirline = '';
            $CorporateCode = '';
            $TourCode = '';
            $Endorsement = '';
            $Remarks = '';
            $ServiceFeeDisplayType = '';
            //SAVE PAX Ticket Details
            $GLOBALS['CI']->flight_model->update_passenger_ticket_info($passenger_fk, $TicketId, $TicketNumber, $IssueDate, $Fare,
            $SegmentAdditionalInfo, $ValidatingAirline, $CorporateCode, $TourCode, $Endorsement, $Remarks, $ServiceFeeDisplayType);
          }
          //5. Update :flight_booking_itinerary_details
          // debug($segment_details);die;
          foreach($segment_details as $seg_k => $ws_val) {
            // foreach($seg_v as $ws_key => $ws_val) {
              // debug($ws_val);die;
              $update_segment_condition = array();
              $update_segement_data = array();
              $update_segment_condition['origin'] = intval(array_shift($itinerary_origins));
              $update_segement_data['airline_pnr'] = $pnr;
              $attributes = array();
              $attributes['departure_terminal'] = $ws_val['OriginDetails']['AirportName'];
              $attributes['arrival_terminal'] = $ws_val['DestinationDetails']['AirportName'];
              $attributes['CabinClass'] = $ws_val['cabin_class'];
              $attributes['Attr'] = $ws_val['cabin_bag'];
              
              $update_segement_data['attributes'] = json_encode($attributes);
              $update_segement_data['status'] = '';
              
              $update_segement_data['FareRestriction'] = '';
              $update_segement_data['FareBasisCode'] = '';
              $update_segement_data['FareRuleDetail'] = '';
              
              $GLOBALS['CI']->custom_db->update_record('flight_booking_itinerary_details', $update_segement_data, $update_segment_condition);
            // }
          }
        } else {//IF Ticket is Failed
          $GLOBALS['CI']->flight_model->update_flight_booking_transaction_failure_status($app_reference, $transaction_details_origin);
          //For Transaction Log
          $book_total_fare[]  = $indexed_transaction_details[$transaction_details_origin]['total_fare'];
          $book_domain_markup[] = $indexed_transaction_details[$transaction_details_origin]['admin_markup'];
          $book_level_one_markup[] = $indexed_transaction_details[$transaction_details_origin]['agent_markup'];
        }
      //}//Ticket Loop Ends
    } else {
      foreach ($indexed_transaction_details as $itd_k => $itd_v){
        $transaction_details_origin = $itd_v['origin'];
        $GLOBALS['CI']->flight_model->update_flight_booking_transaction_failure_status($app_reference, $transaction_details_origin);
        
        $book_total_fare[]  = $itd_v['total_fare'];
        $book_domain_markup[] = $itd_v['admin_markup'];
        $book_level_one_markup[] = $itd_v['agent_markup'];
      }
    }
    /**
     * Data to be returned after transaction is saved completely
     */
    $transaction_description = '';
    $book_total_fare = array_sum($book_total_fare);
    $book_domain_markup = array_sum($book_domain_markup);
    $book_level_one_markup = array_sum($book_level_one_markup);
    $discount = 0;
    if($module == 'b2c') {
      $total_transaction_amount = $book_total_fare+$book_domain_markup;
      $convinence = $currency_obj->convenience_fees($total_transaction_amount, $master_search_id);
    } else {
      $convinence = 0;
    }
    $response['fare'] = $book_total_fare;
    $response['admin_markup'] = $book_domain_markup;
    $response['agent_markup'] = $book_level_one_markup;
    $response['convinence'] = $convinence;
    $response['discount'] = $discount;

    $response['status'] = $flight_master_booking_status;
    $response['status_description'] = $transaction_description;
    $response['name'] = $first_name;
    $response['phone'] = $phone;
    $response['transaction_currency'] = $transaction_currency;
    $response['currency_conversion_rate'] = $currency_conversion_rate;
    
    return $response;
  }

  /**
   * 
   * Enter description here ...
   */
  private function status_code_value($status_code)
  {
    switch ($status_code){
      case BOOKING_CONFIRMED:
      case SUCCESS_STATUS:
         $status_value = 'BOOKING_CONFIRMED';
         break;
      case BOOKING_HOLD:
        $status_value = 'BOOKING_HOLD';
        break;
      default:
        $status_value = 'BOOKING_FAILED';
    }
    return $status_value;
  }

  /**
   * Booking status is valid or not
   * @param unknown_type $booking_status
   */
  private function valid_flight_booking_status($booking_status)
  {
    if(in_array($booking_status, array(BOOKING_CONFIRMED, BOOKING_HOLD)) == true){
      return true;
    } else {
      return false;
    }
  }

  /**
   * Returns Final Price Details For the booking
   * @param unknown_type $Fare
   * @param unknown_type $multiplier
   * @param unknown_type $specific_markup_config
   * @param unknown_type $currency_obj
   * @param unknown_type $deduction_cur_obj
   * @param unknown_type $module
   */
  private function get_final_booking_price_details($Fare, $multiplier, $specific_markup_config, $currency_obj, $deduction_cur_obj, $module)
  {
    $data = array();
    $core_agent_commision = ($Fare['PublishedFare']-$Fare['OfferedFare']);
    $commissionable_fare = $Fare['PublishedFare'];
    if ($module == 'b2c') {       
      $trans_total_fare = $this->total_price($Fare, false, $currency_obj);
      $markup_total_fare  = $currency_obj->get_currency($trans_total_fare, true, false, true, $multiplier, $specific_markup_config);
      $ded_total_fare   = $deduction_cur_obj->get_currency($trans_total_fare, true, true, false, $multiplier, $specific_markup_config);
      $admin_markup = roundoff_number($markup_total_fare['default_value']-$ded_total_fare['default_value']);
      $admin_commission = $core_agent_commision;
      $agent_markup = 0;
      $agent_commission = 0;
      
    } else {
      //B2B Calculation
      //Markup
      $trans_total_fare = $Fare['PublishedFare'];
      $markup_total_fare  = $currency_obj->get_currency($trans_total_fare, true, true, true, $multiplier, $specific_markup_config);
      $ded_total_fare   = $deduction_cur_obj->get_currency($trans_total_fare, true, false, true, $multiplier, $specific_markup_config);
      $admin_markup = abs($markup_total_fare['default_value']-$ded_total_fare['default_value']);
      $agent_markup = roundoff_number($ded_total_fare['default_value']-$trans_total_fare);
      //Commission
      $this->commission = $currency_obj->get_commission();
      $AgentCommission = $this->calculate_commission($core_agent_commision);
      $admin_commission = roundoff_number($core_agent_commision-$AgentCommission);//calculate here
      $agent_commission = roundoff_number($AgentCommission);
    }
    //TDS Calculation
    $admin_tds = $currency_obj->calculate_tds($admin_commission);
    $agent_tds = $currency_obj->calculate_tds($agent_commission);
    
    $data['commissionable_fare'] = $commissionable_fare;
    $data['trans_total_fare'] = $trans_total_fare;
    $data['admin_markup'] = $admin_markup;
    $data['agent_markup'] = $agent_markup;
    $data['admin_commission'] = $admin_commission;
    $data['agent_commission'] = $agent_commission;
    $data['admin_tds'] = $admin_tds;
    $data['agent_tds'] = $agent_tds;
    #print_r($data);die('lib');
    return $data;
  }

  /**
   * Returns Single Pax Breakdown
   * @param unknown_type $passenger_fare_breakdown
   */
  private function get_single_pax_fare_breakup($passenger_fare_breakdown)
  {
    $single_pax_fare_breakup = array();
    // debug($passenger_fare_breakdown);die;
    foreach ($passenger_fare_breakdown as $k => $v){
      $PassengerCount = $v['Count'];
      $single_pax_fare_breakup[$k]['BaseFare'] =  ($v['BaseFare']/$PassengerCount);
      $single_pax_fare_breakup[$k]['Tax'] =       ($v['TaxFare']/$PassengerCount);
      $single_pax_fare_breakup[$k]['TotalPrice'] =  ($v['TotalFare']/$PassengerCount);
    }
    return $single_pax_fare_breakup;
  }

}
