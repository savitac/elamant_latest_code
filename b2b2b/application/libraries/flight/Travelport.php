<?php 
//error_reporting(E_ALL);
require_once 'Common_api_flight.php';
class Travelport extends Common_Api_Flight {
    var $master_search_data;
    var $search_hash;
    protected $source_code = TRAVELPORT_FLIGHT_BOOKING_SOURCE;
    function __construct() {
        parent::__construct ( META_AIRLINE_COURSE, $this->source_code );
        $this->CI = &get_instance ();
        $GLOBALS ['CI']->load->library ( 'Api_Interface' );
        $GLOBALS ['CI']->load->library ( 'converter' );
        $GLOBALS ['CI']->load->library ( 'ArrayToXML' );
        $GLOBALS ['CI']->load->model   ( 'flight_model' );
        $GLOBALS ['CI']->load->helper  ( 'url' ); 
        $this->set_api_credentials ();

    }
    
    /**
     * set API credentials
     */
    private function set_api_credentials() {
        $engine_system = $this->CI->config->item ( 'flight_engine_system' );
        $this->system = $engine_system;
        
        //$this->details = $this->CI->config->item ( 'travelport_flight_' . $engine_system );
        $this->details = $this->CI->config->item ( 'travelport_flight_test');
        //debug($this->details);exit;
        
        $this->url = $this->details ['api_url'];
        $this->username = $this->details ['username'];
        $this->password = $this->details ['password'];
        $this->target_branch = $this->details ['target_branch'];
        
        
    }
    
    /**
     * request Header
     */
    private function get_header() {
        // Vipassana centre
        $header = array (
                'Accept: text/xml',
                'Accept-Encoding: gzip, deflate',
                'Content-Type: text/xml; charset=utf-8' 
        );
        return array (
                'header' => $header 
        );
    }
    public function search_data($search_id) {
        $response ['status'] = true;
        $response ['data'] = array ();
        if (empty ( $this->master_search_data ) == true and valid_array ( $this->master_search_data ) == false) {
            $clean_search_details = $this->CI->flight_model->get_safe_search_data ( $search_id );
            
            if ($clean_search_details ['status'] == true) {
                $response ['status'] = true;
                $response ['data'] = $clean_search_details ['data'];
                // 28/12/2014 00:00:00 - date format

            /*if ($clean_search_details ['data'] ['trip_type'] == 'multicity') {
                $response ['data'] ['from'] = $clean_search_details ['data'] ['from'];
                $response ['data'] ['to'] = $clean_search_details ['data'] ['to'];
                $response ['data'] ['from_city'] = $clean_search_details ['data'] ['from'];
                $response ['data'] ['to_city'] = $clean_search_details ['data'] ['to'];
                $response ['data'] ['depature'] = $clean_search_details ['data'] ['depature'];
                $response ['data'] ['return'] = $clean_search_details ['data'] ['depature'];
                } else {
                $response ['data'] ['from'] = substr ( chop ( substr ( $clean_search_details ['data'] ['from'], - 5 ), ')' ), - 3 );
                $response ['data'] ['to'] = substr ( chop ( substr ( $clean_search_details ['data'] ['to'], - 5 ), ')' ), - 3 );
                $response ['data'] ['from_city'] = $clean_search_details ['data'] ['from'];
                $response ['data'] ['to_city'] = $clean_search_details ['data'] ['to'];
                $response ['data'] ['depature'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['depature'] ) ) . 'T00:00:00';
                $response ['data'] ['return'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['depature'] ) ) . 'T00:00:00';
                
            }*/
                /*switch ($clean_search_details ['data'] ['trip_type']) {
                    
                    case 'oneway' :
                        $response ['data'] ['type'] = 'OneWay';
                        break;
                    
                    case 'circle' :
                        $response ['data'] ['type'] = 'Return';
                        $response ['data'] ['return'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['return'] ) ) . 'T00:00:00';
                        break;
                    case 'multicity' :
                        $response ['data'] ['type'] = 'MultiCity';
                        break;
                    default :
                        $response ['data'] ['type'] = 'OneWay';
                }*/
                switch ($clean_search_details ['data'] ['trip_type']) {
                    case 'oneway' :
                        $response ['data'] ['type'] = 'OneWay';
                        $response ['data'] ['from'] = substr ( chop ( substr ( $clean_search_details ['data'] ['from'], - 5 ), ')' ), - 3 );
                        $response ['data'] ['to'] = substr ( chop ( substr ( $clean_search_details ['data'] ['to'], - 5 ), ')' ), - 3 );
                        $response ['data'] ['depature'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['depature'] ) ) . 'T00:00:00';
                        $response ['data'] ['return'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['depature'] ) ) . 'T00:00:00';
                        break;
                    
                    case 'circle' :
                        $response ['data'] ['type'] = 'Return';
                        $response ['data'] ['from'] = substr ( chop ( substr ( $clean_search_details ['data'] ['from'], - 5 ), ')' ), - 3 );
                        $response ['data'] ['to'] = substr ( chop ( substr ( $clean_search_details ['data'] ['to'], - 5 ), ')' ), - 3 );
                        $response ['data'] ['depature'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['depature'] ) ) . 'T00:00:00';
                        $response ['data'] ['return'] = date ( "Y-m-d", strtotime ( $clean_search_details ['data'] ['return'] ) ) . 'T00:00:00';
                        break;
                    case 'multicity':
                        $response['data']['type'] = 'Multicity';
                        for($i=0; $i<count($clean_search_details['data']['depature']); $i++) {
                            $response['data']['depature'][$i] = date("Y-m-d", strtotime($clean_search_details['data']['depature'][$i])) . 'T00:00:00';
                            $response['data']['return'][$i] = date("Y-m-d", strtotime($clean_search_details['data']['depature'][$i])) . 'T00:00:00';
                            $response['data']['from'][$i] = substr(chop(substr($clean_search_details['data']['from'][$i], -5), ')'), -3);
                            $response['data']['to'][$i] = substr(chop(substr($clean_search_details['data']['to'][$i], -5), ')'), -3);
                        }
                        break;                  
                    default :
                        $response ['data'] ['type'] = 'OneWay';
                }
                $response ['data'] ['adult'] = $clean_search_details ['data'] ['adult_config'];
                $response ['data'] ['child'] = $clean_search_details ['data'] ['child_config'];
                $response ['data'] ['infant'] = $clean_search_details ['data'] ['infant_config'];
                $response ['data'] ['v_class'] = $clean_search_details ['data'] ['v_class'];
                $response ['data'] ['carrier'] = implode ( $clean_search_details ['data'] ['carrier'] );
                $this->master_search_data = $response ['data'];
            } else {
                $response ['status'] = false;
            }
        } else {
            $response ['data'] = $this->master_search_data;
        }
        $this->search_hash = md5 ( serialized_data ( $response ['data'] ) );
        return $response;
    }
    
    /**
     * flight search request
     * 
     * @param $search_id unique
     *          id which identifies search details
     */
    function get_flight_list($search_id,$currency_obj,$module='') {
        //error_reporting(E_ALL);
        $this->CI->load->driver( 'cache' );
        $response ['data'] = array ();
        $response ['status'] = SUCCESS_STATUS;
        $response ['booking_url'] = $this->booking_url(intval($search_id));
        
        /* get search criteria based on search id */
        $search_data = $this->search_data ( $search_id );
        
        $header_info = $this->get_header ();
        // generate unique searchid string to enable caching
        $cache_search = $GLOBALS['CI']->config->item ( 'cache_flight_search' );
        $search_hash = $this->search_hash;
        //unset($cache_search); 
        if ($cache_search) {
            $cache_contents = $this->CI->cache->file->get ( $search_hash );
        }
        $cache_search = FALSE; 
        if ($search_data ['status'] == SUCCESS_STATUS) {
            if ($cache_search == FALSE || ($cache_search === true && empty ( $cache_contents ) == true)) {
                $response ['booking_url'] = $this->booking_url(intval($search_id));             
                $flight_search_request = $this->flight_low_fare_search_req ( $search_data ['data'] );
                
                if ($flight_search_request ['status'] = SUCCESS_STATUS) {                   
                    // return_request
                    $search_response = $this->process_request ( $flight_search_request ['data'] ['request'] );
                
                    /*XML Logs*/
                    $xml_title = 'LowFareResponse'.date('Y-m-dhsi');  
                    $date = date('Ymd_His');
                    if (!file_exists('xml_logs/Flight')) {
                         mkdir('../b2b2b/xml_logs/Flight', 0777, true);
                    }
               //   $responsexml = $xml_title.'_response_'.$date.'_'.date('Y-m-d').'.xml';
                    $responsexml = $xml_title.'.xml';
                    file_put_contents('../b2b2b/xml_logs/Flight/'.$responsexml, $search_response);
                    /*End Of XML Logs*/
                    
                    $search_response_array = Converter::createArray ( $search_response );
                    //echo "search_response_array ";debug($search_response_array);exit;
                    
                    $journey_summary = $this->extract_journey_details($search_data);                    
                    $response['data']['JourneySummary']= $journey_summary;
                    if ($this->valid_search_result ( $search_response_array ) == TRUE) {
                        
                        $key = 0;
                        // SAVE response in 'test' table
                        // $this->CI->custom_db->generate_static_response(json_encode($search_response));
                        $clean_format_data = $this->format_search_data_response ( $search_response_array, $search_data ['data'], $key ,$currency_obj,$module);

                        //$response['booking_url'] = $this->booking_url(intval($search_id));
                        $response ['data'] ['Flights'] [0] = $clean_format_data;
                        if ($cache_search) {
                            $cache_exp = $this->CI->config->item ( 'cache_flight_search_ttl' );
                            $this->CI->cache->file->save ( $search_hash, $response ['data'], $cache_exp );
                        }
                    }
                    
                    if (isset ( $flight_search_request ['data'] ['return'] ) && ! empty ( $flight_search_request ['data'] ['return'] )) {
                        $return_search_response = $this->process_request ( $flight_search_request ['data'] ['return'] );

                        /*XML Logs*/
                        $xml_title = 'LowFareResponse_return'.date('Y-m-dhsi');  
                        $date = date('Ymd_His');
                        if (!file_exists('xml_logs/Flight')) {
                             mkdir('xml_logs/Flight', 0777, true);
                        }
                        //$responsexml = $xml_title.'_response_'.$date.'_'.date('Y-m-d').'.xml';
                        $responsexml = $xml_title.'.xml';
                        file_put_contents('../b2b2b/xml_logs/Flight/'.$responsexml, $return_search_response);
                        /*End Of XML Logs*/


                        $return_search_response_array = Converter::createArray ( $return_search_response );
                        
                        if ($this->valid_search_result ( $return_search_response_array ) == TRUE) {
                            $key = 1;
                            // SAVE response in 'test' table
                            // $this->CI->custom_db->generate_static_response(json_encode($search_response));
                            $clean_format_data = $this->format_search_data_response ( $return_search_response_array, $search_data ['data'], $key ,$currency_obj,$module);
                            //$response['booking_url'] = $this->booking_url(intval($search_id));
                            $response ['data'] ['Flights'] [1] = $clean_format_data;
                            
                            if ($cache_search) {
                                $cache_exp = $this->CI->config->item ( 'cache_flight_search_ttl' );
                                $this->CI->cache->file->save ( $search_hash, $response ['data'], $cache_exp );
                            }
                        }
                    }
                }
            } else {
                $response ['data'] = $cache_contents;
            }
        } else {
            $response ['status'] = FAILURE_STATUS;
        }
        //echo "Response v41 ";debug($response);die('   response'); 
        /*debug($response);
        exit;*/
        return $response;
    } 

    function air_create_reservation($params) {
        echo 'Ticket Booked Successfully On Our Test Environment';
        echo '<br>Neptune System Reference For This Booking Request : NPT-' . rand ( 1000, 100000 ) . '<br>';
        echo 'Booked At : ' . date ( 'd-M-Y H:i:s' );
        exit ();
        $air_create_reservation_request = $this->air_create_reservation_request ( $params );
        if ($air_create_reservation_request ['status'] == SUCCESS_STATUS) {
            $air_create_reservation_response = $this->process_request ( $air_create_reservation_request ['data'] ['request'] );
            
            $test = Converter::createArray ( $search_response );
            echo 'inside';
            debug ( $air_create_reservation_response );
            exit ();
        }
    }


     function seat_mapping_req_new($segment_xml){
        $response['status'] =FAILURE_STATUS;
        $response['seat_xml_request'] =array();
        $seat_map ='';
        $seatResformated = array();
        
        if(!empty($segment_xml)){ //debug($segment_xml); die;
            foreach($segment_xml as $k =>$seg){                 
                $seat_map ='<?xml version="1.0" encoding="utf-8"?>
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
    <soapenv:Header/>
    <soapenv:Body>
        <air:SeatMapReq TargetBranch="'.$this->target_branch.'" TraceId="trace" ReturnSeatPricing="false" AuthorizedBy="uAPI" xmlns:com="http://www.travelport.com/schema/common_v41_0" xmlns:univ="http://www.travelport.com/schema/universal_v41_0" xmlns:air="http://www.travelport.com/schema/air_v41_0">
            <BillingPointOfSaleInfo OriginApplication="UAPI" xmlns="http://www.travelport.com/schema/common_v41_0"/>'.$seg.'</air:SeatMapReq>
        </soapenv:Body>
    </soapenv:Envelope>';               
            $request_xml = 'SeatMapRequest_'.$k.'.xml';             
            file_put_contents('../b2b2b/xml_logs/Flight/'.$request_xml, $seat_map);
            //debug($seat_map); 
            $response = $this->process_request($seat_map);
            $response_xml= 'SeatMapResponse_'.$k.'.xml';
            //$response = file_get_contents('xml_logs/Flight/'.$response_xml)   ;               
            file_put_contents('../b2b2b/xml_logs/Flight/'.$response_xml, $response);                 
            $xmlresponse[$k] = $response;           
            $seatRes_arrar    = Converter::createArray ($xmlresponse[$k]);      
            if (isset($seatRes_arrar['SOAP:Body']['SOAP:Fault'])) {
                $seatResformated[$k] = $seatRes_arrar['SOAP:Body']['SOAP:Fault']['faultstring'];
            } else {
                $seatResformated[$k] = $this->seatFormat($seatRes_arrar, $k);
            debug($seatResformated); die;
            }
        
            } 
        
        
    return $seatResformated;
        }
     }
    
    /*
     * added on july 13 2017 by princess
     * 
     */
     function seat_mapping_req($segment_xml){
        
        $response['status'] =FAILURE_STATUS;
        $response['seat_xml_request'] =array();
        $seat_map ='';
        $seatResformated = array();
        if(!empty($segment_xml)){ //debug($segment_xml); die;
            foreach($segment_xml as $k =>$seg){
            //$seg->registerXPathNamespace ( 'SOAP', 'http://schemas.xmlsoap.org/soap/envelope/' );
            //$seg->registerXPathNamespace ( 'air', 'http://www.travelport.com/schema/air_v41_0' );         
            $seat_map ='<?xml version="1.0" encoding="utf-8"?>
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
    <soapenv:Header/>
    <soapenv:Body>
        <air:SeatMapReq TargetBranch="'.$this->target_branch.'" TraceId="trace" ReturnSeatPricing="false" AuthorizedBy="uAPI" xmlns:com="http://www.travelport.com/schema/common_v41_0" xmlns:univ="http://www.travelport.com/schema/universal_v41_0" xmlns:air="http://www.travelport.com/schema/air_v41_0">
            <BillingPointOfSaleInfo OriginApplication="UAPI" xmlns="http://www.travelport.com/schema/common_v41_0"/>'.$seg.'</air:SeatMapReq>
        </soapenv:Body>
    </soapenv:Envelope>';               
            $request_xml = 'SeatMapRequest_'.$k.'.xml';             
            file_put_contents('../b2b2b/xml_logs/Flight/'.$request_xml, $seat_map);
            //debug($seat_map); 
            $response = $this->process_request($seat_map);
            $response_xml= 'SeatMapResponse_'.$k.'.xml';
            //$response = file_get_contents('xml_logs/Flight/'.$response_xml)   ;               
            file_put_contents('../b2b2b/xml_logs/Flight/'.$response_xml, $response);                 
            $xmlresponse[$k] = $response;           
            $seatRes_arrar    = Converter::createArray ($xmlresponse[$k]);      
            if (isset($seatRes_arrar['SOAP:Body']['SOAP:Fault'])) {
                $seatResformated[$k] = $seatRes_arrar['SOAP:Body']['SOAP:Fault']['faultstring'];
            } else {
                $seatResformated[$k] = $this->seatFormat($seatRes_arrar, $k);
            //debug($seatResformated); die;
            }
        
            } 
        
        
    return $seatResformated;
        }
     } 


    public function seatFormat($seat_res, $se)
    {
        #debug($seat_res);die;
        $seat_re = $seat_res['SOAP:Envelope']['SOAP:Body']['air:SeatMapRsp']['air:Rows']['air:Row'];
        $seat_report = array();
        
        for ($i=0; $i < count($seat_re) ; $i++) { 
            for ($j=0; $j < count($seat_re[$i]['air:Facility']); $j++) { 
                $seat_report[$i][$j]['row_number'] = $seat_re[$i]['@attributes']['Number'];
                $row_wise = $seat_re[$i]['air:Facility'][$j];
                $myseat_type = '';
                $myseat_type_PaidGeneralSeat = '';
                $myseat_type_PaidGeneralSeat_status = false;
                #debug($row_wise); die;
                if (isset($row_wise['air:Characteristic'][0])) {                    
                    for ($k=0; $k < count($row_wise['air:Characteristic']) ; $k++) { 
                        $seat_report[$i][$j][$k]['Characteristic_Value'] = $row_wise['air:Characteristic'][$k]['@attributes']['Value'];
                        if(in_array($row_wise['air:Characteristic'][$k]['@attributes']['Value'], array('Aisle','Window'))){
                            $myseat_type = $row_wise['air:Characteristic'][$k]['@attributes']['Value'];
                        }
                        if(in_array($row_wise['air:Characteristic'][$k]['@attributes']['Value'], array('PaidGeneralSeat'))){
                            $myseat_type_PaidGeneralSeat = 'ChargeableSeat';
                            $myseat_type_PaidGeneralSeat_status = true;
                        }
                    }
                } else{
                    //debug($row_wise); die;
                    if(in_array(@$row_wise['air:Characteristic']['@attributes']['Value'], array('Aisle','Window'))){
                        $myseat_type = $row_wise['air:Characteristic']['@attributes']['Value'];
                    }
                    if(in_array(@$row_wise['air:Characteristic']['@attributes']['Value'], array('PaidGeneralSeat'))){
                        $myseat_type_PaidGeneralSeat = 'ChargeableSeat';
                        $myseat_type_PaidGeneralSeat_status = true;
                    }
                    $seat_report[$i][$j][0]['Characteristic_Value'] = @$row_wise['air:Characteristic']['@attributes']['Value'];
                }
                $seat_report[$i][$j]['Type'] = $myseat_type;
                $seat_report[$i][$j]['chargeableInd'] = @$myseat_type_PaidGeneralSeat;
                $seat_report[$i][$j]['chargeableInd_staus'] = @$myseat_type_PaidGeneralSeat_status;
                $seat_report[$i][$j]['SeatCode'] = @$row_wise['@attributes']['SeatCode'];
                $seat_report[$i][$j]['Availability'] = @$row_wise['@attributes']['Availability'];
                $seat_report[$i][$j]['Paid'] = @$row_wise['@attributes']['Paid'];
                
            }
        }
        // echo 'pformat';debug($seat_report);exit('seat_report');
        $respswavi = $this->formatseat($seat_report, $se);
        // echo 'pformat';debug($respswavi);exit('seat_report');
        return $respswavi;
    }

public function formatseat($saerdata, $se)
    { 
        error_reporting(0);
        $parse_seat_formated_response = array();
        $seat_details_formated = array();
        $columnsTotalAmount = 0;
        $columnsCurrency = get_application_default_currency();
        $columnCharacteristic = array();
        
        if(is_array($saerdata) && !empty($saerdata)){
            // rows
            foreach($saerdata as $seat_row_key=>$seat_rows){
                if(is_array($seat_rows) && !empty($seat_rows)){
                    
                    // columns
                    $iiiiiiiiiiiiiiiiiiiii = 0;
                    $seat_columns = '';
                    foreach($seat_rows as $seat_columns_key=>$seat_columns){

                        
                        // debug(explode('-', $seat_columns['SeatCode'])); exit;
                        $columnsSeatNumber___ = end(explode('-', $seat_columns['SeatCode']));
                        if(!empty($seat_columns['SeatCode']) && ($seat_columns['SeatCode']!='') && !empty($columnsSeatNumber___)){
                            $seat_columnssss = $seat_columns['row_number'];
                            // exit;
                            $columnsSeatNumber[] = $columnsSeatNumber___;
                            //$columnsSeatNumber[] = $seat_columns['SeatCode'];
                            
                            if(in_array($seat_columns['Availability'], array('Available'))){
                                // available
                                $columnsFacilities[] = !empty($seat_columns['chargeableInd'])?$seat_columns['chargeableInd']:'true';
                                $columnsOccupation[] = 'false';
                                $columnsOccupiedInd[] = 'false';
                                $columnsInoperativeInd[] = '';
                                $columnsPremiumInd[] = '';
                                $columnsChargeableInd[] = !empty($seat_columns['chargeableInd'])?'true':'false';
                                $columnsExitRowInd[] = '';
                                $columnsRestrictedReclineInd[] = '';
                                $columnsNoInfantInd[] = '';
                                $columnsTotalAmount[] = 0;
                            }else{  
                                $columnsFacilities[] = 'false';
                                $columnsOccupation[] = 'SeatIsOccupied';
                                $columnsOccupiedInd[] = 'true';
                                $columnsInoperativeInd[] = '';
                                $columnsPremiumInd[] = '';
                                $columnsChargeableInd[] = 'false';
                                $columnsExitRowInd[] = '';
                                $columnsRestrictedReclineInd[] = '';
                                $columnsNoInfantInd[] = '';
                                $columnsTotalAmount[] = 0;
                            }

                            if(!isset($seat_columns['Type'])){
                                if(is_array($seat_columns)){
                                    foreach($seat_columns as $kkk=>$vvv){
                                        // debug($vvv);
                                        if(is_numeric($kkk)){
                                            if(in_array($seat_columns[$kkk]['Characteristic_Value'], array('Aisle', 'Window'))){
                                                $columnCharacteristic[$iiiiiiiiiiiiiiiiiiiii] = $seat_columns[$kkk]['Characteristic_Value'];
                                                break;
                                            }else{
                                                $columnCharacteristic[$iiiiiiiiiiiiiiiiiiiii] = 'CenterSeat';
                                            }
                                        }
                                    }
                                }
                            }else{
                                $columnCharacteristic[$iiiiiiiiiiiiiiiiiiiii] = empty($seat_columns['Type'])?'CenterSeat':$seat_columns['Type'];
                            }

                            $iiiiiiiiiiiiiiiiiiiii++;

                        }else{
                            /*
                                $columnsFacilities[] = '';
                                $columnsOccupation[] = '';
                                $columnsOccupiedInd[] = '';
                                $columnsInoperativeInd[] = '';
                                $columnsPremiumInd[] = '';
                                $columnsChargeableInd[] = '';
                                $columnsExitRowInd[] = '';
                                $columnsRestrictedReclineInd[] = '';
                                $columnsNoInfantInd[] = '';
                                $columnsTotalAmount[] = '';
                            */
                        }

                    }
                    $parse_seat_formated_response[$seat_row_key]['seatRowNumbers'] = $seat_columnssss;
                    $parse_seat_formated_response[$seat_row_key]['seatRowNumber'] = $seat_columnssss;
                    $parse_seat_formated_response[$seat_row_key]['seatColumn'] = $columnsSeatNumber;                        
                    $parse_seat_formated_response[$seat_row_key]['TotalAmount'] = $columnsTotalAmount;                  
                    $parse_seat_formated_response[$seat_row_key]['Currency'] = $columnsCurrency;
                    $parse_seat_formated_response[$seat_row_key]['Facilities'] = $columnsFacilities;
                    $parse_seat_formated_response[$seat_row_key]['Occupation'] = $columnsOccupation;
                    $parse_seat_formated_response[$seat_row_key]['occupiedInd'] = $columnsOccupiedInd;
                    $parse_seat_formated_response[$seat_row_key]['inoperativeInd'] = $columnsInoperativeInd;
                    $parse_seat_formated_response[$seat_row_key]['premiumInd'] = $columnsPremiumInd;
                    $parse_seat_formated_response[$seat_row_key]['chargeableInd'] = $columnsChargeableInd;
                    $parse_seat_formated_response[$seat_row_key]['exitRowInd'] = $columnsExitRowInd;
                    $parse_seat_formated_response[$seat_row_key]['restrictedReclineInd'] = $columnsRestrictedReclineInd;
                    $parse_seat_formated_response[$seat_row_key]['noInfantInd'] = $columnsNoInfantInd;
                    $parse_seat_formated_response[$seat_row_key]['columnCharacteristic'] = $columnCharacteristic;

                    // debug($columnCharacteristic);
                    unset($columnsSeatNumber);
                    unset($columnsTotalAmount);
                    unset($columnsFacilities);
                    unset($columnsOccupation);
                    unset($columnsOccupiedInd);
                    unset($columnsInoperativeInd);
                    unset($columnsPremiumInd);
                    unset($columnsChargeableInd);
                    unset($columnsExitRowInd);
                    unset($columnsRestrictedReclineInd);
                    unset($columnsNoInfantInd);
                    unset($columnCharacteristic);
                    // echo 'parse_seat_formated_response';
                    
                }
            }
            // exit;
            $seat_details_formated['cabin']['firstRow'] = array(@current($parse_seat_formated_response)['seatRowNumbers']);
            $seat_details_formated['cabin']['lastRow'] = array(@end($parse_seat_formated_response)['seatRowNumbers']);
            $seat_details_formated['cabin']['classLocation'] = array();
            $seat_details_formated['cabin']['seatOccupationDefault'] = array();
            $seat_details_formated['cabin']['seatColumn'] = @current($parse_seat_formated_response)['seatColumn'];
            $seat_details_formated['cabin']['columnCharacteristic'] = isset($parse_seat_formated_response[0]['columnCharacteristic'])?$parse_seat_formated_response[0]['columnCharacteristic']:current($parse_seat_formated_response)['columnCharacteristic'];
            $seat_details_formated['row'] = $parse_seat_formated_response;
            $response[$trip] = $seat_details_formated;
            // debug( $seat_details_formated['cabin']['columnCharacteristic']); exit;
        }
        $response =array();
        $response['journey'] = 0;
        $response['segment'] = $se;
        $response['airSeatMapDeatils'] = $seat_details_formated;
        return $response;

    }

    
    public function air_create_reservation_request($app_reference, $params, $seat_no='') {
        error_reporting(-1);
        $response ['data'] = array ();
        $response ['status'] = FAILURE_STATUS;
        
        $params=$params['book_attributes'];
        $pricing_xml = $params['pricing_xml'];
        
        
        $booking_source = $params ['booking_source'];       
        $passenger_title_arr = $params ['name_title'];
        $passenger_first_name_arr = $params ['first_name'];
        $passenger_last_name_arr = $params ['last_name'];
        $passenger_date_of_birth_arr = $params ['date_of_birth'];
        $passenger_passport_number_arr = $params ['passenger_passport_number'];
        $passenger_passport_issuing_country_arr = $params ['passenger_passport_issuing_country'];
        $passenger_passport_expiry_date_arr = @$params ['passenger_passport_expiry_date'];
        $passenger_type_arr = $params ['passenger_type'];
        $lead_passenger_arr = $params ['lead_passenger'];
        $ffnum_arr= $params ['ffnumber'];
        $gender_arr = $params ['gender'];
        $passenger_nationality_arr = $params ['passenger_nationality'];
        
        $street = $params ['billing_address_1'];
        $city = $params ['billing_city'];
        $state = $params ['billing_city'];
        $billing_country = $params ['billing_country'];
        $billing_passenger_contact = $params ['passenger_contact'];
        $postal_code = $params ['billing_zipcode'];
        $billing_email = $params ['billing_email'];
        $payment_method = $params ['payment_method'];//air_seg_key
        //debug($params); die;
        //$flightdet = unserialized_data($params['token_key']);
        $flightdet = json_decode(base64_decode($params['flights_data']), true);
        
        
    
        
        $country = '';
        $sql = 'SELECT iso_country_code FROM `api_country_list` WHERE `origin` = ' . $billing_country;
        $CI = &get_instance ();
        $country = $CI->db->query ( $sql )->row ();
        $country = $country->iso_country_code;
        
        $Trace_ID = @$_SESSION['trace_id'];
        
        
        $holder_details ['first_name'] = $passenger_first_name_arr [0];
        $holder_details ['last_name'] = $passenger_last_name_arr [0];
        $holder_details ['dob'] = $passenger_date_of_birth_arr [0];
        $holder_details ['passport_no'] = $passenger_passport_number_arr [0];
        $holder_details ['passport_issue_country'] = $passenger_passport_issuing_country_arr [0];
        $holder_details ['passport_exp_date'] = $passenger_passport_expiry_date_arr [0];
        $holder_details ['passenger_type'] = $passenger_type_arr [0];
        $holder_details ['gender'] = get_enum_list ( 'gender', $gender_arr [0] );
        $holder_details ['pass_nationality'] = $passenger_nationality_arr [0];
        #debug($params); die;
        $address = '<Address>
                    <AddressName>' . $holder_details ['first_name'] . '</AddressName>
                    <Street>' . $street . '</Street>
                    <City>' . $city . '</City>
                    <State>' . $state . '</State>
                    <PostalCode>' . $postal_code . '</PostalCode>
                    <Country>' . $country . '</Country>
                </Address>';
            
        $flights_data = json_decode(base64_decode($params['flights_data']), true);
        $token_data =  unserialized_data($flights_data['Token']);
        //debug(unserialized_data($token_data)); die;
        $connection = $token_data['0'] ['connection'];
        
        $flightdet = json_decode(base64_decode($pricing_xml), true);
        //echo "2334 flightdet 3444";debug($flightdet); die;        
        $air_price_xml = str_replace ( '<?xml version="1.0"?>', '', str_replace ( 'air:', '', $flightdet ['price_xml'] ) ); 

        $air_price_xml = str_replace ( '</AirPricingSolution>
</AirPricingSolution>', '</AirPricingSolution>', $air_price_xml );   // added by bhola

        // search criteria
        $search_data = $this->search_data ( $params ['search_id'] );                
        $deptaure_time = explode('T', $search_data['data']['depature']);    
        $search_data = $search_data ['data'];
        #debug($token_data); die;
        $total_price = $flights_data['FareDetails']['b2c_PriceDetails']['TotalFare'];
        $currency = $flights_data['FareDetails']['b2c_PriceDetails'] ['Currency'];
        $seat_no = json_decode($seat_no, true);

        // create request
        $paxId =0;
        $adults = '';
        $adult_config = $search_data ['adult_config'];

        for($i = 0; $i < $adult_config; $i ++) {
            $gender_pax = get_enum_list ( 'gender', $gender_arr [$i] ); 
            $gender_pax = substr ( $gender_pax, 0, 1 );         
            if (substr ( $gender_pax, 0, 1 ) == "F") {
                $prefix = 'Prefix="Ms"';
                $adult_Genderp = "F";
            } else {
                $prefix = 'Prefix="Mr"';
                $adult_Genderp = "M";
            }
        
        $passport_ref_key = $flightdet['air_seg_key'][0];
        $air_line_pcode = $flightdet['flights'][0][0]['carrier'];
        $cnt_seg = count($flightdet['air_seg_key']);

        $seat_req ='';
        for($s=0;$s<$cnt_seg; $s++){
            if(!empty($seat_no[$s][$i])){
            $seat_no[$s][$i] = str_replace(' ', '-', $seat_no[$s][$i]);
        $seat_req .='<SpecificSeatAssignment xmlns="http://www.travelport.com/schema/air_v41_0" BookingTravelerRef="'.$paxId.'" SegmentRef="'.$flightdet['air_seg_key'][$s].'" SeatId="'.$seat_no[$s][$i].'"/>';    
            }
            
        }   

        $ssr_a ='';
        for($x=0; $x<$cnt_seg; $x++){
        $ssr_a .= '<SSR Carrier="'.$flightdet['flights'][0][$x]['carrier'].'" SegmentRef="'.$flightdet['air_seg_key'][0][$x].'" FreeText="P/IN/' . $passenger_passport_number_arr [$i] . '/IN' .  '/' . date ( 'dMy', strtotime ( str_replace ( '/', '-', $passenger_date_of_birth_arr [$i] ) ) ) . '/' . $adult_Genderp . '/' . date ( 'dMy', strtotime ( str_replace ( '/', '-', $params ['passenger_passport_expiry_year'][$i].'-'.$params ['passenger_passport_expiry_month'][$i].'-'.$params ['passenger_passport_expiry_day'][$i]))) . '/' . $holder_details ['last_name'] . '/' . $holder_details ['first_name'] . '" Status="HK" Type="DOCS"/>';  

        if(!empty($ffnum_arr[$i])){
    $ssr_a .='<SSR Key="'.$flightdet['air_seg_key'][0][$x].'" Type="FQTV" Status="HK" Carrier="'.$flightdet['flights'][0][$x]['carrier'].'" FreeText="'.$ffnum_arr[$i].'-'.$passenger_last_name_arr [$i].'/'. $passenger_first_name_arr [$i].'"/>';   
    }               
        }
        
                
        $adults .= '<BookingTraveler xmlns="http://www.travelport.com/schema/common_v41_0" Key="' . $paxId . '" TravelerType="ADT" DOB="' . date ( 'Y-m-d', strtotime ( str_replace ( '/', '-', $passenger_date_of_birth_arr [$i] ) ) ) . '" Gender="' . $gender_pax . '"   >
                    <BookingTravelerName ' . $prefix . ' First="' . $passenger_first_name_arr [$i] . '" Last="' . $passenger_last_name_arr [$i] . '" />
                    <PhoneNumber Number="' . $billing_passenger_contact . '" Type="Mobile" ></PhoneNumber>
                    <Email EmailID="' . $billing_email . '" Type="P" />
                    ' . $ssr_a . '    
                    ' . $address . '
                </BookingTraveler>';                

                 $paxId++;
        }
        
        
 
        // infant
        $infants = '';
        $infant_config = $search_data ['infant_config'];
        $i=0;
        foreach($params['passenger_type'] as $type){
            if($type == 'Infant'){
                $infant_var = $i;
            }
            $i++;
        }
        
        for($k = 0; $k < $infant_config; $k ++) {
            $gender_pax = get_enum_list('gender', $gender_arr[$paxId]);
            $gender_pax = substr($gender_pax, 0, 1 );
            
            if (substr($gender_pax, 0, 1 ) == "F") {
                $prefix = 'Prefix="Miss"';
                $infant_Genderp = "FI";
            } else {
                $prefix = 'Prefix="Mstr"';
                $infant_Genderp = "MI";
            }

            //$age = get_age(str_replace('/','-',$passenger_date_of_birth_arr[$paxId]),date('Y-m-d'));
            $age ='2';  
            $cnt_seg = count($flightdet['air_seg_key']);
            $ssr_i ='';
            for($x=$infant_var; $x<$cnt_seg; $x++){
                $ssr_i = '<SSR  Carrier="'.$flightdet['flights'][$x]['carrier'].'" SegmentRef="'.$flightdet['air_seg_key'][$x].'" FreeText="P/IN'  . '/' . @$passenger_passport_number_arr[$paxId] . '/IN'  . '/' . date ('dMy', strtotime (str_replace('/','-',$passenger_date_of_birth_arr[$paxId]))) . '/' . $infant_Genderp . '/' . date ( 'dMy', strtotime ( str_replace ( '/', '-', $params ['passenger_passport_expiry_year'][$i].'-'.$params ['passenger_passport_expiry_month'][$i].'-'.$params ['passenger_passport_expiry_day'][$i])))  . '/' . @$passenger_last_name_arr[$paxId] . '/' . $passenger_first_name_arr[$paxId] . '"  Status="HK"  Type="DOCS"/> ';
            }
            
            $infants .= '<BookingTraveler Key="' . $paxId . '" TravelerType="INF" xmlns="http://www.travelport.com/schema/common_v41_0">
                            <BookingTravelerName ' . $prefix . ' First="' . $passenger_first_name_arr[$paxId] . '"  Last="' . $passenger_last_name_arr[$paxId] . '" ></BookingTravelerName>
                            <PhoneNumber Number="' . $billing_passenger_contact . '" Type="Mobile" ></PhoneNumber>
                            <Email EmailID="' . $billing_email . '" Type="P" ></Email>
                            ' . $ssr_i . '  
                            <NameRemark Category="AIR">
                                <RemarkData>'.date ('dMy', strtotime (str_replace('/','-',$passenger_date_of_birth_arr[$x]))).'</RemarkData>
                            </NameRemark>  
                         </BookingTraveler>';
        #echo $infants; die;        
//'.date ('dMy', strtotime (str_replace('/','-',$passenger_date_of_birth_arr[$x]))).'               
            $paxId ++;
        }

        // child
        $childs = '';
        $child_config = $search_data ['child_config']; 
        for($j = 0; $j < $child_config; $j ++) {
            $gender_pax = get_enum_list('gender', $gender_arr[$paxId]);
            $gender_pax = substr($gender_pax, 0, 1 );
            foreach($params['passenger_type'] as $type){
            if($type == 'Child'){
                $child_var = $j;
            }
            $j++;
            }   
        if (substr($gender_pax, 0, 1 ) == "F") {
                $prefix = 'Prefix="Miss"';
                $child_Genderp = "F";
            } else {
                $prefix = 'Prefix="Mstr"';
                $child_Genderp = "M";
            }
            
        $cnt_seg = count($flightdet['air_seg_key']);        
        for($s=0;$s<$cnt_seg; $s++){
            if(!empty($seat_no[$s][$child_var])){
            $seat_no[$s][$child_var] = str_replace(' ', '-', $seat_no[$s][$child_var]);
        $seat_req .='<SpecificSeatAssignment xmlns="http://www.travelport.com/schema/air_v41_0" BookingTravelerRef="'.$paxId.'" SegmentRef="'.$flightdet['air_seg_key'][$s].'" SeatId="'.$seat_no[$s][$child_var].'"/>';    
            }
            
        }
            //$age = get_age_depar(str_replace('/','-',$passenger_date_of_birth_arr[$paxId]),$deptaure_time[0]);
        $age = '13';
        $cnt_seg = count($flightdet['air_seg_key']);
        $ssr_c ='';
        for($x=0; $x<$cnt_seg; $x++){ 
            $ssr_c = '<SSR Carrier="'.$flightdet['flights'][$x]['carrier'].'" SegmentRef="'.@$flightdet['air_seg_key'][$x].'" FreeText="P/IN/' . $passenger_passport_number_arr [$paxId] . '/IN' .  '/' . date ( 'dMy', strtotime ( str_replace ( '/', '-', $passenger_date_of_birth_arr [$paxId] ) ) ) . '/' . $child_Genderp . '/' . date ( 'dMy', strtotime ( str_replace ( '/', '-', $params ['passenger_passport_expiry_year'][$i].'-'.$params ['passenger_passport_expiry_month'][$i].'-'.$params ['passenger_passport_expiry_day'][$i]))) . '/' . $passenger_last_name_arr[$paxId] . '/' . $passenger_first_name_arr[$paxId] . '" Status="HK" Type="DOCS"/>';
    
        }
            $childs .= '<BookingTraveler  Key="' . $paxId . '" TravelerType="CNN"  xmlns="http://www.travelport.com/schema/common_v41_0">
                            <BookingTravelerName ' . $prefix . ' First="' . $passenger_first_name_arr[$paxId] . '"  Last="' . $passenger_last_name_arr[$paxId] . '"  ></BookingTravelerName>
                            <PhoneNumber Number="' . $billing_passenger_contact . '" Type="Mobile" ></PhoneNumber>
                            <Email EmailID="' . $billing_email . '" Type="P" ></Email>
                                ' . $ssr_c . '
                            <NameRemark Category="AIR">
                                <RemarkData>P-C' . $age . '</RemarkData>
                            </NameRemark>  
                         </BookingTraveler>';
            $paxId ++;
            // NEED TO FIX
            // $child_patch .= '<SearchPassenger Code="CNN" Age="10" xmlns="http://www.travelport.com/schema/common_v41_0" />';
        }
    
        #debug($air_price_xml); die;    
                
        $agency = ' <ContinuityCheckOverride xmlns="http://www.travelport.com/schema/common_v41_0" Key="1G">true</ContinuityCheckOverride> <AgencyContactInfo xmlns="http://www.travelport.com/schema/common_v41_0">
    <PhoneNumber Type="Agency" Location="BLR" CountryCode="0091" AreaCode="11" Number="4536845" Text="ITQ INDIA"/>
   </AgencyContactInfo>';

         
        $payment = '<FormOfPayment xmlns="http://www.travelport.com/schema/common_v41_0"  Type="Cash"></FormOfPayment>';
        $ActionStatus = '<ActionStatus ProviderCode="1G" TicketDate="T*" Type="ACTIVE" QueueCategory="01" xmlns="http://www.travelport.com/schema/common_v41_0" ></ActionStatus>';
    
        
        // $air_price_xml = str_replace('Endorsement','common_v41_0:Endorsement',$air_price_xml);
        $AirCreateReservationReq = '<?xml version="1.0" encoding="utf-8"?>
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                        <s:Header/>
                        <s:Body>
                          <univ:AirCreateReservationReq xmlns:common_v41_0="http://www.travelport.com/schema/common_v41_0" xmlns:univ="http://www.travelport.com/schema/universal_v41_0" AuthorizedBy="user" RetainReservation="None"  xmlns="http://www.travelport.com/schema/air_v41_0" TraceId="'.$Trace_ID.'" TargetBranch="'.$this->target_branch.'">
                            <BillingPointOfSaleInfo  xmlns="http://www.travelport.com/schema/common_v41_0" OriginApplication="UAPI" ></BillingPointOfSaleInfo>
                            ' . $adults . '
                            ' . $infants . ' 
                            ' . $childs . '
                            '. $agency .'
                            ' . $payment . '
                            ' . $air_price_xml[0] . '                            
                            ' . $ActionStatus . '
                            '.$seat_req.'
                            </univ:AirCreateReservationReq>
                        </s:Body>
                        </s:Envelope>';

        $response ['status'] = SUCCESS_STATUS;
        $response ['data'] ['request'] = $AirCreateReservationReq;

        /*Creating XML Log*/
        $xml_title = 'AirCreateReservationReq';
        $date = date('Ymd_His');
        if (!file_exists('xml_logs/Flight')) {
        mkdir('xml_logs/Flight', 0777, true);
        }
        $requestcml = $xml_title.'.xml';
        file_put_contents('../b2b2b/xml_logs/Flight/'.$requestcml, $AirCreateReservationReq);
        /*End OF Creating XML Log*/
        return $response;
    }

    function air_ticket_create_reservation_request($params,$locatorpnr){
        $AirTicketCreateReservationReq = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://www.travelport.com/schema/common_v41_0" xmlns:univ="http://www.travelport.com/schema/universal_v41_0">
<soapenv:Header/>
<soapenv:Body>
<air:AirTicketingReq xmlns:air="http://www.travelport.com/schema/air_v41_0" BulkTicket="false" ReturnInfoOnFail="true" TargetBranch="'.$this->target_branch.'">
<com:BillingPointOfSaleInfo OriginApplication="UAPI"/>
<air:AirReservationLocatorCode>'.$locatorpnr.'</air:AirReservationLocatorCode>
</air:AirTicketingReq>
</soapenv:Body>
</soapenv:Envelope>';
        $response ['status'] = SUCCESS_STATUS;
        $response ['data'] ['request'] = $AirTicketCreateReservationReq;

        /*XML Logs*/
        $xml_title = 'AirTicketCreateReservationReq';
        $date = date('Ymd_His');
        if (!file_exists('xml_logs/Flight')) {
                 mkdir('xml_logs/Flight', 0777, true);
        }
        $requestcml = $xml_title.'_request_'.$date.'_'.date('Y-m-d').'.xml';
        file_put_contents('../b2b2b/xml_logs/Flight/'.$requestcml, $AirTicketCreateReservationReq);
        /*End Of XML Logs*/
        //echo 'request'; debug($AirCreateReservationReq); exit();
        return $response;
    }
    /**
     * Format Search data response
     */
    private function format_search_data_response($low_fare_search_res, $search_data, $jkey, $currency_obj,$module='') {
        $flight_data_list = array ();       
        $low_fare_search_res = $low_fare_search_res ['SOAP:Envelope'] ['SOAP:Body'];        
        // If error is there then return false
        if (isset ( $low_fare_search_res ['SOAP:Fault'] )) {
            return false;
        }
        // debug($low_fare_search_res['air:LowFareSearchRsp']['air:AirSegmentList']);exit;
        $currency_type = $low_fare_search_res ['air:LowFareSearchRsp'] ['@attributes'] ['CurrencyType'];
        
        // air:AirPricingSolution TO FIX - create function for airpricing solution
        $air_flight_list = $low_fare_search_res ['air:LowFareSearchRsp'] ['air:FlightDetailsList'];
        $air_pricing_solutions = $low_fare_search_res ['air:LowFareSearchRsp'] ['air:AirPricingSolution'];
        
        $air_segment_list = @$low_fare_search_res ['air:LowFareSearchRsp'] ['air:AirSegmentList'] ['air:AirSegment'];
        $air_fare =$low_fare_search_res['air:LowFareSearchRsp']['air:FareInfoList'];

        $air_fare_info = $low_fare_search_res['air:LowFareSearchRsp']['air:FareInfoList']['air:FareInfo'];

        if (isset ( $air_pricing_solutions ) && valid_array ( $air_pricing_solutions )) {
            foreach ( $air_pricing_solutions as $ap_key => $pricing_array ) {
                $flight_data_list [] = $this->format_air_pricing_solution ( $air_fare_info, $pricing_array, $air_segment_list, $search_data, $currency_obj, $module);
            }
        }
        #debug($flight_data_list); die;
        $response = $flight_data_list;
        #debug($response); die;
        return $response;
    }
    
    /**
     * array $pricing_array
     */
    function format_air_pricing_solution($air_fare_info, $pricing_array, $air_segment_list, $search_data, $currency_obj,$module='') {

        $currency_obj = new Currency(array('module_type' => 'flight','from' =>"INR", 'to' => get_application_currency_preference()));
        $flight_journey = array ();
        $air_fare_key;
        $journey_detail = force_multple_data_format ( @$pricing_array ['air:Journey'] );

        $air_pricing_info = @$pricing_array ['air:AirPricingInfo'];
        $class_details=@$air_pricing_info['air:BookingInfo'][0]['@attributes']['CabinClass']; 
        $connection_arr = force_multple_data_format ( @$pricing_array ['air:Connection'] );
        //echo 'test'; debug(); exit();
        $connection = '';
        if (isset ( $connection_arr ) && valid_array ( $connection_arr )) {
            foreach ( $connection_arr as $c_key => $connect_flight ) {
                if (isset ( $connect_flight ['@attributes'] ['SegmentIndex'] )) {
                    $connection .= @$connect_flight ['@attributes'] ['SegmentIndex'] . ",";
                } else {
                    $connection .= @$connect_flight ['SegmentIndex'] . ",";
                }
            }
        }
        

    $FareInfoRefKey = @$pricing_array['air:AirPricingInfo']['air:FareInfoRef']['@attributes']['Key'];
//debug($air_segment_list); die;
        if(isset($air_fare_info) && valid_array($air_fare_info)) {
$i=0;
            foreach($air_fare_info as $app_key => $fare_info) {
                                
                $FareInfoRef[$fare_info['air:FareRuleKey']['@attributes']['FareInfoRef']]  = $fare_info['air:FareRuleKey']['@value']."&&".$fare_info['air:FareRuleKey']['@attributes']['FareInfoRef']."&&".$fare_info['air:FareRuleKey']['@attributes']['ProviderCode'];

                $baggage_info= $fare_info['air:BaggageAllowance']['air:MaxWeight']['@attributes']; 
                $flight_journey['Fareinfo'] = $fare_info['air:FareRuleKey']['@value']; 
                $flight_journey['FareInfoList']= $fare_info;
                //$flight_journey['air_segment_list']= $air_segment_list[$i]; 
                
                $i++;
            }
        }
        #debug($flight_journey); die;
        #$flight_journey['FareInfoContent'] = $air_fare_info;
        $flight_journey ['ProvabAuthKey'] = '';
        $flight_journey ['ResultIndex'] = '';
        $flight_journey ['Source'] = '';
        $flight_journey ['IsLCC'] = '';
        $flight_journey ['AirlineRemark'] = '';  

        $flight_journey ['connections'] = $connection;
        $flight_journey ['route_id'] = $pricing_array ['@attributes'] ['Key'];      
        $flight_journey ['vendor'] = '';
        
        //added princess
        if(isset($air_pricing_info[0])){
            //change this two line later            
        $flight_journey ['provider_code'] = @$air_pricing_info [0] ['@attributes'] ['ProviderCode'];
        $flight_journey ['air_pricing_key'] = @$air_pricing_info[0]  ['@attributes']['Key'];    
        $flight_journey['air_fare_key'] = $air_fare_key = !empty(@$air_pricing_info[0]['air:FareInfoRef']['@attributes']['Key'])?@$air_pricing_info[0]['air:FareInfoRef']['@attributes']['Key']:@$air_pricing_info[0]['air:FareInfoRef'][0]['@attributes']['Key'];  
        }else{
        $flight_journey ['provider_code'] = @$air_pricing_info ['@attributes'] ['ProviderCode'];
        $flight_journey ['air_pricing_key'] = @$air_pricing_info ['@attributes'] ['Key'];   
        $flight_journey['air_fare_key'] = $air_fare_key = !empty(@$air_pricing_info['air:FareInfoRef']['@attributes']['Key'])?@$air_pricing_info['air:FareInfoRef']['@attributes']['Key']:@$air_pricing_info['air:FareInfoRef'][0]['@attributes']['Key'];
        }           
        $flight_journey ['pricing_xml_data'] = $pricing_array; 
        //debug($flight_journey); die;
        //$flight_journey ['FareInfoRef'] = $FareInfoRef; 
        
    
        
        // Built price array like travelfusion response
        /*MY CODEING*/
        //error_reporting(E_ALL);
        //$this->CI->library ('common_flight' );

# commented by Bhola */
        /*$admin_details = $this->CI->flight_model->get_travelport_flight_admin_markup_details ();
        echo "Markup ";debug($admin_details);exit;
        if ($admin_details['status'] == '1') {
            $markup_type = $admin_details['markup']['value_type'];
            $markup_val = $admin_details['markup']['value'];
            $total_fare = substr ( @$pricing_array ['@attributes'] ['TotalPrice'], 3 );
            $tax = substr ( @$pricing_array ['@attributes'] ['ApproximateTaxes'], 3 );
            $base_price = substr ( $pricing_array ['@attributes'] ['ApproximateBasePrice'], 3 );
            $multiplier = $search_data['total_pax'];

        $display_price_markup = $this->CI->flight_model->travelport_markup ($markup_type,$markup_val,$total_fare,$multiplier);
        $total_tax_markup = $this->CI->flight_model->travelport_markup_tax ($markup_type,$markup_val,$tax,$multiplier);
        $approx_base_price_markup = $this->CI->flight_model->travelport_markup ($markup_type,$markup_val,$base_price,$multiplier);
        }*/
        
# End commented by Bhola */

        //echo 'debug'; debug($display_price_markup); debug($total_tax_markup); debug($approx_base_price_markup); exit();
        /*NEW CODING END*/
        $currency = substr ( $pricing_array ['@attributes'] ['TotalPrice'], 0, 3 );
        $display_price = substr ( @$pricing_array ['@attributes'] ['TotalPrice'], 3 ) + @$display_price_markup;
        $total_tax = substr ( @$pricing_array ['@attributes'] ['ApproximateTaxes'], 3 ) + @$total_tax_markup;
        $approx_base_price = substr ( $pricing_array ['@attributes'] ['ApproximateBasePrice'], 3 ) + @$approx_base_price_markup;
        
        //$currency_obj = new Currency(array('module_type' => 'fligt', 'from' => get_application_default_currency(), 'to' => get_application_transaction_currency_preference()));
        // $currency_obj->getConversionRate(true,$currency,get_application_transaction_currency_preference());
        
        // $converted_display_price = $currency_obj->force_currency_conversion($display_price);
        // $currency = @$converted_display_price['default_currency'];
        // $display_price = round(@$converted_display_price['default_value']);
        
        // $converted_approximate_taxes = $currency_obj->force_currency_conversion($total_tax);
        // $total_tax = round(@$converted_approximate_taxes['default_value']);
        
        // $converted_approximate_base = $currency_obj->force_currency_conversion($approx_base_price);
        // $approx_base_price = round(@$converted_approximate_base['default_value']);

# Start TBO Format Data #   

       // debug($currency_obj);die(" pricing format");
        $currency_symbol = $currency_obj->get_currency_symbol($currency_obj->to_currency);
//echo "module== ";debug($module);exit;
        $myconversionvalue = $currency_obj->getConversionRate(TRUE);

        $b2c_price = $display_price * $myconversionvalue;
        $CI =& get_instance();
        $CI->load->library('master_currency');
        $markup_with_price = $CI->master_currency->get_currency($b2c_price);

        $markup = round($markup_with_price['default_value'] - $b2c_price);

    if($module == 'b2b') 
    {
        $b2b_PriceDetails['BaseFare']           = $approx_base_price * $myconversionvalue;
        $b2b_PriceDetails['_CustomerBuying']    = $display_price * $myconversionvalue;
        $b2b_PriceDetails['_AgentBuying']       = $display_price * $myconversionvalue;
        $b2b_PriceDetails['_AdminBuying']       = '';
        $b2b_PriceDetails['_Markup']            = '';
        $b2b_PriceDetails['_AgentMarkup']       = '';
        $b2b_PriceDetails['_AdminMarkup']       = '';
        $b2b_PriceDetails['_Commission']        = '';
        $b2b_PriceDetails['_tdsCommission']     = '';
        $b2b_PriceDetails['_AgentEarning']      = '';
        $b2b_PriceDetails['_TaxSum']            = $total_tax * $myconversionvalue;
        $b2b_PriceDetails['_BaseFare']          = $approx_base_price * $myconversionvalue;
        $b2b_PriceDetails['_TotalPayable']      = $display_price * $myconversionvalue;
        $b2b_PriceDetails['Currency']           = $currency;
        $b2b_PriceDetails['CurrencySymbol']     = $currency_symbol;

        $flight_journey ['FareDetails'] ['b2b_PriceDetails']= $b2b_PriceDetails;
    }
    else{

        $b2c_PriceDetails ['BaseFare'] = $approx_base_price * $myconversionvalue;
        $b2c_PriceDetails ['TotalTax'] = $total_tax * $myconversionvalue + $markup;
      	$b2c_PriceDetails ['TotalFare'] = $display_price * $myconversionvalue + $markup;
        $b2c_PriceDetails ['PublishedFare'] = $display_price * $myconversionvalue;
        $b2c_PriceDetails ['PublishedTax'] = $total_tax * $myconversionvalue;
        $b2c_PriceDetails ['Markup'] = $markup;

        $b2c_PriceDetails ['Currency'] = $currency;
        $b2c_PriceDetails ['CurrencySymbol'] = $currency_symbol;

        $flight_journey ['FareDetails'] ['b2c_PriceDetails']= $b2c_PriceDetails;
    }

        $api_PriceDetails ['Currency'] = $currency;
        $api_PriceDetails ['BaseFare'] = $approx_base_price * $myconversionvalue;
        $api_PriceDetails ['Tax'] = $total_tax * $myconversionvalue;
        $api_PriceDetails ['PublishedFare'] = $display_price * $myconversionvalue; 

        $api_PriceDetails ['YQTax'] = '';
        $api_PriceDetails ['AdditionalTxnFeeOfrd'] = '';
        $api_PriceDetails ['AdditionalTxnFeePub'] = '';
        $api_PriceDetails ['OtherCharges'] = '';
        $api_PriceDetails ['AgentCommission'] = '';
        $api_PriceDetails ['PLBEarned'] = '';
        $api_PriceDetails ['IncentiveEarned'] = '';
        $api_PriceDetails ['OfferedFare'] = '';
        $api_PriceDetails ['TdsOnCommission'] = '';
        $api_PriceDetails ['TdsOnPLB'] = '';
        $api_PriceDetails ['TdsOnIncentive'] = '';
        $api_PriceDetails ['ServiceFee'] = '';

        
        $flight_journey ['FareDetails'] ['api_PriceDetails']= $api_PriceDetails;

# END TBO Format Data # 

        #$flight_journey ['+'] ['api_currency'] = $currency;
        #$flight_journey ['price'] ['api_total_display_fare'] = $display_price;
        #$flight_journey ['price'] ['total_breakup'] ['api_total_tax'] = $total_tax;
        #$flight_journey ['price'] ['total_breakup'] ['api_total_fare'] = $approx_base_price;
        #$flight_journey ['price'] ['pax_breakup'] = array ();
        
        #$flight_journey ['fare'] [0] = $flight_journey ['price'];
        
        $flight_key_list = array ();
        $flight_detail_arr = array ();
        /* air journey flight key for onward and return gropu */
        
        if (isset ( $journey_detail ) && valid_array ( $journey_detail )) {
            $flight_detail_key = 0;
            
            foreach ( $journey_detail as $j_key => $journey_flight ) {
                
                if ($j_key == 0) {
                    $trip_type = 'onward';
                } else {
                    $trip_type = 'return';
                }
                
                $air_segment_ref = force_multple_data_format ( @$journey_flight ['air:AirSegmentRef'] );
                
                if (isset ( $air_segment_ref ) && valid_array ( $air_segment_ref )) {
                    foreach ( $air_segment_ref as $seg_key => $segmnt ) {
                        if (isset ( $air_segment_list ) && ! empty ( $air_segment_list )) {
                            foreach ( $air_segment_list as $as_key => $segment ) {
                                $air_segment_arr ['flight_detail_ref'] = $flight_detail_key = @$segment ['air:FlightDetailsRef'] ['@attributes'] ['Key'];
                                $air_code_share_info = @$segment ['air:CodeshareInfo'];
                                $is_leg = false;
                                $segment_ref = @$segment ['@attributes'] ['Key'];
                                // if(isset($air_segment_ref) && valid_array($air_segment_ref)) {
                                // foreach($air_segment_ref as $seg_key => $segmnt) {
                                
                                $flight_key = @$segmnt ['@attributes'] ['Key'];
                                $flight_detail_arr = array ();
                                if ($flight_key == $segment_ref) {
                                    if (isset ( $air_code_share_info ) && valid_array ( $air_code_share_info )) {

                                        $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['air_code_share'] = @$air_code_share_info ['@attributes'] ['OperatingCarrier'];
                                    }

                                    // booking_code
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['booking_counts'] = @$segment ['air:AirAvailInfo'] ['air:BookingCodeInfo'] ['@attributes'] ['BookingCounts'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['group'] = @$segment ['@attributes'] ['Group'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['carrier'] = @$segment ['@attributes'] ['Carrier'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['flight_number'] = @$segment ['@attributes'] ['FlightNumber'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['origin'] = @$segment ['@attributes'] ['Origin'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['destination'] = @$segment ['@attributes'] ['Destination'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['departure_time'] = @$segment ['@attributes'] ['DepartureTime'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['arrival_time'] = @$segment ['@attributes'] ['ArrivalTime'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['flight_time'] = @$segment ['@attributes'] ['FlightTime'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['distance'] = @$segment ['@attributes'] ['Distance'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['e_ticketability'] = @$segment ['@attributes'] ['ETicketability'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['equipment'] = @$segment ['@attributes'] ['Equipment'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['change_of_plane'] = @$segment ['@attributes'] ['ChangeOfPlane'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['participant_level'] = @$segment ['@attributes'] ['ParticipantLevel'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['link_availability'] = @$segment ['@attributes'] ['LinkAvailability'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['polled_availability_option'] = @$segment ['@attributes'] ['PolledAvailabilityOption'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['optional_services_indicator'] = @$segment ['@attributes'] ['OptionalServicesIndicator'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['availability_source'] = @$segment ['@attributes'] ['AvailabilitySource'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['flight_seg_key'] = $flight_key;
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['provider_code'] = @$segment ['air:AirAvailInfo'] ['@attributes'] ['ProviderCode'];
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['flight_detail_key'] = $flight_detail_key;
                                    
                                    $flight_list_on_out ['flight_list'] [$j_key] ['flight_detail'] [$seg_key] ['segmentindex']= @$connection_arr[$seg_key]['@attributes']['SegmentIndex'];
                                    $flight_list_on_out ['connection'] = $connection;
                                    $flight_list_on_out ['price'] ['api_currency'] = $currency;
                                    $flight_list_on_out ['price'] ['api_total_display_fare'] = $display_price;
                                    $flight_list_on_out ['price'] ['total_breakup'] ['api_total_tax'] = $total_tax;
                                    $flight_list_on_out ['price'] ['total_breakup'] ['api_total_fare'] = $approx_base_price;

            //Added by Bhola for Markup                             
                                    $total_pax = $search_data['adult']+$search_data['child']+$search_data['infant'];
                                    $total_fare = $display_price;
                                    
                                    $flight_journey ['FareDetails']['b2c_PriceDetails']['Admin_Markup'] = $this->get_admin_markup_details(@$segment ['@attributes'] ['Carrier'], $total_fare, $total_pax,$myconversionvalue);
            //End added by Bhola for Markup
                                if($trip_type =='onward'){
                                    $flight_journey ['SegmentDetails'][0][] = $this->flight_detail_format ( $segment, $flight_key, $trip_type, $is_leg,$class_details,$baggage_info['Value'].$baggage_info['Unit']);
                                }elseif($trip_type == 'return'){
                                    $flight_journey ['SegmentDetails'][1][] = $this->flight_detail_format ( $segment, $flight_key, $trip_type, $is_leg,$baggage_info['Value'].$baggage_info['Unit'] );

                                }
                                    //$flight_journey ['SegmentDetails'] [0] [] = $this->flight_detail_format ( $segment, $flight_key, $trip_type, $is_leg );
                                    
                                    $flight_key_list [$j_key . '-' . $seg_key] = $flight_key;
                                    $flight_detail_key ++;
                                }
                                // }
                                // }
                            }
                        }
                    }
                }
                
                // $flight_journey['flight_details']['flight_list'][$j_key]['travel_time'] = $journey_flight['@attributes']['TravelTime'];
            }
        }
        //echo 'flight_list_on_out'; debug($flight_list_on_out); exit();
        // Booking info
        if (isset ( $air_pricing_info ) && valid_array ( $air_pricing_info )) {
            $booking_details_flight = force_multple_data_format ( @$air_pricing_info ['air:BookingInfo'] );
            
            if (isset ( $booking_details_flight ) && valid_array ( $booking_details_flight )) {
                foreach ( $booking_details_flight as $b_fkey => $booking ) {
                    $key_index = array_search ( $booking ['@attributes'] ['SegmentRef'], $flight_key_list );
                    $explode_key = explode ( '-', $key_index );


                    if($trip_type =='onward'){
                        $siii = 0;
                    }elseif($trip_type == 'return'){
                        $siii = 1;
                    }

                    /*$flight_journey ['SegmentDetails'] [$siii] [$b_fkey] ['operator_class'] = $booking ['@attributes'] ['CabinClass'];
                    $flight_journey ['SegmentDetails'] [$siii] [$b_fkey] ['class'] ['name'] = $booking ['@attributes'] ['CabinClass'];
                    $flight_journey ['SegmentDetails'] [$siii] [$b_fkey] ['class'] ['description'] = '';
                    $flight_journey ['SegmentDetails'] [$siii] [$b_fkey] ['fare_info_ref'] = $booking ['@attributes'] ['FareInfoRef'];
                    $flight_journey ['SegmentDetails'] [$siii] [$b_fkey] ['booking_code'] = $booking ['@attributes'] ['BookingCode'];
                    $flight_journey ['SegmentDetails'] [$siii] [$b_fkey] ['BookingCount'] = $booking ['@attributes'] ['BookingCount'];*/
                    
                    $flight_list_on_out ['flight_list'] [$explode_key [0]] ['flight_detail'] [$explode_key [1]] ['booking_code'] = $booking ['@attributes'] ['BookingCode'];
                    $flight_list_on_out ['flight_list'] [$explode_key [0]] ['flight_detail'] [$explode_key [1]] ['BookingCount'] = $booking ['@attributes'] ['BookingCount'];
                    $flight_list_on_out ['flight_list'] [$explode_key [0]] ['flight_detail'] [$explode_key [1]] ['cabin_class'] = $booking ['@attributes'] ['CabinClass'];
                    $flight_list_on_out ['flight_list'] [$explode_key [0]] ['flight_detail'] [$explode_key [1]] ['fare_info_ref'] = $booking ['@attributes'] ['FareInfoRef'];
                    if(array_key_exists($air_fare_key, $FareInfoRef)){
                        $flight_list_on_out['flight_list'][$explode_key[0]]['flight_detail'][$explode_key[1]]['fare_keyy'] = $FareInfoRef[$air_fare_key];
                    }
                }
            }
            
            // Tax info
            $tax_info = force_multple_data_format ( @$air_pricing_info ['air:TaxInfo'] );
            if (isset ( $tax_info ) && valid_array ( $tax_info )) {
                foreach ( $tax_info as $t_key => $taxes ) {
                    $flight_journey ['taxes'] [$t_key] ['category'] = $taxes ['@attributes'] ['Category'];
                    $flight_journey ['taxes'] [$t_key] ['amount'] = $taxes ['@attributes'] ['Amount'];
                }
            }
        }
        $token_data [0] = $flight_list_on_out;
        $flight_journey['Attr']        = 
            array(
              'IsRefundable' => '',
              'AirlineRemark' =>"",
            );
        $flight_journey ['Token'] = serialized_data ( $token_data );
        $flight_journey ['TokenKey'] = @md5 ( $flight_journey ['token'] );
        $flight_journey ['booking_source'] = 'PTBSID0000000016';
        
        // Create flight summery
        if (isset ( $flight_list_on_out ['flight_list'] ) && valid_array ( $flight_list_on_out ['flight_list'] )) {
            foreach ( $flight_list_on_out ['flight_list'] as $fl_direction => $list ) {
                $flight_journey ['SegmentSummary'] [] = $this->flight_segment_summary ( $list, $fl_direction );
            }
        }
        
        // $first_flight = current($flight_journey['flight_details']['details']);
        // $last_flight = end($flight_journey['flight_details']['details']);
        // $flight_journey['flight_details']['summery'] = $this->format_segment_summery($first_flight,$last_flight);
        // debug($first_flight);echo 'last';debug($last_flight);exit;
        // debug($flight_journey);echo '$$flight_journey';
        
        $flight_journey ['can_airlines'] = '';
        $flight_journey ['BookingType'] = '';
        #debug(unserialized_data ( $flight_journey ['Token'] )); die;
        return $flight_journey;
    }
    
    /**
     *
     * @param
     *          $flight_list
     * @param
     *          $direction
     */
    private function flight_segment_summary($flight_list, $direction) {
        $flight_list = @$flight_list ['flight_detail'];
        $first_flight = reset ( $flight_list );
        $last_flight = end ( $flight_list );
        
        $departure_time_ = date ( "H:i", strtotime ( $first_flight ['departure_time'] ) );
        $departure_date = date ( "d M", strtotime ( $first_flight ['departure_time'] ) );
        $arrival_time = date ( "H:i", strtotime ( $last_flight ['arrival_time'] ) );
        $arrival_date = date ( "d M", strtotime ( $last_flight ['arrival_time'] ) );
        
        $origin_code = $first_flight ['origin'];
        $destination_code = $last_flight ['destination'];
        
        /* get origin and destination name */
        $origine_name = $this->get_airport_city ( $origin_code );
        $destination_name = $this->get_airport_city ( $destination_code );
        
        $no_of_stops = count ( $flight_list ) - 1;
        $duration = calculate_duration ( $first_flight ['departure_time'], $last_flight ['arrival_time'] );
        $class = isset ( $first_flight ['cabin_class'] ) && ! empty ( $first_flight ['cabin_class'] ) ? $first_flight ['cabin_class'] : 'economy';
        $operator = @$first_flight ['carrier'];
        $flight_number = @$first_flight ['flight_number'];
        $flight_seg_key = @$first_flight ['flight_seg_key'];
        
        if(empty(@$this->get_airline_name($operator))== false){
            $operator_name = @$this->get_airline_name($operator);
        }else{
            $operator_name = $operator;
        }
        
        
        $summary = $this->format_summary_array ( $direction, $origin_code, $destination_code, $this->split_datetime_timezone($first_flight ['departure_time']), $this->split_datetime_timezone($last_flight ['arrival_time']), $operator, $operator_name, $flight_number, $no_of_stops, $class, $origine_name, $destination_name );
        
        return $summary;
    }

    private function flight_detail_format($segment, $flight_key, $trip_type, $is_leg,$class_details,$baggage_allow) {
        $duration_sec = calculate_duration ( @$segment ['@attributes'] ['DepartureTime'], @$segment ['@attributes'] ['ArrivalTime'] );
        $duration = get_duration_label ( @$duration_sec );
        
        $origine_name = $this->get_airport_city ( @$segment ['@attributes'] ['Origin'] );
        $destination_name = $this->get_airport_city ( @$segment ['@attributes'] ['Destination'] );
        
        $depart_date = date ( "d M Y", strtotime ( @$segment ['@attributes'] ['DepartureTime'] ) );
        $depart_time=substr($segment['@attributes']['DepartureTime'],11,16);
        $depart_time=substr($depart_time,0,5);
        //$depart_time = date ( "H:i", strtotime ( @$segment ['@attributes'] ['DepartureTime'] ) );
        /*
        echo "segment111";debug($segment['@attributes']['ArrivalTime']);
        echo "<pre>";*/
        $arrival_date = date ( "d M Y", strtotime ( @$segment ['@attributes'] ['ArrivalTime'] ) );
         $arrival_time=substr($segment['@attributes']['ArrivalTime'],11,16);
         $arrival_time=substr($arrival_time,0,5);

        if(empty($this->get_airline_name(@$segment ['@attributes'] ['Carrier']))== false){
            $operator_name = @$this->get_airline_name(@$segment ['@attributes'] ['Carrier']);
        }else{
            $operator_name = @$segment ['@attributes'] ['Carrier'];
        }
        
        $flight_detail_arr = array ();
        $flight_detail_arr ['Baggage'] = $baggage_allow;
        $flight_detail_arr ['CabinBaggage'] = ''; 
        $flight_detail_arr ['AvailableSeats'] = '';
        $flight_detail_arr ['TripIndicator'] = '';
        $flight_detail_arr ['SegmentIndicator'] = '';
        $flight_detail_arr ['journey_number'] = $trip_type;

    $flight_detail_arr ['AirlineDetails'] = array ( 
                'AirlineCode' => @$segment ['@attributes'] ['Carrier'],
                'AirlineName' => $operator_name,
                'FlightNumber' => @$segment ['@attributes'] ['FlightNumber'],
                'FareClass' => '', 
                'OperatingCarrier' => '',
                'FareClassLabel' => $class_details,
        ) // Derive
;
                
        $flight_detail_arr ['OriginDetails'] = array (
                'AirportCode' => @$segment ['@attributes'] ['Origin'],
                'AirportName' => $origine_name,
                'FlightNumber' => @$segment ['@attributes'] ['FlightNumber'],
                'Terminal' => '',
                'CityCode' => '', 
                'CountryCode' => '', 
                'CountryName' => '', 
                'DepartureTime' => $depart_date, 
                '_DepartureTime' => $depart_time, 
                '_DateTime' => $depart_time,
                'DateTime' => $depart_time, 
               /* 'fdtv' => strtotime ( $depart_time )*/ 
        ) // Derive
;
        
    $flight_detail_arr ['DestinationDetails'] = array (
                'AirportCode' => @$segment ['@attributes'] ['Destination'],
                'AirportName' => $destination_name,
                'Terminal' => @$segment ['@attributes'] ['ArrivalTime'],
                'CityCode' => '',
                'CityName' => '', 
                'CountryCode' => '', 
                'CountryName' => '', 
                'ArrivalTime' => $arrival_date, 
                '_ArrivalTime' => $arrival_time, 
                '_DateTime' => $arrival_time,
                'DateTime' => $arrival_time, 
                'fdtv' => strtotime ( $arrival_time ) 
        ) // Derive
;
        $flight_detail_arr ['SegmentDuration'] = @$duration;
        $flight_detail_arr ['Status'] = '';
        $flight_detail_arr ['Craft'] = '';
        $flight_detail_arr ['IsETicketEligible'] = '';

        $flight_detail_arr ['duration_seconds'] = $duration_sec;
        $flight_detail_arr ['duration'] = @$duration;
        
        $flight_detail_arr ['Group'] = @$segment ['@attributes'] ['Group'];
        $flight_detail_arr ['operator_code'] = @$segment ['@attributes'] ['Carrier'];
        $flight_detail_arr ['operator_name'] = '';
        $flight_detail_arr ['flight_number'] = @$segment ['@attributes'] ['FlightNumber'];
        $flight_detail_arr ['cabin_class'] = '';
        $flight_detail_arr ['is_leg'] = $is_leg;
    
        return $flight_detail_arr;
    }
    
    /*
     *
     * get airport city based on airport code
     */
    private function get_airport_city($airport_code) {
        $CI = & get_instance ();
        $airport_name = $CI->db_cache_api->get_airport_city_name ( array (
                'airport_code' => $airport_code 
        ) );
        $airport_name = $airport_name ['airport_city'];
        return $airport_name;
    }
    /*
     *
     * get airline name based on airport code
     */
    public function get_airline_name($airline_code) {
        $CI = & get_instance ();
        
        $airline_data = $CI->db_cache_api->get_airline_name ( array (
                'code' => $airline_code 
        ) );
        $airline_name = $airline_data ['name'];
        return $airline_name;
    }
    /**
     * flight low fare search request sync
     * 
     * @param array $search_data            
     */
    function flight_low_fare_search_req($search_data) {
        $response ['status'] = SUCCESS_STATUS;
        $response ['data'] = array (); 
        $search_data ['method'] = 'Sync';
        $Trace_ID = @$_SESSION['trace_id']; 
        if($search_data['carrier']!='')
        {

        $AirSearchModifiers = '<AirSearchModifiers>
                                    <PreferredProviders>
                                        <Provider Code="1G" xmlns="http://www.travelport.com/schema/common_v41_0" />
                                    </PreferredProviders>
                                    <PreferredCarriers>
                                         <Carrier Code="'.$search_data['carrier'].'" xmlns="http://www.travelport.com/schema/common_v41_0" /> 
                                    </PreferredCarriers>
                               </AirSearchModifiers>'; 
        }
        else
        {
            $AirSearchModifiers = '<AirSearchModifiers>
                                    <PreferredProviders>
                                        <Provider Code="1G" xmlns="http://www.travelport.com/schema/common_v41_0" />
                                    </PreferredProviders>
                               </AirSearchModifiers>';
        }
        
        
        /* paxes details */
        $infant = '';
        $adult = '';
        $child = '';
        for($i = 0; $i < $search_data ['adult']; $i ++) {
            $adult .= '<SearchPassenger Code="ADT" xmlns="http://www.travelport.com/schema/common_v41_0" />';
        }
        for($i = 0; $i < $search_data ['child']; $i ++) {
            $child .= '<SearchPassenger Code="CNN" Age="8" xmlns="http://www.travelport.com/schema/common_v41_0" />';
        }
        for($i = 0; $i < $search_data ['infant']; $i ++) {
            $infant .= '<SearchPassenger Code="INF" Age="01" PricePTCOnly="true" xmlns="http://www.travelport.com/schema/common_v41_0" />';
        }
        
        $provider = "";
        if ($search_data ['method'] == "Asynch") {
            $search_method = "LowFareSearchAsynchReq";
            $AsynchModifiers = '<AirSearchAsynchModifiers>
                                    <InitialAsynchResult MaxWait="02" />
                                </AirSearchAsynchModifiers>';
        } else {
            $search_method = "LowFareSearchReq";
            $AsynchModifiers = "";
        }
         
        $request = '';
        $return_request = '';
        if ($search_data['trip_type'] == 'circle' 
        && $search_data['is_domestic'] != 1) {
            // Interantional RT 
            $onward = $return = '';
            if(isset($search_data['from_loc'])) {                                   
                $depart_date = date("Y-m-d", strtotime(str_replace('/','-',$search_data['depature'])));
                $dep_date = '<SearchDepTime PreferredTime="' . $depart_date . '" />';       
                $request .= '<SearchAirLeg>
                                <SearchOrigin>
                                    <CityOrAirport Code="' . $search_data['from_loc'] . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                                </SearchOrigin>
                                <SearchDestination>
                                    <CityOrAirport Code="' . $search_data['to_loc'] . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                                </SearchDestination>
                                ' . $dep_date . '
                                <AirLegModifiers>
                                    <PreferredCabins>
                                        <CabinClass Type="'.$search_data['v_class'].'" xmlns="http://www.travelport.com/schema/common_v41_0" />  
                                    </PreferredCabins>
                                </AirLegModifiers>   
                            </SearchAirLeg>';
                
                $retu = date("Y-m-d", strtotime(str_replace('/','-',$search_data['return'])));
                $return_date = '<SearchDepTime PreferredTime="' . $retu . '" />';       
                $request .= '<SearchAirLeg>
                                <SearchOrigin>
                                    <CityOrAirport Code="' . $search_data['to_loc'] . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                                </SearchOrigin>
                                <SearchDestination>
                                    <CityOrAirport Code="' . $search_data['from_loc'] . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                                </SearchDestination>
                                ' . $return_date . '   
                                <AirLegModifiers>
                                    <PreferredCabins>
                                        <CabinClass Type="'.$search_data['v_class'].'" xmlns="http://www.travelport.com/schema/common_v41_0"/>  
                                    </PreferredCabins>
                                </AirLegModifiers>    
                            </SearchAirLeg>';
            }
        } else if ($search_data['trip_type'] == 'circle' 
        && $search_data['is_domestic'] == 1) {
            // Domestic RT
            $origin_code = $search_data ['from_loc'];
            $desti_code = $search_data ['to_loc'];
            $depart_date = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $search_data ['depature'] ) ) );
            $dep_date = '<SearchDepTime PreferredTime="' . $depart_date . '" />';
            $request = '<SearchAirLeg>
                            <SearchOrigin>
                                <CityOrAirport Code="' . $origin_code . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                            </SearchOrigin>
                            <SearchDestination>
                                <CityOrAirport Code="' . $desti_code . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                            </SearchDestination>
                            ' . $dep_date . '
                            <AirLegModifiers>
                                    <PreferredCabins>
                                        <CabinClass Type="'.$search_data['v_class'].'" xmlns="http://www.travelport.com/schema/common_v41_0"/>  
                                    </PreferredCabins>
                                </AirLegModifiers>   
                        </SearchAirLeg>';
            $ret_req = '';
            $retu = date("Y-m-d", strtotime(str_replace('/','-',$search_data['return'])));
            $return_date = '<SearchDepTime PreferredTime="' . $retu . '" />';       
            $ret_req = '<SearchAirLeg>
                            <SearchOrigin>
                                <CityOrAirport Code="' . $search_data['to_loc'] . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                            </SearchOrigin>
                            <SearchDestination>
                                <CityOrAirport Code="' . $search_data['from_loc'] . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                            </SearchDestination>
                            ' . $return_date . '  
                            <AirLegModifiers>
                                    <PreferredCabins>
                                        <CabinClass Type="'.$search_data['v_class'].'" xmlns="http://www.travelport.com/schema/common_v41_0"/>  
                                    </PreferredCabins>
                                </AirLegModifiers>     
                        </SearchAirLeg>';
            $request_xml = '<?xml version="1.0" encoding="utf-8"?>
                    <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                        <s:Header>
                            <Action s:mustUnderstand="1" xmlns="http://schemas.microsoft.com/ws/2005/05/addressing/none">http://provabtech.com/WDMR/myflytrip/index.php/flight/lost</Action>
                        </s:Header>
                        <s:Body xmlns:xsi="http://www.w3.org/2001/xmlschema-instance" xmlns:xsd="http://www.w3.org/2001/xmlschema">
                            <' . $search_method . ' SolutionResult="true" TraceId="'.$Trace_ID.'" AuthorizedBy="user" TargetBranch="'.$this->target_branch.'" xmlns="http://www.travelport.com/schema/air_v41_0">
                                <BillingPointOfSaleInfo OriginApplication="UAPI" xmlns="http://www.travelport.com/schema/common_v41_0" />
                                    ' . $ret_req . '
                                    ' . $AirSearchModifiers . '
                                    ' . $provider . '
                                    ' . $adult . '
                                    ' . $infant . '
                                    ' . $child . '
                                    <AirPricingModifiers FaresIndicator="AllFares" ETicketability="Yes"></AirPricingModifiers>
                                    ' . $AsynchModifiers . '
                                    </' . $search_method . '>
                        </s:Body>
                    </s:Envelope>';
            $response ['data'] ['return'] = $request_xml;

            /*Creating XML Log*/
                $xml_title = 'LowFareRequest_return'.date('Y-m-dhsi');
                $date = date('Ymd_His');
                if (!file_exists('xml_logs/Flight')) {
                     mkdir('xml_logs/Flight', 0777, true);
                }
                $requestcml = $xml_title.'.xml';
                file_put_contents('../b2b2b/xml_logs/Flight/'.$requestcml, $request_xml);
            /*End OF Creating XML Log*/

        } else if (strtolower($search_data['trip_type']) == 'multicity') {
            //Multicity
            $multiciti_way = '';
            if(is_array($search_data['from_loc'])) {
                for($t=0; $t<count($search_data['from_loc']);$t++){
                    $depart_date = date("Y-m-d", strtotime(str_replace('/','-',$search_data['depature'][$t])));
                    $dep_date = '<SearchDepTime PreferredTime="' . $depart_date . '" />';
                    $request .= '<SearchAirLeg>
                                        <SearchOrigin>
                                            <CityOrAirport Code="' . $search_data['from_loc'][$t] . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                                        </SearchOrigin>
                                        <SearchDestination>
                                            <CityOrAirport Code="' . $search_data['to_loc'][$t] . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                                        </SearchDestination>
                                        ' . $dep_date . '
                                        <AirLegModifiers>
                                    <PreferredCabins>
                                        <CabinClass Type="'.$search_data['v_class'].'" xmlns="http://www.travelport.com/schema/common_v41_0"/>  
                                    </PreferredCabins>
                                </AirLegModifiers>   
                                    </SearchAirLeg>';
                }
            }
        } else {
            // Oneway
            $origin_code = $search_data ['from_loc'];
            $desti_code = $search_data ['to_loc'];
            $depart_date = date ( "Y-m-d", strtotime ( str_replace ( '/', '-', $search_data ['depature'] ) ) );
            $dep_date = '<SearchDepTime PreferredTime="' . $depart_date . '" />';
            $request = '<SearchAirLeg>
                            <SearchOrigin>
                                <CityOrAirport Code="' . $origin_code . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                            </SearchOrigin>
                            <SearchDestination>
                                <CityOrAirport Code="' . $desti_code . '" xmlns="http://www.travelport.com/schema/common_v41_0" />
                            </SearchDestination>
                            ' . $dep_date . '
                            <AirLegModifiers>
                                    <PreferredCabins>
                                        <CabinClass Type="'.$search_data['v_class'].'" xmlns="http://www.travelport.com/schema/common_v41_0"/>  
                                    </PreferredCabins>
                                </AirLegModifiers>   
                        </SearchAirLeg>';
        }

        $request_xml = '<?xml version="1.0" encoding="utf-8"?>
                    <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                        <s:Header>
                            <Action s:mustUnderstand="1" xmlns="http://schemas.microsoft.com/ws/2005/05/addressing/none">http://provabtech.com/WDMR/myflytrip/index.php/flight/lost</Action>
                        </s:Header>
                        <s:Body xmlns:xsi="http://www.w3.org/2001/xmlschema-instance" xmlns:xsd="http://www.w3.org/2001/xmlschema">
                            <' . $search_method . ' SolutionResult="true" TraceId="'.$Trace_ID.'" AuthorizedBy="user" TargetBranch="'.$this->target_branch.'" xmlns="http://www.travelport.com/schema/air_v41_0">
                                <BillingPointOfSaleInfo OriginApplication="UAPI" xmlns="http://www.travelport.com/schema/common_v41_0" />
                                    ' . $request . '
                                    ' . $AirSearchModifiers . '
                                    ' . $provider . '
                                    ' . $adult . '
                                    ' . $infant . '
                                    ' . $child . '
                                    <AirPricingModifiers FaresIndicator="AllFares" ETicketability="Yes"></AirPricingModifiers>
                                    ' . $AsynchModifiers . '
                                    </' . $search_method . '>
                        </s:Body>
                    </s:Envelope>';
        $response ['data'] ['request'] = $request_xml;
        //echo $request_xml; die;
        /*Creating XML Log*/
            $xml_title = 'LowFareRequest'.date('Y-m-dhsi');
            $date = date('Ymd_His');
            if (!file_exists('xml_logs/Flight')) {
                 mkdir('../b2b2b/xml_logs/Flight', 0777, true);
            }
            $requestcml = $xml_title.'.xml';
            file_put_contents('../b2b2b/xml_logs/Flight/'.$requestcml,$request_xml);        /*End OF Creating XML Log*/

        return $response;
    }

    public function air_pricing_details($token,$search_data) {
        $response ['data'] = array ();
        $response ['status'] = FAILURE_STATUS;
        $currency_obj = new Currency ( array (
                'module_type' => 'fligt',
                'from' => get_application_default_currency (),
                'to' => get_application_transaction_currency_preference () 
        ) );

        // Air pricing request
        $connections = @$token ['connection'];
    
        //echo 'test'; debug($connections); exit();
        $air_price_request = $this->air_pricing_request ( $token );
        
        $booking_array = array ();
        $air_Seg_xml = '';
        $air_pricing_xml = '';
        
        if ($air_price_request ['status'] = SUCCESS_STATUS) {
            $air_price_response = $this->process_request ( $air_price_request ['data'] ['request'] );
                /*XML Logs*/
                    $xml_title = 'AirPriceResponse';
                    $date = date('Ymd_His');
                    if (!file_exists('xml_logs/Flight')) {
                         mkdir('xml_logs/Flight', 0777, true);
                    }
               $requestcml = $xml_title.'_response_'.$date.'_'.date('Y-m-d').'.xml';
                file_put_contents('../b2b2b/xml_logs/Flight/'.$requestcml, $air_price_response);
                    /*End Of XML Logs*/
            $xml = simplexml_load_string ( $air_price_response );
            $xml->registerXPathNamespace ( 'SOAP', 'http://schemas.xmlsoap.org/soap/envelope/' );
            $xml->registerXPathNamespace ( 'air', 'http://www.travelport.com/schema/air_v41_0' );
            $nodes = $xml->xpath ( '//SOAP:Envelope/SOAP:Body/air:AirPriceRsp/air:AirPriceResult/air:AirPricingSolution' );
            $price_solution_xml = '';
            if (isset ( $nodes [0] )) {
                $price_solution_xml = $nodes [0]->asXML ();
            }
            
            $nodes_ite = $xml->xpath ( '//SOAP:Envelope/SOAP:Body/air:AirPriceRsp/air:AirItinerary/air:AirSegment' );
            $segment_solution_xml = array ();
            foreach ( $nodes_ite as $k => $vsm ) {
                $key_objm = $vsm ['Key'];
                $key_objm = json_encode ( $key_objm );
                $key_arrm = json_decode ( $key_objm, TRUE );
                $sekKey = $key_arrm [0];
                $segment_solution_xml [$sekKey] = $vsm->asXML ();
            }
            
            $air_price_response_arr = Converter::createArray ( $air_price_response );
        
            $passenger = array ();
            $passengers_array = array ();
            $change_panelty = array ();
            $cncel_panelty = array ();
            $flight_details = array ();
            $flights = array ();
            
            if (isset ( $air_price_response_arr ['SOAP:Envelope'] ['SOAP:Body'] ['air:AirPriceRsp'] ['air:AirPriceResult'] )) {
                $itenary_details = $air_price_response_arr ['SOAP:Envelope'] ['SOAP:Body'] ['air:AirPriceRsp'] ['air:AirItinerary'];
                $air_price_result_arr = $air_price_response_arr ['SOAP:Envelope'] ['SOAP:Body'] ['air:AirPriceRsp'] ['air:AirPriceResult'] ['air:AirPricingSolution'];
                $transction_id = $air_price_response_arr ['SOAP:Envelope'] ['SOAP:Body'] ['air:AirPriceRsp'] ['@attributes'] ['TransactionId'];
                
                // Take minimun value price array
                if (isset ( $air_price_result_arr ) && valid_array ( $air_price_result_arr )) {
                    $air_price_result = force_multple_data_format ( $air_price_result_arr );
                    $air_price_result = $air_price_result [0];
                }
                
                $air_segment_booking_arr = array ();
                $air_pricing_info_booking_arr = array ();
                $air_pricing_sol = array ();
                
                // AirPricingSolution
                $air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['xmlns'] = "http://www.travelport.com/schema/air_v41_0";
                $air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['Key'] = $air_price_result ['@attributes'] ['Key'];
                $air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['TotalPrice'] = $air_price_result ['@attributes'] ['TotalPrice'];
                $air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['BasePrice'] = $air_price_result ['@attributes'] ['BasePrice'];
                $air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['ApproximateTotalPrice'] = $air_price_result ['@attributes'] ['ApproximateTotalPrice'];
                $air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['ApproximateBasePrice'] = $air_price_result ['@attributes'] ['ApproximateBasePrice'];
                $air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['EquivalentBasePrice'] = @$air_price_result ['@attributes'] ['EquivalentBasePrice'];
                $air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['Taxes'] = $air_price_result ['@attributes'] ['Taxes'];
                $air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['ApproximateTaxes'] = $air_price_result ['@attributes'] ['ApproximateTaxes'];
                $air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['QuoteDate'] = $air_price_result ['@attributes'] ['QuoteDate'];
                
                $air_seg_key = '';
                $air_seg_ref = force_multple_data_format ( $air_price_result ['air:AirSegmentRef'] );
                if (isset ( $air_seg_ref ) && valid_array ( $air_seg_ref )) {
                    foreach ( $air_seg_ref as $s_key => $seg_key ) {
                        $air_seg_key [] = $seg_key ['@attributes'] ['Key'];
                    }
                }
                
                $air_pricing_info_arr = $this->formate_air_price_info($air_price_result,$itenary_details,$air_seg_key,$currency_obj,$search_id='');
                //debug($air_pricing_info_arr); die;
                
                $flights = $air_pricing_info_arr['data']['flight'];

                $air_pricing_info_booking_arr = $air_pricing_info_arr['data']['air_pricing_info_booking_arr'];
                
                $flight_details = $air_pricing_info_arr['data']['flight_details'];
                
                
                $air_pricing_sol ['AirPricingSolution'] ['AirSegment'] = $air_pricing_info_booking_arr ['AirSegment'];
                $air_pricing_sol ['AirPricingSolution'] ['AirPricingInfo'] = $air_pricing_info_booking_arr ['AirPricingInfo'];
                
                // FareNote
                $air_price_result ['air:FareNote'] = force_multple_data_format ( $air_price_result ['air:FareNote'] );
                if (isset ( $air_price_result ['air:FareNote'] ) && valid_array ( $air_price_result ['air:FareNote'] )) {
                    foreach ( $air_price_result ['air:FareNote'] as $af_not_k => $air_fare_note ) {
                        $air_pricing_sol ['AirPricingSolution'] ['FareNote'] [$af_not_k] ['@attributes'] ['Key'] = @$air_fare_note ['@attributes'] ['Key'];
                        $air_pricing_sol ['AirPricingSolution'] ['FareNote'] [$af_not_k] ['@value'] = @$air_fare_note ['@value'];
                    }
                }
                // debug($air_pricing_sol);
                $xml = ArrayToXML::createXML ( 'AirPricingSolution', $air_pricing_sol );

                $air_pricing_xml .= str_replace ( '<?xml version="1.0" encoding="UTF-8"?>', '', $xml->saveXML () );
                
                $air_pricing_xml = str_replace ( '<AirPricingSolution>', '', $air_pricing_xml );
                $air_pricing_xml = str_replace ( '</AirPricingSolution>
</AirPricingSolution>', '</AirPricingSolution>', $air_pricing_xml );
                
                $flight_details ['air_seg_key'] = $air_seg_key;
                $air_pricing_xml = str_replace ( 'air:', '', str_replace ( 'common_v41_0:', '', $air_pricing_xml ) );
                // debug($air_pricing_xml);exit;
            }
            
            
            
            $flight_details ['flights'] = $flights;
            $flight_details ['price_xml'] = $air_pricing_xml;
            $response ['status'] = SUCCESS_STATUS;
            $response ['data'] = $flight_details;
        }
        
        return $response;
    }

    public function air_pricing_request($flight, $search_data) {
        $token = force_multple_data_format ( $flight );
        $air_segment = array ();
        $CI = & get_instance ();
        
        //$search_data = $this->master_search_data;
        $adults = '';
        $childs = '';
        $infants = '';
    
        $adult_count = $search_data ['adult_config'];
        $child_count = $search_data ['child_config'];
        $infant_count = $search_data ['infant_config'];
        $count = count($token);
        
        $Trace_ID = @$_SESSION['trace_id'];
        $token = unserialized_data($flight['Token']);       

        $exp_conn = explode ( ",", @$token[0]['connection'] );
        
        
        $paxId = 0;
        if ($adult_count != 0) {
            for($i = 0; $i < $adult_count; $i ++) {
                $adults .= '<SearchPassenger BookingTravelerRef="' . $paxId . '" Code="ADT" xmlns="http://www.travelport.com/schema/common_v41_0" ></SearchPassenger>';
                $paxId ++;
            }
        }
        
        if ($infant_count != 0) {
            for($j = 0; $j < $infant_count; $j ++) {
                $infants .= '<SearchPassenger BookingTravelerRef="' . $paxId . '" PricePTCOnly="true" Code="INF" Age="01" xmlns="http://www.travelport.com/schema/common_v41_0" ></SearchPassenger>';
                $paxId ++;
            }

        }

        if ($child_count != 0) {
            for($k = 0; $k < $child_count; $k ++) {
                $childs .= '<SearchPassenger BookingTravelerRef="' . $paxId . '" Code="CNN" Age="8" xmlns="http://www.travelport.com/schema/common_v41_0" ></SearchPassenger>';
                $paxId ++;
            }
        }

        if (isset ( $token [0]) && valid_array ( $token [0])) {         
            foreach ( $token [0] as $fl_ley => $flights_list ) {
                
                if(count($flights_list) == 1){
                    $flights_list = @$flights_list[0]; //check later
                
                }
                
                
                if($search_data['trip_type'] == 'oneway'){  
                
                if (isset ( $flights_list ['flight_detail'] ) && valid_array ( $flights_list ['flight_detail'] )) {
                    //  debug($flights_list); die;
                    foreach ( $flights_list ['flight_detail'] as $flight_key => $flight ) {
                        
                    
                        if (in_array ( $flight_key, $exp_conn )) {
                            
                            //$connection_indc = '<Connection SegmentIndex = '.$flight ['segmentindex'].' />';
                            $connection_indc = '<Connection />';
                        } else {
                            $connection_indc = '';
                        }
                        
                        $segment = '';
                        $segmentindex = intval($flight ['segmentindex']); 
                        $group = $flight ['group'];
                        $carrier = $flight ['carrier'];
                        $flight_number = $flight ['flight_number'];
                        $origin = $flight ['origin'];
                        $destination = $flight ['destination'];
                        $departure_time = $flight ['departure_time'];
                        $arrival_time = $flight ['arrival_time'];
                        $flight_time = $flight ['flight_time'];
                        $distance = isset ( $flight ['distance'] ) ? 'Distance="' . $flight ['distance'] . '"' : '';
                        $e_ticketability = (isset ( $flight ['e_ticketability'] )) ? 'ETicketability="' . $flight ['e_ticketability'] . '"' : '';
                        $equipment = isset ( $flight ['equipment'] ) ? 'Equipment="' . $flight ['equipment'] . '"' : '';
                        $change_of_plane = $flight ['change_of_plane'];
                        $participant_level = isset ( $flight ['participant_level'] ) ? 'ParticipantLevel="' . $flight ['participant_level'] . '"' : '';
                        $polled_availability_option = (isset ( $flight ['polled_availability_option'] )) ? 'PolledAvailabilityOption="' . $flight ['polled_availability_option'] . '"' : '';
                        $flight_seg_key = $flight ['flight_seg_key'];
                        $link_availability = (isset ( $flight ['link_availability'] )) ? 'LinkAvailability="' . $flight ['link_availability'] . '"' : '';
                        $fare_info_ref = @$flight ['fare_info_ref'];
                        $flight_detail_key = $flight ['flight_detail_key'];
                        $booking_counts = @$flight ['BookingCount'];
                        $provider_code = $flight ['provider_code'];
                        $optional_services_indicator = $flight ['optional_services_indicator'];
                        $availability_source = (isset ( $flight ['availability_source'] )) ? 'AvailabilitySource="' . $flight ['availability_source'] . '"' : '';
                        $air_code_share = isset ( $flight ['air_code_share'] ) && ! empty ( $flight ['air_code_share'] ) ? '<CodeshareInfo>"' . @$flight ['air_code_share'] . '"</CodeshareInfo>' : '';
                        
                        $segment = '<AirSegment  Key="' . $flight_seg_key . '" Group="' . $group . '" Carrier="' . $carrier . '" FlightNumber="' . $flight_number . '" ProviderCode="' . $provider_code . '" Origin="' . $origin . '" Destination="' . $destination . '" DepartureTime="' . $departure_time . '" ArrivalTime="' . $arrival_time . '" FlightTime="' . $flight_time . '" ' . $distance . ' ' . $equipment . ' ChangeOfPlane="' . $change_of_plane . '" OptionalServicesIndicator="' . $optional_services_indicator . '" ' . $availability_source . ' ' . $participant_level . ' ' . $polled_availability_option . ' AvailabilityDisplayType="Fare Shop/Optimal Shop" ' . $e_ticketability . ' ' . $link_availability . '>

                            ' . $air_code_share . '
                            <AirAvailInfo ProviderCode="' . $provider_code . '">                            
                            </AirAvailInfo>                         
                            <FlightDetails ' . $equipment . ' Destination="' . $destination . '" Origin="' . $origin . '" Key="' . $flight_detail_key . '" FlightTime="' . $flight_time . '" ArrivalTime="' . $arrival_time . '" DepartureTime="' . $departure_time . '" ></FlightDetails>
                            
                            '.$connection_indc.' 
                            </AirSegment>';
                            //}
                        // OriginTerminal="S" DestinationTerminal="4"
                        
                        $air_segment [] = $segment;
                    }
                    
                }
                }elseif(($search_data['trip_type'] == 'multicity') || $search_data['trip_type'] == 'circle'){
                    
                    error_reporting(0); 
                
                    if (isset ( $flights_list ) && valid_array ( $flights_list)) {

                    $cnt = count($flights_list); 
                    
                    for($i=0; $i<$cnt; $i++)
                    {   
                        foreach ($flights_list [$i]['flight_detail'] as $flight_key => $flight ) 
                        {
                            if (in_array ( $flight_key, $exp_conn )) 
                            {
                                //$connection_indc = '<Connection SegmentIndex = '.$flight ['segmentindex'].' />';
                                $connection_indc = '<Connection />';
                            } else {
                                $connection_indc = '';
                            }
                        
                            $segment = '';
                            $segmentindex = intval($flight ['segmentindex']); 
                            $group = $flight ['group'];
                            $carrier = $flight ['carrier'];
                            $flight_number = $flight ['flight_number'];
                            $origin = $flight ['origin'];
                            $destination = $flight ['destination'];
                            $departure_time = $flight ['departure_time'];
                            $arrival_time = $flight ['arrival_time'];
                            $flight_time = $flight ['flight_time'];
                            $distance = isset ( $flight ['distance'] ) ? 'Distance="' . $flight ['distance'] . '"' : '';
                            $e_ticketability = (isset ( $flight ['e_ticketability'] )) ? 'ETicketability="' . $flight ['e_ticketability'] . '"' : '';
                            $equipment = isset ( $flight ['equipment'] ) ? 'Equipment="' . $flight ['equipment'] . '"' : '';
                            $change_of_plane = $flight ['change_of_plane'];
                            $participant_level = isset ( $flight ['participant_level'] ) ? 'ParticipantLevel="' . $flight ['participant_level'] . '"' : '';
                            $polled_availability_option = (isset ( $flight ['polled_availability_option'] )) ? 'PolledAvailabilityOption="' . $flight ['polled_availability_option'] . '"' : '';
                            $flight_seg_key = $flight ['flight_seg_key'];
                            $link_availability = (isset ( $flight ['link_availability'] )) ? 'LinkAvailability="' . $flight ['link_availability'] . '"' : '';
                            $fare_info_ref = @$flight ['fare_info_ref'];
                            $flight_detail_key = $flight ['flight_detail_key'];
                            
                            
                            $booking_counts = @$flight ['BookingCount'];
                            if(empty($booking_counts)){
                                $booking_counts = @$flight ['booking_counts'];
                            }
                            $provider_code = $flight ['provider_code'];
                            $optional_services_indicator = $flight ['optional_services_indicator'];
                            $availability_source = (isset ( $flight ['availability_source'] )) ? 'AvailabilitySource="' . $flight ['availability_source'] . '"' : '';
                            $air_code_share = isset ( $flight ['air_code_share'] ) && ! empty ( $flight ['air_code_share'] ) ? '<CodeshareInfo>"' . @$flight ['air_code_share'] . '"</CodeshareInfo>' : '';
                            
                            $segment = '<AirSegment  Key="' . $flight_seg_key . '" Group="' . $group . '" Carrier="' . $carrier . '" FlightNumber="' . $flight_number . '" ProviderCode="' . $provider_code . '" Origin="' . $origin . '" Destination="' . $destination . '" DepartureTime="' . $departure_time . '" ArrivalTime="' . $arrival_time . '" FlightTime="' . $flight_time . '" ' . $distance . ' ' . $equipment . ' ChangeOfPlane="' . $change_of_plane . '" OptionalServicesIndicator="' . $optional_services_indicator . '" ' . $availability_source . ' ' . $participant_level . ' ' . $polled_availability_option . ' AvailabilityDisplayType="Fare Shop/Optimal Shop" ' . $e_ticketability . ' ' . $link_availability . '>

                                ' . $air_code_share . '
                                <AirAvailInfo ProviderCode="' . $provider_code . '">
                                
                                </AirAvailInfo>
                                
                                <FlightDetails ' . $equipment . ' Destination="' . $destination . '" Origin="' . $origin . '" Key="' . $flight_detail_key . '" FlightTime="' . $flight_time . '" ArrivalTime="' . $arrival_time . '" DepartureTime="' . $departure_time . '" ></FlightDetails>
                                
                                '.$connection_indc.' 
                                </AirSegment>';
                                //}
                            // OriginTerminal="S" DestinationTerminal="4"
                            
                            $air_segment [] = $segment;
                        }
                    } //for() ends
                    
                }
                }
            }
        } 
    //}
    #debug($air_segment); die;

        $all_price_modfier = '<AirPricingModifiers FaresIndicator="AllFares"  ></AirPricingModifiers>';
        $air_pricing_command = '<AirPricingCommand></AirPricingCommand>';
        //$form_of_payment = '<FormOfPayment xmlns="http://www.travelport.com/schema/common_v41_0"  Type="Cash"/>';
        $search_passenger = '<SearchPassenger Key="0" Code="ADT" xmlns="http://www.travelport.com/schema/common_v41_0" ></SearchPassenger>';
        $HostToken_content = '';
        $HostToken_content1 = '';
        $HostToken_content2 = '';
        $AirSegmentPricingModifiers1 = '';
        $AirSegmentPricingModifiers2 = '';
        $AirSegmentPricingModifiers = '';
        
        $air_price_req = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
            <s:Header>
                <Action s:mustUnderstand="1" xmlns="http://schemas.microsoft.com/ws/2005/05/addressing/none">http://provabtech.com/WDMR/myflytrip/index.php/flight/lost</Action>
            </s:Header>
            <s:Body xmlns:xsi="http://www.w3.org/2001/xmlschema-instance" xmlns:xsd="http://www.w3.org/2001/xmlschema">
                <AirPriceReq TargetBranch="'.$this->target_branch.'" CheckOBFees="All" AuthorizedBy="user" TraceId="'.$Trace_ID.'" xmlns="http://www.travelport.com/schema/air_v41_0" xmlns:common_v41_0="http://www.travelport.com/schema/common_v41_0">
                    <BillingPointOfSaleInfo OriginApplication="UAPI" xmlns="http://www.travelport.com/schema/common_v41_0"></BillingPointOfSaleInfo>
                    <AirItinerary>';
                    
        foreach ( $air_segment as $as_key => $segmnt ) {
            $air_price_req .= $segmnt;
        }
        $air_price_req .= $HostToken_content . '
                        ' . $HostToken_content1 . '
                        ' . $HostToken_content2 . '
                    </AirItinerary>
                      ' . $all_price_modfier . '
                       ' . $adults . '
                       ' . $infants . ' 
                       ' . $childs . '
                       
                    <AirPricingCommand>' . $AirSegmentPricingModifiers1 . $AirSegmentPricingModifiers2 . $AirSegmentPricingModifiers . '</AirPricingCommand>
                  
                </AirPriceReq>
            </s:Body>
        </s:Envelope>';
     # echo 'debug'; debug($air_price_req); exit();
        $response ['status'] = SUCCESS_STATUS;
        $response ['data'] ['request'] = $air_price_req;
                    /*XML Logs*/
                    $xml_title = 'AirPriceRequest.xml';
                    $date = date('Ymd_His');
                    if (!file_exists('xml_logs/Flight')) {
                         mkdir('xml_logs/Flight', 0777, true);
                    }
                 // $requestcml = $xml_title.'_request_'.$date.'_'.date('Y-m-d').'.xml';
                file_put_contents('../b2b2b/xml_logs/Flight/'.$xml_title, $air_price_req);
                
                /*End Of XML Logs*/
        return $response;
    }
    /**
     * check if the search response is valid or not
     * 
     * @param array $search_result
     *          search result response to be validated
     */
    function valid_search_result($search_result) {
        if (valid_array ( $search_result ) == true and isset ( $search_result ['SOAP:Envelope'] ['SOAP:Body'] ) == true and isset ( $search_result ['SOAP:Envelope'] ['SOAP:Body'] ['air:LowFareSearchRsp'] ) == true) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * process soap API request
     * 
     * @param string $request           
     */
    function process_request($request) {
        
        //$soapAction = 'http://provabtech.com/WDMR/myflytrip/index.php/flight/lost';
        #echo 'sanju'; debug($this->url); exit();
        
        $soapAction = '';
        //$soapAction = 'http://provabtech.com/WDMR/myflytrip/index.php/flight/lost';
        $Authorization = base64_encode ( 'Universal API/' . $this->username . ':' . $this->password );
        /*$httpHeader = array (
                "SOAPAction: {$soapAction}",
                "Content-Type: text/xml; charset=UTF-8",
                "Content-Encoding: UTF-8",
                "Authorization: Basic $Authorization",
                "Content-length: " . strlen ( $request ),
                "Accept-Encoding: gzip,deflate" 
        );*/
            $httpHeader = array(
                    "Connection:close",
                    "Content-length: ".strlen($request),
                    "Content-Type: text/xml;charset=UTF-8", 
                    "Accept-Encoding: gzip,deflate",
                    "Authorization: Basic $Authorization",
                    "SOAPAction: {$soapAction}"
                );
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $this->url );
        curl_setopt ( $ch, CURLOPT_TIMEOUT, 180 );
        curl_setopt ( $ch, CURLOPT_HEADER, 0 );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $ch, CURLOPT_POST, 1 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $request );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, 2 ); // sd
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, FALSE );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $httpHeader );
        curl_setopt ( $ch, CURLOPT_ENCODING, "gzip,deflate" );
        $response = curl_exec ( $ch );
        $error = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );
        
        curl_close ( $ch );
        return $response;
    }
    
    /**
     * Update markup currency for price object of flight
     * 
     * @param object $price_summary         
     * @param object $currency_obj          
     */
    function update_markup_currency(& $price_summary, & $currency_obj) {
    }
    
    
    /**
     * calculate and return total price details
     */
    function total_price($price_summary, $retain_commission=false, $currency_obj = '')
    {
        
        $com = 0;
        $com_tds = 0;
        if ($retain_commission == false) {
            $com = 0;
            $com_tds += floatval($currency_obj->calculate_tds($price_summary['AgentCommission']));
            $com_tds += floatval($currency_obj->calculate_tds(@$price_summary['PLBEarned']));
            $com_tds += floatval($currency_obj->calculate_tds(@$price_summary['IncentiveEarned']));
        } else {
            $com += floatval(@$price_summary['AgentCommission']);
            $com += floatval(@$price_summary['PLBEarned']);
            $com += floatval(@$price_summary['IncentiveEarned']);
            $com_tds = 0;
        }
        return (floatval(@$price_summary['OfferedFare'])+$com+$com_tds);
    }

    /**
     * Process booking
     * 
     * @param string $book_id           
     * @param array $booking_params         
     */
    function process_booking($book_id, $temp_booking,$TokenId='') { 

        $response ['status'] = FAILURE_STATUS;
        $response ['data'] = array ();
        $booking_id = $temp_booking ['tmp_flight_pre_booking_id'];
        $b_source = $this->source_code;
        
        // booking source
        $booking_src_arr = unserialized_data ( $temp_booking ['booking_source'] );
        $key = array_search ( TRAVELPORT_FLIGHT, $booking_src_arr );
        $no_of_keys = count ( array_keys ( $booking_src_arr, TRAVELPORT_FLIGHT ) );
        if ($no_of_keys == 2) {
            // both are travelport
            $token_data_list [0] = unserialized_data ( $temp_booking ['token'] [0] ['token'] );
            $token_data_list [1] = unserialized_data ( $temp_booking ['token'] [1] ['token'] );
        } else {
            // oneway travelport
            $token_data_list [0] = unserialized_data ( $temp_booking ['token'] [$key] ['token'] );
        }
        
        if (isset ( $token_data_list ) && valid_array ( $token_data_list )) {
            foreach ( $token_data_list as $_fk => $_btkn ) {
                $air_create_reservation_request = $this->air_create_reservation_request ( $temp_booking, $_btkn );

                if ($air_create_reservation_request ['status'] == SUCCESS_STATUS) {
                    $air_create_reservation_response = $this->process_request ( $air_create_reservation_request ['data'] ['request'] );
                    
                            /*XML Logs*/
                            $xml_title = 'AirCreateReservationRes';
                            $date = date('Ymd_His');
                            if (!file_exists('xml_logs/Flight')) {
                                     mkdir('xml_logs/Flight', 0777, true);
                            }
                            $requestcml = $xml_title.'_response_'.$date.'_'.date('Y-m-d').'.xml';
                            file_put_contents('../b2b2b/xml_logs/Flight/'.$requestcml, $air_create_reservation_response);
                            /*End Of XML Logs*/
                    //echo 'response'; debug($air_create_reservation_response); exit();
                    // update booking status here FIXIT
                }
            }

            $return_search_response_array = Converter::createArray ( $air_create_reservation_response );
            
            //echo 'AirCreateReservationReq'; debug($return_search_response_array); exit();

            $response_details = json_encode($return_search_response_array['SOAP:Envelope']['SOAP:Body']['universal:AirCreateReservationRsp']);

            $universal_pnr_no = $return_search_response_array['SOAP:Envelope']['SOAP:Body']['universal:AirCreateReservationRsp']['universal:UniversalRecord']['universal:ProviderReservationInfo']['@attributes']['LocatorCode'];
            

            /*Ticket Generation */
            $main_universal_pnr_no = $return_search_response_array['SOAP:Envelope']['SOAP:Body']['universal:AirCreateReservationRsp']['universal:UniversalRecord']['air:AirReservation']['@attributes']['LocatorCode'];
            $air_ticket_create_reservation_request = $this->air_ticket_create_reservation_request ( $temp_booking, $main_universal_pnr_no );
                
                if ($air_ticket_create_reservation_request ['status'] == SUCCESS_STATUS) {
                    $air_ticket_create_reservation_response = $this->process_request ( $air_ticket_create_reservation_request ['data'] ['request'] );
                    
                            /*XML Logs*/
                            $xml_title = 'AirTicketCreateReservationRes';
                            $date = date('Ymd_His');
                            if (!file_exists('xml_logs/Flight')) {
                                     mkdir('xml_logs/Flight', 0777, true);
                            }
                            $requestcml = $xml_title.'_response_'.$date.'_'.date('Y-m-d').'.xml';
                            file_put_contents('../b2b2b/xml_logs/Flight/'.$requestcml, $air_ticket_create_reservation_response);
                            /*End Of XML Logs*/
                    echo 'response'; debug($air_create_reservation_response); //exit();
                    // update booking status here FIXIT
                }

                $return_air_ticket_search_response_array = Converter::createArray ( $air_ticket_create_reservation_response );
            
            //echo 'AirTicketCreateReservationReq'; debug($return_air_ticket_search_response_array); exit();

            /*Main Ticket Generation*/
            $b_source = $this->source_code;

            if (isset ( $universal_pnr_no ) && ! empty ( $universal_pnr_no )) {
                    $data = array (
                            'status' => 'BOOKING_CONFIRMED',
                            'pnr' => $universal_pnr_no
                            //'total_fare' => $TotalCost,
                            //'api_total_fare' => $api_total_fare 
                    );
            $this->update_total_price ( $book_id, $data, $b_source );
        }
            if (!empty($universal_pnr_no)) {
                $booking_status = 'BOOKING_CONFIRMED';
            }else{
                $booking_status = 'BOOKING_FAILED';
            }

            $this->CI->flight_model->save_traveller_details_flight ( $book_id,$response_details,$universal_pnr_no,$booking_status );
            //if (isset ( $air_create_reservation_response ) && valid_array ( $air_create_reservation_response )) { //By me
                $response ['data'] = $air_create_reservation_response;
                $response ['status'] = SUCCESS_STATUS;
            //}
        }
        
        
        return $response;
    }
    
    /**
     * booking_url to be used
     */
    function booking_url($search_id)
    {
        return base_url().'index.php/flight/booking/'.intval($search_id);
    }
    function pre_booking_process($unique_onward_return_booking_source, $source_index, $token_param, $transaction_id, $search_id, & $flight_data) { // debug($source_index);exit;
        $response = array ();
        $search_data = $this->search_data ( $search_id );
        
        // $response['status'] = FAILURE_STATUS;
        // obtain fare information for an Air Itinerary
        
        $air_price_info = $this->air_pricing_details ( $token_param,$search_data );
        
        $token_param = force_multple_data_format ( $token_param );

        if ($air_price_info ['status'] == SUCCESS_STATUS) {
            // if($source_index == 0) {
            // $currnecy = $air_price_info['data']['total_price_curr'];
            // $api_total_display_fare = $air_price_info['data']['total_price'];
            // $api_total_tax = $air_price_info['data']['taxes'];
            // $api_total_fare = $air_price_info['data']['base_price'];
            // }else {
            // $currnecy = $air_price_info['data']['total_price_curr'];
            // $api_total_display_fare = $flight_data['price']['api_total_display_fare'] + $air_price_info['data']['total_price'];
            // $api_total_tax = $flight_data['price']['total_breakup']['api_total_tax'] + $air_price_info['data']['taxes'];
            // $api_total_fare = $flight_data['price']['total_breakup']['api_total_fare'] + $air_price_info['data']['base_price'];
            // }
            // $flight_data['price'] = array(
            // 'api_currency' => $currnecy,
            // 'api_total_display_fare' => $api_total_display_fare,
            // 'total_breakup' => array(
            // 'api_total_tax' => $api_total_tax,
            // 'api_total_fare' => $api_total_fare,
            // ),
            // 'pax_breakup' => array()
            // );
            
            $price = $token_param[0]['price'];
            $flight_data ['fare'] [$source_index] = array (
                    /*'api_currency' => $air_price_info ['data'] ['total_price_curr'],
                    'api_total_display_fare' => $air_price_info ['data'] ['total_price'],
                    'total_breakup' => array (
                            'api_total_tax' => $air_price_info ['data'] ['taxes'],
                            'api_total_fare' => $air_price_info ['data'] ['base_price'] 
                    ),*/
                    'api_currency' => $price ['api_currency'],
                    'api_total_display_fare' => $price ['api_total_display_fare'],
                    'total_breakup' => array (
                            'api_total_tax' => $price ['total_breakup'] ['api_total_tax'],
                            'api_total_fare' => $price ['total_breakup'] ['api_total_fare'] 
                    ),
                    'pax_breakup' => array () 
            );

            $page_data ['flight_details'] = $air_price_info ['data'];
            $page_data ['flight_details'] ['connection'] = $token_param [0] ['connection'];
            // $this->template->view ( 'flight/travelport/travelport_booking_page', $page_data );
            if (! empty ( $air_price_info ['data'] ['price_xml'] )) {
                $token = serialized_data ( $page_data ['flight_details'] );
                $response ['status'] = SUCCESS_STATUS;
                $response = array (
                        'token' => $token 
                );
                
                $travelport_price_xml_arr = array (
                        'attributes' => json_encode ( array (
                                'price_xml' => $air_price_info ['data'] ['price_xml'] 
                        ) ),
                        'reference_id' => $transaction_id,
                        'booking_source' => $this->source_code,
                        'comments' => 'Travelport',
                        'created_by_id' => intval ( @$GLOBALS ['CI']->entity_user_id ),
                        'created_datetime' => date ( 'Y-m-d H:i:s' ) 
                );
                
                $CI = &get_instance ();
                $CI->db->insert ( 'tmp_flight_pre_booking_details', $travelport_price_xml_arr );
                // $response['status'] = SUCCESS_STATUS;
            }
        }

        return $response;
    }

    private function update_total_price($booking_id, $data, $b_source) {
        $CI = &get_instance ();
        $CI->db->where ( 'app_reference', $booking_id );
        $CI->db->where ( 'booking_source', $b_source );
        $CI->db->update ( 'flight_booking_transaction_details', $data );
        
        // update PNR
        return TRUE;
    }


function processRequest($requestData) {
   // max_allowed_packet=500M;  
    
       // $soapAction = 'http://localhost:8080/kestrel/AirService'; //profiler soap action
     $soapAction = 'http://localhost:8080/kestrel/AirService';
    $Authorization = base64_encode ( 'Universal API/' . $this->username . ':' . $this->password );
      //  $Authorization = base64_encode('Universal API/' . $credentials->username . ':' . $credentials->password);
        $httpHeader = array("SOAPAction: {$soapAction}",
            "Content-Type: text/xml; charset=UTF-8",
            "Content-Encoding: UTF-8",
            "Authorization: Basic $Authorization",
            "Content-length: " . strlen($requestData),
            "Accept-Encoding: gzip,deflate"); //print_r($httpHeader);die;

      //  $strURL = 'https://twsprofiler.travelport.com/Service/Default.ashx/AirService'; //Profiler url
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_URL, $strURL);//profiler code
        //url1 is test url
        $strURL = 'https://twsprofiler.travelport.com/Service/Default.ashx/AirService'; // profiler url
       curl_setopt($ch, CURLOPT_URL, $strURL);
         
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); //sd
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        //curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");

        // Execute request, store response and HTTP response code
        $response = curl_exec($ch);
        $error = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // header("Content-type: text/xml");
    echo $requestData;echo $response;exit;

        //$response = new SimpleXMLElement($response);
        //$response = $response->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children("http://www.travelport.com/schema/air_v33_0");
        $xml_log = array(
            'Api' => 'UAPI',
            'XML_Type' => 'FLIGHT SUPPORT',
            'XML_Request' => $requestData,
            'XML_Response' => $response,
            'Ip_address' => $_SERVER['REMOTE_ADDR'],
            'XML_Time' => date('Y-m-d H:i:s')
        );

        // $CI->xml_model->insert_xml_log($xml_log);
        return $response;
   
}

    function get_fare_details($params) {
       
       // debug($params);die(" up in library");
        //$response ['status'] = FAILURE_STATUS;
        $Trace_ID =@ $_SESSION['trace_id'];

        $fare_key= $params['air_fare_key'];     
         
        $tag_value = $params['fareinfo_key'];
        $fare_rule_key =$params['air_fare_key'];
        $fare_rule_code = $params['provider_code_key'];
       
        $farerules = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                <soapenv:Header/>
                <soapenv:Body>
                    <AirFareRulesReq xmlns="http://www.travelport.com/schema/air_v41_0" TraceId="d381b891-59a3-49e9-b08b-87955d8e5fd2" TargetBranch="'.$this->target_branch.'">
  <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v41_0" OriginApplication="uAPI" />
  <FareRuleKey FareInfoRef="'.$fare_rule_key.'" ProviderCode="'.$fare_rule_code.'">'.$tag_value.'</FareRuleKey>
</AirFareRulesReq>
                </soapenv:Body>
            </soapenv:Envelope>';

                    /*XML Logs*/
                    $xml_title = 'FareRuleRequest.xml';
                    $date = date('Ymd_His');
                    if (!file_exists('xml_logs/Flight')) {
                         mkdir('xml_logs/Flight', 0777, true);
                    }
             //  $requestcml = $xml_title.'_request_'.$date.'_'.date('Y-m-d').'.xml';
            $requestcml =$xml_title;
            file_put_contents('../b2b2b/xml_logs/Flight/'.$requestcml, $farerules);  

            $farerules_response = $this->process_request ( $farerules );

            /*XML Logs*/
                    $xml_title1 = 'FareRuleResponse.xml';
                    $date = date('Ymd_His');
                    if (!file_exists('xml_logs/Flight')) {
                         mkdir('xml_logs/Flight', 0777, true);
                    }
               $responsecml = $xml_title1;   
      
               file_put_contents('../b2b2b/xml_logs/Flight/'.$responsecml, $farerules_response);
            /*End Of XML Logs*/
                    
        
        $message = "";
        $fare_rules = '';
        $category_array = array(3, 5, 6, 7, 16, 19, 8, 12, 14, 10, 1000);
        $xmlDoc = new DOMDocument();
        $xmlDoc->loadXML($farerules_response);  
        $fareRule = $xmlDoc->getElementsByTagNameNS('http://www.travelport.com/schema/air_v41_0', 'FareRuleLong');
    
            foreach ($fareRule as $value) { 
            if ($value->nodeType == XML_ELEMENT_NODE)
                $category = $value->getAttribute("Category");
            if (in_array($category, $category_array)) {
                 $fare_rules .= '<div class="rowfare">';
                if ($category == 3) {
                    $fare_rules .= '<div class="lablfare">Seasons</div>';
                } elseif ($category == 5) {
                    $fare_rules .= '<div class="lablfare">ADV RES/TKTG</div>';
                } elseif ($category == 6) {
                    $fare_rules .= '<div class="lablfare">Min Stay</div>';
                } elseif ($category == 7) {
                    $fare_rules .= '<div class="lablfare">Max Stay</div>';
                } elseif ($category == 16) {
                    $fare_rules .= '<div class="lablfare">Penalties (Or) Change Fee</div>';
                } elseif ($category == 19) {
                    $fare_rules .= '<div class="lablfare">Accompanied Travel Restrictions</div>';
                } elseif ($category == 10) {
                    $fare_rules .= '<div class="lablfare">Combinations</div>';
                } elseif ($category == 8) {
                    $fare_rules .= '<div class="lablfare">StopOvers</div>';
                } elseif ($category == 12) {
                    $fare_rules .= '<div class="lablfare">Surcharges</div>';
                } elseif ($category == 14) {
                    $fare_rules .= '<div class="lablfare">Travel Restrictions</div>';
                } elseif ($category == 100) {
                    $fare_rules .= '<div class="lablfare">Terms & Condtions</div>';
                }
                $fare_rules .= '<div class="feenotes">'.$value->nodeValue . '</div></div>';
            }
        }
      
        $response = array(
        'data' => $fare_rules,
        'status' => SUCCESS_STATUS
        );
        $data['detail'] =  $fare_rules;
        
        return $response;
    }

    function formate_air_price_info($air_price_result,$itenary_details,$air_seg_key,$currency_obj,$search_id=''){

        $air_pricing_info_booking_arr = array();
        $passenger = array();
        $passengers_array = array();
        $change_panelty = array();
        $cncel_panelty = array();
        $pax_cnt_ref = 0;

        if(isset($air_price_result) && valid_array($air_price_result)) {
                    // Passenger details starts here
                    $air_price_result ['air:AirPricingInfo'] = force_multple_data_format ( $air_price_result ['air:AirPricingInfo'] );
                    
                    foreach ( $air_price_result ['air:AirPricingInfo'] as $ap_key => $air_price_sol ) {

                        // XML for booking AirPricingInfo
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['Key'] = $air_price_sol ['@attributes'] ['Key'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['TotalPrice'] = $air_price_sol ['@attributes'] ['TotalPrice'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['BasePrice'] = $air_price_sol ['@attributes'] ['BasePrice'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['ApproximateTotalPrice'] = $air_price_sol ['@attributes'] ['ApproximateTotalPrice'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['ApproximateBasePrice'] = $air_price_sol ['@attributes'] ['ApproximateBasePrice'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['EquivalentBasePrice'] = @$air_price_sol ['@attributes'] ['EquivalentBasePrice'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['@attributes'] ['ApproximateTaxes'] = $air_price_sol ['@attributes'] ['ApproximateTaxes'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['Taxes'] = $air_price_sol ['@attributes'] ['Taxes'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['LatestTicketingTime'] = @$air_price_sol ['@attributes'] ['LatestTicketingTime'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['PricingMethod'] = @$air_price_sol ['@attributes'] ['PricingMethod'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['@attributes'] ['IncludesVAT'] = @$air_price_sol ['@attributes'] ['IncludesVAT'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['ETicketability'] = @$air_price_sol ['@attributes'] ['ETicketability'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['PlatingCarrier'] = @$air_price_sol ['@attributes'] ['PlatingCarrier'];
                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['@attributes'] ['ProviderCode'] = @$air_price_sol ['@attributes'] ['ProviderCode'];
                        
                        // air:FareInfo
                        $fare_info_response = force_multple_data_format ( $air_price_sol ['air:FareInfo'] );
                        if (isset ( $fare_info_response ) && valid_array ( $fare_info_response )) {
                            foreach ( $fare_info_response as $fk_eky => $fare_infor ) {
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['FareInfo'] [$fk_eky] ['@attributes'] ['Key'] = $fare_infor ['@attributes'] ['Key'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['FareInfo'] [$fk_eky] ['@attributes'] ['FareBasis'] = $fare_infor ['@attributes'] ['FareBasis'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['FareInfo'] [$fk_eky] ['@attributes'] ['PassengerTypeCode'] = $fare_infor ['@attributes'] ['PassengerTypeCode'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['FareInfo'] [$fk_eky] ['@attributes'] ['Origin'] = $fare_infor ['@attributes'] ['Origin'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['FareInfo'] [$fk_eky] ['@attributes'] ['Destination'] = $fare_infor ['@attributes'] ['Destination'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['FareInfo'] [$fk_eky] ['@attributes'] ['EffectiveDate'] = $fare_infor ['@attributes'] ['EffectiveDate'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['FareInfo'] [$fk_eky] ['@attributes'] ['DepartureDate'] = $fare_infor ['@attributes'] ['DepartureDate'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['FareInfo'] [$fk_eky] ['@attributes'] ['Amount'] = $fare_infor ['@attributes'] ['Amount'];
                                if (isset ( $fare_infor ['@attributes'] ['NotValidBefore'] ) && ! empty ( $fare_infor ['@attributes'] ['NotValidBefore'] )) {
                                    $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['FareInfo'] [$fk_eky] ['@attributes'] ['NotValidBefore'] = $fare_infor ['@attributes'] ['NotValidBefore'];
                                }
                                if (isset ( $fare_infor ['@attributes'] ['NotValidAfter'] ) && ! empty ( $fare_infor ['@attributes'] ['NotValidAfter'] )) {
                                    $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['FareInfo'] [$fk_eky] ['@attributes'] ['NotValidAfter'] = $fare_infor ['@attributes'] ['NotValidAfter'];
                                }
                                
                                if (isset ( $fare_infor ['air:FareRuleKey'] ) && valid_array ( $fare_infor ['air:FareRuleKey'] )) {
                                    $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['FareInfo'] [$fk_eky] ['FareRuleKey'] ['@attributes'] ['FareInfoRef'] = $fare_infor ['air:FareRuleKey'] ['@attributes'] ['FareInfoRef'];
                                    $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['FareInfo'] [$fk_eky] ['FareRuleKey'] ['@attributes'] ['ProviderCode'] = $fare_infor ['air:FareRuleKey'] ['@attributes'] ['ProviderCode'];
                                    $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['FareInfo'] [$fk_eky] ['FareRuleKey'] ['@value'] = $fare_infor ['air:FareRuleKey'] ['@value'];
                                }
                            }
                        }
                        
                        // air:BookingInfo
                        $fare_booking_info_Res = force_multple_data_format ( @$air_price_sol ['air:BookingInfo'] );
                        if (isset ( $fare_booking_info_Res ) && valid_array ( $fare_booking_info_Res )) {
                            foreach ( $fare_booking_info_Res as $bif_k => $booking_info_d ) {
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['BookingInfo'] [$bif_k] ['@attributes'] ['BookingCode'] = $booking_info_d ['@attributes'] ['BookingCode'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['BookingInfo'] [$bif_k] ['@attributes'] ['CabinClass'] = $booking_info_d ['@attributes'] ['CabinClass'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['BookingInfo'] [$bif_k] ['@attributes'] ['FareInfoRef'] = $booking_info_d ['@attributes'] ['FareInfoRef'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['BookingInfo'] [$bif_k] ['@attributes'] ['SegmentRef'] = $booking_info_d ['@attributes'] ['SegmentRef'];
                            }
                        }
                        
                        // air:TaxInfo
                        $fare_air_tax_detail_inf = force_multple_data_format ( @$air_price_sol ['air:TaxInfo'] );
                        
                        if (isset ( $fare_air_tax_detail_inf ) && valid_array ( $fare_air_tax_detail_inf )) {
                            foreach ( $fare_air_tax_detail_inf as $tx_keym => $tax_detailsm ) {
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['TaxInfo'] [$tx_keym] ['@attributes'] ['Category'] = $tax_detailsm ['@attributes'] ['Category'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['TaxInfo'] [$tx_keym] ['@attributes'] ['Amount'] = $tax_detailsm ['@attributes'] ['Amount'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['TaxInfo'] [$tx_keym] ['@attributes'] ['Key'] = $tax_detailsm ['@attributes'] ['Key'];
                            }
                        }
                        
                        // air:FareCalc
                        if (isset ( $air_price_sol ['air:FareCalc'] ) && ! empty ( $air_price_sol ['air:FareCalc'] )) {
                            $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['FareCalc'] = $air_price_sol ['air:FareCalc'];
                        }
                        
                        // air:PassengerType
                        $fare_air_pass_info = force_multple_data_format ( @$air_price_sol ['air:PassengerType'] );
                        
                        if (isset ( $fare_air_pass_info ) && valid_array ( $fare_air_pass_info )) {
                            foreach ( $fare_air_pass_info as $fapass_k => $fare_passenger_info ) {
                            
                            if ($fare_passenger_info ['@attributes'] ['Code'] == "ADT") {
                                    $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['PassengerType'] [$fapass_k] ['@attributes'] ['Code'] = @$fare_passenger_info ['@attributes'] ['Code'];
                                //$air_pricing_info_booking_arr ['AirPricingInfo'] ['PassengerType'] [$fapass_k] ['@attributes'] ['BookingTravelerRef'] = @$fare_passenger_info ['@attributes'] ['BookingTravelerRef'];
                                $air_pricing_info_booking_arr['AirPricingInfo'][$ap_key]['PassengerType'][$fapass_k]['@attributes']['BookingTravelerRef'] = $pax_cnt_ref;
                                }elseif($fare_passenger_info ['@attributes'] ['Code'] == "CNN"){
                                    $strAge = "8";
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['PassengerType'] [$fapass_k] ['@attributes'] ['Code'] = @$fare_passenger_info ['@attributes'] ['Code'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['PassengerType'] [$fapass_k] ['@attributes'] ['Age'] = @$strAge;
                                //$air_pricing_info_booking_arr ['AirPricingInfo'] ['PassengerType'] [$fapass_k] ['@attributes'] ['BookingTravelerRef'] = @$fare_passenger_info ['@attributes'] ['BookingTravelerRef'];
                                $air_pricing_info_booking_arr['AirPricingInfo'][$ap_key]['PassengerType'][$fapass_k]['@attributes']['BookingTravelerRef'] = $pax_cnt_ref;
                                }else{
                                    $strAge = "1"; 
                                    $PricePTCOnly ="true";
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['PassengerType'] [$fapass_k] ['@attributes'] ['Code'] = @$fare_passenger_info ['@attributes'] ['Code'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['PassengerType'] [$fapass_k] ['@attributes'] ['Age'] = @$strAge;
                                $air_pricing_info_booking_arr['AirPricingInfo'][$ap_key]['PassengerType'][$fapass_k]['@attributes']['PricePTCOnly'] = $PricePTCOnly;
                                $air_pricing_info_booking_arr['AirPricingInfo'][$ap_key]['PassengerType'][$fapass_k]['@attributes']['BookingTravelerRef'] = $pax_cnt_ref;
                                
                                }/*else{
                                    if($fare_passenger_info ['@attributes'] ['Code'] == "CNN"){
                                        $strAge = "10";

                                     $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['PassengerType'] [$fapass_k] ['@attributes'] ['Code'] = @$fare_passenger_info ['@attributes'] ['Code'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['PassengerType'] [$fapass_k] ['@attributes'] ['Age'] = @$strAge;
                                //$air_pricing_info_booking_arr ['AirPricingInfo'] ['PassengerType'] [$fapass_k] ['@attributes'] ['BookingTravelerRef'] = @$fare_passenger_info ['@attributes'] ['BookingTravelerRef'];
                                $air_pricing_info_booking_arr['AirPricingInfo'][$ap_key]['PassengerType'][$fapass_k]['@attributes']['BookingTravelerRef'] = $pax_cnt_ref;
                                    }else{
                                        $strAge = "1";
                                    }
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['PassengerType'] [$fapass_k] ['@attributes'] ['Code'] = @$fare_passenger_info ['@attributes'] ['Code'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['PassengerType'] [$fapass_k] ['@attributes'] ['Age'] = @$strAge;
                                //$air_pricing_info_booking_arr ['AirPricingInfo'] ['PassengerType'] [$fapass_k] ['@attributes'] ['BookingTravelerRef'] = @$fare_passenger_info ['@attributes'] ['BookingTravelerRef'];
                                $air_pricing_info_booking_arr['AirPricingInfo'][$ap_key]['PassengerType'][$fapass_k]['@attributes']['BookingTravelerRef'] = $pax_cnt_ref;
                                }*/
                                $pax_cnt_ref++;
                            } 
                        }
                        
                        // air:ChangePenalty
                        $air_chage_panelty_arr = force_multple_data_format ( @$air_price_sol ['air:ChangePenalty'] );
                        if (isset ( $air_chage_panelty_arr ) && valid_array ( $air_chage_panelty_arr )) {
                            foreach ( $air_chage_panelty_arr as $acpa_k => $change_panel ) {
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['ChangePenalty'] [$acpa_k] ['Amount'] = @$change_panel ['air:Amount'];
                            }
                        }
                        
                        // air:BaggageAllowances
                        $air_baggage_allow_arr = force_multple_data_format ( @$air_price_sol ['air:BaggageAllowances'] ['air:BaggageAllowanceInfo'] );
                        if (isset ( $air_baggage_allow_arr ) && valid_array ( $air_baggage_allow_arr )) {
                            foreach ( $air_baggage_allow_arr as $bg_al_k => $baggage_info ) {
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['BaggageAllowances'] ['BaggageAllowanceInfo'] [$bg_al_k] ['@attributes'] ['TravelerType'] = $baggage_info ['@attributes'] ['TravelerType'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['BaggageAllowances'] ['BaggageAllowanceInfo'] [$bg_al_k] ['@attributes'] ['Origin'] = $baggage_info ['@attributes'] ['Origin'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['BaggageAllowances'] ['BaggageAllowanceInfo'] [$bg_al_k] ['@attributes'] ['Destination'] = $baggage_info ['@attributes'] ['Destination'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['BaggageAllowances'] ['BaggageAllowanceInfo'] [$bg_al_k] ['@attributes'] ['Carrier'] = $baggage_info ['@attributes'] ['Carrier'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['BaggageAllowances'] ['BaggageAllowanceInfo'] [$bg_al_k] ['URLInfo'] ['URL'] = @$baggage_info ['air:URLInfo'] ['air:URL'];
                                
                                // taxt
                                $taxt_info_arr = force_multple_data_format ( $baggage_info ['air:TextInfo'] ['air:Text'] );
                                if (isset ( $taxt_info_arr ) && valid_array ( $taxt_info_arr )) {
                                    foreach ( $taxt_info_arr as $tx_k => $txt_val ) {
                                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['BaggageAllowances'] ['BaggageAllowanceInfo'] [$bg_al_k] ['TextInfo'] ['Text'] [$tx_k] = @$txt_val;
                                    }
                                }
                                // air:BagDetails
                                $air_bag_details_res = force_multple_data_format ( $baggage_info ['air:BagDetails'] );
                                if (isset ( $air_bag_details_res ) && valid_array ( $air_bag_details_res )) {
                                    foreach ( $air_bag_details_res as $beg_k => $beggage ) {
                                        $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['BaggageAllowances'] ['BaggageAllowanceInfo'] [$bg_al_k] ['BagDetails'] [$beg_k] ['@attributes'] ['ApplicableBags'] = @$beggage ['@attributes'] ['ApplicableBags'];
                                        // air:BaggageRestriction
                                        $baggage_rest_arr = force_multple_data_format ( $beggage ['air:BaggageRestriction'] );
                                        // debug($baggage_rest_arr);exit;
                                        if (isset ( $baggage_rest_arr ) && valid_array ( $baggage_rest_arr )) {
                                            foreach ( $baggage_rest_arr as $begg_r_k => $beg_rest ) {
                                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['BaggageAllowances'] ['BaggageAllowanceInfo'] [$bg_al_k] ['BagDetails'] [$beg_k] ['BaggageRestriction'] ['TextInfo'] ['Text'] [$begg_r_k] = $beg_rest ['air:TextInfo'] ['air:Text'];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        // air:CarryOnAllowanceInfo
                        $carry_on_baggage_arr = force_multple_data_format ( $air_price_sol ['air:BaggageAllowances'] ['air:CarryOnAllowanceInfo'] );
                        if (isset ( $carry_on_baggage_arr ) && valid_array ( $carry_on_baggage_arr )) {
                            foreach ( $carry_on_baggage_arr as $carry_k => $carry_on_beg ) {
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['BaggageAllowances'] ['CarryOnAllowanceInfo'] [$carry_k] ['@attributes'] ['Origin'] = $carry_on_beg ['@attributes'] ['Origin'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['BaggageAllowances'] ['CarryOnAllowanceInfo'] [$carry_k] ['@attributes'] ['Destination'] = $carry_on_beg ['@attributes'] ['Destination'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['BaggageAllowances'] ['CarryOnAllowanceInfo'] [$carry_k] ['@attributes'] ['Carrier'] = $carry_on_beg ['@attributes'] ['Carrier'];
                                $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['BaggageAllowances'] ['CarryOnAllowanceInfo'] [$carry_k] ['TextInfo'] ['Text'] = $carry_on_beg ['air:TextInfo'] ['air:Text'];
                                
                                // CarryOnDetails
                                if (isset ( $carry_on_beg ['air:CarryOnDetails'] ) && valid_array ( $carry_on_beg ['air:CarryOnDetails'] )) {
                                    $carry_on_beg ['air:CarryOnDetails'] = force_multple_data_format ( $carry_on_beg ['air:CarryOnDetails'] );
                                    foreach ( $carry_on_beg ['air:CarryOnDetails'] as $cd_key => $carry_detl ) {
                                        $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['BaggageAllowances'] ['CarryOnAllowanceInfo'] [$carry_k] ['CarryOnDetails'] [$cd_key] ['@attributes'] ['ApplicableCarryOnBags'] = @$carry_detl ['@attributes'] ['ApplicableCarryOnBags'];
                                        $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['BaggageAllowances'] ['CarryOnAllowanceInfo'] [$carry_k] ['CarryOnDetails'] [$cd_key] ['@attributes'] ['BasePrice'] = @$carry_detl ['@attributes'] ['BasePrice'];
                                        $air_pricing_info_booking_arr ['AirPricingInfo'] [$ap_key]['BaggageAllowances'] ['CarryOnAllowanceInfo'] [$carry_k] ['CarryOnDetails'] [$cd_key] ['@attributes'] ['TotalPrice'] = @$carry_detl ['@attributes'] ['TotalPrice'];
                                        if (isset ( $carry_detl ['air:BaggageRestriction'] ) && ! empty ( $carry_detl ['air:BaggageRestriction'] )) {
                                            $baggage_rest_carry_arr = force_multple_data_format ( @$carry_detl ['air:BaggageRestriction'] );
                                            if (isset ( $baggage_rest_carry_arr ) && valid_array ( $baggage_rest_carry_arr )) {
                                                foreach ( $baggage_rest_carry_arr as $begg_rc_k => $beg_carry_rest ) {
                                                    $air_pricing_info_booking_arr ['AirPricingInfo'][$ap_key] ['BaggageAllowances'] ['CarryOnAllowanceInfo'] [$carry_k] ['CarryOnDetails'] [$cd_key] ['BaggageRestriction'] ['TextInfo'] ['Text'] [$begg_rc_k] = @$beg_carry_rest ['air:TextInfo'] ['air:Text'];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        // Price details
                        $passenger_total_price_val = isset ( $air_price_sol ['@attributes'] ['ApproximateTotalPrice'] ) && ! empty ( $air_price_sol ['@attributes'] ['ApproximateTotalPrice'] ) ? @$air_price_sol ['@attributes'] ['ApproximateTotalPrice'] : @$air_price_sol ['@attributes'] ['TotalPrice'];
                        $passenger_total_price = substr ( $passenger_total_price_val, 3 );
                        $passenger_total_price_currency = substr ( $passenger_total_price_val, 0, 3 );
                        // $currency_obj->getConversionRate(true,$passenger_total_price_currency,get_application_transaction_currency_preference());
                        // $converted_total_price_rate = $currency_obj->force_currency_conversion($passenger_total_price);
                        // $passenger_total_price = @$converted_total_price_rate['default_value'];
                        // $passenger_total_price_currency = @$converted_total_price_rate['default_currency'];
                        
                        $passenger_base_price_val = isset ( $air_price_sol ['@attributes'] ['EquivalentBasePrice'] ) && ! empty ( $air_price_sol ['@attributes'] ['EquivalentBasePrice'] ) ? $air_price_sol ['@attributes'] ['EquivalentBasePrice'] : $air_price_sol ['@attributes'] ['ApproximateBasePrice'];
                        $passenger_base_price = substr ( $passenger_base_price_val, 3 );
                        $passenger_base_price_currency = substr ( $passenger_base_price_val, 0, 3 );
                        // $currency_obj->getConversionRate(true,$passenger_base_price_currency,get_application_transaction_currency_preference());
                        // $converted_base_price_rate = $currency_obj->force_currency_conversion($passenger_base_price);
                        // $passenger_base_price = @$converted_base_price_rate['default_value'];
                        // $passenger_base_price_currency = @$converted_base_price_rate['default_currency'];
                        
                        $passenger_taxes_val = isset ( $air_price_sol ['@attributes'] ['ApproximateTaxes'] ) && ! empty ( $air_price_sol ['@attributes'] ['ApproximateTaxes'] ) ? $air_price_sol ['@attributes'] ['ApproximateTaxes'] : $air_price_sol ['@attributes'] ['Taxes'];
                        $passenger_taxes = substr ( $passenger_taxes_val, 3 );
                        $passenger_taxes_currency = substr ( $passenger_taxes_val, 0, 3 );
                        // $currency_obj->getConversionRate(true,$passenger_taxes_currency,get_application_transaction_currency_preference());
                        // $converted_taxes_rate = $currency_obj->force_currency_conversion($passenger_taxes);
                        // $passenger_taxes = @$converted_taxes_rate['default_value'];
                        // $passenger_taxes_currency = @$converted_taxes_rate['default_currency'];
                        
                        $passenger ['total_price'] = $passenger_total_price;
                        $passenger ['base_price'] = $passenger_base_price;
                        $passenger ['taxes'] = $passenger_taxes;
                        $passenger ['total_price_currency'] = $passenger_total_price_currency;
                        $passenger ['base_price_currency'] = $passenger_base_price_currency;
                        $passenger ['taxes_currency'] = $passenger_taxes_currency;
                        
                        // Passengers type
                        /*$passenger_types = $air_price_sol ['air:PassengerType'];
                        if (isset ( $passenger_types ) && valid_array ( $passenger_types )) {
                            $pass_type_code = isset ( $passenger_types ['@attributes'] ['Code'] ) ? @$passenger_types ['@attributes'] ['Code'] : 'ADT';
                            $passengers_array [$pass_type_code] = $passenger;
                        }
                        // change panelty
                        if (isset ( $air_price_sol ['air:ChangePenalty'] ) && valid_array ( $air_price_sol ['air:ChangePenalty'] )) {
                            $change_panelty [] = @$air_price_sol ['air:ChangePenalty'] ['air:Amount'];
                        }
                        // cncel panelty
                        if (isset ( $air_price_sol ['air:CancelPenalty'] ) && valid_array ( $air_price_sol ['air:CancelPenalty'] )) {
                            $cncel_panelty [] = @$air_price_sol ['air:CancelPenalty'] ['air:Amount'];
                        }*/
                        //Passengers type
                    $passenger_types = $air_price_sol['air:PassengerType'];
                if(isset($passenger_types) && valid_array($passenger_types)) {
                    $pass_type_code = isset($passenger_types['@attributes']['Code']) ? @$passenger_types['@attributes']['Code'] : 'ADT';
                    $passengers_array[$pass_type_code] = $passenger;
                }
                    }
                    $flight_details ['passengers'] = $passengers_array;
                    $flight_details ['change_panelty'] = $change_panelty;
                    $flight_details ['cncel_panelty'] = $cncel_panelty;

                    // Passenger ends here
                    
                    $total_price_val = isset ( $air_price_result ['@attributes'] ['ApproximateTotalPrice'] ) && ! empty ( $air_price_result ['@attributes'] ['ApproximateTotalPrice'] ) ? $air_price_result ['@attributes'] ['ApproximateTotalPrice'] : $air_price_result ['@attributes'] ['TotalPrice'];
                    $total_price = substr ( $total_price_val, 3 );
                    $total_price_currency = substr ( $total_price_val, 0, 3 );
                    $currency_obj->getConversionRate ( true, $total_price_currency, get_application_default_currency () );
                    $converted_total_rate = $currency_obj->force_currency_conversion ( $total_price );
                    $total_price = @$converted_total_rate ['default_value'];
                    $total_price_currency = @$converted_total_rate ['default_currency'];
                    
                    $base_price_val = isset ( $air_price_result ['@attributes'] ['EquivalentBasePrice'] ) && ! empty ( $air_price_result ['@attributes'] ['EquivalentBasePrice'] ) ? $air_price_result ['@attributes'] ['EquivalentBasePrice'] : $air_price_result ['@attributes'] ['ApproximateBasePrice'];
                    $base_price = substr ( $base_price_val, 3 );
                    $base_price_currency = substr ( $base_price_val, 0, 3 );
                    $currency_obj->getConversionRate ( true, $base_price_currency, get_application_default_currency () );
                    $converted_base_rate = $currency_obj->force_currency_conversion ( $base_price );
                    $base_price = @$converted_base_rate ['default_value'];
                    $base_price_currency = @$converted_base_rate ['default_currency'];
                    
                    $taxes_val = isset ( $air_price_result ['@attributes'] ['ApproximateTaxes'] ) && ! empty ( $air_price_result ['@attributes'] ['ApproximateTaxes'] ) ? $air_price_result ['@attributes'] ['ApproximateTaxes'] : $air_price_result ['@attributes'] ['Taxes'];
                    $taxes = substr ( $taxes_val, 3 );
                    $taxes_currency = substr ( $taxes_val, 0, 3 );
                    $currency_obj->getConversionRate ( true, $taxes_currency, get_application_default_currency () );
                    $converted_taxes = $currency_obj->force_currency_conversion ( $taxes );
                    $taxes = @$converted_taxes ['default_value'];
                    $taxes_currency = @$converted_taxes ['default_currency'];
                    
                    $flight_details ['total_price'] = $total_price;
                    $flight_details ['total_price_curr'] = $total_price_currency;
                    $flight_details ['base_price'] = $base_price;
                    $flight_details ['base_price_curr'] = $base_price_currency;
                    $flight_details ['taxes'] = $taxes;
                    $flight_details ['taxes_curr'] = $taxes_currency;
                    
                    // $itenary_details

                    if (isset ( $itenary_details ['air:AirSegment'] ) && valid_array ( $itenary_details ['air:AirSegment'] )) {
                        $itenary_details = force_multple_data_format ( $itenary_details ['air:AirSegment'] );
                        $air_seg_cnt = 0;
                        // $air_segment_booking_arr[$s_key]['key']
                        foreach ( $itenary_details as $it_key => $air_segment ) {
                            if (in_array ( $air_segment ['@attributes'] ['Key'], $air_seg_key )) {
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['Key'] = $air_segment ['@attributes'] ['Key'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['Group'] = $air_segment ['@attributes'] ['Group'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['Carrier'] = $air_segment ['@attributes'] ['Carrier'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['FlightNumber'] = $air_segment ['@attributes'] ['FlightNumber'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['ProviderCode'] = $air_segment ['@attributes'] ['ProviderCode'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['Origin'] = $air_segment ['@attributes'] ['Origin'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['Destination'] = $air_segment ['@attributes'] ['Destination'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['DepartureTime'] = $air_segment ['@attributes'] ['DepartureTime'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['ArrivalTime'] = $air_segment ['@attributes'] ['ArrivalTime'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['FlightTime'] = $air_segment ['@attributes'] ['FlightTime'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['TravelTime'] = $air_segment ['@attributes'] ['TravelTime'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['Distance'] = $air_segment ['@attributes'] ['Distance'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['ClassOfService'] = $air_segment ['@attributes'] ['ClassOfService'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['Equipment'] = $air_segment ['@attributes'] ['Equipment'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['ChangeOfPlane'] = $air_segment ['@attributes'] ['ChangeOfPlane'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['OptionalServicesIndicator'] = $air_segment ['@attributes'] ['OptionalServicesIndicator'];
                                
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['AvailabilitySource'] = $air_segment ['@attributes'] ['AvailabilitySource'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['ParticipantLevel'] = $air_segment ['@attributes'] ['ParticipantLevel'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['LinkAvailability'] = @$air_segment ['@attributes'] ['LinkAvailability'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['PolledAvailabilityOption'] = $air_segment ['@attributes'] ['PolledAvailabilityOption'];
                                $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['@attributes'] ['AvailabilityDisplayType'] = $air_segment ['@attributes'] ['AvailabilityDisplayType'];
                                
                                // <CodeshareInfo OperatingCarrier="VY" OperatingFlightNumber="8770" /> air:CodeshareInfo
                                
                                if (isset ( $air_segment ['air:CodeshareInfo'] )) {
                                    if (valid_array ( $air_segment ['air:CodeshareInfo'] )) { // debug($air_segment['air:CodeshareInfo']);
                                        $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['CodeshareInfo'] ['@attributes'] ['OperatingCarrier'] = str_replace ( '"', '', @$air_segment ['air:CodeshareInfo'] ['@attributes'] ['OperatingCarrier'] );
                                        //$air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['CodeshareInfo'] ['@attributes'] ['OperatingFlightNumber'] = str_replace ( '"', '', @$air_segment ['air:CodeshareInfo'] ['@attributes'] ['OperatingFlightNumber'] );
                                    } else {
                                        $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['CodeshareInfo'] ['@attributes'] ['OperatingCarrier'] = str_replace ( '"', '', @$air_segment ['air:CodeshareInfo'] );
                                    }
                                }
                                if (isset ( $air_segment ['air:AirAvailInfo'] ) && valid_array ( $air_segment ['air:AirAvailInfo'] )) {
                                    // booking count provide code
                                    //$flights [$it_key] ['booking_count'] = @$air_segment ['air:AirAvailInfo'] ['air:BookingCodeInfo'] ['@attributes'] ['BookingCounts'];
                                    $flights [$it_key] ['provide_code'] = @$air_segment ['air:AirAvailInfo'] ['@attributes'] ['ProviderCode'];
                                    $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['AirAvailInfo'] ['@attributes'] ['ProviderCode'] = @$air_segment ['air:AirAvailInfo'] ['@attributes'] ['ProviderCode'];
                                    //$air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['AirAvailInfo'] ['BookingCodeInfo'] ['@attributes'] ['BookingCounts'] = @$air_segment ['air:AirAvailInfo'] ['air:BookingCodeInfo'] ['@attributes'] ['BookingCounts'];
                                }
                                
                                // Fligth details
                                if (isset ( $air_segment ['air:FlightDetails'] ) && valid_array ( $air_segment ['air:FlightDetails'] )) {
                                    $flights [$it_key] ['flight_detail_key'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['Key'];
                                    $flights [$it_key] ['origin'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['Origin'];
                                    $flights [$it_key] ['destination'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['Destination'];
                                    $flights [$it_key] ['origin_city'] = $this->get_airport_city ( $air_segment ['air:FlightDetails'] ['@attributes'] ['Origin'] );
                                    $flights [$it_key] ['destination_city'] = $this->get_airport_city ( $air_segment ['air:FlightDetails'] ['@attributes'] ['Destination'] );
                                    $flights [$it_key] ['departure_date'] = date ( 'd M Y', strtotime ( $air_segment ['air:FlightDetails'] ['@attributes'] ['DepartureTime'] ) );
                                    $flights [$it_key] ['departure_datetime'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['DepartureTime'];
                                    $flights [$it_key] ['arrival_date'] = date ( 'd M Y', strtotime ( $air_segment ['air:FlightDetails'] ['@attributes'] ['ArrivalTime'] ) );
                                    $flights [$it_key] ['departure_time'] = date ( 'H:i', strtotime ( $air_segment ['air:FlightDetails'] ['@attributes'] ['DepartureTime'] ) );
                                    $flights [$it_key] ['arrival_time'] = date ( 'H:i', strtotime ( $air_segment ['air:FlightDetails'] ['@attributes'] ['ArrivalTime'] ) );
                                    $flights [$it_key] ['flight_time'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['FlightTime'];
                                    $flights [$it_key] ['travel_time'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['TravelTime'];
                                    $flights [$it_key] ['duration'] = get_duration_label ( calculate_duration ( $air_segment ['air:FlightDetails'] ['@attributes'] ['DepartureTime'], $air_segment ['air:FlightDetails'] ['@attributes'] ['ArrivalTime'] ) );
                                    $flights [$it_key] ['distance'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['Distance'];
                                    
                                    $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['FlightDetails'] ['@attributes'] ['Key'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['Key'];
                                    $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['FlightDetails'] ['@attributes'] ['Origin'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['Origin'];
                                    $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['FlightDetails'] ['@attributes'] ['Destination'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['Destination'];
                                    $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['FlightDetails'] ['@attributes'] ['DepartureTime'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['DepartureTime'];
                                    $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['FlightDetails'] ['@attributes'] ['ArrivalTime'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['ArrivalTime'];
                                    $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['FlightDetails'] ['@attributes'] ['FlightTime'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['FlightTime'];
                                    $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['FlightDetails'] ['@attributes'] ['TravelTime'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['TravelTime'];
                                    $air_pricing_info_booking_arr ['AirSegment'] [$it_key] ['FlightDetails'] ['@attributes'] ['Distance'] = $air_segment ['air:FlightDetails'] ['@attributes'] ['Distance'];
                                }
                                if(isset($air_segment['air:Connection'])) {
                                $air_pricing_info_booking_arr['AirSegment'][$it_key]['Connection'] = '';
                                    }
                                $flights [$it_key] ['air_seg_key'] = $air_segment ['@attributes'] ['Key'];
                                $flights [$it_key] ['group'] = $air_segment ['@attributes'] ['Group'];
                                $flights [$it_key] ['carrier'] = $air_segment ['@attributes'] ['Carrier'];
                                $flights [$it_key] ['flight_number'] = $air_segment ['@attributes'] ['FlightNumber'];
                                $flights [$it_key] ['class_of_service'] = $air_segment ['@attributes'] ['ClassOfService'];
                                $flights [$it_key] ['equipment'] = $air_segment ['@attributes'] ['Equipment'];
                                $flights [$it_key] ['change_of_plane'] = $air_segment ['@attributes'] ['ChangeOfPlane'];
                                $flights [$it_key] ['optional_services_indicator'] = $air_segment ['@attributes'] ['OptionalServicesIndicator'];
                                $flights [$it_key] ['availability_source'] = $air_segment ['@attributes'] ['AvailabilitySource'];
                                $flights [$it_key] ['participant_level'] = $air_segment ['@attributes'] ['ParticipantLevel'];
                                $flights [$it_key] ['link_availability'] = @$air_segment ['@attributes'] ['LinkAvailability'];
                                $flights [$it_key] ['polled_availability_option'] = $air_segment ['@attributes'] ['PolledAvailabilityOption'];
                                $flights [$it_key] ['availability_display_type'] = $air_segment ['@attributes'] ['AvailabilityDisplayType'];
                            }
                            $air_seg_cnt++;
                        }
                        $response['data'] = array('flight'=>$flights,'air_pricing_info_booking_arr'=>$air_pricing_info_booking_arr,'flight_details'=>$flight_details);

                        return $response;
                        
                        //$response ['status'] = SUCCESS_STATUS;
                    }
                } 
    }

    function xml2ary(&$string) {
    $parser = xml_parser_create();
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parse_into_struct($parser, $string, $vals, $index);
    xml_parser_free($parser);

    $mnary=array();
    $ary=&$mnary;
    foreach ($vals as $r) {
        $t=$r['tag'];
        if ($r['type']=='open') {
            if (isset($ary[$t])) {
                if (isset($ary[$t][0])) 
                $ary[$t][]=array(); 
                else
                 $ary[$t]=array($ary[$t], array());
                $cv=&$ary[$t][count($ary[$t])-1];
            } 
            else $cv=&$ary[$t];
            if (isset($r['attributes']))
            {
            foreach ($r['attributes'] as $k=>$v) 
            $cv['_a'][$k]=$v;
            }
            $cv=array();
            $cv['_p']=&$ary;
            $ary=&$cv;

        } 
        elseif
        ($r['type']=='complete')
         {
            if (isset($ary[$t])) { // same as open
                if (isset($ary[$t][0])) $ary[$t][]=array(); else $ary[$t]=array($ary[$t], array());
                $cv=&$ary[$t][count($ary[$t])-1];
            } else $cv=&$ary[$t];
            if (isset($r['attributes'])) {foreach ($r['attributes'] as $k=>$v) $cv['_a'][$k]=$v;}
            $cv['_v']=(isset($r['value']) ? $r['value'] : '');

        } elseif ($r['type']=='close') {
            $ary=&$ary['_p'];
        }
    }    
    
    $this->_del_p($mnary);
        
    return $mnary;
    }

    /*Fare Details For Mobile*/
    function get_fare_details_mobile($data_access_key) {

        //$response ['status'] = FAILURE_STATUS;
        
        $ci = &get_instance ();
        $ci->load->library ( 'flight/common_flight' );
        $flight_data = Common_Flight::read_record ( $data_access_key );
        $flight_data = (json_decode ( $flight_data [0], true ));
        
        $token = unserialized_data ( $flight_data ['token'], $flight_data ['token_key'] );
        
        $count = $token[0]['flight_list'][0]['flight_detail'][0]['fare_keyy'];
        if(!empty($count)){
        $fare_key= explode("&&",$count);
        
          $xmlDoc = new DOMDocument();
        $tag_value = $fare_key[0];
        $fare_rule_key = $fare_key[1];
        $fare_rule_code = $fare_key[2];
       
        $farerules = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                <soapenv:Header/>
                <soapenv:Body>
                    <ns2:AirFareRulesReq xmlns="http://www.travelport.com/schema/common_v41_0" xmlns:ns2="http://www.travelport.com/schema/air_v41_0" FareRuleType="long" TargetBranch="'.$this->target_branch.'" AuthorizedBy="Test">
                      <BillingPointOfSaleInfo OriginApplication="uAPI" />
                      <ns2:FareRuleKey ProviderCode="' . $fare_rule_code . '" FareInfoRef="' . $fare_rule_key . '">' . $tag_value . '   </ns2:FareRuleKey>
                    </ns2:AirFareRulesReq>
                </soapenv:Body>
            </soapenv:Envelope>';

            $farerules_response = $this->process_request ( $farerules );
            $farerules_response_array = Converter::createArray ( $farerules_response );
            //$xmlDoc->loadXML($farerules_response);            
            
        
        $response = array(
        'data' => $farerules_response_array,
        'status' => SUCCESS_STATUS
        );
        $data['detail'] =  $farerules_response_array;
    }else{
        $response = array(
        'data' => " ",
        'status' => FAILURE_STATUS
        );
    }
        
        
        return $response;
    }
    /*End of Fare Details For Mobile*/


    /*End Of Fare Details Code*/

    /**
     * return booking form
     */
    function booking_form($isDomestic, $token='', $token_key='', $search_access_key='', $is_lcc='', $booking_type='', $promotional_plan_type='', $cur_ProvabAuthKey='', $booking_source=TRAVELPORT_FLIGHT_BOOKING_SOURCE)
    {
        $booking_form = '';

        $booking_form .= '<input type="hidden" name="is_domestic" class="" value="'.$isDomestic.'">';
        $booking_form .= '<input type="hidden" name="token[]" class="token data-access-key" value="'.$token.'">';
        $booking_form .= '<input type="hidden" name="token_key[]" class="token_key" value="'.$token_key.'">';
        $booking_form .= '<input type="hidden" name="search_access_key[]" class="search-access-key" value="'.$search_access_key.'">';
        $booking_form .= '<input type="hidden" name="is_lcc[]" class="is-lcc" value="'.$is_lcc.'">';
        $booking_form .= '<input type="hidden" name="promotional_plan_type[]" class="promotional-plan-type" value="'.$promotional_plan_type.'">';
        //$booking_form .= '<input type="hidden" name="provab-auth-key[]" class="provab-auth-key" value="'.$cur_ProvabAuthKey.'">'; since data is cached no need to carry this
        if (empty($booking_type) == false) {
            $booking_form .= '<input type="hidden" name="booking_type" class="booking-type" value="'.$booking_type.'">';
        }
        if (empty($booking_source) == false) {
            $booking_form .= '<input type="hidden" name="booking_source" class="booking-source" value="'.$booking_source.'">';
        }
        return $booking_form;
    }


//added by princess on July 11 2017
    function booking_form_travelport($isDomestic, $token='', $token_key='', $search_access_key='', $is_lcc='', $booking_type='', $promotional_plan_type='', $cur_ProvabAuthKey='', $booking_source=TRAVELPORT_FLIGHT_BOOKING_SOURCE, $flights_data='')
    {
        #echo $flights_data; 
        $booking_form = '';
        $booking_form .= '<input type="hidden" name="is_domestic" class="" value="'.$isDomestic.'">';
        $booking_form .= '<input type="hidden" name="token[]" class="token data-access-key" value="'.$token.'">';
        $booking_form .= '<input type="hidden" name="token_key[]" class="token_key" value="'.$token_key.'">';
        $booking_form .= '<input type="hidden" name="search_access_key[]" class="search-access-key" value="'.$search_access_key.'">';
        $booking_form .= '<input type="hidden" name="is_lcc[]" class="is-lcc" value="'.$is_lcc.'">';
        $booking_form .= '<input type="hidden" name="promotional_plan_type[]" class="promotional-plan-type" value="'.$promotional_plan_type.'">';
        $booking_form .= '<input type="hidden" name="flights_data" class="flights_data" value="'.$flights_data.'">';
        //$booking_form .= '<input type="hidden" name="provab-auth-key[]" class="provab-auth-key" value="'.$cur_ProvabAuthKey.'">'; since data is cached no need to carry this
        if (empty($booking_type) == false) {
            $booking_form .= '<input type="hidden" name="booking_type" class="booking-type" value="'.$booking_type.'">';
        }
        if (empty($booking_source) == false) {
            $booking_form .= '<input type="hidden" name="booking_source" class="booking-source" value="'.$booking_source.'">';
        }
        return $booking_form;
    }
    /**
     * booking_url to be used
     */
    /*function booking_url($search_id)
    {
        return base_url().'index.php/flight/booking/'.intval($search_id);
    }*/
    /**
     * Extracts Journey Details
     * @param unknown_type $search_result
     */
    private function extract_journey_details($search_data)
    {
        //echo "testtt ";debug($search_data);//exit;
        //Journey Summary
        $journey_summary = array();
        $journey_summary['Origin'] = $search_data['data']['from'];
        $journey_summary['Destination'] = $search_data['data']['to'];
        $journey_summary['IsDomestic'] = $search_data['data']['is_domestic'];
        if($search_data['data']['trip_type']=='circle')
        {
            $journey_summary['RoundTrip'] = true;
        }
        else{
            $journey_summary['RoundTrip'] = false;
        }
        
        //$journey_summary['MultiCity'] = $search_data['data']['MultiCity'];
        if($search_data['data']['trip_type']=='multicity')
        {
            $journey_summary['MultiCity'] = true;
        }
        else{
            $journey_summary['MultiCity'] = false;
        }

        $journey_summary['PassengerConfig']['Adult'] = $search_data['data']['adult'];
        $journey_summary['PassengerConfig']['Child'] = $search_data['data']['child'];
        $journey_summary['PassengerConfig']['Infant'] = $search_data['data']['infant'];
        $journey_summary['PassengerConfig']['TotalPassenger'] = $search_data['data']['adult']+
                                                $search_data['data']['child']+$search_data['data']['infant'];
        
        if($search_data['data']['is_domestic'] == true && $journey_summary['RoundTrip'] == true) {
            $is_domestic_roundway = true;
        } else {
            $is_domestic_roundway = false;
        }
        $journey_summary['IsDomesticRoundway'] = $is_domestic_roundway;

        //echo "journey_summary ";debug($journey_summary);exit;
        return $journey_summary;
    }

    public function read_token($token_key)
    {
        //debug($token_key); die;
        //$token_key = explode(DB_SAFE_SEPARATOR, unserialized_data($token_key));
        $token_key = explode(DB_SAFE_SEPARATOR, ($token_key));
        if (valid_array($token_key) == true) {
            $file = DOMAIN_TMP_UPLOAD_DIR.$token_key[0].'.json';//File name
            //debug($file); die;
            //$index = $token_key[1]; // access key
            $index = $token_key[0]; // access key
            if (file_exists($file) == true) {
                $token_content = file_get_contents($file);
                
                if (empty($token_content) == false) {
                    $token = json_decode($token_content, true);
                    if (valid_array($token) == true && isset($token[$index]) == true) {
                        return $token[$index];
                    } else {
                        return false;
                        echo 'Token data not found';
                        exit;
                    }
                } else {
                    return false;
                    echo 'Invalid File access';
                    exit;
                }
            } else {
                return false;
                echo 'Invalid Token access';
                exit;
            }
        } else {
            return false;
            echo 'Invalid Token passed';
            exit;
        }
    }

    /**
     * Return unserialized data
     * @param array $token      serialized data having token
     * @param array $token_key  serialized data having token key
     */
    public function unserialized_token($token, $token_key)
    {
        $response['data'] = array();
        $response['status'] = true;
        foreach($token as $___k => $___v) {
            $tmp_tkn = $this->read_token($___v);
            if ($tmp_tkn != false) {
                $response['data']['token'][$___k] = $tmp_tkn;
                $response['data']['token_key'] = $token_key[$___k];
            } else {
                $response['data']['token'][$___k] = false;
            }

            if ($response['status'] == true) {
                if ($response['data']['token'][$___k] == false) {
                    $response['status'] = false;
                }
            }
        }
        return $response;
    }

    /**
     * Return specific or generic Markup value 
     * @param array $carrier    specific Flight wise 
     * @param array $total_fare , $total_pax Total fare & No. of pax
     */
    public function get_admin_markup($carrier, $total_fare, $total_pax)
    {
        $specific_markup = $this->CI->flight_model->get_travelport_flight_admin_markup_details ($carrier);
        $generic_markup = $this->CI->domain_management_model->airline_markup ();
        
        if($specific_markup['status']== true)
        {
            $markup_type = @$specific_markup['markup'][0]['value_type'];
            $markup_val = @$specific_markup['markup'][0]['value'];
        }
        else
        {
            $markup_type = @$generic_markup['generic_markup_list'][0]['value_type'];
            $markup_val = @$generic_markup['generic_markup_list'][0]['value'];
        }

        $display_price_markup = $this->CI->flight_model->travelport_markup ($markup_type,$markup_val,$total_fare,$total_pax);
        //echo "swe rtyu ".print_r($display_price_markup);exit;
        return $display_price_markup;
    }

    public function get_admin_markup_details($carrier, $total_fare, $total_pax,$myconversionvalue)
    {
      /*  @$specific_markup = $this->CI->flight_model->get_travelport_flight_admin_markup_details ($carrier);*/
        /*$generic_markup = $this->CI->domain_management_model->airline_markup ();*/

        if(empty(@$specific_markup['markup'][0]['value']) == false)
        {
            $admin_markup['type'] = 'specific';
             $admin_markup['currency_val'] = $myconversionvalue;
            $admin_markup['markup_type'] = @$specific_markup['markup'][0]['value_type'];
            $admin_markup['markup_val'] = @$specific_markup['markup'][0]['value'];
        }
        else
        {
            $admin_markup['type'] = 'generic';
             $admin_markup['currency_val'] = $myconversionvalue;
            $admin_markup['markup_type'] = @$generic_markup['generic_markup_list'][0]['value_type'];
            $admin_markup['markup_val'] = @$generic_markup['generic_markup_list'][0]['value'];
        }
            $admin_markup['total_pax']=$total_pax;
    
        return $admin_markup;
    } 

    public function search_data_in_preferred_currency_TP($search_result, $currency_obj)
    {
        //echo "TP ";debug($search_result);exit;
        $flights = $search_result['Flights'];
        $flight_list = array();
        foreach($flights as $fk => $fv){
            foreach($fv as $list_k => $list_v){
                $flight_list[$fk][$list_k] = $list_v;
                $flight_list[$fk][$list_k]['FareDetails'] = $this->preferred_currency_fare_object($list_v['FareDetails'], $currency_obj);
                /*$flight_list[$fk][$list_k]['PassengerFareBreakdown'] = $this->preferred_currency_paxwise_breakup_object($list_v['PassengerFareBreakdown'], $currency_obj);*/
            }
        }
        $search_result['Flights'] = $flight_list;
        return $search_result;
    } 

    public function preferred_currency_fare_details_tp($fare_details, $module='', $currency_obj, $default_currency = '')
    {
       //error_reporting(E_ALL);
       // echo "fare_details ";debug($fare_details);exit; 
        //echo "module ";debug($currency_obj);exit;
        $FareDetails = array();
        
        if($module == 'b2b') 
        {
            $FareDetails['b2b_PriceDetails']['BaseFare']           = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['BaseFare']));
            $FareDetails['b2b_PriceDetails']['_CustomerBuying']    = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_CustomerBuying']));
            $FareDetails['b2b_PriceDetails']['_AgentBuying']       = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_AgentBuying']));
            $FareDetails['b2b_PriceDetails']['_AdminBuying']       = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_AdminBuying']));
            $FareDetails['b2b_PriceDetails']['_Markup']            = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_Markup']));
            $FareDetails['b2b_PriceDetails']['_AgentMarkup']       = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_AgentMarkup']));
            $FareDetails['b2b_PriceDetails']['_AdminMarkup']       = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_AdminMarkup']));
            $FareDetails['b2b_PriceDetails']['_Commission']        = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_Commission']));
            $FareDetails['b2b_PriceDetails']['_tdsCommission']     = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_tdsCommission']));
            $FareDetails['b2b_PriceDetails']['_AgentEarning']      = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_AgentEarning']));
            $FareDetails['b2b_PriceDetails']['_TaxSum']            = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_TaxSum']));
            $FareDetails['b2b_PriceDetails']['_BaseFare']          = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_BaseFare']));
            $FareDetails['b2b_PriceDetails']['_TotalPayable']      = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2b_PriceDetails']['_TotalPayable']));
            $FareDetails['b2b_PriceDetails']['Currency']    = get_application_currency_preference();
            $FareDetails['b2b_PriceDetails']['CurrencySymbol']  = $currency_obj->get_currency_symbol($currency_obj->to_currency);
        }
        else{

            $FareDetails['b2c_PriceDetails']['BaseFare'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2c_PriceDetails']['BaseFare']));
            $FareDetails['b2c_PriceDetails']['TotalTax'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2c_PriceDetails']['TotalTax']));
            $FareDetails['b2c_PriceDetails']['TotalFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['b2c_PriceDetails']['TotalFare']));
            $FareDetails['b2c_PriceDetails']['Currency'] =    get_application_currency_preference();
            $FareDetails['b2c_PriceDetails']['CurrencySymbol'] =    $currency_obj->get_currency_symbol($currency_obj->to_currency);
            $FareDetails['b2c_PriceDetails']['Admin_Markup'] =    $fare_details['b2c_PriceDetails']['Admin_Markup'];
        }

        $FareDetails['api_PriceDetails']['Currency'] =    get_application_currency_preference();
        $FareDetails['api_PriceDetails']['BaseFare'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['BaseFare']));
        $FareDetails['api_PriceDetails']['Tax'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['Tax']));
        $FareDetails['api_PriceDetails']['PublishedFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['PublishedFare']));

        $FareDetails['api_PriceDetails']['YQTax'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['YQTax']));
        $FareDetails['api_PriceDetails']['AdditionalTxnFeeOfrd'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['AdditionalTxnFeeOfrd']));
        $FareDetails['api_PriceDetails']['AdditionalTxnFeePub'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['AdditionalTxnFeePub']));
        $FareDetails['api_PriceDetails']['OtherCharges'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['OtherCharges']));
        $FareDetails['api_PriceDetails']['AgentCommission'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['AgentCommission']));
        $FareDetails['api_PriceDetails']['PLBEarned'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['PLBEarned']));
        $FareDetails['api_PriceDetails']['IncentiveEarned'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['IncentiveEarned']));
        $FareDetails['api_PriceDetails']['OfferedFare'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['OfferedFare']));
        $FareDetails['api_PriceDetails']['TdsOnCommission'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['TdsOnCommission']));
        $FareDetails['api_PriceDetails']['TdsOnPLB'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['TdsOnPLB']));
        $FareDetails['api_PriceDetails']['TdsOnIncentive'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['TdsOnIncentive']));
         $FareDetails['api_PriceDetails']['ServiceFee'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['api_PriceDetails']['ServiceFee']));

        //echo "FareDetails_new++ ";debug($FareDetails);
        return $FareDetails;
    }

    /**
     * Search data for fare search result
     * @param array $search_data
     */
    function calendar_safe_search_data($search_data)
    {
        $safe_data = array();
        //Origin
        if (isset($search_data['from']) == true and empty($search_data['from']) == false) {
            $safe_data['from'] = $search_data['from'];
        } else {
            $safe_data['from'] = 'DEL';
        }

        //Destination
        if (isset($search_data['to']) == true and empty($search_data['to']) == false) {
            $safe_data['to'] = $search_data['to'];
        } else {
            $safe_data['to'] = 'BOM';
        }

        //PreferredCarrier
        if (isset($search_data['carrier']) == true and empty($search_data['carrier']) == false) {
            $safe_data['carrier'] = implode(',', $search_data['carrier']);
        } else {
            $safe_data['carrier'] = '';
        }

        //AdultCount
        if (isset($search_data['adult']) == true and empty($search_data['adult']) == false and intval($search_data['adult']) > 0) {
            $safe_data['adult'] = intval($search_data['adult']);
        } else {
            $safe_data['adult'] = 1;
        }

        //DepartureDate
        if (isset($search_data['depature']) == true and empty($search_data['depature']) == false) {
            if (strtotime($search_data['depature']) < time() ) {
                $safe_data['depature'] = date('Y-m-d');
            } else {
                $safe_data['depature'] = date('Y-m-d', strtotime($search_data['depature']));
            }
        } else {
            $safe_data['depature'] = date('Y-m-d');
        }
        //Type
        $safe_data['trip_type'] = 'OneWay';
        //CabinClass
        $safe_data['cabin'] = 'Economy';
        //ReturnDate
        $safe_data['return'] = '';
        //PromotionalPlanType
        $safe_data['PromotionalPlanType'] = 'Normal';
        return $safe_data;

    }


    /**
     * Jaganath
     * Converts Display currency to application currency
     * @param unknown_type $fare_details
     * @param unknown_type $currency_obj
     * @param unknown_type $module
     */
    public function convert_token_to_application_currency($token, $currency_obj, $module)
    {
        $token_details = $token;
        $token = array();
        $application_default_currency = admin_base_currency();
        foreach($token_details as $tk => $tv) {
            $token[$tk] = $tv;
            $temp_fare_details = $tv['FareDetails'];
            //Fare Details
            $FareDetails = array();
            if($module == 'b2c') {
            $PriceDetails = $temp_fare_details[$module.'_PriceDetails'];
            
            $FareDetails['b2c_PriceDetails']['BaseFare'] =          get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['BaseFare']));
            $FareDetails['b2c_PriceDetails']['TotalTax'] =          get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['TotalTax']));
            $FareDetails['b2c_PriceDetails']['TotalFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['TotalFare']));

            $FareDetails['b2c_PriceDetails']['Markup'] =         get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['Markup']));
            $FareDetails['b2c_PriceDetails']['PublishedFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['PublishedFare']));
            $FareDetails['b2c_PriceDetails']['PublishedTax'] =         get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['PublishedTax']));

            $FareDetails['b2c_PriceDetails']['Currency'] =          $application_default_currency;
            $FareDetails['b2c_PriceDetails']['CurrencySymbol'] =    $currency_obj->get_currency_symbol($currency_obj->to_currency);
            } else if($module == 'b2b') {
                $PriceDetails = $temp_fare_details[$module.'_PriceDetails'];
                
                $FareDetails['b2b_PriceDetails']['BaseFare'] =          get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['BaseFare']));
                $FareDetails['b2b_PriceDetails']['_CustomerBuying'] =   get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_CustomerBuying']));
                $FareDetails['b2b_PriceDetails']['_AgentBuying'] =      get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AgentBuying']));
                $FareDetails['b2b_PriceDetails']['_AdminBuying'] =      get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AdminBuying']));
                $FareDetails['b2b_PriceDetails']['_Markup'] =           get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_Markup']));
                $FareDetails['b2b_PriceDetails']['_AgentMarkup'] =      get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AgentMarkup']));
                $FareDetails['b2b_PriceDetails']['_AdminMarkup'] =      get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AdminMarkup']));
                $FareDetails['b2b_PriceDetails']['_Commission'] =       get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_Commission']));
                $FareDetails['b2b_PriceDetails']['_tdsCommission'] =    get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_tdsCommission']));
                $FareDetails['b2b_PriceDetails']['_AgentEarning'] =     get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_AgentEarning']));
                $FareDetails['b2b_PriceDetails']['_TaxSum'] =           get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_TaxSum']));
                $FareDetails['b2b_PriceDetails']['_BaseFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_BaseFare']));
                $FareDetails['b2b_PriceDetails']['_TotalPayable'] =     get_converted_currency_value($currency_obj->force_currency_conversion($PriceDetails['_TotalPayable']));
                $FareDetails['b2b_PriceDetails']['Currency'] =          $application_default_currency;
                $FareDetails['b2b_PriceDetails']['CurrencySymbol'] =    $currency_obj->get_currency_symbol($currency_obj->to_currency);
            }
            $FareDetails['api_PriceDetails'] = $this->preferred_currency_fare_object($temp_fare_details['api_PriceDetails'], $currency_obj, $application_default_currency);
            $token[$tk]['FareDetails'] = $FareDetails;
            //Passenger Breakdown
            $token[$tk]['PassengerFareBreakdown'] = $this->preferred_currency_paxwise_breakup_object($tv['PassengerFareBreakdown'], $currency_obj);
        }
        return $token;
    }

    /**
     * Jaganath
     * Passenger Fare Breakdown details
     * Converts the API Currency to Preferred Currency
     * @param unknown_type $FareDetails
     */
    public function preferred_currency_paxwise_breakup_object($fare_details, $currency_obj)
    {
        $PassengerFareBreakdown = array();
        foreach($fare_details as $k => $v) {
            $PassengerFareBreakdown[$k] = $v;
            $PassengerFareBreakdown[$k]['BaseFare'] = get_converted_currency_value($currency_obj->force_currency_conversion($v['BaseFare']));
        }
        return $PassengerFareBreakdown;
    }

    /**
     * Jaganath
     * Fare Details
     * Converts the API Currency to Preferred Currency
     * @param unknown_type $FareDetails
     */
    private function preferred_currency_fare_object($fare_details, $currency_obj, $default_currency = '')
    {
        
        $FareDetails = array();
        $FareDetails['Currency'] =              empty($default_currency) == false ? $default_currency : get_application_currency_preference();
        $FareDetails['BaseFare'] =              get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['BaseFare']));
        $FareDetails['Tax'] =                   get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['Tax']));
        $FareDetails['YQTax'] =                 get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['YQTax']));
        $FareDetails['AdditionalTxnFeeOfrd'] =  get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['AdditionalTxnFeeOfrd']));
        $FareDetails['AdditionalTxnFeePub'] =   get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['AdditionalTxnFeePub']));
        $FareDetails['OtherCharges'] =          get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['OtherCharges']));
        $FareDetails['Discount'] =              get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['Discount']));
        $FareDetails['PublishedFare'] =         get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['PublishedFare']));
        $FareDetails['AgentCommission'] =       get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['AgentCommission']));
        $FareDetails['PLBEarned'] =             get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['PLBEarned']));
        $FareDetails['IncentiveEarned'] =       get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['IncentiveEarned']));
        $FareDetails['OfferedFare'] =           get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['OfferedFare']));
        $FareDetails['TdsOnCommission'] =       get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['TdsOnCommission']));
        $FareDetails['TdsOnPLB'] =              get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['TdsOnPLB']));
        $FareDetails['TdsOnIncentive'] =        get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['TdsOnIncentive']));
        $FareDetails['ServiceFee'] =            get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['ServiceFee']));
        return $FareDetails;
    }
    /**
     * 
     * Enter description here ...
     */
    public function reindex_passport_expiry_month($passenger_passport_expiry_month, $search_id)
    {
        $safe_search_data = $this->search_data ( $search_id );
        $is_domestic = $safe_search_data['data']['is_domestic'];
        if($is_domestic == false){
            foreach($passenger_passport_expiry_month as $k => $v){
                $passenger_passport_expiry_month[$k] = ($v+1);
            }
        }
        return $passenger_passport_expiry_month;
    }

    /**
     * Reference number generated for booking from application
     * @param $app_booking_id
     * @param $params
     */
    function save_booking($app_booking_id, $book_params, $currency_obj, $module='b2c')
    {
        //debug($book_params);exit;
        //Need to return following data as this is needed to save the booking fare in the transaction details
        $response['fare'] = $response['domain_markup'] = $response['level_one_markup'] = 0;
        $book_total_fare = array();
        $book_domain_markup = array();
        $book_level_one_markup = array();
        $master_transaction_status = 'BOOKING_INPROGRESS';
        $master_search_id = $book_params['search_id'];

        $domain_origin = get_domain_auth_id();
        $app_reference = $app_booking_id;
        $booking_source = $book_params['token']['booking_source'];

        //PASSENGER DATA UPDATE
        $total_pax_count = count($book_params['passenger_type']);
        $pax_count = $total_pax_count;
        
        //PREFERRED TRANSACTION CURRENCY AND CURRENCY CONVERSION RATE 
        $transaction_currency = get_application_currency_preference();
        $application_currency = admin_base_currency();
        $currency_conversion_rate = $currency_obj->transaction_currency_conversion_rate();
        //********************** only for calculation
        $safe_search_data = $this->search_data($master_search_id);
        $safe_search_data = $safe_search_data['data'];
        $safe_search_data['is_domestic_one_way_flight'] = false;
        $from_to_trip_type = $safe_search_data['trip_type'];
        if(strtolower($from_to_trip_type) == 'multicity') {
            $from_loc = $safe_search_data['from'][0];
            $to_loc = end($safe_search_data['to']);
            $journey_from = $safe_search_data['from_city'][0];
            $journey_to = end($safe_search_data['to_city']);
        } else {
            $from_loc = $safe_search_data['from'];
            $to_loc = $safe_search_data['to'];
            $journey_from = isset($safe_search_data['from_city'])?$safe_search_data['from_city']:$safe_search_data['from'];
            $journey_to = isset($safe_search_data['to_city'])?$safe_search_data['to_city']:$safe_search_data['to'];
        }
        //debug($safe_search_data);
        //echo $journey_from;exit();
        $safe_search_data['is_domestic_one_way_flight'] = $GLOBALS['CI']->flight_model->is_domestic_flight($from_loc, $to_loc);
        if ($safe_search_data['is_domestic_one_way_flight'] == false && strtolower($from_to_trip_type) == 'return') {
            $multiplier = $pax_count * 2;//Multiply with 2 for international round way
        } else if(strtolower($from_to_trip_type) == 'multicity'){
            $multiplier = $pax_count * count($safe_search_data['from']);
        } else {
            $multiplier = $pax_count;
        }
        //********************* only for calculation
        $token = $book_params['token']['token'];
        //debug($token);die;
        $master_booking_source = array();
        $currency = $currency_obj->to_currency;
        $deduction_cur_obj  = clone $currency_obj;
        //Storing Flight Details - Every Segment can repeate also
        foreach ($token as $token_index => $token_value) {

            $segment_details = $token_value['SegmentDetails'];
            //debug($segment_details);die;
            $segment_summary = $token_value['SegmentSummary'];
            $Fare = $token_value['FareDetails']['api_PriceDetails'];
            $tmp_domain_markup = 0;
            $tmp_level_one_markup = 0;
            $itinerary_price    = $Fare['BaseFare'];
            //Calculation is different for b2b and b2c
            //Specific Markup Config
            $specific_markup_config = array();
            $specific_markup_config = $this->get_airline_specific_markup_config($segment_details);//Get the Airline code for setting airline-wise markup
            $admin_commission = 0;
            $agent_commission = 0;
            $admin_tds = 0;
            $agent_tds = 0;
            $core_agent_commision = ($Fare['PublishedFare']-$Fare['OfferedFare']);
            $commissionable_fare = $Fare['PublishedFare'];
            if ($module == 'b2c') {             
                $trans_total_fare = $this->total_price($Fare, false, $currency_obj);
                $markup_total_fare  = $currency_obj->get_currency($trans_total_fare, true, false, true, $multiplier, $specific_markup_config);
                $ded_total_fare     = $deduction_cur_obj->get_currency($trans_total_fare, true, true, false, $multiplier, $specific_markup_config);
                $admin_markup = $token_value['FareDetails']['b2c_PriceDetails']['Markup'];
                //$admin_markup = roundoff_number($markup_total_fare['default_value']-$ded_total_fare['default_value']);
               // $admin_markup = '';
                $agent_markup = roundoff_number($ded_total_fare['default_value']-$trans_total_fare);
                $admin_commission = $core_agent_commision;
                $agent_commission = 0;
                
            } else {
                //B2B Calculation
                //Markup
                $trans_total_fare = $Fare['PublishedFare'];
                $markup_total_fare  = $currency_obj->get_currency($trans_total_fare, true, true, true, $multiplier, $specific_markup_config);
                $ded_total_fare     = $deduction_cur_obj->get_currency($trans_total_fare, true, false, true, $multiplier, $specific_markup_config);
                $admin_markup = abs($markup_total_fare['default_value']-$ded_total_fare['default_value']);
                $agent_markup = roundoff_number($ded_total_fare['default_value']-$trans_total_fare);
                //Commission
                $this->commission = $currency_obj->get_commission();
                $AgentCommission = $this->calculate_commission($core_agent_commision);
                $admin_commission = roundoff_number($core_agent_commision-$AgentCommission);//calculate here
                $agent_commission = roundoff_number($AgentCommission);
            }
            //TDS Calculation
            ## $admin_tds = $currency_obj->calculate_tds($admin_commission); // commented by bhola
            $agent_tds = $currency_obj->calculate_tds($agent_commission);

            //**************Ticketing For Each Token START
            //Following Variables are used to save Transaction and Pax Ticket Details
            $pnr = '';
            $book_id = '';
            $source = '';
            $ref_id = '';
            $transaction_status = 0;
            $GetBookingResult = array();
            $transaction_description = '';
            $getbooking_StatusCode = '';
            $getbooking_Description = '';
            $getbooking_Category = '';
            $WSTicket = array();
            $WSFareRule = array();
            //Saving Flight Transaction Details
            $tranaction_attributes = array();
            $pnr = '';
            $book_id = '';
            $source = 'Travelport';
            $ref_id = '';
            $transaction_status = $master_transaction_status;
            $transaction_description = '';
            //Get Booking Details
            $getbooking_status_details = '';
            $getbooking_StatusCode = '';
            $getbooking_Description = '';
            $getbooking_Category = '';
            $tranaction_attributes['Fare'] = $Fare;
            $sequence_number = $token_index;
            //Transaction Log Details
            $ticket_trans_status_group[] = $transaction_status;
            $book_total_fare[]  = $trans_total_fare;
            $book_domain_markup[]   = $admin_markup;
            $book_level_one_markup[] = $agent_markup;
            //Need individual transaction price details
            //SAVE Transaction Details
            $transaction_insert_id = $GLOBALS['CI']->flight_model->save_flight_booking_transaction_details(
            $app_reference, $transaction_status, $transaction_description, $pnr, $book_id, $source, $ref_id,
            json_encode($tranaction_attributes), $sequence_number, $currency, $commissionable_fare, $admin_markup, $agent_markup,
            $admin_commission, $agent_commission,
            $getbooking_StatusCode, $getbooking_Description, $getbooking_Category,
            $admin_tds, $agent_tds
            );
            $transaction_insert_id = $transaction_insert_id['insert_id'];
            //Saving Passenger Details
            $i = 0;
            for ($i=0; $i<$total_pax_count; $i++)
            {
                $passenger_type = $book_params['passenger_type'][$i];
                $is_lead = $book_params['lead_passenger'][$i];
                $title = get_enum_list('title', $book_params['name_title'][$i]);
                $first_name = $book_params['first_name'][$i];
                $middle_name = $book_params['middle_name'][$i];
                $last_name = $book_params['last_name'][$i];
                $date_of_birth = $book_params['date_of_birth'][$i];
                $gender = get_enum_list('gender', $book_params['gender'][$i]);

                $passenger_nationality_id = intval($book_params['passenger_nationality'][$i]);
                $passport_issuing_country_id = intval($book_params['passenger_passport_issuing_country'][$i]);
                $passenger_nationality = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passenger_nationality_id));
                $passport_issuing_country = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passport_issuing_country_id));

                $passenger_nationality = isset($passenger_nationality[$passenger_nationality_id]) ? $passenger_nationality[$passenger_nationality_id] : '';
                $passport_issuing_country = isset($passport_issuing_country[$passport_issuing_country_id]) ? $passport_issuing_country[$passport_issuing_country_id] : '';

                $passport_number = $book_params['passenger_passport_number'][$i];
                $passport_expiry_date = $book_params['passenger_passport_expiry_year'][$i].'-'.$book_params['passenger_passport_expiry_month'][$i].'-'.$book_params['passenger_passport_expiry_day'][$i];
                //$status = 'BOOKING_CONFIRMED';//Check it
                $status = $master_transaction_status;
                $passenger_attributes = array();
                $flight_booking_transaction_details_fk = $transaction_insert_id;//Adding Transaction Details Origin
                //SAVE Pax Details
                $pax_insert_id = $GLOBALS['CI']->flight_model->save_flight_booking_passenger_details(
                $app_reference, $passenger_type, $is_lead, $title, $first_name, $middle_name, $last_name, $date_of_birth,
                $gender, $passenger_nationality, $passport_number, $passport_issuing_country, $passport_expiry_date, $status,
                json_encode($passenger_attributes), $flight_booking_transaction_details_fk);
                //Save passenger ticket information     
                $passenger_ticket_info = $GLOBALS['CI']->flight_model->save_passenger_ticket_info($pax_insert_id['insert_id']);

            }//Adding Pax Details Ends
                
            //Saving Segment Details

            foreach($segment_details as $seg_k => $seg_v) {

                foreach($seg_v as $ws_key => $ws_val) {
                    foreach($ws_val as $ws_key => $ws_val){
                    $FareRestriction = '';
                    $FareBasisCode = '';
                    $FareRuleDetail = '';
                    $airline_pnr = '';
                    $AirlineDetails = $ws_val['AirlineDetails'];
                    $OriginDetails = $ws_val['OriginDetails'];
                    $DestinationDetails = $ws_val['DestinationDetails'];
                    $segment_indicator = $ws_val['SegmentIndicator'];
                    $airline_code = $AirlineDetails['AirlineCode'];
                    $airline_name = $AirlineDetails['AirlineName'];
                    $flight_number = $AirlineDetails['FlightNumber'];
                    $fare_class = $AirlineDetails['FareClass'];
                    $from_airport_code = $OriginDetails['AirportCode'];
                    $from_airport_name = $OriginDetails['AirportName'];
                    $to_airport_code = $DestinationDetails['AirportCode'];
                    $to_airport_name = $DestinationDetails['AirportName'];
                    $departure_datetime = date('Y-m-d H:i:s', strtotime($OriginDetails['DepartureTime']));
                    $arrival_datetime = date('Y-m-d H:i:s', strtotime($DestinationDetails['ArrivalTime']));
                    $iti_status = $ws_val['Status'];
                    $operating_carrier = $AirlineDetails['OperatingCarrier'];
                    $attributes = array('craft' => $ws_val['Craft'], 'ws_val' => $ws_val);
                    //SAVE ITINERARY
                    $GLOBALS['CI']->flight_model->save_flight_booking_itinerary_details(
                    $app_reference, $segment_indicator, $airline_code, $airline_name, $flight_number, $fare_class, $from_airport_code, $from_airport_name,
                    $to_airport_code, $to_airport_name, $departure_datetime, $arrival_datetime, $iti_status, $operating_carrier, json_encode($attributes),
                    $FareRestriction, $FareBasisCode, $FareRuleDetail, $airline_pnr);
                }

                }
            }//End Of Segments Loop
        }//End Of Token Loop

        //Save Master Booking Details
        $book_total_fare = array_sum($book_total_fare);
        $book_domain_markup = array_sum($book_domain_markup);
        $book_level_one_markup = array_sum($book_level_one_markup);

        $phone = $book_params['passenger_contact'];
        $alternate_number = '';
        $email = $book_params['billing_email'];
        $start = $token[0];
        $end = end($token);
        // debug($segment_summary);exit;
        $journey_start = $segment_summary[0]['OriginDetails']['DepartureTime'];
        $journey_start = date('Y-m-d H:i:s', strtotime($journey_start));
        $journey_end = end($segment_summary);
        $journey_end = $journey_end['DestinationDetails']['ArrivalTime'];
        $journey_end = date('Y-m-d H:i:s', strtotime($journey_end));
        $payment_mode = $book_params['payment_method'];
        $created_by_id = intval(@$GLOBALS['CI']->entity_user_id);

        $passenger_country_id = intval($book_params['billing_country']);
        //$passenger_city_id = intval($book_params['billing_city']);
        $passenger_country = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passenger_country_id));
        //$passenger_city = $GLOBALS['CI']->db_cache_api->get_city_list(array('k' => 'origin', 'v' => 'destination'), array('origin' => $passenger_city_id));

        $passenger_country = isset($passenger_country[$passenger_country_id]) ? $passenger_country[$passenger_country_id] : '';
        //$passenger_city = isset($passenger_city[$passenger_city_id]) ? $passenger_city[$passenger_city_id] : '';
        $passenger_city = $book_params['billing_city'];

        $attributes = array('country' => $passenger_country, 'city' => $passenger_city, 'zipcode' => $book_params['billing_zipcode'], 'address' =>  $book_params['billing_address_1']);
        $flight_booking_status = $master_transaction_status;
        //SAVE Booking Details
		$domain_id = $GLOBALS['CI']->session->userdata('domain_id');
		$branch_id = $GLOBALS['CI']->session->userdata('branch_id');
		$user_details_id = $GLOBALS['CI']->session->userdata('user_details_id');
		$user_type =  $GLOBALS['CI']->session->userdata('user_type'); 
        $GLOBALS['CI']->flight_model->save_flight_booking_details(      
        $domain_origin, $flight_booking_status, $app_reference, $booking_source, $phone, $alternate_number, $email,
        $journey_start, $journey_end, $journey_from, $journey_to, $payment_mode, json_encode($attributes), $created_by_id,
        $from_loc, $to_loc, $from_to_trip_type, $transaction_currency, $currency_conversion_rate
        ,$domain_id,$branch_id,$user_details_id,$user_type);
 
        /************** Update Convinence Fees And Other Details Start ******************/
        //Convinence_fees to be stored and discount
        $convinence = 0;
        $discount = 0;
        $convinence_value = 0;
        $convinence_type = 0;
        $convinence_type = 0;
        if ($module == 'b2c') {         
            $total_transaction_amount = $book_total_fare+$book_domain_markup;
            $convinence = $currency_obj->convenience_fees($total_transaction_amount, $master_search_id);
            
            $convinence_row = $currency_obj->get_convenience_fees();
            $convinence_value = $convinence_row['value'];
            $convinence_type = $convinence_row['type'];
            $convinence_per_pax = $convinence_row['per_pax'];
        } elseif ($module == 'b2b') {
            $discount = 0;
            $convinence_per_pax = 0;
        }
        $GLOBALS['CI']->load->model('transaction');
        //SAVE Convinience and Discount Details
        $GLOBALS['CI']->transaction->update_convinence_discount_details('flight_booking_details', $app_reference, $discount, $convinence, $convinence_value, $convinence_type, $convinence_per_pax);
        /************** Update Convinence Fees And Other Details End ******************/

        /**
         * Data to be returned after transaction is saved completely
         */
        $response['fare'] = $book_total_fare;
        $response['admin_markup'] = $book_domain_markup;
        $response['agent_markup'] = $book_level_one_markup;
        $response['convinence'] = $convinence;
        $response['discount'] = $discount;

        $response['status'] = $flight_booking_status;
        $response['status_description'] = $transaction_description;
        $response['name'] = $first_name;
        $response['phone'] = $phone;
        return $response;
    }
    /**
     *
     */
    private function calculate_commission($agent_com)
    {
        $agent_com_row = $this->commission['admin_commission_list'];
        $b2b_comm = 0;
        if ($agent_com_row['value_type'] == 'percentage') {
            //%
            $b2b_comm = ($agent_com/100)*$agent_com_row['value'];
        } else {
            //plus
            $b2b_comm = ($agent_com-$agent_com_row['value']);
        }
        return number_format($b2b_comm, 2, '.', '');
    }
    /**
   * Returns First segment Airline Code to set the markup based on Airline
   */
      public function get_airline_specific_markup_config($segment_details)
      {
        $specific_markup_config = array();
        $airline_code = $segment_details[0][0]['AirlineDetails']['AirlineCode'];
        $category = 'airline_wise';
        $specific_markup_config[] = array('category' => $category, 'ref_id' => $airline_code);
        return $specific_markup_config;
      }

    /**
     * Search data for Auto ticketing TP Riya
     * @param array $search_data
     */
    function do_autoticket_tp($parent_pnr,$strAirPNR){
        $debug = 0;
        $strFolderPath = "xml_logs/Flight/travelport/ticketing_logs";
        $strMethod     = "Sync";

        $strTargetBranch   = "P3058994";// Riya ticketing target branch
        //$strTraceID        = $Credentials['Trace_ID'];

        $strTicketReq  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://www.travelport.com/schema/common_v41_0" xmlns:univ="http://www.travelport.com/schema/universal_v41_0">';
        $strTicketReq .= '<soapenv:Header/>';
        $strTicketReq .= '<soapenv:Body>';
        $strTicketReq .= '<air:AirTicketingReq BulkTicket="false" ReturnInfoOnFail="true" TargetBranch="'.$strTargetBranch.'" xmlns:air="http://www.travelport.com/schema/air_v41_0">';
        $strTicketReq .= '<com:BillingPointOfSaleInfo OriginApplication="UAPI"/>';
        $strTicketReq .= '<air:AirReservationLocatorCode>'.$strAirPNR.'</air:AirReservationLocatorCode>';
        $strTicketReq .= '</air:AirTicketingReq>';
        $strTicketReq .= '</soapenv:Body>';
        $strTicketReq .= '</soapenv:Envelope>';

        if(true){
            $AirTicketReservationRes = $this->process_request_auto_ticketing($strTicketReq);
          
            /*XML Logs*/  
                $xml_title = 'AirTicketRQ_'.$parent_pnr.'.xml';
                $date = date('Ymd_His');
                if (!file_exists('xml_logs/Flight/travelport/ticketing_logs')) {
                     mkdir('xml_logs/Flight/travelport/ticketing_logs', 0777, true);
                }
            $requestcml =$xml_title;
            file_put_contents('../b2b2b/xml_logs/Flight/travelport/ticketing_logs/'.$requestcml, $strTicketReq); 

            /*XML Logs*/
                $xml_title = 'AirTicketRs_'.$parent_pnr.'.xml';
                $date = date('Ymd_His');
                if (!file_exists('xml_logs/Flight/travelport/ticketing_logs')) {
                     mkdir('xml_logs/Flight/travelport/ticketing_logs', 0777, true);
                }
            $requestcml =$xml_title;
            file_put_contents('../b2b2b/xml_logs/Flight/travelport/ticketing_logs/'.$requestcml, $AirTicketReservationRes);


            $AirTicketReservationReq_Res = array(
                'AirTicketReservationReq' => $strTicketReq,
                'AirTicketReservationRes' => $AirTicketReservationRes
            );
        }

        if(false){
            $AirTicketReservationRes = file_get_contents(FCPATH . "/".$strFolderPath."/AirTicketRs_f3c37n6gnsvm.xml");

            $AirTicketReservationReq_Res = array(
                'AirTicketReservationReq' => $strTicketReq,
                'AirTicketReservationRes' => $AirTicketReservationRes
            );
        }
        return $AirTicketReservationReq_Res;
    }

    /**
     * process soap API request for auto_ticketing TP
     * 
     * @param string $request           
     */
    function process_request_auto_ticketing($request) {
        
        $username_ATP ='uAPI1605217276-4bcd3655';
        $password_ATP ='5w$KB_3a4A';
        $url_ATP ='https://americas.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/AirService';

        $soapAction = '';
        $Authorization = base64_encode ( 'Universal API/' . $username_ATP . ':' . $password_ATP );

        $httpHeader = array(
                "Connection:close",
                "Content-length: ".strlen($request),
                "Content-Type: text/xml;charset=UTF-8", 
                "Accept-Encoding: gzip,deflate",
                "Authorization: Basic $Authorization",
                "SOAPAction: {$soapAction}"
            );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url_ATP );
        curl_setopt ( $ch, CURLOPT_TIMEOUT, 180 );
        curl_setopt ( $ch, CURLOPT_HEADER, 0 );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $ch, CURLOPT_POST, 1 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $request );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, 2 ); // sd
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, FALSE );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $httpHeader );
        curl_setopt ( $ch, CURLOPT_ENCODING, "gzip,deflate" );
        $response = curl_exec ( $ch );
        $error = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );
        
        curl_close ( $ch );
        return $response;
    }
    

    /**
     * Kanha Sahu - kanha.0026@gmail.com
    **/
    function split_datetime_timezone($datetime_zone){
        if(strrpos($datetime_zone,'+') || strrpos($datetime_zone,'-')){
            if(strrpos($datetime_zone,'+')){
                return substr($datetime_zone, 0, strrpos($datetime_zone,'+'));
            }else{
                return substr($datetime_zone, 0, strrpos($datetime_zone,'-'));
            }
        }else{
            return $datetime_zone;
        }
    }
}
