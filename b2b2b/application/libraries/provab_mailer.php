<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * provab
 *
 * Travel Portal Application
 *
 * @package		provab
 * @author		Arjun J<arjun.provab@gmail.com>
 * @copyright	Copyright (c) 2013 - 2014
 * @link		http://provab.com
 */

class Provab_Mailer {

	/**
	 * Provab Email Class
	 *
	 * Permits email to be sent using Mail, Sendmail, or SMTP.
	 *
	 * @package		provab
	 * @subpackage	Libraries
	 * @category	Libraries
	 * @author		Arjun J<arjun.provab@gmail.com>
	 * @link		http://provab.com
	 */
	public $CI;                     //instance of codeigniter super object
	public $mailer_status;         //mailer status which indicates if the mail should be sent or not
	public $mail_configuration;    //mail configurations defined by user

	/**
	 * Constructor - Loads configurations and get ci super object reference
	 */
	public function __construct($data='')
	{ 
		if (empty($data) == false && intval($data['id']) > 0) {
			$id = intval($data['id']);
		} else {
			$id = GENERAL_EMAIL;
		} 
		$CI = & get_instance();
		$domain_id=$CI->session->userdata('domain_id'); 
		$this->CI =& get_instance();
		$this->CI->load->model('custom_db');
		$return_data = $this->CI->custom_db->single_table_records('domain_details','*' ,array('domain_details_id' => $domain_id));
		//debug($return_data['data'][0]);die();     
		$this->mail_configuration = @$return_data['data'][0];
	}

	/**
	 *Initialize mailer to send mail
	 *
	 *return array containing the status of initialization
	 */
	public function initialize_mailer()
	{
		$status = false;
		$message = 'Please Contact Admin To Setup Mail Configuration';
		$this->mailer_status = false;

		if (is_array($this->mail_configuration) == true and count($this->mail_configuration) > 0) {
			if (intval($this->mail_configuration['domain_status']) == "ACTIVE" ) {
				if (isset($this->mail_configuration['domain_user_name']) == true and empty($this->mail_configuration['domain_user_name']) == false and
				isset($this->mail_configuration['domain_password']) == true and empty($this->mail_configuration['domain_password']) == false
				) {
					//configure email settings
					/*	for localhost
					 * $config['protocol'] = 'smtp';
					 * $config['charset'] = 'iso-8859-1';
					 */
					 /*	for server
					 * $config['protocol'] = 'gsmtp';
					 * $config['charset'] = 'utf-8';
					 */
					//set mail configurations
					$config['useragent'] = 'PHPMailer'; 
					$config['smtp_user'] = trim($this->mail_configuration['domain_user_name']);
					$config['smtp_pass'] = trim($this->mail_configuration['domain_password']);
					$config['smtp_port'] = isset($this->mail_configuration['domain_port']) == true ? $this->mail_configuration['domain_port'] : 465;
					$config['smtp_host'] = isset($this->mail_configuration['domain_host']) == true ? $this->mail_configuration['domain_host'] : 'ssl://smtp.gmail.com';

					$config['wordwrap'] = FALSE; 
					$config['mailtype'] = 'html';
					//$config['charset'] = 'iso-8859-1';
					$config['charset'] = 'utf-8';
					$config['crlf'] = "\r\n";
					$config['newline'] = "\r\n";
					$config['protocol'] = 'sendmail';  
					// $config['protocol'] = 'smtp';
					//$config['protocol'] = 'gsmtp';
					$this->CI->load->library('email', $config);
					$CI = & get_instance();
					$project_name=$CI->session->userdata('company_name'); 
					$from = isset($this->mail_configuration['from']) == true ? $this->mail_configuration['from'] : $project_name;  
					$this->CI->email->from($this->mail_configuration['domain_user_name'], $from);
					$this->CI->email->set_newline("\r\n");

					//set cc and bcc
					if (isset($this->mail_configuration['bcc']) == true and empty($this->mail_configuration['bcc']) == false) {
						$this->CI->email->bcc($this->mail_configuration['bcc']);
					}
					if (isset($this->mail_configuration['cc']) == true and empty($this->mail_configuration['cc']) == false) {
						$this->CI->email->cc($this->mail_configuration['cc']);
					}
					$this->mailer_status = true;
					$status = true;
					$message = 'Continue To Send Mail';
				}
			}
		}
		return array('status' => $status, 'message' => $message);
	}

	/**
	 *send mail to the user
	 *
	 *@param string $to_email     email id to which the mail has to be delivered
	 *@param string $mail_subject mail subject which has to be sent in the mail
	 *@param string $mail_message mail message which has to be sent in the mail body
	 *@return boolean status of sending mail
	 *@$attachment for single attachment pass file name , for multiple pass as array of filenames
	 *$cc and $bcc pass as array
	 */
	public function send_mail($to_email, $mail_subject, $mail_message, $attachment='', $cc='', $bcc='')
	{  
		$status = false;
		//initializing mailer configurations
		$ini_status = $this->initialize_mailer();
		$message = $ini_status['message']; 
		//sending mail based on mailer status
		if ($this->mailer_status == true) {
			if ($to_email != '' && $mail_message != '' && $mail_subject != '') {
				//set mail data
				$this->CI->email->to(trim(strip_tags($to_email)));
				$this->CI->email->subject(trim($mail_subject));
				$this->CI->email->message($mail_message);
				//and attachment 
				if (empty($attachment) == false) {
					if(valid_array($attachment)) {
						//for multple attachements
						foreach($attachment as $k => $v) {
							if(empty($v) == false) {
								$this->CI->email->attach($v);
							}
						}
					} else if(strlen($attachment) > 1){
						//for single attachements
						$this->CI->email->attach($attachment);
					}
				}
				//add CC
				if(empty($cc) == false && valid_array($cc)) {
					$ccEmail = '';
					//Validating Email
					foreach($cc as $k => $v) {
						if(filter_var($v, FILTER_VALIDATE_EMAIL) == true) {
							$ccEmail[] = trim($v);							
						}
					}
					if(valid_array($ccEmail)) {
						$this->CI->email->cc($ccEmail);
					}					
				}				
			    //add BCC
				if(empty($bcc) == false && valid_array($bcc)) {
				    $bccEmail = ''; 
					//Validating Email
					foreach($bcc as $k => $v) {
						if(filter_var($v, FILTER_VALIDATE_EMAIL) == true) {
							$bccEmail[] = trim($v);							
						}
					}
					if(valid_array($bccEmail)) {
						$this->CI->email->bcc($bccEmail);
					}
				}
				$this->CI->load->library('email');
				$result = $this->CI->email->send();
				//debug($this->CI->email->print_debugger());die;
				if($result) {
					$status = true;
					$message = 'Mail Sent Successfully'; 
				} else {
					$status = false;
					$message = $this->CI->email->print_debugger();
				}
			} else {
				$status = false;
				$message = 'Please Provide To Email Address, Mail Subject And Mail Message';
			}
		}
		return array('status' => $status, 'message' => $message);
	} 
}
?>