<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
require_once 'Common_Api_Grind.php';
/**
 *
 * @package Provab
 * @subpackage API
 * @author Arjun J<arjunjgowda260389@gmail.com>
 * @version V1
 */
class Provab_hotelcrs extends Common_Api_Grind {
	private $ClientId;
	private $UserName;
	private $Password;
	private $service_url;
	private $Url;
	private $LoginType = '2'; // (For API Users, value should be ‘2’)
	private $EndUserIp = '127.0.0.1';
	private $TokenId; // Token ID that needs to be echoed back in every subsequent request
	public $master_search_data;
	public $search_hash;
	public function __construct() {
		$this->CI = &get_instance ();
		$GLOBALS ['CI']->load->library ( 'Api_Interface' );
		$GLOBALS ['CI']->load->model ( 'hotels_model' );
		$this->TokenId = $GLOBALS ['CI']->session->userdata ( 'tb_auth_token' );
		$this->set_api_credentials ();
		// if found in session then token will not be replaced
		// $this->set_authenticate_token();
	}
	private function get_header() {
		$hotel_engine_system = $this->CI->config->item ( 'hotel_engine_system' ); 
		$response ['UserName'] = 'test';
		$response ['Password'] = 'test';
		$response ['DomainKey'] = $this->CI->config->item ( 'domain_key' );
		$response ['system'] = $hotel_engine_system;
		
		return $response;
	}
	private function set_api_credentials() {
		$hotel_engine_system = $this->CI->config->item ( 'hotel_engine_system' );
		$this->system = $hotel_engine_system;
		$this->UserName = $this->CI->config->item ( $hotel_engine_system . '_username' );
		$this->Password = $this->CI->config->item ( $hotel_engine_system . '_password' );
		$this->Url = $this->CI->config->item ( 'hotel_url' );
		$this->ClientId = $this->CI->config->item ( 'domain_key' );
		// $this->UserName = 'test';
		// $this->Password = 'password'; // miles@123 for b2b
	}

	
	public function get_authenticate_token() {
		return $GLOBALS ['CI']->session->userdata ( 'tb_auth_token' );
	}

	private function hotel_search_request($search_params) {
		//$this->set_authenticate_token ( true );
		$response ['status'] = true;
		$response ['data'] = array ();
		$currency_obj = new Currency ( array (
				'module_type' => 'hotel' 
		) );
		/**
		 * Request to be formed for search *
		 */
		$request ['EndUserIp'] = '127.0.0.1';
		$request ['TokenId'] = sha1(time().uniqid().'crs'.'#%#sumeru');
		$request ['BookingMode'] = 5; // value to be 5 for api users
		$request ['CheckInDate'] = $search_params ['from_date']; // dd/mm/yyyy
		$request ['NoOfNights'] = $search_params ['no_of_nights']; // Min 1
		$request ['CountryCode'] = $search_params ['country_code']; // ISO Country Code of Destination
		$request ['CityId'] = intval ( $search_params ['location_id'] );
		$request ['PreferredCurrency'] = 'INR'; // INR only
		$request ['GuestNationality'] = ISO_INDIA; // ISO Country Code
		$request ['NoOfRooms'] = intval ( $search_params ['room_count'] );
		
		$room_index = $temp_child_index = 0;
		for($room_index = 0; $room_index < $request ['NoOfRooms']; $room_index ++) {
			$temp_room_config = '';
			$temp_room_config ['NoOfAdults'] = intval ( $search_params ['adult_config'] [$room_index] );
			$temp_room_config ['NoOfChild'] = intval ( $search_params ['child_config'] [$room_index] );
			if ($search_params ['child_config'] [$room_index] > 0) {
				$temp_room_config ['ChildAge'] = array_slice ( $search_params ['child_age'], $temp_child_index, intval ( $search_params ['child_config'] [$room_index] ) );
				$temp_child_index += intval ( $search_params ['child_config'] [$room_index] );
			}
			$request ['RoomGuests'] [] = $temp_room_config;
		}
		$request ['PreferredHotel'] = '';
		$request ['MinRating'] = 0;
		$request ['MaxRating'] = 5;
		$request ['IsNearBySearchAllowed'] = true;
		$request ['SortBy'] = 0;
		$request ['OrderBy'] = 0;
		$request ['ResultCount'] = 0;
		$request ['ReviewScore'] = 0;
		
		$response ['data'] ['request'] = json_encode ( $request );
		//$this->credentials ( 'GetHotelResult' );
		//$response ['data'] ['service_url'] = $this->service_url;
		
		return $response;
	}

	function get_hotel_list($search_id = '') { 

		$this->CI->load->driver ( 'cache' );
		$header = $this->get_header (); 
		$response ['data'] = array ();
		$response ['status'] = true;
		
		$search_data = $this->search_data ( $search_id );
		
		$cache_search = $this->CI->config->item ( 'cache_hotel_search' );
		
		$search_hash = $this->search_hash;
		// debug($search_hash);exit;
		if ($cache_search) {
			$cache_contents = $this->CI->cache->file->get ( $search_hash );
			
		}

		$response['search_request'] = $this->hotel_search_request ( $search_data ['data'] );

		$cidate = explode('/', $search_data['data']['from_date']);
		$CIDate = $cidate[2].'-'. $cidate[1].'-'. $cidate[0];

		$codate = explode('/', $search_data['data']['to_date']);
        $CODate = $codate[2].'-'. $codate[1].'-'. $codate[0];

        $datetime1 = new DateTime($CIDate);
        $datetime2 = new DateTime($CODate);

        //$oDiff = $datetime1->diff($datetime2); 
        //$stay_days = intval($oDiff->d); 
        $stay_days = $search_data['data']['no_of_nights'];

        $s_max_adult = max($search_data['data']['adult_config']);
        $s_max_child = max($search_data['data']['child_config']);
        if($s_max_child){
            $s_max_child = $s_max_child;
        }else{
            $s_max_child = 0;
        }
        $checkin_date = date('Y-m-d',strtotime($CIDate));
        $checkout_date = date('Y-m-d',strtotime($CODate));

        $room_count = $search_data['data']['room_count'];
        $city_id = $search_data['data']['city_name'];

        $safe_search_data = $GLOBALS ['CI']->hotels_model->get_crs_search_data ( $datetime1, $datetime2,$stay_days,$s_max_adult,$s_max_child,$checkin_date,$checkout_date,$room_count,$city_id);
        //debug($safe_search_data);exit();
       	$response ['data'] = $safe_search_data;
        return $response;      
	}

	/**
	 * Sanjay Polisetty
	 * convert search params to format
	 */
	public function search_data($search_id) {
		$response ['status'] = true;
		$response ['data'] = array ();
		if (empty ( $this->master_search_data ) == true and valid_array ( $this->master_search_data ) == false) {
			$clean_search_details = $GLOBALS ['CI']->hotels_model->get_safe_search_data ( $search_id );
			if ($clean_search_details ['status'] == true) {
				$response ['status'] = true;
				$response ['data'] = $clean_search_details ['data'];
				// 28/12/2014 00:00:00 - date format
				$response ['data'] ['from_date'] = date ( 'd/m/Y', strtotime ( $clean_search_details ['data'] ['from_date'] ) );
				$response ['data'] ['to_date'] = date ( 'd/m/Y', strtotime ( $clean_search_details ['data'] ['to_date'] ) );
				// get city id based
				$location_details = $GLOBALS ['CI']->hotels_model->tbo_hotel_city_id ( $clean_search_details ['data'] ['city_name'], $clean_search_details ['data'] ['country_name'] );
				if ($location_details ['status']) {
					$response ['data'] ['country_code'] = $location_details ['data'] ['country_code'];
					$response ['data'] ['location_id'] = $location_details ['data'] ['origin'];
				} else {
					$response ['data'] ['country_code'] = $response ['data'] ['location_id'] = '';
				}
				$this->master_search_data = $response ['data'];
			} else {
				$response ['status'] = false;
			}
		} else {
			$response ['data'] = $this->master_search_data;
		}
		$this->search_hash = md5 ( serialized_data ( $response ['data'] ) );
		return $response;
	}

	function booking_url($search_id) {
		return base_url () . 'index.php/hotels/bookings/' . intval ( $search_id );
	}

	function process_booking($book_id, $booking_params) { 
	}
	
	function total_price($price_summary) {
		return ($price_summary ['OfferedPriceRoundedOff']);
	}

	function update_markup_currency(&$price_summary, &$currency_obj){

	}

	function filter_summary($hl) { 
		$h_count = 0;
		$filt ['p'] ['max'] = false;
		$filt ['p'] ['min'] = false;
		$filt ['loc'] = array ();
		$filt ['star'] = array ();
		$filters = array ();
		foreach ( $hl as $hr => $hd ) { //print_r($hd['star_rating']); exit();
			// filters
			$StarRating = intval ( @$hd ['star_rating'] );
			$HotelLocation = '';
			
			if (isset ( $filt ['star'] [$StarRating] ) == false) {
				$filt ['star'] [$StarRating] ['c'] = 1;
				$filt ['star'] [$StarRating] ['v'] = $StarRating;
			} else {
				$filt ['star'] [$StarRating] ['c'] ++;
			}
			$price = '30';
			if (($filt ['p'] ['max'] != false && $filt ['p'] ['max'] < $price) || $filt ['p'] ['max'] == false) {
				$filt ['p'] ['max'] = $price; //roundoff_number ( $hd ['Price'] ['RoomPrice'] );
			}
			if (($filt ['p'] ['min'] != false && $filt ['p'] ['min'] > $price) || $filt ['p'] ['min'] == false) {
				$filt ['p'] ['min'] = $price; //roundoff_number ( $hd ['Price'] ['RoomPrice'] );
			}
			
			if (($filt ['p'] ['min'] != false && $filt ['p'] ['min'] > $price) || $filt ['p'] ['min'] == false) {
				$filt ['p'] ['min'] = $price; //$hd ['Price'] ['RoomPrice'];
			}
			$hloc = 'Others'; //ucfirst ( strtolower ( $HotelLocation ) );
			if (isset ( $filt ['loc'] [$hloc] ) == false) {
				$filt ['loc'] [$hloc] ['c'] = 1;
				$filt ['loc'] [$hloc] ['v'] = $hloc;
			} else {
				$filt ['loc'] [$hloc] ['c'] ++;
			}
			
			$filters ['data'] = $filt;
			$h_count ++;
		}
		ksort ( $filters ['data'] ['loc'] );
		$filters ['hotel_count'] = $h_count;
		
		return $filters;
	}
	/**
	 * Tax price is the price for which markup should not be added
	 */
	function tax_service_sum($markup_price_summary, $api_price_summary) {
		// sum of tax and service ;
		return ($api_price_summary ['ServiceTax'] + $api_price_summary ['Tax'] + ($markup_price_summary ['PublishedPrice'] - $api_price_summary ['PublishedPrice']));
	}
	function cancel_booking($booking_source,$app_reference){
		if(empty($app_reference) == false &&  $booking_source ==PROVAB_HOTEL_CRS_){
			$result = $GLOBALS ['CI']->hotels_model->hotel_crs_cancel_request($app_reference,$booking_source);
		}else{
			$result = ['msg'=>'Some thing went wrong Try again!','status'=>0];
			
		}
		return $result;	
	}

		public function roomlist_in_preferred_currency($room_list, $currency_obj,$search_id,$module='b2c') {

		$level_one = true;
		$current_domain = true;
		if ($module == 'b2c') {
			$level_one = false;
			$current_domain = true;
		} else if ($module == 'b2b') {
			$level_one = true;
			$current_domain = true;
		}

		$application_currency_preference = get_application_currency_preference ();
		$hotel_room_details = $room_list ['data'] ['GetHotelRoomResult'] ['HotelRoomsDetails'];
		$hotel_room_result = array ();
		foreach ( $hotel_room_details as $hr_k => $hr_v ) {
			$hotel_room_result [$hr_k] = $hr_v;
			// Price
			$API_raw_price = $hr_v ['Price'];
			
			$Price = $this->preferred_currency_fare_object ( $hr_v ['Price'], $currency_obj );
			// CancellationPolicies
			$CancellationPolicies = array ();
			foreach ( $hr_v ['CancellationPolicies'] as $ck => $cv ) {
				//add cancellation charge in markup
				
				$Charge = $this->update_cancellation_markup_currency($cv['Charge'],$currency_obj,$search_id,$level_one,$current_domain);
				
				$CancellationPolicies [$ck] = $cv;
				$CancellationPolicies [$ck] ['Currency'] = $application_currency_preference;
				//$CancellationPolicies [$ck] ['Charge'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $Charge ) );
				$CancellationPolicies [$ck] ['Charge'] = $Charge;

			}
			$hotel_room_result [$hr_k] ['API_raw_price'] = $API_raw_price;
			$hotel_room_result [$hr_k] ['Price'] = $Price;
			$hotel_room_result [$hr_k] ['CancellationPolicies'] = $CancellationPolicies;
			// CancellationPolicy:FIXME: convert the INR price to preferred currency
		}
		$room_list ['data'] ['GetHotelRoomResult'] ['HotelRoomsDetails'] = $hotel_room_result;
		return $room_list;
	}
		/**
	*Update Markup currency for Cancellation Charge
	*/
	function update_cancellation_markup_currency(&$cancel_charge,&$currency_obj,$search_id,$level_one_markup=false,$current_domain_markup=true){
		$search_data = $this->search_data ( $search_id );
		
		$no_of_nights = $this->master_search_data ['no_of_nights'];
		$temp_price = $currency_obj->get_currency ( $cancel_charge, true, $level_one_markup, $current_domain_markup, $no_of_nights );
				
		return round($temp_price['default_value']);
	}

	/**
	 * update markup currency and return summary
	 * $attr needed to calculate number of nights markup when its plus based markup
	 */
	private function preferred_currency_fare_object($fare_details, $currency_obj, $default_currency = '') {
		$price_details = array ();
		

		$price_details ['CurrencyCode'] = empty ( $default_currency ) == false ? $default_currency : get_application_currency_preference ();

		$price_details ['RoomPrice'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['RoomPrice'] ) );

		$price_details ['Tax'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['Tax'] ) );

		$price_details ['ExtraGuestCharge'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['ExtraGuestCharge'] ) );

		$price_details ['ChildCharge'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['ChildCharge'] ) );
		$price_details ['OtherCharges'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['OtherCharges'] ) );
		$price_details ['Discount'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['Discount'] ) );
		$price_details ['PublishedPrice'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['PublishedPrice'] ) );
		$price_details ['PublishedPriceRoundedOff'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['PublishedPriceRoundedOff'] ) );
		$price_details ['OfferedPrice'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['OfferedPrice'] ) );
		$price_details ['OfferedPriceRoundedOff'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['OfferedPriceRoundedOff'] ) );
		$price_details ['AgentCommission'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['AgentCommission'] ) );
		$price_details ['AgentMarkUp'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['AgentMarkUp'] ) );
		$price_details ['ServiceTax'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['ServiceTax'] ) );
		$price_details ['TDS'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['TDS'] ) );
		
		return $price_details;
	}

	function update_room_markup_currency(& $price_summary, & $currency_obj, $search_id, $level_one_markup = false, $current_domain_markup = true) {
		$search_data = $this->search_data ( $search_id );
		$no_of_nights = $this->master_search_data ['no_of_nights'];
		$no_of_rooms = 1;
		$multiplier = ($no_of_nights * $no_of_rooms);
		return $this->update_markup_currency ( $price_summary, $currency_obj, $multiplier, $level_one_markup, $current_domain_markup );
	}

		public function convert_token_to_application_currency($token, $currency_obj, $module) {


			// debug($token);
			// exit("364-provab_hotel_crs");
		$master_token = array ();
		$price_token = array ();
		$price_summary = array ();
		$markup_price_summary = array ();
		// Price Token
		foreach ( $token ['price_token'] as $ptk => $ptv ) {
			$price_token [$ptk] = $this->preferred_currency_fare_object ( $ptv, $currency_obj, admin_base_currency () );
		}
		// Price Summary
		$price_summary = $this->preferred_currency_price_summary ( $token ['.9'], $currency_obj );
		// Markup Price Summary
		$markup_price_summary = $this->preferred_currency_price_summary ( $token ['markup_price_summary'], $currency_obj );
		// Assigning the Converted Data
		$master_token = $token;
		$master_token ['price_token'] = $price_token;
		$master_token ['price_summary'] = $price_summary;
		$master_token ['markup_price_summary'] = $markup_price_summary;
		$master_token ['default_currency'] = admin_base_currency ();
		$master_token ['convenience_fees'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $token ['convenience_fees'] ) ); // check this
		return $master_token;
	}

		private function preferred_currency_price_summary($fare_details, $currency_obj) {
		$price_details = array ();
		$price_details ['RoomPrice'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['RoomPrice'] ) );
		$price_details ['PublishedPrice'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['PublishedPrice'] ) );
		$price_details ['PublishedPriceRoundedOff'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['PublishedPriceRoundedOff'] ) );
		$price_details ['OfferedPrice'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['OfferedPrice'] ) );
		$price_details ['OfferedPriceRoundedOff'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['OfferedPriceRoundedOff'] ) );
		$price_details ['ServiceTax'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['ServiceTax'] ) );
		$price_details ['Tax'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['Tax'] ) );
		$price_details ['ExtraGuestCharge'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['ExtraGuestCharge'] ) );
		$price_details ['ChildCharge'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['ChildCharge'] ) );
		$price_details ['OtherCharges'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['OtherCharges'] ) );
		$price_details ['TDS'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['TDS'] ) );
		return $price_details;
	}


	function get_assigned_pax_type_count($pax_type_arr, $pax_type)
	{
		$pax_type_count = 0;
		if(valid_array($pax_type_arr) == true){
			foreach ($pax_type_arr as $k => $v){
				if($pax_type == $v){
					$pax_type_count++;
				}
			}
		}
		return $pax_type_count;
	}

	function save_booking($app_booking_id, $params, $module = 'b2c') {
		// debug($app_booking_id);exit();
		// Need to return following data as this is needed to save the booking fare in the transaction details
		$response ['fare'] = $response ['domain_markup'] = $response ['level_one_markup'] = 0;

		$domain_origin = get_domain_auth_id ();
		$master_search_id = $params ['booking_params'] ['token'] ['search_id'];
		$search_data = $this->search_data ( $master_search_id );
		//$status = BOOKING_CONFIRMED;
		$app_reference = $app_booking_id;
		$booking_source = $params ['booking_params'] ['token'] ['booking_source'];
		
		$currency_obj = $params ['currency_obj'];
		$deduction_cur_obj = clone $currency_obj;
		$promo_currency_obj = $params['promo_currency_obj'];
		// PREFERRED TRANSACTION CURRENCY AND CURRENCY CONVERSION RATE
		$transaction_currency = get_application_currency_preference ();
		$application_currency = admin_base_currency ();
		$currency_conversion_rate = $currency_obj->transaction_currency_conversion_rate ();
		
		$booking_id = $params ['book_response'] ['BookResult'] ['BookingId'];
		$booking_reference = $params ['book_response'] ['BookResult'] ['BookingRefNo'];

		$confirmation_reference = $params ['book_response'] ['BookResult'] ['ConfirmationNo'];
		$status =  $params ['book_response'] ['BookResult'] ['booking_status'];
		$no_of_nights = intval ( $search_data ['data'] ['no_of_nights'] );
		$HotelRoomsDetails = force_multple_data_format ( $params ['room_book_data'] ['HotelRoomsDetails'] );
		//debug($HotelRoomsDetails);
		$total_room_count = count ( $HotelRoomsDetails );
		$book_total_fare = $params ['booking_params'] ['token'] ['price_summary'] ['OfferedPriceRoundedOff']; // (TAX+ROOM PRICE)
		$room_price = $params ['booking_params'] ['token'] ['price_summary'] ['RoomPrice'];
		
		if ($module == 'b2c') {
			$markup_total_fare = $currency_obj->get_currency ( $book_total_fare, true, false, true, $no_of_nights * $total_room_count ); // (ON Total PRICE ONLY)
			$ded_total_fare = $deduction_cur_obj->get_currency ( $book_total_fare, true, true, false, $no_of_nights * $total_room_count ); // (ON Total PRICE ONLY)
			$admin_markup = sprintf ( "%.2f", $markup_total_fare ['default_value'] - $ded_total_fare ['default_value'] );
			$agent_markup = sprintf ( "%.2f", $ded_total_fare ['default_value'] - $book_total_fare );
		} else {
			// B2B Calculation
			$markup_total_fare = $currency_obj->get_currency ( $book_total_fare, true, true, false, $no_of_nights * $total_room_count ); // (ON Total PRICE ONLY)
			$ded_total_fare = $deduction_cur_obj->get_currency ( $book_total_fare, true, false, true, $no_of_nights * $total_room_count ); // (ON Total PRICE ONLY)
			$admin_markup = sprintf ( "%.2f", $markup_total_fare ['default_value'] - $ded_total_fare ['default_value'] );
			$agent_markup = sprintf ( "%.2f", $ded_total_fare ['default_value'] - $book_total_fare );
		}
		$currency = $params ['booking_params'] ['token'] ['default_currency'];
		$hotel_name = $params ['booking_params'] ['token'] ['HotelName'];
		// debug($hotel_name);
		// debug($params);exit;
		$star_rating = $params ['booking_params'] ['token'] ['StarRating'];
		$hotel_code = '';
		$phone_number = $params ['booking_params'] ['passenger_contact'];
		$alternate_number = 'NA';
		$email = $params ['booking_params'] ['billing_email'];
		$hotel_check_in = db_current_datetime ( str_replace ( '/', '-', $search_data ['data'] ['from_date'] ) );
		$hotel_check_out = db_current_datetime ( str_replace ( '/', '-', $search_data ['data'] ['to_date'] ) );
		$payment_mode = $params ['booking_params'] ['payment_method'];
		
		$country_name = $GLOBALS ['CI']->db_cache_api->get_country_list ( array (
				'k' => 'origin',
				'v' => 'name' 
		), array (
				'origin' => $params ['booking_params'] ['billing_country'] 
		) );
		// $city_name = $GLOBALS['CI']->db_cache_api->get_city_list(array('k' => 'origin', 'v' => 'destination'), array('origin' => $params['booking_params']['billing_city']));
		$attributes = array (
				'address' => @$params ['booking_params'] ['billing_address_1'],
				'billing_country' => @$country_name [$params ['booking_params'] ['billing_country']],
				// 'billing_city' => $city_name[$params['booking_params']['billing_city']],
				'billing_city' => @$params ['booking_params'] ['billing_city'],
				'billing_zipcode' => @$params ['booking_params'] ['billing_zipcode'],
				'HotelCode' => @$params ['booking_params'] ['token'] ['HotelCode'],
				'search_id' => @$params ['booking_params'] ['token'] ['search_id'],
				'TraceId' => @$params ['booking_params'] ['token'] ['TraceId'],
				'HotelName' => $hotel_name,
				'StarRating' => @$params ['booking_params'] ['token'] ['StarRating'],
				'HotelImage' => @$params ['booking_params'] ['token'] ['HotelImage'],
				'HotelAddress' => @$params ['booking_params'] ['token'] ['HotelAddress'],
				'CancellationPolicy' => @$params ['booking_params'] ['token'] ['CancellationPolicy'],
				'Boarding_details' => @$params ['booking_params'] ['token'] ['Boarding_details']
		);
		$created_by_id = intval ( @$GLOBALS ['CI']->entity_user_id );
		// SAVE Booking details
		$GLOBALS ['CI']->hotels_model->save_booking_details ( $domain_origin, $status, $app_reference, $booking_source, $booking_id, $booking_reference, $confirmation_reference, $hotel_name, $star_rating, $hotel_code, $phone_number, $alternate_number, $email, $hotel_check_in, $hotel_check_out, $payment_mode, json_encode ( $attributes ), $created_by_id, $transaction_currency, $currency_conversion_rate );
		
		$check_in = db_current_datetime ( str_replace ( '/', '-', $search_data ['data'] ['from_date'] ) );
		//debug($check_in);exit();
		$check_out = db_current_datetime ( str_replace ( '/', '-', $search_data ['data'] ['to_date'] ) );
		// debug($params ['booking_params']['total_amount_val']);
		// exit("505");
		//debug($check_out);exit();
		$location = $search_data ['data'] ['location'];
		// loop token of token
		foreach ( $HotelRoomsDetails as $k => $v ) {
			// debug($v);exit("provab_hotel-509");
			$room_type_name = $params ['booking_params'] ['token']['room_type_name'];
			$bed_type_code = 0; // $v ['RoomTypeCode']
			$smoking_preference = get_smoking_preference ( $v ['SmokingPreference'] );
			$smoking_preference = $smoking_preference ['label'];
			$total_fare =$params ['booking_params'] ["promo_code_discount_val"];//$v ['Price'] ['OfferedPriceRoundedOff'];
			$room_price =  $params ['booking_params']['total_amount_val'];//$v ['Price'] ['RoomPrice'];
			
			if ($module == 'b2c') {
				$markup_total_fare = $currency_obj->get_currency ( $total_fare, true, false, true, $no_of_nights ); // (ON Total PRICE ONLY)
				$ded_total_fare = $deduction_cur_obj->get_currency ( $total_fare, true, true, false, $no_of_nights ); // (ON Total PRICE ONLY)
				$admin_markup = sprintf ( "%.2f", $markup_total_fare ['default_value'] - $ded_total_fare ['default_value'] );
				$agent_markup = sprintf ( "%.2f", $ded_total_fare ['default_value'] - $total_fare );
			} else {
				// B2B Calculation - Room wise price
                            //echo 'total_fare',debug($total_fare);
				$markup_total_fare = $currency_obj->get_currency ( $total_fare, true, true, false, $no_of_nights ); // (ON Total PRICE ONLY)
                                $ded_total_fare = $deduction_cur_obj->get_currency(($markup_total_fare ['default_value']), true, false, true, $no_of_nights ); // (ON Total PRICE ONLY)
                                $admin_markup = sprintf ( "%.2f", $markup_total_fare ['default_value'] -  $total_fare);
				$agent_markup = sprintf ( "%.2f", $ded_total_fare ['default_value'] - $markup_total_fare ['default_value']);
                                
			}
			
			$attributes = '';
			//debug($location);exit();
			// SAVE Booking Itinerary details
			$GLOBALS ['CI']->hotels_model->save_booking_itinerary_details ( $app_reference, $location, $check_in, $check_out, $room_type_name, $bed_type_code, $status, $smoking_preference, $total_fare, $admin_markup, $agent_markup, $currency, $attributes, @$v ['RoomPrice'], @$v ['Tax'], @$v ['ExtraGuestCharge'], @$v ['ChildCharge'], @$v ['OtherCharges'], @$v ['Discount'], @$v ['ServiceTax'], @$v ['AgentCommission'], @$v ['AgentMarkUp'], @$v ['TDS'] );
			$passengers = force_multple_data_format ( $v ['HotelPassenger'] );
			if (valid_array ( $passengers ) == true) {
				foreach ( $passengers as $passenger ) {
					$title = $passenger ['Title'];
					$first_name = $passenger ['FirstName'];
					//debug($first_name);exit();
					$middle_name = $passenger ['MiddleName'];
					$last_name = $passenger ['LastName'];
					$phone = $passenger ['Phoneno'];
					$email = $passenger ['Email'];
					$pax_type = $passenger ['PaxType'];
					//$age = $passenger['Age'];
					$date_of_birth = array_shift ( $params ['booking_params'] ['date_of_birth'] ); //
					
					$passenger_nationality_id = array_shift ( $params ['booking_params'] ['passenger_nationality'] ); //
					$passport_issuing_country_id = array_shift ( $params ['booking_params'] ['passenger_passport_issuing_country'] ); //
					
					$passenger_nationality = $GLOBALS ['CI']->db_cache_api->get_country_list ( array (
							'k' => 'origin',
							'v' => 'name' 
					), array (
							'origin' => $passenger_nationality_id 
					) );
					$passport_issuing_country = $GLOBALS ['CI']->db_cache_api->get_country_list ( array (
							'k' => 'origin',
							'v' => 'name' 
					), array (
							'origin' => $passport_issuing_country_id 
					) );
					
					$passenger_nationality = $passenger_nationality [$passenger_nationality_id];
					$passport_issuing_country = $passport_issuing_country [$passport_issuing_country_id];
					$passport_number = array_shift ( $params ['booking_params'] ['passenger_passport_number'] ); //
					$passport_expiry_date = array_shift ( $params ['booking_params'] ['passenger_passport_expiry_year'] ) . '-' . array_shift ( $params ['booking_params'] ['passenger_passport_expiry_month'] ) . '-' . array_shift ( $params ['booking_params'] ['passenger_passport_expiry_day'] ); //
					$attributes = array ();
					
					// SAVE Booking Pax details
					$GLOBALS ['CI']->hotel_model->save_booking_pax_details ( $app_reference, $title, $first_name, $middle_name, $last_name,$phone, $email, $pax_type, $date_of_birth, $passenger_nationality, $passport_number, $passport_issuing_country, $passport_expiry_date, $status, serialize ( $attributes ) );
				}
			}
		}
		
		/**
		 * ************ Update Convinence Fees And Other Details Start *****************
		 */
		// Convinence_fees to be stored and discount
		$convinence = 0;
		$discount = 0;
		$convinence_value = 0;
		$convinence_type = 0;
		$convinence_per_pax = 0;
		if ($module == 'b2c') {
			$convinence = $currency_obj->convenience_fees ( $book_total_fare, $master_search_id );
			$convinence_row = $currency_obj->get_convenience_fees ();
			$convinence_value = $convinence_row ['value'];
			$convinence_type = $convinence_row ['type'];
			$convinence_per_pax = $convinence_row ['per_pax']; 
			if($params['booking_params']['promo_actual_value']){
				$discount = get_converted_currency_value ( $promo_currency_obj->force_currency_conversion ( $params['booking_params']['promo_actual_value']) );
			}			
			//$discount = @$params ['booking_params'] ['promo_code_discount_val'];
			$promo_code = @$params ['booking_params'] ['promo_code'];
		} elseif ($module == 'b2b') {
			$discount = 0;
		}
		$GLOBALS ['CI']->load->model ( 'transaction' );
		// SAVE Booking convinence_discount_details details
		$GLOBALS ['CI']->transaction->update_convinence_discount_details ( 'hotel_booking_details', $app_reference, $discount, $promo_code, $convinence, $convinence_value, $convinence_type, $convinence_per_pax );
		/**
		 * ************ Update Convinence Fees And Other Details End *****************
		 */
		
		$response ['fare'] = $book_total_fare;
		$response ['admin_markup'] = $admin_markup;
		$response ['agent_markup'] = $agent_markup;
		$response ['convinence'] = $convinence;
		$response ['discount'] = $discount;
		$response ['transaction_currency'] = $transaction_currency;
		$response ['currency_conversion_rate'] = $currency_conversion_rate;
		//booking_status
		$response['booking_status'] = $status;
		return $response;
	}

	/**
	*Villa List
	**/

	function get_villa_list($search_id = '') { 

		$this->CI->load->driver ( 'cache' );
		$header = $this->get_header (); 
		$response ['data'] = array ();
		$response ['status'] = true;
		
		$search_data = $this->search_data ( $search_id );
		
		$cache_search = $this->CI->config->item ( 'cache_hotel_search' );
		
		$search_hash = $this->search_hash;
		// debug($search_hash);exit;
		if ($cache_search) {
			$cache_contents = $this->CI->cache->file->get ( $search_hash );
			
		}

		$response['search_request'] = $this->hotel_search_request ( $search_data ['data'] );

		$cidate = explode('/', $search_data['data']['from_date']);
		$CIDate = $cidate[2].'-'. $cidate[1].'-'. $cidate[0];

		$codate = explode('/', $search_data['data']['to_date']);
        $CODate = $codate[2].'-'. $codate[1].'-'. $codate[0];

        $datetime1 = new DateTime($CIDate);
        $datetime2 = new DateTime($CODate);

        //$oDiff = $datetime1->diff($datetime2); 
        //$stay_days = intval($oDiff->d); 
        $stay_days = $search_data['data']['no_of_nights'];

        $s_max_adult = max($search_data['data']['adult_config']);
        $s_max_child = max($search_data['data']['child_config']);
        if($s_max_child){
            $s_max_child = $s_max_child;
        }else{
            $s_max_child = 0;
        }
        $checkin_date = date('Y-m-d',strtotime($CIDate));
        $checkout_date = date('Y-m-d',strtotime($CODate));

        $room_count = $search_data['data']['room_count'];
        $city_id = $search_data['data']['city_name'];

        $safe_search_data = $GLOBALS ['CI']->hotels_model->get_villa_search_data ( $datetime1, $datetime2,$stay_days,$s_max_adult,$s_max_child,$checkin_date,$checkout_date,$room_count,$city_id);
       	$response ['data'] = $safe_search_data;
        return $response;      
	}

	

}

