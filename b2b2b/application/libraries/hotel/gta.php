<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
require_once 'Common_Api_Grind.php';

/**
 *
 * @package Provab
 * @subpackage API
 * @author HIMANI
 * @version V1
 */

class Gta extends Common_Api_Grind {
	private   $ClientId;
	private   $UserName;
	private   $Password;
	private   $Url;
	private   $LoginType = '2'; // (For API Users, value should be ‘2’)
	private   $EndUserIp = '127.0.0.1';
	private   $TokenId; // Token ID that needs to be echoed back in every subsequent request
	public    $master_search_data;
	public    $search_hash;
	protected $ins_token_file;
	protected $medium_image_base_url;
	protected $small_image_base_url;
	protected $service_url;
	protected $signature;
	protected $api_online_key;
	protected $json_api_header;
	protected $xml_api_header;
	protected $supplier = 'GTA';
	protected $api_data_currency = 'CHF';
	private   $aminity_count = -1;
	private   $details;
	
	public function __construct() {
		$this->CI = &get_instance ();
		$GLOBALS ['CI']->load->library ( 'Api_Interface' );
		$GLOBALS ['CI']->load->library ( 'converter' );
		$GLOBALS ['CI']->load->model ( 'hotel_model' );
		$this->set_api_credentials ();
	}

	private function set_api_credentials() {
		$hotel_engine_system = $this->CI->config->item ( 'hotel_engine_system' );
		$this->system = $hotel_engine_system;
		$this->details = $this->CI->config->item ( 'hotelgta_'.$hotel_engine_system );
		$this->api_username1 = $this->details['api_username1'];
		$this->api_username = $this->details['api_username'];
		$this->service_url = $this->details['api_url'];
		//$this->client_id = $this->details['client_id'];
		$this->password = $this->details['api_password'];
		$this->email = $this->details['api_username'];
		$this->language = 'en';
		//$this->requestMode = $this->details['requestMode'];
		$this->responseCallbackURL = $this->details['responseCallbackURL'];
	}
	
	/**
	 * Header to be used for hotebeds - XML API Version
	 */
	private function xml_header()
	{
		$this->xml_api_header = array('Api-Key: ' . $this->api_online_key,
				'X-Signature: ' . $this->signature,
				'X-Originating-Ip: 14.141.47.106',
				'Content-Type: application/xml',
				'Accept:application/xhtml+xml,application/xml,text/xml,application/xml',
				//'Accept: application/xml',
				'Accept-Encoding: gzip'
				);
				return $this->xml_api_header;
	}
	
	/**
	 * convert search params to format
	 * validate and format request params
	 */
	public function search_data($search_id) {
		$response ['status'] = true;
		$response ['data'] = array ();
		if (empty ( $this->master_search_data ) == true and valid_array ( $this->master_search_data ) == false) {
			$clean_search_details = $GLOBALS ['CI']->hotel_model->get_safe_search_data ( $search_id );
			if ($clean_search_details ['status'] == true) {
				$response ['status'] = true;
				$response ['data'] = $clean_search_details ['data'];

				// 28/12/2014 00:00:00 - date format
				$response ['data'] ['from_date'] = date ( 'Y-m-d', strtotime ( $clean_search_details ['data'] ['from_date'] ) );
				$response ['data'] ['to_date'] = date ( 'Y-m-d', strtotime ( $clean_search_details ['data'] ['to_date'] ) );
				// get city id based
				$location_details = $GLOBALS ['CI']->hotel_model->get_hotels_city_info ( $clean_search_details ['data'] ['hotel_destination']);
				if ($location_details ['status']) {
					$response ['data'] ['country_code'] = $location_details ['data'] [0]['country_code'];
					$response ['data'] ['location_id'] = $location_details ['data'] [0]['hotelbeds'];
					$response ['data'] ['location_gta'] = $location_details ['data'] [0]['gta'];
					$response ['data'] ['location_origin'] = $location_details ['data'] [0]['origin'];
				} else {
					$response ['status'] = false;
				}
				$this->master_search_data = $response ['data'];
			} else {
				$response ['status'] = false;
			}
		} else {
			$response ['data'] = $this->master_search_data;
		}
		//$this->search_hash = md5 ( serialized_data ( $response ['data'] ) );
		$this->search_hash = md5 ( $search_id );

		return $response;
	}
	
	/*search hotel list*/
	function get_hotel_list($search_id = '', $booking_source,$offset='', $filter='',$module='b2c', & $timeline = '') {
		 error_reporting(-1);
		$this->CI->load->driver ( 'cache' );
		$response ['data'] = array ();
		$pre_cache_res =  array();
		$response ['status'] = true;
		$search_data = $this->search_data ( $search_id );
		$cache_search = $this->CI->config->item ( 'cache_hotel_search' );
		$search_hash = $this->search_hash;
		if ($cache_search) {
			if($offset == false && $filter == false){
				$cache_contents = $this->CI->cache->file->get ( $search_hash );
				if(!empty($cache_contents)){
					$pre_cache_res = $cache_contents;
					$cache_contents = array();
				}
			}else{
				$cache_contents = $this->CI->cache->file->get ( $search_hash );
			}
		}
		if ($search_data ['status'] == true) {
			$city_id = $search_data ['data']['location_origin'];
			if ($cache_search === false || ($cache_search === true && empty ( $cache_contents ) == true) ||($offset == false && $filter == false)) {
				$search_request = $this->hotel_search_request ( $search_data ['data'] );
				if($search_request['status'] == SUCCESS_STATUS) {
					$timeline['api'] = microtime(true);
					$hotel_list = $this->process_details($search_request['data']);
					// echo "Search response :";
					// debug($hotel_list); exit;
					$timeline['api'] = microtime(true)-$timeline['api'];
					$tmp_response = Converter::createArray($hotel_list);
					// debug($tmp_response);exit;
					if ($this->valid_search_result ( $tmp_response )) {

						// check cache data
						$cache_contents = $this->CI->cache->file->get ( $search_hash );
						if(!empty($cache_contents)){
							$pre_cache_res = $cache_contents;
							$cache_contents = array();
						}

						$hotel_response = $tmp_response['Response']['ResponseDetails']['SearchHotelPriceResponse']['HotelDetails']['Hotel'];
						$timeline['format'] = microtime(true);
						$this->aminity_count = 6;
						$response ['data']['HotelSearchResult'] = $this->get_formatted_hotel($hotel_response, $search_id,$module,$booking_source);
						$timeline['format'] = microtime(true) - $timeline['format'];
						$total_result_count = count($response ['data']['HotelSearchResult']);
						if ($total_result_count > 0) {
							if ($cache_search) { 
								/*if(!empty($pre_cache_res)){
									$cache_hotels = array();
									//debug($pre_cache_res); exit('filtersss');
									$response ['data']['HotelSearchResult'] = array_merge($response ['data']['HotelSearchResult'],$pre_cache_res['HotelSearchResult']);
									foreach ($response ['data']['HotelSearchResult'] as $key => $value) {
										$cache_hotels['HotelSearchResult'][$value['hotel_code']] = $value;
									}
									if(count($cache_hotels) > 0){
										$response['data']['HotelSearchResult'] = $cache_hotels['HotelSearchResult'];
									}

									$cache_exp = $this->CI->config->item ( 'cache_hotel_search_ttl' );
									$this->CI->cache->file->save ( $search_hash, $cache_hotels, $cache_exp );
									$response['data']['HotelSearchResult'] = $cache_hotels['HotelSearchResult'];
								}else{
									$response['data']['filter_sumry'] = $this->filter_summary($response['data']);
									$cache_exp = $this->CI->config->item ( 'cache_hotel_search_ttl' );
									$this->CI->cache->file->save ( $search_hash, $response ['data'], $cache_exp );
								}*/

								if(!empty($pre_cache_res)){
									$cache_hotels = array();
									$modify_filter = array();

									$filter_sumry = $this->filter_summary($response['data']);

									if(isset($pre_cache_res['filter_sumry']) && !empty($pre_cache_res['filter_sumry'])){
										$cache_mod_fil_data = $pre_cache_res['filter_sumry'];
										$raw_mod_fil_data = $filter_sumry['data'];

										/*debug($cache_mod_fil_data['facility']);
										echo '---------------';
										debug($raw_mod_fil_data['facility']); */

										$modify_filter['filter_sumry']['p']['max'] = ($cache_mod_fil_data['p']['max'] > $raw_mod_fil_data['p']['max']) ? $cache_mod_fil_data['p']['max'] : $raw_mod_fil_data['p']['max'];
										$modify_filter['filter_sumry']['p']['min'] = ($cache_mod_fil_data['p']['min'] < $raw_mod_fil_data['p']['min']) ? $cache_mod_fil_data['p']['min'] : $raw_mod_fil_data['p']['min'];
										

										foreach ($raw_mod_fil_data['loc'] as $key => $value) {
											if (array_key_exists($key,$cache_mod_fil_data['loc'])){
												$value['c'] = $cache_mod_fil_data['loc'][$key]['c'] + $value['c'];
												$cache_mod_fil_data['loc'][$key] = $value;
											}else{
												$cache_mod_fil_data['loc'][$key] = $value;
											}
										}

										$modify_filter['filter_sumry']['loc'] =$cache_mod_fil_data['loc'];


										foreach ($raw_mod_fil_data['star'] as $key => $value) {
											if (array_key_exists($key,$cache_mod_fil_data['star'])){
												@$value['c'] = $cache_mod_fil_data['loc'][$key]['c'] + $value['c'];
												$cache_mod_fil_data['star'][$key] = $value;
											}else{
												$cache_mod_fil_data['star'][$key] = $value;
											}
										}

										$modify_filter['filter_sumry']['star'] = $cache_mod_fil_data['star'];

										foreach ($raw_mod_fil_data['a_type'] as $key => $value) {
											if (array_key_exists($key,$cache_mod_fil_data['a_type'])){
												$cache_mod_fil_data['a_type'][$key] = $value;
											}else{
												$cache_mod_fil_data['a_type'][$key] = $value;
											}
										}

										$modify_filter['filter_sumry']['a_type'] = $cache_mod_fil_data['a_type'];

										$unique_facility = array();
										

										foreach ($raw_mod_fil_data['facility'] as $key => $value) {
											if (array_key_exists($key,$cache_mod_fil_data['facility'])){
												$value['c'] = $cache_mod_fil_data['facility'][$key]['c'] + $value['c'];
												$cache_mod_fil_data['facility'][$key] = $value;
											}else{
												$cache_mod_fil_data['facility'][$key] = $value;
											}
										}
										
										$modify_filter['filter_sumry']['facility'] = $cache_mod_fil_data['facility'];
									}

									/*echo '--------------';
									debug($modify_filter['filter_sumry']['facility']);
									exit('modify_filter');*/

									$response ['data']['HotelSearchResult'] = array_merge($response ['data']['HotelSearchResult'],$pre_cache_res['HotelSearchResult']);

									foreach ($response ['data']['HotelSearchResult'] as $key => $value) {
										$cache_hotels['HotelSearchResult'][$value['hotel_code']] = $value;
									}

									if(count($cache_hotels) > 0){
										$response['data']['HotelSearchResult'] = $cache_hotels['HotelSearchResult'];
										@$response['data']['filter_sumry'] = $modify_filter['filter_sumry'];
									}

									//$cache_hotels['filter_sumry'] = $modify_filter['filter_sumry'];

									$cache_exp = $this->CI->config->item ( 'cache_hotel_search_ttl' );
									$this->CI->cache->file->save ( $search_hash, $cache_hotels, $cache_exp );
									$response['data']['HotelSearchResult'] = $cache_hotels['HotelSearchResult'];
									$response['data']['filter_sumry'] = $modify_filter;

								}else{
									$data_filter_summary = $this->filter_summary($response['data']);
									//debug($data_filter_summary); exit;
									$response['data']['filter_sumry'] = $data_filter_summary['data'];
									$cache_exp = $this->CI->config->item ( 'cache_hotel_search_ttl' );
									$this->CI->cache->file->save ( $search_hash, $response ['data'], $cache_exp );
								}
							}
							// Log Hotels Count
							$this->cache_result_hotel_count ( $city_id, $total_result_count );
						}
					}else {
						$response ['status'] = false;
					}
				}else {
						$response ['status'] = false;
				}
			}else {
				// read from cache
				$response ['data'] = $cache_contents;
			}
		} else {
			$response ['status'] = false;
		}
		//debug($response);die(" Hotels Lists");
		return $response;
	}
	
	/*GTA API hotel SEARCH request */
	function hotel_search_request($search_data) {
 
		$no_of_days = get_no_of_days($search_data['from_date'],$search_data['to_date']);
		$total_adult = COUNT($search_data['adult_config'][0]);
		$total_child = COUNT($search_data['child_config'][0]);
		$child_ages = @$search_data['child_age'];
		$searchRequest = '';
		$searchRequest .= '<Request>
				<Source>
					<RequestorID Client="'.$this->api_username1.'" EMailAddress="'.$this->api_username.'" Password="'.$this->password.'"/>
					<RequestorPreferences Country="CH" Currency="CAD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
					</RequestorPreferences>
				</Source>
				<RequestDetails>
				<SearchHotelPriceRequest>
							<ItemDestination DestinationCode = "'.$search_data['location_gta'].'" DestinationType = "city"/>
							<ImmediateConfirmationOnly/>
							<PeriodOfStay>
								<CheckInDate>'.$search_data['from_date'].'</CheckInDate>
								<Duration>'.$no_of_days.'</Duration>
							</PeriodOfStay>';
		
		if(isset($search_data['adult_config']) && valid_array($search_data['adult_config'])) {
			$adult = $search_data['adult_config'];
			$child = $search_data['child_config'];
			$child_ages = @$search_data['child_age'];
		
			$searchRequest .= '<Rooms>';
			$total_child_Age_key = 0;
			
			for($a=0;$a<COUNT($adult);$a++) {
				$total_adult = $adult[$a];
				$total_child = $child[$a];
				
				if($total_adult == 1 && $total_child == 0) {
					$searchRequest .= '<Room Code="SB"></Room>';
				}else if ($total_adult == 1 && $total_child == 1) {
					for($b=0;$b<$total_child;$b++) {
						if($child_ages[$total_child_Age_key] == 1) {
							$searchRequest .= '<Room Code="TS" NumberOfCots="1"></Room>';
						} else {
							$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$child_ages[$total_child_Age_key].'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
						$total_child_Age_key++;
					}
				}else if($total_adult == 1 && $total_child == 2) {
					$ageString = ''; 
					$numCots = 0;
					for($c = 0;$c<$total_child;$c++) {
						if($child_ages[$total_child_Age_key] == 1) {
							$numCots++;
						} else {
							$ageString .= '<Age>'.$child_ages[$total_child_Age_key].'</Age>';
						}
						$total_child_Age_key++;
					}
					
					if($numCots > 0) {
						$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
					} else {
						$numberOfCotsString = '';
					}
					
					if($ageString != "") {
						//this means that children are more than 2 yr old
						$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
					} else {
						$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ></Room>';
					}
				}else if($total_adult == 2 && $total_child == 0) {
					$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
					$searchRequest .= '</Room>';
				}else if($total_adult == 2 && $total_child == 1) {
					for($d = 0;$d<$total_child;$d++) {
						if($child_ages[$total_child_Age_key] == 1) {
							$searchRequest .= '<Room Code="DB" NumberOfCots="1"></Room>';		
						} else {
							$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$child_ages[$total_child_Age_key].'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
						$total_child_Age_key++;
					}
				}else if($total_adult == 2 && $total_child == 2) {
					$ageString = ''; 
					$numCots = 0;
					for($e = 0;$e<$total_child;$e++) {
						if($child_ages[$total_child_Age_key] == 1) {
							$numCots++;		
						} else {
							$ageString .= '<Age>'.$child_ages[$total_child_Age_key].'</Age>';
						}
						$total_child_Age_key++;
					}
					if($numCots > 0) {
						$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
					} else {
						$numberOfCotsString = '';
					}
					
					if($ageString != "") {    //this means that children are more than 2 yr old
						$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
					} else {
						$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ></Room>';
					}
				}else if($total_adult == 3 && $total_child == 0) {
					$searchRequest .= '<Room Code="TR">';
					$searchRequest .= '</Room>';
				}else if($total_adult == 3 && $total_child == 1) {
					for($f = 0;$f<$total_child;$f++) {
						if($child_ages[$total_child_Age_key] == 1) {
							$searchRequest .= '<Room Code="TR" NumberOfCots="1"></Room>';	
						} else {
							$searchRequest .= '<Room Code="DB">';
							$searchRequest .= '<ExtraBeds><Age>'.$child_ages[$total_child_Age_key].'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';

							$searchRequest .= '<Room Code="DB">';
							$searchRequest .= '</Room>';
						}
						$total_child_Age_key++;
					}
				}else if($total_adult == 3 && $total_child == 2) {
					$age1 = $child_ages[$total_child_Age_key];
					$age2 = $child_ages[$total_child_Age_key + 1];
					
					if($age1 == 1 && $age2 == 1) {
						$searchRequest .= '<Room Code="DB" NumberOfCots="2">';
						$searchRequest .= '</Room>';
	
						$searchRequest .= '<Room Code="SB">';
						$searchRequest .= '</Room>';
					}
					else if($age1 == 1 && $age2 > 1) {
						$searchRequest .= 	'<Room Code="DB" NumberOfCots="1">
												<ExtraBeds>
													<Age>'.$age2.'</Age>
												</ExtraBeds>
											</Room>';
						$searchRequest .= '<Room Code="SB">';
						$searchRequest .= '</Room>';
					}
					else if($age1 > 1 && $age2 == 1) {
						$searchRequest .= 	'<Room Code="DB" NumberOfCots="1">
												<ExtraBeds>
													<Age>'.$age1.'</Age>
												</ExtraBeds>
											</Room>';
						$searchRequest .= '<Room Code="SB">';
						$searchRequest .= '</Room>';
					}
					else if($age1 > 1 && $age2 > 1) {
						$searchRequest .= 	'<Room Code="TB">
												<ExtraBeds>
													<Age>'.$age1.'</Age>
												</ExtraBeds>
											</Room>';
						$searchRequest .= 	'<Room Code="TB">
												<ExtraBeds>
													<Age>'.$age2.'</Age>
												</ExtraBeds>
											</Room>';
					}
				}else if($total_adult == 4 && $total_child == 0) {
					$searchRequest .= '<Room Code="Q">';
					$searchRequest .= '</Room>';
				}
				
			}
			$searchRequest .= '</Rooms>';
			$searchRequest .= 	'<OrderBy>pricelowtohigh</OrderBy></SearchHotelPriceRequest></RequestDetails>
			</Request>';
		}
		$response['data'] = $searchRequest;
		$response['status'] = SUCCESS_STATUS;
			/*Creating XML Log*/
                $xml_title = 'gta_req'.date('Y-m-dhsi');
                $date = date('Ymd_His');
                if (!file_exists('xml_logs/hotel/gta/')) {
                     mkdir('xml_logs/hotel/gta/', 0777, true);
                }
                $requestcml = $xml_title.'.xml';
                file_put_contents('../b2b2b/xml_logs/hotel/gta/'.$requestcml, $searchRequest);
            /*End OF Creating XML Log*/
		return $response;
	}
	
	//Get hotel details based on hotel code
	public function get_hotel_details($search_data,$hotel_id,$module) {
		//debug($search_data);exit;
		$response ['data'] = array ();
		$response ['status'] = false;
		$search_id = $search_data['search_id'];
		$hotel_details_request = $this->hotel_details_request ( $search_data,$hotel_id);

		if($hotel_details_request['status'] == SUCCESS_STATUS) {
			$hotel_detail = $this->process_details($hotel_details_request['request']);
			// debug($hotel_detail); exit();
			$GLOBALS ['CI']->custom_db->generate_static_response ($hotel_detail);
			$hotel_details_response = Converter::createArray($hotel_detail);
			// debug($hotel_details_response);exit;
			if ($this->valid_hotel_details($hotel_details_response)){
				//Combination
				$hotel_details_array = $this->get_hotel_rooms_combinations($hotel_details_response,$search_data,$search_id,$module);
				$response ['data'] = $hotel_details_array;
				$response ['status'] = true;
			}else{
				// Need the complete data so that later we can use it for redirection
				$response ['data'] = $hotel_details_response;
			}
		} 
		//debug($response);exit;
		return $response;
	}
	
	
	/**
	 *
	 *
	 *
	 * Jaganath
	 * caching the Hotel Room details
	 */
	function cache_hotel_room_details(& $hotel_room_details)
	{
		$token = array();
		$this->ins_token_file = time().rand(100, 10000);
		$hotel_id = $hotel_room_details['hotel_code'];
		$tkn_key = $hotel_id;
		$this->push_token($hotel_room_details, $token, $tkn_key);
		$this->save_token($token);
	}
	
	//validate hotel detail response
	function valid_hotel_details($hotel_details_response) {
		if(isset($hotel_details_response['Response']['ResponseDetails']['SearchHotelPriceResponse']['HotelDetails']['Hotel']['RoomCategories']) 
		&& valid_array($hotel_details_response['Response']['ResponseDetails']['SearchHotelPriceResponse']['HotelDetails']['Hotel']['RoomCategories'])) {
			return true;
		}
		return false;
	}
	
	/**
	 * Save token and cache the data
	 * @param array $token
	 */
	private function save_token($token)
	{
		$file = DOMAIN_TMP_UPLOAD_DIR.$this->ins_token_file.'.json';
		file_put_contents($file, json_encode($token));
	}

	/**
	 * adds token and token key to flight and push data to token for caching
	 * @param array $hotel_room_data	Flight for which token and token key has to be generated
	 * @param array $token	Token array for caching
	 * @param string $key	Key to be used for caching
	 */
	private function push_token(& $hotel_room_data, & $token, $key)
	{
		//push data inside token before adding token and key values
		$token[$key] = $hotel_room_data;

		//Adding token and token key
		$hotel_room_data['Token'] = serialized_data($this->ins_token_file.DB_SAFE_SEPARATOR.$key);
		$hotel_room_data['TokenKey'] = md5($hotel_room_data['Token']);
	}

	public function read_token($token_key)
	{
		$token_key = explode(DB_SAFE_SEPARATOR, unserialized_data($token_key));

		if (valid_array($token_key) == true) {
			$file = DOMAIN_TMP_UPLOAD_DIR.$token_key[0].'.json';//File name
			$index = $token_key[1]; // access key

			if (file_exists($file) == true) {
				$token_content = file_get_contents($file);
				if (empty($token_content) == false) {
					$token = json_decode($token_content, true);

					if (valid_array($token) == true && isset($token[$index]) == true) {
						return $token[$index];
					} else {
						return false;
						echo 'Token data not found';
						exit;
					}
				} else {
					return false;
					echo 'Invalid File access';
					exit;
				}
			} else {
				return false;
				echo 'Invalid Token access';
				exit;
			}
		} else {
			return false;
			echo 'Invalid Token passed';
			exit;
		}
	}
	
	
	function get_hotel_rooms_combinations($hotel_details_response,$search_data,$search_id,$module) {
		$hotel_detail = $hotel_details_response['Response']['ResponseDetails']['SearchHotelPriceResponse'];
		$total_adult = array_sum($search_data['adult_config']);
		$total_child = array_sum($search_data['child_config']);
		
		$hotelcode = $_GET['hotel_id'];
		$hotel_array = array();
		$new_arr = array();
		$hotel_detail_responce_arr = $hotel_detail;
		
		if(isset($hotel_detail['HotelDetails']['Hotel']) && valid_array($hotel_detail['HotelDetails']['Hotel'])) {
			$hotel_details = $hotel_detail['HotelDetails']['Hotel'];
			$hotel_array['hotel_code'] = $hotel_details['Item']['@attributes']['Code'];
			$hotel_array['hotel_name'] = $hotel_details['Item']['@cdata'];
			$hotel_array['star_rating'] = $hotel_details['StarRating'];
			$hotel_array['destination'] = $hotel_details['City']['@cdata'];
			$hotel_array['destination_code'] = $hotel_details['City']['@attributes']['Code'];
			
			//no of rooms
			$no_of_room = 0;
			$hotel_room_code = array();
			if(isset($hotel_details['HotelRooms']['HotelRoom']) && valid_array($hotel_details['HotelRooms']['HotelRoom'])) {
				$hotel_details['HotelRooms']['HotelRoom'] = force_multple_data_format($hotel_details['HotelRooms']['HotelRoom']);
				foreach($hotel_details['HotelRooms']['HotelRoom'] as $h_key => $rm_cnt) {
					$no_of_room += $rm_cnt['@attributes']['NumberOfRooms'];
					$hotel_room_code[] = $rm_cnt['@attributes']['Code'];
				}
			}
			$hotel_array['no_of_rooms'] = 	$no_of_room;

			//rooms
			$rooms = array();
			$fare_Arr = array();
			if(isset($hotel_details['RoomCategories']['RoomCategory']) && valid_array($hotel_details['RoomCategories']['RoomCategory'])) {
				$rooms_array = force_multple_data_format($hotel_details['RoomCategories']['RoomCategory']);
				foreach($rooms_array as $r_k => $room_details) {//debug($room_details);exit;
					$rooms[$r_k]['rate_key'] = $room_details['@attributes']['Id'];
					$rooms[$r_k]['name'] = $room_details['Description']['@cdata'];
					$rooms[$r_k]['xml_net'] = $room_details['ItemPrice']['@value'];
					if(isset($room_details['Meals'])){
                        $Meals = $room_details['Meals'];
                        if($Meals['Basis']['@attributes']['Code'] == 'B'){
                            $meal_avail = "Breakfast Included";
                        } else {
                            $meal_avail = "Room Only";
                        }
                    } else {
                        $meal_avail = "Room Only";
                    }

					$rooms[$r_k]['boardName'] = $meal_avail;
					$currency = $room_details['ItemPrice']['@attributes']['Currency'];
					
					if($r_k == 0) {
						$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => $currency, 'to' => get_application_default_currency()));
					}
					
					if ($module == 'b2c') {
						$markup_min_fare = $currency_obj->get_currency ($room_details['ItemPrice']['@value'], true, false, true, 1 ); // (ON Total PRICE ONLY)
					} else {
						$markup_min_fare = $currency_obj->get_currency ($room_details['ItemPrice']['@value'], true, true, true, 1 ); // (ON Total PRICE ONLY)
					}
					$hotel_array['currency'] = $markup_min_fare['default_currency'];
					$rooms[$r_k]['net'] = $markup_min_fare['default_value'];
					//$rooms[$r_k]['net'] = sprintf("%.2f", ceil($rooms[$r_k]['net']));
					$rooms[$r_k]['net'] = round($rooms[$r_k]['net'],2);
					$fare_Arr[] = $rooms[$r_k]['net'];
					//rooms combination
					if(isset($room_details['HotelRoomPrices']['HotelRoom']) && valid_array($room_details['HotelRoomPrices']['HotelRoom'])) {
						$hotel_room_prices = force_multple_data_format($room_details['HotelRoomPrices']['HotelRoom']);
						foreach($hotel_room_prices as $hr_p_k => $__room) {
							if(in_array(@$__room['@attributes']['Code'],$hotel_room_code)) {
								//$rooms[$r_k]['room_detail'][$hr_p_k] = array();
							}
						}
					}
					
					//cancellation
					$charges = $room_details['ChargeConditions']['ChargeCondition'];
					
					if(isset($charges) && valid_array($charges)) {
						$charges = force_multple_data_format($charges);
						foreach($charges as $e_ch_k => $charge) {
							if(isset($charge['@attributes']['Type']) && $charge['@attributes']['Type'] == 'cancellation') {
								if(isset($charge['Condition']) && valid_array($charge['Condition'])) {
									$charge['Condition'] = force_multple_data_format($charge['Condition']);
									foreach($charge['Condition'] as $cel_key => $cancel_policy) {
										if(isset($cancel_policy['@attributes']['ChargeAmount']) && !empty($cancel_policy['@attributes']['ChargeAmount'])) {
											if ($module == 'b2c') {
												$converted_cncel_price = $currency_obj->get_currency ($cancel_policy['@attributes']['ChargeAmount'], true, false, true, 1 ); // (ON Total PRICE ONLY)
											} else {
												// B2B Calculation 
												$converted_cncel_price = $currency_obj->get_currency ($cancel_policy['@attributes']['ChargeAmount'], true, true, true, 1 ); // (ON Total PRICE ONLY)
											}
											$rooms[$r_k]['cancellationPolicies'][$cel_key]['amount'] = $converted_cncel_price['default_value'];
											$rooms[$r_k]['cancellationPolicies'][$cel_key]['amount'] = sprintf("%.2f", ceil($rooms[$r_k]['cancellationPolicies'][$cel_key]['amount']));
										}else {
											$rooms[$r_k]['cancellationPolicies'][$cel_key]['amount'] = 0;
										}
										$rooms[$r_k]['cancellationPolicies'][$cel_key]['from'] = $cancel_policy['@attributes']['FromDate'];
									}
								}
							}
						}
					}
					$rooms[$r_k]['room'] = $no_of_room;
					$rooms[$r_k]['adults'] = $total_adult;
					$rooms[$r_k]['children'] = $total_child;
				}
			}
			
			$hotel_array['minRate'] = min($fare_Arr);
			$hotel_array['maxRate'] = max($fare_Arr);
			
			$hotel_array['checkIn'] = $search_data['from_date'];
			$hotel_array['checkOut'] = $search_data['to_date'];
			$hotel_array['rooms'] = $rooms;
			
			//static info
			$hotel_Static_Data = $this->get_hotel_info($hotel_array['hotel_code'],$hotel_array['destination_code']);
			if(isset($hotel_Static_Data['info'][$hotel_array['hotel_code']]) && valid_array($hotel_Static_Data['info'][$hotel_array['hotel_code']])) {
				$hotel_Static_info = $hotel_Static_Data['info'][$hotel_array['hotel_code']];
				$hotel_array['address'] = $hotel_Static_info['address'];
				$hotel_array['email'] = $hotel_Static_info['email'];
				$hotel_array['latitude'] = $hotel_Static_info['latitude'];
				$hotel_array['longitude'] = $hotel_Static_info['longitude'];
				$hotel_array['facilities'] = $hotel_Static_Data['facility'][$hotel_array['hotel_code']];				$hotel_array['contact'] = $hotel_Static_info['contact'];
				$hotel_array['description'] = $hotel_Static_info['description'];
				//images
				if(isset($hotel_Static_info['img']) && !empty($hotel_Static_info['img'])) {
					$img_arr = explode(',',$hotel_Static_info['img']);
					$hotel_array['image'] = @$img_arr[0];
					$hotel_array['thumbnails'] = $img_arr;
				}
			}
		}
		// debug($hotel_array); exit();
		return $hotel_array;
	}
	
	
	//recheck room price before proceed to booking
	function check_room_rate($hotel_code,$rate_keys,$search_id,$module) {
		$response['status'] = false;
		$response['data'] = array();
		
		$search_details = $this->search_data ($search_id);
		$check_room_rate_request = $this->check_room_rate_request($hotel_code,$search_details['data'],$rate_keys);
		if(isset($check_room_rate_request['status']) && $check_room_rate_request['status'] == SUCCESS_STATUS) {
			$room_rates = $this->process_details($check_room_rate_request['data']);
			if(isset($room_rates) && !empty($room_rates)) {
				$GLOBALS ['CI']->custom_db->generate_static_response ($room_rates);
				$room_rates = Converter::createArray($room_rates);
			//	debug($room_rates);exit;
				if ($this->valid_check_rate_details ( $room_rates )) {
					$hotel_details_array = $this->get_formatted_checkprice_roomdetails($room_rates,$search_id='',$module);
					$hotel_details_array['checkIn'] = date('d-m-Y',strtotime($search_details['data']['from_date']));
					$hotel_details_array['checkOut'] = date('d-m-Y',strtotime($search_details['data']['to_date']));
					$hotel_details_array['rooms']['rooms'] = $search_details['data']['room_count'];
					$hotel_details_array['rooms']['adults'] = array_sum($search_details['data']['adult_config']);
					$hotel_details_array['rooms']['children'] = array_sum($search_details['data']['child_config']);
					$response ['data'] = $hotel_details_array;
					$response ['status'] = true;
				} else {
					// Need the complete data so that later we can use it for redirection
					$response ['data'] = array();
				}
			}
		}
		return $response;
	}
	
	/**
	 * Formate Room details
	 * @param $hotel_detail
	 */
	function get_formatted_checkprice_roomdetails($hotel_detail,$search_id='',$module)
	{//debug($hotel_detail);exit;
		$hotel_detail_responce_arr = $hotel_detail;
		$hotel_array = array();
		$total_price = 0;
		if($hotel_detail['Response']['ResponseDetails']['SearchHotelPriceResponse']['HotelDetails']) {
			$cancellStr = '';
			$hotel_detail_responce = $hotel_detail['Response']['ResponseDetails']['SearchHotelPriceResponse']['HotelDetails']['Hotel'];
			
			$hotel_array['hotel_code'] = $hotel_detail_responce['Item']['@attributes']['Code'];
			$hotel_array['hotel_name'] = $hotel_detail_responce['Item']['@cdata'];
			$hotel_array['star_rating'] = $hotel_detail_responce['StarRating'];
			$hotel_array['destination'] = $hotel_detail_responce['City']['@cdata'];
			$hotel_array['destination_code'] = $hotel_detail_responce['City']['@attributes']['Code'];
			
			//hotel rooms detail type and no of rooms
//			if(isset($hotel_detail_responce['HotelRooms']['HotelRoom']) && valid_array($hotel_detail_responce['HotelRooms']['HotelRoom'])) {
//				$hotel_rooms= force_multple_data_format($hotel_detail_responce['HotelRooms']['HotelRoom']);
//			}
			
			if(isset($hotel_detail_responce['RoomCategories']['RoomCategory']) && valid_array($hotel_detail_responce['RoomCategories']['RoomCategory'])) {
				$room_Details = $hotel_detail_responce['RoomCategories']['RoomCategory'];
				$hotel_array['rooms']['rate_key'] = $room_Details['@attributes']['Id'];
				$hotel_array['rooms']['name'] = $room_Details['Description']['@cdata'];
				$hotel_array['rooms']['xml_price'] = $room_Details['ItemPrice']['@value'];
				$hotel_array['xml_currency'] = $currency = $room_Details['ItemPrice']['@attributes']['Currency'];
				
				$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => $currency, 'to' => get_application_currency_preference()));
				if ($module == 'b2c') {
					$__price_dis = $currency_obj->get_currency ($room_Details['ItemPrice']['@value'], true, false, true, 1 ); // (ON Total PRICE ONLY)
				} else {
					$__price_dis = $currency_obj->get_currency ($room_Details['ItemPrice']['@value'], true, true, true, 1 ); // (ON Total PRICE ONLY)
				}
				//$__price_dis['default_value'] = sprintf("%.2f", ceil($__price_dis['default_value']));
				$__price_dis['default_value'] = round($__price_dis['default_value'],2);
				$hotel_array['rooms']['total_price'] = $__price_dis['default_value'];
				$hotel_array['currency']= $__price_dis['default_currency'];
				$hotel_array['rooms']['name'] = $room_Details['Description']['@cdata'];
				
				/*cancellation policies*/
				
				if(isset($room_Details['ChargeConditions']['ChargeCondition']) && valid_array($room_Details['ChargeConditions']['ChargeCondition'])) {
					$cancel_policies = $room_Details['ChargeConditions']['ChargeCondition'];
					$cancel_policies = force_multple_data_format($cancel_policies);
					$prevDate = '';
					$cntcncle =1;
					foreach($cancel_policies as $cp_key => $cancel_p) {
						if($cancel_p['@attributes']['Type'] == 'cancellation') {
							$cancel_arr = $cancel_p['Condition'];
							$cancel_arr = force_multple_data_format($cancel_arr);
							foreach($cancel_arr as $d_k => $__cncel) {
								if($__cncel['@attributes']['Charge'] == 'true') {
									if ($module == 'b2c') {
										$n_price_dis = $currency_obj->get_currency ($__cncel['@attributes']['ChargeAmount'], true, false, true, 1 ); // (ON Total PRICE ONLY)
									} else {
										$n_price_dis = $currency_obj->get_currency ($__cncel['@attributes']['ChargeAmount'], true, true, true, 1 ); // (ON Total PRICE ONLY)
									}
								}else {
									$n_price_dis['default_value'] = 0;
								}

								$n_price_dis['default_value'] = sprintf("%.2f", ceil($n_price_dis['default_value']));
								$fromDate = date('d-m-Y A',strtotime($__cncel['@attributes']['FromDate']));
								$str = ($cntcncle == 1) ? $hotel_array['rooms']['name'] . ' : </br> From ' . $fromDate . ', ' .$hotel_array['currency'].' '. $n_price_dis['default_value'] . ' will be charged as cancellation penalty.' : 'From ' . $fromDate . ', ' .$hotel_array['currency'].' '. $n_price_dis['default_value'] . ' will be charged as cancellation penalty.';
								$cancellStr = '<li>' . $cancellStr . $str . '<li>';
								$prevDate = $fromDate;
								$cntcncle++;
							}
						}
					}
				}
				$hotel_array['cancellationString'] = '<ul>' . $cancellStr . '</ul>';
				//static data
				$hotel_static_data = $this->get_hotel_info($hotel_array['hotel_code'],$hotel_array['destination_code']);
				if(isset($hotel_static_data['info'][$hotel_array['hotel_code']]) && valid_array($hotel_static_data['info'][$hotel_array['hotel_code']])) {
					$static_info = $hotel_static_data['info'][$hotel_array['hotel_code']];
					$hotel_array['address'] = $static_info['address'];
					$hotel_array['latitude'] = $static_info['latitude'];
					$hotel_array['longitude'] = $static_info['longitude'];
					if(isset($static_info['img']) && !empty($static_info['img'])) {
						$img_Arr = explode(',',$static_info['img']);
						$hotel_array['imagePath'] = $img_Arr[0];
					}
					
				}
				//$__price_dis['default_value'] = sprintf("%.2f", ceil($__price_dis['default_value']));
				$__price_dis['default_value'] = $__price_dis['default_value'];
				$hotel_array['total_price'] = @$__price_dis['default_value'];
			}
			$hotel_array['checkIn'] = '';
			$hotel_array['checkOut'] = '';
			
		}
		// debug($hotel_array); exit();
		return $hotel_array;
	}
	
	//validate recheck rates
	function valid_check_rate_details($check_room_rate_response) {
		if(isset($check_room_rate_response['Response']['ResponseDetails']['SearchHotelPriceResponse']['HotelDetails']['Hotel']['RoomCategories']['RoomCategory']) 
		&& valid_array($check_room_rate_response['Response']['ResponseDetails']['SearchHotelPriceResponse']['HotelDetails']['Hotel']['RoomCategories']['RoomCategory'])) {
			return true;
		}
		return false;
	}
	
	//recheck room rate request
	function check_room_rate_request($hotel_code,$safe_search_data,$rate_keys) {
		$CheckInDate = date('Y-m-d',strtotime($safe_search_data['from_date']));
		$duration = $safe_search_data['no_of_nights'];
		$room_id = $rate_keys[0];
		
		$searchRequest = '';
		$searchRequest .= '<Request>
				<Source>
					<RequestorID Client="'.$this->api_username1.'" EMailAddress="'.$this->api_username.'" Password="'.$this->password.'"/>
					<RequestorPreferences Country="CH" Currency="CAD" Language="en">
					<RequestMode>SYNCHRONOUS</RequestMode>
					</RequestorPreferences>
				</Source>
				<RequestDetails><SearchHotelPriceRequest>
						<ItemDestination DestinationType="city" DestinationCode="'.$safe_search_data['location_gta'].'" />
						<ItemCode>'.$hotel_code.'</ItemCode>
						
						<PeriodOfStay>
							<CheckInDate>'.$CheckInDate.'</CheckInDate>
							<Duration>'.$duration.'</Duration>
						</PeriodOfStay>
						<IncludeChargeConditions DateFormatResponse="true"></IncludeChargeConditions>';
		
		if(isset($safe_search_data['adult_config']) && valid_array($safe_search_data['adult_config'])) {
			$adult = $safe_search_data['adult_config'];
			$child = $safe_search_data['child_config'];
			$childage = @$safe_search_data['child_age'];
			
			$searchRequest .= '<Rooms>';
			$child_Age_arr_cnt = 0;
			for($i=0; $i < count($adult); $i++) {
				$adultCount = $adult[$i];
				$childCount = $child[$i];
				
				if($adultCount == 1 && $childCount == 0) {
					$searchRequest .= '<Room Code="SB" Id="'.$room_id.'" ></Room>';
				}else if($adultCount == 1 && $childCount == 1) {
					for($j=0; $j<$childCount; $j++) {
						if($childage[$child_Age_arr_cnt] == 1) {
							$searchRequest .= '<Room Code="TS" Id="'.$room_id.'" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$childage[$child_Age_arr_cnt].'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
						$child_Age_arr_cnt++;
					}
				}else if($adultCount == 1 && $childCount == 2) {
					$ageString = ''; 
					$numCots = 0;
					for($j=0; $j<$childCount; $j++) {
						if($childage[$child_Age_arr_cnt] == 1) {
							$numCots++;
						} else {
							$ageString .= '<Age>'.$childage[$child_Age_arr_cnt].'</Age>';
						}
						$child_Age_arr_cnt++;
					}
	
					if($numCots > 0) {
						$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
					} else {
						$numberOfCotsString = '';
					}
	
					if($ageString != "") {    //this means that children are more than 2 yr old
						$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
					} else {
						$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" '.$numberOfCotsString.' ></Room>';
					}
				}else if($adultCount == 2 && $childCount == 0) {
					$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" NumberOfRooms="1">';
					$searchRequest .= '</Room>';
				} else if($adultCount == 2 && $childCount == 1) {
					for($j=0; $j<$childCount; $j++) {
						if($childage[$child_Age_arr_cnt] == 1) {
							$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$childage[$child_Age_arr_cnt].'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
						$child_Age_arr_cnt++;
					}
				}else if($adultCount == 2 && $childCount == 2) {
					$ageString = ''; 
					$numCots = 0;
					for($j=0; $j<$childCount; $j++) {
						if($childage[$child_Age_arr_cnt] == 1) {
							$numCots++;
											
						} else {
							$ageString .= '<Age>'.$childage[$child_Age_arr_cnt].'</Age>';
						}
						$child_Age_arr_cnt++;
					}
	
					if($numCots > 0) {
						$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
					} else {
						$numberOfCotsString = '';
					}
	
					if($ageString != "") {    //this means that children are more than 2 yr old
						$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
					} else {
						$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" '.$numberOfCotsString.' ></Room>';
					}
				}else if($adultCount == 3 && $childCount == 0) {
					$searchRequest .= '<Room Id="'.$room_id.'" Code="TR">';
					$searchRequest .= '</Room>';
				}else if($adultCount == 3 && $childCount == 1) {
					/*
					A: 3, C: 1 - Book two rooms
					*/
					for($j=0; $j<$childCount; $j++) {
						if($childage[$child_Age_arr_cnt] == 1) {
							$searchRequest .= '<Room Id="'.$room_id.'" Code="TR" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Id="'.$room_id.'" Code="DB">';
							$searchRequest .= '<ExtraBeds><Age>'.$childage[$child_Age_arr_cnt].'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
							$searchRequest .= '<Room Id="'.$room_id.'" Code="DB">';
							$searchRequest .= '</Room>';
						}
						$child_Age_arr_cnt++;
					}
				}else if($adultCount == 3 && $childCount == 2) {
					$ca1 = $childage[$child_Age_arr_cnt];   // Get the age of individual child
					$ca2 = $childage[$child_Age_arr_cnt + 1];   // Get the age of individual child
	
					if($ca1 == 1 && $ca2 == 1) {
						$searchRequest .= '<Room Id="'.$room_id.'" Code="DB" NumberOfCots="2">';
						$searchRequest .= '</Room>';
	
						$searchRequest .= '<Room Id="'.$room_id.'" Code="SB">';
						$searchRequest .= '</Room>';
					}
					else if($ca1 == 1 && $ca2 > 1) {
						$searchRequest .= 	'<Room Id="'.$room_id.'" Code="DB" NumberOfCots="1">
												<ExtraBeds>
													<Age>'.$ca2.'</Age>
												</ExtraBeds>
											</Room>';
						$searchRequest .= '<Room Id="'.$room_id.'" Code="SB">';
						$searchRequest .= '</Room>';
					}
					else if($ca1 > 1 && $ca2 == 1) {
						$searchRequest .= 	'<Room Id="'.$room_id.'" Code="DB" NumberOfCots="1">
												<ExtraBeds>
													<Age>'.$ca1.'</Age>
												</ExtraBeds>
											</Room>';
						$searchRequest .= '<Room Id="'.$room_id.'" Code="SB">';
						$searchRequest .= '</Room>';
					}
					else if($ca1 > 1 && $ca2 > 1) {
						$searchRequest .= 	'<Room Id="'.$room_id.'" Code="TB">
												<ExtraBeds>
													<Age>'.$ca1.'</Age>
												</ExtraBeds>
											</Room>';
						$searchRequest .= 	'<Room Id="'.$room_id.'" Code="TB">
												<ExtraBeds>
													<Age>'.$ca2.'</Age>
												</ExtraBeds>
											</Room>';
					}	
				}else if($adultCount == 4 && $childCount == 0) {
					$searchRequest .= '<Room Id="'.$room_id.'" Code="Q">';
					$searchRequest .= '</Room>';
				}
			}
			$searchRequest .= '</Rooms>';
			$searchRequest .= 	'</SearchHotelPriceRequest></RequestDetails>
			</Request>';
		}
		$response['data'] = $searchRequest;
		$response['status'] = SUCCESS_STATUS;
		
		return $response;
	}
	
	
	//add booking request
	function add_booking_request($book_id, $book_attributes) {
		$searchRequest = '';
		$booking_reference = $book_id;
		$booking_reference = '<BookingReference>'.$book_id.'</BookingReference>';
		
		$hotel_details = $book_attributes['token']['token']['data'];
		$hotel_code = $hotel_details['hotel_code'];
		$room_id = $hotel_details['rooms']['rate_key'];
		$ExpectedPrice = $hotel_details['rooms']['xml_price'];
		//$currency = $hotel_details['rooms']['xml_currency'];
		
		$search_id = $book_attributes['token']['search_id'];
		$search_data = $this->search_data($search_id);
		$search_data = $search_data['data'];
		
		$checkin_date = date('Y-m-d',strtotime($hotel_details['checkIn']));
		$checkout_date = date('Y-m-d',strtotime($hotel_details['checkOut']));
		$room_count = $hotel_details['rooms']['rooms'];
		
		$total_pax_cnt = 0;
		$pax_id = 1;
		$pax_xml = '';
		$child_age_cnt = 0;
		$child_age_arr = @$search_data['child_age'];
		for($i=0;$i<$room_count;$i++) {
			if(isset($book_attributes['first_name']) && valid_array($book_attributes['first_name'])) {
				$adult_count = $search_data['adult_config'][$i];
				$adult_pax_snip = '';
				for($a=0;$a<$adult_count;$a++) {
					$adult_pax_prefix = get_enum_list('title',$book_attributes['name_title'][$total_pax_cnt]);
					$adult_pax_name = $book_attributes['first_name'][$total_pax_cnt];
					$adult_pax_surname = $book_attributes['last_name'][$total_pax_cnt];
					$type = $book_attributes['passenger_type'][$total_pax_cnt];
					
					$full_name = $adult_pax_prefix.' '.$adult_pax_name.' '.$adult_pax_surname;
					$adult_pax_snip .= '<PaxName PaxId="'.$pax_id.'" PaxType = "adult">'.$full_name.'</PaxName>';
					$pax_id++;
					$total_pax_cnt++;
				}
				$child_pax_snip = "";
				if(isset($search_data['child_config'][$i]) && !empty($search_data['child_config'][$i])) {
					$child_count = $search_data['child_config'][$i];
					for($c=0;$c<$child_count;$c++) {
						$child_pax_prefix = get_enum_list('title',$book_attributes['name_title'][$total_pax_cnt]);
						$child_pax_name = $book_attributes['first_name'][$total_pax_cnt];
						$child_pax_surname= $book_attributes['last_name'][$total_pax_cnt];
						
						$child_full_name = $child_pax_prefix.' '.$child_pax_name.' '.$child_pax_surname;
						$child_pax_snip .= '<PaxName PaxId="'.$pax_id.'" PaxType="child" ChildAge="'.$child_age_arr[$child_age_cnt].'" >'.$child_full_name.'</PaxName>';
						$pax_id++;
						$child_age_cnt++;
						$total_pax_cnt++;
					}
				}
				$pax_xml .= $adult_pax_snip.$child_pax_snip;
			}
		}
		
		$adlt_pass = "";
		$child_pass = "";
		
		$pax_names_block = '<PaxNames>'.$pax_xml.'</PaxNames>'; //paxnames block
		$itemReference = '<ItemReference>1</ItemReference>';
		$itemCity = '<ItemCity Code="'.$search_data['location_gta'].'" />';
		$itemCode = '<Item Code="'.$hotel_code.'" />';
		$checkin = '<PeriodOfStay><CheckInDate>'.$checkin_date.'</CheckInDate>';
		$checkout = '<CheckOutDate>'.$checkout_date.'</CheckOutDate></PeriodOfStay>';
		
		$child_ag_cnt = 0;
		$pad_id_cnt = 1;
		$hotel_rooms_block = "";
		$paxId_snip = ""; 
		
		for($r=0;$r<$room_count;$r++) {
			$room_total_adult = $search_data['adult_config'][$r];
			$room_total_child = 0;
			if(isset($search_data['child_config'][$r]) && !empty($search_data['child_config'][$r])) {
				$room_total_child = $search_data['child_config'][$r];
			}
			if($room_total_adult == 3 && $room_total_child == 1) {
				$child_Ag = $child_age_arr[$child_ag_cnt];
				if($child_Ag == 1) {
					$room_type_details = getRoomType($room_total_adult, $room_total_child, $child_Ag);
					$hotel_room_split = "";
					$hotel_room_split .= '<HotelRoom '.$room_type_details.' Id="'.$room_id.'"><PaxIds>';
					
					for($d=0;$d<$room_total_adult;$d++) {
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					for($e=0;$e<$room_total_child;$e++){
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					$hotel_room_split .= '</PaxIds></HotelRoom>';
					$hotel_rooms_block .= $hotel_room_split;
				}else {
					$room_type_details = $this->getRoomType($room_total_adult, $room_total_child, $child_Ag);
					$hotel_room_split = "";
					$hotel_room_split .= '<HotelRoom Code="DB" Id="'.$room_id.'"><PaxIds>';
					
					for($d=0;$d<2;$d++) {
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					$hotel_room_split .= '</PaxIds></HotelRoom>';

					$hotel_room_split .= '<HotelRoom Code="DB" Id="'.$room_id.'"><PaxIds>';
					
					for($e=0;$e<2;$e++){
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					$hotel_room_split .= '</PaxIds></HotelRoom>';
					$hotel_rooms_block .= $hotel_room_split;
				}
				$child_ag_cnt++;
			}else if($room_total_adult == 3 && $room_total_child == 2) {
				$ca1 = $child_age_arr[0];
				$ca2 = $child_age_arr[1];
				if($ca1 == 1 && $ca2 == 1) {
					$hotel_room_split = "";
					$hotel_room_split .= '<HotelRoom Code="DB" NumberOfCots="2" Id="'.$room_id.'"><PaxIds>';
					
					for($a=0; $a<2; $a++) {
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					$hotel_room_split .= '</PaxIds></HotelRoom>';
					$hotel_room_split .= '<HotelRoom Code="SB" Id="'.$room_id.'"><PaxIds>';
				}else if($ca1 == 1 && $ca2 > 1) {
					$hotel_room_split = "";
					$hotel_room_split .= '<HotelRoom Code="DB" Id="'.$room_id.'"><PaxIds>';
					
					for($a=0; $a<2; $a++) {
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					$hotel_room_split .= '</PaxIds></HotelRoom>';
					$hotel_room_split .= '<HotelRoom Code="DB" NumberOfCots="1" Id="'.$room_id.'"><PaxIds>';
					
					for($a=0; $a<2; $a++) {
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					
					$hotel_room_split .= '</PaxIds></HotelRoom>';
					$hotel_rooms_block .= $hotel_room_split;
				}else if($ca1 > 1 && $ca2 == 1) {
					$hotel_room_split = "";
					$hotel_room_split .= '<HotelRoom Code="DB" Id="'.$room_id.'"><PaxIds>';
					
					for($a=0; $a<2; $a++) {
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					
					$hotel_room_split .= '</PaxIds></HotelRoom>';
					$hotel_room_split .= '<HotelRoom Code="DB" NumberOfCots="1" Id="'.$room_id.'"><PaxIds>';
					
					for($a=0; $a<2; $a++) {
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					
					$hotel_room_split .= '</PaxIds></HotelRoom>';
					$hotel_rooms_block .= $hotel_room_split;
				}else if($ca1 > 1 && $ca2 > 1) { 
					$hotel_room_split = "";
					$hotel_room_split .= '<HotelRoom Code="DB" Id="'.$room_id.'"><PaxIds>';
					
					for($a=0; $a<2; $a++) {
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					$hotel_room_split .= '</PaxIds></HotelRoom>';
					$hotel_room_split .= '<HotelRoom Code="DB" ExtraBed="true" NumberOfExtraBeds="1" Id="'.$room_id.'"><PaxIds>';
					
					for($a=0; $a<3; $a++) {
						$hotel_room_split .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
					}
					$hotel_room_split .= '</PaxIds></HotelRoom>';
					$hotel_rooms_block .= $hotel_room_split;
				}
			}else {
				$child_age_arr = $child_age_arr;
				$room_type_details = $this->getRoomType($room_total_adult, $room_total_child, $child_age_arr); 
				
				$hotel_room = '<HotelRoom '.$room_type_details.' Id="'.$room_id.'"><PaxIds>';
				for($a=0;$a<$room_total_adult;$a++) {
					$paxId_snip .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
					$pad_id_cnt++;
				}
				
				if($room_total_child >0) {
					for($c=0;$c<$room_total_child;$c++) {
						$age_c = $child_age_arr[$child_ag_cnt];
						$paxId_snip .= '<PaxId>'.$pad_id_cnt.'</PaxId>';
						$pad_id_cnt++;
						$child_age_cnt++;
					}
				}
				$hotel_room_tail = '</PaxIds></HotelRoom>';
				$hotel_rooms_block .= $hotel_room.$paxId_snip.$hotel_room_tail;
			
			}
			$paxId_snip = "";  //reset for next iteration
			
		}
		$booking_request = '<Request>
				<Source>
					<RequestorID Client="'.$this->api_username1.'" EMailAddress="'.$this->api_username.'" Password="'.$this->password.'"/>
					<RequestorPreferences Country="CH" Currency="CAD" Language="en">
					<RequestMode>SYNCHRONOUS</RequestMode>
					</RequestorPreferences>
				</Source>
				<RequestDetails><AddBookingRequest Currency="CHF">'.
								$booking_reference.
								$pax_names_block.
								'<BookingItems>'.
									'<BookingItem ItemType="hotel" ExpectedPrice = "'.$ExpectedPrice.'" >'.
										$itemReference.
										$itemCity.
										$itemCode.
										'<HotelItem>'.
											$checkin.$checkout.
											'<HotelRooms>'.
												$hotel_rooms_block.
											'</HotelRooms>'.
										'</HotelItem>'.
									'</BookingItem>'.
								'</BookingItems>'.
							'</AddBookingRequest></RequestDetails>
			</Request>';
												
		$response['request'] = $booking_request;
		$response['status'] = SUCCESS_STATUS;
		// debug($response); exit()
		return $response;
		
	}
	
	//add booking 
	function add_booking($book_id, $book_attributes) {
		$response['data'] = array();
		$response['status'] = FAILURE_STATUS;
		$booking_request = $booking_response_xml = $this->add_booking_request($book_id, $book_attributes);
		//debug($booking_request);//die;
		if($booking_request['status'] == SUCCESS_STATUS) {
			$booking_response = $this->process_details($booking_request['request']);
			//debug($booking_response);//die;
			if(isset($booking_response) && !empty($booking_response)) {
				$booking_response = utf8_encode($booking_response);
				$booking_response = Converter::createArray($booking_response);
				$GLOBALS['CI']->private_management_model->provab_xml_logger('Book_Room', $book_id, 'hotel', $booking_request['request'], $booking_response_xml);
				//debug($booking_response);die;
				if($this->valid_booking_response($booking_response)) {
					$response['data']['book_response']	= $booking_response;
					$response['data']['booking_params']	= $book_attributes;
					$response['status'] = SUCCESS_STATUS;
				}
			}
		}
		return $response;
	}
	
	//save booking details
	function save_booking($book_id, $booking, $module = 'b2c') {
		$app_reference = $book_id;
		
		$booking_details = $booking['book_response'];
		//debug($booking['book_response']['Response']['ResponseDetails']['BookingResponse']['BookingItems']['BookingItem']['ItemConfirmationReference']);die;
		$psbooking_details = $booking['book_response']['Response']['ResponseDetails']['BookingResponse']['BookingItems']['BookingItem']['ItemConfirmationReference'];
		//debug($booking_details);exit;
		$booking_params = $booking['booking_params'];
		
		$search_id = $booking_params['token']['search_id'];
		$search_details = $this->search_data($search_id);
		
		$no_of_nights = $search_details ['data'] ['no_of_nights'];
		$total_room_count = @$booking_params['token']['token']['data']['rooms']['rooms'];
		$imagePath = @$booking_params['token']['token']['data']['imagePath'];
		$email = $booking_params['billing_email'];
		$phone = $booking_params['passenger_contact'];
		$conviencee = @$booking_params['convenience_fees'];
		$booking_source = $booking_params['booking_source'];
		$payment_mode = $booking_params['payment_method'];
		$admin_markup = 0;
		$agent_markup = 0;
		$discount = 0;
		
		if(isset($booking_details) && valid_array($booking_details)) {
			$booking_details = $booking_details['Response']['ResponseDetails']['BookingResponse'];
			if(isset($booking_details['BookingReferences']['BookingReference']) && valid_array($booking_details['BookingReferences']['BookingReference'])) {
				$booking_Reference_Arr = force_multple_data_format($booking_details['BookingReferences']['BookingReference']);
				foreach($booking_Reference_Arr as $r_k => $__refer) {
					if(@$__refer['@attributes']['ReferenceSource'] == 'client') {
						$app_reference = $__refer['@cdata'];
					}else if(@$__refer['@attributes']['ReferenceSource'] == 'api') {
						$booking_reference = $__refer['@cdata'];
					}else {
						
					}
				}
			}
			
			$booking_created_Date = @$booking_details['BookingCreationDate'];
			$lead_pax_name = @$booking_details['BookingName'];
			
			//price
			if(isset($booking_details['BookingPrice']) && valid_array($booking_details['BookingPrice'])) {
				$price_arr = $booking_details['BookingPrice'];
				
				$booking_price = $price_arr['@attributes']['Nett'];
				$booking_currency =  $price_arr['@attributes']['Currency'];
			}
			
			$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => $booking_currency, 'to' => get_application_transaction_currency_preference()));
			$deduction_cur_obj = clone $currency_obj;
			
			if ($module == 'b2c') {
				$markup_total_fare = $currency_obj->get_currency ( $booking_price, true, false, true, $no_of_nights * $total_room_count ); // (ON Total PRICE ONLY)
				$ded_total_fare = $deduction_cur_obj->get_currency ( $booking_price, true, true, false, $no_of_nights * $total_room_count ); // (ON Total PRICE ONLY)
				
				$admin_markup = sprintf ( "%.2f", $markup_total_fare ['default_value'] - $ded_total_fare ['default_value'] );
				$booking_price = $deduction_cur_obj->get_currency ( $booking_price, false, false, false, 1 );
				$agent_markup = sprintf ( "%.2f", $ded_total_fare ['default_value'] - $booking_price['default_value'] );
			} else {
				// B2B Calculation
				$markup_total_fare = $currency_obj->get_currency ( $booking_price, true, true, true, $no_of_nights * $total_room_count ); // (ON Total PRICE ONLY)
				$ded_total_fare = $deduction_cur_obj->get_currency ( $booking_price, true, false, true, $no_of_nights * $total_room_count ); // (ON Total PRICE ONLY)
				$admin_markup = sprintf ( "%.2f", $markup_total_fare ['default_value'] - $ded_total_fare ['default_value'] );
				$booking_price = $deduction_cur_obj->get_currency ( $booking_price, false, false, false, 1 );
				$agent_markup = sprintf ( "%.2f", $ded_total_fare ['default_value'] - $booking_price['default_value'] );
			}
			
			$total_net = $booking_price['default_value'] = round($booking_price['default_value'] + $admin_markup + $agent_markup);

			$currency = $booking_price['default_currency'];
			$status = $booking_details['BookingStatus']['@cdata'];
			//debug($status);//['@attributes']['Code'];
			$booking_status = $this->get_booking_status($status);//BOOKING_CONFIRMED (c)
			
			$CI = &get_instance();
			$CI->db->trans_start();
			
			//booking item details
			if(isset($booking_details['BookingItems']['BookingItem']) && valid_array($booking_details['BookingItems']['BookingItem'])) {
				$booking_item = $booking_details['BookingItems']['BookingItem'];
				//debug($booking_item);exit;
				$city = $booking_item['ItemCity']['@attributes']['Code']; //$booking_item['ItemCity']['@cdata'] name
				$hotel_code = $booking_item['Item']['@attributes']['Code'];
				$hotel_name = $booking_item['Item']['@cdata'];
				$meals = $booking_item['HotelItem']['Meals'];
				if(isset($meals['Basis'])) {
					$boardName = 'BREAKFAST';
					$boardCode = $meals['Basis']['@attributes']['Code'];
				}
				else{
					$boardName = '';
					$boardCode = '';
				}
				if(isset($meals['Breakfast'])){
					if($meals['Breakfast']['Code'] == 'F'){
						$boardName = 'FULL BREAKFAST';
						$boardCode = $boardCode.$meals['Breakfast']['@attributes']['Code'];
					}
					if($meals['Breakfast']['Code'] == 'H'){
						$boardName = 'HALF BREAKFAST';
						$boardCode = $boardCode.$meals['Breakfast']['@attributes']['Code'];
					}
				}
				
				$item_confirmation_no = $booking_item['ItemConfirmationReference'];
				
				if(isset($booking_item['EssentialInformation']['Information']) && valid_array($booking_item['EssentialInformation']['Information'])) {
					$essencial_info = force_multple_data_format($booking_item['EssentialInformation']['Information']);
					$essencial_info_text = array();
					foreach($essencial_info as $a_k => $__ainfo) {
						$from_to = '';
						if(isset($__ainfo['DateRange']['FromDate']) && !empty($__ainfo['DateRange']['FromDate'])) {
							$from_to .= date('d-m-Y',strtotime(@$__ainfo['DateRange']['FromDate']));
						}
						if(isset($__ainfo['DateRange']['ToDate']) && !empty($__ainfo['DateRange']['ToDate'])) {
							$from_to .= ' To '.date('d-m-Y',strtotime(@$__ainfo['DateRange']['ToDate']));
						}
						$essencial_info_text[] = $__ainfo['Text']['@cdata'] .'('.$from_to.')';
					}
				}
				
				//hotel item
				if(isset($booking_item['HotelItem']) && valid_array($booking_item['HotelItem'])) {
					$hotel_checkin = $booking_item['HotelItem']['PeriodOfStay']['CheckInDate'];
					$hotel_checkout = $booking_item['HotelItem']['PeriodOfStay']['CheckOutDate'];
					
					//rooms
					$itenary_arr = array();
					$pass_room_com = array();
					$room_no = 1;
					if(isset($booking_item['HotelItem']['HotelRooms']['HotelRoom']) && valid_array($booking_item['HotelItem']['HotelRooms']['HotelRoom'])) {
						$rooms = force_multple_data_format($booking_item['HotelItem']['HotelRooms']['HotelRoom']);
						foreach($rooms as $r_k => $__room) {
							$room_name = @$__room['Description']['@cdata'];
							$bed_type = @$__room['@attributes']['Code'];
							$air_booking_id1 = substr($app_reference,3);
							$air_booking_id = substr_replace($air_booking_id1,'G-',2,2);
							$airliner_booking_reference = $air_booking_id;
							$itenary_arr = array(
								'app_reference'=>$app_reference,
								'location' => $city,
								'check_in' => $hotel_checkin,
								'check_out' => $hotel_checkout,
								'room_type_name' => $room_name,
								'bed_type_code' => $room_name,
								'status' => $booking_status,
								'total_fare' => @$booking_price['default_value'],
								'admin_markup' => $admin_markup,
								'agent_markup' => $agent_markup,
								'currency' => $currency,
								'attributes' => '',
								'RoomPrice' => '',
								'Tax' => '',
								'ExtraGuestCharge' => '',
								'ChildCharge' => '',
								'OtherCharges' =>'',
								'Discount' => $discount,
								'ServiceTax' => '',
								'AgentCommission' => '',
								'AgentMarkUp' => '',
								'TDS' => ''
							);
							$CI->db->insert('hotel_booking_itinerary_details',$itenary_arr);
							
							if(isset($__room['PaxIds']['PaxId']) && !empty($__room['PaxIds']['PaxId'])) {
								if(valid_array($__room['PaxIds']['PaxId'])) {
									$pax_id_ar = force_multple_data_format($__room['PaxIds']['PaxId']);
									foreach($pax_id_ar as $__pk => $__px_k_v) {
										$pass_room_com[$__px_k_v] = $room_no;
									}
								}else {
									$pass_room_com[$__room['PaxIds']['PaxId']] = $room_no;
								}
							}
							
							$room_no++;
						}
					}
				}
				
				//cancelation and other charges detail
				if(isset($booking_item['ChargeConditions']['ChargeCondition']) && valid_array($booking_item['ChargeConditions']['ChargeCondition'])) {
					$charges_array = force_multple_data_format($booking_item['ChargeConditions']['ChargeCondition']);
					foreach($charges_array as $c_k => $__charge) {
//						/hotel_cancelation_policy
						if($__charge['@attributes']['Type'] == 'cancellation') {
							$__charge['Condition'] = force_multple_data_format($__charge['Condition']);
							foreach($__charge['Condition'] as $ch_k => $__chr) {
								$cncel_charge = 0;
								if($__chr['@attributes']['Charge'] == 'true') {
									$cncel_charge = $__chr['@attributes']['ChargeAmount'];
									$cncel_curr = $__chr['@attributes']['Currency'];
									if ($module == 'b2c') {
										$markup_cncel_fare = $currency_obj->get_currency ( $cncel_charge, true, false, true, $no_of_nights * $total_room_count ); // (ON Total PRICE ONLY)
									} else {
										// B2B Calculation
										$markup_cncel_fare = $currency_obj->get_currency ( $cncel_charge, true, true, true, $no_of_nights * $total_room_count ); // (ON Total PRICE ONLY)
									}
									$cncel_charge = $markup_cncel_fare['default_value'];
								}
								$cncel_currency = get_application_transaction_currency_preference();
								$date = $__chr['@attributes']['FromDate'];
								
								$hotel_cancelation_policy = array(
									'app_reference' => $app_reference,
									'booking_reference' => $booking_reference,
									'amount' => $cncel_charge,
									'date' => $date,
									'currency' => $cncel_currency
								);
								$CI->db->insert('hotel_cancelation_policy',$hotel_cancelation_policy);
							}
						}
						
					}
				}
				
				//passenger detilas
				$only_dob_pax = 0;
				$passenger_array = array();
				if(isset($booking_details['PaxNames']['PaxName']) && valid_array($booking_details['PaxNames']['PaxName'])) {
					$paxes_details = force_multple_data_format($booking_details['PaxNames']['PaxName']);
					foreach($paxes_details as $p_k => $__pax) {
						$room_id = 1;
						if(isset($pass_room_com[$__pax['@attributes']['PaxId']])) {
							$room_id = $pass_room_com[$__pax['@attributes']['PaxId']];
						}
						
						$age = 30;
						$pax_type = 'Adult';
						if(isset($__pax['@attributes']['PaxType']) && $__pax['@attributes']['PaxType'] == 'child') {
							$age = $__pax['@attributes']['ChildAge'];
							$pax_type = 'Child';
						}
						$name = $__pax['@cdata'];
						$expl_nmae= explode(' ',$name);
						$passenger_array[$p_k]=array(
							'name' => $__pax['@cdata'],
							'pax_id' => $__pax['@attributes']['PaxId'],
							'age' => $age
						);
						$passenger_insert_arr = array(
							'app_reference' => $app_reference,
							'title'=> @$expl_nmae[0],
							'first_name' => @$expl_nmae[1],
							'last_name' => @$expl_nmae[2],
							'middle_name' => @$expl_nmae[1],
							'phone' => $phone,
							'email' => $email,
							'pax_type' => $pax_type,
							'age' => $age,
							'date_of_birth' => date("Y-m-d", strtotime($booking_params['dob'][$only_dob_pax])),
							'passenger_nationality' => '',
							'passport_number' => '',
							'passport_issuing_country'=>'',
							'passport_expiry_date' => '',
							'status' => $booking_status,
							'attributes'=>''
						);
						$CI->db->insert('hotel_booking_pax_details',$passenger_insert_arr);
						$only_dob_pax++;
					}
				}
			}
			$gta_contact = $CI->hotel_model->get_gta_hotel_contact($hotel_code);
			$phone_number = $gta_contact->Telephone;
			$address = $gta_contact->Address;
//			if(isset($hotel_code) && isset($city)) {
//				//debug($hotel_code);debug($city);
//				$static_info = $this->get_hotel_info($hotel_code,$city);
//				debug($static_info);
//			}	
			$country_name = $GLOBALS ['CI']->db_cache_api->get_country_list ( array (
				'k' => 'origin',
				'v' => 'name' 
				), array (
				'origin' => $booking_params['billing_country'] 
			) );
			$attribute = array (
				'essencial_info' => @$essencial_info_text,
				'address' => @$booking_params['billing_address_1'],
				'billing_country' => @$country_name [$booking_params['billing_country']],
				'billing_city' => @$booking_params['billing_city'],
				'billing_zipcode' => @$booking_params['billing_zipcode'],
				'HotelCode' => @$hotel_code,
				'search_id' => @$booking_params['token'] ['search_id'],
				'currency' => $currency,
				'supplier' => $this->supplier,		
				'hotel_add_info' => $address,		
				'boardName' => $boardName,		
				'boardCode' => $boardCode,		
				'phone_number' => $phone_number,
				'imagePath' => $imagePath,
				'users_comments' => @$booking_params['users_comments']
			);
			$created_by_id = intval ( @$GLOBALS ['CI']->entity_user_id );
			$hotel_booking_details = array(
				'status' => $booking_status,
				'domain_origin' => 1,
				'app_reference' => $app_reference,
				'airliner_booking_reference' => $airliner_booking_reference,
				'booking_id' => $psbooking_details,
				'booking_source' => $booking_source,
				'booking_reference' => $booking_reference,
				'confirmation_reference' => $item_confirmation_no,
				'hotel_name' => $hotel_name,
				'star_rating' => '',
				'hotel_code' => $hotel_code,
				'phone_number' => $phone,
				'alternate_number' => $phone,
				'email' => $email,
				'hotel_check_in' => @$hotel_checkin,
				'hotel_check_out'=> @$hotel_checkout,
				'payment_mode' => @$payment_mode,
				'payment_mode' => @$booking_price['default_value'],	
				'currency' => @$currency,
				'convinence_value' => '',
				'convinence_value_type' => 'percentage',
				'convinence_per_pax' => '',
				'convinence_amount' => $conviencee,
				'discount' => $discount,
				'attributes' => json_encode(@$attribute),
				'created_by_id' => $created_by_id,
				'created_datetime' => $booking_created_Date
			);
		//	debug($hotel_booking_details);exit;
			$CI->db->insert('hotel_booking_details',$hotel_booking_details);
			$CI->db->trans_complete();
		}
		$response ['fare'] = $booking_price['default_value'];
		$response ['admin_markup'] = $admin_markup;
		$response ['agent_markup'] = $agent_markup;
		$response ['convinence'] = $conviencee;
		$response ['discount'] = $discount;
		return $response;
	}
	
	//booking status
	function get_booking_status($status) {
		if($status == 'Confirmed' || $status == 'c') {
			$booking_status = 'BOOKING_CONFIRMED';
		}else {
			$booking_status = 'BOOKING_INCOMPLETE';
		}
//		switch($status) {
//			case 'C' : $booking_status = 'BOOKING_CONFIRMED';
//						break;
//			default:$booking_status = 'BOOKING_INCOMPLETE';
//						break;
//				
//		}
		return $booking_status;
	}
	
	//validate booking response
	function valid_booking_response($booking_response) {
		if(isset($booking_response['Response']['ResponseDetails']['BookingResponse']['BookingReferences']) 
		&& valid_array($booking_response['Response']['ResponseDetails']['BookingResponse']['BookingReferences'])) {
			return true;
		}
		return false;
	}
	
	function getRoomType($a, $c, $ca){

	// echo "<pre>"; print_r($ca); echo "</pre>"; die();
	if($a == 1 && $c == 0) {	
		$room_type = 'Code="SB"';	
	} 
	else if($a == 2 && $c == 0) {	
		$room_type = 'Code="DB"';	
	} 
	else if($a == 3 && $c == 0) {
		$room_type = 'Code="TR"'; 
	} 
	else if($a == 2 && $c == 1) {

		$noc=0;
		for($j=0; $j<count($ca); $j++) {
			if($ca[$j] == 1) {
				$noc++;
			}
		}
		if($noc > 0) {
			$numberOfCotsString = 'NumberOfCots="'.$noc.'"';
		} else {
			$numberOfCotsString = "";
		}

		$extraBedNeeded = $c - $noc;

		if($extraBedNeeded > 0) {
			$NumberOfExtraBedsString = 'ExtraBed="true" NumberOfExtraBeds="'.$extraBedNeeded.'"';
		} else {
			$NumberOfExtraBedsString = '';
		}

		$room_type = $NumberOfExtraBedsString.$numberOfCotsString.' Code="DB"';
	} 
	else if($a == 2 && $c == 2) {

		$noc=0;
		for($j=0; $j<count($ca); $j++) {
			if($ca[$j] == 1) {
				$noc++;
			}
		}
		if($noc > 0) {
			$numberOfCotsString = 'NumberOfCots="'.$noc.'"';
		} else {
			$numberOfCotsString = "";
		}

		/*
			Total number of child must be subtracted from the number of 
			infants to get the number of extrabeds
			$noc carries the number of infants and $c carries total number of children (including infants).
		*/

		$extraBedNeeded = $c - $noc;

		if($extraBedNeeded > 0) {
			$NumberOfExtraBedsString = 'ExtraBed="true" NumberOfExtraBeds="'.$extraBedNeeded.'"';
		} else {
			$NumberOfExtraBedsString = '';
		}

		/*
			Extrabeds are needed only when there is a child travelling else it will not be needed
		*/

		$room_type = $NumberOfExtraBedsString.' '.$numberOfCotsString.' Code="DB"';
	} 
	else if($a == 3 && $c == 1) {

		$noc=0;
		for($j=0; $j<count($ca); $j++) {
			if($ca[$j] == 1) {
				$noc++;
			}
		}
		if($noc > 0) {
			$numberOfCotsString = 'NumberOfCots="'.$noc.'"';
		} else {
			$numberOfCotsString = "";
		}

		/*
			Total number of child must be subtracted from the number of 
			infants to get the number of extrabeds
			$noc carries the number of infants and $c carries total number of children (including infants).
		*/

		$extraBedNeeded = $c - $noc;

		if($extraBedNeeded > 0) {
			$NumberOfExtraBedsString = 'ExtraBed="true" NumberOfExtraBeds="'.$extraBedNeeded.'"';
			$room_code = 'Code="TB"';
		} else {
			$NumberOfExtraBedsString = '';
			$room_code = 'Code="TR"';
		}

		/*
			Extrabeds are needed only when there is a child travelling else it will not be needed
		*/

		$room_type = $NumberOfExtraBedsString.' '.$numberOfCotsString.' '.$room_code;		
	} 
	else if($a == 1 && $c == 1) {
		
		$noc=0;
		for($j=0; $j<count($ca); $j++) {
			if($ca[$j] == 1) {
				$noc++;    //number of infant kids
			}
		}

		if($noc > 0) {
			$numberOfCotsString = 'NumberOfCots="'.$noc.'"';  //add number of cots only when kid is an infant
		} else {
			$numberOfCotsString = "";
		}

		$extraBedNeeded = $c - $noc;

		if($extraBedNeeded > 0) {
			$NumberOfExtraBedsString = '';
			$room_code = 'Code="DB"';
		} else {
			$NumberOfExtraBedsString = '';
			$room_code = 'Code="TS"';
		}

		$room_type = $NumberOfExtraBedsString.' '.$numberOfCotsString.' '.$room_code;	
	} 
	else if($a == 1 && $c == 2) {

		$noc=0;
		for($j=0; $j<count($ca); $j++) {
			if($ca[$j] == 1) {
				$noc++;
			}
		}

		if($noc > 0) {
			$numberOfCotsString = 'NumberOfCots="'.$noc.'"';
		} else {
			$numberOfCotsString = "";
		}



		/*
			Total number of child must be subtracted from the number of 
			infants to get the number of extrabeds
			$noc carries the number of infants and $c carries total number of children (including infants).
		*/

		$extraBedNeeded = $c - $noc;

		if($extraBedNeeded > 0) {
			if($extraBedNeeded == 2){
				$NumberOfExtraBedsString = 'ExtraBed="true" NumberOfExtraBeds="1"';  //extrabed is 1 because 1 bed will be shared with the adult.
				$code = 'Code="DB"';  //Double Bed for 1 adult 2 child. 1 will share with adult and 1 extra bed will be provided.
			} else {
				$NumberOfExtraBedsString = '';
				$code = 'Code="DB"';
			}

		} else {
			$NumberOfExtraBedsString = '';
			$code = 'Code="TS"';  //twin for sole purpose with two cots
		}

		/*
			Extrabeds are needed only when there is a child travelling else it will not be needed
		*/

		$room_type = $NumberOfExtraBedsString.' '.$numberOfCotsString.' '.$code;
		//$room_type = 'Code="DB" ExtraBed="true" NumberOfExtraBeds="2" '.$numberOfCotsString.' ';	
	}  
	else {	
		$room_type = "";	
	}
	return $room_type;
}
	
	//hotel detail xml request 
	function hotel_details_request($search_data,$hotel_id) {
		$room_count = $search_data['room_count'];
		$room_xml_string = '';
		
		$search_details = $this->search_data ( $search_data['search_id'] );
		$search_details = $search_details['data'];
		$check_in_date_format = date('Y-m-d',strtotime($search_data['from_date']));
		
		for($h=0; $h<$room_count; $h++) {
			$room_total_adult = $search_data['adult_config'][$h];
			$room_total_child = $search_data['adult_config'][$h];
			$room_code = $this->get_room_type_precheck($room_total_adult, $room_total_child);
			$room_xml_string .= '<Room '.$room_code.' />';
		}
		$searchRequest = '';
		$searchRequest .= '<Request>
				<Source>
					<RequestorID Client="'.$this->api_username1.'" EMailAddress="'.$this->api_username.'" Password="'.$this->password.'"/>
					<RequestorPreferences Country="CH" Currency="CAD" Language="en">
					<RequestMode>SYNCHRONOUS</RequestMode>
					</RequestorPreferences>
				</Source>
				<RequestDetails>
					<SearchHotelPriceRequest>
						<ItemDestination DestinationType="city" DestinationCode="'.$search_details['location_gta'].'" />
							<ItemCodes>
								<ItemCode>'.$hotel_id.'</ItemCode>
							</ItemCodes>
							<PeriodOfStay>
								<CheckInDate>'.$check_in_date_format.'</CheckInDate>
								<Duration>'.$search_data['no_of_nights'].'</Duration>
							</PeriodOfStay>
							<IncludePriceBreakdown/>
							<IncludeChargeConditions DateFormatResponse="true"></IncludeChargeConditions>';
		
		if(isset($search_data['adult_config']) && valid_array($search_data['adult_config'])) {
			$adult = $search_data['adult_config'];
			$child = $search_data['child_config'];
			$child_ages = @$search_data['child_age'];
		
			$searchRequest .= '<Rooms>';
			$total_child_Age_key = 0;
			
			for($a=0;$a<COUNT($adult);$a++) {
				$total_adult = $adult[$a];
				$total_child = $child[$a];
				
				if($total_adult == 1 && $total_child == 0) {
					$searchRequest .= '<Room Code="SB"></Room>';
				}else if ($total_adult == 1 && $total_child == 1) {
					for($b=0;$b<$total_child;$b++) {
						if($child_ages[$total_child_Age_key] == 1) {
							$searchRequest .= '<Room Code="TS" NumberOfCots="1"></Room>';
						} else {
							$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$child_ages[$total_child_Age_key].'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
						$total_child_Age_key++;
					}
				}else if($total_adult == 1 && $total_child == 2) {
					$ageString = ''; 
					$numCots = 0;
					for($c = 0;$c<$total_child;$c++) {
						if($child_ages[$total_child_Age_key] == 1) {
							$numCots++;
						} else {
							$ageString .= '<Age>'.$child_ages[$total_child_Age_key].'</Age>';
						}
						$total_child_Age_key++;
					}
					
					if($numCots > 0) {
						$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
					} else {
						$numberOfCotsString = '';
					}
					
					if($ageString != "") {
						//this means that children are more than 2 yr old
						$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
					} else {
						$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ></Room>';
					}
				}else if($total_adult == 2 && $total_child == 0) {
					$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
					$searchRequest .= '</Room>';
				}else if($total_adult == 2 && $total_child == 1) {
					for($d = 0;$d<$total_child;$d++) {
						if($child_ages[$total_child_Age_key] == 1) {
							$searchRequest .= '<Room Code="DB" NumberOfCots="1"></Room>';		
						} else {
							$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$child_ages[$total_child_Age_key].'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
						$total_child_Age_key++;
					}
				}else if($total_adult == 2 && $total_child == 2) {
					$ageString = ''; 
					$numCots = 0;
					for($e = 0;$e<$total_child;$e++) {
						if($child_ages[$total_child_Age_key] == 1) {
							$numCots++;		
						} else {
							$ageString .= '<Age>'.$child_ages[$total_child_Age_key].'</Age>';
						}
						$total_child_Age_key++;
					}
					if($numCots > 0) {
						$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
					} else {
						$numberOfCotsString = '';
					}
					
					if($ageString != "") {    //this means that children are more than 2 yr old
						$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
					} else {
						$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ></Room>';
					}
				}else if($total_adult == 3 && $total_child == 0) {
					$searchRequest .= '<Room Code="TR">';
					$searchRequest .= '</Room>';
				}else if($total_adult == 3 && $total_child == 1) {
					for($f = 0;$f<$total_child;$f++) {
						if($child_ages[$total_child_Age_key] == 1) {
							$searchRequest .= '<Room Code="TR" NumberOfCots="1"></Room>';	
						} else {
							$searchRequest .= '<Room Code="DB">';
							$searchRequest .= '<ExtraBeds><Age>'.$child_ages[$total_child_Age_key].'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';

							$searchRequest .= '<Room Code="DB">';
							$searchRequest .= '</Room>';
						}
						$total_child_Age_key++;
					}
				}else if($total_adult == 3 && $total_child == 2) {
					$age1 = $child_ages[$total_child_Age_key];
					$age2 = $child_ages[$total_child_Age_key + 1];
					
					if($age1 == 1 && $age2 == 1) {
						$searchRequest .= '<Room Code="DB" NumberOfCots="2">';
						$searchRequest .= '</Room>';
	
						$searchRequest .= '<Room Code="SB">';
						$searchRequest .= '</Room>';
					}
					else if($age1 == 1 && $age2 > 1) {
						$searchRequest .= 	'<Room Code="DB" NumberOfCots="1">
												<ExtraBeds>
													<Age>'.$age2.'</Age>
												</ExtraBeds>
											</Room>';
						$searchRequest .= '<Room Code="SB">';
						$searchRequest .= '</Room>';
					}
					else if($age1 > 1 && $age2 == 1) {
						$searchRequest .= 	'<Room Code="DB" NumberOfCots="1">
												<ExtraBeds>
													<Age>'.$age1.'</Age>
												</ExtraBeds>
											</Room>';
						$searchRequest .= '<Room Code="SB">';
						$searchRequest .= '</Room>';
					}
					else if($age1 > 1 && $age2 > 1) {
						$searchRequest .= 	'<Room Code="TB">
												<ExtraBeds>
													<Age>'.$age1.'</Age>
												</ExtraBeds>
											</Room>';
						$searchRequest .= 	'<Room Code="TB">
												<ExtraBeds>
													<Age>'.$age2.'</Age>
												</ExtraBeds>
											</Room>';
					}
				}else if($total_adult == 4 && $total_child == 0) {
					$searchRequest .= '<Room Code="Q">';
					$searchRequest .= '</Room>';
				}
			}
			$searchRequest .= '</Rooms>';
		}
		$searchRequest .= 	'</SearchHotelPriceRequest></RequestDetails>
			</Request>';
		$response['status'] = SUCCESS_STATUS;
		$response['request'] = $searchRequest;
		
		return $response;
	}
	
	//hotel booking cancellation
	function cancel_booking($booking_details,$cncel_amt,$currency,$total_amount,$module='b2c') {
		// 1.SendChangeRequest
		// 2.GetChangeRequestStatus
		$response ['data'] = array ();
		$response ['status'] = FAILURE_STATUS;
		$resposne ['msg'] = 'Remote IO Error';
		$BookingId = $booking_details ['booking_reference'];
		$app_reference = $booking_details ['app_reference'];
		$send_change_request = $this->send_change_request_params ( $BookingId );
		if ($send_change_request ['status']) {
			// 1.SendChangeRequest
			$send_change_request_response = $this->process_details($send_change_request['request']);
			$GLOBALS ['CI']->custom_db->generate_static_response ($send_change_request_response);
			if(isset($send_change_request_response) && !empty($send_change_request_response)) {
				$send_change_request_response = Converter::createArray($send_change_request_response);
			}
			//debug($send_change_request_response);exit;
			/* $send_change_request_response = $GLOBALS['CI']->hotel_model->get_static_response(1755);//1624 //(pocessed-1783, 1755) */
			if (valid_array($send_change_request_response['Response']['ResponseDetails']['BookingResponse']) == true) {
				$this->save_cancellation_data($BookingId,$app_reference,$send_change_request_response,$cncel_amt,$currency,$total_amount,$module);
				return $send_change_request_response;
				//return $this->get_change_request_status ($send_change_request_response['booking'], $app_reference );
			} else {
				$response ['msg'] = $send_change_request_response['error']['message'];
			}
		}
		return $response;
	}
	
	/*
	 * Jaganath
	 * Save the Cancellation Details into Database
	 */
	function save_cancellation_data($booking_reference,$app_reference, $send_change_request_response,$cancel_amt,$currency,$total_amount,$module) {//echo 'save_fun';
		if(isset($send_change_request_response['Response']['ResponseDetails']['BookingResponse']['BookingStatus']) && !empty($send_change_request_response['Response']['ResponseDetails']['BookingResponse']['BookingStatus'])) {
			$status = $send_change_request_response['Response']['ResponseDetails']['BookingResponse']['BookingStatus']['@cdata'];
			$cancellation_reference = '';
			//cancellation Amount
			$api_cncel_amt = '';
			if(isset($send_change_request_response['booking']['hotel']) && valid_array($send_change_request_response['booking']['hotel'])) {
				$cancellation_amount = @$send_change_request_response['Response']['ResponseDetails']['BookingResponse']['BookingPrice']['@attributes']['Nett'];
				$currency = @$send_change_request_response['Response']['ResponseDetails']['BookingResponse']['BookingPrice']['@attributes']['Currency'];
				$cncel_amt = $currency . ' ' .$cancellation_amount;
			}
			
			$HotelChangeRequestStatusResult = $send_change_request_response;
			$app_reference = trim ( $app_reference );
			$ChangeRequestId = @$cancellation_reference;
			$ChangeRequestStatus = $status;
			$status_description = $status; //$this->ChangeRequestStatusDescription ( $ChangeRequestStatus );
			$TraceId = @$cancellation_reference;
			$API_RefundedAmount = $total_amount - $cancel_amt;
			$API_CancellationCharge = @$cancel_amt;
			$attr = json_encode ( $send_change_request_response );
			// Update Booking Status
			if ($ChangeRequestStatus == 'Cancelled') {
				$booking_status = 'BOOKING_CANCELLED';
				// Update Master Booking Status
				$GLOBALS ['CI']->hotel_model->update_master_booking_status ( $app_reference, $booking_status );
				// Update Pax Booing Status
				$GLOBALS ['CI']->hotel_model->update_pax_booking_status ( $app_reference, $booking_status );
			}
			//REFUND TO AGENT IN CASE OF B2B FIXXX
			$GLOBALS ['CI']->hotel_model->save_cancellation_data ( $app_reference, $ChangeRequestId, $ChangeRequestStatus, $status_description, $TraceId, $API_RefundedAmount, $API_CancellationCharge, $attr );
			//echo 'hi';exit;
			/*$hotel_booking_details = array(
				'status' => $status,
				'cancellation_reference' => $cancellation_reference,
				'cancellation_amount' => $cncel_amt
			);
			$CI->db->where('booking_reference',$booking_reference);
			$CI->db->update('hotel_booking_details',$hotel_booking_details);
			
			$hotel_booking_itinerary_details = array(
				'status' => $status
			);
			
			$CI->db->where('app_reference',$app_reference);
			$CI->db->update('hotel_booking_itinerary_details',$hotel_booking_itinerary_details);
			
			$CI->db->where('app_reference',$app_reference);
			$CI->db->update('hotel_booking_pax_details',$hotel_booking_itinerary_details);*/
			
		}
		
		// Update Transaction Details
		// $this->domain_management_model->update_transaction_details('hotel', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount'] );
	}
	
//	hotel cancellation request
	function send_change_request_params($parent_pnr) {
		$HotelCancelRQ = '<Request>
						<Source>
							<RequestorID Client="'.$this->api_username1.'" EMailAddress="'.$this->api_username.'" Password="'.$this->password.'"/>
							<RequestorPreferences Country="CH" Currency="CAD" Language="en">
					<RequestMode>SYNCHRONOUS</RequestMode>
							</RequestorPreferences>
						</Source>
						<RequestDetails>
							<CancelBookingRequest>
								<BookingReference ReferenceSource="api">'.$parent_pnr.'</BookingReference>
							</CancelBookingRequest>
						</RequestDetails>
					</Request>';
		$response['request'] = $HotelCancelRQ;
		$response['status'] = SUCCESS_STATUS;
		return $response;
	}
	
	function get_room_type_precheck($a, $c){
	
		if($a == 1 && $c == 0) {	
			$room_type = 'Code="SB"';	
		} else if($a == 2 && $c == 0) {	
			$room_type = 'Code="DB"';	
		} else if($a == 3 && $c == 0) {
			$room_type = 'Code="DB" '; 
		} else if($a == 2 && $c == 1) {
			$room_type = 'Code="DB"';
		} else if($a == 2 && $c == 2) {
			$room_type = 'Code="DB"';
		} else if($a == 3 && $c == 1) {
			$room_type = 'Code="TB"';			
		} else if($a == 1 && $c == 1) {
			$room_type = 'Code="DB"';	
		} else if($a == 1 && $c == 2) {
			$room_type = 'Code="DB"';	
		}  else {	
			$room_type = "";	
		}
		return $room_type;
	}
	
	
	/**
	 * Get Filter Params - fliter_params
	 */
	function format_search_response($hl, $cobj, $sid, $module = 'b2c', $fltr = array()) {
		if(!empty($fltr)){
			$name_filter = explode("-", $fltr['hn_val']);
			//debug($name_filter);exit;
			//echo $name_filter[count($name_filter)-1];exit;
			if($name_filter[count($name_filter)-1] == 'hotel'){
				$fltr['hn_val'] = rtrim($fltr['hn_val'], "-hotel");	
			}
			else{
				$fltr['hn_val'] = rtrim($fltr['hn_val'], "-apthotel");		
			}
			//echo $fltr['hn_val'];exit;
		}
		//debug($fltr); exit('fltr');
		$level_one = true;
		$current_domain = true;
		if ($module == 'b2c') {
			$level_one = false;
			$current_domain = true;
		} else if ($module == 'b2b') {
			$level_one = true;
			$current_domain = true;
		}
		$h_count = 0;
		$HotelResults = array ();
		if (isset ( $fltr ['hl'] ) == true) {
			foreach ( $fltr ['hl'] as $tk => $tv ) {
				$fltr ['hl'] [urldecode ( $tk )] = strtolower ( urldecode ( $tv ) );
			}
		}

		// Creating closures to filter data
		$check_filters = function ($hd) use($fltr) {

			//_acc type
			$any_facility = function ($cstr, $c_list)
			{
				/*debug($cstr);
				debug($c_list);*/
				foreach ($c_list as $k => $v) {
					if (stripos(($cstr), ($v)) > -1) {
						return true;
					}else{
						return false;
						break;
					}
				}
			};

			if (
			(valid_array ( @$fltr ['hl'] ) == false ||
			(valid_array ( @$fltr ['hl'] ) == true && in_array ( strtolower ( $hd ['location'] ), $fltr ['hl'] ))) &&

			(valid_array ( @$fltr ['_sf'] ) == false || (valid_array ( @$fltr ['_sf'] ) == true && in_array ( $hd ['star_rating'], $fltr ['_sf'] ))) &&

			(@$fltr ['min_price'] <= ceil ( $hd ['price']) && (@$fltr ['max_price'] != 0 && @$fltr ['max_price'] >= floor ( $hd ['price']))) &&

			(( string ) $fltr ['dealf'] == 'false' || empty ($hd ['HotelPromotion'] ) == false) &&

			(empty ( $fltr ['hn_val'] ) == true || (empty ( $fltr ['hn_val'] ) == false &&
			stripos ( strtolower ( $hd ['name'] ), (urldecode ( $fltr ['hn_val'] )) ) > - 1)) &&

			(valid_array ( @$fltr ['_fac'] ) == false ||
			(valid_array( @$fltr ['_fac'] ) == true && $any_facility($hd ['facility_cstr'], $fltr ['_fac']))
			) ) {
				return true;
			} else {
				return false;
			}
		};
		//debug($check_filters);
		$hc = 0;
		$frc = 0;
		foreach ( $hl['HotelSearchResult'] as $hr => $hd ) {
			//debug($hd); exit;
			$hc ++;
			// markup
			//debug($hd ['price']);
			$price = $this->update_search_markup_currency ( $hd ['price'], $cobj, $sid, $level_one, $current_domain );
			//debug($price);exit;
			//$hd ['price'] = $price['value'];
			//$hd ['currency'] = $price['currency'];
			//$hd ['price'] = $hd ['price'];
			// filter after initializing default data and adding markup
			if (valid_array ( $fltr ) == true && $check_filters ( $hd ) == false) {
				continue;
				//debug($check_filters);exit;
			}
			$HotelResults [$hr] = $hd;
			$frc ++;
		}
		//debug($HotelResults);exit;

		// SORTING STARTS
		if (isset ( $fltr ['sort_item'] ) == true && empty ( $fltr ['sort_item'] ) == false && isset ( $fltr ['sort_type'] ) == true && empty ( $fltr ['sort_type'] ) == false) {
			$sort_item = array ();

			foreach ( $HotelResults as $key => $row ) {
				if ($fltr ['sort_item'] == 'price') {
					$sort_item [$key] = floatval ( $row ['price']);
				} else if ($fltr ['sort_item'] == 'star') {
					$sort_item [$key] = floatval ( $row ['star_rating'] );
				} else if ($fltr ['sort_item'] == 'name') {
					$sort_item [$key] = trim ( ucfirst($row ['name']) );
				}
			}
			if ($fltr ['sort_type'] == 'asc') {
				$sort_type = SORT_ASC;
			} else if ($fltr ['sort_type'] == 'desc') {
				$sort_type = SORT_DESC;
			}

			//debug($sort_item); exit;
			if (valid_array ( $sort_item ) == true && empty ( $sort_type ) == false) {
				array_multisort ( $sort_item, $sort_type, $HotelResults );
			}
		} // SORTING ENDS

		$hl ['HotelSearchResult'] = $HotelResults;
		$hl ['source_result_count'] = $hc;
		$hl ['filter_result_count'] = $frc;
		//debug($hl);exit;
		return $hl;
	}
	
	/**
	 * Markup for search result
	 *
	 * @param array $price_summary
	 * @param object $currency_obj
	 * @param number $search_id
	 */
	function update_search_markup_currency(& $price_summary, & $currency_obj, $search_id, $level_one_markup = false, $current_domain_markup = true) {
		$search_data = $this->search_data ( $search_id );
		$no_of_nights = $this->master_search_data ['no_of_nights'];
		$no_of_rooms = $this->master_search_data ['room_count'];
		$multiplier = ($no_of_nights * $no_of_rooms);
		return $this->update_markup_currency ( $price_summary, $currency_obj, $multiplier, $level_one_markup, $current_domain_markup );
	}
	
	/**
	 * set Cache list of hotels
	 *
	 * @param number $search_id
	 */
	private function cache_result_hotel_count($city_id, $hotel_count) {
		$CI = & get_instance ();
		if ($hotel_count > 0 && $city_id > 0) {
			$CI->custom_db->update_record ( 'gta_hb_hotels_city', array (
					'cache_hotelbeds_count' => $hotel_count 
			), array (
					'origin' => $city_id 
			) );
		}
	}
	
	/**
	 * Get Filter Summary of the data list
	 *
	 * @param array $hl
	 */
	function filter_summary($hl) {
		//debug($hl);die();
		// debug($hl['HotelSearchResult']);exit();
		$h_count = 0;
		$filt ['p'] ['max'] = false;
		$filt ['p'] ['min'] = false;
		$filt ['loc'] = array ();
		$filt ['star'] = array ();
		$filters = array ();
		foreach ( $hl ['HotelSearchResult'] as $hr => $hd ) {

			//debug($hd); exit;
			// filters
			$StarRating = intval ( @$hd ['star_rating'] );
			$HotelLocation = $hd ['location'];
			// $AccomodationType = @$hd['accomodation_cstr'];

			if (isset ( $filt ['star'] [$StarRating] ) == false) {
				$filt ['star'] [$StarRating] ['c'] = 1;
				$filt ['star'] [$StarRating] ['v'] = $StarRating;
			} else {
				$filt ['star'] [$StarRating] ['c'] ++;
			}

			if (($filt ['p'] ['max'] != false && $filt ['p'] ['max'] < $hd ['price']) || $filt ['p'] ['max'] == false) {
				$filt ['p'] ['max'] = round ( $hd ['price'],2);
			}
			/*if (($filt ['p'] ['min'] != false && $filt ['p'] ['min'] > $hd ['price']) || $filt ['p'] ['min'] == false) {
			 $filt ['p'] ['min'] = roundoff_number ( $hd ['price'] ['RoomPrice'] );
			 }*/

			if (($filt ['p'] ['min'] != false && $filt ['p'] ['min'] > $hd ['price']) || $filt ['p'] ['min'] == false) {
				$filt ['p'] ['min'] = round($hd ['price'],2);
			}
			$hloc = ucfirst ( strtolower ( $HotelLocation ) );
			$hloc_key = str_replace(' ', '_', $hloc);
			$hloc_key = preg_replace("/[^A-Za-z0-9]/", '', $hloc_key);
			if (isset ( $filt ['loc'] [$hloc_key] ) == false) {
				$filt ['loc'] [$hloc_key] ['c'] = 1;
				$filt ['loc'] [$hloc_key] ['v'] = $hloc;
			} else {
				$filt ['loc'] [$hloc_key] ['c'] ++;
			}

			$a_type =  'Hotel';
			if (isset ( $filt ['a_type'] [$a_type] ) == false) {
				$filt ['a_type'] [$a_type] ['c'] = 1;
				$filt ['a_type'] [$a_type] ['v'] = $a_type;
			} else {
				$filt ['a_type'] [$a_type] ['c'] ++;
			}
			
			// debug($hd['facility']); //exit();
			// facilities
			/*if (array_key_exists('facility', $hd) == true) {

				$CI = & get_instance();					
				$query = " SELECT * FROM gta_Facilities WHERE type IN ('hotel','room')";
				$data = $CI->db->query($query)->result_array();
				$resp = array();
				if (valid_array($data) == true) {
					foreach ($data as $k => $v) {
						$resp[$v['facility']] = array(
							'code' => $v['code'],
							'facility' => $v['facility']
						);
					}
				}
				
				$hd['facility'] = explode(',', $hd['facility']);
				
				foreach ($hd['facility'] as $fk => $fv) {
					if (isset($filt['facility'][$fv]) == false) {
						$filt ['facility'] [$fv] ['c'] = 1;
						$filt ['facility'] [$fv] ['v'] = $fv;
						$filt ['facility'] [$fv] ['icon'] = (array_key_exists($fv, $resp)==true) ? $resp[$fv]['code'] : '' ;
						$filt ['facility'] [$fv] ['cstr'] = $fv;
					} else {
						$filt ['facility'] [$fv] ['c'] ++;
					}
				}
			}*/

			
			if (empty($hd['facility']) == false) {
				foreach ($hd['facility'] as $fk => $fv) {
					if (isset($filt['facility'][$fv['icon_class']]) == false) {
						$filt ['facility'] [$fv['icon_class']] ['c'] = 1;
						$filt ['facility'] [$fv['icon_class']] ['v'] = $fv['name'];
						$filt ['facility'] [$fv['icon_class']] ['icon'] = $fv['icon_class'];
						$filt ['facility'] [$fv['icon_class']] ['cstr'] = '';
						$filt ['facility'] [$fv['icon_class']] ['cstr_inf'] = str_replace(' ', '_', $fv['icon_class']);
						//$filt ['facility'] [$fv['icon']] ['cstr_inf'] = str_replace(' ', '_', $fv['icon']);
					} else {
						$filt ['facility'] [$fv['icon_class']] ['c'] ++;
					}
				}
			}
			$filters ['data'] = $filt;
			$h_count ++;
		}

		ksort ( $filters ['data'] ['loc'] );
		$filters ['hotel_count'] = $h_count;
		 //debug($filters); exit();
		return $filters;
	}
	
	/**
	 * Break data into pages
	 *
	 * @param
	 *        	$data
	 * @param
	 *        	$offset
	 * @param
	 *        	$limit
	 */
	function get_page_data($hl, $offset, $limit) {
		$hl ['HotelSearchResult'] = array_slice ( $hl ['HotelSearchResult'], $offset, $limit );

		return $hl;
	}
	
	
	/*format hotels list data*/
	private function get_formatted_hotel($hl, $search_id,$module='b2c',$booking_source) {
		// debug($hl); exit();

		$CI = & get_instance();
		$hl_tmp = force_multple_data_format($hl);
		$hotel_codes = array();
		$search_data = $this->search_data ( $search_id );
		$no_of_nights = $search_data ['data'] ['no_of_nights'];
		if(COUNT($hl_tmp) > 0) {
			$city_code = @$hl_tmp[0]['City']['@attributes']['Code'];
			foreach ($hl_tmp as $v) {
				$hotel_codes[] = "'".@$v['Item']['@attributes']['Code']."'";
			}
			/*store hotel list in temperory table*/
			$hotel_list['hotel_code'] = $hotel_codes;
			$codes = json_encode($hotel_list);
			
			if(isset($codes) && !empty($codes)) {
				$inser_arr = array(
					'hotel_list' => $codes
				);
				$CI->db->where('search_id',$search_id);
				$query = $CI->db->get('hb_hotel_list_temp');
				if ($query->num_rows() > 0){
					$CI->db->where('search_id', $search_id);
					$CI->db->update('hb_hotel_list_temp', $inser_arr);
				}
				else{
					$inser_arr['search_id'] = $search_id;
					$CI->db->insert('hb_hotel_list_temp',$inser_arr);
				}
			}
			// debug($hl_tmp); exit();
			//Currency Preference
			$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
			//static hotel content
			$cache_list = $this->get_hotel_info($hotel_codes,$city_code);
			//debug($cache_list); exit("new one");
			
			/* build hotels array with required fields */
			$hotel_array = array ();
			// echo count($hl_tmp);
			foreach ( $hl_tmp as $key => $hotel_detail ) {		
				
				if(array_key_exists(0, $hotel_detail['RoomCategories']['RoomCategory']) == true)
				{
					$currency = @$hotel_detail['RoomCategories']['RoomCategory'][0]['ItemPrice']['@attributes']['Currency'];
				}else
				{
					$currency = @$hotel_detail['RoomCategories']['RoomCategory']['ItemPrice']['@attributes']['Currency'];
				}
				
				
				$currency_obj->getConversionRate(false,$currency,get_application_currency_preference());
				$hkey = $hotel_detail ['Item']['@attributes']['Code'];
				if (isset($cache_list['info'][$hkey]) == true) {

				$hotel_array [$hkey] ['hotel_code'] = $hkey;
				$hotel_array [$hkey] ['booking_source'] = $booking_source;
				$hotel_array [$hkey] ['name'] = $hotel_detail ['Item']['@cdata'];
				$hotel_array [$hkey] ['star_rating'] = $hotel_detail['StarRating'];
				$hotel_array [$hkey] ['star_rating_text'] = $hotel_detail['StarRating'].' Star';

				$hotel_array [$hkey] ['accomodation_type'] = 'Hotel';
				$hotel_array [$hkey] ['accomodation_cstr'] ='Hotel';
				$hotel_array [$hkey] ['location'] =  $hotel_detail['City']['@cdata'];
				$cache_facility = isset($cache_list['facility'][$hkey]) ? $cache_list['facility'][$hkey] : false;
				/*$cache_facilities = isset($cache_list['facilities'][$hkey]) ? $cache_list['facilities'][$hkey] : false;*/
				
				//NEED TO ADD MARKUP FIX ME
				if(isset($hotel_detail['RoomCategories']['RoomCategory']) && valid_array($hotel_detail['RoomCategories']['RoomCategory'])) {
					$room_category = force_multple_data_format($hotel_detail['RoomCategories']['RoomCategory']);
					$price = $room_category[0]['ItemPrice']['@value'];
					if ($module == 'b2c') {
						//Convert to default currency and add mark up
						$markup_total_fare = $currency_obj->get_currency ( $price, true, false, true, 1 ); // (ON Total PRICE ONLY)
					} else {
						// B2B Calculation 
						$markup_total_fare = $currency_obj->get_currency ( $price, true, true, true, 1 ); // (ON Total PRICE ONLY)
//						debug($hotel_detail['minRate']);debug($currency);debug($markup_total_fare);exit;
					}
					$hotel_array [$hkey] ['xml_price'] = $price;
					$hotel_array [$hkey] ['currency_price'] = $markup_total_fare['default_value'];
					//$hotel_array [$hkey] ['price'] = sprintf("%.2f", ceil($hotel_array [$hkey] ['price']));
					$hotel_array [$hkey] ['price'] = round($markup_total_fare['default_value'] / $no_of_nights,2);
					$hotel_array [$hkey] ['currency'] = $markup_total_fare['default_currency'];
				}
				
				
					// facilities
					$hotel_array [$hkey]['facility_cstr'] = '';
					$fi = 0;
					$f_limit = 0;
					if ($cache_facility) {
						/*if ($this->aminity_count != -1) {
							$f_limit = $this->aminity_count;
						} else {
							$f_limit = count($cache_facility);
						}*/
						foreach ($cache_facility as $fk => $fv) {
							$hotel_array [$hkey]['facility_cstr'] .= '_'.$fv['icon_class'];
							//$hotel_array [$hkey]['facility_cstr'] .= '_'.str_replace(' ', '_', $fv['v']);
						}
					}

					$cache_info = $cache_list['info'][$hkey];
					$facilities = getGtaFacilities($cache_info['facilities']);
					//debug($facilities); exit;
					$hotel_array [$hkey] ['facility'] = (is_array($cache_facility) ? $cache_facility : false);
					$hotel_array [$hkey] ['latitude'] = $cache_list['info'][$hkey]['latitude'];
					$hotel_array [$hkey] ['longitude'] = $cache_list['info'][$hkey]['longitude'];
					$hotel_array [$hkey] ['address'] = $cache_list['info'][$hkey]['address'];
					$hotel_array [$hkey] ['facilities'] = $cache_info['facilities'];
					$hotel_array [$hkey] ['CityCode'] = $cache_info['CityCode'];
					if(isset($cache_list['info'][$hkey]['img'])) {
						$img_arr = explode(',',$cache_list['info'][$hkey]['img']);
						$hotel_array [$hkey] ['img'] = @$img_arr[0];
					}
					$hotel_array [$hkey] ['postal'] = '';
				}
			}
		}
		 //debug($hotel_array); exit('hotel_array');
		return $hotel_array;
	}
	
	/**
	 * return all the hotel information
	 * @param mixed $hotel_code number of array
	 */
	function get_hotel_info($hotel_code,$city_code)
	{
		$h_cond_val = '';
		if (empty($hotel_code) == false) {
			$static_hotel_info = $this->get_static_hotel_info($hotel_code,$city_code);
			//$static_hotel_facility_info = $this->get_static_hotel_facility_info($hotel_code,'','icon');

			//return array_merge($static_hotel_info, $static_hotel_facility_info);
			return $static_hotel_info;
		}
	}
	
	/**
	 * static data to be read for hotels from db
	 * @param string $h_cond
	 */
	function get_static_hotel_info($hotel_code, $city_code,$cols='')
	{
		$CI = & get_instance();
		if (is_array($hotel_code)) {
			$h_cond = 'HotelCode IN ('.implode(',', $hotel_code).')';
		} else {
			$h_cond = 'HotelCode = '.$CI->db->escape($hotel_code);
		}
		$cols = '*';
//		if ($cols == '') {
//			$cols = 'hi.image_path AS img, h.hotel_code AS hc, h.category_code AS cc, h.address AS address, h.postal_code AS postal,
//			h.chain_code as chain, h.hotel_email as email, h.website as website, h.description as description,
//			hcl.description hotel_chain, cl.accommodationType as acc_type, cl.description as cat_desc';
//		}
		if($city_code) {
			$h_cond .= " AND CityCode ='".$city_code . "'";
		}
	$query = ' SELECT '.$cols.'  FROM gta_Hotels WHERE '.$h_cond.' GROUP BY id';
		$data = $CI->db->query($query)->result_array();

		$query_fac = " SELECT * FROM gta_Facilities WHERE type IN ('hotel','room')";
		$data_fac = $CI->db->query($query_fac)->result_array();

		$resp = array();
		if (valid_array($data_fac) == true) {
			foreach ($data_fac as $key_fac => $v_fac) {
				$resp[$v_fac['facility']] = array(
					'code' => $v_fac['code'],
					'facility' => $v_fac['facility']
				);
			}
		}
		//debug($data);exit();
		//group data with hotel code index
		$resp = array();
		$facilities = array();
		if (valid_array($data) == true) {
			foreach ($data as $k => $v) {

				/*if (array_key_exists('HotelFacilityName', $v) == true) {
					
					$hd['facility'] = explode(',', $v['HotelFacilityName']);
					foreach ($hd['facility'] as $fk => $fv) {
							$resp ['facility'][$v['HotelCode']][$fk]['c'] = 1;
							$resp ['facility'][$v['HotelCode']][$fk]['v'] = $fv;
							$resp ['facility'][$v['HotelCode']][$fk]['icon'] = (array_key_exists($fv, $resp)==true) ? $resp[$fv]['code'] : '' ;
							$resp ['facility'][$v['HotelCode']][$fk]['cstr'] = $fv;
					}
				}*/

				if (array_key_exists('RoomFacilityName', $v) == true) {
					
					$hd['facility'] = explode(',', $v['HotelFacilityName']);
					$facilities = getGtaFacilities($v['RoomFacilityName']);
					/*foreach ($hd['facility'] as $fk => $fv) {
							$resp ['facility'][$v['HotelCode']][$fk]['c'] = 1;
							$resp ['facility'][$v['HotelCode']][$fk]['v'] = $fv;
							$resp ['facility'][$v['HotelCode']][$fk]['icon'] = (array_key_exists($fv, $resp)==true) ? $resp[$fv]['code'] : '' ;
							$resp ['facility'][$v['HotelCode']][$fk]['cstr'] = $fv;
					}*/

					foreach ($facilities as $fk => $fv) {
							$resp ['facility'][$v['HotelCode']][$fk]['icon_class'] = $fv['icon_class'];
							$resp ['facility'][$v['HotelCode']][$fk]['additional_cost'] = 0;
							$resp ['facility'][$v['HotelCode']][$fk]['name'] = $fv['name'];
							$resp ['facility'][$v['HotelCode']][$fk]['cstr'] = $fv['cstr'];
					}
				}

				$resp['info'][$v['HotelCode']] = array(
					'address' => $v['Address'],
					'CityCode' => $v['CityCode'],
					'postal' => $v['Pin'],
					'img' => $v['ImageLinkImage'],
					'email' => $v['EmailAddress'],
					'website' => $v['WebSite'],
					'description'=>$v['ReportInfo'],
					'contact' => $v['Telephone'],
					'latitude' => $v['Latitude'],
					'longitude' => $v['Longitude'],
					'facilities' => $v['RoomFacilityName']
				);
			}
		}
		return $resp;
	}
	
	//validate search result
	function valid_search_result($hotel_list) {
		if(isset($hotel_list['Response']['ResponseDetails']['SearchHotelPriceResponse']['HotelDetails']['Hotel']) && valid_array($hotel_list['Response']['ResponseDetails']['SearchHotelPriceResponse']['HotelDetails']['Hotel'])) {
			return true;
		}
		return false;
	}
	
	function process_details($requestData) {
	    $cs = curl_init();  
	    curl_setopt($cs, CURLOPT_URL, $this->service_url);
	    curl_setopt($cs, CURLOPT_HEADER, false);
	    curl_setopt($cs, CURLOPT_RETURNTRANSFER, true);
	  	curl_setopt($cs, CURLOPT_POST, 1);
		curl_setopt($cs, CURLOPT_POSTFIELDS, $requestData);
	    curl_setopt($cs, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($cs, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($cs, CURLOPT_TIMEOUT, 180);
        $httpHeader2 = array(
      		'Content-type: text/xml'
        );
        curl_setopt($cs, CURLOPT_HTTPHEADER, $httpHeader2); 
        $response = curl_exec($cs);
        $error = curl_error($cs);
        curl_close($cs);
        return $response;
	}
	
	/**
	 * update markup currency and return summary
	 * $attr needed to calculate number of nights markup when its plus based markup
	 */
	function update_markup_currency(& $price_summary, & $currency_obj, $no_of_nights = 1, $level_one_markup = false, $current_domain_markup = true) {
		$tax_service_sum = 0;
		$markup_summary = array ();
		$temp_price = $currency_obj->get_currency ( $price_summary, true, $level_one_markup, $current_domain_markup, $no_of_nights );
		return array('value' => $temp_price ['default_value'], 'currency' => $temp_price['default_currency']);
	}
	
	/**
	 * calculate and return total price details
	 */
	function total_price($price_summary) {
		return ($price_summary ['OfferedPriceRoundedOff']);
	}
	

	function process_booking($book_id, $booking_params, $customer_details=array()){
		return true;
	}
	

	function booking_url($book_id, $booking_params=array()){
		return true;
	}
}
