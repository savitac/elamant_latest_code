<?php

/**
 * Provab Common Functionality For API Class
 *
 *
 * @package	Provab
 * @subpackage	provab
 * @category	Libraries
 * @author		Arjun J<arjun.provab@gmail.com>
 * @link		http://www.provab.com
 */
abstract class Common_Api_Grind {
	protected $DomainKey;
	function __construct()
	{
		$this->DomainKey = get_domain_key();
	}
	
	/**
	 *  Arjun J Gowda
	 * convert search params to format required by booking source
	 * @param number $search_id unique id which identifies search details
	 */
	abstract function search_data($search_id);

	/**
	 * Arjun J Gowda
	 * update markup currency and return summary
	 *
	 * @param array	 $price_summary
	 * @param object $currency_obj
	 */
	abstract function update_markup_currency(& $price_summary, & $currency_obj);

	/**
	 * Arjun J Gowda
	 * calculate and return total price details
	 * @param array $price_summary - price which has to be added
	 */
	abstract function total_price($price_summary);

	/**
	 * Arjun J Gowda
	 *Process Booking
	 * @param array $booking_params
	 */
	abstract function process_booking($book_id, $booking_params, $customer_details=array());
	
	/**
	 * return booking url to be used in the application for all the modules
	 */
	abstract function booking_url($search_id, $booking_params=array());

	/**
	 * Returns price default price object
	 */
	function get_price_object() {
		$price_obj = array (
				"api_currency" => false,
				"api_total_display_fare" => 0,
				"api_fare_val" => 0,
				"total_breakup" => array (
						"api_total_tax" => 0,
						"api_total_fare" => 0 
				),
				"price_breakup" => array (
						'basic_fare' => 0,
						'fuel_charge' => 0,
						'handling_charge' => 0,
						'service_tax' => 0,
						'agent_commission' => 0,
						'agent_tds_on_commision' => 0,
						'admin_commission' => 0,
						'admin_tds_on_commission' => 0,
						'agent_markup' => 0,
						'admin_markup' => 0,
						'dist_markup' => 0,
						'app_user_buying_price' => 0 
				) 
		);
		return $price_obj;
	}
}