<?php
if(! defined ('BASEPATH') ) exit ("No Direct Access Allowed!");
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Ccavenue
{
    static $merchant_id;
    static $access_code; //salt
    static $working_key; //key 
    
    public function __construct() {
            $this->CI = &get_instance (); 
            $this->CI->load->helper('ccavenue_pgi_helper');
            $this->active_payment_system = $this->CI->config->item('active_payment_system');
            $this->payment_gateway_currency = $this->CI->config->item('payment_gateway_currency');
        }
        public function initialize($data)
        {
        #debug($data);exit();
        static $merchant_id;
        static $access_code;
        static $working_key;

      if ($this->active_payment_system == 'test') {
        $this->merchant_id = "113039";
        $this->access_code = $data['access_key'];
        $this->working_key = $data['working_key']; 
        $this->url = 'https://test.ccavenue.com';
        } else {
      //live 
        $this->merchant_id = "113039";
        $this->access_code = $data['access_key'];
        $this->working_key = $data['working_key'];
        $this->url = 'https://secure.ccavenue.com';
        }
        $this->book_id = $data['txnid']; 
        $this->pgi_amount = $data['pgi_amount'];
        $this->firstname = $data['firstname'];
        $this->email = $data['email'];
        $this->phone = $data['phone'];
        $this->productinfo = $data['productinfo'];

      }
        function process_payment(){
                  
            $surl = base_url().'index.php/payment_gateway/success';
            $furl = base_url(). 'index.php/payment_gateway/cancel';
            $url =  $PAYU_BASE_URL = "https://secure.ccavenue.com";
            $hash_string = self::$working_key."|".$this->book_id."|".$this->pgi_amount."|".$this->productinfo."|".$this->firstname."|".$this->email."| | | | | | | | | |".self::$access_code;
            $hash = strtolower(hash('sha512', $hash_string));
            $post_data=array(); 
            $post_data['txnid'] = $this->book_id;
                $post_data['merchant_id'] =$this->merchant_id;
                $post_data['productinfo'] = $this->productinfo;
            $post_data['amount'] = $this->pgi_amount;
                $post_data['currency'] = $this->payment_gateway_currency;
                $post_data['redirect_url'] = base_url().'index.php/payment_gateway/response';
                $post_data['cancel_url'] = base_url().'index.php/payment_gateway/response';
                $post_data['language'] = "EN";
            $post_data['firstname'] = $this->firstname;
            $post_data['email'] = $this->email;
            $post_data['phone'] = $this->phone; 
            $post_data['surl'] = $surl;
            $post_data['furl'] = $furl;   
            $post_data['access_code'] = $this->access_code;
            $post_data['working_key'] = $this->working_key;
               // print_r($post_data);exit;
            return $post_data;
        }  
}     
