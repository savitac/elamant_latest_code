<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
/**
 *
 * @package Provab
 * @subpackage ccavenue
 * @author Pankaj Kumar <pankajprovab212@gmail.com>
 * @version V1
 */
class Ccavenue {
	 

	static $working_key;
	static $access_code;
	static $url;
	static $merchant_id;

	var $active_payment_system;

	var $book_id = '';
	var $book_origin = '';
	var $pgi_amount = '';
	var $name = '';
	var $email = '';
	var $phone = '';
	var $productinfo = '';
	public function __construct() {
		$this->CI = &get_instance ();
		$this->CI->load->helper('custom/ccavenue_pgi_helper');
		$this->active_payment_system = $this->CI->config->item('active_payment_system');
	}

	function initialize($data)
	{  
		self::$merchant_id = MERCHANT_ID; 
		if ($this->active_payment_system == 'test') {
			//test
			self::$working_key = WORKING_KEY; 
			self::$access_code = ACCESS_CODE;  
			self::$url = 'https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction';  
		} else { 
			//live
			self::$working_key = WORKING_KEY; 
			self::$access_code = ACCESS_CODE; 
			self::$url = 'https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
		}
		$this->book_id = $data['txnid'];
		$this->pgi_amount = $data['pgi_amount'];
		$this->firstname = $data['firstname'];
		$this->email = $data['email'];
		$this->phone = $data['phone'];
		$this->productinfo = $data['productinfo'];
	}
	function process_payment(){
		$surl = base_url().'index.php/payment_gateway/response';
		$furl = base_url(). 'index.php/payment_gateway/cancel';
		
		//Post_data to send data to the form (view) page
		$post_data=array();
		$post_data['txnid'] = $this->book_id;
		$post_data['amount'] = $this->pgi_amount;
		$post_data['firstname'] = $this->firstname;
		$post_data['email'] = $this->email;
		$post_data['phone'] = $this->phone;
		$post_data['productinfo'] = $this->productinfo;
		$post_data['surl'] = $surl;
		$post_data['furl'] = $furl;
		$post_data['service_provider'] = 'ccavenue';
		$post_data['merchant_id'] = self::$merchant_id;
		$post_data['pay_target_url'] = self::$url;
		$post_data['access_code'] = self::$access_code;
		$post_data['working_key'] = self::$working_key;
		
		return $post_data; 
	}
}
  