<?php
require_once 'master_currency.php';
/**
 * Provab Currency Class
 *
 * Handle all the currency conversion in the application
 *
 * @package	Provab
 * @subpackage	provab
 * @category	Libraries
 * @author		Arjun J<arjun.provab@gmail.com>
 * @link		http://www.provab.com
 */
class Currency extends Master_currency {


	public function __construct($params=array())
	{
		//call parent
		parent::__construct($params);
	}
	/**
	 * Set Commission
	 */
	function set_commission($override=true, $module)
	{	
		$CI = &get_instance();
		if ($override === true) {
			
			$this->commission_fees_row = $CI->private_management_model->get_commission($this->module_name, $module);
			//Convert if plus to preferred curr
			if ($this->commission_fees_row['admin_commission_list']['value'] > 0 && $this->commission_fees_row['admin_commission_list']['value_type'] == 'plus') {
				//check preferred currency and markup currency
				$from_cur = $this->commission_fees_row['admin_commission_list']['def_currency'];
				$to_cur = $this->from_currency;
				$this->commission_fees_row['admin_commission_list']['commission_currency'] = $to_cur;
				$this->commission_fees_row['admin_commission_list']['value'] = $this->conversion_cache[$from_cur.$to_cur];
			}
		}
	}

	/**
	 * Get Commission
	 */
	public function get_commission($module)
	{
		$this->set_commission(true, $module);
		return $this->commission_fees_row;
	}
}
