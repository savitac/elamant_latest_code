<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="" type="image/x-icon">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
<meta name="description" content="">
<meta name="author" content="">
<title>
<?php if(PAGE_TITLE != '') echo PAGE_TITLE." | "; ?>
Reset Password</title>
<?php $this->load->view('core/load_css'); ?>
</head>
<style>
.popuperror {
    background: #666666 none repeat scroll 0 0;
    color: #ffffff;
    display: block;
    font-size: 13px;
    margin: 0 auto;
    overflow: hidden;
    padding: 10px;
    width: 332px;
}

</style>
<?php echo $this->load->view('core/load_css'); ?>
<link href="<?php echo ASSETS;?>assets/css/backslider.css" rel="stylesheet" />
<?php $this->load->view('core/header'); ?>
<body id="top" class="thebg">

<!-- blocks container -->
<section class="pagesheading ">
  <div class="container">
    <div class="row">
      <div class="popuperror" style="display:none;"></div>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <h1>Reset Your Password</h1>
      </div>
    </div>
  </div>
  <!-- blocks container --> 
</section>

<!--Explore More-->
<section class="pages">
  <div class="agent_login_wrap top80">
      <div class="container">
       <div class="loginsreset">
            <div class="agenthed">Reset Password</div>
            <div class="signdiv">  
              <div class="insigndiv">
                
              
                <form id="resetpwd" name="resetpwd" method="post"  action="<?php echo ASSETS;?>account/resetpwd">
                  <div class="ritpul">
                    <div class="rowput"> 
                      <input type="hidden" name="email" value="<?php echo $email;?>">
                      <input type="hidden" name="account_number" value="<?php echo $account_number;?>">
                      <input type="hidden" name="user_details_id" value="<?php echo $user_details_id;?>">
                     <input class="form-control logpadding" type="password" name="newpassword" id="newpassword" placeholder="Enter New Password" minlength="8" maxlength="30" required/>
                    </div>
                    <div class="rowput"> 
                      <input class="form-control logpadding" type="password"  id="confirm_password" name="confirm_password" placeholder="Re-Enter New Password" minlength="8" maxlength="30" required/>
                    </div>
                   
                    <div class="clearfix"></div>
                    <input type="submit" class="submitlogin" id="btnSubmit" value="Reset Password" >
                   
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
   
</div>
  <div><?php echo $msg;?></div>
  <!--Explore More-->  
</section>
<!-- /Navigation -->
<div class="clearfix"></div>

<?php echo $this->load->view('core/footer'); ?>
<?php echo $this->load->view('core/bottom_footer'); ?>
<script type='text/javascript' src="<?php echo ASSETS; ?>assets/js/custom/jquery.validate.js"></script>

<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function () {
            var password = $("#newpassword").val();
            var confirmPassword = $("#confirm_password").val();
            if (password != confirmPassword) {
                alert("Passwords do not match.");
                return false;
            }
            return true;
        });
    });
</script>

<!-- Page Content -->
<?php /*} else {?>
	<div class="centerfix"><?php echo $msg; ?></div>
<?php }*/ ?>
<!-- </div></div></div> -->
<?php $this->load->view('core/footer'); ?>

<!-- /.container -->
<script>

$("#showLeft").click(function(){
			 $("#cbp-spmenu-s1").toggleClass('cbp-spmenu-open')
			});


		$('.navbak').click(function(){
			$('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
		});

/*submitHandler: function() { 
    
    var action = $("#resetpwd").attr('action');
    $.ajax({
      //alert();
      type: "POST",
      url: action,
      data: $("#resetpwd").serialize(),
      dataType: "json",
      success: function(data){
        
        if(data.status == 1){
          alert(data.msg);
          window.location.href = "<?php site_url(); ?>";
         
        }else{
           alert(data.msg);
           
        }
      }
    });
    return false; 
  }*/
</script>
</body>

</html>
