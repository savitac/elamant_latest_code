<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(session_status() == PHP_SESSION_NONE){ session_start(); } //error_reporting(E_ALL);
ob_start();
class Booking extends CI_Controller {	

	public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Booking_Model');
		$this->load->model('Api_Model');
        $this->load->model('Usertype_Model');
        $this->load->model('Email_Model');
		$this->load->library('form_validation');
		
			$this->lang->load('english','Dynamic_Languages');
		
		$this->TravelLights = $this->lang->line('TravelLights');
		$this->checkAdminLogin();
		define('CANCEL_BUFFER',7);
	}
	
	function checkAdminLogin() {
		if($this->session->userdata('provabAdminLoggedIn') == "") {
	        redirect('login','refresh');
        }else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->session->userdata('provabAdminLoggedIn') == "Lock_Screen") {
			redirect('login/lockScreen','refresh');
		}else if($this->session->userdata('provabAdminLoggedIn') == "Logged_In") {
		}else if($this->ession->userdata('provabAdminLoggedIn') == "Lock_Screen") {
		 	redirect('login/lockScreen','refresh');
		}
    }
 
    function hotelOrders() {
    	$data = $this->General_Model->getHomePageSettings();
       
        //$data['orders'] = $this->Booking_Model->get_booking_details();
        $data['orders'] = $this->Booking_Model->get_booking_hotel_details();
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/userBookings', $data);
    //}
}
	function flightOrders() 
		{
			$data = $this->General_Model->getHomePageSettings();

			$data['orders'] = $this->Booking_Model->get_booking_flight_details();
			echo '<pre>'; print_r($data['orders']); exit(" SERVER");
			$data['user_type'] = $this->Usertype_Model->get_user_type_list();
			$data['user_id'] = '0';

			$this->load->view('bookings/flight_reports', $data);
			//}
		}

 function transferOrders() {
    	$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        
        $data['orders'] = $this->Booking_Model->get_transfer_order_details();
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/transferBookings', $data);
    //}
}



	public function cancel($module,$pnr_no,$parent_pnr_no){
		$pnr_no = base64_decode(base64_decode($pnr_no));
		$parent_pnr_no = base64_decode(base64_decode($parent_pnr_no));
        if($module == 'Hotels'){
            $count = $this->Booking_Model->getBookingPnr($pnr_no)->num_rows();

            if($count == 1) {
                $b_data = $this->Booking_Model->getBookingPnr($pnr_no)->row(); 
                $api_details = $this->Api_Model->getApiList($b_data->api_id);            
                $reference_id = $b_data->referal_id;                 
                $getBookingHotelData = $this->Booking_Model->getBookingHotelData($reference_id)->row();
                $getBookingHotelData_v1 = $this->Booking_Model->getBookingbyPnr_v1($pnr_no,$module)->row();
                //~ echo "data: <pre>";print_r($getBookingHotelData_v1);exit;
                $api = $api_details[0]->api_name;
                $CancelTime = date('Y-m-d H:i:s');
                $book_usertype = $b_data->user_type_id;

                if($b_data->booking_status == 'CONFIRMED' || $b_data->booking_status == 'PENDING'){ 
					
					//~ Cancellation Buffer
					
					$cancel_buffer = $this->General_Model->getCancellationBuffer(1,$b_data->api_id)->row();

					if(empty($cancel_buffer) && $cancel_buffer == '') {
						$buffer_days = 0;
					}
					else {
						$buffer_days = $cancel_buffer->cancel_days;
					}
					
					//~ End
					
                    if($api == "GTA"){
						$api_cancel_pnr = $b_data->api_confirmation_no;
						$this->load->helper('gta_helper');
                        $CancelReq_Res = HotelBookingCancel($api_cancel_pnr);
                         //  echo 'CancelReq_Res : <pre/>';print_r($CancelReq_Res);
                        $CancelReq = $CancelReq_Res['HotelBookingCancelRQ'];
                        $CancelRes = $CancelReq_Res['HotelBookingCancelRS'];
                        
                        if($CancelRes != '') {
							$HotelBookingCancelRS = new SimpleXMLElement($CancelRes);
							$HotelBookingCancelRS = $HotelBookingCancelRS->children()->ResponseDetails->BookingResponse;                
							if(isset($HotelBookingCancelRS->BookingStatus)) {
								$BookingStatus = (string)$HotelBookingCancelRS->BookingStatus;
								$BookingStatus = trim($BookingStatus);  

								if($BookingStatus == "Cancelled"){  //check if api status is sent as cancelled                            
									$update_booking = array(
														'api_status' => 'Cancelled',
														'booking_status' => 'CANCELLED',
														'cancellation_status' => 'Cancelled',
														'cancel_request_time' => $CancelTime,
														'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
														'cancel_response' => $CancelReq_Res['HotelBookingCancelRS']
													);
									//~ echo '<pre/>';print_r($update_booking);exit;
									$cancellation_data = $getBookingHotelData->cancellation_data;
									$profit_percent = $getBookingHotelData->profit_percent;  
									
									                                                                                                 
									if(!empty($cancellation_data)){                                 
										$now = strtotime(date('Y-m-d H:i'));                                    
										$cancellation_data = json_decode($cancellation_data);
										foreach ($cancellation_data as $key => $cancellation) {
											if($buffer_days == 0) {
												$from = strtotime($cancellation->ToDate);
												$to = strtotime($cancellation->FromDate);
											}
											else {
												$from = strtotime($cancellation->ToDate." -".$buffer_days." days");
												$to = strtotime($cancellation->FromDate." -".$buffer_days." days");
											}
											$amount = $cancellation->ChargeAmount;
											$type = $cancellation->Type;
											if ($from == $to) {
												if ($now >= $from) {
													if($type == '1'){
														$charge[] = $amount;
													} else {
														$charge[] = 0;
													}
												}
												else {
													$charge[] = 0;
												}
											} else {
												if ($now >= $from && $now <= $to) {
													if($type == '1'){
														$charge[] = $amount;
													}
												} else {
													$charge[] = 0;
												}
											}                             
										}
										$payment_details = json_decode($getBookingHotelData->TravelerDetails);
										$TotalPrice_API = $payment_details->total;                       
										$Charge_API = array_sum($charge);
										$Refund = $TotalPrice_API - $Charge_API;
										
										$Refund = ($getBookingHotelData_v1->transaction_amount - $getBookingHotelData_v1->PG_Charge) - $Charge_API;
										
										$update_booking = array(
															'api_status' => 'Cancelled',
															'booking_status' => 'CANCELLED',
															'cancellation_status' => 'Cancelled',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
															'cancel_response' => $CancelReq_Res['HotelBookingCancelRS'],
															'cancellation_amount' => $Charge_API,
															'refund_amount' => $Refund,
															'refund_status' => 0
														);
									}
									//~ echo '<pre/>';print_r($update_booking);exit;
									$this->Booking_Model->Update_Booking_Global($b_data->booking_no, $update_booking);
									//~ Commented as of now 
									//~ $this->cancel_mail_voucher($pnr_no);
									
									$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr_no)->result();

									foreach($data['pnr_nos'] as $pnr_nos):
										$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
										$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
										$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
										//echo '<pre>sanjay'; print_r($data['user_details'][0]->user_email); exit();
										$email['message'] = $this->load->view('vouchers/hotel_mailVoucher', $data, TRUE);
										$email['to'] = $data['user_details'][0]->user_email;
										//echo 'sanjay'; print_r($email['to']); exit();
						                $email['email_access'] = $this->Email_Model->get_email_acess();
						                
					            		$Response = $this->Email_Model->sendmail_hotelVoucher($email);
										redirect('booking/hotelOrders', 'refresh');
									endforeach;
									
									$response = array('status' => 1);
									//~ echo json_encode($response);
									redirect('booking/hotelOrders','refresh');
								} 
								else {
									$update_booking = array(
															'cancellation_status' => 'Cancellation Pending',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
															'cancel_response' => $CancelReq_Res['HotelBookingCancelRS']
														);
									$this->Booking_Model->Update_Booking_Global($pnr_no, $update_booking);
									$xml_log = array(
													'api_id' => 'GTA',
													'xml_type' => 'Hotel - Cancel Request',
													'xml_request' => $CancelReq_Res['HotelBookingCancelRQ'],
													'xml_response' => $CancelReq_Res['HotelBookingCancelRS'],
													'ip_address' => $this->input->ip_address()
												);
									$this->General_Model->store_logs($xml_log,'SUCCESS');
									$response = array('status' => 0);
									//~ echo json_encode($response);
									redirect('booking/hotelOrders','refresh');
								} 
							} 
							else {
								$update_booking = array(
														'cancellation_status' => 'Cancellation Pending',
														'cancel_request_time' => $CancelTime,
														'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
														'cancel_response' => $CancelReq_Res['HotelBookingCancelRS']
													);
								$this->Booking_Model->Update_Booking_Global($b_data->booking_no, $update_booking);
								$xml_log = array(
												'api_id' => 'GTA',
												'xml_type' => 'Hotel - Cancel Request',
												'xml_request' => $CancelReq_Res['HotelBookingCancelRQ'],
												'xml_response' => $CancelReq_Res['HotelBookingCancelRS'],
												'ip_address' => $this->input->ip_address()
											);
								$this->General_Model->store_logs($xml_log,'SUCCESS');
								$response = array('status' => 0);
								//~ echo json_encode($response);
								redirect('booking/hotelOrders','refresh');
							}
						}
						else {
							redirect('booking/hotelOrders','refresh');
						}
                    }
                    if($api == "HOTELBEDS") {
						$api_cancel_pnr = $b_data->api_confirmation_no;
						$this->load->helper('hb_helper');
                        $CancelReq_Res = PurchaseCancelRQ($b_data->api_id, $api_cancel_pnr);
                        
                        $CancelReq = $CancelReq_Res['HotelBookingCancelRQ'];
                        $CancelRes = $CancelReq_Res['HotelBookingCancelRS'];
                        
                        /*
                        
                        $CancelReq = $CancelReq_Res['HotelBookingCancelRQ'] = '<?xml version="1.0" encoding="UTF-8"?>
								<PurchaseCancelRQ xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseCancelRQ.xsd" type="C">                                                                      
									<Language>ENG</Language>				
									<Credentials>
										<User>VIBRANIN26193</User>
										<Password>VIBRANIN26193</Password>
									</Credentials>
									<PurchaseReference>
										<FileNumber>654041</FileNumber>
										<IncomingOffice code="148"/>
									</PurchaseReference>
								</PurchaseCancelRQ>';
                        $CancelRes = $CancelReq_Res['HotelBookingCancelRS'] = '<PurchaseCancelRS xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseCancelRS.xsd" type="C"><AuditData><ProcessTime>2773</ProcessTime><Timestamp>2016-09-28 13:03:16.824</Timestamp><RequestHost>14.141.47.106</RequestHost><ServerName>FORM</ServerName><ServerId>FO</ServerId><SchemaRelease>2005/06</SchemaRelease><HydraCoreRelease>3.11.4.20160619</HydraCoreRelease><HydraEnumerationsRelease>3#v4Pc5fpmQCWzGOiKoLXJbA</HydraEnumerationsRelease><MerlinRelease>0</MerlinRelease></AuditData><Purchase purchaseToken="O3292320094" timeToExpiration="1799962"><Reference><FileNumber>654041</FileNumber><IncomingOffice code="148"></IncomingOffice></Reference><Status>CANCELLED</Status><Agency><Code>26193</Code><Branch>1</Branch></Agency><Language>ENG</Language><CreationDate date="20160920"/><CreationUser>VIBRANIN26193</CreationUser><Holder type="AD"><Age>0</Age><Name>GULSAN</Name><LastName>MAHAKUD</LastName></Holder><AgencyReference>LIYNOS57Z0XD</AgencyReference><ServiceList><Service xsi:type="ServiceHotel" SPUI="148#H#1"><Reference><FileNumber>654041-H1</FileNumber><IncomingOffice code="148"></IncomingOffice></Reference><Status>CANCELLED</Status><ContractList><Contract><Name>FIT-BB</Name><IncomingOffice code="148"></IncomingOffice><CommentList><Comment type="CONTRACT">Extra beds on demand YES (without additional debit notes). Check-in hour 14:00 â€“ 14:00. Wi-fi YES (with additional debit notes) 30 AED Per unit/night. Airport Shuttle. Identification card at arrival. Marriage certificate required for a couple to share room. </Comment></CommentList></Contract></ContractList><Supplier name="HOTELBEDS SPAIN S.L.U." vatNumber="ESB28916765"/><CommentList><Comment type="SERVICE">Test</Comment><Comment type="INCOMING">Test</Comment></CommentList><DateFrom date="20170222"/><DateTo date="20170223"/><Currency code="USD">US Dollar</Currency><TotalAmount>0.000</TotalAmount><AdditionalCostList><AdditionalCost type="AG_COMMISSION"><Price><Amount>0.000</Amount></Price></AdditionalCost><AdditionalCost type="COMMISSION_VAT"><Price><Amount>0.000</Amount></Price></AdditionalCost><AdditionalCost type="COMMISSION_PCT"><Price><Amount>0.000</Amount></Price></AdditionalCost></AdditionalCostList><ModificationPolicyList><ModificationPolicy>Cancellation</ModificationPolicy><ModificationPolicy>Confirmation</ModificationPolicy><ModificationPolicy>Modification</ModificationPolicy></ModificationPolicyList><HotelInfo xsi:type="ProductHotel"><Code>8919</Code><Name>Panorama Bur Dubai</Name><Category type="SIMPLE" code="2EST">2 STARS</Category><Destination type="SIMPLE" code="DXB"><Name>Dubai</Name><ZoneList><Zone type="SIMPLE" code="1">Dubai</Zone></ZoneList></Destination></HotelInfo><AvailableRoom><HotelOccupancy><RoomCount>1</RoomCount><Occupancy><AdultCount>1</AdultCount><ChildCount>0</ChildCount></Occupancy></HotelOccupancy><HotelRoom SHRUI="daEfWyakJtcgXAF4baYzSA==" availCount="1" status="CANCELLED"><Board type="SIMPLE" code="BB-E10">BED AND BREAKFAST</Board><RoomType type="SIMPLE" code="DBL-E10" characteristic="ST">DOUBLE STANDARD</RoomType><Price><Amount>0.000</Amount></Price><HotelRoomExtraInfo><ExtendedData><Name>INFO_ROOM_AGENCY_BOOKING_STATUS</Name><Value>O</Value></ExtendedData><ExtendedData><Name>INFO_ROOM_INCOMING_BOOKING_STATUS</Name><Value>O</Value></ExtendedData></HotelRoomExtraInfo></HotelRoom></AvailableRoom></Service></ServiceList><Currency code="USD"></Currency><PaymentData><PaymentType code="C"></PaymentType><Description>*55* Hotelbeds, S.L.U, Bank: CITIBANK(Citigroup Centre, Canary Wharf, London, E14 5LB. United Kingdom) Account:GB13 CITI 185008 13242420,  SWIFT:CITIGB2L,  7 days prior to clients arrival (except group bookings with fixed days in advance at the time of the confirmation) . Please indicate our reference number when making payment. Thank you for your cooperation.</Description></PaymentData><TotalPrice>0.000</TotalPrice></Purchase><Currency code="USD"></Currency><Amount>0.000</Amount></PurchaseCancelRS>';
                        
                        */
                        
                        if ($CancelRes != '') {
							$dom = new DOMDocument();
							$dom->loadXML($CancelRes);
							$Charge_API = $dom->getElementsByTagName("Amount")->item(0)->nodeValue;
							
							if ($dom->getElementsByTagName("PurchaseCancelRS")->item(0)->getElementsByTagName("DetailedMessage")->length > 0) {
								$data['exits'] = $dom->getElementsByTagName("PurchaseCancelRS")->item(0)->getElementsByTagName("DetailedMessage")->item(0)->nodeValue;
								$data['exits'] .= "<br /> OR Your hotel booking has not cancelled, !Please contact admin for further details";
							}
							if ($dom->getElementsByTagName("ServiceList")->length > 0) {
								$status = $dom->getElementsByTagName("ServiceList")->item(0)->getElementsByTagName("Status")->item(0)->nodeValue;
								$totalprice = $dom->getElementsByTagName("ServiceList")->item(0)->getElementsByTagName("TotalPrice")->item(0)->nodeValue;
								$Currency_code = $dom->getElementsByTagName("ServiceList")->item(0)->getElementsByTagName("Currency")->item(0)->getAttribute("code");
								
								if ($status == 'CANCELLED') {
									$hotel_amount = $totalprice;
									$inramount = $totalPrice;

									if ($inramount <= 0) {
										$inramount = 0;
									}
									
									$Refund = ($getBookingHotelData_v1->transaction_amount - $getBookingHotelData_v1->PG_Charge) - $Charge_API;
									
									$update_booking = array(
															'api_status' => 'Cancelled',
															'booking_status' => 'CANCELLED',
															'cancellation_status' => 'Cancelled',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
															'cancel_response' => $CancelReq_Res['HotelBookingCancelRS'],
															'cancellation_amount' => $Charge_API,
															'refund_amount' => $Refund
													);
									
									$data['exits'] = "Booking has been Sucessfully " . $HOTEL_STATUS;
									$this->Booking_Model->Update_Booking_Global($b_data->booking_no, $update_booking);
									//Cancel Email
									$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr_no)->result();

									foreach($data['pnr_nos'] as $pnr_nos):
										$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
										$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
										$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
										//echo '<pre>sanjay'; print_r($data['user_details'][0]->user_email); exit();
										$email['message'] = $this->load->view('vouchers/hotel_mailVoucher', $data, TRUE);
										$email['to'] = $data['user_details'][0]->user_email;
										//echo 'sanjay'; print_r($email['to']); exit();
						                $email['email_access'] = $this->Email_Model->get_email_acess();
						                
					            		$Response = $this->Email_Model->sendmail_hotelVoucher($email);
										redirect('booking/hotelOrders', 'refresh');
									endforeach;
									//Cancel Email End
								} else {
									$data['exits'] = "Your hotel booking has not cancelled, !Please contact admin for further details";
								}
							}
						}
						redirect('booking/hotelOrders','refresh');
					} 

                } else {
                    $response = array('status' => 0);
                    //~ echo json_encode($response);
                    redirect('booking/hotelOrders','refresh');
                }
            } else {
                $response = array('status' => 0);
                //~ echo json_encode($response);
                redirect('booking/hotelOrders','refresh');
            }
        }elseif($module == 'Transfer'){
			             $this->load->library('../controllers/Transferbooking', false);
		                $transferBooking = Transferbooking::transfer_cancel($pnr_no);
		                redirect('booking/transferOrders','refresh');
		}	
    }

    function transfer_cancel(){
		 $pnr_no = '19835179';
		$this->load->helper('gtatransfer_helper');
		$response = TransferBookingCancel($pnr_no);
		
	}

	function PercentageToAmount($total,$percentage){
        $amount = ($percentage/100) * $total;
        $total = $total + $amount;
        return $total;
    }
	
    function view_bookings_old($pnr_no='')
	{
		$user_type_id='';
		if(isset($_POST['pnr_no']) && $_POST['pnr_no']!='')
		{
			$pnr_no =$_POST['pnr_no'];
			$user_type_id =$_POST['user_type_id'];
			
		}
			$data['orders'] = $this->Booking_Model->get_order_details($pnr_no,$user_type_id);
			
			$data['passanger'] = $this->Booking_Model->get_passanger_details($data['orders']->booking_global_id);
			$data['transactions'] = $this->Booking_Model->get_transaction_details_all($data['orders']->booking_transaction_id);
			
			if($data['orders']->product_name == 'Hotels'){
				
				$data['hotel_data'] = $this->Booking_Model->get_hotel_booking_details($data['orders']->referal_id);
			}
			//echo '<pre>'; print_r($data); exit();
			$this->load->view('vouchers/hotel_voucher_view', $data); 
			
	
	} 
	public function view_bookings($parent_pnr=''){
		if(!empty($parent_pnr)){
			$parent_pnr = json_decode(base64_decode($parent_pnr));
			$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
			if($count > 0){
				$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();
				foreach($data['pnr_nos'] as $pnr_nos):
					$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
					$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					if($pnr_nos->product_id == 'Hotels' || $pnr_nos->product_id == 1){
					  $this->load->view('vouchers/hotel_voucher', $data);
				    }else{
					   $this->load->view('vouchers/transfer_voucher', $data);	
					}
				endforeach;
				
			}else{
				 $this->load->view('errors/404');
			}
		}else{
			 $this->load->view('errors/404');
		}
	}

	public function view_flight_bookings($parent_pnr=''){
		if(!empty($parent_pnr)){

			$pnr = json_decode(base64_decode($pnr));
			$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
			
			if($count > 0){
				$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();
				foreach($data['pnr_nos'] as $pnr_nos):
					$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
					$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					if($pnr_nos->product_id == 'Hotels' || $pnr_nos->product_id == 1){
					  $this->load->view('vouchers/hotel_voucher', $data);
				    }else{
					   $this->load->view('vouchers/transfer_voucher', $data);	
					}
				endforeach;
				
			}else{
				 $this->load->view('errors/404');
			}
		}else{
			 $this->load->view('errors/404');
		}
	}

	public function sendVoucher($parent_pnr=''){
		if(!empty($parent_pnr)){
			$parent_pnr = json_decode(base64_decode($parent_pnr));
			$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
			if($count > 0){
				$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();

				foreach($data['pnr_nos'] as $pnr_nos):
					$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
					$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					//echo '<pre>sanjay'; print_r($data['user_details'][0]->user_email); exit();
					$email['message'] = $this->load->view('vouchers/hotel_mailVoucher', $data, TRUE);
					$email['to'] = $data['user_details'][0]->user_email;
	                $email['email_access'] = $this->Email_Model->get_email_acess();
	                
            		$Response = $this->Email_Model->sendmail_hotelVoucher($email);
					redirect('booking/hotelOrders', 'refresh');
				endforeach;
				
			}else{
				  echo 'Invalid Data';
			}
		}else{
			  echo 'Invalid Data';
		}
	}

	public function refineSearch(){
		//echo '<pre>'; print_r($_POST); exit();
		$booking 					= $this->General_Model->getHomePageSettings();
		//$data['moduleType'] = $this->input->post('module');
		$data['moduleType'] = 'HOTEL';
		
		$data['orders'] = $this->Booking_Model->getRefineSearchResult($_POST);
		
		$data['transactions'] = $this->Booking_Model->get_transaction_details_all($data['Bookings'][0]->booking_transaction_id);
		//echo "<pre>"; print_r($data['transactions']); 
		$this->load->view('bookings/ajax_refine/ajaxRefineResults', $data);
	}

	public function transferrefineSearch(){
		//echo '<pre>'; print_r($_POST); exit();
		$booking 					= $this->General_Model->getHomePageSettings();
		//$data['moduleType'] = $this->input->post('module');
		$data['moduleType'] = 'Transfer';
		$data['orders'] 			= $this->Booking_Model->getRefinetransferSearchResult($_POST);
		$data['transactions'] = $this->Booking_Model->get_transaction_details_all($data['Bookings'][0]->booking_transaction_id);
		//echo "<pre>"; print_r($data['transactions']); 
		echo $this->load->view('bookings/ajax_refine/ajaxTranserRefineResults', $data, TRUE);
	}

	public function sendTransferVoucher($parent_pnr=''){
		if(!empty($parent_pnr)){
			$parent_pnr = json_decode(base64_decode($parent_pnr));
			$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
			if($count > 0){
				$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();

				foreach($data['pnr_nos'] as $pnr_nos):
					$data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr_v1($pnr_nos->pnr_no,$pnr_nos->product_name)->row();
					$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					//echo '<pre>sanjay'; print_r($data['user_details'][0]->user_email); exit();
					$email['message'] = $this->load->view('vouchers/transfer_emailvoucher', $data, TRUE);
					$email['to'] = $data['user_details'][0]->user_email;
	                $email['email_access'] = $this->Email_Model->get_email_acess();
	                
            		$Response = $this->Email_Model->sendmail_transferVoucher($email);
					redirect('booking/transferOrders', 'refresh');
				endforeach;
				
			}else{
				  echo 'Invalid Data';
			}
		}else{
			  echo 'Invalid Data';
		}
	}

	//Booknig Logs

	function view_request($parent_pnr){
	$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_booking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/requestBooking', $data);
}

	function view_response($parent_pnr){
	$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_booking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/responseBooking', $data);
}

function view_payment_response($parent_pnr){
	$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_booking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/paymentresponseBooking', $data);
}

function view_transferrequest($parent_pnr){
	$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_transferbooking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/requestTransferBooking', $data);
}

function view_transferresponse($parent_pnr){
	$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_transferbooking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/responseTransferBooking', $data);
}

function view_transferpayment_response($parent_pnr){
	$data = $this->General_Model->getHomePageSettings();
       	//$module = "Hotels"; 
        $parent_pnr = json_decode(base64_decode($parent_pnr));
        $data['orders'] = $this->Booking_Model->get_transferbooking_logs($parent_pnr);
        //echo '<pre>'; print_r($data['orders']); exit();
        $data['user_type'] = $this->Usertype_Model->get_user_type_list();
        $data['user_id'] = '0';
			
        $this->load->view('bookings/paymenttransferresponseBooking', $data);
}
	//End of Booking Logs
}
