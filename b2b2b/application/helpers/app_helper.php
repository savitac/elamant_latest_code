<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function validate_user_login()
{
	if (is_logged_in_user() == false) {
		redirect(base_url());
	}
}
function index_month_number($data)
{
	$imn_data = array();
	if (valid_array($data)) {
		foreach ($data as $k => $v) {
			$imn_data[($v['month_number']-1)] = $v;//Dont Change--Jaganath
			//$imn_data[$k] = $v;//In HighChart, Month starts with 0;
		}
	}
	return $imn_data;
}

/**
 * Hotel Suggestion value for input element
 */
function hotel_suggestion_value($city, $country)
{
	return $city.' ('.$country.')';
}
// ---------------------------------------------------------- Active Module Start
/**
 * Check if current module is enabled
 */
function is_active_airline_module()
{
	$status = false;
	if (in_array(META_AIRLINE_COURSE, $GLOBALS['CI']->active_domain_modules)) {
		$status = true;
	}
	return $status;
}

/**
 * Check if current module is enabled
 */
function is_active_hotel_module()
{
	$status = false;
	if (in_array(META_ACCOMODATION_COURSE, $GLOBALS['CI']->active_domain_modules)) {
		$status = true;
	}
	return $status;
}

/**
 * Check if current module is enabled
 */
function is_active_bus_module()
{
	$status = false;
	if (in_array(META_BUS_COURSE, $GLOBALS['CI']->active_domain_modules)) {
		$status = true;
	}
	return $status;
}

/**
 * Check if current module is enabled
 */
function is_active_package_module()
{
	$status = false;
	if (in_array(META_PACKAGE_COURSE, $GLOBALS['CI']->active_domain_modules)) {
		$status = true;
	}
	return $status;
}
/**
 * Check if end module is enabled
 */
function is_active_module($module)
{
	$cond=array(
			"module" => $module,
			"status" => ACTIVE
	);
	$var = $GLOBALS['CI']->custom_db->single_table_records('active_modules','',$cond);
	return $var['status'];
}
/**
 * checking social login status
 */
function is_active_social_login($module)
{
	$CI = & get_instance();
	$social_links = $CI->db_cache_api->get_active_social_network_list();
	return (isset($social_links[$module]) ? $social_links[$module]['status'] : false);
}
function no_social()
{
	$CI = & get_instance();
	$social_links = $CI->db_cache_api->get_active_social_network_list();
	if (is_array($social_links) == true) {
		foreach ($social_links as $k => $v) {
			if ($v['status'] == false) {
				return false;
				break;
			}
		}
	} else {
		return false;
	}
}
// ---------------------------------------------------------- Active Module End
// ---------------------------------------------------------- Load Library Start
/**
 * Load Hotel Lib based on hotel source
 * @param string $source
 */
function load_hotel_lib($source)
{
	$CI = &get_instance();
	// debug($source);
	switch ($source) {
		case PROVAB_HOTEL_BOOKING_SOURCE :
			$CI->load->library('hotel/provab_private', '', 'hotel_lib');
			break;
		case CRS_HOTEL_BOOKING_SOURCE :
            $CI->load->library('hotel/provab_hotelcrs', '', 'hotel_lib');
            break;
		case GTA_HOTEL_BOOKING_SOURCE :
			$CI->load->library('hotel/gta','','hotel_lib'); 
			break;
		default : redirect(base_url());
	}
}

/**
 * Load Flight Lib based on hotel source
 * @param string $source
 */
function load_flight_lib($source)
{
  
	$CI = &get_instance();
	switch ($source) { 
		case PROVAB_FLIGHT_BOOKING_SOURCE :
			$CI->load->library('flight/provab_private', '', 'flight_lib');
			break;
		case TRAVELPORT_FLIGHT_BOOKING_SOURCE :
			$CI->load->library('flight/travelport', '', 'flight_lib');
			break; 
		case SABRE_FLIGHT_BOOKING_SOURCE :
			$CI->load->library('flight/Sabre_private', '', 'flight_lib');
			break;			
		default : redirect(base_url());
	}
}

/**
 * Load PG Lib based on hotel source
 * @param string $source
 */
function load_pg_lib($source)
{
	$CI = &get_instance();
	switch ($source) {
		case 'PAYU' :
			$GLOBALS['CI']->load->library('payment_gateway/payu', '', 'pg');
			break;
		case 'CCAVENUE' :
			$GLOBALS['CI']->load->library('payment_gateway/ccavenue', '', 'pg');
			break;
		case 'EBS' :
			$GLOBALS['CI']->load->library('payment_gateway/EBS', '', 'pg');
			break;

		case 'AUTHORIZE' :
            $GLOBALS['CI']->load->library('payment_gateway/Authorize', '', 'pg');
            break;

		// default : redirect(base_url());
		default : echo 'Default';
	}
}


/**
 * Load Bus Lib based on bus source
 * @param string $source
 */
function load_bus_lib($source)
{
	$CI = &get_instance();
	switch ($source) {
		case PROVAB_BUS_BOOKING_SOURCE :
			$CI->load->library('bus/provab_private', '', 'bus_lib');
			break;
		case RED_BUS_BOOKING_SOURCE :
			$CI->load->library('bus/red_bus', '', 'bus_lib');	
			break;
		default : redirect(base_url());
	}
}
// ---------------------------------------------------------- Load Library END
/**
 * * Arjun J Gowda
 *Get unserialized
 */
function unserialized_data($data, $data_key=false)
{
	if (empty($data_key) == true || md5($data) == $data_key) {
		/*$data = base64_decode($data);
		if (empty($data) === false && is_string($data) == true) {
			$json_data = json_decode($data, true);
			if (valid_array($json_data) == true) {
				$data = $json_data;
			}
		}*/
		$data = base64_decode($data);
		//debug($data);die();

		if ($data !== false) {
			$data = @unserialize($data);
		} 
	} else {
		$data = false;
	}
	return $data;
}

/**
 * * Arjun J Gowda
 * Serialized
 */
function serialized_data($data)
{
	return base64_encode(serialize($data));
	/*if (is_array($data)) {
	$data = json_encode($data);
	}
	return base64_encode($data);*/
}

/**
 * Check if multiple or force to be multiple
 * @param array $data
 */
function force_multple_data_format($data)
{
	$mul_data = array();
	if (is_object($data) == true) {
		if (isset($data->{0}) == false) {
			$mul_data->{0} = $data;
		} else {
			$mul_data = $data;
		}
	} elseif (is_array($data) == true) {
		if (isset($data[0]) == false) {
			$mul_data[0] = $data;
		} else {
			$mul_data = $data;
		}
	}
	return $mul_data;
}

/**
 * show label based on balance request status
 * @param string $status
 */
function balance_status_label($status)
{
	switch ($status) {
		case 'PENDING': $status_label = 'label-info';
		break;
		case 'ACCEPTED': $status_label = 'label-success';
		break;
		case 'REJECTED': $status_label = 'label-danger';
		break;
	}
	return $status_label;
}

/**
 * show label based on balance request status
 * @param string $status
 */
function booking_status_label($status)
{
	switch ($status) {
		case 'BOOKING_PENDING':
		case 'BOOKING_HOLD': $status_label = 'label label-info';
		break;
		case 'BOOKING_CONFIRMED': $status_label = 'label label-success';
		break;
		case 'CONFIRMED': $status_label = 'label label-success';
		break;
		case 'BOOKING_ERROR' :
		case 'BOOKING_INCOMPLETE' :
		case 'BOOKING_CANCELLED': $status_label = 'label label-danger';
		break;
		default : $status_label = 'label label-primary';
	}
	return $status_label;
}
/**
 * Refund Status Label
 * @param $status
 */
function refund_status_label($status)
{
	switch ($status) {
		case 'INPROGRESS':
			$status_label = 'label label-info';
			break;
		case 'PROCESSED':
			$status_label = 'label label-success';
			break;
		case 'REJECTED':
			$status_label = 'label label-danger';
			break;
		break;
		default : $status_label = 'label label-primary';
	}
	return $status_label;
}

/**
 * * Arjun J Gowda
 * check if the current user has privilege to view the web page
 * @param $privilege_key unique key which identifies privilege to access each page
 * @param $auto_redirect boolean used to prevent auto redirection while checking web page access privilege
 */
function web_page_access_privilege($privilege_key, $auto_redirect=true)
{
	return true;
}

/**
 ** Arjun J Gowda
 */
function get_default_image_loader()
{
	return '<div class="data-utility-loader" style="display:none">
			Please Wait <img src="'.$GLOBALS['CI']->template->template_images('tiny_loader_v1.gif').'" class="img-responsive center-block"></img>
		</div>';
}

/**
 * * Arjun J Gowda
 * check if the user is logged in or not
 */
function is_logged_in_user()
{	$CI =& get_instance();
	$CI->load->library('session'); 
	// $entity_user_id=$CI->session->userdata('user_details_id');
	$logged_in =$CI->session->userdata('user_logged_in');
	//echo $entity_user_id; die(" IN APP NEW"); 
	if (isset($logged_in) == true and intval($logged_in) > 0) {
		return true;
	} else {
		return false;
	}
}

function is_app_user()
{
	if (isset($GLOBALS['CI']->entity_user_type) == true and in_array(intval($GLOBALS['CI']->entity_user_type), array(B2C_USER, B2B_USER))) {
		return true;
	} else {
		return false;
	}
}

/**
 * * Arjun J Gowda
 * return domain name
 */
function domain_name()
{
	$CI =& get_instance();
	$CI->load->library('session');  
	return $CI->session->userdata('company_name');
}

/**
 * check user is Provab Admin or Domain Admin
 * Jaganath (22-05-2015) - 22-05-2015
 */
function is_domain_user()
{
	//check this based on domain id when the user login
	$domain_id = $GLOBALS['CI']->entity_domain_id;
	if (intval($domain_id) > 0) {
		return true;
	} else {
		return false;
	}
}

/**
 * Get Domain Key
 * Jaganath (22-05-2015) - 22-05-2015
 */
function get_domain_auth_id()
{
	$CI = &get_instance();
	$domain_auth_id = $CI->session->userdata(DOMAIN_AUTH_ID);
	$domain_auth_id = 1; 
	if (intval($domain_auth_id) > 0) {
		return intval($domain_auth_id);
	} else {
		return 0;
	}
}

function active_sms_checkpoint($name)
{
	$status = $GLOBALS['CI']->user_model->sms_checkpoint($name);
	if ($status == ACTIVE) {
		return true;
	} else {
		return false;
	}
}

function current_application_balance()
{
	$GLOBALS['CI']->load->library('Api_Interface');
	$resp = json_decode($GLOBALS['CI']->api_interface->rest_service('domain_balance'), true);
	$resp['balance'] = intval(@$resp['balance']);
	$balance['balance'] = @$resp['balance'];
	$balance['face_value'] = @$resp['currency'].' '.@$resp['balance'];
	return $balance;
}
/**
 * Jaganath
 * Setting Domain(Travelomatix) currency as API Data Currency
 * @return string
 */
/*function get_api_data_currency()
{
	$CI = &get_instance();
	$GLOBALS['CI']->load->library('Api_Interface');
	$domain_currency = $CI->session->userdata('base_currency'); 
	return $domain_currency;
}*/
function get_api_data_currency()
{
	$GLOBALS['CI']->load->library('Api_Interface');
	$domain_currency = $GLOBALS['CI']->session->userdata('domain_currency');

	// debug($domain_currency);
	if(empty($domain_currency) == true) {
		$response = json_decode($GLOBALS['CI']->api_interface->rest_service('domain_currency'), true);

		// debug($response);exit;
		if(valid_array($response) == true && $response['status'] == ACTIVE && empty($response['currency']) == false) {
			$domain_currency = trim($response['currency']);
		} else {
			echo 'Invalid API Currency';
			exit;
			//$domain_currency = 'INR';
		}
		$GLOBALS['CI']->session->set_userdata(array('domain_currency' => $domain_currency));
	}
	return $domain_currency;
} 
function db_current_date($date='')
{
	if (empty($date) == false) {
		return date('Y-m-d', strtotime($date));
	} else {
		return date('Y-m-d', time());
	}
}
/**
 * Admin Base Currency
 * @return string
 */
function admin_base_currency()
{
	$GLOBALS['CI']->load->model('db_cache_api');
	return $GLOBALS['CI']->db_cache_api->get_admin_base_currency();
} 
/**
 * Admin Base Currency
 * @return string
 */
function agent_base_currency()
{
	return $GLOBALS['CI']->db_cache_api->get_agent_base_currency();
}

function agent_current_application_balance()
{
	return $GLOBALS['CI']->db_cache_api->get_current_balance();
}
/**
 * Get Domain Config Key
 * Jaganath (26-05-2015) - 26-05-2015
 */
function get_domain_key()
{
return CURRENT_DOMAIN_KEY;
	$CI = &get_instance();
	$domain_key = trim($CI->session->userdata(DOMAIN_KEY));
	if (empty($domain_key) == false) {
		return base64_decode(trim($domain_key));
	} else {
		return '';
	}
}

function user_type_access_control($user_type_heap)
{
	return check_user_type($GLOBALS['CI']->entity_user_type, $user_type_heap);
}

function get_status_strip($status)
{
	$strip = '';
	switch ($status) {
		case ACTIVE : $strip = 'alert-success';
		break;
		case INACTIVE : $strip = 'alert-danger';
		break;
	}
	return $strip;
}

function get_target_status_strip($target_limit, $user_target)
{
	//badge red-shade-3
}
/**
 *
 */
function check_user_type($user_type='', $user_type_heap='')
{
	if (intval($user_type) > 0) {
		//get user type related list
		$heap_data = array();
		if (is_string($user_type_heap) == true) {
			if (in_array($user_type, array_keys(get_enum_list($user_type_heap)))) {
				return true;
			}
		} elseif (valid_array($user_type_heap) == true) {
			foreach ($user_type_heap as $k => $v) {
				if (is_string($v)) {
					if (in_array($user_type, array_keys(get_enum_list($v)))) {
						return true;
					}
				}
			}
		}
	}
}

function refresh()
{
	redirect(uri_string().'?'.(empty($_SERVER['REDIRECT_QUERY_STRING']) == false ? $_SERVER['REDIRECT_QUERY_STRING'] : $_SERVER['QUERY_STRING']), 'Refresh');
}
/**
 * to check if the request from client is ajax or not
 */
function is_ajax()
{

	if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$status = true;
	} else {
		$status = false;
	}
	return $status;
}

function check_default_edit_privilege($user_id=0)
{
	if ($user_id == $GLOBALS['CI']->entity_user_id) {
		return true;
	} else {
		return false;
	}
}

function super_privilege()
{
	return false;
}

/**
 * compress output before sending to browser
 * @param unknown_type $input
 */
function get_compressed_output($data)
{
	ini_set('memory_limit', '-1');
	$search = array(
    '/\>[^\S ]+/s',
    '/[^\S ]+\</s',
  '#(?://)?<!\[CDATA\[(.*?)(?://)?\]\]>#s' //leave CDATA alone
	);
	$replace = array(
     '>',
     '<',
  "//&lt;![CDATA[\n".'\1'."\n//]]>"
  );

  return  preg_replace($search, $replace, $data);
}
/**
 *decode json and returns valid json
 *
 *@param object  $json  Json which has to be decoded
 *@param boolean $assoc boolean which indicates if array should be returned
 */
function json_validate($json, $assoc = TRUE)
{
	//decode the JSON data
	$result = json_decode($json, $assoc);

	// switch and check possible JSON errors
	switch (json_last_error()) {
		case JSON_ERROR_NONE:
			$error = ''; // JSON is valid
			break;
		case JSON_ERROR_DEPTH:
			$error = 'Maximum stack depth exceeded.';
			break;
		case JSON_ERROR_STATE_MISMATCH:
			$error = 'Underflow or the modes mismatch.';
			break;
		case JSON_ERROR_CTRL_CHAR:
			$error = 'Unexpected control character found.';
			break;
		case JSON_ERROR_SYNTAX:
			$error = 'Syntax error, malformed JSON.';
			break;
			// only PHP 5.3+
		case JSON_ERROR_UTF8:
			$error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
			break;
		default:
			$error = 'Unknown JSON error occured.';
			break;
	}

	if($error !== '') {
		// throw the Exception or exit
		$status = false;
		$message = $error;
		$data = '';
		//redirect('general/index');
	} else {
		$status = true;
		$message = '';
		$data = $result;
	}

	return array('status' => $status, 'message' => $message, 'data' => $data);
}

/**
 *get message returns a message with appropriate html
 *
 *@param int $message_type message boxes to be displayed
 *@param str $message to be displayed inside the box
 */
function get_message ($message = "UL003", $message_type=ERROR_MESSAGE, $button_required = false, $override_app_msg = false)
{
	switch ($message_type) {
		case SUCCESS_MESSAGE:
			$alert_class = 'alert-success';
	  break;

		case WARNING_MESSAGE:
			$alert_class = 'alert-warning';
	  break;

		case INFO_MESSAGE:
			$alert_class = 'alert-info';
	  break;

		default:
			$alert_class = 'alert-danger';
	}
	$content = '<div class="alert '.$alert_class.' clearfix" role="alert">';
	if($button_required) {
		$content .= '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>';
	}
	if($override_app_msg == false) {
		$content .= $GLOBALS['CI']->lang->line($message).'</div>';
	} else {
		$content .= $message.'</div>';
	}

	return $content;
}

/**
 *
 * @param string $message
 * @param string $override_app_msg
 */
function extract_message($message = "UL003", $override_app_msg = false) {
	$content = '';
	if (empty ( $override_app_msg ) == true) {
		$content .= $GLOBALS ['CI']->lang->line ( $message );
	} else {
		$content .= $message;
	}
	if (empty ( $content ) == true) {
		$content = "Remote IO Error";
	}
	return $content;
}

function get_toastr_index($message_type) {
	switch ($message_type) {
		case SUCCESS_MESSAGE :
			$toastr = 'success';
			break;

		case WARNING_MESSAGE :
			$toastr = 'warning';
			break;

		case INFO_MESSAGE :
			$toastr = 'info';
			break;

		default :
			$toastr = 'error';
	}
	return $toastr;
}

/**
 * returns string containing message for message id
 * @param $msg_id index of message whose message has to be fetched
 */
function get_label($msg_id)
{
	$msg = $GLOBALS['CI']->lang->line('FL00'.$msg_id);
	return (empty($msg) == false ? $msg : $msg_id);
}

/**
 * returns string containing message for message id
 * @param $msg_id index of message whose message has to be fetched
 */
function get_placeholder($msg_id)
{
	return $GLOBALS['CI']->lang->line('FL000'.$msg_id);
}

/**
 * returns string containing message for message id
 * @param $msg_id index of message whose message has to be fetched
 */
function get_help_text($msg_id)
{
	return $GLOBALS['CI']->lang->line('FL0000'.$msg_id);
}
/**
 * returns string containing message for message id
 * @param $msg_id index of message whose message has to be fetched
 */
function generate_help_text($ip_help_text)
{
	return ' data-container="body" data-toggle="popover" data-original-title="'.get_utility_message('UL004').'" data-placement="bottom" data-trigger="hover focus" data-content="'.$ip_help_text.'"';
}

/**
 * returns string containing message for message id
 * @param $msg_id index of message whose message has to be fetched
 */
function provab_help_text($help_text)
{
	return '<span data-toggle="popover" data-title="'.get_utility_message('UL004').'" data-placement="bottom" data-html=true  class="glyphicon glyphicon-question-sign handCursor provabHelpText" data-content="'.$help_text.'">';
}
/**
 * returns string containing message for message id
 * @param $msg_id index of message whose message has to be fetched
 */
function get_utility_message($msg_id)
{
	return $GLOBALS['CI']->lang->line($msg_id);
}

/**
 * returns string containing message for message id
 * @param $msg_id index of message whose message has to be fetched
 */
function get_app_message($msg_id)
{
	return $GLOBALS['CI']->lang->line($msg_id);
}

/**
 * returns string containing message for message id
 * @param $msg_id index of message whose message has to be fetched
 */
function get_legend($msg_id)
{
	return $GLOBALS['CI']->lang->line($msg_id);
}


/**
 * * returns string containing options
 * @param $option_list    list of option values
 * @param $override_order should the order be changed by sorting values
 */
function generate_options($option_list=array(), $default_value=false, $override_order=false)
{
	$options = '';
	if (valid_array($option_list) == true) {
		$array_values = array_values($option_list);
		if (valid_array($array_values[0]) == true) {
			if ($default_value) {
				foreach ($option_list as $k => $v) {
					if (in_array($v['k'], $default_value)) {
						$selected = ' selected="selected" ';
					} else {
						$selected = '';
					}
					$options .= '<option value="'.$v['k'].'" '.$selected.'>'.$v['v'].'</option>';
				}
			} else {
				foreach ($option_list as $k => $v) {
					$options .= '<option value="'.$v['k'].'">'.$v['v'].'</option>';
				}
			}
		} else {
			if ($default_value) {
				foreach ($option_list as $k => $v) {
					if (in_array($k, $default_value)) {
						$selected = ' selected="selected" ';
					} else {
						$selected = '';
					}
					$options .= '<option value="'.$k.'" '.$selected.'>'.$v.'</option>';
				}
			} else {
				foreach ($option_list as $k => $v) {
					$options .= '<option value="'.$k.'">'.$v.'</option>';
				}
			}
		}
	} else {
		$options .= '<option value="INVALIDIP">---</option>';
	}
	return $options;
}

/**
 * returns enumerated list of values
 * @param $enum name of enumeration datatype
 */
function get_enum_list($enum, $default_value=-1)
{
	if ($GLOBALS['CI']->load->is_loaded('enumeration') == false) {
		$GLOBALS['CI']->load->library('enumeration');
	}
	$enumeration_list = $GLOBALS['CI']->enumeration->getEnumerationList($enum);
	if (intval($default_value) > -1) {
		return (isset($enumeration_list[$default_value]) ? $enumeration_list[$default_value] : '');
	} else {
		return $enumeration_list;
	}
}

/**
 * returns enumerated list of values
 * @param $enum name of enumeration datatype
 */
function provab_solid_regexp($data_key)
{
	if ($GLOBALS['CI']->load->is_loaded('provab_solid') == false) {
		$GLOBALS['CI']->load->library('provab_solid');
	}
	return $GLOBALS['CI']->provab_solid->provab_solid_regexp($data_key);
}

/**
 * returns image
 * @param unknown_type $image_name
 */
function get_profile_image($image_name='')
{
	return (empty($image_name) ? 'face.png' : $image_name);
}

/**
 * Generates Numeric Drop-Down with
 * the given size
 */
function numeric_dropdown($size = array('size' => 10)) {
	$options = "";
	if (valid_array ( $size )) {
		$arr_size = $size ['size'];
		if(isset($size['divider']) == true && $size['divider'] > 0) {
			$divider = floatval($size['divider']);
		} else {
			$divider = 1;
		}
		for($i = 1; $i <= $size ['size']; $i ++) {
			$options [$i] = array (
					'k' => ($i/$divider),
					'v' => ($i/$divider) 
			);
		}
	}
	return $options;
}
/**
 * Jaganath
 * Set Insert meassge
 * @param $msg
 * @param $type
 * @param $attributes
 */
function set_insert_message($msg = 'UL0014', $type = SUCCESS_MESSAGE, $attributes = array())
{
	$error_message = array('message'=>$msg, 'type'=>$type);
	if(valid_array($attributes)) {
		$error_message = array_merge($error_message, $attributes);
	}
	$GLOBALS['CI']->session->set_flashdata($error_message);
}
/**
 * Jaganath
 * Set Update meassge
 * @param $msg
 * @param $type
 * @param $attributes
 */
function set_update_message($msg = 'UL0013', $type = SUCCESS_MESSAGE, $attributes = array())
{
	$error_message = array('message'=>$msg, 'type'=>$type);
	if(valid_array($attributes)) {
		$error_message = array_merge($error_message, $attributes);
	}
	$GLOBALS['CI']->session->set_flashdata($error_message);
}
/**
 * Jaganath
 * Set Error meassge
 * @param $msg
 * @param $type
 * @param $attributes
 */
function set_error_message($msg = 'UL0049', $type = ERROR_MESSAGE, $attributes = array())
{
	$error_message = array('message'=>$msg, 'type'=>$type);
	if(valid_array($attributes)) {
		$error_message = array_merge($error_message, $attributes);
	}
	$GLOBALS['CI']->session->set_flashdata($error_message);
}


function get_arrangement_icon($arrangement='')
{
	$arrangement_icon = '';
	switch($arrangement) {
		/* Hotel */
		case META_ACCOMODATION_COURSE:	$arrangement_icon = 'fa fa-bed';
		break;
		/* Transfers */
		case META_TRANSFERS_COURSE : $arrangement_icon = 'fa fa-car';
		break;
		/* Airline */
		case META_AIRLINE_COURSE:	$arrangement_icon = 'fa fa-plane';
		break;

		/* Meals */
		case 'VHCID1420973863' : $arrangement_icon = 'fa fa-cutlery';
		break;

		/* Activities */
		case 'VHCID1420973899': $arrangement_icon = 'fa fa-camera';
		break;

		/* Guide */
		case 'VHCID1420973949' : $arrangement_icon = 'fa fa-user';
		break;

		/* Visa */
		case 'VHCID1420973967' : $arrangement_icon = 'fa fa-credit-card';
		break;

		/* Insurance */
		case 'VHCID1420973976' : $arrangement_icon = 'fa fa-credit-card';
		break;

		/* Misc */
		case 'VHCID1420973998' : $arrangement_icon = 'fa fa-random';
		break;
			
		/* HANDOVER */
		case 'VHCID1430114195' : $arrangement_icon = 'fa fa-gift';
		break;

		/* BUS */
		case META_BUS_COURSE : $arrangement_icon = 'fa fa-bus';
		break;

		/* Package */
		case META_PACKAGE_COURSE : $arrangement_icon = 'fa fa-suitcase';
		break;

		default : $arrangement_icon = 'fa fa-th';
		break;
	}
	return $arrangement_icon;
}

function get_arrangement_color($arrangement='')
{
	$arrangement_color = '';
	switch($arrangement) {
		/* Hotel */
		case META_ACCOMODATION_COURSE:	$arrangement_color = 'hotel-l-bg';
		break;
		/* Transfers */
		case META_TRANSFERS_COURSE : $arrangement_icon = '';
		break;
		/* Airline */
		case META_AIRLINE_COURSE:	$arrangement_color = 'flight-l-bg';
		break;
		/* BUS */
		case META_BUS_COURSE : $arrangement_color = 'bus-l-bg';
		break;

		/* Package */
		case META_PACKAGE_COURSE : $arrangement_color = 'package-l-bg';
		break;

		default : $arrangement_color = '';
		break;
	}
	return $arrangement_color;
}

/**
 *
 * @param unknown_type $name
 */
function module_name_to_id($name)
{
	$id = '';
	switch ($name) {
		case 'flight': $id = META_AIRLINE_COURSE; break;
		case 'hotel': $id = META_ACCOMODATION_COURSE; break;
		case 'bus': $id = META_BUS_COURSE; break;
		case 'package': $id = META_PACKAGE_COURSE; break;
	}
	return $id;
}


function module_spirit_img($module_name)
{
	$img = '';
	switch ($module_name) {
		case 'flight': $img = 'flihtex'; break;
		case 'hotel': $img = 'htlex'; break;
		case 'bus': $img = 'busex'; break;
		case 'package': $img = 'holidytex'; break;
	}
	return $img;
}

/**
 * Show lazy loading gif to user
 */
function get_lazy_loading_icon()
{
	return '<img src="'.$GLOBALS['CI']->template->template_images('loader.gif').'" class="center-align-image lazy-loader-image">';
}

/**
 * Show lazy loading gif to user
 */
function get_circle_ball_loading_icon()
{
	return '<img src="'.$GLOBALS['CI']->template->template_images('circle-ball-ajax-loader.gif').'" class="">';
}

/**
 * generate color code for string
 * @param $string
 */
function string_color_code($string)
{
	$string = md5($string);
	$string = array(
		"R" => hexdec(substr($string, 0, 2)),
		"G" => hexdec(substr($string, 2, 2)),
		"B" => hexdec(substr($string, 4, 2))
	);
	return 'rgb('.implode(',', $string).')';
}


function get_current_url()
{
	$url = current_base_url();
	return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}

function current_base_url()
{
	$CI =& get_instance();
	return $CI->config->site_url($CI->uri->uri_string());
}

/**
 * default form should be visible if data is posted or if eid is present or if op is defined as add
 */
function form_visible_operation()
{
	if (valid_array($_POST) == true || isset($_GET['eid']) == true || isset($_GET['origin_id']) == true || (isset($_GET['op']) == true && $_GET['op'] == 'add') ) {
		return true;
	} else {
		return false;
	}

}

function login_status($time)
{
	return ((empty($time) == false and strtotime($time) == 0) ? '<i class="fa fa-circle text-success"></i>' : '<i class="fa fa-circle text-warning"></i>');
}
function last_login($time)
{
	return (empty($time) == false ? get_duration_label(time()-strtotime($time)).' Ago('.app_friendly_absolute_date($time).')' : 'Not Seen Online :(');
}


function member_since($time)
{
	return (empty($time) == false ? '(Since:'.date('M, Y', strtotime($time)).')' : '');
}
/**
 * Jaganath
 * Round off the number to upper value
 * @param unknown_type $number
 * @return string
 */
function roundoff_number($number)
{
	return round($number, 2);
	//return $number;
}

function data_to_log_file($data) {
	if (is_array ( $data )) {
		$data = json_encode ( $data );
	}
	error_log ( $data, 3, '/opt/lampp/logs/php_error_log' );
}
/**
 * Jaganath
 * Merges the Numeric Array Values
 */
function array_merge_numeric_values($data)
{
	$merged_array = array();
	foreach ($data as $array)
	{
		foreach ($array as $key => $value)
		{
			if ( ! is_numeric($value))
			{
				$merged_array[$key] = $value;
				continue;
			}
			if ( ! isset($merged_array[$key]))
			{
				$merged_array[$key] = $value;
			}
			else
			{
				$merged_array[$key] += $value;
			}
		}
    }
    return $merged_array;
}
/**
 * Groups the clumns
 * @param $input
 * @param $column_key
 * @param $index_key
 */

function group_array_column( array $input, $column_key, $index_key = null ) 
{
	$result = array();
	foreach( $input as $k => $v ) {
		$result[$k] = $v[$column_key];
	}
	return $result;
 }
function debug($var)
{
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}
function get_no_of_days($from_date,$to_date){
	$datediff = (strtotime($to_date) - strtotime($from_date));
	return ceil($datediff / (60 * 60 * 24));
}
function getGtaFacilities($Facilities)
{
	$Facilities = explode(",", $Facilities);
	$getGtaFacilitiesp = array();
	for ($i=0; $i < count($Facilities) ; $i++) { 
		switch ($Facilities[$i]) {
			case 'Business Center':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'BC';
				$getGtaFacilitiesp['name'] = 'Business Center';
				break;
			case 'Beauty parlour':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'BP';
				$getGtaFacilitiesp['name'] = 'Beauty parlour';
				break;
			case 'Boutique':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'BQ';
				$getGtaFacilitiesp[$i]['name'] = 'Boutique';
				break;
			case 'Baby sitting':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'BS';
				$getGtaFacilitiesp[$i]['name'] = 'Baby sitting';
				break;
			case 'Car parking':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'CP';
				$getGtaFacilitiesp[$i]['name'] = 'Car parking (Payable to hotel, if applicable)';
				break;
			case 'Car rental facilities':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'CR';
				$getGtaFacilitiesp[$i]['name'] = 'Car rental facilities';
				break;
			case 'Voltage':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'VL';
				$getGtaFacilitiesp[$i]['name'] = 'Voltage';
				break;
			case 'Disabled facilities':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'DF';
				$getGtaFacilitiesp[$i]['name'] = 'Disabled facilities';
				break;
			case 'Early check-in':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'EC';
				$getGtaFacilitiesp[$i]['name'] = 'Early check-in';
				break;
			case 'Golf':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'GF';
				$getGtaFacilitiesp[$i]['name'] = 'Golf';
				break;
			case 'Gymnasium':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'GY';
				$getGtaFacilitiesp[$i]['name'] = 'Gymnasium';
				break;
			case 'Coach parking':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'HP';
				$getGtaFacilitiesp[$i]['name'] = 'Coach parking';
				break;
			case 'Indoor pool':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'IP';
				$getGtaFacilitiesp[$i]['name'] = 'Indoor pool';
				break;
			case "Children's pool":
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'KP';
				$getGtaFacilitiesp[$i]['name'] = "Children's pool";
				break;
			case 'Lifts':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'LF';
				$getGtaFacilitiesp[$i]['name'] = 'Lifts';
				break;
			case 'Lobby':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'LS';
				$getGtaFacilitiesp[$i]['name'] = 'Lobby';
				break;
			case 'Laundry facilities':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'LY';
				$getGtaFacilitiesp['name'] = 'Laundry facilities';
				break;
			case 'Porterage':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'PT';
				$getGtaFacilitiesp[$i]['name'] = 'Porterage';
				break;
			case 'Outdoor pool':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'OP';
				$getGtaFacilitiesp['name'] = 'Outdoor pool';
				break;
			case 'Room Service':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'RS';
				$getGtaFacilitiesp[$i]['name'] = 'Room Service';
				break;
			case 'Sauna':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'SA';
				$getGtaFacilitiesp[$i]['name'] = 'Sauna';
				break;
			case 'Shop':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'SH';
				$getGtaFacilitiesp[$i]['name'] = 'Shop';
				break;
			case 'Solarium':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'SO';
				$getGtaFacilitiesp[$i]['name'] = 'Solarium';
				break;
			case 'Travel agency facilities':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'TA';
				$getGtaFacilitiesp[$i]['name'] = 'Travel agency facilities';
				break;
			case 'Tennis':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'TE';
				$getGtaFacilitiesp[$i]['name'] = 'Tennis';
				break;
			case 'Air conditioning':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'AC';
				$getGtaFacilitiesp[$i]['name'] = 'Air conditioning';
				break;
			case 'Automatic wake-up call':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'AW';
				$getGtaFacilitiesp[$i]['name'] = 'Automatic wake-up call';
				break;
			case 'Direct dial phone':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'DD';
				$getGtaFacilitiesp[$i]['name'] = 'Direct dial phone';
				break;
			case 'In-house movie':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'FI';
				$getGtaFacilitiesp[$i]['name'] = 'In-house movie';
				break;
			case 'Hairdryer':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'HD';
				$getGtaFacilitiesp[$i]['name'] = 'Hairdryer';
				break;
			case 'Internet via television':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'IN';
				$getGtaFacilitiesp[$i]['name'] = 'Internet via television';
				break;
			case 'Connection for laptop':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'LT';
				$getGtaFacilitiesp[$i]['name'] = 'Connection for laptop';
				break;
			case 'Mini bar':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'MB';
				$getGtaFacilitiesp[$i]['name'] = 'Mini bar';
				break;
			case 'Radio':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'RA';
				$getGtaFacilitiesp[$i]['name'] = 'Radio';
				break;
			case 'Satellite television':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'SV';
				$getGtaFacilitiesp[$i]['name'] = 'Satellite television';
				break;
			case 'Trouser press':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'TP';
				$getGtaFacilitiesp[$i]['name'] = 'Trouser press';
				break;
			case 'Television':
				$getGtaFacilitiesp[$i]['cstr'] = $getGtaFacilitiesp[$i]['icon_class'] = 'TV';
				$getGtaFacilitiesp[$i]['name'] = 'Television';
				break;
			
		}
	}
	//debug($getGtaFacilitiesp);
	return $getGtaFacilitiesp;
}     

function print_star_rating($star_rating=0)
{
	$inverse_star_key = array(0 => 0, 1 => 5, 2 => 4, 3 => 3, 4 => 2, 5 => 1);
	$max_star_rate = 5;
	$min_star_rate = 0;
	$rating = '';
	$current_rate = $inverse_star_key[intval($star_rating)];
	for ($min_star_rate = 1; $min_star_rate <= $max_star_rate; $min_star_rate++) {
	$active_star_rating = (($current_rate == $min_star_rate) == true ? 'active' : '');
	$rating .= '<span class="star '.$min_star_rate.' '.$active_star_rating.'"></span>';
	}
	return $rating;  
} 

function load_sightseen_lib($source) {
    $CI = &get_instance();
    switch ($source) {
        case PROVAB_SIGHTSEEN_BOOKING_SOURCE :
            $CI->load->library('sightseeing/provab_private', '', 'sightseeing_lib');
            break;
        default : redirect(base_url());
    }
}

/**
 * Load Transfer Lib based on transfer source
 * @param string $source
 */
function load_transferv1_lib($source) {
   
    $CI = &get_instance();
 
    switch ($source) {
   
        case PROVAB_TRANSFERV1_BOOKING_SOURCE :
               
            $CI->load->library('transferv1/provab_private', '', 'transferv1_lib');
            
            break;
        default : redirect(base_url());
    }
}