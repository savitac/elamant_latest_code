<?php if (!defined('BASEPATH'))   exit('No direct script access allowed');

function TransferAvailRQ($request,$api_id,$session_data)
{   
	$credentials = TF_getApiCredentials($api_id);
	$postdata= json_decode(base64_decode($request));
	$api_id = $credentials->api_details_id;  
	$searchrequest = '';
	
	
	$client_id = $credentials->api_username1;
	$client_email = $credentials->api_username;
	$password = $credentials->api_password;
	
	//~ $client_id = '40882';
	//~ $client_email = 'XML.PROVAB@CHINAGAP.COM';
	//~ $password = 'CHINA40882';
	
	     $searchrequest = '<?xml version="1.0" encoding="UTF-8" ?>
						<Request>
						<Source>
						<RequestorID Client = "'.$client_id.'" EMailAddress = "'.$client_email.'" Password = "'.$password.'" />
						<RequestorPreferences Country="CA" Currency="CAD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
						</RequestorPreferences>
						</Source>
						<RequestDetails>
						<SearchTransferPriceRequest>
						<TransferPickUp>
						<PickUpCityCode>'.$postdata->transfer_city_code.'</PickUpCityCode>
						<PickUpCode>'.$postdata->pickup_code.'</PickUpCode>
						</TransferPickUp>
						<TransferDropOff>
						<DropOffCityCode>'.$postdata->transfer_city_code.'</DropOffCityCode>
						<DropOffCode>'.$postdata->dropoff_code.'</DropOffCode>
						</TransferDropOff>
						<TransferDate>'.date('Y-m-d', strtotime($postdata->start_date)).'</TransferDate>
						<NumberOfPassengers>'.$postdata->no_of_travellers.'</NumberOfPassengers>
						<PreferredLanguage>e</PreferredLanguage>
						<IncludeChargeConditions DateFormatResponse = "true"/>
						</SearchTransferPriceRequest>
						</RequestDetails>
						</Request>'; 
		
	$xml_type = '';
	$xml_title = 'GetTransferRequest';
	$TransferSearchRS = processRequest_gta($searchrequest,$xml_type,$xml_title, $api_id); 
	
	$searchname = 'SearchTransferPrice_ReqRs/PriceSearchRequest';
    Savelogs($searchname, $searchrequest);
    
    $searchname = 'SearchTransferPrice_ReqRs/PriceSearchResponse';
    Savelogs($searchname, $TransferSearchRS);
    
    formate_result($TransferSearchRS, $searchrequest ,$api_id, $session_data, $postdata);
	
}

function formate_result($TransferSearchRS, $searchrequest, $api_id, $session_data,  $request){
	           
	    $CI =& get_instance();
	    $CI->load->model('general_model');
		$CI->load->model('markup_model');
		
	         $user_nationality = $request->transfer_country;
	         $travel_date = date('Y-m-d', strtotime($request->start_date));
	        
	     
	    $cancel_buffer = $CI->general_model->getCancellationBuffer(19, $api_id)->row();
	 
	    if(empty($cancel_buffer) || $cancel_buffer == '') {
			$buffer_days = 0;
		}
		else {
			$buffer_days = $cancel_buffer->cancel_days;
		}
	
	    $admin_markup_details = $CI->markup_model->get_admin_markup_details($user_nationality, $travel_date, $product_id ='19', $api_id);
	    $b2b_markup_details = $CI->markup_model->get_b2b_markup_details($user_nationality, $travel_date, $product_id ='19', $api_id);
		
		if($CI->session->userdata('user_type') == 4){
		$sub_agent_markup_details = $CI->markup_model->get_sub_agent_markup_details($user_nationality, $travel_date, $product_id ='19', $api_id);	
		$b2b2b_markup_details = $CI->markup_model->get_b2b2b_markup_details($user_nationality, $travel_date, $product_id ='19', $api_id);	
		}
		
		
	            $dom = new DOMDocument();
				$dom->preserveWhiteSpace = false;
				$dom->loadXML($TransferSearchRS);
			    $req = base64_encode(json_encode($request));
				$TransferDetails 	= $dom->getElementsByTagName("TransferDetails");
				if($TransferDetails->length > 0){
					
					foreach($TransferDetails as $transfer_details){
						$temp = array();
						$TransferDetails = $transfer_details->getElementsByTagName("Transfer");
					    if($TransferDetails->length >0){
							foreach($TransferDetails as $item){
								$city_details = $item->getElementsByTagName("City");
								$temp['city_code'] = $city_details->item(0)->getAttribute('Code');
								$temp['city_name'] = $city_details->item(0)->nodeValue;
								$Item = $item->getElementsByTagName('Item');
								$temp['item_code'] = trim($Item->item(0)->getAttribute('Code'));
								$temp['item_description'] = $Item->item(0)->nodeValue; 
								
								$PickUpDetails = $item->getElementsByTagName('PickUpDetails');
								
								foreach($PickUpDetails as $pickupdetails){
									$PickUp = $pickupdetails->getElementsByTagName('PickUp');
									 $temp['pickup_code'] = $pickup_code = $PickUp->item(0)->getAttribute('Code'); 
									 $temp['pickup_code_description'] = $PickUp->item(0)->nodeValue;
									if($pickup_code ==  'A'){
										 $Airport = $pickupdetails->getElementsByTagName('Airport');
										  $temp['pickup_city_code'] = $Airport->item(0)->getAttribute('Code');  
										  $temp['pickup_city_code_description'] = $Airport->item(0)->nodeValue;
									}elseif($pickup_code ==  'H'){
										$City = $pickupdetails->getElementsByTagName('City');
										if($City->length > 0){
										$temp['pickup_city_code'] = $City->item(0)->getAttribute('Code'); 
										$temp['pickup_city_code_description'] = $City->item(0)->nodeValue;
										} else{
										$Area = $pickupdetails->getElementsByTagName('Area');	
									    $temp['pickup_city_code']= $Area->item(0)->getAttribute('Code'); 
										$temp['pickup_city_code_description'] = $Area->item(0)->nodeValue;
										}
									}elseif($pickup_code ==  'P'){
										$City = $pickupdetails->getElementsByTagName('City');
										$temp['pickup_city_code'] = $City->item(0)->getAttribute('Code'); 
										$temp['pickup_city_code_description'] = $City->item(0)->nodeValue;
									}elseif($pickup_code ==  'S'){
										$City = $pickupdetails->getElementsByTagName('City');
										$temp['pickup_city_code'] = $City->item(0)->getAttribute('Code'); 
										$temp['pickup_city_code_description'] = $City->item(0)->nodeValue;
									}elseif($pickup_code ==  'O'){
										$City = $pickupdetails->getElementsByTagName('City');
										$temp['pickup_city_code'] = $City->item(0)->getAttribute('Code'); 
										$temp['pickup_city_code_description'] = $City->item(0)->nodeValue;
									}
									
								}
								
								$DropOffDetails = $item->getElementsByTagName('DropOffDetails');
								foreach($DropOffDetails as $dropoffdetails){
									$DropOff = $dropoffdetails->getElementsByTagName('DropOff');
									$temp['dropoff_code'] = $dropoff_code = $DropOff->item(0)->getAttribute('Code'); 
									$temp['dropoff_code_description'] = $DropOff->item(0)->nodeValue;
									if($dropoff_code ==  'A'){
										$Airport1 = $dropoffdetails->getElementsByTagName('Airport');
										 $temp['dropoff_city_code'] = $Airport1->item(0)->getAttribute('Code'); 
										 $temp['dropoff_city_code_description'] = $Airport1->item(0)->nodeValue;
									}elseif($dropoff_code ==  'H'){
										$City = $dropoffdetails->getElementsByTagName('City');
										if($City->length > 0){
										$temp['dropoff_city_code']= $City->item(0)->getAttribute('Code'); 
										$temp['dropoff_city_code_description'] = $City->item(0)->nodeValue;
									    }else{
										$City = $dropoffdetails->getElementsByTagName('Area');	
									    $temp['dropoff_city_code']= $City->item(0)->getAttribute('Code'); 
										$temp['dropoff_city_code_description'] = $City->item(0)->nodeValue;
										}
										
									}elseif($dropoff_code ==  'P'){
										$City = $dropoffdetails->getElementsByTagName('City');
										$temp['dropoff_city_code']= $City->item(0)->getAttribute('Code'); 
										$temp['dropoff_city_code_description'] = $City->item(0)->nodeValue;
									}elseif($dropoff_code ==  'S'){
										$City = $dropoffdetails->getElementsByTagName('City');
										$temp['dropoff_city_code']= $City->item(0)->getAttribute('Code'); 
										$temp['dropoff_city_code_description'] = $City->item(0)->nodeValue;
									}elseif($dropoff_code ==  'O'){
										$City = $dropoffdetails->getElementsByTagName('City');
										$temp['dropoff_city_code']= $City->item(0)->getAttribute('Code'); 
										$temp['dropoff_city_code_description'] = $City->item(0)->nodeValue;
									}
								}
								
								$supplementary_details = array();
								$OutOfHoursSupplementsDetails = $item->getElementsByTagName('OutOfHoursSupplements');
								if($OutOfHoursSupplementsDetails->length > 0){
									foreach($OutOfHoursSupplementsDetails as $supplement_details) {
										$OutOfHoursSupplement = $supplement_details->getElementsByTagName('OutOfHoursSupplement');
										if($OutOfHoursSupplement->length > 0){
											$s =0;
											foreach($OutOfHoursSupplement as $supplementary){
												$FromTime = $supplementary->getElementsByTagName('FromTime');
												$ToTime = $supplementary->getElementsByTagName('ToTime');
												$Supplement = $supplementary->getElementsByTagName('Supplement');
												$supplementary_details['supplement'][$s]['FromTime'] = $from_time = $FromTime->item(0)->nodeValue;
												$supplementary_details['supplement'][$s]['ToTime'] = $to_time = $ToTime->item(0)->nodeValue;
												$supplementary_details['supplement'][$s]['Supplement'] = $supplement = $Supplement->item(0)->nodeValue;
												$s = $s+1;
											}
										}
									}
							    }
							    
							  $temp['supplementary_details'] = json_encode($supplementary_details);
							  
							  $ApproximateTransferTime = $item->getElementsByTagName('ApproximateTransferTime');
							  $temp['approximate_time'] = $ApproximateTransferTime->item(0)->getAttribute('Time'); 
							  $temp['approximate_transfer_time'] = $ApproximateTransferTime->item(0)->nodeValue;
							  $vehicle_array = array();
							  $TransferVehicles = $item->getElementsByTagName('TransferVehicles');
					    if($TransferVehicles->length >0 ){
							foreach($TransferVehicles as $t_vehicles){
									  $TransferVehicle = $t_vehicles->getElementsByTagName('TransferVehicle');
									 $v =0;
									  if($TransferVehicle->length > 0){
										  foreach($TransferVehicle as $vehicle){
											 $Vehicle = $vehicle->getElementsByTagName('Vehicle');
										     $temp['vehicle_code'] = $vehicle_code = $Vehicle->item(0)->getAttribute('Code'); 
										     $temp['maximum_luggage'] = $maximum_luggage=  $Vehicle->item(0)->getAttribute('MaximumLuggage'); 
											 $temp['maximum_passengers'] = $maximum_passengers=  $Vehicle->item(0)->getAttribute('MaximumPassengers');
											 $temp['vehicle_name'] = $vehicle_name =  $Vehicle->item(0)->nodeValue;
											 $vehicle_array = array('vehicle_code' => $vehicle_code,
											                         'maximum_luggage' => $maximum_luggage,
											                         'maximum_passengers' => $maximum_passengers,
											                         'vehicle_name' => $vehicle_name);
											
										
									 
									 $temp['vehicle_details'] = json_encode($vehicle_array);
									 $ItemPrice =  $vehicle->getElementsByTagName('ItemPrice');
									 $temp['api_currecny'] = $ItemPrice->item(0)->getAttribute('Currency'); 
									 $temp['api_price'] = $TotalPrice_v= $ItemPrice->item(0)->nodeValue;
									 
									  $Confirmation =  $vehicle->getElementsByTagName('Confirmation');
									 $temp['confirmation_code'] = $Confirmation->item(0)->getAttribute('Code'); 
									 $temp['confirmation_description'] = $Confirmation->item(0)->nodeValue;
									
									 $admin_markup = $b2b_markup = $b2b2_markup = $b2b_agent_markup = $b2b2b_agent_markup = 0;
									 $admin_base_price = $b2b_base_price = $b2b2b_base_price = $b2b_agent_base_price = $b2b2b_agent_base_price = 0;
									
										$total_price_with_markup = $TotalPrice_v;
										if($admin_markup_details != '' && count($admin_markup_details) > 0) {
										if($admin_markup_details[0]->markup_value_type == "Percentage") {
											$admin_markup = $TotalPrice_v* ($admin_markup_details[0]->markup_value / 100);
											$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
										}else{
											$admin_markup = $admin_markup_details[0]->markup_fixed;
											$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
										}
										}else{
											$admin_markup = 0;
											$admin_base_price = $total_price_with_markup;
										}
										
										
										if($b2b_markup_details != '' && count($b2b_markup_details) >0) {
										if($b2b_markup_details[0]->markup_value_type == "Percentage") {
											$b2b_markup = $admin_base_price* ($b2b_markup_details[0]->markup_value / 100);
											$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
										} else {
											$b2b_markup = $b2b_markup_details[0]->markup_value;
											$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
										}
									}else{
										$b2b_markup = 0;
										$b2b_base_price = $total_price_with_markup;
									}
									
											if($CI->session->userdata('user_type') == 4){
												if($sub_agent_markup_details != '' && count($sub_agent_markup_details) > 0){
												if($sub_agent_markup_details[0]->markup_value_type == "Percentage") {
												$b2b_agent_markup = $b2b_base_price* ($sub_agent_markup_details[0]->markup_value / 100);
												$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
													} else {
												$b2b_agent_markup = $sub_agent_markup_details[0]->markup_value;
												$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
											   }
										   }else{
											   $b2b_agent_markup = 0;
											   $b2b_agent_base_price = $total_price_with_markup;
										   }
											  
											  if($b2b2b_markup_details != '' && count($b2b2b_markup_details) > 0){
											  if($b2b2b_markup_details[0]->markup_value_type == "Percentage") {
												$b2b2b_agent_markup = $b2b_agent_base_price * ($b2b2b_markup_details[0]->markup_value / 100);
												$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
												} else {
												$b2b2b_agent_markup = $b2b2b_markup_details[0]->markup_value;
												$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
											  }
										   }else{
											$b2b2b_agent_markup = 0;
										   $b2b2b_agent_base_price = $total_price_with_markup; 
										   }
										}
										
									 $service_charge = 0;
									 $gst_charge = 0;
									 $tax_charge = 0;
				   
									if($CI->session->userdata('user_country_id') != 0){
										$country_name = $CI->general_model->get_country_name($CI->session->userdata('user_country_id'))->row();
										$tax_details = $CI->general_model->get_tax_details($country_name->country_name, $CI->session->userdata('user_state_id'))->row();
										
										if(count($tax_details) > 0){
										 $price_with_markup_tax = $CI->general_model->tax_calculation($tax_details, $total_price_with_markup);
										 if(count($price_with_markup_tax) > 0){
											 $service_charge = $price_with_markup_tax['sc_tax'];
											 $gst_charge = $price_with_markup_tax['gst_tax'];
											 $tax_charge = $price_with_markup_tax['state_tax'];
											  $total_price_with_markup_tax = $price_with_markup_tax['amount'];
											}
										}else{
										 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
										}
									}else{
										 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
									}
			   
			                            $temp['total_cost'] = $total_price_with_markup;
										$temp['amount'] = $total_price_with_markup_tax;
										$temp['admin_markup'] = $admin_markup;
										
										$temp['admin_baseprice'] = $admin_base_price;
										$temp['my_markup'] = $b2b_markup;
										$temp['agent_baseprice'] = $b2b_base_price;
										
										$temp['my_agent_Markup'] = $b2b_agent_markup;
										$temp['sub_agent_baseprice'] = $b2b_agent_base_price;
										$temp['my_b2b2b_markup'] = $b2b2b_agent_markup;
										$temp['b2b2b_baseprice'] = $b2b2b_agent_base_price;
										
										$temp['service_charge'] = $service_charge;
										$temp['gst_charge'] = $gst_charge;
										$temp['tax_charge'] = $tax_charge;
			    
									$Charge = array();
									 $ChargeConditions =  $vehicle->getElementsByTagName('ChargeConditions');
									 $cancellation_till_date = "";
									 if($ChargeConditions->length >0){
										
										 foreach($ChargeConditions as $cancelaltionConditions){
											$ChargeCondition = $cancelaltionConditions->getElementsByTagName('ChargeCondition');
											$i =0;
											foreach($ChargeCondition as $crg_conditons){
												$ChargeConditionType = $ChargeCondition->item($i)->getAttribute('Type');
												$Condition = $crg_conditons->getElementsByTagName('Condition');
												$j = 0;
												foreach($Condition as $cond){
													$Charge[$ChargeConditionType][$j]['Charge'] = $cond->getAttribute('Charge');
													
													if($Charge[$ChargeConditionType][$j]['Charge'] == 'true'){
													//$Charge[$ChargeConditionType][$j]['ChargeAmount'] =  $cond->getAttribute('ChargeAmount');
													  $ChargeAmount = $cond->getAttribute('ChargeAmount');
													 $Charge[$ChargeConditionType][$j]['ChargeAmount'] = $ChargeAmount+$admin_markup+$b2b_markup+$b2b_agent_markup+$b2b2b_agent_markup;
													 $Charge[$ChargeConditionType][$j]['Currency'] = $cond->getAttribute('Currency');
												    }
												    
													//~ $Charge[$ChargeConditionType][$j]['FromDate'] = $cond->getAttribute('FromDate');
													//~ $Charge[$ChargeConditionType][$j]['ToDate'] = $cond->getAttribute('ToDate');
													
													$FromDate = $cond->getAttribute('FromDate');
													if($j == 0){
													  $cancellation_till_date = $FromDate;	
													}
													$fromDateString = date('Y-m-d', strtotime($FromDate. "-".$buffer_days." days"));
													$Charge[$ChargeConditionType][$j]['FromDate'] = $fromDateString;
													$todate = $cond->getAttribute('ToDate');
													if(isset($todate) && $todate != ''){
														$ToDate = $todate;
														$toDateString = date('Y-m-d', strtotime($ToDate. "-".$buffer_days." days"));
													    $Charge[$ChargeConditionType][$j]['ToDate'] = $toDateString;
													}
													
												 $j = $j+1;
												}
										     $i =$i+1;	
											}
										}
									
								    $temp['caneclation_policy'] = json_encode($Charge);
								  
									$transfer_conditions = '';
									$TransferConditions = $item->getElementsByTagName('TransferConditions');	
									if($TransferConditions->length > 0){
										foreach($TransferConditions as $trans_cond){
											$transfer_conditions .= $trans_cond->nodeValue."|||";
										}
									}
									
									$temp['transfer_conditions']	= $transfer_conditions;
									$temp['transfer_request'] = $req;
									$temp['travel_start_date'] = date('Y-m-d', strtotime($request->start_date));
									$temp['no_of_passengers'] =  $request->no_of_travellers;
									$temp['no_of_vehicles'] = $no_of_vehicles =   ceil($request->no_of_travellers/$maximum_passengers);
									$temp['session_id'] = $session_data;
									$temp['cancellation_till_date'] = $cancellation_till_date;
									$temp['api_id'] = $api_id;
									}
									$temp_transfer_results = $CI->general_model->insert_transfer_result($temp); 
									
									$temp['total_cost'] = $total_price_with_markup * $no_of_vehicles;
									$temp['amount'] = $total_price_with_markup_tax * $no_of_vehicles;
									
									    $temp['admin_baseprice'] = $admin_base_price * $no_of_vehicles;
										$temp['my_markup'] = $b2b_markup * $no_of_vehicles;
										$temp['agent_baseprice'] = $b2b_base_price * $no_of_vehicles;
										
										$temp['my_agent_Markup'] = $b2b_agent_markup * $no_of_vehicles;
										$temp['sub_agent_baseprice'] = $b2b_agent_base_price * $no_of_vehicles;
										$temp['my_b2b2b_markup'] = $b2b2b_agent_markup * $no_of_vehicles;
										$temp['b2b2b_baseprice'] = $b2b2b_agent_base_price * $no_of_vehicles;
									
									$temp_vehicle_results = $CI->general_model->insert_vehicle_result($temp, $temp_transfer_results); 
								    $v=  $v+1; 
								}
								
							  }
							
							 }
						
						}
					}
				   }
					}
				
				}
			return;
 }
 
 function AddBookingRequest($cart_transfer_data, $passenger_info_c,$checkout_form, $billingaddress_c, $booking_id,$parent_pnr,$cid){
	 
	  $credentials = TF_getApiCredentials($cart_transfer_data->api_id);
	  $postdata= json_decode(base64_decode($cart_transfer_data->transfer_request));
	  $api_id = $credentials->api_details_id; 
	
	 $client_id = $credentials->api_username1;
	$client_email = $credentials->api_username;
	$password = $credentials->api_password;
	
	//~ $client_id = '40882';
	//~ $client_email = 'XML.PROVAB@CHINAGAP.COM';
	//~ $password = 'CHINA40882';
	
	   /* $searchrequest = '<?xml version="1.0" encoding="UTF-8" ?>
						<Request>
						<Source>
						<RequestorID Client = "'.$client_id.'" EMailAddress = "'.$client_email.'" Password = "'.$password.'" />
						<RequestorPreferences Country="AU" Currency="AUD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
						</RequestorPreferences>
						</Source>
						<RequestDetails>
						<AddBookingRequest Currency="AUD">
						<BookingName>'.$parent_pnr.'asd</BookingName>
						<BookingReference>'.$parent_pnr.'asd</BookingReference>
						<PaxNames>'; */
				//for($item =1; $item <= $cart_transfer_data->no_of_vehicles; $item++) {
						
					$searchrequest = '<?xml version="1.0" encoding="UTF-8" ?>
						<Request>
						<Source>
						<RequestorID Client = "'.$client_id.'" EMailAddress = "'.$client_email.'" Password = "'.$password.'" />
						<RequestorPreferences Country="CA" Currency="CAD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
						</RequestorPreferences>
						</Source>
						<RequestDetails>
						<AddBookingRequest Currency="AUD">
						<BookingName>'.$parent_pnr.'</BookingName>
						<BookingReference>'.$parent_pnr.'</BookingReference>
						<PaxNames>';
						
						
					$checkout_form = json_decode(json_encode($checkout_form), true);
					
				
					$firstname ="first_name".$cid;
					$lastname ="last_name".$cid;
					$flight_code = "flight_code".$cid;
					$flight_number = "flight_number".$cid;
					$flight_code1 = "flight_code1".$cid;
					$flight_number1 = "flight_number1".$cid;
					$hotel_code = "hotel_code".$cid;
					$hotel_code1 = "hotel_code1".$cid;
					$port_code = "port_code".$cid;
					$ship_name = "ship_name".$cid;
					$ship_company = "ship_company".$cid;
					$ship_name1 = "ship_name1".$cid;
					$ship_company1 = "ship_company1".$cid;
					$arrival_time = "arrival_time".$cid;
					$departure_time = "departure_time".$cid;
					$address1 = "address1".$cid;
					$address2 = "address2".$cid;
					$city = "city".$cid;
					$telephone = "telephone".$cid;
					$station_code = "station_code".$cid;
					$train_name = "train_name".$cid;
					$station_code1 = "station_code1".$cid;
					$train_name1 = "train_name1".$cid;
					
					
					
					for($pass =0; $pass < count($checkout_form[$firstname]); $pass++){
						 $sno = $pass +1;
						 $searchrequest .='<PaxName PaxId = "'.$sno.'">'.$checkout_form[$firstname][$pass]." ".$checkout_form[$lastname][$pass].'</PaxName>';
					}
				
						
		$searchrequest .='</PaxNames><BookingItems>';
	    
	   
	   
		$searchrequest .='<BookingItem ItemType = "transfer">
						<ItemReference>1</ItemReference>
						<ItemCity Code = "'.$cart_transfer_data->search_city_code.'" />
						<Item Code = "'.$cart_transfer_data->item_code.'"/>
						<TransferItem>';
						
						
		//PickUpAirport, DropOffAccomodtion				
		if($cart_transfer_data->pickup_code == 'A' && $cart_transfer_data->dropoff_code == 'H') {			
		$searchrequest .='<PickUpDetails>
						<PickUpAirport>
						<ArrivingFrom>
						<Airport Code="'.$checkout_form[$flight_code].'" />
						</ArrivingFrom>
						<FlightNumber>'.$checkout_form[$flight_number].'</FlightNumber>
						<EstimatedArrival>14.45</EstimatedArrival>
						</PickUpAirport>
						</PickUpDetails>
						<DropOffDetails>
						<DropOffAccommodation>
						<DropOffTo>
						<Hotel Code="'.$checkout_form[$hotel_code].'"/>
						</DropOffTo>
						</DropOffAccommodation>
						</DropOffDetails>';
       }
       //PickUpAirport, DropOffAirport			
       elseif($cart_transfer_data->pickup_code == 'A' && $cart_transfer_data->dropoff_code == 'A') {			
		$searchrequest .='<PickUpDetails>
							<PickUpAirport>
							<ArrivingFrom>
							<Airport Code="'.$checkout_form[$flight_code].'"/>
							</ArrivingFrom>
							<FlightNumber>'.$checkout_form[$flight_number].'</FlightNumber>
							<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
							</PickUpAirport>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffAirport>
							<DepartingTo>
							<Airport Code="'.$checkout_form[$flight_code1].'" />
							</DepartingTo>
							<FlightNumber>'.$checkout_form[$flight_number1].'</FlightNumber>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffAirport>
							</DropOffDetails>';
       }
       //PickUpAirport, DropOffStation
       elseif($cart_transfer_data->pickup_code == 'A' && $cart_transfer_data->dropoff_code == 'S') {			
		$searchrequest .= '<PickUpDetails>
							<PickUpAirport>
							<ArrivingFrom>
							<Airport Code="'.$checkout_form[$flight_code].'" />
							</ArrivingFrom>
							<FlightNumber>'.$checkout_form[$flight_number].'</FlightNumber>
							<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
							</PickUpAirport>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffStation>
							<Station Code="'.$checkout_form[$station_code].'" />
							<DepartingTo>
							<City Code="'.$cart_transfer_data->dropoff_city_code.'"/>
							</DepartingTo>
							<TrainName>'.$checkout_form[$train_name].'</TrainName>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffStation>
							</DropOffDetails>';
       } 
       //PickUPAirport, DroppOffPort
       elseif($cart_transfer_data->pickup_code == 'A' && $cart_transfer_data->dropoff_code == 'P') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpAirport>
							<ArrivingFrom>
							<Airport Code="'.$checkout_form[$flight_code].'"/>
							</ArrivingFrom>
							<FlightNumber>'.$checkout_form[$flight_number].'</FlightNumber>
							<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
							</PickUpAirport>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffPort>
							<DepartingTo>'.$cart_transfer_data->dropoff_city_code_description.'</DepartingTo>
							<ShipName>'.$checkout_form[$ship_name].'</ShipName>
							<ShippingCompany>'.$checkout_form[$ship_company].'</ShippingCompany>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffPort>
							</DropOffDetails>';
       } 
       //PickUpAirport, DroppOffOther
       elseif($cart_transfer_data->pickup_code == 'A' && $cart_transfer_data->dropoff_code == 'O') {			
	    $searchrequest .= '<PickUpDetails>
						   <PickUpAirport>
						   <ArrivingFrom>
						   <Airport Code="'.$checkout_form[$flight_code].'" />
						   </ArrivingFrom>
						   <FlightNumber>'.$checkout_form[$flight_number1].'</FlightNumber>
						   <EstimatedArrival>10.25</EstimatedArrival>
						   </PickUpAirport>
						   </PickUpDetails>
						   <DropOffDetails>
						   <DropOffOther>
						   <DropOffTo>
						   <MeetingPoint Code="HEL"/>
						   </DropOffTo>
						   </DropOffOther>
						  </DropOffDetails>';
       } 
       //PickUpStation, DropOffAirport
       elseif($cart_transfer_data->pickup_code == 'S' && $cart_transfer_data->dropoff_code == 'A') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpStation>
							<Station Code="'.$checkout_form[$station_code].'"/>
							<ArrivingFrom>
							<City Code="'.$cart_transfer_data->pickup_city_code.'"/>
							</ArrivingFrom>
							<TrainName>'.$checkout_form[$train_name].'</TrainName>
							<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
							</PickUpStation>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffAirport>
							<DepartingTo>
							<Airport Code="'.$checkout_form[$flight_code].'"/>
							</DepartingTo>
							<FlightNumber>'.$checkout_form[$flight_number].'</FlightNumber>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffAirport>
							</DropOffDetails>';
       }
       //PickUpStation, DropOffStation
       elseif($cart_transfer_data->pickup_code == 'S' && $cart_transfer_data->dropoff_code == 'S') {			
	    $searchrequest .= '<PickUpDetails>
						   <PickUpStation>
						    <Station Code="'.$checkout_form[$station_code].'"/>
							<ArrivingFrom>
							<City Code="'.$cart_transfer_data->pickup_city_code.'"/>
							</ArrivingFrom>
							<TrainName>'.$checkout_form[$train_name].'</TrainName>
							<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
							</PickUpStation>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffStation>
							<Station Code="'.$checkout_form[$station_code1].'"/>
							<DepartingTo>
							<City Code="'.$cart_transfer_data->dropoff_city_code.'" />
							</DepartingTo>
							<TrainName>'.$checkout_form[$train_name1].'</TrainName>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffStation>
							</DropOffDetails>';
       } 
       //PickUpStation, DropOffPort
        elseif($cart_transfer_data->pickup_code == 'S' && $cart_transfer_data->dropoff_code == 'P') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpStation>
							<Station Code="'.$checkout_form[$station_code].'" />
							<ArrivingFrom>
							<City Code="'.$cart_transfer_data->pickup_city_code.'"/>
							</ArrivingFrom>
							<TrainName>'.$checkout_form[$train_name].'</TrainName>
							<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
							</PickUpStation>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffPort>
							<DepartingTo>'.$cart_transfer_data->dropoff_city_code_description.'</DepartingTo>
							<ShipName>'.$checkout_form[$ship_name].'</ShipName>
							<ShippingCompany>'.$checkout_form[$ship_company].'</ShippingCompany>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffPort>
							</DropOffDetails>';
       } 
       //PickUpStation, DropOffAccommodation 
       elseif($cart_transfer_data->pickup_code == 'S' && $cart_transfer_data->dropoff_code == 'H') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpStation>
							<Station Code="'.$checkout_form[$station_code].'"/>
							<ArrivingFrom>
							<City Code="'.$cart_transfer_data->pickup_city_code.'"/>
							</ArrivingFrom>
							<TrainName>'.$checkout_form[$train_name].'</TrainName>
							<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
							</PickUpStation>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffAccommodation>
							<DropOffAddress>
							<AddressLine1>'.$checkout_form[$address1].'</AddressLine1>
							<AddressLine2>'.$checkout_form[$address2].'</AddressLine2>
							<City>'.$checkout_form[$city].'</City>
							<Telephone>'.$checkout_form[$telephone].'</Telephone>
							</DropOffAddress>
							</DropOffAccommodation>
							</DropOffDetails>';
       } 
       //PickUpStation, DropOffOther
       elseif($cart_transfer_data->pickup_code == 'S' && $cart_transfer_data->dropoff_code == 'O') {			
	    $searchrequest .= '<PickUpDetails>
						<PickUpStation>
						<Station Code="BRUS03"/>
						<ArrivingFrom>
						<Airport Code="AMS"/>
						</ArrivingFrom>
						<TrainName>City Flyer</TrainName>
						<EstimatedArrival>08.05</EstimatedArrival>
						</PickUpStation>
						</PickUpDetails>
						<DropOffDetails>
						<DropOffOther>
						<DropOffTo>
						<Hotel Code="BRI"/>
						</DropOffTo>
						</DropOffOther>
						</DropOffDetails>';
       } 
       //PickUpPort, DropOffAirport
        elseif($cart_transfer_data->pickup_code == 'P' && $cart_transfer_data->dropoff_code == 'A') {			
	    $searchrequest .= '<PickUpDetails>
							<City Code="'.$cart_transfer_data->pickup_city_code.'">'.$cart_transfer_data->pickup_city_code_description.'</City>
							<PickUpPort>
							<ArrivingFrom>'.$cart_transfer_data->pickup_city_code_description.'</ArrivingFrom>
							<ShipName>'.$checkout_form[$ship_name].'</ShipName>
							<ShippingCompany>'.$checkout_form[$ship_company].'</ShippingCompany>
							<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
							</PickUpPort>
							</PickUpDetails>
							<DropOffDetails>
							<City Code="'.$cart_transfer_data->dropoff_city_code.'">'.$cart_transfer_data->dropoff_city_code_description.'</City>
							<DropOffAirport>
							<DepartingTo>
							<Airport Code="'.$checkout_form[$flight_code].'" />
							</DepartingTo>
							<FlightNumber>'.$checkout_form[$flight_number].'</FlightNumber>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffAirport>
							</DropOffDetails>';
       }  
       //PickUpPort, DropOffAccommodation
       elseif($cart_transfer_data->pickup_code == 'P' && $cart_transfer_data->dropoff_code == 'H') {
		$searchrequest .= '<PickUpDetails>
						<PickUpPort>
						<ArrivingFrom>'.$cart_transfer_data->pickup_city_code_description.'</ArrivingFrom>
						<ShipName>'.$checkout_form[$ship_name].'</ShipName>
						<ShippingCompany>'.$checkout_form[$ship_company].'</ShippingCompany>
						<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
						</PickUpPort>
						</PickUpDetails>
						<DropOffDetails>
						<DropOffAccommodation>
						<DropOffTo>
						<Hotel Code="'.$checkout_form[$hotel_code].'"/>
						</DropOffTo>
						</DropOffAccommodation>
						</DropOffDetails>';
       }
       //PickUpPort, DropOffStation
       elseif($cart_transfer_data->pickup_code == 'P' && $cart_transfer_data->dropoff_code == 'S') {			
	    $searchrequest .= '<PickUpDetails>
							<City Code="'.$cart_transfer_data->pickup_city_code.'">'.$cart_transfer_data->pickup_city_code_description.'</City>
							<PickUpPort>
							<ArrivingFrom>'.$cart_transfer_data->pickup_city_code_description.'</ArrivingFrom>
							<ShipName>'.$checkout_form[$ship_name].'</ShipName>
							<ShippingCompany>'.$checkout_form[$ship_company].'</ShippingCompany>
							<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
							</PickUpPort>
							</PickUpDetails>
							<DropOffDetails>
							<City Code="'.$cart_transfer_data->dropoff_city_code.'">'.$cart_transfer_data->dropoff_city_code_description.'</City>
							<DropOffStation>
							<Station Code="'.$checkout_form[$station_code].'"/>
							<DepartingTo>
							<City Code="'.$cart_transfer_data->dropoff_city_code.'"/>
							</DepartingTo>
							<TrainName>'.$checkout_form[$train_name].'</TrainName>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffStation>
							</DropOffDetails>';
       } 
       //PickUpPort, DropOffPort
       elseif($cart_transfer_data->pickup_code == 'P' && $cart_transfer_data->dropoff_code == 'P') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpPort>
							<ArrivingFrom>'.$cart_transfer_data->pickup_city_code_description.'</ArrivingFrom>
							<ShipName>'.$checkout_form[$ship_name].'</ShipName>
							<ShippingCompany>'.$checkout_form[$ship_company].'</ShippingCompany>
							<EstimatedArrival>'.$checkout_form[$arrival_time].'</EstimatedArrival>
							</PickUpPort>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffPort>
							<DepartingTo>'.$cart_transfer_data->dropoff_city_code_description.'</DepartingTo>
							<ShipName>'.$checkout_form[$ship_name1].'</ShipName>
							<ShippingCompany>'.$checkout_form[$ship_company1].'</ShippingCompany>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffPort>
							</DropOffDetails>';
       } 
       //PickUPort, DropOffOther
       elseif($cart_transfer_data->pickup_code == 'P' && $cart_transfer_data->dropoff_code == 'O') {			
	    $searchrequest .= '<PickUpDetails>
							<City Code="AMS"><![CDATA[Amsterdam]]></City>
							<PickUpPort>
							<ArrivingFrom>AMS</ArrivingFrom>
							<ShipName>The Queen Elizabeth 2</ShipName>
							<ShippingCompany>Cunard</ShippingCompany>
							<EstimatedArrival>10.00</EstimatedArrival>
							</PickUpPort>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffOther>
							<DropOffAddress>
							<AddressLine1>Goswell Road</AddressLine1>
							<PostCode>123</PostCode>
							</DropOffAddress>
							</DropOffDetails>';
       } 
       //PickUpAccommodation, DropOffAirport
       elseif($cart_transfer_data->pickup_code == 'H' && $cart_transfer_data->dropoff_code == 'A') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpAccommodation>
							<PickUpFrom>
							<Hotel Code="'.$checkout_form[$hotel_code].'" />
							</PickUpFrom>
							<PickUpTime>'.$checkout_form[$arrival_time].'</PickUpTime>
							</PickUpAccommodation>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffAirport>
							<DepartingTo>
							<Airport Code="'.$checkout_form[$flight_code].'"/>
							</DepartingTo>
							<FlightNumber>'.$checkout_form[$flight_number].'</FlightNumber>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffAirport>
							</DropOffDetails>';
       } 
       //PickUpAccommodation, DropOffStation
       elseif($cart_transfer_data->pickup_code == 'H' && $cart_transfer_data->dropoff_code == 'S') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpAccommodation>
							<PickUpFrom>
							<Hotel Code="'.$checkout_form[$hotel_code].'" />
							</PickUpFrom>
							<PickUpTime>'.$checkout_form[$arrival_time].'</PickUpTime>
							</PickUpAccommodation>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffStation>
							<Station Code="'.$checkout_form[$station_code].'"/>
							<DepartingTo>
							<City Code="'.$cart_transfer_data->dropoff_city_code.'"/>
							</DepartingTo>
							<TrainName>'.$checkout_form[$train_name].'</TrainName>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffStation>
							</DropOffDetails>';
       } 
       //PickUpAccommodation, DropOffPort
        elseif($cart_transfer_data->pickup_code == 'H' && $cart_transfer_data->dropoff_code == 'P') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpAccommodation>
							<PickUpFrom>
							<Hotel Code="'.$checkout_form[$hotel_code].'"  />
							</PickUpFrom>
							<PickUpTime>'.$checkout_form[$arrival_time].'</PickUpTime>
							</PickUpAccommodation>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffPort>
							<DepartingTo>'.$cart_transfer_data->dropoff_city_code_description.'</DepartingTo>
							<ShipName>'.$checkout_form[$ship_name].'</ShipName>
							<ShippingCompany>'.$checkout_form[$ship_company].'</ShippingCompany>
							<DepartureTime>'.$checkout_form[$departure_time].'</DepartureTime>
							</DropOffPort>
							</DropOffDetails>';
       } 
       //PickUpAccommodation, DropOffAccommodation
        elseif($cart_transfer_data->pickup_code == 'H' && $cart_transfer_data->dropoff_code == 'H') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpAccommodation>
							<PickUpFrom>
							<Hotel Code="'.$checkout_form[$hotel_code].'" />
							</PickUpFrom>
							<PickUpTime>'.$checkout_form[$arrival_time].'</PickUpTime>
							</PickUpAccommodation>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffAccommodation>
							<DropOffTo>
							<Hotel Code="'.$checkout_form[$hotel_code1].'" />
							</DropOffTo>
							</DropOffAccommodation>
							</DropOffDetails>';
       } 
       //PickUpAccommodation, DropOffOther
       elseif($cart_transfer_data->pickup_code == 'H' && $cart_transfer_data->dropoff_code == 'O') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpAccommodation>
							<PickUpFrom>
							<Hotel Code=”ABA” />
							</PickUpFrom>
							<PickUpTime>13.45</PickUpTime>
							</PickUpAccommodation>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffOther>
							<DropOffAddress>
							<AddressLine1>56 Gloucester
							Road</AddressLine1>
							<AddressLine2>Kensington</AddressLine2>
							<PostCode>SW5 4ER</PostCode>
							</DropOffAddress>
							</DropOffOther>
							</DropOffDetails>';
       } 
       //PickUpOther, DropOffAirport
       elseif($cart_transfer_data->pickup_code == 'O' && $cart_transfer_data->dropoff_code == 'A') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpOther>
							<PickUpFrom>
							<MeetingPoint Code="STA">
							</PickUpFrom>
							<PickUpTime>13.00</PickUpTime>
							</PickUpOther>
							</PickUpDetails>
							<DropOffAirport>
							<DepartingTo>
							<Airport Code="MAD"/>
							</DepartingTo>
							<FlightNumber>AF111</FlightNumber>
							<DepartureTime>17.05</DepartureTime>
							</DropOffAirport>
							</DropOffDetails>';
       }  
       //PickUpOther, DropOffStation
        elseif($cart_transfer_data->pickup_code == 'O' && $cart_transfer_data->dropoff_code == 'S') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpOther>
							<PickUpFrom>
							<Hotel Code="BRI">
							</PickUpFrom>
							<PickUpTime>18.30</PickUpTime>
							</PickUpOther>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffStation>
							<Station Code="BRUS05"/>
							<DepartingTo>
							<City Code="BRUG"/>
							</DepartingTo>
							<TrainName>Benelux Express</TrainName>
							<DepartureTime>19.50</DepartureTime>
							</DropOffStation>
							</DropOffDetails>';
       } 
       //PickUpOther, DropOffPort
        elseif($cart_transfer_data->pickup_code == 'O' && $cart_transfer_data->dropoff_code == 'P') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpOther>
							<PickUpFrom>
							<Apartment Code="ALL1">
							</PickUpFrom>
							<PickUpTime>16.30</PickUpTime>
							</PickUpOther>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffPort>
							<DepartingTo>Venice</DepartingTo>
							<ShipName>Mary Rose</ShipName>
							<ShippingCompany>Cunard</ShippingCompany>
							<DepartureTime>18.30</DepartureTime>
							</DropOffPort>
							</DropOffDetails>';
       }
        //PickUpOther, DropOffAccommodation
        elseif($cart_transfer_data->pickup_code == 'O' && $cart_transfer_data->dropoff_code == 'H') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpOther>
							<PickUpFrom>
							<Hotel Code="'.$checkout_form[$hotel_code].'" />
							</PickUpFrom>
							<PickUpTime>'.$checkout_form[$arrival_time].'</PickUpTime>
							</PickUpOther>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffAccommodation>
							<DropOffTo>
							<Hotel Code="'.$checkout_form[$hotel_code1].'" />
							</DropOffTo>
							</DropOffAccommodation>
							</DropOffDetails>';
       }
        //PickUpOther, DropOffOther
       elseif($cart_transfer_data->pickup_code == 'O' && $cart_transfer_data->dropoff_code == 'O') {			
	    $searchrequest .= '<PickUpDetails>
							<PickUpOther>
							<PickUpAddress>
							<AddressLine1>123 Borough High Street</AddressLine1>
							<AddressLine2>Southwark</AddressLine2>
							<PostCode>SE1 6PT</PostCode>
							</PickUpAddress>
							<PickUpTime>07.30</PickUpTime>
							</PickUpOther>
							</PickUpDetails>
							<DropOffDetails>
							<DropOffOther>
							<DropOffAddress>
							<AddressLine1>56 Gloucester
							Road</AddressLine1>
							<AddressLine2>Kensington</AddressLine2>
							<PostCode>SW5 4ER</PostCode>
							</DropOffAddress>
							</DropOffOther>
							</DropOffDetails>';
        }
  	
		        $searchrequest .='<TransferDate>'.$cart_transfer_data->travel_start_date.'</TransferDate>
						<TransferLanguage></TransferLanguage>
						<TransferVehicle>
						<Vehicle Code = "'.$cart_transfer_data->vehicle_code.'" MaximumPassengers = "'.$cart_transfer_data->maximum_passengers.'"/>
						<Passengers>'.ceil($cart_transfer_data->no_of_passengers/$cart_transfer_data->no_of_vehicles).'</Passengers>
						<LeadPaxId>1</LeadPaxId>
						</TransferVehicle>
						</TransferItem>
						</BookingItem>';  
				
					/*<Passengers>'.ceil($cart_transfer_data->no_of_passengers/$cart_transfer_data->no_of_vehicles).'</Passengers> */
						
		  $searchrequest .='</BookingItems>
								</AddBookingRequest>
								</RequestDetails>
								</Request>'; 
								
	   
					
					
	$searchname = 'TransferBooking_ReqRs/BookingItemSearchRequest';
	
	
    Savelogs($searchname, $searchrequest);
		
	$xml_type = '';
	$xml_title = 'GetTransferRequest';
	$TransferSearchRS = processRequest_gta($searchrequest,$xml_type,$xml_title, $api_id); 
	
	
    $searchname = 'TransferBooking_ReqRs/BookingItemResponse';
    $TransferSearchRqRS = array("bookingRequest" => $searchrequest,
                              "bookingResponse" => $TransferSearchRS);
     Savelogs($searchname, $TransferSearchRS);
     
    return $TransferSearchRqRS;
    }



function TransferBookingCancel($parent_pnr, $api_id='') {
	 $credentials = TF_getApiCredentials($api_id);
	 
	  $client_id = $credentials->api_username1;
	$client_email = $credentials->api_username;
	$password = $credentials->api_password;
	
	//~ $client_id = '40882';
	//~ $client_email = 'XML.PROVAB@CHINAGAP.COM';
	//~ $password = 'CHINA40882';
	
	   $TransferCancelRQ = '<?xml version="1.0" encoding="UTF-8" ?>
						<Request>
						<Source>
						<RequestorID Client = "'.$client_id.'" EMailAddress = "'.$client_email.'" Password = "'.$password.'" />
							<RequestorPreferences Country="CA" Currency="CAD" Language="en">
								<RequestMode>SYNCHRONOUS</RequestMode>
							</RequestorPreferences>
						</Source>
						<RequestDetails>
							<CancelBookingRequest>
								<BookingReference ReferenceSource="api">'.$parent_pnr.'</BookingReference>
							</CancelBookingRequest>
						</RequestDetails>
					</Request>';    
					
    $xml_type = '';
	$xml_title = 'CancelBookingRequest';
    $TransferCancelRS = processRequest_gta($TransferCancelRQ,$xml_type,$xml_title, $api_id);
	//$TransferCancelRS = '';
	$TransferCancelRQ_RS = array(
							'TransferBookingCancelRQ' => $TransferCancelRQ,
							'TransferBookingCancelRS' =>$TransferCancelRS
						);
   
	return $TransferCancelRQ_RS;
}


function processRequest_gta($requestData,$xmltype='',$xml_title, $api_id) {
	
	 $credentials = TF_getApiCredentials($api_id);
    $url = $credentials->api_url;
     //$url = 'https://rs.gta-travel.com/wbsapi/RequestListenerServlet';
	
	$cs = curl_init();
    curl_setopt($cs, CURLOPT_URL,  $url);
    curl_setopt($cs, CURLOPT_HEADER, false);
    curl_setopt($cs, CURLOPT_RETURNTRANSFER, true);
  	curl_setopt($cs, CURLOPT_POST, 1);
	curl_setopt($cs, CURLOPT_POSTFIELDS, $requestData);
    curl_setopt($cs, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($cs, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($cs, CURLOPT_TIMEOUT, 180);
    $httpHeader2 = array('Content-type: text/xml');
    curl_setopt($cs, CURLOPT_HTTPHEADER, $httpHeader2);   
    $login_response = curl_exec($cs);   
    curl_close($cs);
    $CI =& get_instance();    
    $CI->load->helper('file');   
    $requestcml = $xml_title.'_request'.date('mdHis').'.xml';
    $responsecml = $xml_title.'_response'.date('mdHis').'.xml';   
    write_file('xml_logs/'.$requestcml,$requestData, 'w+');
    write_file('xml_logs/'.$responsecml,$login_response, 'w+');    
    $CI->load->model('Hotel_Model');
    $CI->Hotel_Model->store_logs($requestcml,$responsecml,$credentials->api_name);
    return $login_response;
}

function TF_getApiCredentials($api_id) {
    $CI =& get_instance();
    $CI->load->model('General_Model');
	$api ='GTA';
    return $CI->General_Model->get_api_credentials_by_id($api_id)->row();
}


function Savelogs($searchname, $result1){
	$path = $_SERVER['DOCUMENT_ROOT'];
	$strFileName = $searchname.".xml";
	$my_file = $path."/".PAGE_TITLE."/transfer_xmldata/".$strFileName;
	//echo $my_file;exit;
	$handle = fopen($my_file, 'w');
	//print_r($handle);exit;
 	@fwrite($handle, $result1);
	@fclose($handle);
	
}

?>
