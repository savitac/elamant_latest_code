<?php if (!defined('BASEPATH'))   exit('No direct script access allowed');

function HotelValuedAvailRQ($request,$api_id,$session_data)
{
	$credentials = TF_getApiCredentials();
	$postdata= json_decode(base64_decode($request));
	$check_in_date = $postdata->hotel_checkin;
	$check_out_date = $postdata->hotel_checkout;
	$check_in_unix = strtotime($check_in_date);
	$check_out_unix = strtotime($check_out_date);
	$duration_span_str = abs($check_out_unix - $check_in_unix);
	$duration_span = floor($duration_span_str/(60*60*24));
	$nationality = $postdata->nationality;
	$country = explode(",", $postdata->city);
	$country_name = trim($country[1]);
  
   
	
	$check_in_date_format = date('Y-m-d', strtotime($check_in_date));
	$room_count = $postdata->rooms;
	$CI =& get_instance();
	$CI->load->model('general_model');
    $city_details = $CI->general_model->get_city_details_id($postdata->city); 
    $user_nationality = $CI->general_model->getCountryByCountyCodeNew($nationality);
  
  
    $searchRequest = '';
    $rooms_xml = '';
    for($room=0; $room < $room_count; $room++){
		if(isset($postdata->adult)) {
			$rooms_xml .= '<room runno="' . $room . '">';
			for($adult =0; $adult < count($postdata->adult); $adult++){
				$rooms_xml .= '<adultsCode>' . count($postdata->adult) . '</adultsCode>';
			}
		}
	
		if(isset($postdata->child)) {
			$rooms_xml .= '<children no="' . count($postdata->child) . '">';
			for($child =0; $child < count($postdata->child); $child++){
				if($postdata->childAges[$child][0] != 0){ 
				
			   $rooms_xml .= '<child runno="' . $child . '">' . $postdata->childAges[$child][0] . '</child>';
		        }
			}
		}
		
		 $rooms_xml .= '</children>
                        <rateBasis>-1</rateBasis>  
<passengerNationality>' . $user_nationality->iso_numeric . '</passengerNationality>  
<passengerCountryOfResidence>' . $user_nationality->iso_numeric  . '</passengerCountryOfResidence>  
</room>';
	}
	
 	 $searchRequest .= '<?xml version="1.0" encoding="UTF-8" ?>
					<customer>  
                    <username>' . $credentials->api_username . '</username>  
                    <password>' . md5($credentials->api_password) . '</password>  
                    <id>'.$credentials->api_username1.'</id>  
                    <source>1</source>  
                    <product>hotel</product>  
                    <request command="searchhotels">  
                    <bookingDetails>  
                    <fromDate>' . $check_in_date . '</fromDate>  
                    <toDate>' . $check_out_date . '</toDate>  
                    <currency>373</currency>  
                    <rooms no="' . $room_count . '">
                    ' . $rooms_xml . '  
                    </rooms>  
                    </bookingDetails>  
                    <return>  
                    <!--<getRooms>true</getRooms>-->
                     <filters xmlns:a="http://us.dotwconnect.com/xsd/atomicCondition" xmlns:c="http://us.dotwconnect.com/xsd/complexCondition">   
                    <city>'.$city_details->dotw_city_code.'</city>  
                    <noPrice>false</noPrice>  
                    </filters>  
                    </return>  
                    </request>  
					</customer>'; 
					
	$xml_type = '';
	$xml_title = 'GetHotelsRequest';
	if($city_details->dotw_city_code != '') {
		$HotelSearchRS = processRequest_gta($searchRequest,$xml_type,$xml_title);
		$formated_result = ts_formate_result($HotelSearchRS , $session_data ,$api_id, $check_in_date_format, $country_name, $city_details->dotw_city_code);
		$CI =& get_instance();
		$CI->load->model('hotel_model');
		$CI->hotel_model->save_result_gta($formated_result ,$request, $session_data ,$api_id);
	}
} 

function getRoomTypePrecheck($a, $c){

	if($a == 1 && $c == 0) {	
		$room_type = 'Code="SB"';	
	} else if($a == 2 && $c == 0) {	
		$room_type = 'Code="DB"';	
	} else if($a == 3 && $c == 0) {
		$room_type = 'Code="DB" '; 
	} else if($a == 2 && $c == 1) {
		$room_type = 'Code="DB"';
	} else if($a == 2 && $c == 2) {
		$room_type = 'Code="DB"';
	} else if($a == 3 && $c == 1) {
		$room_type = 'Code="TB"';			
	} else if($a == 1 && $c == 1) {
		$room_type = 'Code="DB"';	
	} else if($a == 1 && $c == 2) {
		$room_type = 'Code="DB"';	
	}  else {	
		$room_type = "";	
	}
	return $room_type;
}
function getHotelDetailByCode($request,$item_code) {
	//echo"<pre/>";print_r($item_code);exit;
	$nationality = 'AU';
	$credentials = TF_getApiCredentials();
	$check_in_date = $request->hotel_checkin;
	$check_out_date = $request->hotel_checkout;
	$check_in_unix = strtotime($check_in_date);
	$check_out_unix = strtotime($check_out_date);
	$duration_span_str = abs($check_out_unix - $check_in_unix);
	$duration_span = floor($duration_span_str/(60*60*24));
	/*Calculate duration*/
	
	
	$check_in_date_format = date('Y-m-d', strtotime($check_in_date));
	
	
	$CI =& get_instance();
	$CI->load->model('general_model');
	$city_details = $CI->general_model->get_city_details_id($request->city); 
	$city_code = $city_details->gta_city_code;
	 
 
	$room_count = $request->rooms;

	$room_xml_string = '';
	for($h=0; $h<$room_count; $h++) {
		$room_total_adult = $request->adult[$h];
		$room_total_child = $request->child[$h];

		$room_code = getRoomTypePrecheck($room_total_adult, $room_total_child);

		$room_xml_string .= '<Room '.$room_code.' />';

	}

	$searchRequest = '';
	$searchRequest .= '<Request>
				<Source>
					<RequestorID Client="'.$credentials->api_username1.'" EMailAddress="'.$credentials->api_username.'" Password="'.$credentials->api_password.'"/>
					<RequestorPreferences Country="AU" Currency="AUD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
					</RequestorPreferences>
				</Source>
				<RequestDetails><SearchHotelPriceRequest>
						<ItemDestination DestinationType="city" DestinationCode="'.$city_code.'" />
							<ItemCodes>
								<ItemCode>'.$item_code.'</ItemCode>
							</ItemCodes>
							<PeriodOfStay>
								<CheckInDate>'.$check_in_date_format.'</CheckInDate>
								<Duration>'.$request->days.'</Duration>
							</PeriodOfStay>
							<IncludePriceBreakdown/>
							<IncludeChargeConditions DateFormatResponse="true"></IncludeChargeConditions>';

	 

	if(isset($request->adult)) {
		$adult = $request->adult;
		$child = $request->child;

		$searchRequest .= '<Rooms>';

		for($i=0; $i < count($adult); $i++) {

			/*echo 'Adult: '.$adult[$i];					
			echo 'Child: '.$child[$i];*/

			$adultCount = $adult[$i];
			$childCount = $child[$i];

			

			if($adultCount == 1 && $childCount == 0) {
				$searchRequest .= '<Room Code="SB"></Room>';
			}
			else if($adultCount == 1 && $childCount == 1) {

				$childage = $request->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="TS" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
					}

				}

			}
			else if($adultCount == 1 && $childCount == 2) {
				
				$childage = $request->childAges;

				$ageString = ''; $numCots = 0;
				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$numCots++;
											
						} else {
							$ageString .= '<Age>'.$ca.'</Age>';
						}
					}
				}

				if($numCots > 0) {
					$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
				} else {
					$numberOfCotsString = '';
				}

				if($ageString != "") {    //this means that children are more than 2 yr old
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
				} else {
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ></Room>';
				}

			}
			else if($adultCount == 2 && $childCount == 0) {
					
				$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
				$searchRequest .= '</Room>';

			} 
			else if($adultCount == 2 && $childCount == 1) {
				$childage = $request->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="DB" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
					}

				}
			}
			else if($adultCount == 2 && $childCount == 2) {
				$childage = $request->childAges;

				$ageString = ''; $numCots = 0;
				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$numCots++;
											
						} else {
							$ageString .= '<Age>'.$ca.'</Age>';
						}
					}
				}

				if($numCots > 0) {
					$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
				} else {
					$numberOfCotsString = '';
				}

				if($ageString != "") {    //this means that children are more than 2 yr old
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
				} else {
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ></Room>';
				}
			}
			else if($adultCount == 3 && $childCount == 0) {
				$searchRequest .= '<Room Code="TR">';
				$searchRequest .= '</Room>';
			}
			else if($adultCount == 3 && $childCount == 1) {
				/*
				A: 3, C: 1 - Book two rooms
				*/

				$childage = $request->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="TR" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';

							$searchRequest .= '<Room Code="DB">';
							$searchRequest .= '</Room>';
						}
					}
				}
			}
			else if($adultCount == 3 && $childCount == 2) {
				$childage = $request->childAges;
				
				$ca1 = $childage[$i][0];   // Get the age of individual child
				$ca2 = $childage[$i][1];   // Get the age of individual child

				if($ca1 == 1 && $ca2 == 1) {
					$searchRequest .= '<Room Code="DB" NumberOfCots="2">';
					$searchRequest .= '</Room>';

					$searchRequest .= '<Room Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 == 1 && $ca2 > 1) {
					$searchRequest .= 	'<Room Code="DB" NumberOfCots="1">
											<ExtraBeds>
												<Age>'.$ca2.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= '<Room Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 > 1 && $ca2 == 1) {
					$searchRequest .= 	'<Room Code="DB" NumberOfCots="1">
											<ExtraBeds>
												<Age>'.$ca1.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= '<Room Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 > 1 && $ca2 > 1) {
					$searchRequest .= 	'<Room Code="TB">
											<ExtraBeds>
												<Age>'.$ca1.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= 	'<Room Code="TB">
											<ExtraBeds>
												<Age>'.$ca2.'</Age>
											</ExtraBeds>
										</Room>';
				}	
			}
			else if($adultCount == 4 && $childCount == 0) {
				$searchRequest .= '<Room Code="Q">';
				$searchRequest .= '</Room>';
			}

		}

		$searchRequest .= '</Rooms>';
		$searchRequest .= 	'</SearchHotelPriceRequest></RequestDetails>
			</Request>';
	}

	$getHotelDetail_rq = $searchRequest;

	/*header("Content-Type: text/xml");
	echo $getHotelDetail_rq; die();*/
	
$xml_type = '';
$xml_title = 'GetHotelsRequest';
$HotelSearchRS = processRequest_gta($getHotelDetail_rq,$xml_type,$xml_title);

 

       //header("Content-type: text/xml");echo $HotelSearchRS; die();

	$GetHotelDetailRQ_RS = array(
		'GetHotelDetailRQ' => $getHotelDetail_rq,
		'GetHotelDetailRS' => $HotelSearchRS
	);
	
	return $GetHotelDetailRQ_RS;

}

function ts_formate_result($HotelSearchRS , $session_id ,$api_id, $check_in_date_format, $country_name, $city_id)
{     
	
	    $CI =& get_instance();
	    $CI->load->model('general_model');
		$CI->load->model('hotel_model');
		$CI->load->model('markup_model');
		
		$user_nationality_details = $CI->general_model->getCountryByCountyCode($country_name);
		$user_nationality = $user_nationality_details->country_id;
		//$user_nationality = $country_name;
		$admin_markup_details = $CI->markup_model->get_admin_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);
		$b2b_markup_details = $CI->markup_model->get_b2b_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);
		
		if($CI->session->userdata('user_type') == 4){
		$sub_agent_markup_details = $CI->markup_model->get_sub_agent_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);	
		$b2b2b_markup_details = $CI->markup_model->get_b2b2b_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);	
		}
		
		$final_result=array();
		  if ($HotelSearchRS != '') {
            $dom2 = new DOMDocument();
            $dom2->loadXML($HotelSearchRS);
            $hotel = $dom2->getElementsByTagName("hotel");
        
            $stars = array();
            $price = array();
            $facility = array();
            $chain = array();
            $district = array();
            $map_result = array();
         
            
            $default_facilities = array('Free Internet', 'Free Parking', 'Free Breakfast', 'Room Service', 'Swimming Pool', 'Airport Shuttle', 'No Smooking Rooms/Facilities',
                'Fitness Center', 'Handicapped Rooms/Facilities', 'Business Center');
             $i = 0;
            foreach ($hotel as $val) {
				$currencyShort = $dom2->getElementsByTagName("currencyShort");
				$currency = $currencyShort->item(0)->nodeValue;
                $CurrencyCode = $currencyShort->item(0)->nodeValue;
                $final_result[$i]['session_id'] = $session_id;
                $final_result[$i]['api_id'] = $api_id;
                $final_result[$i]['currency'] = $CurrencyCode;
                $final_result[$i]['api_currency'] = $CurrencyCode;
                 $final_result[$i]['hotel_code'] =  $final_result[$i]['hotel_id']  =$hotel_id = $val->getAttribute("hotelid");
               $final_result[$i]['room_count'] = 0;
                 $final_result[$i]['city'] = $val->getAttribute("cityname");
                
                $amount = $val->getElementsByTagName("formatted");
                $total_cost = (str_replace(',', '', $amount->item(0)->nodeValue));
				
				$totalMinimumSelling = $val->getElementsByTagName("totalMinimumSelling");
				if ($totalMinimumSelling->length != 0) {
                    $total_cost = round(str_replace(",", "", ($totalMinimumSelling->item(0)->getElementsByTagName('formatted')->item(0)->nodeValue)), 2);
                }
                
                $final_result[$i]['sid'] = $city_id;
                
                $final_result[$i]['amount'] = $total_cost;
                $final_result[$i]['net_rate'] = $total_cost;
                $final_result[$i]['admin_markup'] = 0;
                $final_result[$i]['admin_baseprice']= $total_cost;
                $final_result[$i]['my_markup'] = 0;
                $final_result[$i]['agent_baseprice']= $total_cost;
                $final_result[$i]['my_agent_Markup']= 0;
                $final_result[$i]['sub_agent_baseprice'] = $total_cost;
                $final_result[$i]['my_b2b2b_markup'] = 0;
                $final_result[$i]['b2b2b_baseprice'] = $total_cost;
                $final_result[$i]['service_charge'] = 0;
                $final_result[$i]['gst_charge'] = 0;
                $final_result[$i]['tax_charge'] = 0;
                
                
              
                $hotelrating = $val->getElementsByTagName("rating");
               
               $star_rating = $hotelrating->item(0)->nodeValue; 
                $room_status_root = $val->getElementsByTagName("availability");
                $primary_status = $room_status_root->item(0)->nodeValue;
                 
                $final_result[$i]['status'] = $primary_status;
                $dotw_static_data = $CI->hotel_model->dotw_hotel_details($hotel_id)->row();
                if(count($dotw_static_data) >0){
					$final_result[$i]['hotel_name'] = $dotw_static_data->hotel_name;
					$final_result[$i]['hotel_api_images'] = $dotw_static_data->image;
					$o_images = explode("|", $dotw_static_data->other_images);
					$final_result[$i]['images'] = json_encode($o_images);
					$final_result[$i]['description'] = $dotw_static_data->description;
					$final_result[$i]['address'] = $dotw_static_data->address;
					$final_result[$i]['lat'] = $dotw_static_data->latitude;
					$final_result[$i]['lon'] = $dotw_static_data->longitude;
					$final_result[$i]['star_rating'] = $dotw_static_data->star;
					$final_result[$i]['locations'] = $dotw_static_data->location;
					
					$hotel_facilities = $dotw_static_data->hotel_facilities;
					$facc=array();
					if($hotel_facilities !=''){
						$facc  = explode("-",$hotel_facilities);
					}
					if(isset($facc) && !empty($facc)){
						$final_result[$i]['Facilities'] = getFacilities($facc, $facs);
						//$hs['Facilities'] = $Facilities;
					}else{
						
						$final_result[$i]['Facilities'] = array();
					}
				}
				
				  $final_result[$i]['hotel_facility'] =  array();
                
             
				$i= $i+1; 
			}	
			
			
            return $final_result;
        } 

}

     function getFacilities($Facilities, $array){
        $i=0;$fac = array();
        foreach ($Facilities as $key => $Facility) {
            $fac[$i]['Code'] = searchForFac($Facility, $array);
            $fac[$i]['Facility'] = $Facility;    
            $i++;
        }
        return $fac;
        //echo "<pre>"; print_r($fac);die();
    }

     function searchForFac($fac, $array) {
       foreach ($array as $key => $val) {
           if ($val->facility === $fac) {
               return $val->code;
           }
       }
       return 'CM';
    }

    function HotelValuedRoomAvailRQ($request,$api_id,$session_data,$hotel_code){
	
	$credentials = TF_getApiCredentials();
	$postdata= $request;
	$check_in_date = $postdata->hotel_checkin;
	$check_out_date = $postdata->hotel_checkout;
	$check_in_unix = strtotime($check_in_date);
	$check_out_unix = strtotime($check_out_date);
	$duration_span_str = abs($check_out_unix - $check_in_unix);
	$duration_span = floor($duration_span_str/(60*60*24));
	$nationality = $postdata->nationality;
	$country = explode(",", $postdata->city);
	$country_name = trim($country[1]);
  
   
	$check_in_date_format = date('Y-m-d', strtotime($check_in_date));
	$room_count = $postdata->rooms;
	$CI =& get_instance();
	$CI->load->model('general_model');
	$CI->load->model('hotel_model');
    $city_details = $CI->general_model->get_city_details_id($postdata->city); 
    $user_nationality = $CI->general_model->getCountryByCountyCodeNew($nationality);
  
  
    $searchRequest = '';
    $rooms_xml = '';
   // echo "asdfasdf".count($postdata->child); exit;
    for($room=0; $room < $room_count; $room++){
		if(isset($postdata->adult)) {
			$rooms_xml .= '<room runno="' . $room . '">';
			for($adult =0; $adult < count($postdata->adult); $adult++){
				$rooms_xml .= '<adultsCode>' . count($postdata->adult) . '</adultsCode>';
			}
		}
	
		if(isset($postdata->child)) {
			$rooms_xml .= '<children no="0">';
			for($child =0; $child < count($postdata->child); $child++){
				//if($postdata->childAges[$child][0] != 0){ 
				
			   $rooms_xml .= '<child runno="1"></child>';
		       // }
			}
		}
		
	  $rooms_xml .= '</children>
                        <rateBasis>-1</rateBasis>  
<passengerNationality>' . $user_nationality->iso_numeric . '</passengerNationality>  
<passengerCountryOfResidence>' . $user_nationality->iso_numeric  . '</passengerCountryOfResidence>  
</room>';

}
 
 
        //$hotel_code = '306225';

 	     $searchRequest .= '<customer>  
                    <username>chinagap</username>  
                    <password>'.md5('Chinagap06').'</password>  
                    <id>1314905</id>  
                    <source>1</source>  
                    <product>hotel</product>  
                    <request command="getrooms">  
                    <bookingDetails>  
                    <fromDate>2016-11-12</fromDate>  
                    <toDate>2016-11-13</toDate>  
                    <currency>373</currency>  
                    <rooms no="1">
                    <room runno="0"><adultsCode>1</adultsCode><children no="0"></children>
                      <rateBasis>-1</rateBasis>  
<passengerNationality>20</passengerNationality>  
<passengerCountryOfResidence>20</passengerCountryOfResidence>  
</room>  
                    </rooms>
                    <productId>'.$hotel_code.'</productId>
                    </bookingDetails>                    
                    </request>  
            </customer>';   
					
	$xml_type = '';
	$xml_title = '';
	$HotelSRoomRS = processRequest_gta($searchRequest,$xml_type,$xml_title);
	
	if ($HotelSRoomRS != '') {
		        $dom2 = new DOMDocument();
                $dom2->loadXML($HotelSRoomRS);
                $request = base64_encode(json_encode($searchRequest));
                $hotel = $dom2->getElementsByTagName("hotel"); 
                foreach ($hotel as $val) {
                    $currencyShort = $dom2->getElementsByTagName("currencyShort");
                    $data['API_CURR'] =  $result['cost_type'] = $currencyv1 = $currencyShort->item(0)->nodeValue;
                    $data['hotel_code'] = $hotelid = $val->getAttribute("id");
                     $data['hotel_name'] = $val->getAttribute("name"); 
                     $data['request'] = $request; 
                     $data['session_id'] = $session_data; 
                     $data['api'] = 'DOTW'; 
                     if ($hotelid == $hotel_code) {
						  $rooms = $val->getElementsByTagName("rooms");
						  foreach ($rooms as $val7) {
							   $room = $val7->getElementsByTagName("room");
							   foreach ($room as $val8) {
								$main_room_runno = $val8->getAttribute('runno');
                                $adults = $val8->getAttribute('adults');
                                $children = $val8->getAttribute('children');
                                $rcount = $val8->getAttribute('count');
                                $childrenages = $val8->getAttribute('childrenages');
                                $extrabed = $val8->getAttribute('extrabeds');

                                $result['extra_bed'] = $extrabed;
                                if ($adults == 1 && $children == 0) {
                                    $type = 'Single';
                                } else if ($adults == 2 && $children == 0) {
                                    $type = 'Double/Twin';
                                } else if ($adults == 3 && $children == 0) {
                                    $type = 'Triple';
                                } else if ($adults == 4 && $children == 0) {
                                    $type = 'Quad';
                                } else if ($adults == 2 && $children == 1) {
                                    $type = 'Double plus child/Twin plus child';
                                } else if ($adults == 2 && $children == 2) {
                                    $type = 'Double plus Double child/Twin plus Twin child';
                                } else {
                                    $type = "";
                                }
                                
                                $result['adultcode'] = $adults;
                                $result['childcode'] = $children;
                                
                                $price_bkup['room_type'] = $type;
                                $roomType = $val8->getElementsByTagName("roomType");
                                $room_data = array();
                                 foreach ($roomType as $val9) {
									 
									$sub_room_runnocode = $val9->getAttribute('runno'); 
									
									$price_bkup['roomtypecode'] = $room_data['Id'] = $data['option_id'] = $roomcode = $val9->getAttribute('roomtypecode');
									$data['room_type'] = $room_data['Description'] = $room_type = $val9->getElementsByTagName("name")->item(0)->nodeValue;
									$roomInfo = $val9->getElementsByTagName("roomInfo");
									$r = 0;
									foreach($roomInfo as $info){
										$room_data['room_info']['maxOccupancy'][$r] = $info->getElementsByTagName('maxOccupancy')->item(0)->nodeValue;
										$room_data['room_info']['maxAdultWithChildren'][$r] = $info->getElementsByTagName('maxAdultWithChildren')->item(0)->nodeValue;
										$room_data['room_info']['minChildAge'][$r] = $info->getElementsByTagName('minChildAge')->item(0)->nodeValue;
										$room_data['room_info']['maxChildAge'][$r] = $info->getElementsByTagName('maxChildAge')->item(0)->nodeValue;
										$room_data['room_info']['maxAdult'][$r] = $info->getElementsByTagName('maxAdult')->item(0)->nodeValue;
										$room_data['room_info']['maxExtraBed'][$r] = $info->getElementsByTagName('maxExtraBed')->item(0)->nodeValue;
										//$room_data['room_info']['maxChildren'][$r] = $info->getElementsByTagName('maxChildren45-+*')->item(0)->nodeValue;
									}
									
									$rateBases = $val9->getElementsByTagName("rateBases");
									$special_arr = array();
                                    $specials = $val9->getElementsByTagName("specials");
                                    
                                    foreach ($specials as $val10){
                                        $special = $val10->getElementsByTagName("special");     
                                        foreach ($special as $val11) {
                                            $speci = $val11->getAttribute('runno');
                                            $special_arr[$speci]['type']=$val11->getElementsByTagName('type')->item(0)->nodeValue;
                                            $special_arr[$speci]['specialName']=$val11->getElementsByTagName('specialName')->item(0)->nodeValue;
                                            $special_arr[$speci]['discount']=$val11->getElementsByTagName('discount')->item(0)->nodeValue;
                                            $special_arr[$speci]['stay']=$val11->getElementsByTagName('stay')->item(0)->nodeValue;
                                        }
                                    }
                                    $room_data['special_details'] = $special_arr;
                                   
                                    foreach ($rateBases as $val10) {
										$rateBasis = $val10->getElementsByTagName("rateBasis");
										foreach ($rateBasis as $val11) {
											$rate_basis_runno = $val11->getAttribute('runno');
                                            $validForOccupancy = $val11->getElementsByTagName('validForOccupancy');
                                            $extraBed_val = 0;
                                            $valid_adults = 0;
                                            $valid_children = 0;
                                            $valid_childrenages = "";
                                            $valid_extrabedoccupant = "";
                                             if ($validForOccupancy) {
                                                foreach ($validForOccupancy as $vaf) {
                                                    $extraBed = $vaf->getElementsByTagName('extraBed');
                                                    $extraBed_val = $extraBed->item(0)->nodeValue;
                                                    $valid_adults = $vaf->getElementsByTagName('adults')->item(0)->nodeValue;
                                                    if ($vaf->getElementsByTagName('children')->item(0)->nodeValue != "") {
                                                        $valid_children = $vaf->getElementsByTagName('children')->item(0)->nodeValue;
                                                    }
                                                    $valid_childrenages = $vaf->getElementsByTagName('childrenAges')->item(0)->nodeValue;
                                                    $valid_extrabedoccupant = $vaf->getElementsByTagName('extraBedOccupant')->item(0)->nodeValue;
                                                }
                                            }
                                          
                                            $changedoccupancy = $val11->getElementsByTagName('changedOccupancy');
                                            if (isset($changedoccupancy->item(0)->nodeValue) && $changedoccupancy->item(0)->nodeValue != "") {
                                                $changedoccupancy = $changedoccupancy->item(0)->nodeValue;
                                            }
                                            $actual_runno = $main_room_runno . $sub_room_runnocode . $rate_basis_runno;
                                            $mealid = $val11->getAttribute('id');
                                            $meal = $val11->getAttribute('description');
                                            $status = $val11->getElementsByTagName("status")->item(0)->nodeValue;
                                            $specialsApplied = $val11->getElementsByTagName("specialsApplied");
                                            $special_val = array();
                                            foreach($specialsApplied as $sak=>$sav){
                                                $special_val[]=$special_arr[$sav->nodeValue];
                                            }
                                            $paymentMode = "";
                                            if (isset($val11->getElementsByTagName("paymentMode")->item(0)->nodeValue)) {
                                                $paymentMode = $val11->getElementsByTagName("paymentMode")->item(0)->nodeValue;
                                            }
                                            $alloc_det = $val11->getElementsByTagName("allocationDetails")->item(0)->nodeValue;
                                            $minStay_val = $val11->getElementsByTagName("minStay")->item(0)->nodeValue;
                                            $dateApplyMinStay_val = $val11->getElementsByTagName("dateApplyMinStay")->item(0)->nodeValue;
                                            $costval = $result['cost_value'] = $data['net_rate'] = round($val11->getElementsByTagName("total")->item(0)->nodeValue, 2);
                                            $data['total_cost'] = $costval;
                                            $RateNotes = $val11->getElementsByTagName("tariffNotes")->item(0)->nodeValue;
                                            $cancellationRules = $val11->getElementsByTagName("cancellationRules");
                                            $Cancellation_Policy = '';
                                            $before_charge = 0;
                                            foreach ($cancellationRules as $canrule) {

                                                $rules = $canrule->getElementsByTagName("rule");
                                                $can_run = $canrule->getAttribute("count");
                                                $i_c = 0;
                                                foreach ($rules as $rule) {

                                                    if ($i_c == 0) {
                                                        $todatedetail = $rule->getElementsByTagName("toDate");
                                                        //if(isset($todatedetail->item(0)->nodeValue))
                                                        $todatedetail_val = @$todatedetail->item(0)->nodeValue;
                                                    } else if ($i_c == 1) {
                                                        $todatedetail1 = $rule->getElementsByTagName("fromDate");
                                                        $todatedetail_val = $todatedetail1->item(0)->nodeValue;
                                                    } else {
                                                        $todatedetail = $rule->getElementsByTagName("fromDate");
                                                        $fromDate = $rule->getElementsByTagName("fromDate");
                                                        $fromDate_val = $fromDate->item(0)->nodeValue;
                                                    }

                                                    $charge = $rule->getElementsByTagName("charge");

                                                    foreach ($charge as $ch) {

                                                        $formatted = $ch->getElementsByTagName("formatted");
                                                        $before_charge = $formatted->item(0)->nodeValue;
                                                    }

                                                    $i_c++;
                                                }
                                            }
                                            $datee = $new_date = date('Y-m-d H:i:s', strtotime($todatedetail_val));
                                            $canceld1 = date('Y-m-d', (strtotime($datee) - 60 * 60 * 24 * 4));
                                            $canceld2 = $datee;
                          
                                            $Cancellation_Policy.=' Any Cancellation After ' . $canceld2 . ' will charge ' .  /* $this->flight_model->currency_convertor( */  $before_charge/* , $currencyv1, CURR) */ . ' of the stay ';
                                           
                                           $cancel_status = 1;
                                            if ($canceld1 < date('Y-m-d')) {
                                                $cancel_status = 0;
                                            }
                                            if ($cancel_status != 1) {
                                                $Cancellation_Policy = "<font color='red'>Charges applicable after:  $canceld1 </font>.";
                                            }
                                            
                                            $data['policy_description'] = $Cancellation_Policy;
                                            $t_cancel_till_amt = $before_charge;
                                            $t_cancel_till_date = $todatedetail_val;
                                            
                                            $totalMinimumSelling = $val11->getElementsByTagName("totalMinimumSelling");
                                            if ($totalMinimumSelling->length != 0) {
                                                $costval = round(str_replace(",", "", ($totalMinimumSelling->item(0)->getElementsByTagName('formatted')->item(0)->nodeValue)), 2);
                                            }
                                            $org_amt = str_replace(',', '', $costval);
                                            $total_cost = $org_amt;
                                          
                                            $total_cost = $total_cost;
                                            $code = $hotelid;
                                            if ($status == "unchecked") {
                                                $status = "Available";
                                            }
                                             $room_comb = array('alloc_detail' => $alloc_det, 'mealid' => $mealid, 'meal' => $meal);
                                              $room_comb = array('alloc_detail' => $alloc_det, 'mealid' => $mealid, 'meal' => $meal);

                                            $RatePlanCode = $mealid;
                                            $data['inclusion'] = $meal;
                                            $RoomTypeCode = $roomcode;
                                             $RoomTypeName = $room_type;
										}
									}
								}
								     $data['session_id'] = $session_data;
								     $data['room_data'] = json_encode($room_data);
							   }
							
							  $CI->hotel_model->storeHotelTempData($data);
						   }
					 }
                }
	}
	
	}
  

function HotelBookingCancel($parent_pnr) {
	$credentials = TF_getApiCredentials();
	$HotelCancelRQ = '<Request>
						<Source>
							<RequestorID Client="'.$credentials->api_username1.'" EMailAddress="'.$credentials->api_username.'" Password="'.$credentials->api_password.'"/>
							<RequestorPreferences Country="AU" Currency="AUD" Language="en">
								<RequestMode>SYNCHRONOUS</RequestMode>
							</RequestorPreferences>
						</Source>
						<RequestDetails>
							<CancelBookingRequest>
								<BookingReference ReferenceSource="api">'.$parent_pnr.'</BookingReference>
							</CancelBookingRequest>
						</RequestDetails>
					</Request>';

	$xml_type = '';
	$xml_title = 'CancelBookingRequest';
	$HotelCancelRS = processRequest_gta($HotelCancelRQ,$xml_type,$xml_title);
	
	$HotelCancelRQ_RS = array(
							'HotelBookingCancelRQ' => $HotelCancelRQ,
							'HotelBookingCancelRS' => $HotelCancelRS
						);
	return $HotelCancelRQ_RS;
}

function processRequest_gta($requestData,$xmltype='',$xml_title) {
	 
        $credentials = TF_getApiCredentials();
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $credentials->api_url);
        curl_setopt($ch2, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch2, CURLOPT_HEADER, 0);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_POST, 1);
        curl_setopt($ch2, CURLOPT_POSTFIELDS, "$requestData");
        curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch2, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, true);
        $httpHeader2 = array(
            "Content-Type: text/xml; charset=UTF-8",
            "Content-Encoding: UTF-8",
            "Accept-Encoding: gzip,deflate"
        );
        curl_setopt($ch2, CURLOPT_HTTPHEADER, $httpHeader2);
        curl_setopt($ch2, CURLOPT_ENCODING, "gzip,deflate");
        $data2 = curl_exec($ch2);
        $error2 = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
      
        curl_close($ch2);
        return $data2;
}

function TF_getApiCredentials() {
    $CI =& get_instance();
    $CI->load->model('General_Model');
	$api ='DOTW';
    return $CI->General_Model->get_api_credentials($api)->row();
}


/*for testing pupose*/
function debug($arr)
{
	if(is_array($arr) || is_object($arr))
	{
		echo "<pre>"; var_dump($arr);
	}
	else
	{
		echo $arr;
	}
	exit;
}
