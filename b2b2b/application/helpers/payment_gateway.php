<?php
if (! defined ( 'BASEPATH' ))
exit ( 'No direct script access allowed' );
/**
 *
 * @package Provab
 * @subpackage Transaction
 * @author Pravinkuamr P <pravinkumar.provab@gmail.com>
 * @version V1
 */
class Payment_Gateway extends CI_Controller {
	/**
	 *
	 */
	public function __construct() {
		parent::__construct ();
		$book_origin='';
		$this->load->model ( 'module_model' );
		$this->user=$this->session->userdata('user_type');
	}
	/**
	 * Blocked the payment gateway temporarly
	 */
	function demo_booking_blocked()
	{
		echo '<h1>Booking Not Allowed, This Is Demo Site. Go To <a href="'.base_url().'">Travelomatix</a></h1>';
	}
	/**
	 * Redirection to payment gateway
	 * @param string $book_id		Unique string to identify every booking - app_reference
	 * @param number $book_origin	Unique origin of booking
	 */
	public function payment($book_id,$book_origin)
	{

		$this->load->helper('app');
		$this->load->model('transaction');
		$this->book_origin = $book_origin;
	

		$PG = $this->config->item('active_payment_gateway');
		$domain_name=$_SERVER['HTTP_HOST'];
		load_pg_lib ( $PG );
		
		$auth_key  =$this->transaction->payment_gateway_auth($domain_name);
		#print_r($auth_key);die();

		$pg_record = $this->transaction->read_payment_record($book_id);
		//Converting Application Payment Amount to Pyment Gateway Currency
		#$pg_record['amount'] = $pg_record['amount']*$pg_record['currency_conversion_rate'];
                                
		if (empty($pg_record) == false and valid_array($pg_record) == true) {
			$params = json_decode($pg_record['request_params'], true);
			/*$pg_initialize_data = array (
				'txnid' => $params['txnid'],
				'access_key' =>	$auth_key['access_key'],
				'working_key' =>	$auth_key['working_key'],
				'pgi_amount' => $pg_record['amount'],
				'firstname' => $params['firstname'],
				'email'=>$params['email'],
				'phone'=>$params['phone'],
				'productinfo'=> $params['productinfo']
			);*/ 
		} else {
			echo 'Under Construction :p';
			exit; 
		}
		if($this->user=Agent)
		{
		 $this->redirect_booking($params['productinfo'], $params['txnid'], $book_origin);
		}
		else
		{
		 echo " Under Custruction";
		 break; 
	 	}
        //$this->load->helper('ccavenue_pgi_helper');
	/*	$payment_gateway_status = $this->config->item('enable_payment_gateway');
		if ($payment_gateway_status == true) {
        $a =  $this->pg->initialize ( $pg_initialize_data );
		$page_data['pay_data'] = $this->pg->process_payment ();
		$temp_payment_details['txnid'] = $page_data['pay_data']['txnid'];
		$temp_payment_details['productinfo'] = $page_data['pay_data']['productinfo'];
		$temp_payment_details['pgi_amount'] = $page_data['pay_data']['amount'];
		$temp_payment_details['firstname'] = $page_data['pay_data']['firstname'];
		$temp_payment_details['email'] = $page_data['pay_data']['email'];
		$temp_payment_details['phone'] = $page_data['pay_data']['phone'];
		$this->custom_db->insert_record('temp_payment_details',$temp_payment_details);
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
        $post_values['order_id']='';
		$post_values['tid'] = strtotime(date("Y-m-d H:i:s"));
		$post_values['merchant_id'] =$page_data['pay_data']['merchant_id'];
		$post_values['order_id'] = $page_data['pay_data']['txnid'];
		$post_values['amount'] = $page_data['pay_data']['amount'];
		$post_values['currency'] = $page_data['pay_data']['currency']; 
		$post_values['redirect_url'] = $page_data['pay_data']['redirect_url']; 
		$post_values['domain_url'] = base_url(); 
		$post_values['cancel_url'] = $page_data['pay_data']['cancel_url'];
		$post_values['language'] = $page_data['pay_data']['language'];
		$working_key = $page_data['pay_data']['working_key'];
		$access_code = $page_data['pay_data']['access_code'];
		#print_r($post_values);die();
		$merchant_data='';
		foreach ($post_values as $key => $value){
			$merchant_data.=$key.'='.$value.'&';
		}
        $encrypted_data=encrypt($merchant_data,$working_key); // Method for encrypting the data.
		//   debug($encrypted_data);exit;
		$data_to_view['data'] = base64_encode(json_encode(array('encrypted_data'=>$encrypted_data,'access_code'=>$access_code)));                         
		$this->load->view('payment/'.$PG.'/pay', $data_to_view);
		} else {
		echo 'Booking Can Not Be Done!!!';
		exit;
		}*/
	}

	/****
	*****sudheep EBS synch*****
	****/
	private function redirect_booking($product, $book_id, $book_origin)
	{
		switch ($product) {
			case META_AIRLINE_COURSE :
				redirect ( base_url () . 'index.php/flight/process_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_BUS_COURSE :
				redirect ( base_url () . 'index.php/bus/process_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'index.php/hotel/process_booking/' . $book_id . '/' . $book_origin );
				break;
			default :
				redirect ( base_url().'index.php/transaction/cancel' );
				break;
		}
	}
	function response(){

		$order_status="";
		$this->load->model('custom_db');
		$this->load->helper('ccavenue_pgi_helper');
	    $workingKey='CCEBB0F718AAF07954472326B4942AC0';		//Working Key should be provided here.
		$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
		$rcvdString=decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
		$decryptValues=explode('&', $rcvdString);
		$payment_gateway_response = base64_encode(json_encode($decryptValues));
		$dataSize=sizeof($decryptValues);
		$order_array = explode('=',$decryptValues[0]);
		$book_id = $order_id = $order_array[1];
		$book_origin=$this->book_origin;
		$pay_array = explode('=',$decryptValues[1]);
		$payment_gateway_track_id = $pay_array[1];

		$status_array = explode('=',$decryptValues[3]);
		$_REQUEST['status'] = $order_status = $status_array['1'];  //Change to dynamic
		
		$_REQUEST['status'] = $order_status ="Success";
		$_REQUEST['addedon'] = date("Y-d-m H:i:s");  
		$temp_pay_details = $this->custom_db->single_table_records('temp_payment_details','*',array('txnid'=>$book_id));    
		$this->custom_db->update_record('temp_payment_details',array('payment_gateway_track_id'=>$payment_gateway_track_id,'payment_gateway_response'=>$payment_gateway_response,'order_status'=>$order_status),array('txnid'=>$book_id));
		
		$_REQUEST ['productinfo'] = $temp_pay_details['data'][0]['productinfo'];
		$_REQUEST ['txnid'] = $temp_pay_details['data'][0]['txnid']; 
		$_REQUEST ['email'] = $temp_pay_details['data'][0]['email']; 
		$_REQUEST ['firstname'] = $temp_pay_details['data'][0]['firstname']; 
		$_REQUEST ['txnid'] = $temp_pay_details['data'][0]['productinfo']; 
		$_REQUEST ['phone'] = $temp_pay_details['data'][0]['phone']; 
		if($order_status=="Success")
		{
			$this->load->model('transaction');
			$product = $_REQUEST ['productinfo'];
			$email = $_REQUEST['email'];
			$time = @$_REQUEST['addedon'];
			$guest_name = $_REQUEST['firstname'];
			$guest_phone = $_REQUEST['phone'];
			$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
			'book_id' => $book_id 
			));
			$pg_status = @$_REQUEST['status'];
			$pg_record = $this->transaction->read_payment_record($book_id);

			if ($pg_status == 'Success' and empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {

				/*B2C Markup Update*/
				$booking_data = $this->module_model->unserialize_temp_booking_record ( $book_id, $book_origin );
				$book_params = $booking_data['book_attributes'];
				$book_params['billing_email']=  $email;
				$book_params['passenger_contact']=  $phone;
				$book_params ['payment_method']  = $post_params ['type_of_payment'];
				$agent_id= $this->session->userdata('branch_id'); 
						$this->load->model('transaction');
						$update_b2c=$this->transaction->get_agent_markup($agent_id);
						echo "<pre>";
						$markup_type=$update_b2c[0]->markup_value_type;
						switch ($markup_type) {
							case 'Percentage':
							echo $book_params['token']['token'][0]['FareDetails']['api_PriceDetails']['OfferedFare'];die();
						$upate_markup_b2c=$book_params['token']['token'][0]['FareDetails']['api_PriceDetails']['OfferedFare'] / 100 *$update_b2c[0]->markup_value; 
						$agent_balnc=$this->transaction->get_agent_balance($agent_id);

						$update_balance_database=$agent_balnc[0]->balance_credit + $upate_markup_b2c;
						$up_balnc=$this->transaction->update_agent_balance($update_balance_database,$agent_id);
								break;
							
							case 'Fixed':
								
 
								break;
						}
						/*END*/
			$response_params = '';
			$this->transaction->update_payment_record_status($book_id, ACCEPTED, $response_params);
			$book_origin = $temp_booking ['data'] ['0'] ['id']; 
			switch ($product) {
			case META_AIRLINE_COURSE :
				redirect ( base_url () . 'index.php/flight/secure_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_BUS_COURSE :
				redirect ( base_url () . 'index.php/bus/secure_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_ACCOMODATION_COURSE :
			//echo $book_id;exit;
				redirect ( base_url () . 'index.php/hotel/secure_booking/' . $book_id . '/' . $book_origin );
				break;
			default :

				redirect ( base_url().'index.php/transaction/cancel' );
				break;
			}
			}

			if( $product=="recharge" && $pg_status == 'Success' ){

			    redirect ( base_url() . 'index.php/recharge/perform_recharge_request/' . $book_id.'/'.$pg_status.'/'.$email.'/'.$guest_phone.'/'.$guest_name );
			}
		
	}
	else if($order_status==="Aborted")
	{
		echo "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
			$this->load->model('transaction');
		$product = $_REQUEST ['productinfo'];
		$book_id = $_REQUEST ['txnid'];
        $pg_status = $_POST['status'];
        $txnid = $_POST['txnid'];   
        $email = $_REQUEST['email'];
        $reson = $_REQUEST['error_Message'];
        $guest_name = $_REQUEST['firstname'];
        $guest_phone = $_REQUEST['phone'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
				'book_id' => $book_id 
		));
		$pg_record = $this->transaction->read_payment_record($book_id);
		if (empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {
			//$response_params = $_REQUEST;
			$response_params = $decryptValues;
			$this->transaction->update_payment_record_status($book_id, DECLINED, $response_params);
			$msg = "Payment Unsuccessful, Please try again.";
			switch ($product) {
				case META_AIRLINE_COURSE :
					redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_BUS_COURSE :
					redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_ACCOMODATION_COURSE :
					redirect ( base_url () . 'index.php/hotel/exception?op=booking_exception&notification=' . $msg );
					break;
			}
		}
        if( $product=="recharge" && $pg_status == 'Aborted' ){ 
            $get_txn_details    = $this->custom_db->single_table_records("recharge","*",['transid'=>$txnid]);
            if($get_txn_details['status'] == 1){
                $recharged_number = $get_txn_details['data'][0]['mobile_number'];
            }
            $mobile_number  =   $recharged_number;
            $recharge_amount    =   $_REQUEST['amount']." Rs";
            $transition_id      =   $_REQUEST['txnid'];
                
                
            redirect ( base_url() . 'index.php/recharge/payment_failure/' . $book_id.'/'.$pg_status.'/'.$email.'/'.$guest_phone.'/'.$guest_name );
        }	
	}
	else if($order_status==="Failure")
	{
		echo "<br>Thank you for shopping with us.However,the transaction has been declined.";
		$this->load->model('transaction');
		$product = $_REQUEST ['productinfo'];
		$book_id = $_REQUEST ['txnid'];
		    
		     $pg_status = $_POST['status'];
		    $txnid = $_POST['txnid'];   
		    $email = $_REQUEST['email'];
		    $reson = $_REQUEST['error_Message'];
		    $guest_name = $_REQUEST['firstname'];
		    $guest_phone = $_REQUEST['phone'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
			'book_id' => $book_id 
		) );
		$pg_record = $this->transaction->read_payment_record($book_id);
		if (empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {
		//$response_params = $_REQUEST;
		$response_params = $decryptValues;
		$this->transaction->update_payment_record_status($book_id, DECLINED, $response_params);
		$msg = "Payment Unsuccessful, Please try again.";
		switch ($product) {
			case META_AIRLINE_COURSE :
				redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
				break;
			case META_BUS_COURSE :
				redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
				break;
			case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'index.php/hotel/exception?op=booking_exception&notification=' . $msg );
				break;
		}
		}
        if( $product=="recharge" && $pg_status == 'Failure' ){ 
	        $get_txn_details    = $this->custom_db->single_table_records("recharge","*",['transid'=>$txnid]);
	        if($get_txn_details['status'] == 1){
	            $recharged_number = $get_txn_details['data'][0]['mobile_number'];
	        }
	        $mobile_number  =   $recharged_number;
	        $recharge_amount    =   $_REQUEST['amount']." Rs";
	        $transition_id      =   $_REQUEST['txnid'];
	        redirect ( base_url() . 'index.php/recharge/payment_failure/' . $book_id.'/'.$pg_status.'/'.$email.'/'.$guest_phone.'/'.$guest_name );
        }
	}
	else if($order_status==="initiated")
	{ 
		redirect ( base_url() . 'index.php');
	}
	else
	{
		echo "<br>Security Error. Illegal access detected";
	
	} 
	}
}
