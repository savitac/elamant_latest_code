<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function set_crediential($api) {
		$CI = & get_instance();
		$api_usage = "TEST";
		$CI->load->model('General_Model');
        $query = $CI->General_Model->get_api($api, $api_usage);
        if ($query->num_rows() == 0) {
            $data['set_crediential'] = FALSE;
        } else {   
            $api_info = $query->row();
            $data['client_id'] 			= $api_info->api_username;
            $data['password'] 			= $api_info->api_password;
            $data['post_url'] 			= $api_info->api_url;
            $data['set_crediential'] 	= TRUE;
            $data['active_api'] 		= $api;
        }
        return $data;
    }
    
function HotelValuedAvailRQ($request,$api_id,$session_data) { 
		if (!file_exists('hotel_xmldata/HotelValuedAvailRQRS')) {
			mkdir('hotel_xmldata/HotelValuedAvailRQRS', 0777, true);
		} 
		$requests = $request;
        $credencails_data=set_crediential($api_id);
        $request = base64_decode($request);
		$request = json_decode($request);

		$country = explode(",", $request->city);
		$country_name = trim($country[1]); 
		
		$check_in_date_format = date('Y-m-d', strtotime($request->hotel_checkin));

        $language 		= 'ENG';
        $available 		= 'Available';
        $room_used_type = array();
        $adult_count 	= 0;
        $child_count	= 0; 
        $adultval 		= $request->adult;
        $childval 		= $request->child;
        $city 			= $request->city;
        $sd 			= date('d-m-Y',strtotime($request->hotel_checkin));
        $room_count 	= $request->rooms;
        $ed 			= date('d-m-Y',strtotime($request->hotel_checkout));
        
        $CI 			= & get_instance();
        $CI->load->model('Hotel_Model');
         $CI->load->model('general_model');
         if(isset($request->hotel_name)){
         	$hotel_details =   $CI->general_model->get_hotel_details_id($request->hotel_name,$city); 
         	$hotel_code = $hotel_details->HotelCode;
         	
         }
         else{
        //$city_val = $CI->Hotel_Model->get_city_details($city);
        $city_val = $CI->general_model->get_city_details_id($city); 

        if ($city_val != '') {
            $city_code = $city_val->hotelbeds_city_code;
        } else {
           // echo "City Code not Found";
            return;
           
        }   
    }
        $available_data=array();
        $cinval 			= explode("-", $sd);
        $cin 				= $cinval[2] . $cinval[1] . $cinval[0];
        $coutval 			= explode("-", $ed);
        $cout 				= $coutval[2] . $coutval[1] . $coutval[0];
        $room_info 			= '';
        $sb_room_cnt 		= 0;
        $db_room_cnt 		= 0;
        $tr_room_cnt 		= 0;
        $q_room_cnt 		= 0;
        $dbc_room_cnt 		= 0;
        $dbcc_room_cnt 		= 0;
        $dummy_room_count 	= 0;
        $child_age 			= $request->childAges;
    

		for($i=0;$i< $request->rooms;$i++)
			{  
				$array_vals[] =  $adultval[$i].",".$childval[$i];
			}
			   
			$hotelbed_rooms	= '';
			$check_array	= array();
		for($k=0;$k<count($array_vals);$k++){
			
			if(isset($child_age[$k])){
				$child_ages=implode(',', $child_age[$k]);
			} else {
				$child_ages = '';
			} 
				//echo 'child_age--'.$child_age1;print_r($child_ages);
				$key = array_search($array_vals[$k], $check_array);
				//echo 'key '.$key;exit;
				if ($key) {
				
				unset($check_array[$key]);
				$split_key = explode("||",$key);
				$key_count = $split_key[0]+1;
					$check_array[$key_count."||".$split_key[1].",".$child_ages] = $array_vals[$k];
				} else {
					//echo $array_vals[$k];
					$check_array['1||' . $child_ages . '||' . $k]=$array_vals[$k];
					//$check_array['1||'.$child_age.'||'.$k] = $array_vals[$k];
				//print_r($$check_array[ '1||'.$child_age.'||'.$k ]);
				}
				
			} 
				
			$get_chid_age_data=array();
			foreach($check_array as $key=>$value)
			{
				$room_data = explode("||",$key);
				$adult_child_data = explode(",",$value);
				$childage_data = explode(",",$room_data[1]);
				$get_chid_age_data[$value] = 	$room_data[1];
				$hotelbed_rooms.='
								  <HotelOccupancy>
								  <RoomCount>'.$room_data[0].'</RoomCount>
										<Occupancy>
											<AdultCount>'.$adult_child_data[0].'</AdultCount>
											<ChildCount>'.$adult_child_data[1].'</ChildCount>';
								if($adult_child_data[1] != 0)
								{
								   $hotelbed_rooms .= '<GuestList>';
								   for($ac=0;$ac< count($childage_data);$ac++)
								   {	
											if($childage_data[$ac] == ''){
									   $child_data_age=0;
									   }else{
										   $child_data_age=$childage_data[$ac];
										   }
											$hotelbed_rooms .= '
												<Customer type = "CH">
													<Age>'.$child_data_age.'</Age>
												</Customer>';
											
								   }
											$hotelbed_rooms .= '
											</GuestList>';
								}
								
										$hotelbed_rooms .= '</Occupancy>
								  </HotelOccupancy>';
			}
      //echo $hotelbed_rooms;exit;

        if ($city_code != '' || $hotel_code != '') {
            $refcode = date("YmdHis");
            $xml_data = '<HotelValuedAvailRQ echoToken="DummyEchoToken" sessionId="' . $refcode . '" xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages HotelValuedAvailRQ.xsd">
							<Language>' . $language . '</Language>
							<Credentials>
								<User>' . $credencails_data['client_id'] . '</User>
								<Password>' . $credencails_data['password'] . '</Password>
							</Credentials>
							<PaginationData itemsPerPage="999" pageNumber="1"/>
							<CheckInDate date="' . $cin . '"/>
							<CheckOutDate date="' . $cout . '"/>';
							if(!isset($hotel_code)){
							$xml_data .= '<Destination code="' . $city_code . '" type="SIMPLE"/>';
							}
							else{
							$xml_data .= ' <HotelCodeList withinResults="Y">
															<ProductCode>' . $hotel_code . '</ProductCode>
													 </HotelCodeList>';	
							}                                                       
							$xml_data .= '<OccupancyList>' . $hotelbed_rooms . '</OccupancyList>
							<ExtraParamList>
															<ExtraParamList>
																 <ExtendedData type="EXT_ORDER">
																		<Name>ORDER_CONTRACT_PRICE</Name>
																		<Value>ASC</Value>
																 </ExtendedData>
															</ExtraParamList>
													 </ExtraParamList>
						</HotelValuedAvailRQ>';
             
           
            $post_url	= $credencails_data['post_url'];
            
            $searchname = 'HotelValuedAvailRQRS/HotelValuedAvailRQ';
			Savelogs($searchname, $xml_data);
			
			$response 	= curl($xml_data, $post_url); 
			
			$searchname = 'HotelValuedAvailRQRS/HotelValuedAvailRS';
			Savelogs($searchname, $response);
			
			$formated_result = hb_formate_result($response , $session_data ,$api_id, $check_in_date_format, $country_name);
			//~ echo "formated_result: <pre>";print_r($formated_result);exit;
			 
			$CI =& get_instance();
			$CI->load->model('hotel_model');
			$CI->hotel_model->save_result_gta($formated_result ,$requests, $session_data,$api_id);
	
        }
    }

    function hb_formate_result($HotelSearchRS , $session_id ,$api_id, $check_in_date_format, $country_name)
{ 
	
	 $CI =& get_instance();
	 $CI->load->model('general_model');
	 $CI->load->model('markup_model');

		//~ Markup Management
		
		$user_nationality_details = $CI->general_model->getCountryByCountyCode($country_name);
		$user_nationality = $user_nationality_details->country_id;
		//$user_nationality = $country_name;
		$admin_markup_details = $CI->markup_model->get_admin_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);
		$b2b_markup_details = $CI->markup_model->get_b2b_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);
		
		
		if($CI->session->userdata('user_type') == 4){
			$sub_agent_markup_details = $CI->markup_model->get_sub_agent_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);	
			$b2b2b_markup_details = $CI->markup_model->get_b2b2b_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);	
		}

		//~ End
			
			$dom 		= new DOMDocument();
			$dom->loadXML($HotelSearchRS);
            $sh 	= $dom->getElementsByTagName("ServiceHotel");
            $i 		= 0;
			
			$final_result=array();
            foreach ($sh as $val) {
          
			$Classification_valss=$val->getElementsByTagName("Classification");
					$Classification_val = $Classification_valss->item(0)->nodeValue;
					$Observations=$val->getElementsByTagName( "Observations" );
									$ims=0;
									$Promotionsaa='';
									foreach( $Observations as $sadasdasd )
									{
										$Promotionsaa .= $Observations->item($ims)->nodeValue;
										$ims++;
									}
                $token 		= $sh->item($i)->getAttribute("availToken");
                $incode 	= $val->getElementsByTagName("IncomingOffice");
                $inoffcode 	= $incode->item(0)->getAttribute("code");
                $destCode 	= $val->getElementsByTagName("Destination");
                $destCodeVal= $destCode->item(0)->getAttribute("code");
                $currency 	= $val->getElementsByTagName("Currency");
                $currencyva1= $currency->item(0)->nodeValue;
                $currencyv1 = $currency->item(0)->getAttribute("code");
                $org_city 	= '';
                foreach ($destCode as $destCodeddddd) {
                    $destCodeaaaa 	= $destCodeddddd->getElementsByTagName("Name");
                    $org_city 		= $destCodeaaaa->item(0)->nodeValue;
                }
                $contract = $val->getElementsByTagName("Contract");
                foreach ($contract as $contractval) {
                    $contractname 		= $contractval->getElementsByTagName("Name");
                    $contractnameVal 	= $contractname->item(0)->nodeValue;
                }
                $HotelInfo = $val->getElementsByTagName("HotelInfo");
                foreach ($HotelInfo as $HotelInfo1) { 
                    //echo "<pre/>HotelInfo1";print_r($HotelInfo1);
                    $Code 		= $HotelInfo1->getElementsByTagName("Code");
                    $codev1 	= $Code->item(0)->nodeValue;
                    $Name 		= $HotelInfo1->getElementsByTagName("Name");
                    $namev1 	= $Name->item(0)->nodeValue;
                    $image 		= $HotelInfo1->getElementsByTagName("ImageList");
                    $im 		= 0;
                    $Url 		= array();
                    foreach ($image as $Hoimageval) {
                        $Image = $Hoimageval->getElementsByTagName("Image");
                        foreach ($Image as $Imageval) {
                            $Url1 	= $Imageval->getElementsByTagName("Url");
                            $Url[] 	= str_replace('small/','',$Url1->item($im)->nodeValue);
                        }
                        $im++;
                    }
                    $picture 	= implode('-', $Url);
                    $Category 	= $HotelInfo1->getElementsByTagName("Category");
                    $categoryv1 = $Category->item(0)->getAttribute("code");
               		if($categoryv1 == '1EST'){
               		$categoryv1 = 1;	
               		}
               		else if($categoryv1 == '2EST'){
               		$categoryv1 = 2;	
               		}
               		else if($categoryv1 == '3EST'){
               		$categoryv1 = 3;	
               		}
               		else if($categoryv1 == '4EST'){
               		$categoryv1 = 4;	
               		}
               		else if($categoryv1 == '5EST'){
               		$categoryv1 = 5;	
               		}
               		else{
               		$categoryv1 = $categoryv1;	
               		}
                }

            	$hotel_details = $CI->Hotel_Model->get_hb_hotel_data($codev1,$destCodeVal);
            	/*if(@$hotel_details[0]){
            		$hotel_details = $hotel_details[0];
            	} */
              // echo "<pre>",print_r($hotel_details);exit;
                $HotelRoom = $val->getElementsByTagName("AvailableRoom");
                  if($hotel_details!='')
					  {
               

            $final_result[$i]['session_id'] = $session_id;
			$final_result[$i]['api'] = $api_id;
            $final_result[$i]['hotel_code'] =  $codev1; 
            $final_result[$i]['hotel_id'] =   $codev1; 
            $final_result[$i]['sid'] =   $destCodeVal; 
            $final_result[$i]['hotel_name'] = $hotel_details->Name;
			$final_result[$i]['hotel_api_images'] ='';
			 $a=array();
			$kk='';
			
			$ImageLinkImage = $hotel_details->hotel_images;
			if($ImageLinkImage!='')
			{
				$ags = explode(",",$ImageLinkImage);
 				 $a = $ags;
				 $kk = 	$ags[0];
			}
 			
			 $final_result[$i]['hotel_api_images'] = $kk;
			 $final_result[$i]['images'] = json_encode($a);
			
			 	
			 	$availableRoom = $HotelRoom->length;
				$j = 0;
				$amount_array = array();
                foreach ($HotelRoom as $HotelRoom1) {  
                 
                    $adult1 		= $HotelRoom1->getElementsByTagName("AdultCount");
                    $adult 			= $adult1->item(0)->nodeValue;
                    $child1 		= $HotelRoom1->getElementsByTagName("ChildCount");
                    $child 			= $child1->item(0)->nodeValue;
                    $RoomCount 		= $HotelRoom1->getElementsByTagName("RoomCount");
                    $RoomCountval 	= $RoomCount->item(0)->nodeValue;
                    if($child >0) {
						$c_v_b =$adult.','.$child;
						$child_age_v = $get_chid_age_data[$c_v_b];
					}
					else {
						$child_age_v ='';
					}
                    $shrui 			= $HotelRoom1->getElementsByTagName("HotelRoom");
                    $shruiVal 		= $shrui->item(0)->getAttribute("SHRUI");
                    $boardType 		= $HotelRoom1->getElementsByTagName("Board");
                    $boardTypeVal 	= $boardType->item(0)->getAttribute("code");
                    $shortname 		= $boardType->item(0)->getAttribute("shortname");
                    $roomType 		= $HotelRoom1->getElementsByTagName("RoomType");
                    $roomTypeVal 	= $roomType->item(0)->getAttribute("code");
                    $charVal 		= $roomType->item(0)->getAttribute("characteristic");
                    $room1 			= $HotelRoom1->getElementsByTagName("RoomType");
                    $roomv1 		= $room1->item(0)->nodeValue;
					 
					$date_final ='';
										$PriceList = $HotelRoom1->getElementsByTagName( "PriceList" );
										
										foreach($PriceList as $PriceListss)
										{
										$Price_vs = $PriceListss->getElementsByTagName( "Price" );
											foreach($Price_vs as $Price_vssda)
											{
												$Amount = $Price_vssda->getElementsByTagName( "Amount" );
												 $Amount_val_v1 = $Amount->item(0)->nodeValue;
												  $c_val = 1;
												/*if ($currencyv1 != BASE_CURRENCY) {
													$c_val1 = $CI->Hotel_Model->get_currecy_details($currencyv1);
													$c_val = $c_val1->value;
													if ($c_val == '') {
												   $c_val = 1;
														
													}
												}*/
					
												$DateTimeFrom = $Price_vssda->getElementsByTagName( "DateTimeFrom" );
												$DateTimeFrom_val_v1 =$DateTimeFrom->item(0)->getAttribute("date"); 
												$DateTimeTo = $Price_vssda->getElementsByTagName( "DateTimeTo" );
												$DateTimeTo_val_v1 =$DateTimeTo->item(0)->getAttribute("date"); 
												/*if($c_val !=1)
												{
													
													$org_amtq=$Amount_val_v1;
													$amountvq =  $Amount_val_v1 /  $c_val;
												 }
												 else
												 {
													
													 $org_amtq=$Amount_val_v1;
													 $amountvq = $Amount_val_v1;
												 }*/
												$org_amt = $Amount_val_v1;
												
												$CI = & get_instance();
												$CI->load->model('General_Model');
												//$adminmarkup = $CI->General_Model->get_adminmarkup('Hotels', $api);
												
												//~ Commented as of now 
												//~ $TotalPrice_v		= $CI->general_model->convert_api_to_base_currecy_with_markup($org_amt,$currencyv1,$api_id);
												
												$TotalPrice_v		= $org_amt;
												
												//$TotalPrice_v		= $CI->general_model->Hgenerealmarkup($org_amt,$currencyv1,$api_id);
												//if(!empty($adminmarkup)){
													//$markup_fixed=$adminmarkup[0]->markup_fixed;
													//$markup_value=$adminmarkup[0]->markup_value;
												//}else{
														//$markup_fixed=0;
														//$markup_value=0;
												//}
												
										/*		$markup_fixed=0;
												$markup_value=0;
												$total_markup_fixed2 = $amountvq+$markup_fixed;
												$total_markup_value2 = (($total_markup_fixed2*$markup_value) /100);
												$amountvq = $amountvq + $total_markup_value2;
												$amountv1q = $amountvq;*/
												
												//~ Commented as of now
												//~ $amountvq = $TotalPrice_v['TotalPrice'];
												$amountvq = $TotalPrice_v;
												$s =  (strtotime("$DateTimeTo_val_v1") - strtotime("$DateTimeFrom_val_v1")) / (60 * 60 * 24);
												$m= date('m',strtotime("$DateTimeFrom_val_v1"));
												$de= date('d',strtotime("$DateTimeFrom_val_v1"));
												$y= date('Y',strtotime("$DateTimeFrom_val_v1"));
												$ss=$s+1;
												for($i=0; $i< $ss; $i++)
												{
												
													$aa ='';
													$cc = $amountvq;	
													$cc = number_format(($cc) ,2,'.','');
													$date_final .=  '&nbsp;&nbsp;&nbsp;&nbsp;-> On '.date('d D - Y',mktime(0,0,0,$m,($de+$i),$y)).' Cost '.$cc.' '.$aa.'<br>';
												}
											}
										}
									//~ echo "HotelRoom1 data: <pre>";print_r($HotelRoom1);exit;
                    $board1 		= $HotelRoom1->getElementsByTagName("Board");
                    $boardv1 		= $board1->item(0)->nodeValue;
                    $amount 		= $HotelRoom1->getElementsByTagName("Amount");

				/*	if ($currencyv1 != BASE_CURRENCY) {
                        $c_val1 = $CI->Hotel_Model->get_currecy_details($currencyv1);
                    
                        $c_val = $c_val1->value;
                        if ($c_val != '') {
                            $org_amt = $amount->item(0)->nodeValue / $c_val;
                            $amountv = $amount->item(0)->nodeValue / $c_val;
                        } else {
                            $c_val = 1;
                            $org_amt = $amount->item(0)->nodeValue;
                            $amountv = $amount->item(0)->nodeValue;
                        } 
                    } else {
                        $c_val = 1;
                        $org_amt = $amount->item(0)->nodeValue;
                        $amountv = $amount->item(0)->nodeValue;
                    }*/
                    
                    $org_amt = $amount->item(0)->nodeValue;
                    $amountv = $amount->item(0)->nodeValue;
                    
                    $amount_array[] = $org_amt;
                    
                    $org_amt = min($amount_array);
                    $amountv = min($amount_array);
                    
                    //$amountv1 	= $amountv;
                    $j++;
                }
                $CI = & get_instance();
					$CI->load->model('General_Model');
					
						
					//$adminmarkup = $CI->General_Model->get_adminmarkup('Hotels', $api);
					
					//~ Commented as of now
					 
					//~ $TotalPrice_v		= $CI->general_model->convert_api_to_base_currecy_with_markup($org_amt,$currencyv1,$api_id);
					$TotalPrice_v		= $org_amt;
				
					$admin_markup = $b2b_markup = $b2b2_markup = $b2b_agent_markup = $b2b2b_agent_markup = 0;
					$admin_base_price = $b2b_base_price = $b2b2b_base_price = $b2b_agent_base_price = $b2b2b_agent_base_price = 0;
					$total_price_with_markup = $TotalPrice_v;
					if($admin_markup_details[0]->markup_value_type == "Percentage") {
						$admin_markup = $TotalPrice_v* ($admin_markup_details[0]->markup_value / 100);
						$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
					}else{
						$admin_markup = $admin_markup_details[0]->markup_fixed;
						$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
					}
					
					if($b2b_markup_details[0]->markup_value_type == "Percentage") {
						$b2b_markup = $admin_base_price* ($b2b_markup_details[0]->markup_value / 100);
						$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
					} else {
						$b2b_markup = $b2b_markup_details[0]->markup_value;
						$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
					}
					
					if($CI->session->userdata('user_type') == 4){
						if($sub_agent_markup_details[0]->markup_value_type == "Percentage") {
						$b2b_agent_markup = $b2b_base_price* ($sub_agent_markup_details[0]->markup_value / 100);
						$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
							} else {
						$b2b_agent_markup = $sub_agent_markup_details[0]->markup_value;
						$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
					   }
					  
					  if($b2b2b_markup_details[0]->markup_value_type == "Percentage") {
						$b2b2b_agent_markup = $b2b_agent_base_price * ($b2b2b_markup_details[0]->markup_value / 100);
						$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
						} else {
						$b2b2b_agent_markup = $b2b2b_markup_details[0]->markup_value;
						$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
					  }
				   }
				   
				  
				    $service_charge = 0;
				    $gst_charge = 0;
				    $tax_charge = 0;
				   
				    if($CI->session->userdata('user_country_id') != 0){
						$country_name = $CI->general_model->get_country_name($CI->session->userdata('user_country_id'))->row();
						$tax_details = $CI->general_model->get_tax_details($country_name->country_name, $CI->session->userdata('user_state_id'))->row();
						
						if(count($tax_details) > 0){
						 $price_with_markup_tax = $CI->general_model->tax_calculation($tax_details, $total_price_with_markup);
						 if(count($price_with_markup_tax) > 0){
							 $service_charge = $price_with_markup_tax['sc_tax'];
							 $gst_charge = $price_with_markup_tax['gst_tax'];
							 $tax_charge = $price_with_markup_tax['state_tax'];
							  $total_price_with_markup_tax = $price_with_markup_tax['amount'];
							}
						}else{
						 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
						}
					}else{
						 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
					}
					
					
				     $Netrate = $org_amt;
					 
					 $Admin_Markup =$admin_markup;
					 $My_Markup = $b2b_markup;
					 $My_Agent_Markup = $b2b_agent_markup;
					 $My_B2B2B_Markup = $b2b2b_agent_markup;
					 
					 $Admin_BasePrice =$admin_base_price;
					 $agent_baseprice = $b2b_base_price;
					 $sub_agent_baseprice = $b2b_agent_base_price;
					 $b2b2b_baseprice = $b2b2b_agent_base_price;
					 
					 $TotalPrice =$total_price_with_markup_tax;
				
					//$TotalPrice_v		= $CI->general_model->Hgenerealmarkup($org_amt,$currencyv1,$api_id);
					//if(!empty($adminmarkup)){
						//$markup_fixed=$adminmarkup[0]->markup_fixed;
						//$markup_value=$adminmarkup[0]->markup_value;
					//}else{
							//$markup_fixed=0;
							//$markup_value=0;
					//}
					//echo "<pre>",print_r($TotalPrice_v);exit;
				/*	$markup_fixed=0;
					$markup_value=0;
					$total_cost_api=$amountv;
					
					$total_markup_fixed = $total_cost_api+$markup_fixed;
					$total_markup_value = (($total_markup_fixed*$markup_value) /100);
					$total_cost			= $total_cost_api + $total_markup_value;
					$markup 			= $total_markup_value;  $this->Hotel_Model->get_markup_detail($api,$_SESSION['nationailty']);
                    $pay_charge 		= '0';  $this->Hotel_Model->get_payment_charge();*/
                    //$pay_charge 		= $CI->general_model->get_payment_charge();
                     $pay_charge 		= 0;
					//$total_markup		= $markup+$pay_charge;
                    $sec_res 			= $session_id;
                    $available          = "AVAILABLE";
                    //$fcity[] = $city;
                   // $city = explode(",", $city);
                $final_result[$i]['room_count'] = $availableRoom;
                //~ Commented as of now 
                //~ $final_result[$i]['amount'] = ($TotalPrice_v['TotalPrice']+$pay_charge);
				//~ $final_result[$i]['net_rate'] = $TotalPrice_v['Netrate'];
				//~ $final_result[$i]['admin_markup'] = $TotalPrice_v['Admin_Markup'];
				//~ $final_result[$i]['admin_baseprice'] = $TotalPrice_v['Admin_BasePrice'];
				//~ $final_result[$i]['my_markup'] = $TotalPrice_v['My_Markup'];
				$final_result[$i]['amount'] = ($TotalPrice +$pay_charge);
					
			   $final_result[$i]['net_rate'] = $Netrate ;
			   $final_result[$i]['admin_markup'] = $Admin_Markup ;
			   $final_result[$i]['my_markup'] = $My_Markup ;
			   $final_result[$i]['my_agent_Markup'] = $My_Agent_Markup ;
			   $final_result[$i]['my_b2b2b_markup'] = $My_B2B2B_Markup ;
			   
			   $final_result[$i]['admin_baseprice'] = $Admin_BasePrice ;
			   $final_result[$i]['agent_baseprice'] = $agent_baseprice ;
			   $final_result[$i]['sub_agent_baseprice'] = $sub_agent_baseprice ;
			   $final_result[$i]['b2b2b_baseprice'] = $b2b2b_baseprice ;
			   
			   $final_result[$i]['service_charge'] = $service_charge ;
			   $final_result[$i]['gst_charge'] = $gst_charge ;
			   $final_result[$i]['tax_charge'] = $tax_charge ;
			   
				$final_result[$i]['currency'] = BASE_CURRENCY ;
				$final_result[$i]['api_currency'] = $currencyv1 ;
				$final_result[$i]['status'] = 'Available' ;
				$final_result[$i]['search_date'] = date('Y-m-d H:i:s') ;

				$final_result[$i]['description'] = $hotel_details->hotel_info;
				$final_result[$i]['address'] = $hotel_details->Address;
				$final_result[$i]['star_rating'] = $categoryv1;
				$final_result[$i]['city'] =$hotel_details->cityName;
				$final_result[$i]['lat'] = $hotel_details->Latitude;
				$final_result[$i]['lon'] = $hotel_details->Longitude;
				

				$hotel_loc_hotel_s = $CI->Hotel_Model->getHBHotelLocations($codev1)->result();
				$location_s=array();
				if ($hotel_loc_hotel_s != '') {
				$fac_h_s = array();
					foreach ($hotel_loc_hotel_s as $locat) {
						$location_s[] = $locat->poiName;
					}
				}
				$location = implode(",",$location_s);
				$olocc = explode(",",$location);

      
					$hotel_fac_hotel_s = $CI->Hotel_Model->getHBHotelAmenities($codev1)->result();
					//~ echo "data: <pre>";print_r($hotel_fac_hotel_s);exit;
           			$amenitie_s=array();
        			$fac_h_s = '';
		            if ($hotel_fac_hotel_s != '') {
						 $fac_h_s = array();
		                 for ($l = 0; $l < count($hotel_fac_hotel_s); $l++) {
		                    $h_code = $hotel_fac_hotel_s[$l]->CODE;
		                   
		                        $hotel_fac_hotel_des = $CI->Hotel_Model->getHBHotelDesc($h_code, 'ENG')->row();
		                        //~ echo "hotel_fac_hotel_des: <pre>";print_r($hotel_fac_hotel_des);
								
								if(@$hotel_fac_hotel_des->CLASSICON != '')
								{
									//~ echo "amenitie_s array: <pre>",print_r('Gulsan');die();
									$fac_h_s[$hotel_fac_hotel_des->CLASSICON] = $hotel_fac_hotel_des->NAME;
									$amenitie_s[] = $hotel_fac_hotel_des->CLASSICON.'@'.$hotel_fac_hotel_des->NAME;
									//~ echo "amenitie_s array: <pre>",print_r('Gulsan');exit;
		                   		}
		                }
		                //~ echo "amenitie_s array after forloop: <pre>",print_r($amenitie_s);
		              
		            } 
					//~ echo "amenitie_s: <pre>",print_r($amenitie_s);
					$amenities = implode(",",$amenitie_s);
					//~ echo "amenities: <pre>",print_r($amenities);
					$amenities1 = explode(",",$amenities);
					//~ echo "amenities1: <pre>",print_r($amenities1);


					$final_result[$i]['facilities'] =   json_encode($amenities1);
					//~ echo "facilities: <pre>",print_r($final_result[$i]['facilities']);exit;
					$final_result[$i]['locations'] =   json_encode($olocc);
					$temp[] = $codev1;

					/*$CI1 = & get_instance();
					$CI1->load->model('Hotel_Model');
					$CI1->Hotel_Model->insert_hotelsbed_temp_result($available_data);*/ 
            }
                $i++; 
            } 
			//~ exit;
			 return $final_result;
}
    

function HotelValuedRoomAvailRQ($request_v,$api_id,$session_data,$hotel_code){
	
	
	
	if (!file_exists('hotel_xmldata/HotelValuedRoomAvailRQRS')) {
		mkdir('hotel_xmldata/HotelValuedRoomAvailRQRS', 0777, true);
	} 
	$credintials_data = set_crediential($api_id); 
	$request = json_decode(json_encode($request_v), true);

	$URL = $credintials_data['post_url'];
	$timestamp = date("YmdHis");
	
		
	$check_in = str_replace('-', '', date('Y-m-d', strtotime($request['hotel_checkin'])));
	$check_out = str_replace('-', '', date('Y-m-d', strtotime($request['hotel_checkout'])));
	$CI =& get_instance();
	$CI->load->model('Hotel_Model');
	$CI->load->model('general_model');
	/*$city_data = $CI->hotel_model->get_city_details($request['city']);
	$DestinationCode = $city_data->cityCode;*/
	if(isset($request['hotel_name'])){
	$city_data = $CI->general_model->get_city_details_cityname($request['city']);	
	}
	else{
	$city_data = $CI->general_model->get_city_details_id($request['city']);
	}

	 $DestinationCode = $city_data->hotelbeds_city_code;
	$HotelOccupancy = '';

	for ($i = 0; $i < $request['rooms']; $i++) {
		$array_vals[] = $request['adult'][$i] . "," . $request['child'][$i];
	} 
	$check_array = array();


	for ($k = 0; $k < count($array_vals); $k++) {
		//echo $childagevar = 'childAges'.($k+1);exit;
		 //echo $childagevar = 'childAges['.$k.']';

		if (isset($request['childAges'][$k])) {
				$child_age = implode(",",$request['childAges'][$k]);
		} else {
				$child_age = '';
		}

		$key = array_search($array_vals[$k], $check_array);

		if($key) {
				unset($check_array[$key]);
				$split_key = explode("||", $key);
				$key_count = $split_key[0] + 1;
				$check_array[$key_count . "||" . $split_key[1] . "," . $child_age] = $array_vals[$k];
		}else{
				$check_array['1||' . $child_age . '||' . $k] = $array_vals[$k];
		}
	} 
	
	
	$array_count = 0;
	foreach($check_array as $key => $value) {
		$key_explosion = explode('||',$key);
		$value_explosion = explode(',',$value);
		//~ echo "key_explosion: <pre>";print_r($key_explosion);
		//~ echo "value_explosion: <pre>";print_r($value_explosion);
		$room_combination_index = $key_explosion[0];
		$child_age_signature = $key_explosion[1];
		$room_index = $key_explosion[2];
		
		$adult_combination_count = $value_explosion[0];
		$child_combination_count = $value_explosion[1];
		
		$guest_list = '';
		$customer = '';
		
		//~ echo "customer: <pre>";print_r($customer);
		if($child_combination_count > 0) {
			if($child_age_signature != '') {
				$signature_explosion = explode(',',$child_age_signature);
				for($ch_age_iter = 0;$ch_age_iter < count($signature_explosion);$ch_age_iter++) {
					$customer .= '<Customer type="CH">
										<Age>'.$signature_explosion[$ch_age_iter].'</Age>
									</Customer>';
				}
			}
			$guest_list .= '<GuestList>
							'.$customer.'
							</GuestList>';
		}
		
		$HotelOccupancy .= '<HotelOccupancy>
								<RoomCount>'.$room_combination_index.'</RoomCount>
								<Occupancy>
									<AdultCount>'.$adult_combination_count.'</AdultCount>
									<ChildCount>'.$child_combination_count.'</ChildCount>
									'.$guest_list.'
								</Occupancy>
							</HotelOccupancy>';
		
		
		$array_count++;
		//~ echo "value_explosion: <pre>";print_r($available_room);
	}
	
	//~ foreach ($check_array as $key => $value) {
		//~ 
		//~ $room_data = explode("||", $key);
		//~ $adult_child_data = explode(",", $value);
		//~ $childage_data = explode(",", $room_data[1]);
		//~ $HotelOccupancy.='<HotelOccupancy>
							//~ <RoomCount>' . $room_data[0] . '</RoomCount>
							//~ <Occupancy>
								//~ <AdultCount>' . $adult_child_data[0] . '</AdultCount>
								//~ <ChildCount>' . $adult_child_data[1] . '</ChildCount>';
								//~ if ($adult_child_data[1] != 0) {
									//~ $HotelOccupancy .= '<GuestList>';
									//~ for ($ac = 0; $ac < count($childage_data); $ac++) {
										//~ $HotelOccupancy .= '<Customer type = "CH">
																//~ <Age>' . $childage_data[$ac] . '</Age>
															//~ </Customer>';
									//~ }
									//~ $HotelOccupancy .= '</GuestList>';
								//~ }
		//~ $HotelOccupancy .= '</Occupancy></HotelOccupancy>';
	//~ }
	
	$HotelValuedAvailRQ = '<?xml version="1.0" encoding="UTF-8"?>
												<HotelValuedAvailRQ xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" echoToken="DummyEchoToken" sessionId="'.$timestamp.'" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages HotelValuedAvailRQ.xsd" showDiscountsList="Y" version="2011/01">                                                                      
													 <Language>ENG</Language>
													
							<Credentials>
								<User>' . $credintials_data['client_id'] . '</User>
								<Password>' . $credintials_data['password'] . '</Password>
							</Credentials>
													 <PaginationData itemsPerPage="999" pageNumber="1" />
													 <CheckInDate date="'.$check_in.'" />
													 <CheckOutDate date="'.$check_out.'" />
													 <Destination code="'.$DestinationCode.'" type="SIMPLE" />
													  <HotelCodeList withinResults="Y">
															<ProductCode>' . $hotel_code . '</ProductCode>
													 </HotelCodeList>
													 <OccupancyList>
															'.$HotelOccupancy.'
													 </OccupancyList>
													 <ExtraParamList>
															<ExtraParamList>
																 <ExtendedData type="EXT_ORDER">
																		<Name>ORDER_CONTRACT_PRICE</Name>
																		<Value>ASC</Value>
																 </ExtendedData>
															</ExtraParamList>
													 </ExtraParamList>
												</HotelValuedAvailRQ>';
	$type='HotelValuedAvailRQ'; 
	
	$searchname = 'HotelValuedRoomAvailRQRS/HotelValuedAvailRQ';
	Savelogs($searchname, $HotelValuedAvailRQ);
	
	// processHotelRequest($HotelValuedAvailRQ, $URL,$type);
	$HotelValuedAvailRS= curl($HotelValuedAvailRQ, $URL);	
	
	$searchname = 'HotelValuedRoomAvailRQRS/HotelValuedAvailRS';
	Savelogs($searchname, $HotelValuedAvailRS);
	
	//~ header("Content-Type: text/xml");echo $HotelValuedAvailRQ;
	//~ header("Content-Type: text/xml");echo $HotelValuedAvailRS; die(); 
	
	//~ $xmlobj=new SimpleXMLElement($HotelValuedAvailRQ);
	//~ $xmlobj->asXML("hotel_xmldata/HotelValuedRoomAvailRQRS/HotelValuedRoomAvailRQ.xml");
	//~ $xmlobj1=new SimpleXMLElement($HotelValuedAvailRS);
	//~ $xmlobj1->asXML("hotel_xmldata/HotelValuedRoomAvailRQRS/HotelValuedRoomAvailRS.xml");
	
	//header("Content-Type: text/xml");echo $HotelValuedAvailRS; die();     
	    
	$reqres =  array('request'=>$HotelValuedAvailRQ, 'response'=>$HotelValuedAvailRS, 'xml_title'=>$type, 'api_id'=>$api_id, 'xml_url'=>$URL);
	HB_format_api_response_room($reqres, $api_id, $session_data,$request,$hotel_code);
	
}	

function HB_format_api_response_room($reqres, $api_id, $session_id,  $request,$hotel_code){	
	  //~ echo "request: <pre>";print_r($request);exit;
	  
	 
	  	$CI =& get_instance();
		$CI->load->model('Hotel_Model');
		$CI->load->model('general_model');
		
		 $CI->load->model('markup_model');
		 $check_in_date_format = $request['hotel_checkin'];
         $country = explode(",", $request['city']);
         $country_name = trim($country[1]);
		//~ Markup Management
		// user nationality changed to searching country variable name is not changed
		$user_nationality_details = $CI->general_model->getCountryByCountyCode($country_name);
		$user_nationality = $user_nationality_details->country_id;
		//$user_nationality = $country_name;
		$admin_markup_details = $CI->markup_model->get_admin_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);
		$b2b_markup_details = $CI->markup_model->get_b2b_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);
		
		if($CI->session->userdata('user_type') == 4){
			$sub_agent_markup_details = $CI->markup_model->get_sub_agent_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);	
			$b2b2b_markup_details = $CI->markup_model->get_b2b2b_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);	
		}
		
		if(isset($request['hotel_name'])){
		$city_data = $CI->general_model->get_city_details_cityname($request['city']);	
		$city = $city_data->city_name;
		}
		else{
		$city_data = $CI->Hotel_Model->get_city_details($request['city']);
		//~ echo "request: <pre>";print_r($city_data);exit;
		$city = $city_data->cityName;
	   } 
	    $DestinationCode = $city_data->hotelbeds_city_code;
		$dom 		= new DOMDocument();
		if(isset($reqres['response'])){
			//~ echo $reqres['request'].$reqres['response'];exit;
		$dom->loadXML($reqres['response']);
		$sh 	= $dom->getElementsByTagName("ServiceHotel");
		$i 		= 0;
		
		
		for($i=0;$i< $request['rooms'];$i++){
			$array_vals[] =  $request['adult'][$i].",".$request['child'][$i];
		}
		$hotelbed_rooms	= '';
		$check_array	= array();
		for ($k = 0; $k < count($array_vals); $k++) {
			$childagevar = 'child_age'.($k+1);
			if (isset($request[$childagevar])) {
					$child_age = implode(",",$request[$childagevar]);
			} else {
					$child_age = '';
			}
			$key = array_search($array_vals[$k], $check_array);

			if($key) {
					unset($check_array[$key]);
					$split_key = explode("||", $key);
					$key_count = $split_key[0] + 1;
					$check_array[$key_count . "||" . $split_key[1] . "," . $child_age] = $array_vals[$k];
			}else{
					$check_array['1||' . $child_age . '||' . $k] = $array_vals[$k];
			}
		}
		$get_chid_age_data=array();
		foreach($check_array as $key=>$value){
			$room_data = explode("||",$key);
			$adult_child_data = explode(",",$value);
			$childage_data = explode(",",$room_data[1]);
			$get_chid_age_data[$value] = 	$room_data[1];
		}
		foreach ($sh as $val) {
			$Classification_valss=$val->getElementsByTagName("Classification");
			$Classification_val = $Classification_valss->item(0)->nodeValue;
			$Observations=$val->getElementsByTagName( "Observations" );
			$ims=0;
			$Promotionsaa='';
			foreach( $Observations as $sadasdasd ){
				$Promotionsaa .= $Observations->item($ims)->nodeValue;
				$ims++;
			}
			//if (is_object($sh->item($i))){
				$token 		= $sh->item(0)->getAttribute("availToken");
	//		} 
			
			
			$incode 	= $val->getElementsByTagName("IncomingOffice");
			$inoffcode 	= $incode->item(0)->getAttribute("code");
			$destCode 	= $val->getElementsByTagName("Destination");
			$destCodeVal= $destCode->item(0)->getAttribute("code");
			$currency 	= $val->getElementsByTagName("Currency");
			$currencyva1= $currency->item(0)->nodeValue;
			$currencyv1 = $currency->item(0)->getAttribute("code");
			$org_city 	= '';
			foreach ($destCode as $destCodeddddd) {
				$destCodeaaaa 	= $destCodeddddd->getElementsByTagName("Name");
				$org_city 		= $destCodeaaaa->item(0)->nodeValue;
			}
			$contract = $val->getElementsByTagName("Contract");
			foreach ($contract as $contractval) {
				$contractname 		= $contractval->getElementsByTagName("Name");
				$contractnameVal 	= $contractname->item(0)->nodeValue;
			}
			$HotelInfo = $val->getElementsByTagName("HotelInfo");

			foreach ($HotelInfo as $HotelInfo1) {
				  $Code 		= $HotelInfo1->getElementsByTagName("Code");
				
				
				$codev1 	= $Code->item(0)->nodeValue;
				$Name 		= $HotelInfo1->getElementsByTagName("Name");
				$namev1 	= $Name->item(0)->nodeValue;
				$image 		= $HotelInfo1->getElementsByTagName("ImageList");
				$im 		= 0;
				$Url 		= array();
				foreach ($image as $Hoimageval) {
					$Image = $Hoimageval->getElementsByTagName("Image");
					foreach ($Image as $Imageval) {
						$Url1 	= $Imageval->getElementsByTagName("Url");
						$Url[] 	= str_replace('small/','',$Url1->item($im)->nodeValue);
					}
					$im++;
				}
				$picture 	= implode('-', $Url);
				$Category 	= $HotelInfo1->getElementsByTagName("Category");
				$categoryv1 = $Category->item(0)->getAttribute("code");
			} 
			$HotelRoom = $val->getElementsByTagName("AvailableRoom");
			$j = 0;
			$hotel_details = $CI->Hotel_Model->get_hb_hotel_data($hotel_code,$destCodeVal);
			$contact_details = $CI->Hotel_Model->get_contact_details($hotel_code);
			$contact_details = $contact_details->result();
			foreach ($contact_details as $contact_detail) {
						
				if($contact_detail->phone_type == "phoneHotel"){
					$con_no = $contact_detail->phone_number;
				}
			}
			
			foreach ($HotelRoom as $HotelRoom1) {
				 $adult1 		= $HotelRoom1->getElementsByTagName("AdultCount");
				 $adult 			= $adult1->item(0)->nodeValue;
				 $child1 		= $HotelRoom1->getElementsByTagName("ChildCount");
				 $child 			= $child1->item(0)->nodeValue;
				 $RoomCount 		= $HotelRoom1->getElementsByTagName("RoomCount");
				 $RoomCountval 	= $RoomCount->item(0)->nodeValue;
				 if($child >0) {
					 $c_v_b =$adult.','.$child;
					 $child_age_v = $get_chid_age_data[$c_v_b];
				 } else {
					 $child_age_v ='';
				 }
                 $shrui 			= $HotelRoom1->getElementsByTagName("HotelRoom");
                 $shruiVal 			= $shrui->item(0)->getAttribute("SHRUI");
                 $rooms_avilable 	= $shrui->item(0)->getAttribute("availCount");
                 $boardType 		= $HotelRoom1->getElementsByTagName("Board");
                 $boardTypeVal 	= $boardType->item(0)->getAttribute("code");
                 $shortname 		= $boardType->item(0)->getAttribute("shortname");
                 $roomType 		= $HotelRoom1->getElementsByTagName("RoomType");
                 $roomTypeVal 	= $roomType->item(0)->getAttribute("code");
                 $charVal 		= $roomType->item(0)->getAttribute("characteristic");
                 $room1 			= $HotelRoom1->getElementsByTagName("RoomType");
                 $roomv1 		= $room1->item(0)->nodeValue;
                 $date_final ='';
                 $PriceList = $HotelRoom1->getElementsByTagName( "PriceList" );
                 foreach($PriceList as $PriceListss){
					 $Price_vs = $PriceListss->getElementsByTagName( "Price" );
					 foreach($Price_vs as $Price_vssda){
						 $Amount = $Price_vssda->getElementsByTagName( "Amount" );
						 $Amount_val_v1 = $Amount->item(0)->nodeValue;
						
						$DateTimeFrom = $Price_vssda->getElementsByTagName( "DateTimeFrom" );
						$DateTimeFrom_val_v1 =$DateTimeFrom->item(0)->getAttribute("date"); 
						$DateTimeTo = $Price_vssda->getElementsByTagName( "DateTimeTo" );
						$DateTimeTo_val_v1 =$DateTimeTo->item(0)->getAttribute("date"); 
						
						 //$pay_charge 		= $CI->general_model->get_payment_charge();
						 $pay_charge 		= 0;
						 //~ Commented as of now 
						 //~ $amountvqs = $CI->general_model->convert_api_to_base_currecy_with_markup($Amount_val_v1,$currencyv1,$api_id);
						 $amountvqs = $Amount_val_v1;
						 //~ $amountvq = ($amountvqs['TotalPrice'] + $pay_charge);
						 $amountvq = ($amountvqs + $pay_charge);
						// $amountvq		= $CI->general_model->Hgenerealmarkup($Amount_val_v1,$currencyv1,$api_id);
						 $s =  (strtotime("$DateTimeTo_val_v1") - strtotime("$DateTimeFrom_val_v1")) / (60 * 60 * 24);
						$m= date('m',strtotime("$DateTimeFrom_val_v1"));
						$de= date('d',strtotime("$DateTimeFrom_val_v1"));
						$y= date('Y',strtotime("$DateTimeFrom_val_v1"));
						$ss=$s+1;
						for($i=0; $i< $ss; $i++){
							$aa ='';
							$cc = $amountvq;	
							$cc = number_format(($cc) ,2,'.','');
							$date_final .=  '&nbsp;&nbsp;&nbsp;&nbsp;-> On '.date('d D - Y',mktime(0,0,0,$m,($de+$i),$y)).' Cost '.$cc.' '.$aa.'<br>';
						}
					}
				}	
					$board1 		= $HotelRoom1->getElementsByTagName("Board");
                    $boardv1 		= $board1->item(0)->nodeValue;
                    $amount 		= $HotelRoom1->getElementsByTagName("Amount");
                    $org_amt = $amount->item(0)->nodeValue;
					$pay_charge 		= 0;
					$TotalPrice_v		= $org_amt;
					$Netrate = $org_amt;
					
					
					$admin_markup = $b2b_markup = $b2b2_markup = $b2b_agent_markup = $b2b2b_agent_markup = 0;
					$admin_base_price = $b2b_base_price = $b2b2b_base_price = $b2b_agent_base_price = $b2b2b_agent_base_price = 0;
					$total_price_with_markup = $TotalPrice_v;
					if($admin_markup_details[0]->markup_value_type == "Percentage") {
						$admin_markup = $TotalPrice_v* ($admin_markup_details[0]->markup_value / 100);
						$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
					}else{
						$admin_markup = $admin_markup_details[0]->markup_fixed;
						$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
					}
					
					if($b2b_markup_details[0]->markup_value_type == "Percentage") {
						$b2b_markup = $admin_base_price* ($b2b_markup_details[0]->markup_value / 100);
						$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
					} else {
						$b2b_markup = $b2b_markup_details[0]->markup_value;
						$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
					}
					
					if($CI->session->userdata('user_type') == 4){
						if($sub_agent_markup_details[0]->markup_value_type == "Percentage") {
						$b2b_agent_markup = $b2b_base_price* ($sub_agent_markup_details[0]->markup_value / 100);
						$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
							} else {
						$b2b_agent_markup = $sub_agent_markup_details[0]->markup_value;
						$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
					   }
					  
					  if($b2b2b_markup_details[0]->markup_value_type == "Percentage") {
						$b2b2b_agent_markup = $b2b_agent_base_price * ($b2b2b_markup_details[0]->markup_value / 100);
						$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
						} else {
						$b2b2b_agent_markup = $b2b2b_markup_details[0]->markup_value;
						$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
					  }
				   }
					
					if(@$admin_markup == '' || @$admin_markup == null):
								$admin_markup = 0;
							endif;
							if(@$b2b_markup == '' || @$b2b_markup == null):
								$b2b_markup = 0;
							endif;
							if(@$b2b_agent_markup == '' || @$b2b_agent_markup == null):
								$b2b_agent_markup = 0;
							endif;
							if(@$b2b2b_agent_markup == '' || @$b2b2b_agent_markup == null):
								$b2b2b_agent_markup = 0;
							endif;
						
				     $service_charge = 0;
				    $gst_charge = 0;
				    $tax_charge = 0;
				   
				    if($CI->session->userdata('user_country_id') != 0){
						$country_name = $CI->general_model->get_country_name($CI->session->userdata('user_country_id'))->row();
						$tax_details = $CI->general_model->get_tax_details($country_name->country_name, $CI->session->userdata('user_state_id'))->row();
						
						if(count($tax_details) > 0){
						 $price_with_markup_tax = $CI->general_model->tax_calculation($tax_details, $total_price_with_markup);
						 if(count($price_with_markup_tax) > 0){
							 $service_charge = $price_with_markup_tax['sc_tax'];
							 $gst_charge = $price_with_markup_tax['gst_tax'];
							 $tax_charge = $price_with_markup_tax['state_tax'];
							  $total_price_with_markup_tax = $price_with_markup_tax['amount'];
							}
						}else{
						 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
						}
					}else{
						 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
					}      
					       
						
							  $Admin_Markup =$admin_markup;
							  $My_Markup = $b2b_markup;
							  $My_Agent_Markup = $b2b_agent_markup;
					          $My_B2B2B_Markup = $b2b2b_agent_markup;
							   
							  $Admin_BasePrice =$admin_base_price;
							  $agent_baseprice = $b2b_base_price;
							  $sub_agent_baseprice = $b2b_agent_base_price;
							  $b2b2b_baseprice = $b2b2b_agent_base_price;
							  
							  $TotalPrice =$total_price_with_markup_tax;
					 
					 
			$hotel_fac_hotel_s = $CI->Hotel_Model->getHBHotelAmenities($codev1)->result();
			$amenitie_s=array();
			$fac_h_s = '';
            if ($hotel_fac_hotel_s != '') {
				 $fac_h_s = array();
                 for ($l = 0; $l < count($hotel_fac_hotel_s); $l++) {
                    $h_code = $hotel_fac_hotel_s[$l]->CODE;
                   
                        $hotel_fac_hotel_des = $CI->Hotel_Model->getHBHotelDesc($h_code, 'ENG')->row();
						if(isset($hotel_fac_hotel_des->CLASSICON) && $hotel_fac_hotel_des->CLASSICON!='')
						{
                       	 $fac_h_s[$hotel_fac_hotel_des->CLASSICON] = $hotel_fac_hotel_des->NAME;
                    	$amenitie_s[] = $hotel_fac_hotel_des->CLASSICON;
                   		 }
                }
              
            }
					$amenities = implode(",",$amenitie_s);
			$fac_h_s_json = '';
			if(is_array($fac_h_s))
			{
			$fac_h_s_json = json_encode($fac_h_s);
			}
			 
                    $available_data = array(
						'session_id' 		=> $session_id, 
						'api' 				=> $api_id, 
						'hotel_code' 		=> $codev1, 
						'room_code' 		=> $roomTypeVal, 
						'room_type' 		=> $roomv1, 
						'hotel_api_images'	=> $picture,
						'inclusion' 		=> $boardv1, 
						'amount' 		=> ($TotalPrice+$pay_charge), 
						'total_cost' 		=> ($TotalPrice+$pay_charge), 
						'net_rate' 		=> $Netrate, 
						'admin_markup' => $Admin_Markup,
                        'my_markup' =>  $My_Markup,
                        'my_agent_Markup' => $My_Agent_Markup,
		                'my_b2b2b_markup' => $My_B2B2B_Markup,
                        'admin_baseprice' => $Admin_BasePrice,
						'agent_baseprice' => $agent_baseprice,
						'sub_agent_baseprice' => $sub_agent_baseprice,
						'b2b2b_baseprice' => $b2b2b_baseprice,
						
						'service_charge' => $service_charge,
						'gst_charge' => $gst_charge,
						'tax_charge' => $tax_charge,
						'total_cost' => $TotalPrice,
						'xml_currency' 		=> $currencyv1, 
					 	'hotel_description' => $hotel_details->hotel_info,
					 	'hotel_contact' => $con_no,
					 	'hotel_address' => $hotel_details->Address,
						'hotel_rating' => $hotel_details->CategoryCode,
						'status' 			=> 'Available', 
						'shurival' 			=> $shruiVal, 
						'charval' 			=> $charVal, 
						'adult' 			=> $adult, 
						'child' 			=> $child, 
						'board_type' 		=> $boardTypeVal, 
						'city' 				=> $city,
						'token' 			=> $token, 
						'inoffcode' 		=> $inoffcode, 
						'contractnameVal'	=> $contractnameVal, 
						'Classification_val'=> $Classification_val,
						'destCodeVal' 		=> $destCodeVal, 
						'shortname' 		=> $shortname, 
						'room_count' 		=> $RoomCountval, 
						'available_room_count'	=> $rooms_avilable,
						'Promotionsaa' 		=> $Promotionsaa,
						'hotel_facility'   => $fac_h_s_json,
						'perroomcost' 		=> $date_final,
						'child_age' 		=> $child_age_v,
						
					);
					//~ echo "available data: <pre>";print_r($available_data);
					$CI->Hotel_Model->insert_hotelsbed_temp_result($available_data);
                   
                    $j++;		
			}
			$i++;
		}
	
		}
	}
    
function curl($request, $post_url) {
        ini_set("memory_limit", "-1"); 
        $ch2 = curl_init();
        //echo $request;
         //~ echo  $post_url;exit;
        curl_setopt($ch2, CURLOPT_URL, $post_url);
        curl_setopt($ch2, CURLOPT_TIMEOUT, 580);
        curl_setopt($ch2, CURLOPT_HEADER, 0);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_POST, 1);
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, true);
        //~ curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
		//~ curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, false);
        $httpHeader2 = array(
            "Content-Type: text/xml; charset=UTF-8",
            "Content-Encoding: UTF-8",
            "Accept-Encoding: gzip,deflate"
        );
        curl_setopt($ch2, CURLOPT_HTTPHEADER, $httpHeader2);
        curl_setopt($ch2, CURLOPT_ENCODING, "gzip,deflate");
        // Execute request, store response and HTTP response code
        $data2 = curl_exec($ch2);
        if(curl_errno($ch2)){
			//~ echo 'Curl error: ' . curl_error($ch2);
		}
        //~ echo "data: <pre>";print_r($request);
        //~ echo "data: <pre>";print_r($data2);exit;
        $error2 = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
        return $data2;
    }
     
 function HotelServiceAddRQ($request,$hotel_code,$booking_cart_id,$booking_global_id, $session_id, $api, $flag){
	 
	  $currency = $_SESSION['currency'];
	  $exchange_rate = $_SESSION['currency_value'];
	  	
	if (!file_exists('hotel_xmldata/ServiceAddRQRS')) {
		mkdir('hotel_xmldata/ServiceAddRQRS', 0777, true);
	}
	
		$request = json_decode(base64_decode($request));
		
		$request_data_v1 = $request;
		$credintials_data=set_crediential($api);
		$URL = $credintials_data['post_url'];
		$timestamp = date("YmdHis");
		$CI =& get_instance();
		$CI->load->model('Hotel_Model');
		$CI->load->model('cart_model');
		$CI->load->model('general_model');
		$CI->load->model('markup_model');
		$cart_result = $CI->cart_model->getBookingTemp_hotel($booking_global_id);

		$check_in = str_replace('-', '', date('Ymd', strtotime($request_data_v1->hotel_checkin)));
		$check_in_date_format = date('Y-m-d', strtotime($request_data_v1->hotel_checkin));
		$check_out = str_replace('-', '', date('Ymd', strtotime($request_data_v1->hotel_checkout)));
		$country = explode(",", $request_data_v1->city);
		$country_name = trim($country[1]);
		//~ Markup Management
		
		$user_nationality_details = $CI->general_model->getCountryByCountyCode($country_name);
		$user_nationality = $user_nationality_details->country_id;
		//$user_nationality = $country_name;
		$admin_markup_details = $CI->markup_model->get_admin_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api);
		$b2b_markup_details = $CI->markup_model->get_b2b_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api);
		if($CI->session->userdata('user_type') == 4){
			$sub_agent_markup_details = $CI->markup_model->get_sub_agent_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api);	
			$b2b2b_markup_details = $CI->markup_model->get_b2b2b_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api);	
		}
		
		//~ Cancellation Buffer
		
		$cancel_buffer = $CI->General_Model->getCancellationBuffer(1,$api)->row();
        if(empty($cancel_buffer) && $cancel_buffer == '') {
			$buffer_days = 0;
		}
		else {
			$buffer_days = $cancel_buffer->cancel_days;
		}
		
		//~ End 
		
		
		$room_cnt = $cart_result->room_count;
		$token_code = array();
		$contractnameVal = array();
		$inoffcode = array();
		$destCodeVal = array();
		$child = array();
		$room_count = array();
		$adult = array();
		$shurival = array();
		$board_type = array();
		$shortname = array();
		$charval = array();
		$room_code = array();
		$child_age_db = array();
		for ($i = 0; $i < $request_data_v1->rooms; $i++) {
			$array_vals[] = $request_data_v1->adult[$i] . "," . $request_data_v1->child[$i];
		}
		$hotelbed_rooms = '';
		$check_array = array();
		for ($k = 0; $k < count($array_vals); $k++) {
				if (isset($request_data_v1->childAges[$k])) {
					$child_age = implode(",",$request_data_v1->childAges[$k]);
				} else {
					$child_age = '';
				}
				$key = array_search($array_vals[$k], $check_array);
				if ($key) {
						unset($check_array[$key]);
						$split_key = explode("||", $key);
						$key_count = $split_key[0] + 1;
						$check_array[$key_count . "||" . $split_key[1] . "," . $child_age] = $array_vals[$k];
				} else {
						$check_array['1||' . $child_age . '||' . $k] = $array_vals[$k];
				}
		}
		//~ echo "check_array: <pre>";print_r($check_array);
		//~ echo "fgfgf data: <pre>";print_r(json_decode(base64_decode($cart_result->request)));exit;
		$cAdultArray = explode("<br>",rtrim($cart_result->adult,"<br>"));
		$cChildArray = explode("<br>",rtrim($cart_result->child,"<br>"));
		//~ echo "cAdultArray: <pre>";print_r($cAdultArray);
		//~ echo "cChildArray: <pre>";print_r($cChildArray);
		if (strrpos($room_cnt, "<br>") == false) {
			$token_code[] = $cart_result->token;
			$contractnameVal[] = $cart_result->contractnameVal;
			$inoffcode[] = $cart_result->inoffcode;
			$hotel_code = $cart_result->hotel_code;
			$destCodeVal[] = $cart_result->destCodeVal;
			$child[] = $cart_result->child;
			$room_count[] = $cart_result->room_count;
			$adult[] = $cart_result->adult;
			$shurival[] = $cart_result->shurival;
			$board_type[] = $cart_result->board_type;
			$shortname[] = $cart_result->shortname;
			$charval[] = $cart_result->charval;
			$child_age_db[] = $cart_result->child_age;
			$room_code[] = $cart_result->room_code;
			$room_count_data = 1;
		} else {
			$token_code = explode("<br>", $cart_result->token_id);
			$contractnameVal = explode("<br>", $cart_result->contractnameVal);
			$inoffcode = explode("<br>", $cart_result->inoffcode);
			$hotel_code = $cart_result->hotel_code;
			$destCodeVal = explode("<br>", $cart_result->destCodeVal);
			$child = explode("<br>", $cart_result->child);
			$room_count = explode("<br>", $cart_result->room_count);
			$adult = explode("<br>", $cart_result->adult);
			$shurival = explode("<br>", $cart_result->shurival);
			$board_type = explode("<br>", $cart_result->board_type);
			$shortname = explode("<br>", $cart_result->shortname);
			$charval = explode("<br>", $cart_result->charval);
			$room_code = explode("<br>", $cart_result->room_id);
			$child_age_db = explode("<br>", $cart_result->child_age);
			$room_count_data = count($token_code);
		}
		//~ echo "shurival: <pre>";print_r($shurival);
		//~ echo "board_type: <pre>";print_r($board_type);
		//~ echo "shortname: <pre>";print_r($shortname);
		//~ echo "room_code: <pre>";print_r($room_code);
		//~ echo "charval: <pre>";print_r($charval);
		$room1ss = '';

		$l = 0;
		//~ if($request_data_v1->rooms == 3) {
			//~ $check_array = array_reverse($check_array,true);
		//~ }
		
		//~ echo "Check Array: <pre>";print_r($check_array);exit;
		$array_count = 0;
		$available_room = '';
		foreach($check_array as $key => $value) {
			$key_explosion = explode('||',$key);
			$value_explosion = explode(',',$value);
			//~ echo "key_explosion: <pre>";print_r($key_explosion);
			//~ echo "value_explosion: <pre>";print_r($value_explosion);
			$room_combination_index = $key_explosion[0];
			$child_age_signature = $key_explosion[1];
			$room_index = $key_explosion[2];
			
			$adult_combination_count = $value_explosion[0];
			$child_combination_count = $value_explosion[1];
			//~ echo "adult_combination_count: <pre>";print_r($adult_combination_count);
			//~ echo "child_combination_count: <pre>";print_r($child_combination_count);
			//~ echo "val: <pre>";print_r($cAdultArray[array_search($adult_combination_count, $cAdultArray)]);
			for($pax_iter = 0; $pax_iter < count($cAdultArray); $pax_iter++ ) {
				if($adult_combination_count == $cAdultArray[$pax_iter] && $child_combination_count == $cChildArray[$pax_iter]) {
					$SHURIVal = $shurival[$pax_iter];
					$BOARDTypeVal = $board_type[$pax_iter];
					$SHORTNameVal = $shortname[$pax_iter];
					$ROOMCodeVal = $room_code[$pax_iter];
					$CHARVal = $charval[$pax_iter];
					break;
				}
				else {
					continue;
				}
			}
			//~ echo "SHURIVal: <pre>";print_r($SHURIVal);exit;
			
			$guest_list = '';
			$customer = '';
			
			//~ echo "customer: <pre>";print_r($customer);
			if($child_combination_count > 0) {
				if($child_age_signature != '') {
					$signature_explosion = explode(',',$child_age_signature);
					for($ch_age_iter = 0;$ch_age_iter < count($signature_explosion);$ch_age_iter++) {
						$customer .= '<Customer type="CH">
											<Age>'.$signature_explosion[$ch_age_iter].'</Age>
										</Customer>';
					}
				}
				$guest_list .= '<GuestList>
								'.$customer.'
								</GuestList>';
			}
			
			$available_room .= '<AvailableRoom>
									<HotelOccupancy>
										<RoomCount>'.$room_combination_index.'</RoomCount>
										<Occupancy>
											<AdultCount>'.$adult_combination_count.'</AdultCount>
											<ChildCount>'.$child_combination_count.'</ChildCount>
											'.$guest_list.'
										</Occupancy>
									</HotelOccupancy>
									<HotelRoom SHRUI="' . str_replace('<br>','',$SHURIVal) . '" >
										<Board type="SIMPLE" code="' . str_replace('<br>','',$BOARDTypeVal) . '" shortname="' . str_replace('<br>','',$SHORTNameVal) . '"/>
										<RoomType type="SIMPLE" code="' . str_replace('<br>','',rtrim($ROOMCodeVal,"<")) . '" characteristic="' . str_replace('<br>','',$CHARVal) . '"/>
									</HotelRoom>
								</AvailableRoom>';
			
			
			$array_count++;
			//~ echo "value_explosion: <pre>";print_r($available_room);
		}
		$room1ss = $available_room;
		//~ echo "value_explosion: <pre>";print_r($available_room);
		
		if (strrpos($token_code[0], "<br>") == false) {
			$tokencode = $token_code[0];
		} else {
			$tokencode = str_replace('<br>','', $token_code[0]);
		}  
		$request = '<ServiceAddRQ xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages ServiceAddRQ.xsd" echoToken="token">
									 <Language>ENG</Language>
													
							<Credentials>
								<User>' . $credintials_data['client_id'] . '</User>
								<Password>' . $credintials_data['password'] . '</Password>
							</Credentials>
									<Service xsi:type="ServiceHotel"  availToken="' . $tokencode . '">
										<ContractList>
											<Contract>
												<Name>' . str_replace('<br>','',$contractnameVal[0]) . '</Name>
												<IncomingOffice code="' . str_replace('<br>','',$inoffcode[0]) . '"/>
											</Contract>
										</ContractList>
										<DateFrom date="' . $check_in . '"/>
										<DateTo date="' . $check_out . '"/>
										<HotelInfo xsi:type="ProductHotel">
											<Code>' . $hotel_code . '</Code>
											<Destination type="SIMPLE" code="' . str_replace('<br>','',$destCodeVal[0]) . '"/>
										</HotelInfo>' . $room1ss . '
									</Service>
								</ServiceAddRQ>';
		//~ header("Content-type: text/xml"); echo $request;die();
		$type='ServiceAddRQ';
		$searchname = 'ServiceAddRQRS/ServiceAddRQ';
		Savelogs($searchname, $request);
		$HotelCancellation_PolicyRS = curl($request,$URL);
		$searchname = 'ServiceAddRQRS/ServiceAddRS';
		Savelogs($searchname, $HotelCancellation_PolicyRS);
		
		$xmlobj=new SimpleXMLElement($request);
		$xmlobj->asXML("hotel_xmldata/ServiceAddRQRS/ServiceAddRQ.xml");
		$xmlobj1=new SimpleXMLElement($HotelCancellation_PolicyRS);
		$xmlobj1->asXML("hotel_xmldata/ServiceAddRQRS/ServiceAddRS.xml");
		
		//~ header("Content-type: text/xml");echo $HotelCancellation_PolicyRS;
		$country_m = explode(",",$request_data_v1->city);
		$m_country = '';
		if(isset($country_m[1])){
			$m_country = trim($country_m[1]);
		}
		
		$xmllog['request'] = $request;
		$xmllog['response'] = $HotelCancellation_PolicyRS;
		$xmllog['api_id'] = $credintials_data['active_api'];
		$xmllog['xml_title'] = $type;
		$xmllog['xml_url'] = $URL;
		
		$xml_log_id = $CI->Hotel_Model->store_logs($xmllog['request'],$xmllog['response'],$xmllog['api_id']);
		if (!empty($HotelCancellation_PolicyRS)) {					
				$dom1 = new DOMDocument();
				$dom1->loadXML($HotelCancellation_PolicyRS);
				$Messageval = '';
				$Message = $dom1->getElementsByTagName("Message");
				foreach ($Message as $dddd) {
						$Messageval = $Message->item(0)->nodeValue;
				}

				$Commentval = '';
				if ($Messageval == '') {
						$dom = new DOMDocument();
						$dom->loadXML($HotelCancellation_PolicyRS);
						$purToken = $dom->getElementsByTagName("Purchase");
						$purTokenVal = $purToken->item(0)->getAttribute("purchaseToken");
						$service = $dom->getElementsByTagName("Service");
						$serviceval = $service->item(0)->getAttribute("SPUI");

						$Contract = $dom->getElementsByTagName("Contract");
						foreach ($Contract as $contractval) {
								$CommentList = $contractval->getElementsByTagName("CommentList");
								foreach ($CommentList as $commentval) {
										$Comment = $commentval->getElementsByTagName("Comment");
										$Commentval = $Comment->item(0)->nodeValue;
								}
						}

						$Supplier = $dom->getElementsByTagName("Supplier");
						$vatNumber = $Supplier->item(0)->getAttribute("vatNumber");
						$cancel = $dom->getElementsByTagName("CancellationPolicy");
						foreach ($cancel as $canval) {
								$cancelprice = $canval->getElementsByTagName("Amount");
								$canceldisplayValc = $cancelprice->item(0)->nodeValue;
								$data['cancel_amt'] = $canceldisplayValc;
								$dateFromc = $canval->getElementsByTagName("DateTimeFrom");
								$dateFromValc = $dateFromc->item(0)->getAttribute("date");
								$time = $dateFromc->item(0)->getAttribute("time");
								$data['datefrom'] = $dateFromValc;
								$data['time'] = $time;
								$dateToc = $canval->getElementsByTagName("DateTimeTo");
								$dateToValc = $dateToc->item(0)->getAttribute("date");
								$data['dateto'] = $dateToValc;
								$CommentListval = '';
								$data['comment'] = $CommentListval;
								$curr = $dom->getElementsByTagName("Currency");
								$data['currencyCode'] = $currencyCode = $curr->item(0)->getAttribute("code");
								$dateToc = $canval->getElementsByTagName("DateTimeTo");
								$dateToValc = $dateToc->item(0)->getAttribute("date");
								$data['dateto'] = $dateToValc;
						}
				 		$cancel_policy = '';
						$AvailableRoom = $dom->getElementsByTagName("AvailableRoom");
						$rt = 0;
						$rt = 0;
						$cust_id = array();
						$age = array();
						$RoomCount_val_n1 = array();
						$Room_val_n1 = array();
						$rby_rom_cnt = '';
						$rby_rom = '';
						$cancel_date_v1 = array();
						$cancel_amt_v1 =array();
						$buffer_cancel_policy = array();
						
						$SupplementList = $dom->getElementsByTagName("SupplementList");
						//~ echo "DiscountList<pre>";print_r($DiscountList);
						
						foreach($SupplementList as $SupplementList_val) {
							$supl_iter = 0;
							$Price = $SupplementList_val->getElementsByTagName("Price");
							foreach ($Price as $Price_val) {
								$Amount = $SupplementList_val->getElementsByTagName("Amount");
								$AmountVal = $Amount->item($supl_iter)->nodeValue;
								if($AmountVal  > 0) {
									$Suppliment_Amount[] = $Amount->item($supl_iter)->nodeValue;
								}
								$supl_iter++;
							}
						}
						
						//~ echo "Suppliment_Amount<pre>";print_r($Suppliment_Amount);
						
						$AdditionalCostList = $dom->getElementsByTagName("AdditionalCostList");
						foreach($AdditionalCostList as $AdditionalCostList_val) {
							$AdditionalCost = $AdditionalCostList_val->getElementsByTagName("AdditionalCost");
							foreach ($AdditionalCost as $AdditionalCost_val) {
								$Price = $AdditionalCost_val->getElementsByTagName("Price");
								foreach($Price as $Price_val) {
									$Amount = $Price_val->getElementsByTagName("Amount");
									$AmountVal = $Amount->item(0)->nodeValue;
									$AdditionalCost_Amount[] = $AmountVal;
								}
							}
						}
						//~ echo "AdditionalCost_Amount<pre>";print_r($AdditionalCost_Amount);
						
						$DiscountList = $dom->getElementsByTagName("DiscountList");
						//~ echo "DiscountList<pre>";print_r($DiscountList);
						
						foreach($DiscountList as $DiscountList_val) {
							$disc_iter = 0;
							$Price = $DiscountList_val->getElementsByTagName("Price");
							foreach ($Price as $Price_val) {
								$Amount = $DiscountList_val->getElementsByTagName("Amount");
								$AmountVal = $Amount->item($disc_iter)->nodeValue;
								if($AmountVal  > 0) {
									$Discount_Amount[] = $Amount->item($disc_iter)->nodeValue;
								}
								$disc_iter++;
							}
						}
						//~ echo "Discount_Amount: <pre>";print_r($Discount_Amount);
						$total_room_key = $AvailableRoom->length;
						foreach ($AvailableRoom as $AvailableRoomval) {
								$HotelRoom = $AvailableRoomval->getElementsByTagName("HotelRoom");
								
								if($total_room_key == 1) {
									if(!empty($Discount_Amount) && $Discount_Amount != '') {
										$discount_val = array_sum($Discount_Amount);
									}
									else {
										$discount_val = 0;
									}
								}
								else {
									if(!empty($Discount_Amount) && $Discount_Amount != '') {
										if($Discount_Amount[$rt] != '') {
											$discount_val = $Discount_Amount[$rt];
										}
										else {
											$discount_val = 0;
										}
									}
									else {
										$discount_val = 0;
									}
								}
								
								if(!empty($Suppliment_Amount) && $Suppliment_Amount != '') {
									$Suppliment_val = array_sum($Suppliment_Amount);
								}
								else {
									$Suppliment_val = 0;
								}
								
								foreach($HotelRoom as $HotelRoom_val) {
									
									$Price = $AvailableRoomval->getElementsByTagName("Price");
									
									foreach($Price as $Price_val) {
										$Amount = $Price_val->getElementsByTagName("Amount");
										$RoomAmount = $Amount->item(0)->nodeValue;
										
										/* Markup Calculation */
										
										$org_amt = $TotalPrice_v = $TotalPrice_API = (($RoomAmount + $Suppliment_val) - $discount_val);  
						
										//~ echo "API Price: <pre>";print_r($org_amt);

										$admin_markup = $b2b_markup = $b2b2_markup = $b2b_agent_markup = $b2b2b_agent_markup = 0;
										$admin_base_price = $b2b_base_price = $b2b2b_base_price = $b2b_agent_base_price = $b2b2b_agent_base_price = 0;
										$total_price_with_markup = $TotalPrice_v;
										if($admin_markup_details[0]->markup_value_type == "Percentage") {
											$admin_markup = $TotalPrice_v* ($admin_markup_details[0]->markup_value / 100);
											$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
										}else{
											$admin_markup = $admin_markup_details[0]->markup_fixed;
											$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
										}
										
										if($b2b_markup_details[0]->markup_value_type == "Percentage") {
											$b2b_markup = $admin_base_price* ($b2b_markup_details[0]->markup_value / 100);
											$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
										} else {
											$b2b_markup = $b2b_markup_details[0]->markup_value;
											$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
										}
										
										if($CI->session->userdata('user_type') == 4){
											if($sub_agent_markup_details[0]->markup_value_type == "Percentage") {
											$b2b_agent_markup = $b2b_base_price* ($sub_agent_markup_details[0]->markup_value / 100);
											$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
												} else {
											$b2b_agent_markup = $sub_agent_markup_details[0]->markup_value;
											$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
										   }
										  
										  if($b2b2b_markup_details[0]->markup_value_type == "Percentage") {
											$b2b2b_agent_markup = $b2b_agent_base_price * ($b2b2b_markup_details[0]->markup_value / 100);
											$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
											} else {
											$b2b2b_agent_markup = $b2b2b_markup_details[0]->markup_value;
											$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
										  }
										}
										$service_charge = 0;
										$gst_charge = 0;
										$tax_charge = 0;
				   
										if($CI->session->userdata('user_country_id') != 0){
											$country_name = $CI->general_model->get_country_name($CI->session->userdata('user_country_id'))->row();
											$tax_details = $CI->general_model->get_tax_details($country_name->country_name, $CI->session->userdata('user_state_id'))->row();
											
											if(count($tax_details) > 0){
											 $price_with_markup_tax = $CI->general_model->tax_calculation($tax_details, $total_price_with_markup);
											 if(count($price_with_markup_tax) > 0){
												 $service_charge = $price_with_markup_tax['sc_tax'];
												 $gst_charge = $price_with_markup_tax['gst_tax'];
												 $tax_charge = $price_with_markup_tax['state_tax'];
												  $total_price_with_markup_tax = $price_with_markup_tax['amount'];
												}
											}else{
											 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
											}
										}else{
											 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
										}     
										
										$admin_markup_v1[] = $admin_markup;
										$b2b_markup_v1[] = $b2b_markup;
										$b2b_agent_markup_v1[] = $b2b_agent_markup;
										$b2b2b_agent_markup_v1[] = $b2b2b_agent_markup;
										$admin_base_price_v1[] = $admin_base_price; 
										$b2b_base_price_v1[] = $b2b_base_price; 
										$b2b_agent_base_price_v1[] = $b2b_agent_base_price; 
										$b2b2b_agent_base_price_v1[] = $b2b2b_agent_base_price; 
										
										$total_fare[] = $total_price_with_markup_tax;
										
										/* End of Total Price Markup Calculation */
										
										break;
									}
								}
								//~ echo "Total fare: <pre>";print_r($total_fare);
								
								$RoomType = $AvailableRoomval->getElementsByTagName("RoomType");
								$rtname = $RoomType->item(0)->nodeValue;
								$rby_rom = $RoomType->item(0)->nodeValue;
								$HotelOccupancy = $AvailableRoomval->getElementsByTagName("HotelOccupancy");
								foreach ($HotelOccupancy as $Hote_O) {
										$Occupancy = $Hote_O->getElementsByTagName("Occupancy");
										foreach ($Occupancy as $occ) {
												$GuestList = $occ->getElementsByTagName("GuestList");
												foreach ($GuestList as $gl) {
														$Customer = $gl->getElementsByTagName("Customer");
														foreach ($Customer as $cus) {
																$customer_type[] = $cus->getAttribute("type");
																$CustomerId = $cus->getElementsByTagName("CustomerId");
																foreach ($CustomerId as $cid) {
																		$cust_id[] = $cid->nodeValue;
																}
																$Age = $cus->getElementsByTagName("Age");
																foreach ($Age as $a) {
																		$age[] = $a->nodeValue;
																}
														}
												}
										}
								}
								
								$RoomCount = $AvailableRoomval->getElementsByTagName("RoomCount");
								$RoomCount_val = $RoomCount->item(0)->nodeValue;
				
								$rby_rom_cnt = $RoomCount->item(0)->nodeValue;
								$from1=array();$time1=array();$amt=array();
				
								$CancellationPolicy = $AvailableRoomval->getElementsByTagName("CancellationPolicy");
								foreach ($CancellationPolicy as $CancellationPolicyval) {
									$k = 0;
									$Price = $CancellationPolicyval->getElementsByTagName("Price");
									foreach ($Price as $Pricessds) {
											$RoomCount_val_n1[] = $rby_rom_cnt;
											$Room_val_n1[] = $rby_rom;
											$DateTimeFrom = $Pricessds->getElementsByTagName("DateTimeFrom");
											$from1[] = $frmd = $DateTimeFrom->item(0)->getAttribute("date");
											$time1[] = $frmt = $DateTimeFrom->item(0)->getAttribute("time");
											$Amount = $CancellationPolicyval->getElementsByTagName("Amount");
											$amt[] = $camt = $Amount->item($k)->nodeValue;
											
											$DateTimeTo = $Pricessds->getElementsByTagName("DateTimeTo");
											$tod = $DateTimeTo->item(0)->getAttribute("date");
											if ($DateTimeTo->item(0)->hasAttribute("time")) {
												$tot = $DateTimeTo->item(0)->getAttribute("time");
												$to_hour = substr($frmt, 0, 2);
												$to_min = substr($frmt, 2, 2);
											}else{
												$to_hour = '00';
												$to_min = '00';
											}
											

											$frm_year = substr($frmd, 0, 4);
											$frm_month = substr($frmd, 4, 2);
											$frm_date = substr($frmd, 6, 2);
											$frm_hour = substr($frmt, 0, 2);
											$frm_min = substr($frmt, 2, 2);

											$to_year = substr($tod, 0, 4);
											$to_month = substr($tod, 4, 2);
											$to_date = substr($tod, 6, 2);

											$can_pol[] = array(
												'from' => $frm_year.'-'.$frm_month.'-'.$frm_date.' '.$frm_hour.':'.$frm_min,
												'to' => $to_year.'-'.$to_month.'-'.$to_date.' '.$to_hour.':'.$to_min,
												'amount' => $camt
											);


											$k++;
									}
								}
								for ($ii = 0; $ii < count($amt); $ii++) {
										$cancel_policy .= 'In the event of cancellation or changes for ' . $RoomCount_val . 'x' . $rtname . ' ';
										$cancel_amt_eur_org = number_format($amt[$ii], 2, '.', '');
										$aMarkup = 0;
										$myMarkup = 0;
										$amountv = $TotalPrice_v = $amt[$ii];
										
										$admin_markup = $b2b_markup = $b2b2_markup = $b2b_agent_markup = $b2b2b_agent_markup = 0;
										$admin_base_price = $b2b_base_price = $b2b2b_base_price = $b2b_agent_base_price = $b2b2b_agent_base_price = 0;
										$total_price_with_markup = $TotalPrice_v;
										if($admin_markup_details[0]->markup_value_type == "Percentage") {
											$admin_markup = $TotalPrice_v* ($admin_markup_details[0]->markup_value / 100);
											$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
										}else{
											$admin_markup = $admin_markup_details[0]->markup_fixed;
											$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
										}
										
										if($b2b_markup_details[0]->markup_value_type == "Percentage") {
											$b2b_markup = $admin_base_price* ($b2b_markup_details[0]->markup_value / 100);
											$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
										} else {
											$b2b_markup = $b2b_markup_details[0]->markup_value;
											$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
										}
										
										if($CI->session->userdata('user_type') == 4){
											if($sub_agent_markup_details[0]->markup_value_type == "Percentage") {
											$b2b_agent_markup = $b2b_base_price* ($sub_agent_markup_details[0]->markup_value / 100);
											$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
												} else {
											$b2b_agent_markup = $sub_agent_markup_details[0]->markup_value;
											$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
										   }
										  
										  if($b2b2b_markup_details[0]->markup_value_type == "Percentage") {
											$b2b2b_agent_markup = $b2b_agent_base_price * ($b2b2b_markup_details[0]->markup_value / 100);
											$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
											} else {
											$b2b2b_agent_markup = $b2b2b_markup_details[0]->markup_value;
											$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
										  }
									   }
									   
									   //~ echo $total_price_with_markup;
									   
										$service_charge = 0;
										$gst_charge = 0;
										$tax_charge = 0;
									   
										if($CI->session->userdata('user_country_id') != 0){
											$country_name = $CI->general_model->get_country_name($CI->session->userdata('user_country_id'))->row();
											$tax_details = $CI->general_model->get_tax_details($country_name->country_name, $CI->session->userdata('user_state_id'))->row();
											
											if(count($tax_details) > 0){
											 $price_with_markup_tax = $CI->general_model->tax_calculation($tax_details, $total_price_with_markup);
											 if(count($price_with_markup_tax) > 0){
												 $service_charge = $price_with_markup_tax['sc_tax'];
												 $gst_charge = $price_with_markup_tax['gst_tax'];
												 $tax_charge = $price_with_markup_tax['state_tax'];
												  $total_price_with_markup_tax = $price_with_markup_tax['amount'];
												}
											}else{
											 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
											}
										}else{
											 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
										}
										$amountv = $total_price_with_markup_tax;
										
										$year = substr($from1[$ii], 0, 4);
										$mon = substr($from1[$ii], 4, 2);
										$date = substr($from1[$ii], 6, 2);
										$hour = substr($time1[$ii], 0, 2);
										$min = substr($time1[$ii], 2, 2);
										$can_date = $year.'-'.$mon.'-'.$date;
										$can_check_date = date('d-m-Y', strtotime($can_date. "+1 days"));
										$can_check_datetime = date_create($can_check_date);
										$today = date('d-m-Y');
										$today_datetime = date_create($today);
										$diff=date_diff($can_check_datetime,$today_datetime);
										$interval = $diff->format("%a");
										if($interval == 0 || $buffer_days == 0) {
											$can_date = date('d-m-Y', strtotime($can_date));
										}
										elseif($interval >= $buffer_days) {
											$can_date = date('d-m-Y', strtotime($can_date. "-".$buffer_days." days"));
										}
										elseif($interval < $buffer_days) {
											$buffer_days = $interval;
											$can_date = date('d-m-Y', strtotime($can_date. "-".$buffer_days." days"));
										}
										else {
											$can_date = date('d-m-Y', strtotime($can_date));
										}
										$cancel_policy .= ' after ' . $hour . ':' . $min . ' on ' . $can_date;
										$cancel_policy .= ' the following charges will be applied:  ' .$currency.' '.number_format(($amountv*$exchange_rate),2);
										$newdate3 = date('Y-m-d', strtotime($can_date));
										$cancel_date_v1[] = $newdate3;
										$cancel_amt_v1[] = $amountv;
					
										$cancel_policy .= '|';
										
										$buffer_cancel_policy[$rt][$ii][$RoomCount_val.'x'.$rtname]['from'] = $can_date.' '.$hour.':'.$min;
										$buffer_cancel_policy[$rt][$ii][$RoomCount_val.'x'.$rtname]['amount'] = $amountv;
								}
								
							 $rt++;
						}
						//~ echo "Markup Cancellation Price Array: <pre>";print_r($cancel_amt_v1);
						
						//$CI->hotel_model->insert_booking_attrib('23', 'Hotelbeds', $purTokenVal, $serviceval, $canceldisplayValc, $dateFromValc, $dateToValc);
						if ($Commentval == '') {
								$Commentval = "No remarks from this hotel|";
						} else {
								$Commentval = $Commentval . '|';
						}
			
				
				 
						if (isset($Commentval)) {
								$Commentval = mysql_real_escape_string($Commentval);
						} else {
								$Commentval = '';
						}
						$cancel_policy = mysql_real_escape_string($cancel_policy);
						
						$price = $dom->getElementsByTagName("TotalPrice");
						$org_amt = $TotalPrice_v = $TotalPrice_API = $price->item(0)->nodeValue;  
						
						//~ echo "API Price: <pre>";print_r($org_amt);
//~ 
						$currency = $dom->getElementsByTagName("Currency");

						$currency_code = $currency->item(0)->getAttribute("code");
					//~ $admin_markup = $b2b_markup = $b2b2_markup = $b2b_agent_markup = $b2b2b_agent_markup = 0;
					//~ $admin_base_price = $b2b_base_price = $b2b2b_base_price = $b2b_agent_base_price = $b2b2b_agent_base_price = 0;
					//~ $total_price_with_markup = $TotalPrice_v;
					//~ if($admin_markup_details[0]->markup_value_type == "Percentage") {
						//~ $admin_markup = $TotalPrice_v* ($admin_markup_details[0]->markup_value / 100);
						//~ $admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
					//~ }else{
						//~ $admin_markup = $admin_markup_details[0]->markup_fixed;
						//~ $admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
					//~ }
					//~ 
					//~ if($b2b_markup_details[0]->markup_value_type == "Percentage") {
						//~ $b2b_markup = $admin_base_price* ($b2b_markup_details[0]->markup_value / 100);
						//~ $b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
					//~ } else {
						//~ $b2b_markup = $b2b_markup_details[0]->markup_value;
						//~ $b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
					//~ }
					//~ 
					//~ if($CI->session->userdata('user_type') == 4){
						//~ if($sub_agent_markup_details[0]->markup_value_type == "Percentage") {
						//~ $b2b_agent_markup = $b2b_base_price* ($sub_agent_markup_details[0]->markup_value / 100);
						//~ $b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
							//~ } else {
						//~ $b2b_agent_markup = $sub_agent_markup_details[0]->markup_value;
						//~ $b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
					   //~ }
					  //~ 
					  //~ if($b2b2b_markup_details[0]->markup_value_type == "Percentage") {
						//~ $b2b2b_agent_markup = $b2b_agent_base_price * ($b2b2b_markup_details[0]->markup_value / 100);
						//~ $b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
						//~ } else {
						//~ $b2b2b_agent_markup = $b2b2b_markup_details[0]->markup_value;
						//~ $b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
					  //~ }
				   //~ }
				   //~ 
				   //~ 
				    //~ $service_charge = 0;
				    //~ $gst_charge = 0;
				    //~ $tax_charge = 0;
				   //~ 
				    //~ if($CI->session->userdata('user_country_id') != 0){
						//~ $country_name = $CI->general_model->get_country_name($CI->session->userdata('user_country_id'))->row();
						//~ $tax_details = $CI->general_model->get_tax_details($country_name->country_name, $CI->session->userdata('user_state_id'))->row();
						//~ 
						//~ if(count($tax_details) > 0){
						 //~ $price_with_markup_tax = $CI->general_model->tax_calculation($tax_details, $total_price_with_markup);
						 //~ if(count($price_with_markup_tax) > 0){
							 //~ $service_charge = $price_with_markup_tax['sc_tax'];
							 //~ $gst_charge = $price_with_markup_tax['gst_tax'];
							 //~ $tax_charge = $price_with_markup_tax['state_tax'];
							  //~ $total_price_with_markup_tax = $price_with_markup_tax['amount'];
							//~ }
						//~ }else{
						 //~ $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
						//~ }
					//~ }else{
						 //~ $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
					//~ } 
					     
					//~ echo "Markup API Price: <pre>";print_r($total_price_with_markup_tax);
					//~ echo "total_fare: <pre>";print_r($total_fare);
					//~ echo "Discount_Amount: <pre>";print_r($Discount_Amount);
					$can_pol = json_encode($can_pol);
					$guest_list = array(
						'type' => $customer_type,
						'id' => $cust_id,
						'age' => $age
					);
					
				
				     $Netrate = $org_amt;
				     $Admin_Markup = array_sum($admin_markup_v1);
					 $My_Markup = array_sum($b2b_markup_v1);
					 $My_Agent_Markup = array_sum($b2b_agent_markup_v1);
					 $My_B2B2B_Markup = array_sum($b2b2b_agent_markup_v1);
					 $Admin_BasePrice = array_sum($admin_base_price_v1);
					 $agent_baseprice = array_sum($b2b_base_price_v1);
					 $sub_agent_baseprice = array_sum($b2b_agent_base_price_v1);
					 $b2b2b_baseprice = array_sum($b2b2b_agent_base_price_v1);
					 
					//~ $TotalPrice = array_sum($total_fare) - array_sum($Discount_Amount);
					$TotalPrice = array_sum($total_fare);
					//~ echo "total_fare: <pre>";print_r($total_fare);
					//~ echo "TotalPrice: <pre>";print_r($TotalPrice);
					//~ echo "Admin_BasePrice: <pre>";print_r($Admin_BasePrice);exit;
					 
					$guest_list = json_encode($guest_list);

					if($flag == "HB_Cancellation") {
						return $cancel_policy;
					}
					else {
						$CI->db->query("UPDATE cart_hotel SET 
							room_data='$cancel_policy',
							policy_description = '$cancel_policy',
							buffer_cancellation_policy = '".json_encode($buffer_cancel_policy)."',
							cancellation_commisionable_nettrate = '".json_encode($buffer_cancel_policy)."',
							amendment_policy = '$Commentval',
							comment='$Commentval',
							purTokenVal='$purTokenVal',
							serviceval='$serviceval',
							cancellation_till_date='$newdate3', 
							total_cost='$TotalPrice',
							net_rate = '$Netrate' ,
							admin_markup = '$Admin_Markup' ,
							my_markup = '$My_Markup' ,
							my_agent_Markup = '$My_Agent_Markup' ,
							my_b2b2b_markup = '$My_B2B2B_Markup' ,
						   
							admin_baseprice = '$Admin_BasePrice' ,
							agent_baseprice = '$agent_baseprice' ,
							sub_agent_baseprice = '$sub_agent_baseprice' ,
							b2b2b_baseprice = '$b2b2b_baseprice' ,
							service_charge = '$service_charge' ,
							gst_charge = '$gst_charge' ,
							tax_charge = '$tax_charge' ,
								API_CURR = '$currency_code',
								org_amt = '$Netrate',
								cancel_policy = '$can_pol',
								xml_logid = '$xml_log_id',
								guest_list = '$guest_list'
								 WHERE id='$booking_cart_id'");
								
							$CI->db->query("UPDATE cart_global SET total_cost='$TotalPrice' WHERE cart_id='$booking_global_id'");
					}
					//~ exit;
						return true;
				} else {
					return false;	
				}
		}else{
			return false;
		}
}

function PurchaseConfirmRQ($cid, $checkout_form,$parent_pnr_no, $cart_result_id){  

		

	if (!file_exists('hotel_xmldata/PurchaseConfirmRQRS')) {
		mkdir('hotel_xmldata/PurchaseConfirmRQRS', 0777, true);
	}
		//$checkout_form = json_decode(base64_decode($checkout_form), 1);
		//$cart_result_id = json_decode(base64_decode($cart_result_id));
				
		$total_payable = $cart_result_id->total_cost;
		$cids = $cart_result_id->cart_id;
		
		
		$request_data_v1 = json_decode($cart_result_id->request_info);
		
		$CI =& get_instance();
		$CI->load->model('Hotel_Model');
		
		$agen_ref_no_v1 = 'SLH' . time('his');
		$room_catagery_id = $cart_result_id->room_id;
		$room_adult_count = $cart_result_id->adult;
		$room_child_count = $cart_result_id->child;
		$hotel_code = $cart_result_id->hotel_code;
		$purTokenVal = $cart_result_id->purTokenVal;
		$serviceval = $cart_result_id->serviceval;
		$bookroom = '';
		$m = 1;
		$nameval = '';
		$fname1 = 'test';
		$pax_info = '';
		
		for ($i = 0; $i < $request_data_v1->rooms; $i++) {
			$array_vals[] = $request_data_v1->adult[$i] . "," . $request_data_v1->child[$i];
		} 
				$hotelbed_rooms = '';
				$check_array = array();
		  //echo '<pre/>';  print_r($array_vals); exit;
				for ($k = 0; $k < count($array_vals); $k++) {
						if (isset($request_data_v1->childAges[$k])) {
								$child_age = implode(",",$request_data_v1->childAges[$k]);
						} else {
								$child_age = '';
						}
						$key = array_search($array_vals[$k], $check_array);
						if ($key) {
								unset($check_array[$key]);
								$split_key = explode("||", $key);
								$key_count = $split_key[0] + 1;
								$check_array[$key_count . "||" . $split_key[1] . "," . $child_age] = $array_vals[$k];
						} else {
								$check_array['1||' . $child_age . '||' . $k] = $array_vals[$k];
						}
				}
				
				$addservice_guests = json_decode($cart_result_id->guest_list);
				//echo '<pre>';print_r($checkout_form); exit;
				
				$Customer='';
				$a=0;$c=0;
				
				$user_id_array['AD'] = array();
				$user_id_array['CH'] = array();
				for($sq=0; $sq < count($addservice_guests->type); $sq++){
					if($addservice_guests->type[$sq]=='AD'){
						$user_id_array['AD'][] = $addservice_guests->id[$sq];
					}
					if($addservice_guests->type[$sq]=='CH'){
						$user_id_array['CH'][] = $addservice_guests->id[$sq];
					}
				}
			
				$pcnt1=0; $pcnt2=0;
				if(isset($checkout_form[0])){ 
					for($pcnt = 0; $pcnt< count($checkout_form); $pcnt++){
						$checkout_form[$pcnt] = json_decode(json_encode($checkout_form[$pcnt]), true);
						if ($checkout_form[$pcnt]['passenger_type'] == 'ADULT') {	
												
							$Customer .= '<Customer type="AD">
											<CustomerId>'.$user_id_array['AD'][$pcnt1].'</CustomerId>
											<Age>30</Age>
											<Name>'.$checkout_form[$pcnt]['first_name'].'</Name>
											<LastName>'.$checkout_form[$pcnt]['last_name'].'</LastName>
										</Customer>';
							$a++; $pcnt1++;	
						}
						else {
							$Customer .= '<Customer type="CH">
											<CustomerId>'.$user_id_array['CH'][$pcnt2].'</CustomerId>
											<Age>'.$checkout_form[$pcnt]['age'].'</Age>
											<Name>'.$checkout_form[$pcnt]['first_name'].'</Name>
											<LastName>'.$checkout_form[$pcnt]['last_name'].'</LastName>
										</Customer>';
							$c++; $pcnt2++;
						}
					}
				} else { 
					if ($checkout_form['passenger_type'] == 'ADULT') {
						for($at=0; $at < count($addservice_guests->type); $at++){
							if($addservice_guests->type[$at] == 'AD'){
								$Customer .= '<Customer type="AD">
											<CustomerId>'.$addservice_guests->id[$at].'</CustomerId>
											<Age>'.$addservice_guests->age[$at].'</Age>
											<Name>'.$checkout_form['first_name'].'</Name>
											<LastName>'.$checkout_form['last_name'].'</LastName>
										</Customer>';
							$a++;
							}
							
						}							

					}	else {
						for($at=0; $at < count($addservice_guests->type); $at++){
							if($addservice_guests->type[$at] == 'CH'){
								$Customer .= '<Customer type="CH">
											<CustomerId>'.$addservice_guests->id[$at].'</CustomerId>
											<Age>'.$checkout_form['age'].'</Age>
											<Name>'.$checkout_form['first_name'].'</Name>
											<LastName>'.$checkout_form['last_name'].'</LastName>
										</Customer>';
							$c++;
							}
							
						}	
							
						}
				}
				  

				$m = 1;
				$aca = 0;
				$acaa = 0;
				
				$gb = array();
				if(isset($request_data_v1->childAges)){
						for ($ru = 0; $ru < count($request_data_v1->childAges); $ru++) {
								$cc = $request_data_v1->childAges[$ru];
								for ($rus = 0; $rus < count($cc); $rus++) {
										$gb[] = $cc[$rus];
								}
						}
				}
				
				//echo '<pre/>'; print_r($checkout_form); exit;
				$pass_info = '';
				if(isset($checkout_form[0])){
					for ($ru = 0; $ru < count($checkout_form); $ru++) {
							$pass_info .= $checkout_form[$ru]['salutation'] . ' ' . $checkout_form[$ru]['first_name'] . ' ' . $checkout_form[$ru]['last_name'] . '<br>';
					}
				} else {
					$pass_info .= $checkout_form['salutation'] . ' ' . $checkout_form['first_name'] . ' ' . $checkout_form['last_name'] . '<br>';
				}
/*
				if (isset($checkout_form['CHD_GivenName'][0])) {
						for ($ru = 0; $ru < count($checkout_form['CHD_GivenName']); $ru++) {
								$pass_info .= 'Master : ' . $checkout_form['CHD_GivenName'][$ru] . ' ' . $checkout_form['CHD_Surname'][$ru] . ' / Age - ' . $gb[$ru] . '<br>';
						}
				}
*/
//exit; 
//echo "<pre/>"; print_r($cart_result_id); exit;
				if (isset($checkout_form['booking_info']['rema'][0]) && $checkout_form['booking_info']['rema'] != '') {
						$remarks_k1 = implode(",", $checkout_form['booking_info']['rema']);
						$remarks_k2 = $checkout_form['booking_info']['remar'];
						if ($remarks_k2 != '') {
								$remarks_k1 = implode(",", $checkout_form['booking_info']['rema']);
								$remarks_k2 = $checkout_form['booking_info']['remar'];
								$rem_g1 = $remarks_k1 . ', ' . $remarks_k2;
								$CommentList = '<CommentList>
																	<Comment type = "SERVICE">' . $rem_g1 . '</Comment>
																	<Comment type = "INCOMING">' . $rem_g1 . '</Comment>
																</CommentList>';
						} else {
								$CommentList = '<CommentList>
																	<Comment type = "SERVICE">' . $remarks_k1 . '</Comment>
																		 <Comment type = "INCOMING">' . $remarks_k1 . '</Comment>
																</CommentList>';
						}
				} elseif (isset($checkout_form['booking_info']['remar']) && $checkout_form['booking_info']['remar'] != '') {
						$remarks_k2 = $_SESSION['booking_info']['remar'];
						$CommentList = '<CommentList>
											 				<Comment type = "SERVICE">' . $remarks_k2 . '</Comment>
															<Comment type = "INCOMING">' . $remarks_k2 . '</Comment>
													</CommentList>';
				} else {
						$CommentList = '<CommentList>
											 				<Comment type = "SERVICE">Test</Comment>
															<Comment type = "INCOMING">Test</Comment>
													</CommentList>';
				}
				$ad_name = $checkout_form[0]['first_name'];
				$ad_name1 = $checkout_form[0]['last_name'];
				$credencails_data=set_crediential($cart_result_id->api_id);
				
				 //$credintials = json_decode($credintials); //get api credintials
				 $URL = $credencails_data['post_url'];
				 $api_id = $credencails_data['active_api'];
				$PurchaseConfirmRQ = '<PurchaseConfirmRQ xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseConfirmRQ.xsd" echoToken="DummyEchoToken">
																<Language>ENG</Language>
																	<Credentials>
								<User>' . $credencails_data['client_id'] . '</User>
								<Password>' . $credencails_data['password'] . '</Password>
							</Credentials>
																<ConfirmationData purchaseToken="' . $purTokenVal . '">
																	<Holder type="AD">
																		<Name>' . $ad_name . '</Name>
																		<LastName>' . $ad_name1 . '</LastName>
																	</Holder>
																	<AgencyReference>' . $parent_pnr_no . '</AgencyReference>
																	<ConfirmationServiceDataList>
																		<ServiceData xsi:type="ConfirmationServiceDataHotel" SPUI="' . $serviceval . '">
																			<CustomerList>' . $Customer . '
																			</CustomerList>
																					' . $CommentList . '
																		</ServiceData>
																	</ConfirmationServiceDataList>
																</ConfirmationData>
															</PurchaseConfirmRQ>';
	
	$type='PurchaseConfirmRQ';
	//~ header("Content-type: text/xml");
	//~ echo $PurchaseConfirmRQ;die; 
	
	
	$PurchaseConfirmRS = curl($PurchaseConfirmRQ,$URL);
	
	
		/*header("Content-type: text/xml");
	echo $PurchaseConfirmRQ;die; */
	
	//~ $PurchaseConfirmRS = '<PurchaseConfirmRS xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseConfirmRS.xsd" echoToken="DummyEchoToken"><AuditData><ProcessTime>3730</ProcessTime><Timestamp>2016-06-18 12:38:55.287</Timestamp><RequestHost>14.141.47.106</RequestHost><ServerName>FORM|1XtBIFhwxCUv5kAnT1/25Qcd|NG|05|1|1|0</ServerName><ServerId>FO</ServerId><SchemaRelease>2005/06</SchemaRelease><HydraCoreRelease>3.10.0.20160605</HydraCoreRelease><HydraEnumerationsRelease>3#Yu01tvBRQ+S+Uq6zilJ07Q</HydraEnumerationsRelease><MerlinRelease>0</MerlinRelease></AuditData><Purchase purchaseToken="O1964481861" timeToExpiration="1799962"><Reference><FileNumber>131454</FileNumber><IncomingOffice code="270"></IncomingOffice></Reference><Status>BOOKING</Status><Agency><Code>180305</Code><Branch>1</Branch></Agency><Language>ENG</Language><CreationDate date="20160618"/><CreationUser>WORTRASHOPNG180305</CreationUser><Holder type="AD"><Age>0</Age><Name>TEST</Name><LastName>TEST</LastName></Holder><AgencyReference>NTSLRECQTA8D</AgencyReference><ServiceList><Service xsi:type="ServiceHotel" SPUI="270#H#1"><Reference><FileNumber>131454-H1</FileNumber><IncomingOffice code="270"></IncomingOffice></Reference><Status>CONFIRMED</Status><ContractList><Contract><Name>BAR-ALL</Name><IncomingOffice code="270"></IncomingOffice><CommentList><Comment type="CONTRACT">Extra beds on demand YES (with additional debit notes). Wi-fi YES (without additional debit notes)   . . All the guest is required to provide the identification card or passport when they check in the hotel.</Comment></CommentList></Contract></ContractList><Supplier name="HOTELBEDS" vatNumber="B22257273"/><CommentList><Comment type="SERVICE">Test</Comment><Comment type="INCOMING">Test</Comment></CommentList><DateFrom date="20160901"/><DateTo date="20160902"/><Currency code="USD">US Dollar</Currency><TotalAmount>25.540</TotalAmount><SupplementList><Price unitCount="1" paxCount="1"><Amount>0.370</Amount><DateTimeFrom date="20160901"/><DateTimeTo date="20160902"/><Description>CITY TAX </Description></Price></SupplementList><AdditionalCostList><AdditionalCost type="AG_COMMISSION"><Price><Amount>0.000</Amount></Price></AdditionalCost><AdditionalCost type="COMMISSION_VAT"><Price><Amount>0.000</Amount></Price></AdditionalCost><AdditionalCost type="COMMISSION_PCT"><Price><Amount>0.000</Amount></Price></AdditionalCost></AdditionalCostList><ModificationPolicyList><ModificationPolicy>Cancellation</ModificationPolicy><ModificationPolicy>Confirmation</ModificationPolicy><ModificationPolicy>Modification</ModificationPolicy></ModificationPolicyList><HotelInfo xsi:type="ProductHotel"><Code>128562</Code><Name>Citadel Suites</Name><Category type="SIMPLE" code="3EST">3 STARS</Category><Destination type="SIMPLE" code="BLR"><Name>Bangalore</Name><ZoneList><Zone type="SIMPLE" code="1">Bangalore</Zone></ZoneList></Destination></HotelInfo><AvailableRoom><HotelOccupancy><RoomCount>1</RoomCount><Occupancy><AdultCount>1</AdultCount><ChildCount>0</ChildCount><GuestList><Customer type="AD"><CustomerId>1</CustomerId><Age>30</Age><Name>Test</Name><LastName>Test</LastName></Customer></GuestList></Occupancy></HotelOccupancy><HotelRoom SHRUI="0ZJAXw93X6jyYWPcJSBr3Q==" availCount="1" status="CONFIRMED"><Board type="SIMPLE" code="BB-E10">BED AND BREAKFAST</Board><RoomType type="SIMPLE" code="DBT-E10" characteristic="LX">Double or Twin LUXURY</RoomType><Price><Amount>25.170</Amount></Price><CancellationPolicy><Price><Amount>25.540</Amount><DateTimeFrom date="20160828" time="2359"/><DateTimeTo date="20160901"/></Price></CancellationPolicy><HotelRoomExtraInfo><ExtendedData><Name>INFO_ROOM_AGENCY_BOOKING_STATUS</Name><Value>O</Value></ExtendedData><ExtendedData><Name>INFO_ROOM_INCOMING_BOOKING_STATUS</Name><Value>O</Value></ExtendedData></HotelRoomExtraInfo></HotelRoom></AvailableRoom></Service></ServiceList><Currency code="USD"></Currency><PaymentData><PaymentType code="P"></PaymentType><Description>*55* Hotelbeds, S.L.U, Bank: CITIBANK(Citigroup Centre, Canary Wharf, London, E14 5LB. United Kingdom) Account:GB13 CITI 185008 13242420,  SWIFT:CITIGB2L,  7 days prior to clients arrival (except group bookings with fixed days in advance at the time of the confirmation) . Please indicate our reference number when making payment. Thank you for your cooperation.</Description></PaymentData><TotalPrice>25.540</TotalPrice></Purchase></PurchaseConfirmRS>';
	

	$xmlobj=new SimpleXMLElement($PurchaseConfirmRQ);
	$xmlobj->asXML("hotel_xmldata/PurchaseConfirmRQRS/PurchaseConfirmRQ.xml");
	$xmlobj1=new SimpleXMLElement($PurchaseConfirmRS);
	$xmlobj1->asXML("hotel_xmldata/PurchaseConfirmRQRS/PurchaseConfirmRS.xml");

	//~ $PurchaseConfirmRS_ = ' <PurchaseConfirmRS xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseConfirmRS.xsd" echoToken="DummyEchoToken"><AuditData><ProcessTime>4381</ProcessTime><Timestamp>2013-07-22 23:03:48.385</Timestamp><RequestHost>123.201.135.77:77</RequestHost><ServerName>FORM</ServerName><ServerId>FO</ServerId><SchemaRelease>2005/06</SchemaRelease><HydraCoreRelease>2.0.201305161604</HydraCoreRelease><HydraEnumerationsRelease>1.0.201305161604</HydraEnumerationsRelease><MerlinRelease>N/A</MerlinRelease></AuditData><Purchase purchaseToken="FO110020078" timeToExpiration="1799982"><Reference><FileNumber>1808804</FileNumber><IncomingOffice code="164"></IncomingOffice></Reference><Status>BOOKING</Status><Agency><Code>81413</Code><Branch>1</Branch></Agency><Language>ENG</Language><CreationDate date="20130722"/><CreationUser>SKYTRAVEL1IQ81413</CreationUser><Holder type="AD"><Age>0</Age><Name>TEST</Name><LastName>TEST</LastName></Holder><AgencyReference>BS1374527029</AgencyReference><ServiceList><Service xsi:type="ServiceHotel" SPUI="164#H#1"><Reference><FileNumber>1808804-H1</FileNumber><IncomingOffice code="164"></IncomingOffice></Reference><Status>CONFIRMED</Status><ContractList><Contract><Name>NRF BAR</Name><IncomingOffice code="164"></IncomingOffice></Contract></ContractList><Supplier name="HOTELBEDS UK LIMITED" vatNumber="234328478"/><CommentList><Comment type="SERVICE">
  //~ //Test</Comment><Comment type="INCOMING">Test</Comment></CommentList><DateFrom date="20130927"/><DateTo date="20130928"/><Currency code="GBP">United Kingdom Pound</Currency><TotalAmount>31.110</TotalAmount><AdditionalCostList><AdditionalCost type="AG_COMMISSION"><Price><Amount>0.000</Amount></Price></AdditionalCost><AdditionalCost type="COMMISSION_VAT"><Price><Amount>0.000</Amount></Price></AdditionalCost></AdditionalCostList><ModificationPolicyList><ModificationPolicy>Cancellation</ModificationPolicy><ModificationPolicy>Confirmation</ModificationPolicy><ModificationPolicy>Modification</ModificationPolicy></ModificationPolicyList><HotelInfo xsi:type="ProductHotel"><Code>39920</Code><Name>Comfort Hotel Heathrow</Name><Category type="SIMPLE" code="3EST">3 STARS</Category><Destination type="SIMPLE" code="LON"><Name>London</Name><ZoneList><Zone type="SIMPLE" code="10">Heathrow Airport</Zone></ZoneList></Destination></HotelInfo><AvailableRoom><HotelOccupancy><RoomCount>1</RoomCount><Occupancy><AdultCount>2</AdultCount><ChildCount>0</ChildCount><GuestList><Customer type="AD"><CustomerId>2</CustomerId><Age>30</Age></Customer><Customer type="AD"><CustomerId>1</CustomerId><Age>30</Age><Name>test</Name><LastName>test</LastName></Customer></GuestList></Occupancy></HotelOccupancy><HotelRoom SHRUI="lY3wwRZKRLN7y2+HhkAemg==" availCount="1" status="CONFIRMED"><Board type="SIMPLE" code="RO-U02">ROOM ONLY</Board><RoomType type="SIMPLE" code="DBT-U02" characteristic="ST">Double or Twin STANDARD</RoomType><Price><Amount>31.110</Amount></Price><CancellationPolicy><Price><Amount>31.110</Amount><DateTimeFrom date="20130721" time="2359"/><DateTimeTo date="20130927"/></Price></CancellationPolicy><HotelRoomExtraInfo><ExtendedData><Name>INFO_ROOM_AGENCY_BOOKING_STATUS</Name><Value>O</Value></ExtendedData><ExtendedData><Name>INFO_ROOM_INCOMING_BOOKING_STATUS</Name><Value>O</Value></ExtendedData></HotelRoomExtraInfo></HotelRoom></AvailableRoom></Service></ServiceList><Currency code="GBP"></Currency><PaymentData><PaymentType code="C"></PaymentType><Description>The total amount for this pro-forma invoice should be made in full to HOTELBEDS UK LIMITED, Bank: NatWest Bank PLC(City of London Office, PO Box 12258, 1 Princess Street, London EC2R 8PA.) Account:39256464, Sort code: 60 00 01, IBAN: GB60NWBK60000139256464,  SWIFT:NWBKGB2L,  7 days prior to clients arrival (except group bookings with fixed days in advance at the time of the confirmation) . Please indicate our reference number when making payment. Thank you for your cooperation.</Description></PaymentData><TotalPrice>31.110</TotalPrice></Purchase></PurchaseConfirmRS>';

	$PurchaseConfirmRQ_RS = array(
		'PurchaseConfirmRQ' => $PurchaseConfirmRQ,       
		'PurchaseConfirmRS' => $PurchaseConfirmRS,   
	);
	//~ header("Content-type: text/xml");echo $PurchaseConfirmRS;die;   
	
	return array('request'=>$PurchaseConfirmRQ, 'response'=>$PurchaseConfirmRS, 'xml_title'=>$type, 'api_id'=>$api_id, 'xml_url'=>$URL);
}

	function PurchaseCancelRQ($api_id, $api_cancel_pnr) {
		$credintials_data = set_crediential($api_id);

		$URL = $credintials_data['post_url'];
		$filenumber = explode("-", $api_cancel_pnr);
		
		$PurchaseCancelRQ = '<?xml version="1.0" encoding="UTF-8"?>
								<PurchaseCancelRQ xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseCancelRQ.xsd" type="C">                                                                      
									<Language>ENG</Language>				
									<Credentials>
										<User>' . $credintials_data['client_id'] . '</User>
										<Password>' . $credintials_data['password'] . '</Password>
									</Credentials>
									<PurchaseReference>
										<FileNumber>' . $filenumber[1] . '</FileNumber>
										<IncomingOffice code="' . $filenumber[0] . '"/>
									</PurchaseReference>
								</PurchaseCancelRQ>';
		$type='PurchaseCancelRQ'; 
		$HotelValuedAvailRS= curl($PurchaseCancelRQ, $URL);
		$HotelCancelRQ_RS = array(
							'HotelBookingCancelRQ' => $PurchaseCancelRQ,
							'HotelBookingCancelRS' => $HotelValuedAvailRS
						);
		return $HotelCancelRQ_RS;
	}
	
	
	
function Savelogs($searchname, $result1){
	$path = $_SERVER['DOCUMENT_ROOT'];
	$strFileName = $searchname.".xml";
	$my_file = $path."/".PAGE_TITLE."/hotel_xmldata/".$strFileName;
	//echo $my_file;exit;
	$handle = fopen($my_file, 'w');
	//print_r($handle);exit;
 	@fwrite($handle, $result1);
	@fclose($handle);
	
}

?>
