<?php 
 
 
function TransferAvailRQ($city,$api_id)
{   
	$credentials = TF_getApiCredentials($api_id);
	$searchrequest = '';
	
	$client_id = '40882';
	$client_email = 'XML.PROVAB@CHINAGAP.COM';
	$password = 'CHINA40882';
	
	  $searchrequest = '<?xml version="1.0" encoding="UTF-8" ?>
						<Request>
						<Source>
						<RequestorID Client = "'.$client_id.'" EMailAddress = "'.$client_email.'" Password = "'.$password.'" />
						<RequestorPreferences Country="AU" Currency="AUD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
						</RequestorPreferences>
						</Source>
						<RequestDetails>
						<SearchItemInformationRequest ItemType = "transfer">
						<ItemDestination DestinationCode = "'.$city.'" DestinationType = "city"/>
						<LocationCode/>
						<ItemName/>
						<ItemCode/>
						<SearchTransferInformation/>
						</SearchItemInformationRequest>
						</RequestDetails>
					   </Request>'; 
		 
	$xml_type = '';
	$xml_title = 'GetTransferRequest';
	$TransferSearchRS = processRequest_gta($searchrequest,$xml_type,$xml_title, $api_id);
	
	formate_result_transfers($TransferSearchRS);
}


function formate_result_transfers($TransferSearchRS){
	            $dom = new DOMDocument();
				$dom->preserveWhiteSpace = false;
				$dom->loadXML($TransferSearchRS);
				$ItemDetails 	= $dom->getElementsByTagName("ItemDetails");
				
				foreach($ItemDetails as $item){
					$ItemDetail = $item->getElementsByTagName('ItemDetail');
					foreach($ItemDetail as $itemd){
						$City = $itemd->getElementsByTagName('City');
						$city_code = $City->item(0)->getAttribute('Code');
						$city_name = $City->item(0)->nodeValue;
						
						$Item = $itemd->getElementsByTagName('Item');
						$item_code = $Item->item(0)->getAttribute('Code');
						$item_description = $Item->item(0)->nodeValue; 
						
						
						$TransferInformation = $itemd->getElementsByTagName('TransferInformation');
						$Description = $itemd->getElementsByTagName('Description');
						$driver_description = $Description->item(0)->nodeValue; 
						
						$MeetingPoint = $itemd->getElementsByTagName('MeetingPoint');
						$meeting_point = $MeetingPoint->item(0)->nodeValue; 
						$transfer_condition = "";
						$TransferConditions = $itemd->getElementsByTagName('TransferConditions');
						foreach($TransferConditions as $transcond){
							$TransferCondition = $transcond->getElementsByTagName('TransferCondition');
							foreach($TransferCondition as $conditions){
								$transfer_condition .= $conditions->nodeValue."|||"; 
							}
							
						}
						
						$TransferType = $itemd->getElementsByTagName('TransferType');
						$transfer_type_code = $TransferType->item(0)->getAttribute('Code');
						$transfer_type = $TransferType->item(0)->nodeValue;
						
						$Copyright = $itemd->getElementsByTagName('Copyright');
						$copy_rights = $Copyright->item(0)->nodeValue; 
						$flash_links ='';
						$Links = $itemd->getElementsByTagName('Links');
						if($Links->length > 0){
							$FlashLinks = $itemd->getElementsByTagName('FlashLinks');
							if($FlashLinks->length > 0){
								$FlashLink = $itemd->getElementsByTagName('FlashLink');
								foreach($FlashLink as $links){
									$flash_links .= $links->nodeValue."|||";
								}
							}
						}
						
					  $data = array("city_code" => $city_code,
					                 "city_name" =>$city_name,
					                 "item_code" => $item_code,
					                 "item_description" => $item_description,
					                 "driver_description" => $driver_description,
					                 "meeting_point" =>$meeting_point,
					                 "transfer_condition" => $transfer_condition,
					                 "transfer_type_code" => $transfer_type_code,
					                 "transfer_type" => $transfer_type,
					                 "copy_rights" => $copy_rights,
					                 "flash_links" => $flash_links);
					      $CI =& get_instance();
					     $CI->load->model('general_model');
					     $CI->general_model->insert_transfer_static($data);
					   
					 }
				}
			
 }
 
 
function TransferAvailStationRQ($city,$country_code,$api_id)
{   
	$credentials = TF_getApiCredentials($api_id);
	$searchrequest = '';
	
	
	$client_id = '40882';
	$client_email = 'XML.PROVAB@CHINAGAP.COM';
	$password = 'CHINA40882';
	
	  $searchrequest = '<?xml version="1.0" encoding="UTF-8" ?>
						<Request>
						<Source>
						<RequestorID Client = "'.$client_id.'" EMailAddress = "'.$client_email.'" Password = "'.$password.'" />
						<RequestorPreferences Country="AU" Currency="AUD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
						</RequestorPreferences>
						</Source>
						<RequestDetails>
						<SearchHotelRequest>

							<ItemDestination DestinationCode = "EDI"  DestinationType = "city"/>

						</SearchHotelRequest>
						</RequestDetails>
					   </Request>';
		 
	$xml_type = '';
	$xml_title = 'GetTransferRequest';
	$TransferSearchRS = processRequest_gta($searchrequest,$xml_type,$xml_title, $api_id);
	
	formate_stationresult($TransferSearchRS, $country_code);
}

function formate_stationresult($TransferSearchRS, $country_code){
	
	            $dom = new DOMDocument();
				$dom->preserveWhiteSpace = false;
				$dom->loadXML($TransferSearchRS);
				$StationDetails 	= $dom->getElementsByTagName("StationDetails");
				$temp = array();
				foreach($StationDetails as $station_details){
					$StationDetail = $station_details->getElementsByTagName('StationDetail');
					foreach($StationDetail as $stations){
						$City = $stations->getElementsByTagName("City");
						$temp['city_code'] = $City->item(0)->getAttribute('Code');
						$temp['city_name'] = $City->item(0)->nodeValue; 
						
						$Station = $stations->getElementsByTagName("Station");
						$temp['station_code'] = $Station->item(0)->getAttribute('Code');
						$temp['station_name'] = $Station->item(0)->nodeValue; 
						$temp['country_code'] = $country_code;
						 $CI =& get_instance();
					     $CI->load->model('general_model');
					     $CI->general_model->insert_transfer_station_static($temp);
						
					}
				}
	
}

 function formate_result($TransferSearchRS){
	            $dom = new DOMDocument();
				$dom->preserveWhiteSpace = false;
				$dom->loadXML($TransferSearchRS);
				$ItemDetails 	= $dom->getElementsByTagName("ItemDetails");
				
				foreach($ItemDetails as $item){
					$ItemDetail = $item->getElementsByTagName('ItemDetail');
					foreach($ItemDetail as $itemd){
						$City = $itemd->getElementsByTagName('City');
						$city_code = $City->item(0)->getAttribute('Code');
						$city_name = $City->item(0)->nodeValue;
						
						$Item = $itemd->getElementsByTagName('Item');
						$item_code = $Item->item(0)->getAttribute('Code');
						$item_description = $Item->item(0)->nodeValue; 
						
						
						$TransferInformation = $itemd->getElementsByTagName('TransferInformation');
						$Description = $itemd->getElementsByTagName('Description');
						$driver_description = $Description->item(0)->nodeValue; 
						
						$MeetingPoint = $itemd->getElementsByTagName('MeetingPoint');
						$meeting_point = $MeetingPoint->item(0)->nodeValue; 
						$transfer_condition = "";
						$TransferConditions = $itemd->getElementsByTagName('TransferConditions');
						foreach($TransferConditions as $transcond){
							$TransferCondition = $transcond->getElementsByTagName('TransferCondition');
							foreach($TransferCondition as $conditions){
								$transfer_condition .= $conditions->nodeValue."|||"; 
							}
							
						}
						
						$TransferType = $itemd->getElementsByTagName('TransferType');
						$transfer_type_code = $TransferType->item(0)->getAttribute('Code');
						$transfer_type = $TransferType->item(0)->nodeValue;
						
						$Copyright = $itemd->getElementsByTagName('Copyright');
						$copy_rights = $Copyright->item(0)->nodeValue; 
						$flash_links ='';
						$Links = $itemd->getElementsByTagName('Links');
						if($Links->length > 0){
							$FlashLinks = $itemd->getElementsByTagName('FlashLinks');
							if($FlashLinks->length > 0){
								$FlashLink = $itemd->getElementsByTagName('FlashLink');
								foreach($FlashLink as $links){
									$flash_links .= $links->nodeValue."|||";
								}
							}
						}
						
					  $data = array("city_code" => $city_code,
					                 "city_name" =>$city_name,
					                 "item_code" => $item_code,
					                 "item_description" => $item_description,
					                 "driver_description" => $driver_description,
					                 "meeting_point" =>$meeting_point,
					                 "transfer_condition" => $transfer_condition,
					                 "transfer_type_code" => $transfer_type_code,
					                 "transfer_type" => $transfer_type,
					                 "copy_rights" => $copy_rights,
					                 "flash_links" => $flash_links);
					      $CI =& get_instance();
					     $CI->load->model('general_model');
					     $CI->general_model->insert_transfer_static($data);
					   
					 }
				}
			
 }
 
 function processRequest_gta($requestData,$xmltype='',$xml_title, $api_id) {
	 $credentials = TF_getApiCredentials($api_id);
	 $url = 'https://rs.gta-travel.com/wbsapi/RequestListenerServlet';
	$cs = curl_init();
    curl_setopt($cs, CURLOPT_URL, $url);
    curl_setopt($cs, CURLOPT_HEADER, false);
    curl_setopt($cs, CURLOPT_RETURNTRANSFER, true);
  	curl_setopt($cs, CURLOPT_POST, 1);
	curl_setopt($cs, CURLOPT_POSTFIELDS, $requestData);
    curl_setopt($cs, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($cs, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($cs, CURLOPT_TIMEOUT, 180);
    $httpHeader2 = array('Content-type: text/xml');
    curl_setopt($cs, CURLOPT_HTTPHEADER, $httpHeader2);   
    $login_response = curl_exec($cs);    
    curl_close($cs);
    $CI =& get_instance();    
    $CI->load->helper('file'); 
    echo   $login_response; exit;
    return $login_response;
		
		 
}

function TF_getApiCredentials($api_id) {
    $CI =& get_instance();
    $CI->load->model('General_Model');
	$api ='GTA';
    return $CI->General_Model->get_api_credentials_by_id($api_id)->row();
}

?>
