<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------|
| Author: Provab Technosoft Pvt Ltd.                                       |
|--------------------------------------------------------------------------|
| Developer: Ciranjeeb Debnath                                             |
| Started Date: February 2016                                              |
| Project : Nastaran                                                       |
|--------------------------------------------------------------------------|
*/



function generate_randomNumber($digits=10)
{
  $random = '';

  for($i=1;$i<=$digits;$i++)
  {
    $number_rand = rand(1,9);
    $random.=$number_rand;
  }
  return $random;
}


  

  function filter_markUp($user_type,$user_id,$module,$country_iso,$api_id,$markup_db_object)
  {

    $CI = &get_instance();
    $CI->load->model('general_model');
    $filtered_index = array();
    
    if($markup_db_object['country_code']=='0')
    {
        if($markup_db_object['data']->num_rows()==1)
        {
          $each_markup_data = $markup_db_object['data']->result_array()[0];
          $market_id = $each_markup_data['markets'];
          $market_data = get_markup_marketData_bymarketId($market_id);
          if($market_data->num_rows()==1)
          {
            $market_data=$market_data->result_array()[0];
            if(strpos($market_data['countries'], ','))
            {
              $each_market_countries = explode(',', $market_data['countries']);

              foreach($each_market_countries as $market_countries)
              {
                  if($country_iso==$market_countries || 'ZZ'==$market_countries)
                  {
                      $filtered_index = $markup_db_object['data']->result_array()[0];
                  }
              }
            } else {
                if($country_iso==$market_data['countries'] || $country_iso=='ZZ')
                {
                    $filtered_index = $markup_db_object['data']->result_array()[0];
                }
            }
          } 

        } else if($markup_db_object['data']->num_rows()>1)
        { 
            for($i=(sizeof($markup_db_object['data']->result_array()) - 1);$i>=0;$i--)
            {
                if($markup_db_object['country_code']=='0')
                {
                  $each_markup_data = $markup_db_object['data']->result_array()[$i];
                  $market_id = $each_markup_data['markets'];
                  $market_data = get_markup_marketData_bymarketId($market_id);
               
                  if($market_data->num_rows()==1)
                  {
                    $market_data=$market_data->result_array()[0];

                    if(strpos($market_data['countries'], ','))
                    {
                      $each_market_countries = explode(',', $market_data['countries']);

                      foreach($each_market_countries as $market_countries)
                      {
                          if($country_iso==$market_countries || 'ZZ'==$market_countries)
                          {
                              $filtered_index = $markup_db_object['data']->result_array()[$i];
                          }
                      }
                    } else {
                        if($country_iso==$market_data['countries'] || $country_iso=='ZZ')
                        {
                            $filtered_index = $markup_db_object['data']->result_array()[$i];
                        }
                    }
                  } 
                } 
            }
          } 


          if(sizeof($filtered_index)==0)
          { 
            $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,'1',$api_id);
            if($markup_data->num_rows()>=1)
            {
               $sizeof = sizeof($markup_data->result_array());
               $filtered_index = $markup_data->result_array()[$sizeof-1];
            } else {
                 $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$markup_db_object['country_code_cache'],'');
                 
                 if($markup_data->num_rows()>=1)
                 {
                   $sizeof = sizeof($markup_data->result_array());
                   $filtered_index = $markup_data->result_array()[$sizeof-1];
                   
                 } else {
                     $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,'1','');
                     
                     if($markup_data->num_rows()>=1)
                     {
                       $sizeof = sizeof($markup_data->result_array());
                       $filtered_index = $markup_data->result_array()[$sizeof-1];
                       
                     } 

                 }
             }
            
          }

    } else {
        $sizeof = sizeof($markup_db_object['data']->result_array());
        $filtered_index = $markup_db_object['data']->result_array()[$sizeof-1];
    }
    return $filtered_index;
  }

  function get_markup_marketData_bymarketId($market_id )
  {
    $CI = &get_instance();
    $CI->load->model('general_model');
    return $CI->general_model->get_GM_marketData_marketId($market_id);
  }
  function getMarkupAdmin($user_type,$user_id,$current_product_module,$user_from_country_iso,$api_id){
    $markup_data = get_markUp_data($user_type,$user_id,$current_product_module,$user_from_country_iso,$api_id);
    print_r($markup_data);exit;

  }
  function getMarkup($user_type,$user_id,$current_product_module,$user_from_country_iso,$api_id)
  {

          if(intval($user_type)==2)
          {
              $markup_data = get_markUp_data('1','0',$current_product_module,$user_from_country_iso,$api_id);
// /echo "string";exit;
               if($markup_data['result']=='-1')
              {
                     $success_a = 'false';
                     $data_top_a = $markup_data['data'];
              } else {
                     
                    if(($markup_data['result'])=='0')
                    {
                        $markup=false;

                    } else if(($markup_data['result'])=='1'){
                        
                         $markup_data = filter_markUp('1','0',$current_product_module,$user_from_country_iso,$api_id,$markup_data);
                        
                         if(sizeof($markup_data)==0)
                         {
                            $markup=false;
                         } else {
                            $markup=$markup_data;
                         }
                    }
                    $b2b_markup_data = $markup;

                    $success_a = 'true';
                    $data_top_a = $markup;

              }
          } else {
            $b2b_markup_data = 'false';
            $success_a = 'true';
            $data_top_a = $b2b_markup_data;
          }
          
             $markup_data = get_markUp_data($user_type,$user_id,$current_product_module,$user_from_country_iso,$api_id);


            if($markup_data['result']=='-1')
            {
                   $success_b = 'false';
                   $data_top_b = $markup_data['data'];

            } else {
                   
                  if(($markup_data['result'])=='0')
                  {
                      $markup=false;
                  } else if(($markup_data['result'])=='1'){
                      
                       $markup_data = filter_markUp($user_type,$user_id,$current_product_module,$user_from_country_iso,$api_id,$markup_data);
                      
                       if(sizeof($markup_data)==0)
                       {
                          $markup=false;
                       } else {
                          $markup=$markup_data;
                       }
                  }

                  $success_b = 'true';
                  $data_top_b = $markup;

            }

            return array('case_a_success' => $success_a,'case_b_success' => $success_b , 'case_a_markup' =>$data_top_a,'case_b_markup' => $data_top_b);
      }


  function total_markup_calculate($markup,$admin_markup,$actual_price)
  {
    
   // echo "<pre/>";print_r($admin_markup);exit;
    $response = array();
    if(!$admin_markup || $admin_markup=='false')
    {
      $markup_price='0.00';
      $markedup_price = $actual_price;
      $actual_price = $markedup_price;

      $response['case_a_markedup_price'] =  $markedup_price;
      $response['case_a_markup_price'] =    $markup_price;
    } else {

        if($admin_markup['markup_fixed']=='0' || $admin_markup['markup_fixed']=='' || is_null($admin_markup['markup_fixed']) || $admin_markup['markup_fixed']=='NULL')
        {
          $markup_percentage = floatval($admin_markup['markup_value']); //percentage
          $markup_price = (
                (
                floatval($markup_percentage / 100) 
                *  
                floatval($actual_price) 
                )
              );
          
        } else {
          $markup_price = floatval($admin_markup['markup_fixed']);
        }
  
      $markedup_price = (floatval($markup_price) + floatval($actual_price));
      $markedup_price = number_format($markedup_price, 2, '.', '');
      $markup_price = number_format($markup_price, 2, '.', '');

      $actual_price = $markedup_price;

      $response['case_a_markedup_price'] =  $markedup_price;
      $response['case_a_markup_price'] =    $markup_price;

    }

    if(!$markup || $markup=='false')
    {
      $markup_price=floatval('0.00') + $markup_price; 
      $markedup_price = $actual_price;
    } else {

        if($markup['markup_fixed']=='0' || $markup['markup_fixed']=='' || is_null($markup['markup_fixed']) || $markup['markup_fixed']=='NULL')
        {
          $markup_percentage = floatval($markup['markup_value']); //percentage
          $markup_price = (
                (
                floatval($markup_percentage / 100) 
                *  
                floatval($actual_price) 
                )
              );
          
        } else {
          $markup_price = floatval($markup['markup_fixed']);
        }
  
      $markedup_price = (floatval($markup_price) + floatval($actual_price));
    }

      $markedup_price = number_format($markedup_price, 2, '.', '');
      $markup_price = number_format($markup_price, 2, '.', '');

   return array('case_a_markedup_price' => $response['case_a_markedup_price'] ,'case_a_markup_price' => $response['case_a_markup_price'],'markedup_price' => $markedup_price,'markup_price' => $markup_price);
  }



  function markup_calculate($markup,$actual_price,$user_type="",$user_id="")
  {//error_reporting(0);
    //echo "<pre/>";print_r($markup);exit;
    if($user_type == 1 or $user_type= 4 or $user_type==""){
      //echo "string";exit;
      if(!$markup || $markup=='false')
      {
        $markup_price='0.00';
        $markedup_price = $actual_price;
      } else {

          if($markup['markup_fixed']=='0' || $markup['markup_fixed']=='' || is_null($markup['markup_fixed']) || $markup['markup_fixed']=='NULL')
          {
            $markup_percentage = floatval($markup['markup_value']); //percentage
            $markup_price = (
                  (
                  floatval($markup_percentage / 100) 
                  *  
                  floatval($actual_price) 
                  )
                );
            //echo $markup_price;exit;
            
          } else {
            $markup_price = floatval($markup['markup_fixed']);
          }
    
        $markedup_price = (floatval($markup_price) + floatval($actual_price));
      }
    }else{
        $markup_price='0.00';
        $markedup_price = $actual_price;
    }
      $my_markup = 0;
      $markedup_price = number_format($markedup_price, 2, '.', '');
      $markup_price = number_format($markup_price, 2, '.', '');
      return array('markedup_price' => $markedup_price,'markup_price' => $markup_price,'my_markup' => $my_markup);
  }

  function get_markUp_data($user_type,$user_id,$module,$country_iso,$api_id)
  {
    // usertype => 1=b2c,2=b2b , $uers_id = 0 if b2c user or else b2b if > 0,
    // $module => 2=Hotel
    // $api_id => 0=CRS

    $CI = &get_instance();
    $CI->load->model('general_model');
    $api_id_cache = $api_id;
    $country_data = $CI->general_model->get_countryId_bycountryISO($country_iso);

    if($country_data->num_rows()==1)
    { 
      $country_data = $country_data->result_array()[0];
      $country_code = $country_data['country_id']; 
      $country_code_cache = $country_data['country_id'];


      $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$country_code,$api_id);

      if($markup_data->num_rows() >= 1)
      { 
        return array('result' => '1','data' => $markup_data,'country_code' => $country_code,'api_id' => $api_id,'country_code_cache' => $country_code_cache);
      } else if($markup_data->num_rows()==0)
      { 
         $country_code = '0'; // if any market place row is inserted with the rest of same arguments
         $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$country_code,$api_id);
 //echo "<pre/>";print_r($markup_data->result());exit;
          if($markup_data->num_rows() >= 1)
          { 
            return array('result' => '1','data' => $markup_data,'country_code' => $country_code,'api_id' => $api_id,'country_code_cache' => $country_code_cache);
          } else if($markup_data->num_rows()==0)
          { 
             $country_code = '1'; // if ALL countries are inserted
             $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$country_code,$api_id);
             if($markup_data->num_rows() >= 1)
             { 
              return array('result' => '1','data' => $markup_data,'country_code' => $country_code,'api_id' => $api_id,'country_code_cache' => $country_code_cache);
             } else if($markup_data->num_rows()==0)
             { 
               $api_id = ''; // now lets check all the above conditions with the no api being used 
               $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$country_code_cache,$api_id);

               if($markup_data->num_rows() >= 1)
               { 
                 return array('result' => '1','data' => $markup_data,'country_code' => $country_code_cache,'api_id' => $api_id,'country_code_cache' => $country_code_cache);
               } else if($markup_data->num_rows()==0)
               { 
                 $api_id = ''; // now lets check all the above conditions with the no api being used 
                 $country_code = '0'; // if any market place row is inserted with the rest of same arguments
                 $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$country_code,$api_id);
                 if($markup_data->num_rows() >= 1)
                 { 
                   return array('result' => '1','data' => $markup_data,'country_code' => $country_code,'api_id' => $api_id,'country_code_cache' => $country_code_cache);
                 } else if($markup_data->num_rows()==0)
                 { 
                   $api_id = ''; // now lets check all the above conditions with the no api being used 
                   $country_code = '1'; // if ALL countries are inserted
                   $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$country_code,$api_id);
                   
                   if($markup_data->num_rows() >= 1)
                   {  
                     return array('result' => '1','data' => $markup_data,'country_code' => $country_code,'api_id' => $api_id,'country_code_cache' => $country_code_cache);
                   } else if($markup_data->num_rows()==0)
                   { 
                     // so if till here no mark up rows found lets assume that no markups set for the above arguments
                      return array('result' => '0','data' => false);
                   }

                 }
               }
             }
          }

      }
    } else {
      return array('result' => '-1','data' => 'Check the nationality iso code');
    }
  }

  function get_markUp2($user_type,$user_id,$module,$country_iso,$api_id)
  {
    // usertype => 1=b2c,2=b2b , $uers_id = 0 if b2c user or else b2b if > 0,
    // $module => 2=Hotel
    // $api_id => 0=CRS

    $CI = &get_instance();
    $CI->load->model('general_model');
    $api_id_cache = $api_id;
    $country_data = $CI->general_model->get_countryId_bycountryISO($country_iso);


    if($country_data->num_rows()==1)
    { 
      $country_data = $country_data->result_array()[0];
      $country_code = $country_data['country_id']; 
      $country_code_cache = $country_data['country_id'];


      $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$country_code,$api_id);

      if($markup_data->num_rows() >= 1)
      { 
        return array('result' => true,'data' => $markup_data);
      } else if($markup_data->num_rows()==0)
      { 
         $country_code = '1';
         $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$country_code,$api_id);
         if($markup_data->num_rows() >= 1)
         {
           return array('result' => true,'data' => $markup_data);
         } else if($markup_data->num_rows()==0)
         { 
             $api_id = '0';
             $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$country_code_cache,$api_id);
             if($markup_data->num_rows() > 1)
             { 
               return array('result' => true,'data' => $markup_data);
             } else if($markup_data->num_rows()==0)
             {  
                 $api_id = '0';
                 $country_code = '1';
                 $markup_data = $CI->general_model->get_Markup_data($user_type,$user_id,$module,$country_code,$api_id,'debug');

                 if($markup_data->num_rows() >= 1)
                 { 
                   return array('result' => true,'data' => $markup_data);
                 } else if($markup_data->num_rows()==0)
                 { 
                     
                     return array('result' => true,'data' => $markup_data);
                 }

             }
         }
      }
    } else {
      return array('result' => false,'data' => 'Check the nationality iso code');
    }
  }

  function calculate_crs_roomPrice($node_data,$GET,$hotel_details='',$hotelid,$currency_obj)
    { 
   
    $cidate = explode('/', $GET['data']['from_date']);
    $CIDate = $cidate[2].'-'. $cidate[1].'-'. $cidate[0];

    $codate = explode('/', $GET['data']['to_date']);
        $CODate = $codate[2].'-'. $codate[1].'-'. $codate[0];

        $datetime1 = new DateTime($CIDate);
        $datetime2 = new DateTime($CODate);

        //$oDiff = $datetime1->diff($datetime2); 
        //$stay_days = intval($oDiff->d); 
        $stay_days = $GET['data']['no_of_nights'];

        $s_max_adult = max($GET['data']['adult_config']);
        $s_max_child = max($GET['data']['child_config']);
        if($s_max_child){
            $s_max_child = $s_max_child;
        }else{
            $s_max_child = 0;
        }
        $checkin_date = date('Y-m-d',strtotime($CIDate));
        $checkout_date = date('Y-m-d',strtotime($CODate));

        $room_count = $GET['data']['room_count'];
        $city_id = $GET['data']['city_name'];
      
      $CI = &get_instance();
      $CI->load->model(('hotels_model'));
      //$CI->load->library(array('CurrencyConverter'));
      $room_type_data = $CI->hotels_model->get_roomType_data($node_data['hotel_room_type_id'],$node_data['hotel_details_id'])->result_array()[0];

      if($node_data['gst_tax']=='EXCLUSIVE' || $node_data['service_tax']=='EXCLUSIVE')
      {
        //$tax_details = $CI->Hotel_Model->get_tax_details();
        //echo "<pre/>";print_r($room_type_data);exit;
       /* if($tax_details->num_rows()>=1)
        {
          $tax_details = $tax_details->result_array()[0];
          
           if($node_data['gst_tax']=='EXCLUSIVE')
          {
            $gst_tax = $tax_details['gst'];
           
            if(!is_numeric($gst_tax))
            {
              $gst_tax='0';
            }
          }
           if($node_data['service_tax']=='EXCLUSIVE')
          {
            $service_tax = $tax_details['service_charge'];
            if(!is_numeric($service_tax))
            {
              $gst_tax='0';
            }
          }

        } else {
          //echo "string";exit;
          if($node_data['gst_tax']=='EXCLUSIVE')
          {
            $gst_tax = '0';
          }
           if($node_data['service_tax']=='EXCLUSIVE')
          {
            $service_tax = '0';
          }
        }*/
      } //error_reporting(E_ALL);
       // /echo "<pre/>";print_r($tax_details);exit;
      $child_group_a = $hotel_details['child_group_a'];
      $child_group_b = $hotel_details['child_group_b'];
      $child_group_c = $hotel_details['child_group_c'];
      $child_group_d = $hotel_details['child_group_d'];
      $child_group_e = $hotel_details['child_group_e'];
   
      if(
        ($hotel_details['child_group_a']=='' || $hotel_details['child_group_a']=='NULL' || is_null($hotel_details['child_group_a'])) &&
        ($hotel_details['child_group_b']=='' || $hotel_details['child_group_b']=='NULL' || is_null($hotel_details['child_group_b'])) &&
        ($hotel_details['child_group_c']=='' || $hotel_details['child_group_c']=='NULL' || is_null($hotel_details['child_group_c'])) &&
        ($hotel_details['child_group_d']=='' || $hotel_details['child_group_d']=='NULL' || is_null($hotel_details['child_group_d'])) &&
        ($hotel_details['child_group_e']=='' || $hotel_details['child_group_e']=='NULL' || is_null($hotel_details['child_group_e']))
        )
      {
          
        //$CI->load->library(array('CurrencyConverter'));
     
        $general_settings = $CI->hotels_model->get_general_settings($hotelid);
        //$general_settings = $CI->hotel_model->get_general_settings();

       //echo "<pre/>";print_r($general_settings);exit;
        if($general_settings->num_rows()>=1)
        {
          $general_settings = $general_settings->result_array()[0];
          $child_group_a = $general_settings['child_group_a'];
          $child_group_b = $general_settings['child_group_b'];
          $child_group_c = $general_settings['child_group_c'];
          $child_group_d = $general_settings['child_group_d'];
          $child_group_e = $general_settings['child_group_e'];
        } 
      }
      $child_age = array();
      $serial_ages = array();
      $age_groups_index = array();
      $count_age_no  = 0;
      

      $result =array();
      $j =1;
      for($i=0;$i<=intval($room_count);$i++)
      {
        if ($GET['data']['child_config']>0) {
           $age ='childage'.$j;
           $result = array_merge($result,$GET['data']['child_age']);
        }
         $j++;
      } 
      $room_type_max_adult = intval($room_type_data['adult']); 
      $room_type_max_child = intval($room_type_data['child']); 
      $room_type_max_pax = intval($room_type_data['max_pax']);
      $room_type_extra_bed = ($room_type_data['extra_bed']); 
      $room_type_extra_bed_count = intval($room_type_data['extra_bed_count']);

      foreach($result as $kk2 => $each_ages)
      {
       
          if($child_group_a!='')
          {
          
            $child_group_a_array = explode('-', $child_group_a); 
            
            if(intval($each_ages)>=intval($child_group_a_array[0]) && intval($each_ages)<=intval($child_group_a_array[1]))
            {
              $age_groups_index[$kk2] = 'room_child_price_a';
            }
          }

          if($child_group_b!='')
          {
            $child_group_b_array = explode('-', $child_group_b);

            if(intval($each_ages)>=intval($child_group_b_array[0]) && intval($each_ages)<=intval($child_group_b_array[1]))
            {
              $age_groups_index[$kk2] = 'room_child_price_b';
            
            }
          }

          if($child_group_c!='')
          {
            $child_group_c_array = explode('-', $child_group_c);

            if(intval($each_ages)>=intval($child_group_c_array[0]) && intval($each_ages)<=intval($child_group_c_array[1]))
            {
              $age_groups_index[$kk2] = 'room_child_price_c';
              
            }
          }

          if($child_group_d!='')
          {
            $child_group_d_array = explode('-', $child_group_d);

            if(intval($each_ages)>=intval($child_group_d_array[0]) && intval($each_ages)<=intval($child_group_d_array[1]))
            {
              $age_groups_index[$kk2] = 'room_child_price_d';
            }
          }

          if($child_group_e!='')
          {
            $child_group_e_array = explode('-', $child_group_e);

            if(intval($each_ages)>=intval($child_group_e_array[0]) && intval($each_ages)<=intval($child_group_e_array[1]))
            {
              $age_groups_index[$kk2] = 'room_child_price_e';
            }
          }

      }

      
      
      $response = array();
      $adult_price = $node_data['single_room_price']; 
      $adult_count = array_sum($GET['data']['adult_config']);
      $children_count = array_sum($GET['data']['child_config']); 
      
      $days = array();
      $checkin_unix_time = strtotime($CIDate);
      $weekdays = array('Sat','Sun');
      
      for($m=0;$m<=$stay_days;$m++)
      {
        if($m==0)
        {
          $days[] = date('D',$checkin_unix_time);
        } else {
          $days[] = date('D',strtotime('+'.$m.' day',$checkin_unix_time));
        }
      }
     
      $z=0;
      $child_price_array=array();
      for($i=0;$i<intval($GET['data']['room_count']);$i++)
      {
        // lets check here if extrabed is required for the particular room
        $room_children_count = intval($GET['data']['child_config'][$i]);
        $age_groups_i = array();
        $price_array = array();
        $price_array_m= array();
        

        if($room_type_extra_bed == 'Available')
        {  
             if($room_children_count > $room_type_max_child) // check if extra bed price is required
              { 
                    $extra_bed = true;
                    $child_extra_bed_available = $room_type_max_pax - $room_type_max_child;
                    $extra_bed_required = $room_children_count - $room_type_max_child;

                    if($room_children_count > $room_type_max_child)
                    { 
                       for($m=0;$m<$room_children_count;$m++)
                      {
                        $z++;
                        $age_groups_i[$m] = $age_groups_index[$z-1];
                        if(!isset($node_data[$age_groups_index[$z-1]]) || $node_data[$age_groups_index[$z-1]]=='0' || is_null($node_data[$age_groups_index[$z-1]]))
                        {
                          $price_array_m[$m] = $node_data['room_child_price_a'];
                        } else {
                          $price_array_m[$m] = $node_data[$age_groups_index[$z-1]];
                        }
                      }
                    } 
                   
                    for($r=0;$r<$room_type_max_child;$r++)
                      {
                        $price_array[$r] = $price_array_m[$r];
                      }
                      
                   
                    if($extra_bed_required<=$adult_extra_bed_available)
                    { 
                      // lets grab the extra bed price
                      $rate_key = 'child_extra_bed_price';
                      for($o=0;$o<$room_children_count - $room_type_max_child;$o++)
                      {
                        $ii = $room_type_max_child;
                        
                        $price_array[$ii] = $node_data[$rate_key];
                        $ii++;
                      }
                    } else { 
                      // this condition will never fall back still we are taking care of this condition and still charge extra adults with the number of extra beds 
                      $rate_key = 'child_extra_bed_price';
                      for($o=0;$o<$room_children_count - $room_type_max_child;$o++)
                      {
                        $ii = $room_type_max_child;
                        
                        $price_array[$ii] = $node_data[$rate_key];
                        $ii++;
                      }
                    } 
                  
              } else {

                 // here extra bed is not required, ie we have avaliable bed for the number of adults
                  $extra_bed = false;
                  for($m=0;$m<$room_children_count;$m++)
                  { 
                    $z++;
                    $age_groups_i[$m] = $age_groups_index[$z-1];
                    if(!isset($node_data[$age_groups_index[$z-1]]) || $node_data[$age_groups_index[$z-1]]=='0' || is_null($node_data[$age_groups_index[$z-1]]))
                    {
                      $price_array[$m] = $node_data['room_child_price_a'];
                    } else {
                      $price_array[$m] = $node_data[$age_groups_index[$z-1]];
                    }
                  }


              }
        }  else if($room_type_extra_bed == 'NotAvailable')
        { 
           for($m=0;$m<$room_children_count;$m++)
            { 
              $z++;
              $age_groups_i[$m] = $age_groups_index[$z-1]; //echo '<pre>'; print_r($age_groups_i[$i]);
              if(!isset($node_data[$age_groups_index[$z-1]]) || $node_data[$age_groups_index[$z-1]]=='0' || is_null($node_data[$age_groups_index[$z-1]]))
              { 
                $price_array[$m] = $node_data['room_child_price_a'];
              } else { 
                $price_array[$m] = $node_data[$age_groups_index[$z-1]]; 
              }
            }
           
        }
        
       $child_price_array[] =  array_sum($price_array) * $stay_days;
      }

       //exit();
     // echo '<pre>'; print_r($price_array); exit();
      $total_price_children =  ($children_count == 0) ? 0.00 : floatval(array_sum($child_price_array)); 
      $adults_week_price_array = array();
      $adults_week_price_array_multiplies_days = array();
      //echo 'sanjay'; print_r($total_price_children); exit();
      // lets grab the adult price data satisfying all conditions
       
      if($node_data['weekend_price']=='1' || $node_data['weekend_price']==1)
      { 
         for($i=0;$i<intval($GET['data']['room_count']);$i++)
          {
              $adults_week_days_price_array = array();

              for($j=0;$j<intval($stay_days);$j++)
              {
                
                $current_day = date('D',strtotime('+'.($j).' day',$checkin_unix_time));
                $weekend_day = false;
               
                foreach ($weekdays as $each_weekdays) 
                {
                  if($each_weekdays==$current_day)
                  {
                    $weekend_day=true;
                  }
                }
               
               if($room_type_extra_bed=='NotAvailable')
               { 
                   if($weekend_day)
                   {
                      $rate_key = filter_node_weekend_rateKey(intval($GET['data']['adult_config'][$i]),$node_data);
                      $adults_week_days_price_array[]=$node_data[$rate_key];
                   } else {
                      //$rate_key = filter_node_rateKey(intval($GET->adult[$i]),$node_data);
                    $rate_key = filter_node_ratekey_rooms($GET); 
                      $adults_week_days_price_array[]=$node_data[$rate_key];
                   }
               } else if($room_type_extra_bed=='Available'){
                 
                 if($weekend_day)
                  {
                    // this is a weekend day and extra beds are available , DUH !!!!
                    if(intval($GET['data']['adult_config'][$i]) > $room_type_max_adult) // check if extra bed price is required
                    { 
                          $extra_bed = true;
                          $adult_extra_bed_available = $room_type_max_pax - $room_type_max_adult;
                          $extra_bed_required = intval($GET['data']['adult_config'][$i]) - $room_type_max_adult;

                          if($extra_bed_required<=$adult_extra_bed_available)
                          {
                            // lets grab the extra bed price
                            $rate_key = 'adult_extra_bed_price';
                          } else {
                            // this condition will never fall back still we are taking care of this condition and still charge extra adults with the number of extra beds 
                            $rate_key = 'adult_extra_bed_price';
                          } 
                    } else {
                       // here extra bed is not required, ie we have avaliable bed for the number of adults
                        $extra_bed = false;
                        $rate_key = filter_node_weekend_rateKey(intval($GET['data']['adult_config'][$i]),$node_data);
                    }

                    if($extra_bed)
                    {
                      $rat_key = filter_node_weekend_rateKey($GET['data']['adult_config'][$i],$node_data);
                      $adults_week_days_price_array[]=$node_data[$rat_key] + $node_data[$rate_key] * $extra_bed_required;
                     } else 
                    {
                      $adults_week_days_price_array[]=$node_data[$rate_key];
                     }

                  } 
                  else 
                  {
                    if(intval($GET['data']['adult_config'][$i]) > $room_type_max_adult) // check if extra bed price is required
                    { 
                          $extra_bed = true;
                          $adult_extra_bed_available = $room_type_max_pax - $room_type_max_adult;
                          $extra_bed_required = intval($GET['data']['adult_config'][$i]) - $room_type_max_adult;

                          if($extra_bed_required<=$adult_extra_bed_available)
                          {
                            // lets grab the extra bed price
                            $rate_key = 'adult_extra_bed_price';
                          } else {
                            // this condition will never fall back still we are taking care of this condition and still charge extra adults with the number of extra beds 
                            $rate_key = 'adult_extra_bed_price';
                          } 
                    } else {
                       // here extra bed is not required, ie we have avaliable bed for the number of adults
                        $extra_bed = false;
                        //$rate_key = filter_node_rateKey(intval($GET->adults[$i]),$node_data);
                        $rate_key = filter_node_ratekey_rooms($GET);
                    }

                    if($extra_bed)
                    {
                      //$rat_key = filter_node_rateKey($GET->adults[$i],$node_data);
                      $rat_key = filter_node_ratekey_rooms($GET); 
                      $adults_week_days_price_array[]=$node_data[$rat_key] + $node_data[$rate_key] * $extra_bed_required;
                     } else 
                    {
                      $adults_week_days_price_array[]=$node_data[$rate_key];
                     }
                  }
               }
              }

              $adults_week_price_array[] = array_sum($adults_week_days_price_array);
          }
        //print_r($adults_week_price_array);exit;
       //$total_adult_price = ($adult_count == 0) ? 0.00 :  array_sum($adults_week_price_array);
       $total_adult_price = ($adult_count == 0) ? 0.00 : array_sum($adults_week_price_array);
     
      } else { 
        
         if($room_type_extra_bed=='NotAvailable')
         {  
              $adults_week_price_array = array();
              $adults_week_price_array_multiplies_days = array(); 

             /* for($i=0;$i<intval($GET->rooms);$i++)
              {  */ 
                  $rate_key = filter_node_ratekey_rooms($GET);  //New One 
                  $rat_key = filter_node_ratekey_rooms($GET);
                  //$rate_key = filter_node_rateKey(intval($GET->adult[$i]),$node_data);
                  
                  //$adults_week_price_array[]=$node_data[$rate_key];
                  //$adults_week_price_array[]=$node_data[$rate_key] * $stay_days;
                   for ($j=0; $j < count($rat_key) ; $j++) { 
                    //echo 'sanjay'; print_r($node_data);
                  
                //$adults_week_price_array[]= $node_data[$rat_key[$i]] + $node_data[$rate_key] * $stay_days; 
                  
                $adults_week_price_array[]=$node_data[$rate_key[$j]] * $stay_days; 
                //$adults_week_price_array[]= $node_data[$rat_key[$j]] + $node_data[$rate_key] * $stay_days; 
                 }
                  $adults_week_price_array =  array_sum($adults_week_price_array);

             // }
              
             //$total_adult_price = ($adult_count == 0) ? 0.00 :  array_sum($adults_week_price_array); //My Change
              $total_adult_price = $adults_week_price_array;
             //print_r($total_adult_price); exit();
         } else if($room_type_extra_bed=='Available')
         { 
            $adults_week_price_array = array();
            $adults_week_price_array_multiplies_days = array(); 
            
            for($i=0;$i<intval($GET['data']['room_count']);$i++)
            { 
                if(intval($GET['data']['adult_config'][$i]) > $room_type_max_adult) // check if extra bed price is required
                { 
                    $extra_bed = true;
                    $adult_extra_bed_available = $room_type_max_pax - $room_type_max_adult; 
                    $extra_bed_required = intval($GET['data']['adult_config'][$i]) - $room_type_max_adult;

                    if($extra_bed_required<=$adult_extra_bed_available)
                    {
                      // lets grab the extra bed price
                      $rate_key = 'adult_extra_bed_price';
                    } else {
                      // this condition will never fall back still we are taking care of this condition and still charge extra adults with the number of extra beds 
                      $rate_key = 'adult_extra_bed_price';
                    }
                }  else {
                    // here extra bed is not required, ie we have avaliable bed for the number of adults
                    $extra_bed = false;
                    //$rate_key = filter_node_rateKey(intval($GET->adult[$i]),$node_data);
                    $rate_key = filter_node_ratekey_rooms($GET); 
                }
//NEW ONE EXTRA BED                
                if($extra_bed)
                {   $extra_bed = 1; 
                  $rate_key_extra = $rate_key;
                  //$rat_key = filter_node_rateKey($GET->adult[$i],$node_data);
                  $rate_key = filter_node_ratekey_rooms_extra($GET,$extra_bed);
                  $rat_key = filter_node_ratekey_rooms_extra($GET,$extra_bed); //print_r($rat_key); exit();
                  //$rat_key = filter_node_ratekey_rooms($GET); print_r($rat_key); exit();
                  //NEW
                  for ($i=0; $i < count($rat_key) ; $i++) { 
                    
                  
                $adults_week_price_array[]= $node_data[$rate_key[$i]] * $stay_days + $node_data[$rate_key_extra] * $stay_days; 
                 }
                 
               //echo 'sanjay'; print_r($adults_week_price_array); exit();
                 //End Of New
                  //exit();
                  //$adults_week_price_array[]=($node_data[$rat_key] + $node_data[$rate_key] * $extra_bed_required) * $stay_days;

//Need to test

                } else 
                {  
                  
                 
                 for ($i=0; $i < count($rate_key) ; $i++) { 
                   //$test = $rate_key[$i]
                
                  //$adults_week_price_array[]=($node_data[$rate_key]) * $stay_days; 
                $adults_week_price_array[]=$node_data[$rate_key[$i]] * $stay_days; 
                 }
                }
              }
          $total_price =  array_sum($adults_week_price_array); //print_r($total_price); exit();
            //$total_adult_price = ($adult_count == 0) ? 0.00 :  array_sum($adults_week_price_array); //by me
              $total_adult_price = $total_price;
             //
         }
          
      }

      //echo '<pre>'; print_r($total_adult_price); exit();
       //$total_price_children =  ($children_count == 0) ? 0.00 : floatval(array_sum($child_price_array) * $stay_days); 
        //echo '<pre>'; print_r($node_data); exit();
        $response['weekdays_single_room_price'] = $node_data['single_room_price'];
        $response['weekend_single_room_price'] = $node_data['weekend_single_room_price'];
        $response['weekdays_double_room_price'] = $node_data['double_room_price'];
        $response['weekend_double_room_price'] = $node_data['weekend_double_room_price'];
        $response['weekdays_triple_room_price'] = $node_data['triple_room_price'];
        $response['weekend_triple_room_price'] = $node_data['weekend_triple_room_price'];
        $response['weekdays_quad_room_price'] = $node_data['quad_room_price'];
        $response['weekend_quad_room_price'] = $node_data['weekend_quad_room_price'];
        $response['weekdays_hex_room_price'] = $node_data['hex_room_price'];
        $response['weekend_hex_room_price'] = $node_data['weekend_hex_room_price'];
        $response['require_weekend_price'] = $node_data['weekend_price'];
        $response['stay_days'] = $stay_days;
        $response['total_adult'] = $adult_count;
        $response['total_child'] = $children_count;
        $response['child_price_array'] = $child_price_array;
        $response['adult_price_array'] = $adults_week_price_array;
        $response['child_price_without_tax'] = $total_price_children;
        $response['adult_price_without_tax'] = $total_adult_price;
      //echo '<pre>san';  print_r($response); exit();
        
      if($node_data['gst_tax']=='EXCLUSIVE')
      {
        if($gst_tax!='0' && $total_adult_price!=0.00 && isset($gst_tax))
        {
            $gst_price = (
                (
                floatval($gst_tax / 100) 
                *  
                floatval($total_adult_price) 
                )
              );
            $total_adult_price = $total_adult_price + $gst_price;
        }

        if($gst_tax!='0' && $total_price_children!=0.00 && isset($gst_tax))
        {
            $gst_price = (
                (
                floatval($gst_tax / 100) 
                *  
                floatval($total_price_children) 
                )
              );
            $total_price_children = $total_price_children + $gst_price;
        }

      }

      if($node_data['service_tax']=='EXCLUSIVE')
      {
        if($service_tax!='0' && $total_adult_price!=0.00 && isset($service_tax))
        {
              $service_price = (
                (
                floatval($service_tax / 100) 
                *  
                floatval($total_adult_price) 
                )
              );
            $total_adult_price = $total_adult_price + $service_price;
        }

        if($service_tax!='0' && $total_price_children!=0.00 && isset($service_tax))
        {
             $service_price = (
                (
                floatval($service_tax / 100) 
                *  
                floatval($total_price_children) 
                )
              );
            $total_price_children = $total_price_children + $service_price;
        }
      }


      $response['gst_percentage'] = isset($gst_tax) ? $gst_tax : 0;;
      $response['sc_percentage'] = isset($service_tax) ? $service_tax : 0;
      $response['gst_price'] = isset($gst_price) ? $gst_price : 0;
      $response['sc_price'] = isset($service_price) ? $service_price : 0;
      $response['child_price'] = $total_price_children;
      $response['adult_price'] = $total_adult_price;
      //echo '<pre>'; print_r($response); exit();
      // final price conversion

     /* if($hotel_details['currency_type']!='USD' && $hotel_details['currency_type']!='')
      {
        $response['child_price'] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$response['child_price']);
        $response['adult_price'] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$response['adult_price']);
        $response['weekdays_single_room_price'] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$response['weekdays_single_room_price']);
        $response['weekend_single_room_price'] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$response['weekend_single_room_price']);
        $response['weekdays_double_room_price'] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$response['single_room_price']);
        $response['weekend_double_room_price'] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$response['double_room_price']);
        $response['child_price_without_tax'] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$response['child_price_without_tax']);
        $response['adult_price_without_tax'] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$response['adult_price_without_tax']);
        $response['gst_priced'] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$response['gst_price']);
        $response['sc_priced'] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$response['sc_price']);

        if(isset($response['adult_price_array']) && sizeof($response['adult_price_array']) > 0)
        {
          foreach($response['adult_price_array'] as $keya => $adult_price_array_ar)
          {
            $response['adult_price_array'][$keya] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$adult_price_array_ar);
          }
        }

        if(isset($response['child_price_array']) && sizeof($response['child_price_array']) > 0)
        {
          foreach($response['child_price_array'] as $keya => $each_child_price_ar)
          {
            $response['child_price_array'][$keya] = $CI->currencyconverter->convert($hotel_details['currency_type'],'USD',$each_child_price_ar);
          }
        }
      }*/
    //echo 'sanjay<pre>'; print_r($response); 
    $response_converted = search_data_in_preferred_currency($response, $currency_obj);
    return $response_converted;
    
    }

    function filter_node_weekend_rateKey($adult_count,$node_data)
    {
        if(intval($adult_count)==1)
        {
          $rate_key = 'weekend_single_room_price';
        } 
        else if(intval($adult_count)>=2)
        {
          
          if($node_data['weekend_double_room_price']=='0' || $node_data['weekend_double_room_price']=='' || is_null($node_data['weekend_double_room_price']) || $node_data['weekend_double_room_price']=='NULL')
          {
             $rate_key = 'weekend_single_room_price';
          } else {
             $rate_key = 'weekend_double_room_price';
          }
          
        }
        return $rate_key;
    }

    function filter_node_ratekey_rooms($GET)
    { 
        //echo 'dsds<pre>'; print_r($GET); exit();
        $rate_key = array();
        for ($i=0; $i < $GET['data']['room_count'] ; $i++) { 
         if ($GET['data']['adult_config'][$i] == '1') {
            $rate_key[$i] = 'single_room_price';
          }elseif($GET['data']['adult_config'][$i] == '2'){
            $rate_key[$i] = 'double_room_price';
          }elseif ($GET['data']['adult_config'][$i] == '3') {
            $rate_key[$i] = 'triple_room_price';
          }
        }
        
      /*if ($GET->rooms == 1) {
          if ($GET->adult[0] == '1') {
            $rate_key[0] = 'single_room_price';
          }elseif($GET->adult[0] == '2'){
            $rate_key[0] = 'double_room_price';
          }elseif ($GET->adult[0] == '3') {
            $rate_key[0] = 'triple_room_price';
          }
        //$rate_key = 'single_room_price';
      }elseif ($GET->rooms == 2) {
        if ($GET->adult[1] == '1') {
            $rate_key[1] = 'single_room_price';
          }elseif($GET->adult[1] == '2'){
            $rate_key[1] = 'double_room_price';
          }elseif ($GET->adult[1] == '3') {
            $rate_key[1] = 'triple_room_price';
          }
          //print_r($rate_key); exit();
        //$rate_key = 'double_room_price';
      }elseif ($GET->rooms == 3) {
        if ($GET->adult[2] == '1') {
            $rate_key[2] = 'single_room_price';
          }elseif($GET->adult[2] == '2'){
            $rate_key[2] = 'double_room_price';
          }elseif ($GET->adult[2] == '3') {
            $rate_key[2] = 'triple_room_price';
          }
        //$rate_key = 'triple_room_price';
      }elseif ($GET->rooms == 4) {
        if ($GET->adult[3] == '1') {
            $rate_key[3] = 'single_room_price';
          }elseif($GET->adult[3] == '2'){
            $rate_key[3] = 'double_room_price';
          }elseif ($GET->adult[3] == '3') {
            $rate_key[3] = 'triple_room_price';
          }
        //$rate_key = 'quad_room_price';
      }elseif ($GET->rooms == 5) {
        if ($GET->adult[4] == '1') {
            $rate_key[4] = 'single_room_price';
          }elseif($GET->adult[4] == '2'){
            $rate_key[4] = 'double_room_price';
          }elseif ($GET->adult[4] == '3') {
            $rate_key[4] = 'triple_room_price';
          }
        //$rate_key = 'hex_room_price';
      }*/
      //print_r($rate_key); exit();
     /* if(intval($adult_count)==1)
        {
          $rate_key = 'single_room_price';
        } 
        else if(intval($adult_count)>=2)
        {
          
          if($node_data['double_room_price']=='0' || $node_data['double_room_price']=='' || is_null($node_data['double_room_price']) || $node_data['double_room_price']=='NULL')
          {
             $rate_key = 'single_room_price';
          } else {
             $rate_key = 'double_room_price';
          }
          
        }*/
        return $rate_key;
    }

     function filter_node_ratekey_rooms_extra($GET,$extra)
    { 
        
        $rate_key = array();
        for ($i=0; $i <$GET['data']['room_count'] ; $i++) { 
         if ($GET['data']['adult_config'][$i]-$extra == '1') {
            $rate_key[$i] = 'single_room_price';
          }elseif($GET['data']['adult_config'][$i] - $extra == '2'){
            $rate_key[$i] = 'double_room_price';
          }elseif ($GET['data']['adult_config'][$i] - $extra == '3') {
            $rate_key[$i] = 'triple_room_price';
          }
        }
        
     //echo '<pre>'; print_r($rate_key); exit();
        return $rate_key;
    }

    function filter_node_rateKey($adult_count,$node_data)
    {
        if(intval($adult_count)==1)
        {
          $rate_key = 'single_room_price';
        } 
        else if(intval($adult_count)>=2)
        {
          
          if($node_data['double_room_price']=='0' || $node_data['double_room_price']=='' || is_null($node_data['double_room_price']) || $node_data['double_room_price']=='NULL')
          {
             $rate_key = 'single_room_price';
          } else {
             $rate_key = 'double_room_price';
          }
          
        }
        return $rate_key;
    }

    function fetch_crs_image($pic)
    {
      //echo '<pre>san'; print_r($pic); exit();
      $pic = (string) $pic;
      $pic_r=array();

      if(strpos($pic, ','))
      { 
        $pics = explode(',', $pic);
        foreach($pics as $each_pic)
        {
          if($each_pic!='')
          {
            if(is_file(FCPATH.'cpanel/uploads/hotel_images/'.$each_pic))
            {
               $pic_r[]=base_url().'cpanel/uploads/hotel_images/'.$each_pic;
            }
          }
        }
      } else 
      { 
         if(is_file(FCPATH.'cpanel/uploads/hotel_images/'.$pic))
            {
               $pic_r[]=base_url().'cpanel/uploads/hotel_images/'.$pic;
            }
      }
      return $pic_r;
    }

    function check_seasonPackage_availability($room_type_data,$GET)
    { 
        $s_room_max_adult = max($GET['data']['adult_config']);
        $s_room_max_child = max($GET['data']['child_config']);
        $each_rooms = $room_type_data;
        $max_pax = intval($each_rooms['max_pax']);
        $adult_capacity = intval($each_rooms['adult']);
        $child_capacity = intval($each_rooms['child']);
        $extra_bed = $each_rooms['extra_bed'];
            
            
          $package_available  = false;
           if($s_room_max_adult>$adult_capacity)
            { 
                if($extra_bed=='Available')
                {   
                    $available_adult_extra_bed = $max_pax - $adult_capacity;
                    $extra_bed_required = $s_room_max_adult- $adult_capacity;

                    if($extra_bed_required<=$available_adult_extra_bed)
                    { 
                        $adult_package_available = true;
                        $available_beds = $available_adult_extra_bed  - $extra_bed_required;
                    } else {
                        $adult_package_available = false;
                    }
                }  else if($extra_bed=='NotAvailable')
                {
                    $adult_package_available = false;
                }
                
            } else {
                 $adult_package_available = true;
            }

            if($s_room_max_child>$child_capacity)
            {
                if($extra_bed=='Available')
                {   
                    $available_child_extra_bed = ($package_available==false) ? $max_pax - $child_capacity : $available_beds;
                    
                    if($available_child_extra_bed<=0)
                    {
                       $available_child_extra_bed=1; // 1 extra bed per room 
                    } 
                    $extra_bed_required = $s_room_child_adult - $child_capacity;

                    if($extra_bed_required<=$available_child_extra_bed)
                    {
                        $child_package_available = true;
                    } else {
                        $child_package_available = false;
                    }
                }  else if($extra_bed=='NotAvailable')
                {
                    $child_package_available = false;
                }
            } else {
                 $child_package_available = true;
            }

            if($adult_package_available && $child_package_available)
            {
                $package_available=true;
            } else {  $package_available=false; }
                   
        return $package_available;
 }

 function sort_array_of_array(&$array, $subfield, $sort="")
{
    $sortarray = array();
    foreach ($array as $key => $row)
    {
        $sortarray[$key] = $row[$subfield];
    }

    array_multisort($sortarray, $sort, $array);
}


function pnr_generate($length=10){

    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "123456789";
    $max = strlen($codeAlphabet) - 1;
    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[rand(0, $max)];
    }
    return $token;
}

function app_reference($length=15){

    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "123456789";
    $max = strlen($codeAlphabet) - 1;
    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[rand(0, $max)];
    }
    return $token;
}

function client_reference($length=15){

    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "123456789";
    $max = strlen($codeAlphabet) - 1;
    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[rand(0, $max)];
    }
    return $token;
}



function skypicker_filter_api_response($flights,$flight_type,$case_a_markup=false,$case_b_markup=false)
{
  $CI = &get_instance();
  $CI->load->library(array('xml_to_array','CurrencyConverter'));
  $CI->load->model('Flight_Model');
  $flight_response_index_hashing_salt = $CI->config->item('flight_response_index_hashing_salt');
  $default_currency_array=$CI->currencyconverter->get_currentCurrency();
  $search_response = array();
  
   if($flight_type=='oneway' || $flight_type=='circle')
  {
      foreach($flights['data'] as $k => $each_flights)
      {
              $search_response[$k]['location_from'] = array('city' => $each_flights['cityFrom'],'country' => array('code' => $each_flights['countryFrom']['code'],'name' => $each_flights['countryFrom']['name']));
              $search_response[$k]['location_to'] = array('city' => $each_flights['cityTo'],'country' => array('code' => $each_flights['countryTo']['code'],'name' =>$each_flights['countryTo']['name']));
              $markup_rates = total_markup_calculate($case_b_markup,$case_a_markup,$each_flights['price']);
              $total_price = $CI->currencyconverter->convert('USD',$default_currency_array[0],$markup_rates['markedup_price']);
              $search_response[$k]['total_price'] =  number_format($total_price, 2, '.', '');
              $search_response[$k]['fly_duration'] =  $each_flights['fly_duration'];
              $search_response[$k]['distance'] =  $each_flights['distance'];
              $search_response[$k]['max_weight'] =  $each_flights['baglimit']['hold_weight'];

              $InComing_count = -1;
              $OutGoing_count = -1;
              $incoming_index = 0;
              $outgoing_index = 0;

          foreach($each_flights['route'] as $k1 => $each_fligh_routes){

               $route_type = ($each_fligh_routes['return'] == '1') ? 'InComing' : 'OutGoing';

              if($flight_type=='circle')
              {
                 if($each_fligh_routes['return'] == '1')
                   {
                      $InComing_count++;
                      $in = $incoming_index;
                      if($InComing_count==0)
                      {
                          $inti = $k1;
                      }
                   } else if($each_fligh_routes['return'] == '0'){
                      $OutGoing_count++;
                      $out = $outgoing_index;
                   }
              }
               
               if($flight_type=='oneway')
              {   
                  if($each_fligh_routes['return'] == '0')
                  {
                          $OutGoing_count++;
                          $out = $outgoing_index;
                   }
              }
               
               $route_type_index = ($each_fligh_routes['return'] == '1') ? $InComing_count : $OutGoing_count;

               $search_response[$k]['route'][$route_type][$route_type_index]['from'] = $each_fligh_routes['cityFrom'];
               $search_response[$k]['route'][$route_type][$route_type_index]['to'] = $each_fligh_routes['cityTo'];
               $search_response[$k]['route'][$route_type][$route_type_index]['departure_time'] = date('d M H:i',$each_fligh_routes['dTime']);
               $search_response[$k]['route'][$route_type][$route_type_index]['arrival_time'] = date('d M H:i',$each_fligh_routes['aTime']);
               $search_response[$k]['route'][$route_type][$route_type_index]['airline_code'] = $each_fligh_routes['airline'];
               $search_response[$k]['route'][$route_type][$route_type_index]['type'] = $route_type;
               $search_response[$k]['route'][$route_type][$route_type_index]['image'] = 'true';
              
             
               $airlineName_db = $CI->Flight_Model->get_airlineName_by_airlineCode($each_fligh_routes['airline']);
               if($airlineName_db->num_rows()==0)
               {
                  $search_response[$k]['route'][$route_type][$route_type_index]['airline_name'] = $each_fligh_routes['airline'];
               } else {
                  $airlineName = $airlineName_db->result_array();
                  $search_response[$k]['route'][$route_type][$route_type_index]['airline_name'] = $airlineName[0]['airline_name'];
               }
              $incoming_index++;
              $outgoing_index++;
          }
           $search_response[$k]['fro_stops'] =  ($OutGoing_count == -1 || $OutGoing_count == 0) ? 'Non-stop' : (($OutGoing_count)).' Stop' ;
           $search_response[$k]['fro_departure_time'] =   date('d M H:i',$each_flights['dTime']);
           $search_response[$k]['fro_arrival_time'] =  date('d M H:i',$each_flights['route'][$out]['aTime']);

           if($flight_type=='circle')
          {
               $search_response[$k]['return_duration'] =  $each_flights['return_duration'];
               $search_response[$k]['back_stops'] =  ($InComing_count == -1 || $InComing_count == 0) ? 'Non-stop' : (($InComing_count)).' Stop' ;
               $search_response[$k]['back_departure_time'] =   date('d M H:i',$each_flights['route'][$inti]['dTime']);
               $search_response[$k]['back_arrival_time'] =  date('d M H:i',$each_flights['route'][$in]['aTime']);
          }
          $search_response[$k]['fi'] =  sha1($k.$flight_response_index_hashing_salt); 
  }
} else if($flight_type=='multicity'){

  $route_str = 'route';
  foreach($flights as $k => $each_flights)
      {
              //$markup_rates = markup_calculate($markup_data,$each_flights['price']);
              $markup_rates = total_markup_calculate($case_b_markup,$case_a_markup,$each_flights['price']);
              echo $each_flights['price'];
              print_r($markup_rates);
              $total_price = $CI->currencyconverter->convert('USD',$default_currency_array[0],$markup_rates['markedup_price']);
              $search_response[$k]['total_price'] =  number_format($total_price, 2, '.', '');

              foreach($each_flights['route'] as $k1 => $each_flights_routes)
              {

                $search_response[$k]['routes'][$route_str.$k1]['location_from'] = array('city' => $each_flights_routes['cityFrom'],'country' => array('code' => $each_flights_routes['countryFrom']['code'],'name' => $each_flights_routes['countryFrom']['name']));
                $search_response[$k]['routes'][$route_str.$k1]['location_to'] = array('city' => $each_flights_routes['cityTo'],'country' => array('code' => $each_flights_routes['countryTo']['code'],'name' =>$each_flights_routes['countryTo']['name']));
                $search_response[$k]['routes'][$route_str.$k1]['fly_duration'] =  $each_flights_routes['fly_duration'];
                $search_response[$k]['routes'][$route_str.$k1]['distance'] =  $each_flights_routes['distance'];
                $search_response[$k]['routes'][$route_str.$k1]['max_weight'] =  $each_flights_routes['baglimit']['hold_weight'];
                $route_markup_rates = total_markup_calculate($case_b_markup,$case_a_markup,$each_flights_routes['price']);
                $route_price = $CI->currencyconverter->convert('USD',$default_currency_array[0],$route_markup_rates['markedup_price']);
                $search_response[$k]['routes'][$route_str.$k1]['route_price'] =  number_format($route_price, 2, '.', '');

                $OutGoing_count = -1;
                $outgoing_index = 0;

                foreach($each_flights_routes['route'] as $k2 => $each_flights_route_segments)
                {
                   $route_type = ($each_flights_route_segments['return'] == '1') ? 'InComing' : 'OutGoing';

                     if($flight_type=='multicity')
                      {   
                          if($each_flights_route_segments['return'] == '0')
                          {
                                  if($OutGoing_count==-1)
                                  {
                                    $top_inroute = $outgoing_index;
                                  }
                                  $OutGoing_count++;
                                  $out = $outgoing_index;
                           }
                      }
                       
                       $route_type_index = ($each_flights_route_segments['return'] == '1') ? $InComing_count : $OutGoing_count;
                       $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['from'] = $each_flights_route_segments['cityFrom'];
                       $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['to'] = $each_flights_route_segments['cityTo'];
                       $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['departure_time'] = date('d M H:i',$each_flights_route_segments['dTime']);
                       $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['arrival_time'] = date('d M H:i',$each_flights_route_segments['aTime']);
                       $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['airline_code'] = $each_flights_route_segments['airline'];
                       $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['type'] = $route_type;

                     $outgoing_index++;
                  }
                   $search_response[$k]['routes'][$route_str.$k1]['fro_stops'] =  ($OutGoing_count == -1 || $OutGoing_count == 0) ? 'Non-stop' : (($OutGoing_count)).' Stop' ;
                   $search_response[$k]['routes'][$route_str.$k1]['fro_departure_time'] =   date('d M H:i',$each_flights_routes['dTime']);
                   $search_response[$k]['routes'][$route_str.$k1]['fro_arrival_time'] =  date('d M H:i',$each_flights_routes['route'][$out]['aTime']);

                  if($k1==0)
                  {
                    $search_response[$k]['top_image'] =  $each_flights_routes['route'][$top_inroute]['airline'];
                    $airlineName_db = $CI->Flight_Model->get_airlineName_by_airlineCode($each_flights_routes['route'][$top_inroute]['airline']);
                    
                     if($airlineName_db->num_rows()==0)
                     {
                        $search_response[$k]['top_airline_name'] = $each_fligh_routes['airline'];
                     } else {
                        $airlineName = $airlineName_db->result_array();
                        $search_response[$k]['top_airline_name'] = $airlineName[0]['airline_name'];
                     }

                  } 
                        $search_response[$k]['fi'] =  sha1($k.$flight_response_index_hashing_salt); 

                   
                  
            }
              
      }
     
}
  return $search_response;

}

function skypicker_filter_keyNode_api_response($flight,$flight_type)
{
  $CI = &get_instance();
  $CI->load->library(array('xml_to_array','CurrencyConverter'));
  $CI->load->model('Flight_Model');
  $flight_response_index_hashing_salt = $CI->config->item('flight_response_index_hashing_salt');
  $default_currency_array=$CI->currencyconverter->get_currentCurrency();
  $search_response = array();
  $k=0;
  $each_flights = $flight;

   if($flight_type=='oneway' || $flight_type=='circle')
  {
      
              $search_response[$k]['location_from'] = array('city' => $each_flights['cityFrom'],'country' => array('code' => $each_flights['countryFrom']['code'],'name' => $each_flights['countryFrom']['name']));
              $search_response[$k]['location_to'] = array('city' => $each_flights['cityTo'],'country' => array('code' => $each_flights['countryTo']['code'],'name' =>$each_flights['countryTo']['name']));
              $total_price = $CI->currencyconverter->convert('USD',$default_currency_array[0],$each_flights['price']);
              $search_response[$k]['total_price'] =  number_format($total_price, 2, '.', '');
              $search_response[$k]['fly_duration'] =  $each_flights['fly_duration'];
              $search_response[$k]['distance'] =  $each_flights['distance'];
              $search_response[$k]['max_weight'] =  $each_flights['baglimit']['hold_weight'];

              $InComing_count = -1;
              $OutGoing_count = -1;
              $incoming_index = 0;
              $outgoing_index = 0;

          foreach($each_flights['route'] as $k1 => $each_fligh_routes)
          {

               $route_type = ($each_fligh_routes['return'] == '1') ? 'InComing' : 'OutGoing';

              if($flight_type=='circle')
              {
                 if($each_fligh_routes['return'] == '1')
                   {
                      $InComing_count++;
                      $in = $incoming_index;
                      if($InComing_count==0)
                      {
                          $inti = $k1;
                      }
                   } else if($each_fligh_routes['return'] == '0'){
                      $OutGoing_count++;
                      $out = $outgoing_index;
                   }
              }
               
               if($flight_type=='oneway')
              {   
                  if($each_fligh_routes['return'] == '0')
                  {
                          $OutGoing_count++;
                          $out = $outgoing_index;
                   }
              }
               
               $route_type_index = ($each_fligh_routes['return'] == '1') ? $InComing_count : $OutGoing_count;

               $search_response[$k]['route'][$route_type][$route_type_index]['from'] = $each_fligh_routes['cityFrom'];
               $search_response[$k]['route'][$route_type][$route_type_index]['to'] = $each_fligh_routes['cityTo'];
               $search_response[$k]['route'][$route_type][$route_type_index]['departure_time'] = date('d M H:i',$each_fligh_routes['dTime']);
               $search_response[$k]['route'][$route_type][$route_type_index]['arrival_time'] = date('d M H:i',$each_fligh_routes['aTime']);
               $search_response[$k]['route'][$route_type][$route_type_index]['airline_code'] = $each_fligh_routes['airline'];
               $search_response[$k]['route'][$route_type][$route_type_index]['flight_number'] = $each_fligh_routes['flight_no'];
               $search_response[$k]['route'][$route_type][$route_type_index]['from_airport_code'] = $each_fligh_routes['flyFrom'];
               $search_response[$k]['route'][$route_type][$route_type_index]['to_airport_code'] = $each_fligh_routes['flyTo'];

               $search_response[$k]['route'][$route_type][$route_type_index]['type'] = $route_type;
               
               //if(file_exists(FCPATH.'assets/images/iataim/'.$each_fligh_routes['airline'].'.png'))
               //{
                  $search_response[$k]['route'][$route_type][$route_type_index]['image'] = 'true';
              // } else {
                 // $search_response[$k]['route'][$route_type][$route_type_index]['image'] = '';
              // }
             
               $airlineName_db = $CI->Flight_Model->get_airlineName_by_airlineCode($each_fligh_routes['airline']);
               if($airlineName_db->num_rows()==0)
               {
                  $search_response[$k]['route'][$route_type][$route_type_index]['airline_name'] = $each_fligh_routes['airline'];
               } else {
                  $airlineName = $airlineName_db->result_array();
                  $search_response[$k]['route'][$route_type][$route_type_index]['airline_name'] = $airlineName[0]['airline_name'];
               }
              $incoming_index++;
              $outgoing_index++;
          }
           $search_response[$k]['fro_stops'] =  ($OutGoing_count == -1 || $OutGoing_count == 0) ? 'Non-stop' : (($OutGoing_count)).' Stop' ;
           $search_response[$k]['fro_departure_time'] =   date('d M H:i',$each_flights['dTime']);
           $search_response[$k]['fro_arrival_time'] =  date('d M H:i',$each_flights['route'][$out]['aTime']);

           if($flight_type=='circle')
          {
               $search_response[$k]['return_duration'] =  $each_flights['return_duration'];
               $search_response[$k]['back_stops'] =  ($InComing_count == -1 || $InComing_count == 0) ? 'Non-stop' : (($InComing_count)).' Stop' ;
               $search_response[$k]['back_departure_time'] =   date('d M H:i',$each_flights['route'][$inti]['dTime']);
               $search_response[$k]['back_arrival_time'] =  date('d M H:i',$each_flights['route'][$in]['aTime']);
          }
          $search_response[$k]['fi'] =  sha1($k.$flight_response_index_hashing_salt); 
 
} else if($flight_type=='multicity'){

        $route_str = 'route';

        $total_price = $CI->currencyconverter->convert('USD',$default_currency_array[0],$each_flights['price']);
        $search_response[$k]['total_price'] =  number_format($total_price, 2, '.', '');
        foreach($each_flights['route'] as $k1 => $each_flights_routes)
        {

          $search_response[$k]['routes'][$route_str.$k1]['location_from'] = array('city' => $each_flights_routes['cityFrom'],'country' => array('code' => $each_flights_routes['countryFrom']['code'],'name' => $each_flights_routes['countryFrom']['name']));
          $search_response[$k]['routes'][$route_str.$k1]['location_to'] = array('city' => $each_flights_routes['cityTo'],'country' => array('code' => $each_flights_routes['countryTo']['code'],'name' =>$each_flights_routes['countryTo']['name']));
          $search_response[$k]['routes'][$route_str.$k1]['fly_duration'] =  $each_flights_routes['fly_duration'];
          $search_response[$k]['routes'][$route_str.$k1]['distance'] =  $each_flights_routes['distance'];
          $search_response[$k]['routes'][$route_str.$k1]['max_weight'] =  $each_flights_routes['baglimit']['hold_weight'];
          $route_price = $CI->currencyconverter->convert('USD',$default_currency_array[0],$each_flights_routes['price']);
          $search_response[$k]['routes'][$route_str.$k1]['route_price'] =  number_format($route_price, 2, '.', '');

          $OutGoing_count = -1;
          $outgoing_index = 0;

          foreach($each_flights_routes['route'] as $k2 => $each_flights_route_segments)
          {
             $route_type = ($each_flights_route_segments['return'] == '1') ? 'InComing' : 'OutGoing';

               if($flight_type=='multicity')
                {   
                    if($each_flights_route_segments['return'] == '0')
                    {
                            if($OutGoing_count==-1)
                            {
                              $top_inroute = $outgoing_index;
                            }
                            $OutGoing_count++;
                            $out = $outgoing_index;
                     }
                }
                 
                 $route_type_index = ($each_flights_route_segments['return'] == '1') ? $InComing_count : $OutGoing_count;
                 $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['from'] = $each_flights_route_segments['cityFrom'];
                 $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['to'] = $each_flights_route_segments['cityTo'];
                 $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['departure_time'] = date('d M H:i',$each_flights_route_segments['dTime']);
                 $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['arrival_time'] = date('d M H:i',$each_flights_route_segments['aTime']);
                 $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['airline_code'] = $each_flights_route_segments['airline'];
                 $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['flight_number'] = $each_flights_route_segments['flight_no'];
                 $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['from_airport_code'] = $each_flights_route_segments['flyFrom'];
                 $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['to_airport_code'] = $each_flights_route_segments['flyTo'];

                  $airlineName_db = $CI->Flight_Model->get_airlineName_by_airlineCode($each_flights_route_segments['airline']);
                   if($airlineName_db->num_rows()==0)
                   {
                       $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['airline_name'] = $each_flights_route_segments['airline'];
                   } else {
                       $airlineName = $airlineName_db->result_array();
                       $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['airline_name'] = $airlineName[0]['airline_name'];
                   }

                 $search_response[$k]['routes'][$route_str.$k1]['route'][$route_type][$route_type_index]['type'] = $route_type;

               $outgoing_index++;
            }
             $search_response[$k]['routes'][$route_str.$k1]['fro_stops'] =  ($OutGoing_count == -1 || $OutGoing_count == 0) ? 'Non-stop' : (($OutGoing_count)).' Stop' ;
             $search_response[$k]['routes'][$route_str.$k1]['fro_departure_time'] =   date('d M H:i',$each_flights_routes['dTime']);
             $search_response[$k]['routes'][$route_str.$k1]['fro_arrival_time'] =  date('d M H:i',$each_flights_routes['route'][$out]['aTime']);

            if($k1==0)
            {
              //if(file_exists(FCPATH.'assets/images/iataim/'.$each_flights_routes['route'][$top_inroute]['airline'].'.png'))
               //{
                  $search_response[$k]['top_image'] =  $each_flights_routes['route'][$top_inroute]['airline'];
               //} else {
                 // $search_response[$k]['top_image'] =  '';
               //}
            } 
                  $search_response[$k]['fi'] =  sha1($k.$flight_response_index_hashing_salt); 

             
            
      }
  }
  return $search_response;

}


function skypicker_filter_checkFlight_response($checkFlight_data,$trip_type)
{
 
  $CI = &get_instance();
  $CI->load->library(array('xml_to_array','CurrencyConverter'));
  $CI->load->model('Flight_Model');
  $flight_response_index_hashing_salt = $CI->config->item('flight_response_index_hashing_salt');
  $default_currency_array=$CI->currencyconverter->get_currentCurrency();
  $search_response = array();

  $search_response['flight_check'] = $checkFlight_data['flights_checked'];
  $search_response['cannot_book'] = $checkFlight_data['flights_invalid'];
  $search_response['total_price'] = $CI->currencyconverter->convert('USD',$default_currency_array[0],$checkFlight_data['conversion']['amount']);
  $search_response['per_extra_beg_price'] = $CI->currencyconverter->convert('USD',$default_currency_array[0],$checkFlight_data['conversion']['bags_price'][1]);
  $search_response['is_document_required'] = $checkFlight_data['document_options']['document_need'];

    return $search_response;
}
function fetch_flight_nodeData_byIndex($complete_data,$index_key,$flight_type)
{ 
    $CI = &get_instance();
    $flight_response_index_hashing_salt = $CI->config->item('flight_response_index_hashing_salt');

    if(sizeof($complete_data) > 0)
    {
        $SESSION=json_decode($complete_data[0]['data'],true);
      
        if(intval($SESSION) == 0)
        { 
            $response = array(
                    'success' => 'false',
                    'response' => 'Session expired'
                    );
        } else {  
            if(isset($SESSION) && sizeof($SESSION) > 0)
            {
                $expiry_times =$complete_data[0]['expiry_and_insertion_unixtime']; 
                $expiry_time = explode('|', $expiry_times);
                if(time() > $expiry_time[0])
                {  
                    $response = array(
                        'success' => 'false',
                        'response' => 'Session expired'
                    );
                } else {  
                  $node = array();

                  if($flight_type=='oneway' || $flight_type=='circle')
                  {
                      $ar = $SESSION['data'];
                  } else if($flight_type=='multicity'){
                      $ar = $SESSION;
                  }
                  
                  foreach($ar as $k => $each_flights_segments)
                    {
                      if($index_key== sha1($k.$flight_response_index_hashing_salt))
                      {
                        $node = $each_flights_segments;
                      }
                    }

                  if(sizeof($node) > 0){
                    $response = array(
                        'success' => 'true',
                        'response' => $node
                    );
                  } else {
                    $response = array(
                        'success' => 'false',
                        'response' => 'Data not found'
                    );
                  }
                }
            }
        }

        return $response;
    } 

}

function createDummy_passport_details($type){
   
   if($type=='adult')
    {
       $age = '25';
       $passport_number = substr((str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ')),4,4).rand(11111111,99999999);
       $passport_expiery_date = date('Y-m-d',strtotime('+9years')); 
       $dob = date('Y-m-d',strtotime("-".($age-1)."years"));
      
    }
    else if($type=='child')
    {
        $age = '11'; 
        $passport_number = substr((str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ')),4,4).rand(11111111,99999999);
        $passport_expiery_date = date('Y-m-d',strtotime('+9years')); 
        $dob = date('Y-m-d',strtotime("-".($age-1)."years"));
    }
    else if($type=='infant')
    {
       $age = '1'; 
       $passport_number = substr((str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ')),4,4).rand(11111111,99999999);
       $passport_expiery_date = date('Y-m-d',strtotime('+119month'));
       $dob = date('Y-m-d',strtotime("-1years"));
    }

    $return = array(
                  'passport_number' => $passport_number,
                  'passport_expiery_date' => strtotime($passport_expiery_date),
                  'dob' => strtotime($dob)
                  );

    return $return;

}

function get_recaptchaHtml($create_callback='')
{
    $CI=&get_instance();
    $lang = 'en';
    $secret_key = $CI->config->item('recaptcha_secret_key');
    $site_key = $CI->config->item('recaptcha_site_key');

    $htm = '<div class="g-recaptcha"';
    
    if(!empty($create_callback))
    {
      $htm .= ' data-callback="'.$create_callback.'"';  
    }
    
    $htm.=' data-sitekey="'.$site_key.'"></div>
              <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl='.$lang.'"></script>';
        return $htm;

  }

  function payment_charge($total_amt,$payemt_charge){
    //$markedup_price = number_format($markedup_price, 2, '.', '');
    $total_amt = $total_amt*($payemt_charge/100);
    $total_amt = number_format($total_amt, 2, '.', '');
    return $total_amt;
  }

  function markup_calculate_agent($markup,$amount){
   //echo "<pre/>"; print_r($markup);exit;
   if($markup == false){
    $data = array('markedup_price' => $amount['markedup_price'],'markup_price' => $amount['markup_price'],'my_markup' => $amount['my_markup']);
   }else{
    $markup_price = $amount['markup_price'];
    // /error_reporting(E_ALL);
    if($markup == false){
      $markup_value = $amount;
    }else{
      if($markup->markup_fixed == 0){
        $test = $markup->markup_value / 100;
         $my_markup = (
                (
                floatval($markup->markup_value / 100) 
                * 
                floatval($amount['markedup_price']) 
                )
              );
         $markedup_price = $amount['markedup_price']+$my_markup;
         //echo $markedup_price;
      }else{
        $my_markup = $markup->markup_fixed;
        $markedup_price = (floatval($markup->markup_value) + floatval($amount['markedup_price']));
      }

       $my_markup = number_format($my_markup, 2, '.', '');
      $markedup_price = number_format($markedup_price, 2, '.', '');
      // /echo $markedup_price.'<br/>';
      $data =  array('markedup_price' => $markedup_price,'markup_price' => $markup_price,'my_markup' => $my_markup);
    }
  }
  return $data;
  }

  function get_agent_markup_details($tp){
    //error_reporting(E_ALL);
      $CI = &get_instance();
      $CI->load->model('General_Model');
    if($CI->session->userdata('user_type_id') == 2){
      $user_id   = $CI->session->userdata('user_b2_id');
      $user_type = $CI->session->userdata('user_type_id');
      //echo "<pre/>";print_r($user_type);exit;
      $agent_markup_value = $CI->General_Model->getAgentMarkup($user_type,$user_id);
      
      $price_agent = markup_calculate_agent($agent_markup_value,$tp);
      return $price_agent;
    }else{
      return $tp;
    }
    
  }

  function get_Admin_Markup($product_details_id,$user_type,$user_id,$user_country_code){
     $CI = &get_instance();
     $CI->load->model('General_Model');
     $country_id = $CI->General_Model->getMarkupCountry($user_country_code);
   // echo "$country_id";exit;
     
     $markup = $CI->General_Model->get_Admin_Markup($product_details_id,$user_type,$user_id,$country_id);
     //echo "string";
     //echo "<pre/>";print_r($markup);exit;
     return $markup;
  
  }

  function gat_service_charge($nation){
     $CI = &get_instance();
     $CI->load->model('General_Model');
    
     $service = $CI->General_Model->getService($nation);
     //echo "string";
     //echo "<pre/>";print_r($markup);exit;
     return $service;
  
  }

  function claculate_admin_markup($markup,$actual_price){
    if(!$markup || $markup=='false'){
        $markup_price='0.00';
        $markedup_price = $actual_price;
      } else {

          if($markup['markup_fixed']=='0' || $markup['markup_fixed']=='' || is_null($markup['markup_fixed']) || $markup['markup_fixed']=='NULL')
          {
            //echo "string";
            $markup_percentage = floatval($markup['markup_value']); //percentage
            $markup_price = ((($markup_percentage / 100) * ($actual_price)));
            
          } else {
            $markup_price = floatval($markup['markup_fixed']);
          }
    
        $markedup_price = (floatval($markup_price) + floatval($actual_price));
      }

      $my_markup = 0;
      $my_markup = number_format($my_markup, 2, '.', '');
      $markedup_price = number_format($markedup_price, 2, '.', '');
      // /echo $markedup_price.'<br/>';
      
      $data =  array('markedup_price' => $markedup_price,'markup_price' => $markup_price,'my_markup' => $my_markup);
      return $data;
  }

  function claculate_service_charge($markup,$actual_price){
    if(!$markup || $markup=='false'){ 
        $markup_price='0.00';
        $markedup_price = $actual_price;
      } else { 

        
            //echo "string";
            $markup_percentage = floatval($markup['service_charge']); //percentage
            $markup_price = ((($markup_percentage / 100) * ($actual_price)));
            
         
    
        $markedup_price = (floatval($markup_price) + floatval($actual_price));
        //echo '<pre>'; print_r($markedup_price); exit();
      }

      $my_markup = 0;
      $my_markup = number_format($my_markup, 2, '.', '');
      $markedup_price = number_format($markedup_price, 2, '.', '');
      // /echo $markedup_price.'<br/>';
      
      $data =  array('servicedup_price' => $markedup_price,'service_price' => $markup_price,'my_service' => $my_markup);
      return $data;
  }

  function calculate_flight_markup_admin($price,$search_data,$airline_code){
  //echo "<pre/>";print_r($search_data);exit;
     $CI = &get_instance();
     $CI->load->model('General_Model');
     $dep           = $search_data['departure_from'][0];
     $depo          = explode('(', $dep);
     $deps          = explode(')', $depo[1]);
     $airport_code  = $deps[0];

    $country_id = $CI->General_Model->getFlightMarkupCountry($airport_code);
    //echo "<pre/>";print_r($country_id);exit;
    if($country_id != false){
      $user_id   = $CI->session->userdata('user_b2_id');
      $user_type = $CI->session->userdata('user_type_id');
      $product_details_id = 1;
      //$airline_code = 'WB';
      //echo $airline_code;exit;
      $markup = $CI->General_Model->get_Admin_Markup_Flight($product_details_id,$user_type,$user_id,$country_id,$airline_code);
      //echo "<pre/>";print_r($markup);exit;
      $data = claculate_admin_markup($markup,$price);
      
    }else{
      $data =  array('markedup_price' => $price,'markup_price' => 0.00,'my_markup' => 0);
    }
    return $data;
  }

  function calculate_crs_roomPriceEvent($price_data,$GET,$hotel_dataa){
  // /echo "<pre/>";print_r($GET);exit;
  $datetime1 = new DateTime($GET['hotel_checkin']);
  $datetime2 = new DateTime($GET['hotel_checkout']);
  $oDiff = $datetime1->diff($datetime2);
  $stay_days = intval($oDiff->d);
   $adults = array_sum($GET['adults']);
   $child  = array_sum($GET['children']);
   $total_count = $adults+$child;
   $total_price = $total_count*$price_data[0]->price;
   $total_price = $stay_days*$total_price;
   return array('price' => $total_price);
    // /echo "<pre/>";print_r($total_price);exit;
  }

   function search_data_in_preferred_currency($search_result, $currency_obj) {
   // debug($currency_obj);exit;
    $hotels = $search_result;
    $hotel_list = array ();
    if(!empty($search_result)){
      $hotel_list = preferred_currency_fare_object ( $search_result, $currency_obj );
    }
    return $hotel_list;
  }

   function preferred_currency_fare_object($fare_details, $currency_obj, $default_currency = '') {
    $currency_obj = new Currency ( array (
        'module_type' => 'hotel',
        'from' => get_application_default_currency (),
        'to' => get_application_currency_preference () 
    ) );
   // debug($fare_details);exit;
    $price_details = array ();
    /*$price_details ['CurrencyCode'] = empty ( $default_currency ) == false ? $default_currency : get_application_currency_preference ();*/
    $price_details ['weekdays_single_room_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['weekdays_single_room_price'] ) );
    $price_details ['weekend_single_room_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['weekend_single_room_price'] ) );
     $price_details ['weekdays_double_room_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['weekdays_double_room_price'] ) );
     $price_details ['weekend_double_room_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['weekend_double_room_price'] ) );
     $price_details ['weekdays_triple_room_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['weekdays_triple_room_price'] ) );
     $price_details ['weekend_triple_room_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['weekend_triple_room_price'] ) );
     $price_details ['weekdays_quad_room_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['weekdays_quad_room_price'] ) );
      $price_details ['weekend_quad_room_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['weekend_quad_room_price'] ) );
      $price_details ['weekdays_hex_room_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['weekdays_hex_room_price'] ) );
      $price_details ['weekend_hex_room_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['weekend_hex_room_price'] ) );
      $price_details ['require_weekend_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['require_weekend_price'] ) );
      $price_details ['stay_days'] = $fare_details ['stay_days'];
      $price_details ['total_adult'] = $fare_details ['total_adult'];
      $price_details ['total_child'] = $fare_details ['total_child'];

      $price_details ['child_price_array'] = (is_array($fare_details['child_price_array']) ? child_price_array_converter($fare_details['child_price_array'],$currency_obj) : get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['child_price_array'] ) ) ) ;

      $price_details ['adult_price_array'] = (is_array($fare_details['adult_price_array']) ? child_price_array_converter($fare_details['adult_price_array'],$currency_obj) : get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['adult_price_array'] ) ) ) ;

      $price_details ['child_price_without_tax'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['child_price_without_tax'] ) );
      $price_details ['child_price_without_tax'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['child_price_without_tax'] ) );
      $price_details ['adult_price_without_tax'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['adult_price_without_tax'] ) );
      $price_details ['gst_percentage'] = $fare_details ['gst_percentage'];
      $price_details ['gst_percentage'] = $fare_details ['gst_percentage'];
      $price_details ['sc_percentage'] = $fare_details ['sc_percentage'] ;
       $price_details ['gst_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['gst_price'] ) );
       $price_details ['sc_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['sc_price'] ) );
       $price_details ['child_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['child_price'] ) );
       $price_details ['adult_price'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $fare_details ['adult_price'] ) );
    return $price_details;
  }
  function child_price_array_converter($child_array,$currency_obj){
    $child_price_return  = [];
    foreach($child_array as $key => $val){
      $child_price_return[] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $val ) );
    }
    return $child_price_return;
  }

?>