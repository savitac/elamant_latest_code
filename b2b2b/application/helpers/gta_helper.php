<?php if (!defined('BASEPATH'))   exit('No direct script access allowed');

function HotelValuedAvailRQ($request,$api_id,$session_data)
{
	$credentials = TF_getApiCredentials();
	$postdata= json_decode(base64_decode($request));
	$check_in_date = $postdata->hotel_checkin;
	$check_out_date = $postdata->hotel_checkout;
	$check_in_unix = strtotime($check_in_date);
	$check_out_unix = strtotime($check_out_date);
	$duration_span_str = abs($check_out_unix - $check_in_unix);
	$duration_span = floor($duration_span_str/(60*60*24));
	$country = explode(",", $postdata->city);
	$country_name = trim($country[1]);

	
	$check_in_date_format = date('Y-m-d', strtotime($check_in_date));
	$room_count = $postdata->rooms;
	$CI =& get_instance();
	$CI->load->model('general_model');
	
    $city_details = $CI->general_model->get_city_details_id($postdata->city); 
 
   
    $searchRequest = '';

 	  $searchRequest .= '<Request>
				<Source>
					<RequestorID Client="'.$credentials->api_username1.'" EMailAddress="'.$credentials->api_username.'" Password="'.$credentials->api_password.'"/>
					<RequestorPreferences Country="CA" Currency="CAD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
					</RequestorPreferences>
				</Source>
				<RequestDetails>';

		if(count($city_details) > 0) {
			for($city =0; $city < count($city_details); $city++) {			
				 $searchRequest .= '<SearchHotelPriceRequest>
							<ItemDestination DestinationCode = "'.$city_details[$city]->gta_city_code.'" DestinationType = "city"/>
							<ImmediateConfirmationOnly/>
							<PeriodOfStay>
								<CheckInDate>'.$check_in_date_format.'</CheckInDate>
								<Duration>'.$postdata->days.'</Duration>
							</PeriodOfStay>
							<IncludePriceBreakdown/>
							<IncludeChargeConditions DateFormatResponse="true"></IncludeChargeConditions>';
	
	if(isset($postdata->adult)) {
		$adult = $postdata->adult;
		$child = $postdata->child;

		$searchRequest .= '<Rooms>';

		for($i=0; $i < count($adult); $i++) {

			$adultCount = $adult[$i];
			$childCount = $child[$i];

			

			if($adultCount == 1 && $childCount == 0) {
				$searchRequest .= '<Room Code="SB"></Room>';
			}
			else if($adultCount == 1 && $childCount == 1) {

				$childage = $postdata->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="TS" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
					}

				}

			}
			else if($adultCount == 1 && $childCount == 2) {
				
				$childage = $postdata->childAges;

				$ageString = ''; $numCots = 0;
				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$numCots++;
											
						} else {
							$ageString .= '<Age>'.$ca.'</Age>';
						}
					}
				}

				if($numCots > 0) {
					$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
				} else {
					$numberOfCotsString = '';
				}

				if($ageString != "") {    //this means that children are more than 2 yr old
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
				} else {
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ></Room>';
				}

			}
			else if($adultCount == 2 && $childCount == 0) {
					
				$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
				$searchRequest .= '</Room>';

			} 
			else if($adultCount == 2 && $childCount == 1) {
				$childage = $postdata->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="DB" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
					}
				}
			}
			else if($adultCount == 2 && $childCount == 2) {
				$childage = $postdata->childAges;

				$ageString = ''; $numCots = 0;
				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$numCots++;
											
						} else {
							$ageString .= '<Age>'.$ca.'</Age>';
						}
					}
				}

				if($numCots > 0) {
					$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
				} else {
					$numberOfCotsString = '';
				}

				if($ageString != "") {    //this means that children are more than 2 yr old
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
				} else {
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ></Room>';
				}
			}
			else if($adultCount == 3 && $childCount == 0) {
				$searchRequest .= '<Room Code="TR">';
				$searchRequest .= '</Room>';
			}
			else if($adultCount == 3 && $childCount == 1) {

				$childage = $postdata->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="TR" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';

							$searchRequest .= '<Room Code="DB">';
							$searchRequest .= '</Room>';
						}
					}
				}

			}
			else if($adultCount == 3 && $childCount == 2) {
				
				$childage = $postdata->childAges;
				
				$ca1 = $childage[$i][0];   // Get the age of individual child
				$ca2 = $childage[$i][1];   // Get the age of individual child

				if($ca1 == 1 && $ca2 == 1) {
					$searchRequest .= '<Room Code="DB" NumberOfCots="2">';
					$searchRequest .= '</Room>';

					$searchRequest .= '<Room Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 == 1 && $ca2 > 1) {
					$searchRequest .= 	'<Room Code="DB" NumberOfCots="1">
											<ExtraBeds>
												<Age>'.$ca2.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= '<Room Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 > 1 && $ca2 == 1) {
					$searchRequest .= 	'<Room Code="DB" NumberOfCots="1">
											<ExtraBeds>
												<Age>'.$ca1.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= '<Room Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 > 1 && $ca2 > 1) {
					$searchRequest .= 	'<Room Code="TB">
											<ExtraBeds>
												<Age>'.$ca1.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= 	'<Room Code="TB">
											<ExtraBeds>
												<Age>'.$ca2.'</Age>
											</ExtraBeds>
										</Room>';
				}	

			}
			else if($adultCount == 4 && $childCount == 0) {
				$searchRequest .= '<Room Code="Q">';
				$searchRequest .= '</Room>';
			}

		}
		$searchRequest .= '</Rooms>';
		$searchRequest  .='<OrderBy>pricelowtohigh</OrderBy></SearchHotelPriceRequest>' ;
		
	}
}
}

	
	 $searchRequest .= 	'</RequestDetails>
			</Request>';  
			
		
	$xml_title = '/HotelValuedAvailRQRS/HotelsAvailableRequest';
    Savelog($xml_title,$searchRequest); 
		
	
		$HotelSearchRS = processRequest_gta($searchRequest,$xml_type,$xml_title);
		$xml_title = '/HotelValuedAvailRQRS/HotelsAvailableResponse';
        Savelog($xml_title,$HotelSearchRS); 
		$formated_result = ts_formate_result($HotelSearchRS , $session_data ,$api_id, $check_in_date_format, $country_name);
		$CI =& get_instance();
		$CI->load->model('hotel_model');
	
		$CI->hotel_model->save_result_gta($formated_result ,$request, $session_data ,$api_id);
	
} 

function getRoomTypePrecheck($a, $c){

	if($a == 1 && $c == 0) {	
		$room_type = 'Code="SB"';	
	} else if($a == 2 && $c == 0) {	
		$room_type = 'Code="DB"';	
	} else if($a == 3 && $c == 0) {
		$room_type = 'Code="DB" '; 
	} else if($a == 2 && $c == 1) {
		$room_type = 'Code="DB"';
	} else if($a == 2 && $c == 2) {
		$room_type = 'Code="DB"';
	} else if($a == 3 && $c == 1) {
		$room_type = 'Code="TB"';			
	} else if($a == 1 && $c == 1) {
		$room_type = 'Code="DB"';	
	} else if($a == 1 && $c == 2) {
		$room_type = 'Code="DB"';	
	}  else {	
		$room_type = "";	
	}
	return $room_type;
}



function getHotelDetailByCode($request,$item_code, $city_code) {
	//echo"<pre/>";print_r($item_code);exit;
	$nationality = 'US';
	$credentials = TF_getApiCredentials();
	$check_in_date = $request->hotel_checkin;
	$check_out_date = $request->hotel_checkout;
	$check_in_unix = strtotime($check_in_date);
	$check_out_unix = strtotime($check_out_date);
	$duration_span_str = abs($check_out_unix - $check_in_unix);
	$duration_span = floor($duration_span_str/(60*60*24));
	/*Calculate duration*/
	
	$check_in_date_format = date('Y-m-d', strtotime($check_in_date));
	
	
	$CI =& get_instance();
	$CI->load->model('general_model');
	//$city_details = $CI->general_model->get_city_details_id($request->city); 
	//$city_code = $city_details->gta_city_code;
	//$city_code = 'MIA';
	 
 
	$room_count = $request->rooms;

	$room_xml_string = '';
	for($h=0; $h<$room_count; $h++) {
		$room_total_adult = $request->adult[$h];
		$room_total_child = $request->child[$h];

		$room_code = getRoomTypePrecheck($room_total_adult, $room_total_child);

		$room_xml_string .= '<Room '.$room_code.' />';

	}

	$searchRequest = '';
	 $searchRequest .= '<Request>
				<Source>
					<RequestorID Client="'.$credentials->api_username1.'" EMailAddress="'.$credentials->api_username.'" Password="'.$credentials->api_password.'"/>
					<RequestorPreferences Country="CA" Currency="CAD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
					</RequestorPreferences>
				</Source>
				<RequestDetails><SearchHotelPriceRequest>
						<ItemDestination DestinationType="city" DestinationCode="'.$city_code.'" />
							<ItemCodes>
								<ItemCode>'.$item_code.'</ItemCode>
							</ItemCodes>
							<PeriodOfStay>
								<CheckInDate>'.$check_in_date_format.'</CheckInDate>
								<Duration>'.$request->days.'</Duration>
							</PeriodOfStay>
							<IncludePriceBreakdown/>
							<IncludeChargeConditions DateFormatResponse="true"></IncludeChargeConditions>'; 

	 

	if(isset($request->adult)) {
		$adult = $request->adult;
		$child = $request->child;

		$searchRequest .= '<Rooms>';

		for($i=0; $i < count($adult); $i++) {

			/*echo 'Adult: '.$adult[$i];					
			echo 'Child: '.$child[$i];*/

			$adultCount = $adult[$i];
			$childCount = $child[$i];

			

			if($adultCount == 1 && $childCount == 0) {
				$searchRequest .= '<Room Code="SB"></Room>';
			}
			else if($adultCount == 1 && $childCount == 1) {

				$childage = $request->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="TS" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
					}

				}

			}
			else if($adultCount == 1 && $childCount == 2) {
				
				$childage = $request->childAges;

				$ageString = ''; $numCots = 0;
				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$numCots++;
											
						} else {
							$ageString .= '<Age>'.$ca.'</Age>';
						}
					}
				}

				if($numCots > 0) {
					$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
				} else {
					$numberOfCotsString = '';
				}

				if($ageString != "") {    //this means that children are more than 2 yr old
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
				} else {
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ></Room>';
				}

			}
			else if($adultCount == 2 && $childCount == 0) {
					
				$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
				$searchRequest .= '</Room>';

			} 
			else if($adultCount == 2 && $childCount == 1) {
				$childage = $request->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="DB" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
					}

				}
			}
			else if($adultCount == 2 && $childCount == 2) {
				$childage = $request->childAges;

				$ageString = ''; $numCots = 0;
				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$numCots++;
											
						} else {
							$ageString .= '<Age>'.$ca.'</Age>';
						}
					}
				}

				if($numCots > 0) {
					$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
				} else {
					$numberOfCotsString = '';
				}

				if($ageString != "") {    //this means that children are more than 2 yr old
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
				} else {
					$searchRequest .= '<Room Code="DB" '.$numberOfCotsString.' ></Room>';
				}
			}
			else if($adultCount == 3 && $childCount == 0) {
				$searchRequest .= '<Room Code="TR">';
				$searchRequest .= '</Room>';
			}
			else if($adultCount == 3 && $childCount == 1) {
				/*
				A: 3, C: 1 - Book two rooms
				*/

				$childage = $request->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="TR" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';

							$searchRequest .= '<Room Code="DB">';
							$searchRequest .= '</Room>';
						}
					}
				}
			}
			else if($adultCount == 3 && $childCount == 2) {
				$childage = $request->childAges;
				
				$ca1 = $childage[$i][0];   // Get the age of individual child
				$ca2 = $childage[$i][1];   // Get the age of individual child

				if($ca1 == 1 && $ca2 == 1) {
					$searchRequest .= '<Room Code="DB" NumberOfCots="2">';
					$searchRequest .= '</Room>';

					$searchRequest .= '<Room Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 == 1 && $ca2 > 1) {
					$searchRequest .= 	'<Room Code="DB" NumberOfCots="1">
											<ExtraBeds>
												<Age>'.$ca2.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= '<Room Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 > 1 && $ca2 == 1) {
					$searchRequest .= 	'<Room Code="DB" NumberOfCots="1">
											<ExtraBeds>
												<Age>'.$ca1.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= '<Room Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 > 1 && $ca2 > 1) {
					$searchRequest .= 	'<Room Code="TB">
											<ExtraBeds>
												<Age>'.$ca1.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= 	'<Room Code="TB">
											<ExtraBeds>
												<Age>'.$ca2.'</Age>
											</ExtraBeds>
										</Room>';
				}	
			}
			else if($adultCount == 4 && $childCount == 0) {
				$searchRequest .= '<Room Code="Q">';
				$searchRequest .= '</Room>';
			}

		}

		$searchRequest .= '</Rooms>';
		$searchRequest .= 	'</SearchHotelPriceRequest></RequestDetails>
			</Request>';
	}

	$getHotelDetail_rq = $searchRequest;

	//~ header("Content-Type: text/xml");
	//~ echo $getHotelDetail_rq;
	
$xml_title = '/HotelValuedRoomAvailRQRS/HotelsAvailableRequest';
Savelog($xml_title,$getHotelDetail_rq);

$xml_type = '';
$HotelSearchRS = processRequest_gta($getHotelDetail_rq,$xml_type,$xml_title);

$xml_title = '/HotelValuedRoomAvailRQRS/HotelsAvailableResponse';
Savelog($xml_title,$HotelSearchRS); 

       //~ header("Content-type: text/xml"); //echo $HotelSearchRS; die();

	$GetHotelDetailRQ_RS = array(
		'GetHotelDetailRQ' => $getHotelDetail_rq,
		'GetHotelDetailRS' => $HotelSearchRS
	);
	
	return $GetHotelDetailRQ_RS;

}

function ts_formate_result($HotelSearchRS , $session_id ,$api_id, $check_in_date_format, $country_name)
{     
	    $CI =& get_instance();
	    $CI->load->model('general_model');
		$CI->load->model('hotel_model');
		$CI->load->model('markup_model');
		
		
		$user_nationality_details = $CI->general_model->getCountryByCountyCode($country_name);
		$user_nationality = $user_nationality_details->country_id;
		//$user_nationality = $country_name;
		$admin_markup_details = $CI->markup_model->get_admin_markup_details($user_nationality ='', $check_in_date_format, $product_id ='1', $api_id);
		$b2b_markup_details = $CI->markup_model->get_b2b_markup_details($user_nationality ='', $check_in_date_format, $product_id ='1', $api_id);
		//~ echo "b2b_markup_details<pre>";print_r($b2b_markup_details);exit;
		if($CI->session->userdata('user_type') == 4){
		$sub_agent_markup_details = $CI->markup_model->get_sub_agent_markup_details($user_nationality ='', $check_in_date_format, $product_id ='1', $api_id);	
		$b2b2b_markup_details = $CI->markup_model->get_b2b2b_markup_details($user_nationality ='', $check_in_date_format, $product_id ='1', $api_id);	
		}
		
		$temp = array();
				$dom2=new DOMDocument();
				$dom2->loadXML($HotelSearchRS);
				$hotel=$dom2->getElementsByTagName("Hotel");
				$i=0;
				$final_result=array();
				
		foreach($hotel as $val)
		{
			$room = $val->getElementsByTagName("RoomCategory");
			 $availableRoom = $room->length;
			
			 
					$item = $val->getElementsByTagName("Item");
					$itemCode=$item->item(0)->getAttribute("Code");
					
					$City = $val->getElementsByTagName("City");
					$CityCode=$City->item(0)->getAttribute("Code");
 
		  			$hotel_details = $CI->hotel_model->get_gta_hotel_data($itemCode,$CityCode); 
		  			
		 if($hotel_details!='')
			{
			   
            $final_result[$i]['session_id'] = $session_id;
			$final_result[$i]['api'] = $api_id;
            $final_result[$i]['hotel_code'] =  $itemCode; 
            $final_result[$i]['hotel_id'] =   $itemCode; 
            $final_result[$i]['sid'] =   $CityCode; 
			  
            $final_result[$i]['hotel_name'] = $hotel_details->HotelName;
			 $final_result[$i]['hotel_api_images'] ='';
			 $a=array();
			$kk='';
			
			$ImageLinkImage = $hotel_details->ImageLinkImage;
			if($ImageLinkImage!='')
			{
				$ags = explode(",",$ImageLinkImage);
 				 $a = $ags;
				 $kk = 	$ags[0];
			}
 			
			 $final_result[$i]['hotel_api_images'] = $kk;
			 $final_result[$i]['images'] = json_encode($a);
			 			 
            
			
			
			$ii=0;
			 
					foreach($room as $category)
					{ 
						
							if(!in_array($itemCode,$temp))
						    {	
							$room_code=$room->item($ii)->getAttribute("Id");
							$roomCategory = $category->getElementsByTagName("Description");
							$room_type=$roomCategory->item(0)->nodeValue;
							$cost = $category->getElementsByTagName("ItemPrice");
							$currencyv1=$cost->item(0)->getAttribute("Currency");
							$org_amt=$cost->item(0)->nodeValue;
							$status = $category->getElementsByTagName("Confirmation");
							$status_val=$status->item(0)->nodeValue;
							$meals = $category->getElementsByTagName("Meals");
							$meals_val=$meals->item(0)->nodeValue;
						
						    //~ $TotalPrice_v		= $CI->general_model->convert_api_to_base_currecy_with_markup($org_amt,$currencyv1,$api_id); // Commented as of now
					    $TotalPrice_v = $org_amt;
					   
							//$pay_charge 		= $CI->general_model->get_payment_charge();
								$pay_charge = 0;
							//$TotalPrice_v		= $CI->general_model->Hgenerealmarkup($org_amt,$currencyv1,$api_id);
							//~ Commented as of now 
							//~ $Netrate = $TotalPrice_v['Netrate'];
					//~ $Admin_Markup =$TotalPrice_v['Admin_Markup'];
					//~ $Admin_BasePrice =$TotalPrice_v['Admin_BasePrice'];
					//~ $My_Markup = $TotalPrice_v['My_Markup'];
					//~ $TotalPrice =$TotalPrice_v['TotalPrice'];
					$admin_markup = $b2b_markup = $b2b2_markup = $b2b_agent_markup = $b2b2b_agent_markup = 0;
					$admin_base_price = $b2b_base_price = $b2b2b_base_price = $b2b_agent_base_price = $b2b2b_agent_base_price = 0;
					$total_price_with_markup = $TotalPrice_v;
					if($admin_markup_details[0]->markup_value_type == "Percentage") {
						$admin_markup = $TotalPrice_v* ($admin_markup_details[0]->markup_value / 100);
						$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
					}else{
						$admin_markup = $admin_markup_details[0]->markup_fixed;
						$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
					}
					if($b2b_markup_details[0]->markup_value_type == "Percentage") {
						$b2b_markup = $admin_base_price* ($b2b_markup_details[0]->markup_value / 100);
						$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
					} else {
						$b2b_markup = $b2b_markup_details[0]->markup_value;
						$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
					}
					if($CI->session->userdata('user_type') == 4){
						if($sub_agent_markup_details[0]->markup_value_type == "Percentage") {
						$b2b_agent_markup = $b2b_base_price* ($sub_agent_markup_details[0]->markup_value / 100);
						$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
							} else {
						$b2b_agent_markup = $sub_agent_markup_details[0]->markup_value;
						$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
					   }
					  
					  if($b2b2b_markup_details[0]->markup_value_type == "Percentage") {
						$b2b2b_agent_markup = $b2b_agent_base_price * ($b2b2b_markup_details[0]->markup_value / 100);
						$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
						} else {
						$b2b2b_agent_markup = $b2b2b_markup_details[0]->markup_value;
						$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
					  }
				   }
				   
				    $service_charge = 0;
				    $gst_charge = 0;
				    $tax_charge = 0;
				   
				    if($CI->session->userdata('user_country_id') != 0){
						
						$country_name = $CI->general_model->get_country_name($CI->session->userdata('user_country_id'))->row();
						$tax_details = $CI->general_model->get_tax_details($country_name->country_name, $CI->session->userdata('user_state_id'))->row();
					    if(count($tax_details) > 0){
						 $price_with_markup_tax = $CI->general_model->tax_calculation($tax_details, $total_price_with_markup);
						 if(count($price_with_markup_tax) > 0){
							 $service_charge = $price_with_markup_tax['sc_tax'];
							 $gst_charge = $price_with_markup_tax['gst_tax'];
							 $tax_charge = $price_with_markup_tax['state_tax'];
							  $total_price_with_markup_tax = $price_with_markup_tax['amount'];
							}
						}else{
						 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
						}
					}else{
						 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
					} 
					
					 $Netrate = $org_amt;
					 
					 $Admin_Markup =$admin_markup;
					 $My_Markup = $b2b_markup;
					 $My_Agent_Markup = $b2b_agent_markup;
					 $My_B2B2B_Markup = $b2b2b_agent_markup;
					 
					 $Admin_BasePrice =$admin_base_price;
					 $agent_baseprice = $b2b_base_price;
					 $sub_agent_baseprice = $b2b_agent_base_price;
					 $b2b2b_baseprice = $b2b2b_agent_base_price;
					 
					 $TotalPrice =$total_price_with_markup_tax;
			$final_result[$i]['amount'] = ($TotalPrice+$pay_charge) ;
		   $final_result[$i]['room_count'] = $availableRoom;
		   
		   $final_result[$i]['net_rate'] = $Netrate ;
		   $final_result[$i]['admin_markup'] = $Admin_Markup ;
		   $final_result[$i]['my_markup'] = $My_Markup ;
		   $final_result[$i]['my_agent_Markup'] = $My_Agent_Markup ;
		   $final_result[$i]['my_b2b2b_markup'] = $My_B2B2B_Markup ;
		   
		   $final_result[$i]['admin_baseprice'] = $Admin_BasePrice ;
		   $final_result[$i]['agent_baseprice'] = $agent_baseprice ;
		   $final_result[$i]['sub_agent_baseprice'] = $sub_agent_baseprice ;
		   $final_result[$i]['b2b2b_baseprice'] = $b2b2b_baseprice ;
		   
		   $final_result[$i]['service_charge'] = $service_charge ;
		   $final_result[$i]['gst_charge'] = $gst_charge ;
		   $final_result[$i]['tax_charge'] = $tax_charge ;
		   
		   
		   $final_result[$i]['currency'] = BASE_CURRENCY ;
		    $final_result[$i]['api_currency'] = $currencyv1 ;
			 $final_result[$i]['status'] = 'Available' ;
  			 $final_result[$i]['search_date'] = date('Y-m-d H:i:s') ;
			  
            $final_result[$i]['description'] = $hotel_details->ReportInfo ;
            $final_result[$i]['address'] = $hotel_details->Address;
            $final_result[$i]['star_rating'] = $hotel_details->StarRating;
			
  		   $final_result[$i]['city'] =$hotel_details->CityName;
		
			
            $final_result[$i]['lat'] = $hotel_details->Latitude;
            $final_result[$i]['lon'] = $hotel_details->Longitude;
          //  $final_result[$i]['offers'] =  count( $temp['offers'] ) > 0 ? json_encode($temp['offers']['offer']) : json_encode(array())  ;
          //  $final_result[$i]['total_room'] = count( $temp['offers']["offer"] )  ;
			$facc=array();
			$facs = $CI->hotel_model->getFacilities()->result();
			/*if($hotel_details->HotelFacilityName!='')
			{
				$facc  = explode(",",$hotel_details->HotelFacilityName);
			}*/
			if($hotel_details->RoomFacilityName!='')
			{
				$facc  = explode(",",$hotel_details->RoomFacilityName);
			}
			$olocc=array();
			if($hotel_details->LocationName!='')
			{
				$olocc  = explode(",",$hotel_details->LocationName);
			}

			if(isset($facc) && !empty($facc)){
				$Facilities = getFacilities($facc, $facs);
				$hs['Facilities'] = $Facilities;
			}else{
				$hs['Facilities'] = array();
			}
			unset($FomatFacility);
			if(isset($hs['Facilities']) && !empty($hs['Facilities'])){
				
				foreach ($hs['Facilities'] as $keyF => $Facili) {
					$FomatFacility[] = $Facili['Code'].'@'.$Facili['Facility'];
				}
			}else{
				$FomatFacility = array();
			} 
			
             $final_result[$i]['facilities'] =   json_encode($FomatFacility);
			 $final_result[$i]['locations'] =   json_encode($olocc);
				$temp[] = $itemCode;
				$ii++;
				$i++;
						}
					} 
			 
					 
					 
          
			} 
				
		  } 
				 
				
				
				return $final_result;
			
	 

}
     function getFacilities($Facilities, $array){
        $i=0;$fac = array();
        foreach ($Facilities as $key => $Facility) {
            $fac[$i]['Code'] = searchForFac($Facility, $array);
            $fac[$i]['Facility'] = $Facility;    
            $i++;
        }
        return $fac;
        //echo "<pre>"; print_r($fac);die();
    }

     function searchForFac($fac, $array) {
       foreach ($array as $key => $val) {
           if ($val->facility === $fac) {
               return $val->code;
           }
       }
       return 'CM';
    }

    function formatHotelSearchResponse_gta1($response) {
        
        $hotelsData = $response->HotelDetails;
        //echo "<pre>"; print_r($hotelsData); echo "</pre>"; die();

        $i = 0;
        foreach($hotelsData->Hotel as $hdk) {
            $hs['Hotel'][$i]['API'] = 'GTA-H';
            $hs['Hotel'][$i]['SITE_CURR'] = CURR;
            $hs['Hotel'][$i]['HasExtraInfo'] = $extraInfo = (string)$hdk['HasExtraInfo'];
            $hs['Hotel'][$i]['HasMap'] = (string)$hdk['HasMap'];
            $hs['Hotel'][$i]['HasPictures'] = (string)$hdk['HasPictures'];
            $hs['Hotel'][$i]['HasPictures'] = (string)$hdk['HasPictures'];
            $hs['Hotel'][$i]['City'] = (string)$hdk->City;
            $hs['Hotel'][$i]['CityCode'] = (string)$hdk->City['Code'];
            $hs['Hotel'][$i]['Item'] = (string)$hdk->Item;
            $hs['Hotel'][$i]['ItemCode'] = (string)$hdk->Item['Code'];

            $j=0;
            foreach($hdk->LocationDetails->Location as $lk) {
                $hs['Hotel'][$i]['LocationDetails'][$j]['LocationName'] = (string)$lk;
                $hs['Hotel'][$i]['LocationDetails'][$j]['LocationCode'] = (string)$lk['Code'];
                $j++;
            }

            $hs['Hotel'][$i]['StarRating'] = (string)$hdk->StarRating;
            
            

            // echo "<pre>"; print_r($hdk); echo "</pre>"; 
            foreach($hdk->RoomCategories as $roomCategory) {
                $c = 0;
                foreach($roomCategory as $rck) {

                    $CurrencyCode = (string)$rck->ItemPrice['Currency'];
                    $TotalFare = (string)$rck->ItemPrice;
                    $hs['Hotel'][$i]['API_CURR'] = $CurrencyCode;

                    /*Adds Markup to current item price*/
                    $TotalFare = $this->flight_model->currency_convertor($TotalFare,$CurrencyCode,CURR);
                    $TotalFare = $this->account_model->PercentageToAmount($TotalFare,$aMarkupGta);
                    $Markup = $this->account_model->PercentageAmount($TotalFare,$myMarkup);
                    $TotalFare = $this->account_model->PercentageToAmount($TotalFare,$myMarkup);
                    /*Adds Markup to current item price*/

                    $hs['Hotel'][$i]['ItemPrice'][$c] = $TotalFare;
                    $hs['Hotel'][$i]['SharingBedding'][$c] = (string)$rck->SharingBedding;
                    $c++;
                }
            
            }

            $i++; 
        }
        //die();
        // echo "<pre>"; print_r($hs); echo "</pre>"; die();
        return $hs;
    }
    
    
	function HotelPriceRecheck($hotel_code, $room_id, $user_request, $city_code) {  
		$credentials = TF_getApiCredentials();
		$user_request = json_decode(base64_decode(($user_request)));
		$request = $user_request;
		$room_count = $request->rooms;
		$CI =& get_instance();
		$CI->load->model('general_model');
		//$city_details = $CI->general_model->get_city_details_id($user_request->city); 

		//$city_code = $city_details->gta_city_code;
		//$city_code = 'MIA';

		// echo "<pre>"; print_r($user_request->request->nationality); echo "</pre>"; die();
		$nationality = 'CA';

		$room_xml_string = '';
		for($h=0; $h<$room_count; $h++) {
			$room_total_adult = $request->adult[$h];
			$room_total_child = $request->child[$h];
			$room_code = getRoomTypePrecheck($room_total_adult, $room_total_child);
			$room_xml_string .= '<Room '.$room_code.' Id="'.$room_id.'" />';
		}

		$CheckInDate = $request->hotel_checkin;
		$duration = $request->days;

	 
	$searchRequest = '';
	 $searchRequest .= '<Request>
				<Source>
					<RequestorID Client="'.$credentials->api_username1.'" EMailAddress="'.$credentials->api_username.'" Password="'.$credentials->api_password.'"/>
					<RequestorPreferences Country="CA" Currency="CAD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
					</RequestorPreferences>
				</Source>
				<RequestDetails><SearchHotelPriceRequest>
						<ItemDestination DestinationType="city" DestinationCode="'.$city_code.'" />
						<ItemCode>'.$hotel_code.'</ItemCode>
						
						<PeriodOfStay>
							<CheckInDate>'.$CheckInDate.'</CheckInDate>
							<Duration>'.$duration.'</Duration>
						</PeriodOfStay>
						<IncludeChargeConditions DateFormatResponse="true"></IncludeChargeConditions>'; 

	if(isset($request->adult)) {
		$adult = $request->adult;
		$child = $request->child;

		$searchRequest .= '<Rooms>';

		for($i=0; $i < count($adult); $i++) {

			/*echo 'Adult: '.$adult[$i];					
			echo 'Child: '.$child[$i];*/

			$adultCount = $adult[$i];
			$childCount = $child[$i];

			

			if($adultCount == 1 && $childCount == 0) {
				$searchRequest .= '<Room Code="SB" Id="'.$room_id.'" ></Room>';
			}
			else if($adultCount == 1 && $childCount == 1) {

				$childage = $request->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="TS" Id="'.$room_id.'" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
					}

				}

			}
			else if($adultCount == 1 && $childCount == 2) {
				
				$childage = $request->childAges;

				$ageString = ''; $numCots = 0;
				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$numCots++;
											
						} else {
							$ageString .= '<Age>'.$ca.'</Age>';
						}
					}
				}

				if($numCots > 0) {
					$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
				} else {
					$numberOfCotsString = '';
				}

				if($ageString != "") {    //this means that children are more than 2 yr old
					$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
				} else {
					$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" '.$numberOfCotsString.' ></Room>';
				}

			}
			else if($adultCount == 2 && $childCount == 0) {
					
				$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" NumberOfRooms="1">';
				$searchRequest .= '</Room>';

			} 
			else if($adultCount == 2 && $childCount == 1) {
				$childage = $request->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" NumberOfRooms="1">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';
						}
					}

				}
			}
			else if($adultCount == 2 && $childCount == 2) {
				$childage = $request->childAges;

				$ageString = ''; $numCots = 0;
				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$numCots++;
											
						} else {
							$ageString .= '<Age>'.$ca.'</Age>';
						}
					}
				}

				if($numCots > 0) {
					$numberOfCotsString = 'NumberOfCots="'.$numCots.'"';
				} else {
					$numberOfCotsString = '';
				}

				if($ageString != "") {    //this means that children are more than 2 yr old
					$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" '.$numberOfCotsString.' ><ExtraBeds>'.$ageString.'</ExtraBeds></Room>';
				} else {
					$searchRequest .= '<Room Code="DB" Id="'.$room_id.'" '.$numberOfCotsString.' ></Room>';
				}
			}
			else if($adultCount == 3 && $childCount == 0) {
				$searchRequest .= '<Room Id="'.$room_id.'" Code="TR">';
				$searchRequest .= '</Room>';
			}
			else if($adultCount == 3 && $childCount == 1) {
				/*
				A: 3, C: 1 - Book two rooms
				*/

				$childage = $request->childAges;

				for($j=0; $j<count($childage[$i]); $j++) {
					
					$ca = $childage[$i][$j];
					if($ca > 0) {

						if($ca == 1) {
							$searchRequest .= '<Room Id="'.$room_id.'" Code="TR" NumberOfCots="1"></Room>';				
						} else {
							$searchRequest .= '<Room Id="'.$room_id.'" Code="DB">';
							$searchRequest .= '<ExtraBeds><Age>'.$ca.'</Age></ExtraBeds>';
							$searchRequest .= '</Room>';

							$searchRequest .= '<Room Id="'.$room_id.'" Code="DB">';
							$searchRequest .= '</Room>';
						}
					}
				}
			}
			else if($adultCount == 3 && $childCount == 2) {
				$childage = $request->childAges;
				
				$ca1 = $childage[$i][0];   // Get the age of individual child
				$ca2 = $childage[$i][1];   // Get the age of individual child

				if($ca1 == 1 && $ca2 == 1) {
					$searchRequest .= '<Room Id="'.$room_id.'" Code="DB" NumberOfCots="2">';
					$searchRequest .= '</Room>';

					$searchRequest .= '<Room Id="'.$room_id.'" Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 == 1 && $ca2 > 1) {
					$searchRequest .= 	'<Room Id="'.$room_id.'" Code="DB" NumberOfCots="1">
											<ExtraBeds>
												<Age>'.$ca2.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= '<Room Id="'.$room_id.'" Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 > 1 && $ca2 == 1) {
					$searchRequest .= 	'<Room Id="'.$room_id.'" Code="DB" NumberOfCots="1">
											<ExtraBeds>
												<Age>'.$ca1.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= '<Room Id="'.$room_id.'" Code="SB">';
					$searchRequest .= '</Room>';
				}
				else if($ca1 > 1 && $ca2 > 1) {
					$searchRequest .= 	'<Room Id="'.$room_id.'" Code="TB">
											<ExtraBeds>
												<Age>'.$ca1.'</Age>
											</ExtraBeds>
										</Room>';
					$searchRequest .= 	'<Room Id="'.$room_id.'" Code="TB">
											<ExtraBeds>
												<Age>'.$ca2.'</Age>
											</ExtraBeds>
										</Room>';
				}	
			}
			else if($adultCount == 4 && $childCount == 0) {
				$searchRequest .= '<Room Id="'.$room_id.'" Code="Q">';
				$searchRequest .= '</Room>';
			}
		}

		$searchRequest .= '</Rooms>';
		$searchRequest .= 	'</SearchHotelPriceRequest></RequestDetails>
			</Request>';
	}


	$HotelReCheckRQ =  $searchRequest ;
	
	$xml_title = '/PurchaseConfirmRQRS/PurchaseConfirmRequest';
    Savelog($xml_title,$HotelReCheckRQ);

	/*	header("Content-Type: text/xml");
	echo $HotelReCheckRQ; die();*/
	$xml_type = '';
	$xml_title = 'GetHotelReCheck';
	$HotelReCheckRS = processRequest_gta($HotelReCheckRQ,$xml_type,$xml_title);
    
    $xml_title = '/PurchaseConfirmRQRS/PurchaseConfirmResponse';
    Savelog($xml_title,$HotelReCheckRS);
    
	$HotelReCheckRQ_RS = array(
		'HotelReCheckRQ' => $HotelReCheckRQ,
		'HotelReCheckRS' => $HotelReCheckRS
	);

	return $HotelReCheckRQ_RS;	
}

    function HotelBooking($cart_hotel_data,$passenger_info_c, $checkout_form, $billingaddress_c,$booking_id,$parent_pnr,$cid) {
	$credentials = TF_getApiCredentials();
	$room_description = $cart_hotel_data->room_type;

	$ExpectedPrice = $cart_hotel_data->total_cost;

	/*$checkout_form = json_encode($checkout_form);
	$checkout_form = json_decode($checkout_form);*/

	$booking_reference = $parent_pnr;
	$booking_reference = '<BookingReference>'.$booking_reference.'</BookingReference>';

	$user_request_enc = $cart_hotel_data->request;
	$user_request = json_decode(base64_decode(($user_request_enc))); 
	 	
	$CI =& get_instance();
	$CI->load->model('general_model');
	$city_details = $CI->general_model->get_city_details_id($user_request->city); 


	//~ $city_data = $city_details->gta_city_code;
	
	$city_data = 'MIA';
	
	
	$checkin_date = $user_request->hotel_checkin;
	$checkout_date = $user_request->hotel_checkout;
	$room_count = $user_request->rooms;

	$nationality = 'AU';
	$passenger_info_cs = json_decode($passenger_info_c);
	$checkout_form = json_decode(json_encode($checkout_form), true);

	$pax_id = 1;
	$pax_xml = "";
	
	$adult_prefix = 'a_gender'.$cid;
	$adult_name = 'first_name'.$cid;
	$adult_surname = 'last_name'.$cid;

	$child_prefix = 'c_gender'.$cid;
	$child_name = 'cfirst_name'.$cid;
	$child_surname = 'clast_name'.$cid;
	$child_age = 'childAges_room_'.$cid;
	
	for($r=0; $r<$room_count; $r++) { 

		if(isset($checkout_form[$adult_name][$r])) {
			$adult_count = count($checkout_form[$adult_name][$r]);
			//~ echo "data: <pre>";print_r($checkout_form[$child_name][$r]);
			
			$adult_pax_snip = "";
			
			for($a=0; $a<$adult_count; $a++) {
				
				
				$adult_pax_prefix = $checkout_form[$adult_prefix][$r][$a];
					
				//$adult_pax_prefix = call_user_func_array('array_merge', $adult_pax_prefix);
		
				$adult_pax_name = $checkout_form[$adult_name][$r][$a];
				$adult_pax_surname = $checkout_form[$adult_surname][$r][$a];

				$full_name = $adult_pax_prefix.' '.$adult_pax_name.' '.$adult_pax_surname;
				$adult_pax_snip .= '<PaxName PaxId="'.$pax_id.'" PaxType = "adult">'.$full_name.'</PaxName>';
				
				$pax_id = $pax_id+1;
			}
		}
		$child_pax_snip = ""; 
		if(isset($checkout_form[$child_age][$r]) && !empty($checkout_form[$child_age][$r])){ 
			//~ echo "Room-".$r.": <pre>";print_r($checkout_form[$child_name]);
			$child_count = count($checkout_form[$child_name][$r]);
			for($c=0; $c<$child_count; $c++){ 
				
				$pre_child_pax_prefix = $checkout_form[$child_prefix][$r];
				$child_pax_prefix = $pre_child_pax_prefix[$c];
				$pre_child_pax_name = $checkout_form[$child_name][$r];
				$child_pax_name = $pre_child_pax_name[$c];
				$pre_child_pax_surname = $checkout_form[$child_surname][$r];
				$child_pax_surname = $pre_child_pax_surname[$c];
				$pre_child_ag = $checkout_form[$child_age][$r];
				$child_ag = $pre_child_ag[$c];

				$child_full_name = $child_pax_prefix.' '.$child_pax_name.' '.$child_pax_surname;
				
				if($child_ag > 0) {
					$child_pax_snip .= '<PaxName PaxId="'.$pax_id.'" PaxType="child" ChildAge="'.$child_ag.'" >'.$child_full_name.'</PaxName>';
					$pax_id = $pax_id+1;
				}
				
			}
		} 
		$pax_xml .= $adult_pax_snip.$child_pax_snip;	
		
	}
	$adlt_pass = "";
	$child_pass = "";

	$pax_names_block = '<PaxNames>'.$pax_xml.'</PaxNames>'; //paxnames block
	$itemReference = '<ItemReference>1</ItemReference>';
	$itemCity = '<ItemCity Code="'.$cart_hotel_data->city_id.'" />';
	$itemCode = '<Item Code="'.$cart_hotel_data->hotel_code.'" />';


	$checkin = '<PeriodOfStay><CheckInDate>'.$checkin_date.'</CheckInDate>';
	$checkout = '<CheckOutDate>'.$checkout_date.'</CheckOutDate></PeriodOfStay>';

	$hotel_rooms_block = "";
	$b=1;
	$paxId_snip = "";
	$childCount = 0;

	for($h=0; $h<$room_count; $h++) { 
		
		$room_total_adult = $user_request->adult[$h];
		$room_total_child = $user_request->child[$h];
		$room_child_age = $user_request->childAges[$h];

		/*
			If 3 adult 1 child are booking, the rooms will be split into two rooms.
			If the child is an infant then the regular flow will be followed.
		*/


		if($room_total_adult == 3 && $room_total_child == 1) {
			$child_age_t = $room_child_age[0];

			if($child_age_t == 1) {
						
				$room_type_details = getRoomType($room_total_adult, $room_total_child, $room_child_age);  //get the room type parameters based on search

				$hotel_room_split = "";

				$hotel_room_split .= '<HotelRoom '.$room_type_details.' Id="'.$cart_hotel_data->option_id.'"><PaxIds>';
				


				for($a=0; $a<$user_request->adult[$h]; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}
				
				for($ca=0; $ca<count($user_request->childAges[$h]); $ca++) {
					$chi = $user_request->childAges[$h][$ca];
					if($chi > 1) {
						$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
						$b++;
					}
				}		
			
				$hotel_room_split .= '</PaxIds></HotelRoom>';


				$hotel_rooms_block .= $hotel_room_split;

			} else {
				
				$room_type_details = getRoomType($room_total_adult, $room_total_child, $room_child_age);

				$hotel_room_split = "";

				$hotel_room_split .= '<HotelRoom Code="DB" Id="'.$cart_hotel_data->option_id.'"><PaxIds>';

				for($a=0; $a<2; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}

				$hotel_room_split .= '</PaxIds></HotelRoom>';

				$hotel_room_split .= '<HotelRoom Code="DB" Id="'.$cart_hotel_data->option_id.'"><PaxIds>';

				for($a=0; $a<2; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}

				$hotel_room_split .= '</PaxIds></HotelRoom>';

				$hotel_rooms_block .= $hotel_room_split;
			}

		} 
		else if($room_total_adult == 3 && $room_total_child == 2) {
			$ca1 = $room_child_age[0];
			$ca2 = $room_child_age[1];

			if($ca1 == 1 && $ca2 == 1) {
				
				$hotel_room_split = "";
				$hotel_room_split .= '<HotelRoom Code="DB" NumberOfCots="2" Id="'.$cart_hotel_data->option_id.'"><PaxIds>';

				for($a=0; $a<2; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}
				

				$hotel_room_split .= '</PaxIds></HotelRoom>';

				$hotel_room_split .= '<HotelRoom Code="SB" Id="'.$cart_hotel_data->option_id.'"><PaxIds>';

				for($a=0; $a<1; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}

				$hotel_room_split .= '</PaxIds></HotelRoom>';

				$hotel_rooms_block .= $hotel_room_split;

			}
			else if($ca1 == 1 && $ca2 > 1) {
				
				$hotel_room_split = "";
				$hotel_room_split .= '<HotelRoom Code="DB" Id="'.$cart_hotel_data->option_id.'"><PaxIds>';

				for($a=0; $a<2; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}
				

				$hotel_room_split .= '</PaxIds></HotelRoom>';

				$hotel_room_split .= '<HotelRoom Code="DB" NumberOfCots="1" Id="'.$cart_hotel_data->option_id.'"><PaxIds>';

				for($a=0; $a<2; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}

				$hotel_room_split .= '</PaxIds></HotelRoom>';

				$hotel_rooms_block .= $hotel_room_split;

			}
			else if($ca1 > 1 && $ca2 == 1) {
				
				$hotel_room_split = "";
				$hotel_room_split .= '<HotelRoom Code="DB" Id="'.$cart_hotel_data->option_id.'"><PaxIds>';

				for($a=0; $a<2; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}
				

				$hotel_room_split .= '</PaxIds></HotelRoom>';

				$hotel_room_split .= '<HotelRoom Code="DB" NumberOfCots="1" Id="'.$cart_hotel_data->option_id.'"><PaxIds>';

				for($a=0; $a<2; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}

				$hotel_room_split .= '</PaxIds></HotelRoom>';

				$hotel_rooms_block .= $hotel_room_split;
				
			}
			else if($ca1 > 1 && $ca2 > 1) { 
				$hotel_room_split = "";
				$hotel_room_split .= '<HotelRoom Code="DB" Id="'.$cart_hotel_data->option_id.'"><PaxIds>';



				for($a=0; $a<2; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}
				
				/*$hotel_room_split .= '<PaxId>1</PaxId>';
				$hotel_room_split .= '<PaxId>2</PaxId>';
				$hotel_room_split .= '<PaxId>4</PaxId>';*/

				$hotel_room_split .= '</PaxIds></HotelRoom>';

				$hotel_room_split .= '<HotelRoom Code="DB" ExtraBed="true" NumberOfExtraBeds="1" Id="'.$cart_hotel_data->option_id.'"><PaxIds>';

				for($a=0; $a<3; $a++) {
					$hotel_room_split .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}

				/*$hotel_room_split .= '<PaxId>3</PaxId>';
				$hotel_room_split .= '<PaxId>5</PaxId>';*/

				$hotel_room_split .= '</PaxIds></HotelRoom>';

				$hotel_rooms_block .= $hotel_room_split;
			}


		} 
		else {
			$room_type_details = getRoomType($room_total_adult, $room_total_child, $room_child_age);  //get the room type parameters based on search

			$hotel_room = '<HotelRoom '.$room_type_details.' Id="'.$cart_hotel_data->token_id.'"><PaxIds>';

			for($a=0; $a<$user_request->adult[$h]; $a++) {
				$paxId_snip .= '<PaxId>'.$b.'</PaxId>';
				$b++;
			}

			

			/*for($c=0; $c<$user_request->child[$h]; $c++) {
				$paxId_snip .= '<PaxId>'.$b.'</PaxId>';
				$b++;	
			}*/

			if($user_request->child[$h] != 0){ 
			 	for($ca=0; $ca<count($user_request->childAges[$h]); $ca++) {
				$chi = $user_request->childAges[$h][$ca];
				if($chi > 1) {
					$paxId_snip .= '<PaxId>'.$b.'</PaxId>';
					$b++;
				}
			}	
			}
				
		
			$hotel_room_tail = '</PaxIds></HotelRoom>';
		
			$hotel_rooms_block .= $hotel_room.$paxId_snip.$hotel_room_tail;
		}

		

		$paxId_snip = "";  //reset for next iteration
	
	}

	
	// die();
	 $booking_request = 	 
							'<Request>
				<Source>
					<RequestorID Client="'.$credentials->api_username1.'" EMailAddress="'.$credentials->api_username.'" Password="'.$credentials->api_password.'"/>
					<RequestorPreferences Country="AU" Currency="AUD" Language="en">
						<RequestMode>SYNCHRONOUS</RequestMode>
					</RequestorPreferences>
				</Source>
				<RequestDetails><AddBookingRequest Currency="AUD">'.
								$booking_reference.
								$pax_names_block.
								'<BookingItems>'.
									'<BookingItem ItemType="hotel" ExpectedPrice = "'.$ExpectedPrice.'" >'.
										$itemReference.
										$itemCity.
										$itemCode.
										'<HotelItem>'.
											$checkin.$checkout.
											'<HotelRooms>'.
												$hotel_rooms_block.
											'</HotelRooms>'.
										'</HotelItem>'.
									'</BookingItem>'.
								'</BookingItems>'.
							'</AddBookingRequest></RequestDetails>
			</Request>'; 



	//echo "Booking Request: <pre>";print_r($booking_request);exit;
	$xml_type = '';
	$xml_title = '/ServiceAddRQRS/ServiceAddRequest';
    Savelog($xml_title,$booking_request);
	$HotelBookingRS = processRequest_gta($booking_request,$xml_type,$xml_title); 
	
	$xml_title = '/ServiceAddRQRS/ServiceAddResponse';
    Savelog($xml_title,$HotelBookingRS);
    
/*	header('Content-Type:text/xml');
echo $HotelBookingRS;exit;*/
	/*  $HotelBookingRS ='<?xml version="1.0" encoding="UTF-8"?>
<Response ResponseReference="REF_D_010_1906-3958-845974036214645" ResponseSequence="1"><ResponseDetails Language="en"><BookingResponse><BookingReferences><BookingReference ReferenceSource="client"><![CDATA[ZIXOAJMOYY0R]]></BookingReference><BookingReference ReferenceSource="api"><![CDATA[567883]]></BookingReference></BookingReferences><BookingCreationDate>2016-04-17</BookingCreationDate><BookingDepartureDate>2016-08-24</BookingDepartureDate><BookingName><![CDATA[MR ASDSA DASD]]></BookingName><BookingPrice Commission="0.00" Currency="AED" Gross="996.00" Nett="996.00"/><BookingStatus Code="C "><![CDATA[Confirmed]]></BookingStatus><PaxNames><PaxName PaxId="1"><![CDATA[Mr asdsa dasd]]></PaxName><PaxName PaxId="2"><![CDATA[Mr das asdasd]]></PaxName><PaxName ChildAge="6" PaxId="3" PaxType="child"><![CDATA[Master asda dasd]]></PaxName><PaxName PaxId="4"><![CDATA[Mr asdasdas dasdas]]></PaxName><PaxName PaxId="5"><![CDATA[Mr asdasdas dasd]]></PaxName></PaxNames><BookingItems><BookingItem ItemType="hotel"><ItemReference>1</ItemReference><ItemCity Code="LON"><![CDATA[London]]></ItemCity><Item Code="HEN2"><![CDATA[Hendon Hall]]></Item><ItemPrice Commission="0.00" Currency="AED" Gross="996.00" Nett="996.00"/><ItemStatus Code="C "><![CDATA[Confirmed]]></ItemStatus><ItemConfirmationReference>LL8F449195 - 010/567883</ItemConfirmationReference><HotelItem><PeriodOfStay><CheckInDate>2016-08-24</CheckInDate><CheckOutDate>2016-08-25</CheckOutDate></PeriodOfStay><HotelRooms><HotelRoom Code="DB" ExtraBed="true" Id="001:HEN4:9730:S9464:10852:45030" SharingBedding="false"><Description><![CDATA[Standard Room]]></Description><PaxIds><PaxId>1</PaxId><PaxId>2</PaxId><PaxId>3</PaxId></PaxIds></HotelRoom><HotelRoom Code="SB" Id="001:HEN4:9730:S9464:10852:45030"><Description><![CDATA[Standard Room]]></Description><PaxIds><PaxId>4</PaxId></PaxIds></HotelRoom><HotelRoom Code="SB" Id="001:HEN4:9730:S9464:10852:45030"><Description><![CDATA[Standard Room]]></Description><PaxIds><PaxId>5</PaxId></PaxIds></HotelRoom></HotelRooms><Meals><Basis Code="N"><![CDATA[None]]></Basis></Meals></HotelItem><ChargeConditions><ChargeCondition Type="cancellation"><Condition Charge="true" ChargeAmount="996.00" Currency="AED" FromDate="2016-08-24"/></ChargeCondition><ChargeCondition MaximumPossibleChargesShown="true" Type="amendment"><Condition Allowable="false" FromDate="2016-08-24" ToDate="2016-08-21"/><Condition Charge="false" FromDate="2016-08-20"/></ChargeCondition><PassengerNameChange Allowable="true"/></ChargeConditions></BookingItem></BookingItems></BookingResponse></ResponseDetails></Response>';
*/

	$GetHotelRequestRQ_RS = array(
		'request' => $HotelBookingRQ,
		'response' => $HotelBookingRS
		, 'xml_title'=>$xml_title, 'api_id'=>'1', 'xml_url'=>''
	);

	return $GetHotelRequestRQ_RS;
}

    function getRoomType($a, $c, $ca){

	// echo "<pre>"; print_r($ca); echo "</pre>"; die();
	if($a == 1 && $c == 0) {	
		$room_type = 'Code="SB"';	
	} 
	else if($a == 2 && $c == 0) {	
		$room_type = 'Code="DB"';	
	} 
	else if($a == 3 && $c == 0) {
		$room_type = 'Code="TR"'; 
	} 
	else if($a == 2 && $c == 1) {

		$noc=0;
		for($j=0; $j<count($ca); $j++) {
			if($ca[$j] == 1) {
				$noc++;
			}
		}
		if($noc > 0) {
			$numberOfCotsString = 'NumberOfCots="'.$noc.'"';
		} else {
			$numberOfCotsString = "";
		}

		$extraBedNeeded = $c - $noc;

		if($extraBedNeeded > 0) {
			$NumberOfExtraBedsString = 'ExtraBed="true" NumberOfExtraBeds="'.$extraBedNeeded.'"';
		} else {
			$NumberOfExtraBedsString = '';
		}

		$room_type = $NumberOfExtraBedsString.$numberOfCotsString.' Code="DB"';
	} 
	else if($a == 2 && $c == 2) {

		$noc=0;
		for($j=0; $j<count($ca); $j++) {
			if($ca[$j] == 1) {
				$noc++;
			}
		}
		if($noc > 0) {
			$numberOfCotsString = 'NumberOfCots="'.$noc.'"';
		} else {
			$numberOfCotsString = "";
		}

		/*
			Total number of child must be subtracted from the number of 
			infants to get the number of extrabeds
			$noc carries the number of infants and $c carries total number of children (including infants).
		*/

		$extraBedNeeded = $c - $noc;

		if($extraBedNeeded > 0) {
			$NumberOfExtraBedsString = 'ExtraBed="true" NumberOfExtraBeds="'.$extraBedNeeded.'"';
		} else {
			$NumberOfExtraBedsString = '';
		}

		/*
			Extrabeds are needed only when there is a child travelling else it will not be needed
		*/

		$room_type = $NumberOfExtraBedsString.' '.$numberOfCotsString.' Code="DB"';
	} 
	else if($a == 3 && $c == 1) {

		$noc=0;
		for($j=0; $j<count($ca); $j++) {
			if($ca[$j] == 1) {
				$noc++;
			}
		}
		if($noc > 0) {
			$numberOfCotsString = 'NumberOfCots="'.$noc.'"';
		} else {
			$numberOfCotsString = "";
		}

		/*
			Total number of child must be subtracted from the number of 
			infants to get the number of extrabeds
			$noc carries the number of infants and $c carries total number of children (including infants).
		*/

		$extraBedNeeded = $c - $noc;

		if($extraBedNeeded > 0) {
			$NumberOfExtraBedsString = 'ExtraBed="true" NumberOfExtraBeds="'.$extraBedNeeded.'"';
			$room_code = 'Code="TB"';
		} else {
			$NumberOfExtraBedsString = '';
			$room_code = 'Code="TR"';
		}

		/*
			Extrabeds are needed only when there is a child travelling else it will not be needed
		*/

		$room_type = $NumberOfExtraBedsString.' '.$numberOfCotsString.' '.$room_code;		
	} 
	else if($a == 1 && $c == 1) {
		
		$noc=0;
		for($j=0; $j<count($ca); $j++) {
			if($ca[$j] == 1) {
				$noc++;    //number of infant kids
			}
		}

		if($noc > 0) {
			$numberOfCotsString = 'NumberOfCots="'.$noc.'"';  //add number of cots only when kid is an infant
		} else {
			$numberOfCotsString = "";
		}

		$extraBedNeeded = $c - $noc;

		if($extraBedNeeded > 0) {
			$NumberOfExtraBedsString = '';
			$room_code = 'Code="DB"';
		} else {
			$NumberOfExtraBedsString = '';
			$room_code = 'Code="TS"';
		}

		$room_type = $NumberOfExtraBedsString.' '.$numberOfCotsString.' '.$room_code;	
	} 
	else if($a == 1 && $c == 2) {

		$noc=0;
		for($j=0; $j<count($ca); $j++) {
			if($ca[$j] == 1) {
				$noc++;
			}
		}

		if($noc > 0) {
			$numberOfCotsString = 'NumberOfCots="'.$noc.'"';
		} else {
			$numberOfCotsString = "";
		}



		/*
			Total number of child must be subtracted from the number of 
			infants to get the number of extrabeds
			$noc carries the number of infants and $c carries total number of children (including infants).
		*/

		$extraBedNeeded = $c - $noc;

		if($extraBedNeeded > 0) {
			if($extraBedNeeded == 2){
				$NumberOfExtraBedsString = 'ExtraBed="true" NumberOfExtraBeds="1"';  //extrabed is 1 because 1 bed will be shared with the adult.
				$code = 'Code="DB"';  //Double Bed for 1 adult 2 child. 1 will share with adult and 1 extra bed will be provided.
			} else {
				$NumberOfExtraBedsString = '';
				$code = 'Code="DB"';
			}

		} else {
			$NumberOfExtraBedsString = '';
			$code = 'Code="TS"';  //twin for sole purpose with two cots
		}

		/*
			Extrabeds are needed only when there is a child travelling else it will not be needed
		*/

		$room_type = $NumberOfExtraBedsString.' '.$numberOfCotsString.' '.$code;
		//$room_type = 'Code="DB" ExtraBed="true" NumberOfExtraBeds="2" '.$numberOfCotsString.' ';	
	}  
	else {	
		$room_type = "";	
	}
	return $room_type;
}

function HotelBookingCancel($parent_pnr) {
	$credentials = TF_getApiCredentials();
	$HotelCancelRQ = '<Request>
						<Source>
							<RequestorID Client="'.$credentials->api_username1.'" EMailAddress="'.$credentials->api_username.'" Password="'.$credentials->api_password.'"/>
							<RequestorPreferences Country="CA" Currency="CAD" Language="en">
								<RequestMode>SYNCHRONOUS</RequestMode>
							</RequestorPreferences>
						</Source>
						<RequestDetails>
							<CancelBookingRequest>
								<BookingReference ReferenceSource="api">'.$parent_pnr.'</BookingReference>
							</CancelBookingRequest>
						</RequestDetails>
					</Request>';

	$xml_type = '';
	$xml_title = 'CancelBookingRequest';
	$HotelCancelRS = processRequest_gta($HotelCancelRQ,$xml_type,$xml_title);
	
	$HotelCancelRQ_RS = array(
							'HotelBookingCancelRQ' => $HotelCancelRQ,
							'HotelBookingCancelRS' => $HotelCancelRS
						    );
	return $HotelCancelRQ_RS;
}

function processRequest_gta($requestData,$xmltype='',$xml_title) {
	$credentials = TF_getApiCredentials();
	$cs = curl_init();
    curl_setopt($cs, CURLOPT_URL, $credentials->api_url);
    curl_setopt($cs, CURLOPT_HEADER, false);
    curl_setopt($cs, CURLOPT_RETURNTRANSFER, true);
  	curl_setopt($cs, CURLOPT_POST, 1);
	curl_setopt($cs, CURLOPT_POSTFIELDS, $requestData);
    curl_setopt($cs, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($cs, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($cs, CURLOPT_TIMEOUT, 180);
        $httpHeader2 = array(
          'Content-type: text/xml'
        );
        curl_setopt($cs, CURLOPT_HTTPHEADER, $httpHeader2); 
        $login_response = curl_exec($cs);
        
        curl_close($cs);
        $CI =& get_instance();
        $CI->load->helper('file');
        $requestcml = $xml_title.'_request'.date('mdHis').'.xml';
        $responsecml = $xml_title.'_response'.date('mdHis').'.xml';
        //~ echo "requestData: ".$requestData;
        //~ echo "requestData: ".$login_response;exit;
        write_file('xml_logs/'.$requestcml,$requestData, 'w+');
        write_file('xml_logs/'.$responsecml,$login_response, 'w+');
        $CI->load->model('Hotel_Model');
        $CI->Hotel_Model->store_logs($requestcml,$responsecml,$credentials->api_name);
        return $login_response;
}

function TF_getApiCredentials() {
    $CI =& get_instance();
    $CI->load->model('General_Model');
	$api ='GTA';
    return $CI->General_Model->get_api_credentials($api)->row();
}

function Savelog($searchname, $result1){
	
//error_reporting(E_ALL);
	$path = $_SERVER['DOCUMENT_ROOT']; 
	$strFileName = $searchname.".xml"; 
	 $my_file = $path."/TravelLights/hotel_xmldata".$strFileName; 
	//echo $my_file;exit;
	$handle = fopen($my_file, 'w');
	//print_r($handle);exit;
	@fwrite($handle, $result1);
	@fclose($handle);
	return $strFileName;
}



/*for testing pupose*/
function debug($arr)
{
	if(is_array($arr) || is_object($arr))
	{
		echo "<pre>"; var_dump($arr);
	}
	else
	{
		echo $arr;
	}
	exit;
}
