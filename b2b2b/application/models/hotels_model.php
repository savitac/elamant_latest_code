<?php
/**
 * Library which has generic functions to get data
 *
 * @package    Provab Application
 * @subpackage Hotel Model
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class Hotels_Model extends CI_Model
{
	private $master_search_data;

	/**
	 * return top destinations in hotel
	 */
	function hotel_top_destinations()
	{
		$query = 'Select CT.*, CN.country_name AS country from all_api_city_master CT, api_country_master CN where CT.country_code=CN.iso_country_code AND top_destination = '.ACTIVE;
		$data = $this->db->query($query)->result_array();
		return $data;
	}
	/*
	 *
	 * Get Airport List
	 *
	 */
	function get_hotel_city_list($search_chars)
	{
		$raw_search_chars = $this->db->escape($search_chars);
		if(empty($search_chars)==false){
			$r_search_chars = $this->db->escape($search_chars.'%');
			$search_chars = $this->db->escape($search_chars.'%');
		}else{
			$r_search_chars = $this->db->escape($search_chars);
			$search_chars = $this->db->escape($search_chars);
		}
		
		$query = 'Select cm.country_name,cm.city_name,cm.origin,cm.country_code from all_api_city_master as cm where  cm.city_name like '.$search_chars.' or cm.country_name like '.$search_chars.' 
				ORDER BY cm.top_city_having_hotel desc, CASE
			WHEN	cm.city_name	LIKE	'.$raw_search_chars.'	THEN 1
			WHEN	cm.city_name	LIKE	'.$r_search_chars.'	THEN 2	
			WHEN	cm.city_name	LIKE	'.$search_chars.'	THEN 3
			ELSE 4 END, cm.top_city_having_hotel desc LIMIT 0, 30
		';
		/*$query='Select cm.country_name,cm.city_name,cm.origin,cm.country_code,vi.hotel_count from all_api_city_master as cm,hotel_count vi where cm.grn_city_id=vi.city_code and cm.city_name like '.$search_chars.' or cm.country_name like '.$search_chars.' ORDER BY vi.hotel_count desc, CASE WHEN cm.city_name LIKE '.$raw_search_chars.' THEN 1 WHEN cm.city_name LIKE '.$r_search_chars.' THEN 2 WHEN cm.city_name LIKE '.$search_chars.' THEN 3 ELSE 4 END, cm.cache_hotels_count desc LIMIT 0, 30';*/
		//debug($query);exit;
		//Select cm.country_name,cm.city_name,cm.origin,cm.country_code from all_api_city_master as cm where  cm.city_name like '.$search_chars.' 
				//ORDER BY cm.origin ASC, CASE
		//echo $query;exit;
		return $this->db->query($query)->result_array();
	}
	function get_hotel_city_list_base($search_chars)
	{
		$raw_search_chars = $this->db->escape($search_chars);
		$r_search_chars = $this->db->escape($search_chars.'%');
		$search_chars = $this->db->escape('%'.$search_chars.'%');
		$query = 'Select * from hotels_city where city_name like '.$search_chars.'
		OR country_name like '.$search_chars.' OR country_code like '.$search_chars.'
		ORDER BY top_destination DESC, CASE
			WHEN	city_name	LIKE	'.$raw_search_chars.'	THEN 1
			WHEN	country_name	LIKE	'.$raw_search_chars.'	THEN 2
			WHEN	country_code			LIKE	'.$raw_search_chars.'	THEN 3
			
			WHEN	city_name	LIKE	'.$r_search_chars.'	THEN 4
			WHEN	country_name	LIKE	'.$r_search_chars.'	THEN 5
			WHEN	country_code			LIKE	'.$r_search_chars.'	THEN 6
			
			WHEN	city_name	LIKE	'.$search_chars.'	THEN 7
			WHEN	country_name	LIKE	'.$search_chars.'	THEN 8
			WHEN	country_code			LIKE	'.$search_chars.'	THEN 9
			ELSE 10 END, 
			cache_hotels_count DESC
		LIMIT 0, 20';
		return $this->db->query($query)->result_array();
	}

	/**
	 * get all the booking source which are active for current domain
	 */
	function active_booking_source()
	{
		$query = 'select BS.source_id, BS.origin from meta_course_list AS MCL, booking_source AS BS, activity_source_map AS ASM WHERE
		MCL.origin=ASM.meta_course_list_fk and ASM.booking_source_fk=BS.origin and MCL.course_id='.$this->db->escape(META_ACCOMODATION_COURSE).'
		and BS.booking_engine_status='.ACTIVE.' AND MCL.status='.ACTIVE.' AND ASM.status="active"';
		return $this->db->query($query)->result_array();
	}
	/**
	 * return booking list
	 */
	function booking($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) as total_records 
					from hotel_booking_details BD
					join hotel_booking_itinerary_details AS HBID on BD.app_reference=HBID.app_reference
					join payment_option_list AS POL on BD.payment_mode=POL.payment_category_code 
					where BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$GLOBALS['CI']->entity_user_id.''.$condition;

			$data = $this->db->query($query)->row_array();

			//echo $this->db->last_query();exit;
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$cancellation_details = array();
			$bd_query = 'select * from hotel_booking_details AS BD 
						WHERE BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$GLOBALS['CI']->entity_user_id.''.$condition.'
						order by BD.origin desc limit '.$offset.', '.$limit;
			$booking_details = $this->db->query($bd_query)->result_array();
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				$id_query = 'select * from hotel_booking_itinerary_details AS ID 
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				$cd_query = 'select * from hotel_booking_pax_details AS CD 
							WHERE CD.app_reference IN ('.$app_reference_ids.')';
				$cancellation_details_query = 'select * from hotel_cancellation_details AS HCD 
							WHERE HCD.app_reference IN ('.$app_reference_ids.')';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$cancellation_details	= $this->db->query($cancellation_details_query)->result_array();
			}
			$response['data']['booking_details']			= $booking_details;
			$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			$response['data']['booking_customer_details']	= $booking_customer_details;
			$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}
	/**
	 * Return Booking Details based on the app_reference passed
	 * @param $app_reference
	 * @param $booking_source
	 * @param $booking_status
	 */
	function get_booking_details($app_reference, $booking_source, $booking_status='')
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		$bd_query = 'select * from hotel_booking_details AS BD WHERE BD.app_reference like '.$this->db->escape($app_reference);
		if (empty($booking_source) == false) {
			$bd_query .= '	AND BD.booking_source = '.$this->db->escape($booking_source);
		}
		if (empty($booking_status) == false) {
			$bd_query .= ' AND BD.status = '.$this->db->escape($booking_status);
		}
		$id_query = 'select * from hotel_booking_itinerary_details AS ID WHERE ID.app_reference='.$this->db->escape($app_reference);
		$cd_query = 'select * from hotel_booking_pax_details AS CD WHERE CD.app_reference='.$this->db->escape($app_reference);
		$cancellation_details_query = 'select HCD.* from hotel_cancellation_details AS HCD WHERE HCD.app_reference='.$this->db->escape($app_reference);
		$response['data']['booking_details']			= $this->db->query($bd_query)->result_array();
		$response['data']['booking_itinerary_details']	= $this->db->query($id_query)->result_array();
		$response['data']['booking_customer_details']	= $this->db->query($cd_query)->result_array();
		$response['data']['cancellation_details']	= $this->db->query($cancellation_details_query)->result_array();
		if (valid_array($response['data']['booking_details']) == true and valid_array($response['data']['booking_itinerary_details']) == true and valid_array($response['data']['booking_customer_details']) == true) {
			$response['status'] = SUCCESS_STATUS;
		}
		return $response;
	}

	/**
	 * get search data and validate it
	 */
	function get_safe_search_data($search_id)
	{
		$search_data = $this->get_search_data($search_id);
		$success = true;
		$clean_search = '';
		if ($search_data != false) {
			//validate
			//debug($search_data);exit;
			$temp_search_data = json_decode($search_data['search_data'], true);
			//debug($temp_search_data);exit;
			$clean_search = $this->clean_search_data($temp_search_data);
			$success = $clean_search['status'];
			$clean_search = $clean_search['data'];
		} else {
			$success = false;
		}
		return array('status' => $success, 'data' => $clean_search);
	}

	/**
	 * Clean up search data
	 */
	function clean_search_data($temp_search_data)
	{
		$success = true;
		//make sure dates are correct
		if ((strtotime($temp_search_data['hotel_checkin']) > time() && strtotime($temp_search_data['hotel_checkout']) > time()) || date('Y-m-d', strtotime($temp_search_data['hotel_checkin'])) == date('Y-m-d')) {
			//	if (strtotime($temp_search_data['hotel_checkin']) > strtotime($temp_search_data['hotel_checkout'])) {
			//Swap dates if not correctly set
			$clean_search['from_date'] = $temp_search_data['hotel_checkin'];
			$clean_search['to_date'] = $temp_search_data['hotel_checkout'];
			/*} else {
			 $clean_search['from_date'] = $temp_search_data['hotel_checkout'];
			 $clean_search['to_date'] = $temp_search_data['hotel_checkin'];
			 }*/
			$clean_search['no_of_nights'] = abs(get_date_difference($clean_search['from_date'], $clean_search['to_date']));
		} else {
			$success = false;
		}
		//city name and country name
		
		if (isset($temp_search_data['hotel_destination']) == true) {
			$clean_search['hotel_destination'] = $temp_search_data['hotel_destination'];
		}
		if (isset($temp_search_data['city']) == true) {
			$clean_search['location'] = $temp_search_data['city'];
			$temp_location = explode('(', $temp_search_data['city']);
			$clean_search['city_name'] = trim($temp_location[0]);
			if (isset($temp_location[1]) == true) {
				//Pop will get last element in the array since element patterns can repeat
				$clean_search['country_name'] = trim(array_pop($temp_location), '() ');
			} else {
				$clean_search['country_name'] = '';
			}
		} else {
			$success = false;
		}

		//Occupancy
		if (isset($temp_search_data['rooms']) == true) {
			$clean_search['room_count'] = abs($temp_search_data['rooms']);
		} else {
			$success = false;
		}
		if (isset($temp_search_data['adult']) == true) {
			$clean_search['adult_config'] = $temp_search_data['adult'];
		} else {
			$success = false;
		}

		if (isset($temp_search_data['child']) == true) {
			$clean_search['child_config'] = $temp_search_data['child'];
		}

		if (valid_array($temp_search_data['child'])) {
			foreach ($temp_search_data['child'] as $tc_k => $tc_v) {
				if (intval($tc_v) > 0) {
					$child_age_index = $tc_v;
					foreach($temp_search_data['childAge_'.($tc_k+1)] as $ic_k => $ic_v) {
						$clean_search['child_age'][] = $ic_v;
					}
				}
			}
		}
		if (strtolower($clean_search['country_name']) == 'india') {
			$clean_search['is_domestic'] = true;
		} else {
			$clean_search['is_domestic'] = false;
		}
		return array('data' => $clean_search, 'status' => $success);
	}

	/**
	 * get search data without doing any validation
	 * @param $search_id
	 */
	function get_search_data($search_id)
	{
		if (empty($this->master_search_data)) {
			$search_data = $this->custom_db->single_table_records('search_history', '*', array('search_type' => META_ACCOMODATION_COURSE, 'origin' => $search_id));
			if ($search_data['status'] == true) {
				$this->master_search_data = $search_data['data'][0];
			} else {
				return false;
			}
		}
		return $this->master_search_data;
	}

	/**
	 * get hotel city id of tbo from tbo hotel city list
	 * @param string $city	  city name for which id has to be searched
	 * @param string $country country name in which the city is present
	 */
	function tbo_hotel_city_id($city, $country)
	{
		$response['status'] = true;
		$response['data'] = array();
		$location_details = $this->custom_db->single_table_records('hotels_city', 'country_code, origin', array('city_name like' => $city, 'country_name like' => $country));
		if ($location_details['status']) {
			$response['data'] = $location_details['data'][0];
		} else {
			$response['status'] = false;
		}
		return $response;
	}

	/**
	 *
	 * @param number $domain_origin
	 * @param string $status
	 * @param string $app_reference
	 * @param string $booking_source
	 * @param string $booking_id
	 * @param string $booking_reference
	 * @param string $confirmation_reference
	 * @param number $total_fare
	 * @param number $domain_markup
	 * @param number $level_one_markup
	 * @param string $currency
	 * @param string $hotel_name
	 * @param number $star_rating
	 * @param string $hotel_code
	 * @param number $phone_number
	 * @param string $alternate_number
	 * @param string $email
	 * @param string $payment_mode
	 * @param string $attributes
	 * @param number $created_by_id
	 */
	function save_booking_details($domain_origin, $status, $app_reference, $booking_source, $booking_id, $booking_reference, $confirmation_reference,
	$hotel_name, $star_rating, $hotel_code, $phone_number, $alternate_number, $email,
	$hotel_check_in, $hotel_check_out, $payment_mode,	$attributes, $created_by_id, $transaction_currency, $currency_conversion_rate)
	{
		$data['domain_origin'] = $domain_origin;
		$data['status'] = $status;
		$data['app_reference'] = $app_reference;
		$data['booking_source'] = $booking_source;
		$data['booking_id'] = $booking_id;
		$data['booking_reference'] = $booking_reference;
		$data['confirmation_reference'] = $confirmation_reference;
		$data['hotel_name'] = $hotel_name;
		$data['star_rating'] = $star_rating;
		$data['hotel_code'] = $hotel_code;
		$data['phone_number'] = $phone_number;
		$data['alternate_number'] = $alternate_number;
		$data['email'] = $email;
		$data['hotel_check_in'] = $hotel_check_in;
		$data['hotel_check_out'] = $hotel_check_out;
		$data['payment_mode'] = $payment_mode;
		$data['attributes'] = $attributes;
		$data['created_by_id'] = $created_by_id;
		$data['created_datetime'] = date('Y-m-d H:i:s');
		
		$data['currency'] = $transaction_currency;
		$data['currency_conversion_rate'] = $currency_conversion_rate;
		
		$status = $this->custom_db->insert_record('hotel_booking_details', $data);
		return $status;
	}

	/**
	 *
	 * @param string $app_reference
	 * @param string $location
	 * @param date	 $check_in
	 * @param date	 $check_out
	 * @param string $room_type_name
	 * @param string $bed_type_code
	 * @param string $status
	 * @param string $smoking_preference
	 * @param string $attributes
	 */
	function save_booking_itinerary_details($app_reference, $location, $check_in, $check_out, $room_type_name, $bed_type_code,
	$status, $smoking_preference, $total_fare, $admin_markup, $agent_markup, $currency, $attributes,
	$RoomPrice, $Tax, $ExtraGuestCharge, $ChildCharge, $OtherCharges,
	$Discount, $ServiceTax, $AgentCommission, $AgentMarkUp, $TDS)
	{
		$data['app_reference'] = $app_reference;
		$data['location'] = $location;
		$data['check_in'] = $check_in;
		$data['check_out'] = $check_out;
		$data['room_type_name'] = $room_type_name;
		$data['bed_type_code'] = $bed_type_code;
		$data['status'] = $status;
		$data['smoking_preference'] = $smoking_preference;
		$data['total_fare'] = $total_fare;
		$data['admin_markup'] = $admin_markup;
		$data['agent_markup'] = $agent_markup;
		$data['currency'] = $currency;
		$data['attributes'] = $attributes;

		$data['RoomPrice'] = floatval($RoomPrice);
		$data['Tax'] = floatval($Tax);
		$data['ExtraGuestCharge'] = floatval($ExtraGuestCharge);
		$data['ChildCharge'] = floatval($ChildCharge);
		$data['OtherCharges'] = floatval($OtherCharges);
		$data['Discount'] = floatval($Discount);
		$data['ServiceTax'] = floatval($ServiceTax);
		$data['AgentCommission'] = floatval($AgentCommission);
		$data['AgentMarkUp'] = floatval($AgentMarkUp);
		$data['TDS'] = floatval($TDS);
		
		$status = $this->custom_db->insert_record('hotel_booking_itinerary_details', $data);
		return $status;
	}

	/**
	 *
	 * @param $app_reference
	 * @param $title
	 * @param $first_name
	 * @param $middle_name
	 * @param $last_name
	 * @param $phone
	 * @param $email
	 * @param $pax_type
	 * @param $date_of_birth
	 * @param $passenger_nationality
	 * @param $passport_number
	 * @param $passport_issuing_country
	 * @param $passport_expiry_date
	 * @param $status
	 * @param $attributes
	 */
	function save_booking_pax_details($app_reference, $title, $first_name, $middle_name, $last_name, $phone, $email, $pax_type, $date_of_birth,
	$passenger_nationality, $passport_number, $passport_issuing_country, $passport_expiry_date, $status, $attributes)
	{
		//echo $date_of_birth;
		$data['app_reference'] = $app_reference;
		$data['title'] = $title;
		$data['first_name'] = $first_name;
		$data['middle_name'] = (empty($middle_name) == true ?  $last_name: $middle_name);
		$data['last_name'] = $last_name;
		$data['phone'] = $phone;
		$data['email'] = $email;
		$data['pax_type'] = $pax_type;
		$data['date_of_birth'] = $date_of_birth;
		$data['passenger_nationality'] = $passenger_nationality;
		$data['passport_number'] = $passport_number;
		$data['passport_issuing_country'] = $passport_issuing_country;
		$data['passport_expiry_date'] = $passport_expiry_date;
		$data['status'] = $status;
		$data['attributes'] = $attributes;
		
		$status = $this->custom_db->insert_record('hotel_booking_pax_details', $data);
		return $status;
	}
	/**
	 *
	 */
	function get_static_response($token_id)
	{
		$static_response = $this->custom_db->single_table_records('test', '*', array('origin' => intval($token_id)));
		return json_decode($static_response['data'][0]['test'], true);
	}

	/**
	 * SAve search data for future use - Analytics
	 * @param array $params
	 */
	function save_search_data($search_data, $type)
	{
		$data['domain_origin'] = get_domain_auth_id();
		$data['search_type'] = $type;
		$data['created_by_id'] = intval(@$this->entity_user_id);
		$data['created_datetime'] = date('Y-m-d H:i:s');

		$temp_location = explode('(', $search_data['city']);
		$data['city'] = trim($temp_location[0]);
		if (isset($temp_location[1]) == true) {
			$data['country'] = trim($temp_location[1], '() ');
		} else {
			$data['country'] = '';
		}
		$data['check_in'] = date('Y-m-d', strtotime($search_data['hotel_checkin']));
		$data['nights'] = abs(get_date_difference($search_data['hotel_checkin'], $search_data['hotel_checkout']));
		$data['rooms'] = $search_data['rooms'];
		$data['total_pax'] = array_sum($search_data['adult']) + array_sum($search_data['child']);
		$this->custom_db->insert_record('search_hotel_history', $data);
	}
	/**
	 * Jaganath
	 * Update Cancellation details and Status
	 * @param $AppReference
	 * @param $cancellation_details
	 */
	public function update_cancellation_details($AppReference, $cancellation_details)
	{
		$AppReference = trim($AppReference);
		$booking_status = 'BOOKING_CANCELLED';
		//1. Add Cancellation details
		$this->update_cancellation_refund_details($AppReference, $cancellation_details);
		//2. Update Master Booking Status
		$this->custom_db->update_record('hotel_booking_details', array('status' => $booking_status), array('app_reference' => $AppReference));//later
		//3.Update Itinerary Status
		$this->custom_db->update_record('hotel_booking_itinerary_details', array('status' => $booking_status), array('app_reference' => $AppReference));//later
	}
	/**
	 * Add Cancellation details
	 * @param unknown_type $AppReference
	 * @param unknown_type $cancellation_details
	 */
	private function update_cancellation_refund_details($AppReference, $cancellation_details)
	{
		$hotel_cancellation_details = array();
		$hotel_cancellation_details['app_reference'] = 				$AppReference;
		$hotel_cancellation_details['ChangeRequestId'] = 			$cancellation_details['ChangeRequestId'];
		$hotel_cancellation_details['ChangeRequestStatus'] = 		$cancellation_details['ChangeRequestStatus'];
		$hotel_cancellation_details['status_description'] = 		$cancellation_details['StatusDescription'];
		$hotel_cancellation_details['API_RefundedAmount'] = 		@$cancellation_details['RefundedAmount'];
		$hotel_cancellation_details['API_CancellationCharge'] = 	@$cancellation_details['CancellationCharge'];
		if($cancellation_details['ChangeRequestStatus'] == 3){
			$hotel_cancellation_details['cancellation_processed_on'] =	date('Y-m-d H:i:s');
		}
		$cancel_details_exists = $this->custom_db->single_table_records('hotel_cancellation_details', '*', array('app_reference' => $AppReference));
		if($cancel_details_exists['status'] == true) {
			//Update the Data
			unset($hotel_cancellation_details['app_reference']);
			$this->custom_db->update_record('hotel_cancellation_details', $hotel_cancellation_details, array('app_reference' => $AppReference));
		} else {
			//Insert Data
			$hotel_cancellation_details['created_by_id'] = 				(int)@$this->entity_user_id;
			$hotel_cancellation_details['created_datetime'] = 			date('Y-m-d H:i:s');
			$data['cancellation_requested_on'] = date('Y-m-d H:i:s');
			$this->custom_db->insert_record('hotel_cancellation_details',$hotel_cancellation_details);
		}
	}
	/**
	*Image masking
	*/
	function setImgDownload($imagePath){
		$image = imagecreatefromjpeg($imagePath);
	    header('Content-Type: image/jpeg');
	    imagejpeg($image);
	}
    function add_hotel_images($sid,$HotelPicture,$HotelCode) {
         
        $image_url= $this->custom_db->single_table_records('hotel_image_url','image_url',array('hotel_code'=>$HotelCode));            
     
        if($image_url['status']==0) {
            foreach($HotelPicture as $key=>$value) {
			$data['image_url'] = $value;
			$data['ResultIndex'] = $key;
	                $data['hotel_code'] = $HotelCode;
			$this->custom_db->insert_record('hotel_image_url', $data);
            }
        }
    }

     function getCancellationDetails($hotel_id){
     	$this->db->select('*');
     	$this->db->from('hotel_cancellation');
     	$this->db->where('hotel_details_id',$hotel_id);
     	$query = $this->db->get();
     	if($query->num_rows > 0){
     		return $query->result();
     	}else{
     		return false;
     	}
     }//Hotel-CRS
	function get_crs_search_data($datetime1, $datetime2,$stay_days,$s_max_adult,$s_max_child,$checkin_date,$checkout_date,$room_count,$city_id){
		 $q2 = "
                SELECT 
                    hd.hotel_details_id,hd.country_id,hd.city_details_id,hd.hotel_type_id,hd.hotel_address,
                    hd.hotel_name,hd.hotel_code,hd.hotel_price,currency_type,hd.min_stay_day,hd.hotel_amenities,hd.latitude,hd.longitude,
                    hd.max_stay_day,hd.star_rating,hd.hotel_info,hd.thumb_image,
                    hd.hotel_images,hd.hotel_image_url,
                    hd.child_group_a,hd.child_group_b,hd.child_group_c,hd.child_group_d,hd.child_group_e

                FROM 
                    hotel_details hd 
                WHERE 
                    hd.city_details_id like '%".$city_id."%'
                AND hd.status='ACTIVE' 
                AND hd.hotel_type_id != " . VILLA . "
                AND hd.contract_expire_date >= CURDATE()
                AND hd.hotel_details_id IN (
                    SELECT  
                        DISTINCT hotel_details_id 
                    FROM
                        `seasons_details` hsd 
                    WHERE 
                        hotel_details_id = hd.hotel_details_id 
                   
                     AND 
                        '".$checkin_date."' >= hsd.seasons_from_date AND '".$checkout_date."' <= hsd.seasons_to_date
                     AND '".$checkin_date."' >= '".Date('Y-m-d', strtotime("+3 days"))."'
                     AND 
                        ".$stay_days." >= hsd.minimum_stays AND hsd.status='ACTIVE'
                     AND hotel_room_type_id IN  
                        (
                            SELECT 
                                DISTINCT hrci.hotel_room_type_id 
                            FROM 
                                hotel_room_type ht 
                            LEFT JOIN
                                hotel_season_room_count_info hrci 
                                ON (
                                        (
                                            ht.hotel_room_type_id = hrci.hotel_room_type_id
                                        )
                                    AND 
                                        ht.hotel_details_id = hrci.hotel_details_id
                                   )
                            LEFT JOIN
                                 hotel_room_rate_info rate
                                 ON (
                                        rate.seasons_details_id = hrci.hotel_season_id
                                        AND 
                                        rate.hotel_details_id = hrci.hotel_details_id
                                        AND 
                                        rate.hotel_room_type_id = hrci.hotel_room_type_id
                                    )
                            WHERE 
                                ht.status='ACTIVE'
                            AND
                                hrci.status='ACTIVE' 
                            AND 
                                rate.hotel_room_rate_info_id IS NOT NULL    
                            AND
                                ht.hotel_details_id = hd.hotel_details_id
                            AND 
                                hrci.hotel_room_type_id IS NOT NULL
                            AND 
                                rate.seasons_details_id IS NOT NULL
                            AND 
                                (
                                    hrci.no_of_room_available > hrci.no_of_room_booked AND  (hrci.no_of_room_available - hrci.no_of_room_booked >= ".$room_count.")
                                )
                            
                            AND 
                                (  rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<=ht.adult AND ".$s_max_child."<=ht.child)
                                OR
                                (  rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<= ht.max_pax AND ".$s_max_child."<=ht.child AND ht.extra_bed ='Available')
                                OR
                                (  rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<= ht.max_pax AND ".$s_max_child."<=ht.child + 1 AND ht.extra_bed ='Available')
                                OR
                                (   rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<= ht.adult AND ".$s_max_child."<=ht.child + 1 AND ht.extra_bed ='Available')
                        )
                )
                ORDER BY hd.creation_date DESC";
               //echo $q2;exit;
        return $this->db->query($q2);
	}

	 public function gets_crs_topPrice_forSearch($hotel_id,$hotel_room_type_id='')
   { 
        $q = "SELECT hri.*,hr.*,ht.* FROM hotel_room_rate_info hri 
              LEFT JOIN hotel_room_rate hr ON (hri.hotel_room_rate_info_id=hr.hotel_rome_rate_info_id)
              LEFT JOIN hotel_room_type ht ON (hri.hotel_room_type_id = ht.hotel_room_type_id)
              WHERE hri.hotel_details_id=".$hotel_id." AND hri.roomrate_status='ACTIVE'";
       
        /*if($seasons_details_id!='')
        {
            $q.=" AND hri.seasons_details_id=".$seasons_details_id; 
        }*/
        
        if($hotel_room_type_id!='')
        {
            $q.=" AND hri.hotel_room_type_id=".$hotel_room_type_id;  
        }  
        $q.=" AND hri.seasons_details_id IS NOT NULL AND hr.hotel_room_rate_id IS NOT NULL";
        $q.=" ORDER BY hr.single_room_price,hr.weekend_single_room_price,hr.double_room_price,hr.weekend_double_room_price,hr.triple_room_price,hr.weekend_triple_room_price,hr.quad_room_price,hr.weekend_quad_room_price,hr.hex_room_price,hr.weekend_hex_room_price LIMIT 1";
         //print_r($q);exit;
        return $this->db->query($q);
   }

   public function get_roomType_data($room_type_id,$hotel_details_id)
   {
        $q="SELECT * FROM hotel_room_type WHERE status='ACTIVE' AND hotel_details_id=".$hotel_details_id." AND hotel_room_type_id=".$room_type_id." ORDER BY hotel_room_type_id DESC LIMIT 1";
        //echo $q;exit;
        return $this->db->query($q);
   }

   function get_general_settings($id){
        $this->db->select('*');
        $this->db->where('hotel_details_id',$id);
        return $this->db->get('hotel_details');
        //echo $this->db->last_query(); exit();
        /*if($query->num_rows() > 0){
            return $query->row();
        }else{
            
        }*/
    }

    public function get_crsHotels_byHotelId($hotel_id)
    {
        $q = "SELECT h.*,hc.city_name,hc.country_name FROM hotel_details h INNER JOIN api_hotel_city_list as hc ON (hc.city_name = h.city_details_id) WHERE h.hotel_details_id=".$hotel_id." AND h.status='ACTIVE' AND h.contract_expire_date >= CURDATE() ORDER BY h.creation_date DESC";
        //echo $q; exit;
        return $this->db->query($q);
    }

    public function get_crs_allRooms($hotel_id,$GET)
   {    
        $adult_count = max($GET['data']['adult_config']);
        $children_count = max($GET['data']['child_config']);
        $total_pax = $adult_count + $children_count;
        $checkin = explode('/', $GET['data']['from_date']);
        $checkin = $checkin[2].'-'.$checkin[1].'-'.$checkin[0];
        $datetime1 = new DateTime($checkin);
        $checkout = explode('/', $GET['data']['to_date']);
        $checkout = $checkout[2].'-'.$checkout[1].'-'.$checkout[0];
        $datetime2 = new DateTime($checkout); 
        //$oDiff = $datetime1->diff($datetime2);
        $stay_days = $GET['data']['no_of_nights'];
        $checkin_date = date('Y-m-d',strtotime($checkin));
        $checkout_date = date('Y-m-d',strtotime($checkout));
        $room_count = $GET['data']['room_count'];


         $q="
            SELECT 
                sd.*,h.* 
            FROM  
                seasons_details sd
            LEFT JOIN 
                hotel_room_type h ON (sd.hotel_room_type_id=h.hotel_room_type_id)
            WHERE 
                sd.hotel_details_id=".$hotel_id." 
            AND sd.status='ACTIVE' 
            AND '".$checkin_date."' >= sd.seasons_from_date 
            AND '".$checkout_date."' <= sd.seasons_to_date 
            AND ".$stay_days." >= sd.minimum_stays
            AND h.status='ACTIVE' 
            
            AND 
                h.hotel_details_id=".$hotel_id."
            ORDER BY 
                sd.seasons_from_date";
               // echo $q; exit("hotel_model");
         return $this->db->query($q);
   }

    function getAvailableRoomsHotel($checkin,$checkout,$roomid,$season){
       
        //$sql = "select sum(`no_of_room_available`) as rooms from hotel_room_count_info where `hotel_details_id` = $hotelid";

          $sql = "select sum(confirmed_room_count) as 'rooms_booked_count' from booking_global,booking_hotel, hotel_season_room_count_info
               where  booking_hotel.hotel_room_type_id = hotel_season_room_count_info.hotel_room_type_id 
            and booking_global.ref_id = booking_hotel.id and booking_global.booking_status = 'CONFIRM' 
            and booking_hotel.actual_check_in_date <='$checkin' and  booking_hotel.actual_check_out_date >= '$checkout' 
            and hotel_season_room_count_info.hotel_room_type_id  = '$roomid' and hotel_season_room_count_info.hotel_season_id = '$season' ";
        /*$query =  $this->db->query($sql);
        //echo $this->db->last_query(); exit();
         if($query->num_rows() > 0){
            return $query->row();
        }else{
            return 0;
        }*/
        return $this->db->query($sql);
       
    }

    function getAvailableRooms($checkin,$checkout,$roomid,$season){
       
        //$sql = "select sum(`no_of_room_available`) as rooms from hotel_room_count_info where `hotel_details_id` = $hotelid";

          $sql = "select sum(confirmed_room_count) as 'rooms_booked_count' from booking_global,booking_hotel, hotel_season_room_count_info
               where  booking_hotel.hotel_room_type_id = hotel_season_room_count_info.hotel_room_type_id 
            and booking_global.ref_id = booking_hotel.id and booking_global.booking_status = 'CONFIRM' 
            and booking_hotel.actual_check_in_date <='$checkin' and  booking_hotel.actual_check_out_date >= '$checkout' 
            and hotel_season_room_count_info.hotel_room_type_id  = '$roomid' and hotel_season_room_count_info.hotel_season_id = '$season' ";
       
        $query =  $this->db->query($sql);
        //echo $this->db->last_query(); exit();
         if($query->num_rows() > 0){
            return $query->row();
        }else{
            return 0;
        }
       
    }

    public function getBookedRooms($roomid,$season){
        $this->db->select('*');
        $this->db->from('hotel_season_room_count_info');
        $this->db->where('hotel_room_type_id',$roomid);
        $this->db->where('hotel_season_id',$season);
        $query = $this->db->get();
       // echo $this->db->last_query(); 
        if($query->num_rows > 0){
            return $query->row();
        }else{
            return false;
        }

    }

    public function get_crs_topPrice_room($hotel_id,$seasons_details_id='',$hotel_room_type_id='')
   { 
        $q = "SELECT hri.*,hr.* FROM hotel_room_rate_info hri 
              LEFT JOIN hotel_room_rate hr ON (hri.hotel_room_rate_info_id=hr.hotel_rome_rate_info_id)
              WHERE hri.hotel_details_id=".$hotel_id." AND hri.roomrate_status='ACTIVE'";
       
        if($seasons_details_id!='')
        {
            $q.=" AND hri.seasons_details_id=".$seasons_details_id; 
        }
        
        if($hotel_room_type_id!='')
        {
            $q.=" AND hri.hotel_room_type_id=".$hotel_room_type_id;  
        }  
        $q.=" LIMIT 1";
        return $this->db->query($q);
   }

    public function get_crs_topPrice($hotel_id,$seasons_details_id='',$hotel_room_type_id='')
   { //echo $hotel_id."---".$seasons_details_id."--".$hotel_room_type_id;exit;
        $q = "SELECT hri.*,hr.* FROM hotel_room_rate_info hri 
              LEFT JOIN hotel_room_rate hr ON (hri.hotel_room_rate_info_id=hr.hotel_rome_rate_info_id)
              WHERE hri.hotel_details_id=".$hotel_id." AND hri.roomrate_status='ACTIVE'";
       
        if($seasons_details_id!='')
        {
            $q.=" AND hri.seasons_details_id=".$seasons_details_id; 
        }
        
        if($hotel_room_type_id!='')
        {
            $q.=" AND hri.hotel_room_type_id=".$hotel_room_type_id;  
        }  
        $q.=" LIMIT 1";
        //echo $q;exit;
        return $this->db->query($q);
   }

   public function get_hotel_room_details($hotel_details_id,$hotel_room_type_id)
   {
        $q="SELECT * FROM hotel_room_details WHERE hotel_details_id=".$hotel_details_id." AND hotel_room_type_id=".$hotel_room_type_id;
        return $this->db->query($q);
   }

   public function get_hotel_amenities_byamenityId($amenityId)
   {
        $q="SELECT * FROM hotel_amenities WHERE hotel_amenities_id=".$amenityId;
        return $this->db->query($q);
   }

   public function get_hotel_amenities_name_byamenityId($amenityId)
   {
        $q="SELECT amenities_name FROM hotel_amenities WHERE hotel_amenities_id=".$amenityId;
        return $this->db->query($q);
   }

   public function get_room_details($hotelid,$roomid){ 
   	$q="SELECT * FROM hotel_room_type WHERE hotel_details_id=".$hotelid." AND hotel_room_type_id=".$roomid;
   	// print_r($q); exit();
        return $this->db->query($q);
   }

   public function insert_globel_data($post_params,$search_id,$book_id,$user_id,$temp_booking,$valid_temp_token,$safe_search_data){

       //echo '<pre>sajay'; print_r($valid_temp_token['price']); exit();
        
        //16-10-18 added for promocode

        // if(isset($valid_temp_token['promocode_value']) && $valid_temp_token['promocode_value'] != 0){
        // $valid_temp_token['price'] = $valid_temp_token['price'] - $valid_temp_token['promocode_value'];	
        // }
		//
        $booking_status     = 'PROCESS';
        
         
        $contact_email_id   = $post_params['booking_email_id'];
        $room_type_data     = $this->get_room_type_data($valid_temp_token['HotelCode'],$valid_temp_token['room_id']);
         
        $room_type_data     = ($room_type_data->num_rows()>=1) ? $room_type_data->result_array()[0] : false;

        $hotel_rooms_count_info     = $this->get_hotel_rooms_count_info($valid_temp_token['HotelCode'],$valid_temp_token['room_id'],$valid_temp_token['season_id'])->row_array();

        $hotel_room_count_info_id   = intval($hotel_rooms_count_info['hotel_season_room_count_info_id']);

        // debug($post_params);
        // exit("858");
       
            $booking_hotel = array(
                                'booking_hotel_code'                => '',
                                'request'                           => json_encode($valid_temp_token),
                                'response'                          => '',
                                'cart_data'                         => base64_encode(json_encode($post_params)),
                                'hotel_name'                        => $valid_temp_token['HotelName'],
                                'hotel_data'                        => '',
                                'book_no'                           => '0',
                                'bh_booking_status'                 => $booking_status,
                                'nationality'                       => 'India',
                                'bh_parent_pnr'                        => '',
                                'parent_booking_number'             => '',
                                'ip_address'                        => $this->input->ip_address(),
                                'actual_check_in_date'              => date('Y-m-d',strtotime($safe_search_data['data']['from_date'])),
                                'actual_check_out_date'             => date('Y-m-d',strtotime($safe_search_data['data']['to_date'])),
                                'check_in_date'                     => $safe_search_data['data']['from_date'],
                                'check_out_date'                    => $safe_search_data['data']['to_date'],
                                'contact_email'                     => $post_params['billing_email'],
                                'contact_fname'                     => $post_params['first_name'][0],
                                'contact_mname'                     => '',
                                'contact_sur_name'                  => $post_params['last_name'][0],
                                'contact_company_name'              => '',
                                'contact_address_line1'             => $post_params['billing_address_1'],
                                'contact_country_mobile_code'       => '',
                                'contact_mobile_number'             => $post_params['passenger_contact'],
                                'contact_country'                   => '',
                                'visit_type'                        => '',
                                'passenger_details'                 => json_encode($post_params),
                                'user_details_id'                   => $user_id,
                                //'user_type'                         => '',
                                'book_date'                         => date('y-m-d h:i:s'),
                                'api_temp_hotel_id'                 => '',
                                'shopping_cart_hotel_id'            => '',
                                'session_id'                        => $book_id,
                                'api'                               => 'CRS',
                                'hotel_code'                        => $valid_temp_token['HotelCode'],
                                'room_code'                         => $valid_temp_token['room_id'],
                                'hotel_room_details_id'             => $valid_temp_token['room_id'],
                                'hotel_details_id'                  => $valid_temp_token['HotelCode'],
                                'hotel_room_type_id'                => $valid_temp_token['room_id'],
                                'confirmed_room_count'              => $safe_search_data['data']['room_count'],
                                'onrequest_room_count'              => $safe_search_data['data']['room_count'],
                                'room_name'                         => ($room_type_data == false) ? '' : $room_type_data['room_type_name'],
                                'room_info'                         => '',
                                'hotel_room_count_info_id'          => $hotel_room_count_info_id,
                                'room_count'                        => $safe_search_data['data']['room_count'],
                                'cancellation_policy'               => '',
                                'room_amenities'                    => '',
                                'room_type_name'                    => ($room_type_data == false) ? '' : $room_type_data['room_type_name'],
                                'room_type_description'             => '',
                                'adult'                             => max($safe_search_data['data']['adult_config']),
                                'child'                             => max($safe_search_data['data']['child_config']),
                                'max_pax'                           => ($room_type_data == false) ? '' : $room_type_data['max_pax'],
                                'extra_bed'                         => '',
                                'extra_bed_count'                   => '',
                                'hotel_room_rate_info_id'           => '',
                                'tax_rate_info_id'                  => '',
                                'TotalPrice'                        => $valid_temp_token['price'],
                                'breakfast_price'                   => '',
                                'lunch_price'                       => '',
                                'dinner_price'                      => '',
                                'breakfast_price_flag'              => '0',
                                'lunch_price_flag'                  => '0',
                                'dinner_price_flag'                 => '0',
                                'adult_price'                       => '',
                                'child_price_a'                     => '',
                                'child_price_b'                     => '',
                                'child_price_c'                     => '',
                                'child_price_d'                     => '',
                                'child_price_e'                     => '',
                                'sgl_price'                         => '',
                                'dbl_price'                         => '',
                                'tpl_price'                         => '',
                                'quad_price'                        => '',
                                'hex_price'                         => '',
                                'extra_bed_price'                   => '',
                                'extra_bed_price_total'             => '',
                                'ad_markup'                         => '',
                                'cancel_policy'                     => '',
                                'cancel_till_date'                  => '',
                                'cancel_amount'                     => '',
                                'purchase_token'                    => '',
                                'service_val'                       => '',
                                'cancel_date_from'                  => '',
                                'cancel_date_to'                    => '',
                                'comment_remarks'                   => '',
                                'special_request'                   => '',
                                'transfer_details'                  => '',
                                'flight_details'                    => '',
                                'arr_flight_name'                   => '',
                                'arr_flight_no'                     => '',
                                'arr_flight_date'                   => '',
                                'arr_flight_time'                   => '',
                                'dpt_flight_name'                   => '',
                                'dpt_flight_no'                     => '',
                                'dpt_flight_date'                   => '',
                                'dpt_flight_time'                   => '',
                                'child_age'                         => '',
                                'booking_secret_key'                => $book_id,
                                'booking_challenge_key'             => $search_id,
                                'amended_date'                      => '',
                                'service_charge'                    =>'',
                                'payment_type'                      => '',
                                'unit_type'							=> $post_params['unit_type'],
                                'street_type'						=> $post_params['street_type'],
                                'HotelAddress'                      => $valid_temp_token['HotelAddress']
                                                    );
            

                        $this->db->insert('booking_hotel',$booking_hotel);
                        return $this->db->insert_id();
    }

    public function get_room_type_data($hotel_details_id,$hotel_room_type_id)
   {
        $q="SELECT * FROM hotel_room_type WHERE status='ACTIVE' AND hotel_room_type_id=".$hotel_room_type_id." AND hotel_details_id=".$hotel_details_id." ORDER BY hotel_room_type_id DESC LIMIT 1";
        return $this->db->query($q);
   }

   public function get_hotel_rooms_count_info($hotel_details_id,$room_type_id,$seasons_details_id)
   {
   $q=" SELECT ht.*,
        hi.hotel_season_room_count_info_id,hi.hotel_season_id,hi.hotel_room_count_info_id,
        hi.no_of_room,hi.no_of_room_available,hi.no_of_room_booked
        FROM 
            hotel_room_type ht
        LEFT JOIN 
            hotel_season_room_count_info hi ON(ht.hotel_room_type_id=hi.hotel_room_type_id) 
        WHERE 
            ht.hotel_room_type_id=".$room_type_id." AND ht.hotel_details_id=".$hotel_details_id."
        AND 
            hi.hotel_details_id=".$hotel_details_id." AND hi.hotel_details_id=".$hotel_details_id."
        AND 
            ht.status='ACTIVE' AND hi.status='ACTIVE' AND hi.hotel_season_id=".$seasons_details_id."
      ";

      //echo $q; exit();
      return $this->db->query($q);

   }

    public function hotel_add_bookingGlobalData($insert)
    {
        if($this->db->insert('booking_global', $insert)){
            return $this->db->insert_id() ;
        } else {
            return 0;
        }
    }

     public function Update_Booking_Global($booking_temp_id, $update_booking, $module){
        $this->db->where('id',$booking_temp_id);
        $this->db->where('module',$module);
        $this->db->update('booking_global', $update_booking);
    }

    public function getBookingDetails($parent_pnr){
        $this->db->select('*');
        $this->db->where('parent_pnr',$parent_pnr);
        $query = $this->db->get('booking_global');
        if($query->num_rows() > 0){
        return $query->result();
        }
    }

      public function getHotelDetailsId($id){
        $this->db->select('hotel_details_id,ref_id');
        $this->db->from('booking_global');
        $this->db->where('id',$id);
        $this->db->where('module','HOTEL');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function getHotelRoomCount($id){
        $sql    = "select room_count,hotel_room_type_id,hotel_details_id from booking_hotel where id ='$id'";
        $query  = $this->db->query($sql);
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return 0;
        }
    }

    public function check_room_count_details($room_count,$hotel_room_type_id,$hotel_details_id){
        $sql = "SELECT * FROM hotel_season_room_count_info WHERE (hotel_room_type_id = '$hotel_room_type_id' AND hotel_details_id = '$hotel_details_id') ";
        $query  = $this->db->query($sql);
        //echo $this->db->last_query();exit;
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return 0;
        }
    }

    public function update_hotel_room_count_info($room_count,$hotel_room_type_id,$hotel_details_id){
        $sql = "UPDATE hotel_season_room_count_info SET no_of_room_booked = '$room_count' WHERE(hotel_room_type_id = '$hotel_room_type_id' AND hotel_details_id = '$hotel_details_id')";
      $this->db->query($sql);
    }

    public function update_globel_booking_status($id,$data){
        $this->db->set($data);
        $this->db->where('id',$id);
        $this->db->update('booking_global');
    }

    public function get_bookingHotel($id,$module){
        //echo $id;exit;
        $q = 'SELECT * FROM booking_hotel bh LEFT JOIN booking_global bg ON(bh.id=bg.ref_id) WHERE bg.id=\''.$id.'\'';
        
        if($module!='')
        {
           $q.=' AND bg.module=\''.$module.'\'';
        } //echo $q;exit;
        return $this->db->query($q);
    }

    function get_voucher_details($parent_pnr){
        $this->db->select('*');
        $this->db->from('booking_global');
        $this->db->join('booking_hotel', 'booking_hotel.id = booking_global.ref_id');
        $this->db->where('booking_global.parent_pnr',$parent_pnr);
        $this->db->where('booking_global.module','HOTEL');
        return $this->db->get()->result();
        //$this->db->where('')
    }
    	/**Promo code checking ***/
	function get_promo($promo){
		$query = "SELECT * FROM `promo_code_list` WHERE `promo_code` = '$promo' ";
		return($this->db->query($query));
	}
     
    public function booking_crs_details($condition){
     	$user_id  = $this->entity_user_id;
		$this->db->select('bg.*,bh.*');
        $this->db->from('booking_global bg');
        $this->db->join('booking_hotel bh','bg.ref_id=bh.id');
        $this->db->where('bg.user_id',$user_id);
        $this->db->where('bg.module','HOTEL');
        if(!empty($condition)){
        	$booking_reference = $condition['app_reference']; 
        	 $this->db->where('bg.parent_pnr',$booking_reference);
        }

        $query = $this->db->get();

            $new_array = ['status'=>0,'data'=>''];
        //echo $this->db->last_query();exit;
        if($query->num_rows() > 0){
        	$new_array2 = [];
            $response_array =  $query->result_array();
            $i=0;$j=0;
					
            foreach($response_array as $key => $val){
            	$new_array = ['status'=>1];
            	$request = json_decode($val['request'],true);
            	//debug($val);exit;
            	$passenger_details = json_decode($val['passenger_details'],true);
            	$billing_country  = $this->get_country_name( $passenger_details['billing_country']);

            	$hotel_id  = $passenger_details['hotel_id'];
            	$hotel_image = $this->get_hotel_crs_image($hotel_id);
            	$hotel_image = base_url().'/supervision/uploads/hotel_images/'.$hotel_image;

            	/*booking_details*/
				$new_array2['booking_details'][$i]['origin'] = $val['id'];
            	$new_array2['booking_details'][$i]['domain_origin'] = 1;
            	$new_array2['booking_details'][$i]['status'] = $val['booking_status'];
            	$new_array2['booking_details'][$i]['app_reference'] = $val['parent_pnr'];
            	$new_array2['booking_details'][$i]['booking_source'] = PROVAB_HOTEL_CRS_;
            	$new_array2['booking_details'][$i]['booking_id'] =  '';
            	$new_array2['booking_details'][$i]['booking_reference'] = $val['parent_pnr'];
            	$new_array2['booking_details'][$i]['confirmation_reference'] = $val['parent_pnr'];
            	$new_array2['booking_details'][$i]['hotel_name'] = $val['hotel_name'];
                $new_array2['booking_details'][$i]['star_rating'] = $request['StarRating']; 
                $new_array2['booking_details'][$i]['hotel_code'] = $val['hotel_code'];
                $new_array2['booking_details'][$i]['phone_number'] = $passenger_details['passenger_contact'];
                $new_array2['booking_details'][$i]['alternate_number'] = 'NA';
                $new_array2['booking_details'][$i]['email'] = $val['email_id'];
                $new_array2['booking_details'][$i]['hotel_check_in'] =  date('Y-m-d',strtotime($val['check_in_date']));
                $new_array2['booking_details'][$i]['hotel_check_out'] = date('Y-m-d',strtotime($val['check_out_date']));
                $new_array2['booking_details'][$i]['payment_mode'] = $val['payment_method'];
                $new_array2['booking_details'][$i]['convinence_value'] = $request['convenience_fees'];
                $new_array2['booking_details'][$i]['convinence_value_type'] = 'fixed';
                $new_array2['booking_details'][$i]['convinence_per_pax'] = 1;
                $new_array2['booking_details'][$i]['convinence_amount'] = $request['convenience_fees'];
                $new_array2['booking_details'][$i]['discount'] = 0;
                $new_array2['booking_details'][$i]['currency'] = $request['default_currency'];
                $new_array2['booking_details'][$i]['currency_conversion_rate'] = 1;
                $new_array2['booking_details'][$i]['attributes'] = json_encode(array("address"=>$passenger_details['billing_address_1'],"billing_country"=>$billing_country,'billing_city'=>$passenger_details['billing_city'],'billing_zipcode'=>$passenger_details['billing_zipcode'],'HotelCode'=>$request['HotelCode'],'search_id'=>$request['search_id'],'TraceId'=>$request['TraceId'],'HotelName'=>$request['HotelName'],'StarRating'=>$request['StarRating'],'HotelImage'=>$hotel_image,'HotelAddress'=>$request['HotelAddress'],'CancellationPolicy'=>$request['CancellationPolicy']));
                $new_array2['booking_details'][$i]['created_by_id'] = $val['user_id'];
                $new_array2['booking_details'][$i]['created_datetime'] = $val['voucher_date'];

                /*booking_itinerary_details*/
				$new_array2['booking_itinerary_details'][$i]['origin'] = $val['id'];
				$new_array2['booking_itinerary_details'][$i]['app_reference'] = $val['parent_pnr'];
				$new_array2['booking_itinerary_details'][$i]['location'] = '';
				$new_array2['booking_itinerary_details'][$i]['check_in'] = date('Y-m-d H:i:s',strtotime($val['check_in_date']));
				$new_array2['booking_itinerary_details'][$i]['check_out'] = date('Y-m-d H:i:s',strtotime($val['check_out_date']));
				$new_array2['booking_itinerary_details'][$i]['room_type_name'] = $val['room_type_name'];
				$new_array2['booking_itinerary_details'][$i]['bed_type_code'] = '';
				$new_array2['booking_itinerary_details'][$i]['smoking_preference'] = 0;
				$new_array2['booking_itinerary_details'][$i]['total_fare'] = $val['TotalPrice'];
				$new_array2['booking_itinerary_details'][$i]['admin_markup'] = 0;
				$new_array2['booking_itinerary_details'][$i]['agent_markup'] = 0;
				$new_array2['booking_itinerary_details'][$i]['currency'] = $val['payment_currency'];
				$new_array2['booking_itinerary_details'][$i]['attributes'] = '';
				$new_array2['booking_itinerary_details'][$i]['RoomPrice'] = 0;
				$new_array2['booking_itinerary_details'][$i]['Tax'] = 0;
				$new_array2['booking_itinerary_details'][$i]['ExtraGuestCharge'] = 0;
				$new_array2['booking_itinerary_details'][$i]['ChildCharge'] = 0;
				$new_array2['booking_itinerary_details'][$i]['OtherCharges'] = 0;
				$new_array2['booking_itinerary_details'][$i]['Discount'] = 0;
				$new_array2['booking_itinerary_details'][$i]['ServiceTax'] = 0;
				$new_array2['booking_itinerary_details'][$i]['AgentCommission'] = 0;
				$new_array2['booking_itinerary_details'][$i]['AgentMarkUp'] = 0;
				$new_array2['booking_itinerary_details'][$i]['TDS'] = 0;
				/*booking_customer_details*/
				foreach($passenger_details['first_name'] as $pk => $val2){
					$array_passenger_type = ["1"=>"Adult","2"=>"Child"];
					$array_passenger_title = ["1"=>"MR","2"=>"MS","3"=>"MISS","5"=>"MRS"];
					$raw_title =  $passenger_details['name_title'][$pk];
					$psngr_title = (isset($array_passenger_title[$raw_title])) ? $array_passenger_title[$raw_title] : '';
					$raw_type =  $passenger_details['passenger_type'][$pk];
					$psngr_type = (isset($array_passenger_type[$raw_type])) ? $array_passenger_type[$raw_type] : '';
					$new_array2['booking_customer_details'][$j] ['origin']= $val['id'];
					$new_array2['booking_customer_details'][$j] ['app_reference'] = $val['parent_pnr'];
					$new_array2['booking_customer_details'][$j]['title'] = $psngr_title;
					$new_array2['booking_customer_details'][$j]['first_name'] = $passenger_details['first_name'][$pk];
                    $new_array2['booking_customer_details'][$j]['middle_name'] = $passenger_details['middle_name'][$pk];
                    $new_array2['booking_customer_details'][$j]['last_name'] = $passenger_details['last_name'][$pk];
                    $new_array2['booking_customer_details'][$j]['phone'] =  $passenger_details['passenger_contact'];
                    $new_array2['booking_customer_details'][$j]['email'] =  $passenger_details['billing_email'];
                    $new_array2['booking_customer_details'][$j]['pax_type'] =  $psngr_type;
                    $new_array2['booking_customer_details'][$j]['date_of_birth'] = $passenger_details['date_of_birth'][$pk];
                    $new_array2['booking_customer_details'][$j]['passenger_nationality'] = $this->get_country_name($passenger_details['passenger_nationality'][$pk]);
                    $new_array2['booking_customer_details'][$j]['passport_number'] = $passenger_details['passenger_passport_number'][$pk];
                    $new_array2['booking_customer_details'][$j]['passport_issuing_country'] = $this->get_country_name($passenger_details['passenger_passport_issuing_country'][$pk]);
                    $new_array2['booking_customer_details'][$j]['passport_expiry_date'] = ($passenger_details['passenger_passport_expiry_year'][$pk].'-'.$passenger_details['passenger_passport_expiry_month'][$pk].'-'.$passenger_details['passenger_passport_expiry_day'][$pk]);
                    $new_array2['booking_customer_details'][$j]['status'] = $val['booking_status'];
                    $new_array2['booking_customer_details'][$j]['attributes'] = '';
                    $j++;
				}
				/*cancellation_details*/
				$new_array2['cancellation_details'] = [];/*
				$new_array2['cancellation_details'][0]['origin'] = 1;
				$new_array2['cancellation_details'][0]['origin'] = 1;
				$new_array2['cancellation_details'][0]['origin'] = 1;
				$new_array2['cancellation_details'][0]['origin'] = 1;
				$new_array2['cancellation_details'][0]['origin'] = 1;
				$new_array2['cancellation_details'][0]['origin'] = 1;*/

                $i++;
            }
            $new_array['data'] = $new_array2;
	return $new_array;
        }else{
            return $new_array;
        }
    }
    public function get_country_name($country_id){
    	$result = '';
		if($country_id!=''){
			$this->db->select('name');
	        $this->db->from('api_country_list');
	        $this->db->where('origin',$country_id);
	        $query = $this->db->get();
	        if($query->num_rows() > 0){
	            $data =  $query->row();
	            $result  = $data->name;
	    	}
	    }
	    return $result;
	}
	public function get_hotel_crs_image($hotel_id){
		$result = '';
		if($hotel_id!=''){
			$this->db->select('hotel_images');
	        $this->db->from('hotel_details');
	        $this->db->where('hotel_details_id',$hotel_id);
	        $query = $this->db->get();
	        if($query->num_rows() > 0){
	            $data =  $query->row();
	            $single_image = explode(',',$data->hotel_images);
	            if(!empty($single_image)){
	            	$result  = $single_image[0];	
	            }
	       }
	    }
	    return $result;
	}
	public function hotel_crs_cancel_request ($app_reference,$booking_source){
		$data = array('booking_status'=>'CANCELLED');
		$this->db->set($data);
        $this->db->where('parent_pnr',$app_reference);
        $this->db->update('booking_global');
        if( $this->db->affected_rows() > 0){
        	$result = ['status'=>1,"msg"=>"Booking Cancelled Successfully."];
        }else{
        	$result = ['status'=>0,"msg"=>"Cancel Request Sent Already."];
        }
        return $result;
	}
	public function format_crs_and_tbo($table_data,$crs_data){

		if(isset($table_data['status']) &&  $table_data['status']==1){

			$crs_data_raw = $crs_data['data'];
			foreach($crs_data_raw['booking_details'] as $key => $val){
				if(!empty($val)){
					array_push($table_data['data']['booking_details'],$val);	
				}
			}
			foreach($crs_data_raw['booking_itinerary_details'] as $key => $val){
				if(!empty($val)){
					array_push($table_data['data']['booking_itinerary_details'],$val);	
				}
			}
			foreach($crs_data_raw['booking_customer_details'] as $key => $val){
				if(!empty($val)){
					array_push($table_data['data']['booking_customer_details'],$val);	
				}
			}
			foreach($crs_data_raw['cancellation_details'] as $key => $val){
				if(!empty($val)){
					array_push($table_data['data']['cancellation_details'],$val);	
				}
			}
		}
		return $table_data;
	}
	public function hotel_crs_booking_count_user(){
		$user_id  = $this->entity_user_id;
		$this->db->select('bg.*');
        $this->db->from('booking_global bg');
        $this->db->where('bg.user_id',$user_id);
        $this->db->where('bg.module','HOTEL');
        $query = $this->db->get();
        $crs_count = $query->num_rows;
        return $crs_count;
	}

	public function get_hotel_amenities_name($amenityId)
   {
        $q="SELECT amenities_name FROM hotel_amenities WHERE hotel_amenities_id=".$amenityId;
        $am = $this->db->query($q)->row_array();
        return $am['amenities_name'];
   }
   public function get_hotel_amenities($amenityId='')
   {
   		// debug($amenityId);exit;
   		if(isset($amenityId) && !empty($amenityId))
   		{

		    $q="SELECT * FROM hotel_amenities WHERE hotel_amenities_id=".$amenityId;
		    $am = $this->db->query($q)->row_array();
   		}else{
   			return true;
   		}
        //echo $this->db->last_query();exit;
        //debug($am);
        return $am;
   }
   function get_hotel_list($search_data){

		// debug($search_data);exit;
		 $q2 = "SELECT 
                    hd.hotel_id,hd.country_id,hd.city_id,hd.address_info,
                    hd.hotel_name,hd.hotel_code,hd.hotel_price,hd.minimum_days,hd.latitude,hd.longitude,
                    hd.maximum_days,hd.star_rating,hd.hotel_description,
                    hd.child_group_a,hd.child_group_b,hd.child_group_c,hd.child_group_d,hd.child_group_e

                FROM 
                    hotel_details hd
                WHERE 
                    hd.city_details_id='".$city_id."'
                AND hd.status='1' 
                AND hd.hotel_id IN (
                    SELECT  
                        DISTINCT hotel_details_id 
                    FROM
                        `seasons_details` hsd 
                    WHERE 
                        hotel_details_id = hd.hotel_details_id 
                   
                     AND 
                        '".$checkin_date."' >= hsd.seasons_from_date AND '".$checkout_date."' <= hsd.seasons_to_date
                     AND 
                        ".$stay_days." >= hsd.minimum_stays AND hsd.status='ACTIVE'
                     AND hotel_room_type_id IN  
                        (
                            SELECT 
                                DISTINCT hrci.hotel_room_type_id 
                            FROM 
                                hotel_room_type ht 
                            LEFT JOIN
                                hotel_season_room_count_info hrci 
                                ON (
                                        (
                                            ht.hotel_room_type_id = hrci.hotel_room_type_id
                                        )
                                    AND 
                                        ht.hotel_details_id = hrci.hotel_details_id
                                   )
                            LEFT JOIN
                                 hotel_room_rate_info rate
                                 ON (
                                        rate.seasons_details_id = hrci.hotel_season_id
                                        AND 
                                        rate.hotel_details_id = hrci.hotel_details_id
                                        AND 
                                        rate.hotel_room_type_id = hrci.hotel_room_type_id
                                    )
                            WHERE 
                                ht.status='ACTIVE'
                            AND
                                hrci.status='ACTIVE' 
                            AND 
                                rate.hotel_room_rate_info_id IS NOT NULL    
                            AND
                                ht.hotel_details_id = hd.hotel_details_id
                            AND 
                                hrci.hotel_room_type_id IS NOT NULL
                            AND 
                                rate.seasons_details_id IS NOT NULL
                            AND 
                                (
                                    hrci.no_of_room_available > hrci.no_of_room_booked AND  (hrci.no_of_room_available - hrci.no_of_room_booked >= ".$room_count.")
                                )
                            
                            AND 
                                (  rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<=ht.adult AND ".$s_max_child."<=ht.child)
                                OR
                                (  rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<= ht.max_pax AND ".$s_max_child."<=ht.child AND ht.extra_bed ='Available')
                                OR
                                (  rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<= ht.max_pax AND ".$s_max_child."<=ht.child + 1 AND ht.extra_bed ='Available')
                                OR
                                (   rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<= ht.adult AND ".$s_max_child."<=ht.child + 1 AND ht.extra_bed ='Available')
                        )
                )
                ORDER BY hd.creation_date DESC";

               
        //$q2 = trim($q2);print_r($q2); exit();
        //echo $q2;exit;
        return $this->db->query($q2);
	}
	function get_trip_adv_not_exists(){

		// $items = array('1'=>1,'2'=>2,'7'=>7,'8'=>8,'9'=>9,'10'=>10,'11'=>11,'12'=>12,'15'=>15,'16'=>16,'18'=>18,'22'=>22,'25'=>25,'26'=>26,'40'=>40,'41'=>41,'42'=>42,'43'=>43,'44'=>44,'46'=>46);
		// $this->db->select('*');
		// $this->db->from('trip_advisor');
	 //    $this->db->where('hotel_code', array_rand($items));
		// $query = $this->db->get();
		// $num = $query->num_rows();
		// //echo $this->db->last_query(); exit;
		// if($num){
		// 	$data['result'] = $query->row_array();
		// 	$data['status'] = true;
		// }else{
		// 	$data['status'] = false;
		// }

		return false;
	}
	function b2c_crs_hotel_report($condition=array(), $count=false, $offset=0, $limit=100000000000)
	  {
	  	//debug($condition);exit;
		$condition = $this->custom_db->get_custom_condition($condition);
		// debug($condition);exit;

		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BG.ref_id)) as total_records
					from booking_global BG
					join booking_hotel AS BH on BG.ref_id=BH.id
					left join user as U on BG.user_id = U.user_id 
					where (U.user_type='.B2C_USER.' OR BG.user_type = 0)'.' '.$condition.'';
					// echo $query;die;
			$data = $this->db->query($query)->row_array();
			//echo $this->db->last_query();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$cancellation_details = array();
			
			$bd_query = 'select BG.*,BH.*,HD.city_details_id as hotel_location,U.user_name,U.first_name,U.last_name,BG.ref_id as app_reference from booking_global AS BG 
			             join booking_hotel AS BH on BG.ref_id=BH.id 
			             join hotel_details AS HD on BG.hotel_details_id= HD.hotel_details_id  
					     left join user U on BG.user_id =U.user_id 					     
						 WHERE  (U.user_type='.B2C_USER.' OR BG.user_id = 0)'.$condition.'						 
						 order by BH.book_date desc, BH.id desc limit '.$offset.', '.$limit.'';
						// $this->db->last_query();exit;

			$booking_details = $this->db->query($bd_query)->result_array();
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			/*if(empty($app_reference_ids) == false) {
				$id_query = 'select * from hotel_booking_itinerary_details AS ID 
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				$cd_query = 'select * from hotel_booking_pax_details AS CD 
							WHERE CD.app_reference IN ('.$app_reference_ids.')';
				$cancellation_details_query = 'select * from hotel_cancellation_details AS HCD 
							WHERE HCD.app_reference IN ('.$app_reference_ids.')';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$cancellation_details	= $this->db->query($cancellation_details_query)->result_array();
			}*/
			$response['data']['booking_details']			= $booking_details;
			$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			$response['data']['booking_customer_details']	= $booking_customer_details;
			$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}
	function b2c_crs_hotel_report_info($condition=array(), $count=false, $offset=0, $limit=100000000000)
	  {
	  	//debug($condition);exit;
		$condition = $this->custom_db->get_custom_condition($condition);
		// debug($condition);exit;

		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BG.ref_id)) as total_records
					from booking_global BG
					join booking_hotel AS BH on BG.ref_id=BH.id
					left join user as U on BG.user_id = U.user_id 
					where (U.user_type='.B2C_USER.' OR BG.user_type = 0)'.' '.$condition.'';
					// echo $query;die;
			$data = $this->db->query($query)->row_array();
			//echo $this->db->last_query();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$cancellation_details = array();
			
			$bd_query = 'select BG.*,BH.*,HD.city_details_id as hotel_location,U.user_name,U.first_name,U.last_name,BG.ref_id as app_reference from booking_global AS BG 
			             join booking_hotel AS BH on BG.ref_id=BH.id 
			             join hotel_details AS HD on BG.hotel_details_id= HD.hotel_details_id  
					     left join user U on BG.user_id =U.user_id 					     
						 WHERE  (U.user_type='.B2C_USER.' OR BG.user_id = 0)'.$condition.'						 
						 order by BH.book_date desc, BH.id desc limit '.$offset.', '.$limit.'';
						// $this->db->last_query();exit;

			$booking_details = $this->db->query($bd_query)->result_array();
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			/*if(empty($app_reference_ids) == false) {
				$id_query = 'select * from hotel_booking_itinerary_details AS ID 
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				$cd_query = 'select * from hotel_booking_pax_details AS CD 
							WHERE CD.app_reference IN ('.$app_reference_ids.')';
				$cancellation_details_query = 'select * from hotel_cancellation_details AS HCD 
							WHERE HCD.app_reference IN ('.$app_reference_ids.')';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$cancellation_details	= $this->db->query($cancellation_details_query)->result_array();
			}*/
			$response['data']['booking_details']			= $booking_details;
			$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			$response['data']['booking_customer_details']	= $booking_customer_details;
			$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}

	/**
	* Villa List
	**/
	function get_villa_search_data($datetime1, $datetime2,$stay_days,$s_max_adult,$s_max_child,$checkin_date,$checkout_date,$room_count,$city_id){
		 $q2 = "
                SELECT 
                    hd.hotel_details_id,hd.country_id,hd.city_details_id,hd.hotel_type_id,hd.hotel_address,
                    hd.hotel_name,hd.hotel_code,hd.hotel_price,currency_type,hd.min_stay_day,hd.hotel_amenities,hd.latitude,hd.longitude,
                    hd.max_stay_day,hd.star_rating,hd.hotel_info,hd.thumb_image,
                    hd.hotel_images,hd.hotel_image_url,
                    hd.child_group_a,hd.child_group_b,hd.child_group_c,hd.child_group_d,hd.child_group_e

                FROM 
                    hotel_details hd 
                WHERE 
                    hd.city_details_id like '%".$city_id."%'
                AND hd.status='ACTIVE' 
                AND hd.contract_expire_date >= CURDATE()
                AND hd.hotel_details_id IN (
                    SELECT  
                        DISTINCT hotel_details_id 
                    FROM
                        `seasons_details` hsd 
                    WHERE 
                        hotel_details_id = hd.hotel_details_id 
                   
                     AND 
                        '".$checkin_date."' >= hsd.seasons_from_date AND '".$checkout_date."' <= hsd.seasons_to_date
                     AND '".$checkin_date."' >= '".Date('Y-m-d', strtotime("+3 days"))."'
                     AND 
                        ".$stay_days." >= hsd.minimum_stays AND hsd.status='ACTIVE'
                     AND hd.hotel_type_id = 23 
                     AND hotel_room_type_id IN  
                        (
                            SELECT 
                                DISTINCT hrci.hotel_room_type_id 
                            FROM 
                                hotel_room_type ht 
                            LEFT JOIN
                                hotel_season_room_count_info hrci 
                                ON (
                                        (
                                            ht.hotel_room_type_id = hrci.hotel_room_type_id
                                        )
                                    AND 
                                        ht.hotel_details_id = hrci.hotel_details_id
                                   )
                            LEFT JOIN
                                 hotel_room_rate_info rate
                                 ON (
                                        rate.seasons_details_id = hrci.hotel_season_id
                                        AND 
                                        rate.hotel_details_id = hrci.hotel_details_id
                                        AND 
                                        rate.hotel_room_type_id = hrci.hotel_room_type_id
                                    )
                            WHERE 
                                ht.status='ACTIVE'
                            AND
                                hrci.status='ACTIVE' 
                            AND 
                                rate.hotel_room_rate_info_id IS NOT NULL    
                            AND
                                ht.hotel_details_id = hd.hotel_details_id
                            AND 
                                hrci.hotel_room_type_id IS NOT NULL
                            AND 
                                rate.seasons_details_id IS NOT NULL
                            AND 
                                (
                                    hrci.no_of_room_available > hrci.no_of_room_booked AND  (hrci.no_of_room_available - hrci.no_of_room_booked >= ".$room_count.")
                                )
                            
                            AND 
                                (  rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<=ht.adult AND ".$s_max_child."<=ht.child)
                                OR
                                (  rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<= ht.max_pax AND ".$s_max_child."<=ht.child AND ht.extra_bed ='Available')
                                OR
                                (  rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<= ht.max_pax AND ".$s_max_child."<=ht.child + 1 AND ht.extra_bed ='Available')
                                OR
                                (   rate.hotel_room_rate_info_id IS NOT NULL AND ht.hotel_details_id = hd.hotel_details_id AND ".$s_max_adult."<= ht.adult AND ".$s_max_child."<=ht.child + 1 AND ht.extra_bed ='Available')
                        )
                )
                ORDER BY hd.creation_date DESC";
               //echo $q2;exit;
        return $this->db->query($q2);
	}
	
}
