<?php

class Payment_Model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
    }
	public function validate_order_id($order_id){
		$this->db->where('parent_pnr',$order_id);
		$this->db->where('transaction_status','PROCESS');
		return $this->db->get('booking_global');
	}
	
	public function validate_order_id_org($order_id){
		$this->db->where('parent_pnr_no',$order_id);
		$this->db->join('product_details', 'product_details.product_details_id = booking_global.product_id','LEFT');
		$this->db->join('api_details', 'api_details.api_details_id = booking_global.api_id','LEFT');
		$this->db->join('user_type', 'user_type.user_type_id = booking_global.user_type_id','LEFT');
			$this->db->join('booking_transaction', 'booking_transaction.booking_transaction_id = booking_global.booking_transaction_id','LEFT');
		$this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id','LEFT');
		return $this->db->get('booking_global');
	}
	public function update_transactions($order_id,$transaction){
		$this->db->where('parent_pnr_no',$order_id);
		$this->db->update('booking_global',$transaction);
	}
	public function updateTransactions($trans_id,$transaction){
		$this->db->where('booking_transaction_id',$trans_id);
		$this->db->update('booking_transaction',$transaction);
	}

	public function update_pay_transactions($payment_id,$transaction){
		$this->db->where('payment_id',$payment_id);
		$this->db->update('booking_payment',$transaction);
	}

	public function validate_order_id_v1($order_id){
		$this->db->where('parent_pnr',$order_id);
		$this->db->where('payment_status','5');
		return $this->db->get('booking_global');
	}
	public function update_transaction($order_id,$transaction){
		$this->db->where('parent_pnr',$order_id);
		$this->db->update('booking_global',$transaction);
	}
	public function getPaymentDetails() {
		$query = $this->db->get('payment_api_details');
		return $query->row();
	}

}

