<?php
/**
 * Library which has generic functions to get data
 *
 * @package    Provab Application
 * @subpackage Hotel Model
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
class Hotel_Model extends CI_Model {
	private $master_search_data;
	
	/**
	 * return top destinations in hotel
	 */
	public function get_loc($value, $value1)
	{
		$this->db->select('*');
		$this->db->from('gta_hb_hotels_city');
		$this->db->where('city_name', $value);
		$this->db->where('country_name', $value1);
		return $this->db->get()->row_array();
	}
	 function get_hotel_advertisement() {
		$query = 'Select * from Advertisement_images where slider_module=2 and slider_status = ' . ACTIVE.' ORDER BY image_id DESC  LIMIT 1';
		$data = $this->db->query ( $query )->row_array ();
		return $data;
	}
	function hotel_top_destinations1() {
		$query = 'Select CT.*, CN.name AS country from api_city_list CT, api_country_list CN where CT.country=CN.origin AND top_destination = ' . ACTIVE.'and module_type = 1';
		$data = $this->db->query ( $query )->result_array ();
		return $data;
	}
	function hotel_top_destinations() {
		$query = 'Select * from gta_hb_hotels_city where top_destination = ' . ACTIVE;
		$data = $this->db->query ( $query )->result_array ();
		return $data;
	}
	/*
	 *
	 * Get Airport List
	 *
	 */
	function get_hotel_city_list($search_chars) {
		//debug($search_chars);exit;
		if($search_chars == ''){
			//debug($_SERVER['REMOTE_ADDR']);die;
			$ip = $_SERVER['REMOTE_ADDR'];
			//$ip = "72.229.28.185"; 
			$getcustomercountry = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip);
			$getcustomercountry = json_decode($getcustomercountry,true);
			//debug($getcustomercountry['geoplugin_countryName']);die;
			//echo $getcustomercountry;die;
			$search_chars = $getcustomercountry['geoplugin_countryName'];
			if ($getcustomercountry['geoplugin_countryName'] == "United States") {
				$search_chars = 'USA';
			}
			
			//debug($search_chars);exit;
			//$search_chars = 'Canada';
		}
		$raw_search_chars = $this->db->escape ( $search_chars );
		$r_search_chars = $this->db->escape ( $search_chars . '%' );
		$search_chars = $this->db->escape ( '%' . $search_chars . '%' );
		

		/*$query = 'Select * from hotels_city where city_name like ' . $search_chars . '
		OR country_name like ' . $search_chars . ' OR country_code like ' . $search_chars . '
		ORDER BY top_destination DESC, CASE
			WHEN	city_name	LIKE	' . $raw_search_chars . '	THEN 1
			WHEN	country_name	LIKE	' . $raw_search_chars . '	THEN 2
			WHEN	country_code			LIKE	' . $raw_search_chars . '	THEN 3
			
			WHEN	city_name	LIKE	' . $r_search_chars . '	THEN 4
			WHEN	country_name	LIKE	' . $r_search_chars . '	THEN 5
			WHEN	country_code			LIKE	' . $r_search_chars . '	THEN 6
			
			WHEN	city_name	LIKE	' . $search_chars . '	THEN 7
			WHEN	country_name	LIKE	' . $search_chars . '	THEN 8
			WHEN	country_code			LIKE	' . $search_chars . '	THEN 9
			ELSE 10 END, 
			cache_hotels_count DESC
		LIMIT 0, 20';*/

		/*$query = 'Select * from  gta_hb_hotels_city where city_name like ' . $r_search_chars . '
		OR country_name like ' . $r_search_chars . ' AND country_name IS NOT NULL
		ORDER BY city_name asc, CASE
			WHEN	city_name	LIKE	' . $raw_search_chars . '	THEN 1
			WHEN	country_name	LIKE	' . $raw_search_chars . '	THEN 2
			
			WHEN	city_name	LIKE	' . $search_chars . '	THEN 3
			WHEN	country_name	LIKE	' . $search_chars . '	THEN 4
			
			ELSE 10 END
		LIMIT 0, 10';*/

		$query = 'Select * from  api_hotel_city_list where city_name like ' . $r_search_chars . '
		OR airport_code like ' . $r_search_chars . '
		 AND country_name IS NOT NULL
		ORDER BY origin asc, CASE
			WHEN	city_name	LIKE	' . $raw_search_chars . '	THEN 1
			WHEN	airport_code	LIKE	' . $raw_search_chars . '	THEN 2

			WHEN	city_name	LIKE	' . $search_chars . '	THEN 3
			WHEN	airport_code	LIKE	' . $search_chars . '	THEN 4
			
			ELSE 10 END
		LIMIT 0, 10';
		//echo $query;exit;
		return $this->db->query ( $query )->result_array ();
	}
	
	
	/**
	 * get all the booking source which are active for current domain
	 */
	function active_booking_source() {
		$query = 'select BS.source_id, BS.origin from meta_course_list AS MCL, booking_source AS BS, activity_source_map AS ASM WHERE
		MCL.origin=ASM.meta_course_list_fk and ASM.booking_source_fk=BS.origin and MCL.course_id=' . $this->db->escape ( META_ACCOMODATION_COURSE ) . '
		and BS.booking_engine_status=' . ACTIVE . ' AND MCL.status=' . ACTIVE . ' AND ASM.status="active"';
		//echo $query;die;
		return $this->db->query ( $query )->result_array ();
	}
	/**
	 * return booking list
	 */
	function booking($condition = array(), $count = false, $offset = 0, $limit = 100000000000) {
		$condition = $this->custom_db->get_custom_condition ( $condition );
		// BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) as total_records 
					from hotel_booking_details BD
					join hotel_booking_itinerary_details AS HBID on BD.app_reference=HBID.app_reference
					join payment_option_list AS POL on BD.payment_mode=POL.payment_category_code 
					where BD.domain_origin=' . get_domain_auth_id () . ' and BD.created_by_id =' . $GLOBALS ['CI']->entity_user_id . '' . $condition;
			$data = $this->db->query ( $query )->row_array ();
			return $data ['total_records'];
		} else {
			$this->load->library ( 'booking_data_formatter' );
			$response ['status'] = SUCCESS_STATUS;
			$response ['data'] = array ();
			$booking_itinerary_details = array ();
			$booking_customer_details = array ();
			$cancellation_details = array ();
			$bd_query = 'select * from hotel_booking_details AS BD 
						WHERE BD.domain_origin=' . get_domain_auth_id () . ' and BD.created_by_id =' . $GLOBALS ['CI']->entity_user_id . '' . $condition . '
						order by BD.origin desc limit ' . $offset . ', ' . $limit;
			$booking_details = $this->db->query ( $bd_query )->result_array ();
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids ( $booking_details );
			if (empty ( $app_reference_ids ) == false) {
				$id_query = 'select * from hotel_booking_itinerary_details AS ID 
							WHERE ID.app_reference IN (' . $app_reference_ids . ')';
				$cd_query = 'select * from hotel_booking_pax_details AS CD 
							WHERE CD.app_reference IN (' . $app_reference_ids . ')';
				$cancellation_details_query = 'select * from hotel_cancellation_details AS HCD 
							WHERE HCD.app_reference IN (' . $app_reference_ids . ')';
				$booking_itinerary_details = $this->db->query ( $id_query )->result_array ();
				$booking_customer_details = $this->db->query ( $cd_query )->result_array ();
				$cancellation_details = $this->db->query ( $cancellation_details_query )->result_array ();
			}
			$response ['data'] ['booking_details'] = $booking_details;
			$response ['data'] ['booking_itinerary_details'] = $booking_itinerary_details;
			$response ['data'] ['booking_customer_details'] = $booking_customer_details;
			$response ['data'] ['cancellation_details'] = $cancellation_details;
			return $response;
		}
	}
	/**
	 * Return Booking Details based on the app_reference passed
	 * 
	 * @param
	 *        	$app_reference
	 * @param
	 *        	$booking_source
	 * @param
	 *        	$booking_status
	 */
	function get_booking_details($app_reference, $booking_source, $booking_status = '') {
		$response ['status'] = FAILURE_STATUS;
		$response ['data'] = array ();
		$bd_query = 'select * from hotel_booking_details AS BD WHERE BD.app_reference like ' . $this->db->escape ( $app_reference );
		if (empty ( $booking_source ) == false) {
			$bd_query .= '	AND BD.booking_source = ' . $this->db->escape ( $booking_source );
		}
		if (empty ( $booking_status ) == false) {
			$bd_query .= ' AND BD.status = ' . $this->db->escape ( $booking_status );
		}
		$id_query = 'select * from hotel_booking_itinerary_details AS ID WHERE ID.app_reference=' . $this->db->escape ( $app_reference );
		$cd_query = 'select * from hotel_booking_pax_details AS CD WHERE CD.app_reference=' . $this->db->escape ( $app_reference );
		$cancellation_details_query = 'select HCD.* from hotel_cancellation_details AS HCD WHERE HCD.app_reference=' . $this->db->escape ( $app_reference );
		$hotel_cancelation_policy = 'select HCD.* from hotel_cancelation_policy AS HCD WHERE HCD.app_reference=' . $this->db->escape ( $app_reference );

		$response ['data'] ['booking_details'] = $this->db->query ( $bd_query )->result_array ();
		$response ['data'] ['booking_itinerary_details'] = $this->db->query ( $id_query )->result_array ();
		$response ['data'] ['booking_customer_details'] = $this->db->query ( $cd_query )->result_array ();
		$response ['data'] ['cancellation_details'] = $this->db->query ( $cancellation_details_query )->result_array ();
		$response ['data'] ['hotel_cancelation_policy'] = $this->db->query ( $hotel_cancelation_policy )->result_array ();
		if (valid_array ( $response ['data'] ['booking_details'] ) == true and valid_array ( $response ['data'] ['booking_itinerary_details'] ) == true and valid_array ( $response ['data'] ['booking_customer_details'] ) == true) {
			$response ['status'] = SUCCESS_STATUS;
		}
		return $response;
	}
	
	/**
	 * get search data and validate it
	 */
	function get_safe_search_data($search_id) {

		$search_data = $this->get_search_data ( $search_id );

		$success = true;
		$clean_search = '';
		if ($search_data != false) {
			// validate
			$temp_search_data = json_decode ( $search_data ['search_data'], true );
			$clean_search = $this->clean_search_data ( $temp_search_data );
			$success = $clean_search ['status'];
			$clean_search = $clean_search ['data'];
			
		} else {
			$success = false;
		}

		return array (
				'status' => $success,
				'data' => $clean_search 
		);
	}
	
	/**
	 * Clean up search data
	 */
	function clean_search_data($temp_search_data) {
		$success = true;
		// make sure dates are correct
		if ((strtotime ( $temp_search_data ['hotel_checkin'] ) > time () && strtotime ( $temp_search_data ['hotel_checkout'] ) > time ()) || date ( 'Y-m-d', strtotime ( $temp_search_data ['hotel_checkin'] ) ) == date ( 'Y-m-d' )) {
			// if (strtotime($temp_search_data['hotel_checkin']) > strtotime($temp_search_data['hotel_checkout'])) {
			// Swap dates if not correctly set
			$clean_search ['from_date'] = $temp_search_data ['hotel_checkin'];
			$clean_search ['to_date'] = $temp_search_data ['hotel_checkout'];
			/*
			 * } else {
			 * $clean_search['from_date'] = $temp_search_data['hotel_checkout'];
			 * $clean_search['to_date'] = $temp_search_data['hotel_checkin'];
			 * }
			 */
			$clean_search ['no_of_nights'] = abs ( get_date_difference ( $clean_search ['from_date'], $clean_search ['to_date'] ) );
		} else {
			$success = false;
		}
		// city name and country name
		
		if (isset ( $temp_search_data ['hotel_destination'] ) == true) {
			$clean_search ['hotel_destination'] = $temp_search_data ['hotel_destination'];
		}
		if (isset ( $temp_search_data ['city'] ) == true) {
			$clean_search ['location'] = $temp_search_data ['city'];
			$temp_location = explode ( '(', $temp_search_data ['city'] );
			$clean_search ['city_name'] = trim ( $temp_location [0] );
			if (isset ( $temp_location [1] ) == true) {
				// Pop will get last element in the array since element patterns can repeat
				$clean_search ['country_name'] = trim ( array_pop ( $temp_location ), '() ' );
			} else {
				$clean_search ['country_name'] = '';
			}
		} else {
			$success = false;
		}
		
		// Occupancy
		if (isset ( $temp_search_data ['rooms'] ) == true) {
			$clean_search ['room_count'] = abs ( $temp_search_data ['rooms'] );
		} else {
			$success = false;
		}
		if (isset ( $temp_search_data ['adult'] ) == true) {
			$clean_search ['adult_config'] = $temp_search_data ['adult'];
		} else {
			$success = false;
		}
		
		if (isset ( $temp_search_data ['child'] ) == true) {
			$clean_search ['child_config'] = $temp_search_data ['child'];
		}
		
		if (valid_array ( $temp_search_data ['child'] )) {
			foreach ( $temp_search_data ['child'] as $tc_k => $tc_v ) {
				if (intval ( $tc_v ) > 0) {
					$child_age_index = $tc_v;
					foreach ( $temp_search_data ['childAge_' . ($tc_k + 1)] as $ic_k => $ic_v ) {
						if($ic_k < $child_age_index){
							$clean_search ['child_age'] [] = $ic_v;
						}
					}
				}
			}
		}
		if (strtolower ( $clean_search ['country_name'] ) == 'india') {
			$clean_search ['is_domestic'] = true;
		} else {
			$clean_search ['is_domestic'] = false;
		}
		return array (
				'data' => $clean_search,
				'status' => $success 
		);
	}
	
	/**
	 * get search data without doing any validation
	 * 
	 * @param
	 *        	$search_id
	 */
	function get_search_data($search_id) {
		if (empty ( $this->master_search_data )) {
			$search_data = $this->custom_db->single_table_records ( 'search_history', '*', array (
					'search_type' => META_ACCOMODATION_COURSE,
					'origin' => $search_id 
			) );
			if ($search_data ['status'] == true) {
				$this->master_search_data = $search_data ['data'] [0];
			} else {
				return false;
			}
		}
		return $this->master_search_data;
	}
	
	/**
	 * get hotel city id of tbo from tbo hotel city list
	 * 
	 * @param string $city
	 *        	city name for which id has to be searched
	 * @param string $country
	 *        	country name in which the city is present
	 */
	function tbo_hotel_city_id($city, $country) {
		$response ['status'] = true;
		$response ['data'] = array ();
		$location_details = $this->custom_db->single_table_records ( 'hotels_city', 'country_code, origin', array (
				'city_name like' => $city,
				'country_name like' => $country 
		) );
		if ($location_details ['status']) {
			$response ['data'] = $location_details ['data'] [0];
		} else {
			$response ['status'] = false;
		}
		return $response;
	}
	
	/**
	 *
	 * @param number $domain_origin        	
	 * @param string $status        	
	 * @param string $app_reference        	
	 * @param string $booking_source        	
	 * @param string $booking_id        	
	 * @param string $booking_reference        	
	 * @param string $confirmation_reference        	
	 * @param number $total_fare        	
	 * @param number $domain_markup        	
	 * @param number $level_one_markup        	
	 * @param string $currency        	
	 * @param string $hotel_name        	
	 * @param number $star_rating        	
	 * @param string $hotel_code        	
	 * @param number $phone_number        	
	 * @param string $alternate_number        	
	 * @param string $email        	
	 * @param string $payment_mode        	
	 * @param string $attributes        	
	 * @param number $created_by_id        	
	 */
	function save_booking_details($mypanattributes, $domain_origin, $status, $app_reference, $psbooking_details, $booking_source, $booking_id, $booking_reference, $confirmation_reference, $hotel_name, $star_rating, $hotel_code, $phone_number, $alternate_number, $email, $hotel_check_in, $hotel_check_out,$payment_mode, $admin_markup,$agent_markup,$convinence_value,$currency_val, $total_amount,  $attributes , $created_by_id,$air_booking_id,$airl_app_ref) {
		//debug();
		//echo $created_by_id; exit;
		$data ['domain_origin'] = $domain_origin;
		$data ['status'] = $status;
		$data ['app_reference'] = $app_reference;
		$data ['booking_id'] = $psbooking_details;
		$data ['booking_source'] = $booking_source;
		$data ['booking_reference'] = $booking_reference;
		$data ['confirmation_reference'] = $confirmation_reference;
		$data ['hotel_name'] = $hotel_name;
		$data ['star_rating'] = $star_rating;
		$data ['hotel_code'] = $hotel_code;
		$data ['phone_number'] = $phone_number;
		$data ['alternate_number'] = $alternate_number;
		//$data ['hotel_address'] = $mypanattributes['address'];
		$data ['email'] = $email;
		$data ['hotel_check_in'] = $hotel_check_in;
		$data ['hotel_check_out'] = $hotel_check_out;
		$data ['payment_mode'] = $payment_mode;
		$data ['attributes'] = json_encode($mypanattributes);
		$data ['created_by_id'] = $created_by_id;
		$data ['created_datetime'] = date ( 'Y-m-d H:i:s' );
		
		$data ['currency'] = $transaction_currency;
		$data ['currency_conversion_rate'] = $currency_conversion_rate;

		$data ['airliner_booking_reference'] = $air_booking_id;
		$data ['airl_app_reference'] = $airl_app_ref;
		
		//debug($data); exit;
		
		$status = $this->custom_db->insert_record ( 'hotel_booking_details', $data );
		return $status;
	}
	
	/**
	 *
	 * @param string $app_reference        	
	 * @param string $location        	
	 * @param date $check_in        	
	 * @param date $check_out        	
	 * @param string $room_type_name        	
	 * @param string $bed_type_code        	
	 * @param string $status        	
	 * @param string $smoking_preference        	
	 * @param string $attributes        	
	 */
	function save_booking_itinerary_details($app_reference, $location, $check_in, $check_out, $room_type_name, $bed_type_code, $status, $smoking_preference, $total_fare, $admin_markup, $agent_markup, $currency, $attributes, $RoomPrice, $Tax, $ExtraGuestCharge, $ChildCharge, $OtherCharges, $Discount, $ServiceTax, $AgentCommission, $AgentMarkUp, $TDS) {
		$data ['app_reference'] = $app_reference;
		$data ['location'] = $location;
		$data ['check_in'] = $check_in;
		$data ['check_out'] = $check_out;
		$data ['room_type_name'] = $room_type_name;
		$data ['bed_type_code'] = $bed_type_code;
		$data ['status'] = $status;
		$data ['smoking_preference'] = $smoking_preference;
		$data ['total_fare'] = $total_fare;
		$data ['admin_markup'] = $admin_markup;
		$data ['agent_markup'] = $agent_markup;
		$data ['currency'] = $currency;
		$data ['attributes'] = $attributes;
		
		$data ['RoomPrice'] = floatval ( $Tax );
		$data ['Tax'] = floatval ( $Tax );
		$data ['ExtraGuestCharge'] = floatval ( $ExtraGuestCharge );
		$data ['ChildCharge'] = floatval ( $ChildCharge );
		$data ['OtherCharges'] = floatval ( $OtherCharges );
		$data ['Discount'] = floatval ( $Discount );
		$data ['ServiceTax'] = floatval ( $ServiceTax );
		$data ['AgentCommission'] = floatval ( $AgentCommission );
		$data ['AgentMarkUp'] = floatval ( $AgentMarkUp );
		$data ['TDS'] = floatval ( $TDS );

		//debug($data); exit;
		
		$status = $this->custom_db->insert_record ( 'hotel_booking_itinerary_details', $data );
		return $status;
	}
	
	/**
	 *
	 * @param
	 *        	$app_reference
	 * @param
	 *        	$title
	 * @param
	 *        	$first_name
	 * @param
	 *        	$middle_name
	 * @param
	 *        	$last_name
	 * @param
	 *        	$phone
	 * @param
	 *        	$email
	 * @param
	 *        	$pax_type
	 * @param
	 *        	$date_of_birth
	 * @param
	 *        	$passenger_nationality
	 * @param
	 *        	$passport_number
	 * @param
	 *        	$passport_issuing_country
	 * @param
	 *        	$passport_expiry_date
	 * @param
	 *        	$status
	 * @param
	 *        	$attributes
	 */
	function save_booking_pax_details($app_reference, $title, $first_name, $middle_name, $last_name, $phone, $email, $pax_type, $date_of_birth, $passenger_nationality, $passport_number, $passport_issuing_country, $passport_expiry_date, $status, $attributes) {
		// echo $date_of_birth;
		$date_of_birth = date("Y-m-d", strtotime($date_of_birth));
		$data ['app_reference'] = $app_reference;
		$data ['title'] = $title;
		$data ['first_name'] = $first_name;
		$data ['middle_name'] = (empty ( $middle_name ) == true ? $last_name : $middle_name);
		$data ['last_name'] = $last_name;
		$data ['phone'] = $phone;
		$data ['email'] = $email;
		$data ['pax_type'] = $pax_type;
		$data ['date_of_birth'] = $date_of_birth;
		$data ['age'] = $passenger_nationality;
		$data ['passport_number'] = $passport_number;
		$data ['passport_issuing_country'] = $passport_issuing_country;
		$data ['passport_expiry_date'] = $passport_expiry_date;
		$data ['status'] = $status;
		$data ['attributes'] = $attributes;
		
		$status = $this->custom_db->insert_record ( 'hotel_booking_pax_details', $data );
		return $status;
	}
	/**
	 */
	function get_static_response($token_id) {
		$static_response = $this->custom_db->single_table_records ( 'test', '*', array (
				'origin' => intval ( $token_id ) 
		) );
		return json_decode ( $static_response ['data'] [0] ['test'], true );
	}
	
	/**
	 * SAve search data for future use - Analytics
	 * 
	 * @param array $params        	
	 */
	function save_search_data($search_data, $type) {
		$data ['domain_origin'] = get_domain_auth_id ();
		$data ['search_type'] = $type;
		$data ['created_by_id'] = intval ( @$this->entity_user_id );
		$data ['created_datetime'] = date ( 'Y-m-d H:i:s' );
		
		$temp_location = explode ( '(', $search_data ['city'] );
		$data ['city'] = trim ( $temp_location [0] );
		if (isset ( $temp_location [1] ) == true) {
			$data ['country'] = trim ( $temp_location [1], '() ' );
		} else {
			$data ['country'] = '';
		}
		$data ['check_in'] = date ( 'Y-m-d', strtotime ( $search_data ['hotel_checkin'] ) );
		$data ['nights'] = abs ( get_date_difference ( $search_data ['hotel_checkin'], $search_data ['hotel_checkout'] ) );
		$data ['rooms'] = $search_data ['rooms'];
		$data ['total_pax'] = array_sum ( $search_data ['adult'] ) + array_sum ( $search_data ['child'] );
		$this->custom_db->insert_record ( 'search_hotel_history', $data );
	}
	/**
	 * Jaganath
	 * Save Cancellation data
	 * 
	 * @param
	 *        	$app_reference
	 * @param
	 *        	$ChangeRequestId
	 * @param
	 *        	$ChangeRequestStatus
	 * @param
	 *        	$TraceId
	 * @param
	 *        	$attr
	 */
	function save_cancellation_data($app_reference, $ChangeRequestId, $ChangeRequestStatus, $status_description, $TraceId, $API_RefundedAmount, $API_CancellationCharge, $attr) {
		$data = array ();
		$data ['ChangeRequestId'] = $ChangeRequestId;
		$data ['ChangeRequestStatus'] = $ChangeRequestStatus;
		$data ['status_description'] = $status_description;
		//$data ['TraceId'] = $TraceId;
		$data ['API_RefundedAmount'] = (empty ( $API_RefundedAmount ) == false ? $API_RefundedAmount : '');
		$data ['API_CancellationCharge'] = (empty ( $API_CancellationCharge ) == false ? $API_CancellationCharge : '');
		$data ['attributes'] = $attr;
		if ($ChangeRequestStatus == 3) {
			$data ['cancellation_processed_on'] = date ( 'Y-m-d H:i:s' );
		}
		$cancellation_details_exists = $this->custom_db->single_table_records ( 'hotel_cancellation_details', '*', array (
				'app_reference' => $app_reference 
		) );
		if ($cancellation_details_exists ['status'] == true) {
			// Update the Data
			$this->custom_db->update_record ( 'hotel_cancellation_details', $data, array (
					'app_reference' => $app_reference 
			) );
		} else {
			// Insert Data
			$data ['app_reference'] = $app_reference;
			if(is_logged_in_user()){
				$data ['created_by_id'] = $this->entity_user_id;
			}else{
				$data ['created_by_id'] = 0;
			}
			$data ['created_datetime'] = date ( 'Y-m-d H:i:s' );
			$this->custom_db->insert_record ( 'hotel_cancellation_details', $data );
		}
	}
	/*
	 * Jaganath
	 * Update Master Booking Status
	 */
	function update_master_booking_status($app_reference, $booking_status) {
		$update_condition ['app_reference'] = trim ( $app_reference );
		$update_data ['status'] = trim ( $booking_status );
		$GLOBALS ['CI']->custom_db->update_record ( 'hotel_booking_details', $update_data, $update_condition );
	}
	/*
	 * Jaganath
	 * Update Passenger Booking Status
	 */
	function update_pax_booking_status($app_reference, $booking_status) {
		$update_condition ['app_reference'] = trim ( $app_reference );
		$update_data ['status'] = trim ( $booking_status );
		$GLOBALS ['CI']->custom_db->update_record ( 'hotel_booking_pax_details', $update_data, $update_condition );
	}

	function get_hotels_city_info($location_id)
	{
		$data = $this->custom_db->single_table_records('api_hotel_city_list', '*', array('origin' => intval($location_id)));
		return $data;

	}
	public function reviews()
	{
		$query = "select * from user_review where module='hotel' order by origin desc"; echo $query; exit;
		$exe   = mysql_query($query);
		while($fetch = mysql_fetch_assoc($exe))
		{
			$result[] = $fetch;
		}
		return $result;
	}
	
	public function get_top_hotel_desination_title()
	{
		$query = 'select * from top_hotel_desination_title '; //echo $query; exit;
		$exe   = mysql_query($query);
		while($fetch = mysql_fetch_row($exe))
		{
			$result = $fetch;
		}
		return $result;
	}
	public function get_hotel_country_name($country_code){
		 $this->db->select('country_name');
		 $this->db->from('hb_activity_countries');
         $this->db->where('country_code',$country_code);
         $result = $this->db->get();
        
         return $result->row();
	}
	public function get_hotel_contact($hotel_code,$booking_source='',$city_code=''){

		if($booking_source == "PTBSID0000000043"){
			$this->db->select('Telephone as phone_number');
			$this->db->from('hb_hotel_contacts');
	        $this->db->where('HotelCode',$hotel_code);
	        $this->db->where('CityCode',$city_code);
		}else{
			$this->db->select('phone_number');
			$this->db->from('hb_hotel_contacts');
	        $this->db->where('hotel_code',$hotel_code);
	        $this->db->where('phone_type','PHONEHOTEL');
		}
        $result = $this->db->get();
        return $result->row();
	}

	function get_trip_adv($hotel_code){
		
		//echo $hotel_code; exit;
		$this->db->select('*');
		$this->db->from('trip_advisor');
	    $this->db->where('hotel_code',$hotel_code);
		//$query = 'SELECT * FROM `trip_advisor` WHERE hotel_code =$hotel_code';
		$query = $this->db->get();
		$num = $query->num_rows();

		if($num){
			$data['result'] = $query->row_array();
			$data['status'] = true;
		}else{
			$data['status'] = false;
		}

		return $data;
	}

	function get_trip_adv_not_exists(){

		$items = array('1'=>1,'2'=>2,'7'=>7,'8'=>8,'9'=>9,'10'=>10,'11'=>11,'12'=>12,'15'=>15,'16'=>16,'18'=>18,'22'=>22,'25'=>25,'26'=>26,'40'=>40,'41'=>41,'42'=>42,'43'=>43,'44'=>44,'46'=>46);
		$this->db->select('*');
		$this->db->from('trip_advisor');
	    $this->db->where('hotel_code', array_rand($items));
		$query = $this->db->get();
		$num = $query->num_rows();
		//echo $this->db->last_query(); exit;
		if($num){
			$data['result'] = $query->row_array();
			$data['status'] = true;
		}else{
			$data['status'] = false;
		}

		return $data;
	}


	function get_hotel_image($hotel_code){
		$query = 'select * from hb_hotel_images where hotel_code = '.$hotel_code;
		
		$data = $this->db->query($query)->row();
		return $data;
	}
	function hotel_apply_cancel($app_reference){
		$data=array("status"=>"CANCELLATION_IN_PROCESS");
		$this->db->where("app_reference",$app_reference);
		$result=$this->db->update("hotel_booking_details",$data);    
		return $result;
	}
	public function get_gta_hotel_contact($hotel_code){		
		$this->db->select('Telephone, Address');		
		$this->db->from('gta_Hotels');		
        $this->db->where('HotelCode',$hotel_code);		
    	$result = $this->db->get();		
        return $result->row();		
	}
}
