<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
	Author: Provab Technosoft Private Limited
	Developer: Poornima K (2054)
	Page Title: Email model
	Purpose: email sending for all user operations.
*/

class Email_Model extends CI_Model {
    
    public function commonMail($subject, $body, $to_email_id, $from_email_id, $from_name){
        $access = $this->get_email_acess()->row();

        $this->load->library('email');
        $config['protocol'] = 'mail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';

        $config['mailtype'] = 'html';
        $config['protocol'] = $access->smtp;
        $config['smtp_host'] = $access->host;
        $config['smtp_port'] = $access->port;
        $config['smtp_user'] = $access->username;
        $config['smtp_pass'] = $access->password;
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $this->email->initialize($config);

        $this->email->from($from_email_id, $from_name);
        $to = $to_email_id;
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($body);

        if ($this->email->send()) {
            return true;
        } else {
            $this->email->print_debugger();
            die;
            return false;
        }
    }

    function get_email_acess()
        {
            $this->db->select('*');
            $this->db->from('email_access');
            $query = $this->db->get();
            if ( $query->num_rows > 0 ) {
             return $query->row();
            }
            return false;
        }


    public function get_email_template($email_type) {
		
		///echo $email_type;exit;
		$this->db->where('email_type', $email_type);
        return $this->db->get('email_template');
        //echo $this->db->last_query();exit;
    }
   
public function sendmail_reg($data){ 
   
    $mail = $data['user_email'];
    $message1 = $data['email_template']->message;
    $company=$this->session->userdata('company_name');
     if($data['profile_photo'] == ''){
            $profile_photo = 'http://provabextranet.com/WDMA/TCG-DEMO/assets/images/user-avatar.jpg';
        }else{
            $profile_photo = 'http://provabextranet.com/WDMA/TCG-DEMO/assets/images/user-avatar.jpg';
        }
    $message1 = str_replace("{%%FIRSTNAME%%}", $data['name'], $message1);
    $message1 = str_replace("{%%ACCOUNTNO%%}", $data['user_acc_no'], $message1);
    $message1 = str_replace("{%%USERNAME%%}", $data['name'], $message1);
    $message1 = str_replace("{%%USEREMAIL%%}", $data['user_email'], $message1);
    $message1 = str_replace("{%%USERIMAGE%%}", $profile_photo, $message1);
    $message1 = str_replace("{%%COMPANY%%}",$company , $message1);
    $subject = $data['email_template']->subject;
    $this->load->library('provab_mailer');
    $this->provab_mailer->send_mail($mail,'New User Registeration',$message1);

   }
   
   
   
     
public function sendmail_guest_reg($data){ 
   
  
    $mail = $data['user_email'];
    $message1 = $data['email_template']->message;
     if($data['profile_photo'] == ''){
            $profile_photo = 'http://provabextranet.com/WDMA/TCG-DEMO/assets/images/user-avatar.jpg';
        }else{
            $profile_photo = 'http://provabextranet.com/WDMA/TCG-DEMO/assets/images/user-avatar.jpg';
        }
    $message1 = str_replace("{%%FIRSTNAME%%}", $data['name'], $message1);
    $message1 = str_replace("{%%PASSWORD%%}", $data['password'], $message1);
  // $message1 = str_replace("{%%USERNAME%%}", $data['name'], $message1);
    $message1 = str_replace("{%%USEREMAIL%%}", $data['user_email'], $message1);
    $message1 = str_replace("{%%USERIMAGE%%}", $profile_photo, $message1);
    $subject = $data['email_template']->subject;

 

    $data['email_access'] = $this->get_email_acess();
   $delimiters = $data['user_email'];
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
            
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
        $this->email->to($mail);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
   }
   
   
   // The is commented by Dinesh

     /*public function sendmail_forgot_password($data) {
		
        $message1 = $data['email_template']->message; 
        $message1 = str_replace("{%%FIRSTNAME%%}", str_replace('-', ' ',$data['user_data']->user_name), $message1);
        $message1 = str_replace("{%%USERNAME%%}", $data['user_data']->user_email, $message1);

        if($data['user_data']->user_profile_pic=='') { 
           $prof_pic="http://provabextranet.com/WDMA/TCG-DEMO/assets/images/user-avatar.jpg";
        } else { 
            $prof_pic="http://provabextranet.com/cpanel/uploads/users/".$data['user_data']->user_profile_pic;
        }
        $message1 = str_replace("{%%USERIMAGE%%}", $prof_pic, $message1);
        $message1 = str_replace("{%%WEB_URL%%}", base_url(), $message1);
        $message1 = str_replace("{%%RESETLINK%%}", $data['reset_link'], $message1);
       
        $subject = $data['email_template']->subject;
        $subject = str_replace("{%%USER%%}", str_replace('-', ' ',$data['user_data']->user_name), $subject);
       
        $data['email_access'] = $this->get_email_acess();
         $delimiters = $data['user_data']->user_email;
        $config = Array(
            'protocol' => $data['email_access']->smtp,
           'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );  
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_template']->email_from, $data['email_template']->email_from_name);
        $this->email->to($data['user_data']->user_email, $delimiters);
        $this->email->subject($data['email_template']->subject);
        $this->email->message($message1);
        $this->email->send();
        return 1;
    }*/

//
    /**
    * Dinesh Kumar
    * Email Template
    **/
    public function sendmail_forgot_password($data) {
        $this->load->library("email");
        $message1 = $data['email_template']->message; 
        $message1 = str_replace("{%%FIRSTNAME%%}", str_replace('-', ' ',$data['user_data']->user_name), $message1);
        $message1 = str_replace("{%%USERNAME%%}", $data['user_data']->user_email, $message1);

        if($data['user_data']->user_profile_pic=='') { 
           $prof_pic="http://provabextranet.com/WDMA/TCG-DEMO/assets/images/user-avatar.jpg";
        } else { 
            $prof_pic="http://provabextranet.com/cpanel/uploads/users/".$data['user_data']->user_profile_pic;
        }
        $message1 = str_replace("{%%USERIMAGE%%}", $prof_pic, $message1);
        $message1 = str_replace("{%%WEB_URL%%}", base_url(), $message1);
        $message1 = str_replace("{%%RESETLINK%%}", $data['reset_link'], $message1);
       
        $subject = $data['email_template']->subject;
        $subject = str_replace("{%%USER%%}", str_replace('-', ' ',$data['user_data']->user_name), $subject);
       
        $data['email_access'] = $this->get_email_acess();
         $delimiters = $data['user_data']->user_email;
        $config = Array(
            'protocol' => $data['email_access']->smtp,
           'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );  
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_template']->email_from, $data['email_template']->email_from_name);
        $this->email->to($data['user_data']->user_email, $delimiters);
        //$this->email->to('dk.provab@gmail.com', $delimiters);
        $this->email->subject($data['email_template']->subject);
        $this->email->message($message1);
        // Added by dinesh
        $this->email->set_mailtype("html");
        ////
        $op = $this->email->send();
        //Debugger added by dinesh
       
         //echo $this->email->print_debugger();
        if($op == 1){
            return 1;    
        }else{
            return 0;
        }
        
    }

    //
    /**
    * Dinesh Kumar
    * Ends Here
    **/


	public function send_mail_change_password($data){
		$email_to_2 = '';
		$message1 = $data['email_template']->message;
		$message1 = str_replace("{%%FIRSTNAME%%}", $data['user_name'], $message1);
		$data['email_access'] = $this->get_email_acess();
		$delimiters = $data['email_template']->to_email;
        $email_to = explode(";", $delimiters);
        $email_to_1 = $email_to[0];
        if(isset($email_to[1]))
			$email_to_2 = $email_to[1];
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            //'protocol' => 'mail',
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_template']->email_from, $data['email_template']->email_from_name);
        if($email_to_2!='')
			$this->email->to($data['user_email'], $email_to_1, $email_to_2);
		else
			$this->email->to($data['user_email'], $email_to_1);
        $this->email->subject($data['email_template']->subject);
        $this->email->message($message1);
        $this->email->send();
   
   }
	
 public function SubAgentRegistration($data){
        $message1 = $data['email_template']->message;
        $message1 = str_replace("{%%FIRSTNAME%%}", $data['user_data']->firstname, $message1);
        $message1 = str_replace("{%%USERNAME%%}", $data['user_data']->email_id, $message1);
        $message1 = str_replace("{%%PASSWORD%%}", $data['user_data']->password, $message1);
        $message1 = str_replace("{%%URL%%}", WEB_URL.'/agent/login', $message1);
        $message1 = str_replace("{%%USER%%}", 'Agent', $message1);
        $message1 = str_replace("{%%USERIMAGE%%}", $data['user_data']->agent_logo, $message1);
        $message1 = str_replace("{%%WEB_URL%%}", WEB_URL, $message1);
        $subject = $data['email_template']->subject;
        $subject = str_replace("{%%USER%%}", 'Agent', $subject);

        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_template']->email_from, $data['email_template']->email_from_name);
        $this->email->to($data['user_data']->email_id);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
   }    

    public function sendmail_agentreg($data) {
        $message = $data['email_template']->message;
        $social_url = $data['social_url'];
        $delimiters = $data['email_template']->to_email;
        $email_to = explode(";", $delimiters);
        $email_to_1 = $email_to[0];
        
        if($data['user_data']->agent_logo == ''){
            $agent_logo = 'http://provabextranet.com/TCG-DEMO/assets/images/user-avatar.jpg';
        }else{
            $agent_logo = $data['user_data']->agent_logo;
        }
        
        $message = str_replace("{%%FIRSTNAME%%}", $data['user_data']->firstname, $message);
        $message = str_replace("{%%USERIMAGE%%}", $agent_logo, $message);
        
         $message = str_replace("{%%WEB_URL%%}", WEB_URL, $message);
        $message = str_replace("{%%SOCIALURLFACE%%}", $social_url['facebook_social_url'], $message);
        $message = str_replace("{%%SOCIALURLTWIT%%}", $social_url['twitter_social_url'], $message);
        $message = str_replace("{%%SOCIALURLGPLUS%%}",$social_url['google_social_url'], $message);
        
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_template']->email_from, $data['email_template']->email_from_name);
        $this->email->to($data['user_data']->email_id);
        $this->email->subject($data['email_template']->subject);
        $this->email->message($message);
        $this->email->send();
    }

   public function sendmail_agentVerification($data) {
        $message = $data['email_template']->message;
        $delimiters = $data['email_template']->to_email;
        $email_to = explode(";", $delimiters);
        $email_to_1 = $email_to[0];
       
        if($data['user_data']->agent_logo == ''){
            $agent_logo = 'http://provabextranet.com/TCG-DEMO/assets/images/user-avatar.jpg';
        }else{
            $agent_logo = $data['user_data']->agent_logo;
        }
        $message = str_replace("{%%FIRSTNAME%%}", $data['user_data']->firstname, $message);
        $message = str_replace("{%%VERIFICATION_CODE%%}", $data['random_code'], $message);
        $message = str_replace("{%%WEB_URL%%}", WEB_URL, $message);
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_template']->email_from, $data['email_template']->email_from_name);
        $this->email->to($data['user_data']->email_id);
        $this->email->subject($data['email_template']->subject);
        $this->email->message($message);
        $this->email->send();
    }
    
    public function send_contactus_mail($data,$input){
    	$contactdata = $input['message'];
    	$message = $data['email_template']->message;
        $delimiters = $data['email_template']->to_email;
        $email_to = explode(";", $delimiters);
        $email_to_1 = $email_to[0];
        $to_email  = array($input['email'], $data['email_template']->email_from); 
        $message = str_replace("{%%FIRSTNAME%%}", $input['name'], $message);
        $message = str_replace("{%%MESSAGE%%}", $contactdata, $message);
          $message = str_replace("{%%WEB_URL%%}", WEB_URL, $message);
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_template']->email_from, $data['email_template']->email_from_name);
        $this->email->to($to_email);
        $this->email->subject($input['subject']);
        $this->email->message($message);
        if(!$this->email->send()){
        	return false;
        }else{
        	return true;
        }
    }
 public function send_contactus_mail_v1($data,$input){
    	$contactdata = $input['message'];
    	$message = $data['email_template']->message;
        $delimiters = $data['email_template']->to_email;
        $email_to = explode(";", $delimiters);
        $email_to_1 = $email_to[0];
        $message = str_replace("{%%FIRSTNAME%%}", $input['name'], $message);
        $message = str_replace("{%%MESSAGE%%}", $contactdata, $message);
          $message = str_replace("{%%WEB_URL%%}", WEB_URL, $message);
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($input['email'], $input['name']);
        $this->email->to($email_to_1);
        $this->email->subject($input['subject']);
        $this->email->message($message);
        if(!$this->email->send()){
        	return false;
        }else{
        	return true;
        }
    }
    
    public function sendmail_hotelVoucher($email) {
       
        $message = $email['message'];

        $to = $email['to'];
        $cc = $email['cc'].",sivaraman.provab@gmail.com";
        
        $subject = 'Hotel Booking Voucher';
       
        $config = Array(
            'protocol' => $email['email_access']->smtp,
            'smtp_host' => $email['email_access']->host,
            'smtp_port' => $email['email_access']->port,
            'smtp_user' => $email['email_access']->username,
            'smtp_pass' => $email['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE
        ); 
        
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        //$this->email->from($email['email_template']->email_from, $email['email_template']->email_from_name);
        $this->email->from("provab.technosoft2016@gmail.com", "provab.technosoft2016");
        $this->email->to($to);
        $this->email->cc($cc);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
      
         if ($this->email->send()) {
            return 1;
        }else{
            return 0;
        }
    }

    public function sendmail_transferVoucher($email) {
       
        $message = $email['message'];

        $to = $email['to'];
        
        $subject = 'Transfer Booking Voucher';
       
        $config = Array(
            'protocol' => $email['email_access']->smtp,
            'smtp_host' => $email['email_access']->host,
            'smtp_port' => $email['email_access']->port,
            'smtp_user' => $email['email_access']->username,
            'smtp_pass' => $email['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE
        ); 
        
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($email['email_template']->email_from, $email['email_template']->email_from_name);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
         if ($this->email->send()) {
            return 1;
        }else{
            return 0;
        }
    }

    public function send_add_user_active($data){ 
	 $mail = $data['user_info'][0]->user_email;
    $message1 = $data['email_template']->message; 
        $message1 = str_replace("{%%FIRSTNAME%%}", $data['user_info'][0]->user_email, $message1);
    $message1 = str_replace("{%%USERNAME%%}", $data['user_info'][0]->user_name, $message1);
    $message1 = str_replace("{%%URL%%}","http://provabextranet.com/WDMA/TCG-DEMO/",$message1);
    $message1 = str_replace("{%%USER%%}", $data['user_info'][0]->user_email, $message1);
    $message1 = str_replace("{%%USERIMAGE%%}", $data['user_info'][0]->user_profile_pic, $message1);
    $subject = $data['email_template']->subject;
    $subject = str_replace("{%%USER%%}", $data['user_info'][0]->user_name, $subject);
    $data['email_access'] = $this->get_email_acess();
    $delimiters = $data['user_info'][0]->user_email;
        $config = Array(
           'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
            
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
        $this->email->to($mail);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
   }

   public function send_agent_deposit_mail($data,$deposit){ 
        $message1 = $data['email_template']->message;
        $message1 = str_replace("{%%FIRSTNAME%%}", $data['user_info'][0]->user_name, $message1);
        $message1 = str_replace("{%%AMOUNT%%}", $deposit[0]->amount_credit, $message1);
        $message1 = str_replace("{%%CURAMOUNT%%}", $deposit[0]->amount_credit, $message1);
        $message1 = str_replace("{%%TRANSID%%}", $deposit[0]->transaction_id, $message1);
        $message1 = str_replace("{%%TOTALAMOUNT%%}", $deposit[0]->amount_credit, $message1);
        $subject = $data['email_template']->subject;
        
        $data['email_access'] = $this->get_email_acess();
        $delimiters = $data['email_template']->to_email;
        $mail = $data['user_info'][0]->user_email;
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
        $this->email->to($mail);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
   }

   public function sendAgentDepositMail($data,$deposit){ 
        $logo= ASSETS."cpanel/uploads/domain/".$this->session->userdata('domain_logo');
        $companyurl=$data[0]->domain_name;   
        $message1 = $data['email_template']->message;
        $message1 = str_replace("{%%LOGOURL%%}", $logo , $message1);
        $message1 = str_replace("{%%COMPANYURL%%}", $companyurl , $message1);
        $message1 = str_replace("{%%FIRSTNAME%%}", $data[0]->user_name, $message1);    
        $message1 = str_replace("{%%AMOUNT%%}", $deposit[0]->amount_credit, $message1);
        $message1 = str_replace("{%%CURAMOUNT%%}", $deposit['current_balance'][0]->balance_credit, $message1); 
        $message1 = str_replace("{%%TRANSID%%}", $deposit[0]->transaction_id, $message1);
        $message1 = str_replace("{%%TOTALAMOUNT%%}", $deposit[0]->amount_credit, $message1);  
        $subject = $data['email_template']->subject;
        $data['email_access'] = $this->get_email_acess();
        $mail = $data['user_info'][0]->user_email; 
        //$mail="pawan.provabmail@gmail.com";   
        $this->load->library('provab_mailer'); 
        $this->provab_mailer->send_mail($mail,'Deposit Notification',$message1);
    } 

    public function sendAgentDepositAlertMail($test,$user,$email,$user_account_number){
        
        
        $message1 = $user.'('.$user_account_number.')'.'Requested you to accept the deposit' .' '. $test['amount_credit'].'('.$test['currencycode'].')';
        $subject = 'Deposit Request Pending';
        $data['email_access'] = $this->get_email_acess();
        $mail = $email;
        //echo 'sanjy'; print_r($message1); exit();
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
        $this->email->to($mail);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
    }

    public function sendAgentSupportAlertMail($test,$user,$user_account_number){
       //echo '<pre>hi'; print_r($test['sub']); exit();
        
        $message1 = $user.'('.$user_account_number.')'.'has started a convesation regarding' .' '. $test['sub'];
        $subject = 'Conversation of Support Ticket';
        $data['email_access'] = $this->get_email_acess();
        $mail = 'Shashi@thechinagap.com';
        //$mail = 'polisettysanjay.provab@gmail.com';
        
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
        $this->email->to($mail);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
    }

    public function sendAgentSupportAlertMailReply($test,$user,$user_account_number){
       //echo '<pre>hi'; print_r($test['sub']); exit();
        
        $message1 = $user.'('.$user_account_number.')'.'has started a convesation regarding' .' '. $test['textcounter'];
        $subject = 'Conversation of Support Ticket';
        $data['email_access'] = $this->get_email_acess();
        $mail = 'Shashi@thechinagap.com';
        //$mail = 'polisettysanjay.provab@gmail.com';
        
        $config = Array(
            'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email_access']->username);
        $this->email->to($mail);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
    }
    
    function sendDepositeNotification($mail_data){
	
	$message1 = $mail_data['message']; 
    $message1 = str_replace("{%%FIRSTNAME%%}", $mail_data['user_name'], $message1);
	$message1 = str_replace("{%%AMOUNT%%}", $mail_data['balance'], $message1);
	if($mail_data['user_type'] == 2) {
	$message1 = str_replace("{%%TYPE%%}", 'B2B', $message1);
    }else{
	$message1 = str_replace("{%%TYPE%%}", 'B2B2B', $message1);	
	}
    $subject = $mail_data['subject'];
   
	$data['email_access'] = $this->get_email_acess();
	        $config = Array(
	        'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        	
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($mail_data['from']);
         $this->email->to($mail_data['to']);
         $this->email->cc($mail_data['cc']);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
	}
	
	 function sendPaymentNotification($mail_data){
	
	$message1 = $mail_data['message']; 
    $message1 = str_replace("{%%FIRSTNAME%%}", $mail_data['user_name'], $message1);
	$message1 = str_replace("{%%BOOKINGS%%}", $mail_data['bookings'], $message1);
    $subject = $mail_data['subject'];
   
	$data['email_access'] = $this->get_email_acess();
	        $config = Array(
	        'protocol' => $data['email_access']->smtp,
            'smtp_host' => $data['email_access']->host,
            'smtp_port' => $data['email_access']->port,
            'smtp_user' => $data['email_access']->username,
            'smtp_pass' => $data['email_access']->password,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        	
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($mail_data['from']);
         $this->email->to($mail_data['to']);
         $this->email->cc($mail_data['cc']);
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();
	}
   
        public function sendmail_deposit($data,$user_data){  
        $logo =ASSETS."cpanel/uploads/domain/".$this->session->userdata('domain_logo');
        $mail = $user_data['user_email'];   
        $mail = "pawan.provabmail@gmail.com";    
        $domain_name=$this->session->userdata('company_name');   
        $message1 = $data['email_template']->message;    
        $message1 = str_replace("{%%LOGOURL%%}", $logo, $message1);
        $message1 = str_replace("{%%COMPANYURL%%}",$domain_name, $message1);    
        $message1 = str_replace("{%%FIRSTNAME%%}", $user_data['user_name'], $message1);
        $message1 = str_replace("{%%AMOUNT%%}", $user_data['amount'], $message1);
        $message1 = str_replace("{%%CURAMOUNT%%}", $user_data['current_amount'], $message1);
        $subject = $data['email_template']->subject; 
        $this->load->library('provab_mailer');     
        $email_status=$this->provab_mailer->send_mail($mail,'New Notfication',$message1);
            if($email_status== TRUE)
            {  
                return true; 
            }   
            else
            {
                return false;   
            }    
        }   

}
