<?php
class Staffmanagement_Model extends CI_Model {
   function __construct(){
         parent::__construct();
    }
    
   function get_staff_roles($branch_id = ''){
	   $this->db->select('*');
	   $this->db->from('agent_roles');
	   if($branch_id != ''){
	   $this->db->where('agent_branch_id', $branch_id);
	   }
	   return $this->db->get()->result();
   }
   
   function add_agent_roles($data){
	 $post_data = $this->role_form_data($data);
	 $post_data['role_created_by_id'] = $this->session->userdata('user_details_id');
	 $post_data['role_created_date'] =  date('Y-m-d');
	 $this->db->insert("agent_roles", $post_data);
	 return;
	 }
  
   
    function update_agent_roles($data){
	 $post_data = $this->role_form_data($data);
	 $post_data['role_modified_by_id'] = $this->session->userdata('user_details_id');
	 $post_data['role_modified_date'] =  date('Y-m-d');
	 $this->db->insert("agent_roles", $post_data);
	 return;
	 }
	 
	 function get_dashboard_details(){
		 $this->db->select('*');
		 $this->db->from('homepage_leftmenu');
		 $this->db->where('dashboard_status', 'ACTIVE');
		 $this->db->order_by('position', 'asc');
		 	if($query->num_rows() ==''){
			$data['dashboard_info'] = '';
		}else{
			$data['dashboard_info'] = $query->result();
		}
		if($data['user_info']!=''){
			for($u=0;$u<count($data['user_info']);$u++){
				$data['domain_details'][$u] 	= $this->Users_Model->get_domain_details($data['user_info'][$u]->user_details_id);
				
			}
		}
		return $data;
	 }
	  
   function role_form_data($data){
	   $post_data = array("agent_roles_name" => $data['role_name'],
	                      "agent_branch_id" => $this->session->userdata('branch_id'),
	                      "role_status" => 1);
	    return $post_data;
   }
   
   function activate_roles($roles_id){
	   $data = array('role_status' => 1,
	                 'role_modified_by_id' => $this->session->userdata('user_details_id'), 
	                 'role_modified_date' => date('Y-m-d'));
	   $this->db->where("agent_roles_id", $roles_id);
	   $this->db->update("agent_roles", $data);
   }
   
   function inactivate_roles($roles_id){
	   $data = array('role_status' => 0,
	                 'role_modified_by_id' => $this->session->userdata('user_details_id'), 
	                 'role_modified_date' => date('Y-m-d'));
	   $this->db->where("agent_roles_id", $roles_id);
	   $this->db->update("agent_roles", $data);
	   return;
	}
	
	function update_privileges($data){
		
		$role_id = json_decode(base64_decode($data['role_id']));
		$this->db->where("user_role_id", $role_id);
		$this->db->where("user_branch_id", $this->session->userdata('branch_id'));
		$this->db->delete("user_privileges");
		if(count($data['user_privilege']) > 0){
			for($i=0; $i < count($data['user_privilege']); $i++){
				  $datas = array(
				                 "dashbaord_id" => $data['user_privilege'][$i],
				                 "user_branch_id" => $this->session->userdata('branch_id'),
				                 "user_role_id" => $role_id,
				                 "user_type_id" => 2);
				 $this->db->insert("user_privileges", $datas);
			 }
			
		}
		return;
	}

	

}
?>
