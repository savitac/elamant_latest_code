<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart_Model extends CI_Model {

  public function getBookingTemp($session_id){
        $this->db->where('session_id',$session_id);
		$this->db->join('product_details','cart_global.product_id = product_details.product_details_id');
        return $this->db->get('cart_global');
    }
	
	public function getBookingTemp_flight($cart_global_id){
        $this->db->where('cart_id',$cart_global_id);
        $this->db->join('cart_flight','cart_flight.referal_id = cart_global.referal_id');
       
		$query = $this->db->get('cart_global');
		if($query->num_rows() != 0 )
		{
			return $query->row();
		}
		else
		{
			return '';
		}
		
    }
     public function get_meal($session_id){
        $this->db->select('meal');
        $this->db->where('session_id',$session_id);
		return $this->db->get('cart_flight');
    }

    public function getBookingTemp_hotel($cart_global_id){
        $this->db->where('cart_id',$cart_global_id);
        $this->db->join('cart_hotel','cart_hotel.id = cart_global.referal_id');
       
		$query = $this->db->get('cart_global');
		
		if($query->num_rows() != 0 )
		{
			return $query->row();
		}
		else
		{
			return '';
		}
		
    }
    
     public function getBookingTemp_transfer($cart_global_id){
        $this->db->where('cart_id',$cart_global_id);
        $this->db->join('cart_transfer','cart_transfer.cart_transfer_id = cart_global.referal_id');
       
		$query = $this->db->get('cart_global');
		
		if($query->num_rows() != 0 )
		{
			return $query->row();
		}
		else
		{
			return '';
		}
		
    }


    public function getBookingTemp_hoteloder($cart_global_id){
        $this->db->where('cart_global.referal_id',$cart_global_id);
        $this->db->join('cart_hotel','cart_hotel.id = cart_global.referal_id');
       
		$query = $this->db->get('cart_global');
		//echo $this->db->last_query();exit;
		if($query->num_rows() != 0 )
		{
			return $query->row();
		}
		else
		{
			return '';
		}
		
    }
    
	/*public function getBookingTemp_hotel($cart_global_id){
        $this->db->where('cart_id',$cart_global_id);
        $this->db->join('cart_hotel','cart_hotel.cart_hotel_id = cart_global.referal_id');
       
		$query = $this->db->get('cart_global');
		if($query->num_rows() != 0 )
		{
			return $query->row();
		}
		else
		{
			return '';
		}
		
    }*/
    public function getBookingTemp_car($cart_global_id){
        $this->db->where('cart_id',$cart_global_id);
        $this->db->join('cart_car','cart_car.car_cart_id = cart_global.referal_id');
       
		$query = $this->db->get('cart_global');
		if($query->num_rows() != 0 )
		{
			return $query->row();
		}
		else
		{
			return '';
		}
		
    }
	public function getBookingTemphotel($cart_global_id){
        $this->db->where('id',$cart_global_id);
       // $this->db->join('cart_global','cart_global.referal_id = cart_hotel.cart_hotel_id');      
		$query = $this->db->get('cart_hotel');
		//echo $this->db->last_query();exit;
		if($query->num_rows() != 0 )
		{
			return $query->row();
		}
		else
		{
			return '';
		}
		
    }
    public function getcancelpolicy($api_temp_hotel_id){
        $this->db->where('api_temp_hotel_id',$api_temp_hotel_id);
       	$query = $this->db->get('api_hotel_detail_t');
		if ($query->num_rows() == '') {
		return '';
		} else {
		return $query->result_array();
		}
	}
    
    

	public function getCartDataByModule($cart_id,$module){
		$this->db->where('cart_id',$cart_id);
		if($module == 'Hotels'){
			$this->db->join('cart_hotel','cart_hotel.id = cart_global.referal_id');
		}
		if($module == 'Transfer'){
			$this->db->join('cart_transfer','cart_transfer.cart_transfer_id = cart_global.referal_id');
		}
		return $this->db->get('cart_global');
	}
	public function clearCart($session_id) {
		$tables = array('cart_hotel', 'cart_global');
		$this->db->where('session_id',$session_id);
		$this->db->delete($tables);
	}
	
	function update_cart($schema, $update_data, $condition) {
		$this->db->update($schema, $update_data, $condition);
	}
}

?>
