<?php
require_once 'abstract_management_model.php';
/**
 * @package    current domain Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class Domain_Management_Model extends Abstract_Management_Model
{
	private $airline_markup;
	private $hotel_markup;
	private $bus_markup;
	var $verify_domain_balance;

	function __construct() {
		parent::__construct('level_2');
		$this->verify_domain_balance = $this->config->item('verify_domain_balance');
		$this->load->library('Application_Logger');
	}

	/**
	 * Arjun J Gowda
	 * Get markup based on different modules
	 * @return array('value' => 0, 'type' => '')
	 */
	function get_markup($module_name)
	{

		$markup_data = '';
		switch ($module_name) {
			case 'flight' : $markup_data = $this->airline_markup();
			break;
			case 'hotel' : $markup_data = $this->hotel_markup();
			break;
			case 'bus' : $markup_data = $this->bus_markup();
			break;
		}

		return $markup_data;
	}
	function agent_domain_markup($module_name){
		$markup_data = '';
		switch ($module_name) {
			case 'flight' : $markup_data = $this->agent_airline_markup();
			break;
			case 'hotel' : $markup_data = $this->agent_hotel_markup();
			break;
			case 'bus' : $markup_data = $this->agent_bus_markup();
			break;
		} 
		return $markup_data;
	}

	function subagent_domain_markup($module_name){
		$markup_data = '';
		switch ($module_name) {
			case 'flight' : $markup_data = $this->subagent_airline_markup();
			break;
			/*case 'hotel' : $markup_data = $this->subagent_hotel_markup();
			break;
			case 'bus' : $markup_data = $this->subagent_bus_markup();
			break;*/
		} 
		return $markup_data;
	}

	function get_branchmarkup($module_name){
		$markup_data = '';
		switch($module_name){
			case 'flight': $markup_data = $this->airline_branchmarkup();
			break;
			/*case 'hotel' : $markup_data = $this->hotel_branchmarkup();
			break;*/
			case 'bus' : $markup_data = $this->bus_branchmarkup();
			break;
		}
		
		return $markup_data;
	}

	/**
	 * 
	 * Arjun J Gowda
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function airline_markup()
	{
		if (empty($this->airline_markup) == true) {
			$response['specific_markup_list'] = array();
			$specific_ailine_markup_list = $this->specific_airline_markup('b2c_flight');
			$response['specific_markup_list'] = $specific_ailine_markup_list;
			$response['generic_markup_list'] = $this->generic_domain_markup('b2c_flight');
			$this->airline_markup = $response;
		} else {
			$response = $this->airline_markup;
		}
		return $response;
	}

	function agent_airline_markup()
	{
		if (empty($this->agent_airline_markup) == true) {
			$response['agent_specific_markup_list'] = array();
			$agent_specific_ailine_markup_list = $this->agent_specific_airline_markup('b2c_flight');
			$response['agent_specific_markup_list'] = $agent_specific_ailine_markup_list;
			$response['agent_generic_markup_list'] = $this->agent_generic_domain_markup('b2c_flight');
			$this->agent_airline_markup = $response;
		} else {
			$response = $this->agent_airline_markup;
		}
		return $response;
	}
	function subagent_airline_markup()
	{
		if (empty($this->subagent_airline_markup) == true) {
			$response['subagent_specific_markup_list'] = array();
			$subagent_specific_ailine_markup_list = $this->subagent_specific_airline_markup('b2c_flight');
			$response['subagent_specific_markup_list'] = $subagent_specific_ailine_markup_list;
			$response['subagent_generic_markup_list'] = $this->subagent_generic_domain_markup('b2c_flight');
			$this->subagent_airline_markup = $response;
		} else {
			$response = $this->subagent_airline_markup;
		}
		return $response;
	}
	function airline_branchmarkup(){
		if (empty($this->airline_branchmarkup) == true) {
			$response['specific_branchmarkup_list'] = array();
			$specific_branchailine_markup_list = $this->specific_branchairline_markup('b2c_flight');
			$response['specific_branchmarkup_list'] = $specific_branchailine_markup_list;
			$response['generic_branchmarkup_list'] = $this->agent_generic_domain_markup('b2c_flight');
			$this->airline_branchmarkup = $response;
		} else {
			$response = $this->airline_branchmarkup;
		}
		return $response;	
	}

	/*function hotel_branchmarkup(){
		if (empty($this->hotel_branchmarkup) == true) {
			$response['specific_branchmarkup_list'] = array();
			$specific_branchhotel_markup_list = $this->specific_branchhotel_markup('b2c_hotel');
			$response['specific_branchmarkup_list'] = $specific_branchhotel_markup_list;
			$response['generic_branchmarkup_list'] = $this->generic_branchdomain_markup('b2c_hotel');
			$this->hotel_branchmarkup = $response;
		} else {
			$response = $this->hotel_branchmarkup;
		}
		return $response;	
	}*/
	function bus_branchmarkup()
	{
		if (empty($this->bus_branchmarkup) == true) {
			$response['specific_branchmarkup_list'] = array();
/*			$specific_branchbus_markup_list = $this->specific_branchbus_markup('b2c_bus');
			$response['specific_branchmarkup_list'] = $specific_branchbus_markup_list;*/
			$response['generic_branchmarkup_list'] = $this->generic_branchdomain_markup('b2c_bus');
			$this->bus_branchmarkup = $response;
		} else {
			$response = $this->bus_branchmarkup;
		}
		return $response;
	}


	function specific_branchairline_markup(){
		return '';
	}
function agent_specific_airline_markup(){
	return '';
}
function subagent_specific_airline_markup(){
	return '';
}


	/**
	 * Arjun J Gowda
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function hotel_markup()
	{
		if (empty($this->hotel_markup) == true) {
			$response['specific_markup_list'] = '';
			$response['generic_markup_list'] = $this->generic_domain_markup('b2c_hotel');
			$this->hotel_markup = $response;
		} else {
			$response = $this->hotel_markup;
		}
		return $response;
	}

	function agent_hotel_markup(){

		if (empty($this->agent_hotel_markup) == true) {
			$response['agent_specific_markup_list'] = '';
			$response['agent_generic_markup_list'] = $this->agent_generic_domain_markup('b2c_hotel');
			$this->agent_hotel_markup = $response;
		} else {
			$response = $this->agent_hotel_markup;
		}
		return $response;
	}
	function subagent_generic_domain_markup($module_type)
	{
		if($this->markup_level == 'level_2'){
			$markup_level = 4;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 1;
		}
		if($module_type == 'b2c_bus'){
			$module_type = 5;
		}
		$query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM b2b2b_agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and 	ML.markup_type="GENERAL" AND markup_status = "ACTIVE" AND user_details_id = "'.$this->session->userdata('user_details_id').'" AND created_branch_id = "'.$this->session->userdata('branch_id').'"';
		//echo $query_check_value_type; exit;
		$value_type = $this->db->query($query_check_value_type)->result_array();
		/*echo '<pre>';
		print_r($value_type);
		exit;*/
		return $value_type;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function bus_markup()
	{
		if (empty($this->bus_markup) == true) {
			$response['specific_markup_list'] = '';
			$response['generic_markup_list'] = $this->generic_domain_markup('b2c_bus');
			$this->bus_markup = $response;
		} else {
			$response = $this->bus_markup;
		}
		return $response;
	}

	function agent_bus_markup()
	{
		if (empty($this->bus_markup) == true) {
			$response['agent_specific_markup_list'] = '';
			$response['agent_generic_markup_list'] = $this->agent_generic_domain_markup('b2c_bus');
			$this->agent_bus_markup = $response;
		} else {
			$response = $this->agent_bus_markup;
		}
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Get generic markup based on the module type
	 * @param $module_type
	 * @param $markup_level
	 */
	function generic_domain_markup($module_type)
	{
		//echo $module_type; exit;
		if($this->markup_level == 'level_2'){
			$markup_level = 0;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 1;
		}
		if($module_type == 'b2c_bus'){
			$module_type = 5;
		}
		/*$query = 'SELECT ML.origin AS markup_origin, ML.type AS markup_type, ML.reference_id, ML.value, ML.value_type, ML.markup_currency AS markup_currency
		FROM markup_list AS ML where ML.value != "" and ML.module_type = "'.$module_type.'" and
		ML.markup_level = "'.$this->markup_level.'" and ML.type="generic" and ML.domain_list_fk='.get_domain_auth_id();*/

		/* Checks Value type Precentage / Fixed */
		$query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and 	ML.markup_type="GENERAL" AND markup_status = "ACTIVE"';

		/*echo 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and 	ML.markup_type="GENERAL" AND markup_status = "ACTIVE"';
		exit;*/

		$value_type = $this->db->query($query_check_value_type)->result_array();
		
		if(!empty($value_type))
		{
		 switch ($value_type[0]['value_type']) {
		 	case 'Percentage':
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
		 	
		 	case 'Fixed':
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_fixed !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
		 		default:
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
		 		
		 }
			
		$generic_data_list = $this->db->query($query)->result_array();
		}
		else
		{
			$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
			$generic_data_list = $this->db->query($query)->result_array();	
		}
		
		return $generic_data_list;
		//}
	}

	function agent_generic_domain_markup($module_type)
	{
		$user_type=$this->session->userdata('user_type');
		if($this->markup_level == 'level_2'){
			$markup_level = 2;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 1;
		}
		if($module_type == 'b2c_bus'){
			$module_type = 5;
		}
		/*$query = 'SELECT ML.origin AS markup_origin, ML.type AS markup_type, ML.reference_id, ML.value, ML.value_type, ML.markup_currency AS markup_currency
		FROM markup_list AS ML where ML.value != "" and ML.module_type = "'.$module_type.'" and
		ML.markup_level = "'.$this->markup_level.'" and ML.type="generic" and ML.domain_list_fk='.get_domain_auth_id();*/

		/* Checks Value type Precentage / Fixed */

		/*echo $this->session->userdata('branch_id').'<br/>';
		echo $this->session->userdata('user_details_id');
		exit;*/
		
		$query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$user_type.'"AND markup_status = "ACTIVE" AND created_branch_id = "'.$this->session->userdata('branch_id').'"';


		/*echo 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$user_type.'"AND markup_status = "ACTIVE" AND created_branch_id = "'.$this->session->userdata('branch_id').'"';
		exit;*/

		$value_type = $this->db->query($query_check_value_type)->result_array();

		
		/*if(!empty($value_type))
		{
		 switch ($value_type[0]['value_type']) {
		 	case 'Percentage':
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
		 	
		 	case 'Fixed':
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_fixed !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
		 		default:
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
		 		
		 }
			
		$generic_data_list = $this->db->query($query)->result_array();
		}
		else
		{
			$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
			$generic_data_list = $this->db->query($query)->result_array();	
		}*/
		return $value_type;
		//}
	}

	function generic_branchdomain_markup($module_type){
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 3;
		}
		if($module_type == 'b2c_bus'){
			$module_type = 3;
		}
		if($this->markup_level == 'level_2'){
			$markup_level = 2;
		}
		//$this->session->userdata('user_details_id');
		//$this->session->userdata('branch_id');
		if($this->session->userdata('user_details_id')){
			$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.created_branch_id = "'.$this->session->userdata('branch_id').'" and user_details_id = "'.$this->session->userdata('user_details_id').'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"';
		}else{
			$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.created_branch_id = "'.$this->session->userdata('branch_id').'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"';
		}
			$generic_data_list = $this->db->query($query)->result_array();
			return $generic_data_list;
	}

	/**
	 *  Arjun J Gowda
	 * Get specific markup based on module type
	 * @param string $module_type	Name of the module for which the markup has to be returned
	 * @param string $markup_level	Level of markup
	 */
	function specific_airline_markup($module_type)
	{
		$markup_list = '';
		$query = 'SELECT AL.origin AS airline_origin, AL.name AS airline_name, AL.code AS airline_code,
		ML.origin AS markup_origin, ML.type AS markup_type, ML.reference_id, ML.value, ML.value_type, ML.markup_currency AS markup_currency
		FROM airline_list AS AL JOIN markup_list AS ML where ML.value != "" and
		ML.module_type = "'.$module_type.'" and ML.markup_level = "'.$this->markup_level.'" and AL.origin=ML.reference_id and ML.type="specific"
		and ML.domain_list_fk != 0  and ML.domain_list_fk='.get_domain_auth_id().' order by AL.name ASC';
		$specific_data_list = $this->db->query($query)->result_array();
		if (is_array($specific_data_list) && count($specific_data_list) > 0) {
			foreach ($specific_data_list as $__k => $__v) {
				$markup_list[$__v['airline_code']] = $__v;
			}
		}
		return $markup_list;
	}

	/**
	 * Check if the Booking Amount is allowed on Client Domain
	 */
	function verify_current_balance($amount, $currency)
	{
		$status = FAILURE_STATUS;
		if ($this->verify_domain_balance == true) {
			//OBSELETE - NO USE OF THIS
			if ($amount > 0) {
				$query = 'SELECT DL.balance, CC.country as currency, CC.value as conversion_value from domain_list as DL, currency_converter AS CC where CC.id=DL.currency_converter_fk
			AND DL.status='.ACTIVE.' and DL.origin='.$this->db->escape(get_domain_auth_id()).' and DL.domain_key = '.$this->db->escape(get_domain_key());
				$balance_record = $this->db->query($query)->row_array();
				if ($currency == $balance_record['currency']) {
					$balance = $balance_record['balance'];
					if ($balance >= $amount) {
						$status = SUCCESS_STATUS;
					} else {
						//Notify User about current balance problem
						//FIXME - send email, notification for less balance to domain admin and current domain admin
						$this->application_logger->balance_status('Your Balance Is Very Low To Make Booking Of '.$amount.' '.$currency);
					}
				} else {
					echo 'Under Construction';
					exit;
				}
			}
		} else {
			$status = SUCCESS_STATUS;
		}
		return $status;
	}

	/**
	 *
	 * @param $fare
	 * @param $domain_markup
	 * @param $level_one_markup this is 0 as default as it is not mandatory to keep level one markup
	 */
	function update_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup=0, $convinence=0, $discount=0, $currency='INR', $currency_conversion_rate=1)
	{
		$currency = empty($currency) == true ? get_application_currency_preference(): $currency; 
		$status = FAILURE_STATUS;
		$amount = floatval($fare+$level_one_markup+$convinence-$discount);
		$remarks = $transaction_type.' Transaction was Successfully done';
		 $CI =& get_instance();
         $CI->load->model('user_model');
		$notification_users = $this->user_model->get_admin_user_id();
		$action_query_string = array('app_reference' => $app_reference, 'type' => $transaction_type, 'module' => $this->config->item('current_module'));
		if ($this->verify_domain_balance == true) {
			echo 'We Dont Support This';
			exit;
			if ($amount > 0) {
				//deduct balance and continue
				$this->private_management_model->update_domain_balance(get_domain_auth_id(), (-$amount));
				//Log transaction details

				$this->save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $convinence, $discount, $remarks, $currency, $currency_conversion_rate);
				$this->application_logger->transaction_status($remarks.'('.$amount.')', $action_query_string, $notification_users);
			}
		} else {
			$this->save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $convinence, $discount, $remarks, $currency, $currency_conversion_rate);
			$this->application_logger->transaction_status($remarks.'('.$amount.')', $action_query_string, $notification_users);
			$status = SUCCESS_STATUS;
		}
		return $status;
	}

	/**
	 * Save transaction logging for security purpose
	 * @param string $transaction_type
	 * @param string $app_reference
	 * @param number $fare
	 * @param number $domain_markup
	 * @param number $level_one_markup
	 * @param string $remarks
	 */
	function save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $convinence, $discount, $remarks, $currency='', $currency_conversion_rate=1)
	{
		$currency = empty($currency) == true ? get_application_currency_preference(): $currency;
		$transaction_log['system_transaction_id']	= date('Ymd-His').'-S-'.rand(1, 10000);
		$transaction_log['transaction_type']		= $transaction_type;
		$transaction_log['domain_origin']			= get_domain_auth_id();
		$transaction_log['app_reference']			= $app_reference;
		$transaction_log['fare']					= $fare;
		$transaction_log['level_one_markup']		= $level_one_markup;
		$transaction_log['domain_markup']			= $domain_markup;
		$transaction_log['convinence_fees']			= $convinence;
		$transaction_log['promocode_discount']		= $discount;
		$transaction_log['remarks']					= $remarks;
		$transaction_log['created_by_id']			= intval(@$this->entity_user_id) ;
		$transaction_log['created_datetime']		= date('Y-m-d H:i:s', time());
		
		$transaction_log['currency']				= $currency;
		$transaction_log['currency_conversion_rate']= $currency_conversion_rate;
		$this->custom_db->insert_record('transaction_log', $transaction_log);
	}
}
