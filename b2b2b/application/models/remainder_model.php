<?php 
class Remainder_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
   function user_deposite_details()
	{
		$this->db->select("*");
		$this->db->from("user_details");
		$this->db->join("user_accounts", "user_details_id = user_id");
		$this->db->where("balance_credit <", "200.00");
		return $this->db->get()->result();
    }
    
   function get_b2buser_emailid($user_id){
	   $this->db->select('user_email');
	   $this->db->from('user_details');
	   $this->db->where('user_details_id', $user_id);
	   return $this->db->get();
   }
   function getagents($user_type){
	   $this->db->select('*');
	   $this->db->from('user_details');
	   $this->db->where('user_type_id', $user_type);
	   $this->db->where('user_status', 'ACTIVE');
	   $this->db->where('branch_id', 0);
	   return $this->db->get();
   }
   
   function get_user_bookings($user_id){
	   $this->db->select('*');
	   $this->db->from('booking_global');
	   $this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id');
	   $this->db->where('booking_global.booking_status', 'CONFIRMED');
	   $this->db->where('booking_payment.payment_type', 'PAYLATER');
	   $this->db->where('booking_payment.payment_status', 'PENDING');
	   $this->db->where('branch_id' , $user_id);
	   return $this->db->get();
   }
   
   function get_pending_hotel_bookings($hotel_booking_id){
	   $sql = "select * from booking_global, booking_hotel , booking_transaction where booking_hotel_id in(".$hotel_booking_id.") and booking_hotel_id = referal_id
	   and booking_transaction.booking_transaction_id = booking_global.booking_transaction_id";
	   return $this->db->query($sql);
   }
}
?>
