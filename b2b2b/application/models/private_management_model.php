<?php

require_once 'abstract_management_model.php';
/**
 * @package    Provab Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */

Class Private_Management_Model extends Abstract_Management_Model
{
	private $airline_markup;
	private $hotel_markup;
	private $bus_markup;

	function __construct() {

		parent::__construct('level_1');
	}

	/**
	 * Arjun J Gowda
	 * Get markup based on different modules
	 * @return array('value' => 0, 'type' => '')
	 */
	function get_markup($module_name)
	{
		$markup_data = '';
		switch ($module_name) {
			case 'flight' : $markup_data = $this->airline_markup();
			break;
			case 'hotel' : $markup_data = $this->hotel_markup();
			break;
			case 'bus' : $markup_data = $this->bus_markup();
			break;
			default : $markup_data = array('value' => 0, 'type' => '');
			break;
		}
		return $markup_data;
	}

	/**
	 * Get Convinence fees of module
	 */
	function get_convinence_fees($module_name, $search_id,$domain_id)
	{
		$convinence_fees = '';
		switch ($module_name) {
			case 'flight' : $convinence_fees = $this->airline_convinence_fees($search_id,$domain_id);
			//Calculate Convenience fees
			break;
			case 'hotel' : $convinence_fees = $this->hotel_convinence_fees($search_id,$domain_id);
			break;
			case 'transferv1' : $convinence_fees = $this->convinence_fees($search_id,$domain_id,$module_name);
			break;
			case 'sightseeing' : $convinence_fees = $this->convinence_fees($search_id,$domain_id,$module_name);
			break;
			case 'bus' : $convinence_fees = $this->convinence_fees($search_id,$domain_id,$module_name);
			break;
			default : $convinence_fees = array('value' => 0, 'type' => '', 'per_pax' => true);
			break;
		}
		return $convinence_fees;
	}

	function get_commission($module_name, $module)
    {
        $commission_data = '';
        switch ($module_name) {
            case 'flight' : $commission_data = $this->airline_commission($module);
            break;
            case 'bus' : $commission_data = $this->bus_commission($module);
            break;
            case 'sightseeing':$commission_data = $this->sightseeing_commission($module);
            break;
            case 'transferv1':$commission_data = $this->transfer_commission($module);
            break;
        }
        return $commission_data;
    }

    function airline_commission($module)
	{
		if (empty($this->airline_commission) == true) {
			$response['admin_commission_list'] = $this->admin_b2b_airline_commission_list($module);
			$this->airline_commission = $response;
		} else {
			$response = $this->airline_commission;
		}
		return $response;
	}

	function bus_commission($module)
	{
		if (empty($this->bus_commission) == true) {
			$response['admin_commission_list'] = $this->admin_b2b_bus_commission_list($module);
			$this->bus_commission = $response;
		} else {
			$response = $this->bus_commission;
		}
		return $response;
	}

	function sightseeing_commission($module){
		if (empty($this->sightseeing_commission) == true) {
			$response['admin_commission_list'] = $this->admin_b2b_sightseeing_commission_list($module);
			$this->sightseeing_commission = $response;
		} else {
			$response = $this->sightseeing_commission;
		}
		return $response;
	}

	function transfer_commission($module){
		if (empty($this->transfer_commission) == true) {
			$response['admin_commission_list'] = $this->admin_b2b_transfer_commission_list($module);
			$this->transfer_commission = $response;
		} else {
			$response = $this->transfer_commission;
		}
		return $response;
	}

	function admin_b2b_transfer_commission_list($module)
	{
		$commission=array(); 
		$domain_origin = get_domain_auth_id();
		if($module == 'b2b2b') {
			$query = 'select value,value_type, commission_currency From b2b_transfer_commission_details where type="specific" AND 
			agent_fk = '.intval($this->session->userdata('user_details_id')).' AND domain_list_fk = '.$this->session->userdata('domain_id').' ORDER BY type DESC';
            $com = $this->db->query($query)->row_array();
			if($com['value']==0)
			{
				//echo "dfsdf";
	                   $query_gen = 'select value,value_type, commission_currency From b2b2b_transfer_commission_details where
			   agent_fk IN (0) AND domain_list_fk = '. $this->session->userdata('domain_id') .' and type="generic" ORDER BY type DESC';
	                   $com = $this->db->query($query_gen)->row_array();
			}
		} else {
			$query = 'select value,value_type, commission_currency From b2b_transfer_commission_details where type="specific" AND 
			agent_fk = '.intval($this->session->userdata('user_details_id')).' AND domain_list_fk = '.$this->session->userdata('domain_id').' ORDER BY type DESC';
            $com = $this->db->query($query)->row_array();
			if($com['value']==0)
			{
	                   $query_gen = 'select value,value_type, commission_currency From b2b_transfer_commission_details where
			   agent_fk IN (0) AND domain_list_fk = '.$domain_origin.' and type="generic" ORDER BY type DESC';
	                   $com = $this->db->query($query_gen)->row_array();
			}
			
 
		}
        $this->value_type_to_lower_case($com);
		return $com;
	}

	function admin_b2b_airline_commission_list($module)
	{
        $commission=array(); 
		$domain_origin = get_domain_auth_id();
		if($module == 'b2b2b') {
			$query = 'select value,value_type, commission_currency From b2b2b_flight_commission_details where type="specific" AND 
			agent_fk = '.intval($this->session->userdata('user_details_id')).' AND domain_list_fk = '.$this->session->userdata('domain_id').' ORDER BY type DESC';
            $com = $this->db->query($query)->row_array();
			if($com['value']==0)
			{
				//echo "dfsdf";
	                   $query_gen = 'select value,value_type, commission_currency From b2b2b_flight_commission_details where
			   agent_fk IN (0) AND domain_list_fk = '. $this->session->userdata('domain_id') .' and type="generic" ORDER BY type DESC';
	                   $com = $this->db->query($query_gen)->row_array();
			}
		} else {
			$query = 'select value,value_type, commission_currency From b2b_flight_commission_details where type="specific" AND 
			agent_fk = '.intval($this->session->userdata('user_details_id')).' AND domain_list_fk = '.$this->session->userdata('domain_id').' ORDER BY type DESC';
            $com = $this->db->query($query)->row_array();
			if($com['value']==0)
			{
	                   $query_gen = 'select value,value_type, commission_currency From b2b_flight_commission_details where
			   agent_fk IN (0) AND domain_list_fk = '.$domain_origin.' and type="generic" ORDER BY type DESC';
	                   $com = $this->db->query($query_gen)->row_array();
			}
			
 
		}
		//debug($com);die;
        $this->value_type_to_lower_case($com);
		return $com;
	}

	function admin_b2b_bus_commission_list($module)
	{
		$domain_origin = get_domain_auth_id();
		if($module == 'b2b2b') {
			$query = 'select value, value_type, commission_currency, commission_currency as def_currency, value as def_value From b2b2b_bus_commission_details where type="specific" AND
			agent_fk IN (0, '.intval($this->session->userdata('user_details_id')).') AND domain_list_fk = '.$this->session->userdata('domain_id').' ORDER BY type DESC';
			$com = $this->db->query($query)->row_array();
			if($com['value']==0)
			{
	                   $query_gen = 'select value,value_type, commission_currency, commission_currency as def_currency, value as def_value From b2b2b_bus_commission_details where
			   agent_fk IN (0) AND domain_list_fk = '. $this->session->userdata('domain_id') .' and type="generic" ORDER BY type DESC';
	                   $com = $this->db->query($query_gen)->row_array();
			}
		} else {
			$query = 'select value, value_type, commission_currency, commission_currency as def_currency, value as def_value From b2b_bus_commission_details where type="specific" AND
			agent_fk IN (0, '.intval($this->session->userdata('user_details_id')).') AND domain_list_fk = '.$this->session->userdata('domain_id').' ORDER BY type DESC';
			$com = $this->db->query($query)->row_array();
			if($com['value']==0)
			{
	                   $query_gen = 'select value,value_type, commission_currency, commission_currency as def_currency, value as def_value From b2b_bus_commission_details where
			   agent_fk IN (0) AND domain_list_fk = '. $this->session->userdata('domain_id') .' and type="generic" ORDER BY type DESC';
	                   $com = $this->db->query($query_gen)->row_array();
			}
		}
		$this->value_type_to_lower_case($com);
		return $com;
	}

	function admin_b2b_sightseeing_commission_list($module)
	{
		$domain_origin = get_domain_auth_id();
		if($module == 'b2b2b') {
			$query = 'select value, value_type, commission_currency, commission_currency as def_currency, value as def_value From b2b2b_sightseeing_commission_details where type="specific" AND
			agent_fk IN (0, '.intval($this->session->userdata('user_details_id')).') AND domain_list_fk = '.$this->session->userdata('domain_id').' ORDER BY type DESC';
			$com = $this->db->query($query)->row_array();
			if($com['value']==0)
			{
	                   $query_gen = 'select value,value_type, commission_currency, commission_currency as def_currency, value as def_value From b2b2b_sightseeing_commission_details where
			   agent_fk IN (0) AND domain_list_fk = '. $domain_origin .' and type="generic" ORDER BY type DESC';
	                   $com = $this->db->query($query_gen)->row_array();
			}
		} else {
			$query = 'select value, value_type, commission_currency, commission_currency as def_currency, value as def_value From b2b_sightseeing_commission_details where type="specific" AND
			agent_fk IN (0, '.intval($this->session->userdata('user_details_id')).') AND domain_list_fk = '.$this->session->userdata('domain_id').' ORDER BY type DESC';
			$com = $this->db->query($query)->row_array();
			if($com['value']==0)
			{
	            $query_gen = 'select value,value_type, commission_currency, commission_currency as def_currency, value as def_value From b2b_sightseeing_commission_details where
			   agent_fk IN (0) AND domain_list_fk = '. $domain_origin .' and type="generic" ORDER BY type DESC';
	            $com = $this->db->query($query_gen)->row_array();
			}
		}
		$this->value_type_to_lower_case($com);
		return $com;
	}

	private function value_type_to_lower_case(& $row)
	{
		if (isset($row['value_type']) == true) {
			$row['value_type'] = strtolower($row['value_type']);
		} else {
			$row['value'] = 0;
			$row['value_type'] = 'plus';
			$row['commission_currency'] = MARKUP_CURRENCY;
		}
	}

	/**
	 * Get Convinence fees for Airline
	 */
	function airline_convinence_fees($search_id,$domain_id)
	{
		//Based on destination we need to load convinence fees so need search id
		$this->load->model('flight_model');
		$search_data = $this->flight_model->get_safe_search_data($search_id);
		$is_domestic = @$search_data['data']['is_domestic'];
		$cond = '';
		if ($is_domestic == true) {
			$cond .= ' module = "domestic_flight" and domain_id="'.$domain_id.'"';
		} else {
			$cond .= ' module = "international_flight" and domain_id="'.$domain_id.'"';
		}
		$query = 'select value, value_type as type, per_pax,convenience_fee_currency from convenience_fees Where '.$cond; 
		$convinence_fees = $this->db->query($query)->row_array();
		if (valid_array($convinence_fees) == true) {
			return $convinence_fees;
		}
	}

	function hotel_convinence_fees($search_id,$domain_id)
	{
		//Based on destination we need to load convinence fees so need search id
		$this->load->model('hotel_model');
		$search_data = $this->hotel_model->get_safe_search_data($search_id);//
		$is_domestic = @$search_data['data']['is_domestic'];
		$cond = '';

		if ($is_domestic == true) {
			$cond .= ' module = "domestic_hotel" and domain_id="'.$domain_id.'"'; 
		} else {
			$cond .= ' module = "international_hotel" and domain_id="'.$domain_id.'"'; 
		}
		$query = 'select value, value_type as type, per_pax from convenience_fees Where '.$cond;
		$convinence_fees = $this->db->query($query)->row_array();

		if (valid_array($convinence_fees) == true) {
			return $convinence_fees;
		}
	}

	function convinence_fees($search_id,$domain_id,$module_name)
	{
		$cond = '';
		if($module_name == 'transferv1') {
			$cond .= ' module = "transfers" and domain_id="'.$domain_id.'"';
		}
		if($module_name == 'sightseeing') {
			$cond .= ' module = "sightseeing" and domain_id="'.$domain_id.'"';
		}
		if($module_name == 'bus') {
			$cond .= ' module = "bus" and domain_id="'.$domain_id.'"';
		}
		$query = 'select value, value_type as type, per_pax from convenience_fees Where '.$cond;
		$convinence_fees = $this->db->query($query)->row_array();
		
		if (valid_array($convinence_fees) == true) {
			return $convinence_fees;
		}
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for provab - Domain wise and module wise
	 */
	function airline_markup()
	{
		//get generic only if specific is not available
		if (empty($this->airline_markup) == true) {
			$response['specific_markup_list'] = $this->specific_domain_markup('b2c_flight');
			if (valid_array($response['specific_markup_list']) == false) {
				$response['generic_markup_list'] = $this->generic_domain_markup('b2c_flight');
			}
			$this->airline_markup = $response;
		} else {
			$response = $this->airline_markup;
		}
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for provab - Domain wise and module wise
	 */
	function hotel_markup()
	{
		//debug('hotel_markup');die;
		if (empty($this->hotel_markup) == true) {
			$response['specific_markup_list'] = $this->specific_domain_markup('Hotels');
			if (valid_array($response['specific_markup_list']) == false) {
				$response['generic_markup_list'] = $this->generic_domain_markup('Hotels');
			}
			$this->hotel_markup = $response;
		} else {
			$response = $this->hotel_markup;
		}
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for provab - Domain wise and module wise
	 */
	function bus_markup()
	{
		if (empty($this->bus_markup) == true) {
			$response['specific_markup_list'] = $this->specific_domain_markup('Bus');
			if (valid_array($response['specific_markup_list']) == false) {
				$response['generic_markup_list'] = $this->generic_domain_markup('Bus');
			}
			$this->bus_markup = $response;
		} else {
			$response = $this->bus_markup;
		}
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Get generic markup based on the module type
	 * @param $module_type
	 * @param $markup_level
	 */
	function generic_domain_markup($module_type)
	{
		if($this->markup_level == 'level_1'){
			$markup_level = 0;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'Hotels'){
			$module_type = 1;
		}
		if($module_type == 'Bus'){
			$module_type = 5;
		}
		if($module_type == 'Activity') {
			$module_type = 7;
		}
		
		$query = 'SELECT ML.markup_details_id AS id, ML.markup_type AS markup_type, ML.markup_value, ML.markup_fixed, ML.markup_value_type,  ML.markup_currency AS markup_currency
		FROM markup_details AS ML where ML.markup_level = "'.$this->markup_level.'" and ML.markup_type="GENERAL" and ML.markup_status="ACTIVE" and ML.product_details_id = ' . $module_type . ' and ML.user_type_id = ' . $markup_level;
		$generic_data_list = $this->db->query($query)->result_array();

		return $generic_data_list;
	}

	/**
	 * Arjun J Gowda
	 * Get specific markup based on module type
	 * @param string $module_type	Name of the module for which the markup has to be returned
	 * @param string $markup_level	Level of markup
	 */
	function specific_domain_markup($module_type)
	{
		if($this->session->userdata('user_logged_in') == 0) {
			$userType = 5;
		} else {
			$userType = $this->session->userdata('user_type');
		} 
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'Hotels'){
			$module_type = 1;
		}
		if($module_type == 'Bus'){
			$module_type = 5;
		}
		if($module_type == 'Activity') {
			$module_type = 7;
		}

		$query = 'SELECT ML.markup_details_id AS id, ML.markup_type AS markup_type, ML.markup_value, ML.markup_fixed, ML.markup_value_type,  ML.markup_currency AS markup_currency
		FROM markup_details AS ML where ML.markup_level = "'.$this->markup_level.'" and ML.markup_type="SPECIFIC" and ML.markup_status="ACTIVE" and ML.product_details_id = ' . $module_type . ' and ML.user_type_id = ' . $userType;

		// $query = 'SELECT ML.origin AS markup_origin, ML.value, ML.value_type,  ML.markup_currency AS markup_currency FROM domain_list AS DL JOIN markup_list AS ML where ML.value != "" and
		// ML.module_type = "'.$module_type.'" and ML.markup_level = "'.$this->markup_level.'" and DL.origin=ML.domain_list_fk and ML.type="specific"
		// and ML.domain_list_fk != 0 and ML.reference_id='.get_domain_auth_id().' and ML.domain_list_fk = '.get_domain_auth_id().' order by DL.created_datetime DESC';
		$specific_data_list = $this->db->query($query)->result_array();
		return $specific_data_list;
	}

	/**
	 * update domain balance details
	 * @param number $domain_origin	doamin unique key
	 * @param number $amount		amount to be added or deducted(-100 or +100)
	 */
	function update_domain_balance($domain_origin, $amount)
	{
		$current_balance = 0;
		$cond = array('origin' => intval($domain_origin));
		$details = $this->custom_db->single_table_records('domain_list', 'balance', $cond);
		if ($details['status'] == true) {
			$details['data'][0]['balance'] = $current_balance = ($details['data'][0]['balance'] + $amount);
			$this->custom_db->update_record('domain_list', $details['data'][0], $cond);
		}
		return $current_balance;
	}

	/**
	 * Log XML For Provab Security
	 * @param string $operation_name
	 * @param string $app_reference
	 * @param string $module
	 * @param json 	 $request
	 * @param json	 $response


	 */
	public function provab_xml_logger($operation_name, $app_reference, $module, $request, $response)
	{
		
		$data['operation_name'] = $operation_name;
		$data['app_reference'] = $app_reference;
		$data['module'] = $module;
		if (is_array($request)) {
			$request = json_encode($request);
		}
		if (is_array($response)) {
			$response = json_encode($response);
		}
		$data['request'] = $request;
		$data['response'] = $response;
		$data['ip_address'] = $_SERVER['REMOTE_ADDR'];
		$data['created_datetime'] = date('Y-m-d H:i:s');
		
		$this->custom_db->insert_record('provab_xml_logger', $data);
	}
}
