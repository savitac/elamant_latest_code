<?php
class Markup_Model extends CI_Model {
    
    function __construct() {
         parent::__construct();
    }
    
    function get_agent_balance($user_id){
		$this->db->select('balance_credit');
		$this->db->from('user_accounts');
		$this->db->where('user_id', $user_id);
		return $this->db->get()->row();
	}
    
    function checking($reslt){
    	$this->db->insert('checking',$reslt);
    }

    function get_admin_markup_details($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name='markup_details'){
		$markup_details = '';
	 	$markup_details 	= $this->get_specific_markup($markup_country_id, $check_in_date_format, $product_id, $api_id, $table_name);
	 	if($markup_details =='')	{
			$markup_details = $this->get_generic_markup($product_id , $api_id, $table_name='markup_details');
		}
		print_r($markup_details);die('Markup_Model'); 
		return $markup_details;
	}
	
	function get_b2b_markup_details($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name='agent_markup'){
		$markup_details = '';
	 	$markup_details 	= $this->get_specific_markup($markup_country_id, $check_in_date_format, $product_id, $api_id, $table_name);
	 	if($markup_details =='')	{
			$markup_details = $this->get_generic_markup($product_id , $api_id, $table_name);
		}
		return $markup_details;
	}
	
	function get_sub_agent_markup_details($markup_country_id, $check_in_date_format, $product_id, $api_id, $table_name='sub_agent_markup'){
		$markup_details = '';
	 	$markup_details 	= $this->get_specific_markup($markup_country_id, $check_in_date_format, $product_id, $api_id, $table_name);
	 	if($markup_details =='')	{
			$markup_details = $this->get_generic_markup($product_id , $api_id, $table_name);
		}
		return $markup_details;
	}
	
	function get_b2b2b_markup_details($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name='b2b2b_agent_markup'){
		$markup_details = '';
	 	$markup_details 	= $this->get_specific_markup($markup_country_id, $check_in_date_format, $product_id, $api_id, $table_name);
	 	if($markup_details =='')	{
			$markup_details = $this->get_generic_markup($product_id , $api_id, $table_name);
		}
		return $markup_details;
	}
	
	
	function get_generic_markup($product_id , $api_id, $table_name){
		  $this->db->select("*");
		  $this->db->from($table_name);
		  $this->db->where("product_details_id", $product_id);
		  if($table_name=='markup_details'){
		  $this->db->where("api_details_id", $api_id);
	      }elseif($table_name == 'agent_markup'){
		  $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
		  }elseif($table_name == 'sub_agent_markup'){
			 $this->db->where('created_branch_id', $this->session->userdata('branch_id'));  
		  }elseif($table_name == 'b2b2b_agent_markup'){
			  $this->db->where('user_details_id', $this->session->userdata('user_details_id'));
		  }
		  $this->db->where("markup_type", 'GENERAL');
		  $this->db->where("markup_status", 'ACTIVE');
		  $query = $this->db->get();
		
		   if($query->num_rows() > 0){
			  return $query->result();
		  }else{
			  return "";
		  }
	}
	
	function get_specific_markup($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name){
		$markup_details =  "";
		$markup_details1 = "";
		$markup_details2 = "";
		
		$markup_details = $this->get_markup_with_all_conditions($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name);
		if($markup_details == ''){
			 $markup_details1 = $this->get_markup_with_daterange_and_country_and_without_users($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name);
			 if($markup_details1 ==''){
				 $markup_details3 = $this->get_markup_with_daterange_and_without_country_and_withusers($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name);
				 if($markup_details3 == ''){
					 $markup_details4 = $this->get_markup_wthout_daterange_and_with_country_and_withusers($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name);
					 if($markup_details4 == ''){
						$markup_details5 = $this->get_markup_without_daterange_and_with_country_and_withoutusers($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name); 
						if($markup_details5 == ''){
							 $markup_details6 = $this->get_markup_without_daterange_and_without_country_and_withusers($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name);
							 if($markup_details6 == ''){
								 $markup_details7 = $this->get_markup_with_daterange_and_without_country_and_withoutusers($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name);
								 if($markup_details7 == ''){
									 return "";
								 }else{
									return $markup_details7; 
								 }
							 }else{
								 return $markup_details6;
							 }
						}else{
							return $markup_details5;
						}
					 }else{
						return $markup_details4;
					 }
				 }else{
					 return $markup_details3;
				 }
			 }else{
				 return $markup_details1;
			 }
		}else{
			return $markup_details;
		}
	}

    function get_markup_with_all_conditions($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name) {
		  $this->db->select("*");
		  $this->db->from($table_name);
		  $sql1 = "DATEDIFF(markup_start_date, '".$check_in_date_format."') <=0 and DATEDIFF(markup_end_date, '".$check_in_date_format."') >= 0";
		  $this->db->where($sql1);
		  
		  //$this->db->where("markup_start_date >=", $check_in_date_format);
		  //$this->db->where("markup_end_date <=", $check_in_date_format);
		  //$this->db->where("markup_expiry_date >=", date('Y-m-d'));
		  //$this->db->where("markup_expiry_date_to <=",  date('Y-m-d'));
		  
		  $sql1 = "DATEDIFF(markup_expiry_date,'".date('Y-m-d')."') <=0 and DATEDIFF(markup_expiry_date_to, '".date('Y-m-d')."') >= 0";
		  $this->db->where($sql1);
		  
		  $this->db->where("country_id", $markup_country_id);
		  $this->db->where("product_details_id", $product_id);
		  if($table_name=='markup_details'){
		  $this->db->where("api_details_id", $api_id);
		  $this->db->where("user_type_id", $this->session->userdata('user_type'));
		  $this->db->where("user_details_id", $this->session->userdata('branch_id'));
	      }elseif($table_name == 'agent_markup'){
		  $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
		  }elseif($table_name == 'sub_agent_markup'){
			 $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
			 $this->db->where("user_type_id", $this->session->userdata('user_type'));
		     $this->db->where("user_details_id", $this->session->userdata('branch_id'));  
		  }elseif($table_name == 'b2b2b_agent_markup'){
			  $this->db->where('user_details_id', $this->session->userdata('user_details_id'));
		  }
		  
		  $this->db->where("markup_type", 'SPECIFIC');
		  $this->db->where("markup_status", 'ACTIVE');
		  $query = $this->db->get();
		 
		  if($query->num_rows() > 0){
			  return $query->result();
		  }else{
			  return "";
		  }
	}
	
	function get_markup_with_daterange_and_country_and_without_users($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name) {
		  $this->db->select("*");
		  $this->db->from($table_name);
		   $sql1 = "DATEDIFF(markup_start_date, '".$check_in_date_format."') <=0 and DATEDIFF(markup_end_date, '".$check_in_date_format."') >= 0";
		  $this->db->where($sql1);
		  
		  //$this->db->where("markup_start_date >=", $check_in_date_format);
		  //$this->db->where("markup_end_date <=", $check_in_date_format);
		  //$this->db->where("markup_expiry_date >=", date('Y-m-d'));
		  //$this->db->where("markup_expiry_date_to <=",  date('Y-m-d'));
		  
		  $sql1 = "DATEDIFF(markup_expiry_date,'".date('Y-m-d')."') <=0 and DATEDIFF(markup_expiry_date_to, '".date('Y-m-d')."') >= 0";
		  $this->db->where($sql1);
		  $this->db->where("country_id", $markup_country_id);
		  $this->db->where("product_details_id", $product_id);
		  if($table_name=='markup_details'){
		  $this->db->where("api_details_id", $api_id);
		  $this->db->where("user_type_id", $this->session->userdata('user_type'));
		  $this->db->where("user_details_id",0);
	      }elseif($table_name == 'agent_markup'){
		  $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
		  }elseif($table_name == 'sub_agent_markup'){
			 $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
			 $this->db->where("user_type_id", $this->session->userdata('user_type'));
		     $this->db->where("user_details_id", 0);  
		  }elseif($table_name == 'b2b2b_agent_markup'){
			  $this->db->where('user_details_id', $this->session->userdata('user_details_id'));
		  }
		 
		  $this->db->where("markup_type", 'SPECIFIC');
		  $this->db->where("markup_status", 'ACTIVE');
		  $query1 = $this->db->get();
		 
		  if($query1->num_rows() > 0){
			  return $query1->result();
		  }else{
			  return "";
		  }
	}
	
	function get_markup_with_daterange_and_without_country_and_withusers($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name) {
		  $this->db->select("*");
		  $this->db->from($table_name);
		   $sql1 = "DATEDIFF(markup_start_date, '".$check_in_date_format."') <=0 and DATEDIFF(markup_end_date, '".$check_in_date_format."') >= 0";
		  $this->db->where($sql1);
		  
		  //$this->db->where("markup_start_date >=", $check_in_date_format);
		  //$this->db->where("markup_end_date <=", $check_in_date_format);
		  //$this->db->where("markup_expiry_date >=", date('Y-m-d'));
		  //$this->db->where("markup_expiry_date_to <=",  date('Y-m-d'));
		  
		  $sql1 = "DATEDIFF(markup_expiry_date,'".date('Y-m-d')."') <=0 and DATEDIFF(markup_expiry_date_to, '".date('Y-m-d')."') >= 0";
		  $this->db->where($sql1);
		  $this->db->where("country_id", 0);
		  $this->db->where("product_details_id", $product_id);
		  if($table_name=='markup_details'){
		  $this->db->where("api_details_id", $api_id);
		  $this->db->where("user_type_id", $this->session->userdata('user_type'));
		  $this->db->where("user_details_id", $this->session->userdata('branch_id'));
	      }elseif($table_name == 'agent_markup'){
		  $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
		  }elseif($table_name == 'sub_agent_markup'){
			 $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
			 $this->db->where("user_type_id", $this->session->userdata('user_type'));
		     $this->db->where("user_details_id", $this->session->userdata('branch_id'));  
		  }elseif($table_name == 'b2b2b_agent_markup'){
			  $this->db->where('user_details_id', $this->session->userdata('user_details_id'));
		  }
		 
		  $this->db->where("markup_type", 'SPECIFIC');
		  $this->db->where("markup_status", 'ACTIVE');
		  $query = $this->db->get();
		  
		  if($query->num_rows() > 0){
			  return $query->result();
		  }else{
			  return "";
		  }
	}
	
	function get_markup_wthout_daterange_and_with_country_and_withusers($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name){
		  $this->db->select("*");
		  $this->db->from($table_name);
		  $this->db->where("markup_start_date =", '0000-00-00');
		  $this->db->where("markup_end_date =", '0000-00-00');
		  $this->db->where("markup_expiry_date =", '0000-00-00');
		  $this->db->where("markup_expiry_date_to =",  '0000-00-00');
		  $this->db->where("country_id", 0);
		  $this->db->where("product_details_id", $product_id);
		  if($table_name=='markup_details'){
		  $this->db->where("api_details_id", $api_id);
		  $this->db->where("user_type_id", $this->session->userdata('user_type'));
		  $this->db->where("user_details_id", $this->session->userdata('branch_id'));
	      }elseif($table_name == 'agent_markup'){
		  $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
		  }elseif($table_name == 'sub_agent_markup'){
			 $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
			 $this->db->where("user_type_id", $this->session->userdata('user_type'));
		     $this->db->where("user_details_id", $this->session->userdata('branch_id'));  
		  }elseif($table_name == 'b2b2b_agent_markup'){
			  $this->db->where('user_details_id', $this->session->userdata('user_details_id'));
		  }
		 
		  $this->db->where("markup_type", 'SPECIFIC');
		  $this->db->where("markup_status", 'ACTIVE');
		  $query = $this->db->get();
		  
		  if($query->num_rows() > 0){
			  return $query->result();
		  }else{
			  return "";
		  }
	}
	
	function get_markup_without_daterange_and_with_country_and_withoutusers($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name) {
		  $this->db->select("*");
		  $this->db->from($table_name);
		  $this->db->where("markup_start_date =", '0000-00-00');
		  $this->db->where("markup_end_date =", '0000-00-00');
		  $this->db->where("markup_expiry_date =", '0000-00-00');
		  $this->db->where("markup_expiry_date_to =",  '0000-00-00');
		  $this->db->where("country_id", $markup_country_id);
		  $this->db->where("product_details_id", $product_id);
		  if($table_name=='markup_details'){
		  $this->db->where("api_details_id", $api_id);
		  $this->db->where("user_type_id", $this->session->userdata('user_type'));
		  $this->db->where("user_details_id", 0);
	      }elseif($table_name == 'agent_markup'){
		  $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
		  }elseif($table_name == 'sub_agent_markup'){
			 $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
			 $this->db->where("user_type_id", $this->session->userdata('user_type'));
		     $this->db->where("user_details_id",0);  
		  }elseif($table_name == 'b2b2b_agent_markup'){
			  $this->db->where('user_details_id', $this->session->userdata('user_details_id'));
		  }
		
		  $this->db->where("markup_type", 'SPECIFIC');
		  $this->db->where("markup_status", 'ACTIVE');
		  $query = $this->db->get();
		  
		  if($query->num_rows() > 0){
			  return $query->result();
		  }else{
			  return "";
		  }
	}
	
	function get_markup_without_daterange_and_without_country_and_withusers($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name) {
		  $this->db->select("*");
		  $this->db->from($table_name);
		  $this->db->where("markup_start_date =", '0000-00-00');
		  $this->db->where("markup_end_date =", '0000-00-00');
		  $this->db->where("markup_expiry_date =", '0000-00-00');
		  $this->db->where("markup_expiry_date_to =",  '0000-00-00');
		  $this->db->where("country_id", 0);
		  $this->db->where("product_details_id", $product_id);
		  if($table_name=='markup_details'){
		  $this->db->where("api_details_id", $api_id);
		  $this->db->where("user_type_id", $this->session->userdata('user_type'));
		  $this->db->where("user_details_id", $this->session->userdata('branch_id'));
	      }elseif($table_name == 'agent_markup'){
		  $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
		  }elseif($table_name == 'sub_agent_markup'){
			 $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
			 $this->db->where("user_type_id", $this->session->userdata('user_type'));
		     $this->db->where("user_details_id", $this->session->userdata('user_details_id'));  
		  }elseif($table_name == 'b2b2b_agent_markup'){
			  $this->db->where('user_details_id', $this->session->userdata('user_details_id'));
		  }
		 
		  $this->db->where("markup_type", 'SPECIFIC');
		  $this->db->where("markup_status", 'ACTIVE');
		  $query = $this->db->get();
		
		  if($query->num_rows() > 0){
			  return $query->result();
		  }else{
			  return "";
		  }
	}
	
	
	function get_markup_with_daterange_and_without_country_and_withoutusers($markup_country_id, $check_in_date_format, $product_id , $api_id, $table_name) {
		  $this->db->select("*");
		  $this->db->from($table_name);
		  $sql1 = "DATEDIFF(markup_start_date, '".$check_in_date_format."') <=0 and DATEDIFF(markup_end_date, '".$check_in_date_format."') >= 0";
		  $this->db->where($sql1);
		  
		  //$this->db->where("markup_start_date >=", $check_in_date_format);
		  //$this->db->where("markup_end_date <=", $check_in_date_format);
		  //$this->db->where("markup_expiry_date >=", date('Y-m-d'));
		  //$this->db->where("markup_expiry_date_to <=",  date('Y-m-d'));
		  
		  $sql1 = "DATEDIFF(markup_expiry_date,'".date('Y-m-d')."') <=0 and DATEDIFF(markup_expiry_date_to, '".date('Y-m-d')."') >= 0";
		  $this->db->where($sql1);
		  
		  $this->db->where("country_id", 0);
		  $this->db->where("product_details_id", $product_id);
		  if($table_name=='markup_details'){
		  $this->db->where("api_details_id", $api_id);
		  $this->db->where("user_type_id", $this->session->userdata('user_type'));
		  $this->db->where("user_details_id", 0);
	      }elseif($table_name == 'agent_markup'){
		  $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
		  }elseif($table_name == 'sub_agent_markup'){
			 $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
			 $this->db->where("user_type_id", $this->session->userdata('user_type'));
		     $this->db->where("user_details_id", 0);  
		  }elseif($table_name == 'b2b2b_agent_markup'){
			  $this->db->where('user_details_id', $this->session->userdata('user_details_id'));
		  }
		  
		  $this->db->where("markup_type", 'SPECIFIC');
		  $this->db->where("markup_status", 'ACTIVE');
		  $query = $this->db->get();
		 
		  if($query->num_rows() > 0){
			  return $query->result();
		  }else{
			  return "";
		  }
	}
	
	
	
}
?>
