<?php
class Hotel_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    } 
    
    public function isRegistered_Guest($email, $user_type =''){
	
		$this->db->where('user_email', $email);
		if($user_type != '') {
		$this->db->where('user_type_id', $user_type);
    	}
		return $this->db->get('user_details');
	}
	
	public function create_guest_User($postData){
		return $this->db->insert('user_details',$postData);
	 }
	 
	 
     public function get_gta_cities($term) {
        $this->db->select('gta_cities.*, COUNT(gta_Hotels.HotelCode) as hCount');
        $this->db->join('gta_Hotels', 'gta_cities.city_code = gta_Hotels.CityCode', 'left');
        $this->db->like('city_name',$term);
        $this->db->or_like('country_name',$term);
        $this->db->order_by("city_name", "asc");
        $this->db->group_by("gta_cities.city_code"); 
        $this->db->limit(8);
        $this->db->where('city_status', 'ACTIVE');
        return $this->db->get('gta_cities');
    }
    /*This method will return the hotel information like images, description etc from the DB.*/
    public function getMoreInfoFromDb($itemCode, $cityCode) {
        $this->db->where('HotelCode', $itemCode);
        $this->db->where('CityCode', $cityCode);
        $query= $this->db->get('gta_Hotels');
		//echo 'last<pre/>';print_r($this->db->last_query());exit;
		return $query;
    }
    public function getCityDataFromDB($city_code){
        $this->db->where('city_code',$city_code);
        return $this->db->get('gta_cities');
    }
    public function get_city_data_old($city){
        $this->db->where('city',trim($city));
        return $this->db->get('api_hotels_cities');

    }
    
     public function get_city_data($city){
		$city_arr = explode(",", $city);
		
		if(count($city_arr) > 0) {
		 if(count($city_arr) == 3){
			if($city_arr[0] != '') {
		       $this->db->where('city',$city_arr[0]);
		    }
		    if($city_arr[1] != '') {
		       $this->db->where('state',$city_arr[1]);
		    }
		    if($city_arr[2] != '') {
		       $this->db->where('country',$city_arr[2]);
		    }
		}elseif(count($city_arr) == 2){
			if($city_arr[0] != '') {
		       $this->db->where('city',$city_arr[0]);
		    }
		    if($city_arr[1] != '') {
		       $this->db->where('country',$city_arr[1]);
		    }
		}elseif(count($city_arr) == 1){
			if($city_arr[0] != '') {
		       $this->db->where('city',$city_arr[0]);
		    }
		}
		    $this->db->group_by('gta_city_code, city, state');
	    }
	 
        $query = $this->db->get('api_hotels_cities_1');
      
        return $query;

    }
    
    public function getCityCode_gta($city_name, $country_name) {
        $this->db->where('country_name', $country_name);
        $this->db->where('city_name', $city_name);
        return $this->db->get('gta_cities');
    }
    public function getPerPage(){
        return $this->db->get('page_count');
    }
    public function getAPI() {
		$query = $this->db->get_where('api_details',array('api_status'=>'ACTIVE'));
		return $query->result();
	}
	public function getFacilities(){
        return $this->db->get('gta_Facilities');   
    }
    public function currency_convertor($amount,$from,$to){
        $from = strtoupper($from);
        $to = strtoupper($to);
        //$this->db->select('value');
        if($from === $to){
            $amount = $amount/1;
            return number_format(($amount) ,2,'.','');
        }else{
            $this->db->where('currency_code',$from);
            $price = $this->db->get('currency_details')->row();
            $value = $price->value;
            $amount = ($amount)/($value);
            return number_format(($amount) ,2,'.','');
        }
    }
    public function storeHotelTempData($temp_data) {
        $this->db->insert('api_hotel_detail_t', $temp_data);
        return $this->db->insert_id();
    }
    public function getHotelRoomData($room_index, $option_id) {
        $this->db->where('option_id', $option_id);
        $this->db->where('api_temp_hotel_id', $room_index);
        return $this->db->get('api_hotel_detail_t');
    }
    public function getClientSettings($api){
        $this->db->select('*');
        $this->db->from('api_details');
        $this->db->where('api_name', $api);
        $this->db->where('api_status', 'ACTIVE');
        $query = $this->db->get();
        return $query->row();
    }
    
    
    public function get_hotels_list_old($city){
        $this->db->select('*');
        $this->db->from('api_hotels_cities');
        $this->db->like('api_hotels_cities.city',$city);
        $this->db->order_by("api_hotels_cities.city", "asc"); 
        $this->db->group_by("api_hotels_cities.city, api_hotels_cities.city_name");
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result();
    }
    
     public function get_hotels_list($city){
        $this->db->select('*');
        $this->db->from('api_hotels_cities_1');
        $this->db->like('city',$city);
        $this->db->order_by("city", "asc"); 
        $this->db->group_by("city, city_name, country, state");
        $this->db->limit(10);
        $query = $this->db->get();
       // echo $this->db->last_query(); 
       return $query->result();
    }

  
 
	public function get_hotels_list_ByName($term){
        $this->db->select('*');
        $this->db->from('hotelbeds_Hotels');
        $this->db->like('hotelbeds_Hotels.Name',$term);
        $this->db->order_by("hotelbeds_Hotels.Name", "asc"); 
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result();
	}
	public function get_hotel_name_data($term){
        $this->db->where('Name',trim($term));
        return $this->db->get('hotelbeds_Hotels');
    }
    function checkcache($session){
         $this->db->select('session_id');
        $this->db->from('tf_hotel_result');
        $this->db->where('session_id', $session);
        $query = $this->db->get(); 
        if ($query->num_rows() > 0) {
            return '1';
        } else {
           return '0';
        }
    } 
    public function clear_prev_result() {
		$this->db->truncate('tf_facility_result');
		$this->db->truncate('tf_hotel_result');
	}
    function fetch_search_result($request,$ses_id, $api) { 
        $where = '';
        $order = 'ORDER BY low_cost ASC';
        $where .= " AND `status` IN ('AVAILABLE', 'OK', 'Available', 'InstantConfirmation', 'true')  ";
     //   $where .= "AND (t.city = p.cityName OR t.`destCodeVal` = p.DestinationCode)";
        
        $select = "SELECT SQL_CALC_FOUND_ROWS *, MIN(amount) as low_cost FROM tf_hotel_result where `session_id` = '$ses_id' $where GROUP BY hotel_code $order";
        $query = $this->db->query($select);
        $result_count = $query->result_array();

        if ($query->num_rows > 0) {
            $data['result'] = $result = $query->result();
            $j = 0;
             foreach ($result_count as $row){ 
           $data['result'][$j]->details  = base_url().'hotel/hotel_details/'.base64_encode((json_encode($row['hotel_code']))).'/'.$row['id'].'/'.base64_encode((json_encode($request))).'/'.$row['api_id']; 
          
           $j++;
            }
            
            
             
            $count_result = $this->db->query('SELECT FOUND_ROWS() as rowcount');
            $count = $count_result->result();
            $data['totRow'] = $count[0]->rowcount;
            $select2 = "select MIN(low_cost) as minVal, MAX(low_cost) as maxVal from (SELECT MIN(amount) as low_cost FROM tf_hotel_result  WHERE   `session_id` = '$ses_id' $where group by  hotel_code ) as tab";
            $query2 = $this->db->query($select2);
            $result2 = $query2->result();

            $data['minVal'] = $result2[0]->minVal;
            $data['maxVal'] = $result2[0]->maxVal;
         
            return $data;
        }
        return false;
    }
	function store_logs($req,$res,$api) {
		$data = array(
				'xml_type' => 'Hotel',
				'xml_request' => $req,
				'xml_request' => $req,
				'api_name' => $api
				);
				if($this->db->insert('xml_logs', $data)){
				return true;
			}else{
				return false;
			}
	}
	public function save_result_gta($result,$request,$session_id,$api_id) {
		$data2= array();
		foreach ($result as  $val) { 
            $data['session_id'] = $session_id;
            $data['hotel_code'] = $val['hotel_code'];
            $data['hotel_id'] = $val['hotel_id'];
            $data['sid'] = $val['sid'];
            $data['api_id'] = $api_id;
            $data['request'] = serialize($request);
            $data['hotel_name'] = $val['hotel_name'];
            $data['hotel_api_images'] = $val['hotel_api_images'];
            
            
            $data['amount'] = $val['amount'];
            $data['net_rate'] = $val['net_rate'];
            $data['admin_markup'] = $val['admin_markup'];
            $data['my_markup'] = $val['my_markup'];
            $data['my_agent_Markup'] = $val['my_agent_Markup'];
            $data['my_b2b2b_markup'] = $val['my_b2b2b_markup'];
             
             
            $data['admin_baseprice'] = $val['admin_baseprice'];
            $data['agent_baseprice'] = $val['agent_baseprice'];
            $data['sub_agent_baseprice'] = $val['sub_agent_baseprice'];
            $data['b2b2b_baseprice'] = $val['b2b2b_baseprice'];
            
             $data['service_charge'] = $val['service_charge'];
            $data['gst_charge'] = $val['gst_charge'];
            $data['tax_charge'] = $val['tax_charge'];
           
            
            $data['currency'] = $val['currency'];
            $data['api_currency'] = $val['api_currency'];
            $data['status'] = $val['status'];
            $data['search_date'] = $val['search_date'];
            $data['room_count'] = $val['room_count'];
            $data['description'] = $val['description'] ;
            $data['address'] = $val['address'];
            $data['star_rating'] = $val['star_rating'];
      
            $data['city'] = $val['city'];
            $data['lat'] = $val['lat'];
            $data['lon'] = $val['lon'];
         //   $data['offers'] = $val['offers'];
         //   $data['total_room'] = $val['total_room'];
            $data['hotel_facility'] = $val['facilities'];
            $data['locations'] = $val['locations'];
            $data['images'] = $val['images'];
            
           if( $data['hotel_name'] != ''){
            $this->db->insert('tf_hotel_result',$data);
            
            $insert_id = $this->db->insert_id() ;

            if($val['facilities'] != '')
            {

                $this->db->where('session_id', $session_id);
                $this->db->delete('tf_facility_result');

                $facility_res = json_decode($val['facilities'] , true);
              
                $i = 0 ;
                foreach ( $facility_res  as $val2) 
                if(isset($val2) &&  $val != '') 
                {

                    $data2[$i]['session_id'] = $session_id;
                    $data2[$i]['result_id'] = $insert_id;
                    $data2[$i]['facility'] = $val2;
                    $i++;
                }
                
                if(count($data2) > 0)
                $this->db->insert_batch('tf_facility_result', $data2);
            }
		}
	
	
}
           
	}
	public function get_gta_hotel_data($hottel_code,$cittty_code)
{
    $this->db->where('HotelCode' , $hottel_code);
     $this->db->where('CityCode' , $cittty_code);
    $res = $this->db->get('gta_Hotels');
    
    if($res->num_rows() > 0 )
    {
        return $res->row();
    }

    return '';

}
function get_last_response($session_id,$params=array(), $cond=array())
{ 
        $this->db->select('*');
      
        $this->db->where('tf_hotel_result.session_id',$session_id);
       
    if(count($cond) > 0)
    {
        $this->db->where($cond['amount_filter']);
        if($cond['hotel_name'] != NULL )
        {
            $this->db->like('hotel_name', $cond['hotel_name']);
        }
       
        if($cond['star_rating'] !== NULL &&  count($cond['star_rating']) > 0 )
        {
             
           for($s=0; $s < count($cond['star_rating']); $s++){
        /*      if($s==0){
                    $this->db->like('star_rating', $cond['star_rating'][$s]);
                } else {
                    $this->db->or_like('star_rating', $cond['star_rating'][$s]);
                }   */
                // changed by siva
                $rating = $cond['star_rating'][$s];
                //$likes[] = "star_rating LIKE '%$rating%'";     
                 $likes[] = "star_rating = '$rating'";      
                
           }
           $likesQ = '('.implode(' || ', $likes).')';;
           $this->db->where($likesQ, NULL, FALSE);
        } 
 if($cond['accommodation'] !== NULL &&  count($cond['accommodation']) > 0 )
        {
           $kx=array();
            for($a = 0; $a< count($cond['accommodation']); $a++){
                
                 $accommodation = mysql_real_escape_string($cond['accommodation'][$a]);
                 $kx[] = "tf_hotel_result.locations LIKE '%$accommodation%'"; 
                //if($a==0){
                    //$this->db->like('api_hotel_detail_t.inclusion', $cond['accommodation'][$a]);
                //} else {
                    
                    //$this->db->or_like('api_hotel_detail_t.inclusion', $cond['accommodation'][$a]);
                    //$l=$cond['accommodation'][$a];
                    //$kx[] = "tf_hotel_result.inclusion LIKE '%$l%'";
                //}
            }
            $accQ = "(".implode(" || ",$kx).")";
            $this->db->where($accQ); 
        }
        if($cond['ammenities'] !== NULL &&  count($cond['ammenities']) > 0 )
        {
              $kxs=array();
           for($am = 0; $am < count($cond['ammenities']); $am++){
                /*if($am==0){
                    $this->db->like('api_hotel_detail_t.hotel_facility', $cond['ammenities'][$am]);
                } else {
                    $this->db->or_like('api_hotel_detail_t.hotel_facility', $cond['ammenities'][$am]);
                }*/
                $ls=mysql_real_escape_string($cond['ammenities'][$am]);
                    $kxs[] = "tf_hotel_result.hotel_facility LIKE '%$ls%'";
            }
            $this->db->where("(".implode(" || ",$kxs).")"); 
        } 
      /*  if($cond['accommodation'] !== NULL &&  count($cond['accommodation']) > 0 )
        {
           
            for($a = 0; $a< count($cond['accommodation']); $a++){
                if($a==0){
                    $this->db->like('api_hotel_detail_t.inclusion', $cond['accommodation'][$a]);
                } else {
                    $this->db->or_like('api_hotel_detail_t.inclusion', $cond['accommodation'][$a]);
                }
            }
        }
        if($cond['ammenities'] !== NULL &&  count($cond['ammenities']) > 0 )
        {
           for($am = 0; $am < count($cond['ammenities']); $am++){
                if($am==0){
                    $this->db->like('api_hotel_detail_t.hotel_facility', $cond['ammenities'][$am]);
                } else {
                    $this->db->or_like('api_hotel_detail_t.hotel_facility', $cond['ammenities'][$am]);
                }
            }
        } */
        $this->db->order_by($cond['sort_col'],$cond['sort_val']);
        //~ $this->db->order_by('amount','asc');
        # next condition
    }
    else
    {
        $this->db->order_by('amount','asc');
    }

    if(array_key_exists("start",$params) && array_key_exists("limit",$params))
    {
        $this->db->limit($params['limit'],$params['start']);
    } 
    elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
    {
         $this->db->limit($params['limit']);
    }
    
    
      $this->db->group_by('hotel_code');
    $q = $this->db->get('tf_hotel_result');
  //echo $this->db->last_query(); exit;

    if( $q->num_rows() > 0 )
    {
        
        return $q->result();
    }
        return false;
}
	public function get_hotel_information_id($hotelid) 
     {
 
    $this->db->where('id', $hotelid);
    $query = $this->db->get('tf_hotel_result');
     
        if ($query->num_rows() > 0)
        {
            return $query->row();
        } 
        else{
            return '';
        }
    }
     function get_nearby_hotels($latitude,$longitude){

         $this->db->select("*, ( 3959 * acos( cos( radians($latitude) ) * cos( radians( lat  ) ) * cos( radians( lon  ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( lat  ) ) ) ) AS distance");                         
         $this->db->from('tf_hotel_result');
         $this->db->having('distance <= 5');
         $this->db->where('hotel_api_images !=','');
         $this->db->where('lat !=',$latitude);
         $this->db->where('lon !=',$longitude);
         $this->db->group_by('hotel_code');
         $this->db->order_by('hotel_name', 'ASC');
         $this->db->limit(5);
         $query = $this->db->get();

         if ($query->num_rows() == '') {
             return '';
        } else {
            return $query->result();
        }
    }
    public function get_gta_hotel($hottel_code,$city_code) {
		$this->db->where('HotelCode' , $hottel_code);
		$this->db->where('CityCode' , $city_code);
		$res = $this->db->get('gta_Hotels');
		
		if($res->num_rows() > 0 )
		{
			return $res->row();
		}
		return '';
	}
	public function get_hb_hotel_data($hottel_code,$cittty_code) {
	/*    $query = $this->db->query("select hotelbeds_Hotels.*, hotelbeds_facilities_description.NAME as HotelFacilityName from `hotelbeds_Hotels`, `hotelbeds_facilities` ,`hotelbeds_facilities_description` where hotelbeds_Hotels.HotelCode = hotelbeds_facilities.HOTELCODE and hotelbeds_facilities_description.CODE = hotelbeds_facilities.CODE and hotelbeds_Hotels.HotelCode = '$hottel_code' and hotelbeds_Hotels.DestinationCode = '$cittty_code' GROUP BY hotelbeds_facilities_description.NAME");
	return $query->result();*/
	/* $query = $this->db->query("select hotelbeds_Hotels.*, 
		GROUP_CONCAT(hotelbeds_facilities_description.NAME) as HotelFacilityName
		  from `hotelbeds_Hotels` Left Join `hotelbeds_facilities` on  hotelbeds_Hotels.HotelCode = hotelbeds_facilities.HOTELCODE,  
		  `hotelbeds_facilities_description` Left Join `hotelbeds_facilities` on hotelbeds_facilities_description.CODE = hotelbeds_facilities.CODE  
		   where 
		  and hotelbeds_Hotels.HotelCode = '$hottel_code' 
	  and hotelbeds_Hotels.DestinationCode = '$cittty_code' 
	  group by hotelbeds_Hotels.HotelCode");
	 echo "select hotelbeds_Hotels.*, 
		GROUP_CONCAT(DISTINCT hotelbeds_facilities_description.NAME) as HotelFacilityName
		  from `hotelbeds_Hotels`, `hotelbeds_facilities` ,`hotelbeds_facilities_description` where hotelbeds_Hotels.HotelCode = hotelbeds_facilities.HOTELCODE
	  and hotelbeds_facilities_description.CODE = hotelbeds_facilities.CODE 
	  and hotelbeds_Hotels.HotelCode = '$hottel_code' 
	  and hotelbeds_Hotels.DestinationCode = '$cittty_code' 
	  group by hotelbeds_Hotels.HotelCode";exit;
	return $query->result();*/

		$this->db->select("*");
		$this->db->from("hotelbeds_Hotels");
		$this->db->where('hotelbeds_Hotels.HotelCode' , $hottel_code);
        $this->db->where('hotelbeds_Hotels.DestinationCode' , $cittty_code);
		$res = $this->db->get();
   
		if($res->num_rows() > 0 ) {
			return $res->row();
		}
		return '';
	}

	public function addtocart($data) {
        $this->db->insert('cart_hotel', $data);
        return $this->db->insert_id();
    }
    public function insert_cart_global($cart_global) {
      $this->db->insert('cart_global', $cart_global);
      return $this->db->insert_id();
    }
    
    function get_filter_result($session_id,$post){
        $max = $post['max'];
        $min = $post['min'];
        $star_rating = $post['starRating'];
        $rate = '';
        for ($i=0; $i < count($star_rating); $i++) { 
           $rate .= $star_rating[$i].' OR ';
        }
        /*$sort = explode(',',$post['hotel_sorting']);
        if(($sort[0])==1){
            $order = "ORDER BY hotel_name $sort[1]";
        }elseif ($sort[0] == 2) {
           $order = "ORDER BY amount $sort[1]";
        }else{
            $order = "ORDER BY star_rating $sort[1]";
        }*/

        if(!empty($post['hotel_namefilter'])){
            $name = $post['hotel_namefilter'];
            $like = "AND hotel_name LIKE '%$name%'";
        }else{
            $like = "";
        }
        //echo "<pre/>";print_r($like);exit;
        $star = substr($rate, 0, -3);
        $sql = "SELECT * FROM tf_hotel_result WHERE ((session_id = '$session_id') AND (amount >= '$min' AND amount <= '$max') AND (star_rating = $star) $like) ";
        //echo $sql;exit;
        $query = $this->db->query($sql);
        if($query->num_rows > 0){
            return $query->result();
        }
    }
    
    function get_last_response_count($session_id,$cond = array())
{ 
	// echo $session_id;
	//~ print_r($cond);exit;
    $this->db->select('*');
    
        $this->db->where('tf_hotel_result.session_id',$session_id);
       
    if(count($cond) > 0)
    {
        $this->db->where($cond['amount_filter']);
        if($cond['hotel_name'] != NULL )
        {
            $this->db->like('tf_hotel_result.hotel_name', $cond['hotel_name']);
        }
        
  if($cond['star_rating'] !== NULL &&  count($cond['star_rating']) > 0 )
        {
             
           for($s=0; $s < count($cond['star_rating']); $s++){
        /*      if($s==0){
                    $this->db->like('star_rating', $cond['star_rating'][$s]);
                } else {
                    $this->db->or_like('star_rating', $cond['star_rating'][$s]);
                }   */
                // changed by siva
                $rating = $cond['star_rating'][$s];
               // $likes[] = "star_rating LIKE '%$rating%'"; 
                 $likes[] = "star_rating = '$rating'";         
                
           }
           $likesQ = '('.implode(' || ', $likes).')';;
           $this->db->where($likesQ, NULL, FALSE);
        } 

        if($cond['accommodation'] !== NULL &&  count($cond['accommodation']) > 0 )
        {
           $kx=array();
            for($a = 0; $a< count($cond['accommodation']); $a++){
                
                 $accommodation = mysql_real_escape_string($cond['accommodation'][$a]);
                 $accommodation = '"'.$accommodation.'"';
                 //$kx[] = "tf_hotel_result.locations LIKE '%$accommodation%'"; 
                 $kx[] = "find_in_set('".$accommodation."',REPLACE(REPLACE(tf_hotel_result.locations,'[',''),']','')) > 0";
                //if($a==0){
                    //$this->db->like('api_hotel_detail_t.inclusion', $cond['accommodation'][$a]);
                //} else {
                    
                    //$this->db->or_like('api_hotel_detail_t.inclusion', $cond['accommodation'][$a]);
                    //$l=$cond['accommodation'][$a];
                    //$kx[] = "tf_hotel_result.inclusion LIKE '%$l%'";
                //}
            }
            $accQ = "(".implode(" || ",$kx).")";
            $this->db->where($accQ); 
        }
        if($cond['ammenities'] !== NULL &&  count($cond['ammenities']) > 0 )
        {
              $kxs=array();
           for($am = 0; $am < count($cond['ammenities']); $am++){
                /*if($am==0){
                    $this->db->like('api_hotel_detail_t.hotel_facility', $cond['ammenities'][$am]);
                } else {
                    $this->db->or_like('api_hotel_detail_t.hotel_facility', $cond['ammenities'][$am]);
                }*/
                $ls= mysql_real_escape_string($cond['ammenities'][$am]);
				//~ $kxs[] = "tf_hotel_result.hotel_facility LIKE '%$ls%'";
				$kxs[] = "locate('".$ls."',tf_hotel_result.hotel_facility) > 0";
            }
            $this->db->where("(".implode(" || ",$kxs).")"); 
        } 
        $this->db->order_by($cond['sort_col'],$cond['sort_val']);
        //~ $this->db->order_by('amount','asc');
        # next condition
    }
    else
    {
        $this->db->order_by('amount','asc');
    }

 
    
    
      $this->db->group_by('hotel_code');
    $result = $this->db->get('tf_hotel_result');
	//echo $this->db->last_query();exit;
   // debug($this->db->last_query());
    return $result->num_rows();
}
	
	public function getHBHotelLocations($hb_code){
        $this->db->where('hotel_code', $hb_code);
        $result = $this->db->get('hotelbeds_interestPoints');
        return $result; 
    }
    public function getHBHotelAmenities($hb_code) {
        $this->db->where('HOTELCODE', $hb_code);
        $this->db->where('GROUP_', '70');
        $result = $this->db->get('hotelbeds_facilities');
        return $result;
    }
    public function getHBHotelDesc($code, $l_code){
        $this->db->where('CODE', $code);
        $this->db->where('GROUP_', '70');
        $this->db->where('LANGUAGECODE', $l_code);
        $result = $this->db->get('hotelbeds_facilities_description');
        return $result;
    }
    public function getHBRoomAmenities($hb_code){
		$this->db->where('HOTELCODE', $hb_code);
        $this->db->where('GROUP_', '60');
        $result = $this->db->get('hotelbeds_facilities');
        return $result;
    }
    public function getHBRoomDesc($code, $l_code){
        $this->db->where('CODE', $code);
        $this->db->where('GROUP_', '60');
        $this->db->where('LANGUAGECODE', $l_code);
        $result = $this->db->get('hotelbeds_facilities_description');
        return $result;
    }
    function get_contact_details($hotel_code){
         $this->db->select('*');
         $this->db->from('hotelbeds_hotel_phone_numbers');
         $this->db->where('hotel_code', $hotel_code);
         $query=$this->db->get();
         return $query;
    }
    public function get_hotel_information_all($hotelid) {

		$this->db->where('HotelCode', $hotelid);
        $this->db->join('hotelbeds_Destinations', 'hotelbeds_Destinations.DestinationCode = hotelbeds_Hotels.DestinationCode');
		$query = $this->db->get('hotelbeds_Hotels');
     
        if ($query->num_rows() > 0)
        {
            return $query->row();
        } 
        else{
            return '';
        }
    }
    
    
    
    function get_city_details($city) {
		$city = explode(',',$city);
		$city_name = trim($city[0]);
		$country = trim($city[1]);
		
        $this->db->select('*');
        $this->db->from('hotelbeds_cities');
        $this->db->like('city', $city_name, 'after');
        $this->db->where('countryName', $country);
        $query = $this->db->get();
        //~ echo $this->db->last_query();exit;
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->row();
        }
    }
    function insert_hotelsbed_temp_result($avalable_data) {
        $this->db->insert('api_hotel_detail_t', $avalable_data);
        return true;
    }
    public function fetch_gta_temp_result_room_m1($ses_id, $hotel_code) {
        $que = "SELECT * FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id'  GROUP BY Classification_val ORDER BY api_temp_hotel_id";
        $query = $this->db->query($que);
        //~ echo $this->db->last_query();exit;
        return $query->result();
    }
    public function fetch_gta_temp_result_room_m2($ses_id, $hotel_code, $classval) {

        $que = "SELECT * FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id' AND `Classification_val` = '$classval'  GROUP BY Promotionsaa ORDER BY api_temp_hotel_id";
        //echo $que;exit;
        $query = $this->db->query($que);

        return $query->result();
    }
    public function fetch_gta_temp_result_room_m3($ses_id, $hotel_code, $classval, $prom) {

        $que = "SELECT * FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id' AND `Classification_val` = '$classval' AND `Promotionsaa` = '$prom' GROUP BY room_type ORDER BY api_temp_hotel_id";
        //echo $que;exit;
        $query = $this->db->query($que);

        return $query->result();
    }
    public function fetch_gta_temp_result_room_m4($ses_id, $hotel_code, $classval, $prom, $room_type) {


        $que = "SELECT * FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id' AND `Classification_val` = '$classval' AND `Promotionsaa` = '$prom' AND `room_type` = '$room_type' GROUP BY inclusion,room_type ORDER BY api_temp_hotel_id,total_cost";
        //echo $que;exit;
        $query = $this->db->query($que);

        return $query->result();
    }
    public function fetch_gta_temp_result_room_m5($ses_id, $hotel_code, $classval, $prom, $room_type, $incl) {
        $que = "SELECT *,min(total_cost) as least_cost FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id' AND `Classification_val` = '$classval' AND `Promotionsaa` = '$prom' AND `room_type` = '$room_type' AND `inclusion` = '$incl' ORDER BY api_temp_hotel_id,total_cost";

        $query1 = $this->db->query($que);

        if ($query1->num_rows() == '') {
            return '0';
        } else {
            $res = $query1->result();

            $least_cost = $res[0]->least_cost;

            $que1 = "SELECT * FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id' AND `Classification_val` = '$classval' AND `Promotionsaa` = '$prom' AND `room_type` = '$room_type' AND `inclusion` = '$incl' AND `total_cost` = '$least_cost' ORDER BY api_temp_hotel_id,total_cost";

            $query = $this->db->query($que1);
            return $query->result();
        }
    }
    public function fetch_gta_temp_result_room_m2_v1($ses_id, $hotel_code, $classval) {
        $que = "SELECT * FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id' AND `Classification_val` = '$classval'  GROUP BY Promotionsaa ORDER BY amount ASC";
       // echo $que;die;
        $query = $this->db->query($que);

        return $query->result();
    }
    public function fetch_gta_temp_result_room_m3_v1($ses_id, $hotel_code, $classval, $prom) {
        $que = "SELECT * FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id' AND `Classification_val` = '$classval' AND `Promotionsaa` = '$prom' GROUP BY inclusion ORDER BY amount ASC";
      
        $query = $this->db->query($que);

        return $query->result();
    }
    public function fetch_gta_temp_result_room_m4_v1($ses_id, $hotel_code, $classval, $prom, $inclusion) {
        $que = "SELECT * FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id' AND `Classification_val` = '$classval' AND `Promotionsaa` = '$prom' AND `inclusion` = '$inclusion' group by adult,child  ORDER BY amount ASC";
        $query1 = $this->db->query($que);
        if ($query1->num_rows() == '') {
            return '';
        } else {
            $res = $query1->result();
            $aa = array();
            for ($k = 0; $k < count($res); $k++) {
                $adult = $res[$k]->adult;
                $child = $res[$k]->child;
                $que2 = "SELECT *,min(amount) as least_cost FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id' AND `Classification_val` = '$classval' AND `Promotionsaa` = '$prom' AND `inclusion` = '$inclusion' AND `adult` = '$adult' AND `child` = '$child' ORDER BY amount ASC";
                $query2 = $this->db->query($que2);

                $res2 = $query2->result();

                $least_cost = $res2[0]->least_cost;

                $que1 = "SELECT * FROM (`api_hotel_detail_t`) WHERE `hotel_code` = '$hotel_code' AND `session_id` = '$ses_id' AND `Classification_val` = '$classval' AND `Promotionsaa` = '$prom' AND `inclusion` = '$inclusion' AND `amount` = '$least_cost'  AND `adult` = '$adult' AND `child` = '$child'";
                $query = $this->db->query($que1);
                $aa[] = $query->result();
            }
            return $aa;
        }
    }
    function get_hotel_detail_list($hotel_code, $hotelid='') {
        $query = $this->db->query("SELECT * FROM api_hotel_detail_t temp, hotelbeds_Hotels hotel WHERE temp.hotel_code = hotel.HotelCode AND temp.hotel_code = '$hotel_code' AND temp.api_temp_hotel_id = '$hotelid'");
     
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
    function add_cart_details_hotel($ses_id, $details) {
        $details = json_decode(base64_decode($details), 1);
        $result_data = array();
        $citydata = $this->get_city_details($details['request_data']['city']);
        $requests = base64_encode(json_encode($details['request_data']));
        
        $split_room = explode("-", $details['api_temp_id']);
        $sec_city = $details['request_data']['city'];
        $sec_cin = $details['request_data']['hotel_checkin'];
        $sec_cout = $details['request_data']['hotel_checkout'];
        $sec_room_count = $details['request_data']['rooms'];
        $sec_adult_count = array_sum($details['request_data']['adult']);
        $sec_child_count = array_sum($details['request_data']['child']);
        $diff = date_diff(date_create($sec_cin),date_create($sec_cout));
        $sec_days = $diff->format('%a');
        
        for ($k = 0; $k < count($split_room); $k++) {
            $this->db->select('*');
            $this->db->from('api_hotel_detail_t');
            $this->db->where('api_hotel_detail_t.api_temp_hotel_id', $split_room[$k]);
            $this->db->join('hotelbeds_Hotels', 'api_hotel_detail_t.hotel_code = hotelbeds_Hotels.HotelCode');
            $query = $this->db->get();
            $result_data[] = $query->row();    
       }
       //echo '<pre/>';print_r($result_data);exit;
       if($this->session->userdata('user_id')){
            $user_type =$this->session->userdata('user_type');
            $user_id = $this->session->userdata('user_details_id');
        }else{
            $user_type = '';
            $user_id = '';
        }
        $api_temp_hotel_id_key ='';   $room_code='';   $room_type='';   $inclusion='';
        $shurival='';   $charval='';   $adult='';   $child='';  $board_type='';     $token='';
        $total_cost=0;  $inoffcode='';  $contractnameVal='';    $room_count='';     $rate_typeval='';
        $destCodeVal='';    $shortname='';  $child_age='';  $org_amt='';    $policy_description='';     $hotel_shop_id_main='';
        
        
        if (isset($result_data[0]->api_temp_hotel_id) && $result_data[0]->api_temp_hotel_id != '') {
            for ($fk = 0; $fk < count($result_data); $fk++) {
				
                    $api_temp_hotel_id_key.=$result_data[$fk]->api_temp_hotel_id . "<br>";
                    $room_code.=$result_data[$fk]->room_code . "<br>";
                    $room_type.=$result_data[$fk]->room_type . "<br>";
                    $inclusion.=$result_data[$fk]->inclusion . "<br>";
                    $shurival.=$result_data[$fk]->shurival . "<br>";
                    $charval.=$result_data[$fk]->charval . "<br>";
                    
                    for($rc=0; $rc<$result_data[$fk]->room_count; $rc++){
                        $adult.=$result_data[$fk]->adult . "<br>";
                        $child.=$result_data[$fk]->child . "<br>";
                        $child_age.=$result_data[$fk]->child_age . "<br>";
                    }
                    
                    $org_amt.=$result_data[$fk]->amount. "<br>";
                    $board_type.=$result_data[$fk]->board_type . "<br>";
                    $token.=$result_data[$fk]->token . "<br>";
                    $total_cost = $total_cost + $result_data[$fk]->amount;
                    $inoffcode.=$result_data[$fk]->inoffcode . "<br>";
                    $contractnameVal.=$result_data[$fk]->contractnameVal . "<br>";
                    $room_count.=$result_data[$fk]->room_count . "<br>";
                    $rate_typeval.=$result_data[$fk]->rate_typeval . "<br>";
                    $destCodeVal.=$result_data[$fk]->destCodeVal . "<br>";
                    $shortname.=$result_data[$fk]->shortname . "<br>";
                    
                    $policy_description = $result_data[$fk]->policy_description."<br/>";
            
            }
            $contry = $this->Hotel_Model->get_country_details($citydata->cityCode);
            $api = $result_data[0]->api;
           
            if($result_data[0]->hotel_images != '')
                $imagedata = $result_data[0]->hotel_images;
            else
                $imagedata = str_replace('-', ',', $result_data[0]->hotel_api_images);
                
                
             
            $data = array(
                   // 'parent_cart_id'            => $details['api_temp_id'],
                   // 'api_temp_hotel_id_key'     => $api_temp_hotel_id_key,
                    //'domain_id'                 => 1,
                    'hotel_name'                => $result_data[0]->Name,
                    'session_id'                => $result_data[0]->session_id,
                    'hotel_code'                => $result_data[0]->hotel_code,
                    'api_id'                       => $result_data[0]->api,
                    'room_id'                 => $room_code,
                    'room_type'                 => $room_type,
                    'inclusion'                 => $inclusion,
                    'total_cost'                => $total_cost,
                    //'status'                    => $result_data[0]->status,
                    'shurival'                  => $shurival,
                    'charval'                   => $charval,
                    'adult'                     => $adult,
                    'child'                     => $child,
                    'board_type'                => $board_type,
                    'token_id'                     => $token,
                    'inoffcode'                 => $inoffcode,
                    'contractnameVal'           => $contractnameVal,
                    'destCodeVal'               => $destCodeVal,
                    'shortname'                 => $shortname,
                    'room_count'                => $room_count,
                    'city'                      => $result_data[0]->city,
                    'Promotionsaa'              => $result_data[0]->Promotionsaa,
                    'ShortNameaa'               => $result_data[0]->ShortNameaa,
                    'Classification_val'        => $result_data[0]->Classification_val,
                    'des_offer_value'           => $result_data[0]->des_offer_value,
                    'Remarksaa'                 => $result_data[0]->Remarksaa,
                    'star'                      => $result_data[0]->CategoryCode,
                    'images'                     => $imagedata,
                    'description'               => $result_data[0]->hotel_info,
                    'rate_typeval'              => $rate_typeval,
                    'child_age'                 => $child_age,
                    'admin_markup'               => $result_data[0]->admin_markup,
                    'admin_baseprice'               => $result_data[0]->admin_baseprice,
                    'my_markup'               => $result_data[0]->my_markup,
                    'agent_baseprice'               => $result_data[0]->agent_baseprice,
                    'my_agent_Markup'               => $result_data[0]->my_agent_Markup,
                    'sub_agent_baseprice'               => $result_data[0]->sub_agent_baseprice,
                    'my_b2b2b_markup'               => $result_data[0]->my_b2b2b_markup,
                    'b2b2b_baseprice'               => $result_data[0]->b2b2b_baseprice,
                    'service_charge'               => $result_data[0]->service_charge,
                    'gst_charge'               => $result_data[0]->gst_charge,
                    'tax_charge'               => $result_data[0]->tax_charge,
                    'cancel_policy'             => $result_data[0]->policy_description,
                    'longitude'                 => $result_data[0]->Longitude,
                    'latitude'                  => $result_data[0]->Latitude,
                    'sec_city'                  => $sec_city,
                    'sec_city_code'             => $citydata->cityCode,
                    'checkin'                   => $sec_cin,
                    'checkout'                  => $sec_cout,
                    'hotel_address_full'        => $result_data[0]->Address,
                    'city'                      => $result_data[0]->city,
                    'country_name'              => $result_data[0]->countryCode,
                    'postal_code'               => $result_data[0]->PostalCode,
                    'org_amt'                   => $org_amt,
                    'API_CURR'              	=> $result_data[0]->xml_currency,
                    'SITE_CURR'             	=> BASE_CURRENCY,
                    'sec_cin'                   => $sec_cin,
                    'sec_cout'                  => $sec_cout,
                    'sec_room_count'            => $details['request_data']['rooms'],
                    'sec_adult'                 => array_sum($details['request_data']['adult']),
                    'sec_child'                 => array_sum($details['request_data']['child']),
                    'request_info'              => json_encode($details['request_data']),
                    'request'              => $requests
                );
                
              
                $cart_hotel_id = $this->insert_cart_hotel($data, $ses_id);
               
                $cart_global = array(
                        'parent_cart_id' => 0,
                        'referal_id' => $cart_hotel_id,
                        'product_id' => '1',
                        'user_type' => $user_type,
                        'user_id' => $user_id,
                        'session_id' => $ses_id,
                        'site_currency' => BASE_CURRENCY,
                        'total_cost' => $total_cost,
                        'ip_address' =>  $this->input->ip_address(),
                        'timestamp' => date('Y-m-d H:i:s')
                    );
                //'session_id'      => $this->session->userdata('session_id'),
                $cart_global_id = $this->insert_cart_global($cart_global);
                $this->db->query("UPDATE cart_hotel SET shopping_global_id='$cart_global_id' WHERE id='$cart_hotel_id'"); 
                $data['cart_hotel_id'] = $cart_hotel_id;
                $data['shopping_global_id'] = $cart_global_id;
                $data['cart_status'] = 1;
                $data['isCart'] = true;
                $data['C_URL'] = base_url().'booking/index/'.$ses_id;
        }  else {
            $data['isCart'] = false;
        }
        return $data;
    }
    
    public function insert_cart_hotel($cart_hotel){
        $user_type =  $user_id = '';
        if($this->session->userdata('user_details_id')) {
			$user_type =$this->session->userdata('user_type');
			$user_id = $this->session->userdata('user_details_id');
			$this->db->where('cart_global.user_id', $user_id);
			$this->db->delete('cart_global');
        }
        unset($cart_hotel->id);
        $this->db->insert('cart_hotel',$cart_hotel);
        return $this->db->insert_id();
    }
    function get_country_details($city_code) {
        $this->db->select('*');
        $this->db->from('hotelbeds_cities');
        $this->db->where('cityCode', $city_code);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row(); 
        } else {
            return '';
        }
    }
    
    function get_hotel_address($hotel_code, $city_code){
		$this->db->select('*');
		$this->db->from('gta_Hotels');
		$this->db->where('HotelCode', $hotel_code);
		$this->db->where('CityCode', $city_code);
		return $this->db->get()->row();
	}
	
	function dotw_hotel_details($hotel_code = '', $city_code=''){
		$this->db->select('*');
		$this->db->from('Dotw_hoteldetails');
		$this->db->where('hotel_code', $hotel_code);
		if($city_code != ''){
		$this->db->where('city', $city_code);	
		}
		return $this->db->get();
	}
	
	function get_room_details_dotw($hotel_code, $session_data){
		$this->db->select('*');
		$this->db->from('api_hotel_detail_t');
		$this->db->where('session_id', $session_data);
		$this->db->where('hotel_code', $hotel_code);
		$this->db->group_by('option_id');
		$this->db->order_by('total_cost');
		return $this->db->get()->result();
	}
    
   }
?>
