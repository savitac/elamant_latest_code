<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_Model extends CI_Model {
 
    public function get_xml_log_id($xml_log_id) {
	 
    	$this->db->where('xml_logs_id',$xml_log_id);
		$query = $this->db->get('xml_logs');
		if ( $query->num_rows > 0 ) {
       		return $query->row();
		}
		else
		{
      		return '';
		}
    }
	public function get_recent_billing_details($user_details_id, $user_type){
		$this->db->where('user_id',$user_details_id);
		$this->db->order_by('billing_address_id','DESC');
		$query = $this->db->get('booking_billing_address');
		if ( $query->num_rows > 0 ) {
       		return $query->row();
		}
		else
		{
      		return '';
		}
    }
	  public function getBookingPnr_jul4($parent_pnr_no){
		 
		  $this->db->join('product','product.product_id = booking_global.product_id','LEFT');
		$this->db->where('booking_global.parent_pnr_no',$parent_pnr_no);
	 return  $this->db->get('booking_global');
	  // $this->db->last_query();exit();
	  
	   
    }

    public function get_booking_activity_details($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){
           $query = 'SELECT * FROM  sightseeing_booking_details AS BD LEFT JOIN sightseeing_booking_itinerary_details ON BD.app_reference = sightseeing_booking_itinerary_details.app_reference LEFT JOIN sightseeing_booking_pax_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC' ; 
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM sightseeing_booking_details AS BD LEFT JOIN sightseeing_booking_itinerary_details ON BD.app_reference = sightseeing_booking_itinerary_details.app_reference LEFT JOIN sightseeing_booking_pax_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC'; 
       }
        return $this->db->query($query)->result();

       $this->db->select('sightseeing_booking_details.*,sightseeing_booking_itinerary_details.*,sightseeing_booking_pax_details.*,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('sightseeing_booking_itinerary_details','sightseeing_booking_pax_details.app_reference=sightseeing_booking_details.app_reference','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('sightseeing_booking_itinerary_details','sightseeing_booking_itinerary_details.app_reference=sightseeing_booking_details.app_reference','LEFT');
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','sightseeing_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','sightseeing_booking_details.user_details_id=user_details.user_details_id');

        $this->db->group_by('sightseeing_booking_details.app_reference');
        $this->db->where($condition);
        $result = $this->db->get('sightseeing_booking_details');
        #echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;

    }

    public function get_booking_transfer_details($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){
           $query = 'SELECT * FROM transferv1_booking_details AS BD LEFT JOIN   transferv1_booking_itinerary_details ON BD.app_reference = transferv1_booking_itinerary_details.app_reference LEFT JOIN transferv1_booking_pax_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC' ; 
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM transferv1_booking_details AS BD LEFT JOIN transferv1_booking_itinerary_details ON BD.app_reference = transferv1_booking_itinerary_details.app_reference LEFT JOIN transferv1_booking_pax_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC'; 
       }
        return $this->db->query($query)->result();

       $this->db->select('transferv1_booking_details.*,transferv1_booking_itinerary_details.*,transferv1_booking_pax_details.*,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('transferv1_booking_itinerary_details','transferv1_booking_pax_details.app_reference=transferv1_booking_details.app_reference','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('transferv1_booking_itinerary_details','transferv1_booking_itinerary_details.app_reference=transferv1_booking_details.app_reference','LEFT');
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','transferv1_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','transferv1_booking_details.user_details_id=user_details.user_details_id');

        $this->db->group_by('transferv1_booking_details.app_reference');
        $this->db->where($condition);
        $result = $this->db->get('transferv1_booking_details');
        #echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;

    }

    public function get_booking_transfer_details_sub($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){
           $query = 'SELECT * FROM transferv1_booking_details AS BD LEFT JOIN transferv1_booking_itinerary_details ON BD.app_reference = transferv1_booking_itinerary_details.app_reference LEFT JOIN transferv1_booking_pax_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC' ; 
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM transferv1_booking_details AS BD LEFT JOIN transferv1_booking_itinerary_details ON BD.app_reference = transferv1_booking_itinerary_details.app_reference LEFT JOIN transferv1_booking_pax_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC'; 
       }
        return $this->db->query($query)->result();

       $this->db->select('transferv1_booking_details.*,transferv1_booking_itinerary_details.*,transferv1_booking_pax_details.*,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('transferv1_booking_pax_details','transferv1_booking_pax_details.app_reference=transferv1_booking_details.app_reference','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('transferv1_booking_itinerary_details','transferv1_booking_itinerary_details.app_reference=transferv1_booking_details.app_reference','LEFT');
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','transferv1_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','transferv1_booking_details.user_details_id=user_details.user_details_id');

        $this->db->group_by('transferv1_booking_details.app_reference');
        $this->db->where($condition);
        $result = $this->db->get('transferv1_booking_details');
        debug($result);die;
        #echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;

    } 

    public function get_booking_activity_details_sub($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){
           $query = 'SELECT * FROM sightseeing_booking_details AS BD LEFT JOIN sightseeing_booking_itinerary_details ON BD.app_reference = sightseeing_booking_itinerary_details.app_reference LEFT JOIN sightseeing_booking_pax_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC' ; 
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM sightseeing_booking_details AS BD LEFT JOIN sightseeing_booking_itinerary_details ON BD.app_reference = sightseeing_booking_itinerary_details.app_reference LEFT JOIN sightseeing_booking_pax_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC'; 
       }
        return $this->db->query($query)->result();

       $this->db->select('sightseeing_booking_details.*,sightseeing_booking_itinerary_details.*,sightseeing_booking_pax_details.*,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('sightseeing_booking_pax_details','sightseeing_booking_pax_details.app_reference=sightseeing_booking_details.app_reference','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('sightseeing_booking_itinerary_details','sightseeing_booking_itinerary_details.app_reference=sightseeing_booking_details.app_reference','LEFT');
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','sightseeing_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','sightseeing_booking_details.user_details_id=user_details.user_details_id');

        $this->db->group_by('sightseeing_booking_details.app_reference');
        $this->db->where($condition);
        $result = $this->db->get('sightseeing_booking_details');
        #echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;

    } 

    public function get_booking_flight_details($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){
           $query = 'SELECT * FROM flight_booking_details AS BD LEFT JOIN flight_booking_passenger_details ON BD.app_reference = flight_booking_passenger_details.app_reference LEFT JOIN flight_booking_transaction_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC' ; 
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM flight_booking_details AS BD LEFT JOIN flight_booking_passenger_details ON BD.app_reference = flight_booking_passenger_details.app_reference LEFT JOIN flight_booking_transaction_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC'; 
       }
        return $this->db->query($query)->result();

       $this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('flight_booking_transaction_details','flight_booking_transaction_details.app_reference=flight_booking_details.app_reference','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('flight_booking_passenger_details','flight_booking_passenger_details.app_reference=flight_booking_details.app_reference','LEFT');
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','flight_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','flight_booking_details.user_details_id=user_details.user_details_id');

        $this->db->group_by('flight_booking_details.app_reference');
        $this->db->where($condition);
        $result = $this->db->get('flight_booking_details');
        #echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;

    }  
    public function get_booking_flight_details_sub($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){
           $query = 'SELECT * FROM flight_booking_details AS BD LEFT JOIN flight_booking_passenger_details ON BD.app_reference = flight_booking_passenger_details.app_reference LEFT JOIN flight_booking_transaction_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC' ; 
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM flight_booking_details AS BD LEFT JOIN flight_booking_passenger_details ON BD.app_reference = flight_booking_passenger_details.app_reference LEFT JOIN flight_booking_transaction_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC'; 
       }
        return $this->db->query($query)->result();

       $this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('flight_booking_transaction_details','flight_booking_transaction_details.app_reference=flight_booking_details.app_reference','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('flight_booking_passenger_details','flight_booking_passenger_details.app_reference=flight_booking_details.app_reference','LEFT');
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','flight_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','flight_booking_details.user_details_id=user_details.user_details_id');

        $this->db->group_by('flight_booking_details.app_reference');
        $this->db->where($condition);
        $result = $this->db->get('flight_booking_details');
        #echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;

    }  
    public function get_booking_flight_details_sum($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){
            $query = 'SELECT SUM(total_fare) as total_sum FROM flight_booking_details AS BD LEFT JOIN flight_booking_passenger_details ON BD.app_reference = flight_booking_passenger_details.app_reference LEFT JOIN flight_booking_transaction_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'"'; 
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM flight_booking_details AS BD LEFT JOIN flight_booking_passenger_details ON BD.app_reference = flight_booking_passenger_details.app_reference LEFT JOIN flight_booking_transaction_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference';
       }
        return $this->db->query($query)->result();
    }

    public function get_booking_flight_details_current_month_sum($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){
            $query = 'SELECT SUM(total_fare) as total_month_sum FROM flight_booking_details AS BD LEFT JOIN flight_booking_passenger_details ON BD.app_reference = flight_booking_passenger_details.app_reference LEFT JOIN flight_booking_transaction_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE MONTH(created_datetime)=MONTH(CURRENT_DATE()) AND BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'"'; 
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM flight_booking_details AS BD LEFT JOIN flight_booking_passenger_details ON BD.app_reference = flight_booking_passenger_details.app_reference LEFT JOIN flight_booking_transaction_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference';
       }
        return $this->db->query($query)->result();
    }

    public function get_booking_hotel_details($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){

           $query = 'SELECT  * FROM hotel_booking_details AS BD LEFT JOIN hotel_booking_pax_details ON BD.app_reference = hotel_booking_pax_details.app_reference LEFT JOIN hotel_booking_itinerary_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC'; 

           
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM hotel_booking_details AS BD LEFT JOIN hotel_booking_pax_details ON BD.app_reference = hotel_booking_pax_details.app_reference LEFT JOIN hotel_booking_itinerary_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC';
       }
        return $this->db->query($query)->result();
        exit;


      $this->db->select('hotel_booking_details.*,hotel_booking_itinerary_details.*,hotel_booking_pax_details.*,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('hotel_booking_itinerary_details','hotel_booking_itinerary_details.app_reference=hotel_booking_details.app_reference','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('hotel_booking_pax_details','hotel_booking_pax_details.app_reference=hotel_booking_details.app_reference','LEFT');
        /*$this->db->join('hotel_cancellation_details','hotel_cancellation_details.app_reference=hotel_booking_details.app_reference','LEFT');*/
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','hotel_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','hotel_booking_details.user_details_id=user_details.user_details_id');

        $this->db->group_by('hotel_booking_details.app_reference');
        $this->db->where(array('hotel_booking_details.user_details_id' => $user_details_id, 'hotel_booking_details.branch_id' => $branch_id, 'hotel_booking_details.domain_id' => $domain_id ));
        $result = $this->db->get('hotel_booking_details');
        #echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;

    } 

    public function get_booking_hotel_crs_details($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count != 0) {
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
        }

        $query = 'SELECT  * FROM booking_global AS BG JOIN booking_hotel AS BH ON BG.ref_id = BH.id JOIN hotel_details AS HD ON BG.hotel_details_id = HD.hotel_details_id LEFT JOIN domain_details ON BG.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BG.user_details_id=user_details.user_details_id WHERE BG.branch_id = "'.$branch_id.'" AND BG.domain_id = "'.$domain_id.'" AND HD.hotel_type_id !='."'". VILLA . "'" . $cond . ' GROUP BY BG.ref_id ORDER BY BG.id DESC';   

        $result = $this->db->query($query);
        if($result->num_rows() > 0 )
        {
            return $result->result_array();
        }
        return '' ;
    } 

     public function get_booking_villa_details($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count != 0) {
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
        }
           $query = 'SELECT  * FROM booking_global AS BG JOIN booking_hotel AS BH ON BG.ref_id = BH.id JOIN hotel_details AS HD ON BG.hotel_details_id = HD.hotel_details_id LEFT JOIN domain_details ON BG.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BG.user_details_id=user_details.user_details_id WHERE BG.branch_id = "'.$branch_id.'" AND BG.domain_id = "'.$domain_id.'" AND HD.hotel_type_id ='."'".VILLA."'". $cond . ' GROUP BY BG.ref_id ORDER BY BG.id DESC'; 
       
        $result = $this->db->query($query);
        if($result->num_rows() > 0 )
        {
            return $result->result_array();
        }
        
        return '' ;
    } 

    public function get_booking_hotel_details_sub($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){

           $query = 'SELECT  * FROM hotel_booking_details AS BD LEFT JOIN hotel_booking_pax_details ON BD.app_reference = hotel_booking_pax_details.app_reference LEFT JOIN hotel_booking_itinerary_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC'; 

           
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM hotel_booking_details AS BD LEFT JOIN hotel_booking_pax_details ON BD.app_reference = hotel_booking_pax_details.app_reference LEFT JOIN hotel_booking_itinerary_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC';
       }
        return $this->db->query($query)->result();
        exit;


      $this->db->select('hotel_booking_details.*,hotel_booking_itinerary_details.*,hotel_booking_pax_details.*,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('hotel_booking_itinerary_details','hotel_booking_itinerary_details.app_reference=hotel_booking_details.app_reference','LEFT');
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        $this->db->join('hotel_booking_pax_details','hotel_booking_pax_details.app_reference=hotel_booking_details.app_reference','LEFT');
        /*$this->db->join('hotel_cancellation_details','hotel_cancellation_details.app_reference=hotel_booking_details.app_reference','LEFT');*/
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','hotel_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','hotel_booking_details.user_details_id=user_details.user_details_id');

        $this->db->group_by('hotel_booking_details.app_reference');
        $this->db->where(array('hotel_booking_details.user_details_id' => $user_details_id, 'hotel_booking_details.branch_id' => $branch_id, 'hotel_booking_details.domain_id' => $domain_id ));
        $result = $this->db->get('hotel_booking_details');
        #echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;

    }

    public function get_booking_villa_details_sub($user_details_id,$branch_id,$domain_id,$condition=array())
    {
        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0) {
           // exit('iff');
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
           $query = 'SELECT  * FROM booking_global AS BG JOIN booking_hotel AS BH ON BG.ref_id = BH.id JOIN hotel_details AS HD ON BG.hotel_details_id = HD.hotel_details_id LEFT JOIN domain_details ON BG.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BG.user_details_id=user_details.user_details_id WHERE BG.user_details_id = "'.$user_details_id.'" AND BG.branch_id = "'.$branch_id.'" AND BG.domain_id = "'.$domain_id.'" AND HD.hotel_type_id ='."'".VILLA."'".' GROUP BY BG.ref_id ORDER BY BG.id DESC'; 
        }
       else {
         //exit('elsee');
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM hotel_booking_details AS BD LEFT JOIN hotel_booking_pax_details ON BD.app_reference = hotel_booking_pax_details.app_reference LEFT JOIN hotel_booking_itinerary_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference ORDER BY BD.created_datetime DESC';
       }
        $result = $this->db->query($query);
        if($result->num_rows() > 0 )
        {
            return $result->result_array();
        }
        
        return '' ;

    }

    public function get_booking_bus_details($user_details_id,$branch_id,$domain_id,$condition=array())
    {

        $count = 0;
        $count = count($condition);
        $cond = '';
        if($count == 0){
           $query = 'SELECT * FROM bus_booking_details AS BD LEFT JOIN bus_booking_customer_details ON BD.app_reference = bus_booking_customer_details.app_reference LEFT JOIN bus_booking_itinerary_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" GROUP BY BD.app_reference'; 
       }else{
            for($i=0;$i<$count;$i++){
                $cond .= 'AND '.$condition[$i][0].' '.$condition[$i][1].' '.$condition[$i][2].'';
            }
            $query = 'SELECT * FROM bus_booking_details AS BD LEFT JOIN bus_booking_customer_details ON BD.app_reference = bus_booking_customer_details.app_reference LEFT JOIN bus_booking_itinerary_details AS TD ON BD.app_reference = TD.app_reference LEFT JOIN domain_details ON BD.domain_id = domain_details.domain_details_id LEFT JOIN user_details ON BD.user_details_id=user_details.user_details_id WHERE BD.user_details_id = "'.$user_details_id.'" AND BD.branch_id = "'.$branch_id.'" AND BD.domain_id = "'.$domain_id.'" '.$cond.' GROUP BY BD.app_reference';
       }
        return $this->db->query($query)->result();

         /*if($module == 'Hotels'){
            $this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.booking_hotel_id');
        }*/
    
      $this->db->select('bus_booking_details.*,bus_booking_itinerary_details.*,,domain_details.*,user_details.*');
         //$this->db->select('flight_booking_details.*,flight_booking_passenger_details.*,flight_booking_transaction_details.*');
        $this->db->join('bus_booking_itinerary_details','bus_booking_itinerary_details.app_reference=bus_booking_details.app_reference','LEFT'); 
        /*$this->db->join('user_details','user_details.user_details_id=booking_global.branch_id','LEFT');*/
        /*$this->db->join('bus_cancellation_details','bus_cancellation_details.app_reference=bus_booking_details.app_reference','LEFT');*/
                //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        $this->db->join('domain_details','bus_booking_details.domain_id=domain_details.domain_details_id');
        $this->db->join('user_details','bus_booking_details.user_details_id=user_details.user_details_id');

        $this->db->group_by('bus_booking_details.app_reference');
        $this->db->where(array('bus_booking_details.user_details_id' => $user_details_id, 'bus_booking_details.branch_id' => $branch_id, 'bus_booking_details.domain_id' => $domain_id ));
        $result = $this->db->get('bus_booking_details');
        #echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->result();
        }
        
        return '' ;

    }
    function getBookingPnr_v1($pnr_no){
        $this->db->where('pnr_no',$pnr_no);
        return $this->db->get('booking_global');
    }

    public function getBookingPnr($pnr_no){
		  $this->db->join('product','product.product_id = booking_global.product_id','LEFT');
		$this->db->where('booking_global.pnr_no',$pnr_no);
        return $this->db->get('booking_global');
    }

 public function check_promocode($promo_code){
    	$this->db->where('promo_code',$promo_code);
		$this->db->where('status','ACTIVE');
		return $this->db->get('promo');
    }
	public function insert_booking_flight($booking_flight){
        $this->db->insert('booking_flight',$booking_flight);
        return $this->db->insert_id();
    }
    public function insert_booking_hotel($booking_hotel){
        $this->db->insert('booking_hotel',$booking_hotel);
        return $this->db->insert_id();
    }
    public function update_booking_hotel($booking_id,$booking_hotel){
        $this->db->where('booking_hotel_id', $booking_id);
		$this->db->update('booking_hotel', $booking_hotel); 
    }
     public function insert_booking_car($booking_car){
        $this->db->insert('booking_car',$booking_car);
        return $this->db->insert_id();
    }

    public function getBookingHotelTemp($booking_hotel_id){
        $this->db->where('booking_hotel_id',$booking_hotel_id);
        return $this->db->get('booking_hotel');
    }
    
    public function delete_cart_global($cGlobalId){
		$this->db->where('cart_id', $cGlobalId);
		$this->db->delete('cart_global');
	}
	
	public function delete_cart_hotel($cHotelId){
		$this->db->where('id', $cHotelId);
		$this->db->delete('cart_hotel');
	}
    
    public function get_cart_details($cid){
		$this->db->select('cart_id, referal_id');
		$this->db->where('cart_id', $cid);
		return $this->db->get('cart_global');
	}
    
      
    public function getBookingTransferTemp($booking_transfer_id){
        $this->db->where('booking_transfer_id',$booking_transfer_id);
        return $this->db->get('booking_transfer');
    }
    
     public function getCountryName($country_code){
		$this->db->select('country_name');
        $this->db->where('country_code',$country_code);
        return $this->db->get('country_list');
    }
	public function insert_booking_transaction_data($data){
        $this->db->insert('booking_transaction',$data);
        return $this->db->insert_id();
    }
    public function update_booking_transaction_data($booking_transaction_id, $update_data){
        $this->db->where('booking_transaction_id',$booking_transaction_id);	 
        $this->db->update('booking_transaction', $update_data);
    }
	public function insert_booking_payment_data($data){
        $this->db->insert('booking_payment',$data);
        return $this->db->insert_id();
    }
    public function check_transaction_reference($transaction_reference) {
		$this->db->select('transaction_reference');
		$this->db->where('transaction_reference',$transaction_reference);
		return $this->db->get('booking_payment');
	}
	public function insert_booking_xml_data($data){
        $this->db->insert('booking_xml_data',$data);
        return $this->db->insert_id();
    }
	public function insert_booking_supplier_data($data){
        $this->db->insert('booking_supplier',$data);
        return $this->db->insert_id();
    }
	public function insert_booking_billing_address($data){
        $this->db->insert('booking_billing_address',$data);
        return $this->db->insert_id();
    }
	public function insert_booking_passenger($data){
        $this->db->insert_batch('booking_passenger',$data);
        return $this->db->insert_id();
    }
	public function insert_booking_global_data($booking){
        $this->db->insert('booking_global', $booking);
        return $this->db->insert_id();
    }
	public function getBookingFlightTemp($booking_flight_id){
        $this->db->where('booking_flight_id',$booking_flight_id);
        return $this->db->get('booking_flight');
    }
    public function getBookingCarTemp($booking_car_id){
        $this->db->where('booking_car_id',$booking_car_id);
        return $this->db->get('booking_car');
    }
    public function getBookingCardetailbyid($referal_id){
        $this->db->where('booking_car_id',$referal_id);       
        $query = $this->db->get('booking_car');
        if ( $query->num_rows > 0 ) {        	
       		return $result = $query->result();
		}
    }




	public function getBookingpassengerTempFlight($booking_flight_id){
		$this->db->where('booking_global_id',$booking_flight_id);
		$this->db->where('passenger_type','ADULT');
		$query = $this->db->get('booking_passenger');
		if ( $query->num_rows > 0 ) {
       		$data['ADULT'] = $query->result();
		}else{
      		$data['ADULT'] =  '';
		}
		
		$this->db->where('booking_global_id',$booking_flight_id);
		$this->db->where('passenger_type','CHILD');
		$query = $this->db->get('booking_passenger');
		if ( $query->num_rows > 0 ) {
       		$data['CHILD'] = $query->result();
		}else{
      		$data['CHILD'] =  '';
		}
		
		$this->db->where('booking_global_id',$booking_flight_id);
		$this->db->where('passenger_type','INFANT');
		$query = $this->db->get('booking_passenger');
		if ( $query->num_rows > 0 ) {
       		$data['INFANT'] = $query->result();
		}else{
      		$data['INFANT'] =  '';
		}
		return $data;
	}
	public function getBookingpassengerTempCar($booking_flight_id){
		$this->db->where('booking_global_id',$booking_flight_id);
		$this->db->where('passenger_type','ADULT');
		$query = $this->db->get('booking_passenger');
		if ( $query->num_rows > 0 ) {
       		$data['ADULT'] = $query->result();
		}else{
      		$data['ADULT'] =  '';
		}
		return $data;
	}




	public function getBookingpassengerTemp($booking_flight_id){
		$this->db->where('booking_global_id',$booking_flight_id);
        return $this->db->get('booking_passenger');
    }
	public function getbillingaddressTemp($billingaddressId){
        $this->db->where('billing_address_id',$billingaddressId);
        return $this->db->get('booking_billing_address');
    }
	public function Update_Booking_Global($booking_temp_id, $update_booking){
        $this->db->where('booking_global_id',$booking_temp_id);	 
        $this->db->update('booking_global', $update_booking);
       //echo $this->db->last_query();exit();
    }
    public function Update_Booking_Global_v1($booking_no, $update_booking, $module=""){
        $this->db->where('booking_no',$booking_no);
        $this->db->update('booking_global', $update_booking);
    }
    
    	public function Update_Booking_GlobalAfter($booking_temp_id, $update_booking){
        $this->db->where('booking_global_id',$booking_temp_id);
        $this->db->update('booking_global', $update_booking);
    }
	    public function get_item_code($referal_id){
		//$this->db->select("hotel_code,check_in,check_out, gta_city_code");
		$this->db->from("booking_hotel");
		$this->db->where("id", $referal_id);
	   return $query = $this->db->get()->result();
	 
	 }
	
	public function Update_Booking_xmldata($booking_temp_id, $update_booking){
        $this->db->where('booking_xml_data_id',$booking_temp_id);	 
        $this->db->update('booking_xml_data', $update_booking);
    }
		public function Update_Booking_Supplier($booking_temp_id, $update_booking){
        $this->db->where('booking_supplier_id',$booking_temp_id);
	    $this->db->update('booking_supplier', $update_booking);
	 }

        public function getBookingHotelData($ref_id) {
        $this->db->where('booking_hotel_id', $ref_id);
        return $this->db->get('booking_hotel');
    }
	public function getBookingByParentPnr($parent_pnr){
        $this->db->where('booking_global.parent_pnr_no',$parent_pnr);
		$this->db->join('product_details', 'product_details.product_details_id = booking_global.product_id','LEFT');
		$this->db->join('api_details', 'api_details.api_details_id = booking_global.api_id','LEFT');
		$this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id','LEFT');
        return $this->db->get('booking_global');
    }
	 public function get_booking_supplier_details($supplier_id){
        $this->db->where('booking_supplier.booking_supplier_id',$supplier_id);
		$this->db->join('booking_xml_data', 'booking_xml_data.booking_xml_data_id = booking_supplier.booking_xml_data_id','LEFT');
        return $this->db->get('booking_supplier');
    }
    
	public function getPassangerbyBookingId($bookid){
		$this->db->select('booking_passenger.*');
		$this->db->join('booking_global','booking_global.booking_global_id = booking_passenger.booking_global_id ','LEFT');
		//$this->db->join('booking_hotel','booking_hotel.id = booking_global.referal_id','LEFT');
		$this->db->where('booking_global.referal_id',$bookid);
		$this->db->where('booking_global.product_id',2);
       return  $this->db->get('booking_passenger');
        //return $this->db->last_query();exit;
	}    
    
	
	public function getBookingbyPnr_jun23($pnr_no,$module){
	 if($module == 'FLIGHT'){
			$this->db->join('booking_flight','booking_global.referal_id = booking_flight.booking_flight_id');
		}
	if($module == 'HOTEL'){
		  $this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.id');

	}
			$this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id','LEFT');
			$this->db->join('booking_transaction', 'booking_transaction.booking_transaction_id = booking_global.booking_transaction_id','LEFT');
			$this->db->join('booking_supplier', 'booking_supplier.booking_supplier_id = booking_global.booking_supplier_id','LEFT');
			
			$this->db->join('booking_billing_address', 'booking_billing_address.billing_address_id = booking_global.billing_address_id','LEFT');
			
			$this->db->join('user_type', 'user_type.user_type_id = booking_global.user_type_id','LEFT');
			
			
       		$this->db->where('booking_global.parent_pnr_no',$pnr_no);
       return  $this->db->get('booking_global');
         //echo $this->db->last_query();exit;
    }

    public function getBookingbyPnr($pnr_no,$module){
		
		 if($module == 1){
			$this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.booking_hotel_id');
		}elseif($module == 19){
			$this->db->join('booking_transfer','booking_global.referal_id = booking_transfer.booking_transfer_id');
		}
		$this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id','LEFT');
		$this->db->join('booking_transaction', 'booking_transaction.booking_transaction_id = booking_global.booking_transaction_id','LEFT');
	    $this->db->join('booking_supplier', 'booking_supplier.booking_supplier_id = booking_global.booking_supplier_id','LEFT');
			
			$this->db->join('booking_billing_address', 'booking_billing_address.billing_address_id = booking_global.billing_address_id','LEFT');
			
			$this->db->join('user_type', 'user_type.user_type_id = booking_global.user_type_id','LEFT');
			
			
        $this->db->where('booking_global.pnr_no',$pnr_no);
        $query = $this->db->get('booking_global');
        return $query;
    }

     public function getBookingFlightData($ref_id) {
        $this->db->where('booking_flight_id', $ref_id);
        return $this->db->get('booking_flight');
    }

public function getPGcharges($api, $gateway){
		$this->db->join('api_details', 'payment_gateway_charges.api_id = api_details.api_details_id');
		$this->db->join('gateway', 'gateway.gateway_id = payment_gateway_charges.gateway_id');
		$this->db->where('gateway.status', 1);
		$this->db->where('api_details.api_details_id', $api);
		$this->db->where('gateway.gateway_id', $gateway);
		$this->db->where('gateway_status', 1);
		$data = $this->db->get('payment_gateway_charges');
		
		$count = $data->num_rows();
		if($count == 1){
			$charges = $data->row();
			 return $charges;
		}else{
			return 0;
		}
	}

     public function getGatewayCredentials($payment_mode){
        $this->db->where('gateway_id', $payment_mode);
        $this->db->where('status', '1');
        return $this->db->get('gateway');
    }

    public function Update_Booking_payment($payment_id, $update_booking){
        $this->db->where('booking_payment.payment_id',$payment_id);
        $this->db->update('booking_payment', $update_booking);
    }

    public function getBookingbyPnr_jul4($pnr_no,$module){		
		 if($module == 'FLIGHT'){
			$this->db->join('booking_flight','booking_global.referal_id = booking_flight.booking_flight_id');
		}
	if($module == 'HOTEL'){
		  $this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.booking_hotel_id');

	}
			//$this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id','LEFT');
			//$this->db->join('booking_transaction', 'booking_transaction.booking_transaction_id = booking_global.booking_transaction_id','LEFT');
		//	$this->db->join('booking_supplier', 'booking_supplier.booking_supplier_id = booking_global.booking_supplier_id','LEFT');
			
			$this->db->join('booking_billing_address', 'booking_billing_address.billing_address_id = booking_global.billing_address_id','LEFT');
			
			$this->db->join('user_type', 'user_type.user_type_id = booking_global.user_type_id','LEFT');
			
			
       		$this->db->where('booking_global.parent_pnr_no',$pnr_no);
       return  $this->db->get('booking_global');
         //echo $this->db->last_query();exit;
    }
 
    public function getBookingbyPnrhotel($pnr_no,$module){		
		
		 if($module == 'FLIGHT'){
			$this->db->join('booking_flight','booking_global.referal_id = booking_flight.booking_flight_id');
		}
		 if($module == 'HOTEL'){
			$this->db->join('booking_hotel','booking_global.referal_id = booking_hotel.booking_hotel_id');
		}
		 if($module == 'PACKAGES'){
			$this->db->join('booking_package','booking_global.referal_id = booking_package.booking_package_id');
		}
		 if($module == 'TOUR'){
			$this->db->join('booking_tour','booking_global.referal_id = booking_tour.booking_tour_id');
		}
 	    if($module == 'TRANSFERS'){
            $this->db->join('booking_transfer','booking_global.referal_id = booking_transfer.booking_transfer_id');
        }
  
			$this->db->join('booking_payment', 'booking_payment.payment_id = booking_global.payment_id','LEFT');
			$this->db->join('booking_transaction', 'booking_transaction.booking_transaction_id = booking_global.booking_transaction_id','LEFT');
			$this->db->join('booking_supplier', 'booking_supplier.booking_supplier_id = booking_global.booking_supplier_id','LEFT');
			
			$this->db->join('booking_billing_address', 'booking_billing_address.billing_address_id = booking_global.billing_address_id','LEFT');
			
			$this->db->join('user_type', 'user_type.user_type_id = booking_global.user_type_id','LEFT');
			
			
       		$this->db->where('booking_global.pnr_no',$pnr_no);
        return $this->db->get('booking_global');
    }
   
    
	public function getPassengerbyPnr($pnr_no){
		$this->db->where('booking_global_id',$pnr_no);
        return $this->db->get('booking_passenger');
    }
	public function getPassengerbyid($booking_global_id){
				$this->db->where('booking_global_id',$booking_global_id);
        return $this->db->get('booking_passenger');

    }
    public function getRequestbyid($referal_id){
				$this->db->where('booking_flight_id',$referal_id);
        return $this->db->get('booking_flight');

    }

     public function getRequestHotelbyid($referal_id){
				$this->db->where('booking_hotel_id',$referal_id);
        return $this->db->get('booking_hotel');

    }
    public function getagentbyid($billing_address_id){
				$this->db->where('billing_address_id',$billing_address_id);
        return $this->db->get('booking_billing_address');
    }
	
	public function update_booking_xml($booking_xml_log,$booking_xml_data_id)
	{
		//print_r($booking_xml_log); exit();
		$this->db->where("booking_xml_data_id",$booking_xml_data_id);
		$this->db->update("booking_xml_data",$booking_xml_log);
		

	}

	 public function Update_Booking_Global_rs($booking_temp_id, $update_booking, $module){
        $this->db->where('booking_global_id',$booking_temp_id);
		$this->db->where('module',$module);
        $this->db->update('booking_global', $update_booking);
        //echo $this->db->last_query();
        return;
    }

    //Sanjay

    function get_order_details($branch_id, $search_data)
    {  
		
        $this->db->select('booking_global.*,booking_hotel.*,booking_transaction.*,user_details.user_name,user_details.user_email,user_details.user_profile_pic,booking_payment.*,booking_billing_address.*,api_details.api_name,product_details.product_name');
        $this->db->from('booking_global');
        $this->db->join('booking_transaction','booking_transaction.booking_transaction_id=booking_global.booking_transaction_id','LEFT');
        $this->db->join('user_details','user_details.user_details_id=booking_global.user_id','LEFT');
        $this->db->join('booking_billing_address','booking_billing_address.billing_address_id=booking_global.billing_address_id','LEFT');
        $this->db->join('api_details','api_details.api_details_id=booking_global.api_id','LEFT');
        $this->db->join('booking_payment','booking_payment.payment_id=booking_global.payment_id','LEFT');
        $this->db->join('product_details','product_details.product_details_id=booking_global.product_id','LEFT');
        
        $this->db->join('booking_hotel','booking_hotel.booking_hotel_id = booking_global.referal_id');
        //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        
        if(count($search_data) > 0){
			if($search_data['reference_type'] != ''){
				 if($search_data['reference_type'] == 'pnr_no'){
					$this->db->where("booking_global.pnr_no =", $search_data['reference_no'] ); 
				 }
				 if($search_data['reference_type'] == 'booking_no'){
					 $this->db->where("booking_global.booking_no =", $search_data['reference_no'] ); 
				 }
			}
			
			if($search_data['by_booking'] != ''){
				if($search_data['by_booking'] == 'booking_date'){
				$this->db->where("booking_global.voucher_date >=", date('Y-m-d', strtotime($search_data['start_date']))  );
				$this->db->where("booking_global.voucher_date <=", date('Y-m-d', strtotime($search_data['end_date']))  );
			   }
			   if($search_data['by_booking'] == 'checkin_date'){
				$this->db->where("booking_global.travel_date >=", date('Y-m-d', strtotime($search_data['start_date']))  );
				$this->db->where("booking_global.travel_date <=", date('Y-m-d', strtotime($search_data['end_date']))  );
			   }
			   
			   if($search_data['by_booking'] == 'checkout_date'){
				$this->db->where("booking_hotel.checkout >=", date('Y-m-d', strtotime($search_data['start_date']))  );
				$this->db->where("booking_hotel.checkout <=", date('Y-m-d', strtotime($search_data['end_date']))  );
			   }
		}
		
		if($search_data['by_user'] != ''){
			 if($search_data['by_user'] == 'ByStaff'){
				  if($search_data['staff_id'] == 0){
					  $this->db->where("booking_global.user_type_id", 2);
				  }else{
					 $this->db->where('booking_global.user_id', $search_data['staff_id']);
					   $this->db->where("booking_global.user_type_id", 2);
				  }
				
			 }
			 if($search_data['by_user'] == 'BySubAgents'){
				  if($search_data['sub_agent_id'] == 0){
					  $this->db->where("booking_global.user_type_id", 4);
				  }else{
					 $this->db->where('booking_global.user_id', $search_data['sub_agent_id']);
					   $this->db->where("booking_global.user_type_id", 4);
				  }
			 }
		}

	  if($search_data['by_type'] != ''){
			 if($search_data['by_type'] == 'Booking'){ 
				$this->db->where("booking_global.booking_status", $search_data['status']);
			 }
			 if($search_data['by_type'] == 'payment'){
				$this->db->where("booking_payment.payment_status", $search_data['pay_status']);
			 }
		  }
	   }
		
    
        if($branch_id!='')       
        {
            $this->db->where('booking_global.branch_id',$branch_id);
        }
        
        		
	  if($this->session->userdata('user_type') == 4){
		$this->db->where("booking_global.user_type_id", 4);
		$this->db->where("booking_global.user_id", $this->session->userdata('user_details_id'));
	  }
	  
	  if($this->session->userdata('user_type') == 5){
		$this->db->where("booking_global.user_type_id", 5);
		$this->db->where("booking_global.user_id", $this->session->userdata('user_details_id'));
	  }
		
		
        $this->db->where('booking_global.product_id', 1);
        $this->db->group_by("booking_global.pnr_no");
        $this->db->order_by("booking_global.booking_global_id", 'desc');
        
        $result = $this->db->get();
       
         return $result->result();
        
	}
	
	function get_flight_book_count($branch_id){
		/* $this->db->where('booking_global.product_id', 1);
		$query = $this->db->query('SELECT * FROM flight_booking_details');
        return $query->num_rows();*/
        
        $sql = "select * from flight_booking_details where created_by_id=".$this->session->userdata('user_details_id');
        $result = $this->db->query($sql);
        return $result->num_rows();
	}
	
	
	
	 function get_transfer_order_details($branch_id, $search_data)
    {  
		
        $this->db->select('booking_global.*,booking_transfer.*,booking_transaction.*,user_details.user_name,user_details.user_email,user_details.user_profile_pic,booking_payment.*,booking_billing_address.*,api_details.api_name,product_details.product_name');
        $this->db->join('booking_transaction','booking_transaction.booking_transaction_id=booking_global.booking_transaction_id','LEFT');
        $this->db->join('user_details','user_details.user_details_id=booking_global.user_id','LEFT');
        $this->db->join('booking_billing_address','booking_billing_address.billing_address_id=booking_global.billing_address_id','LEFT');
        $this->db->join('api_details','api_details.api_details_id=booking_global.api_id','LEFT');
        $this->db->join('booking_payment','booking_payment.payment_id=booking_global.payment_id','LEFT');
        $this->db->join('product_details','product_details.product_details_id=booking_global.product_id','LEFT');
        $this->db->join('booking_transfer','booking_transfer.booking_transfer_id = booking_global.referal_id');
        //$this->db->join('booking_xml_data','booking_xml_data.booking_xml_data_id=booking_supplier.booking_xml_data_id','LEFT');
        
        if(count($search_data) > 0){
			if($search_data['t_reference_type'] != ''){
				 if($search_data['t_reference_type'] == 'pnr_no'){
					$this->db->where("booking_global.pnr_no =", $search_data['t_reference_no'] ); 
				 }
				 if($search_data['t_reference_type'] == 'booking_no'){
					 $this->db->where("booking_global.booking_no =", $search_data['t_reference_no'] ); 
				 }
			}
			
			if($search_data['t_by_booking'] != ''){
				if($search_data['t_by_booking'] == 'booking_date'){
				$this->db->where("booking_global.voucher_date >=", date('Y-m-d', strtotime($search_data['t_start_date']))  );
				$this->db->where("booking_global.voucher_date <=", date('Y-m-d', strtotime($search_data['t_end_date']))  );
			   }
			   if($search_data['t_by_booking'] == 'travel_date'){
				$this->db->where("booking_global.travel_date >=", date('Y-m-d', strtotime($search_data['t_start_date']))  );
				$this->db->where("booking_global.travel_date <=", date('Y-m-d', strtotime($search_data['t_end_date']))  );
			   }
			
		}
		
		if($search_data['t_by_user'] != ''){
			 if($search_data['t_by_user'] == 'ByStaff'){
				  if($search_data['t_staff_id'] == 0){
					  $this->db->where("booking_global.user_type_id", 2);
				  }else{
					 $this->db->where('booking_global.user_id', $search_data['t_staff_id']);
					   $this->db->where("booking_global.user_type_id", 2);
				  }
				
			 }
			 if($search_data['t_by_user'] == 'ByStaff'){
				  if($search_data['t_sub_agent_id'] == 0){
					  $this->db->where("booking_global.user_type_id", 4);
				  }else{
					 $this->db->where('user_id', $search_data['t_sub_agent_id']);
					   $this->db->where("booking_global.user_type_id", 4);
				  }
			 }
			
		}

	  if($search_data['t_by_type'] != ''){
			 if($search_data['t_by_type'] == 'Booking'){ 
				$this->db->where("booking_global.booking_status", $search_data['t_status']);
			 }
			 if($search_data['t_by_type'] == 'payment'){
				$this->db->where("booking_payment.payment_status", $search_data['t_pay_status']);
			 }
		  }
	   }
		
		
		if($search_data['pickup'] != ''){
			$this->db->where("booking_transfer.pickup_code", $search_data['pickup']);
		}
		
		if($search_data['dropoff'] != ''){
			$this->db->where("booking_transfer.dropoff_code", $search_data['dropoff']);
		}
      
			if($branch_id!='')       
			{
				$this->db->where('booking_global.branch_id',$branch_id);
			}
			
        		
		  if($this->session->userdata('user_type') == 4){
			$this->db->where("booking_global.user_type_id", 4);
			$this->db->where("booking_global.user_id", $this->session->userdata('user_details_id'));
		  }
		  
		    if($this->session->userdata('user_type') == 5){
			$this->db->where("booking_global.user_type_id", 5);
			$this->db->where("booking_global.user_id", $this->session->userdata('user_details_id'));
		  }
			
		
        $this->db->where('booking_global.product_id', 19);
        $this->db->group_by("booking_global.pnr_no");
        $this->db->order_by('booking_global.booking_global_id', 'desc');
        $result = $this->db->get('booking_global');
      
        return $result->result();
        
	}
	
  function get_passanger_details($referal_id)
    {
        $this->db->select('*');
        $this->db->where('booking_hotel.booking_hotel_id',$referal_id);

        $result = $this->db->get('booking_hotel');
    $status='';
        if($result->num_rows() > 0 )
        {
            $status = $result->result();
        }
        
        return $status ;

    }

 function get_transaction_details_all($booking_transaction_id)
    {            
        $this->db->where('booking_transaction_id',$booking_transaction_id);
        $result = $this->db->get('booking_transaction');        
        if($result->num_rows() > 0 )
        {           
        return  $result->row();
        }
        
        return '' ;

    }
    public function get_hotel_booking_details($hotel_id)
    { 
        $this->db->where('booking_hotel.booking_hotel_id',$hotel_id);
        $result = $this->db->get('booking_hotel');
        if($result->num_rows() > 0 )
        {
            $status = $result->row();
        }
        
        return $status ;

    }

 function get_hotel_order_details($pnr,$user_type_id)
    { 
 
        $this->db->select('booking_global.*,booking_transaction.*,user_details.user_name,user_details.user_email,user_details.user_profile_pic,booking_payment.*,booking_billing_address.*,api_details.api_name,product_details.product_name');
        $this->db->join('booking_transaction','booking_transaction.booking_transaction_id=booking_global.booking_transaction_id','LEFT');
        $this->db->join('user_details','user_details.user_details_id=booking_global.user_id','LEFT');
        $this->db->join('booking_billing_address','booking_billing_address.billing_address_id=booking_global.billing_address_id','LEFT');
        $this->db->join('api_details','api_details.api_details_id=booking_global.api_id','LEFT');
        $this->db->join('booking_payment','booking_payment.payment_id=booking_global.payment_id','LEFT');
        $this->db->join('product_details','product_details.product_details_id=booking_global.product_id','LEFT');
        
        $this->db->join('booking_hotel','booking_hotel.booking_hotel_id','LEFT');
        $this->db->where('booking_global.user_id',$user_type_id);
    	$this->db->where('booking_global.pnr_no',$pnr);
      
        
        
        $result = $this->db->get('booking_global');
        //echo $this->db->last_query(); exit();
        if($result->num_rows() > 0 )
        {
        return $result->row();
        }
        
        return '' ;
	}
	
  public function insert_booking_transfer($data){
	  $this->db->insert("booking_transfer", $data);
	  return $this->db->insert_id();
  }
  
  function update_transfer_booking_xml($booking_id, $transfer_bookings_details){
	  $this->db->where("booking_transfer_id", $booking_id);
	  $this->db->update("booking_transfer", $transfer_bookings_details);
  }
  
  public function updateTransactions($trans_id,$transaction){
		$this->db->where('booking_transaction_id',$trans_id);
		$this->db->update('booking_transaction',$transaction);
	}
	
  function transfer_cancel_history($cancel_data){
	  $this->db->insert("transfer_cancellation_history", $cancel_data);
	  return;
  }
  
  function get_transaction_details($transaction_id){
	  $this->db->where('booking_transaction_id', $transaction_id);
	 return  $this->db->get('booking_transaction');
  }
	
}

?>
