<?php
class Privilege_Model extends CI_Model {
   function __construct(){
         parent::__construct();
    }
    
    function get_user_privileges($user_id,$controller_name,$function_name, $function_parameter = ''){
		$this->db->select("*");
		$this->db->from('homepage_leftmenu');
		$this->db->join('user_privileges', 'user_privileges.dashbaord_id = homepage_leftmenu.homepage_details_id');
		
		if($function_parameter == 1){
			//~ if($function_name != 'index'){
				//~ $controller_name = $controller_name."/".$function_name;
			//~ }
		   $this->db->where('dashboard_url', $controller_name);
	   }
		$this->db->where('user_details_id', $user_id);
		$this->db->where('user_type_id', $this->session->userdata('user_type'));
		$this->db->order_by('position', 'asc');
		$query = $this->db->get();
	   
		if($query->num_rows == 0){
			$this->db->select("*");
			$this->db->from('homepage_leftmenu');
			if($this->session->userdata('user_type') != 5){
			$this->db->join('user_privileges', 'user_privileges.dashbaord_id = homepage_leftmenu.homepage_details_id');
			
			if($function_parameter == 1){
				 $this->db->where('dashboard_url', $controller_name);
			}
			$this->db->where('user_role_id', $this->session->userdata('role_id'));
			$this->db->where('user_type_id', $this->session->userdata('user_type'));
			$this->db->where('user_branch_id', $this->session->userdata('branch_id'));
		    } elseif($this->session->userdata('user_type') == 5){
				$sql = "find_in_set(5, user_types) >0";
				$this->db->where($sql);
			}
			$this->db->order_by('position', 'asc');
			$query1 = $this->db->get();
			
		
			return $query1;
		}else {
	       return $query;
	   }
		
	}
	
	
   
}
?>
