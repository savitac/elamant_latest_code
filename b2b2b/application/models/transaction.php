<?php
/**
 * MODEL HAVING TRANASCTION AND LOGS FUNCTIONS FOR AGENT AND SUB-AGENT
 *
 * @package    Provab Application
 * @author     PAWAN BAGGA
 */
Class Transaction extends CI_Model 
{
	/**
	 * Lock All the tables necessary for flight transaction to be processed
	 */
	public static function lock_tables()
	{
		echo 'Under Construction';
	}

	public function __construct(){
		parent::__construct();
		$CI =&get_instance();
		$CI->load->model('custom_db');
	}

	/**
	 * Create Payment record for payment gateway used in the application
	 */
	public function create_payment_record($app_reference, $booking_fare, $firstname, $email, $phone, $productinfo, $convenience_fees=0, $promocode_discount=0, $currency_conversion_rate)
	{
		$duplicate_pg = $this->read_payment_record($app_reference);
		if ($duplicate_pg == false) {
			$payment_gateway_currency = $this->config->item('payment_gateway_currency');
			$request_params = array('txnid' => $app_reference,
				'booking_fare' => $booking_fare,
				'convenience_amount' => $convenience_fees,
				'promocode_discount' => $promocode_discount,
				'firstname' => $firstname,
				'email'=> $email,
				'phone'=> $phone,
				'productinfo'=> $productinfo
			);

			//Add total amount and remove discount from total amount
			$data['amount'] = roundoff_number($booking_fare+$convenience_fees-$promocode_discount);
			$data['domain_origin'] = get_domain_auth_id();
			$data['app_reference'] = $app_reference;
			$data['request_params'] = json_encode($request_params);
			$data['currency'] = $payment_gateway_currency;
			$data['currency_conversion_rate'] = $currency_conversion_rate;
			// debug($data);die;
			$this->custom_db->insert_record('payment_gateway_details', $data);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Read Payment record with payment gateway reference
	 * @param $app_reference
	 */
	function read_payment_record($app_reference)
	{
		$cond['app_reference'] = $app_reference;
		$data = $this->custom_db->single_table_records('payment_gateway_details', '*', $cond);
		if ($data['status'] == SUCCESS_STATUS) {
			return $data['data'][0];
		} else {
			return false;
		}
	}
	
	/**
	 * Read Working key and access key for payment 
	 * @param $domain_name
	 */	
		function payment_gateway_auth($domain_name)
		{
		$this->load->model ('custom_db'); // we need to load user model to access provab sms library
		
		$cond['domain_name'] = $domain_name;
		$data = $this->custom_db->single_table_records('domain_details', '*', $cond);
		#print_r($data);
		if ($data['status'] == SUCCESS_STATUS) {
			return $data['data'][0];
		} else {
			return false;
		}

		}
	/**
	 * Update Payment record with payment gateway reference
	 * @param $app_reference
	 */
	function update_payment_record_status($app_reference, $status, $response_params=array())
	{
		$cond['app_reference'] = $app_reference;
		$data['status'] = $status;
		if (valid_array($response_params) == true) {
			$data['response_params'] = json_encode($response_params);
		}
		$this->custom_db->update_record('payment_gateway_details', $data, $cond);
	}

	public function get_payment_status($app_reference)
	{
		$this->db->select('status');
		$this->db->where('app_reference =', trim($app_reference));
 		$this->db->from('payment_gateway_details');
 		return $this->db->get()->row_array();
	}

	/**
	 * Update additional details of transaction
	 */
	function update_convinence_discount_details($book_detail_table, $app_reference, $discount=0, $convinence=0, $convinence_value=0, $convinence_type=0, $convinence_per_pax=0)
	{
		$data = array();
		if (empty($discount) == false) {
			$data['discount'] = $discount;
		} else {
			$data['discount'] = 0;
		}

		if (empty($convinence) == false) {
			$data['convinence_amount'] = $convinence;
		}

		if (empty($convinence_value) == false) {
			$data['convinence_value'] = $convinence_value;
		}

		if (empty($convinence_type) == false) {
			$data['convinence_value_type'] = $convinence_type;
		}

		if (empty($convinence_per_pax) == false) {
			$data['convinence_per_pax'] = $convinence_per_pax;
		}

		$cond['app_reference'] = $app_reference;
		$this->custom_db->update_record($book_detail_table, $data, $cond);

	}

	/**
	 * Unlock All The Tables
	 */
	public static function release_locked_tables()
	{
		$CI = & get_instance();
		$CI->db->query('UNLOCK TABLES');
	}
	public function get_agent_balance($agent_id)
	{
	$this->db->select('*');
	$this->db->from('user_accounts');
	$this->db->where('user_id',$agent_id);
	$query=$this->db->get()->row_array(); 
	return $query;

	}
	public function update_agent_balance($update_agent_balance,$agent_id)
	{
		$this->db->set('balance_credit',$update_agent_balance);
		$this->db->where('user_id',$agent_id);
		$this->db->update('user_accounts'); 
	}
	public function update_transaction_logs($transaction_logs)
	{
		$this->db->insert('transaction_logs',$transaction_logs);      	 	
	}  

	public function accounts_logs($user_id)
	{
		if($user_id !='')
		{
			$this->db->SELECT('*');
			$this->db->FROM('transaction_logs');
			$this->db->WHERE('user_id',$user_id);
			$this->db->order_by("created_date", "DESC"); 
			$query=$this->db->get()->result();
			return $query; 
		}     
	} 

	public function update_agent_transaction_logs($transaction_logs,$amount)
	{
	$update_balance = $transaction_logs->balance_credit-$amount;      
	$data = array(
	'user_id' => $transaction_logs->user_id,
	'last_balance'=> $transaction_logs->balance_credit,
	'current_balance'=> $update_balance,
	'amount_debited'=> $amount,
	'log_type'=> "Sub-Agent-Deposit",
	); 
 		$this->db->insert('transaction_logs',$data);         	 	
	}

	public function update_subagent_debit_transaction_logs($transaction_logs,$amount)
	{
	$update_balance = $transaction_logs[0]->balance_credit-$amount;      
	$data = array(
	'user_id' => $transaction_logs[0]->user_id,
	'last_balance'=> $transaction_logs[0]->balance_credit,
	'current_balance'=> $update_balance,
	'amount_debited'=> $amount,
	'log_type'=> "Sub-Agent-Debit", 
	); 
 		$this->db->insert('transaction_logs',$data);         	 	
	}
 
	public function update_agent_add_debit_transaction_logs($transaction_logs,$amount)
	{
	$update_balance = $transaction_logs[0]->balance_credit+$amount;      
	$data = array(
	'user_id' => $transaction_logs[0]->user_id,
	'last_balance'=> $transaction_logs[0]->balance_credit,
	'current_balance'=> $update_balance,
	'amount_credited'=> $amount,
	'log_type'=> "Sub-Agent-Debit-Added", 
	); 
 		$this->db->insert('transaction_logs',$data);         	 	
	} 
  
	public function update_sub_agent_transaction_logs($transaction_logs,$amount,$update_balance_amount)
	{  
		$data = array(
		'user_id' => $transaction_logs->user_id,
		'last_balance'=> $transaction_logs->balance_credit,
		'current_balance'=> $update_balance_amount,
		'amount_credited'=> $amount,
		'log_type'=> "Deposit",
		);  
		$this->db->insert('transaction_logs',$data);         	 	
	} 
} 
