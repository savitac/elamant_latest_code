<?php
class Dashboard_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    } 
    
    function getTopDeals($user_id = '') {
      $this->db->select('*');
      $this->db->from('last_minute_deals');
      if($user_id !='')
			$this->db->where('users_id', $user_id);
      $this->db->order_by('position','asc');
      $this->db->where('status', 'ACTIVE');
      $query = $this->db->get();	
      
      if($query->num_rows() > 0){
			return $query->result();
		}else{
           $this->db->select('*');
		   $this->db->from('last_minute_deals');
		   $this->db->where('users_id','0');
		   $this->db->order_by('position','asc');
           $this->db->where('status', 'ACTIVE');
           return $query = $this->db->get()->result();
		}
	}
	
	function getProducts(){
	  $this->db->select('*');
      $this->db->from('product_details');
      $this->db->where('product_status', 1);
      $this->db->join("user_product_privileges","product_id = product_details_id");
      if($this->session->userdata('user_type') != 5){
		$this->db->where("user_details_id", $this->session->userdata('user_details_id'));  
	  }
	  if($this->session->userdata('user_type') == 5){
		$this->db->where("user_details_id", $this->session->userdata('branch_id'));    
	  }
      
      $this->db->order_by('product_order');
      $query = $this->db->get();
      //debug($query->result());die;
      return $query->result();
	}
	
    function getInnerBanner($user_id = '') {
    	$this->db->select('*');
		$this->db->from('banner_details');
		$this->db->where('banner_type', 'INNER_SLIDER');
		if($user_id !='') {
			$this->db->where('users_id', $user_id);
		}
		$this->db->where('status', 'ACTIVE');
		$this->db->order_by('position');
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();
		}else{
           $this->db->select('*');
			$this->db->from('banner_details');
			$this->db->where('banner_type', 'INNER_SLIDER');
		    $this->db->where('users_id','0');
		    $this->db->where('status', 'ACTIVE');
		     $this->db->order_by('position');
           return $query = $this->db->get()->result();
		}
	}
	
	function getSideBanner($user_id = '') {
		$this->db->select('*');
		$this->db->from('banner_details');
		$this->db->where('banner_type', 'INNER_BANNER');
		if($user_id !='')
			$this->db->where('users_id', $user_id);
		$this->db->where('status', 'ACTIVE');
		$this->db->order_by('position');
		$query =  $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
            $this->db->select('*');
			$this->db->from('banner_details');
			$this->db->where('banner_type', 'INNER_BANNER');
		    $this->db->where('users_id','0');
		    $this->db->where('status', 'ACTIVE');
		    $this->db->order_by('position');
           return $query = $this->db->get()->result();
		}
	}

 	function get_travel_destination($user_id = ''){
        $this->db->select('*');
        $this->db->from('travel_destinations');
        $this->db->order_by('position','asc');
        if($user_id !='')
			$this->db->where('users_id', $user_id);
        $this->db->where('status', 'ACTIVE');
        $this->db->order_by('position');
        $query=$this->db->get();
        if($query->num_rows() > 0){
			return $query->result();
		}else{
            $this->db->select('*');
			$this->db->from('travel_destinations');
		    $this->db->where('users_id','0');
		    $this->db->where('status', 'ACTIVE');
		    $this->db->order_by('position');
           return $query = $this->db->get()->result();
		}
    }

	/*function get_sociallinks($agentId = '') {
      $this->db->select('*');
      $this->db->from('social_link_details');
      $this->db->order_by('position','asc');
      $this->db->where('status', 'ACTIVE');
      $query = $this->db->get();	
      return $query->result();
	}*/
	function get_sociallinks($agentId = ''){
		$this->db->select('*');
        $this->db->from('social_link_details');
        $this->db->order_by('position','asc');
        if($agentId !='')
			$this->db->where('users_id', $agentId);
        $this->db->where('status', 'ACTIVE');
        $this->db->order_by('position');
        $query=$this->db->get();
        if($query->num_rows() > 0){
			return $query->result();
		}else{
            $this->db->select('*');
			$this->db->from('social_link_details');
		    $this->db->where('users_id','0');
		    $this->db->where('status', 'ACTIVE');
		    $this->db->order_by('position');
           return $query = $this->db->get()->result();
		}
	}

	function get_dashboardmenu(){
		if($this->session->userdata('user_type') == 2){
			$sql = "find_in_set(2, user_types) >0";
		}elseif($this->session->userdata('user_type') == 4){
			$sql = "find_in_set(4, user_types) >0";
		}elseif($this->session->userdata('user_type') == 5){
			$sql = "find_in_set(5, user_types) >0";
		}

			$this->db->select('*');
			$this->db->from('homepage_leftmenu');
			$this->db->join("user_privileges", "user_privileges.dashbaord_id = homepage_leftmenu.homepage_details_id");
			$this->db->where('dashboard_status', 'ACTIVE');
			$this->db->where('user_privileges.user_details_id', $this->session->userdata('user_details_id'));
			$this->db->where($sql); 
			$this->db->order_by('position');
			$query = $this->db->get()->result();
			
			/*echo $this->db->last_query();
			exit("last_query");
*/
			/*echo "<pre>";
			print_r($query);
			exit("..");*/
        if(count($query) == 0){
        	//$this->db->distinct();
			$this->db->select('*');
			$this->db->from('homepage_leftmenu');
			if($this->session->userdata('user_type') != 5){
			$this->db->join("user_privileges", "user_privileges.dashbaord_id = homepage_leftmenu.homepage_details_id");
			$this->db->where('dashboard_status', 'ACTIVE');
			$this->db->where('user_privileges.user_role_id', $this->session->userdata('role_id'));
			$this->db->where('user_privileges.user_branch_id', $this->session->userdata('branch_id'));
		   }
		    $this->db->where($sql); 
		    $this->db->group_by('dashboard_name');
			$this->db->order_by('position');
			$query1 = $this->db->get()->result();
			return $query1;
		}else{

		return $query;
		}
          
    }


function get_sub_agent_dashboardmenu(){
       
        $this->db->select('*');
		$this->db->from('homepage_leftmenu');
	    $this->db->where('dashboard_status', 'ACTIVE');
	    $where = "find_in_set(4, `user_types`) > 0";
	    $this->db->where($where);
		$this->db->order_by('position');
          return $query = $this->db->get()->result();
        
    }
    
   function get_hotelcities($user_id = ''){ 
   		$this->db->select('*');
        $this->db->from('best_deals');
        $this->db->order_by('position','asc');
        if($user_id !='')
			$this->db->where('users_id', $user_id);
        $this->db->where('status', 'ACTIVE');
        $this->db->order_by('position');
        $query=$this->db->get();
        if($query->num_rows() > 0){
			return $query->result();
		}else{
            $this->db->select('*');
			$this->db->from('best_deals');
		    $this->db->where('users_id','0');
		    $this->db->where('status', 'ACTIVE');
		    $this->db->order_by('position');
           return $query = $this->db->get()->result();
		}
   }

   function get_hoteldeals($id){
       
        $this->db->select('*');
		$this->db->from('hotel_deals');
	   	$this->db->where('users_id', $id);
	    $this->db->where('status', 'ACTIVE');
	    $this->db->order_by('position');
       return $query = $this->db->get()->result();
		
    }
   public function get_footer(){
		$this->db->select('*');
		$this->db->from('footer_details');
		$this->db->where('status', 'ACTIVE');
		$this->db->order_by('position');
		return $this->db->get()->result();
	}

	public function get_managefooter($footer_id){

		$this->db->select('*');
		$this->db->from('managefooter_details');
		$this->db->where('footerid', $footer_id);
		$this->db->where('status', 'ACTIVE');
		$this->db->order_by('position');
		return $this->db->get()->result();
	}

  public function get_subfooter(){
		$this->db->select('*');
		$this->db->from('subfooter_details');
		$this->db->where('status', 'ACTIVE');
		$this->db->order_by('position');
		return $this->db->get()->result();
	}

	/*function getHeaderProducts(){
	  $this->db->select('*');
      $this->db->from('header_product');
      $this->db->where('user_type', 0);
      $this->db->join("user_product_privileges","product_id = product_details_id");
      $this->db->where("user_details_id", $this->session->userdata('user_details_id'));
      $this->db->order_by('product_order');
      $query = $this->db->get();	
      return $query->result();
	}*/


	function getHeaderProducts(){
	  $this->db->select('*');
      $this->db->from('header_product');
      $this->db->where('user_type', 0);
      $this->db->where('agent_id', 0);
      $this->db->join("header_details", "header_details.header_details_id = header_product.header_id");
      $this->db->where('header_product.status','ACTIVE');
      $this->db->order_by('header_product.position');
      $query = $this->db->get();	
      return $query->result();
	}

	function getHeaderServices(){
	  $this->db->select('*');
      $this->db->from('header_service');
      $this->db->where('user_type', 0);
      $this->db->where('agent_id', 0);
      $this->db->join("header_details", "header_details.header_details_id = header_service.header_id");
      $this->db->where('header_service.status','ACTIVE');
      $this->db->order_by('header_service.position');
      $query = $this->db->get();	
      return $query->result();
	}

	function getHeaderAboutus(){
	  $this->db->select('*');
      $this->db->from('header_aboutus');
      $this->db->where('user_type', 0);
      $this->db->where('agent_id', 0);
      $this->db->join("header_details", "header_details.header_details_id = header_aboutus.header_id");
      $this->db->join("header_manageaboutus", "header_manageaboutus.header_id = header_aboutus.headerabout_id");
      $this->db->where('header_aboutus.status','ACTIVE');
      $this->db->order_by('header_aboutus.position');
      $query = $this->db->get();	
      return $query->result();
	}

	function getHeaderManageAboutus($id){
	  $this->db->select('*');
      $this->db->from('header_manageaboutus');
      //$this->db->join("header_aboutus", "header_aboutus.headerabout_id = header_manageaboutus.header_id");
      $this->db->where('header_id',$id);
      $this->db->where('status','ACTIVE');
      $this->db->order_by('position');
      $query = $this->db->get(); //echo $this->db->last_query(); exit();	
      return $query->result();
	}

	function getHeaderContact(){
	  $this->db->select('*');
      $this->db->from('header_contact');
      $this->db->where('user_type', 0);
      $this->db->where('agent_id', 0);
      $this->db->join("header_details", "header_details.header_details_id = header_contact.header_id");
      $this->db->where('header_contact.status','ACTIVE');
      $this->db->order_by('header_contact.position');
      $query = $this->db->get();	
      return $query->result();
	}

	function getFooterCarrer(){
	  $this->db->select('*');
      $this->db->from('footer_carrer');
      $this->db->where('user_type', 0);
      $this->db->where('agent_id', 0);
      $this->db->join("subfooter_details", "subfooter_details.footer_details_id = footer_carrer.footer_id");
      $this->db->where('footer_carrer.status','ACTIVE');
      $this->db->order_by('footer_carrer.position');
      $query = $this->db->get();	
      return $query->result();
	}

	function getFootertermsnconditions(){
	  $this->db->select('*');
      $this->db->from('footer_termsconditions');
      $this->db->where('user_type', 0);
      $this->db->where('agent_id', 0);
      $this->db->join("subfooter_details", "subfooter_details.footer_details_id = footer_termsconditions.footer_id");
      $this->db->where('footer_termsconditions.status','ACTIVE');
      $this->db->order_by('footer_termsconditions.position');
      $query = $this->db->get();	
      return $query->result();
	}

	
	function addContactDetails($input){
		
		$insert_data = array(
							'name' => $input['name'],
							'position' 	=> $input['position'],
							'company' 		=> $input['company'],
							'wechat' => $input['wechat'],
							'qq' 	=> $input['qq'],
							'email' 	=> $input['email'],
							'subject' 	=> $input['subject'],
							'message'	=> $input['message'],
							'creation_date'			=> (date('Y-m-d H:i:s'))
						);		
			//echo '<pre>'; print_r($insert_data); exit();
		$this->db->insert('user_contact',$insert_data);
	}

	function get_all_flight_booking($user_id,$user_type,$domain_id)
	{
		$this->db->select('*');
		$this->db->from('flight_booking_details');
		$this->db->where('domain_id', $domain_id);
		$query = $this->db->get();	
		return $query->result_array();  
	}
	function get_all_flight_booking_confirm($user_id,$user_type,$domain_id)
	{
		$this->db->select('*');
		$this->db->from('flight_booking_details');
		$this->db->where('domain_id', $domain_id);
		$this->db->where('status','BOOKING_CONFIRMED');
		$query = $this->db->get();	
		return $query->num_rows();  
	}
	function get_all_hotel_booking($user_id,$user_type,$domain_id)
	{
		$this->db->select('*');
		$this->db->from('hotel_booking_details');
		$this->db->where('domain_id', $domain_id);
		$query = $this->db->get();	 
		return $query->result_array();   
	}
	function get_all_hotel_booking_confirm($user_id,$user_type,$domain_id)
	{
		$this->db->select('*');
		$this->db->from('hotel_booking_details');
		$this->db->where('domain_id', $domain_id);
		$this->db->where('status', "BOOKING_CONFIRMED");
		$query = $this->db->get();	 
		return $query->num_rows();  
	}
	function get_all_flight_booking_sub($user_id,$user_type,$domain_id)
	{
		$this->db->select('*');
		$this->db->from('flight_booking_details');
		$this->db->where('domain_id', $domain_id);
		$this->db->where('user_details_id', $user_id);
		$this->db->where('user_type', $user_type);
		$query = $this->db->get();	
		return $query->result_array();   
	}
	function get_all_flight_booking_sub_confirm($user_id,$user_type,$domain_id)
	{
		$this->db->select('*');
		$this->db->from('flight_booking_details');
		$this->db->where('domain_id', $domain_id);
		$this->db->where('user_details_id', $user_id);
		$this->db->where('user_type', $user_type);
		$this->db->where('status', "BOOKING_CONFIRMED"); 
		$query = $this->db->get();	
		return $query->num_rows();   
	}
	function get_all_hotel_booking_sub($user_id,$user_type,$domain_id)
	{
		$this->db->select('*');
		$this->db->from('hotel_booking_details');
		$this->db->where('domain_id', $domain_id);
		$this->db->where('user_details_id', $user_id);
		$this->db->where('user_type', $user_type);
		$query = $this->db->get();	
		return $query->result_array();     
	}
	function get_all_hotel_booking_sub_confirm($user_id,$user_type,$domain_id)
	{
		$this->db->select('*');
		$this->db->from('hotel_booking_details');
		$this->db->where('domain_id', $domain_id);
		$this->db->where('user_details_id', $user_id);
		$this->db->where('user_type', $user_type);
		$this->db->where('status', "BOOKING_CONFIRMED");
		$query = $this->db->get();	
		return $query->num_rows();    
	}
}
?>
