<?php
class Usermanagement_Model extends CI_Model {

    function __construct()
    {
       
        parent::__construct();
    }   
    function getAgentList($user_id = ''){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		if($user_id !='')
			$this->db->where('user_details_id', $user_id);
		$this->db->where('branch_id', 0 );
		$this->db->where('user_type_id', '2');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			$data['user_info'] = '';
		}else{
			$data['user_info'] = $query->result();
		}
		if($data['user_info']!=''){
			for($u=0;$u<count($data['user_info']);$u++){
				$data['domain_details'][$u] 	= $this->Users_Model->get_domain_details($data['user_info'][$u]->user_details_id);
				$data['user_type_details'][$u] 	= $this->Users_Model->get_user_type_details($data['user_info'][$u]->user_details_id);
				$data['product_details'][$u] 	= $this->Users_Model->get_product_details($data['user_info'][$u]->user_details_id);
				$data['api_details'][$u] 		= $this->Users_Model->get_api_details($data['user_info'][$u]->user_details_id);
				$data['country_details'][$u] 	= $this->Users_Model->get_country_details($data['user_info'][$u]->user_details_id);
			}
		}
		return $data;	
	} 
	
	function get_domain_details($user_details_id) {
		$this->db->select('um.domain_details_id, d.domain_name');
		$this->db->from('user_management_details um');
		$this->db->join('domain_details d', 'um.domain_details_id = d.domain_details_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_user_type_details($user_details_id) {
		$this->db->select('um.user_type_id, ut.user_type_name');
		$this->db->from('user_management_details um');
		$this->db->join('user_type ut', 'um.user_type_id = ut.user_type_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_product_details($user_details_id) {
		$this->db->select('um.product_details_id, p.product_name');
		$this->db->from('user_management_details um');
		$this->db->join('product_details p', 'um.product_details_id = p.product_details_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_api_details($user_details_id) {
		$this->db->select('um.api_details_id, a.api_name, a.api_alternative_name');
		$this->db->from('user_management_details um');
		$this->db->join('api_details a', 'um.api_details_id = a.api_details_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function get_country_details($user_details_id) {
		$this->db->select('um.product_details_id, c.country_name, c.iso3_code');
		$this->db->from('user_management_details um');
		$this->db->join('country_details c', 'um.country_id = c.country_id');
		$this->db->distinct();
		$this->db->where('user_details_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function get_all_user_email($user_email) {
		$this->db->select('user_email');
		$this->db->from('user_details');
		if($user_email !='')
			$this->db->where('user_email', $user_email);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return 'NO';
		}else{
			return 'YES';
		}
	}
	
	function getAgentadminList($user_id){
		
		$this->db->select('*');
		$this->db->from('user_details');
		//$this->db->where('user_details_id', $user_id);
		$this->db->where('user_type_id', 4);
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function getAgentusersList(){
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('branch_id', $this->session->userdata('branch_id'));
		$this->db->where('user_type_id', 4);
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		$this->db->join('domain_details', 'domain_details.domain_details_id = user_details.domain_list_fk'); 
		$this->db->join('user_accounts','user_accounts.user_id = user_details.user_details_id','Left');   
		$query=$this->db->get();
		return $query->result();
	}
	
	function getusersList($user_id){ 
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('domain_list_fk',$this->session->userdata('domain_id'));
		$this->db->where('user_type_id', $user_id);
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_branch_users($branch_id, $user_id = ''){
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('user_type_id', 2);
		$this->db->where("branch_id", $this->session->userdata('branch_id'));
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		$this->db->join('agent_roles', 'agent_roles.agent_roles_id = user_details.user_role_id');
		if($user_id != ''){
			$this->db->where("user_details_id", $user_id);
		}
		$this->db->order_by('user_details_id', 'asc');
		$query=$this->db->get();
		
			return $query->result();
		
	}


	public function userAddress($addressing){ 
		$this->db->insert('address_details',$addressing);
		$id = $this->db->insert_id(); //print_r($id); exit();
			if (!empty($id)) {
				return $id;
			} else {
				return false;
			}
		
	}
	function addUseragentDetails($input, $acc_no,$address,$domain_id,$branch_id){
		
     try{ 
					$insert_data_admin = 
					array(
					'address_details_id' 		=> $address,
					'user_name' 				=> $input['user_name'],
					'domain_list_fk'            => $domain_id,
					/*'agent_name' 				=> $input['user_name'],*/
					'user_type_id' 				=> $input['user_type'],
					'user_email' 				=> $input['email'],
					'user_cell_phone' 			=> $input['mob_number'],
					'branch_id'					=> $this->session->userdata('branch_id'),
					'user_account_number' 		=> $acc_no,					
					'country_id' 				=> $input['country'],
					'company_name' 				=> isset($input['companyname'])?$input['companyname']:'',		
					'user_creation_date_time' 	=> (date('Y-m-d H:i:s')),
					'user_status' => "ACTIVE"				
					);	

	     if(isset($input['role_id']) && $input['role_id'] != ''){
		     $insert_data_admin['user_role_id']			= $input['role_id']; 
	     }   
		$this->db->insert('user_details',$insert_data_admin);
		$user_details_id = $this->db->insert_id();
	} catch(Exception $ex) {
		return $e;
	}
	try{ 
		$insert_data_admin_login = array(
									'user_details_id' 			=> $user_details_id,
									'user_name' 	=> $input['email'],
									'user_password' 	=> "AES_ENCRYPT(".$input['password'].",'".SECURITY_KEY."')",
									'user_type_id'      => '4',
								);			
		$this->db->insert('user_login_details',$insert_data_admin_login);
		
	} catch(Exception $ex1) {
		return $e;
	}
	}
	
	function updateUseragentDetails($input, $agentId){
		
     try{ 
		$insert_data_admin = array(
								'address_details_id' 		=> 1,
								'user_name' 				=> $input['user_name'],
								'user_email' 				=> $input['email'],
								'user_cell_phone' 			=> $input['mob_number'],
								'company_name'				=> $input['companyname'],
								'country_id' 	            => $input['country'],			
								'user_updation_date_time' 	=> (date('Y-m-d H:i:s')),
								'user_status' => "ACTIVE"				
							);	
		$this->db->where('user_details_id', $agentId);
		$this->db->update('user_details',$insert_data_admin);
	 } catch(Exception $ex) {
		return $e;
	}
  }
	
	public function updateAgentAddressDetails($addressId,$postData){
		$this->db->where('address_details_id', $addressId);	
		$this->db->update('address_details',$postData);
		return $addressId;
	} 
	
	public function insertAgentAddressDetails($postData){
		$this->db->insert('address_details',$postData);
		return $address_id = $this->db->insert_id();
	}
	
	public function updateAgentDetails($ssid,$postData){
		$this->db->where('user_details_id', $ssid);	
		$this->db->update('user_details',$postData);
		return;
	}
	
	function activeAgents($user_id){
		$data = array(
					'user_status' => 'ACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
		$this->General_Model->insert_log('4','activeAgents',json_encode($data),'updating  user status to active','user_details','user_details_id',$user_id);
	}
	
	function inactiveAgent($user_id){
		$data = array(
					'user_status' => 'INACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
		$this->General_Model->insert_log('4','inactiveAgent',json_encode($data),'updating  user status to inactive','user_details','user_details_id',$user_id);
	}
	
	function delete_users($user_id){
		$this->db->where('user_details_id', $user_id);
		$this->db->delete('user_details'); 
		$this->General_Model->insert_log('4','delete_users',json_encode(array()),'deleting  user details from database','user_details','user_details_id',$user_id);				
	}
	
	function updateAgents($update,$user_id,$user_profile_name){
		if(!isset($update['user_status']))
			$update['user_status'] = "INACTIVE";
			
		$update_data_address = array(
								'address' 		=> $update['address'],
								'city_name' 	=> $update['city'],
								'zip_code' 		=> $update['zip_code'],
								'state_name' 	=> $update['state_name'],
								'country_id' 	=> $update['country']					
		 					);
		$this->db->where('address_details_id', $update['address_details_id']);
		$this->db->update('address_details', $update_data_address);
		$this->General_Model->insert_log('4','updateAgents',json_encode($update_data_address),'updating  user address  Details to database','address_details','address_details_id',$update['address_details_id']);
		
		$update_data_user = array(
								'user_type_id' 				=> $update['user_type'][0],
								'company_name' 				=> $update['comapany_name'],
								'user_name' 				=> $update['first_name']."-".$update['middle_name']."-".$update['last_name'],
								'user_home_phone' 			=> $update['phone_no'],
								'user_cell_phone' 			=> $update['mobile_no'],					
								'user_profile_pic' 			=> $user_profile_name,					
								'user_status' 				=> $update['user_status'],
								'country_id' 					=> $update['country']				
							);		
		$this->db->where('user_details_id', $user_id);						
		$this->db->update('user_details',$update_data_user);
		$this->General_Model->insert_log('4','updateAgents',json_encode($update_data_user),'updating  user  Details to database','user_details','user_details_id',$user_id);
		
	}
	
	function getDepositList($user_details_id) {
		$this->db->select('*');
		$this->db->from('agent_deposit');
		$this->db->join('user_details', 'user_details.user_details_id = agent_deposit.user_details_id');
		if($user_details_id !='')
		$this->db->where('agent_deposit.user_details_id', $user_details_id);
		$this->db->order_by('current_stamp','DESC');  
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function getAgentDepsoiteList($user_details_id) {
		$this->db->select('*');
		$this->db->from('agent_deposit');
		if($user_details_id !='')
			$this->db->where('agent_deposit_id', $user_details_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	function getRequestedDepositList($user_details_id) {
		$this->db->select('*');
		$this->db->from('agent_deposit');
		$this->db->join('user_details', 'user_details.user_details_id = agent_deposit.user_details_id');
		$this->db->join('bank_account_details','bank_account_details.origin = agent_deposit.bank_acc_id');
		if($user_details_id !='')
		$this->db->where('agent_deposit.branch_id', $user_details_id);
		$this->db->where('agent_deposit.user_type_id', 4);
		$this->db->order_by('current_stamp','DESC');        
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return ''; 
		}else{
			//debug($query->result());die(" MODAL"); 
			return $query->result();
		}
	}
	
	function approveDeposit($user_id){
		$data = array(
					'status' => 'Accepted'
					);
		$this->db->where('agent_deposit_id', $user_id);
		$this->db->update('agent_deposit', $data); 
	}

	function insertAgentBalance($user_id,$amount){
		$data = array(
					'balance_credit' => $amount,
					'last_credit' => $amount,
					'user_id' => $user_id,
					);
		$this->db->insert('user_accounts', $data); 
	}

	function updateAgentBalance($user_id,$amount,$update_balance_amount){
		$data = array(
			'balance_credit' => $update_balance_amount,
			'last_credit' => $amount,
		);
		
		$this->db->where('user_id', $user_id);
		$this->db->update('user_accounts', $data); 
	}

	function update_agent_balance_debit($user_id,$amount,$update_balance_amount,$agent_balance)
	{
		$data = array(
					'balance_credit' => $update_balance_amount,
					'last_debit' => $amount,
				);
		$this->db->where('user_id', $user_id);
		$this->db->update('user_accounts', $data); 
	}

	function getAgentDepositBalance($user_id){
		$this->db->select('*');
		$this->db->where('agent_id', $user_id);
		$query = $this->db->get('b2b_acc_info');
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->row();
		}
	}

	/*function updateAgentBalance($user_id,$amount,$update_balance_amount){
		$data = array(
					'balance_credit' => $update_balance_amount,
					'last_credit' => $amount,
					);
		$this->db->where('user_id', $user_id);
		$this->db->update('user_accounts', $data); 
	}*/
	
	function cancelDepoist($user_id){
		$data = array(
					'status' => 'Cancelled'
					);
		$this->db->where('agent_deposit_id', $user_id);
		$this->db->update('agent_deposit', $data); 		
	}
	
	function addDepositDetails($input,$exchange,$user_id){	
		$input['product_status'] = "Pending";
		$insert_data = array(
							'user_details_id' 	=> $user_id,
							'user_type_id'      => '2',
							'deposit_type' 		=> $input['deposit_type'],
							'current_exchangerate' => $exchange,
							'amount_credit' 	=> $input['amount_credit'],
							'deposited_date' 	=> $input['deposit_date'],
							'remarks' 			=> $input['remarks'],
							'currencycode'      => $input['currencycode'],
							'bank_acc_id' 		=> $input['bank_acc_id'],
							'branch_id'      => $this->session->userdata('branch_id'),
							'user_type_id'    => $this->session->userdata('user_type'),
							'status' 			=> $input['product_status']				
						);
		$this->db->insert('agent_deposit',$insert_data);
	}

	function add_b2b2b_deposite_details($input,$user_id,$branch_id){	
		$input['product_status'] = "Pending";
		$insert_data = array(
							'user_details_id' 	=> $user_id,
							'user_type_id'      => '2',
							'branch_id'			=> $branch_id,
							'deposit_type' 		=> $input['deposit_type'],
							'amount_credit' 	=> $input['amount_credit'],
							'deposited_date' 	=> (date('Y-m-d H:i:s')),
							'remarks' 			=> $input['remarks'],
							'currencycode'      => $input['currencycode'],
							'status' 			=> $input['product_status']				
						); //print_r($insert_data); exit();
		$this->db->insert('agent_deposit',$insert_data);
	}

	function getAllAgentEmail($user_email) 
	{
		$this->db->select('user_email');
		$this->db->from('user_details');
		if($user_email !='')
		$this->db->where('user_email', $user_email);
		$this->db->where('user_type_id', '2');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return 'NO';
		}else{
			return 'YES';
		}
	}

	public function export_b2b_users($ids) 
	{
        $this->db->where_in('user_details_id', $ids);
        return $this->db->get('user_details');
	}

	public function getCountryListAgent()
	{
		$this->db->select('*')->from('country_details');
		$query = $this->db->get();
	    if ( $query->num_rows > 0 ) {
         return $query->result();
        }
      return false;
   	}

 	function currenyAmount($currency_code)
 	{
		$this->db->select('*');
		$this->db->from('currency_details');
		$this->db->where('currency_code', $currency_code);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function inactive_users($user_id)
	{
		$data = array(
					'user_status' => 'INACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
	}
	
	public function active_users($user_id)
	{
		$data = array(
					'user_status' => 'ACTIVE'
					);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data); 
	}
	
	function addAgentDepositDetails($input,$exchange,$user_id)
	{
		$input['product_status'] = "Pending";
		$insert_data = array(
							'branch_id' 	=> $user_id,
							'user_details_id' => $input['subagents'],
							'user_type_id'      => '4',
							'deposit_type' 		=> $input['deposit_type'],
							'current_exchangerate' => $exchange,
							'amount_credit' 	=> $input['amount_credit'],
							'deposited_date' 	=> $input['deposit_date'],
							'remarks' 			=> $input['remarks'],
							'currencycode'      => $input['currencycode'],
							'status' 			=> $input['product_status']				
						);
		$this->db->insert('agent_deposit',$insert_data);
	}

	function getSubAgentList($user_id = ''){
		 
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		if($user_id !='')
		$this->db->where('user_details_id', $user_id);
		
		$this->db->where('user_type_id', '4');
		$query=$this->db->get();

		if($query->num_rows() ==''){
			$data['user_info'] = '';
		}else{
			$data['user_info'] = $query->result();
		}
		return $data;	
	}
 
    public function get_agent_deposit_balance($user_id)
    { 
   	    //echo '<pre>'; print_r($user_id); exit();
		$this->db->select('*');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('user_accounts');
		//echo "<pre/>";print_r($this->db->last_query());die();
		if($query->num_rows() ==''){
			return '';
		}else{ 
			return $query->result();
		}
	}
     
    function approveSubAgentDeposit($user_id,$adminRemarks){
		$data = array(
					'status' => 'Accepted',
					'admin_remarks' => $adminRemarks
					); 
		$this->db->where('agent_deposit_id', $user_id);
		$this->db->update('agent_deposit', $data); 
	}

	function cancelSubAgentDepoist($user_id,$adminRemarks){
		$data = array(
					'status' => 'Cancelled',
					'admin_remarks' => $adminRemarks
					);
		$this->db->where('agent_deposit_id', $user_id);
		$this->db->update('agent_deposit', $data); 		
	}

	/*function insertAgentBalance($user_id,$amount){
		$data = array(
					'balance_credit' => $amount,
					'last_credit' => $amount,
					'user_id' => $user_id,
					);
		$this->db->insert('user_accounts', $data); 
	}*/

	function getAgentDepositList($user_details_id,$depositid) {
		$this->db->select('*');
		$this->db->from('agent_deposit');
		$this->db->join('user_details', 'user_details.user_details_id = agent_deposit.user_details_id');
		if($user_details_id !='')
		$this->db->where('agent_deposit.user_details_id', $user_details_id);
		$this->db->where('agent_deposit.agent_deposit_id',$depositid);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function update_product_privileges($data){
		$user_id = json_decode(base64_decode($data['user_id']));
		$this->db->where("user_details_id", $user_id);
		$this->db->delete("user_product_privileges");
		if(count($data['user_product']) > 0){
			for($i=0; $i < count($data['user_product']); $i++){
				  $datas = array("user_details_id" => $user_id, 
				                 "product_id" => $data['user_product'][$i]);
				   $this->db->insert("user_product_privileges", $datas);
				
			 }
		}
		return;
	}

	function getDepositAgentList($user_id){
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('user_details_id', $user_id);
		$this->db->where('user_type_id', 2);
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		$query=$this->db->get();
		//echo $this->db->last_query(); exit();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}

	function getDepositSubAgentList($user_id){	
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where('user_details_id', $user_id);
		$this->db->where('user_type_id', 4);
		$this->db->join('address_details', 'address_details.address_details_id = user_details.address_details_id');
		$query=$this->db->get();
		//echo $this->db->last_query(); exit();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
	
	function update_privileges($data){
		$user_id = json_decode(base64_decode($data['user_id']));
		$this->db->where("user_details_id", $user_id);
		$this->db->where("user_branch_id", $this->session->userdata('branch_id'));
		$this->db->delete("user_privileges");
	    if(count($data['user_privilege']) > 0){
			  for($i=0; $i < count($data['user_privilege']); $i++){
				  $datas = array(
				                 "dashbaord_id" => $data['user_privilege'][$i],
				                 "user_branch_id" => $this->session->userdata('branch_id'),
				                 "user_details_id" => $user_id,
				                 "user_type_id" => 4);
				 $this->db->insert("user_privileges", $datas);
			 }
		}
		return;
	}


	function update_subagent_balance($user_id,$update_amount_data)
	{
		$this->db->WHERE('user_id',$user_id);
		$this->db->UPDATE('user_accounts',$update_amount_data);  
	}        

	function update_agent_debit_balance($agent_data,$agent_id)
	{ 
		$this->db->WHERE('user_id',$agent_id);
		$this->db->UPDATE('user_accounts',$agent_data);  
		return;
	}

	function get_agent_details($user_id) 
	{
		$this->db->SELECT('*');
		$this->db->FROM('user_details');
		$this->db->WHERE('user_details_id',$user_id); 
		$query=$this->db->get();
		return $query->row_array();   
	} 

 
}
?>
