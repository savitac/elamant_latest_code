<?php
require_once 'abstract_management_model.php';
/**
 * @package    current domain Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class Domain_Management_Model extends Abstract_Management_Model
{
	private $airline_markup;
	private $hotel_markup;
	private $bus_markup;
	var $verify_domain_balance;

	function __construct() {
		parent::__construct('level_2');
		$this->verify_domain_balance = $this->config->item('verify_domain_balance');
		$this->load->library('Application_Logger');
	}

	/**
	 * Arjun J Gowda
	 * Get markup based on different modules
	 * @return array('value' => 0, 'type' => '')
	 */
	function get_markup($module_name)
	{

		$markup_data = '';
		switch ($module_name) {
			case 'flight' : $markup_data = $this->airline_markup();
			break;
			case 'hotel' : $markup_data = $this->hotel_markup();
			break;
			case 'bus' : $markup_data = $this->bus_markup();
			break;
			case 'sightseeing' : $markup_data = $this->sight_markup();
			break;
			case 'transferv1' : $markup_data = $this->transfer_markup();
			break;
		}

		return $markup_data;
	}

	function agent_domain_markup($module_name){
		$markup_data = '';
		switch ($module_name) {
			case 'flight' : $markup_data = $this->agent_airline_markup();
			break;
			case 'hotel' : $markup_data = $this->agent_hotel_markup();
			break;
			case 'bus' : $markup_data = $this->agent_bus_markup();
			break;
			case 'sightseeing' : $markup_data = $this->agent_sight_markup();
			break;
			case 'transferv1' : $markup_data = $this->agent_transfer_markup();
			break;

		} 
		return $markup_data;
	}

	function subagent_domain_markup($module_name){
		$markup_data = '';
		switch ($module_name) {
			case 'flight' : $markup_data = $this->subagent_airline_markup();
			break;
			/*case 'hotel' : $markup_data = $this->subagent_hotel_markup();
			break;*/
			case 'bus' : $markup_data = $this->subagent_bus_markup();
			break;
			case 'transferv1' : $markup_data = $this->subagent_transfer_markup();
			break;
		} 
		return $markup_data;
	}

	function get_branchmarkup($module_name){
		$markup_data = '';
		switch($module_name){
			case 'flight': $markup_data = $this->airline_branchmarkup();
			break;
			/*case 'hotel' : $markup_data = $this->hotel_branchmarkup();
			break;*/
			case 'bus' : $markup_data = $this->bus_branchmarkup();
			break;
		}
		
		return $markup_data;
	}

	/**
	 * 
	 * Arjun J Gowda
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function airline_markup()
	{
		if (empty($this->airline_markup) == true) {
			$response['specific_markup_list'] = array();
			$specific_ailine_markup_list = $this->specific_airline_markup('b2c_flight');
			$response['specific_markup_list'] = $specific_ailine_markup_list;
			$response['generic_markup_list'] = $this->generic_domain_markup('b2c_flight');
			$this->airline_markup = $response;
		} else {
			$response = $this->airline_markup;
		}
		return $response;
	}

	function sight_markup()
	{
		if (empty($this->sight_markup) == true) {
			$response['specific_markup_list'] = array();
			$specific_ailine_markup_list = $this->specific_domain_markup('Activity');
			$response['specific_markup_list'] = $specific_ailine_markup_list;
			$response['generic_markup_list'] = $this->generic_domain_markup('Activity');
			$this->sight_markup = $response;
		} else {
			$response = $this->sight_markup;
		}
		
		return $response;
	}

	function agent_airline_markup()
	{
		if (empty($this->agent_airline_markup) == true) {
			$response['agent_specific_markup_list'] = array();
			$agent_specific_ailine_markup_list = $this->agent_specific_airline_markup('b2c_flight');
			$response['agent_specific_markup_list'] = $agent_specific_ailine_markup_list;
			$response['agent_generic_markup_list'] = $this->agent_generic_domain_markup('b2c_flight');
			$this->agent_airline_markup = $response;
		} else {
			$response = $this->agent_airline_markup;
		}
		return $response;
	}
	function subagent_airline_markup()
	{
		if (empty($this->subagent_airline_markup) == true) {
			$response['subagent_specific_markup_list'] = array();
			$subagent_specific_ailine_markup_list = $this->subagent_specific_airline_markup('b2c_flight');
			$response['subagent_specific_markup_list'] = $subagent_specific_ailine_markup_list;
			$response['subagent_generic_markup_list'] = $this->subagent_generic_domain_markup('b2c_flight');
			$this->subagent_airline_markup = $response;
		} else {
			$response = $this->subagent_airline_markup;
		}
		return $response;
	}
	function airline_branchmarkup(){
		if (empty($this->airline_branchmarkup) == true) {
			$response['specific_branchmarkup_list'] = array();
			$specific_branchailine_markup_list = $this->specific_branchairline_markup('b2c_flight');
			$response['specific_branchmarkup_list'] = $specific_branchailine_markup_list;
			$response['generic_branchmarkup_list'] = $this->agent_generic_domain_markup('b2c_flight');
			$this->airline_branchmarkup = $response;
		} else {
			$response = $this->airline_branchmarkup;
		}
		return $response;	
	}

	/*function hotel_branchmarkup(){
		if (empty($this->hotel_branchmarkup) == true) {
			$response['specific_branchmarkup_list'] = array();
			$specific_branchhotel_markup_list = $this->specific_branchhotel_markup('b2c_hotel');
			$response['specific_branchmarkup_list'] = $specific_branchhotel_markup_list;
			$response['generic_branchmarkup_list'] = $this->generic_branchdomain_markup('b2c_hotel');
			$this->hotel_branchmarkup = $response;
		} else {
			$response = $this->hotel_branchmarkup;
		}
		return $response;	
	}*/
	function bus_branchmarkup()
	{
		if (empty($this->bus_branchmarkup) == true) {
			$response['specific_branchmarkup_list'] = array();
/*			$specific_branchbus_markup_list = $this->specific_branchbus_markup('b2c_bus');
			$response['specific_branchmarkup_list'] = $specific_branchbus_markup_list;*/
			$response['generic_branchmarkup_list'] = $this->generic_branchdomain_markup('b2c_bus');
			$this->bus_branchmarkup = $response;
		} else {
			$response = $this->bus_branchmarkup;
		}
		return $response;
	}


	function specific_branchairline_markup(){
		return '';
	}
function agent_specific_airline_markup($module_type){

		$user_type=$this->session->userdata('user_type');
		 if($user_type==5)
		 {
		 	$user_type=2; 	
		 }
		if($this->markup_level == 'level_2'){
			$markup_level = 2;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 1;
		}
		if($module_type == 'b2c_bus'){
			$module_type = 5;
		}
	
		 $query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" AND markup_status = "ACTIVE" AND markup_type = "SPECIFIC" AND user_type_id = "'.$user_type.'" AND created_branch_id = "'.$this->session->userdata('branch_id').'"';   
		$value_type = $this->db->query($query_check_value_type)->result_array();
 
		return $value_type;
	
	
} 
function subagent_specific_airline_markup(){
	return '';
}


	/**
	 * Arjun J Gowda
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function hotel_markup()
	{
		if (empty($this->hotel_markup) == true) {
			$response['specific_markup_list'] = $this->specific_domain_markup('Hotels');
			if (valid_array($response['specific_markup_list']) == false) {
				$response['generic_markup_list'] = $this->generic_domain_markup('Hotels');
			}
			$this->hotel_markup = $response;
		} else {
			$response = $this->hotel_markup;
		}
		return $response;
	}

	function agent_hotel_markup(){

		if (empty($this->agent_hotel_markup) == true) {
			$response['agent_specific_markup_list'] = $this->agent_specific_domain_markup('b2c_hotel');
			if (valid_array($response['agent_specific_markup_list']) == false) {
				$response['agent_generic_markup_list'] = $this->agent_generic_domain_markup('b2c_hotel');
			}
			$this->agent_hotel_markup = $response;
		} else {
			$response = $this->agent_hotel_markup;
		}
		return $response;
	}

	function agent_sight_markup()
	{
		if (empty($this->agent_sight_markup) == true) {
			$response['agent_specific_markup_list'] = $this->agent_specific_domain_markup('Activity');
			if (valid_array($response['agent_specific_markup_list']) == false) {
				$response['agent_generic_markup_list'] = $this->agent_generic_domain_markup('Activity');
			}
			$this->agent_sight_markup = $response;
		} else {
			$response = $this->agent_hotel_markup;
		}
		return $response;
	}

	function subagent_generic_domain_markup($module_type)
	{
		if($this->markup_level == 'level_2'){
			$markup_level = 4;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 1;
		}
		if($module_type == 'b2c_bus'){
			$module_type = 5;
		}
		$query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM b2b2b_agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and 	ML.markup_type="GENERAL" AND markup_status = "ACTIVE" AND user_details_id = "'.$this->session->userdata('user_details_id').'" AND created_branch_id = "'.$this->session->userdata('branch_id').'"';
		//echo $query_check_value_type; exit;
		$value_type = $this->db->query($query_check_value_type)->result_array();
		/*echo '<pre>';
		print_r($value_type);
		exit;*/
		return $value_type;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function bus_markup()
	{
		if (empty($this->bus_markup) == true) {
			$response['specific_markup_list'] = array();
			$specific_ailine_markup_list = $this->specific_domain_markup('Bus');
			$response['specific_markup_list'] = $specific_ailine_markup_list;
			$response['generic_markup_list'] = $this->generic_domain_markup('Bus');
			$this->bus_markup = $response;
		} else {
			$response = $this->bus_markup;
		}
		return $response;
	} 

	function transfer_markup()
	{
		if (empty($this->transfer_markup) == true) {
			$response['specific_markup_list'] = array();
			$specific_ailine_markup_list = $this->specific_domain_markup('Transfer');
			$response['specific_markup_list'] = $specific_ailine_markup_list;
			$response['generic_markup_list'] = $this->generic_domain_markup('Transfer');
			$this->transfer_markup = $response;
		} else {
			$response = $this->transfer_markup;
		}
		return $response;
	} 

	function agent_bus_markup()
	{
		if (empty($this->agent_bus_markup) == true) {
			$response['agent_specific_markup_list'] = $this->agent_specific_domain_markup('Bus');
			if (valid_array($response['agent_specific_markup_list']) == false) {
				$response['agent_generic_markup_list'] = $this->agent_generic_domain_markup('Bus');
			}
			$this->agent_bus_markup = $response;
		} else {
			$response = $this->agent_bus_markup;
		}

		return $response;
	}

	function agent_transfer_markup()
	{
		if (empty($this->agent_transfer_markup) == true) {
			$response['agent_specific_markup_list'] = $this->agent_specific_domain_markup('Transfer');
			if (valid_array($response['agent_specific_markup_list']) == false) {
				$response['agent_generic_markup_list'] = $this->agent_generic_domain_markup('Transfer');
			}
			$this->agent_transfer_markup = $response;
		} else {
			$response = $this->agent_transfer_markup;
		}

		return $response;
	}

	function subagent_bus_markup()
	{
		if (empty($this->subagent_bus_markup) == true) {
			$response['sub_agent_specific_markup_list'] = $this->sub_agent_specific_domain_markup('Bus');
			if (valid_array($response['agent_specific_markup_list']) == false) {
				$response['sub_agent_generic_markup_list'] = $this->sub_agent_generic_domain_markup('Bus');
			}
			$this->subagent_bus_markup = $response;
		} else {
			$response = $this->subagent_bus_markup;
		}
		
		return $response;
	}

	function subagent_transfer_markup()
	{
		if (empty($this->subagent_transfer_markup) == true) {
			$response['sub_agent_specific_markup_list'] = $this->sub_agent_specific_domain_markup('Transfer');
			if (valid_array($response['agent_specific_markup_list']) == false) {
				$response['sub_agent_generic_markup_list'] = $this->sub_agent_generic_domain_markup('Transfer');
			}
			$this->subagent_transfer_markup = $response;
		} else {
			$response = $this->subagent_transfer_markup;
		}
		
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Get generic markup based on the module type
	 * @param $module_type
	 * @param $markup_level
	 */
	function generic_domain_markup($module_type)
	{
		
		if($this->markup_level == 'level_2'){
			$markup_level = 0;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'Hotels'){
			$module_type = 1;
		}
		if($module_type == 'Bus'){
			$module_type = 5;
		}
		if($module_type == 'Transfer'){
			$module_type = 8;
		}
		if($module_type == 'Activity'){
			$module_type = 7;
		}

		$query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and 	ML.markup_type="GENERAL" AND markup_status = "ACTIVE"';
		$value_type = $this->db->query($query_check_value_type)->result_array();
		
		if(!empty($value_type))
		{
		 	switch ($value_type[0]['value_type']) {
			 	case 'Percentage':
			 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
			 		break;
			 	
			 	case 'Fixed':
			 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_fixed !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
			 		break;
			 	default:
			 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
			 		break;	
		 	}
			
			$generic_data_list = $this->db->query($query)->result_array();
		}
		else
		{
			$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
			$generic_data_list = $this->db->query($query)->result_array();	
		}

		return $generic_data_list;
	}

	function specific_domain_markup($module_type)
	{
		if($this->markup_level == 'level_2'){
			$markup_level = 0;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'Hotels'){
			$module_type = 1;
		}
		if($module_type == 'Bus'){
			$module_type = 5;
		}
		if($module_type == 'Transfer'){
			$module_type = 8;
		}
		if($module_type == 'Activity') {
			$module_type = 7;
		}

		if($this->session->userdata('user_logged_in') == 0) {
			$userType = 5;
		} else {
			$userType = $this->session->userdata('user_type');
		} 

		$query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$userType.'" and 	ML.markup_type="SPECIFIC" AND markup_status = "ACTIVE"';

		$value_type = $this->db->query($query_check_value_type)->result_array();
		
		if(!empty($value_type))
		{
		 switch ($value_type[0]['value_type']) {
		 	case 'Percentage':
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$userType.'" and ML.markup_type="SPECIFIC" AND markup_status = "ACTIVE"'; 
		 		break;
		 	
		 	case 'Fixed':
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_fixed !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$userType.'" and ML.markup_type="SPECIFIC" AND markup_status = "ACTIVE"'; 
		 		break;
		 		default:
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$userType.'" and ML.markup_type="SPECIFIC" AND markup_status = "ACTIVE"'; 
		 		break;
		 		
		 }
			
			$specific_data_list = $this->db->query($query)->result_array();
		}
		else
		{
			$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$userType.'" and ML.markup_type="SPECIFIC" AND markup_status = "ACTIVE"'; 
			$specific_data_list = $this->db->query($query)->result_array();	
		}
		
		return $specific_data_list;
	}

	// function specific_domain_markup($module_type)
	// {
	// 	if($this->markup_level == 'level_2'){
	// 		$markup_level = 'level_1';
	// 	}

	// 	if($this->session->userdata('user_logged_in') == 0) {
	// 		$userType = 5;
	// 	} else {
	// 		$userType = $this->session->userdata('user_type');
	// 	} 

	// 	$query1 = 'SELECT PD.product_details_id, PD.product_name
	// 	FROM product_details AS PD where PD.product_name = "'.$module_type.'"';
	// 	$product_details = $this->db->query($query1)->row();

	// 	$query = 'SELECT ML.markup_details_id AS id, ML.markup_type AS markup_type, ML.markup_value, ML.markup_fixed, ML.markup_value_type,  ML.markup_currency AS markup_currency
	// 	FROM markup_details AS ML where ML.markup_level = "'.$markup_level.'" and ML.markup_type="SPECIFIC" and ML.markup_status="ACTIVE" and ML.product_details_id = ' . $product_details->product_details_id . ' and ML.user_type_id = ' . $userType;
		
	// 	$specific_data_list = $this->db->query($query)->result_array();
	// 	return $specific_data_list;
	// }

	function agent_generic_domain_markup($module_type)
	{
		if($this->markup_level == 'level_2'){
			$markup_level = 0;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 1;
		}
		if($module_type == 'Bus'){
			$module_type = 5;
		}
		if($module_type == 'Transfer'){
			$module_type = 8;
		}
		if($module_type == 'Activity'){
			$module_type = 7;
		}

		// if($this->session->userdata('user_logged_in') == 0) {
		// 	$userType = 5;
		// } else {
		// 	$userType = $this->session->userdata('user_type');
		// } 
		// debug($userType);die;
		$query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" AND markup_status = "ACTIVE" AND markup_type = "GENERAL" AND created_branch_id = "'.$this->session->userdata('branch_id').'"'; 

		$value_type = $this->db->query($query_check_value_type)->result_array();

		 
		/*if(!empty($value_type))
		{
		 switch ($value_type[0]['value_type']) {
		 	case 'Percentage':
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
		 	
		 	case 'Fixed':
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_fixed !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
		 		default:
		 		$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
		 		
		 }
			
		$generic_data_list = $this->db->query($query)->result_array();
		}
		else
		{
			$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_fixed as value, ML.markup_value_type as value_type FROM markup_details AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"'; 
		 		break;
			$generic_data_list = $this->db->query($query)->result_array();	
		}*/
		return $value_type;
		//}
	}

	function sub_agent_generic_domain_markup($module_type)
	{
		if($this->markup_level == 'level_2'){
			$markup_level = 0;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 1;
		}
		if($module_type == 'Bus'){
			$module_type = 5;
		}
		if($module_type == 'Transfer'){
			$module_type = 8;
		}
		if($module_type == 'Activity'){
			$module_type = 7;
		}

		$query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM b2b2b_agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" AND markup_status = "ACTIVE" AND markup_type = "GENERAL" AND created_branch_id = "'.$this->session->userdata('branch_id').'"'; 

		$value_type = $this->db->query($query_check_value_type)->result_array();
		return $value_type;
	}

	function agent_specific_domain_markup($module_type)
	{
		$user_type=$this->session->userdata('user_type');
		if($this->markup_level == 'level_2'){
			$markup_level = 2;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 1;
		}
		if($module_type == 'Bus'){
			$module_type = 5;
		}
		if($module_type == 'Transfer'){
			$module_type = 8;
		}
		if($module_type == 'Activity'){
			$module_type = 7;
		}

		if($this->session->userdata('user_logged_in') == 0) {
			$userType = 5;
		} else {
			$userType = $this->session->userdata('user_type');
		} 
		
		$query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$userType.'" AND markup_status = "ACTIVE" AND markup_type = "SPECIFIC" AND created_branch_id = "'.$this->session->userdata('branch_id').'"'; 

		$value_type = $this->db->query($query_check_value_type)->result_array();
		return $value_type;
	}

	function sub_agent_specific_domain_markup($module_type)
	{
		$user_type=$this->session->userdata('user_type');
		if($this->markup_level == 'level_2'){
			$markup_level = 2;
		}
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 1;
		}
		if($module_type == 'Bus'){
			$module_type = 5;
		}
		if($module_type == 'Transfer'){
			$module_type = 8;
		}
		if($module_type == 'Activity'){
			$module_type = 7;
		}

		if($this->session->userdata('user_logged_in') == 0) {
			$userType = 5;
		} else {
			$userType = $this->session->userdata('user_type');
		} 
		
		$query_check_value_type = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM b2b2b_agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$userType.'" AND markup_status = "ACTIVE" AND markup_type = "SPECIFIC" AND created_branch_id = "'.$this->session->userdata('branch_id').'"'; 

		$value_type = $this->db->query($query_check_value_type)->result_array();
		return $value_type;
	}

	function generic_branchdomain_markup($module_type){
		if($module_type == 'b2c_flight'){
			$module_type = 3;
		}
		if($module_type == 'b2c_hotel'){
			$module_type = 3;
		}
		if($module_type == 'b2c_bus'){
			$module_type = 3;
		}
		if($this->markup_level == 'level_2'){
			$markup_level = 2;
		}
		//$this->session->userdata('user_details_id');
		//$this->session->userdata('branch_id');
		if($this->session->userdata('user_details_id')){
			$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.created_branch_id = "'.$this->session->userdata('branch_id').'" and user_details_id = "'.$this->session->userdata('user_details_id').'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"';
		}else{
			$query = 'SELECT ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type, ML.markup_value as value, ML.markup_value_type as value_type FROM agent_markup AS ML WHERE ML.markup_value !="" and ML.product_details_id = "'.$module_type.'" and ML.user_type_id = "'.$markup_level.'" and ML.created_branch_id = "'.$this->session->userdata('branch_id').'" and ML.markup_type="GENERAL" AND markup_status = "ACTIVE"';
		}
			$generic_data_list = $this->db->query($query)->result_array();
			return $generic_data_list;
	}

	/**
	 *  Arjun J Gowda MODIFIED BY PAWAN BAGGA
	 * Get specific markup based on module type
	 * @param string $module_type	Name of the module for which the markup has to be returned
	 * @param string $markup_level	Level of markup
	 */
	function specific_airline_markup($module_type)
	{
		$module_type=2; 
		$markup_list = ''; 
		 $query='SELECT AL.origin AS airline_origin, AL.name AS airline_name, AL.code AS airline_code, ML.markup_details_id AS markup_origin, ML.markup_type AS markup_type,AL.origin AS reference_id, ML.markup_value,ML.markup_fixed as value,ML.markup_value_type as value_type, ML.markup_currency AS markup_currency FROM airline_list AS AL JOIN markup_details AS ML where  ML.user_type_id = "'.$module_type.'" and ML.markup_level = "level_1" and AL.origin=ML.airline_details_code and ML.markup_type="SPECIFIC" order by AL.name';       
		$specific_data_list = $this->db->query($query)->result_array();
		if (is_array($specific_data_list) && count($specific_data_list) > 0) {
			foreach ($specific_data_list as $__k => $__v) {
				$markup_list[$__v['airline_code']] = $__v;
			}
		}
		return $markup_list;
	}

	/**
	 * Check if the Booking Amount is allowed on Client Domain
	 */
	function verify_current_balance($amount, $currency)
	{
		$status = FAILURE_STATUS;
		if ($this->verify_domain_balance == true) {
			//OBSELETE - NO USE OF THIS
			if ($amount > 0) {
				$query = 'SELECT DL.balance, CC.country as currency, CC.value as conversion_value from domain_list as DL, currency_converter AS CC where CC.id=DL.currency_converter_fk
			AND DL.status='.ACTIVE.' and DL.origin='.$this->db->escape(get_domain_auth_id()).' and DL.domain_key = '.$this->db->escape(get_domain_key());
				$balance_record = $this->db->query($query)->row_array();
				if ($currency == $balance_record['currency']) {
					$balance = $balance_record['balance'];
					if ($balance >= $amount) {
						$status = SUCCESS_STATUS;
					} else {
						//Notify User about current balance problem
						//FIXME - send email, notification for less balance to domain admin and current domain admin
						$this->application_logger->balance_status('Your Balance Is Very Low To Make Booking Of '.$amount.' '.$currency);
					}
				} else {
					echo 'Under Construction';
					exit;
				}
			}
		} else {
			$status = SUCCESS_STATUS;
		}
		return $status;
	}

	/**
	 *
	 * @param $fare
	 * @param $domain_markup
	 * @param $level_one_markup this is 0 as default as it is not mandatory to keep level one markup
	 */
	function update_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup=0, $convinence=0, $discount=0, $currency='INR', $currency_conversion_rate=1)
	{
		$currency = empty($currency) == true ? get_application_currency_preference(): $currency; 
		$status = FAILURE_STATUS;
		$amount = floatval($fare+$level_one_markup+$convinence-$discount);
		$remarks = $transaction_type.' Transaction was Successfully done';
		 $CI =& get_instance();
         $CI->load->model('user_model');
		$notification_users = $this->user_model->get_admin_user_id();
		$action_query_string = array('app_reference' => $app_reference, 'type' => $transaction_type, 'module' => $this->config->item('current_module'));
		if ($this->verify_domain_balance == true) {
			echo 'We Dont Support This';
			exit;
			if ($amount > 0) {
				//deduct balance and continue
				$this->private_management_model->update_domain_balance(get_domain_auth_id(), (-$amount));
				//Log transaction details

				$this->save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $convinence, $discount, $remarks, $currency, $currency_conversion_rate);
				$this->application_logger->transaction_status($remarks.'('.$amount.')', $action_query_string, $notification_users);
			}
		} else {
			$this->save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $convinence, $discount, $remarks, $currency, $currency_conversion_rate);
			$this->application_logger->transaction_status($remarks.'('.$amount.')', $action_query_string, $notification_users);
			$status = SUCCESS_STATUS;
		}
		return $status;
	}

	/**
	 * Save transaction logging for security purpose
	 * @param string $transaction_type
	 * @param string $app_reference
	 * @param number $fare
	 * @param number $domain_markup
	 * @param number $level_one_markup
	 * @param string $remarks
	 */
	function save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $convinence, $discount, $remarks, $currency='', $currency_conversion_rate=1)
	{
		$currency = empty($currency) == true ? get_application_currency_preference(): $currency;
		$transaction_log['system_transaction_id']	= date('Ymd-His').'-S-'.rand(1, 10000);
		$transaction_log['transaction_type']		= $transaction_type;
		$transaction_log['domain_origin']			= get_domain_auth_id();
		$transaction_log['app_reference']			= $app_reference;
		$transaction_log['fare']					= $fare;
		$transaction_log['level_one_markup']		= $level_one_markup;
		$transaction_log['domain_markup']			= $domain_markup;
		$transaction_log['convinence_fees']			= $convinence;
		$transaction_log['promocode_discount']		= $discount;
		$transaction_log['remarks']					= $remarks;
		$transaction_log['created_by_id']			= intval(@$this->entity_user_id) ;
		$transaction_log['created_datetime']		= date('Y-m-d H:i:s', time());
		
		$transaction_log['currency']				= $currency;
		$transaction_log['currency_conversion_rate']= $currency_conversion_rate;
		$this->custom_db->insert_record('transaction_log', $transaction_log);
	}

	function addHotelCrsMarkup(& $total_fare , $hotel_crs_markup,$currency_obj)
	{

		$markup_value = 0;
		//$total_fare = $fare['adult_price'] + $fare['child_price'];
		// echo $total_fare;
		if (isset($hotel_crs_markup['generic_markup_list'][0])) {
			$markup_list = $hotel_crs_markup['generic_markup_list'][0];
			
			if($markup_list['markup_value_type'] == 'Percentage'){
				$markup_value = (($total_fare / 100) * $markup_list ['markup_value']);
			}else{
				$temp_conversion = $currency_obj->getConversionRate ( false, $markup_list ['markup_currency'], $this->to_currency );
				$markup_value = (($markup_list ['markup_fixed'] * $temp_conversion));
			}
		} elseif(isset($hotel_crs_markup['specific_markup_list'][0])) {
			$markup_list = $hotel_crs_markup['specific_markup_list'][0];
			
			if($markup_list['markup_value_type'] == 'Percentage'){
				$markup_value = (($total_fare / 100) * $markup_list ['markup_value']);
			}else{
				$temp_conversion = $currency_obj->getConversionRate ( false, $markup_list ['markup_currency'], $this->to_currency );
				$markup_value = (($markup_list ['markup_fixed'] * $temp_conversion));
			}
		}
		//echo $markup_value;exit;
		return $markup_value;
	}

	 /*Commission for b2b2b 
	 Karthick
	 Start*/

	 function get_admin_commission_details($agent_fk)
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();

		$domain_auth_id = $this->session->userdata('domain_list_fk');
		
		$agent_details_query = 'select U.*,UT.user_type_id
								FROM user_details AS U 
								INNER JOIN user_type AS UT ON U.user_type_id=UT.origin
								
								WHERE U.domain_list_fk ='.$domain_auth_id.' and U.user_details_id = '.intval($agent_fk);
		$flight_commission_query = 'select BFCD.* from b2b_flight_commission_details as BFCD 
								inner join user_details as U on BFCD.agent_fk = U.user_details_id
								where BFCD.domain_list_fk ='.$domain_auth_id.' and BFCD.agent_fk='.intval($agent_fk).' and BFCD.type="specific"'; 
		$bus_commission_query = 'select BBCD.* from b2b_bus_commission_details as BBCD 
								inner join user_details as U on BBCD.agent_fk = U.user_details_id
								where BBCD.domain_list_fk ='.$domain_auth_id.' and BBCD.agent_fk='.intval($agent_fk).' and BBCD.type="specific"';

		$sightseeing_commission_query = 'select BSCD.* from b2b_sightseeing_commission_details as BSCD 
								inner join user_details as U on BSCD.agent_fk = U.user_details_id
								where BSCD.domain_list_fk ='.$domain_auth_id.' and BSCD.agent_fk='.intval($agent_fk).' and BSCD.type="specific"';
						
		$transfer_commission_query = 'select BTCD.* from b2b_transfer_commission_details as BTCD 
								inner join user_details as U on BTCD.agent_fk = U.user_details_id
								where BTCD.domain_list_fk ='.$domain_auth_id.' and BTCD.agent_fk='.intval($agent_fk).' and BTCD.type="specific"';


		$response['data']['agent_details']				= $this->db->query($agent_details_query)->row_array();
		$response['data']['flight_commission_details']	= $this->db->query($flight_commission_query)->row_array();;
		// $response['data']['hotel_commission_details']	= '';
		$response['data']['bus_commission_details']		= $this->db->query($bus_commission_query)->row_array();

		$response['data']['sightseeing_commission_details'] = $this->db->query($sightseeing_commission_query)->row_array();

		$response['data']['transfer_commission_details'] = $this->db->query($transfer_commission_query)->row_array();

		
		if (valid_array($response['data']['agent_details']) == true) {
			$response['status'] = SUCCESS_STATUS;
		}
		return $response;
	}



	function get_commission_details($agent_fk)
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();

		$domain_auth_id = $this->session->userdata('domain_list_fk');
		
		$agent_details_query = 'select U.*,UT.user_type_id
								FROM user_details AS U 
								INNER JOIN user_type AS UT ON U.user_type_id=UT.origin
								
								WHERE U.domain_list_fk ='.$domain_auth_id.' and U.user_details_id = '.intval($agent_fk);
		$flight_commission_query = 'select BFCD.* from b2b2b_flight_commission_details as BFCD 
								inner join user_details as U on BFCD.agent_fk = U.user_details_id
								where BFCD.domain_list_fk ='.$domain_auth_id.' and BFCD.agent_fk='.intval($agent_fk).' and BFCD.type="specific"'; 
		$bus_commission_query = 'select BBCD.* from b2b2b_bus_commission_details as BBCD 
								inner join user_details as U on BBCD.agent_fk = U.user_details_id
								where BBCD.domain_list_fk ='.$domain_auth_id.' and BBCD.agent_fk='.intval($agent_fk).' and BBCD.type="specific"';

		$sightseeing_commission_query = 'select BSCD.* from b2b2b_sightseeing_commission_details as BSCD 
								inner join user_details as U on BSCD.agent_fk = U.user_details_id
								where BSCD.domain_list_fk ='.$domain_auth_id.' and BSCD.agent_fk='.intval($agent_fk).' and BSCD.type="specific"';
						
		$transfer_commission_query = 'select BTCD.* from b2b2b_transfer_commission_details as BTCD 
								inner join user_details as U on BTCD.agent_fk = U.user_details_id
								where BTCD.domain_list_fk ='.$domain_auth_id.' and BTCD.agent_fk='.intval($agent_fk).' and BTCD.type="specific"';


		$response['data']['agent_details']				= $this->db->query($agent_details_query)->row_array();
		$response['data']['flight_commission_details']	= $this->db->query($flight_commission_query)->row_array();;
		// $response['data']['hotel_commission_details']	= '';
		$response['data']['bus_commission_details']		= $this->db->query($bus_commission_query)->row_array();

		$response['data']['sightseeing_commission_details'] = $this->db->query($sightseeing_commission_query)->row_array();

		$response['data']['transfer_commission_details'] = $this->db->query($transfer_commission_query)->row_array();

		
		if (valid_array($response['data']['agent_details']) == true) {
			$response['status'] = SUCCESS_STATUS;
		}
		return $response;
	}
	/**
	 * Jaganath
	 * b2b2b Agent Commission Details: Flight, Bus
	 */
	function agent_commission_details($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		if (!$count) {
			$agent_details_query = 'select U.*,BFCD.value as flight_commission_value,BFCD.api_value as flight_api_value,BFCD.value_type as flight_commission_type,
									BBCD.value as bus_commission_value,BBCD.api_value as bus_api_value,BBCD.value_type as bus_commission_type,

									BSCD.value as sightseeing_commission_value,BSCD.api_value as sightseeing_api_value,BSCD.value_type as sightseeing_commission_type,
									
									BTCD.value as transfer_commission_value,BTCD.api_value as transfer_api_value,BTCD.value_type as transfer_commission_type

									FROM user AS U 
									INNER JOIN user_type AS UT ON U.user_type_id=UT.origin
									left join b2b2b_flight_commission_details as BFCD on U.user_details_id=BFCD.agent_fk and BFCD.type="specific"
									left join b2b2b_bus_commission_details as BBCD on U.user_details_id=BBCD.agent_fk and BBCD.type="specific"
									left join b2b2b_sightseeing_commission_details as BSCD on U.user_details_id=BSCD.agent_fk and BSCD.type="specific"

									left join b2b2b_transfer_commission_details as BTCD on U.user_details_id=BTCD.agent_fk and BTCD.type="specific"



									WHERE U.user_type_id = '.B2B2B_USER.' and U.domain_list_fk ='.get_domain_auth_id().$condition.'
									ORDER BY U.user_details_id DESC limit '.$offset.', '.$limit;
			$response['data']['agent_commission_details']				= $this->db->query($agent_details_query)->result_array();
			return $response;
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U, user_type AS UT, api_country_list AS ACL
				 WHERE U.user_type_id=UT.origin 
				 AND U.country_code=ACL.origin and U.domain_list_fk ='.get_domain_auth_id().$condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}
	/**
	 * Jaganath
	 * b2b2b Agent Commission Details: Flight, Bus,Sightseeing
	 * @param $search_filter_condition => (condition)
	 */
	function filter_agent_commission_details($search_filter_condition = '', $count=false, $offset=0, $limit=100000000000)
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		if(empty($search_filter_condition) == false) {
			$search_filter_condition = ' and'.$search_filter_condition;
		}
		if (!$count) {
			$agent_details_query = 'select U.*,BFCD.value as flight_commission_value,BFCD.api_value as flight_api_value,BFCD.value_type as flight_commission_type,
									BBCD.value as bus_commission_value,BBCD.api_value as bus_api_value,BBCD.value_type as bus_commission_type,

									BSCD.value as sightseeing_commission_value,BSCD.api_value as sightseeing_api_value,BSCD.value_type as sightseeing_commission_type,

									BTCD.value as transfer_commission_value,BTCD.api_value as transfer_api_value,BTCD.value_type as transfer_commission_type


									FROM user AS U 
									INNER JOIN user_type AS UT ON U.user_type_id=UT.origin
									left join b2b2b_flight_commission_details as BFCD on U.user_details_id=BFCD.agent_fk and BFCD.type="specific"
									left join b2b2b_bus_commission_details as BBCD on U.user_details_id=BBCD.agent_fk and BBCD.type="specific"

									left join b2b2b_sightseeing_commission_details as BSCD on U.user_details_id=BSCD.agent_fk and BSCD.type="specific"

									left join b2b2b_transfer_commission_details as BTCD on U.user_details_id=BTCD.agent_fk and BTCD.type="specific"



									WHERE U.user_type_id = '.B2B2B_USER.' and U.domain_list_fk ='.$domain_auth_id.$search_filter_condition.'
									ORDER BY U.user_details_id DESC limit '.$offset.', '.$limit;
			$response['data']['agent_commission_details']				= $this->db->query($agent_details_query)->result_array();
			return $response;
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U, user_type AS UT, api_country_list AS ACL
				 WHERE U.user_type_id=UT.origin AND U.user_type_id = '.b2b2b_USER.' and U.domain_list_fk ='.get_domain_auth_id().' and  U.country_code=ACL.origin'.$search_filter_condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}
	/**
	 * Jaganath
	 * b2b2b Default Commission Details: Flight, Hotel, Bus,Sightseeing
	 * @param $agent_fk
	 */
	function default_commission_details()
	{
		$response['status'] = SUCCESS_STATUS;
		$response['data'] = array();

		$domain_auth_id = $this->session->userdata('domain_list_fk');
		$flight_commission_query = 'select BFCD.* from b2b2b_flight_commission_details as BFCD
								where BFCD.domain_list_fk ='.$domain_auth_id.' and BFCD.type="generic"';
								
		$bus_commission_query = 'select BBCD.* from b2b2b_bus_commission_details as BBCD
								where BBCD.domain_list_fk ='.$domain_auth_id.' and BBCD.type="generic"';

		$sightseeing_commission_query = 'select BSCD.* from b2b2b_sightseeing_commission_details as BSCD
								where BSCD.domain_list_fk ='.$domain_auth_id.' and BSCD.type="generic"';

		$transfer_commission_query = 'select BTCD.* from b2b2b_transfer_commission_details as BTCD
								where BTCD.domain_list_fk ='.$domain_auth_id.' and BTCD.type="generic"';
													

		$response['data']['flight_commission_details']	= $this->db->query($flight_commission_query)->row_array();;
		// $response['data']['hotel_commission_details']	= '';
		$response['data']['bus_commission_details']		= $this->db->query($bus_commission_query)->row_array();
		$response['data']['sightseeing_commission_details'] = $this->db->query($sightseeing_commission_query)->row_array();
		$response['data']['transfer_commission_details'] = $this->db->query($transfer_commission_query)->row_array();

		return $response;
	}
	/**
	 * Jaganath 
	 */
	function auto_suggest_agency_name($chars, $limit=15)
	{
		$query = 'select U.*
					FROM user AS U 
					INNER JOIN user_type AS UT ON U.user_type_id=UT.origin 
					WHERE U.agency_name!="" and U.domain_list_fk ='.get_domain_auth_id().' and 
					(U.uuid like "%'.$chars.'%" OR U.agency_name like "%'.$chars.'%" OR U.first_name like "%'.$chars.'%" OR U.last_name like "%'.$chars.'%" OR U.email like "%'.$chars.'%" OR U.phone like "%'.$chars.'%" )
					order by U.agency_name asc limit 0, '.$limit;
		return $this->db->query($query)->result_array();
	}
	/**
	 * Jaganath
	 * Bank Account Details
	 */
	function bank_account_details() 
	{
		$query='SELECT BAD.*,CONCAT(U.first_name," ",U.last_name) as created_by_name 
		        FROM bank_account_details BAD
		        JOIN user U on U.user_details_id=BAD.created_by_id
		        where BAD.domain_list_fk='.get_domain_auth_id();
		$tmp_data = $this->db->query($query);
		if($tmp_data->num_rows()>0) {
			$tmp_data=$tmp_data->result_array();
			$data = array('status' => QUERY_SUCCESS, 'data' => $tmp_data);
		} else {
			$data = array('status' => QUERY_FAILURE);
		 }
		 return $data;
	}

}
