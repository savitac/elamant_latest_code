<?php
class Account_Model extends CI_Model {
   
   function __construct(){
         parent::__construct();
    }
    
  
    public function default_address(){
		$this->db->where('address_details_id', '1');
		return $this->db->get('address_details');
	}
	
	public function isRegisteredB2B($email, $user_type =''){
		$this->db->where('user_email', $email);
		if($user_type != '') {
			$this->db->where('user_type_id', $user_type);
    	}
		return $this->db->get('user_details');
	}
	
	
	
	

	public function isRegistered($email){
		$this->db->where('user_email', $email);
		return $this->db->get('user_details');
	}
	
	 public function createUsers($postData){
		return $this->db->insert('user_details',$postData);
	 }
	
	public function createLogin($login_data){ 
		
		$this->db->insert('user_login_details', $login_data);
		$id = $this->db->insert_id();
			if (!empty($id)) {
				return true;
			} else {
				return false;
			}
	}
	
	public function isValidUser($email, $password, $users,$guest=false){
	     $subqry = '';
      
         if($users != ''){
			
			 if($users == 'admin'){
				$subqry .= ' and user_details.user_type_id = 2 and user_details.domain_list_fk ='.$this->session->userdata('domain_id'); 
			 }elseif($users== 'agent'){
				$subqry .= ' and user_details.user_type_id = 4 and  user_details.domain_list_fk ='. $this->session->userdata('domain_id');
			 }elseif($users == 'users' && $guest==1){
				$subqry .= ' and user_details.user_type_id = 6   and  user_details.domain_list_fk ='. $this->session->userdata('domain_id'); 
			 }
			 elseif($users == 'users'){
				$subqry .= ' and user_details.user_type_id = 5   and  user_details.domain_list_fk ='. $this->session->userdata('domain_id'); 
			 }
			 else{
				$subqry .= ' and user_details.user_type_id = 2';  
			 }
		 }
        
       $sql = '
             SELECT * FROM (`user_login_details`) JOIN `user_details` ON `user_details`.`user_details_id` = `user_login_details`.`user_details_id`
              WHERE `user_login_details`.`user_password` = "'.addslashes($password).'" 
              AND `user_login_details`.`user_name` = "'.$email.'" '. $subqry ; 
    	return $this->db->query($sql);
      
	}
	
	public function getLogin($email){
		$this->db->where('user_name', $email);
		return $this->db->get('user_login_details');
	}

	public function getLoginParticular($email,$account){
		
		$this->db->where('user_email', $email);
		$this->db->where('user_account_number', $account);
    	return $this->db->get('user_details');
	}

	public function isvalidSecrect($key,$secret){ 
		$this->db->where('pwd_reset_random_key', $key);
		$this->db->where('pwd_reset_secret_key', $secret);
		return $this->db->get('user_details');
	}
	
	public function updatePwdResetLink($user_id,$key,$secret){
		//echo $key; exit();
		$update = array(
			'pwd_reset_random_key' => $key,
			'pwd_reset_secret_key' => $secret
		);
		$this->db->where('user_details_id', $user_id);
		return $this->db->update('user_details', $update);
	}

		public function updateB2b($password,$email,$account_number,$user_details_id){
		//echo $password;exit;
		$update = array(
			'pwd_reset_random_key' => ''
		);
		$this->db->where('user_email', $email);
		$this->db->where('user_account_number', $account_number);
		$this->db->update('user_details', $update);
		$login_update = array(
			'user_password' => $password
		);
		$this->db->where('user_name', $email);
		$this->db->where('user_details_id', $user_details_id);
		$this->db->update('user_login_details', $login_update);	
	}
	
	public function checkUserPassword($password, $user_id){
		$this->db->where('user_password', $password); 
		$this->db->where('user_details_id', $user_id);
		$this->db->from('user_login_details');
		return $this->db->get();
		
	}
	public function update_user_password($password, $user_id) {
		$data = array("user_password" => $password);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_login_details', $data);
		return ;
	}
	
	public function update_theme($theme,$user_id) {
		$data = array("theme_name" => $theme);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data);
		return ;
	}
	public function update_url($urlName, $url,$user_id) {
		$data = array("site_name"=>$urlName ,"url" => $url);
		$this->db->where('user_details_id', $user_id);
		$this->db->update('user_details', $data);
		return ;
	}
	
	
	
	function get_user_list($user_id = ''){
		 
		$this->db->select('*');
		$this->db->from('user_details u');
		$this->db->join('address_details a', 'a.address_details_id = u.address_details_id');
		$this->db->join('country_details c', 'c.country_id = a.country_id');
		if($user_id !='')
			$this->db->where('user_details_id', $user_id);
		//$this->db->where('u.user_type_id', '5');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			$data['user_info'] = '';
		}else{
			$data['user_info'] = $query->result();
		}
		return $data;	
	}
	
	function get_user_details($user_id = ''){
		 
		$this->db->select('*');
		$this->db->from('user_details u');
		$this->db->join('address_details a', 'a.address_details_id = u.address_details_id');
		$this->db->join('country_details c', 'c.country_id = a.country_id');
		if($user_id !='')
			$this->db->where('user_details_id', $user_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			$data['user_info'] = '';
		}else{
			$data['user_info'] = $query->result();
		}
		return $data;	
	}
	public function get_deposit_amount($user_id, $branch_id="") {
		$this->db->where('user_id', $user_id);
		return $this->db->get('user_accounts');
	}
	public function PercentageMinusAmount($total,$amount){
		$total = number_format(($total-$amount) ,2,'.','');
		return $total;
	}
	public function update_credit_amount($update_credit_amount,$b2b_id){
		$this->db->where('user_id',$b2b_id);
		$this->db->update('user_accounts',$update_credit_amount); 
	}

	public function userAddress($addressing){ 
		$this->db->insert('address_details',$addressing);
		$id = $this->db->insert_id(); //print_r($id); exit();
			if (!empty($id)) {
				return $id;
			} else {
				return false;
			}
		
	}

	function add_user_image($ssid,$user_profile_name){ 
		$update_user_image =  array(
			'user_profile_pic' => $user_profile_name 
			);
		
		$this->db->where('user_details_id', $ssid);
		$this->db->update('user_details', $update_user_image);
	}

	function getHeaderProducts($user_id = ''){ 
   		$this->db->select('*');
        $this->db->from('header_product');
        
        if($user_id !='')
			$this->db->where('agent_id', $user_id);
		$this->db->join("header_details", "header_details.header_details_id = header_product.header_id");
        $this->db->where('header_product.status', 'ACTIVE');
        $this->db->order_by('header_product.position','asc');
        $query=$this->db->get();
        if($query->num_rows() > 0){
			return $query->result();
		}else{
            $this->db->select('*');
			$this->db->from('header_product');
		    $this->db->where('agent_id','0');
		    $this->db->join("header_details", "header_details.header_details_id = header_product.header_id");
		    $this->db->where('header_product.status', 'ACTIVE');
		    $this->db->order_by('header_product.position');
           return $query = $this->db->get()->result();
		}
   }

	
	function getHeaderServices($user_id = ''){ 
   		$this->db->select('*');
        $this->db->from('header_service');
        $this->db->order_by('header_service.position','asc');
        if($user_id !='')
			$this->db->where('agent_id', $user_id);
		$this->db->join("header_details", "header_details.header_details_id = header_service.header_id");
        $this->db->where('header_service.status', 'ACTIVE');
        //$this->db->order_by('header_service.position');
        $query=$this->db->get();
        if($query->num_rows() > 0){
			return $query->result();
		}else{
            $this->db->select('*');
			$this->db->from('header_service');
		    $this->db->where('agent_id','0');
		    $this->db->join("header_details", "header_details.header_details_id = header_service.header_id");
		    $this->db->where('header_service.status', 'ACTIVE');
		    $this->db->order_by('header_service.position');
           return $query = $this->db->get()->result();
		}
   }

	function getHeaderAboutus($user_id = ''){ 
   		$this->db->select('*');
        $this->db->from('header_aboutus');
        $this->db->order_by('header_aboutus.position','asc');
        if($user_id !='')
			$this->db->where('agent_id', $user_id);
		$this->db->join("header_details", "header_details.header_details_id = header_aboutus.header_id");
        $this->db->where('header_aboutus.status', 'ACTIVE');
        //$this->db->order_by('header_service.position');
        $query=$this->db->get();
        if($query->num_rows() > 0){
			return $query->result();
		}else{
            $this->db->select('*');
			$this->db->from('header_aboutus');
		    $this->db->where('agent_id','0');
		    $this->db->join("header_details", "header_details.header_details_id = header_aboutus.header_id");
		    $this->db->where('header_aboutus.status', 'ACTIVE');
		    $this->db->order_by('header_aboutus.position');
           return $query = $this->db->get()->result();
		}
   }

	function getHeaderManageAboutus($id){
	  $this->db->select('*');
      $this->db->from('header_manageaboutus');
      //$this->db->join("header_aboutus", "header_aboutus.headerabout_id = header_manageaboutus.header_id");
      $this->db->where('header_id',$id);
      $this->db->where('status','ACTIVE');
      $this->db->order_by('position');
      $query = $this->db->get(); //echo $this->db->last_query(); exit();	
      return $query->result();
	}

	function getHeaderContact($user_id = ''){ 
   		$this->db->select('*');
        $this->db->from('header_contact');
        $this->db->order_by('header_contact.position','asc');
        if($user_id !='')
			$this->db->where('agent_id', $user_id);
		$this->db->join("header_details", "header_details.header_details_id = header_contact.header_id");
        $this->db->where('header_contact.status', 'ACTIVE');
        //$this->db->order_by('header_service.position');
        $query=$this->db->get();
        if($query->num_rows() > 0){
			return $query->result();
		}else{
            $this->db->select('*');
			$this->db->from('header_contact');
		    $this->db->where('agent_id','0');
		    $this->db->join("header_details", "header_details.header_details_id = header_contact.header_id");
		    $this->db->where('header_contact.status', 'ACTIVE');
		    $this->db->order_by('header_contact.position');
           return $query = $this->db->get()->result();
		}
   }

	function getFooterCarrer($user_id = ''){
   		$this->db->select('*');
        $this->db->from('footer_carrer');
        $this->db->order_by('footer_carrer.position','asc');
        if($user_id !='')
			$this->db->where('agent_id', $user_id);
		$this->db->join("subfooter_details", "subfooter_details.footer_details_id = footer_carrer.footer_id");
        $this->db->where('footer_carrer.status', 'ACTIVE');
        //$this->db->order_by('header_service.position');
        $query=$this->db->get();
        if($query->num_rows() > 0){
			return $query->result();
		}else{
            $this->db->select('*');
			$this->db->from('footer_carrer');
		    $this->db->where('agent_id','0');
		    $this->db->join("subfooter_details", "subfooter_details.footer_details_id = footer_carrer.footer_id");
		    $this->db->where('footer_carrer.status', 'ACTIVE');
		    $this->db->order_by('footer_carrer.position');
           return $query = $this->db->get()->result();
		}
   }

	

	function getFootertermsnconditions($user_id = ''){
   		$this->db->select('*');
        $this->db->from('footer_termsconditions');
        $this->db->order_by('footer_termsconditions.position','asc');
        if($user_id !='')
			$this->db->where('agent_id', $user_id);
		$this->db->join("subfooter_details", "subfooter_details.footer_details_id = footer_termsconditions.footer_id");
        $this->db->where('footer_termsconditions.status', 'ACTIVE');
        //$this->db->order_by('header_service.position');
        $query=$this->db->get();
        if($query->num_rows() > 0){
			return $query->result();
		}else{
            $this->db->select('*');
			$this->db->from('footer_termsconditions');
		    $this->db->where('agent_id','0');
		    $this->db->join("subfooter_details", "subfooter_details.footer_details_id = footer_termsconditions.footer_id");
		    $this->db->where('footer_termsconditions.status', 'ACTIVE');
		    $this->db->order_by('footer_termsconditions.position');
           return $query = $this->db->get()->result();
		}
   }

   function addContactDetails($input){
		
		$insert_data = array(
							'name' => $input['name'],
							'position' 	=> $input['position'],
							'company' 		=> $input['company'],
							'wechat' => $input['wechat'],
							'qq' 	=> $input['qq'],
							'email' 	=> $input['email'],
							'subject' 	=> $input['subject'],
							'message'	=> $input['message'],
							'creation_date'			=> (date('Y-m-d H:i:s'))
						);		
			//echo '<pre>'; print_r($insert_data); exit();
		$this->db->insert('user_contact',$insert_data);
	}
	
}
?>
