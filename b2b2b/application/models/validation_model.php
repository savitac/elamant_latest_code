<?php
class Validation_Model extends CI_Model {
   function __construct(){
         parent::__construct();
    }
   
   function alphabetValidation($name){
	   $regex = '/^[a-zA-Z ]*$/';
	   
	   if(strlen($name) < 1){
		 return false;  
	   } elseif(!preg_match($regex, $name)){
		   return false;
	   } else {
		   return true; 
	   }
	  
   }

   function alphanumericValidationSpaces($name){
   		  $regex = '/^[a-zA-Z0-9\\-\\s]+$/';
	   if(strlen($name) < 1){
		   return false;
	   } elseif(!preg_match($regex, $name)){
		   return false;
	   } else {
		   return true; 
	   }
   }
   
    function alphanumericValidation($name){
	   $regex = '/^[a-zA-Z0-9]+$/';
	   if(strlen($name) < 1){
		   return false;
	   } elseif(!preg_match($regex, $name)){
		   return false;
	   } else {
		   return true; 
	   }
	 }

	 function alphanumericnumber($name){
	 	$regex = '/^(?=.*[a-z].*[a-z])[a-z-]{1,100}$/';
	 	if(strlen($name) < 3){
		   return false;
	   } elseif(!preg_match($regex, $name)){
		   return false;
	   } else {
		   return true; 
	   }
	 }
	 
	 
	  function numberWithSpecialCharacter($name){
	   $regex = '/^[0-9 ()-_]*$/';
	   if(strlen($name) < 8){
		   return false;
	   } elseif(!preg_match($regex, $name)){
		   return false;
	   } else {
		   return true; 
	   }
	 }

	  function numberWithContact($name){ 

	  	$regex = '/^((\+*)((0[ -]+)*|(91 )*)(\d{3}+|\d{3}+))|\d{3}([- ]*)\d{3}/';
	   if(strlen($name) < 3){ 
		   return false;
	   } elseif(!preg_match($regex, $name)){  //print_r($regex); exit();
		   return false;
	   } else {
		   return true; 
	   }
	 }
	 
	   function emailIdFormat($name){
		    
	   $regex = '/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/';
	   if($name == ''){
		   return false;
	   }
	   if(strlen($name) < 8){
		   return false;
	   } elseif(!preg_match($regex, $name)){
		   return false;
	   } else {
		   return true; 
	   }
	 }
	
	 
	 function passwordValidation($password) {
		 $regex = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/';
		 if(strlen($password) < 8){
		   return false;
	   } elseif(!preg_match($regex, $password)){
		   return false;
	   } else {
		   return true; 
	   }
	 }
	 
	 function checkPasswords($password, $cpassword) {
		 if($password == $cpassword){
			 return true;
		 }else{
			 return false;
		 }
	 }
	 
	 function amountValidation($amount){
		 $regex = '/^[0-9]+(?:\.[0-9]{0,2})?$/';
		 if(!preg_match($regex, $amount)){
			 return false;
		 }else{
			  return true;
		 }
	 }

	 function amountValidationSpaces($number){
	 	$regex = '/^[0-9 ]+/';
		 if(!preg_match($regex, $number)){
			 return false;
		 }else{
			  return true;
		 }
	 }

}
?>
