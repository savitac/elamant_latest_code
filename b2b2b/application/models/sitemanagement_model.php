<?php
class Sitemanagement_Model extends CI_Model {

   function __construct(){
         parent::__construct();
    }

      public function get_hotels_list_site(){
        $this->db->select('*');
        $this->db->from('api_hotels_cities');
        $this->db->order_by("city", "asc"); 
        $this->db->group_by("city, city_name, country, state");
        $query = $this->db->get();
       // echo $this->db->last_query(); 
       return $query->result();
    }
    
  function get_domain_data($user_id = ''){
	   $this->db->select('*');
	   $this->db->from('domain_data');
	   if($user_id != ''){
	   $this->db->where('domain_creater', $user_id);
	   $this->db->order_by('id', 'asc');
	   $this->db->limit(1);
	   }
	   return $this->db->get()->result();
   }
  
   
   function add_domain_data($data){

	 $post_data = $this->domain_form_data($data);
	 $post_data['domain_creater'] = $this->session->userdata('user_details_id');
	 
	 $this->db->insert("domain_data", $post_data);
	 return;
	 }

	 function domain_form_data($data){
	   $post_data = array("email" => $data['domain_email'],
	                      "phone" => $data['domain_phone'],
	                      "facebook" => $data['facebook'],
	                      "twitter" => $data['twitter'],
	                      "google" => $data['google'],
	                      "youtube" => $data['youtube'],

	                      );
	    return $post_data;
   }


   function delete_domain_data($id){
		
		
		$this->db->where("id", $id);
		
		$this->db->delete("domain_data");
		
		return;
	}




   /** banner ****/

   function get_banner_data($user_id = ''){
	   $this->db->select('*');
	   $this->db->from('domain_banner');
	   if($user_id != ''){
		   $this->db->where('domain_creater', $user_id);
		   $this->db->order_by('id', 'asc');   
	   }
	   return $this->db->get()->result();
   }


  
   
   function add_banner_data($data,$path){

	 $post_data = $this->banner_form_data($data,$path);
	 $post_data['domain_creater'] = $this->session->userdata('user_details_id');
	 
	 $this->db->insert("domain_banner", $post_data);
	 return;
	 }

	 function banner_form_data($data,$path){
	   $post_data = array("banner" => $path,
	                       "msg" => $data['msg'],
	                      );
	    return $post_data;
   }

   function delete_banner_data($id){
		
		
		$this->db->where("id", $id);
		
		$this->db->delete("domain_banner");
		
		return;
	}

	function delete_package_data($id){
		
		
		$this->db->where("id", $id);
		
		$this->db->delete("domain_package"); 
		
		return;
	}

    /** Hotel ****/

   function get_Hotel_data($user_id = ''){
	   $this->db->select('*');
	   $this->db->from('api_city_list');
	   if($user_id != ''){
	   $this->db->where('domain_id', $user_id);
	   $this->db->where('top_destination', ACTIVE);
	   
	   }
	   return $this->db->get()->result();
   }
 

  
   
   function add_hotel_data($data,$path){

	 $post_data = $this->hotel_form_data($data,$path);
	 $post_data['domain_creater'] = $this->session->userdata('user_details_id');
	 
	 $this->db->insert("domain_hotel", $post_data);
	 return;
	 }

	 function hotel_form_data($data,$path){
	   $post_data = array("hotel_image" => $path,
	   	                   "hotel_name" => $data['hotel_name'],
	                      
	                      );
	    return $post_data;
   }

   function delete_hotel_data($id){

		$data=array('top_destination'=>0);
		$this->db->where('origin',$id);
		$this->db->update('api_city_list',$data);
		return;
	}

   /** Packages ****/

   function get_Package_data($user_id = ''){
	   $this->db->select('*');
	   $this->db->from('domain_package');
	   if($user_id != ''){
	   $this->db->where('domain_creater', $user_id);
	   $this->db->order_by('id', 'asc');
	   
	   }
	   return $this->db->get()->result();
   }


  
   
   function add_package_data($data,$path){

	 $post_data = $this->package_form_data($data,$path);
	 $post_data['domain_creater'] = $this->session->userdata('user_details_id');
	 
	 $this->db->insert("domain_package", $post_data);
	 return;
	 }

	 function package_form_data($data,$path){
	   $post_data = array("package_image" => $path,
	   	                   "package_name" => $data['package_name'],
	                      
	                      );
	    return $post_data;
   }


   /** Airlines ****/

   function get_Airline_data($user_id = ''){
	   $this->db->select('*');
	   $this->db->from('domain_airlines');
	   if($user_id != ''){
	   $this->db->where('domain_creater', $user_id);
	   $this->db->order_by('id', 'asc');
	   
	   }
	   return $this->db->get()->result();
   }


  
   
   function add_airline_data($data,$path){

	 $post_data = $this->airline_form_data($data,$path);
	 $post_data['domain_creater'] = $this->session->userdata('user_details_id');
	 
	 $this->db->insert("domain_airlines", $post_data);
	 return;
	 }

	 function airline_form_data($data,$path){
	   $post_data = array("airlines_image" => $path,
	   	                   
	                      
	                      );
	    return $post_data;
   }

   function delete_air_data($id){
		
		
		$this->db->where("id", $id);
		
		$this->db->delete("domain_airlines");
		
		return;
	}


   /** static Content ****/

   function get_Page_data($user_id = ''){
	   $this->db->select('*');
	   $this->db->from('domain_pages_data');
	   if($user_id != ''){
	   $this->db->where('domain_creater', $user_id);
	   $this->db->order_by('id', 'asc');
	   
	   }
	   return $this->db->get()->result();
   }


  
   
   function add_page_data($data){

	 $post_data = $this->page_form_data($data);
	 $post_data['domain_creater'] = $this->session->userdata('user_details_id');
	 
	 $this->db->insert("domain_pages_data", $post_data);
	 return;
	 }

	 function page_form_data($data,$path){
	   $post_data = array("page_name" => $data['page_name'],
	                      "page_title" => $data['page_title'],
	                      "page_data" => $data['page_data'],
	   	                   
	                      
	                      );
	    return $post_data;
   }



   function get_Page_content($user_id = '',$id,$page_name){

   	   //echo 'ji';
   	   //echo 'heelo';
   	   
   	   $sql = "SELECT * from  domain_pages_data WHERE domain_creater='$user_id' and id='$id' and page_name='$page_name'";
   	    $query = $this->db->query($sql);
   	    return $query->result();


   	   exit;
	   
   }


   function get_pageing_data($id = ''){
	   $this->db->select('*');
	   $this->db->from('domain_pages_data');
	   if($id != ''){
	   $this->db->where('id', $id);
	   
	   
	   }
	   return $this->db->get()->result();
   }


   function editing_page_data($data,$id){
	 $post_data = $this->pageedit_form_data($data);
	 $post_data['domain_creater'] = $this->session->userdata('user_details_id');
	 
       $this->db->where("id", $id);
	   $this->db->update("domain_pages_data", $post_data);
	   return;
	 return;
	 }

	 function pageedit_form_data($data){
	   $post_data = array(
	                     
	                      "page_name" => $data['page_name'],
	                      "page_title" => $data['page_title'],
	                      "page_data" => $data['page_data']
	   	                   
	                      
	                      );
	    return $post_data;
   }
   
		function delete_page($id) 
		{
		$this->db->where('id', $id);
		$this->db->delete('domain_pages_data'); 
		}
    public function get_news_list_site(){
        $this->db->select('*');
        $this->db->from('news_letter');
        
        $query = $this->db->get();
       // echo $this->db->last_query(); 
       return $query->result();
    }


   function add_news_data($data){

	 $post_data = $this->news_form_data($data);
	 
	 
	 $this->db->insert("news_letter", $post_data);
	 return;
	 }

	 function news_form_data($data){
	   $post_data = array("email" => $data['email'],
	                      

	                      );
	    return $post_data;
   }









  
   
    function update_agent_roles($data){
	 $post_data = $this->role_form_data($data);
	 $post_data['role_modified_by_id'] = $this->session->userdata('user_details_id');
	 $post_data['role_modified_date'] =  date('Y-m-d');
	 $this->db->insert("agent_roles", $post_data);
	 return;
	 }
	 
	 function get_dashboard_details(){
		 $this->db->select('*');
		 $this->db->from('homepage_leftmenu');
		 $this->db->where('dashboard_status', 'ACTIVE');
		 $this->db->order_by('position', 'asc');
		 	if($query->num_rows() ==''){
			$data['dashboard_info'] = '';
		}else{
			$data['dashboard_info'] = $query->result();
		}
		if($data['user_info']!=''){
			for($u=0;$u<count($data['user_info']);$u++){
				$data['domain_details'][$u] 	= $this->Users_Model->get_domain_details($data['user_info'][$u]->user_details_id);
				
			}
		}
		return $data;
	 }
	  
   
   
   function activate_roles($roles_id){
	   $data = array('role_status' => 1,
	                 'role_modified_by_id' => $this->session->userdata('user_details_id'), 
	                 'role_modified_date' => date('Y-m-d'));
	   $this->db->where("agent_roles_id", $roles_id);
	   $this->db->update("agent_roles", $data);
   }
   
   function inactivate_roles($roles_id){
	   $data = array('role_status' => 0,
	                 'role_modified_by_id' => $this->session->userdata('user_details_id'), 
	                 'role_modified_date' => date('Y-m-d'));
	   $this->db->where("agent_roles_id", $roles_id);
	   $this->db->update("agent_roles", $data);
	   return;
	}
	
	function update_privileges($data){
		
		$role_id = json_decode(base64_decode($data['role_id']));
		$this->db->where("user_role_id", $role_id);
		$this->db->where("user_branch_id", $this->session->userdata('branch_id'));
		$this->db->delete("user_privileges");
		if(count($data['user_privilege']) > 0){
			for($i=0; $i < count($data['user_privilege']); $i++){
				  $datas = array(
				                 "dashbaord_id" => $data['user_privilege'][$i],
				                 "user_branch_id" => $this->session->userdata('branch_id'),
				                 "user_role_id" => $role_id,
				                 "user_type_id" => 2);
				 $this->db->insert("user_privileges", $datas);
			 }
			
		}
		return;
	}

	

}
?>
