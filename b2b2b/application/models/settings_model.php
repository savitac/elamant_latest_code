<?php
 
class Settings_Model extends CI_Model {
   function __construct(){
         parent::__construct();
    }

   
   function getAgentMarkups($agent_id, $markup_id = '', $tablename){
   
	    $this->db->select('*');
	    $this->db->from($tablename);
	    $this->db->join('product_details', 'product_details.product_details_id = '.$tablename.'.product_details_id');
	    //$this->db->join('api_details', 'api_details.api_details_id = '.$tablename.'.api_details_id');
	    $this->db->join('country_details', 'country_details.country_id = '.$tablename.'.country_id', 'left');
	    //$this->db->where('user_details_id', $agent_id);
	    if($markup_id != ''){
		 $this->db->where('markup_details_id', $markup_id);
		}

		 if($tablename == 'agent_markup'){
		 	 $this->db->where('created_branch_id', $this->session->userdata('branch_id'));
		 }else if($tablename == 'sub_agent_markup'){
			 $this->db->where('created_branch_id', $this->session->userdata('branch_id'));  
		 }else if($tablename == 'b2b2b_agent_markup'){
			  $this->db->where('user_details_id', $this->session->userdata('user_details_id'));
		 }
	     $query = $this->db->get()->result();
	     return $query;
   }
   
   function getSubAgentMarkups($agent_id, $markup_id =''){
	    $this->db->select('*');
	    $this->db->from('sub_agent_markup');
	    $this->db->join('product_details', 'product_details.product_details_id = sub_agent_markup.product_details_id');
	    //$this->db->join('api_details', 'api_details.api_details_id = sub_agent_markup.api_details_id');
	    $this->db->join('country_details', 'country_details.country_id = sub_agent_markup.country_id', 'left');
	    $this->db->join('user_details', 'user_details.user_details_id = sub_agent_markup.user_details_id', 'left');
	    $this->db->where('created_branch_id', $agent_id);
	    if($markup_id != ''){
		 $this->db->where('markup_details_id', $markup_id);
		}
	    $query = $this->db->get()->result();
		return $query;
   }

   /*function getUserAgentMarkups($agent_id, $markup_id){
	    $this->db->select('*');
	    $this->db->from('agent_markup');
	    $this->db->join('product_details', 'product_details.product_details_id = agent_markup.product_details_id');
	    $this->db->join('api_details', 'api_details.api_details_id = agent_markup.api_details_id');
	    $this->db->join('country_details', 'country_details.country_id = agent_markup.country_id', 'left');
	    $this->db->where('user_details_id', $agent_id);
	    if($markup_id != ''){
		 $this->db->where('markup_details_id', $markup_id);
		}
	     $query = $this->db->get()->result();
	     //echo $this->db->last_query($query); exit();
	     return $query;
   }*/
  
  function addAgentMarkup($data, $tablename){
	  //$postData = $this->markupFormData($data);
			$postData = array( "product_details_id" => $data['product_id'],
			"markup_type" => $data['markup_type'],
			"markup_value_type" => $data['markup_value_type'],
			"markup_value" => $data['markup_value'],
			"user_type_id" => empty($data['user_type_id']) ? 0 : $data['user_type_id'],
			"user_details_id" => $this->session->userdata('user_details_id'),
			"created_branch_id" => $this->session->userdata('branch_id'),
			"markup_status" => "ACTIVE",
			);
	
	  $this->db->insert($tablename, $postData);
	  return;
  }
  
   function updateAgentMarkup($data, $markup_id, $tablename){
	  $postData = $this->markupFormData($data);
	  $this->db->where("markup_details_id", $markup_id);
	  $this->db->update($tablename, $postData);
	  return;
  }
  
  function addSubAgentMarkup($data){
	  $postData = $this->subAgentMarkupFormData($data);
	  $postData['created_branch_id'] = $this->session->userdata('branch_id');
	  $postData['created_by_id'] = $this->session->userdata('user_details_id');
	  $this->db->insert("sub_agent_markup", $postData);
	  return;
  }
  
  function updateSubAgentMarkup($data, $markupid){
	  $postData = $this->subAgentMarkupFormData($data);
	  $this->db->where("markup_details_id", $markupid);
	  $this->db->update("sub_agent_markup", $postData);
	}
  
  
  function markupFormData($data){
  	$data['user_type_id'] = $this->session->userdata('user_type');
	  $inputData = array( "product_details_id" => $data['product_id'],
	                      "markup_type" => $data['markup_type'],
	                      "markup_value_type" => $data['markup_value_type'],
	                      "markup_value" => $data['markup_value'],
	                      "user_type_id" => $data['user_type_id'],
	                      "user_details_id" => $this->session->userdata('user_details_id'),
	                      "created_branch_id" => $this->session->userdata('branch_id'),
	                      "markup_status" => "ACTIVE",
	                    );
	  if(isset($data['country_code']) && $data['country_code'] != ''){
		  $inputData["country_id"] = $data['country_code'];
	  }
	  
	  if(isset($data['start_date']) && $data['start_date'] != ''){
		  $inputData["markup_start_date"] =  date('Y-m-d', strtotime($data['start_date']));
	  }
	  
	   if(isset($data['end_date']) && $data['end_date'] != ''){
		  $inputData["markup_end_date"] = date('Y-m-d', strtotime($data['end_date']));
	  }
	  
	   if(isset($data['expiry_date']) && $data['expiry_date'] != ''){
		  $inputData["markup_expiry_date"] = date('Y-m-d', strtotime($data['expiry_date']));
	  }

	  if(isset($data['expiry_date_to']) && $data['expiry_date_to'] != ''){
		  $inputData["markup_expiry_date_to"] = date('Y-m-d', strtotime($data['expiry_date_to']));
	  }

	  return $inputData;
 }
 
 function subAgentMarkupFormData($data){
	  $inputData = array( "markup_type" => $data['markup_type'],
	                      "product_details_id" => $data['product_id'],
	                      "markup_value_type" => $data['markup_value_type'],
	                      "markup_value" => $data['markup_value'],
	                      "markup_status" => 'ACTIVE',
	                    );
	             
	   if(isset($data['country_code']) && $data['country_code'] != ''){
		  $inputData["country_id"] = $data['country_code'];
	  }
	  
	
		  $inputData["user_type_id"] = 4;
	 
	  if(isset($data['sub_agent_id']) && $data['sub_agent_id'] != ''){
		  $inputData["user_details_id"] = $data['sub_agent_id'];
	  }
	  
	  if(isset($data['start_date']) && $data['start_date'] != ''){
		  $inputData["markup_start_date"] =  date('Y-m-d', strtotime($data['start_date']));
	  }
	  
	   if(isset($data['end_date']) && $data['end_date'] != ''){
		  $inputData["markup_end_date"] = date('Y-m-d', strtotime($data['end_date']));
	  }
	  
	   if(isset($data['expiry_date']) && $data['expiry_date'] != ''){
		  $inputData["markup_expiry_date"] = date('Y-m-d', strtotime($data['expiry_date']));
	  } 
	  if(isset($data['expiry_date_to']) && $data['expiry_date_to'] != ''){
		  $inputData["markup_expiry_date_to"] = date('Y-m-d', strtotime($data['expiry_date_to']));
	  }                  
	                    
	  return $inputData;
 }
 
 function changeMarkupStatus($markup_id,$tablename,$status){
	 $this->db->where('markup_details_id', $markup_id);
	 $data =  array('markup_status'=>$status);
	 $this->db->update($tablename, $data);
	 
	 return;
 }


 function changeSubAgentMarkupStatus($markup_id,$status){
	 $this->db->where('markup_details_id', $markup_id);
	 $data =  array('markup_status'=>$status);
	 $this->db->update('sub_agent_markup', $data);
	 
	 return;
 }
 
 function deletemarkups($markup_id){
	 $this->db->where('agent_markup_id', $markup_id);
	 $this->db->delete('agent_markup');
	 return;
}

 function get_available_general_markup($product_id, $markup_value_type = '', $markupvalue = '', $table_name, $user_id,$created_id,$branch_id ){
 	//print_r($table_name); exit();
	   $this->db->select("*");
	  $this->db->from($table_name);
	  //$this->db->where("api_details_id", $api_id);
	  $this->db->where("product_details_id", $product_id);
	  $this->db->where("user_details_id", $user_id);
	  $this->db->where("markup_type", "GENERAL");

	  if($markupvalue != ''){
        $this->db->where("markup_value", $markupvalue);  
         } 
         if($markup_value_type != ''){
        $this->db->where("markup_value_type", $markup_value_type);  
         } 
	  //$this->db->where('created_branch_id', $this->session->userdata('branch_id'));
	  if($markup_id != ''){
		$this->db->where("markup_details_id !=", $markup_id);  
	  }

	  if($branch_id != ''){
		$this->db->where("created_branch_id", $branch_id);  
	  }
	  if($created_id != ''){
		$this->db->where("created_by_id", $created_id);  
	  }
	  
	  $query = $this->db->get()->row();
	 //echo  $this->db->last_query($query); exit();
	  return $query;
  }
  

   function get_available_specific_markup($product_id, $markup_value_type = '', $markupvalue = '', $table_name, $user_id,$user_type){    
	  $this->db->select("*"); 
	  $this->db->from($table_name);
	  $this->db->where("user_details_id", $user_id);
	  $this->db->where("markup_type", "SPECIFIC");
	  $this->db->where("product_details_id", $product_id); 
	  $this->db->where("user_type_id", $user_type); 
	  $query = $this->db->get()->row();     
	  return $query;
  } 
	
}
?>
