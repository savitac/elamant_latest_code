<?php
class Transfer_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    } 
   
    function fetch_search_result($request,$session_data, $api_id){
		$this->db->select('gta_transfer_data.*, transfer_temp_results.*, min(total_cost) as total_cost');
		$this->db->from('transfer_temp_results');
		$this->db->join('gta_transfer_data', 'gta_transfer_data.item_code = transfer_temp_results.item_code');
		$this->db->where('session_id', $session_data);
		$this->db->group_by('transfer_temp_results.item_code');
		$this->db->order_by('total_cost');
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function fetch_temp_result_old($temp_resullt_id, $item_code){
		$this->db->select('*');
		$this->db->from('transfer_temp_results');
		$this->db->join('gta_transfer_data', 'gta_transfer_data.item_code = transfer_temp_results.item_code');
		$this->db->where('transfer_temp_results.transfer_temp_results', $temp_resullt_id);
		$this->db->where('transfer_temp_results.item_code', $item_code);
		$this->db->group_by('transfer_temp_results.item_code');
		$query = $this->db->get();
		return $query->result();
	}
	
	function clear_temp_cart($session_id, $item_code){
		$this->db->where("session_id", $session_id);
		$this->db->where("item_code", $item_code);
		$this->db->delete("cart_transfer");
	} 
	
	function clear_temp_global($session_id){
		$this->db->where("session_id", $session_id);
		$this->db->where("product_id", 19);
		$this->db->delete("cart_global");
	}
	
	
	function fetch_temp_result($temp_resullt_id, $item_code){
		$this->db->select('*,min(total_cost)');
		$this->db->from('transfer_vehicle_results');
		$this->db->join('gta_transfer_data', 'gta_transfer_data.item_code = transfer_vehicle_results.item_code');
		$this->db->where('transfer_vehicle_results.transfer_vehicle_id', $temp_resullt_id);
		$this->db->where('transfer_vehicle_results.item_code', $item_code);
		$this->db->group_by('transfer_vehicle_results.item_code');
		$query = $this->db->get();
		return $query->result();
	}
	
	function add_transfer_cart($transfer_data){
		$this->db->insert('cart_transfer', $transfer_data);
		return $this->db->insert_id();
	}
	
	function clear_search_transfers($ses_id){
		$this->db->where("session_id", $ses_id);
		$this->db->delete("transfer_temp_results");
		
		
		$this->db->where("session_id", $ses_id);
		$this->db->delete("transfer_vehicle_results");
		return;
	}
	
	function clear_old_search_transfers(){
		$sql = "delete from transfer_temp_results where search_date < CURDATE()";
		$this->db->query($sql);
		$sql = "delete from transfer_vehicle_results where search_date < CURDATE()";
		$this->db->query($sql);
		
	}
	
	function TransferCodeDetails($code){
		$this->db->select('*');
		$this->db->from('transfer_list_codes');
		$this->db->where('transfer_list_code', $code);
		return $query = $this->db->get();
	}
	
	function get_transfer_vehicles($item_code, $session_id){
		$this->db->select('*');
		$this->db->from('transfer_vehicle_results');
		$this->db->where('session_id', $session_id);
		$this->db->where('item_code', $item_code);
		$this->db->group_by('vehicle_details');
		$this->db->order_by('total_cost', 'asc');
		$query = $this->db->get();
		return $query;
	}
	
	
	
   }
?>
