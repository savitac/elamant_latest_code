<?php
class General_Model extends CI_Model {

   function __construct(){
   	
         parent::__construct();
    }
    public function getNationalityCountries() {
		$this->db->order_by('country_name', 'asc');
		return $this->db->get('country_details');
	}
	
	public function getPages($page_id = ""){
		$this->db->select('*');
		$this->db->from('static_page_details');
		$this->db->where('status', 'ACTIVE');
		return $this->db->get()->result();
	}
	
	public function get_home_contents(){
		$this->db->select('*');
		$this->db->from('home_page_content');
		$this->db->where('status', '1');
		return $this->db->get()->result();
	}
	
	public function getUserDetails($user_id){
		$this->db->select('*');
		$this->db->from('user_details');
		$this->db->where("user_details_id", $user_id);
		return $this->db->get();
	}
	public function getnoticeDetails($user_id)
	{
		$this->db->select('*');
		$this->db->from('noticeboard');
		$this->db->where("b2b_id", $user_id);
		$this->db->where("status", "ACTIVE");    
		return $this->db->get();  
	}
	
	 function getCountryList($country_id = ''){
		$this->db->select('*');
		$this->db->from('country_details');
		if($country_id !='')
			$this->db->where('country_id', $country_id);
		$this->db->where_not_in('iso_code','');
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
		}
		
		function getCountryByCountyCode($country_code){
		$this->db->select('*');
		$this->db->from('country_details');
		if($country_code !='')
			$this->db->where('country_name', $country_code);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->row();
		}
		}
		
		function getCountryByCountyCodeNew($country_code){
		$this->db->select('*');
		$this->db->from('country_details');
		if($country_code !='')
			$this->db->where('iso_code', $country_code);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->row();
		}
		}
		
		/*function getCurrencyList($currency_id = ''){
		$this->db->select('*');
		$this->db->from('currency_details');
		if($currency_id !='')
			$this->db->where('currency_details_id', $currency_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}*/
	
	public function currency_convertor($amount,$from,$to){
    	$to = strtoupper($to);
    	$this->db->where('currency_code',$from);
    	$price = $this->db->get('currency_details')->row();
    	$value = $price->value;
    	if($from === $to){
    		$amount = $amount/1; 
    		return number_format(($amount) ,2,'.','');
    	} else {
    		$amount = ($amount)/($value);
    		return number_format(($amount) ,2,'.','');
    	}
    	
    }
    public function get_api($api,$api_usage = ''){
		$this->db->where('api_details_id', $api);
		if($api_usage!=''){
			$this->db->where('api_credential_type', $api_usage);
		} else {
			$this->db->where('api_credential_type', 'TEST');
		}
        $this->db->where('api_status', 'ACTIVE');
        $query = $this->db->get('api_details');
        return $query;
	}
	function getApiList($api_id = ''){
		$this->db->select('*');
		$this->db->from('api_details');
		if($api_id !='')
			$this->db->where('api_details_id', $api_id);
		$query=$this->db->get();
		if($query->num_rows() ==''){
			return '';
		}else{
			return $query->result();
		}
	}
    function get_api_details($module_name) {
        $this->db->select('*');
        $this->db->from('api_details');
        $this->db->join('product_details', 'product_details.product_details_id = api_details.product_id');
		$this->db->where('product_details.product_name',$module_name);
		$this->db->where('api_details.api_status','ACTIVE');
        $query = $this->db->get();
      //  print $this->db->last_query();exit;
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
    
    function get_api_details_id($api_id ='') {
        $this->db->select('*');
        $this->db->from('api_details');
        $this->db->join('product_details', 'product_details.product_details_id = api_details.product_id');
	    $this->db->where('api_details.api_status','ACTIVE');
	    if($api_id != '') {
	    	$this->db->where('api_details.api_details_id',$api_id);
		}
        $query = $this->db->get();
      //  echo $this->db->last_query(); exit;
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->row();
        }
    }
    
     function get_api_list($api_id ='') {
        $this->db->select('*');
        $this->db->from('api_details');
        $this->db->join('product_details', 'product_details.product_details_id = api_details.product_id');
	    $this->db->where('api_details.api_status','ACTIVE');
	    if($api_id != '') {
	    	$this->db->where('api_details.api_details_id',$api_id);
		}
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
    public function get_api_credentials($api){
		$this->db->where('api_name', $api);
		$this->db->where('api_status', 'ACTIVE');
		return $this->db->get('api_details');
	}
	
	public function get_api_credentials_by_id($api_id){
		$this->db->where('api_details_id', $api_id);
		$this->db->where('api_status', 'ACTIVE');
		return $this->db->get('api_details');
	}
	
	function get_city_details_id_new($city_code) {
		
		$city_array = explode(",", $city_code); 
		 $this->db->select('*');
		$this->db->from('api_hotels_cities'); 
		$this->db->like('city',$city_array[0], '%'); 
		$this->db->where('country', $city_array[1]);
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
    
    function get_city_details_id_old($city_code) {
        $this->db->select('*');
		$this->db->from('api_hotels_cities'); 
		$this->db->where('city',$city_code); 
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->row();
        }
    }
    
    function get_city_details_id($city_code) {
		$city_arr = explode(",", $city_code);
		$this->db->select('*');
		$this->db->from('api_hotels_cities_1'); 
		if(count($city_arr) > 0) {
		 if(count($city_arr) == 3){
			if($city_arr[0] != '') {
		       $this->db->where('city',$city_arr[0]);
		    }
		    if($city_arr[1] != '') {
		       $this->db->where('state',$city_arr[1]);
		    }
		    if($city_arr[2] != '') {
		       $this->db->where('country',$city_arr[2]);
		    }
		}elseif(count($city_arr) == 2){
			if($city_arr[0] != '') {
		       $this->db->where('city',$city_arr[0]);
		    }
		    if($city_arr[1] != '') {
		       $this->db->where('country',$city_arr[1]);
		    }
		}elseif(count($city_arr) == 1){
			if($city_arr[0] != '') {
		       $this->db->where('city',$city_arr[0]);
		    }
		}
		    $this->db->group_by('gta_city_code, city, state');
	    }
		
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
    }
    
    
    
   function begin_transaction(){
		$this->db->trans_begin();
	}
	
	function commit_transaction(){
		$this->db->trans_commit();
	}
	
	function rollback_transaction(){
		$this->db->trans_rollback();
	}
	
	public function get_user_details($user_details_id,$user_type =''){
		$this->db->select('user_details.*,user_type.user_type_name,address_details.*,country_details.*');
		$this->db->where('user_details.user_details_id', $user_details_id);
		if($user_type != ''){
		$this->db->where('user_details.user_type_id', $user_type);
	    }
		$this->db->from('user_details');
		$this->db->join('user_type', 'user_type.user_type_id=user_details.user_type_id', 'left');
		$this->db->join('address_details', 'address_details.address_details_id=user_details.address_details_id', 'left');
		$this->db->join('country_details', 'country_details.country_id=address_details.country_id', 'left');
		$query = $this->db->get();   
		
		if ($query->num_rows() > 0) {
			 
			return $query->row();
		}else{
			return false;
		}
	}
	function store_logs($connection_response,$status,$error_text='')
    {
		//~ echo "data: <pre>";print_r($connection_response);exit;
        $data = array(
                'api_name' => $connection_response['api_id'],
                'xml_type' => $connection_response['xml_title'],
                'xml_request' => $connection_response['request'],
                'xml_response' =>  $connection_response['response'],
				'ip_address' =>  $this->input->ip_address(),
				//'xml_url' => $connection_response['xml_url'],
				'xml_status' =>  $status,
				//'error_text' =>  $error_text
                );
        $this->db->insert('xml_logs', $data);
		return $this->db->insert_id();
           
    }
    
    public function getHeader(){
		$this->db->select('*');
		$this->db->from('header_details');
		$this->db->where('menu_status', 'ACTIVE');
		$this->db->order_by('position','asc');
		return $this->db->get()->result();
	}

	public function getAgentBalance(){
		$this->db->select('*');
		$this->db->from('user_accounts');
		if($this->session->userdata('user_type') == 4){
		  $this->db->where("user_id", $this->session->userdata('user_details_id'));	
		}else{
		  $this->db->where("user_id", $this->session->userdata('branch_id'));
			// $this->db->where("user_id", 1);
		}
		
		$query = $this->db->get();
		return $query;
	}
	function get_city_details_cityname($city) {
        $this->db->select('*');
        /*$this->db->from('api_hotel_cities'); 
		$this->db->where('city_code',$city_code); */
		$this->db->from('api_hotels_cities'); 
		$this->db->where('city_name',$city); 
        $query = $this->db->get();
        if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->row();
        }
    }
    
    function getTransferCountries($country_code = ''){
		$this->db->select("*");
		$this->db->from("city_code_gta_transfer");
		if($country_code != ''){
		$this->db->where('country_code', $country_code);
		}
		$this->db->group_by('country_code');
		$this->db->order_by('country_name', 'asc');
		return $this->db->get();
	}
	
	function getTransferCities($country_code, $city_code =''){
		$this->db->select('*, city_code as "transfer_city_code", city as "city_name"');
		$this->db->from('city_code_gta_transfer');
		$this->db->where('country_code', $country_code);
		if($city_code != ''){
			$this->db->where('city_code', $city_code);
		}
		$this->db->group_by('city_code');
		$this->db->order_by('city');
		return $this->db->get();
	}
	
	function gatTranferListCode(){
		$this->db->select('*');
		$this->db->from('transfer_list_codes');
		$this->db->where('status', 'ACTIVE');
		return $this->db->get();
	}
	
	function get_api_details_by_id($product_id, $mode){
		$this->db->select('*');
        $this->db->from('api_details');
        $this->db->join('product_details', 'product_details.product_details_id = api_details.product_id');
        $this->db->where('api_details.api_credential_type',$mode);
		$this->db->where('product_details.product_details_id',$product_id);
		$this->db->where('api_details.api_status','ACTIVE');
        $query = $this->db->get();
         if ($query->num_rows() == '') {
            return '';
        } else {
            return $query->result();
        }
	}
	
	function insert_transfer_static($data){
		$this->db->insert('gta_transfer_data', $data);
		return;
	}
	

	function getTransferAirports($aiport_code = ''){
		$this->db->select('*, airport_code as "iata_code" ');
		$this->db->from('flight_airport_list');
		if($aiport_code != ''){
		$this->db->where('airport_code', $aiport_code);
		}
		return $this->db->get();
		
	}
	
	function getTransferHotels($CityCode ='', $hotel_code = ''){
		$this->db->select('*');
		$this->db->from('gta_Hotels');
		if($CityCode != ''){
		$this->db->where('CityCode', $CityCode);
		}
		if($hotel_code != ''){
		$this->db->where('HotelCode', $hotel_code);
		}
		$query = $this->db->get();
		return $query;
	}
	
	function getCancellationBuffer($module, $api) {
		$this->db->select('*');
		$this->db->from('cancelbuffer');
		if($module != ''){
			$this->db->where('product_id', $module);
		}
		if($api != ''){
			$this->db->where('api_id', $api);
		}
		$this->db->where('buffer_status', 'ACTIVE');
		return $this->db->get();
	}
	
		function insert_transfer_result($data){
		$this->db->insert('transfer_temp_results', $data);
		return $this->db->insert_id();
	}
	
	
	function insert_vehicle_result($data, $temp_id){
		$data['temp_result_id'] = $temp_id;
		$this->db->insert('transfer_vehicle_results', $data);
		return;
	}
	
	function getPortCities($city_code = ''){
		$this->db->select('*');
		$this->db->from('gta_port_cities');
		if($city_code != ''){
			$this->db->where('city_code', $city_code);
		}
		return $this->db->get();
		
	}
	
	function getStationsDetails($city_code = '', $station_code =''){
		$this->db->select('*');
		$this->db->from('gta_transfer_station');
		if($city_code != ''){
			$this->db->where('city_code', $city_code);
		}
		if($station_code != ''){
			$this->db->where('station_code', $station_code);
		}
		$query = $this->db->get();
		return $query;
		
	}
	
	function insert_transfer_station_static($data){
		$this->db->insert('gta_transfer_station', $data);
		return;
	}

	public function getCurrencyList($currency = ''){
		$this->db->select('*');
		if($currency != ''){
			$this->db->where('currency_code',$currency);
		}
        $this->db->order_by("currency_code", "asc");
		$query = $this->db->get('currency_details');
		if ( $query->num_rows > 0 ) {
			return $query->result();
		}
		return false;
	}
	
	function get_promo_details_old($product_id, $promo_code,$travel_date, $booking_date){
		
		$sql = "select * from promo_code_details where 
		         product ='$product_id' and promo_code = '$promo_code' and 
		        DATEDIFF('$travel_date', travel_date_from) >=0  and DATEDIFF('$travel_date', travel_date_to) <=0 and 
		        DATEDIFF('$travel_date' , DATE_FORMAT(exp_date, '%Y-%m-%d')) <=0 and  
		         status = 0";
		 
		$query =  $this->db->query($sql);
		if($query->num_rows == 0){
			
			$sql1 = "select * from promo_code_details where 
		         product ='$product_id' and promo_code = '$promo_code' and 
		        DATEDIFF('$booking_date', booking_date_from) >=0  and DATEDIFF('$booking_date', booking_date_to) <=0 and
		        DATEDIFF('$booking_date', DATE_FORMAT(exp_date, '%Y-%m-%d')) <=0 and  
		         status = 0";
		     $query1  = $this->db->query($sql1);
		    if($query1->num_rows == 0){
				return "";
			}else{
				return $query1->row();
			}
		}else{
		  return $query->row();
		}
	}
	
	function get_promo_details($product_id, $promo_code,$travel_date, $booking_date, $booking_amount){
		
		     $sql = "select * from promo_code_details where 
		        product ='$product_id' and promo_code = '$promo_code' and 
		        DATEDIFF('$travel_date', travel_date_from) >=0  and DATEDIFF('$travel_date', travel_date_to) <=0 and 
		        DATEDIFF('$booking_date', booking_date_from) >=0  and DATEDIFF('$booking_date', booking_date_to) <=0 
		        and status = 0 and amount_valid_from <=".$booking_amount." and amount_valid_to >=".$booking_amount; 
		       
		     
		$query =  $this->db->query($sql);
		
		if($query->num_rows == 0){
			return "";
		}else{
		  return $query->row();
		}
	}
	
	
	function promo_calcualtion($amount, $promo_type, $discount){
		if($promo_type == 'Promo code by %'){
			 $discount_calc = $amount *($discount/ 100);
			 $discount_amount = $amount - $discount_calc;
		}elseif($promo_type == 'Promo code by amount'){
			  $discount_calc = $discount;
			  $discount_amount = $amount- $discount; 
		}
		return array("discount"=> $discount_calc, "amount" =>$discount_amount);
	}
	
	function get_tax_details($countr_id, $state_id){
		$this->db->select('*');
		$this->db->from('tax_rate_info');
		$this->db->where('country', $countr_id);
		if($state_id != 0){
		$this->db->where('state_id', $state_id);	
		}
		return $this->db->get();
	}
	
	function tax_calculation($tax_details, $amount){
		if(false){
		if($tax_details->service_charge != null || $tax_details->service_charge != 0 || $tax_details->service_charge = ''){
			$sc_tax = $amount * ($tax_details->service_charge / 100);
			$amount_with_sc = $amount + $sc_tax;
		}else{
			$sc_tax = 0;
			$amount_with_sc = $amount;
		}
		
		if($tax_details->gst != null || $tax_details->gst != 0 || $tax_details->gst = ''){
			$gst_tax = $amount_with_sc * ($tax_details->gst / 100);
			$amount_with_sc_gst = $amount_with_sc + $gst_tax;
		}else{
			$gst_tax = 0;
			$amount_with_sc_gst = $amount_with_sc;
		}
		
		if($tax_details->tax != null || $tax_details->tax != 0 || $tax_details->tax = ''){
			$state_tax = $amount_with_sc_gst * ($tax_details->tax / 100);
			$amount_with_sc_gst_tax = $amount_with_sc_gst + $state_tax;
		}else{
			$state_tax = 0;
			$amount_with_sc_gst_tax = $amount_with_sc_gst;
		}
		
		return array("sc_tax" => $sc_tax, "gst_tax" => $gst_tax, 'state_tax' =>$state_tax, "amount" => $amount_with_sc_gst_tax);
	   }else{
		 return array("sc_tax" => 0, "gst_tax" =>0, 'state_tax' =>0, "amount" => $amount);  
	   }
	}
	
	
	function get_country_name($countr_id){
		$this->db->select('country_name');
		$this->db->from('country_details');
		$this->db->where('country_id', $countr_id);
		return $this->db->get();
	}
	
	 public function get_site_details($site_name){
		$this->db->select('*');
		$this->db->from('domain_details');
		$this->db->join('user_details', 'domain_details.agent_id = user_details.user_details_id');
		$this->db->where('domain_name', $site_name);
		$query = $this->db->get();
		return $query->row();
	}
	/**
	* Dinesh Kumar
	* Is Registered Model
	**/
	public function isRegistered($email) {
        $this->db->where('user_email', $email);
        return $this->db->get('user_details');
    }
	
	public function getLogin($email, $user_details_id) {
        $this->db->where('user_name', $email);
        $this->db->where('user_details_id', $user_details_id);
        return $this->db->get('user_login_details');
    }
    public function updatePwdResetLink($userid, $key, $secret) {
        $update = array(
            'pwd_reset_random_key' => $key,
            'pwd_reset_secret_key' => $secret
        );
        $this->db->where('user_details_id', $userid);
        return $this->db->update('user_details', $update);
    }
    /**
	* Dinesh Kumar
	* Code Ends 
	**/
}
?>
