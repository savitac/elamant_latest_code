<?php
require_once 'abstract_management_model.php';
/**
 * @package    Provab Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class Commission_Model extends Abstract_Management_Model
{
	function __construct() {
		parent::__construct('level_2');
	}


 /*Commission for b2b2b 
	 Karthick
	 Start*/

	 function get_admin_commission_details($agent_fk)
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();

		$domain_auth_id = $this->session->userdata('domain_list_fk');
		
		$agent_details_query = 'select U.*,UT.user_type_id
								FROM user_details AS U 
								INNER JOIN user_type AS UT ON U.user_type_id=UT.user_type_id
								
								WHERE U.domain_list_fk ='.$domain_auth_id.' and U.user_details_id = '.intval($agent_fk);
		$flight_commission_query = 'select BFCD.* from b2b_flight_commission_details as BFCD 
								inner join user_details as U on BFCD.agent_fk = U.user_details_id
								where BFCD.domain_list_fk ='.$domain_auth_id.' and BFCD.agent_fk='.intval($agent_fk).' and BFCD.type="specific"'; 
		$bus_commission_query = 'select BBCD.* from b2b_bus_commission_details as BBCD 
								inner join user_details as U on BBCD.agent_fk = U.user_details_id
								where BBCD.domain_list_fk ='.$domain_auth_id.' and BBCD.agent_fk='.intval($agent_fk).' and BBCD.type="specific"';

		$sightseeing_commission_query = 'select BSCD.* from b2b_sightseeing_commission_details as BSCD 
								inner join user_details as U on BSCD.agent_fk = U.user_details_id
								where BSCD.domain_list_fk ='.$domain_auth_id.' and BSCD.agent_fk='.intval($agent_fk).' and BSCD.type="specific"';
						
		$transfer_commission_query = 'select BTCD.* from b2b_transfer_commission_details as BTCD 
								inner join user_details as U on BTCD.agent_fk = U.user_details_id
								where BTCD.domain_list_fk ='.$domain_auth_id.' and BTCD.agent_fk='.intval($agent_fk).' and BTCD.type="specific"';


		$response['data']['agent_details']				= $this->db->query($agent_details_query)->row_array();

		$response['data']['flight_commission_details']	= $this->db->query($flight_commission_query)->row_array();

		// $response['data']['hotel_commission_details']	= '';
		$response['data']['bus_commission_details']		= $this->db->query($bus_commission_query)->row_array();

		$response['data']['sightseeing_commission_details'] = $this->db->query($sightseeing_commission_query)->row_array();

		$response['data']['transfer_commission_details'] = $this->db->query($transfer_commission_query)->row_array();

		
		if (valid_array($response['data']['agent_details']) == true) {
			$response['status'] = SUCCESS_STATUS;

		if(valid_array($response['data']['flight_commission_details']) &&valid_array($response['data']['bus_commission_details']) && valid_array($response['data']['sightseeing_commission_details']) && valid_array($response['data']['transfer_commission_details'])){
			$response['commission_status'] = SUCCESS_STATUS;
			
		}
		}
		return $response;
	}



	function get_commission_details($agent_fk)
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();

		$domain_auth_id = $this->session->userdata('domain_list_fk');
		
		$agent_details_query = 'select U.*,UT.user_type_id
								FROM user_details AS U 
								INNER JOIN user_type AS UT ON U.user_type_id=UT.user_type_id
								
								WHERE U.domain_list_fk ='.$domain_auth_id.' and U.user_details_id = '.intval($agent_fk);
		$flight_commission_query = 'select BFCD.* from b2b2b_flight_commission_details as BFCD 
								inner join user_details as U on BFCD.agent_fk = U.user_details_id
								where BFCD.domain_list_fk ='.$domain_auth_id.' and BFCD.agent_fk='.intval($agent_fk).' and BFCD.type="specific"'; 
		$bus_commission_query = 'select BBCD.* from b2b2b_bus_commission_details as BBCD 
								inner join user_details as U on BBCD.agent_fk = U.user_details_id
								where BBCD.domain_list_fk ='.$domain_auth_id.' and BBCD.agent_fk='.intval($agent_fk).' and BBCD.type="specific"';

		$sightseeing_commission_query = 'select BSCD.* from b2b2b_sightseeing_commission_details as BSCD 
								inner join user_details as U on BSCD.agent_fk = U.user_details_id
								where BSCD.domain_list_fk ='.$domain_auth_id.' and BSCD.agent_fk='.intval($agent_fk).' and BSCD.type="specific"';
						
		$transfer_commission_query = 'select BTCD.* from b2b2b_transfer_commission_details as BTCD 
								inner join user_details as U on BTCD.agent_fk = U.user_details_id
								where BTCD.domain_list_fk ='.$domain_auth_id.' and BTCD.agent_fk='.intval($agent_fk).' and BTCD.type="specific"';


		$response['data']['agent_details']				= $this->db->query($agent_details_query)->row_array();
		$response['data']['flight_commission_details']	= $this->db->query($flight_commission_query)->row_array();;
		// $response['data']['hotel_commission_details']	= '';
		$response['data']['bus_commission_details']		= $this->db->query($bus_commission_query)->row_array();

		$response['data']['sightseeing_commission_details'] = $this->db->query($sightseeing_commission_query)->row_array();

		$response['data']['transfer_commission_details'] = $this->db->query($transfer_commission_query)->row_array();

		
		if (valid_array($response['data']['agent_details']) == true) {
			$response['status'] = SUCCESS_STATUS;
		}
		return $response;
	}
	

	function agent_commission_details($condition=array(), $count=false, $offset=0, $limit=100000000000)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		$domain_auth_id = $this->session->userdata('domain_list_fk');
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();

		if (!$count) {
			$agent_details_query = 'select U.*,BFCD.value as flight_commission_value,BFCD.api_value as flight_api_value,BFCD.value_type as flight_commission_type,
									BBCD.value as bus_commission_value,BBCD.api_value as bus_api_value,BBCD.value_type as bus_commission_type,

									BSCD.value as sightseeing_commission_value,BSCD.api_value as sightseeing_api_value,BSCD.value_type as sightseeing_commission_type,
									
									BTCD.value as transfer_commission_value,BTCD.api_value as transfer_api_value,BTCD.value_type as transfer_commission_type

									FROM user_details AS U 
									INNER JOIN user_type AS UT ON U.user_type_id=UT.user_type_id
									left join b2b_flight_commission_details as BFCD on U.user_details_id=BFCD.agent_fk and BFCD.type="specific"
									left join b2b_bus_commission_details as BBCD on U.user_details_id=BBCD.agent_fk and BBCD.type="specific"
									left join b2b_sightseeing_commission_details as BSCD on U.user_details_id=BSCD.agent_fk and BSCD.type="specific"

									left join b2b_transfer_commission_details as BTCD on U.user_details_id=BTCD.agent_fk and BTCD.type="specific"



									WHERE U.user_type_id = '.B2B2B_USER.' and U.domain_list_fk ='.$domain_auth_id.$condition.'
									ORDER BY U.user_details_id DESC limit '.$offset.', '.$limit;
									
			$response['data']['agent_commission_details']				= $this->db->query($agent_details_query)->result_array();
			return $response;
		} else {
			return $this->db->query('SELECT count(*) as total FROM user_details AS U, user_type AS UT, api_country_list AS ACL
				 WHERE U.user_type_id=UT.user_type_id 
				 AND U.country_id=ACL.origin '.$condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}
	/**
	 * Jaganath
	 * b2b2b Agent Commission Details: Flight, Bus,Sightseeing
	 * @param $search_filter_condition => (condition)
	 */
	function filter_agent_commission_details($search_filter_condition = '', $count=false, $offset=0, $limit=100000000000)
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		if(empty($search_filter_condition) == false) {
			$search_filter_condition = ' and'.$search_filter_condition;
		}
		if (!$count) {
			$agent_details_query = 'select U.*,BFCD.value as flight_commission_value,BFCD.api_value as flight_api_value,BFCD.value_type as flight_commission_type,
									BBCD.value as bus_commission_value,BBCD.api_value as bus_api_value,BBCD.value_type as bus_commission_type,

									BSCD.value as sightseeing_commission_value,BSCD.api_value as sightseeing_api_value,BSCD.value_type as sightseeing_commission_type,

									BTCD.value as transfer_commission_value,BTCD.api_value as transfer_api_value,BTCD.value_type as transfer_commission_type


									FROM user AS U 
									INNER JOIN user_type AS UT ON U.user_type_id=UT.user_type_id
									left join b2b2b_flight_commission_details as BFCD on U.user_details_id=BFCD.agent_fk and BFCD.type="specific"
									left join b2b2b_bus_commission_details as BBCD on U.user_details_id=BBCD.agent_fk and BBCD.type="specific"

									left join b2b2b_sightseeing_commission_details as BSCD on U.user_details_id=BSCD.agent_fk and BSCD.type="specific"

									left join b2b2b_transfer_commission_details as BTCD on U.user_details_id=BTCD.agent_fk and BTCD.type="specific"



									WHERE U.user_type_id = '.B2B2B_USER.' and U.domain_list_fk ='.$domain_auth_id.$search_filter_condition.'
									ORDER BY U.user_details_id DESC limit '.$offset.', '.$limit;
			$response['data']['agent_commission_details']				= $this->db->query($agent_details_query)->result_array();
			return $response;
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U, user_type AS UT, api_country_list AS ACL
				 WHERE U.user_type_id=UT.user_type_id AND U.user_type_id = '.b2b2b_USER.' and U.domain_list_fk ='.get_domain_auth_id().' and  U.country_code=ACL.origin'.$search_filter_condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}
	/**
	 * Jaganath
	 * b2b2b Default Commission Details: Flight, Hotel, Bus,Sightseeing
	 * @param $agent_fk
	 */
	function default_commission_details()
	{
		$response['status'] = SUCCESS_STATUS;
		$response['data'] = array();

		$domain_auth_id = $this->session->userdata('domain_list_fk');
		$flight_commission_query = 'select BFCD.* from b2b2b_flight_commission_details as BFCD
								where BFCD.domain_list_fk ='.$domain_auth_id.' and BFCD.type="generic"';
								
		$bus_commission_query = 'select BBCD.* from b2b2b_bus_commission_details as BBCD
								where BBCD.domain_list_fk ='.$domain_auth_id.' and BBCD.type="generic"';

		$sightseeing_commission_query = 'select BSCD.* from b2b2b_sightseeing_commission_details as BSCD
								where BSCD.domain_list_fk ='.$domain_auth_id.' and BSCD.type="generic"';

		$transfer_commission_query = 'select BTCD.* from b2b2b_transfer_commission_details as BTCD
								where BTCD.domain_list_fk ='.$domain_auth_id.' and BTCD.type="generic"';
													

		$response['data']['flight_commission_details']	= $this->db->query($flight_commission_query)->row_array();;
		// $response['data']['hotel_commission_details']	= '';
		$response['data']['bus_commission_details']		= $this->db->query($bus_commission_query)->row_array();
		$response['data']['sightseeing_commission_details'] = $this->db->query($sightseeing_commission_query)->row_array();
		$response['data']['transfer_commission_details'] = $this->db->query($transfer_commission_query)->row_array();

		return $response;
	}
	/**
	 * Jaganath 
	 */
	function auto_suggest_agency_name($chars, $limit=15)
	{
		$query = 'select U.*
					FROM user AS U 
					INNER JOIN user_type AS UT ON U.user_type_id=UT.user_type_id 
					WHERE U.agency_name!="" and U.domain_list_fk ='.get_domain_auth_id().' and 
					(U.uuid like "%'.$chars.'%" OR U.agency_name like "%'.$chars.'%" OR U.first_name like "%'.$chars.'%" OR U.last_name like "%'.$chars.'%" OR U.email like "%'.$chars.'%" OR U.phone like "%'.$chars.'%" )
					order by U.agency_name asc limit 0, '.$limit;
		return $this->db->query($query)->result_array();
	}
	/**
	 * Jaganath
	 * Bank Account Details
	 */
	function bank_account_details() 
	{
		$query='SELECT BAD.*,U.user_name 
		        FROM bank_account_details BAD
		        JOIN user_details U on U.user_details_id=BAD.created_by_id
		        where BAD.domain_list_fk='.$this->session->userdata('domain_list_fk');
		$tmp_data = $this->db->query($query);
		if($tmp_data->num_rows()>0) {
			$tmp_data=$tmp_data->result_array();
			$data = array('status' => QUERY_SUCCESS, 'data' => $tmp_data);
		} else {
			$data = array('status' => QUERY_FAILURE);
		 }
		 return $data;
	}

	function admin_bank_account_details() 
	{
		$query='SELECT BAD.*,U.admin_name as created_by_name 
		        FROM bank_account_details BAD
		        JOIN admin_details U on U.admin_id=BAD.created_by_id
		        where BAD.status = 1 AND BAD.domain_list_fk='.get_domain_auth_id();
		$tmp_data = $this->db->query($query);
		if($tmp_data->num_rows()>0) {
			$tmp_data=$tmp_data->result_array();
			$data = array('status' => QUERY_SUCCESS, 'data' => $tmp_data);
		} else {
			$data = array('status' => QUERY_FAILURE);
		 }
		 return $data;
	}

	function agent_bank_account_details() 
	{
		$query='SELECT BAD.*,U.user_name 
		        FROM bank_account_details BAD
		        JOIN user_details U on U.user_details_id=BAD.created_by_id
		        where BAD.status = 1 AND BAD.domain_list_fk='.$this->session->userdata('domain_list_fk');
		$tmp_data = $this->db->query($query);
		if($tmp_data->num_rows()>0) {
			$tmp_data=$tmp_data->result_array();
			$data = array('status' => QUERY_SUCCESS, 'data' => $tmp_data);
		} else {
			$data = array('status' => QUERY_FAILURE);
		 }
		 return $data;
	}
}