<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
error_reporting(0);
session_start();
ini_set('memory_limit', '-1');
class Home extends CI_Controller {
	
    public function __construct(){
		parent::__construct();	
		//$this->check_isvalidated();
		
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		
		$this->load->model('Security_Model');
		
		$this->load->model('Dashboard_Model');
		$this->load->model('Sitemanagement_Model');
		$this->load->model('Hotel_Model');
		
		$this->load->helper('gta_helper');
		
		$this->agent_site_settings();
		
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
		
		
	}
	
	private function check_isvalidated()
	{
		if($this->session->userdata('user_details_id'))
		{
				redirect('dashboard');
		
		}
	 }
	 
	function agent_site_settings(){
		$site_name = str_replace('/','',$_SERVER['REQUEST_URI']);
        //echo $_SERVER['SERVER_NAME'];
        
         $site_name = 'provab';
        
         //$site_name = '';
   
		if($site_name != '' ){
			
		$site_details  =  $this->General_Model->get_site_details($site_name);
		
		if(count($site_details) > 0){
			
		$site_session =  array(
		                'domain_id' => $site_details->domain_details_id,
		                'branch_id' => $site_details->user_details_id,
		                'theme_name' => $site_details->theme_name,
		                'site_name' => $site_details->domain_name,
		                'domain_logo' => $site_details->domain_logo,
		                'required_access' => $site_details->required_access,
		                'company_name' => $site_details->company_name,
		             );
		  $this->session->set_userdata($site_session); 
		 
		}else{
			
			$this->session->unset_userdata('domain_logo');
			$this->session->unset_userdata('branch_id');
			$this->session->unset_userdata('theme_name');
			$this->session->unset_userdata('site_name');
			$this->session->unset_userdata('domain_logo');
			$this->session->unset_userdata('required_access');
			$this->session->unset_userdata('company_name');
		}
	  }else{
		    $this->session->unset_userdata('domain_logo');
			$this->session->unset_userdata('branch_id');
			$this->session->unset_userdata('theme_name');
			$this->session->unset_userdata('site_name');
			$this->session->unset_userdata('domain_logo');
			$this->session->unset_userdata('required_access');
			$this->session->unset_userdata('company_name');
		  
	  }
	  
	//  echo "<pre />"; print_r($this->session->userdata);
	 
	  
	}
	
	function index(){
	
	 $domain_id=$this->session->userdata('branch_id');
	 
	 $home['content'] = $this->General_Model->get_home_contents();
	 $home['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
	 $home['transfer_countries'] = $this->General_Model->getTransferCountries()->result();
	 $home['tranfer_list_code'] = $this->General_Model->gatTranferListCode()->result();
	 if($this->session->userdata('site_name') == ''){
	 $this->load->view('b2b/index', $home);
     }else{
     $home['domain_data'] = $this->Sitemanagement_Model->get_domain_data($user_id);
	  $home['banner'] = $this->Sitemanagement_Model->get_banner_data($user_id);
	  $home['hotel'] = $this->Sitemanagement_Model->get_Hotel_data($user_id);
	  $home['packages'] = $this->Sitemanagement_Model->get_Package_data($user_id);
	   $home['airlines'] = $this->Sitemanagement_Model->get_Airline_data($user_id);
	 $home['pages'] = $this->Sitemanagement_Model->get_Page_data($user_id);

	 $this->load->view('b2b/index', $home);	 
	 }
	}


	function page_data($id,$page_name){ 

	 $domain_id=$this->session->userdata('branch_id'); 
	  $page=urldecode($page_name);
	  $users['pages'] = $this->Sitemanagement_Model->get_Page_content($domain_id,$id,$page);
	  //var_dump($users['pages']);
	  //exit;
	  $this->load->view('general/pages',$users);	
	}

	function add_newsletter(){ 

	 if(count($_POST) > 0) {

	    $this->Sitemanagement_Model->add_news_data($_POST);
        $this->index();
	    }	
	}
	
	function login(){
		 $this->load->view('b2b/login');
	}
	
	function get_hotel_cities(){
		ini_set('memory_limit', '-1');
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$result = array();
		$cities = $this->Hotel_Model->get_gta_cities($term)->result();
		if(!empty($cities)) {
			foreach($cities as $city){
				if($city->hCount > 0 ){
					$auto_city['label'] = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($city->city_name)))).', '.$city->country_name;
					$auto_city['value'] = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($city->city_name)))).', '.$city->country_name;
					$auto_city['CityId'] = $city->city_id;
					$auto_city['HotelCode'] = '';
					$auto_city['HotelCount'] = $city->hCount;
					$auto_city['category'] = 'Cities';
					$result[] = $auto_city; 
				}
			}
		}
		echo json_encode($result);
	}
	
	function Products(){
		$userid = $this->session->userdata('user_details_id');
		$products['products'] = $this->Dashboard_Model->getHeaderProducts();

		$this->load->view('general/products',$products);
	}

	function Services(){
		$userid = $this->session->userdata('user_details_id');
		$service['services'] = $this->Dashboard_Model->getHeaderServices();
		
		$this->load->view('general/services',$service);
	}

	function AboutUs(){
		$userid = $this->session->userdata('user_details_id');
		$service['aboutus'] = $this->Dashboard_Model->getHeaderAboutus();
		//echo '<pre>'; print_r($service['aboutus']); 
		$this->load->view('general/aboutus',$service);
	}

	function Contact(){
		$userid = $this->session->userdata('user_details_id');
		$contact['contact'] = $this->Dashboard_Model->getHeaderContact();
		//echo '<pre>'; print_r($service['aboutus']); 
		$this->load->view('general/contact',$contact);
	}
	function carrers(){
		$userid = $this->session->userdata('user_details_id');
		$carrer['carrer'] = $this->Dashboard_Model->getFooterCarrer();
		//echo '<pre>'; print_r($service['aboutus']); 
		$this->load->view('general/carrers',$carrer);
	}

	function termsnconditions(){
		$userid = $this->session->userdata('user_details_id');
		$terms['terms'] = $this->Dashboard_Model->getFootertermsnconditions();
		//echo '<pre>'; print_r($service['aboutus']); 
		$this->load->view('general/termsnconditions',$terms);
	}

	function createContact(){ //echo '<pre>'; print_r($_POST); exit();
		$this->Dashboard_Model->addContactDetails($_POST);
		redirect('home','refresh');
	}

	// Function to change language of the website
	public function change_language(){
		$language = $this->input->post('language');
		$_SESSION['TheChinaGap']['language'] = $language; //echo 'sanjay'; print_r($language); exit();
        $response = array(
        	'status' => 1
        );
        echo json_encode($response);
	}

	//End Of Language
}
?>
