<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

error_reporting(0);
class Transferbooking extends CI_Controller {
	function __construct(){
		parent::__construct();
	
		$this->load->model('cart_model');
		$this->load->model('booking_model');
		$this->load->model('payment_model');
		$this->load->model('account_model');
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Hotel_Model');
		$this->load->model('XML_Model');
		$this->load->helper('file');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	}

	function transfer_booking($global_id, $bid, $billing_address_id,$booking_id, $parent_pnr){
		               
		              $this->load->helper('gtatransfer_helper');
						$booking_supplier_details = $this->booking_model->get_booking_supplier_details($global_id->booking_supplier_id)->row();
						$passenger_info = $this->booking_model->getBookingpassengerTemp($bid)->result();
					    $billingaddress = $this->booking_model->getbillingaddressTemp($billing_address_id)->row();
						$cart_transfer_data = $this->booking_model->getBookingTransferTemp($booking_id)->row();
						
						$checkout_form = json_decode($cart_transfer_data->TravelerDetails);
						$passenger_info_c = json_encode($passenger_info);
					    $billingaddress_c = json_encode($billingaddress);
					  
					  $pickupcode = $cart_transfer_data->pickup_code;
					  $dropoff_code = $cart_transfer_data->dropoff_code;
					
					    $ProcessTerms_response = AddBookingRequest($cart_transfer_data, $passenger_info_c,$checkout_form, $billingaddress_c, $booking_id,$parent_pnr,$cart_transfer_data->cid);
					    $request = $ProcessTerms_response['bookingRequest'];
						$response = $ProcessTerms_response['bookingResponse'];
						//print_r($request);
						//print_r($response);exit;
						$ProcessTermsRequest_ = 'xml_logs/'.$parent_pnr.'/BookingRequest_'.$booking_id.'.xml'; 
						$ProcessTermsResponse_ = 'xml_logs/'.$parent_pnr.'/BookingResponse_'.$booking_id.'.xml'; 
								
								write_file($ProcessTermsRequest_,$ProcessTerms_response['bookingRequest'], 'w+');
								write_file($ProcessTermsResponse_,$ProcessTerms_response['bookingResponse'], 'w+');
					   
					     $dom = new DOMDocument();
						$dom->preserveWhiteSpace = false;
						$dom->loadxml($ProcessTerms_response['bookingResponse']);
					
						$booking_response_details  	= $dom->getElementsByTagName("BookingResponse");
					   $booking_data = array();
					    if(!empty($ProcessTerms_response['bookingResponse'])){
							
							if($booking_response_details->length > 0){
								
								foreach($booking_response_details as $booking_response){
									$booking_reference_details = $booking_response->getElementsByTagName("BookingReferences");
									if($booking_reference_details->length > 0) {
										foreach($booking_reference_details as $booking_references){
											 $booking_reference_detail  = $booking_references->getElementsByTagName("BookingReference");
											 foreach($booking_reference_detail as $booking_reference){
												 $booking_reference_code = $booking_reference->getAttribute('ReferenceSource');
												 if($booking_reference_code == 'client'){
													$booking_data['booking_no'] = $booking_no = $booking_reference->nodeValue;
												 }else{
													$booking_data['api_confirmation_no'] = $api_confirmation_no = $booking_reference->nodeValue;
												 }
												 
												 $voucher_date_details = $booking_response->getElementsByTagName("BookingCreationDate");
												 $booking_data['voucher_date'] = $voucher_date = $voucher_date_details->item(0)->nodeValue;
												 
												 $travel_date_details = $booking_response->getElementsByTagName("BookingCreationDate");
												 $booking_data['travel_date'] = $travel_date = $travel_date_details->item(0)->nodeValue;
												 
												 $booking_price_details = $booking_response->getElementsByTagName("BookingPrice"); 
												 $booking_data['commission_rate'] = $booking_price_details->item(0)->getAttribute('Commission');
												 $booking_data['currency'] = $booking_price_details->item(0)->getAttribute('Currency');
												 $booking_data['gross_rate'] = $booking_price_details->item(0)->getAttribute('Gross');
												 $booking_data['net_rate'] = $booking_price_details->item(0)->getAttribute('Nett');
												 
												  $booking_status_details = $booking_response->getElementsByTagName("BookingStatus");
												  $booking_status_code = $booking_status_details->item(0)->getAttribute('Code');
												  if($booking_status_code == 'CP'){
													  $booking_status = 'PENDING';
												  }
												  $booking_details = array();
												  $booking_item_details = $booking_response->getElementsByTagName('BookingItems');
												  if($booking_item_details->length > 0){
													  foreach($booking_item_details as $booking_items){
														$BookingItems = $booking_items->getElementsByTagName("BookingItem");
														foreach($BookingItems as $BookingItem){
															 $ItemCity = $BookingItem->getElementsByTagName("ItemCity");
															 $booking_details['item_city_code'] = $ItemCity->item(0)->getAttribute('Code');
															 $booking_details['item_city'] = $ItemCity->item(0)->nodeValue;
															 
															 $Item = $BookingItem->getElementsByTagName("Item");
															 $booking_details['item_code'] = $Item->item(0)->getAttribute('Code');
															 $booking_details['item_description'] = $item_description = $Item->item(0)->nodeValue;
															 
															 $ItemPrice = $BookingItem->getElementsByTagName("ItemPrice");
															 $booking_details['Commission'] = $trans_commission_rate = $ItemPrice->item(0)->getAttribute('Commission');
															 $booking_details['currency'] = $currency = $ItemPrice->item(0)->getAttribute('Currency');
															 $booking_details['gross_rate'] = $trans_gross_rate=  $ItemPrice->item(0)->getAttribute('Gross');
															 $booking_details['net_rate'] = $trans_net_rate  =$ItemPrice->item(0)->getAttribute('Nett');
															
															 $ItemStatus = $BookingItem->getElementsByTagName("ItemStatus");
															 $booking_details['booking_status'] = $ItemStatus->item(0)->getAttribute('ItemStatus');
															 $booking_details['booking_status_description'] = $booking_status_description = $ItemStatus->item(0)->nodeValue;
															 
															 $ItemConfirmationReference = $BookingItem->getElementsByTagName("ItemConfirmationReference");
														     $booking_details['ItemConfirmationReference_no'] = $ItemConfirmationReference->item(0)->nodeValue;
														     
														     $TransferItem = $BookingItem->getElementsByTagName("TransferItem");
														     
														     if($TransferItem->length > 0){
																 foreach($TransferItem as $item){
															       	$SupplierTelephoneNumber = $item->getElementsByTagName("SupplierTelephoneNumber");
																		if($SupplierTelephoneNumber->length > 0){
																			 $booking_details['supplier_telephone_no'] = $supplier_telephone_no = $SupplierTelephoneNumber->item(0)->nodeValue;
																		}else{
																			$booking_details['supplier_telephone_no'] = '';
																		}
																		$TransferConditions = $item->getElementsByTagName("TransferConditions");
																		if($TransferConditions->length > 0){
																		   $booking_details['transfer_conditions'] = $transfer_conditions = $TransferConditions->item(0)->nodeValue;
																		}else{
																			$booking_details['transfer_conditions'] = '';
																		}
																}
															}
														}
													  }
												  }
												
											 }
										}
										
									
										
										$data['error_message'] = $booking_id.'|Transfer RESPONSE SUCCESS';
										$data['update_booking_status'] = array(
												'booking_no' => $booking_no,
												'api_confirmation_no' => $api_confirmation_no,
												'voucher_date' => $voucher_date,
												'travel_date' => $travel_date,
												'booking_status' => $booking_status_description,
										);
							
									   $transfer_bookings_details  =  array("booking_request_xml" => $request,
																			 "booking_response_xml" => $response,
																			 "transfer_conditions" => $transfer_conditions,
																			 "supplier_telephone_no" => $supplier_telephone_no);
							
										$this->booking_model->update_transfer_booking_xml($booking_id, $transfer_bookings_details);
							
										$update_transaction = array("trans_commission_rate" => $trans_commission_rate,
																	"trans_gross_rate"  => $trans_gross_rate,
																	"trans_net_rate"  => $trans_net_rate);
																	
										
										$this->booking_model->updateTransactions($global_id->booking_transaction_id, $update_transaction);
							
										$data['update_booking_supplier'] = array(
																			'supplier_reference_number' => $booking_no,
																			'booking_supplier_number' => $api_confirmation_no,
																			'supplier_status' => $booking_status_description
																		);
							            }else{
											$transfer_bookings_details  =  array("booking_request_xml" => $request,
	                                                             "booking_response_xml" => $response
	                                                             );
							
										   $this->booking_model->update_transfer_booking_xml($booking_id, $transfer_bookings_details);
											$data['error_message']  = '|Transfer RESPONSE FAILURE';	
											
											$data['update_booking_status'] = array(
													'voucher_date' => date('Y-m-d'),
													'travel_date' => $cart_transfer_data->travel_start_date,
													'booking_status' => 'FAILED',
											);
											
											$data['update_booking_supplier'] = array(
																			'supplier_status' => 'FAILED'
																		);
										}
								}
							
							}else{
								
							
							$transfer_bookings_details  =  array("booking_request_xml" => $request,
	                                                             "booking_response_xml" => $response
	                                                             );
							
						   $this->booking_model->update_transfer_booking_xml($booking_id, $transfer_bookings_details);
							$data['error_message']  = '|Transfer RESPONSE FAILURE';	
							
							$data['update_booking_status'] = array(
									'voucher_date' => date('Y-m-d'),
									'travel_date' => $cart_transfer_data->travel_start_date,
						            'booking_status' => 'FAILED',
							);
							
							$data['update_booking_supplier'] = array(
																			'supplier_status' => 'FAILED'
																		);
							
							}
						}else{
							
						
							$transfer_bookings_details  =  array("booking_request_xml" => $request,
	                                                             "booking_response_xml" => $response
	                                                            );
							
						   $this->booking_model->update_transfer_booking_xml($booking_id, $transfer_bookings_details);
							$data['error_message']  = '|Transfer RESPONSE EMPTY';	
							$data['update_booking_status'] = array(
									'voucher_date' => date('Y-m-d'),
									'travel_date' => $cart_transfer_data->travel_start_date,
						            'booking_status' => 'FAILED',
							);
							
							$data['update_booking_supplier'] = array(
																			'supplier_status' => 'FAILED'
																		);
						}
						
						
					return $data;	
	}
	
	function transfer_cancel($pnr_no){
		
	      $count = $this->Booking_Model->getBookingPnr_v1($pnr_no)->num_rows();      
         if($count == 1) {
         	 $b_data = $this->Booking_Model->getBookingPnr_v1($pnr_no)->row(); 
         	
         	  $api_details = $this->General_Model->getApiList($b_data->api_id);     
         	  $reference_id = $b_data->referal_id;  
         	  $getBookingTransferData = $this->Booking_Model->getBookingTransferTemp($reference_id)->row();
         	  $transaction_data = $this->Booking_Model->get_transaction_details($b_data->booking_transaction_id)->row();
         	   
         	
         	  $api = $api_details[0]->api_name;
              $CancelTime = date('Y-m-d H:i:s');
              $canceldate = date('Y-m-d');
              $book_usertype = $b_data->user_type_id;
             if($b_data->booking_status == 'CONFIRMED' || $b_data->booking_status == 'PENDING CONFIRMATION'){ 
                 if($getBookingTransferData->caneclation_policy != ''){
					$cancellation_data = json_decode($getBookingTransferData->caneclation_policy);
                    $this->load->helper('gtatransfer_helper');
                    $transferCancellationRQ_RS = TransferBookingCancel($b_data->api_confirmation_no, $b_data->api_id);
                   
                    $CancelReq = $transferCancellationRQ_RS['TransferBookingCancelRQ'];
                    $CancelRes = $transferCancellationRQ_RS['TransferBookingCancelRS'];
                    
                  
                    if($CancelRes != ''){
						$dom = new DOMDocument();
					    $dom->loadXML($CancelRes);
					    $BookingReferences = $dom->getElementsByTagName("BookingResponse");
					    $BookingReferences1 =  $dom->getElementsByTagName("bookingresponse");
					    if($BookingReferences->length > 0){
							foreach($BookingReferences as $reference){
								$BookingReference  =  $reference->getElementsByTagName("BookingReference");
								if($BookingReference->length > 0){
									foreach($BookingReference as $b_reference){
										$ReferenceSource = $b_reference->getAttribute("ReferenceSource");
										if($ReferenceSource == 'client'){
										 $client_ref_number = $b_reference->nodeValue;
										}else{
									     $api_ref_number = $b_reference->nodeValue;
										}
									}
									$BookingCreationDate = 	$reference->getElementsByTagName("BookingCreationDate");
									$booking_date =  $BookingCreationDate->item(0)->nodeValue;	
									
									$BookingDepartureDate = 	$reference->getElementsByTagName("BookingDepartureDate");
									$departure_date =  $BookingDepartureDate->item(0)->nodeValue;	
									
									$BookingPrice = $reference->getElementsByTagName("BookingPrice");	
									 $commissionable_rate = $BookingPrice->item(0)->getAttribute('Commission');		   	
									 $currency = $BookingPrice->item(0)->getAttribute('Currency');		   	
									 $gross_rate = $BookingPrice->item(0)->getAttribute('Gross'); 		
									 $nett_rate = $BookingPrice->item(0)->getAttribute('Nett');	
									 
									 $BookingStatus = $reference->getElementsByTagName("BookingStatus");	 
									 $cancellation_code = $BookingStatus->item(0)->getAttribute('Code');		   	
									 $cancellaiton_description = $BookingStatus->item(0)->nodeValue; 
									 
									 $transfer_cancel_history = array("booking_global_id" =>$b_data->booking_global_id,
									                                   "booking_referel_id" =>$b_data->referal_id,
									                                   "client_ref_number" => $client_ref_number,
									                                   "api_ref_number" => $api_ref_number,
									                                   "booking_date" => $booking_date,
									                                   "departure_date" => $departure_date,
									                                   "commissionable_rate" => $commissionable_rate,
									                                   "currency"=> $currency,
									                                   "gross_rate"=> $gross_rate,
									                                   "nett_rate"=> $nett_rate,
									                                   "cancellation_code"=> $cancellation_code,
									                                   "cancellaiton_description" =>  $cancellaiton_description
									                                    );
									 
									                     
								 	$Charge_API = $nett_rate ;
								 	
								 	$Refund =  (($transaction_data->net_rate + $transaction_data->admin_markup +$transaction_data->agent_markup +$transaction_data->my_agent_Markup +$transaction_data->my_agent_Markup)- $transaction_data->discount ) - $Charge_API;
								 	
									$this->Booking_Model->transfer_cancel_history($transfer_cancel_history);
									
									$update_booking = array(
															'api_status' => $cancellaiton_description,
															'booking_status' => 'CANCELLED',
															'cancellation_status' => 'Cancelled',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $transferCancellationRQ_RS['TransferBookingCancelRQ'],
															'cancel_response' => $transferCancellationRQ_RS['TransferBookingCancelRS'],
															'cancellation_amount' => $Charge_API,
															'refund_amount' => $Refund,
															'refund_status' => 0
														);
								   $this->Booking_Model->Update_Booking_Global_v1($b_data->booking_no, $update_booking);
								 
								}
								$data = array("status" => 1,
								              "message" => "Booking has been Sucessfully " . $cancellaiton_description);
							}
						}
						
						elseif($BookingReferences1->length > 0){
							
						 foreach($BookingReferences1 as $reference){
								$BookingReference  =  $reference->getElementsByTagName("bookingreference");
								if($BookingReference->length > 0){
									foreach($BookingReference as $b_reference){
										$ReferenceSource = $b_reference->getAttribute("referencesource");
										if($ReferenceSource == 'client'){
										 $client_ref_number = $b_reference->nodeValue;
										}else{
									     $api_ref_number = $b_reference->nodeValue;
										}
									}
									$BookingCreationDate = 	$reference->getElementsByTagName("bookingcreationdate");
									$booking_date =  $BookingCreationDate->item(0)->nodeValue;	
									
									$BookingDepartureDate = 	$reference->getElementsByTagName("bookingdeparturedate");
									$departure_date =  $BookingDepartureDate->item(0)->nodeValue;	
									
									$BookingPrice = $reference->getElementsByTagName("bookingprice");	
									 $commissionable_rate = $BookingPrice->item(0)->getAttribute('commission');		   	
									 $currency = $BookingPrice->item(0)->getAttribute('currency');		   	
									 $gross_rate = $BookingPrice->item(0)->getAttribute('gross');		   	
									 $nett_rate = $BookingPrice->item(0)->getAttribute('nett');	
									 
									 $BookingStatus = $reference->getElementsByTagName("bookingstatus");	 
									 $cancellation_code = $BookingStatus->item(0)->getAttribute('code');		   	
									 $cancellaiton_description = $BookingStatus->item(0)->nodeValue; 
									 
									 $transfer_cancel_history = array("booking_global_id" =>$b_data->booking_global_id,
									                                   "booking_referel_id" =>$b_data->referal_id,
									                                   "client_ref_number" => $client_ref_number,
									                                   "api_ref_number" => $api_ref_number,
									                                   "booking_date" => $booking_date,
									                                   "departure_date" => $departure_date,
									                                   "commissionable_rate" => $commissionable_rate,
									                                   "currency"=> $currency,
									                                   "gross_rate"=> $gross_rate,
									                                   "nett_rate"=> $nett_rate,
									                                   "cancellation_code"=> $cancellation_code,
									                                   "cancellaiton_description" =>  $cancellaiton_description
									                                    );
									$Charge_API = $nett_rate;
									$Refund =  (($transaction_data->net_rate + $transaction_data->admin_markup +$transaction_data->agent_markup +$transaction_data->my_agent_Markup +$transaction_data->my_agent_Markup)- $transaction_data->discount ) - $Charge_API;
									
									$this->Booking_Model->transfer_cancel_history($transfer_cancel_history);
									$update_booking = array(
															'api_status' => $cancellaiton_description,
															'booking_status' => 'CANCELLED',
															'cancellation_status' => 'Cancelled',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $transferCancellationRQ_RS['TransferBookingCancelRQ'],
															'cancel_response' => $transferCancellationRQ_RS['TransferBookingCancelRS'],
															'cancellation_amount' => $Charge_API,
															'refund_amount' => $Refund,
															'refund_status' => 0
														);
								   $this->Booking_Model->Update_Booking_Global_v1($b_data->booking_no, $update_booking);
								}
								$data = array("status" => 1,
								              "message" => "Booking has been Sucessfully " . $cancellaiton_description);
							}
						
					     }else{
							$update_booking = array(
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $transferCancellationRQ_RS['TransferBookingCancelRQ'],
															'cancel_response' => $transferCancellationRQ_RS['TransferBookingCancelRS'],
															
														);
						    $this->Booking_Model->Update_Booking_Global_v1($b_data->booking_no, $update_booking);
							$data = array("status" => 1,
								           "message" => "Booking has not been Sucessfully ");
						}
					}
					else{
						 $update_booking = array(
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $transferCancellationRQ_RS['TransferBookingCancelRQ'],
															'cancel_response' => $transferCancellationRQ_RS['TransferBookingCancelRS'],
															
														);
						    $this->Booking_Model->Update_Booking_Global_v1($b_data->booking_no, $update_booking);
						    
						    $data = array("status" => 1,
								           "message" => "Sorry for the inconvinience we are not getting response ");
					}
			    }
            }else{
				   $data = array("status" => 1,
								           "message" => "Sorry for the inconvinience we are not getting response ");
			}
        } 
        return $data;
	}
	
	function booking_response(){
		$xml_rawdata = file_get_contents("php://input");
		echo '<pre/>';print_r($xml_rawdata);
		echo "asdfasdf";exit;
		 
		 
	}
}
?>
