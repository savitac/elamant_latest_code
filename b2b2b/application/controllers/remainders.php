<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
error_reporting(E_ALL);
class Remainders extends CI_Controller {
	
    public function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Remainder_Model');
		$this->load->model('Email_Model');
		
	}
	
	
	function deposite_remainder(){
		$users_deposites = $this->Remainder_Model->user_deposite_details();
		$email_template = $this->Email_Model->get_email_template('user_deposite_notification')->row();
		
		if(count($users_deposites) > 0) {
		   for($u=0; $u < count($users_deposites); $u++) {
			    $email['user_name'] = $users_deposites[$u]->user_name;
			    $email['user_type'] = $users_deposites[$u]->user_type_id;
			    $email['balance'] = $users_deposites[$u]->balance_credit;
				$email['message'] = $email_template->message;
				if($email['user_type'] == 2) {
				$email['from'] = $email_template->email_from;
			    }else{
				$b2buser_mail_id = $this->Remainder_Model->get_b2buser_emailid($users_deposites[$u]->branch_id)->row();
				$email['from'] = $b2buser_mail_id->user_email;
				}
				$email['to'] = $users_deposites[$u]->user_email;
				$email['cc'] = $email_template->email_cc.",sivaraman.provab@gmail.com,ravi.t@provabtechnosoft.com";
				$email['subject'] = $email_template->subject;
				$this->Email_Model->sendDepositeNotification($email);
			}
		}
	
	}
	
	function b2b_payment_due_remainder(){
		$users = $this->Remainder_Model->getagents(2)->result();
		$currenct_date = date('Y-m-d');
		$msg = '';

		$email_template = $this->Email_Model->get_email_template('user_payment_notification')->row();
		for($u = 0; $u < count($users); $u++){
			$msg = '<table border=1>';
			$msg .= '<tr><th>PNR No</th><th>Booking Ref No</th><th>Check In Date</th><th>Check Out Date</th><th>Booked Date</th><th>Amount to be Pay</th><th>Due Days</th></tr>';
			$bookings = $this->Remainder_Model->get_user_bookings($users[$u]->user_details_id)->result();
			$hotel_booking_ids = '';
			$transfer_booking_ids = '';
			foreach($bookings as $global){
				if($global->product_id == 1){
					 $hotel_booking_ids .= $global->referal_id.",";
				}
			}
		
			$msg1 = '';				        
			if($hotel_booking_ids != ''){
				$hotel_booking_ids =  substr($hotel_booking_ids, 0, -1);
				$hotel_bookings = $this->Remainder_Model->get_pending_hotel_bookings($hotel_booking_ids)->result();
				
				foreach($hotel_bookings as $h_booking){
					if($h_booking->buffer_cancellation_policy != '' && $h_booking->buffer_cancellation_policy != NULL){
						if($h_booking->api_id == 1){
						$buffer_cancellation_data = json_decode($h_booking->buffer_cancellation_policy, true);
						if(count($buffer_cancellation_data) > 0){
							
							$date1=strtotime($buffer_cancellation_data[0]['ToDate']);
							$date2=strtotime($currenct_date);
							$diff =$date1 - $date2;
							$day_diff = floor($diff/ 86400);
							
							if($day_diff >= 1 && $day_diff <= 150){
								$msg1 .= "<tr><td>".$h_booking->pnr_no."</td>";
								$msg1 .= "<td>".$h_booking->booking_no."</td>";
								$msg1 .= "<td>".date('d-m-Y', strtotime($h_booking->checkin))."</td>";
								$msg1 .= "<td>".date('d-m-Y', strtotime($h_booking->checkout))."</td>";
								$msg1 .= "<td>".date('d-m-Y', strtotime($h_booking->voucher_date))."</td>";
								$msg1 .= "<td>".$h_booking->admin_baseprice."</td>";
								$msg1 .= "<td>".$day_diff."</td>";
								$msg1 .= "<tr>";
						   }
						}
					  }
					}
				}
				 $message = $msg.$msg1."</table>"; 
			}
			
			if($msg1 != ''){
				$email['user_name'] = $users[$u]->user_name;
			    $email['message'] = $email_template->message;
				$email['from'] = $email_template->email_from;
				$email['to'] = $users[$u]->user_email;
				$email['cc'] = $email_template->email_cc.",sivaraman.provab@gmail.com,ravi.t@provabtechnosoft.com";
				$email['subject'] = $email_template->subject;
				$email['bookings'] = $message;
				$this->Email_Model->sendPaymentNotification($email);
				
			}
		}
	}
	
	function b2b2b_payment_due_remainder(){
		$users = $this->Remainder_Model->getagents(4)->result();
		$currenct_date = date('Y-m-d');
		$msg = '';
		$email_template = $this->Email_Model->get_email_template('user_payment_notification')->row();
		for($u = 0; $u < count($users); $u++){
			$msg = '<table border=1>';
			$msg .= '<tr><th>PNR No</th><th>Booking Ref No</th><th>Check In Date</th><th>Check Out Date</th><th>Booked Date</th><th>Amount to be Pay</th><th>Due Days</th></tr>';
			$bookings = $this->Remainder_Model->get_user_bookings($users[$u]->user_details_id)->result();
			$hotel_booking_ids = '';
			$transfer_booking_ids = '';
			foreach($bookings as $global){
				if($global->product_id == 1){
					 $hotel_booking_ids .= $global->referal_id.",";
				}
			}
		
			$msg1 = '';				        
			if($hotel_booking_ids != ''){
				$hotel_booking_ids =  substr($hotel_booking_ids, 0, -1);
				$hotel_bookings = $this->Remainder_Model->get_pending_hotel_bookings($hotel_booking_ids)->result();
				
				foreach($hotel_bookings as $h_booking){
					if($h_booking->buffer_cancellation_policy != '' && $h_booking->buffer_cancellation_policy != NULL){
						if($h_booking->api_id == 1){
						$buffer_cancellation_data = json_decode($h_booking->buffer_cancellation_policy, true);
						if(count($buffer_cancellation_data) > 0){
							
							$date1=strtotime($buffer_cancellation_data[0]['ToDate']);
							$date2=strtotime($currenct_date);
							$diff =$date1 - $date2;
							$day_diff = floor($diff/ 86400);
							
							if($day_diff >= 1 && $day_diff <= 150){
								$msg1 .= "<tr><td>".$h_booking->pnr_no."</td>";
								$msg1 .= "<td>".$h_booking->booking_no."</td>";
								$msg1 .= "<td>".date('d-m-Y', strtotime($h_booking->checkin))."</td>";
								$msg1 .= "<td>".date('d-m-Y', strtotime($h_booking->checkout))."</td>";
								$msg1 .= "<td>".date('d-m-Y', strtotime($h_booking->voucher_date))."</td>";
								$msg1 .= "<td>".$h_booking->admin_baseprice."</td>";
								$msg1 .= "<td>".$day_diff."</td>";
								$msg1 .= "<tr>";
						   }
						}
					  }
					}
				}
				 $message = $msg.$msg1."</table>"; 
			}
			
			if($msg1 != ''){
				$email['user_name'] = $users[$u]->user_name;
			    $email['message'] = $email_template->message;
			    $b2buser_mail_id = $this->Remainder_Model->get_b2buser_emailid($users[$u]->branch_id)->row();
				$email['from'] = $b2buser_mail_id->user_email;
				$email['to'] = $users[$u]->user_email;
				$email['cc'] = $email_template->email_cc.",sivaraman.provab@gmail.com,ravi.t@provabtechnosoft.com";
				$email['subject'] = $email_template->subject;
				$email['bookings'] = $message;
				$this->Email_Model->sendPaymentNotification($email);
				
			}
		}
	}
}
?>
