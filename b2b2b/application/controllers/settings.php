<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }  
class Settings extends CI_Controller {
	
    public function __construct(){
		parent::__construct();	
		$this->check_isvalidated();
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Settings_Model');
		$this->load->model('Validation_Model');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	}
	
	private function check_isvalidated()
	{
		if($this->session->userdata('user_details_id'))
		{
			$user_id = $this->session->userdata('user_details_id');
			$controller_name = $this->router->fetch_class();
			$function_name = $this->router->fetch_method();
			$this->load->model('Privilege_Model');
            $sub_admin_id = $this->session->userdata('admin_id');
            $privilege_details = $this->Privilege_Model->get_user_privileges($user_id,$controller_name,$function_name, $function_parameter = 1)->row();
               if(count($privilege_details)  == 0)
				{	
				  redirect('error/access_denied');
				} 
		}
	 }
	
	function index(){
		$dashboard['products'] = $this->Dashboard_Model->getProducts();
		//echo '<pre>'; print_r($dashboard['products']); 
		$dashboard['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
		$dashboard['api'] = $this->General_Model->get_api_list();
		 if($this->session->userdata('user_type') == 4){
		   $tablename = "b2b2b_agent_markup";
	     }else{
		   $tablename = "agent_markup";
		}
		if(isset($_GET['class'])){

			$dashboard['class'] = $_GET['class'];
		}
		$dashboard['agent_markups'] = $this->Settings_Model->getAgentMarkups($this->session->userdata('user_details_id'),"", $tablename);
		$this->load->view('settings/settings', $dashboard);
	}
	

	
	function addMarkup($class){ 
		$user_id = $_POST['user_type_id'];
		$class =json_decode(base64_decode($class));
		//debug($_POST);die;
		if(count($_POST) > 0) {
			if(isset($_POST['markup_type']) && $_POST['markup_type'] == "") {
					$data['error']['markup-type-error'] = "Please select valid markup type";
			}
		
			if(isset($_POST['product_id']) && $_POST['product_id'] == "") {
				$data['error']['markup-type-error'] = "Please select valid agent type";
			}
			
			if(isset($_POST['markup_value_type']) && $_POST['markup_value_type'] == "") {
				$data['error']['markup-value-type-error'] = "Please select valid agent type";
			}
			
			if(isset($_POST['markup_value']) && $_POST['markup_value'] == "") {
				$data['error']['markup-value-error'] = "Please enter the valid amount";
			}
		   
		    if(isset($_POST['markup_value']) && $_POST['markup_value'] != "") {
				  $amount_validation = $this->Validation_Model->amountValidation($_POST['markup_value']);	
				    if(!$amount_validation){
				        $data['error']['markup-value-error'] = "Please enter the correct amount (1000.00)";
			       }
			}

			 if($this->session->userdata('user_type') == 4){
				   $tablename = "b2b2b_agent_markup";
			   }else{
				   $tablename = "agent_markup";
			   }
			
			$user_id = $this->session->userdata('user_details_id');
			$user_type_id = $this->session->userdata('user_type');
			if($_POST['markup_type'] == 'GENERAL') {
				$markup = $this->Settings_Model->get_available_general_markup($_POST['product_id'], $markup_value_type = '', $markupvalue = '', $tablename,$user_id);
				if(count($markup) > 0){
					 $data['error']['markup-already-available'] = 'Entered markup is already available. Please edit it.';
				}
		   }else{      
				$markup = $this->Settings_Model->get_available_specific_markup($_POST['product_id'],  $_POST['markup_type'], $markupvalue = '', $tablename,$user_id,$_POST['user_type_id']); 
				if(count($markup) > 0){
					 $data['error']['markup-already-available'] = 'Entered Specific markup is alrady available. Please edit it.';
				}  
			}   
			
		
			if(isset($data['error']) && count($data['error']) > 0) {
				 $data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			   }
			   
			   if($this->session->userdata('user_type') == 4){
				   $tablename = "b2b2b_agent_markup";
			   }else{
				   $tablename = "agent_markup";
			   }
			//$_POST['user_type_id'] = $user_type_id;
			$this->Settings_Model->addAgentMarkup($_POST, $tablename);
			$data['status'] = 1;
			$data['url'] = base_url().'settings?class='.$class.'';
            print json_encode($data);
		}else{
			$data['status'] = 1;
			$data['url'] = base_url().'settings?class='.$class.'';
            print json_encode($data);
		}
	}
	
	function editMarkup($markup_id1){
		$markup_id = json_decode(base64_decode($markup_id1));
		$dashboard['products'] = $this->Dashboard_Model->getProducts();
		$dashboard['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
		$dashboard['api'] = $this->General_Model->get_api_list();
		if($this->session->userdata('user_type') == 4){
		   $tablename = "b2b2b_agent_markup";
	     }else{
		   $tablename = "agent_markup";
		}
		$dashboard['agent_markups'] = $this->Settings_Model->getAgentMarkups($this->session->userdata('user_details_id'), $markup_id, $tablename);
		$dashboard['updateMarkup'] = $this->load->view('settings/edit_markup', $dashboard, true);
		print json_encode($dashboard);
		return;
	}
	
	function updateMarkup($markup_id1,$class){
		
		
		
		$markup_id = json_decode(base64_decode($markup_id1));
		$class =json_decode(base64_decode($class));
		if(count($_POST) > 0) {
			if(isset($_POST['markup_type']) && $_POST['markup_type'] == "") {
					$data['error']['markup-type-error'] = "Please select valid markup type";
			}
			
			if(isset($_POST['product_id']) && $_POST['product_id'] == "") {
				$data['error']['product-id-error'] = "Please select valid agent type";
			}
			
		
			if(isset($_POST['markup_value_type']) && $_POST['markup_value_type'] == "") {
				$data['error']['markup-value-type-error'] = "Please select valid agent type";
			}
			
			if(isset($_POST['markup_value']) && $_POST['markup_value'] == "") {
				$data['error']['markup-value-error'] = "Please enter the valid amount";
			}
			
		    if(isset($_POST['markup_value']) && $_POST['markup_value'] != "") {
				  $amount_validation = $this->Validation_Model->amountValidation($_POST['markup_value']);	
				    if(!$amount_validation){
				        $data['error']['markup-value-error'] = "Please enter the correct amount (1000.00)";
			       }
			}
			
			  if($this->session->userdata('user_type') == 4){
				   $tablename = "b2b2b_agent_markup";
			   }else{
				   $tablename = "agent_markup";
			   }
			//echo '<pre>'; print_r($_POST); exit();
			if($_POST['markup_type'] == 'GENERAL') {
				$markup = $this->Settings_Model->get_available_general_markup($_POST['product_id'],$_POST['markup_value_type'],$_POST['markup_value'], $tablename , $markup_id);
				//print_r(count($markup)); exit();
				if(count($markup) > 0){
					 $data['error']['markup-already-edit-available'] = 'Entered markup is already available';
				}
		    }else{
				$markup = $this->Settings_Model->get_available_specific_markup($_POST['product_id'],  $_POST['markup_type'], $markupvalue = '', $tablename, $markup_id, $_POST['country_code'], $_POST['start_date'],$_POST['end_date'], $_POST['expiry_date'],$_POST['expiry_date_to']);
				if(count($markup) > 0){
					 $data['error']['markup-already-edit-available'] = 'Entered markup is already available';
				}
			}
		
			if(isset($data['error']) && count($data['error']) > 0) {
				 $data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			   }
		$this->Settings_Model->updateAgentMarkup($_POST, $markup_id, $tablename);
	    $data['status'] = 1;
        $data['url'] = base_url().'settings?class='.$class.'';
        print json_encode($data);
		}else{
	    $data['status'] = 1;
        $data['url'] = base_url().'settings?class='.$class.'';
        print json_encode($data);
		}
	}
	
	
	
	
	function changeMarkupStatus($markup_id1 ,  $status1,$class){
		
		$class =json_decode(base64_decode($class));
		$markup_id =json_decode(base64_decode($markup_id1));
		$status = json_decode(base64_decode($status1));
		//print_r($status); exit();
		if($this->session->userdata('user_type') == 4){
		   $tablename = "b2b2b_agent_markup";
	     }else{
		   $tablename = "agent_markup";
		}
		if($markup_id != ''){
			//$this->Settings_Model->changeMarkupStatus($markup_id, $status);
			$this->Settings_Model->changeMarkupStatus($markup_id,$tablename, $status);
			redirect('settings?class='.$class.'');
		}else{
			redirect('settings');
		}
	}
	
	function deleteMarkup($markup_id1){
		$markup_id =json_decode(base64_decode($markup_id1));
		if($markup_id != ''){
			$this->Settings_Model->deletemarkups($markup_id);
			redirect('settings');
		}else{
			redirect('settings');
		}
	}
}
?>
