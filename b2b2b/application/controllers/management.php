<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
/**
 *
 * @package    Provab - Provab Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com> on 01-06-2015
 * @version    V2
 */

class Management extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model ( 'Custom_Db' );
		$this->load->model('Transaction_Model');
		$this->load->model('domain_management_model'); 
		$this->load->model('commission_model');
		$this->check_login();
		// $this->load->library('provab_page_loader');
	}

	private function check_login()
	{
		if($this->session->userdata('user_logged_in')!=1)
		{
			redirect(base_url()); 
		}  
	}
	
	public function promocode() {
		$all_post=$this->input->post();
		//debug($all_post);exit; 
		$application_default_currency = admin_base_currency();
		$currency_obj = new Currency ( array ('module_type' => 'flight','from' => admin_base_currency (),'to' => $all_post['currency']));
		 
		$condition['promo_code'] = $all_post['promocode'];
		$condition['status'] = 1;
		$promo_code_res=$this->Custom_Db->single_table_records('promo_code_list', '*', $condition );
		if($promo_code_res['status']==1)
		{ 
			$promo_code=$promo_code_res['data'][0];

			if(md5($promo_code['module'])!=$all_post['moduletype'])
			{
				$result['status']=0;
				$result['error_msg']='Invalid Promo Code';
			}elseif($promo_code['expiry_date']<=date('Y-m-d')){
				
				$result['status']=0;
				$result['error_msg']='Promo Code Expired';
			}
			
			else{
				// echo $this->entity_user_id; exit();
				if($promo_code['module']=='car')
				{
					$booking_table = 'Car_booking_details';
					$booking_user_table = 'Car_user_details';
				}elseif($promo_code['module']=='hotel')
				{
					$booking_table = 'hotel_booking_details';
				}elseif($promo_code['module']=='flight')
				{  	   
					$booking_table = 'flight_booking_details';
				}
				###################################################################################
				if(is_logged_in_user()){
					$this->entity_user_id=1; 
					$query = "SELECT BD.origin FROM payment_gateway_details AS PGD RIGHT JOIN ".$booking_table." AS BD ON PGD.app_reference = BD.app_reference WHERE BD.created_by_id='".$this->entity_user_id."' "; 
				}else{
					$email = $all_post['email'];
					if($promo_code['module']!='car')
					{
						$query = "SELECT BD.origin FROM payment_gateway_details AS PGD RIGHT JOIN ".$booking_table." AS BD ON PGD.app_reference = BD.app_reference WHERE BD.email='".$email."' ";
					}else{
						$query = "SELECT BD.origin FROM payment_gateway_details AS PGD RIGHT JOIN ".$booking_table." AS BD ON PGD.app_reference = BD.app_reference RIGHT JOIN ".$booking_user_table." AS BUD ON BD.app_reference=BUD.app_reference  WHERE BUD.email='".$email."' ";
					}
				}
				###################################################################################
				//debug($query); exit;
				$user_promocode_check=$this->Custom_Db->get_result_by_query($query);
				$user_promocode_check = 0;
				if($promo_code['airline_code']!='')
				{
				$airlinecodes=$this->Transaction_Model->airline_codes($promo_code['airline_code']);
		  
				if(!in_array($all_post['airline_code'], array_column($airlinecodes, 'code')))
				{ 
				$result['status']=0;
				$result['error_msg']='Flight Not Covers Promo Code';
				} 
				else
				{
					if($user_promocode_check > 0){ 

					$result['status']=0;
					$result['error_msg']='Already used';
				}else{
					if($promo_code['value_type']=='percentage'){
						$result['value']=($all_post['total_amount_val']*round($promo_code['value']))/100;
						$result['value'] = number_format($result['value'],2);
						$result['actual_value']= number_format($result['value'],2);
					}else
					{
						$result['value']= $promo_code['value'];
						$result['actual_value']= number_format($promo_code['value'],2);
						$result['value'] = get_converted_currency_value($currency_obj->force_currency_conversion($result['value']));
						$result['value'] = number_format($result['value'],2);
					}					
					
					$total_amount_val=($all_post['total_amount_val']+$all_post['convenience_fee'])-$result['value'];
					$total_amount_val=($total_amount_val>0)? $total_amount_val: 0;
					$result['total_amount_val'] = number_format($total_amount_val, 2);
					// $result['value'] = sprintf("%.2f", ceil($result['value']));
					//$result['value'] = round($result['value']).'.00';
					$result['total_amount_data'] = $all_post['currency_symbol']." ".number_format($total_amount_val, 2);
					$result['convenience_fee']=$all_post['convenience_fee'];
					$result['promocode']=$all_post['promocode'];	
					$result['discount_value']= $all_post['currency_symbol']." ".number_format($result['value'],2);
					$result['module']=$all_post['moduletype'];
					$result['status']=1;
				}
				}
				}
				else
				{
					if($user_promocode_check > 0){ 

					$result['status']=0;
					$result['error_msg']='Already used';
				}else{
					if($promo_code['value_type']=='percentage'){
						$result['value']=($all_post['total_amount_val']*round($promo_code['value']))/100;
						$result['value'] = number_format($result['value'],2);
						$result['actual_value']= number_format($result['value'],2);
					}else
					{
						$result['value']= $promo_code['value'];
						$result['actual_value']= number_format($promo_code['value'],2);
						$result['value'] = get_converted_currency_value($currency_obj->force_currency_conversion($result['value']));
						$result['value'] = number_format($result['value'],2);
					}					
					
					$total_amount_val=($all_post['total_amount_val']+$all_post['convenience_fee'])-$result['value'];
					$total_amount_val=($total_amount_val>0)? $total_amount_val: 0;
					$result['total_amount_val'] = number_format($total_amount_val, 2);
					// $result['value'] = sprintf("%.2f", ceil($result['value']));
					//$result['value'] = round($result['value']).'.00';
					$result['total_amount_data'] = $all_post['currency_symbol']." ".number_format($total_amount_val, 2);
					$result['convenience_fee']=$all_post['convenience_fee'];
					$result['promocode']=$all_post['promocode'];	
					$result['discount_value']= $all_post['currency_symbol']." ".number_format($result['value'],2);
					$result['module']=$all_post['moduletype'];
					$result['status']=1;
				}
				}
				

			}
		}
		else{
			$result['status']=$promo_code_res['status'];
			$result['error_msg']='Invalid Promo Code';
		}
		echo json_encode($result);
	}

	/**
	 * Jaganath
	 * Update B2B Agent Commission
	 */
	function agent_commission($offset=0)
	{
		$get_data = $this->input->get();
		$post_data = $this->input->post();
		$page_data = array();
		
		if(isset($get_data['agent_ref_id']) == true && empty($get_data['agent_ref_id']) == false && valid_array($post_data) == false) {
			//Get Data
			$agent_ref_id = base64_decode(trim($get_data['agent_ref_id']));
			$page_data['agent_ref_id'] = $agent_ref_id;
			$agent_commission_details = $this->commission_model->get_commission_details($agent_ref_id);
			if($agent_commission_details['status'] == true) {
				$page_data['commission_details'] = $agent_commission_details['data'];
			} else {
				//Invalid CRUD
				redirect('security/log_event?event=InvalidAgent');
			}
		} else if(valid_array($post_data) == true && isset($post_data['module']) == true && empty($post_data['module']) == false) {
			// debug($post_data);
			// exit;
			foreach($post_data['module'] as $module_k => $module_v) {
				$module = trim($module_v);
				$module = trim($module_v);
				switch ($module) {
					case META_AIRLINE_COURSE://Airline Commission
						$update_flight_commission_data['module'] = $post_data['module'][$module_k];
						$update_flight_commission_data['agent_ref_id'] = $post_data['agent_ref_id'][$module_k];
						$update_flight_commission_data['flight_commission_origin'] = $post_data['commission_origin'][$module_k];
						$update_flight_commission_data['flight_commission'] = $post_data['commission'][$module_k];
						$update_flight_commission_data['api_value'] = $post_data['api_value'][$module_k];
						$this->update_b2b_flight_commission($update_flight_commission_data);
						break;
					case META_BUS_COURSE://Bus Commission
						$update_bus_commission_data['module'] = $post_data['module'][$module_k];
						$update_bus_commission_data['agent_ref_id'] = $post_data['agent_ref_id'][$module_k];
						$update_bus_commission_data['bus_commission_origin'] = $post_data['commission_origin'][$module_k];
						$update_bus_commission_data['bus_commission'] = $post_data['commission'][$module_k];
						$update_bus_commission_data['api_value'] = $post_data['api_value'][$module_k];
						$this->update_b2b_bus_commission($update_bus_commission_data);
						break;
					case META_SIGHTSEEING_COURSE://Sightseeing Commission

						$update_sightseeing_commission_data['module'] = $post_data['module'][$module_k];
						$update_sightseeing_commission_data['agent_ref_id'] = $post_data['agent_ref_id'][$module_k];
						$update_sightseeing_commission_data['sightseeing_commission_origin'] = $post_data['commission_origin'][$module_k];
						$update_sightseeing_commission_data['sightseeing_commission'] = $post_data['commission'][$module_k];
						$update_sightseeing_commission_data['api_value'] = $post_data['api_value'][$module_k];
						$this->update_b2b_sightseeing_commission($update_sightseeing_commission_data);
						break;
					case META_TRANSFERV1_COURSE://Sightseeing Commission

						$update_transfer_commission_data['module'] = $post_data['module'][$module_k];
						$update_transfer_commission_data['agent_ref_id'] = $post_data['agent_ref_id'][$module_k];
						$update_transfer_commission_data['transfer_commission_origin'] = $post_data['commission_origin'][$module_k];
						$update_transfer_commission_data['transfer_commission'] = $post_data['commission'][$module_k];
						$update_transfer_commission_data['api_value'] = $post_data['api_value'][$module_k];
						$this->update_b2b_transfer_commission($update_transfer_commission_data);
						break;

				}
			}
			//set_update_message();
			if(empty($_SERVER['QUERY_STRING']) == false) {
				$query_string = '?'.$_SERVER['QUERY_STRING'];
			} else {
				$query_string = '';
			}
			redirect('management/agent_commission'); //.$query_string
		}
		if(isset($get_data['default_commission']) == true && $get_data['default_commission'] == ACTIVE) {
			//Default Commission
			$page_data['default_commission'] = ACTIVE;
			$commission_details = $this->commission_model->default_commission_details();
			//Default Commission Details
			$page_data['commission_details'] = $commission_details['data'];
		} else {
			
			//Agent's List
			if(isset($get_data['filter']) == true && $get_data['filter'] == 'search_agent' &&
			isset($get_data['filter_agency']) == true && empty($get_data['filter_agency']) == false) {
				$filter_agency = trim($get_data['filter_agency']);
				//Search Filter
				$search_filter_condition = '(U.uuid like "%'.$filter_agency.'%" OR U.agency_name like "%'.$filter_agency.'%" OR U.first_name like "%'.$filter_agency.'%" OR U.last_name like "%'.$filter_agency.'%" OR U.email like "%'.$filter_agency.'%" OR U.phone like "%'.$filter_agency.'%")';
				$total_records = $this->commission_model->filter_agent_commission_details($search_filter_condition, true);
				$agent_list = $this->commission_model->filter_agent_commission_details($search_filter_condition, false, $offset, RECORDS_RANGE_1);
			} else {
				/** TABLE PAGINATION */
				$condition[] = array('U.user_type_id', ' IN', '('.B2B2B_USER.')');
				//princess added (agent list only active)
				$condition[]= array('U.user_status', 'IN', "('ACTIVE')");
				//$page_data['agent_list'] = $this->user_model->get_domain_user_list($condition, false, $offset, RECORDS_RANGE_1);
	
				$total_records = $this->commission_model->agent_commission_details($condition, true);
				$agent_list = $this->commission_model->agent_commission_details($condition, false, $offset, RECORDS_RANGE_1);


			}
			$page_data['agent_list'] = $agent_list['data']['agent_commission_details'] ;
			$this->load->library('pagination');
			if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
			$config['base_url'] = base_url().'index.php/management/agent_commission/';
			$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
			$config['total_rows'] = $total_records->total;
			$config['per_page'] = RECORDS_RANGE_1;
			$this->pagination->initialize($config);
			/** TABLE PAGINATION */
		}
		/****************Admin Commission**********************/
		
		$admin_id = $this->session->userdata('user_details_id');

		$admin_commission_details = $this->commission_model->get_admin_commission_details($admin_id);

		


		if($admin_commission_details['commission_status'] == true){
			$admin_bus_commission['value'] = $admin_commission_details['data']['bus_commission_details']['value'];
			$admin_bus_commission['api_value'] = $admin_commission_details['data']['bus_commission_details']['api_value'];

			$admin_flight_commission['value'] = $admin_commission_details['data']['flight_commission_details']['value'];
			$admin_flight_commission['api_value'] = $admin_commission_details['data']['flight_commission_details']['api_value'];

			$admin_sightseeing_commission['value'] = $admin_commission_details['data']['sightseeing_commission_details']['value'];
			$admin_sightseeing_commission['api_value'] = $admin_commission_details['data']['sightseeing_commission_details']['api_value'];

			$admin_transfer_commission['value'] = $admin_commission_details['data']['transfer_commission_details']['value'];
			$admin_transfer_commission['api_value'] = $admin_commission_details['data']['transfer_commission_details']['api_value'];
		}else{
			$admin_bus_commission['value'] = 0 ;
			$admin_bus_commission['api_value'] = 0;

			$admin_flight_commission['value'] = 0;
			$admin_flight_commission['api_value'] = 0;

			$admin_sightseeing_commission['value'] = 0;
			$admin_sightseeing_commission['api_value'] = 0;

			$admin_transfer_commission['value'] = 0;
			$admin_transfer_commission['api_value'] = 0;
		}


		$page_data['admin_bus_commission'] = $admin_bus_commission;
		$page_data['admin_flight_commission'] = $admin_flight_commission;
		$page_data['admin_sightseeing_commission'] = $admin_sightseeing_commission;
		$page_data['admin_transfer_commission'] = $admin_transfer_commission;
		
		// debug($page_data['super_admin_flight_commission']);die;
		// exit;
		/****************Super Admin Commission**********************/
		// debug($page_data);
		// exit;
		$this->load->view('management/agent_commission', $page_data);
	}

	/**
	 * Jaganath
	 * Update Flight Commission Details
	 * @param $commission_details
	 */
	function update_b2b_flight_commission($commission_details)
	{
		if(isset($commission_details['module']) == true && empty($commission_details['module']) == false &&
		isset($commission_details['agent_ref_id']) == true && empty($commission_details['agent_ref_id']) == false &&
		isset($commission_details['flight_commission_origin']) == true && isset($commission_details['flight_commission']) == true) {
			$origin = trim($commission_details['flight_commission_origin']);
			$agent_ref_id = base64_decode(trim($commission_details['agent_ref_id']));
			$commission_value = floatval(trim($commission_details['flight_commission']));
			$api_value = floatval(trim($commission_details['api_value']));
			$b2b_flight_commission_details = array();
			if(intval($agent_ref_id) > 0) {
				$b2b_flight_commission_details['type'] = COMMISSION_SPECIFIC;
			} else {
				$b2b_flight_commission_details['type'] = COMMISSION_GENERIC;
			}
			$b2b_flight_commission_details['value'] = $commission_value;
			$b2b_flight_commission_details['api_value'] = $api_value;
			$b2b_flight_commission_details['value_type'] = COMMISSION_VALUE_PERCENTAGE;
			$b2b_flight_commission_details['commission_currency'] = MARKUP_CURRENCY;
			$b2b_flight_commission_details['created_by_id'] = $this->session->userdata('user_details_id');
			$b2b_flight_commission_details['created_datetime'] = date('Y-m-d H:i:s');
			if($origin >0) {
				//UPDATE
				if(intval($agent_ref_id) > 0) {//COMMISSION_SPECIFIC Agent Commission
					$update_condition['agent_fk'] = $agent_ref_id;
				} else {//Default Commission
					$update_condition['type'] = COMMISSION_GENERIC;
				}
				$this->custom_db->update_record('b2b2b_flight_commission_details', $b2b_flight_commission_details, $update_condition);
			} else {
				//ADD
				$b2b_flight_commission_details['agent_fk'] = $agent_ref_id;
				$b2b_flight_commission_details['domain_list_fk'] = $this->session->userdata('domain_list_fk');
				if(intval($agent_ref_id) > 0) {//COMMISSION_SPECIFIC Agent Commission
					$delete_condition['agent_fk'] = $agent_ref_id;
				} else {//Default Commission
					$delete_condition['type'] = COMMISSION_GENERIC;
				}
				$this->custom_db->delete_record('b2b2b_flight_commission_details', $delete_condition);
				$this->custom_db->insert_record('b2b2b_flight_commission_details', $b2b_flight_commission_details);
			}
		} else {
			redirect('security/log_event?event=InvalidFlightCommissionDetails');
		}
	}
	/**
	 * Jaganath
	 * Update Bus Commission Details
	 * @param $commission_details
	 */
	function update_b2b_bus_commission($commission_details)
	{
		if(isset($commission_details['module']) == true && empty($commission_details['module']) == false &&
		isset($commission_details['agent_ref_id']) == true && empty($commission_details['agent_ref_id']) == false &&
		isset($commission_details['bus_commission_origin']) == true && isset($commission_details['bus_commission']) == true) {
			$origin = trim($commission_details['bus_commission_origin']);
			$agent_ref_id = base64_decode(trim($commission_details['agent_ref_id']));
			$commission_value = floatval(trim($commission_details['bus_commission']));
			$api_value = floatval(trim($commission_details['api_value']));
			$b2b_bus_commission_details = array();
			if(intval($agent_ref_id) > 0) {
				$b2b_bus_commission_details['type'] = COMMISSION_SPECIFIC;
			} else {
				$b2b_bus_commission_details['type'] = COMMISSION_GENERIC;
			}
			$b2b_bus_commission_details['value'] = $commission_value;
			$b2b_bus_commission_details['api_value'] = $api_value;
			$b2b_bus_commission_details['value_type'] = COMMISSION_VALUE_PERCENTAGE;
			$b2b_bus_commission_details['commission_currency'] = MARKUP_CURRENCY;
			$b2b_bus_commission_details['created_by_id'] = $this->session->userdata('user_details_id');
			$b2b_bus_commission_details['created_datetime'] = date('Y-m-d H:i:s');
			if($origin >0) {
				//UPDATE
				if(intval($agent_ref_id) > 0) {//COMMISSION_SPECIFIC Agent Commission
					$update_condition['agent_fk'] = $agent_ref_id;
				} else {//Default Commission
					$update_condition['type'] = COMMISSION_GENERIC;
				}
				$this->custom_db->update_record('b2b2b_bus_commission_details', $b2b_bus_commission_details, $update_condition);
			} else {
				//ADD
				$b2b_bus_commission_details['agent_fk'] = $agent_ref_id;
				$b2b_bus_commission_details['domain_list_fk'] = $this->session->userdata('domain_list_fk');
				if(intval($agent_ref_id) > 0) {//COMMISSION_SPECIFIC Agent Commission
					$delete_condition['agent_fk'] = $agent_ref_id;
				} else {//Default Commission
					$delete_condition['type'] = COMMISSION_GENERIC;
				}
				$this->custom_db->delete_record('b2b2b_bus_commission_details', $delete_condition);
				$this->custom_db->insert_record('b2b2b_bus_commission_details', $b2b_bus_commission_details);
			}
		} else {
			redirect('security/log_event?event=InvalidBusCommissionDetails');
		}
	}
	/**
	 * Elavarasi
	 * Update Sightseeing Commission Details
	 * @param $commission_details
	 */
	function update_b2b_sightseeing_commission($commission_details)
	{
		if(isset($commission_details['module']) == true && empty($commission_details['module']) == false &&
		isset($commission_details['agent_ref_id']) == true && empty($commission_details['agent_ref_id']) == false &&
		isset($commission_details['sightseeing_commission_origin']) == true && isset($commission_details['sightseeing_commission']) == true) {
			$origin = trim($commission_details['sightseeing_commission_origin']);
			$agent_ref_id = base64_decode(trim($commission_details['agent_ref_id']));
			$commission_value = floatval(trim($commission_details['sightseeing_commission']));
			$api_value = floatval(trim($commission_details['api_value']));
			$b2b_sightseeing_commission_details = array();
			if(intval($agent_ref_id) > 0) {
				$b2b_sightseeing_commission_details['type'] = COMMISSION_SPECIFIC;
			} else {
				$b2b_sightseeing_commission_details['type'] = COMMISSION_GENERIC;
			}
			$b2b_sightseeing_commission_details['value'] = $commission_value;
			$b2b_sightseeing_commission_details['api_value'] = $api_value;
			$b2b_sightseeing_commission_details['value_type'] = COMMISSION_VALUE_PERCENTAGE;
			$b2b_sightseeing_commission_details['commission_currency'] = MARKUP_CURRENCY;
			$b2b_sightseeing_commission_details['created_by_id'] = $this->session->userdata('user_details_id');
			$b2b_sightseeing_commission_details['created_datetime'] = date('Y-m-d H:i:s');
			if($origin >0) {
				//UPDATE
				if(intval($agent_ref_id) > 0) {//COMMISSION_SPECIFIC Agent Commission
					$update_condition['agent_fk'] = $agent_ref_id;
				} else {//Default Commission
					$update_condition['type'] = COMMISSION_GENERIC;
				}
				$this->custom_db->update_record('b2b2b_sightseeing_commission_details', $b2b_sightseeing_commission_details, $update_condition);
			} else {
				//ADD
				$b2b_sightseeing_commission_details['agent_fk'] = $agent_ref_id;
				$b2b_sightseeing_commission_details['domain_list_fk'] = $this->session->userdata('domain_list_fk');
				if(intval($agent_ref_id) > 0) {//COMMISSION_SPECIFIC Agent Commission
					$delete_condition['agent_fk'] = $agent_ref_id;
				} else {//Default Commission
					$delete_condition['type'] = COMMISSION_GENERIC;
				}
				$this->custom_db->delete_record('b2b2b_sightseeing_commission_details', $delete_condition);
				$this->custom_db->insert_record('b2b2b_sightseeing_commission_details', $b2b_sightseeing_commission_details);
			}
		} else {
			redirect('security/log_event?event=InvalidBusCommissionDetails');
		}
	}
	/**
	 * Elavarasi
	 * Update Transfer Commission Details
	 * @param $commission_details
	 */
	function update_b2b_transfer_commission($commission_details)
	{
		if(isset($commission_details['module']) == true && empty($commission_details['module']) == false &&
		isset($commission_details['agent_ref_id']) == true && empty($commission_details['agent_ref_id']) == false &&
		isset($commission_details['transfer_commission_origin']) == true && isset($commission_details['transfer_commission']) == true) {
			$origin = trim($commission_details['transfer_commission_origin']);
			$agent_ref_id = base64_decode(trim($commission_details['agent_ref_id']));
			$commission_value = floatval(trim($commission_details['transfer_commission']));
			$api_value = floatval(trim($commission_details['api_value']));
			$b2b_transfer_commission_details = array();
			if(intval($agent_ref_id) > 0) {
				$b2b_transfer_commission_details['type'] = COMMISSION_SPECIFIC;
			} else {
				$b2b_transfer_commission_details['type'] = COMMISSION_GENERIC;
			}
			$b2b_transfer_commission_details['value'] = $commission_value;
			$b2b_transfer_commission_details['api_value'] = $api_value;
			$b2b_transfer_commission_details['value_type'] = COMMISSION_VALUE_PERCENTAGE;
			$b2b_transfer_commission_details['commission_currency'] = MARKUP_CURRENCY;
			$b2b_transfer_commission_details['created_by_id'] = $this->session->userdata('user_details_id');
			$b2b_transfer_commission_details['created_datetime'] = date('Y-m-d H:i:s');
			if($origin >0) {
				//UPDATE
				if(intval($agent_ref_id) > 0) {//COMMISSION_SPECIFIC Agent Commission
					$update_condition['agent_fk'] = $agent_ref_id;
				} else {//Default Commission
					$update_condition['type'] = COMMISSION_GENERIC;
				}
				$this->custom_db->update_record('b2b2b_transfer_commission_details', $b2b_transfer_commission_details, $update_condition);
			} else {
				//ADD
				$b2b_transfer_commission_details['agent_fk'] = $agent_ref_id;
				$b2b_transfer_commission_details['domain_list_fk'] = $this->session->userdata('domain_list_fk');
				if(intval($agent_ref_id) > 0) {//COMMISSION_SPECIFIC Agent Commission
					$delete_condition['agent_fk'] = $agent_ref_id;
				} else {//Default Commission
					$delete_condition['type'] = COMMISSION_GENERIC;
				}
				$this->custom_db->delete_record('b2b2b_transfer_commission_details', $delete_condition);
				$this->custom_db->insert_record('b2b2b_transfer_commission_details', $b2b_transfer_commission_details);
			}
		} else {
			redirect('security/log_event?event=InvalidBusCommissionDetails');
		}
	}
	/**
	 * Jaganath
	 * Manages Bank Account Details
	 */
	function bank_account_details()
	{ 
		//error_reporting(E_ALL);
		$post_data['form_data'] = $this->input->post();

		$get_data = $this->input->get();
		$page_data['form_data'] = '';
		if(valid_array($post_data['form_data']) == false && isset($get_data['eid']) && intval($get_data['eid'])>0) {
			$temp_data=$this->custom_db->single_table_records('bank_account_details', '*', array('origin' => $get_data['eid']));
			$page_data['form_data']['origin']=$temp_data['data'][0]['origin'];
			$page_data['form_data']['en_account_name']=$temp_data['data'][0]['en_account_name'];
			$page_data['form_data']['en_bank_name']=$temp_data['data'][0]['en_bank_name'];
			$page_data['form_data']['en_branch_name']=$temp_data['data'][0]['en_branch_name'];
			$page_data['form_data']['bank_icon']=$temp_data['data'][0]['bank_icon'];
			$page_data['form_data']['account_number']=$temp_data['data'][0]['account_number'];
			$page_data['form_data']['ifsc_code']=$temp_data['data'][0]['ifsc_code'];
			$page_data['form_data']['pan_number']=$temp_data['data'][0]['pan_number'];
			$page_data['form_data']['status']=$temp_data['data'][0]['status'];
		} else if( valid_array($post_data['form_data']) ) {
			// debug($post_data['form_data']);
			// debug($this->session->userdata);

			// $this->current_page->set_auto_validator();
			// if ($this->form_validation->run()) {
				// exit("1");
				$origin = intval($post_data['form_data']['origin']);
				unset($post_data['form_data']['FID']);
				unset($post_data['form_data']['origin']);
				if($origin > 0) {
					/** UPDATE **/
					$post_data['form_data']['updated_by_id'] = $this->entity_user_id;
					$post_data['form_data']['updated_datetime'] = date('Y-m-d H:i:s');
					$this->custom_db->update_record('bank_account_details', $post_data['form_data'],array('origin' => $origin) );
					set_update_message();
				} elseif($origin == 0){
					/** INSERT **/
					$post_data['form_data']['domain_list_fk'] = $this->session->userdata('domain_list_fk');
					$post_data['form_data']['created_by_id'] = $this->session->userdata('user_details_id');
					$post_data['form_data']['created_datetime'] = date('Y-m-d H:i:s');
					// debug($post_data);
					$insert_id=$this->custom_db->insert_record('bank_account_details',$post_data['form_data']);
					set_insert_message();
				}
				// debug($_FILES);
				// exit;
				//FILE UPLOAD
				if (valid_array($_FILES) == true and $_FILES['bank_icon']['error'] == 0 and $_FILES['bank_icon']['size'] > 0) {
					$config['upload_path'] = '../../b2b2b/extras/custom/'.CURRENT_DOMAIN_KEY.'/images/bank_logo/';
					$config['allowed_types'] = '*';
					$config['file_name'] = time();
					$config['max_size'] = '1000000';
					$config['max_width']  = '';
					$config['max_height']  = '';
					$config['remove_spaces']  = false;
					if (empty($insert_id) == true) {
						//UPDATE
						$temp_record = $this->custom_db->single_table_records('bank_account_details', 'bank_icon', array('origin' => $origin));
						$icon = $temp_record['data'][0]['bank_icon'];
						//DELETE OLD FILES
						if (empty($icon) == false) {
							if (file_exists($config['upload_path'].$icon)) {
								unlink($config['upload_path'].$icon);
							}
						}
					} else {
						$origin = $insert_id['insert_id'];
					}
					//UPLOAD IMAGE
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('bank_icon')) {
						//echo $this->upload->display_errors();
					} else {
						$image_data =  $this->upload->data();
					}
					$this->custom_db->update_record('bank_account_details', array('bank_icon' => $image_data['file_name']), array('origin' => $origin));
				}
				redirect('management/bank_account_details');
			// }
			// exit("2");
		} else {
			$page_data['form_data']['origin']=0;
		}
		/** Table Data **/
		$temp_data=$this->commission_model->bank_account_details();
		if($temp_data['status']) {
			$page_data['table_data'] = $temp_data['data'];
		} else {
			$page_data['table_data'] = "";
		}
		// debug($page_data);
		// exit;

		$this->load->view('management/bank_account_details',$page_data);
	}

}