<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @package    Provab
 * @subpackage Accounts log
 * @author     Pawan Bagga<pawan.provabmail@gmail.com>
 */

class Accounts_log extends CI_Controller { 
	public function __construct()
	{
		parent::__construct();
		$this->load->model('transaction');
		$this->load->library('session');
		$this->user_id=$this->session->userdata('user_details_id');
	}

	/**
	 * index page of application will be loaded here
	 */
	function index()
	{
		$accounts_logs['data']=$this->transaction->accounts_logs($this->user_id); 
		$this->load->view('reports/accounts_log',$accounts_logs); 
	}

	}


	?> 