<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
error_reporting(0);
class Booking extends CI_Controller {
	function __construct(){
		parent::__construct();
		
		$this->load->model('Validation_Model');
		$this->load->model('cart_model');
		$this->load->model('booking_model');
		$this->load->model('payment_model');
		$this->load->model('Account_Model');
		$this->load->model('General_Model');
		$this->load->model('Booking_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Hotel_Model');
		$this->load->model('Transfer_Model');
		$this->load->model('XML_Model');
		$this->load->model('Email_Model');
		$this->load->helper('file');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	}

	public function index($session_id=''){	
	
        if(!empty($session_id)){
		
             $count = $this->cart_model->getBookingTemp($session_id)->num_rows();
			 $data['session_id_c'] = $session_id;
			  if($count > 0){
                $data['countries'] = $this->General_Model->getCountryList();
                $data['book_temp_data'] = $book_temp_data = $this->cart_model->getBookingTemp($session_id)->result();
                foreach ($book_temp_data as $key => $value) {
                    $cart_global_id[] = $value->product_name.','.$value->cart_id;
                }
              	$data['cart_global'] = $cart_global_id;
                $data['cart_global_id'] = base64_encode(json_encode($cart_global_id));
                $user_details_id = $this->session->userdata('user_details_id');
                if($this->session->userdata('user_details_id')){
                    $data['user_type'] = $user_type = $this->session->userdata('user_type');
                    $data['user_details_id'] = $user_id = $this->session->userdata('user_details_id');
					$data['userInfo'] =$this->General_Model->get_user_details($user_details_id, $user_type);
					$data['recent_billing'] =$this->booking_model->get_recent_billing_details($user_details_id, $user_type);
                    $data12['s_session_id'] = $session_id;
					$data12['user_id'] = $user_details_id;
 					$data12['user_type_id'] = $user_type;
				 	$data['session_id_c_v'] =  base64_encode(json_encode($data12));
					$data['user_logs_status'] ='You are logged in already. Now you can continue the booking.';
                }
				else
				{
					$data12['s_session_id'] = $session_id;
				 	$data['session_id_c_v'] =  base64_encode(json_encode($data12));
					$data['user_logs_status'] ='';
				}
				  
              	$data['payment_data'] = $payment_data = $this->payment_model->getPaymentDetails();
		
              
                $this->load->view('booking/index', $data);
            }else{

                $this->load->view('errors/expiry');
            }
        }else{
            $this->load->view('errors/expiry');
        }
    }
    public function report()
    {
    	$this->load->view('reports/report');
    }
    public function create_guest_account(){

		    if(isset($_POST['mobile']) && $_POST['mobile'] != '')  {	 
		        $contact_validation = $this->Validation_Model->numberWithContact($_POST['mobile']);	
		        if(!$contact_validation){
			         $data['error']['mobile-error'] = "Please enter valid  Number";
		         }
			}
			
			if(isset($_POST['email'])  && $_POST['email'] != '') {	
			    $email_validation = $this->Validation_Model->emailIdFormat($_POST['email']);	
			    if(!$email_validation){
				  $data['error']['g_email-error'] = "Please enter valid Email";
			   }
			}
			if(isset($data['error']) && count($data['error']) > 0) {
				$data['status']['status'] = 3;
				echo json_encode(array_merge($data['error'], $data['status']));
				return ;
			}
			
		   $address  = $this->Account_Model->default_address()->row();	
		   $count    = $this->Hotel_Model->isRegistered_Guest($_POST['email'])->num_rows();
		   if($count == 0){
			 $postData = array(
			'user_name'  =>"GUEST USER",
			'user_email' => $_POST['email'],
			'user_cell_phone' => $_POST['mobile'],
		    'address_details_id' => $address->address_details_id,
		    'user_profile_pic' =>  'user-avatar.jpg',
		    'user_status' => 'ACTIVE',
			'user_type_id'=>6,
		    'user_creation_date_time' => date('Y-m-d H:i:s')
			);
			
			$data =array();
			 //create guser
		       $this->Hotel_Model->create_guest_User($postData);
			   $user_data = $this->Hotel_Model->isRegistered_Guest($_POST['email'],6)->row();
		     
		        $login_data = array(
				'user_details_id' => $user_data->user_details_id,
				'user_name' => $_POST['email'],
				'user_type_id' => 6,
				'user_password' => "AES_ENCRYPT(".$_POST['mobile'].",'".SECURITY_KEY."')"
			    );
		     
		       if($this->Account_Model->createLogin($login_data)){
				$b2c_session =  array(
					'user_id' => $user_data->user_details_id,
					'user_type' =>6,
				);
		     
		    
			    $data['user'] 		= "Guest User";
				$data['user_email'] = $_POST['email'];
				$data['name']   	= $_POST['email'];
			    $data['password']   	= $_POST['mobile'];
				$data['url'] 			= base_url();
				$data['profile_photo']= $user_data->user_profile_pic;
				
				$data['email_template'] = $this->Email_Model->get_email_template('guest_registration')->row();
               
            
				$result=$this->Email_Model->sendmail_guest_reg($data);   
							
				$response = array(
					'status' => '1',
					'success' => 'true',
					'msg' => 'Successfully Created!',
					'rid' => $user_data->user_details_id,
					'fname' =>  $_POST['email'],
					'profile_photo' =>  $user_data->user_profile_pic,
					'success_url' => base_url().'home/login',
				); 
			}
			  	   
		   }
		   else{
			   $response = array(
				'status' => '2',
				'success' => 'false',
				'email-error' => 'That email address is already in use. Please log in or use Another Email.'
			);
		   }
		echo json_encode($response); 
	}
    
    
    function register_guest_user()
	{



		error_reporting(-1);
		    if(isset($_POST['mobile']) && $_POST['mobile'] != '')  {	 
		        $contact_validation = $this->Validation_Model->numberWithContact($_POST['mobile']);	
		        if(!$contact_validation){
			         $data['error']['mobile-error'] = "Please enter valid  Number";
		         }
			}
			
			if(isset($_POST['email'])  && $_POST['email'] != '') {	
			    $email_validation = $this->Validation_Model->emailIdFormat($_POST['email']);	
			    if(!$email_validation){
				  $data['error']['g_email-error'] = "Please enter valid Email";
			   }
			}
			if(isset($data['error']) && count($data['error']) > 0) {
				$data['status']['status'] = 3;
				echo json_encode(array_merge($data['error'], $data['status']));
				return ;
			}
			
		   $address  = $this->Account_Model->default_address()->row();	
		   $count    = $this->Hotel_Model->isRegistered_Guest($_POST['email'])->num_rows();
		   if($count == 0){
			 $postData = array(
			'user_name'  =>"GUEST USER",
			'user_email' => $_POST['email'],
			'user_cell_phone' => $_POST['mobile'],
		    'address_details_id' => $address->address_details_id,
		    'user_profile_pic' =>  'user-avatar.jpg',
		    'user_status' => 'ACTIVE',
			'user_type_id'=>6,
		    'user_creation_date_time' => date('Y-m-d H:i:s')
			);
			
			$data =array();
			 //create guser
		       $this->Hotel_Model->create_guest_User($postData);
			   $user_data = $this->Hotel_Model->isRegistered_Guest($_POST['email'],6)->row();
		     
		        $login_data = array(
				'user_details_id' => $user_data->user_details_id,
				'user_name' => $_POST['email'],
				'user_type_id' => 6,
				'user_password' => "AES_ENCRYPT(".$_POST['mobile'].",'".SECURITY_KEY."')"
			    );
		     
		       if($this->Account_Model->createLogin($login_data)){
				$b2c_session =  array(
					'user_id' => $user_data->user_details_id,
					'user_type' =>6,
				);
		     
		    
			    $data['user'] 		= "Guest User";
				$data['user_email'] = $_POST['email'];
				$data['name']   	= $_POST['email'];
			    $data['password']   	= $_POST['mobile'];
				$data['url'] 			= base_url();
				$data['profile_photo']= $user_data->user_profile_pic;
				
				$data['email_template'] = $this->Email_Model->get_email_template('guest_registration')->row();
               
            
				$result=$this->Email_Model->sendmail_guest_reg($data);   
							
				$response = array(
					'status' => '1',
					'success' => 'true',
					'msg' => 'Successfully Created!',
					'rid' => $user_data->user_details_id,
					'fname' =>  $_POST['email'],
					'profile_photo' =>  $user_data->user_profile_pic,
					'success_url' => base_url().'home/login',
				); 
			}
			  	   
		   }
		   else{
			   $response = array(
				'status' => '2',
				'success' => 'false',
				'email-error' => 'That email address is already in use. Please log in or use Another Email.'
			);
		   }
		echo json_encode(array('status' => $response['status'], 'data' => $response)); 
		exit;

		/*$this->load->model('user_model');
		$post_data = $this->input->post();
		$status = false;
		$data = '';
		if (empty($post_data['username']) == false && empty($post_data['mobile_number']) == false) {
			
			$user_name = trim($post_data['username']);
			$mobile_number = trim($post_data['mobile_number']);
			
			$user_exists = $this->username_check($user_name);
			$status = true;
			
			if($user_exists == false) {//Check User Exists based on Username
				$data = 'User Exists';
			} else {//If not exists add the guest user details
				$password = 'test';
				$first_name = 'user';
				$creation_source = 'guest';
				$creation = $this->user_model->create_user($user_name, $password, $first_name, $mobile_number, $creation_source);
				$data = 'Added guest User';
			}
		}
		header('content-type:application/json');
		echo json_encode(array('status' => $status, 'data' => $data));
		exit;*/
	}

	function flightOrders() 
	{
		$user_details_id = $this->session->userdata('user_details_id');
		$branch_id = $this->session->userdata('branch_id');
		$domain_id = $this->session->userdata('domain_id');
		$user_type = $this->session->userdata('user_type');
		$condition = array();
		// debug($branch_id);die;
		$filter_data = $this->format_basic_search_filters();
		if(empty($filter_data)){
		}else{
			$page_data['from_date'] = $filter_data['from_date'];
			$page_data['to_date'] = $filter_data['to_date'];

			$condition = $filter_data['filter_condition'];
		}
		if($user_type==2)
		{
		$data['orders'] = $this->Booking_Model->get_booking_flight_details($user_details_id,$branch_id,$domain_id,$condition);	
		}
		else
		{			
		$data['orders'] = $this->Booking_Model->get_booking_flight_details_sub($user_details_id,$branch_id,$domain_id,$condition);
		} 
		$data['orders_sum'] = $this->Booking_Model->get_booking_flight_details_sum($user_details_id,$branch_id,$domain_id,$condition);
		$data['orders_month_sum'] = $this->Booking_Model->get_booking_flight_details_current_month_sum($user_details_id,$branch_id,$domain_id,$condition);
		/*print_r($data['orders_month_sum']);die();*/
		$this->load->view('reports/report', $data);
	}

	function hotelOrders() 
	{
		$user_details_id = $this->session->userdata('user_details_id');
		$branch_id = $this->session->userdata('branch_id');
		$domain_id = $this->session->userdata('domain_id');
		$user_type = $this->session->userdata('user_type');
		$condition = array();
		$filter_data = $this->format_basic_search_filters();
		if(empty($filter_data)){
		}else{
			$page_data['from_date'] = $filter_data['from_date'];
			$page_data['to_date'] = $filter_data['to_date'];

			$condition = $filter_data['filter_condition'];
		}
		if($user_type==2)
		{
		$data['orders'] = $this->Booking_Model->get_booking_hotel_details($user_details_id,$branch_id,$domain_id,$condition);	
		}
		else
		{ 
			
		$data['orders'] = $this->Booking_Model->get_booking_hotel_details_sub($user_details_id,$branch_id,$domain_id,$condition);
		}
		$this->load->view('reports/hotel_reports', $data);			
	}

	function hotelcrsOrders() 
	{
		$user_details_id = $this->session->userdata('user_details_id');
		$branch_id = $this->session->userdata('branch_id');
		$domain_id = $this->session->userdata('domain_id');
		$user_type = $this->session->userdata('user_type');
		$condition = array();
		$filter_data = $this->format_basic_search_filters();
		if(!empty($filter_data)) 
		{
			$page_data['from_date'] = $filter_data['from_date'];
			$page_data['to_date'] = $filter_data['to_date'];
			$condition = $filter_data['filter_condition'];
		}

		if($user_type != 2) {
			$condition[] = array('BG.user_details_id', '=', $user_details_id);
		}

		$data['orders'] = $this->Booking_Model->get_booking_hotel_crs_details($user_details_id,$branch_id,$domain_id,$condition);
		$this->load->view('reports/hotel_crs_reports.php', $data);			
	}

	function villaOrders() 
	{
		$user_details_id = $this->session->userdata('user_details_id');
		$branch_id = $this->session->userdata('branch_id');
		$domain_id = $this->session->userdata('domain_id');
		$user_type = $this->session->userdata('user_type');
		$condition = array();
		$filter_data = $this->format_basic_search_filters();
		if(!empty($filter_data)) 
		{
			$page_data['from_date'] = $filter_data['from_date'];
			$page_data['to_date'] = $filter_data['to_date'];
			$condition = $filter_data['filter_condition'];
		}
		if($user_type != 2) {
			$condition[] = array('BG.user_details_id', '=', $user_details_id);
		}

		$data['orders'] = $this->Booking_Model->get_booking_villa_details($user_details_id,$branch_id,$domain_id,$condition);	
		
		$this->load->view('reports/villa_reports.php', $data);			
	}

	function busOrders() 
	{
		$user_details_id = $this->session->userdata('user_details_id');
		$branch_id = $this->session->userdata('branch_id');
		$domain_id = $this->session->userdata('domain_id');
		$condition = array();
		$filter_data = $this->format_basic_search_filters();
		if(empty($filter_data)){
		}else{
			$page_data['from_date'] = $filter_data['from_date'];
			$page_data['to_date'] = $filter_data['to_date'];

			$condition = $filter_data['filter_condition'];
		}
		$data['orders'] = $this->Booking_Model->get_booking_bus_details($user_details_id,$branch_id,$domain_id,$condition);
		// print_r($data['orders']);die();
		$this->load->view('reports/bus_reports', $data);
	}

	function activityOrders() 
	{
		$user_details_id = $this->session->userdata('user_details_id');
		$branch_id = $this->session->userdata('branch_id');
		$domain_id = $this->session->userdata('domain_id');
		$user_type = $this->session->userdata('user_type');
		$condition = array();

		$filter_data = $this->format_basic_search_filters();
		if($user_type==2)
		{
		$data['orders'] = $this->Booking_Model->get_booking_activity_details($user_details_id,$branch_id,$domain_id,$condition);	
		}
		else
		{			
		$data['orders'] = $this->Booking_Model->get_booking_activity_details_sub($user_details_id,$branch_id,$domain_id,$condition);
		} 
		
		//print_r($data['orders']);die();
		$this->load->view('reports/activity_reports', $data);
	}

	function transferOrders() {
		$user_details_id = $this->session->userdata('user_details_id');
		$branch_id = $this->session->userdata('branch_id');
		$domain_id = $this->session->userdata('domain_id');
		$user_type = $this->session->userdata('user_type');
		$condition = array();

		$filter_data = $this->format_basic_search_filters();
		if($user_type==2)
		{
		$data['orders'] = $this->Booking_Model->get_booking_transfer_details($user_details_id,$branch_id,$domain_id,$condition);	
		}
		else
		{			
		$data['orders'] = $this->Booking_Model->get_booking_transfer_details_sub($user_details_id,$branch_id,$domain_id,$condition);
		} 
		
		//debug($data['orders']);die();
		$this->load->view('reports/transfer_reports', $data);
	}

	private function format_basic_search_filters($module='')
	{
		$get_data = $this->input->get();
		
		if(valid_array($get_data) == true) {

			$user_details_id = $this->session->userdata('user_details_id');
			$branch_id = $this->session->userdata('branch_id');
			$domain_id = $this->session->userdata('domain_id');


			$filter_condition = array();
			//From-Date and To-Date
			$from_date = trim(@$get_data['created_datetime_from']);
			$to_date = trim(@$get_data['created_datetime_to']);
			//Auto swipe date
			if(empty($from_date) == false && empty($to_date) == false)
			{
				$valid_dates = auto_swipe_dates($from_date, $to_date);
				$from_date = $valid_dates['from_date'];
				$to_date = $valid_dates['to_date'];
			}
			if(empty($from_date) == false) {
				$filter_condition[] = array('DATE(BD.created_datetime)', '>=', $this->db->escape(db_current_date($from_date)));
			}
			if(empty($to_date) == false) {
				$filter_condition[] = array('DATE(BD.created_datetime)', '<=', $this->db->escape(db_current_date($to_date)));
			}
	
			/*if (empty($get_data['created_by_id']) == false) {
				$filter_condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}*/
			
			if (empty($get_data['created_by_id']) == false && strtolower($get_data['created_by_id'])!='all') {
				$filter_condition[] = array('BD.created_by_id', '=', $this->db->escape($get_data['created_by_id']));
			}
	
			if (empty($get_data['status']) == false && strtolower($get_data['status']) != 'all') {
				$filter_condition[] = array('BD.status', '=', $this->db->escape($get_data['status']));
			}
		
			/*if (empty($get_data['phone']) == false) {
				$filter_condition[] = array('BD.phone', ' like ', $this->db->escape('%'.$get_data['phone'].'%'));
			}
	
			if (empty($get_data['email']) == false) {
				$filter_condition[] = array('BD.email', ' like ', $this->db->escape('%'.$get_data['email'].'%'));
			}*/
			
			if($module == 'bus'){
					if (empty($get_data['pnr']) == false) {
					$filter_condition[] = array('BD.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
				}
			}else{
				if (empty($get_data['pnr']) == false) {
					$filter_condition[] = array('TD.pnr', ' like ', $this->db->escape('%'.$get_data['pnr'].'%'));
				}
			}
			
	
			if (empty($get_data['app_reference']) == false) {
				$filter_condition[] = array('BD.app_reference', ' like ', $this->db->escape('%'.$get_data['app_reference'].'%'));
			}
			
			$page_data['from_date'] = $from_date;
			$page_data['to_date'] = $to_date;

			//Today's Booking Data
			if(isset($get_data['today_booking_data']) == true && empty($get_data['today_booking_data']) == false) {
				$filter_condition[] = array('DATE(BD.created_datetime)', '=', '"'.date('Y-m-d').'"');
			}
			//Last day Booking Data
			if(isset($get_data['last_day_booking_data']) == true && empty($get_data['last_day_booking_data']) == false) {
				$filter_condition[] = array('DATE(BD.created_datetime)', '=', '"'.trim($get_data['last_day_booking_data']).'"');
			}
			//Previous Booking Data: last 3 days, 7 days, 15 days, 1 month and 3 month
			if(isset($get_data['prev_booking_data']) == true && empty($get_data['prev_booking_data']) == false) {
				$filter_condition[] = array('DATE(BD.created_datetime)', '>=', '"'.trim($get_data['prev_booking_data']).'"');
			}
			/*$user_details_id = $this->session->userdata('user_details_id');
			$branch_id = $this->session->userdata('branch_id');
			$domain_id = $this->session->userdata('domain_id');
			$filter_condition[] = array('flight_booking_details.user_details_id', '=', '"'.trim($user_details_id).'"');
			$filter_condition[] = array('flight_booking_details.branch_id', '=', '"'.trim($branch_id).'"');
			$filter_condition[] = array('flight_booking_details.domain_id', '=', '"'.trim($domain_id).'"');*/

			//$filter_condition[] = array('flight_booking_details.user_details_id' => $user_details_id, 'flight_booking_details.branch_id' => $branch_id, 'flight_booking_details.domain_id' => $domain_id );
			
			return array('filter_condition' => $filter_condition, 'from_date' => $from_date, 'to_date' => $to_date);
		}
	}
 
 	public function username_check($name)
	{
		$this->load->model('custom_db');
		$condition['user_email'] = $name;
		$condition['user_type_id'] = '6';
		$condition['domain_list_fk'] = intval(get_domain_auth_id());
		$data = $this->custom_db->single_table_records('user_details', 'user_details_id', $condition);
		if ($data['status'] == SUCCESS_STATUS) {
			//$this->form_validation->set_message('username_check', $name.' Is Not Available!!!');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	public function generate_parent_pnr($length = 12) {
        $alphabets = range('A','Z');
        $numbers = range('0','9');
        $final_array = array_merge($alphabets,$numbers);
        $id = '';
        while($length--) {
          $key = array_rand($final_array);
          $id .= $final_array[$key];
        }
        return $id;
    }
    
    public function generate_key_string() {
		$tokens = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$segment_chars = 5;
		$num_segments = 4;
		$key_string = '';
 
		for ($i = 0; $i < $num_segments; $i++) {
	 
			$segment = '';
	 
			for ($j = 0; $j < $segment_chars; $j++) {
					$segment .= $tokens[rand(0, 35)];
			}
			$key_string .= $segment;
	 
			if ($i < ($num_segments - 1)) {
					$key_string .= '-';
			}
		}
		return $key_string;
	}
	
	public function b2b_check_payment($payable_amount,$user_id){
    	//~ Checking for multilpe conditions; I. Pay From Wallet, II. Payment Gateway, III. Pay Later, IV. Pay Partially From Wallet and rest by Payment Gateway, etc. 
		//~ I. Pay From Wallet
		$deposit_amount_det = $this->General_Model->getAgentBalance()->row();
    	$credit_amount = $deposit_amount_det->balance_credit;
    	if($payable_amount <= $credit_amount):
			//~ $this->do_deduction($payable_amount,$credit_amount,$user_id);
			return true;
		else:
			return false;
    	endif;
		 
		return true;
    }
	
	public function checkout() {
		$currency = $_SESSION['currency'];
		$exchange_rate = $_SESSION['currency_value'];
		$checkout_form = $this->input->post();
        $payment_logic = $checkout_form['payment_logic'];
		$payment_type = $checkout_form['payment_type'];
		
		$cids = base64_decode($checkout_form['cid']);
        $cids = json_decode($cids);
        
        $user_type = $this->session->userdata('user_type');
		$user_id = $this->session->userdata('user_details_id');
        
        $parent_pnr = $this->generate_parent_pnr();
        $ttl = array();
        
        foreach($cids as $key => $cid) {
			list($module, $cid) = explode(',', $cid);
			if($module == "Hotels") {
			   $cart_details = $this->cart_model->getBookingTemp_hotel($cid);	
			   $checkout_form['product_id'] = 1;
			}
			elseif($module == "Transfer"){
				$cart_details = $this->cart_model->getBookingTemp_transfer($cid);	
				$checkout_form['product_id'] = 19;
			}
			else {
				#Todo - Booking Failed Page
				die();
			}
			$cost_details = $this->prepare_calculation($checkout_form,$cid);
			if(!empty($cost_details) && $cost_details != '') {
				
				//~ Deposit Verification 
			
				$userInfo =$this->General_Model->get_user_details($user_id, $user_type);
				if($payment_type == "DEPOSIT" ) {
					$status = $this->b2b_check_payment(str_replace(',','',$cost_details['prepare_data']['total_cost']), $user_id);
					if($status) {
						
					}
					else {
						if($status == false):
							$data['status'] = -2;
							echo json_encode($data);die;
							# Todo
						endif;
					}
				}
				elseif($payment_type == "BOTH" ) {
					#Checking Deposit amount
					$status = $this->b2b_check_payment(str_replace(',','',$cost_details['prepare_data']['deposit_amount']), $user_id);
					if($status) {
						
					}
					else {
						#Redirecting Payment Gateway
						$both_flag = true;
						$payment_type == "PAYPAL";
						#Todo
					}
				}
				else {
					
				}
				
				//~ End of deposit verification 
				
				//$branch_id = ($userInfo->branch_id == 0)?$user_id:$userInfo->branch_id;
				$branch_id = $this->session->userdata('branch_id');
			
				$TravelerDetails = json_encode($checkout_form); 
				$cart_data_encoded = base64_encode(json_encode($cart_details));
				
				$images = $cart_details->images;
				$imgar = explode(',',$images);
				
				if($checkout_form['product_id'] == 1) {
					
					$booking_hotel = array( 
											'hotel_name' => $cart_details->hotel_name,
											'hotel_code' => $cart_details->hotel_code,
											'option_id' => $cart_details->token_id,
											'room_data' => $cart_details->room_id,
											'checkin' => $cart_details->checkin,
											'checkout' => $cart_details->checkout,
											'room_type' => $cart_details->room_type,
											'inclusion' => $cart_details->inclusion,
											'room_count' => $cart_details->room_count,
											'cancel_policy' => $cart_details->policy_description, 
											'amendment_policy' => $cart_details->amendment_policy,
											'hotel_address' => $cart_details->address,
											'hotel_contact' => $cart_details->hotel_contact,
											'cancellation_data' => $cart_details->cancellation_data,
											'buffer_cancellation_policy' => $cart_details->buffer_cancellation_policy,
											'adult' => $cart_details->adult,
											'child' => $cart_details->child,
											'city' => $cart_details->city,
											'star' => $cart_details->star,
											'image' => $imgar[0],
											'hotel_contact' => $cart_details->hotel_contact,
											'TravelerDetails' => $TravelerDetails,
											'description' => $cart_details->description,
											'longitude' => $cart_details->lon,
											'latitude' => $cart_details->lat,
											'cart_data' => $cart_data_encoded,
											'cid'=>$cid,
									);
					$booking_referal_id = $this->booking_model->insert_booking_hotel($booking_hotel);
					
					//~ Booking Hotel Operation
				}
				elseif($checkout_form['product_id'] == 19) {
					$booking_transfer = array( 
											'api_id' => $cart_details->api_id,
											'item_code' => $cart_details->item_code,
											'travel_start_date' => $cart_details->travel_start_date,
											'travel_end_date' => $cart_details->travel_start_date,
											'search_city_code' => $cart_details->search_city_code,
											'search_city_name' => $cart_details->search_city_name,
											'item_description' => $cart_details->item_description,
											'pickup_code' => $cart_details->pickup_code,
											'pickup_code_description' => $cart_details->pickup_code_description,
											'pickup_city_code' => $cart_details->pickup_city_code, 
											'pickup_city_code_description' => $cart_details->pickup_city_code_description,
											'dropoff_code' => $cart_details->dropoff_code,
											'dropoff_code_description' => $cart_details->dropoff_code_description,
											'dropoff_city_code' => $cart_details->dropoff_city_code,
											'dropoff_city_code_description' => $cart_details->dropoff_city_code_description,
											'approximate_time' => $cart_details->approximate_time,
											'approximate_transfer_time' => $cart_details->approximate_transfer_time,
											'vehicle_code' => $cart_details->vehicle_code,
											'maximum_luggage' =>  $cart_details->maximum_luggage,
											'maximum_passengers' => $cart_details->maximum_passengers,
											'no_of_passengers' =>  $cart_details->no_of_passengers,
											'no_of_vehicles' => $cart_details->no_of_vehicles,
											'vehicle_details' =>  $cart_details->vehicle_details,
											'api_currecny' => $cart_details->api_currecny,
											'net_rate' => $cart_details->net_rate,
											'caneclation_policy' => $cart_details->caneclation_policy,
											'transfer_request' => $cart_details->transfer_request,
											'TravelerDetails' => $TravelerDetails,
											'cid'=>$cid,
									);
				
					 $booking_referal_id = $this->booking_model->insert_booking_transfer($booking_transfer);
				}
				else {
					#Todo - Booking Failed
					die();
				}
				
				if(isset($cost_details['prepare_data']['CC_fees']) && $cost_details['prepare_data']['CC_fees'] != '') {
					$PG_Charge = str_replace(',','',$cost_details['prepare_data']['CC_fees']);
					$PG_Charge = $PG_Charge / $exchange_rate;
				}
				else {
					$PG_Charge = 0;
				}
				if(isset($cost_details['prepare_data']['deposit_amount']) && $cost_details['prepare_data']['deposit_amount'] != '') {
					$deposit_amount = str_replace(',','',$cost_details['prepare_data']['deposit_amount']);
					$deposit_amount = $deposit_amount / $exchange_rate;
				}
				else {
					$deposit_amount = 0;
				}
				if(isset($cost_details['prepare_data']['transaction_amount']) && $cost_details['prepare_data']['transaction_amount'] != '') {
					$transaction_amount = str_replace(',','',$cost_details['prepare_data']['transaction_amount']);
					$transaction_amount = $transaction_amount / $exchange_rate;
					$transaction_amount = ceil($transaction_amount);
				}
				else {
					$transaction_amount = 0;
				}
				
				$booking_transaction = array(
											'api_rate' => 0,
											'api_currency' => $cart_details->API_CURR,
											'booking_currency' => $currency,
											'currency_exchange_rate' => $exchange_rate,
											'net_rate' => $cart_details->net_rate,
											'admin_markup' => $cart_details->admin_markup,
											'admin_baseprice' =>$cart_details->admin_baseprice,
											'agent_markup' => $cart_details->my_markup,
											'base_amount' => $cart_details->admin_baseprice,
											'agent_base_amount' => $cart_details->agent_baseprice,
											'my_agent_Markup' => $cart_details->my_agent_Markup,
											'sub_agent_baseprice' => $cart_details->sub_agent_baseprice,
											'my_b2b2b_markup' => $cart_details->my_b2b2b_markup,
											'b2b2b_baseprice' => $cart_details->b2b2b_baseprice,
											'promo_code' => $cart_details->promo_code,
											'discount' => $cart_details->discount,
											'service_charge' => str_replace(',','',$cost_details['prepare_data']['service_charge'])/$exchange_rate,
											'gst_charge' => str_replace(',','',$cost_details['prepare_data']['gst_charge'])/$exchange_rate,
											'tax_charge' => str_replace(',','',$cost_details['prepare_data']['tax_charge'])/$exchange_rate,
											'total_tax' => str_replace(',','',$cost_details['prepare_data']['total_tax'])/$exchange_rate,
											'extra_markup' => 0,
											'total_amount' => str_replace(',','',$cost_details['prepare_data']['total_cost'])/$exchange_rate,
											'PG_Charge' => $PG_Charge,
											'total_PG_payble' => $transaction_amount,
											'wallet_Charge' => $deposit_amount,
											
										); 
										
				$booking_transaction_id = $this->booking_model->insert_booking_transaction_data($booking_transaction);
			
				/* Generating Transaction Reference */
				
				do {
					$key_string = $this->generate_key_string();
					$result = $this->booking_model->check_transaction_reference($key_string)->result();
				} while (!empty($result));
				
				/* End of Transaction reference generation */
				
				$booking_payment = array(
										'payment_logic' =>$payment_logic,
										'payment_type' =>$payment_type,
										'payment_gateway_id' => 1,
										'amount' => str_replace(',','',$cost_details['prepare_data']['total_cost']),
										'payment_status' => 'PENDING',
										'pg_markup' => $PG_Charge,
										'transaction_reference' => $key_string,
										'payment_gateway_status' => 'INITIATED'
									);
				$booking_payment_id = $this->booking_model->insert_booking_payment_data($booking_payment);
				@mkdir('./xml_logs/'.$parent_pnr, 0777, true);
				
				$booking_xml = array(
									'api_id' => $cart_details->api_id,
									'product_id' => $cart_details->product_id,
									'search_scenario' =>'',
								);

				$booking_xml_data_id = $this->booking_model->insert_booking_xml_data($booking_xml);
				
			
				$booking_supplier = array(
										'api_id' => $cart_details->api_id,
										'booking_supplier_number' => 'XXXXXXXXXX',
										'supplier_status' => '',
										'supplier_reference_number' => '',
										'booking_xml_data_id' => $booking_xml_data_id
									);
									
				$booking_supplier_id = $this->booking_model->insert_booking_supplier_data($booking_supplier);
			
				$checkout_form['salutation'] = '';
				$checkout_form['first_name'] = $userInfo->user_name;
				$checkout_form['last_name'] = '';
				$checkout_form['email'] = $userInfo->user_email;
				$checkout_form['mobile'] = $userInfo->user_cell_phone;
				$checkout_form['country'] = $userInfo->country_name;
				$checkout_form['street_address'] = $userInfo->address;
				$checkout_form['address2'] = '';
				$checkout_form['city'] = $userInfo->city_name;
				$checkout_form['state'] = $userInfo->state_name;
				$checkout_form['zip'] = $userInfo->zip_code;
			
				$billing_address = array(
										'user_id' => $user_id, 
										'billing_salutation' => trim($checkout_form['salutation']),
										'billing_first_name' => trim($checkout_form['first_name']),
										'billing_last_name' => trim($checkout_form['last_name']),
										'billing_email' => trim($checkout_form['email']),
										'billing_contact_number' => trim($checkout_form['mobile']),
										'billing_country_code' => trim($checkout_form['country']),
										'billing_address' => trim($checkout_form['street_address'].' '.$checkout_form['address2']),
										'billing_city' => trim($checkout_form['city']),
										'billing_state' => trim($checkout_form['state']),
										'billing_zip' => trim($checkout_form['zip'])
									);
				$billing_address_id = $this->booking_model->insert_booking_billing_address($billing_address);

				$booking = array(
								'product_id' => $cart_details->product_id,
								'api_id'=> $cart_details->api_id,
								'referal_id' => $booking_referal_id,
								'booking_transaction_id' => $booking_transaction_id,
								'payment_id' => $booking_payment_id,
								'branch_id' => $branch_id,
								'booking_supplier_id' => $booking_supplier_id,
								'billing_address_id' => $billing_address_id,
								'parent_pnr_no' => $parent_pnr,
								'user_id' => $user_id,
								'user_type_id' => $user_type,
								'voucher_date' => date('Y-m-d'),
								'travel_date' => date('Y-m-d'),
								
								'ip_address' => $this->input->ip_address(),
							);
				if($checkout_form['product_id'] == 1) {
					$booking['leadpax'] = trim($checkout_form['a_gender'.$cid][0][0].' '.$checkout_form['first_name'.$cid][0][0].' '.$checkout_form['last_name'.$cid][0][0]);
				}elseif($checkout_form['product_id'] == 19){
					$booking['leadpax'] = trim($checkout_form['a_gender'.$cid][0].' '.$checkout_form['first_name'.$cid][0].' '.$checkout_form['last_name'.$cid][0]);
				}
						
				$bid = $this->booking_model->insert_booking_global_data($booking);
			 
				$p_fname = $checkout_form['first_name'.$cid];
				$p_lname = $checkout_form['last_name'.$cid];
				$a_gender = $checkout_form['a_gender'.$cid];
				
				if(isset($checkout_form['cfirst_name'.$cid])):
					$c_fname = array_values($checkout_form['cfirst_name'.$cid]);
					$c_lname = array_values( $checkout_form['clast_name'.$cid]);
					$cdob = array_values($checkout_form['childAges_room_'.$cid]);
					$cdobs = array_values($checkout_form['child_dob_'.$cid]);
					$c_gender = array_values($checkout_form['c_gender'.$cid]);
				endif;
				$booking_passenger_v = array();
			
				for($ps=0;$ps<count($p_fname);$ps++):
					for($p=0;$p<count($p_fname[$ps]);$p++):
						if($a_gender[$ps][$p] == 'Mr'):
							$gender = 'MALE';
						elseif($a_gender[$ps][$p] == 'Mrs' || $a_gender[$ps][$p] == 'Ms' || $a_gender[$ps][$p] == 'Miss'):
							$gender = 'FEMALE';
						else:
							$gender = 'NA';
						endif;
						$booking_passenger = array(
													'booking_global_id'=>$bid,
													'first_name' =>trim($p_fname[$ps][$p]),
													'salutation' =>trim($a_gender[$ps][$p]),
													'last_name' =>trim($p_lname[$ps][$p]),
													'passenger_type' =>'ADULT',
													'age' => '30',
													'gender' =>$gender
											);
						$booking_passenger_v[] = $booking_passenger;
					endfor;
				endfor;
				$this->booking_model->insert_booking_passenger($booking_passenger_v);
				
				if(isset($c_fname) && is_array($c_fname)):
					for($ps=0;$ps<count($c_fname);$ps++):
						for($p=0; $p <count($c_fname[$ps]);$p++):
							$gender = 'FEMALE';
							if($c_gender[$ps][$p] == 'Master'):
								$gender = 'MALE';
							endif;
							$booking_passenger = array(
													'booking_global_id'=>$bid,
													'first_name' =>trim($c_fname[$ps][$p]),
													'salutation' =>trim($c_gender[$ps][$p]),
													'last_name' =>trim($c_lname[$ps][$p]),
													'passenger_type' =>'CHILD',
													'age' => $cdob[$ps][$p],
													'dob' => $cdobs[$ps][$p],
													'gender' =>$gender
												);
							$booking_passenger_v[] = $booking_passenger;
							$booking_passenger_v1[] = $booking_passenger;
						endfor;
					endfor;
				endif;
				//echo "pax: <pre/>";print_r($booking_passenger_v1);exit;
			  
				if(!empty($booking_passenger_v1) && $booking_passenger_v1 != '') {
					$this->booking_model->insert_booking_passenger($booking_passenger_v1);
				}
			 
				$vid_v = strtoupper(substr(PROJECT_NAME,0,3));
				$pnr_no = $vid_v.date('m').'F'.date('dHi').$bid;
			
				if($userInfo->user_type_name=='B2B'):
					$update_booking = array(
											'pnr_no' => $pnr_no,
											'booking_status' => 'PROCESS' 
									);
				else:
					 $update_booking = array(
											'pnr_no' => $pnr_no,
											'booking_status' => 'FAILED' 	
										);
				endif;
				$this->booking_model->Update_Booking_Global($bid, $update_booking);
			}
			else {
				#Todo - Booking Failed
				die();
			}
                      
			if($parent_pnr != ""){	 		
				$OrderId = base64_encode(json_encode($parent_pnr)); 
				$GateURL = base_url().'booking/doPaymentGate/'.$OrderId;
				$data['status'] = 555;
				$data['GateURL'] = $GateURL;
				echo json_encode($data);
				die;
			}
			
		}
	}
	
	public function doPaymentGate($parent_pnr) {
		//~ echo "parent_pnr: <pre>";print_r($parent_pnr);exit;
		 $parent_pnr1 = $parent_pnr;
        $data['OrderId'] = $parent_pnr = json_decode(base64_decode($parent_pnr1));
        $data['payment_mode'] = $payment_mode = '1';
        $count = $this->booking_model->getBookingByParentPnr($parent_pnr)->num_rows();
        if($count > 0){
			$data['pnr_nos'] = $pnr_nos = $this->booking_model->getBookingByParentPnr($parent_pnr)->result();
			
            foreach ($pnr_nos as $key => $pnr) {
				
                //$getBookingHotelData = $this->booking_model->getBookingHotelData($pnr->referal_id)->row();
               $booking = $this->booking_model->getBookingbyPnr($pnr->pnr_no,$pnr->product_id)->row();
				//~ echo "booking data: <pre>";print_r($booking);exit;
				if($booking->user_type_name == 'B2B'):
					if ($booking->payment_type == 'DEPOSIT') {
						$Total[] = $booking->total_amount;
						$wallet_debit[] = $booking->total_amount; // Deducting total amount irrespective of B2B Markup
					}
					elseif($booking->payment_type == 'PAYPAL') {
						$Total[] = $booking->total_amount;
						$wallet_debit[] = 0;
					}
					else {
						#Todo - Booking Failed
						die();
					}
				elseif($booking->user_type_name == 'B2B2B'):
					if ($booking->payment_type == 'DEPOSIT') {
						$Total[] = $booking->total_amount;
						$wallet_debit[] = $booking->total_amount; // Deducting total amount irrespective of B2B2B Markup
					}
					elseif($booking->payment_type == 'PAYPAL') {
						$Total[] = $booking->total_amount;
						$wallet_debit[] = 0;
					}
					else {
						#Todo
					}
				else:
					#Todo
				endif; 
            } 
            
            /* Initiating Payment */
            
         
            
            $wallet_debit_charge = array_sum($wallet_debit);
            $user_type = $this->session->userdata('user_type');
			$user_id = $this->session->userdata('user_details_id');
			$branch_id = $this->session->userdata('branch_id');
            
            if($booking->payment_type == 'DEPOSIT') {
				$deposit_amount_det = $this->General_Model->getAgentBalance()->row();
				$credit_amount = $deposit_amount_det->balance_credit;
				$user_balance_credit = $this->do_deduction($wallet_debit_charge,$credit_amount,$user_id);
			}
            elseif($booking->payment_type == 'PAYPAL') {
				#No Deduction
				$user_balance_credit = $deposit_amount_det->balance_credit;
			}
            elseif($booking->payment_type == 'BOTH') {
				#Deducting Deposit Amount From Wallet
				$deposit_amount_det = $this->General_Model->getAgentBalance()->row();
				$credit_amount = $deposit_amount_det->balance_credit;
				$user_balance_credit = $this->do_deduction($wallet_debit_charge,$credit_amount,$user_id);
			}
            elseif($booking->payment_type == 'PAYLATER') {
				#No Deduction
			}
			else {
				#Todo
			}
			
		    $booking_transaction_data = array(
												'transaction_amount' => ceil(array_sum($Total)),
												'wallet_balance' => $user_balance_credit,
												'credit_in_wallet' => $deposit_amount_det->balance_credit
										);
            $this->booking_model->update_booking_transaction_data($pnr_nos[0]->booking_transaction_id, $booking_transaction_data);
             $this->preparePaymentGateway($parent_pnr1,$pnr_nos[0]->product_id);
            
            /* End of Initiating Paymnet */
            
            
            //~ if ($booking->payment_type == 'PAYMENT') {
                //~ 
            //~ }
            //~ elseif($booking->payment_type == 'DEPOSIT') {
				//~ $Total = array_sum($Total);
				//~ $data['Total'] = ceil($Total);
				//~ redirect('booking/book/'.$parent_pnr1);  
			//~ }
			//~ elseif($booking->payment_type == 'BOTH') {
			//~ }
            //~ else{
                //~ $this->load->view('errors/expiry');
            //~ }
             
			//~ if ($booking->payment_type == 'PAYMENT') {
                //~ #Todo
            //~ }
            //~ elseif($booking->payment_type == 'DEPOSIT') {
				//~ $Total = array_sum($Total);
				//~ $data['Total'] = ceil($Total);
				//~ # Deducting Amount - Todo
				//~ redirect('booking/book/'.$parent_pnr1);  
			//~ }
			//~ elseif($booking->payment_type == 'BOTH') {
				//~ #Todo
			//~ }
            //~ else{
                //~ $this->load->view('errors/expiry');
            //~ } 
//~ 
			//~ /* Payment Logic Definition */
			//~ 
            //~ $getBookingHotelData = $this->booking_model->getBookingHotelData($pnr->referal_id)->row();
            //~ $booking = $this->booking_model->getBookingbyPnr($pnr->pnr_no,$pnr->product_name)->row();
                	//~ 
			//~ $data['email'] = $booking->billing_email;
			//~ if ($booking->payment_type == 'PAYMENT') {
				//~ $this->preparePaymentGateway($pnr->pnr_no,$pnr->product_name);
			//~ }    
			//~ elseif($booking->payment_type == 'DEPOSIT') {
				//~ $this->do_deduction($payable_amount,$credit_amount,$user_id);
				//~ $Total[] = $Ttl;
			//~ }  
			//~ elseif($booking->payment_type == 'BOTH') {  
				//~ #Todo
				//~ $this->do_deduction($payable_amount,$credit_amount,$user_id);
				//~ $this->preparePaymentGateway($pnr->pnr_no,$pnr->product_name);
			//~ }
			//~ elseif($booking->payment_type == 'PAYLATER') {  
				//~ #Todo
			//~ }
			//~ else {
				//~ # Todo
				//~ die();
			//~ }
            //~ 
            //~ /* End of Payment Login */           
        }
        else{
            $this->load->view('errors/expiry');
        }
    }
    public function preparePaymentGateway($parent_pnr,$product_id) {
		$parent_pnr_encoded = $parent_pnr;
		$parent_pnr = json_decode(base64_decode($parent_pnr));
		$count = $this->booking_model->getBookingByParentPnr($parent_pnr)->num_rows();
		$booking_data = $this->booking_model->getBookingByParentPnr($parent_pnr)->result();
		$billing_address = $this->booking_model->getbillingaddressTemp($booking_data[0]->billing_address_id)->result(); 
		if($booking_data[0]->product_id == 1){
			$booking_hotel=  $this->booking_model->getBookingHotelTemp($booking_data[0]->referal_id)->row();
			
			$cart_details =  $this->booking_model->get_cart_details($booking_hotel->cid)->row();
			$this->booking_model->delete_cart_global($cart_details->cart_id);
			$this->booking_model->delete_cart_hotel($cart_details->referal_id);
		}
		
		if($count > 0) {
			if($booking_data[0]->payment_type == "DEPOSIT") {
				$update_payment = array(
										'payment_gateway_status' => 'SUCCESS',
										'transaction_status' => 'SUCCESS',
										'payment_status' => 'SUCCESS'
				);
				$this->booking_model->Update_Booking_payment($booking_data[0]->payment_id, $update_payment);
				redirect('booking/book/'.$parent_pnr_encoded);
			}
			elseif($booking_data[0]->payment_type == "PAYPAL") {
				
				//~ echo "data: <pre>";print_r($booking_data);
				$booking = $this->booking_model->getBookingbyPnr($booking_data[0]->pnr_no,$booking_data[0]->product_id)->row();
				$card_details = json_decode($booking->TravelerDetails);
				
				 $vpc_Amount = round($booking->transaction_amount, 2);
				
				$payableAmount = 1;
				$nameArray = explode(' ',$card_details->name_on_card);
    //Buyer information
				$firstName = $nameArray[0];
				$lastName = $nameArray[1];
				$city = $billing_address[0]->billing_city;
				$zipcode = $billing_address[0]->billing_zip;
				$countryCode = 'IN';
    
    //Create an instance of PaypalPro class
    
	//'amount' => $booking->transaction_amount,
	//Payment details
    $paypalParams = array(
        'paymentAction' => 'Sale',
        'amount' =>  1 ,
        'currencyCode' => 'CAD',
        'creditCardType' => $card_details->card_type,
        'creditCardNumber' => trim(str_replace(" ","",$card_details->card_number)),
        'expMonth' => $card_details->expiry_month,
        'expYear' => $card_details->expiry_year,
        'cvv' => $card_details->cvv,
        'firstName' => $firstName,
        'lastName' => $lastName,
        'city' => $city,
        'zip'	=> $zipcode,
        'countryCode' => $countryCode,
    );
    
   
    //$response = $paypal->paypalCall($paypalParams);
   
    $this->load->library('../controllers/Paypalpro', false);
    $paypal_response = Paypalpro::paypalCall($paypalParams);
  
		   
		    if(count($paypal_response) > 0){
				 if($paypal_response['ACK'] == 'Success'){
					 $update_payment = array(
										'payment_gateway_status' => $paypal_response['ACK'],
										'transaction_status' => $paypal_response['ACK'],
										'payment_status' => $paypal_response['ACK'],
										'transaction_reference' =>$paypal_response['TRANSACTIONID'],
										'payment_request' =>json_encode($paypalParams),
										'payment_response' => json_encode($paypal_response),
				     );
				   $this->booking_model->Update_Booking_payment($booking_data[0]->payment_id, $update_payment);
					 
					redirect(base_url()."booking/book/".$parent_pnr_encoded);
				 }else{
					 
					redirect('booking/payment_failed/'.$parent_pnr_encoded);
				 }
			}            
			}
				
			elseif($booking_data[0]->payment_type == "PAYLATER") {
				//~ echo "data: <pre>";print_r($booking_data);exit;
				$update_payment = array(
										'payment_gateway_status' => 'PENDING',
										'transaction_status' => 'PENDING',
										'payment_status' => 'PENDING'
				);
				$this->booking_model->Update_Booking_payment($booking_data[0]->payment_id, $update_payment);
				redirect('booking/book/'.$parent_pnr_encoded);
			}
			else {
				#Todo
				die();
			}
		}
		else {
			$this->load->view('errors/expiry');
		}
	}
	
	
	
    public function payment_success($parent_pnr_encoded) {
		$payment_response = $this->input->get();
		$paypal_response  = json_encode($payment_response);
		$paypal_transaction_no = $payment_response['tx'];
		$paypal_status = $payment_response['st'];
		$paypal_paid_amount = $payment_response['amt'];
		$paypal_paid_currency = $payment_response['cc'];
		$paypal_paid_messages = $payment_response['cm'];
		$paypal_iteml_number = $payment_response['item_number']; 
		
		
		if($paypal_status == 'Completed') {
			$payment_status = 'SUCCESS';
			$payment_gateway_status = 'SUCCESS';
		}
		else {
			$payment_status = 'FAILED';
			$payment_gateway_status = 'FAILURE';
		}
		
		$update_booking_payment = array(
										'transaction_id' => $paypal_transaction_no,
										'payment_response' => $paypal_response,
										'transaction_status' => $paypal_status,
										'payment_gateway_status' => $payment_gateway_status,
										'payment_status' => $payment_status,
								       );
		//~ echo "Payment Response: <pre>";print_r($update_booking_payment);exit;				
		$this->booking_model->Update_Booking_payment($payment_id, $update_booking_payment);
		if($payment_status == "SUCCESS") {
			//Email Management
			
			$user_type = $this->session->userdata('user_type');
			$user_id = $this->session->userdata('user_details_id');
			$userInfo =$this->General_Model->get_user_details($user_id, $user_type);
			//echo 'sanjay<pre>'; print_r($userInfo); exit();
			//$this->Email_Model->sendmail_paymentsuccess($email,$data);
			# Email For Successful Payment Transaction
			redirect('booking/book/'.$parent_pnr_encoded);
		}
		else {
			# Email For Failed Payment Transaction
			redirect('booking/payment_failed/'.$parent_pnr_encoded);
		}
	}
	public function payment_failed($parent_pnr = "") {
		$data['title'] = "Transaction Failure";
		$data['caption'] = "Payment Failed!";
		$data['message'] = "Transaction process could not be successful. Sorry for the inconvinience!";
		$data['image_url'] = base_url()."assets/images/faild_payment.png";
		$this->load->view('general/error_info',$data);
	}
    public function book($parent_pnr){ 
		
		$parent_pnr = json_decode(base64_decode($parent_pnr));
		
        $count = $this->payment_model->validate_order_id_org($parent_pnr)->num_rows();
		if($count >= 1){
			$global_ids = $this->payment_model->validate_order_id_org($parent_pnr)->result();
			
 			foreach ($global_ids as $key => $global_id) { 
				$booking_id = $global_id->referal_id;
				$module = $global_id->product_name;
				$bid = $global_id->booking_global_id;
				$product_id = $global_id->product_id;
				$pnr_no = $global_id->pnr_no;
				$payment_method = $global_id->payment_type;
				$billing_address_id = $global_id->billing_address_id;
				$booking_supplier_id = $global_id->booking_supplier_id;
				
				$user_id = $global_id->user_id;
				$user_type_id = $global_id->user_type_id;
				$error_message='';
				$update_booking = array(
									'supplier_reference_number' => 'XXXXXXXXX',
									'supplier_status' => 'PENDING',
									'booking_supplier_number' => 'XXXXXXXXX'
									);

				$update_booking_status = array(
							'booking_status' => 'PENDING'
							);
					
				if($payment_method=='DEPOSIT'){
					$user_type = $global_id->user_type_id;
					$user_id = $global_id->user_id;
					if($this->session->userdata('user_type') == $user_type && $this->session->userdata('user_details_id') == $user_id){
						$book_check = 'OK';
						$error_message.='|Deposit - OK.';
					}else{
						$book_check = 'NOTOK';
						$error_message.='|Deposit - User ID and User Type Not a valid.';
					}
				}
				elseif($payment_method=='PAYPAL'){
					$book_check = 'OK';
				}
				elseif($payment_method=='BOTH'){
					$book_check = 'OK';
					#Todo
				}
				elseif($payment_method=='PAYLATER'){
					$book_check = 'OK';
					#Todo
				}
				else{
					$book_check = 'NOTOK';
					$error_message.='|Not a valid user.';
				}
				if($book_check=='OK')
				{
					 $this->load->library('xml_to_array');
			
					if($module == 'Hotels' || $product_id == 1)
					{ 
						$count = $this->booking_model->getBookingHotelTemp($booking_id)->num_rows();
						$bdetails = $this->booking_model->getBookingHotelTemp($booking_id)->result();
						
						if($count == 1){
							
							if($global_id->api_name =='GTA') {
								$this->load->helper('gta_helper');
								$booking_supplier_details = $this->booking_model->get_booking_supplier_details($global_id->booking_supplier_id)->row();
								$passenger_info = $this->booking_model->getBookingpassengerTemp($bid)->result();
								$billingaddress = $this->booking_model->getbillingaddressTemp($billing_address_id)->row();
								$book_temp_data = $this->booking_model->getBookingHotelTemp($booking_id)->row();
								
								
								$cart_hotel_data = json_decode(base64_decode($book_temp_data->cart_data));
								$checkout_form = json_decode($book_temp_data->TravelerDetails);
								$passenger_info_c = json_encode($passenger_info);
								$billingaddress_c = json_encode($billingaddress);
									
								$ProcessTerms_response = HotelBooking($cart_hotel_data, $passenger_info_c,$checkout_form, $billingaddress_c, $booking_id,$parent_pnr,$book_temp_data->cid);

								$ProcessTermsRequest_ = 'xml_logs/'.$parent_pnr.'/BookingRequest_'.$booking_id.'.xml'; 
								$ProcessTermsResponse_ = 'xml_logs/'.$parent_pnr.'/BookingResponse_'.$booking_id.'.xml'; 
								
								write_file($ProcessTermsRequest_,$ProcessTerms_response['request'], 'w+');
								write_file($ProcessTermsResponse_,$ProcessTerms_response['response'], 'w+');
								$ProcessTermsRequest_s = array(
								'ProcessTermsRequest' => $ProcessTermsRequest_,
								'ProcessTermsResponse' => $ProcessTermsResponse_
								);
								 $hotelBookingResponse = $ProcessTerms_response['response'];
						
								$this->booking_model->Update_Booking_xmldata($booking_supplier_details->booking_xml_data_id, $ProcessTermsRequest_s);
								if(!empty($hotelBookingResponse)){

                                    $hotelBookingResponse = new SimpleXMLElement($hotelBookingResponse);
                                    
                                    if(isset($hotelBookingResponse->ResponseDetails->BookingResponse)){
                                        $BookingResponse = $hotelBookingResponse->ResponseDetails->BookingResponse;

                                        if(isset($BookingResponse->BookingItems->BookingItem->ChargeConditions)) {
                                            $cancellation_type = (string)$BookingResponse->BookingItems->BookingItem->ChargeConditions->ChargeCondition['Type'];
                                            if($cancellation_type == "cancellation") {
                                                if(isset($BookingResponse->BookingItems->BookingItem->ChargeConditions->ChargeCondition)) {
                                                    $cancellation_conditions = $BookingResponse->BookingItems->BookingItem->ChargeConditions->ChargeCondition;

                                                    $cancellation_policy_string = "";
                                                    foreach($cancellation_conditions->Condition as $cck) {
                                                        $Charge = (string)$cck['Charge'];
                                                        
                                                        if($Charge == "true") {
                                                            $ChargeAmount = (string)$cck['ChargeAmount'];
                                                            $FromDate = (string)$cck['FromDate'];
                                                            $ToDate = (string)$cck['ToDate'];
                                                            $currency = (string)$cck['Currency'];

                                                            
                                                            $chargeAmountCurrency = BASE_CURRENCY;
                                                            // $chargeAmount_c = $this->account_model->currency_convertor($ChargeAmount);
															 //~ Commented as of now
															 //~ $chargeAmount_c		= $this->general_model->convert_api_to_base_currecy_with_markup($ChargeAmount,$currency,$ProcessTerms_response['api_id']);
															 $chargeAmount_c		= $ChargeAmount;
		
                                                            //~ Commented as of now
                                                            //~ $cancellation_policy_string .= "The cancellation charges from <b>".$FromDate."</b> to <b>".$ToDate."</b> is <b>".$chargeAmountCurrency.$chargeAmount_c['TotalPrice'].'</b>|';
                                                            $cancellation_policy_string .= "The cancellation charges from <b>".$FromDate."</b> to <b>".$ToDate."</b> is <b>".$chargeAmountCurrency.' '.$chargeAmount_c.'</b>|';

                                                        }
                                                        
                                                    }

                                                }
                                            }
                                        }

                                        if(isset($BookingResponse->BookingItems->BookingItem->ItemConfirmationReference)) {
                                            $BookingApiConfirmationNumber = (string)$BookingResponse->BookingItems->BookingItem->ItemConfirmationReference;
                                        } else {
                                            $BookingApiConfirmationNumber = "";
                                        }
                                        
                                        $essential_info_json = "";
                                        if(isset($BookingResponse->BookingItems->BookingItem->EssentialInformation)) {
                                            $essentialInfo = $BookingResponse->BookingItems->BookingItem->EssentialInformation;
                                            if(isset($essentialInfo->Information)){
                                                $ei_text = "";
                                                foreach($essentialInfo->Information as $eik) {
                                                    $ei_text = (string)$eik->Text;
                                                    
                                                    $ei_date_from = "";
                                                    $ei_date_to = "";
                                                    if(isset($eik->DateRange)) {
                                                        foreach($eik->DateRange as $drk) {
                                                            $ei_date_from = (string)$drk->FromDate;
                                                            $ei_date_to = (string)$drk->ToDate;
                                                        }
                                                    }
                                                    $essential_info[] = array('text'=>$ei_text, 'fromDate'=>$ei_date_from, 'toDate'=>$ei_date_to);                                                
                                                    $essential_info_json = json_encode($essential_info);
                                                }
                                            } else {
                                                $essential_info_json = "";
                                            }
                                        } else {
                                            $essential_info_json = "";
                                        }
                                        
                                        if(isset($essential_info_json) && $essential_info_json != "") {
                                            $Remarks = $essential_info_json;     
                                        } else {
                                            $Remarks = "No Remarks";
                                        }
                                        
                                        
                                        $Status = (string)$BookingResponse->BookingStatus;
                                        if($Status === "Confirmed"){
                                            $BookingStatus = 'CONFIRMED';  
                                        }else if($Status == 'Pending Confirmation'){
                                            $BookingStatus = 'PENDING';  
                                        }
                                        else{
                                        	$BookingStatus = "";
                                        }
		
                                        foreach ($BookingResponse->BookingReferences->BookingReference as $key => $Reference) {
                                            $ReferenceSource = (string)$Reference['ReferenceSource'];
                                            if ($ReferenceSource == 'api') {
                                                $LocatorCode = (string)$Reference;
                                            }
                                            elseif ($ReferenceSource == 'client') {
                                                $clientRef = (string)$Reference;
                                            }
                                            elseif ($ReferenceSource == 'agent') {
                                                $agentRef = (string)$Reference;
                                            }
                                            else {
												
											}
                                        }
                                        if(!isset($LocatorCode)) {
											$LocatorCode = "";
										}
										if(!isset($clientRef)) {
											$clientRef = "";
										}
										if(!isset($agentRef)) {
											$agentRef = "";
										}
										
										if(isset($LocatorCode)) {
                                            $booking_confirmation_num = $LocatorCode;
                                        }
                                        elseif(isset($clientRef)) {
											$booking_confirmation_num = $clientRef;
										}
										else {
											$booking_confirmation_num = "";
										}
                                       
                                        $error_message .= '|'.$booking_id.'-Hotel RESPONSE SUCCESS';  
                                    
                                    }else{
                                        
                                        $error_message.='|'.$booking_id.'-HOTEL ERROR RESPONSE';
                                    }
                                }else{
                                    $error_message.='|'.$booking_id.'-HOTEL EMPTY RESPONSE';
                                }
								 
									 
								$xml_log_id = $this->General_Model->store_logs($ProcessTerms_response,'SUCCESS');
								 	
								$update_booking = array(
									'supplier_reference_number' => $LocatorCode,
									'supplier_status' => $BookingStatus,
									'booking_supplier_number' => $clientRef
									);
								$update_booking_status = array(
															'booking_no' => $clientRef,
															'api_confirmation_no' => $LocatorCode,
															'booking_status' => $BookingStatus
														);
							}
							else {
									
							}
							$booking_res = array(
								'LocatorCode' => $LocatorCode,
								'clientRef' => $clientRef,
								'agentRef' => $agentRef,
								'BookingStatus' => $BookingStatus,
								'BookingApiConfirmationNumber' => $booking_confirmation_num,
								'BookingApiCancellation' => $cancellation_policy_string,
								'Status' => $Status,
								'Remarks' => $Remarks
							);                            

							//~ echo '<pre>';print_r($booking_res);die;
							$booking_res = json_encode($booking_res);
							$booking_hotel = array(
								'HotelBookingRQ' =>  $ProcessTerms_response['request'],
								'HotelBookingRS' => $ProcessTerms_response['response'],
								'booking_res' => $booking_res
							);                            
						
							$this->booking_model->update_booking_hotel($booking_id, $booking_hotel);
						}
					}
					elseif($product_id == 19){
						$this->load->library('../controllers/Transferbooking', false);
		                $transferBooking = Transferbooking::transfer_booking($global_id, $bid, $billing_address_id,$booking_id, $parent_pnr);
		                $update_booking_status = $transferBooking['update_booking_status'];
						$update_booking = $transferBooking['update_booking_supplier'];
						
						$error_message.= $transferBooking['error_message'];
					}
					
					
				}
				else
				{
						$error_message.='|Booking process failure';
				}
			 
			$update_booking_process = array(
						'flow_tracking' => $error_message
					);
			/*echo "<pre>",print_r($update_booking_process);
			echo "<pre>",print_r($update_booking);
			echo "<pre>",print_r($update_booking_status);

			exit;*/
			$this->booking_model->Update_Booking_Global($bid, $update_booking_process);
							
			//store booking_info
			$this->booking_model->Update_Booking_Supplier($booking_supplier_id, $update_booking);
			$this->booking_model->Update_Booking_Global($global_id->booking_global_id, $update_booking_status);
							
			}
			
			
			# Email For Successful booking
			redirect(base_url().'booking/confirm/'.base64_encode($parent_pnr), 'refresh');
		}
		else
		{
			$msg = 'Booking Failure, your order doest not exists.';
			$orderid =  '';
			$payid =  '';
			$msg = base64_encode($msg);
			# Email For UnSuccessful booking
			exit;
			redirect(WEB_URL.'error/payment/'.$msg,'refresh');
		}
	}
	public function user_do_payment($payable_amount, $myMarkup,$user_id){
    	$deposit_amount_det = $this->General_Model->getAgentBalance()->row();
    	$credit_amount = $deposit_amount_det->balance_credit;
        $payable_amount = $this->account_model->PercentageMinusAmount($payable_amount,$myMarkup);
    	if($credit_amount >= $payable_amount){
			$balance_credit = $credit_amount - $payable_amount;
			$update_credit_amount = array(
				'balance_credit' => $balance_credit,
				'last_debit' => $payable_amount
			);
			$this->account_model->update_credit_amount($update_credit_amount,$user_id);	
		}
    }
    
    public function do_deduction($deduct_amount, $wallet_balance, $user_id) {
		$balance_credit = $wallet_balance - $deduct_amount;
		$update_credit_amount = array(
			'balance_credit' => $balance_credit,
			'last_debit' => $deduct_amount
		);
		$this->Account_Model->update_credit_amount($update_credit_amount,$user_id);	
		return $balance_credit;
		# Email For Deposit Deduction
	}

	public function user_do_payment_account($payable_amount, $myMarkup,$user_id,$booking_id,$pnr){
    	$deposit_amount_det = $this->account_model->get_deposit_amount($user_id)->row();
    	$credit_amount = $deposit_amount_det->balance_credit;
        //echo $myMarkup;
        $payable_amount = $this->account_model->PercentageMinusAmount($payable_amount,$myMarkup);
        //die;
    	if($credit_amount >= $payable_amount){
			$balance_credit = $credit_amount;
			$description='BOOKING - : <a href="'.base_url().'invoice/'.base64_encode(base64_encode($pnr)).'" target="_blank">'.$pnr.'</a>';
			
				$account_transaction = array(
						'transaction_type' => 'WITHDRAW',
						 'booking_id' => $booking_id,
						'user_id' => $user_id,
						'amount' => $payable_amount,
						'balance_amount' => $balance_credit,
						'description' => $description
					);
					$this->db->insert('user_transaction',$account_transaction); 
					$bid = $this->db->insert_id();
					$timing = date('Ymd');
					$timing1 = date('His');
					$txno = 'TX'.$timing.$bid.$timing1;
								$update_account = array(
									'transaction_number' => $txno
								);
								
					$this->db->where('user_transaction_id',$bid);
					
					$this->db->update('user_transaction', $update_account);
		}
    }
	public function confirm($parent_pnr=''){
		if(!empty($parent_pnr)){
			$encoded_pnr = $parent_pnr;
			$parent_pnr = base64_decode($parent_pnr);
			$count = $this->booking_model->getBookingByParentPnr($parent_pnr)->num_rows();
			if($count > 0){
				$data['pnr_nos'] = $this->booking_model->getBookingByParentPnr($parent_pnr)->result();
				foreach($data['pnr_nos'] as $pnr_nos):
				    $data['Booking'][] = $booking = $this->booking_model->getBookingbyPnr($pnr_nos->pnr_no,$pnr_nos->product_id)->row();
					$data['Passenger'][] = $booking = $this->booking_model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					if($pnr_nos->product_id == 'Hotels' || $pnr_nos->product_id == 1){
					  //$this->load->view('hotel/hotel_voucher', $data);
						$user_type = $this->session->userdata('user_type');
						$user_details_id = $this->session->userdata('user_details_id');
						$userInfo =$this->General_Model->get_user_details($user_details_id, $user_type);
						
						$email['message'] = $this->load->view('hotel/hotel_mailVoucher', $data, TRUE);
                        $email['to'] = $userInfo->user_email;
                        if($userInfo->branch_id != 0){
						$mainUserInfo =$this->General_Model->get_user_details($userInfo->branch_id);	
						$email['cc'] = $mainUserInfo->user_email;
						}
						$email['email_access'] = $this->Email_Model->get_email_acess();
		                
                		$Response = $this->Email_Model->sendmail_hotelVoucher($email);
                		redirect(base_url().'booking/voucher/'.$encoded_pnr, 'refresh');

				    }elseif($pnr_nos->product_id == 19){
						//$this->load->view('transfer/transfer_voucher', $data);
						$user_type = $this->session->userdata('user_type');
						$user_details_id = $this->session->userdata('user_details_id');
						$userInfo =$this->General_Model->get_user_details($user_details_id, $user_type);
						
						$email['message'] = $this->load->view('transfer/transfer_voucher', $data, TRUE);

						$email['to'] = $userInfo->user_email;
		                $email['email_access'] = $this->Email_Model->get_email_acess();
		                
                		$Response = $this->Email_Model->sendmail_transferVoucher($email);

						redirect(base_url().'booking/voucher/'.$encoded_pnr, 'refresh');

					}
				endforeach;
				
			}else{
				 $this->load->view('errors/404');
			}
		}else{
			 $this->load->view('errors/404');
		}
	}
	public function voucher($parent_pnr=''){
		if(!empty($parent_pnr)){
			$parent_pnr = base64_decode($parent_pnr);
			$count = $this->booking_model->getBookingByParentPnr($parent_pnr)->num_rows();
			if($count > 0){
				$data['pnr_nos'] = $this->booking_model->getBookingByParentPnr($parent_pnr)->result();
				foreach($data['pnr_nos'] as $pnr_nos):
				   
				    $data['Booking'][] = $booking = $this->booking_model->getBookingbyPnr($pnr_nos->pnr_no,$pnr_nos->product_id)->row();
					$data['Passenger'][] = $booking = $this->booking_model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					if($pnr_nos->product_id == 'Hotels' || $pnr_nos->product_id == 1){
					  $this->load->view('hotel/hotel_voucher', $data);
				    }elseif($pnr_nos->product_id == 19){
						$this->load->view('transfer/transfer_voucher', $data);	
					}
				endforeach;
				
			}else{
				 $this->load->view('errors/404');
			}
		}else{
			 $this->load->view('errors/404');
		}
	}
	function prepare_calculation($post_data,$cid) {
		
		$currency = $_SESSION['currency'];
		$exchange_rate = $_SESSION['currency_value'];
		
		if($post_data['product_id'] == 1) {
		   $cart_details = $this->cart_model->getBookingTemp_hotel($cid);	
		}
		elseif($post_data['product_id'] == 19){
			$cart_details = $this->cart_model->getBookingTemp_transfer($cid);	
		}
		else {
			
		}
		//~ echo "cart_details: <pre>";print_r($cart_details);exit;
		
		$payment_logic = $post_data['payment_logic'];
		$payment_type = $post_data['payment_type'];
		$prepare_data = array();
			
		$prepare_data['prepare_data']['payment_logic'] = $payment_logic;
		$prepare_data['prepare_data']['payment_type'] = $payment_type;
		
		//~ Tax Calculation
			
		//~ $service_charge = $cart_details->service_charge;
		//~ $gst_charge = $cart_details->gst_charge;
		//~ $tax_charge = $cart_details->tax_charge;
		//~ 
		//$total_tax = number_format(($service_charge + $gst_charge + $tax_charge),2);
		
		$total_tax = number_format((0 + 0 + 0),2);
		
		//$prepare_data['prepare_data']['service_charge'] = $service_charge;
		//~ $prepare_data['prepare_data']['gst_charge'] = $gst_charge;
		//~ $prepare_data['prepare_data']['tax_charge'] = $tax_charge;
		//~ $prepare_data['prepare_data']['total_tax'] = $total_tax;
		$prepare_data['prepare_data']['service_charge'] = 0;
		$prepare_data['prepare_data']['gst_charge'] = 0;
		$prepare_data['prepare_data']['tax_charge'] = 0;
		$prepare_data['prepare_data']['total_tax'] = 0;
		
		//~ Promocode Initialization  
		
		if($cart_details->promo_code != '' || $cart_details->promo_code != NULL) {
			$promo_code = $cart_details->promo_code;
			$discount = $cart_details->discount;
		}
		else {
			if($post_data['promo_signature'] == 'SET') {
				$promo_code = $post_data['promo_code'];
				$discount = 0;
			}
			elseif($post_data['promo_signature'] == 'NOTSET') {
				$promo_code = NULL;
				$discount = 0;
			}
			else {
				$promo_code = NULL;
				$discount = 0;
			}
		}
		
		//~ End of Promocode Initialization 
		   if($this->session->userdata('user_type') == 2):
				$processing_promo_price = $commissionable_rate = $cart_details->agent_baseprice;
				$markup = $cart_details->my_markup;
			elseif($this->session->userdata('user_type') == 4):
				$processing_promo_price = $commissionable_rate = $cart_details->b2b2b_baseprice;
			elseif($this->session->userdata('user_type') == 5):
				$processing_promo_price = $commissionable_rate = $cart_details->agent_baseprice;
				$markup = $cart_details->my_markup;
			else:
			endif;
			
			/* Promocode Calculation */
			
			if($post_data['promo_signature'] == "SET" && ($promo_code != '' || $promo_code != NULL) ) {
				$promo_details = $this->processing_promo($post_data,$cart_details,$processing_promo_price);
				//~ $cost_details = $this->prepare_calculation($post_data,$post_data['cart_id']);
				
				$prepare_data['prepare_data']['msg'] = $promo_details['prepare_data']['msg'];
				$prepare_data['prepare_data']['promo_theme'] = $promo_details['prepare_data']['promo_theme'];
				$prepare_data['prepare_data']['promo_status'] = $promo_details['prepare_data']['promo_status'];
				
				$prepare_data['prepare_data']['promo_code'] = $promo_code;
				$prepare_data['prepare_data']['discount'] = $discount = $promo_details['prepare_data']['discount'];
			}
			elseif($post_data['promo_signature'] == "NOTSET" && ($promo_code != '' || $promo_code != NULL) ) {
				$post_data['promo_code'] = $promo_code;
				$promo_details = $this->processing_promo($post_data,$cart_details,$processing_promo_price);
				//~ $cost_details = $this->prepare_calculation($post_data,$post_data['cart_id']);
				
				$prepare_data['prepare_data']['promo_status'] = $promo_details['prepare_data']['promo_status'];
				$prepare_data['prepare_data']['promo_theme'] = $promo_details['prepare_data']['promo_theme'];
				
				$prepare_data['prepare_data']['promo_code'] = $promo_code;
				$prepare_data['prepare_data']['discount'] = $discount = $promo_details['prepare_data']['discount'];
			}
			else {
				$promo_details = array();
				
				$prepare_data['prepare_data']['promo_code'] = $promo_code;
				$prepare_data['prepare_data']['discount'] = $discount;
			}
			
			/* End of Promocode Calculation */
			
			if($post_data['product_id'] == 1) {
			   $cart_details = $this->cart_model->getBookingTemp_hotel($cid);	
			}
			elseif($post_data['product_id'] == 19){
				$cart_details = $this->cart_model->getBookingTemp_transfer($cid);	
			}
			else {
				
			}
			//~ echo "cart_details: <pre>";print_r($cart_details);exit;
			
			$prepare_data['prepare_data']['sub_total_cost'] = $commissionable_rate;
			
			$cancellation_txt = '';
			if($post_data['product_id'] == 1){
				if($cart_details->api_id == 1) {
					$cancellation = json_decode($cart_details->cancellation_commisionable_nettrate, true); 
			
					if(count($cancellation) > 0 && !empty($cancellation)){
							
							for($c=0; $c < count($cancellation); $c++){
							if(isset($cancellation[$c]['Type'])) {
							   if($cancellation[$c]['Type'] == 1) {
									$cancellation_amount = $cancellation[$c]['ChargeAmount']- $discount;
									$cancellation['cancellation'][$c]['ChargeAmount'] = $cancellation_amount;
									$cancellation_txt .='Cancellation between <strong>'.date('dS M Y', strtotime($cancellation[$c]['ToDate'])).'</strong> and <strong>'.date('dS M Y', strtotime($cancellation[$c]['FromDate'])).'</strong>, will incur cancellation charge <strong>'.$currency.' '.number_format($cancellation_amount*$exchange_rate,2).'</strong> |'  ;
								}
								elseif($cancellation[$c]['Type'] == 0) {
									
									 $cancellation_txt .='No cancellation fee (or Free cancellation) until '.date('dS M Y', strtotime($cancellation[$c]['FromDate'])).'| ';
								}	
							}
						  }
					}
				}
				else {
					
				}
			}
			elseif($post_data['product_id'] == 19){
				$cancellation = json_decode($cart_details->cancellation_commisionable_nettrate, true); 
				if(count($cancellation) > 0 && !empty($cancellation)){
					if(isset($cancellation['cancellation'])){
						for($c=0; $c < count($cancellation['cancellation']); $c++){
						   if($cancellation['cancellation'][$c]['Charge'] == 'true'){
							     $cancellation_amount = $cancellation['cancellation'][$c]['ChargeAmount'] - $discount;
							     $cancellation['cancellation'][$c]['ChargeAmount'] = $cancellation_amount;
								 $cancellation_txt .='Cancellation from '.date('dS M Y', strtotime($cancellation['cancellation'][$c]['FromDate'])).' onwards , will incur cancellation charge  <b>'.$currency.' '.$cancellation_amount*$exchange_rate.'</b>|' ;
								}else{
								 $cancellation_txt .='No cancellation fee (or Free cancellation) until '.date('dS M Y', strtotime($cancellation['cancellation'][$c]['FromDate'])).'| ';
							}	
						}
					}
				}
			}
			
			
			$prepare_data['prepare_data']['cancellation_txt'] = $cancellation_txt; 
			
			
			if($this->session->userdata('user_country_id') != 0){
				$country_name = $this->General_Model->get_country_name($this->session->userdata('user_country_id'))->row();
				$tax_details = $this->General_Model->get_tax_details($country_name->country_name, $this->session->userdata('user_state_id'))->row();
			
				if(!empty($tax_details) && count($tax_details) > 0){
					$tax_charge_details = $this->General_Model->tax_calculation($tax_details, $commissionable_rate);	
					
					if(!empty($tax_charge_details) && $tax_charge_details != '') {
						$service_charge = $tax_charge_details['sc_tax'];
						$gst_charge = $tax_charge_details['gst_tax'];
						$tax_charge = $tax_charge_details['state_tax'];
						
						$total_tax = number_format(($service_charge + $gst_charge + $tax_charge),2);
						
						$prepare_data['prepare_data']['service_charge'] = $service_charge;
						$prepare_data['prepare_data']['gst_charge'] = $gst_charge;
						$prepare_data['prepare_data']['tax_charge'] = $tax_charge;
						$prepare_data['prepare_data']['total_tax'] = $total_tax;
					}
				}
				else{
					$tax_charge_details = array();
				}
			}
			
			//~ End of Tax Calculation
			
			$total_cost_after_promo = $commissionable_rate - $discount;
			$total_cost_after_promo_with_tax = $total_cost_after_promo + str_replace(',','',$total_tax);
			if($payment_type == "DEPOSIT") {
				$total_cost = number_format($total_cost_after_promo_with_tax,2);
				
				$prepare_data['prepare_data']['total_cost'] = $total_cost;
			}
			elseif($payment_type == "PG") {
				$CC_transaction_fee = 2.63; // In Percentage
				$transaction_details = $this->percentage_conversion(str_replace(',','',$total_cost_after_promo_with_tax), $CC_transaction_fee);
				$CC_fees = number_format($transaction_details['CC_fees'],2);
				$transaction_amount = number_format($transaction_details['total_fees'],2);
				
				$total_cost = $transaction_amount;
				
				$prepare_data['prepare_data']['CC_fees'] = $CC_fees;
				$prepare_data['prepare_data']['total_cost'] = $total_cost;
			}
			elseif($payment_type == "PAYPAL") {
				 $deposit_payment = $post_data['deposit_payment'];
				  $deposit_payment_converted  = $post_data['deposit_payment'] / $exchange_rate; 
				$remaining_payment = number_format(($total_cost_after_promo_with_tax - $deposit_payment_converted),2) ;
				
				$CC_transaction_fee = 0; 
				$transaction_details = $this->percentage_conversion(str_replace(',','',$remaining_payment), $CC_transaction_fee);
				 
				$CC_fees = number_format($transaction_details['CC_fees'],2);
				$transaction_amount = number_format($transaction_details['total_fees'],2);
				
				$total_cost = (str_replace(',','',$transaction_amount) + $deposit_payment_converted);
				
				$prepare_data['prepare_data']['CC_fees'] = $CC_fees;
				$prepare_data['prepare_data']['transaction_amount'] = $transaction_amount;
				$prepare_data['prepare_data']['deposit_amount'] = $deposit_payment ;
				 $prepare_data['prepare_data']['total_cost'] = $total_cost;
				
			}
			else {
				#Todo
			}
		
		
		if($post_data['product_id'] == 1) {
			
			$update_data = array(
								'service_charge' => $service_charge,
								'gst_charge' => $gst_charge,
								'tax_charge' => $tax_charge,
								'buffer_cancellation_policy' => json_encode($cancellation, true),
								'policy_description' => $cancellation_txt,
			);
			
			
			$condition = array(
								'id' => $cart_details->referal_id
		   	);
		   	$schema = 'cart_hotel'; 
		}
		elseif($post_data['product_id'] == 19){
			$update_data = array(
								'service_charge' => $service_charge,
								'gst_charge' => $gst_charge,
								'tax_charge' => $tax_charge,
								 'caneclation_policy' => json_encode($cancellation),
			);
		   	$condition = array(
								'cart_transfer_id' => $cart_details->referal_id
		   	);
			$schema = 'cart_transfer';
		}
       else {
			
		}
		
		
		$this->cart_model->update_cart($schema, $update_data, $condition);
		
		$prepare_data['prepare_data']['discount'] = number_format($prepare_data['prepare_data']['discount'] * $exchange_rate, 2);
		$prepare_data['prepare_data']['sub_total_cost'] = number_format($prepare_data['prepare_data']['sub_total_cost'] * $exchange_rate,2);
		$prepare_data['prepare_data']['total_cost'] = number_format($prepare_data['prepare_data']['total_cost'],2);
		if(isset($prepare_data['prepare_data']['CC_fees']) && $prepare_data['prepare_data']['CC_fees'] != ''){
		$prepare_data['prepare_data']['CC_fees'] = number_format($prepare_data['prepare_data']['CC_fees'] * $exchange_rate,2);	
		} 
		
		if(isset($prepare_data['prepare_data']['deposit_amount']) && $prepare_data['prepare_data']['deposit_amount']!= ''){
		$prepare_data['prepare_data']['deposit_amount'] = number_format(($prepare_data['prepare_data']['deposit_amount'] / $exchange_rate)*$exchange_rate,2);	
		} 
		
		if(isset($prepare_data['prepare_data']['transaction_amount']) && $prepare_data['prepare_data']['transaction_amount']!= ''){
		$prepare_data['prepare_data']['transaction_amount'] = number_format(($prepare_data['prepare_data']['transaction_amount'] *$exchange_rate),2);	
		} 
		
		if(isset($prepare_data['prepare_data']['total_cost']) && $prepare_data['prepare_data']['total_cost'] != ''){
		$prepare_data['prepare_data']['total_cost'] = number_format(($prepare_data['prepare_data']['total_cost']  * $exchange_rate) ,2);	
		}
		
		$prepare_data['prepare_data']['status'] = 1;
		return $prepare_data;
	}
	function calculate_price() {
		//~ echo "data: <pre>";print_r($_POST);
		if($_POST['product_id'] == 1) {
		   $cart_details = $this->cart_model->getBookingTemp_hotel($_POST['cart_id']);	
		}
		elseif($_POST['product_id'] == 19){
			$cart_details = $this->cart_model->getBookingTemp_transfer($_POST['cart_id']);	
		}
		else {
			
		}
		//echo "cart_details: <pre>";print_r($cart_details);
		if(count($cart_details) > 0){
			$payment_logic = $this->input->post('payment_logic');
			$payment_type = $this->input->post('payment_type');
			
			$promo_signature = $this->input->post('promo_signature');
			
			$cost_details = $this->prepare_calculation($_POST,$_POST['cart_id']);

			//~ echo "prepared data:<pre>";print_r($cost_details);exit;
			echo json_encode($cost_details['prepare_data']);
		}
		else {
			#Todo
		}
	}
	function processing_promo($post_data,$cart_details,$total_amount) {
		$prepare_data = array();
		
		$product_id = $cart_details->product_id;
		if($product_id == 1) {
			$travel_date = $cart_details->checkin;
			$module = "Hotels";
		}
		elseif($product_id == 19) {
			$travel_date = $cart_details->travel_start_date;	
			$module = "Transfer";
		}
		else {
			#Todo
		}
		$booking_date = date('Y-m-d');
		$theme['promo_details'] = $promo_details = $this->General_Model->get_promo_details($module, $post_data['promo_code'],$travel_date, $booking_date, $total_amount);
		if($promo_details != ''){
			$promo_offer = $this->General_Model->promo_calcualtion($total_amount,$promo_details->promo_type, $promo_details->discount);
			$update_global_data = array(
										'promo_code' => $promo_details->promo_code,
										'discount' => $promo_offer['discount']
								);
			$this->cart_model->update_cart('cart_global',$update_global_data, array('cart_id'=>$cart_details->cart_id));
			$promo_theme = $this->load->view('booking/applied_promo',$theme,true);
			
			$prepare_data['prepare_data']['msg'] = 'Promo code applied successfully!';
			$prepare_data['prepare_data']['promo_theme'] = $promo_theme;
			$prepare_data['prepare_data']['discount'] = $promo_offer['discount'];
			$prepare_data['prepare_data']['promo_status'] = 1;
		}
		else {
			$prepare_data['prepare_data']['msg'] = 'Not a valid promo code!';
			$prepare_data['prepare_data']['promo_status'] = 2;
		}
		return $prepare_data;
	}

	function calculate_price_old() {
		//~ echo "data: <pre>";print_r($_POST);
		if($_POST['product_id'] == 1) {
		   $cart_details = $this->cart_model->getBookingTemp_hotel($_POST['cart_id']);	
		}
		elseif($_POST['product_id'] == 19){
			$cart_details = $this->cart_model->getBookingTemp_transfer($_POST['cart_id']);	
		}
		else {
			
		}
		//~ echo "cart data:<pre>";print_r($cart_details);
		if(count($cart_details) > 0){
			$payment_logic = $this->input->post('payment_logic');
			$payment_type = $this->input->post('payment_type');
			
			$promo_signature = $this->input->post('promo_signature');
			
			$this->prepare_calculation($this->input->post(),$cart_details);
			$prepare_data = array();
			
			$prepare_data['payment_logic'] = $payment_logic;
			$prepare_data['payment_type'] = $payment_type;
			
			//~ Tax Calculation
			
			$service_charge = $cart_details->service_charge;
			$gst_charge = $cart_details->gst_charge;
			$tax_charge = $cart_details->tax_charge;
			
			$total_tax = $service_charge + $gst_charge + $tax_charge;
			
			$prepare_data['service_charge'] = $service_charge;
			$prepare_data['gst_charge'] = $gst_charge;
			$prepare_data['tax_charge'] = $tax_charge;
			$prepare_data['total_tax'] = $total_tax;
			
			//~ Promocode Calculation  
			
			if($cart_details->promo_code != '' || $cart_details->promo_code != NULL) {
				$promo_code = $cart_details->promo_code;
				$discount = $cart_details->discount;
			}
			else {
				$promo_code = null;
				$discount = 0;
			}
			$prepare_data['promo_code'] = $promo_code;
			$prepare_data['discount'] = $discount;
			
			if($payment_logic == 'NET') {
				if($this->session->userdata('user_type') == 2):
					$net_rate = $cart_details->net_rate;
				elseif($this->session->userdata('user_type') == 4):
					$net_rate = $cart_details->sub_agent_baseprice;
				else:
					#Todo
				endif;
				
				$prepare_data['sub_total_cost'] = $net_rate;
				
				$total_cost_after_promo = $net_rate - $discount;
				$total_cost_after_promo_with_tax = $total_cost_after_promo + $total_tax;
				
				if($payment_type == "DEPOSIT") {
					$total_cost = $total_cost_after_promo_with_tax;
					$prepare_data['total_cost'] = $total_cost;
				}
				elseif($payment_type == "PG") {
					$CC_transaction_fee = 2.63; // In Percentage
					$transaction_details = $this->percentage_conversion($total_cost_after_promo_with_tax, $CC_transaction_fee);
					$CC_fees = $transaction_details['CC_fees'];
					$transaction_amount = $transaction_details['total_fees'];
					
					$total_cost = $transaction_amount;
					
					$prepare_data['CC_fees'] = $CC_fees;
					$prepare_data['total_cost'] = $total_cost;
				}
				elseif($payment_type == "BOTH") {
					$deposit_payment = $this->input->post('deposit_payment');
					$remaining_payment = $total_cost_after_promo_with_tax - $deposit_payment;
					
					$CC_transaction_fee = 2.63; // In Percentage
					$transaction_details = $this->percentage_conversion($total_cost_after_promo_with_tax, $CC_transaction_fee);
					$CC_fees = $transaction_details['CC_fees'];
					$transaction_amount = $transaction_details['total_fees'];
					
					$total_cost = ($transaction_amount + $remaining_payment);
					
					$prepare_data['CC_fees'] = $CC_fees;
					$prepare_data['total_cost'] = $total_cost;
				}
				elseif($payment_type == "PAYLATER") {
					$total_cost = $total_cost_after_promo_with_tax;
					
					$prepare_data['total_cost'] = $total_cost;
				}
				else {
					#Todo
				}
			}
			elseif($payment_logic == 'COMMISSIONABLE') {
				if($this->session->userdata('user_type') == 2):
					$commissionable_rate = $cart_details->agent_baseprice;
				elseif($this->session->userdata('user_type') == 4):
					$commissionable_rate = $cart_details->b2b2b_baseprice;
				else:
				endif;
				
				$prepare_data['sub_total_cost'] = $commissionable_rate;
				
				$total_cost_after_promo = $commissionable_rate - $discount;
				$total_cost_after_promo_with_tax = $total_cost_after_promo + $total_tax;
				
				if($payment_type == "DEPOSIT") {
					$total_cost = $total_cost_after_promo_with_tax;
					
					$prepare_data['total_cost'] = $total_cost;
				}
				elseif($payment_type == "PG") {
					$CC_transaction_fee = 2.63; // In Percentage
					$transaction_details = $this->percentage_conversion($total_cost_after_promo_with_tax, $CC_transaction_fee);
					$CC_fees = $transaction_details['CC_fees'];
					$transaction_amount = $transaction_details['total_fees'];
					
					$total_cost = $transaction_amount;
					
					$prepare_data['CC_fees'] = $CC_fees;
					$prepare_data['total_cost'] = $total_cost;
				}
				elseif($payment_type == "BOTH") {
					$deposit_payment = $this->input->post('deposit_payment');
					$remaining_payment = $total_cost_after_promo_with_tax - $deposit_payment;
					
					$CC_transaction_fee = 2.63; // In Percentage
					$transaction_details = $this->percentage_conversion($remaining_payment, $CC_transaction_fee);
					$CC_fees = $transaction_details['CC_fees'];
					$transaction_amount = $transaction_details['total_fees'];
					
					$total_cost = ($transaction_amount + $deposit_payment);
					
					$prepare_data['CC_fees'] = $CC_fees;
					$prepare_data['transaction_amount'] = $transaction_amount;
					$prepare_data['total_cost'] = $total_cost;
				}
				elseif($payment_type == "PAYLATER") {
					$total_cost = $total_cost_after_promo_with_tax;
					
					$prepare_data['total_cost'] = $total_cost;
				}
				else {
					#Todo
				}
			}
			else {
				#Todo
			}
			$prepare_data['status'] = 1;
			//~ echo "prepared data:<pre>";print_r($prepare_data);exit;
			echo json_encode($prepare_data);
		}
		else {
			#Todo
		}
	}
	function percentage_conversion($total_cost, $CC_transaction_fee) {
		//~ echo "total_cost: <pre>";print_r($total_cost);
		$data['CC_fees'] = $CC_fees = $total_cost *($CC_transaction_fee/100);
		$data['total_fees'] = $total_fees = $CC_fees + $total_cost;
		//~ echo "data: <pre>";print_r($total_fees);exit;
		return $data;
	}
}
?>
