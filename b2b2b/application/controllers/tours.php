<?php
if (! defined ( 'BASEPATH' ))
exit ( 'No direct script access allowed' );
class Tours extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$current_url = $_SERVER ['QUERY_STRING'] ? '?' . $_SERVER ['QUERY_STRING'] : '';
		$current_url = $this->config->site_url () . $this->uri->uri_string () . $current_url;
		$url = array (
				'continue' => $current_url 
		);
		$this->session->set_userdata ( $url );
		$this->helpMenuLink = "";
		$this->load->model ( 'Help_Model' );
		$this->helpMenuLink = $this->Help_Model->fetchHelpLinks ();
		$this->load->model ( 'Package_Model' );
	}

	/**
	 * get all tours
	 */
	public function index() {
		$domain_id=$this->session->userdata('branch_id');
		$data ['packages'] = $this->Package_Model->getAllPackages ($domain_id); 
		$data ['countries'] = $this->Package_Model->getPackageCountries ($domain_id);
		$data ['package_types'] = $this->Package_Model->getPackageTypes ();

		if (! empty ( $data ['packages'] )) {
			$this->load->view ( 'holiday/tours', $data );
		} else {
			redirect ();
		}
	}
	/**
	 * get the package details
	 */
	public function details($package_id) {
		$data ['package'] = $this->Package_Model->getPackage ( $package_id );
		$data ['package_itinerary'] = $this->Package_Model->getPackageItinerary ( $package_id );
		$data ['package_price_policy'] = $this->Package_Model->getPackagePricePolicy ( $package_id );
		$data ['package_cancel_policy'] = $this->Package_Model->getPackageCancelPolicy ( $package_id );
		$data ['package_traveller_photos'] = $this->Package_Model->getTravellerPhotos ( $package_id );
		if (! empty ( $data ['package'] )) {
			$this->load->view('holiday/tours_detail', $data );
		} else {
			redirect ( "tours/index" );
		}
	}
	public function enquiry() {
		$package_id = $this->input->post ( 'package_id' );
		if ($package_id !='') {
			$data = $this->input->post ();
			$package = $this->Package_Model->getPackage ( $package_id );
			$data ['package_name'] = $package->package_name;
			$data ['package_duration'] = $package->duration;
			$data ['package_type'] = $package->package_type;
			$data ['with_or_without'] = $package->price_includes;
			$data ['package_description'] = $package->package_description;
			$data ['ip_address'] = $this->session->userdata ( 'ip_address' );
			$data ['status'] = '0';
			$data ['date'] = date ( 'Y-m-d' );
			$data ['domain_list_fk'] = get_domain_auth_id ();
				
			$result = $this->Package_Model->saveEnquiry ( $data );
			$data ['sucess'] = "Thank you for submitting your enquiry for this package, will get back to soon";
			$this->session->set_flashdata(array('message' => "Thank you for submitting your enquiry for this package, will get back to you soon", 'type' => SUCCESS_MESSAGE));
			redirect ( 'tours/details/' . $package_id );
		} else {
			redirect ();
		}
	}

	function temp_index($search_id)
	{
		$this->load->model('hotel_model');
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		// Get all the hotels bookings source which are active
		$active_booking_source = $this->hotel_model->active_booking_source();
		if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
			$safe_search_data['data']['search_id'] = abs($search_id);
			$this->load->view('tours/search_result_page', array('hotel_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
		} else {
			$this->load->view ( 'general/popup_redirect');
		}
	}

	public function search() {
		//error_reporting(E_ALL);
		$domain_id=$this->session->userdata('branch_id');
		$data = $this->input->get ();
		//debug($this->session->all_userdata());die;
		$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		if (! empty ( $data )) {
			$country = $data ['country'];
			$packagetype = $data ['package_type'];
			if ($data ['duration']) {
				$duration = explode ( '-', $data ['duration'] );
				if (count ( $duration ) > 1) {
					$duration = "duration between " . $duration ['0'] . " AND " . $duration ['1'];
				} else {
					$duration = "duration >" . $duration ['0'];
				}
			} else {
				$duration = $data ['duration'];
			}
			if ($data ['budget']) {
				$budget = explode ( '-', $data ['budget'] );

				if (count ( $budget ) > 1) {
					$budget = "price between " . $budget ['0'] . " AND " . $budget ['1'];
				} else if ($budget [0]) {
					$budget = "price >" . $budget ['0'];
				}
			} else {
				$budget = $data ['budget'];
			} 
			$domail_list_pk = get_domain_auth_id ();
			$data['scountry'] = $country;
			$data['spackage_type'] = $packagetype;
			$data['sduration'] = $data ['duration'];
			$data['sbudget'] = $data ['budget'];
			$data['packages'] = $this->Package_Model->search($country, $packagetype, $duration, $budget, $domail_list_pk,$domain_id);
			$data['caption'] = $this->Package_Model->getPageCaption ('tours_packages' )->row ();
			//$data['countries'] = $this->Package_Model->getPackageCountries();
			$data['countries'] = $this->Package_Model->getPackageCountries($this->session->userdata('domain_id'));
			$data['package_types'] = $this->Package_Model->getPackageTypes ($this->session->userdata('domain_id'));
			$data['currency_obj'] = $currency_obj;
			
			$this->load->view ( 'holiday/tours', $data );
		} else {
			redirect ( 'tours/all_tours' );
		}
	}
	function package_user_rating() {
		$rate_data = explode ( ',', $_POST ['rate'] );
		$pkg_id = $rate_data [0];
		$rating = $rate_data [1];

		$arr_data = array (
				'package_id' => $pkg_id,
				'rating' => $rating 
		);
		$res = $this->Package_Model->add_user_rating ( $arr_data );
	}
	public function all_tours() {
		$data ['caption'] = $this->Package_Model->getPageCaption ( 'tours_packages' )->row ();
		$data ['packages'] = $this->Package_Model->getAllPackages ();
		$data ['countries'] = $this->Package_Model->getPackageCountries ();
		$data ['package_types'] = $this->Package_Model->getPackageTypes ();
		if (! empty ( $data ['packages'] )) {
			print_r($data);die('heyyyyyyy');
			$this->load->view ( 'holiday/tours', $data );
		} else {
			redirect ();
		}
	}
}
