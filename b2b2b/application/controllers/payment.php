<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
if(session_status() == PHP_SESSION_NONE){ session_start(); }
class Payment extends CI_Controller {
	
    public function __construct(){
		parent::__construct();	
		$this->check_isvalidated();
		$this->load->model('General_Model');
		$this->load->model('Account_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Usermanagement_Model');
		$this->load->model('Booking_Model');
		$this->load->model('Validation_Model');
		$this->load->model('cart_model');
		$this->lang->load('english','Dynamic_Languages');
		$this->thechinagap = $this->lang->line('TheChinaGap');
	}
	
	private function check_isvalidated()
	{
		if($this->session->userdata('user_details_id'))
		{
			$user_id = $this->session->userdata('user_details_id');
			$controller_name = $this->router->fetch_class();
			$function_name = $this->router->fetch_method();
			$this->load->model('Privilege_Model');
            $sub_admin_id = $this->session->userdata('admin_id');
            $privilege_details = $this->Privilege_Model->get_user_privileges($user_id,$controller_name,$function_name, $function_parameter = 1)->row();
               //~ if(count($privilege_details)  == 0)
               if(false)
				{	
				  redirect('error/access_denied');
				}
		}
	}
	function payment_view($parent_pnr,$product_id){ 
		$branch_id = $this->session->userdata('branch_id');
		if(!empty($parent_pnr) && !empty($product_id)){
			$parent_pnr = json_decode(base64_decode($parent_pnr));
			//~ $data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();
			$data['booking_details'] = $this->Booking_Model->getBookingbyPnr($parent_pnr,$product_id)->result();
			//~ echo "data: <pre>";print_r($data['booking_details']);exit;
			$this->load->view('dashboard/payment/payment_view',$data);
		}
		else {
			#Todo
		}
	}
	function do_payment($parent_pnr,$product_id) {
		if(!empty($parent_pnr) && !empty($product_id)) {
			$parent_pnr_encoded = $parent_pnr;
			$parent_pnr = json_decode(base64_decode($parent_pnr));
			$checkout_form = $this->input->post();
			$checkout_form['product_id'] = $product_id;
			//~ echo "data: <pre>";print_r($this->input->post());exit;
			
			$payment_logic = $checkout_form['payment_logic'];
			$payment_type = $checkout_form['payment_type'];
			if($payment_type == 'PG') {
				$payment_type = 'CREDITCARD';
			}
			
			$user_type = $this->session->userdata('user_type');
			$user_id = $this->session->userdata('user_details_id');
			
			$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
			
			if($count > 0) {
				$data['pnr_nos'] = $pnr_nos = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();
				$data['booking_details'] = $booking_details = $this->Booking_Model->getBookingbyPnr($pnr_nos[0]->pnr_no,$product_id)->result();
				$cost_details = $this->prepare_calculation($checkout_form,$pnr_nos[0]->pnr_no);
				//~ echo "cost_details: <pre>";print_r($cost_details);exit;
				
				if(!empty($cost_details) && $cost_details != '') {
					
					//~ Deposit Verification 
			
					$userInfo =$this->General_Model->get_user_details($user_id, $user_type);
					if($payment_type == "DEPOSIT" ) {
						$status = $this->b2b_check_payment(str_replace(',','',$cost_details['prepare_data']['total_cost']), $user_id);
						if($status) {
							
						}
						else {
							if($status == false):
								$data['status'] = -2;
								echo json_encode($data);die;
								# Todo
							endif;
						}
					}
					elseif($payment_type == "BOTH" ) {
						#Checking Deposit amount
						$status = $this->b2b_check_payment(str_replace(',','',$cost_details['prepare_data']['deposit_amount']), $user_id);
						if($status) {
							
						}
						else {
							#Redirecting Payment Gateway
							$both_flag = true;
							$payment_type == "CREDITCARD";
							#Todo
						}
					}
					else {
						
					}
					
					//~ End of deposit verification
					
					if(isset($cost_details['prepare_data']['CC_fees']) && $cost_details['prepare_data']['CC_fees'] != '') {
						$PG_Charge = str_replace(',','',$cost_details['prepare_data']['CC_fees']);
					}
					else {
						$PG_Charge = 0;
					}
					if(isset($cost_details['prepare_data']['deposit_amount']) && $cost_details['prepare_data']['deposit_amount'] != '') {
						$deposit_amount = str_replace(',','',$cost_details['prepare_data']['deposit_amount']);
					}
					else {
						$deposit_amount = 0;
					}
					if(isset($cost_details['prepare_data']['transaction_amount']) && $cost_details['prepare_data']['transaction_amount'] != '') {
						$transaction_amount = str_replace(',','',$cost_details['prepare_data']['transaction_amount']);
						$transaction_amount = ceil($transaction_amount);
					}
					else {
						$transaction_amount = 0;
					}
					
					$booking_transaction = array(
												'total_amount' => str_replace(',','',$cost_details['prepare_data']['total_cost']),
												'PG_Charge' => $PG_Charge,
												'total_PG_payble' => $transaction_amount,
												'wallet_Charge' => $deposit_amount
											); 
					$this->Booking_Model->updateTransactions($booking_details[0]->booking_transaction_id,$booking_transaction);
				
					$booking_payment = array(
											'payment_logic' =>$payment_logic,
											'payment_type' =>$payment_type,
											'paylater_flag' =>true,
											'payment_gateway_id' => 1,
											'amount' => str_replace(',','',$cost_details['prepare_data']['total_cost']),
											'payment_status' => 'PENDING',
											'pg_markup' => $PG_Charge
										);
					$this->Booking_Model->Update_Booking_payment($booking_details[0]->payment_id, $booking_payment);
					
					$GateURL = base_url().'payment/doPaymentGate/'.$parent_pnr_encoded;
					$data['status'] = 555;
					$data['GateURL'] = $GateURL;
					echo json_encode($data);
				}
			}
		}
		else {
			#Todo
		}
	}
	
	public function doPaymentGate($parent_pnr) {
		//~ echo "parent_pnr: <pre>";print_r($parent_pnr);exit;
        $parent_pnr1 = $parent_pnr;
        $data['OrderId'] = $parent_pnr = json_decode(base64_decode($parent_pnr1));
        $data['payment_mode'] = $payment_mode = '1';
        $count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
        if($count > 0){
			$data['pnr_nos'] = $pnr_nos = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();
			//~ echo "booking data: <pre>";print_r($pnr_nos);
            foreach ($pnr_nos as $key => $pnr) {
				
                //$getBookingHotelData = $this->Booking_Model->getBookingHotelData($pnr->referal_id)->row();
               $booking = $this->Booking_Model->getBookingbyPnr($pnr->pnr_no,$pnr->product_id)->row();
				//~ echo "booking data: <pre>";print_r($booking);exit;
				if($booking->user_type_name == 'B2B'):
					if ($booking->payment_type == 'DEPOSIT') {
						$Total[] = $booking->total_amount;
						$wallet_debit[] = $booking->total_amount; // Deducting total amount irrespective of B2B Markup
					}
					elseif($booking->payment_type == 'CREDITCARD') {
						$Total[] = $booking->total_amount;
						$wallet_debit[] = 0;
					}
					elseif($booking->payment_type == 'BOTH') {
						$Total[] = $booking->total_amount;
						$wallet_debit[] = $booking->wallet_Charge;
					}
					else {
						#Todo - Booking Failed
						die();
					}
				elseif($booking->user_type_name == 'B2B2B'):
					if ($booking->payment_type == 'DEPOSIT') {
						$Total[] = $booking->total_amount;
						$wallet_debit[] = $booking->total_amount; // Deducting total amount irrespective of B2B2B Markup
					}
					elseif($booking->payment_type == 'CREDITCARD') {
						$Total[] = $booking->total_amount;
						$wallet_debit[] = 0;
					}
					elseif($booking->payment_type == 'BOTH') {
						$Total[] = $booking->total_amount;
						$wallet_debit[] = $booking->wallet_Charge;
					}
					else {
						#Todo
					}
				else:
					#Todo
				endif; 
            } 
            
            /* Initiating Payment */
            
            $booking_transaction_data = array(
												'transaction_amount' => ceil(array_sum($Total))
										);
            $this->Booking_Model->update_booking_transaction_data($pnr_nos[0]->booking_transaction_id, $booking_transaction_data);
            
            $wallet_debit_charge = array_sum($wallet_debit);
            $user_type = $this->session->userdata('user_type');
			$user_id = $this->session->userdata('user_details_id');
			$branch_id = $this->session->userdata('branch_id');
            
            if($booking->payment_type == 'DEPOSIT') {
				$deposit_amount_det = $this->General_Model->getAgentBalance()->row();
				$credit_amount = $deposit_amount_det->balance_credit;
				$this->do_deduction($wallet_debit_charge,$credit_amount,$user_id);
			}
            elseif($booking->payment_type == 'CREDITCARD') {
				#No Deduction
			}
            elseif($booking->payment_type == 'BOTH') {
				#Deducting Deposit Amount From Wallet
				$deposit_amount_det = $this->General_Model->getAgentBalance()->row();
				$credit_amount = $deposit_amount_det->balance_credit;
				$this->do_deduction($wallet_debit_charge,$credit_amount,$user_id);
			}
			else {
				#Todo
			}
            
            $this->preparePaymentGateway($parent_pnr1,$pnr_nos[0]->product_id);
            
            /* End of Initiating Paymnet */      
        }
        else{
            $this->load->view('errors/expiry');
        }
    }
	
    public function preparePaymentGateway($parent_pnr,$product_id) {
		$parent_pnr_encoded = $parent_pnr;
		$parent_pnr = json_decode(base64_decode($parent_pnr));
		$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
		$booking_data = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();
		//~ echo "data: <pre>";print_r($booking_data);exit;
		
        if($count > 0) {
			if($booking_data[0]->payment_type == "DEPOSIT") {
				$update_payment = array(
										'payment_gateway_status' => 'SUCCESS',
										'transaction_status' => 'SUCCESS',
										'payment_status' => 'SUCCESS'
				);
				$this->Booking_Model->Update_Booking_payment($booking_data[0]->payment_id, $update_payment);
				redirect('bookings');
			}
			elseif($booking_data[0]->payment_type == "CREDITCARD" || $booking_data[0]->payment_type == "BOTH") {
				//~ echo "data: <pre>";print_r($booking_data);
				$booking = $this->Booking_Model->getBookingbyPnr($booking_data[0]->pnr_no,$booking_data[0]->product_name)->row();
				//~ echo "booking data: <pre>";print_r($booking);exit;
				
				//~ $vpc_Amount = round($booking->transaction_amount, 2);
				if($booking_data[0]->payment_type == "BOTH") {
					$vpc_Amount = intval(str_replace('.', '', $booking->total_PG_payble));
				}
				else {
					$vpc_Amount = intval(str_replace('.', '', $booking->transaction_amount));
				}
				
				$data['user_SessionId'] = $user_SessionId = "gdgrfggfrgr65656";
				$data['vpc_AccessCode'] = $vpc_AccessCode = "4F604EBC";
				$data['vpc_Amount'] = $vpc_Amount;
				$data['vpc_Command'] = $vpc_Command = "pay";
				$data['vpc_Currency'] = $vpc_Currency = "AUD";
				$data['vpc_MerchTxnRef'] = $vpc_MerchTxnRef = $booking->transaction_reference;
				$data['vpc_Merchant'] = $vpc_Merchant = "TESTCHIGAPCOM01";
				$data['vpc_OrderInfo'] = $vpc_OrderInfo = base64_encode(json_encode($parent_pnr."|||".$booking->payment_id));
				$data['vpc_ReturnURL'] = $vpc_ReturnURL = base_url()."payment/payment_success";
				$data['vpc_Version'] = $vpc_Version = "1";
				
				$preparedString = 'user_SessionId='.$user_SessionId.'&vpc_AccessCode='.$vpc_AccessCode.'&vpc_Amount='.$vpc_Amount.'&vpc_Command='.$vpc_Command.
								'&vpc_Currency='.$vpc_Currency.'&vpc_MerchTxnRef='.$vpc_MerchTxnRef.'&vpc_Merchant='.$vpc_Merchant.'&vpc_OrderInfo='.$vpc_OrderInfo.
								'&vpc_ReturnURL='.$vpc_ReturnURL.'&vpc_Version='.$vpc_Version;
				
				$merchantSecret = 'AD2B21DD38FAF085C7C97815FBF5610D';
				$key = pack('H*', $merchantSecret);
				$data['secureHash'] = $secureHash = hash_hmac('sha256', $preparedString, $key);
				
				$data['payment_url'] = $payment_url = "https://migs.mastercard.com.au/vpcpay";
				$this->load->view('booking/initiate_payment',$data);
			}
			else {
				#Todo
				die();
			}
		}
		else {
			$this->load->view('errors/expiry');
		}
	}
    public function payment_success() {
		//~ echo "Payment Response: <pre>";print_r($this->input->get());exit;
		$payment_response = json_encode($this->input->get());
		$vpc_AcqResponseCode = $this->input->get('vpc_AcqResponseCode');
		$vpc_Amount = $this->input->get('vpc_Amount');
		$vpc_Card = $this->input->get('vpc_Card');
		$vpc_Currency = $this->input->get('vpc_Currency');
		$vpc_Message = $this->input->get('vpc_Message');
		$vpc_OrderInfo = $this->input->get('vpc_OrderInfo');
		
		$vpc_OrderInfo_explosion = explode("|||",json_decode(base64_decode($vpc_OrderInfo)));
		$parent_pnr = $vpc_OrderInfo_explosion[0];
		$payment_id = $vpc_OrderInfo_explosion[1];
		
		$parent_pnr_encoded = base64_encode(json_encode($parent_pnr));
		
		$vpc_TransactionNo = $this->input->get('vpc_TransactionNo');
		$vpc_TxnResponseCode = $this->input->get('vpc_TxnResponseCode');
		
		if($vpc_Message == 'Approved' && $vpc_TxnResponseCode == 0) {
			$payment_status = 'SUCCESS';
			$payment_gateway_status = 'SUCCESS';
		}
		else {
			$payment_status = 'FAILED';
			$payment_gateway_status = 'FAILURE';
		}
		
		$update_booking_payment = array(
										'transaction_id' => $vpc_TransactionNo,
										'payment_response' => $payment_response,
										'transaction_status' => $vpc_Message,
										'payment_gateway_status' => $payment_gateway_status,
										'payment_status' => $payment_status,
										'vpc_Card' => $vpc_Card
								);
		//~ echo "Payment Response: <pre>";print_r($update_booking_payment);exit;				
		$this->Booking_Model->Update_Booking_payment($payment_id, $update_booking_payment);
		if($payment_status == "SUCCESS") {
			//Email Management
			
			$user_type = $this->session->userdata('user_type');
			$user_id = $this->session->userdata('user_details_id');
			$userInfo =$this->General_Model->get_user_details($user_id, $user_type);
			//echo 'sanjay<pre>'; print_r($userInfo); exit();
			//$this->Email_Model->sendmail_paymentsuccess($email,$data);
			# Email For Successful Payment Transaction
			redirect('bookings');
		}
		else {
			# Email For Failed Payment Transaction
			redirect('booking/payment_failed/'.$parent_pnr_encoded);
		}
	}
	public function payment_failed($parent_pnr = "") {
		$data['title'] = "Transaction Failure";
		$data['caption'] = "Payment Failed!";
		$data['message'] = "Transaction process could not be successful. Sorry for the inconvinience!";
		$data['image_url'] = base_url()."assets/images/faild_payment.png";
		$this->load->view('general/error_info',$data);
	}
	public function b2b_check_payment($payable_amount,$user_id){
    	//~ Checking for multilpe conditions; I. Pay From Wallet, II. Payment Gateway, III. Pay Later, IV. Pay Partially From Wallet and rest by Payment Gateway, etc. 
		//~ I. Pay From Wallet
		$deposit_amount_det = $this->General_Model->getAgentBalance()->row();
    	$credit_amount = $deposit_amount_det->balance_credit;
    	if($payable_amount <= $credit_amount):
			//~ $this->do_deduction($payable_amount,$credit_amount,$user_id);
			return true;
		else:
			return false;
    	endif;
		 
		return true;
    }
    public function do_deduction($deduct_amount, $wallet_balance, $user_id) {
		$balance_credit = $wallet_balance - $deduct_amount;
		$update_credit_amount = array(
			'balance_credit' => $balance_credit,
			'last_debit' => $deduct_amount
		);
		$this->Account_Model->update_credit_amount($update_credit_amount,$user_id);	
		# Email For Deposit Deduction
	}

	function prepare_calculation($post_data,$cid) {
		$booking_details = $this->Booking_Model->getBookingbyPnr($cid,$post_data['product_id'])->row();
		//~ echo "booking_details: <pre>";print_r($booking_details);exit;
		
		$payment_logic = $post_data['payment_logic'];
		$payment_type = $post_data['payment_type'];
		$prepare_data = array();
			
		$prepare_data['prepare_data']['payment_logic'] = $payment_logic;
		$prepare_data['prepare_data']['payment_type'] = $payment_type;
		
		//~ Tax Calculation
			
		$service_charge = $booking_details->service_charge;
		$gst_charge = $booking_details->gst_charge;
		$tax_charge = $booking_details->tax_charge;
		
		$total_tax = number_format(($service_charge + $gst_charge + $tax_charge),2);
		
		$prepare_data['prepare_data']['service_charge'] = $service_charge;
		$prepare_data['prepare_data']['gst_charge'] = $gst_charge;
		$prepare_data['prepare_data']['tax_charge'] = $tax_charge;
		$prepare_data['prepare_data']['total_tax'] = $total_tax;
		
		//~ Promocode Calculation  
		
		if($booking_details->promo_code != '' || $booking_details->promo_code != NULL) {
			$promo_code = $booking_details->promo_code;
			$discount = $booking_details->discount;
		}
		else {
			$promo_code = null;
			$discount = 0;
		}
		$prepare_data['prepare_data']['promo_code'] = $promo_code;
		$prepare_data['prepare_data']['discount'] = $discount;
		
		if($payment_logic == 'NET') {
			if($this->session->userdata('user_type') == 2):
				$net_rate = $booking_details->admin_baseprice;
			elseif($this->session->userdata('user_type') == 4):
				$net_rate = $booking_details->sub_agent_baseprice;
			else:
				#Todo
			endif;
			
			$prepare_data['prepare_data']['sub_total_cost'] = $net_rate;
			
			//~ Tax Calculation
			
			if($this->session->userdata('user_country_id') != 0){
				$country_name = $this->General_Model->get_country_name($this->session->userdata('user_country_id'))->row();
				$tax_details = $this->General_Model->get_tax_details($country_name->country_name, $this->session->userdata('user_state_id'))->row();
			
				if(!empty($tax_details) && count($tax_details) > 0){
					$tax_charge_details = $this->General_Model->tax_calculation($tax_details, $net_rate);	
					
					if(!empty($tax_charge_details) && $tax_charge_details != '') {
						$service_charge = $tax_charge_details['sc_tax'];
						$gst_charge = $tax_charge_details['gst_tax'];
						$tax_charge = $tax_charge_details['state_tax'];
						
						$total_tax = number_format(($service_charge + $gst_charge + $tax_charge),2);
						
						$prepare_data['prepare_data']['service_charge'] = $service_charge;
						$prepare_data['prepare_data']['gst_charge'] = $gst_charge;
						$prepare_data['prepare_data']['tax_charge'] = $tax_charge;
						$prepare_data['prepare_data']['total_tax'] = $total_tax; 
						
						//~ Updating Booking Transaction
						
						/* Disabling Updating Tax to Booking Transaction 
						 * 
						 * 
						 * */

						$booking_transaction_data = array(
														'service_charge' => $service_charge,
														'gst_charge' => $gst_charge,
														'tax_charge' => $tax_charge
													);
						$this->Booking_Model->update_booking_transaction_data($booking_details->booking_transaction_id, $booking_transaction_data);
								
						/*
						 * 
						 *  End of Disabling Updating Tax to Booking Transaction */
									
					}
				}
				else{
					$tax_charge_details = array();
				}
			}
			
			//~ End of Tax Calculation
			
			$total_cost_after_promo = $net_rate - $discount;
			$total_cost_after_promo_with_tax = $total_cost_after_promo + str_replace(',','',$total_tax);
			
			if($payment_type == "DEPOSIT") {
				$total_cost = number_format($total_cost_after_promo_with_tax,2);
				$prepare_data['prepare_data']['total_cost'] = $total_cost;
			}
			elseif($payment_type == "PG") {
				$CC_transaction_fee = 2.63; // In Percentage
				$transaction_details = $this->percentage_conversion(str_replace(',','',$total_cost_after_promo_with_tax), $CC_transaction_fee);
				$CC_fees = number_format($transaction_details['CC_fees'],2);
				$transaction_amount = number_format($transaction_details['total_fees'],2);
				
				$total_cost = $transaction_amount;
				
				$prepare_data['prepare_data']['CC_fees'] = $CC_fees;
				$prepare_data['prepare_data']['total_cost'] = $total_cost;
			}
			elseif($payment_type == "BOTH") {
				$deposit_payment = $post_data['deposit_payment'];
				$remaining_payment = number_format(($total_cost_after_promo_with_tax - $deposit_payment),2);
				
				$CC_transaction_fee = 2.63; // In Percentage
				$transaction_details = $this->percentage_conversion(str_replace(',','',$remaining_payment), $CC_transaction_fee);
				$CC_fees = number_format($transaction_details['CC_fees'],2);
				$transaction_amount = number_format($transaction_details['total_fees'],2);
				
				$total_cost = (str_replace(',','',$transaction_amount) + $deposit_payment);
				
				$prepare_data['prepare_data']['CC_fees'] = $CC_fees;
				$prepare_data['prepare_data']['transaction_amount'] = $transaction_amount;
				$prepare_data['prepare_data']['deposit_amount'] = $deposit_payment;
				$prepare_data['prepare_data']['total_cost'] = $total_cost;
			}
			elseif($payment_type == "PAYLATER") {
				$total_cost = number_format($total_cost_after_promo_with_tax,2);
				
				$prepare_data['prepare_data']['total_cost'] = $total_cost;
			}
			else {
				#Todo
			}
		}
		elseif($payment_logic == 'COMMISSIONABLE') {
			if($this->session->userdata('user_type') == 2):
				$commissionable_rate = $booking_details->agent_base_amount;
			elseif($this->session->userdata('user_type') == 4):
				$commissionable_rate = $booking_details->b2b2b_baseprice;
			else:
			endif;
			
			$prepare_data['prepare_data']['sub_total_cost'] = $commissionable_rate;
			
			//~ Tax Calculation
			
			if($this->session->userdata('user_country_id') != 0){
				$country_name = $this->General_Model->get_country_name($this->session->userdata('user_country_id'))->row();
				$tax_details = $this->General_Model->get_tax_details($country_name->country_name, $this->session->userdata('user_state_id'))->row();
			
				if(!empty($tax_details) && count($tax_details) > 0){
					$tax_charge_details = $this->General_Model->tax_calculation($tax_details, $commissionable_rate);	
					
					if(!empty($tax_charge_details) && $tax_charge_details != '') {
						$service_charge = $tax_charge_details['sc_tax'];
						$gst_charge = $tax_charge_details['gst_tax'];
						$tax_charge = $tax_charge_details['state_tax'];
						
						$total_tax = number_format(($service_charge + $gst_charge + $tax_charge),2);
						
						$prepare_data['prepare_data']['service_charge'] = $service_charge;
						$prepare_data['prepare_data']['gst_charge'] = $gst_charge;
						$prepare_data['prepare_data']['tax_charge'] = $tax_charge;
						$prepare_data['prepare_data']['total_tax'] = $total_tax; 
						
						//~ Updating Booking Transaction
						
						/* Disabling Updating Tax to Booking Transaction 
						 * 
						 * 
						 * */

						$booking_transaction_data = array(
														'service_charge' => $service_charge,
														'gst_charge' => $gst_charge,
														'tax_charge' => $tax_charge
													);
						$this->Booking_Model->update_booking_transaction_data($booking_details->booking_transaction_id, $booking_transaction_data);
								
						/*
						 * 
						 *  End of Disabling Updating Tax to Booking Transaction */
									
					}
				}
				else{
					$tax_charge_details = array();
				}
			}
			
			//~ End of Tax Calculation
			
			$total_cost_after_promo = $commissionable_rate - $discount;
			$total_cost_after_promo_with_tax = $total_cost_after_promo + str_replace(',','',$total_tax);
			
			if($payment_type == "DEPOSIT") {
				$total_cost = number_format($total_cost_after_promo_with_tax,2);
				
				$prepare_data['prepare_data']['total_cost'] = $total_cost;
			}
			elseif($payment_type == "PG") {
				$CC_transaction_fee = 2.63; // In Percentage
				$transaction_details = $this->percentage_conversion(str_replace(',','',$total_cost_after_promo_with_tax), $CC_transaction_fee);
				$CC_fees = number_format($transaction_details['CC_fees'],2);
				$transaction_amount = number_format($transaction_details['total_fees'],2);
				
				$total_cost = $transaction_amount;
				
				$prepare_data['prepare_data']['CC_fees'] = $CC_fees;
				$prepare_data['prepare_data']['total_cost'] = $total_cost;
			}
			elseif($payment_type == "BOTH") {
				$deposit_payment = $post_data['deposit_payment'];
				$remaining_payment = number_format(($total_cost_after_promo_with_tax - $deposit_payment),2);
				
				$CC_transaction_fee = 2.63; // In Percentage
				//~ echo "remaining paymnet before :<pre>";print_r($remaining_payment);
				$transaction_details = $this->percentage_conversion(str_replace(',','',$remaining_payment), $CC_transaction_fee);
				//~ echo "transaction_details: <pre>";print_r($transaction_details);exit;
				
				$CC_fees = number_format($transaction_details['CC_fees'],2);
				$transaction_amount = number_format($transaction_details['total_fees'],2);
				
				$total_cost = (str_replace(',','',$transaction_amount) + $deposit_payment);
				
				$prepare_data['prepare_data']['CC_fees'] = $CC_fees;
				$prepare_data['prepare_data']['transaction_amount'] = $transaction_amount;
				$prepare_data['prepare_data']['deposit_amount'] = $deposit_payment;
				$prepare_data['prepare_data']['total_cost'] = $total_cost;
			}
			elseif($payment_type == "PAYLATER") {
				$total_cost = number_format($total_cost_after_promo_with_tax,2);
				
				$prepare_data['prepare_data']['total_cost'] = $total_cost;
			}
			else {
				#Todo
			}
		}
		else {
			#Todo
		}

		$prepare_data['prepare_data']['status'] = 1;
		return $prepare_data;
	}
	function calculate_price() {
		//~ echo "data: <pre>";print_r($_POST);
		$booking_details = $this->Booking_Model->getBookingbyPnr($this->input->post('cart_id'),$this->input->post('product_id'))->result();
		//~ echo "cart data:<pre>";print_r($booking_details);exit;
		if(count($booking_details) > 0){
			$payment_logic = $this->input->post('payment_logic');
			$payment_type = $this->input->post('payment_type');
			
			$promo_signature = $this->input->post('promo_signature');
			
			$cost_details = $this->prepare_calculation($_POST,$_POST['cart_id']);
			//~ echo "promo_signature: <pre>";print_r($cost_details);exit;
			
			if($promo_signature == "SET" && ($cost_details['prepare_data']['promo_code'] == '' || $cost_details['prepare_data']['promo_code'] == NULL) ) {
				$total_amount = $cost_details['prepare_data']['sub_total_cost'];
				$promo_details = $this->processing_promo($_POST,$cart_details,$total_amount);
				$cost_details = $this->prepare_calculation($_POST,$_POST['cart_id']);
				
				$cost_details['prepare_data']['msg'] = $promo_details['prepare_data']['msg'];
				$cost_details['prepare_data']['promo_theme'] = $promo_details['prepare_data']['promo_theme'];
				$cost_details['prepare_data']['promo_status'] = $promo_details['prepare_data']['promo_status'];
			}
			else {
				$promo_details = array();
			}
			
			//~ echo "prepared data:<pre>";print_r($cost_details);exit;
			echo json_encode($cost_details['prepare_data']);
		}
		else {
			#Todo
		}
	}
	function processing_promo($post_data,$cart_details,$total_amount) {
		$prepare_data = array();
		
		$product_id = $cart_details->product_id;
		if($product_id == 1) {
			$travel_date = $cart_details->checkin;
			$module = "Hotels";
		}
		elseif($product_id == 19) {
			$travel_date = $cart_details->travel_start_date;	
			$module = "Transfer";
		}
		else {
			#Todo
		}
		$booking_date = date('Y-m-d');
		$theme['promo_details'] = $promo_details = $this->General_Model->get_promo_details($module, $post_data['promo_code'],$travel_date, $booking_date);
		if($promo_details != ''){
			$promo_offer = $this->General_Model->promo_calcualtion($total_amount,$promo_details->promo_type, $promo_details->discount);
			$update_global_data = array(
										'promo_code' => $promo_details->promo_code,
										'discount' => $promo_offer['discount']
								);
			$this->cart_model->update_cart('cart_global',$update_global_data, array('cart_id'=>$cart_details->cart_id));
			$promo_theme = $this->load->view('booking/applied_promo',$theme,true);
			
			$prepare_data['prepare_data']['msg'] = 'Promo code applied successfully!';
			$prepare_data['prepare_data']['promo_theme'] = $promo_theme;
			$prepare_data['prepare_data']['promo_status'] = 1;
		}
		else {
			$prepare_data['prepare_data']['msg'] = 'Not a valid promo code!';
			$prepare_data['prepare_data']['promo_status'] = 2;
		}
		return $prepare_data;
	}

	function percentage_conversion($total_cost, $CC_transaction_fee) {
		//~ echo "total_cost: <pre>";print_r($total_cost);
		$data['CC_fees'] = $CC_fees = $total_cost*($CC_transaction_fee/100);
		$data['total_fees'] = $total_fees = $CC_fees + $total_cost;
		//~ echo "data: <pre>";print_r($total_fees);exit;
		return $data;
	}

}
	
