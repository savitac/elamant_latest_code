<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
if(session_status() == PHP_SESSION_NONE){ session_start(); }
class Bookings extends CI_Controller {
	
    public function __construct(){
		parent::__construct();	
		$this->check_isvalidated();
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Usermanagement_Model');
		$this->load->model('Booking_Model');
		$this->load->model('Validation_Model');
		$this->load->model('cart_model');
		$this->load->model ( 'flight_model' );
		$this->lang->load('english','Dynamic_Languages');
	    $this->load->library ( 'booking_data_formatter' );
		$this->TravelLights = $this->lang->line('TravelLights');
	}
	
	private function check_isvalidated()
	{
		if($this->session->userdata('user_details_id'))
		{
			$user_id = $this->session->userdata('user_details_id');
			$controller_name = $this->router->fetch_class();
			$function_name = $this->router->fetch_method();
			$this->load->model('Privilege_Model');
            $sub_admin_id = $this->session->userdata('admin_id');
            $privilege_details = $this->Privilege_Model->get_user_privileges($user_id,$controller_name,$function_name, $function_parameter = 1)->row();
               if(count($privilege_details)  == 0)
				{	
				  redirect('error/access_denied');
				}
		}
	 }
	 

	function index(){ 
		
		$branch_id = $this->session->userdata('branch_id');
		$search_data = array();
		$data['hotel_orders'] = $this->Booking_Model->get_order_details($branch_id, $search_data);
		$data['transfer_orders'] = $this->Booking_Model->get_transfer_order_details($branch_id, $search_data);	
		$data['agentadmin_list'] = $this->Usermanagement_Model->getAgentusersList();
		$data['agentstaff_list'] = $this->Usermanagement_Model->get_branch_users($user_id);
		$data['tranfer_list_code'] = $this->General_Model->gatTranferListCode()->result();
		$data['flight_book_count'] = $this->Booking_Model->get_flight_book_count($branch_id);
		$this->load->view('dashboard/booking/bookings',$data);
	}
	
	
	function get_hotel_bookings(){
		if(isset($_POST)){
		  $search_data =  $_POST;
		}else{
			 $search_data = array();
		}
		$data['hotel_orders'] = $this->Booking_Model->get_order_details($this->session->userdata('branch_id'), $search_data);
		$data['hotel_bookings'] = $this->load->view('dashboard/booking/hotel_bookings',$data, true);
		echo  json_encode($data); exit;
	}


    function get_transfer_bookings(){
	
      if(isset($_POST)){
		  $search_data =  $_POST;
		}else{
			 $search_data = array();
		}
		$data['transfer_orders'] = $this->Booking_Model->get_transfer_order_details($this->session->userdata('branch_id'), $search_data);
		$data['transfer_bookings'] = $this->load->view('dashboard/booking/transfer_bookings',$data, true);
		echo  json_encode($data); exit;
	}
	
	function get_flight_bookings(){
					//echo "<pre>"; print_r($this->session->userdata('user_type')); exit;
	    $total_records = $this->flight_model->booking ();
	    $data['total_records'] =  $total_records;
	 //   print_r($data);exit;
        // $data['booking_details']=	$total_records['data']['booking_details'];
		//$data['booking_transaction_details']=	$total_records['data']['booking_transaction_details'];
		//$data['booking_customer_details']=	$total_records['data']['booking_customer_details'];
		//$data['cancellation_details]']=	$total_records['data']['cancellation_details]'];
	    $data['transfer_bookings'] = $this->load->view('dashboard/booking/flight_bookings',$data, true);
		echo json_encode($data);
		exit;
		
	}
	
	

    function view_bookings_old($pnr_no='')
	{
		$user_type_id='';
		if(isset($_POST['pnr_no']))
		{
			$pnr_no =$_POST['pnr_no'];
		} 
			$user_type_id = $_SESSION['b2c_details_id'];

			$data['orders'] = $this->Booking_Model->get_hotel_order_details($pnr_no,$user_type_id);
			
			$data['passanger'] = $this->Booking_Model->get_passanger_details($data['orders']->booking_global_id);
			$data['transactions'] = $this->Booking_Model->get_transaction_details_all($data['orders']->booking_transaction_id);
			
			if($data['orders']->product_name == 'Hotels'){
				
				$data['hotel_data'] = $this->Booking_Model->get_hotel_booking_details($data['orders']->referal_id);
			}
			
			$this->load->view('dashboard/booking/hotelVoucherView', $data); 
			
	
	}
	public function view_bookings($parent_pnr=''){
		if(!empty($parent_pnr)){
			$parent_pnr = json_decode(base64_decode($parent_pnr));
			$count = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->num_rows();
			if($count > 0){
				$data['pnr_nos'] = $this->Booking_Model->getBookingByParentPnr($parent_pnr)->result();
				foreach($data['pnr_nos'] as $pnr_nos):
				    $data['Booking'][] = $booking = $this->Booking_Model->getBookingbyPnr($pnr_nos->pnr_no,$pnr_nos->product_id)->row();
					$data['Passenger'][] = $booking = $this->Booking_Model->getPassengerbyPnr($pnr_nos->booking_global_id)->result();
					$data['user_details'][] = $this->General_Model->get_user_details($pnr_nos->user_id,$pnr_nos->user_type_id);
					if($pnr_nos->product_id == 'Hotels' || $pnr_nos->product_id == 1){
					  $this->load->view('hotel/hotel_voucher', $data);
				    }else{
					    $this->load->view('transfer/transfer_voucher', $data);	
					}
				endforeach;
				
			}else{
				 $this->load->view('errors/404');
			}
		}else{
			 $this->load->view('errors/404');
		}
	} 
	
	public function cancel($module,$pnr_no){
		$pnr_no = base64_decode(base64_decode($pnr_no));              
        if($module == 'Hotels'){
            $count = $this->Booking_Model->getBookingPnr_v1($pnr_no)->num_rows();      
            if($count == 1) {
                $b_data = $this->Booking_Model->getBookingPnr_v1($pnr_no)->row(); 
                $api_details = $this->General_Model->getApiList($b_data->api_id);            
                $reference_id = $b_data->referal_id;                 
                $getBookingHotelData = $this->Booking_Model->getBookingHotelData($reference_id)->row();
                $transaction_data = $this->Booking_Model->get_transaction_details($b_data->booking_transaction_id)->row();
                $api = $api_details[0]->api_name;
                $CancelTime = date('Y-m-d H:i:s');
                $book_usertype = $b_data->user_type_id;
				//~ echo '<pre/>';print_r($transaction_data);exit;
                if($b_data->booking_status == 'CONFIRMED' || $b_data->booking_status == 'PENDING'){ 
					
					//~ Cancellation Buffer
		
					$cancel_buffer = $this->General_Model->getCancellationBuffer(1,$b_data->api_id)->row();
					if(empty($cancel_buffer) && $cancel_buffer == '') {
						$buffer_days = 0;
					}
					else {
						$buffer_days = $cancel_buffer->cancel_days;
					}
					
					//~ End
					
                    if($api == "GTA"){
						$api_cancel_pnr = $b_data->api_confirmation_no;
						$this->load->helper('gta_helper');
                        $CancelReq_Res = HotelBookingCancel($api_cancel_pnr);
                        //  echo 'CancelReq_Res : <pre/>';print_r($CancelReq_Res);
                        $CancelReq = $CancelReq_Res['HotelBookingCancelRQ'];
                        $CancelRes = $CancelReq_Res['HotelBookingCancelRS'];
                        if($CancelRes != '') {
							$HotelBookingCancelRS = new SimpleXMLElement($CancelRes);
							$HotelBookingCancelRS = $HotelBookingCancelRS->children()->ResponseDetails->BookingResponse;                  
							if(isset($HotelBookingCancelRS->BookingStatus)){
								$BookingStatus = (string)$HotelBookingCancelRS->BookingStatus;
								$BookingStatus = trim($BookingStatus);                            
								if($BookingStatus == "Cancelled"){  //check if api status is sent as cancelled                            
									$update_booking = array(
														'api_status' => 'Cancelled',
														'booking_status' => 'CANCELLED',
														'cancellation_status' => 'Cancelled',
														'cancel_request_time' => $CancelTime,
														'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
														'cancel_response' => $CancelReq_Res['HotelBookingCancelRS']
													);
									//echo '<pre/>';print_r($update_booking);exit;
									$cancellation_data = $getBookingHotelData->cancellation_data;
									$profit_percent = $getBookingHotelData->profit_percent;  

									if(!empty($cancellation_data)){                                 
										$now = strtotime(date('Y-m-d H:i'));                                    
										$cancellation_data = json_decode($cancellation_data);
										foreach ($cancellation_data as $key => $cancellation) {
											if($buffer_days == 0) {
												$from = strtotime($cancellation->ToDate);
												$to = strtotime($cancellation->FromDate);
											}
											else {
												$from = strtotime($cancellation->ToDate." -".$buffer_days." days");
												$to = strtotime($cancellation->FromDate." -".$buffer_days." days");
											}
											$amount = $cancellation->ChargeAmount;
											$type = $cancellation->Type;
											if ($from == $to) {
												if ($now >= $from) {
													if($type == '1'){
														$charge[] = $amount;
													} else {
														$charge[] = 0;
													}
												}
												else {
													$charge[] = 0;
												}
											} else {
												if ($now >= $from && $now <= $to) {
													if($type == '1'){
														$charge[] = $amount;
													}
												} else {
													$charge[] = 0;
												}
											}                             
										}
										$payment_details = json_decode($getBookingHotelData->TravelerDetails);
										$TotalPrice_API = $payment_details->total;                       
										$Charge_API = array_sum($charge);
										$Refund = $TotalPrice_API - $Charge_API;
										$Refund = ($transaction_data->transaction_amount - $transaction_data->PG_Charge) - $Charge_API;
										
										$update_booking = array(
															'api_status' => 'Cancelled',
															'booking_status' => 'CANCELLED',
															'cancellation_status' => 'Cancelled',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
															'cancel_response' => $CancelReq_Res['HotelBookingCancelRS'],
															'cancellation_amount' => $Charge_API,
															'refund_amount' => $Refund,
															'refund_status' => 0
														);
									}

									$this->Booking_Model->Update_Booking_Global_v1($b_data->booking_no, $update_booking);
									//~ Commented as of now 
									//~ $this->cancel_mail_voucher($pnr_no);
									$response = array('status' => 1);
									//~ echo json_encode($response);
									redirect('bookings','refresh');

								} 
								else {
									$update_booking = array(
															'cancellation_status' => 'Cancellation Pending',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
															'cancel_response' => $CancelReq_Res['HotelBookingCancelRS']
														);
									$this->Booking_Model->Update_Booking_Global_v1($b_data->booking_no, $update_booking);
									$xml_log = array(
													'api_id' => 'GTA',
													'xml_title' => 'Hotel - Cancel Request',
													'xml_request' => $CancelReq_Res['HotelBookingCancelRQ'],
													'xml_response' => $CancelReq_Res['HotelBookingCancelRS'],
													'ip_address' => $this->input->ip_address()
												);
									$this->General_Model->store_logs($xml_log,'SUCCESS');
									$response = array('status' => 0);
									//~ echo json_encode($response);
									redirect('bookings','refresh');
								} 

							} 
							else {
								$update_booking = array(
														'cancellation_status' => 'Cancellation Pending',
														'cancel_request_time' => $CancelTime,
														'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
														'cancel_response' => $CancelReq_Res['HotelBookingCancelRS']
													);
								$this->Booking_Model->Update_Booking_Global_v1($b_data->booking_no, $update_booking);
								$xml_log = array(
												'api_id' => 'GTA',
												'xml_title' => 'Hotel - Cancel Request',
												'xml_request' => $CancelReq_Res['HotelBookingCancelRQ'],
												'xml_response' => $CancelReq_Res['HotelBookingCancelRS'],
												'ip_address' => $this->input->ip_address()
											);
								$this->General_Model->store_logs($xml_log,'SUCCESS');
								$response = array('status' => 0);
								//~ echo json_encode($response);
								redirect('bookings','refresh');
							}
						}
						else {
							redirect('bookings','refresh');
						}
                    }
                    if($api == "HOTELBEDS") {
						$api_cancel_pnr = $b_data->api_confirmation_no;
						$this->load->helper('hb_helper');
                        $CancelReq_Res = PurchaseCancelRQ($b_data->api_id, $api_cancel_pnr);
                        //~ echo "data: <pre>";print_r($CancelReq_Res);exit;
                        //~ $CancelReq_Res = array();
                        //~ $CancelReq_Res['HotelBookingCancelRQ'] = '<PurchaseCancelRQ xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseCancelRQ.xsd" type="C">
																	//~ <Language>ENG</Language>
																	//~ <Credentials>
																		//~ <User>VIBRANIN26193</User>
																		//~ <Password>VIBRANIN26193</Password>
																	//~ </Credentials>
																	//~ <PurchaseReference>
																		//~ <FileNumber>654257</FileNumber>
																		//~ <IncomingOffice code="148" />
																	//~ </PurchaseReference>
																//~ </PurchaseCancelRQ>';
                        //~ $CancelReq_Res['HotelBookingCancelRS'] = '<PurchaseCancelRS xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseCancelRS.xsd" type="C">
																	//~ <AuditData>
																		//~ <ProcessTime>2550</ProcessTime>
																		//~ <Timestamp>2016-09-22 11:26:36.659</Timestamp>
																		//~ <RequestHost>14.141.47.106</RequestHost>
																		//~ <ServerName>FORM</ServerName>
																		//~ <ServerId>FO</ServerId>
																		//~ <SchemaRelease>2005/06</SchemaRelease>
																		//~ <HydraCoreRelease>3.11.4.20160619</HydraCoreRelease>
																		//~ <HydraEnumerationsRelease>2#lx/bz6S3SdeVkt7gCVUPEQ</HydraEnumerationsRelease>
																		//~ <MerlinRelease>0</MerlinRelease>
																	//~ </AuditData>
																	//~ <Purchase purchaseToken="O2564990273" timeToExpiration="1799911">
																		//~ <Reference>
																			//~ <FileNumber>654257</FileNumber>
																			//~ <IncomingOffice code="148"></IncomingOffice>
																		//~ </Reference>
																		//~ <Status>CANCELLED</Status>
																		//~ <Agency>
																			//~ <Code>26193</Code>
																			//~ <Branch>1</Branch>
																		//~ </Agency>
																		//~ <Language>ENG</Language>
																		//~ <CreationDate date="20160922" />
																		//~ <CreationUser>VIBRANIN26193</CreationUser>
																		//~ <Holder type="AD">
																			//~ <Age>0</Age>
																			//~ <Name>GULSAN</Name>
																			//~ <LastName>MAHAKUD</LastName>
																		//~ </Holder>
																		//~ <AgencyReference>P1PYDH3ND67E</AgencyReference>
																		//~ <ServiceList>
																			//~ <Service xsi:type="ServiceHotel" SPUI="148#H#1">
																				//~ <Reference>
																					//~ <FileNumber>654257-H1</FileNumber>
																					//~ <IncomingOffice code="148"></IncomingOffice>
																				//~ </Reference>
																				//~ <Status>CANCELLED</Status>
																				//~ <ContractList>
																					//~ <Contract>
																						//~ <Name>NRF-FIT-RO</Name>
																						//~ <IncomingOffice code="148"></IncomingOffice>
																						//~ <CommentList>
																							//~ <Comment type="CONTRACT">Extra beds on demand YES (without additional debit notes). Check-in hour 14:00 â€“ 14:00. Wi-fi YES (with additional debit notes) 30 AED Per unit/night. Airport Shuttle. Identification card at arrival. Marriage certificate required for a couple to share room. . No cancellation or amendment is allowed once the reservation is made, fully Non-Refundable.. Extra beds on demand YES (without additional debit notes). Check-in hour 14:00 â€“ 14:00. Wi-fi YES (with additional debit notes) 30 AED Per unit/night. Airport Shuttle. Identification card at arrival. Marriage certificate required for a couple to share room. . No cancellation or amendment is allowed once the reservation is made, fully Non-Refundable.</Comment>
																						//~ </CommentList>
																					//~ </Contract>
																				//~ </ContractList>
																				//~ <Supplier name="HOTELBEDS SPAIN S.L.U." vatNumber="ESB28916765" />
																				//~ <CommentList>
																					//~ <Comment type="SERVICE">
																//~ Test
																//~ Test</Comment>
																					//~ <Comment type="INCOMING">Test
																//~ Test</Comment>
																				//~ </CommentList>
																				//~ <DateFrom date="20170215" />
																				//~ <DateTo date="20170216" />
																				//~ <Currency code="USD">US Dollar</Currency>
																				//~ <TotalAmount>0.000</TotalAmount>
																				//~ <AdditionalCostList>
																					//~ <AdditionalCost type="AG_COMMISSION">
																						//~ <Price>
																							//~ <Amount>0.000</Amount>
																						//~ </Price>
																					//~ </AdditionalCost>
																					//~ <AdditionalCost type="COMMISSION_VAT">
																						//~ <Price>
																							//~ <Amount>0.000</Amount>
																						//~ </Price>
																					//~ </AdditionalCost>
																					//~ <AdditionalCost type="COMMISSION_PCT">
																						//~ <Price>
																							//~ <Amount>0.000</Amount>
																						//~ </Price>
																					//~ </AdditionalCost>
																				//~ </AdditionalCostList>
																				//~ <ModificationPolicyList>
																					//~ <ModificationPolicy>Cancellation</ModificationPolicy>
																					//~ <ModificationPolicy>Confirmation</ModificationPolicy>
																					//~ <ModificationPolicy>Modification</ModificationPolicy>
																				//~ </ModificationPolicyList>
																				//~ <HotelInfo xsi:type="ProductHotel">
																					//~ <Code>8919</Code>
																					//~ <Name>Panorama Bur Dubai</Name>
																					//~ <Category type="SIMPLE" code="2EST">2 STARS</Category>
																					//~ <Destination type="SIMPLE" code="DXB">
																						//~ <Name>Dubai</Name>
																						//~ <ZoneList>
																							//~ <Zone type="SIMPLE" code="1">Dubai</Zone>
																						//~ </ZoneList>
																					//~ </Destination>
																				//~ </HotelInfo>
																				//~ <AvailableRoom>
																					//~ <HotelOccupancy>
																						//~ <RoomCount>2</RoomCount>
																						//~ <Occupancy>
																							//~ <AdultCount>2</AdultCount>
																							//~ <ChildCount>0</ChildCount>
																						//~ </Occupancy>
																					//~ </HotelOccupancy>
																					//~ <HotelRoom SHRUI="eiEAhvJy7LNHmwurH16Rqg==" availCount="2" status="CANCELLED">
																						//~ <Board type="SIMPLE" code="RO-E10">ROOM ONLY</Board>
																						//~ <RoomType type="SIMPLE" code="DBL-E10" characteristic="ST">DOUBLE STANDARD</RoomType>
																						//~ <Price>
																							//~ <Amount>0.000</Amount>
																						//~ </Price>
																						//~ <HotelRoomExtraInfo>
																							//~ <ExtendedData>
																								//~ <Name>INFO_ROOM_AGENCY_BOOKING_STATUS</Name>
																								//~ <Value>O</Value>
																							//~ </ExtendedData>
																							//~ <ExtendedData>
																								//~ <Name>INFO_ROOM_INCOMING_BOOKING_STATUS</Name>
																								//~ <Value>O</Value>
																							//~ </ExtendedData>
																						//~ </HotelRoomExtraInfo>
																					//~ </HotelRoom>
																				//~ </AvailableRoom>
																				//~ <AvailableRoom>
																					//~ <HotelOccupancy>
																						//~ <RoomCount>1</RoomCount>
																						//~ <Occupancy>
																							//~ <AdultCount>1</AdultCount>
																							//~ <ChildCount>1</ChildCount>
																						//~ </Occupancy>
																					//~ </HotelOccupancy>
																					//~ <HotelRoom SHRUI="sw8QU3CPllHnG/Alg3M7RA==" availCount="1" status="CANCELLED">
																						//~ <Board type="SIMPLE" code="RO-E10">ROOM ONLY</Board>
																						//~ <RoomType type="SIMPLE" code="DBL-E10" characteristic="ST">DOUBLE STANDARD</RoomType>
																						//~ <Price>
																							//~ <Amount>0.000</Amount>
																						//~ </Price>
																						//~ <HotelRoomExtraInfo>
																							//~ <ExtendedData>
																								//~ <Name>INFO_ROOM_AGENCY_BOOKING_STATUS</Name>
																								//~ <Value>O</Value>
																							//~ </ExtendedData>
																							//~ <ExtendedData>
																								//~ <Name>INFO_ROOM_INCOMING_BOOKING_STATUS</Name>
																								//~ <Value>O</Value>
																							//~ </ExtendedData>
																						//~ </HotelRoomExtraInfo>
																					//~ </HotelRoom>
																				//~ </AvailableRoom>
																			//~ </Service>
																		//~ </ServiceList>
																		//~ <Currency code="USD"></Currency>
																		//~ <PaymentData>
																			//~ <PaymentType code="C"></PaymentType>
																			//~ <Description>*55* Hotelbeds, S.L.U, Bank: CITIBANK(Citigroup Centre, Canary Wharf, London, E14 5LB. United Kingdom) Account:GB13 CITI 185008 13242420,  SWIFT:CITIGB2L,  7 days prior to clients arrival (except group bookings with fixed days in advance at the time of the confirmation) . Please indicate our reference number when making payment. Thank you for your cooperation.</Description>
																		//~ </PaymentData>
																		//~ <TotalPrice>0.000</TotalPrice>
																	//~ </Purchase>
																	//~ <Currency code="USD"></Currency>
																	//~ <Amount>0.000</Amount>
																//~ </PurchaseCancelRS>';
                        $CancelReq = $CancelReq_Res['HotelBookingCancelRQ'];
                        $CancelRes = $CancelReq_Res['HotelBookingCancelRS'];
                        if ($CancelRes != '') {
							$dom = new DOMDocument();
							$dom->loadXML($CancelRes);
							
							$Charge_API = $dom->getElementsByTagName("Amount")->item(0)->nodeValue;
							
							if ($dom->getElementsByTagName("PurchaseCancelRS")->item(0)->getElementsByTagName("DetailedMessage")->length > 0) {
								$data['exits'] = $dom->getElementsByTagName("PurchaseCancelRS")->item(0)->getElementsByTagName("DetailedMessage")->item(0)->nodeValue;
								$data['exits'] .= "<br /> OR Your hotel booking has not cancelled, !Please contact admin for further details";
							}
							if ($dom->getElementsByTagName("ServiceList")->length > 0) {
								$status = $dom->getElementsByTagName("ServiceList")->item(0)->getElementsByTagName("Status")->item(0)->nodeValue;
								$totalprice = $dom->getElementsByTagName("ServiceList")->item(0)->getElementsByTagName("TotalPrice")->item(0)->nodeValue;
								$Currency_code = $dom->getElementsByTagName("ServiceList")->item(0)->getElementsByTagName("Currency")->item(0)->getAttribute("code");
								
								if ($status == 'CANCELLED') {
									$hotel_amount = $totalprice;
									$inramount = $totalPrice;

									if ($inramount <= 0) {
										$inramount = 0;
									}
									$Refund = ($transaction_data->transaction_amount - $transaction_data->PG_Charge) - $Charge_API;
									$update_booking = array(
															'api_status' => 'Cancelled',
															'booking_status' => 'CANCELLED',
															'cancellation_status' => 'Cancelled',
															'cancel_request_time' => $CancelTime,
															'cancel_request' => $CancelReq_Res['HotelBookingCancelRQ'],
															'cancel_response' => $CancelReq_Res['HotelBookingCancelRS'],
															'cancellation_amount' => $Charge_API,
															'refund_amount' => $Refund
													);
									$data['exits'] = "Booking has been Sucessfully " . $HOTEL_STATUS;
									$this->Booking_Model->Update_Booking_Global_v1($b_data->booking_no, $update_booking);
								} else {
									$data['exits'] = "Your hotel booking has not cancelled, !Please contact admin for further details";
								}
							}
						}
						redirect('bookings','refresh');
					} 

                } else {
                    $response = array('status' => 0);
                    //~ echo json_encode($response);
                    redirect('bookings','refresh');
                }
            } else {
                $response = array('status' => 0);
                //~ echo json_encode($response);
                redirect('bookings','refresh');
            }
        }elseif($module == 'Transfer'){
			             $this->load->library('../controllers/Transferbooking', false);
		                $transferBooking = Transferbooking::transfer_cancel($pnr_no);
		                redirect('bookings','refresh');
		}
       	
    }
    
    function transfer_cancel(){
		 $pnr_no = '19835179';
		$this->load->helper('gtatransfer_helper');
		$response = TransferBookingCancel($pnr_no);
		
		
	}
	
	function process_promo_code(){
	
		if($_POST['module'] == 'Hotels'){
		   $cart_details = $this->cart_model->getBookingTemp_hotel($_POST['cart_id']);	
		}elseif($_POST['module'] == 'Transfer'){
			$cart_details = $this->cart_model->getBookingTemp_transfer($_POST['cart_id']);	
		}
		if(count($cart_details) > 0){
			$total_amount  = $cart_details->total_cost-($cart_details->service_charge +$cart_details->gst_charge +$cart_details->tax_charge) ;
			$product_id = $cart_details->product_id;
			if($product_id == 1){
			 $travel_date = $cart_details->checkin;
			}elseif($product_id == 19){
			  $travel_date = $cart_details->travel_start_date;	
			}
			$booking_date = date('Y-m-d');
			$theme['promo_details'] = $promo_details = $this->General_Model->get_promo_details($_POST['module'], $_POST['promo_code'],$travel_date, $booking_date);
			if($promo_details != ''){
				$promo_offer = $this->General_Model->promo_calcualtion($total_amount,$promo_details->promo_type, $promo_details->discount);
				$update_global_data = array(
											'promo_code' => $promo_details->promo_code,
											'discount' => $promo_offer['discount']
									);
				$this->cart_model->update_cart('cart_global',$update_global_data, array('cart_id'=>$cart_details->cart_id));
				$promo_theme = $this->load->view('booking/applied_promo',$theme,true);
				$data = array("offer_amount" => $promo_offer['amount']+($cart_details->service_charge +$cart_details->gst_charge +$cart_details->tax_charge),
				              "discount" =>  $promo_offer['discount'],
				              "msg" => 'Promo code applied successfully',
				              "promo_theme" => $promo_theme,
				              "service_charge" => $cart_details->service_charge,
				              "gst_charge" => $cart_details->gst_charge,
				              "tax_charge" => $cart_details->tax_charge,
				               "status" => 1);
			}else{
				$data = array("status" =>2,
		               "msg" => 'Not a valid promo code');
			}
			
		}else{
		 $data = array("status" =>2,
		               "msg" => 'Promocode apply failure');
		}
		
		print  json_encode($data);
	}
    function clear_promo() {
		if($_POST['module'] == 'Hotels') {
		   $cart_details = $this->cart_model->getBookingTemp_hotel($_POST['cart_id']);	
		}
		elseif($_POST['module'] == 'Transfer'){
			$cart_details = $this->cart_model->getBookingTemp_transfer($_POST['cart_id']);	
		}
		else {
			
		}
		
		if(count($cart_details) > 0){
			$update_global_data = array(
										'promo_code' => NULL,
										'discount' => 0
								);
			$this->cart_model->update_cart('cart_global',$update_global_data, array('cart_id'=>$cart_details->cart_id));
			$theme['msg'] = 'Promo removed successfully';
			$promo_theme = $this->load->view('booking/prepare_promo',$theme,true);
			$data = array(
						"msg" => 'Promo removed successfully',
						"promo_theme" => $promo_theme,
						"status" => 1
					);
		}
		else {
			$data = array(
						"msg" => 'Promo could not be removed successfully!',
						"status" =>2
					);
		}
		print  json_encode($data);
	}

}
	
