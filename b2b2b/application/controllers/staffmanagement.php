<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
error_reporting(0);
if(session_status() == PHP_SESSION_NONE){ session_start(); }
class Staffmanagement extends CI_Controller {
	
    public function __construct(){
		parent::__construct();	
		$this->check_isvalidated();
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Account_Model');
		$this->load->model('Security_Model');
		$this->load->model('Validation_Model');
		$this->load->model('Email_Model');
		$this->load->model('Staffmanagement_Model');
		$this->load->model('Usermanagement_Model');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	}
	
	private function check_isvalidated()
	{
		if($this->session->userdata('user_details_id'))
		{
			$user_id = $this->session->userdata('user_details_id');
			$controller_name = $this->router->fetch_class();
			$function_name = $this->router->fetch_method();
			$this->load->model('Privilege_Model');
            $sub_admin_id = $this->session->userdata('admin_id');
            $privilege_details = $this->Privilege_Model->get_user_privileges($user_id,$controller_name,$function_name, $function_parameter = 1)->row();
               if(count($privilege_details)  == 0)
				{	
				  redirect('error/access_denied');
				}
		}
	 }

	function index(){ 
	  $user_id 	= $this->session->userdata('user_details_id');
	  $users['roles'] = $this->Staffmanagement_Model->get_staff_roles($this->session->userdata('branch_id'));
	  $users['agentadmin_list'] = $this->Usermanagement_Model->get_branch_users($this->session->userdata('branch_id'));
	  $users['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
	  $this->load->view('dashboard/staffmanagement/staff_management', $users);	
	}
	
	function add_roles(){
		if(count($_POST) > 0) {
			$this->Staffmanagement_Model->add_agent_roles($_POST);
			     $data['status'] = 1;
				$data['message'] = "Agent roles added successfully";
				$data['url'] = base_url()."staffmanagement";
				$data['active_div'] = "sub_agent";
				print  json_encode($data);
				return;
			
		}
		        $data['url'] = base_url()."staffmanagement";
				$data['active_div'] = "sub_agent";
				print  json_encode($data);
				return;
	}
	
	
	function active_rolese($roles_id1){
		$role_id = json_decode(base64_decode($roles_id1));
		if($role_id != ''){
			$this->Staffmanagement_Model->activate_roles($role_id);
		    redirect('staffmanagement');
		}else{
			redirect('staffmanagement');
		}
	}
	
	function inactive_roles($roles_id1){
		$role_id = json_decode(base64_decode($roles_id1));
		if($role_id != ''){
			$this->Staffmanagement_Model->inactivate_roles($role_id);
			redirect('staffmanagement');
		}else{
			redirect('staffmanagement');
		}
	}
	
	function add_agent_staff(){
		$domain_id=$this->session->userdata('domain_id'); 
		$branch_id=$this->session->userdata('branch_id'); 
		if(count($_POST) > 0){
			$agent_validation = $this->agentUserFormValidation($_POST);
			
			if($agent_validation['status']['status'] == 3) {
				print  json_encode(array_merge($agent_validation['error'], $agent_validation['status']));
				return;
			}
			$email = $_POST['email'];
			$Query="select * from  user_details  where user_email ='".$email."' AND user_type_id='4' ";
			$query=$this->db->query($Query);
			if ($query->num_rows() > 0)
		    {
				$data['status'] = 3;
				$data['email-error'] = "Email Id Already exists";
				print  json_encode($data);
				return;
		  }else{
			try{
		    $this->General_Model->begin_transaction() ;
		    $addressing = array( 
				'country_id'        => $_POST['country'],
				'address' => $_POST['address'],
				'city_name' => $_POST['cityname'],
				); 
			$address = $this->Usermanagement_Model->userAddress($addressing);  
		    $acc_no = $this->session->userdata('user_account_no'); 
		    $this->Usermanagement_Model->addUseragentDetails($_POST, $acc_no,$address,$domain_id,$branch_id); 
		    
			$user['email_template'] = $this->Email_Model->get_email_template('add_user_active')->row();
			$user['user_email'] = $_POST['email'];
		    $user['user_name'] = $_POST['user_name'];
		    $user['user_acc_no'] = $acc_no;
		    $user['profile_photo'] = 'user-avatar.jpg';
		    
			 
			$this->Email_Model->sendmail_reg($user);
			$this->General_Model->commit_transaction() ;
		        $data['status'] = 1;
				$data['message'] = "Agent added successfully";
				$data['url'] = base_url()."staffmanagement";
				$data['active_div'] = "sub_agent";
				print  json_encode($data);
				return;
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction() ;
			return $e;
		} 
	}
   }
 }
 
  function editAdminagent($agent_id1){
	 $agent_id = json_decode(base64_decode($agent_id1));
	 $data['agentadmin_list'] =  $this->Usermanagement_Model->get_branch_users($this->session->userdata('branch_id'), $agent_id);
	 $data['roles'] = $this->Staffmanagement_Model->get_staff_roles($this->session->userdata('branch_id'));
	 $data['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
	 $data['editAgent'] = $this->load->view('dashboard/staffmanagement/edit_agent', $data, true);
	 print json_encode($data); 
	 return;
	 
}

 function updateAgentUsers($agent_id1){
	   $agent_id = json_decode(base64_decode($agent_id1));
	 
		if(count($_POST) > 0){
			$agent_validation = $this->agentUserFormValidation($_POST);
			
			if($agent_validation['status']['status'] == 3) {
				print  json_encode(array_merge($agent_validation['error'], $agent_validation['status']));
				return;
			}
			$email = $_POST['email'];
			$Query="select * from  user_details  where user_email ='".$email."' AND user_type_id='2' and user_details_id !=".$agent_id;
			
			$query=$this->db->query($Query);
			if ($query->num_rows() > 0)
		    {
				$data['status'] = 3;
				$data['email-error'] = "Email Id Already exists";
				print  json_encode($data);
				return;
		  }else{
			try{
		    $this->General_Model->begin_transaction() ;
			$this->Usermanagement_Model->updateUseragentDetails($_POST, $agent_id);
			$this->General_Model->commit_transaction() ;
		        $data['status'] = 1;
				$data['message'] = "Staff updated successfully";
				$data['url'] = base_url()."staffmanagement";
				$data['active_div'] = "sub_agent";
				print  json_encode($data);
				return;
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction() ;
			return $e;
		} 
	}
   }
 }
 
 function agentUserFormValidation($post)
  {
	     if( $post['user_name'] == ""){
			 $data['error']['name-error'] = "Please enter valid name";
			}
			
			if(isset($post['user_name']) && $post['user_name'] != '') {
			 $name_validation = $this->Validation_Model->alphabetValidation($_POST['user_name']);
			 if(!$name_validation){
				$data['error']['name-error'] = "Please enter valid name";
			 }
			}
			
			if($post['email'] == ''){
			   $data['error']['email-error'] = "Please enter valid Email Address";
			 }
			 
			if(isset($post['email'])  && $post['email'] != ''){
			   $email_validation = $this->Validation_Model->emailIdFormat($post['email']);	
			    if(!$email_validation){
				  $data['error']['email-error'] = "Please enter valid Email";
			   }
			}
			
			if(isset($post['password']) && $post['password'] == ""){
				$data['error']['password-error'] = "Please enter password";
			}
			
			if(isset($post['password']) && $post['password'] != ''){
			   $password_validation = $this->Validation_Model->passwordValidation($post['password']);	
			    if(!$password_validation){
				   $data['error']['password-error'] = "Password should contain atleast on Capital letter, one Numbers and one Special Characters";
			 }
			}
			
			if(isset($post['cpassword']) && $post['cpassword'] == ""){
				$data['error']['password-error'] = "Please enter confirm password";
			}
			 
			 if($post['country'] == ""){
				$data['error']['password-error'] = "Please enter confirm password";
			}
			
			if(isset($post['cpassword']) && $post['cpassword'] != ''){
			   $password_validation = $this->Validation_Model->checkPasswords($post['password'], $post['cpassword']);	
			    if(!$password_validation){
				   $data['error']['password-error'] = "Password not valid";
			 }
			}
			
			if(isset($post['mob_number']) && $post['mob_number'] != ''){
			  $contact_validation = $this->Validation_Model->numberWithSpecialCharacter($post['mob_number']);	
			   if(!$contact_validation){
				 $data['error']['contact-error'] = "Please enter valid Contact Number";
			 }
			}
			
		    if(isset($data['error']) && count($data['error']) > 0) {
				$data['status']['status'] = 3;
			}else{
				$data['status']['status'] = 1;
			}
			
			return $data;
  }
  
  
    function getUserPrivileges($role_id1){
	     $user['role_id'] = $role_id1;
	     $user['privilege'] = $this->Dashboard_Model->get_dashboardmenu();
	     $user['privilege_template'] = $this->load->view("dashboard/staffmanagement/user_privilege", $user, true);
	     $user['status'] = 1;
	   echo json_encode($user);
    }
    
    function update_staff_privilege(){
	
		if(count($_POST) > 0){
			$this->Staffmanagement_Model->update_privileges($_POST);
			redirect('staffmanagement');
		}else{
			redirect('staffmanagement');
		}
	}
	
	function getUserProducts($user_id1){
		$user['user_id'] = $user_id1;
		$user['products'] = $this->Dashboard_Model->getProducts();
		$user['product_template'] = $this->load->view("dashboard/staffmanagement/user_product", $user, true);
	    $user['status'] = 1;
	   echo json_encode($user);
	}
	
	
	
	 function update_product_privilege(){
		if(count($_POST) > 0){
			$this->Usermanagement_Model->update_product_privileges($_POST);
			redirect('staffmanagement');
		}else{
			redirect('staffmanagement');
		}
	}
} ?>
	
