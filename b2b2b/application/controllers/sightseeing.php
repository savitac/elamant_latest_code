<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @package    Provab
 * @subpackage Sightseeing
 * @author     Elavarasi<elavarasi.k@provabmial.com>
 * @version    V1
 */
 // error_reporting(E_ALL);

class Sightseeing extends CI_Controller {
	private $current_module;
	public function __construct()
	{
		parent::__construct();
		//we need to activate activity api which are active for current domain and load those libraries
		$this->load->model('sightseeing_model');
		$this->load->model('domain_management_model');
		$this->load->model('transaction');
		// $this->load->library('social_network/facebook');//Facebook Library to enable login button		
		//$this->output->enable_profiler(TRUE);
		$this->current_module = $this->config->item('current_module');
		$this->user_type = $this->config->item('user_type');
	}

	/**
	 * index page of application will be loaded here
	 */
	function index()
	{
		//	echo number_format(0, 2, '.', '');
	}

	/**
	 *  Arjun J Gowda
	 * Load Activity Search Result
	 * @param number $search_id unique number which identifies search criteria given by user at the time of searching
	 */
	function search($search_id)
	{	

		$safe_search_data = $this->sightseeing_model->get_safe_search_data($search_id,META_SIGHTSEEING_COURSE);
		
		// Get all the hotels bookings source which are active
		$active_booking_source = $this->sightseeing_model->active_booking_source(); 
		$domain_details = $this->custom_db->single_table_records ( 'domain_list', '*', '', 0, 100000, array () );
		if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
			$safe_search_data['data']['search_id'] = abs($search_id);
			$this->load->view('sightseeing/search_result_page', array('domain_details' => $domain_details['data'][0], 'sight_seen_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
		} else {
			$this->load->view ( 'general/popup_redirect', array('domain_details' => $domain_details['data'][0]));
		}
	}

	/**
	 *  
	 * Get Product Details
	 */
	function sightseeing_details()
	{
		$params = $this->input->get();
		$safe_search_data = $this->sightseeing_model->get_safe_search_data($params['search_id'],META_SIGHTSEEING_COURSE);
		$safe_search_data['data']['search_id'] = abs($params['search_id']);
		$currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		$search_id = $params['search_id'];
		if (isset($params['booking_source']) == true) {
			//We will load different page for different API providers... As we have dependency on API for hotel details page
			load_sightseen_lib($params['booking_source']);
			if ($params['booking_source'] == PROVAB_SIGHTSEEN_BOOKING_SOURCE && isset($params['result_token']) == true and isset($params['op']) == true and
			$params['op'] == 'get_details' and $safe_search_data['status'] == true) {

				$params['result_token']	= urldecode($params['result_token']);


				$raw_product_deails = $this->sightseeing_lib->get_product_details($params);
				
				// debug($raw_product_deails);
				// exit;
				if ($raw_product_deails['status']) {
					
					
					 $calendar_availability_date = $this->enable_calendar_availability($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Product_available_date']);

					
					$raw_product_deails['data']['ProductDetails']['SSInfoResult']['Calendar_available_date'] = $calendar_availability_date;

					if($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price']){

						//details data in preffered currency
						

						$Price = $this->sightseeing_lib->details_data_in_preffered_currency($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price'],$currency_obj,$this->user_type);

						$currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

						//calculation Markup 
						$raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price'] = $this->sightseeing_lib->update_booking_markup_currency($Price,$currency_obj,$search_id);	
						// debug($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price']);
						// exit;

					}
					
					$this->load->view('sightseeing/viator/sightseeing_details', array('currency_obj' => $currency_obj, 'product_details' => $raw_product_deails['data']['ProductDetails']['SSInfoResult'], 'search_params' => $safe_search_data['data'], 'search_id'=>$search_id,'active_booking_source' => $params['booking_source'], 'params' => $params));
				} else {
					
					$msg= $raw_product_deails['Message'];

					redirect(base_url().'index.php/sightseeing/exception?op='.$msg.'&notification=session');
				}
			} else {
				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	}

	/**
	*
	*Enable Particular day only in calendar
	*/
	public function enable_calendar_availability($calendar_availability_date){
		
		if(valid_array($calendar_availability_date)){
			$available_str=array();
			 foreach ($calendar_availability_date as $m_key => $m_value) {
			 	$avail_month_str = $m_key;
			 	
			 	foreach ($m_value as $d_key => $d_value) {
			 		//j- to remove 0 from date, n to remove 0 from month
			 		$date_str = $avail_month_str.'-'.$d_value;
			 		$available_str[] = date('j-n-Y',strtotime($date_str));
			 		
			 	}
			 }
			
			return $available_str;
		}else{
			return '';
		}
	}

	/**
	*
	*Getting Available Tourgrade Based on Passenger Mix
	*/
	public function select_tourgrade(){
		$post_params = $this->input->post();
		$response['data'] = '';
        $response['msg'] = '';
        $response['status'] = FAILURE_STATUS;
		$safe_search_data = $this->sightseeing_model->get_safe_search_data($post_params['search_id'],META_SIGHTSEEING_COURSE);
		if(trim($post_params['product_code'])!=''){

			load_sightseen_lib($post_params['booking_source']);

			if(trim($post_params['op']=='check_tourgrade')&&$post_params['product_code']!=''){

					$search_product_tourgrade = $this->sightseeing_lib->get_tourgrade_list($post_params);

					// debug($search_product_tourgrade);die;
					$currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));

					
					if($search_product_tourgrade['status']){

						$module_currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

						$raw_product_list = $this->sightseeing_lib->tourgrade_in_preferred_currency($search_product_tourgrade['data'], $currency_obj,$module_currency_obj,$this->user_type);
						
						$search_product_tourgrade['data'] = $raw_product_list;
						$response['data'] = get_compressed_output($this->load->view('sightseeing/viator/select_tourgrade',
							array('tourgrade_list'=>$search_product_tourgrade['data'],'search_params'=>$post_params,
								'currency_obj'=>$currency_obj,
								'booking_source'=>$post_params['booking_source']), true));
						$response['msg'] = 'success';
						$response['status'] = SUCCESS_STATUS;
						
					}else{
						$search_product_tourgrade['Message'] = $search_product_tourgrade['Message'];
						
						if(!valid_array($search_product_tourgrade['data'])){
							
							$response['msg'] = $search_product_tourgrade['Message'] ;
						
						}
					}
			}else{
				
			}
		}else{
			
		}
		// debug($response);die;
		 $this->output_compressed_data($response);
		
	}

	/**
     * Compress and output data
     * @param array $data
     */
    private function output_compressed_data($data) {
        while (ob_get_level() > 0) {
            ob_end_clean();
        }
        ob_start("ob_gzhandler");
        header('Content-type:application/json');
        echo json_encode($data);
        ob_end_flush();
        exit;
    }

	/**
	* check the available date for the product
	**/
	public function select_date()
	{
		$post_params = $this->input->post();
		$selected_date = trim($post_params['selected_date']);
		if(!empty($selected_date))
		{	
			$product_get_booking_date = json_decode(base64_decode($post_params['available_date']),true);
			if(valid_array($product_get_booking_date))
			{
				$selected_date_details = $product_get_booking_date;
				$options = '';

				foreach ($selected_date_details[$selected_date] as $key => $value) {
					$selected = '';
					if(isset($post_params['s_date'])){

						if($value==$post_params['s_date']){
							$selected =' selected';
						}
					}
					
					$options .='<option value='.$value.' '.$selected.' >'.($value).'</option>';
				}
				echo $options;
				exit;	
			}
		}
	}

	/**
	 *  
	 * Passenger Details page for final bookings
	 * Here we need to run booking based on api
	 */
	function booking()
	{
		
		$pre_booking_params = $this->input->post();		
		$search_id = $pre_booking_params['search_id'];
		$safe_search_data = $this->sightseeing_model->get_safe_search_data($pre_booking_params['search_id'],META_SIGHTSEEING_COURSE);
		$safe_search_data['data']['search_id'] = abs($search_id);
		$page_data['active_payment_options'] = $this->module_model->get_active_payment_module_list();
		if (isset($pre_booking_params['booking_source']) == true) {
			//We will load different page for different API providers... As we have dependency on API for tourgrade details page
			$page_data['search_data'] = $safe_search_data['data'];
			load_sightseen_lib($pre_booking_params['booking_source']);
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");

			if ($pre_booking_params['booking_source'] == PROVAB_SIGHTSEEN_BOOKING_SOURCE && isset($pre_booking_params['tour_uniq_id']) == true and
			isset($pre_booking_params['op']) == true and $pre_booking_params['op'] == 'block_trip' and $safe_search_data['status'] == true)
			{
				
				if ($pre_booking_params['tour_uniq_id'] != false) {

					$trip_block_details = $this->sightseeing_lib->block_trip($pre_booking_params);
					
					if ($trip_block_details['status'] == false) {
						redirect(base_url().'index.php/sightseeing/exception?op='.$trip_block_details['data']['msg']);
					}
					//Converting API currency data to preferred currency
					$currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
					
					$trip_block_details = $this->sightseeing_lib->tripblock_data_in_preferred_currency($trip_block_details, $currency_obj,$this->user_type);

					//Display
					$currency_obj = new Currency(array('module_type' => 'sightseeing', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

					$pre_booking_params = $this->sightseeing_lib->update_block_details($trip_block_details['data']['BlockTrip'], $pre_booking_params,$currency_obj,$this->user_type);
					
					/*
					 * Update Markup
					 */					
					$pre_booking_params['markup_price_summary'] = $this->sightseeing_lib->update_booking_markup_currency($pre_booking_params['price_summary'], $currency_obj, $safe_search_data['data']['search_id']);
					
					if ($trip_block_details['status'] == SUCCESS_STATUS) {
						if(!empty($this->entity_country_code)){
							$page_data['user_country_code'] = $this->entity_country_code;
						}
						else{
							$page_data['user_country_code'] = '';	
						}
						$page_data['booking_source'] = $pre_booking_params['booking_source'];
						$page_data['pre_booking_params'] = $pre_booking_params;
						$page_data['pre_booking_params']['default_currency'] = get_application_currency_preference();
						
						$page_data['iso_country_list']	= $this->db_cache_api->get_iso_country_list();
						$page_data['country_list']		= $this->db_cache_api->get_country_list();
						$page_data['currency_obj']		= $currency_obj;
						$page_data['total_price']		= $this->sightseeing_lib->total_price($pre_booking_params['markup_price_summary']);
						//for rewards points
							$page_data['reward_usable'] = 0;
							$page_data['reward_earned'] = 0;
							$page_data ['total_price_with_rewards'] = 0;
							// if(is_logged_in_user()){
							// 		    $data =  $this->rewards->page_data_reward_details(META_SIGHTSEEING_COURSE,$page_data ['total_price']);
							// 		    $page_data= array_merge($page_data,$data);
										
							// 	}
							//end rewards system
						//calculate convience fees by pax wise
								// debug($page_data);exit();
						$ageband_details = $trip_block_details['data']['BlockTrip']['AgeBands'];
						$domain_id=$this->session->userdata('domain_id'); 
						$page_data['convenience_fees']  = $currency_obj->convenience_fees($page_data['total_price'], $ageband_details, $this->session->userdata('domain_id'));
						
						$page_data['tax_service_sum']	=  $this->sightseeing_lib->tax_service_sum($pre_booking_params['markup_price_summary'], $pre_booking_params['price_summary']);
						//$pre_booking_params['markup_price_summary']
						//debug($page_data['tax_service_sum']);die;
						//Traveller Details
						$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
						//Get the country phone code 
						$Domain_record = $this->custom_db->single_table_records('domain_list', '*');
						$page_data['active_data'] =$Domain_record['data'][0];
						$temp_record = $this->custom_db->single_table_records('api_country_list', '*');
						$page_data['phone_code'] =$temp_record['data'];
						
						$page_data['search_id'] = $search_id;
						//debug($page_data);die;
						$this->load->view('sightseeing/viator/viator_booking_page', $page_data);
					}
				} else {
					redirect(base_url().'index.php/sightseeing/exception?op=Data Modification&notification=Data modified while transfer(Invalid Data received while validating tokens)');
				}
			} else {

				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	}

	/**
	 *  
	 * sending for booking	 
	 */
	function pre_booking($search_id)
	{
		$post_params = $this->input->post();
		$post_params['billing_city'] = 'Bangalore';
		$post_params['billing_zipcode'] = '560100';
		$post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';
		
		//Make sure token and temp token matches
		$valid_temp_token = unserialized_data($post_params['token'], $post_params['token_key']);
		
		if ($valid_temp_token != false) {
			load_sightseen_lib($post_params['booking_source']);

			/****Convert Display currency to Application default currency***/
			//After converting to default currency, storing in temp_booking
			$post_params['token'] = unserialized_data($post_params['token']);
			$currency_obj = new Currency ( array (
						'module_type' => 'transferv1',
						'from' => get_application_currency_preference (),
						'to' => get_application_default_currency () 
			));
			$post_params['token'] = $this->sightseeing_lib->convert_token_to_application_currency($post_params['token'], $currency_obj, $this->user_type);

			$post_params['token'] = serialized_data($post_params['token']);

			$temp_token = unserialized_data($post_params['token']);

			//Insert To temp_booking and proceed
			$temp_booking = $this->module_model->serialize_temp_booking_record($post_params, SIGHTSEEING_BOOKING);

			$this->load->model('General_Model');
			$balance = $this->General_Model->getAgentBalance()->row();
			// debug($balance->balance_credit);die;
			if(isset($balance->balance_credit) && $balance->balance_credit < $post_params['total_amount_val']) 
			{
				$this->session->set_flashdata('message', 'Your balance is low.');
                redirect(base_url().'dashboard/index');				
            } else {
				$book_id = $temp_booking['book_id'];
				$book_origin = $temp_booking['temp_booking_origin'];

				if ($post_params['booking_source'] == PROVAB_SIGHTSEEN_BOOKING_SOURCE) {
					$amount	  = $this->sightseeing_lib->total_price($temp_token['markup_price_summary']);
					$currency = $temp_token['default_currency'];
				}
				$currency_obj = new Currency ( array (
							'module_type' => 'sightseeing',
							'from' => admin_base_currency (),
							'to' => admin_base_currency () 
				));

				/********* Convinence Fees Start ********/
				$search_data = $temp_token['AgeBands'];
				$convenience_fees = $currency_obj->convenience_fees($amount, $search_data);
				
				/********* Promocode Start ********/
				$promocode_discount = $post_params['promo_code_discount_val'];
				
				//details for PGI
				$email = $post_params ['billing_email'];
				$phone = $post_params ['passenger_contact'];
				//	$verification_amount = roundoff_number($amount+$convenience_fees-$promocode_discount);
				$verification_amount = roundoff_number($amount);
				$firstname = $post_params ['first_name'] ['0'];
				$productinfo = META_SIGHTSEEING_COURSE;

				//check current balance before proceeding further
				$domain_balance_status = $this->domain_management_model->verify_current_balance($verification_amount, $currency);				
				if ($domain_balance_status == true) {
					switch($post_params['payment_method']) {
						case PAY_NOW :
							$pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
							$this->transaction->create_payment_record($book_id, $verification_amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount, $pg_currency_conversion_rate);

					if($this->session->userdata('user_logged_in') == 0 || $this->session->userdata('user_type') ==B2B2C_USER){
					
							 redirect(base_url() . 'index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin);
						}
						else 
						{ 
							redirect(base_url().'index.php/sightseeing/process_booking/'.$book_id.'/'.$book_origin);
							}					
							break;
						case PAY_AT_BANK : echo 'Under Construction - Remote IO Error';exit;
						break;
					}
				} else {
					redirect(base_url().'index.php/sightseeing/exception?op=Amount Activities Booking&notification=insufficient_balance');
				}
			}
		} else {
			redirect(base_url().'index.php/sightseeing/exception?op=Remote IO error @ Activities Booking&notification=validation');
		}
	}


	/*
		process booking in backend until show loader 
	*/
	function process_booking($book_id, $temp_book_origin){
		
		if($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0){

			$page_data ['form_url'] = base_url () . 'index.php/sightseeing/secure_booking';
			$page_data ['form_method'] = 'POST';
			$page_data ['form_params'] ['book_id'] = $book_id;
			$page_data ['form_params'] ['temp_book_origin'] = $temp_book_origin;
			// debug($page_data);
			// exit;
			$this->load->view('share/loader/booking_process_loader', $page_data);	

		}else{
			redirect(base_url().'index.php/sightseeing/exception?op=Invalid request&notification=validation');
		}
		
	}

	function agent_balance_mangment($book_params,$temp_booking)
	{ 
		$agent_id= $this->session->userdata('user_details_id');  
		$agent_balance=$this->transaction->get_agent_balance($agent_id);
		if($this->user_type == 'b2c') {
			$fair_detail=$book_params['fare']+$book_params['admin_markup']+$book_params['convinence']-$book_params['agent_markup'];
		} else {
			$fair_detail=$book_params['fare']+$book_params['admin_markup']+$book_params['convinence']-$book_params['agent_markup']-$book_params['agent_commission']+$book_params['agent_tds'];
		}
		//$fair_detail=$book_params['fare']+$book_params['admin_markup']+$book_params['convinence']-$book_params['agent_markup'];
		$agent_account_balance=$agent_balance['balance_credit'];
		$total_fair=$fair_detail; 
		$update_agent_balance= $agent_account_balance - $total_fair;

		$transaction_logs =
			array(
				'user_id' => $agent_id,
				'last_balance'=> $agent_balance['balance_credit'],
				'current_balance'=> $update_agent_balance,
				'amount_debited'=> $total_fair,
				'app_ref'=> $temp_booking['book_id'], 
				'module'=> "SIGHTSEEING",
				'log_type'=> "BOOKING",
			);
		$this->transaction->update_transaction_logs($transaction_logs);  
		$this->transaction->update_agent_balance($update_agent_balance,$agent_id);  
	}

	/**
	 * 
	 *Do booking once payment is successfull - Payment Gateway
	 *and issue voucher
	 */
	function secure_booking()
	{
		$post_data = $this->input->post();
		
		if(valid_array($post_data) == true && isset($post_data['book_id']) == true && isset($post_data['temp_book_origin']) == true &&
			empty($post_data['book_id']) == false && intval($post_data['temp_book_origin']) > 0){
			//verify payment status and continue
			$book_id = trim($post_data['book_id']);
			$temp_book_origin = intval($post_data['temp_book_origin']);
			// $booking_status = $this->transaction->get_payment_status($book_id);
			
			// if($booking_status['status'] !== 'accepted'){
			// 	redirect(base_url().'index.php/sightseeing/exception?op=Payment Not Done&notification=validation');
			// }
		} else{
			redirect(base_url().'index.php/sightseeing/exception?op=InvalidBooking&notification=invalid');
		}	
		
		//run booking request and do booking
		$temp_booking = $this->module_model->unserialize_temp_booking_record($book_id, $temp_book_origin);

		$promo_currency_obj = new Currency(array('module_type' => 'sightseeing', 'from' => get_application_currency_preference(), 'to' => admin_base_currency()));
		
		//Delete the temp_booking record, after accessing
		// $this->module_model->delete_temp_booking_record ($book_id, $temp_book_origin);
		load_sightseen_lib($temp_booking['booking_source']);

		//verify payment status and continue		

		$total_booking_price = $this->sightseeing_lib->total_price($temp_booking['book_attributes']['token']['markup_price_summary']);		
		$currency = $temp_booking['book_attributes']['token']['default_currency'];
		//also verify provab balance
		//check current balance before proceeding further
		$domain_balance_status = $this->domain_management_model->verify_current_balance($total_booking_price, $currency);
		 // debug(PROVAB_SIGHTSEEN_BOOKING_SOURCE);exit('ghf');

		if ($domain_balance_status) {
			//lock table
			if ($temp_booking != false) {
				switch ($temp_booking['booking_source']) {
					case PROVAB_SIGHTSEEN_BOOKING_SOURCE :
					
						//FIXME : COntinue from here - Booking request
						$booking = $this->sightseeing_lib->process_booking($book_id, $temp_booking['book_attributes']);
						//Save booking based on booking status and book id
						break;
				}
				// debug($booking['status']);die;
				if ($booking['status'] == SUCCESS_STATUS) {

					$currency_obj = new Currency(array('module_type' => 'sightseeing', 'from' => admin_base_currency(), 'to' => admin_base_currency()));

					$booking['data']['currency_obj'] = $currency_obj;
					$booking['data']['promo_currency_obj'] = $promo_currency_obj;
					$booking['data']['domain_id'] = $GLOBALS['CI']->session->userdata('domain_id');
					$booking['data']['branch_id'] = $GLOBALS['CI']->session->userdata('branch_id');
					$user_details_id = $GLOBALS['CI']->session->userdata('user_details_id');
					$booking['data']['user_details_id'] = $GLOBALS['CI']->session->userdata('user_details_id');
					$booking['data']['user_type'] = $GLOBALS['CI']->session->userdata('user_type');
					//Save booking based on booking status and book id
					$data = $this->sightseeing_lib->save_booking($book_id, $booking['data'],$this->user_type);

					if($data['booking_status'] == "BOOKING_CONFIRMED")
					{
						$this->agent_balance_mangment($data,$temp_booking);
						//$this->send_mail_agent($temp_booking);   	
					}

					$this->domain_management_model->update_transaction_details('sightseeing', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount'],$data['transaction_currency'], $data['currency_conversion_rate'] );
					// echo 'dfhg11';exit;	
					// exit('test1');
					redirect(base_url().'index.php/voucher/sightseeing/'.$book_id.'/'.$temp_booking['booking_source'].'/'.$data['booking_status'].'/show_voucher');
				} else {
					// echo 'dfhg22';exit;	
					redirect(base_url().'index.php/sightseeing/exception?op=booking_exception&notification='.$booking['Message']);
				}
			}
			//release table lock
		} else {
			// echo 'dfhg2';exit;	
			redirect(base_url().'index.php/sightseeing/exception?op=Remote IO error @ Insufficient&notification=validation');
		}
		//redirect(base_url().'index.php/hotel/exception?op=Remote IO error @ Hotel Secure Booking&notification=validation');
	}

	function test(){
		$currency_obj = new Currency(array('module_type' => 'sightseeing', 'from' => admin_base_currency(), 'to' => admin_base_currency()));
		debug($currency_obj);
	}	
	/**
	 * Elavarasi
	 */
	function pre_cancellation($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$booking_details = $this->sightseeing_model->get_booking_details($app_reference, $booking_source);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_sightseeing_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				$this->template->view('sightseeing/pre_cancellation', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	/*
	 * Elavarasi
	 * Process the Booking Cancellation
	 * Full Booking Cancellation
	 *
	 */
	function cancel_booking($app_reference, $booking_source)
	{
		if(empty($app_reference) == false) {
			$get_params = $this->input->get();

			$master_booking_details = $this->sightseeing_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_sightseeing_booking_data($master_booking_details, 'b2c');
				$master_booking_details = $master_booking_details['data']['booking_details'][0];
				load_sightseen_lib($booking_source);
				$cancellation_details = $this->sightseeing_lib->cancel_booking($master_booking_details,$get_params);//Invoke Cancellation Methods
				if($cancellation_details['status'] == false) {
					$query_string = '?error_msg='.$cancellation_details['msg'];
				} else {
					$query_string = '';
				}
				redirect('sightseeing/cancellation_details/'.$app_reference.'/'.$booking_source.$query_string);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	/**
	 * Elavarasi
	 * Cancellation Details
	 * @param $app_reference
	 * @param $booking_source
	 */
	function cancellation_details($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $GLOBALS['CI']->sightseeing_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$page_data = array();
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_sightseeing_booking_data($master_booking_details, 'b2c');
				$page_data['data'] = $master_booking_details['data'];
				$this->template->view('sightseeing/cancellation_details', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}

	}
/**
	 * Elavarasi
	 */
	function exception($redirect=true)
	{
		$module = META_SIGHTSEEING_COURSE;

		$op = (empty($_GET['op']) == true ? '' : $_GET['op']);
		$notification = (empty($_GET['notification']) == true ? '' : $_GET['notification']);

		if($op == 'Some Problem Occured. Please Search Again to continue'){
			$op = 'Some Problem Occured. ';
		}
		if($notification=='In Sufficiant Balance'){
		
			$notification = 'In Sufficiant Balance For Activities';
		}	

		$eid = $this->module_model->log_exception($module, $op, $notification);

		//set ip log session before redirection
		$this->session->set_flashdata(array('log_ip_info' => true));
		
		if($redirect){
			redirect(base_url().'index.php/sightseeing/event_logger/'.$eid);
		}
		
	}
	function event_logger($eid='')
	{
		
		$log_ip_info = $this->session->flashdata('log_ip_info');

		$exception_data  = $this->custom_db->single_table_records('exception_logger','*',array('exception_id'=>$eid),0,1);
		
		$exception=$exception_data['data'][0];
		
		$this->load->view('sightseeing/exception', array('log_ip_info' => $log_ip_info, 'exception'=>$exception,'eid' => $eid));
	}


}
