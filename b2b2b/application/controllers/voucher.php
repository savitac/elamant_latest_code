<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package    Provab
 * @subpackage Bus
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */
error_reporting(E_ALL);
class Voucher extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->load->library("provab_pdf");
		$this->load->library('provab_mailer');
		$this->load->library('booking_data_formatter'); 
		$this->load->model('General_Model'); 
		$this->load->model('module_model');
		$this->load->model('transferv1_model');
		$this->load->model('hotels_model');
		$this->user_type = $this->config->item('user_type');
		//we need to activate bus api which are active for current domain and load those libraries
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 *
	 */
	function bus($app_reference, $booking_source='', $booking_status='', $operation='show_voucher')
	{
		//echo 'under working';exit;
		$this->load->model('bus_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->bus_model->get_booking_details($app_reference, $booking_source, $booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_bus_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
					$domain_address = $this->custom_db->single_table_records ( 'domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					
				
				}
				switch ($operation) {
					case 'show_voucher' :
						$page_data['button'] = ACTIVE;
						$page_datap['image'] = ACTIVE;
						$this->load->view('voucher/bus_voucher', $page_data);
					break;
					case 'show_pdf' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->load->isolated_view('voucher/bus_pdf', $page_data);
						$create_pdf->create_pdf($get_view,'show');
						break;
					case 'email_voucher' : 
						$page_data['button'] = INACTIVE;
						$page_data['image'] = INACTIVE;
						$mail_template = $this->load->isolated_view('voucher/bus', $page_data);
						//$pdf = $this->provab_pdf->create_pdf($mail_template);
						$pdf = "";
						$email = $this->entity_email;
						$this->provab_mailer->send_mail($email, domain_name().' - Bus Ticket', $mail_template,$pdf);
					break;
				}
			}
		}
	}
	/*Transfers Viator*/
	function transferv1($app_reference, $booking_source='', $booking_status='', $operation='show_voucher') {
		// debug($app_reference);die;
		if (empty($app_reference) == false) {
			$booking_details = $this->transferv1_model->get_booking_details($app_reference, $booking_source, $booking_status);

			
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_transferv1_booking_data($booking_details, $this->user_type);
				$page_data['data'] = $assembled_booking_details['data'];
                if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
				
					$domain_address = $this->custom_db->single_table_records ('domain_list','address,domain_logo,phone,domain_name',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					$page_data['data']['phone'] = $domain_address['data'][0]['phone'];
					$page_data['data']['domainname'] = $domain_address['data'][0]['domain_name'];

					
				}

				switch ($operation) {
					case 'show_voucher' : $this->load->view('voucher/transferv1_voucher', $page_data);
					break;
					case 'show_pdf' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->load->view('voucher/transferv1_pdf', $page_data,true);
						$create_pdf->create_pdf($get_view,'show');
						break;
				}
			}
		}
	}


	function hotel($app_reference, $booking_source='', $booking_status='', $operation='show_voucher')
	{
		$this->load->model('hotel_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $booking_status);
			//debug($booking_details);die(); 
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, 'b2c');
				//debug($assembled_booking_details); exit;
				$page_data['data'] = $assembled_booking_details['data'];
                if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
				
					$domain_address = $this->custom_db->single_table_records ('domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					
				}

				switch ($operation) {
					case 'show_voucher' : $this->load->view('voucher/hotel_voucher', $page_data);
					break;
					case 'show_pdf' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->load->view('voucher/hotel_pdf', $page_data);
						$create_pdf->create_pdf($get_view,'show');
						break;
				}
			}
		}
	}

	function hotel_email()
	{
		$app_reference=$_POST['app_reference'];
		$booking_source=$_POST['booking_source'];
		$booking_status=$_POST['status'];
		$operation="email_voucher";
		$email=$_POST['email'];
		$this->load->model('hotel_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $booking_status);
			//debug($booking_details);die(); 
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, 'b2c');
				//debug($assembled_booking_details); exit;
				$page_data['data'] = $assembled_booking_details['data'];
                if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
				
					$domain_address = $this->custom_db->single_table_records ('domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					
				}

				switch ($operation) {
				   case 'email_voucher': 
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('voucher/hotel_pdf', $page_data,TRUE);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$mail_status=$this->provab_mailer->send_mail($email, domain_name().' - Flight Ticket',$mail_template ,'');
						if($mail_status['status']==TRUE)
						{
							redirect("booking/hotelOrders");  
						}
						else
						{
							 
						}
						; 
						break; 
				}
			}
		}
	}

	function hotelcrs_email($hotel_type)
	{
		$app_reference=$_POST['app_reference'];
		$booking_source=$_POST['booking_source'];
		$booking_status=$_POST['status'];
		$operation="email_voucher";
		$email=$_POST['email'];
		$this->load->model('hotels_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->hotels_model->getBookingDetails($app_reference);
			for ($b=0; $b < count($booking_details); $b++) 
			{ 
				if ($booking_details[$b]->booking_status == "CONFIRM" || $booking_details[$b]->booking_status == "CANCELLED") 
				{
					// Assemble Booking Data
					$email = $booking_details[$b]->email_id;
					$email='savisoman92@gmail.com';
					$voucher_details['other'] = $this->hotels_model->get_voucher_details($app_reference);
					$voucher_details['convenience_fee'] =$booking_details[$b]->service_charge;

					switch ($operation) {
					   case 'email_voucher': 
							$this->load->library('provab_pdf');
							$create_pdf = new Provab_Pdf();
							$mail_template = $this->load->view('voucher/hotels_voucher', $voucher_details,true);
							$pdf = $create_pdf->create_pdf($mail_template,'');
							$mail_status=$this->provab_mailer->send_mail($email, domain_name().' - Hotel Ticket',$mail_template ,'');
							if($mail_status['status']==TRUE)
							{
								if($hotel_type == 'villa')
									redirect("booking/villaOrders");
								else 
									redirect("booking/hotelcrsOrders");
							}
							break; 
					}
				}
			}
		}
	}

	function format_hotel_booking_data($complete_booking_details, $module)
	{
		$response['status']	= SUCCESS_STATUS;
		$response['data']	= array();
		$booking_details = array();
		$currency_obj = new Currency();
		$master_booking_details = $complete_booking_details['data']['booking_details'];
	   //echo debug($master_booking_details);exit;
		$itinerary_details = $this->format_itinerary_details($complete_booking_details['data']['booking_itinerary_details']);
	    //echo debug($itinerary_details);exit;
	
		$customer_details = $this->format_customer_details($complete_booking_details['data']['booking_customer_details']);
		$cancellation_details = $this->format_hotel_cancellation_details($complete_booking_details['data']['cancellation_details']);
		foreach($master_booking_details as $book_k => $book_v) {
			$core_booking_details = $book_v;
			
			$app_reference = $core_booking_details['app_reference'];
			$booking_itinerary_details = $itinerary_details[$app_reference];
			$booking_customer_details = $customer_details[$app_reference];
			$booking_cancellation_details = @$cancellation_details[$app_reference];		
			if($module == 'b2b' || $module == 'b2c' ){
				//get the converted currency rate for booking transaction
				$booking_itinerary_details = $this->display_currency_for_hotel($book_v,$itinerary_details[$app_reference]);
				//get the converted currency rate for convenience amount
				$core_booking_details = $this->display_convenience_amount($book_v);
			}
			//Calculating Price
			$fare = 0;
			$admin_markup = 0;
			$agent_markup = 0;
			foreach($booking_itinerary_details as $itinerary_k => $itinerary_v) {
				$fare += $itinerary_v['total_fare'];
				$admin_markup += floatval($itinerary_v['admin_markup']);
				$agent_markup += floatval($itinerary_v['agent_markup']);
			}
			//PaxCount
			$adult_count = 0;
			$child_count = 0;
			foreach($booking_customer_details as $customer_k => $customer_v) {
				$pax_type = $customer_v['pax_type'];
				if($pax_type == 'Adult') {
					$adult_count++;
				} else if($pax_type == 'Child'){
					$child_count++;
				}
			}
			//Formatiing the data
			//Booking Details
			$attributes = json_decode($core_booking_details['attributes'], true);
			$total_nights = get_date_difference($core_booking_details['hotel_check_in'], $core_booking_details['hotel_check_out']);
			$core_booking_details['hotel_image'] = $attributes['HotelImage'];
			$core_booking_details['hotel_location'] = $booking_itinerary_details[0]['location'];
			$core_booking_details['hotel_address'] = $attributes['HotelAddress'];
			$core_booking_details['cancellation_policy'] = $attributes['CancellationPolicy'];
			$core_booking_details['total_nights'] = $total_nights;
			$core_booking_details['total_rooms'] = count($booking_itinerary_details);
			$core_booking_details['adult_count'] = $adult_count;
			$core_booking_details['child_count'] = $child_count;
			$core_booking_details['fare'] = roundoff_number($fare);
			$core_booking_details['admin_markup'] = roundoff_number($admin_markup);
			$core_booking_details['agent_markup'] = roundoff_number($agent_markup);
			$admin_buying_price = $this->admin_buying_price($core_booking_details);
			$core_booking_details['admin_buying_price'] = roundoff_number($admin_buying_price[0]);
			if($module == 'b2b') {
				$agent_buying_price = $this->agent_buying_price($core_booking_details);
				$core_booking_details['agent_buying_price'] = roundoff_number($agent_buying_price[0]);
			} else {
				$core_booking_details['agent_buying_price'] = 0;
			}
			$grand_total = $this->total_fare($core_booking_details, $module);
			$core_booking_details['grand_total'] = roundoff_number($grand_total[0]);
			
			//get currency
			$currency = admin_base_currency();
			if($module == 'b2b' || $module == 'b2c' ){
			    $currency = $book_v['currency'];
			}
			$core_booking_details['currency'] = $currency_obj->get_currency_symbol($currency);
			
			$core_booking_details['voucher_date'] = app_friendly_absolute_date($core_booking_details['created_datetime']);
			//Lead Pax Details
			$core_booking_details['cutomer_city'] = $attributes['billing_city'];
			$core_booking_details['cutomer_zipcode'] = $attributes['billing_zipcode'];
			$core_booking_details['cutomer_address'] = $attributes['address'];
			$core_booking_details['cutomer_country'] = $attributes['billing_country'];
			$core_booking_details['lead_pax_name'] = $booking_customer_details[0]['title'].' '.$booking_customer_details[0]['first_name'].' '.$booking_customer_details[0]['last_name'];
			$core_booking_details['lead_pax_phone_number'] = $core_booking_details['phone_number'];
			$core_booking_details['lead_pax_email'] = $core_booking_details['email']; 
			//Domain Details
			$domain_details = $this->domain_details($core_booking_details['domain_origin']);
			$core_booking_details['domain_name'] = $domain_details['domain_name'];
			$core_booking_details['domain_ip'] = $domain_details['domain_ip'];
			$core_booking_details['domain_key'] = $domain_details['domain_key'];
			$core_booking_details['theme_id'] = $domain_details['theme_id'];
			$core_booking_details['domain_logo'] = $domain_details['domain_logo'];
			//Formating the data
			//Booking Details
			$booking_details[$app_reference] = $core_booking_details;
			//Itenary Details
			$booking_details[$app_reference]['itinerary_details'] = $booking_itinerary_details;
			//Customer Details
			$booking_details[$app_reference]['customer_details'] = $booking_customer_details;
			$booking_details[$app_reference]['cancellation_details'] = $booking_cancellation_details;
		}
	
		$booking_details = $this->convert_as_array($booking_details);
		$response['data']['booking_details'] = $booking_details;
		return $response;
	}

	/**
	 *
	 */
	function flight($app_reference, $booking_source='', $booking_status='', $operation='show_voucher',$email='')
	{
		$this->load->model('flight_model');
		if (empty($app_reference) == false) {

			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);				

			if ($booking_details['status'] == SUCCESS_STATUS) {
				//$this->load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->user_type);
				$page_data['data'] = $assembled_booking_details['data'];
					
			/*if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
					
					$domain_address = $this->custom_db->single_table_records ( 'domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					if($assembled_booking_details['data']['booking_details'][0]['created_by_id'] > 0){
						$get_agent_info = $this->user_model->get_agent_info($assembled_booking_details['data']['booking_details'][0]['created_by_id']);
						if(!empty($get_agent_info)){
						$page_data['data']['address'] = $get_agent_info[0]['address'];
						$page_data['data']['logo'] = $get_agent_info[0]['logo'];
						}
					}
			
				}*/
				switch ($operation) {
					case 'show_voucher' : $this->load->view('voucher/flight_voucher', $page_data);
					break;
					case 'show_pdf' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->load->view('voucher/flight_pdf', $page_data,TRUE);
						$create_pdf->create_pdf($get_view,'show');
						break;
				   case 'email_voucher':
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('voucher/flight_pdf', $page_data,TRUE);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$this->provab_mailer->send_mail($email,' -Itinerary Segement Information For New Booking',$mail_template ,$pdf); 
						break;     
				}
			}
		}
	}
	function flight_email()
	{
		$app_reference=$_POST['app_reference'];
		$booking_source=$_POST['booking_source'];
		$booking_status=$_POST['status'];
		$operation="email_voucher";
		$email=$_POST['email'];  
		$this->load->model('flight_model');
		if (empty($app_reference) == false) {

			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);

			if ($booking_details['status'] == SUCCESS_STATUS) {
				//$this->load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				switch ($operation) {
				   case 'email_voucher': 
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('voucher/flight_pdf', $page_data,TRUE);
						$pdf = $create_pdf->create_pdf($mail_template,'');
						$mail_status=$this->provab_mailer->send_mail($email,' - Itinerary Segment Information For New Booking ',$mail_template ,$pdf);
						if($mail_status['status']==TRUE)
						{
							redirect("booking/flightOrders");  
						}
						else
						{
							 
						}
						; 
						break; 
				}
			}
		}
	} 
	function flight_pay_later()
	{
		error_reporting(0);
		$uniq_data=$this->session->userdata("uniqdata");
		$this->load->model('flight_model');
		$book_id=$uniq_data['form_params']['book_id'];
		$temp_book_origin=$uniq_data['form_params']['temp_book_origin'];
		$domain_address = $this->custom_db->single_table_records ( 'domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
		$flight_data = $this->flight_model->get_fight_details_booking_on_hold($book_id);
		$booking_details = $this->flight_model->get_booking_details($book_id, '', '');


		//echo '<pre>';print_r($booking_details);exit;
		$this->load->library('booking_data_formatter'); 
		$email=$booking_details['data']['booking_details']['0']['email'];
		$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2c');
		//exit("");
		$this->load->model('module_model');
		$temp_booking = $this->module_model->unserialize_temp_booking_record( $book_id, 'BOOKING_HOLD' );
		$site_name = str_replace('/','',$_SERVER['HTTP_HOST']);
      	$site_name = str_ireplace('www.','',$_SERVER['HTTP_HOST']); 
      	$site_details  =  $this->General_Model->get_site_details($site_name);
		$page_data['data'] = $assembled_booking_details['data'];
		$page_data['data']['address'] =$domain_address['data'][0]['address'];
		$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
      	$page_data['admin_details']= $site_details; 
		//$page_data['offline_msg'] = "For Paying offline please contact the on email"..

		$this->load->library('provab_pdf'); 

			//$create_pdf = new Provab_Pdf();
						
		//$mail_template = $this->load->view('voucher/flight_pdf', $page_data, TRUE);
		//$pdf = $create_pdf->create_pdf($mail_template,'');
		//$email="pawan.provabmail@gmail.com";  
		$mail=$this->provab_mailer->send_mail($email, domain_name().' -offline Flight Ticket',$mail_template );
		$this->load->view('voucher/flight_pdf', $page_data);
	}

	function hotel_pay_later()
	{
		error_reporting(0);
		$uniq_data=$this->session->userdata("uniqdata");
		//debug($uniq_data);die(" UP ON>LY"); 
		$this->load->model('flight_model');
		$book_id=$uniq_data['form_params']['book_id'];
		$temp_book_origin=$uniq_data['form_params']['temp_book_origin'];
		$domain_address = $this->custom_db->single_table_records ( 'domain_list','address,domain_logo',array('origin'=>get_domain_auth_id()));
		$temp_booking = $this->module_model->unserialize_temp_booking_record($book_id, $temp_book_origin);
		//debug($temp_booking);die(" UP ONLY THEN");  
		$site_name = str_replace('/','',$_SERVER['HTTP_HOST']);
      	$site_name = str_ireplace('www.','',$_SERVER['HTTP_HOST']); 
      	$site_details  =  $this->General_Model->get_site_details($site_name);
		$page_data['data'] = $assembled_booking_details['data'];
		$page_data['data']['address'] =$domain_address['data'][0]['address'];
		$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
      	$page_data['admin_details']= $site_details; 
      	$page_data['temp_booking'] = $temp_booking;  
      	$email=$site_details->user_email;
		$this->load->library('provab_pdf'); 						
		$mail_template = $this->load->view('voucher/hotel_offline_voucher', $page_data, TRUE); 
		//$email="pawan.provabmail@gmail.com";   
		$mail=$this->provab_mailer->send_mail($email, domain_name().' -offline Hotel Ticket',$mail_template ); 
		$this->load->view('voucher/hotel_offline_voucher', $page_data); 
	}
	function load_flight_lib($source)
	{
		
		 switch ($source) {
			case PROVAB_FLIGHT_BOOKING_SOURCE :
			    $this->load->library('flight/provab_private', '', 'flight_lib');
				break;
			default : redirect(base_url());
		}
	}

	/*For Sightseeing*/
	function sightseeing($app_reference, $booking_source='', $booking_status='', $operation='show_voucher'){
		$this->load->model('sightseeing_model');

		if (empty($app_reference) == false) {
			$booking_details = $this->sightseeing_model->get_booking_details($app_reference, $booking_source, $booking_status);

			
			if ($booking_details['status'] == SUCCESS_STATUS) {
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_sightseeing_booking_data($booking_details, $this->user_type);
				

				$page_data['data'] = $assembled_booking_details['data'];
                if(isset($assembled_booking_details['data']['booking_details'][0])){
					//get agent address & logo for b2b voucher
				
					$domain_address = $this->custom_db->single_table_records ('domain_list','address,domain_logo,phone,domain_name',array('origin'=>get_domain_auth_id()));
					$page_data['data']['address'] =$domain_address['data'][0]['address'];
					$page_data['data']['logo'] = $domain_address['data'][0]['domain_logo'];
					$page_data['data']['phone'] = $domain_address['data'][0]['phone'];
					$page_data['data']['domainname'] = $domain_address['data'][0]['domain_name'];
				}

				switch ($operation) {
					case 'show_voucher' : $this->load->view('voucher/sightseeing_voucher', $page_data);
					break;
					case 'show_pdf' :
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$get_view=$this->template->load('voucher/sightseeing_pdf', $page_data);
						$create_pdf->create_pdf($get_view,'show');
						break;
				}
			}
		}
	}

	function hotels($app_reference, $booking_source = '', $booking_status = '', $operation = 'show_voucher') {
		$this->load->model ( 'hotels_model' );
		if (empty ( $app_reference ) == false) 
		{
			$booking_details = $this->hotels_model->getBookingDetails($app_reference);
			//debug($booking_details);exit;
			for ($b=0; $b < count($booking_details); $b++) 
			{ 
				if ($booking_details[$b]->booking_status == "CONFIRM" || $booking_details[$b]->booking_status == "CANCELLED") 
				{
					// Assemble Booking Data
					$email = $booking_details[$b]->email_id;
					$voucher_details['other'] = $this->hotels_model->get_voucher_details($app_reference);
					$voucher_details['convenience_fee'] =$booking_details[$b]->service_charge;
					
					switch ($operation) {
						case 'show_voucher' :
							$this->load->library('provab_pdf');
							$create_pdf = new Provab_Pdf();
							$mail_template = $this->load->view('voucher/hotels_voucher', $voucher_details,true);
							//$pdf = $create_pdf->create_pdf($mail_template,'');
							//$this->provab_mailer->send_mail($email, domain_name().' - Hotel Voucher',$mail_template ,'');
							$this->load->view ( 'voucher/hotels_voucher', $voucher_details );
							break;	
						case 'show_pdf' :
							$this->load->library('provab_pdf');
							$create_pdf = new Provab_Pdf();
							$mail_template = $this->load->view('voucher/hotels_voucher', $voucher_details,true);
							//$get_view = $create_pdf->create_pdf($mail_template,'');
							//$this->provab_mailer->send_mail($email, domain_name().' - Hotel Voucher',$mail_template ,'');
							$this->provab_pdf->create_pdf ( $mail_template, 'D'); 
							$this->load->view ( 'voucher/hotels_voucher', $voucher_details );
							break;
						default:	
					}
				} else {
					$msg = 'Room booking Failed , Room unavailable';
					redirect ( base_url () . 'index.php/hotel/exception?op=booking_exception&notification=' . $msg );
				}
			}
		}
	}
}



