<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
/**
 *
 * @package Provab
 * @subpackage Flight
 * @author Arjun J<arjunjgowda260389@gmail.com>
 * @Modified Pawan Bagga<pawan.provabmail@gmail.com>
 * @version V1
 */
 
 error_reporting(0); 
 session_start();
class Flight extends CI_Controller {
	private $current_module; 
	public function __construct() {
		parent::__construct ();
		// $this->output->enable_profiler(TRUE);
		$this->load->model ('flight_model');
		$this->load->model ('General_Model');
		$this->load->model ('Sitemanagement_Model');
		$this->load->model ('user_model'); // we need to load user model to access provab sms library
		$this->load->library('provab_mailer'); 
		$this->load->model('transaction');
		$this->load->model ('custom_db'); // we need to load user model to access provab sms library
		$this->load->model ('db_cache_api'); 
		$this->load->model('module_model');	
		$this->load->model ('account_model'); 
		$this->load->model('domain_management_model');	
		$this->lang->load('english','Dynamic_Languages');
        $this->TravelLights = $this->lang->line('TravelLights');
        $this->load->helper('app');
		//$this->load->helper('currency');
		$this->load->library('xml_to_array');
		$this->load->library('booking_data_formatter'); 
		$this->current_module = $this->config->item('current_module'); 
		$this->user_type = $this->config->item('user_type');
		// loading libray added by ajaz
		/*$this->load->library('template');
		$this->load->library('js_loader');*/
	}
	
	/**
	 * Pre Search For Flight
	 */
	function pre_flight_search($search_id='')
	{  
		$search_params = $this->input->get();	
		//echo"<pre>";print_r($search_params);die;
        $params = http_build_query($search_params);
		//echo"<pre>";print_r($params);die;
		$myqry = '';
		$search_id = $this->save_pre_search(META_AIRLINE_COURSE);
		//echo $search_id;die;
		$this->save_search_cookie(META_AIRLINE_COURSE, $search_id);
		//echo $search_id;die;
	    //Analytics
		$this->load->model('flight_model');
		$this->flight_model->save_search_data($search_params, META_AIRLINE_COURSE);
		//redirect(base_url().'index.php/flight/search/'.$search_id.'?'.$myqry.$_SERVER['QUERY_STRING']);
		redirect(base_url().'index.php/flight/search/'.$search_id.'?'.$myqry.$params);
	}
	
	/**
	 * Pre Search used to save the data
	 *
	 */
	private function save_pre_search($search_type)
	{
		//Save data
		$search_params = $this->input->get();
		//echo"<pre>";print_r($search_params);die;
		$search_data = json_encode($search_params);
		//echo"<pre>";print_r($search_type);die;
		$insert_id = $this->custom_db->insert_record('search_history', array('search_type' => $search_type, 'search_data' => $search_data, 'created_datetime' => date('Y-m-d H:i:s')));
		//echo"<pre>";print_r($insert_id);die;
		return $insert_id['insert_id'];
	}
	
	/**
	 * Set Search id in cookie
	 */
	private function save_search_cookie($module, $search_id)
	{
		$sparam = array();
		$sparam = $this->input->cookie('sparam', TRUE);
		if (empty($sparam) == false) {
			$sparam = unserialize($sparam);
		}
		$sparam[$module] = $search_id;
        $cookie = array(
			'name' => 'sparam',
			'value' => serialize($sparam),
			'expire' => '86500',
			'path' => PROJECT_COOKIE_PATH
		);
		$this->input->set_cookie($cookie);
	}


	
	/**
	 * App Validation and reset of data
	 */
	function pre_calendar_fare_search()
	{
		$params = $this->input->get();
		$safe_search_data = $this->flight_model->calendar_safe_search_data($params);
		//Need to check if its domestic travel
		$from_loc = $safe_search_data['from_loc'];
		$to_loc = $safe_search_data['to_loc'];
		$safe_search_data['is_domestic_one_way_flight'] = false;
        $safe_search_data['is_domestic_one_way_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);
		if ($safe_search_data['is_domestic_one_way_flight'] == false) {
			$page_params['from'] = '';
			$page_params['to'] = '';
		} else {
			$page_params['from'] = $safe_search_data['from'];
			$page_params['to'] = $safe_search_data['to'];
		}

		$page_params['depature'] = $safe_search_data['depature'];
		$page_params['carrier'] = $safe_search_data['carrier'];
		$page_params['adult'] = $safe_search_data['adult'];
		redirect(base_url().'index.php/flight/calendar_fare?'.http_build_query($page_params));
	}

	/**
	 * Airfare calendar
	 */
	function calendar_fare()
	{
		$params = $this->input->get();
		$active_booking_source = $this->flight_model->active_booking_source ();
		if (valid_array($active_booking_source) == true) {
			$safe_search_data = $this->flight_model->calendar_safe_search_data($params);
			$page_params = array (
					'flight_search_params' => $safe_search_data ,
					'active_booking_source' => $active_booking_source
			);
			$page_params ['from_currency'] = get_application_default_currency ();
			$page_params ['to_currency'] = get_application_currency_preference ();
			$this->template->view ( 'flight/calendar_fare_result', $page_params );
		}
	}
	/**
	 * Jaganaath
	 */
	function add_days_todate()
	{
		
		$get_data = $this->input->get();
		if(isset($get_data['search_id']) == true && intval($get_data['search_id']) > 0 && isset($get_data['new_date']) == true && empty($get_data['new_date']) == false) {
			$search_id = intval($get_data['search_id']);
			$new_date = trim($get_data['new_date']);
			$safe_search_data = $this->flight_model->get_safe_search_data ( $search_id );

			$day_diff = get_date_difference($safe_search_data['data']['depature'], $new_date);
			if(valid_array($safe_search_data) == true && $safe_search_data['status'] == true) {
				$safe_search_data = $safe_search_data['data'];
				$search_params = array();
				$search_params['trip_type'] = trim($safe_search_data['trip_type']);
				$search_params['from'] = trim($safe_search_data['from']);
				$search_params['to'] = trim($safe_search_data['to']);
				$search_params['depature'] = date('d-m-Y', strtotime($new_date));//Adding new Date
				if(isset($safe_search_data['return'])) {
					$search_params['return'] = add_days_to_date($day_diff, $safe_search_data['return']);//Check it
				}
				$search_params['adult'] = intval($safe_search_data['adult_config']);
				$search_params['child'] = intval($safe_search_data['child_config']);
				$search_params['infant'] = intval($safe_search_data['infant_config']);
				$search_params['search_flight'] = 'search';
				$search_params['v_class'] = trim($safe_search_data['v_class']);
				$search_params['carrier'] = $safe_search_data['carrier'];
				redirect(base_url().'index.php/general/pre_flight_search/?'.http_build_query($search_params));
			} else {
				$this->load->view ( 'general/popup_redirect');
			}
		} else {
			$this->load->view ( 'general/popup_redirect');
		}
	}
	/**
	 * Jaganath
	 * Search Request from Fare Calendar
	 */
	function pre_fare_search_result()
	{
		$get_data = $this->input->get();
		if(isset($get_data['from']) == true && empty($get_data['from']) == false &&
		isset($get_data['to']) == true && empty($get_data['to']) == false &&
		isset($get_data['depature']) == true && empty($get_data['depature']) == false) {
			$from = trim($get_data['from']);
			$to = trim($get_data['to']);
			$depature = trim($get_data['depature']);
			$from_loc_details = $this->custom_db->single_table_records('flight_airport_list', '*', array('airport_code' => $from));
			$to_loc_details = $this->custom_db->single_table_records('flight_airport_list', '*', array('airport_code' => $to));
			if($from_loc_details['status'] == true && $to_loc_details['status'] == true) {
				$depature = date('Y-m-d', strtotime($depature));
				$airport_code = trim($from_loc_details['data'][0]['airport_code']);
				$airport_city = trim($from_loc_details['data'][0]['airport_city']);
				$from = $airport_city.' ('.$airport_code.')';
				//To
				$airport_code = trim($to_loc_details['data'][0]['airport_code']);
				$airport_city = trim($to_loc_details['data'][0]['airport_city']);
				$to = $airport_city.' ('.$airport_code.')';

				//Forming Search Request
				$search_params = array();
				$search_params['trip_type'] = 'oneway';
				$search_params['from'] = $from;
				$search_params['to'] = $to;
				$search_params['depature'] = $depature;
				$search_params['adult'] = 1;
				$search_params['child'] = 0;
				$search_params['infant'] = 0;
				$search_params['search_flight'] = 'search';
				$search_params['v_class'] = 'All';
				$search_params['carrier'] = array('');
				redirect(base_url().'index.php/general/pre_flight_search/?'.http_build_query($search_params));
			} else {
				$this->template->view ( 'general/popup_redirect');
			}
		} else {
			$this->template->view ( 'general/popup_redirect');
		}
	}

	function marup_update(){
		
	}
	/**
	 * Search Result
	 * @param number $search_id
	 */
	function search($search_id)
	{ 
		
		//$queryParams = $this->input->get();
		//echo"<pre>";print_r($queryParams);
		// if(isset($queryParams) && !empty($queryParams)){

		// }
		
		//redirect('home/index');

		$user_type = $this->session->userdata('user_type');

		$safe_search_data = $this->flight_model->get_safe_search_data ( $search_id );
		//echo"<pre>";print_r($safe_search_data);
		$active_booking_source = $this->flight_model->active_booking_source($user_type);
		//echo"<pre>";print_r($active_booking_source);die;
		/*echo "<pre>";print_r($active_booking_source);
		echo "<pre>";print_r(count($active_booking_source));
		echo "<pre>";print_r($safe_search_data);
		exit();*/
		if ((is_array ( $active_booking_source ) && count($active_booking_source) > 0) && $safe_search_data ['status'] == true) {
			//print_r($search_id."sd");exit();
			$safe_search_data ['data'] ['search_id'] = abs ( $search_id );
			$page_params = array (
					'flight_search_params' => $safe_search_data ['data'],
					'active_booking_source' => $active_booking_source 
			);
			
			$page_params ['from_currency'] = BASE_CURRENCY;
			$page_params ['to_currency'] = BASE_CURRENCY; 
			//Need to check if its domestic travel
			$from_loc = $safe_search_data['data']['from_loc']; 
			$to_loc = $safe_search_data['data']['to_loc'];
			$page_params['is_domestic_one_way_flight'] = false;
			if ($safe_search_data['data']['trip_type'] == 'oneway') {
				$page_params['is_domestic_one_way_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);
			}
### Added
			/*if (isset($page_params['is_domestic_round_way_flight']) && $page_params['is_domestic_round_way_flight'] == 1) {
				$page_params['col_2x_result'] = true;
			}*/

		//debug($page_params);exit('gdf');
			if (isset($page_params['flight_search_params']['trip_type']) && $page_params['flight_search_params']['trip_type'] == 'circle' && $page_params['flight_search_params']['is_domestic'] == 1) {
				$page_params['col_2x_result'] = true; 
			}
			else
			{
				$page_params['col_2x_result'] = false;  	
			}
			//debug($page_params['col_2x_result']);exit('gdf');
			$page_params['airline_list'] = $this->db_cache_api->get_airline_code_list();//Jaganath

			$this->load->view ( 'flight/search_result_page', $page_params );
		} else {
			//print_r($search_id);exit();
			if ($safe_search_data['status'] == true) {
				$this->load->view( 'general/popup_redirect');
			} else {
				$this->load->view( 'flight/exception');
			}
		}
	}

	private function module_type() {
		$user_type=$this->session->userdata('user_type');
		if($user_type== Agent)
		{
			$module = 'b2b';
		}
		elseif ($user_type== Sub_Agent) {
			$module = 'b2b2b';
		}
		else
		{
			$module = 'b2c';
		}
		
		return $module;
	}

	/**
	 * Arjun J Gowda
	 * Passenger Details page for final bookings
	 * Here we need to run farequote/booking based on api
	 * View Page for booking
	 */
	function booking($search_id) {
		/*
		 * FIXME
		 * The length of Passenger first name and last name combine cannot more
		 * than 35 characters
		 */
		$pre_booking_params = $this->input->post ();
		$domain_id=$this->session->userdata('domain_id'); 
		if($pre_booking_params['booking_source']==CRS_FLIGHT_BOOKING_SOURCE){
			$safe_search_data = $this->flight_model->get_safe_search_data($search_id);
			if(isset ( $pre_booking_params ['booking_source'] ) == true && $pre_booking_params ['booking_source']  == CRS_FLIGHT_BOOKING_SOURCE && $safe_search_data ['status'] == true)
			{
			$search_key=$pre_booking_params['search_access_key'][0];
			$from_loc = $safe_search_data['data']['from_loc'];
			$to_loc = $safe_search_data['data']['to_loc'];
			$depature=$safe_search_data['data']['depature'];
			$trip_type = $safe_search_data['data']['trip_type'];
			$safe_search_data['data']['is_domestic_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);

			$page_data ['active_payment_options'] = $this->module_model->get_active_payment_module_list ();
			$page_data ['search_data'] = $safe_search_data ['data'];
			// We will load different page for different API providers... As we have dependency on API for Flight details
			$page_data ['search_data'] = $safe_search_data ['data'];
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			//Not to show cache data in browser
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			$currency_obj = new Currency ( array (
						'module_type' => 'flight',
						'from' => get_application_currency_preference (),
						'to' => get_application_currency_preference () 
					) );
					// Load View
					$page_data ['booking_source'] = $pre_booking_params ['booking_source'];
					$page_data ['pre_booking_params'] ['default_currency'] = get_application_default_currency ();
					$page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['currency_obj'] = $currency_obj;
					//Traveller Details
					$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
					//Extracting Segment Summary and Fare Details
					$updated_flight_details = $pre_booking_params['token'];
					$is_price_Changed = false;
					if($trip_type=='oneway')
			      {
			      	if($pre_booking_params['supplier_id']==62)
			      	{
				         $flight_details=$this->flight_model->get_crs_oneflight_book_main($from_loc, $to_loc,$depature,$search_key);
			      		
			      	} 
			      	else
			      	{
			      		$flight_details=$this->flight_model->get_crs_oneflight_book($from_loc, $to_loc,$depature,$search_key);
			      	} 
				 
				  }
			     if($trip_type=='circle')
			   {
				  $return_to=$safe_search_data['data']['return'];
				  $flight_details=$this->flight_model->get_crs_circleflight_book($from_loc, $to_loc,$depature,$return_to,$search_key);
			   }
					$page_data ['pre_booking_params'] = $flight_details;
					//$page_data['pre_booking_summery'] = $flight_pre_booking_summary;
					// $TotalPrice = $flight_pre_booking_summary['FareDetails'][$this->current_module.'_PriceDetails']['TotalFare'];
					//$page_data['convenience_fees'] = $currency_obj->convenience_fees($TotalPrice, $search_id);
					$page_data['is_price_Changed'] = $is_price_Changed;

					//Get the country phone code 
					$Domain_record = $this->custom_db->single_table_records('domain_list', '*');
					$page_data['active_data'] =$Domain_record['data'][0];
					$temp_record = $this->custom_db->single_table_records('api_country_list', '*');
					$page_data['phone_code'] =$temp_record['data'];
					$page_data['search_id']=$search_id; 
					$page_data['supplier_id']=$pre_booking_params['supplier_id']; 
					//$this->load->view ( 'flight/tbo/tbo_crs_booking_page', $page_data );
					
		
			}
			else
			{
				redirect(base_url()); 
			}
		}else{

        load_flight_lib ( $pre_booking_params ['booking_source'] );
		}   
		$search_key=$pre_booking_params['search_access_key'][0];
  
        if($pre_booking_params ['booking_source']==PROVAB_FLIGHT_BOOKING_SOURCE)
        {
        	$safe_search_data = $this->flight_lib->search_data ( $search_id );
        	$token = $this->flight_lib->unserialized_token ( $pre_booking_params ['token'], $pre_booking_params ['token_key'] );
		    if ($token ['status'] == SUCCESS_STATUS) {
			          $pre_booking_params ['token'] = $token ['data'] ['token'];
		     }
        }
		else
		{
			$safe_search_data = $this->flight_model->get_safe_search_data($search_id);
		}
		
		$safe_search_data ['data'] ['search_id'] = intval ( $search_id );
		
		if (isset ( $pre_booking_params ['booking_source'] ) == true && $safe_search_data ['status'] == true) {
			//Jaganath - Check Travel is Domestic or International
			$from_loc = $safe_search_data['data']['from_loc'];
			$to_loc = $safe_search_data['data']['to_loc'];
			$safe_search_data['data']['is_domestic_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);

			$page_data ['active_payment_options'] = $this->module_model->get_active_payment_module_list ();
			$page_data ['search_data'] = $safe_search_data ['data'];
			// We will load different page for different API providers... As we have dependency on API for Flight details
			$page_data ['search_data'] = $safe_search_data ['data'];
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			//Not to show cache data in browser
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			switch ($pre_booking_params ['booking_source']) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					//$extra_services = $this->get_extra_services($pre_booking_params);//remove it, place after farequote
					// upate fare details
					$quote_update = $this->fare_quote_booking ( $pre_booking_params );
				
					if ($quote_update ['status'] == FAILURE_STATUS) {
									redirect ( base_url () . 'index.php/flight/exception?op=Remote IO error @ Session Expiry&notification=session' );
					} else {
						$pre_booking_params = $quote_update ['data'];
						//Get Extra Services
						//$extra_services = $this->get_extra_services($pre_booking_params);
					}
					$currency_obj = new Currency ( array (
						'module_type' => 'flight',
						'from' => get_application_currency_preference (),
						'to' => get_application_currency_preference () 
					) );
					// Load View
					$page_data ['booking_source'] = $pre_booking_params ['booking_source'];
					$page_data ['pre_booking_params'] ['default_currency'] = get_application_default_currency ();
					$page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['currency_obj'] = $currency_obj;
					//Traveller Details
					$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
					//Extracting Segment Summary and Fare Details
					$updated_flight_details = $pre_booking_params['token'];
					$is_price_Changed = false;
					$flight_details = array();
					foreach($updated_flight_details as $k => $v) {
						
						//TODO: Implement this using old and new price
						/*if($is_price_Changed == false && $v['IsPriceChanged'] == true) {
							$is_price_Changed = true;
						}*/
						
						$temp_flight_details = $this->flight_lib->extract_flight_segment_fare_details($v, $currency_obj, $search_id, $this->user_type);
						unset($temp_flight_details[0]['BookingType']);//Not needed in Next page
						$flight_details[$k] = $temp_flight_details[0];
					}
					//Merge the Segment Details and Fare Details For Printing Purpose
					$flight_pre_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
					//debug($flight_pre_booking_summary);die('ne');
					$pre_booking_params['token'] = $flight_details;
					$page_data ['pre_booking_params'] = $pre_booking_params;
					$page_data['pre_booking_summery'] = $flight_pre_booking_summary;
					$TotalPrice = $flight_pre_booking_summary['FareDetails'][$this->current_module.'_PriceDetails']['TotalFare'];

					if($this->session->userdata('user_type')==5)
					{

					$page_data['convenience_fees'] = $currency_obj->convenience_fees($TotalPrice, $search_id,$domain_id);  
					} 
					else
					{
						$page_data['convenience_fees']=0; 
					}
					$page_data['is_price_Changed'] = $is_price_Changed;

					//Get the country phone code 
					$Domain_record = $this->custom_db->single_table_records('domain_list', '*');
					$page_data['active_data'] =$Domain_record['data'][0];
					$temp_record = $this->custom_db->single_table_records('api_country_list', '*');
					$page_data['phone_code'] =$temp_record['data'];

					//session expiry time calculation
					//$page_data['session_expiry_details'] = $this->flight_lib->set_flight_search_session_expiry(true, $search_hash);
                   	
					$this->load->view ( 'flight/tbo/tbo_booking_page', $page_data );
					break;
					case TRAVELPORT_FLIGHT_BOOKING_SOURCE :
					error_reporting(E_ALL);
					$flights_data = array();

					if(is_array($_POST['flights_data'])){
						$flights_data = $_POST['flights_data'];
					}else{
						$flights_data[] = $_POST['flights_data'];
					}
					$page_data['convenience_fees']=$TotalTax=$api_BaseFare=0;
					$flight_details_segment = array();
					$flight_merge = array();
					$flight_details_merge = array();
					$pricing_xml_merge = array();
					$TotalPrice = '';
					$BaseFare = '';
					$flight_details = array();
					$merge_segment_details = array();
					$separate_price = array();
					$currency_obj22 = new Currency ( array (
						'module_type' => 'flight',
						'from' => get_application_default_currency (),
						'to' => get_application_currency_preference () 
					) );
					$sp=0;

					foreach($flights_data as $flights_data){
						$flights_data = json_decode(base64_decode($flights_data), true);

						$merge_segment_summary[] = $flights_data['SegmentSummary'];
						$merge_segment_details[] = $flights_data['SegmentDetails'];
						$request = $page_data['search_data'];			
						$currency_obj = new Currency ( array (
							'module_type' => 'flight',
							'from' => get_application_currency_preference (),
							'to' => get_application_currency_preference () 
						) ); 
						$AirPriceReq_Req = $this->flight_lib->air_pricing_request($flights_data, $request);  
						//echo "55 AirPriceReq_Req ";debug($AirPriceReq_Req);exit;	
						if($AirPriceReq_Req['status'] == true)
						{	
						$AirPriceReq_Req = $AirPriceReq_Req['data']['request'];
						$AirPriceReq_Res = $this->flight_lib->process_request($AirPriceReq_Req);
						//echo "77 AirPriceReq_Res ";debug($AirPriceReq_Res);exit;
						$xml_title = 'AirPriceResponse.xml';
				    	$date = date('Ymd_His');
				    	if (!file_exists('xml_logs/Flight')) {
			   				 mkdir('xml_logs/Flight', 0777, true);
						}
			      		 $requestcml = $xml_title;
		       			file_put_contents('xml_logs/Flight/'.$requestcml, $AirPriceReq_Res);
						$booking_array = array ();
						$air_Seg_xml = '';
						$air_pricing_xml = '';
						$xml = simplexml_load_string ( $AirPriceReq_Res );					
						$xml->registerXPathNamespace ( 'SOAP', 'http://schemas.xmlsoap.org/soap/envelope/' );
						$xml->registerXPathNamespace ( 'air', 'http://www.travelport.com/schema/air_v41_0' );
						$nodes = $xml->xpath ( '//SOAP:Envelope/SOAP:Body/air:AirPriceRsp/air:AirPriceResult/air:AirPricingSolution' );
						$price_solution_xml = '';
						if (isset ( $nodes [0] )) {
							$price_solution_xml = $nodes [0]->asXML ();
						}
						$nodes_ite = $xml->xpath ( '//SOAP:Envelope/SOAP:Body/air:AirPriceRsp/air:AirItinerary/air:AirSegment' );
						$segment_solution_xml = array ();
						foreach ( $nodes_ite as $k => $vsm ) {
							$key_objm = $vsm ['Key'];
							$key_objm = json_encode ( $key_objm );
							$key_arrm = json_decode ( $key_objm, TRUE );
							$sekKey = $key_arrm [0];
							$segment_solution_xml [$k] = $vsm->asXML ();
						}
						$air_price_response_arr = Converter::createArray ( $AirPriceReq_Res );
						
						$passenger = array ();
						$passengers_array = array ();
						$change_panelty = array ();
						$cncel_panelty = array ();
						$flight_details = array ();
						$flights = array ();
						if (isset ( $air_price_response_arr ['SOAP:Envelope'] ['SOAP:Body'] ['air:AirPriceRsp'] ['air:AirPriceResult'] )) 
						{
							$itenary_details = $air_price_response_arr ['SOAP:Envelope'] ['SOAP:Body'] ['air:AirPriceRsp'] ['air:AirItinerary'];
							$air_price_result_arr = $air_price_response_arr ['SOAP:Envelope'] ['SOAP:Body'] ['air:AirPriceRsp'] ['air:AirPriceResult'] ['air:AirPricingSolution'];
							$transction_id = $air_price_response_arr ['SOAP:Envelope'] ['SOAP:Body'] ['air:AirPriceRsp'] ['@attributes'] ['TransactionId'];
							// Take minimun value price array
							if (isset ( $air_price_result_arr ) && valid_array ( $air_price_result_arr )) {
								$air_price_result = force_multple_data_format ( $air_price_result_arr );

								$air_price_result = $air_price_result [0];
							}
							$air_segment_booking_arr = array ();
							$air_pricing_info_booking_arr = array ();
							$air_pricing_sol = array ();
							// AirPricingSolution
							$air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['xmlns'] = "http://www.travelport.com/schema/air_v41_0";
							$air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['Key'] = $air_price_result ['@attributes'] ['Key'];
							$air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['TotalPrice'] = $air_price_result ['@attributes'] ['TotalPrice'];
							$air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['BasePrice'] = $air_price_result ['@attributes'] ['BasePrice'];
							$air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['ApproximateTotalPrice'] = $air_price_result ['@attributes'] ['ApproximateTotalPrice'];
							$air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['ApproximateBasePrice'] = $air_price_result ['@attributes'] ['ApproximateBasePrice'];
							$air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['EquivalentBasePrice'] = @$air_price_result ['@attributes'] ['EquivalentBasePrice'];
							$air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['Taxes'] = $air_price_result ['@attributes'] ['Taxes'];
							$air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['ApproximateTaxes'] = $air_price_result ['@attributes'] ['ApproximateTaxes'];
							$air_pricing_sol ['AirPricingSolution'] ['@attributes'] ['QuoteDate'] = $air_price_result ['@attributes'] ['QuoteDate'];

							$air_seg_key = '';

							$air_seg_ref = force_multple_data_format ( $air_price_result ['air:AirSegmentRef'] );

							if (isset ( $air_seg_ref ) && valid_array ( $air_seg_ref )) {
								foreach ( $air_seg_ref as $s_key => $seg_key ) {
									$air_seg_key [] = $seg_key ['@attributes'] ['Key'];
								}
							}
							$air_pricing_info_arr = $this->flight_lib->formate_air_price_info($air_price_result,$itenary_details,$air_seg_key,$currency_obj,$search_id='');
							
							$flights = $air_pricing_info_arr['data']['flight'];
							$air_pricing_info_booking_arr = $air_pricing_info_arr['data']['air_pricing_info_booking_arr'];				
							$flight_details = $air_pricing_info_arr['data']['flight_details'];				

							$air_pricing_sol ['AirPricingSolution'] ['AirSegment'] = $air_pricing_info_booking_arr ['AirSegment'];
							$air_pricing_sol ['AirPricingSolution'] ['AirPricingInfo'] = $air_pricing_info_booking_arr ['AirPricingInfo'];
							
							// FareNote
							$air_price_result ['air:FareNote'] = force_multple_data_format ( $air_price_result ['air:FareNote'] );

							if (isset ( $air_price_result ['air:FareNote'] ) && valid_array ( $air_price_result ['air:FareNote'] )) {
								foreach ( $air_price_result ['air:FareNote'] as $af_not_k => $air_fare_note ) {
									$air_pricing_sol ['AirPricingSolution'] ['FareNote'] [$af_not_k] ['@attributes'] ['Key'] = @$air_fare_note ['@attributes'] ['Key'];
									$air_pricing_sol ['AirPricingSolution'] ['FareNote'] [$af_not_k] ['@value'] = @$air_fare_note ['@value'];
								}
							}

						// debug($air_pricing_sol);
							$xml = ArrayToXML::createXML ( 'AirPricingSolution', $air_pricing_sol );

							$air_pricing_xml .= str_replace ( '<?xml version="1.0" encoding="UTF-8"?>', '', $xml->saveXML () );
							
							$air_pricing_xml = str_replace ( '<AirPricingSolution>', '', $air_pricing_xml );
							$air_pricing_xml = str_replace ( '</AirPricingSolution>
							</AirPricingSolution>', '</AirPricingSolution>', $air_pricing_xml );
							
							$air_key_seg[] = $air_seg_key;
							//$flight_details ['air_seg_key'] = $air_seg_key;
							$air_pricing_xml = str_replace ( 'air:', '', str_replace ( 'common_v41_0:', '', $air_pricing_xml ) );
							#debug($air_pricing_xml);exit;
							}
							elseif(isset ( $air_price_response_arr ['SOAP:Envelope'] ['SOAP:Body'] ['SOAP:Fault'] ['faultcode'] )){				
								$msg =$air_price_response_arr ['SOAP:Envelope'] ['SOAP:Body'] ['SOAP:Fault'] ['faultstring'];
								redirect ( base_url () . 'index.php/flight/exception?op='.$msg.'&notification='.$msg.'' );
							}
							$flight_details ['flights'] = $flights;
							
							$flight_merge[] = $flights;
							
							$flight_details ['price_xml'] = $air_pricing_xml;

							$flight_details_merge[] = $air_pricing_xml;
							
							$flight_details_segment[]  = $segment_solution_xml;
							
							$response ['status'] = SUCCESS_STATUS;
							//$response ['data'] = $flight_details;
							$merge_flight_details[] = $flight_details;
										
							//$page_data ['pricing_xml'] = $response['data'];

							//$pricing_xml_merge[] = $response['data'];			
						}
						$markup = $flights_data['FareDetails']['b2c_PriceDetails']['Markup'];
						$fare_diff = ceil(intval($flight_details['total_price'])) - ceil(intval($flights_data['FareDetails']['b2c_PriceDetails']['TotalFare']));
						
						if($fare_diff == 0)
						{
							$is_price_Changed = false;
							$TotalPrices = $flights_data['FareDetails']['b2c_PriceDetails']['TotalFare'];
						}
						else
						{
							$is_price_Changed = true;
							$TotalPrices = $flight_details['total_price'] + @$markup;
							$flights_data['FareDetails']['b2c_PriceDetails']['BaseFare'] = $flight_details['base_price'];
							$flights_data['FareDetails']['b2c_PriceDetails']['TotalTax'] = $flight_details['taxes'];
							$flights_data['FareDetails']['b2c_PriceDetails']['TotalFare'] = $flight_details['total_price'];
							$flights_data['FareDetails']['b2c_PriceDetails']['currency'] = $flight_details['total_price_curr'];
							$flights_data['FareDetails']['api_PriceDetails']['Currency'] = $flight_details['base_price_curr'];
							$flights_data['FareDetails']['api_PriceDetails']['BaseFare'] = $flight_details['base_price'];
							$flights_data['FareDetails']['api_PriceDetails']['Tax'] = $flight_details['taxes'];
							$flights_data['FareDetails']['api_PriceDetails']['PublishedFare'] = $flight_details['total_price'];
						}
						$separate_price[$sp]['b2c_price_details']=  array('BaseFare'=>$flight_details['base_price'],'TotalTax'=>$flight_details['taxes'],'TotalFare'=>$flight_details['total_price'],'currency'=> get_application_default_currency());
						$separate_price[$sp]['api_price_details'] =  array('Currency'=>$flight_details['base_price_curr'],'BaseFare'=>$flight_details['base_price'],'Tax'=>$flight_details['taxes'],'PublishedFare'=>$flight_details['total_price']);
						if($this->user_type == 'b2c') {
							$page_data['convenience_fees'] += $currency_obj->convenience_fees($TotalPrice, $search_id);
						}

						//echo $TotalPrices.'<br />';
						$TotalPrice += $TotalPrices;
						$BaseFare += $flights_data['FareDetails']['b2c_PriceDetails']['BaseFare'];
						@$Markup += $flights_data['FareDetails']['b2c_PriceDetails']['Markup'];
						@$b2cPublishedfare += $flights_data['FareDetails']['b2c_PriceDetails']['PublishedFare'];
						@$b2cPublishedtax += $flights_data['FareDetails']['b2c_PriceDetails']['PublishedTax'];

						//$BaseFare += $flights_data['FareDetails']['b2c_PriceDetails']['BaseFare'];
						$TotalTax += $flights_data['FareDetails']['b2c_PriceDetails']['TotalTax'] + @$markup;
						$TotalFare = $TotalPrice;
						//$currency += $flights_data['FareDetails']['b2c_PriceDetails']['currency'];
						$api_currency = $flights_data['FareDetails']['api_PriceDetails']['Currency'];
						$api_BaseFare += $flights_data['FareDetails']['api_PriceDetails']['BaseFare'];
						$api_Tax = $flights_data['FareDetails']['api_PriceDetails']['Tax'];
						$PublishedFare = $flights_data['FareDetails']['api_PriceDetails']['PublishedFare'];

						$sp++;
					}

					$currency = get_application_default_currency();
					$flight_details ['air_seg_key'] = $air_key_seg;
					
					$flights_data['FareDetails']['b2c_PriceDetails']['BaseFare'] = $BaseFare;
					$flights_data['FareDetails']['b2c_PriceDetails']['TotalTax'] = $TotalTax;
					$flights_data['FareDetails']['b2c_PriceDetails']['TotalFare'] = $TotalFare;
					$flights_data['FareDetails']['b2c_PriceDetails']['Markup'] = $Markup;
					$flights_data['FareDetails']['b2c_PriceDetails']['PublishedFare'] = $b2cPublishedfare;
					$flights_data['FareDetails']['b2c_PriceDetails']['PublishedTax'] = $b2cPublishedtax;

					$flights_data['FareDetails']['b2c_PriceDetails']['currency'] = $currency;

					$flights_data['FareDetails']['api_PriceDetails']['Currency'] = $api_currency;
					$flights_data['FareDetails']['api_PriceDetails']['BaseFare'] = $api_BaseFare;
					$flights_data['FareDetails']['api_PriceDetails']['Tax'] = $api_Tax;
					$flights_data['FareDetails']['api_PriceDetails']['PublishedFare'] = $PublishedFare;
					$flights_data['segment_solution_xml'] = $flight_details_segment;
					$flights_data['SegmentSummary'] = $merge_segment_summary;
					$flights_data['SegmentDetails'] = $merge_segment_details;
					$flight_details ['flights'] = $flight_merge;
					$flight_details ['price_xml'] = $flight_details_merge;
					$response ['data'] = $merge_flight_details;	
					$response ['data'] = $flight_details;
					$page_data ['pricing_xml'] = $response ['data'];
					$page_data['pricing_xml']['merged_total_price'] = $TotalFare;
					$page_data['pricing_xml']['merged_base_price'] = $BaseFare;
					$page_data['pricing_xml']['merged_taxes'] = $TotalTax;
					/*$page_data['']*/
					// Load View
					$page_data ['booking_source'] = $pre_booking_params ['booking_source'];
					$page_data ['pre_booking_params'] ['default_currency'] = get_application_default_currency ();
					$page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['currency_obj'] = $currency_obj;
					//Traveller Details
					$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
					//Extracting Segment Summary and Fare Details
					$updated_flight_details = $pre_booking_params['token'];				
					$page_data['is_price_Changed'] = $is_price_Changed;
					$flights_data['FareDetails']['breakup_fare'] = $separate_price;
					$flights_data['FareDetails'] = $this->flight_lib->preferred_currency_fare_details_tp($flights_data['FareDetails'], $this->user_type, $currency_obj22);
					$flights_data['FareDetails']['b2c_PriceDetails']['Markup'] = $Markup;
					$flights_data['FareDetails']['b2c_PriceDetails']['PublishedFare'] = $b2cPublishedfare;
					$flights_data['FareDetails']['b2c_PriceDetails']['PublishedTax'] = $b2cPublishedtax; 
					$page_data['pre_booking_summery'] = $flights_data;
					$TotalPrice = $flights_data['FareDetails'][$this->current_module.'_PriceDetails']['TotalFare'];
					//Get the country phone code 
					$Domain_record = $this->custom_db->single_table_records('domain_list', '*');
					$page_data['active_data'] =$Domain_record['data'][0];
					$temp_record = $this->custom_db->single_table_records('api_country_list', '*');
					$page_data['phone_code'] =$temp_record['data'];		
					//echo "55 page_data ";debug($page_data);exit;					
					$this->load->view ( 'flight/tbo/tbo_booking_page', $page_data );
					break;
				case SABRE_FLIGHT_BOOKING_SOURCE : 
					error_reporting(E_ALL);
					$flights_data = array();

					if(is_array($_POST['flights_data'])){
						$flights_data = $_POST['flights_data'];
					}else{
						$flights_data[] = $_POST['flights_data'];
					}
					$page_data['convenience_fees']=$TotalTax=$api_BaseFare=0;
					$flight_details_segment = array();
					$flight_merge = array();
					$flight_details_merge = array();
					$pricing_xml_merge = array();
					$TotalPrice = '';
					$BaseFare = '';
					$flight_details = array();
					$merge_segment_details = array();
					$separate_price = array();
					$currency_obj22 = new Currency ( array (
						'module_type' => 'flight',
						'from' => get_application_default_currency (),
						'to' => get_application_currency_preference () 
					) );
					$sp=0;

					foreach($flights_data as $flights_data){
						$flights_data = json_decode(base64_decode($flights_data), true);

						// debug($flights_data);die;

						$merge_segment_summary[] = $flights_data['SegmentSummary'];
						$merge_segment_details[] = $flights_data['SegmentDetails'];
						$request = $page_data['search_data'];
						$currency_obj = new Currency ( array (
							'module_type' => 'flight',
							'from' => get_application_currency_preference (),
							'to' => get_application_currency_preference () 
						) ); 	
					}

					$page_data ['booking_source'] = $pre_booking_params ['booking_source'];
					$page_data ['pre_booking_params'] ['default_currency'] = get_application_default_currency ();
					$page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['currency_obj'] = $currency_obj;
					//Traveller Details
					$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
					//Extracting Segment Summary and Fare Details
					$updated_flight_details = $pre_booking_params['token'];				
					$page_data['is_price_Changed'] = 0;
					$flights_data['FareDetails']['breakup_fare'] = $separate_price;
					
					$flights_data['FareDetails'] = $this->flight_lib->preferred_currency_fare_details_tp($flights_data['FareDetails'], $this->current_module, $currency_obj22);
					
					$page_data['pre_booking_summery'] = $flights_data;
					
					$TotalPrice = $flights_data['FareDetails'][$this->current_module.'_PriceDetails']['TotalFare'];
					//Get the country phone code 
					$Domain_record = $this->custom_db->single_table_records('domain_list', '*');
					$page_data['active_data'] =$Domain_record['data'][0];
					$temp_record = $this->custom_db->single_table_records('api_country_list', '*');
					$page_data['phone_code'] =$temp_record['data'];		
					// echo "689 page_data ";debug($page_data);exit;					
					$this->load->view ( 'flight/tbo/tbo_booking_page', $page_data );

					break;
			}
		}  
		else if(isset ( $pre_booking_params ['booking_source'] ) == true && $pre_booking_params ['booking_source']  == CRS_FLIGHT_BOOKING_SOURCE && $safe_search_data ['status'] == true)
		{
			$from_loc = $safe_search_data['data']['from_loc'];
			$to_loc = $safe_search_data['data']['to_loc'];
			$depature=$safe_search_data['data']['depature'];
			$trip_type = $safe_search_data['data']['trip_type'];
			$safe_search_data['data']['is_domestic_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);

			$page_data ['active_payment_options'] = $this->module_model->get_active_payment_module_list ();
			$page_data ['search_data'] = $safe_search_data ['data'];
			// We will load different page for different API providers... As we have dependency on API for Flight details
			$page_data ['search_data'] = $safe_search_data ['data'];
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			//Not to show cache data in browser
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			$currency_obj = new Currency ( array (
						'module_type' => 'flight',
						'from' => get_application_currency_preference (),
						'to' => get_application_currency_preference () 
					) );
					// Load View
					$page_data ['booking_source'] = $pre_booking_params ['booking_source'];
					$page_data ['pre_booking_params'] ['default_currency'] = get_application_default_currency ();
					$page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['currency_obj'] = $currency_obj;
					//Traveller Details
					$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
					//Extracting Segment Summary and Fare Details
					$updated_flight_details = $pre_booking_params['token'];
					$is_price_Changed = false;
					if($trip_type=='oneway')
			      {
				         $flight_details=$this->flight_model->get_crs_oneflight_book($from_loc, $to_loc,$depature,$search_key);
				
				  }
			    if($trip_type=='circle')
				{
					$return_to=$safe_search_data['data']['return'];
					$flight_details=$this->flight_model->get_crs_circleflight_book($from_loc, $to_loc,$depature,$return_to,$search_key);
				}
					$page_data ['pre_booking_params'] = $flight_details;
					//$page_data['pre_booking_summery'] = $flight_pre_booking_summary;
					// $TotalPrice = $flight_pre_booking_summary['FareDetails'][$this->current_module.'_PriceDetails']['TotalFare'];
					//$page_data['convenience_fees'] = $currency_obj->convenience_fees($TotalPrice, $search_id);
					$page_data['is_price_Changed'] = $is_price_Changed;

					//Get the country phone code 
					$Domain_record = $this->custom_db->single_table_records('domain_list', '*');
					$page_data['active_data'] =$Domain_record['data'][0];
					$temp_record = $this->custom_db->single_table_records('api_country_list', '*');
					$page_data['phone_code'] =$temp_record['data'];
					//debug($page_data); 
					$this->load->view ( 'flight/tbo/tbo_crs_booking_page', $page_data );
					
		}else {
			redirect(base_url());
		}
	
	}
	
	function load_flight_lib($source)
	{
		switch ($source) {
			case PROVAB_FLIGHT_BOOKING_SOURCE :
			
				$this->load->library('flight/provab_private', '', 'flight_lib');
				break;
			default : redirect(base_url());
		}
	}

	
	/**
	 * Fare Quote Booking
	 * This will be used for TBO LCC carrier
	 */
	private function fare_quote_booking($flight_booking_details) 
	{ 
		$this->load->library('currency');
		$fare_quote_details =  $this->flight_lib->fare_quote_details ( $flight_booking_details );
		if($fare_quote_details['status'] == SUCCESS_STATUS) {
			//Converting API currency data to preferred currency
			$currency_obj = new Currency(array('module_type' => 'flight','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
			//debug($currency_obj);die();   
			$fare_quote_details = $this->flight_lib->farequote_data_in_preferred_currency($fare_quote_details, $currency_obj);
		}
		return $fare_quote_details; 
	}
	function process_booking($book_id, $temp_book_origin){

		$user_type=$this->session->userdata('user_type');  
		if($user_type == 5)
		{ 
		$page_data['book_id']= $book_id;
		$page_data['temp_book_origin']= $temp_book_origin;
		$page_data['productinfo']= META_AIRLINE_COURSE; 
		$this->load->view('payment/PAYEESY/payment_gateway_card',$page_data); 			
		}
		else
		{	 	
		if($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0){

			$page_data ['form_url'] = base_url () . 'index.php/flight/secure_booking';
			$page_data ['form_method'] = 'POST';
			$page_data ['form_params'] ['book_id'] = $book_id;
			$page_data ['form_params'] ['temp_book_origin'] = $temp_book_origin;

			$this->load->view('share/loader/booking_process_loader', $page_data);	

		}else{
			redirect(base_url().'index.php/flight/exception?op=Invalid request&notification=validation');
		}
		}  
		
	}
	/**
	 * Arjun J Gowda
	 * Secure Booking of FLIGHT
	 * Process booking no view page
	 */
	function pre_booking($search_id) 
	{
		$site_name = str_replace('/','',$_SERVER['HTTP_HOST']);
      	$site_name = str_ireplace('www.','',$_SERVER['HTTP_HOST']); 
      	$site_details  =  $this->General_Model->get_site_details($site_name);
		error_reporting(0);
		$post_params = $this->input->post();
		if (valid_array ( $post_params ) == false) {
			redirect ( base_url () );
		}
		//Setting Static Data - Jaganath
		$post_params['billing_city'] = 'Bangalore';
		$post_params['billing_zipcode'] = '560100';
		$post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue, Konnappana Agrahara, Electronic city';
		// Make sure token and temp token matches
		$valid_temp_token = unserialized_data ( $post_params ['token'] );
		if ($valid_temp_token != false) {
			if($post_params['booking_source'] == TRAVELPORT_FLIGHT_BOOKING_SOURCE){
					$arrFltData = json_decode(base64_decode($post_params['flights_data']),true);
					$arrTokenData['flights_data'] 				=  $post_params['flights_data'];
					//debug($arrFltData);exit;
					$arrTokenData['token'][0]					=  $arrFltData;
					$arrTokenData['token_key'][0]				=  $arrFltData['Token'];
					$arrTokenData['search_access_key'][0] 		=  "";
					$arrTokenData['is_lcc'][0] 					=  0;
					$arrTokenData['promotional_plan_type'][0]	=  "";
					$arrTokenData['booking_type'] 				=  "";
					$arrTokenData['booking_source'] 			=  $post_params['booking_source'];
					$arrTokenData['search_hash'] 				=  "";
					//debug($arrTokenData);exit;
					$post_params['token']				= $arrTokenData;
				}

			if($post_params['booking_source'] == SABRE_FLIGHT_BOOKING_SOURCE ){
				// debug($post_params);die;
				$arrFltData = json_decode(base64_decode($post_params['flights_data']),true);
				$arrTokenData['flights_data'] 				=  $post_params['flights_data'];
				$arrTokenData['token'][0]					=  $arrFltData;
				$arrTokenData['token_key'][0]				=  $arrFltData['Token'];
				$arrTokenData['search_access_key'][0] 		=  "";
				$arrTokenData['is_lcc'][0] 					=  0;
				$arrTokenData['promotional_plan_type'][0]	=  "";
				$arrTokenData['booking_type'] 				=  "";
				$arrTokenData['booking_source'] 			=  $post_params['booking_source'];
				$arrTokenData['search_hash'] 				=  "";
				// debug($arrTokenData);exit;
				$post_params1['token']						= $arrTokenData;
				// debug($post_params);die;

			}

			if($post_params ['booking_source'] == CRS_FLIGHT_BOOKING_SOURCE)
			{
				$temp_booking = $this->module_model->serialize_temp_booking_record ($post_params, FLIGHT_BOOKING );
			    $book_id = $temp_booking ['book_id'];
			    $book_origin = $temp_booking ['temp_booking_origin'];
			    $booking_data = $this->module_model->unserialize_temp_booking_record ( $book_id, $book_origin );
				$book_params = $booking_data['book_attributes'];
				$clean_search_details = $this->flight_model->get_safe_search_data($search_id);
				$page_data ['data'] = $book_params;
				$page_data ['search_data'] = $clean_search_details;
                
			   $mail_template = $this->template->isolated_view ( 'flight/booking_crs_request_email', $page_data ); 
				$email_id='priyankaz.provab@gmail.com';
				$this->load->library('provab_mailer');
			    $this->provab_mailer->send_mail($email_id, 'Request for new Booking', $mail_template);
			    redirect ( base_url () . 'index.php/' );
				print_r($c); exit;
			}
			if($post_params ['booking_source'] == TRAVELPORT_FLIGHT_BOOKING_SOURCE){
					$currency_obj = new Currency ( array (
							'module_type' => 'flight',
							'from' => admin_base_currency (),
							'to' => admin_base_currency () 
					) );
					 /*debug($post_params ['token']);
					 exit;*/
					 $temp_token = $post_params ['token'];
					//$temp_token = unserialized_data ( $post_params ['token'] );
					/*debug($temp_token);
					exit;*/
					$fare_details = $temp_token['token'][0]['FareDetails'][$this->current_module.'_PriceDetails'];

					$amount = $fare_details['TotalFare'];
					$currency = $fare_details['CurrencySymbol'];				
					//debug($temp_token);
					//echo "hhh";

					$flighs_data = json_decode(base64_decode($post_params['flights_data']), true);

					/********* Admin_Markup Start ********/   // By bhola
					/*$flight_admin_markup = $this->flight_model->travelport_markup(
						$flighs_data['FareDetails']['b2c_PriceDetails']['Admin_Markup']['markup_type'], 
						$flighs_data['FareDetails']['b2c_PriceDetails']['Admin_Markup']['markup_val'],
						 $amount, 
						 $flighs_data['FareDetails']['b2c_PriceDetails']['Admin_Markup']['total_pax']); */ 
					/********* Admin_Markup Start ********/	
					/*debug($flight_admin_markup);
					exit;*/
				}

			if($post_params ['booking_source'] ==PROVAB_FLIGHT_BOOKING_MYSTIFLY_SOURCE)
			{
				$temp_booking = $this->module_model->serialize_temp_booking_record ($post_params, FLIGHT_BOOKING );
			    $book_id = $temp_booking ['book_id'];
			    $book_origin = $temp_booking ['temp_booking_origin'];
			    $booking_data = $this->module_model->unserialize_temp_booking_record ( $book_id, $book_origin );
				$book_params = $booking_data['book_attributes'];
				//debug($booking_data);
				$clean_search_details = $this->flight_model->get_safe_search_data($search_id);
				$page_data ['data'] = $book_params;
				$page_data ['search_data'] = $clean_search_details;
                //debug($page_data);
			   echo $mail_template = $this->loadd->view ( 'flight/booking_iinternational_request_email', $page_data ); 
				$email_id='admin@gmail.com';
				$this->load->library('provab_mailer');
			    $this->provab_mailer->send_mail($email_id, 'Request for new Booking', $mail_template); exit;
			    redirect ( base_url () . 'index.php/' );
				print_r($c); exit;
			}
			load_flight_lib ( $post_params['booking_source'] );
			$amount = 0;
			$currency = '';
			/****Convert Display currency to Application default currency***/
			//After converting to default currency, storing in temp_booking
			if($post_params ['booking_source'] == TRAVELPORT_FLIGHT_BOOKING_SOURCE){
				$post_params['token'] = $post_params['token'];
			}else{
				$post_params['token'] = unserialized_data($post_params['token']);
			}
			$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => get_application_currency_preference (),
					'to' => get_application_default_currency () 
			));

			if($post_params ['booking_source'] == SABRE_FLIGHT_BOOKING_SOURCE){
				$post_params['token']['token'] = $this->flight_lib->convert_token_to_application_currency($post_params1['token']['token'], $currency_obj, $this->user_type);
			}else{
				$post_params['token']['token'] = $this->flight_lib->convert_token_to_application_currency($post_params['token']['token'], $currency_obj, $this->user_type);
			}
			$post_params['token'] = serialized_data($post_params['token']);
			$temp_booking = $this->module_model->serialize_temp_booking_record ($post_params, FLIGHT_BOOKING );

			$this->load->model('General_Model');
			$balance = $this->General_Model->getAgentBalance()->row();
			if(isset($balance->balance_credit) && $balance->balance_credit < $post_params['total_amount_val']) 
			{
				$this->session->set_flashdata('message', 'Your balance is low.');
                redirect(base_url().'dashboard/index');				
            } else {
				$book_id = $temp_booking ['book_id'];
				$book_origin = $temp_booking ['temp_booking_origin'];
				if ($post_params ['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {
					$currency_obj = new Currency ( array (
							'module_type' => 'flight',
							'from' => admin_base_currency (),
							'to' => admin_base_currency () 
					) );
					$temp_token = unserialized_data ( $post_params ['token'] );
					$flight_details = $temp_token['token'];
					$flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
					// debug($flight_booking_summary);
					// exit();
					// $fare_details = $flight_booking_summary['FareDetails'][$this->current_module.'_PriceDetails'];
					$fare_details = $flight_booking_summary['FareDetails']['api_PriceDetails'];
					$amount = $fare_details['PublishedFare'];
					$currency = $fare_details['CurrencySymbol'];
				}
				/********* Convinence Fees Start ********/
				$convenience_fees = ceil($currency_obj->convenience_fees($amount, $search_id));
				/********* Convinence Fees End ********/
			 	
				/********* Promocode Start ********/
				$promocode_discount = $post_params['promo_code_discount_val'];
				//details for PGI
				$email = $post_params ['billing_email'];
				$phone = $post_params ['passenger_contact'];
				$verification_amount = ($amount+$convenience_fees-$promocode_discount);
				$firstname = $post_params ['first_name'] ['0'] . " " . $post_params ['last_name'] ['0'];   
				$book_id = $book_id;
				$productinfo = META_AIRLINE_COURSE;
				// check current balance before proceeding further
				$domain_balance_status = $this->domain_management_model->verify_current_balance ( $verification_amount, $currency );
				// debug($domain_balance_status);die;
				if ($domain_balance_status == true) {
					//Save the Booking Data
					$booking_data = $this->module_model->unserialize_temp_booking_record ( $book_id, $book_origin );
					$book_params = $booking_data['book_attributes'];
							$data = $this->flight_lib->save_booking($book_id, $book_params,$currency_obj, $this->user_type);  
							//$post_params ['type_of_payment'] = PAY_NOW;

						switch ($post_params ['payment_method']) {
						case PAY_AT_BANK :
							//echo 'Under Construction - Remote IO Error';
							$this->offline_request($book_id,$book_origin);
							exit ();
							break; 

						    case PAY_NOW :
							$this->load->model('transaction');
							$pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
							$this->transaction->create_payment_record($book_id, $verification_amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount, $pg_currency_conversion_rate);
							$user_type=$this->session->userdata('user_type'); 

							// if($user_type == 5 && $site_details->payment_gateway_status == 0) 
							// {
								// $this->offline_request($book_id,$book_origin);
								// exit ();
							/*if($this->session->userdata('user_logged_in') == 0 || $this->session->userdata('user_type') ==B2B2C_USER){*/
								// debug($this->session->userdata); 
							if($this->session->userdata('user_logged_in') == 1){
						
								 redirect(base_url() . 'index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin.'/'.$post_params['card'].'/'.$post_params['month'].'/'.$post_params['year']);
								 echo 'step1'; exit();
							}
							else 
							{ 
								echo ' Unauthorized access.';
								exit();
								 redirect(base_url().'index.php/flight/process_booking/'.$book_id.'/'.$book_origin); 
							}
							break;
					}
				} else {
					redirect ( base_url () . 'index.php/flight/exception?op=Amount Flight Booking&notification=insufficient_balance' );
				}
			}
		}
		redirect ( base_url () . 'index.php/flight/exception?op=Remote IO error @ FLIGHT Booking&notification=validation' );
	}

	/*
	* PAWAN BAGGA
	* Deduct Balance From Agent & Sub- Agent  added date 11/01/2018
	*/
	function agent_balance_mangment($book_params,$temp_booking)
	{
		$agent_id= $this->session->userdata('user_details_id');  
		$agent_balance=$this->transaction->get_agent_balance($agent_id);
		if($this->user_type == 'b2c') {
			$fair_detail=$book_params['fare']+$book_params['admin_markup']+$book_params['convinence']-$book_params['agent_markup'];
		} else {
			$fair_detail=$book_params['fare']+$book_params['admin_markup']-$book_params['agent_markup']-$book_params['agent_commission']+$book_params['agent_tds'];
		}
		$agent_account_balance=$agent_balance['balance_credit'];
		$total_fair=$fair_detail; 
		$update_agent_balance=$agent_account_balance - $total_fair;

		$transaction_logs =
		array(
		'user_id' => $agent_id,
		'last_balance'=> $agent_balance['balance_credit'],
		'current_balance'=> $update_agent_balance,
		'amount_debited'=> $total_fair,
		'app_ref'=> $temp_booking['book_id'], 
		'module'=> "FLIGHT",
		'log_type'=> "BOOKING",
		);
		$this->transaction->update_transaction_logs($transaction_logs);  
		$this->transaction->update_agent_balance($update_agent_balance,$agent_id);  
	}
	function agent_balance_mangment_crs($amount,$temp_booking)
	{
		//debug($book_params);  die(" FOR AGENT BALANCE"); 
		$agent_id= $this->session->userdata('user_details_id');  
		$agent_balance=$this->transaction->get_agent_balance($agent_id);
		$total_fair=$amount; 
		$agent_account_balance=$agent_balance['balance_credit'];
		if($agent_account_balance > $total_fair)
		{
		$update_agent_balance=$agent_account_balance - $total_fair;
		$transaction_logs =
		array(
		'user_id' => $agent_id,
		'last_balance'=> $agent_balance['balance_credit'],
		'current_balance'=> $update_agent_balance,
		'amount_debited'=> $total_fair,
		'app_ref'=> $temp_booking['book_id'], 
		'module'=> "FLIGHT",
		'log_type'=> "CRS BOOKING",
		); 
		$this->transaction->update_transaction_logs($transaction_logs);  
		$this->transaction->update_agent_balance($update_agent_balance,$agent_id);  
		$status = array(
		'payment_status'     => 1,
		'payment_status_msg' => "Payment Successfull Done", );
		return $status; 
		}
		else 
		{
			$status = array(
				'payment_status'     => 0,
				'payment_status_msg' => "Balance Low", );
		 return $status; 	
		} 
	}
	function send_mail_agent($temp_booking)
	{	

		$app_reference=$temp_booking['book_id']; 
		$booking_source=$temp_booking['booking_source'];
		$booking_status="BOOKING_CONFIRMED";   
		$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				$operation='email_voucher';
				$email=$this->session->userdata('user_email');   
				switch ($operation) {
				   case 'email_voucher':
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('voucher/flight_pdf', $page_data,TRUE);
						$pdf = $create_pdf->create_pdf($mail_template,''); 
						$this->provab_mailer->send_mail($email, domain_name().' New Booking heppend Flight Ticket',$mail_template ,$pdf);

						break;   
				}  
		
	}
	function send_mail_agent_crs($temp_booking)
	{	
		$app_reference=$temp_booking['book_id']; 
		$booking_source="PTBSID0000000005";
		$booking_status="BOOKING_CONFIRMED";   
		$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				$operation='email_voucher';
				$email=$this->session->userdata('user_email');   
				$email="pawan.provabmail@gmail.com";
				switch ($operation) {
				   case 'email_voucher':
						$this->load->library('provab_pdf');
						$create_pdf = new Provab_Pdf();
						$mail_template = $this->load->view('voucher/flight_pdf', $page_data,TRUE);
						$pdf = $create_pdf->create_pdf($mail_template,''); 
						$this->provab_mailer->send_mail($email, domain_name().' New Booking heppend Flight Ticket',$mail_template ,$pdf);

						break;   
				}  
		
	}
   
	function offline_request($book_id, $temp_book_origin){ 
		if($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0){
			$page_data ['form_params'] ['book_id'] = $book_id;
			$page_data ['form_params'] ['temp_book_origin'] = $temp_book_origin;
			$this->session->set_userdata("uniqdata",$page_data);
			redirect('index.php/voucher/flight_pay_later');
			//$this->template->view('share/loader/booking_process_loader', $page_data);	
			
		}else{
			exit("exit here");
			redirect(base_url().'index.php/flight/exception?op=Invalid request&notification=validation');
		}
		
	}

	function payment_failed($payment_mode, $search_id){
		if($payment_mode == 'deposite'){
		   	$data['title'] = "Transaction Failure";
			$data['caption'] = "Payment Failed!";
			$data['message'] = "Transaction process could not be successful. due to insufficient balance.";
			$data['image_url'] = base_url()."assets/images/faild_payment.png";
		}
		$this->load->view('flight/error_info',$data);
	}
	/**
	 * Arjun J Gowda
	 * Do booking once payment is successfull - Payment Gateway
	 * and issue voucher
	 */
	function secure_booking() 
	{
		$post_data = $this->input->post(); 

		if(valid_array($post_data) == true && isset($post_data['book_id']) == true && isset($post_data['temp_book_origin']) == true &&
			empty($post_data['book_id']) == false && intval($post_data['temp_book_origin']) > 0){
			//verify payment status and continue
			$book_id = trim($post_data['book_id']);
			$temp_book_origin = intval($post_data['temp_book_origin']);
		} else{
			redirect(base_url().'index.php/flight/exception?op=InvalidBooking&notification=invalid');
		}    
		$this->load->library('currency');
		//run booking request and do booking
		$temp_booking = $this->module_model->unserialize_temp_booking_record ( $book_id, $temp_book_origin );
		// FIXME Delete record from database
		// echo SABRE_FLIGHT_BOOKING_SOURCE;
		// debug($temp_booking);die;
		load_flight_lib ( $temp_booking ['booking_source'] );

		if ($temp_booking ['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {
			$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => admin_base_currency (),
					'to' => admin_base_currency () 
			) );
			$flight_details = $temp_booking ['book_attributes'] ['token'] ['token'];
			$book_params = $temp_booking ['book_attributes'];  
			$flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
			$fare_details = $flight_booking_summary['FareDetails'][$this->current_module.'_PriceDetails'];
			$total_booking_price = $fare_details['TotalFare'];
			$currency = $fare_details['Currency'];
		}
 
			// echo "string";die;
		if ($temp_booking['booking_source'] == SABRE_FLIGHT_BOOKING_SOURCE) {

			$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => admin_base_currency (),
					'to' => admin_base_currency () 
			) );
			$flight_details = $temp_booking ['book_attributes'] ['token'] ['token'];
			$book_params = $temp_booking ['book_attributes'];  
			$flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);

			$fare_details = $flight_booking_summary['FareDetails'][$this->current_module.'_PriceDetails'];
			// debug($fare_details);die;
			$total_booking_price = $fare_details['TotalFare'];
			$currency = $fare_details['Currency'];

		}

		if ($temp_booking ['booking_source']==TRAVELPORT_FLIGHT_BOOKING_SOURCE) {
			//echo base_url () . 'index.php/flight/secure_booking_tp/'.$book_id.'/'.$temp_book_origin;
			redirect(base_url () . 'index.php/flight/secure_booking_tp/'.$book_id.'/'.$temp_book_origin);
		}

		// verify payment status and continue
		// Flight_Model::lock_tables();
		$domain_balance_status = $this->domain_management_model->verify_current_balance ( $total_booking_price, $currency );

		if ($domain_balance_status) {
			if ($temp_booking != false) {
				switch ($temp_booking ['booking_source']) {
					case PROVAB_FLIGHT_BOOKING_SOURCE :
					 try {
							$booking = $this->flight_lib->process_booking($book_id, $temp_booking ['book_attributes'] );
						}catch (Exception $e) {
							$booking ['status'] = BOOKING_ERROR;
						}
						// Save booking based on booking status and book id
					 break;
					case SABRE_FLIGHT_BOOKING_SOURCE :
					 try {
							$SessionCreateRQ_RS = $this->flight_lib->SessionCreateRQ($book_id,'BOOK');
							$SessionCreateRS 	= $this->parse_session_create_response($SessionCreateRQ_RS);

							$search_id = $book_params['search_id'];
							$clean_search_details = $this->flight_model->get_safe_search_data($search_id);
		
							$TravelItineraryAddInfoRQ_RS = $this->flight_lib->TravelItineraryAddInfoRQ($SessionCreateRS['BinarySecurityToken'],$book_id, $clean_search_details,$book_params);
							$TravelItineraryAddInfoRS = $TravelItineraryAddInfoRQ_RS['TravelItineraryAddInfoRS'];

							if (!empty($TravelItineraryAddInfoRS) && substr($TravelItineraryAddInfoRS, 0, 5) == "<?xml") {
									$response = $this->xml_to_array->XmlToArray($TravelItineraryAddInfoRS);
									$TravelItineraryAddInfoStatus = $response['soap-env:Body']['TravelItineraryAddInfoRS']['stl:ApplicationResults']['@attributes']['status'];
									if ($TravelItineraryAddInfoStatus == 'Complete') {

										$OTA_AirBookRQ_RS 	= $this->flight_lib->OTA_AirBookRQ($SessionCreateRS['BinarySecurityToken'],$book_id, $clean_search_details,$book_params);
										$OTA_AirBookRS 		= $OTA_AirBookRQ_RS['OTA_AirBookRS'];
										if (!empty($OTA_AirBookRS) && substr($OTA_AirBookRS, 0, 5) == "<?xml") {
											$response = $this->xml_to_array->XmlToArray($OTA_AirBookRS);
											$OTA_AirBookStatus = $response['soap-env:Body']['EnhancedAirBookRS']['stl:ApplicationResults']['@attributes']['status'];
											if ($OTA_AirBookStatus == 'Complete') {

												$OTA_AirPriceRQ_RS 	= $this->flight_lib->OTA_AirPriceRQ($SessionCreateRS['BinarySecurityToken'],$book_id, $clean_search_details,$book_params);
												$OTA_AirPriceRS 	= $OTA_AirPriceRQ_RS['OTA_AirPriceRS'];
												if (!empty($OTA_AirPriceRS) && substr($OTA_AirPriceRS, 0, 5) == "<?xml") {

													$response = $this->xml_to_array->XmlToArray($OTA_AirPriceRS);
													$OTA_AirPriceStatus = $response['soap-env:Body']['OTA_AirPriceRS']['stl:ApplicationResults']['@attributes']['status'];
													if ($OTA_AirPriceStatus == 'Complete') {

														$SpecialServiceRQ_RS = $this->flight_lib->SpecialServiceRQ($SessionCreateRS['BinarySecurityToken'],$book_id, $clean_search_details,$book_params);
														$SpecialServiceRS 	= $SpecialServiceRQ_RS['SpecialServiceRS'];
														if (!empty($SpecialServiceRS) && substr($SpecialServiceRS, 0, 5) == "<?xml") {

															$response = $this->xml_to_array->XmlToArray($SpecialServiceRS);
															$SpecialServiceStatus = $response['soap-env:Body']['SpecialServiceRS']['stl:ApplicationResults']['@attributes']['status'];
															// debug($SpecialServiceStatus);die;
															if ($SpecialServiceStatus == 'Complete') {

																$EndTransactionRQ_RS 	= $this->flight_lib->EndTransactionRQ($SessionCreateRS['BinarySecurityToken'],$book_id, $clean_search_details,$book_params);

																$EndTransactionRS 		= $EndTransactionRQ_RS['EndTransactionRS'];
																if (!empty($EndTransactionRS) && substr($EndTransactionRS, 0, 5) == "<?xml") {

																	$response = $this->xml_to_array->XmlToArray($EndTransactionRS);
																	$EndTransactionStatus = $response['soap-env:Body']['EndTransactionRS']['stl:ApplicationResults']['@attributes']['status'];
																	if ($EndTransactionStatus == 'Complete'){

																		$ItineraryRef = $response['soap-env:Body']['EndTransactionRS']['ItineraryRef']['@attributes']['ID']; 

																		$booking = array(
																							'pnr_no' 		=> $ItineraryRef,
																							'status' => "1",
																							'booking_params' => $book_params
																						 );
																		// debug($booking);die;
																	}
																}


															}else{
																$error_message.='|'.$booking_id.'-FLIGHT-TravelItineraryAddInfoRS ERROR RESPONSE';
															}

														}else{
															$error_message.='|'.$booking_id.'-FLIGHT-TravelItineraryAddInfoRS ERROR RESPONSE';
														}



														
													}else{
														$error_message.='|'.$booking_id.'-FLIGHT-TravelItineraryAddInfoRS ERROR RESPONSE';
													}


												}else{
													$error_message.='|'.$booking_id.'-FLIGHT-TravelItineraryAddInfoRS ERROR RESPONSE';
												}


											}else{
												$error_message.='|'.$booking_id.'-FLIGHT-TravelItineraryAddInfoRS ERROR RESPONSE';
											}
										}else{
											$error_message.='|'.$booking_id.'-FLIGHT-TravelItineraryAddInfoRS ERROR RESPONSE';
										}

									} else {
										$error_message.='|'.$booking_id.'-FLIGHT-TravelItineraryAddInfoRS ERROR RESPONSE';
									}
							} else {
									$error_message.='|'.$booking_id.'-FLIGHT-TravelItineraryAddInfoRS EMPTY RESPONSE';							
								}

						} catch (Exception $e) {
							$booking ['status'] = BOOKING_ERROR;
						}
						// Save booking based on booking status and book id
						break;
				}
				
				//echo '1223';debug($booking);die(" FINAL BOOKING");     
				if (in_array($booking ['status'], array(SUCCESS_STATUS, BOOKING_CONFIRMED, BOOKING_PENDING, BOOKING_FAILED, BOOKING_ERROR,BOOKING_HOLD)) == true) {
					$currency_obj = new Currency ( array (
							'module_type' => 'flight',  
							'from' => admin_base_currency (),
							'to' => admin_base_currency () 
					) );
					$booking ['data'] ['booking_params'] ['currency_obj'] = $currency_obj;
					//Update the booking Details
					$icket_details = @$booking ['data'] ['ticket'];
					$icket_details['master_booking_status'] = $booking ['status'];
					if($temp_booking ['booking_source'] == SABRE_FLIGHT_BOOKING_SOURCE){

						$data = $this->flight_lib->update_booking_details( $book_id, $booking['booking_params'], $booking, $this->user_type,$currency_obj);
					}else{
						$data = $this->flight_lib->update_booking_details( $book_id, $booking ['data'] ['booking_params'], $icket_details, @$booking ['data'] ['book'], $this->user_type);
					}

					if($data['status'] == "BOOKING_CONFIRMED")
					{
						$this->agent_balance_mangment($data,$temp_booking);
						//$this->send_mail_agent($temp_booking);   	
					}
					$this->domain_management_model->update_transaction_details ( 'flight', $book_id, $data ['fare'], $data ['admin_markup'], $data ['agent_markup'], $data['convinence'], $data['discount'],$data['transaction_currency'], $data['currency_conversion_rate'] );
                  
                    if (in_array ( $data ['status'], array (
							'BOOKING_CONFIRMED',
							'BOOKING_PENDING',
							'BOOKING_HOLD' 
							) )) {
                    	//debug('sd');die;
								// Sms config & Checkpoint
								/* if (active_sms_checkpoint ( 'booking' )) {
								$msg = "Dear " . $data ['name'] . " Thank you for Booking your ticket with us.Ticket Details will be sent to your email id";
								$msg = urlencode ( $msg );
								$sms_status = $this->provab_sms->send_msg ( $data ['phone'], $msg );
								// return $sms_status;
								} */

								redirect ( base_url () . 'index.php/voucher/flight/' . $book_id . '/' . $temp_booking ['booking_source'] . '/' . $data ['status'] . '/show_voucher' );
							} else {
								die(" CASE 111111");  
								
								redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $booking ['msg'] );
							}
				} else {
					redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $booking ['msg'] );
				}
			}
			// release table lock
			Flight_Model::release_locked_tables ();
		} else {
			// release table lock
			Flight_Model::release_locked_tables ();
			exit ();
		}
		
		
	}

	function pre_cancellation($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);
				$page_data['data'] = $assembled_booking_details['data'];
				$this->load->view('flight/pre_cancellation', $page_data) ;
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	/**
	 * Jaganath
	 * @param $app_reference
	 */
	function cancel_booking()
	{
		$post_data = $this->input->post();
		if (isset($post_data['app_reference']) == true && isset($post_data['booking_source']) == true && isset($post_data['transaction_origin']) == true &&
			valid_array($post_data['transaction_origin']) == true && isset($post_data['passenger_origin']) == true && valid_array($post_data['passenger_origin']) == true) {
			$app_reference = trim($post_data['app_reference']);
			$booking_source = trim($post_data['booking_source']);
			$transaction_origin = $post_data['transaction_origin'];
			$passenger_origin = $post_data['passenger_origin']; 	
			$booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib($booking_source);
				//Formatting the Data
				$this->load->library('booking_data_formatter');
				$booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);
				$booking_details = $booking_details['data'];
				//Grouping the Passenger Ticket Ids
				$grouped_passenger_ticket_details = $this->flight_lib->group_cancellation_passenger_ticket_id($booking_details, $passenger_origin); 
				if($booking_source == SABRE_FLIGHT_BOOKING_SOURCE)
				{
					$cancellation_details['status'] = $grouped_passenger_ticket_details; 
				}
				$passenger_origin = $grouped_passenger_ticket_details['passenger_origin'];
				$passenger_ticket_id = $grouped_passenger_ticket_details['passenger_ticket_id'];
				if($booking_source == PROVAB_FLIGHT_BOOKING_SOURCE)
				{
					
				$cancellation_details = $this->flight_lib->cancel_booking($booking_details, $passenger_origin, $passenger_ticket_id); 
				}
				redirect('flight/cancellation_details/'.$app_reference.'/'.$booking_source.'/'.$cancellation_details['status']);    
			} else {  
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	function cancellation_details($app_reference, $booking_source,$cancellation_status)
	{  
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$page_data = array();
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_flight_booking_data($master_booking_details, $this->current_module);
				$page_data['data'] = $master_booking_details['data'];
				$page_data['cancellation_status'] = $cancellation_status; 
				$this->load->view('flight/cancellation_details', $page_data);
			} else {   
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}

	}
	/**
	 * Import Temp Booking data
	 */
	function import_temp_booking_data($book_id='FB03-125019-840625', $temp_book_origin=140)
	{
		$temp_booking = $this->module_model->unserialize_temp_booking_record ( $book_id, $temp_book_origin );
		if (valid_array($temp_booking) == true) {
			//debug($temp_booking);
			//echo 'transaction log tabel wrt app_reference';
			load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
			$currency_obj = new Currency ( array (
						'module_type' => 'flight',
						'from' => get_application_default_currency (),
						'to' => get_application_default_currency () 
				) );
			$book_params = $temp_booking['book_attributes'];
			$book_params['currency_obj'] = $currency_obj;
															
			$data = $this->flight_lib->save_booking($book_id, $book_params);
			debug($data);
			$this->domain_management_model->update_transaction_details ( 'flight', $book_id, $data ['fare'], $data ['admin_markup'], $data ['agent_markup'], $data['convinence'], $data['discount'] );
			echo 'data saved';exit;
		} else {
			echo 'No data';exit;
		}
	}
	/**
	 * Arjun J Gowda
	 */
	function exception() {
		$module = META_AIRLINE_COURSE;
		$op = @$_GET ['op'];
		$notification = @$_GET ['notification'];
		$eid = $this->module_model->log_exception ( $module, $op, $notification );
		// set ip log session before redirection
		$this->session->set_flashdata ( array (
				'log_ip_info' => true 
		) );
		redirect ( base_url () . 'index.php/flight/event_logger/' . $eid );
	}
	function event_logger($eid = '') {
		$log_ip_info = $this->session->flashdata ( 'log_ip_info' );
		$this->load->view ( 'flight/exception', array (
				'log_ip_info' => $log_ip_info,
				'eid' => $eid 
		) );
	}
	function test_server()
	{
		$data = $this->custom_db->single_table_records('test', '*', array('origin' => 851));
		$response = json_decode($data['data'][0]['test'], true);
	}


	// crs functions 
	function search_crs($search_id)
	{
		$user_type=$this->session->userdata('user_type');

		$safe_search_data = $this->flight_model->get_safe_search_data ( $search_id );
		
		// Get all the FLIGHT bookings source which are active
		$active_booking_source = $this->flight_model->active_booking_source ($user_type);  
		/*debug($safe_search_data);
		exit("dont know");
		*/
		if (valid_array ( $active_booking_source ) == true and $safe_search_data ['status'] == true) {
				
		
			$safe_search_data ['data'] ['search_id'] = abs ( $search_id );
			$page_params = array (
					'flight_search_params' => $safe_search_data ['data'],
					'active_booking_source' => $active_booking_source 
			);
			$page_params ['from_currency'] = get_application_default_currency ();
			$page_params ['to_currency'] = get_application_currency_preference ();

			//Need to check if its domestic travel
			$from_loc = $safe_search_data['data']['from_loc'];
			$to_loc = $safe_search_data['data']['to_loc'];
			$trip_type = $safe_search_data['data']['trip_type']; 
			$depature = $safe_search_data['data']['depature'];
			//$depature="2017-07-28";
			
			if($trip_type=='oneway')
			{
					
				$crs_data['crs_data']=$this->flight_model->get_crs_oneflight($from_loc, $to_loc,$depature);		
				/*debug($crs_data);
				exit("here");*/
				
			}
			if($trip_type=='circle')
			{
				 $return_to = $safe_search_data['data']['return'];
				 $crs_data['crs_data']=$this->flight_model->get_crs_circleflight($from_loc, $to_loc,$depature,$return_to);
					
					/*$adult_per_price_tax= $crs_data['crs_data'][0]['adult_price']+$crs_data['crs_data'][0]['adult_tax'];
					$child_price_tax= $crs_data['crs_data'][0]['child_price']+$crs_data['crs_data'][0]['child_tax'];
					$infant_price_tax= $crs_data['crs_data'][0]['infant_price']+$crs_data['crs_data'][0]['infant_tax'];
					$adult = $safe_search_data['data']['adult_config'];
					$child_config = $safe_search_data['data']['child_config'];
					$infant_config = $safe_search_data['data']['infant_config'];
				*/
			}
			

			$page_params['crs_flight'] = $crs_data['crs_data'];
			
			$page_params['is_domestic_one_way_flight'] = false;
			if ($safe_search_data['data']['trip_type'] == 'oneway') {
				$page_params['is_domestic_one_way_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);
			}
			$page_params['airline_list'] = $this->db_cache_api->get_airline_code_list();//Jaganath
			$this->load->view ( 'flight/search_crs_result_page', $page_params );
		} else {
			if ($safe_search_data['status'] == true) {
				$this->load->view ( 'general/popup_redirect'); 
			} else {
				$this->load->view ( 'flight/exception');
			}
		}
	}

	function pre_crs_flight_search($search_id='')
	{
		$search_params = $this->input->get();
		
		//Global search Data
		$search_id = $this->save_pre_search(META_AIRLINE_COURSE);
		$this->save_search_cookie(META_AIRLINE_COURSE, $search_id);

		//Analytics
		$this->load->model('flight_model');
		$this->flight_model->save_search_data($search_params, META_AIRLINE_COURSE);
		redirect('index.php/flight/search_crs/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
	}

	function pre_crs_booking(){

		$post_params = $this->input->post ();	
		$search_id=$post_params['search_id'];
		//debug($post_params);
		if (valid_array ( $post_params ) == false) {
			redirect ( base_url () );
		}
		//debug($post_params);exit;
		// $this->custom_db->generate_static_response(json_encode($post_params));
		// Insert To temp_booking and proceed
		/* $post_params = $this->flight_model->get_static_response($static_search_result_id); */

		//Setting Static Data - Jaganath
		$post_params['billing_city'] = 'Bangalore';
		$post_params['billing_zipcode'] = '560100';

		
		// Make sure token and temp token matches
		$valid_temp_token = unserialized_data ( $post_params ['token'] );
		
		if ($valid_temp_token != false) {
			
			if($post_params ['booking_source'] == "PTBSID0000000005")
			{//if($post_params ['booking_source'] == CRS_FLIGHT_BOOKING_SOURCE)
				$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => get_application_currency_preference (),
					'to' => admin_base_currency() 
			    ));
				$temp_booking = $this->module_model->serialize_temp_booking_record ( $post_params, FLIGHT_BOOKING );
		        $book_id = $temp_booking ['book_id'];
		        $book_origin = $temp_booking ['temp_booking_origin'];
		        $booking_data = $this->module_model->unserialize_temp_booking_record ( $book_id, $book_origin );
		      	$safe_search_data = $this->flight_model->get_safe_search_data($search_id);
				$adult_count=$safe_search_data['data']['adult_config'];
                $child_count=$safe_search_data['data']['child_config'];
                $infant_count=$safe_search_data['data']['infant_config'];
				$book_params = $booking_data['book_attributes'];
				
				
				foreach ($valid_temp_token as $token_index => $token_value) {  
             	$adult=($token_value['adult_price'] + $token_value['adult_tax'] + $token_value['adult_markup']) * $adult_count ;
            	 $child=($token_value['child_price'] + $token_value['child_tax'] + $token_value['child_markup']) * $child_count ;
            	 $infant=($token_value['infant_price'] + $token_value['infant_tax'] + $token_value['infant_markup'] ) * $infant_count ;
            	 $total_fare=$adult + $child + $infant;
             	$fare= $token_value['adult_price'] +  $token_value['child_price'] + $token_value['infant_price'];
              	$tax=($token_value['adult_tax'] + $token_value['adult_markup']) + ($token_value['child_tax'] + $token_value['child_markup']) + ($token_value['infant_tax'] + $token_value['infant_markup']) ;
                 
                 }
				$amount = $total_fare;
				$currency = 'USD'; 
				@$is_connection_trip=$book_params['token'][0]['connection_type'];   
				if($is_connection_trip=="yes"){
					$this->save_crs_booking_connection($book_id, $book_params,$currency_obj, $this->current_module);
				}else{ 
				$data = $this->save_crs_booking($book_id, $book_params,$currency_obj, $this->current_module);  
				}
				if($post_params['supplier_id']==62)
				{
					$crs_balance_deduction=$this->agent_balance_mangment_crs($amount,$temp_booking);

					if($crs_balance_deduction['payment_status']!=0)
					{

					$this->send_mail_agent_crs($temp_booking);
					die(" CHECK MAIL"); 
					$this->offline_request($book_id,$book_origin);  
					}
					else
					{
					redirect ( base_url () . 'index.php/flight/exception?op=Remote IO error @ FLIGHT Booking&notification=validation' ); 	
					}  
				}
				else
				{
				$this->offline_request($book_id,$book_origin);
				}


				//function secure_booking($book_id, $temp_book_origin) 
		        
		        
		    }
			
		}
		redirect ( base_url () . 'index.php/flight/exception?op=Remote IO error @ FLIGHT Booking&notification=validation' );
	
    }


    function save_crs_booking($app_booking_id, $book_params, $currency_obj, $module='b2c')
	   {

		//Need to return following data as this is needed to save the booking fare in the transaction details
		$response['fare'] = $response['domain_markup'] = $response['level_one_markup'] = 0;
		$book_total_fare = array();
		$book_domain_markup = array();
		$book_level_one_markup = array();
		$master_transaction_status = 'BOOKING_HOLD';
		$master_search_id = $book_params['search_id'];
		$domain_origin = get_domain_auth_id(); 
		$app_reference = $app_booking_id;
		$booking_source = $book_params['booking_source'];
		//PASSENGER DATA UPDATE
		$total_pax_count = count($book_params['passenger_type']);
		 $pax_count = $total_pax_count; 
		//PREFERRED TRANSACTION CURRENCY AND CURRENCY CONVERSION RATE 
		$transaction_currency = get_application_currency_preference();
		$application_currency = admin_base_currency();
		$currency_conversion_rate = '1'; 
		//********************** only for calculation
		$safe_search_data = $this->flight_model->get_safe_search_data($master_search_id);
		//debug($safe_search_data);
		$safe_search_data = $safe_search_data['data'];
		$adult_count=$safe_search_data['adult_config'];
        $child_count=$safe_search_data['child_config'];
        $infant_count=$safe_search_data['infant_config'];
		$safe_search_data['is_domestic_one_way_flight'] = false;
		$from_to_trip_type = $safe_search_data['trip_type'];
		if(strtolower($from_to_trip_type) == 'multicity') {
			$from_loc = $safe_search_data['from'][0];
			$to_loc = end($safe_search_data['to']);
			//$journey_from = $safe_search_data['from_city'][0];
			//$journey_to = end($safe_search_data['to_city']);
		} else {
			$from_loc = $safe_search_data['from'];
			$to_loc = $safe_search_data['to'];
			//$journey_from = $safe_search_data['from_city'];
			//$journey_to = $safe_search_data['to_city'];
		}
		$safe_search_data['is_domestic_one_way_flight'] = $GLOBALS['CI']->flight_model->is_domestic_flight($from_loc, $to_loc);
		if ($safe_search_data['is_domestic_one_way_flight'] == false && strtolower($from_to_trip_type) == 'return') {
			$multiplier = $pax_count ;//Multiply with 2 for international round way
		} else if(strtolower($from_to_trip_type) == 'multicity'){
			$multiplier = $pax_count ;
		} else {
			$multiplier = $pax_count;
		}
		//********************* only for calculation
		$token = $book_params['token'];
		//debug($token);die(" OUT");  

        $master_booking_source = array();
		$currency = $currency_obj->to_currency;
		$deduction_cur_obj	= clone $currency_obj;
		//Storing Flight Details - Every Segment can repeate also
		foreach ($token as $token_index => $token_value) {
              
             $flight_orgin=$token_value['orgin'];
             $flight_aviablity=$token_value['left_seats'];
             $flight_hold=@$token_value['hold_seats'];
             $token_value['adult_price']; 
    		  $adult=($token_value['adult_price'] + $token_value['adult_tax'] + $token_value['adult_markup']) * $adult_count ; 
    		  $child=($token_value['child_price'] + $token_value['child_tax'] + $token_value['child_markup']) * $child_count ; 
    		  $infant=($token_value['infant_price'] + $token_value['infant_tax'] + $token_value['infant_markup'] ) * $infant_count ; 
     		 $total_fare= $adult + $child + $infant; 
      		$fare= ($token_value['adult_price'] * $adult_count) + ($token_value['child_price'] * $child_count ) + ( $token_value['infant_price'] * $infant_count );
      
      		$tax=($token_value['adult_tax'] + $token_value['adult_markup']) + ($token_value['child_tax'] + $token_value['child_markup']) + ($token_value['infant_tax'] + $token_value['infant_markup']) ;
      		/*$markup=($token_value['adult_markup']  * $adult_count) +
                 ($token_value['child_markup']  * $child_count) +
                 ($token_value['infant_markup']  * $infant_count) ;*/
      		$markup=0;
     		 $net_fare=(($token_value['adult_price'] + $token_value['adult_tax']) * $adult_count) +
                 (($token_value['child_price'] + $token_value['child_tax']) * $child_count) +
                 (($token_value['infant_price'] + $token_value['infant_tax']) * $infant_count) ;

     		$total_amount=$net_fare + $markup;
			$airline_code= $token_value['onward_code'];
			//$segment_details = $token_value['SegmentDetails'];
			//$segment_summary = $token_value['SegmentSummary'];
			//$Fare = $token_value['FareDetails']['api_PriceDetails'];
			$tmp_domain_markup = 0;
			$tmp_level_one_markup = 0;
			$itinerary_price	= $fare;
			//Calculation is different for b2b and b2c
			//Specific Markup Config
			$specific_markup_config = array();
			$specific_markup_config = $this->get_airline_specific_markup_config($airline_code);//Get the Airline code for setting airline-wise markup
			$admin_commission = 0;
			$agent_commission = 0;
			$admin_tds = 0;
			$agent_tds = 0;
			$core_agent_commision = ($total_amount);
			if ($module == 'b2c') {
				//$trans_total_fare = $this->total_price($Fare, false, $currency_obj); 
				if($from_to_trip_type=="oneway"){
					// need to update 1 substraction
					if($token_value['flight_trip']=="oneway" || $token_value['flight_trip']=="circle"){
    					$flight_crs_no=$book_params['token'][0]['flight_crs_no'];
						$trans_total_fare = $total_amount;
					}
					}elseif($from_to_trip_type=="circle" &&  $token_value['flight_trip']=="circle"){
						$trans_total_fare = $total_amount*2;
					}
				
				// for crs no markup is added only tax has been added 
				$markup_total_fare=0;
				$ded_total_fare=0; 
				$admin_markup=0;
				$agent_markup=0;
				$admin_commission =0;
				
			} 
			//TDS Calculation
			$admin_tds = $currency_obj->calculate_tds($admin_commission);
			$agent_tds = $currency_obj->calculate_tds($agent_commission);
			//**************Ticketing For Each Token START
			//Following Variables are used to save Transaction and Pax Ticket Details
			$pnr_g=substr(md5(microtime()*rand(0,99)),0,6);
			
			$pnr = strtoupper($pnr_g);
			$book_id = '';
			$source = '';
			$ref_id = '';
			$transaction_status = 0;
			$GetBookingResult = array();
			$transaction_description = '';
			$getbooking_StatusCode = '';
			$getbooking_Description = '';
			$getbooking_Category = '';
			$WSTicket = array();
			$WSFareRule = array();
			//Saving Flight Transaction Details
			$tranaction_attributes = array();
			$pnr = '';
			$book_id = '';
			$source = $this->get_tbo_source_name($token_value['airline_name']);
			$ref_id = '';
			$transaction_status = $master_transaction_status;
			$base_fare=$fare;
			$transaction_description = '';
			//Get Booking Details
			$getbooking_status_details = '';
			$getbooking_StatusCode = '';
			$getbooking_Description = '';
			$getbooking_Category = '';
			$tranaction_attributes['Fare'] = $fare;
			$sequence_number = $token_index;
			//Transaction Log Details
			$ticket_trans_status_group[] = $transaction_status;
			$book_total_fare[]	= $trans_total_fare;
			$book_domain_markup[]	= $admin_markup;
			$book_level_one_markup[] = $agent_markup;
			//Need individual transaction price details
			//SAVE Transaction Details

			$transaction_insert_id = $GLOBALS['CI']->flight_model->save_flight_booking_transaction_details_crs(
			$app_reference, $transaction_status, $transaction_description, $pnr, $book_id, $source, $ref_id,$base_fare,
			json_encode($tranaction_attributes), $sequence_number, $currency, $trans_total_fare, $admin_markup, $agent_markup,
			$admin_commission, $agent_commission,
			$getbooking_StatusCode, $getbooking_Description, $getbooking_Category,
			$admin_tds, $agent_tds
			);
			$transaction_insert_id = $transaction_insert_id['insert_id'];

			//Saving Passenger Details
			$i = 0;
			for ($i=0; $i<$total_pax_count; $i++)
			{
				$passenger_type = $book_params['passenger_type'][$i];
				$is_lead = $book_params['lead_passenger'][$i];
				$title = get_enum_list('title', $book_params['name_title'][$i]);
				$first_name = $book_params['first_name'][$i];
				$middle_name = '';//$book_params['middle_name'][$i];
				$last_name = $book_params['last_name'][$i];
				$date_of_birth = $book_params['date_of_birth'][$i];
				$gender = get_enum_list('gender', $book_params['gender'][$i]);

				$passenger_nationality_id = intval($book_params['passenger_nationality'][$i]);
				$passport_issuing_country_id = intval($book_params['passenger_passport_issuing_country'][$i]);
				$passenger_nationality = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passenger_nationality_id));
				$passport_issuing_country = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passport_issuing_country_id));

				$passenger_nationality = isset($passenger_nationality[$passenger_nationality_id]) ? $passenger_nationality[$passenger_nationality_id] : '';
				$passport_issuing_country = isset($passport_issuing_country[$passport_issuing_country_id]) ? $passport_issuing_country[$passport_issuing_country_id] : '';

				$passport_number = $book_params['passenger_passport_number'][$i];
				$passport_expiry_date = $book_params['passenger_passport_expiry_year'][$i].'-'.$book_params['passenger_passport_expiry_month'][$i].'-'.$book_params['passenger_passport_expiry_day'][$i];
				//$status = 'BOOKING_CONFIRMED';//Check it
				$status = $master_transaction_status;
				$passenger_attributes = array();
				$flight_booking_transaction_details_fk = $transaction_insert_id;//Adding Transaction Details Origin 

				//SAVE Pax Details
				$pax_insert_id = $GLOBALS['CI']->flight_model->save_flight_booking_passenger_details(
				$app_reference, $passenger_type, $is_lead, $title, $first_name, $middle_name, $last_name, $date_of_birth,
				$gender, $passenger_nationality, $passport_number, $passport_issuing_country, $passport_expiry_date, $status,
				json_encode($passenger_attributes), $flight_booking_transaction_details_fk);
			}//Adding Pax Details Ends
				
			//Saving Segment Details

            if($from_to_trip_type=='oneway') 
            {

                foreach($token as $seg_k => $seg_v) {
				
					$FareRestriction = '';
					$FareBasisCode = '';
					$FareRuleDetail = $seg_v['fare_rules'];
					$airline_pnr = strtoupper($pnr_g);
					$depature=$safe_search_data['depature'] . ' ' . $seg_v['onwards_departure_time'];
					$arrival=$safe_search_data['depature'] . ' ' . $seg_v['onwards_arrival_time'];
					
					$segment_indicator ='1';
					$airline_code = $seg_v['onward_code'];
					$airline_name = $seg_v['airline_name'];
					$flight_number = $seg_v['onwards_flight_number'];
					$fare_class = $seg_v['class'];
					$from_airport_code = substr($seg_v['from_city'],-5);
					$from_airport_name = substr($seg_v['from_city'],0,-5);
					$to_airport_code = substr($seg_v['to_city'],-5);
					$to_airport_name = substr($seg_v['to_city'],0,-5);
					$departure_datetime = date('Y-m-d H:i:s', strtotime($depature));
					$arrival_datetime = date('Y-m-d H:i:s', strtotime($arrival));
					$iti_status = $seg_v['status'];
					$operating_carrier = $seg_v['onward_code'];
					$attributes = array('craft' => $seg_v['orgin'], 'ws_val' => $seg_v);
					//SAVE ITINERARY
					$GLOBALS['CI']->flight_model->save_flight_booking_itinerary_details(
					$app_reference, $segment_indicator, $airline_code, $airline_name, $flight_number, $fare_class, $from_airport_code, $from_airport_name,
					$to_airport_code, $to_airport_name, $departure_datetime, $arrival_datetime, $iti_status, $operating_carrier, json_encode($attributes),
					$FareRestriction, $FareBasisCode, $FareRuleDetail, $airline_pnr);
				}
            }
            if($from_to_trip_type=='circle') {


            	foreach($token as $seg_k => $seg_v) {
				
					$FareRestriction = '';
					$FareBasisCode = '';
					$FareRuleDetail = $seg_v['fare_rules'];
					$airline_pnr = '';
					$depature=$safe_search_data['depature'] . ' ' . $seg_v['onwards_departure_time'];
					$arrival=$safe_search_data['depature'] . ' ' . $seg_v['onwards_arrival_time'];
					
					$segment_indicator ='1';
					$airline_code = $seg_v['onward_code'];
					$airline_name = $seg_v['airline_name'];
					$flight_number = $seg_v['onwards_flight_number'];
					$fare_class = $seg_v['class'];
					$from_airport_code = substr($seg_v['from_city'],-5);
					$from_airport_name = substr($seg_v['from_city'],0,-5);
					$to_airport_code = substr($seg_v['to_city'],-5);
					$to_airport_name = substr($seg_v['to_city'],0,-5);
					$departure_datetime = date('Y-m-d H:i:s', strtotime($depature));
					$arrival_datetime = date('Y-m-d H:i:s', strtotime($arrival));
					$iti_status = $seg_v['status'];
					$operating_carrier = $seg_v['onward_code'];
					$attributes = array('craft' => $seg_v['orgin'], 'ws_val' => $seg_v);
					//SAVE ITINERARY
					$GLOBALS['CI']->flight_model->save_flight_booking_itinerary_details(
					$app_reference, $segment_indicator, $airline_code, $airline_name, $flight_number, $fare_class, $from_airport_code, $from_airport_name,
					$to_airport_code, $to_airport_name, $departure_datetime, $arrival_datetime, $iti_status, $operating_carrier, json_encode($attributes),
					$FareRestriction, $FareBasisCode, $FareRuleDetail, $airline_pnr);


					$returnFareRestriction = '';
					$returnFareBasisCode = '';
					$returnFareRuleDetail = $seg_v['fare_rules'];
					$returnairline_pnr = '';
					  $returndepature=$safe_search_data['return'] . ' ' . $seg_v['return_departure_time'];
					  $returnarrival=$safe_search_data['return'] . ' ' . $seg_v['return_arrival_time'];
						
					$returnsegment_indicator ='2';
					$returnairline_code = $seg_v['onward_code'];
					$returnairline_name = $seg_v['airline_namee'];
					$returnflight_number = $seg_v['return_flight_number'];
					$returnfare_class = $seg_v['class'];
					$returnfrom_airport_code = substr($seg_v['return_from_city'],-5);
					$returnfrom_airport_name = substr($seg_v['return_from_city'],0,-5);
					$returnto_airport_code = substr($seg_v['return_to_city'],-5);
					$returnto_airport_name = substr($seg_v['return_to_city'],0,-5);
					$returndeparture_datetime = date('Y-m-d H:i:s', strtotime($returndepature));
					$returnarrival_datetime = date('Y-m-d H:i:s', strtotime($returnarrival));
					$returniti_status = $seg_v['status'];
					$returnoperating_carrier = $seg_v['onward_code'];
					$returnattributes = array('craft' => $seg_v['orgin'], 'ws_val' => $seg_v);
					//SAVE ITINERARY
					$GLOBALS['CI']->flight_model->save_flight_booking_itinerary_details(
					$app_reference, $returnsegment_indicator, $returnairline_code, $returnairline_name, $returnflight_number, 
					$returnfare_class, $returnfrom_airport_code, $returnfrom_airport_name,
					$returnto_airport_code, $returnto_airport_name, $returndeparture_datetime, $returnarrival_datetime, $returniti_status, 
					$returnoperating_carrier, json_encode($returnattributes),
					$returnFareRestriction, $returnFareBasisCode, $returnFareRuleDetail, $returnairline_pnr);
                     }
			
				}
			//End Of Segments Loop
		}//End Of Token Loop

		$hold_seat=$pax_count;
		$hold=$flight_hold + $hold_seat;
		$left=$flight_aviablity - $hold_seat ;
		$request['hold_seats']=$hold;
		$request['left_seats']=$left;

		
		$this->load->model ( 'flight_model' );
		$this->custom_db->update_record ('flight_crs_list',$request,array('orgin' => $flight_orgin) );
                        

		//Save Master Booking Details
		$book_total_fare = array_sum($book_total_fare);
		$book_domain_markup = array_sum($book_domain_markup);
		$book_level_one_markup = array_sum($book_level_one_markup);

		$phone = $book_params['passenger_contact'];
		$alternate_number = '';
		$email = $book_params['billing_email'];
		$start = $token[0];
		$end = end($token);
		
        foreach($token as $seg_k => $seg_v) {
        	$depature=$safe_search_data['depature'] . ' ' . $seg_v['onwards_departure_time'];
			$arrival=$safe_search_data['depature'] . ' ' . $seg_v['onwards_arrival_time'];
			$journey_start = date('Y-m-d H:i:s', strtotime($depature));
		//$journey_end = end($segment_summary);
		//$journey_end = $journey_end['DestinationDetails']['ArrivalTime'];
		$journey_end = date('Y-m-d H:i:s', strtotime($arrival));
        }
		//$journey_start = $segment_summary[0]['OriginDetails']['DepartureTime'];
		//$journey_start = date('Y-m-d H:i:s', strtotime($depature));
		//$journey_end = end($segment_summary);
		//$journey_end = $journey_end['DestinationDetails']['ArrivalTime'];
		//$journey_end = date('Y-m-d H:i:s', strtotime($arrival));
		$journey_from = $safe_search_data['from_loc'];
		$journey_to = $safe_search_data['to_loc'];
		$payment_mode = $book_params['payment_method'];
		$created_by_id = intval(@$GLOBALS['CI']->entity_user_id);
		$passenger_country_id = intval($book_params['billing_country']);
		//$passenger_city_id = intval($book_params['billing_city']);
		$passenger_country = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passenger_country_id));
		//$passenger_city = $GLOBALS['CI']->db_cache_api->get_city_list(array('k' => 'origin', 'v' => 'destination'), array('origin' => $passenger_city_id));

		$passenger_country = isset($passenger_country[$passenger_country_id]) ? $passenger_country[$passenger_country_id] : '';
		//$passenger_city = isset($passenger_city[$passenger_city_id]) ? $passenger_city[$passenger_city_id] : '';
		$passenger_city = $book_params['billing_city'];
		$attributes = array('country' => $passenger_country, 'city' => $passenger_city, 'zipcode' => $book_params['billing_zipcode'], 'address' =>  $book_params['billing_address_1']);
		$flight_booking_status = $master_transaction_status;
		//SAVE Booking Details
		//exit($domain_origin."domain");
		$domain_id = $GLOBALS['CI']->session->userdata('domain_id');
		$branch_id = $GLOBALS['CI']->session->userdata('branch_id');
		$user_details_id = $GLOBALS['CI']->session->userdata('user_details_id');
		$user_type =  $GLOBALS['CI']->session->userdata('user_type');  
		if($this->session->userdata('user_logged_in') == 0) {
			$user_type =6;
		}
	
		$GLOBALS['CI']->flight_model->save_flight_booking_details(
		$domain_origin, $flight_booking_status,$app_reference, $booking_source, $phone, $alternate_number, $email,
		$journey_start, $journey_end, $journey_from, $journey_to, $payment_mode, json_encode($attributes), $created_by_id,
		$from_loc, $to_loc, $from_to_trip_type, $transaction_currency, $currency_conversion_rate,$domain_id,$branch_id,$user_details_id,$user_type
		); 

		/************** Update Convinence Fees And Other Details Start ******************/
		//Convinence_fees to be stored and discount
		$convinence = 0;
		$discount = 0;
		$convinence_value = 0;
		$convinence_type = 0;
		$convinence_type = 0; 		
		if ($module == 'b2c') {
			$master_search_id=$book_params['search_id']; ; 
			$convinence = $currency_obj->convenience_fees($book_total_fare, $master_search_id,$domain_id);    
			$convinence_row = $currency_obj->get_convenience_fees();
			$convinence_value = $convinence_row['value'];
			$convinence_type = $convinence_row['type'];
			$convinence_per_pax = $convinence_row['per_pax'];
		} elseif ($module == 'b2b') {

			$discount = 0;
			$convinence_per_pax = 0;
			
		}


		$GLOBALS['CI']->load->model('transaction');
		//SAVE Convinience and Discount Details
		$GLOBALS['CI']->transaction->update_convinence_discount_details('flight_booking_details', $app_reference, $discount, $convinence, $convinence_value, $convinence_type, $convinence_per_pax);
		/************** Update Convinence Fees And Other Details End ******************/
        
		/**
		 * Data to be returned after transaction is saved completely
		 */
		$response['fare'] = $book_total_fare;
		$response['admin_markup'] = $book_domain_markup;
		$response['agent_markup'] = $book_level_one_markup;
		$response['convinence'] = $convinence;
		$response['discount'] = $discount;

		$response['status'] = $flight_booking_status;
		$response['status_description'] = $transaction_description;
		$response['name'] = $first_name;
		$response['phone'] = $phone; 
		return $response;
	}


public function get_airline_specific_markup_config($airline_code)
	{
		$specific_markup_config = array();
		$airline_code = $airline_code;
		$category = 'airline_wise';
		$specific_markup_config[] = array('category' => $category, 'ref_id' => $airline_code);
		return $specific_markup_config;
	}

	private function get_tbo_source_name($source)
	{
		switch($source)
		{
			case 0 : $source = 'WorldSpan';
			break;
			case  1 : $source = 'Abacus';
			break;
			case  3: $source = 'SpiceJet';
			break;
			case  4 : $source = 'Amadeus';
			break;
			case 5 : $source = 'Galileo';
			break;
			case 6 : $source = 'Indigo';
			break;
			/*case 6 : $source = 'Paramount';
			 break;*/
			/*case 7 : $source = 'AirDeccan';
			 break;*/
			/*case  8 : $source = 'MDLR';
			 break;*/
			case 10: $source = 'GoAir';
			break;
			case 19 : $source = 'AirAsia';
			break;
			case 13 : $source = 'AirArabia';
			break;
			case 17 : $source = 'FlyDubai';
			break;
			case 14 : $source = 'AirIndiaExpress';
			break;
			case 46 : $source = 'AirCosta';
			break;
			case 48 : $source = 'BhutanAirlines';
			break;
			case 49 : $source = 'AirPegasus';
			break;
			case 50 : $source = 'TruJet';
			break;
		}
		return  $source;
	}

	function parse_session_create_response($SessionCreateRQ_RS){ 
        $SessionCreateRS 		= $SessionCreateRQ_RS['SessionCreateRS'];
        $response 				= $this->xml_to_array->XmlToArray($SessionCreateRS);
        $BinarySecurityToken	= array();
		if(isset($response['soap-env:Header']['eb:MessageHeader'])){
			$BinarySecurityToken['ConversationId']		 	= $response['soap-env:Header']['eb:MessageHeader']['eb:ConversationId'];
			$BinarySecurityToken['BinarySecurityToken'] 	= $response['soap-env:Header']['wsse:Security']['wsse:BinarySecurityToken']['@content'];
        }
        return $BinarySecurityToken;
	}


	/*ADDED 29/11/2107*/
	function secure_booking_tp($book_id, $book_origin) 
	{ 	
		//echo $book_id.', '.$book_origin;die();
		error_reporting(0);
		//$post_data = $this->input->post();exit;
		$book_origin = $this->uri->segment(4);
		//$book_origin = $this->
		if(isset($book_id) == true && isset($book_origin) == true && empty($book_id) == false && intval($book_origin) > 0)
		{
			//verify payment status and continue
			//$book_id = trim($post_data['book_id']);
			$temp_book_origin = intval($book_origin);
			//pls remove comment below
			/*$this->load->model('transaction');
			/$booking_status = $this->transaction->get_payment_status($book_id); */
			$booking_status['status'] = 'accepted';
			
			if($booking_status['status'] !== 'accepted'){
				redirect(base_url().'index.php/flight/exception?op=Payment Not Done&notification=validation');
				// echo "1";
			}
		} else{
			// echo "2";
			redirect(base_url().'index.php/flight/exception?op=InvalidBooking&notification=invalid');
		}
		// echo "jhere";exit;
		$flight_booking = $this->custom_db->single_table_records('flight_booking_details', 'seat_no', array('app_reference'=>$book_id));
		//debug($flight_booking);die;
		$seat_no = @$flight_booking['data']['0']['seat_no'];
		//run booking request and do booking
		$temp_booking = $this->module_model->unserialize_temp_booking_record ( $book_id, $temp_book_origin );
		// debug($temp_booking);die;
		
		//Delete the temp_booking record, after accessing
		// $this->module_model->delete_temp_booking_record ($book_id, $temp_book_origin);
		load_flight_lib ( $temp_booking ['booking_source'] );
		// if ($temp_booking ['booking_source'] == TRAVELPORT_FLIGHT_BOOKING_SOURCE)
		if (true){
			$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => admin_base_currency (),
					'to' => admin_base_currency () 
			) );
			$flight_details = json_decode(base64_decode($temp_booking ['book_attributes'] ['flights_data']), true) ;
			
			$fare_details = $flight_details['FareDetails'];
			//$flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
			//$fare_details = $flight_booking_summary['FareDetails'][$this->current_module.'_PriceDetails'];
			
			$total_booking_price = $fare_details['b2c_PriceDetails']['TotalFare'];
			$currency = $fare_details['b2c_PriceDetails']['Currency'];
		}
		// verify payment status and continue
		// Flight_Model::lock_tables();
		//no need for travelport bookng
		//$domain_balance_status = $this->domain_management_model->verify_current_balance ( $total_booking_price, $currency );
		
		if ($temp_booking != false)
		{
			//switch ($temp_booking ['booking_source'])
			//{
				//case TRAVELPORT_FLIGHT_BOOKING_SOURCE :
					try {
							//debug($temp_booking);exit;
							$segment = json_decode(base64_decode($temp_booking['book_attributes']['flights_data']));
							$segment_details = $segment->SegmentDetails;
							// debug($segment_details); exit;
							$ds = 0;
							foreach ($segment_details as $key => $value) {

								//$this->flight_lib->air_create_reservation_request ( $book_id, $temp_booking, $seat_no,$key)

							$request = $this->flight_lib->air_create_reservation_request ( $book_id, $temp_booking, $seat_no,$key);
							//debug($request);exit;
							if($request['status'] == true)
							{	
								//echo "air reser request";debug($request);exit;

								$AirCreateReservationReq = $request['data']['request'];					
							
								$AirCreateReservationRes = $this->flight_lib->process_request($AirCreateReservationReq);
								$responsexml ='AirCreateReservationRes.xml';
								file_put_contents('../b2b2b/xml_logs/Flight/'.$responsexml, $AirCreateReservationRes); 
								
								/*$responsexml='AirCreateReservationRes.xml';
								$AirCreateReservationRes = file_get_contents('../b2b2b/xml_logs/Flight/'.$responsexml);*/ 
								//$AirCreateReservationRes = $request['data']['response'];			 		
								$return_search_response_array = Converter::createArray ( $AirCreateReservationRes );						
								
								$response_details = ($return_search_response_array['SOAP:Envelope']['SOAP:Body']);
								//debug($response_details);
								if(isset($response_details['SOAP:Fault'])){
									error_reporting(0);
									$booking ['status'] = BOOKING_ERROR;
									//$booking ['status'] = BOOKING_FAILED;	
									$booking ['msg'] = $response_details['SOAP:Fault']['faultstring'];
									$response_att = $response_details['SOAP:Fault']['faultstring'];

									$GLOBALS['CI']->custom_db->update_record('flight_booking_details', array('status' =>'BOOKING_FAILED', 'res_attributes' =>$response_att), array('app_reference' => $book_id));
									$GLOBALS['CI']->custom_db->update_record('flight_booking_passenger_details', array('status' => 'BOOKING_FAILED', 'attributes' =>$response_att), array('app_reference' => $book_id));
									$GLOBALS['CI']->custom_db->update_record('flight_booking_transaction_details', array('status' => 'BOOKING_FAILED', 'getbooking_Description' =>$response_att), array('app_reference' => $book_id));

									//echo 'booking failed error'; die;
									redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $booking ['msg'] );
									//echo "3";
								}
								elseif($response_details)
								{
									@$UniversalRecord_Deatils = $response_details['universal:AirCreateReservationRsp']['universal:UniversalRecord'];
									$seat_response = $response_att ='';
									if(isset( $response_details['universal:AirCreateReservationRsp']['common_v41_0:ResponseMessage'])){
										$reponse_msg =  $response_details['universal:AirCreateReservationRsp']['common_v41_0:ResponseMessage'];
										//check this line
										if(isset($reponse_msg[0])){
											$reponse_msg = $reponse_msg; 
										}else{
											$reponse_msg = array($reponse_msg);
										}
										foreach($reponse_msg as $msg){
											$response_att .=$msg['@value'].'<br>';
										}
									}
									
									if(isset($response_details['universal:AirCreateReservationRsp']['universal:UniversalRecord']['common_v41_0:BookingTraveler']['common_v41_0:AirSeatAssignment'])){
										$airSeatSegment =$response_details['universal:AirCreateReservationRsp']['universal:UniversalRecord']['common_v41_0:BookingTraveler']['common_v41_0:AirSeatAssignment'];
										if(isset($airSeatSegment[0])){
											$airSeatSegment = $airSeatSegment; 
										}else{
											$airSeatSegment = array($airSeatSegment);
										}
										foreach($airSeatSegment as $k=> $seat){
											if(@$seat['@attributes']['Status'] == 'HK'){
												$seat_response[$k] .= $seat['@attributes']['Seat'].'';
											}
										}
									}
									# debug($seat_response); die;
									$seat_response = json_encode($seat_response);
									 $booking ['status'] =BOOKING_CONFIRMED;
								}

						//Riya Auto ticketing pnr have to do in this place//
							   
							   // $arrItinaryXML = $this->Xml_to_array->XmlToArray($AirCreateReservationRes);
						//echo "ticketing ++ ";debug($arrItinaryXML);exit;
								$parent_pnr = '';
								$strAirPNR  = '';
								$AirCreateTicketingRes = $this->flight_lib->do_autoticket_tp($parent_pnr,$strAirPNR);	

						//END Riya Auto ticketing 
								
								$currency_obj = new Currency ( array (
										'module_type' => 'flight',
										'from' => admin_base_currency (),
										'to' => admin_base_currency () 
								) );
								$booking ['data'] ['booking_params'] ['currency_obj'] = $currency_obj;
								
								//Update the booking Details
								$icket_details = @$UniversalRecord_Deatils;
								$icket_details['master_booking_status'] = $booking ['status'];
					
								$response = array('book_id' => $book_id, 'icket_details' => $icket_details, 'temp_booking' => @$temp_booking, 'response_attr' => $response_att);
								$data = $this->update_booking_details($book_id, $icket_details, @$temp_booking, $this->current_module, $response_att, @$seat_reponse,$ds);
								//debug($data);die(" new  down 564564");	
								/*$this->domain_management_model->update_transaction_details ( 'flight', $book_id, $data ['fare'], $data ['admin_markup'], $data ['agent_markup'], $data['convinence'], $data['discount'],$data['transaction_currency'], $data['currency_conversion_rate'] );*/
						
								if (in_array ( $data ['status'], array (
								'BOOKING_CONFIRMED',
								'BOOKING_PENDING',
								'BOOKING_HOLD', 'SUCCESS_STATUS','BOOKING_FAILED'
								) )) {
									// Sms config & Checkpoint
									/* if (active_sms_checkpoint ( 'booking' )) {
									$msg = "Dear " . $data ['name'] . " Thank you for Booking your ticket with us.Ticket Details will be sent to your email id";
									$msg = urlencode ( $msg );
									$sms_status = $this->provab_sms->send_msg ( $data ['phone'], $msg );
									// return $sms_status;
									} */
								//echo "Url: ";debug($data ['status']);
									/*redirect (base_url ().'index.php/voucher/flight_TP/' .$book_id .'/'.$temp_booking ['booking_source'].'/'.$data ['status'].'/show_voucher' );*/
									//echo "4";
								} else {

									redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $booking ['msg'] );
									//echo "5";
								}
								
							}else{
								echo 'problem in booking request data'; die;
							}
						$ds++;
						}//For each end
							redirect (base_url ().'index.php/voucher/flight/' .$book_id .'/'.$temp_booking ['booking_source'].'/'.$data ['status'].'/show_voucher' );
						}catch (Exception $e) {
							$booking ['status'] = BOOKING_ERROR;
						}
						// Save booking based on booking status and book id
						//break;
				//}
			}

			// release table lock
			Flight_Model::release_locked_tables ();
		
		// redirect(base_url().'index.php/flight/exception?op=Remote IO error @ FLIGHT Secure Booking&notification=validation');
	}

	/**
 * added by princess on july 12 to update travelport datas
*/
public function update_booking_details($book_id, $ticket_details, $booking_details = array(), $module='b2c', $response_att, $seat_reponse, $AutoTicketing_TicketNo)
	{
		error_reporting(E_ALL);
		//PRICE DETAILS
		//STATUS
		$response = array();
		$book_total_fare = array();
		$book_domain_markup = array();
		$book_level_one_markup = array();
		
		$app_reference = $book_id;
		$book_params = $booking_details['book_attributes'];
		$master_search_id = $book_params ['search_id'];
		//Setting Master Booking Status

		$AirReservation_PNR = $ticket_details['air:AirReservation']['common_v41_0:SupplierLocator']['@attributes']['SupplierLocatorCode']; //GDS Airline Locator Code eg 9W
		if($ticket_details['master_booking_status'] == SUCCESS_STATUS) {
			
				$master_transaction_status = 'BOOKING_FAILED';

		} else {
			$master_transaction_status = 'BOOKING_FAILED';
		}

		if(isset($ticket_details) == true && valid_array($ticket_details) == true){
			$ticket_details = $ticket_details;
		} else {
			$ticket_details = array();
		}
		
		$saved_booking_data = $GLOBALS['CI']->flight_model->get_booking_details($book_id);
		
		if($saved_booking_data['status'] == false) {
			$response['status'] = BOOKING_ERROR;
			$response['msg'] = 'No Data Found';
			return $response;
		}
		//Extracting the Saved data
		$s_master_data = $saved_booking_data['data']['booking_details'][0];
		$s_booking_itinerary_details = $saved_booking_data['data']['booking_itinerary_details'];
		$s_booking_transaction_details = $saved_booking_data['data']['booking_transaction_details'];
		$s_booking_customer_details = $saved_booking_data['data']['booking_customer_details'];
		$first_name = $s_booking_customer_details[0]['first_name'];
		$phone = $s_master_data['phone'];
		$current_master_booking_status = $s_master_data['status'];
		//Extracting the Origins
		$transaction_origins = group_array_column($s_booking_transaction_details, 'origin');
		$passenger_origins = group_array_column($s_booking_customer_details, 'origin');
		$itinerary_origins = group_array_column($s_booking_itinerary_details, 'origin');
		//Indexing the data with origin
		$indexed_transaction_details = array();
		foreach($s_booking_transaction_details as $s_tk => $s_tv){
			$indexed_transaction_details[$s_tv['origin']] = $s_tv;
		}
		//1.Update : flight_booking_details
		$update_master_booking_status = true;
		if($current_master_booking_status == 'BOOKING_HOLD' && $master_transaction_status == 'BOOKING_FAILED'){
			$update_master_booking_status = false;
			$flight_master_booking_status = $current_master_booking_status;
		} else {
			$flight_master_booking_status = $master_transaction_status;
		}
		//IF IT IS IN HOLD STATUS AND TICKET IS FAILED, THEN DONT UPDATE THE STATUS
		if($update_master_booking_status == true){
			$GLOBALS['CI']->custom_db->update_record('flight_booking_details', array('status' => $master_transaction_status, 'res_attributes' =>$response_att, 'seat_attributes'=>$seat_reponse), array('app_reference' => $app_reference));
		#	echo $this->db->last_query(); die;
		}

		$total_pax_count = count($book_params['passenger_type']);
		
		$pax_count = $total_pax_count;
		//********************** only for calculation
		$safe_search_data = $this->search_data($master_search_id);
		$safe_search_data = $safe_search_data['data'];
		$from_loc = $safe_search_data['from'];
		$to_loc = $safe_search_data['to'];
		$safe_search_data['is_domestic_one_way_flight'] = false;
		$from_to_trip_type = $safe_search_data['trip_type'];
		
		$safe_search_data['is_domestic_one_way_flight'] = $GLOBALS['CI']->flight_model->is_domestic_flight($from_loc, $to_loc);
		if ($safe_search_data['is_domestic_one_way_flight'] == false && strtolower($from_to_trip_type) == 'return') {
			$multiplier = $pax_count * 2;//Multiply with 2 for international round way
		} else if(strtolower($from_to_trip_type) == 'multicity'){
			$multiplier = $pax_count * count($safe_search_data['from']);
		} else {
			$multiplier = $pax_count;
		}
		//********************* only for calculation
		$currency_obj = new Currency ( array (
							'module_type' => 'flight',
							'from' => admin_base_currency (),
							'to' => admin_base_currency () 
					) );
		$currency = $currency_obj->to_currency;
		$deduction_cur_obj	= clone $currency_obj;
		//PREFERRED TRANSACTION CURRENCY AND CURRENCY CONVERSION RATE 
		$transaction_currency = get_application_currency_preference();
		$application_currency = admin_base_currency();
		$currency_conversion_rate = $currency_obj->transaction_currency_conversion_rate();
		if(valid_array($ticket_details) == true) {
			
			$ProviderReservationInfo = $ticket_details['universal:ProviderReservationInfo']['@attributes'];
			$AirReservation = $ticket_details['air:AirReservation']['common_v41_0:SupplierLocator']['@attributes'];
			$book_attribtues = $booking_details['book_attributes'];
			$flights_data = json_decode(base64_decode($book_attribtues['flights_data']), true);

// By bhola for change in airpricing need to update price.
			$flights_pricing_xml  = json_decode(base64_decode($book_attribtues['pricing_xml']), true);
			//echo "soham "; debug($flights_pricing_xml);exit; 
// end By bhola for change in airpricing need to update price.
			
			//foreach ($ticket_details as $ticket_index => $ticket_value) {
				
				$transaction_details_origin = intval($transaction_origins[0]);
				if(valid_array($ticket_details) == true) { //IF Ticket is successfull
					
					$status = $master_transaction_status;					
					$api_booking_id = $ProviderReservationInfo['LocatorCode']; //GDS Locator CoDe
					$pnr = $AirReservation['SupplierLocatorCode']; //GDS Airline Locator Code eg 9W
					
					
					$Fare = $flights_data['FareDetails'];
					$segment_details = $flights_data['SegmentDetails'];


// By bhola for change in airpricing need to update price.
					$fare_diff = ceil(intval($Fare['b2c_PriceDetails']['TotalFare'])) - ceil(intval($flights_pricing_xml['total_price']));
					if($fare_diff == 0)
					{
						$Fare = $flights_data['FareDetails'];
					}
					else
					{

						$Fare['b2c_PriceDetails']['BaseFare'] = $flights_pricing_xml['base_price'];
						$Fare['b2c_PriceDetails']['TotalTax'] = $flights_pricing_xml['taxes'];
						$Fare['b2c_PriceDetails']['TotalFare'] = $flights_pricing_xml['total_price'];
						$Fare['b2c_PriceDetails']['currency'] = $flights_pricing_xml['total_price_curr'];

						$Fare['api_PriceDetails']['Currency'] = $flights_pricing_xml['base_price_curr'];
						$Fare['api_PriceDetails']['BaseFare'] = $flights_pricing_xml['base_price'];
						$Fare['api_PriceDetails']['Tax'] = $flights_pricing_xml['taxes'];
						$Fare['api_PriceDetails']['PublishedFare'] = $flights_pricing_xml['total_price'];
					}

// By bhola for change in airpricing need to update price.


					//$ticket_value = $ticket_value['data'];
					//$passenger_details = $ticket_value['PassengerDetails'];
					//$fare_rule = $ticket_value['FareRule'];
					
					
					
					//$api_booking_details = $this->get_booking_details($api_booking_id, $pnr);
					
					//if($api_booking_details['status'] == SUCCESS_STATUS) { //Updating the details
						//$FlightItinerary = $api_booking_details['data']['api_booking_details']['FlightItinerary'];
						$segment_details = $flights_data['SegmentDetails'];
//	Repeating in up		$Fare = $flights_data['FareDetails'];
						$passenger_details = @$flights_data['PassengerDetails'];
						$fare_rule = $flights_data['FareInfoList'];
					//}
					$tmp_domain_markup = 0;
					$tmp_level_one_markup = 0;
					
					$itinerary_price	= $Fare['b2c_PriceDetails']['BaseFare'];
					//Calculation is different for b2b and b2c
					//Specific Markup Config
					$specific_markup_config = array();
					//$specific_markup_config = $this->flight_lib->get_airline_specific_markup_config($segment_details);//Get the Airline code for setting airline-wise markup
					
					
					//added by princess
					$api_PriceDetails =$Fare['api_PriceDetails'];
					$b2c_PriceDetails = $Fare['b2c_PriceDetails'];
					$admin_commission = 0;
					$agent_commission = 0;
					$admin_tds = 0;
					$agent_tds = 0;
					
					
					$core_agent_commision = ($api_PriceDetails['PublishedFare']-$api_PriceDetails['OfferedFare']);
					$commissionable_fare = $b2c_PriceDetails['TotalFare'];
					
					
					if ($module == 'b2c') {
						$trans_total_fare = $Fare['b2c_PriceDetails']['TotalFare'];
						
						//markup as on current currency
						$markup_total_fare	= $currency_obj->get_currency($trans_total_fare, true, false, true, $multiplier, $specific_markup_config);
						$ded_total_fare		= $deduction_cur_obj->get_currency($trans_total_fare, true, true, false, $multiplier, $specific_markup_config);
						//$admin_markup = intval(roundoff_number($markup_total_fare['default_value']-$ded_total_fare['default_value']));

// Admin markup by bhola
						$flight_admin_markup = $Fare['b2c_PriceDetails']['Admin_Markup'];

						/*$admin_markup = roundoff_number( $GLOBALS['CI']->flight_model->travelport_markup($flight_admin_markup['markup_type'], $flight_admin_markup['markup_val'], $Fare['b2c_PriceDetails']['TotalFare'], $flight_admin_markup['total_pax']) );*/
//End Admin markup by bhola

						$agent_markup = roundoff_number($ded_total_fare['default_value']-$trans_total_fare);
						//$admin_commission = $core_agent_commision; 		// commented by bhola
						$admin_commission = 0;
						$agent_commission = 0;
						
					} else {//B2B Calculation
						//Markup
						$trans_total_fare = $Fare['PublishedFare'];
						$markup_total_fare	= $currency_obj->get_currency($trans_total_fare, true, true, true, $multiplier, $specific_markup_config);
						$ded_total_fare		= $deduction_cur_obj->get_currency($trans_total_fare, true, false, true, $multiplier, $specific_markup_config);
						$admin_markup = roundoff_number($markup_total_fare['default_value']-$ded_total_fare['default_value']);
						$agent_markup = roundoff_number($ded_total_fare['default_value']-$trans_total_fare);
						//Commission
						$this->commission = $currency_obj->get_commission();
						$AgentCommission = $this->calculate_commission($core_agent_commision);
						$admin_commission = roundoff_number($core_agent_commision-$AgentCommission);//calculate here
						$agent_commission = roundoff_number($AgentCommission);
					}
					//TDS Calculation
					$admin_tds = 0;
					$agent_tds = 0;
					//2.Update : flight_booking_transaction_details
					$update_transaction_condition = array();
					$update_transaction_data = array();
					$update_transaction_condition['origin'] = $transaction_details_origin;
					$update_transaction_data['pnr'] = $pnr;
					$update_transaction_data['book_id'] = $api_booking_id;
					$update_transaction_data['status'] = $status;
					$update_transaction_data['total_fare'] = $commissionable_fare;
					$update_transaction_data['admin_commission'] = $admin_commission;
					$update_transaction_data['agent_commission'] = $agent_commission;
					$update_transaction_data['admin_tds'] = $admin_tds;
					$update_transaction_data['agent_tds'] = $agent_tds;
					$update_transaction_data['admin_markup'] = $admin_markup;
					$update_transaction_data['agent_markup'] = $agent_markup;
					//For Transaction Log
					$book_total_fare[]	= $trans_total_fare;
					$book_domain_markup[]	= $admin_markup;
					$book_level_one_markup[] = $agent_markup;
					$GLOBALS['CI']->custom_db->update_record('flight_booking_transaction_details', $update_transaction_data, $update_transaction_condition);
	
					//3.Update: flight_booking_passenger_details
					$update_passenger_condition = array();
					$update_passenger_data = array();
					$update_passenger_condition['flight_booking_transaction_details_fk'] = $transaction_details_origin;
					$update_passenger_data['status'] = $master_transaction_status;
					$GLOBALS['CI']->custom_db->update_record('flight_booking_passenger_details', $update_passenger_data, $update_passenger_condition);
	
		


					//4.Insert: Add Ticket details to flight_passenger_ticket_info
					/*foreach($passenger_details as $pax_k => $pax_v){
						$pax_ticket_details = $pax_v['Ticket'];
						$passenger_fk = intval(array_shift($passenger_origins));
						$TicketId = $pax_ticket_details['TicketId'];
						$TicketNumber = $pax_ticket_details['TicketNumber'];
						$IssueDate = $pax_ticket_details['IssueDate'];
						$Fare = json_encode($pax_v['FareDetails']);
						$SegmentAdditionalInfo = json_encode($pax_v['SegmentAdditionalInfo']);
						$ValidatingAirline = $pax_ticket_details['ValidatingAirline'];
						$CorporateCode = '';
						$TourCode = '';
						$Endorsement = '';
						$Remarks = $pax_ticket_details['Remarks'];
						$ServiceFeeDisplayType = $pax_ticket_details['ServiceFeeDisplayType'];
						
						//Update passenger ticket information
						$GLOBALS['CI']->flight_model->update_passenger_ticket_info($passenger_fk, $TicketId, $TicketNumber, $IssueDate, $Fare,
						$SegmentAdditionalInfo,	$ValidatingAirline, $CorporateCode, $TourCode, $Endorsement, $Remarks, $ServiceFeeDisplayType);
					}
					*/
					//5. Update :flight_booking_itinerary_details
					
					$AirReservation_Segments = $ticket_details['air:AirReservation']['air:AirSegment'];
					$AirReservation_Attr =$ticket_details['air:AirReservation']['@attributes'];
					 // for adjustment added this line.
					$segment_details = $AirReservation_Segments;
					//echo "roundway Int";debug($AirReservation_Segments); die;
					
					if($segment_details[0][0]['air:FlightDetails'] == true)
					{
						$segment_details[0][0]  = $AirReservation_Segments;
//echo "dfg 1 ";debug($segment_details);die;
						foreach($segment_details as $seg_k => $seg_v) 
						{
							foreach($seg_v as $ws_key => $ws_val) 
							{
								$update_segment_condition = array();
								$update_segement_data = array();
								$update_segment_condition['origin'] = intval(array_shift($itinerary_origins));
								$update_segement_data['airline_pnr'] = $AirReservation_Attr['LocatorCode'];
								$update_segement_data['status'] = $ws_val['@attributes']['Status'];
								//Fare Rule Details
								#$fare_rule_details = array_shift($fare_rule);
								$update_segement_data['FareRestriction'] = $ws_val['@attributes']['Key'];
								$update_segement_data['FareBasisCode'] =  $ws_val['@attributes']['ProviderReservationInfoRef'];
								$update_segement_data['FareRuleDetail'] = $ws_val['@attributes']['ProviderReservationInfoRef'];
							//echo "check ";debug($update_segement_data); die;
								$GLOBALS['CI']->custom_db->update_record('flight_booking_itinerary_details', $update_segement_data, $update_segment_condition);
							}
						}

					}
					elseif($segment_details[0]['air:FlightDetails'] == true)
					{
						$segment_details  = $AirReservation_Segments;
//echo "dfg 2 ";debug($segment_details);die;
						foreach($segment_details as $seg_k => $seg_v) 
						{
								$update_segment_condition = array();
								$update_segement_data = array();
								$update_segment_condition['origin'] = intval(array_shift($itinerary_origins));
								$update_segement_data['airline_pnr'] = $AirReservation_Attr['LocatorCode'];
								$update_segement_data['status'] = $seg_v['@attributes']['Status'];
								//Fare Rule Details
								#$fare_rule_details = array_shift($fare_rule);
								$update_segement_data['FareRestriction'] = $seg_v['@attributes']['Key'];
								$update_segement_data['FareBasisCode'] =  $seg_v['@attributes']['ProviderReservationInfoRef'];
								$update_segement_data['FareRuleDetail'] = $seg_v['@attributes']['ProviderReservationInfoRef'];
							//echo "check ";debug($update_segement_data); die;
								$GLOBALS['CI']->custom_db->update_record('flight_booking_itinerary_details', $update_segement_data, $update_segment_condition);
						}


					}
					elseif($segment_details['air:FlightDetails'] == true)
					{

						$segment_details  = $AirReservation_Segments;
//echo "dfg 3 ";debug($segment_details);die;
						$update_segment_condition = array();
						$update_segement_data = array();
						$update_segment_condition['origin'] = intval(array_shift($itinerary_origins));
						$update_segement_data['airline_pnr'] = $AirReservation_Attr['LocatorCode'];
						$update_segement_data['status'] = $segment_details['@attributes']['Status'];
						//Fare Rule Details
						#$fare_rule_details = array_shift($fare_rule);
						$update_segement_data['FareRestriction'] = $segment_details['@attributes']['Key'];
						$update_segement_data['FareBasisCode'] =  $segment_details['@attributes']['ProviderReservationInfoRef'];
						$update_segement_data['FareRuleDetail'] = $segment_details['@attributes']['ProviderReservationInfoRef'];
					//echo "check ";debug($update_segement_data); die;
						$GLOBALS['CI']->custom_db->update_record('flight_booking_itinerary_details', $update_segement_data, $update_segment_condition);

					}


				} else {//IF Ticket is Failed
					$GLOBALS['CI']->flight_model->update_flight_booking_transaction_failure_status($app_reference, $transaction_details_origin);
					//For Transaction Log
					$book_total_fare[]	= $indexed_transaction_details[$transaction_details_origin]['total_fare'];
					$book_domain_markup[]	= $indexed_transaction_details[$transaction_details_origin]['admin_markup'];
					$book_level_one_markup[] = $indexed_transaction_details[$transaction_details_origin]['agent_markup'];
				}
			//} //main tikcetvalue forloopends
			
		} else {
			foreach ($indexed_transaction_details as $itd_k => $itd_v){
				$book_total_fare[]	= $itd_v['total_fare'];
				$book_domain_markup[]	= $itd_v['admin_markup'];
				$book_level_one_markup[] = $itd_v['agent_markup'];
			}
		}
		/**
		 * Data to be returned after transaction is saved completely
		 */
		$transaction_description = '';
		$book_total_fare = array_sum($book_total_fare);
		$book_domain_markup = array_sum($book_domain_markup);
		$book_level_one_markup = array_sum($book_level_one_markup);
		$discount = 0;
		if($module == 'b2c') {
			$total_transaction_amount = $book_total_fare+$book_domain_markup;
			$convinence = $currency_obj->convenience_fees($total_transaction_amount, $master_search_id);
		} else {
			$convinence = 0;
		}
		$response['fare'] = $book_total_fare;
		$response['admin_markup'] = $book_domain_markup;
		$response['agent_markup'] = $book_level_one_markup;
		$response['convinence'] = $convinence;
		$response['discount'] = $discount;

		$response['status'] = $flight_master_booking_status;
		$response['status_description'] = $transaction_description;
		$response['name'] = $first_name;
		$response['phone'] = $phone;
		$response['transaction_currency'] = $transaction_currency;
		$response['currency_conversion_rate'] = $currency_conversion_rate;
		
		return $response;
	}

	public function search_data($search_id)
	{
		$response['status'] = true; 
		$response['data'] = array();
		
			$clean_search_details =$GLOBALS['CI']->flight_model->get_safe_search_data($search_id);
			if ($clean_search_details['status'] == true) {
				$response['status'] = true;
				$response['data'] = $clean_search_details['data'];
				$response['data']['from'] = $clean_search_details['data']['from'];
				$response['data']['to'] = $clean_search_details['data']['to'];
				
				switch($clean_search_details['data']['trip_type']){
					case 'oneway':
						$response['data']['type'] = 'OneWay';
						$response['data']['depature'] = date("Y-m-d", strtotime($clean_search_details['data']['depature'])) . 'T00:00:00';
						$response['data']['return'] = date("Y-m-d", strtotime($clean_search_details['data']['depature'])) . 'T00:00:00';
						$response['data']['from'] = substr(chop(substr($clean_search_details['data']['from'], -5), ')'), -3);
						$response['data']['to'] = substr(chop(substr($clean_search_details['data']['to'], -5), ')'), -3);
						break;
					case 'circle':
						$response['data']['type'] = 'Return';
						$response['data']['depature'] = date("Y-m-d", strtotime($clean_search_details['data']['depature'])) . 'T00:00:00';
						$response['data']['return'] = date("Y-m-d", strtotime($clean_search_details['data']['return'])) . 'T00:00:00';
						$response['data']['from'] = substr(chop(substr($clean_search_details['data']['from'], -5), ')'), -3);
						$response['data']['to'] = substr(chop(substr($clean_search_details['data']['to'], -5), ')'), -3);
						break;
					case 'multicity':
						$response['data']['type'] = 'Multicity';
						for($i=0; $i<count($clean_search_details['data']['depature']); $i++) {
							$response['data']['depature'][$i] = date("Y-m-d", strtotime($clean_search_details['data']['depature'][$i])) . 'T00:00:00';
							$response['data']['return'][$i] = date("Y-m-d", strtotime($clean_search_details['data']['depature'][$i])) . 'T00:00:00';
							$response['data']['from'][$i] = substr(chop(substr($clean_search_details['data']['from'][$i], -5), ')'), -3);
							$response['data']['to'][$i] = substr(chop(substr($clean_search_details['data']['to'][$i], -5), ')'), -3);
						}
						break;

					default : $response['data']['type'] = 'OneWay';
				}
				$response['data']['adult'] = $clean_search_details['data']['adult_config'];
				$response['data']['child'] = $clean_search_details['data']['child_config'];
				$response['data']['infant'] = $clean_search_details['data']['infant_config'];
				$response['data']['v_class'] = $clean_search_details['data']['v_class'];
				$response['data']['carrier'] = implode($clean_search_details['data']['carrier']);
				
				$this->master_search_data = $response['data'];
			} else {
				$response['status'] = false;
			}
		
		$this->search_hash = md5(serialized_data($response['data']));
		return $response;
	}
}
