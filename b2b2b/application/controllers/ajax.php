<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// error_reporting(E_ALL);
class Ajax extends CI_Controller {
	private $current_module;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('flight_model');
		$this->load->model('General_Model');
		$this->load->model('hotel_model');
		$this->load->model('hotels_model');
		$this->load->model('transferv1_model');
		$this->current_module = $this->config->item('current_module');
		$this->user_type = $this->config->item('user_type');
		$this->load->model('db_cache_api');
		$this->load->library('currency');
		$this->load->helper('common');
		$this->load->model('domain_management_model');
		// $this->load->helper('custom/app');
		// $this->load->helper('custom/api_share');
	}

	/**
	 * get city list based on country
	 * @param $country_id
	 * @param $default_select
	 */
	function get_city_list($country_id=0, $default_select=0)
	{
		if (intval($country_id) != 0) {
			$condition = array('country' => $country_id);
			$order_by = array('destination' => 'asc');
			$option_list = $this->custom_db->single_table_records('api_city_list', 'origin as k, destination as v', $condition, 0, 1000000, $order_by);  
			if (valid_array($option_list['data'])) {
				echo get_compressed_output(generate_options($option_list['data'], array($default_select)));
				exit;
			}
		}
	}

	/*
     *
     * Sightseeing AutoSuggest List
     *
     */

    function get_sightseen_city_list() {

        $this->load->model('sightseeing_model');
        $term = $this->input->get('term'); //retrieve the search term that autocomplete sends
        $term = trim(strip_tags($term));
        $data_list = $this->sightseeing_model->get_sightseen_city_list($term);
        if (valid_array($data_list) == false) {
            $data_list = $this->sightseeing_model->get_sightseen_city_list('');
        }
        $suggestion_list = array();
        $result = array();
        foreach ($data_list as $city_list) {
            $suggestion_list['label'] = $city_list['city_name'];

            $suggestion_list['value'] = $city_list['city_name'];

          //  $suggestion_list['value'] = hotel_suggestion_value($city_list['city_name'], $city_list['country_name']);
            $suggestion_list['id'] = $city_list['origin'];
            if (empty($city_list['top_destination']) == false) {
                $suggestion_list['category'] = 'Top cities';
                $suggestion_list['type'] = 'Top cities';
            } else {
                $suggestion_list['category'] = 'Location list';
                $suggestion_list['type'] = 'Location list';
            }
           
            $suggestion_list['count'] = 0;
            $result[] = $suggestion_list;
        }
        $this->output_compressed_data($result);
    }



     public function transferv1_list($offset=0){     
        $search_params = $this->input->get();
        $safe_search_data = $this->transferv1_model->get_safe_search_data($search_params['search_id'],META_TRANSFERV1_COURSE);
        $limit = $this->config->item('transferv1_page_limit');

        if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
            load_transferv1_lib($search_params['booking_source']);

            switch($search_params['booking_source']) {

                case PROVAB_TRANSFERV1_BOOKING_SOURCE :
               
                    $raw_sightseeing_result = $this->transferv1_lib->get_transfer_list($safe_search_data);
                   # debug($raw_sightseeing_result);
                    if ($raw_sightseeing_result['status']) {
                        //Converting API currency data to preferred currency
                        $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
                        $raw_sightseeing_result = $this->transferv1_lib->search_data_in_preferred_currency($raw_sightseeing_result, $currency_obj,$this->user_type);
                       
                        //Display 
                        $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

                        $filters = array();
                       //Update currency and filter summary appended
                        if (isset($search_params['filters']) == true and valid_array($search_params['filters']) == true) {
                            $filters = $search_params['filters'];
                        } else {
                            $filters = array();
                        }
                        
                        $raw_sightseeing_result['data'] = $this->transferv1_lib->format_search_response($raw_sightseeing_result['data'], $currency_obj, $search_params['search_id'], $this->user_type, $filters);

                        $source_result_count = $raw_sightseeing_result['data']['source_result_count'];
                        $filter_result_count = $raw_sightseeing_result['data']['filter_result_count'];
                        //debug($raw_hotel_list);exit;
                        if (intval($offset) == 0) {
                            //Need filters only if the data is being loaded first time
                            $filters = $this->transferv1_lib->filter_summary($raw_sightseeing_result['data']);
                            $response['filters'] = $filters['data'];
                        }
                       
                        $attr['search_id'] = abs($search_params['search_id']);
                       // debug($raw_sightseeing_result);exit;
                        $response['data'] = get_compressed_output(
                                $this->load->view('transferv1/viator/viator_search_result', array('currency_obj' => $currency_obj, 'raw_sightseeing_list' => $raw_sightseeing_result['data'],
                                    'search_id' => $search_params['search_id'], 'booking_source' => $search_params['booking_source'],
                                    'attr' => $attr,
                                    'search_params' => $safe_search_data['data']
                        ), true));
                        $response['status'] = SUCCESS_STATUS;
                        $response['total_result_count'] = $source_result_count;
                        $response['filter_result_count'] = $filter_result_count;
                        $response['offset'] = $offset + $limit;
                    }else{
                        $response['status'] = FAILURE_STATUS;
                        
                    }
                break;
            }
        }
        $this->output_compressed_data($response);
    }


	/**
	 *
	 * @param $continent_id
	 * @param $default_select
	 * @param $zone_id
	 */
	function get_country_list($continent_id=array(), $default_select=0,$zone_id=0)
	{
		$this->load->model('general_model');
		$continent_id=urldecode($continent_id);
		if (intval($continent_id) != 0) {
			$option_list = $this->general_model->get_country_list($continent_id,$zone_id);
			if (valid_array($option_list['data'])) {
				echo get_compressed_output(generate_options($option_list['data'], array($default_select)));
			}
		}
	}

	/**
	 *Get Location List
	 */
	function location_list($limit=AUTO_SUGGESTION_LIMIT)
	{
		$chars = $_GET['term'];
		$list = $this->general_model->get_location_list($chars, $limit);
		$temp_list = '';
		if (valid_array($list) == true) {
			foreach ($list as $k => $v) {
				$temp_list[] = array('id' => $k, 'label' => $v['name'], 'value' => $v['origin']);
			}
		}
		$this->output_compressed_data($temp_list);
	}

	/**
	 *Get Location List
	 */
	function city_list($limit=AUTO_SUGGESTION_LIMIT)
	{
		$chars = $_GET['term'];
		$list = $this->general_model->get_city_list($chars, $limit);
		$temp_list = '';
		if (valid_array($list) == true) {
			foreach ($list as $k => $v) {
				$temp_list[] = array('id' => $k, 'label' => $v['name'], 'value' => $v['origin']);
			}
		}
		$this->output_compressed_data($temp_list);
	}

	/**
	 * Arjun J Gowda
	 * @param unknown_type $currency_origin origin of currency - default to USD
	 */
	function get_currency_value($currency_origin=0)
	{
		$data = $this->custom_db->single_table_records('currency_converter', 'value', array('id' => intval($currency_origin)));
		if (valid_array($data['data'])) {
			$response = $data['data'][0]['value'];
		} else {
			$response = 1;
		}
		header('Content-type:application/json');
		echo json_encode(array('value' => $response));
		exit;
	}

	/*
	 *
	 * Flight(Airport) auto suggest
	 *
	 */
	function get_airport_code_list()
	{
		$flagpath =ASSETS.'assets/images/flags/'; 
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$result = array();

		$__airports = $this->flight_model->get_airport_list($term)->result();
		if (valid_array($__airports) == false) {
			$__airports = $this->flight_model->get_airport_list('')->result();
		}
		$airports = array();
		foreach($__airports as $airport){
			$airports['label'] = $airport->airport_city.', '.$airport->country.' ('.$airport->airport_code.')';
			$airports['code'] = $airport->airport_code;
			$airports['value'] = $airport->airport_city.' ('.$airport->airport_code.')';
			$airports['country_code'] = $flagpath.strtolower($airport->airport_country_code).'.png';     
			$airports['id'] = $airport->origin;
			if (empty($airport->top_destination) == false) {
				$airports['category'] = 'Top cities';
				$airports['type'] = 'Top cities';
			} else {
				$airports['category'] = 'Search Results';
				$airports['type'] = 'Search Results';
			}

			array_push($result,$airports);
		} 
		$this->output_compressed_data($result);
	} 

	/*
	 *
	 * Hotels City auto suggest
	 *
	 */
	function get_hotel_city_list()
	{
		$this->load->model('hotel_model');
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$data_list = $this->hotel_model->get_hotel_city_list($term);
		if (valid_array($data_list) == false) {
			$data_list = $this->hotel_model->get_hotel_city_list('');
		}
		$suggestion_list = array();
		$result = array();
		foreach($data_list as $city_list){
			$suggestion_list['label'] = $city_list['city_name'].', '.$city_list['country_name'].'';
			$suggestion_list['value'] = hotel_suggestion_value($city_list['city_name'], $city_list['country_name']);
			$suggestion_list['id'] = $city_list['origin'];
			if (empty($city_list['top_destination']) == false) {
				$suggestion_list['category'] = 'Top cities';
				$suggestion_list['type'] = 'Top cities';
			} else {
				$suggestion_list['category'] = 'Search Results';
				$suggestion_list['type'] = 'Search Results';
			}
			if (intval($city_list['cache_hotels_count']) > 0) {
				$suggestion_list['count'] = $city_list['cache_hotels_count'];
			} else {
				$suggestion_list['count'] = 0;
			}
			$result[] = $suggestion_list;
		}
		$this->output_compressed_data($result);
	}

	/**
	 * Auto Suggestion for bus stations
	 */
	function bus_stations()
	{
		$this->load->model('bus_model');
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$data_list = $this->bus_model->get_bus_station_list($term);
		if (valid_array($data_list) == false) {
			$data_list = $this->bus_model->get_bus_station_list('');
		}
		$suggestion_list = array();
		$result = array();
		foreach($data_list as $city_list){
			$suggestion_list['label'] = $city_list['name'];
			$suggestion_list['value'] = $city_list['name'];
			$suggestion_list['id'] = $city_list['origin'];
			if (empty($city_list['top_destination']) == false) {
				$suggestion_list['category'] = 'Top cities';
				$suggestion_list['type'] = 'Top cities';
			} else {
				$suggestion_list['category'] = 'Search Results';
				$suggestion_list['type'] = 'Search Results';
			}
			$result[] = $suggestion_list;
		}
		$this->output_compressed_data($result);
	}

	/**
	 * Load hotels from different source
	 */
	// function hotel_list($offset=0)
	// {
	// 	$response['data'] = '';
	// 	$response['msg'] = '';
	// 	$response['status'] = FAILURE_STATUS;
	// 	$search_params = $this->input->get();
	// 	$limit = $this->config->item('hotel_per_page_limit');
	// 	// debug($search_params['booking_source']);exit;
	// 	if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
	// 		load_hotel_lib($search_params['booking_source']);
	// 		switch($search_params['booking_source']) {
	// 			case PROVAB_HOTEL_BOOKING_SOURCE :
	// 				//getting search params from table
	// 				$safe_search_data = $this->hotel_model->get_safe_search_data($search_params['search_id']);
					
	// 				//Meaning hotels are loaded first time
	// 				$raw_hotel_list = $this->hotel_lib->get_hotel_list(abs($search_params['search_id']));

	// 				if ($raw_hotel_list['status']) {
	// 					//Converting API currency data to preferred currency
	// 					$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
	// 					$raw_hotel_list = $this->hotel_lib->search_data_in_preferred_currency($raw_hotel_list, $currency_obj);

	// 					//Display 
	// 					$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
	// 					//Update currency and filter summary appended
	// 					if (isset($search_params['filters']) == true and valid_array($search_params['filters']) == true) {
	// 						$filters = $search_params['filters'];
	// 					} else {
	// 						$filters = array();
	// 					}
	// 					$raw_hotel_list['data'] = $this->hotel_lib->format_search_response($raw_hotel_list['data'], $currency_obj, $search_params['search_id'], 'b2c', $filters);
                                                
	// 					$source_result_count = $raw_hotel_list['data']['source_result_count'];
	// 					$filter_result_count = $raw_hotel_list['data']['filter_result_count'];
	// 					if (intval($offset) == 0) {
	// 						//Need filters only if the data is being loaded first time
	// 						$filters = $this->hotel_lib->filter_summary($raw_hotel_list['data']);
	// 						$response['filters'] = $filters['data'];
	// 					}
	// 					$raw_hotel_list['data'] = $this->hotel_lib->get_page_data($raw_hotel_list['data'], $offset, $limit);

	// 					$attr['search_id'] = abs($search_params['search_id']);

	// 					$response['data'] = get_compressed_output(
	// 					$this->load->view('hotel/tbo/tbo_search_result',
	// 					array('currency_obj' => $currency_obj, 'raw_hotel_list' => $raw_hotel_list['data'],
	// 								'search_id' => $search_params['search_id'], 'booking_source' => $search_params['booking_source'],							
	// 								'attr' => $attr,
	// 								'search_params'=>$safe_search_data
	// 					),true));
	// 					$response['status'] = SUCCESS_STATUS;
	// 					$response['total_result_count'] = $source_result_count;
	// 					$response['filter_result_count'] = $filter_result_count;
	// 					$response['offset'] = $offset+$limit;
	// 				}
	// 				break;
	// 		}
	// 	}
	// 	$this->output_compressed_data($response);
	// } 

	/**
     * Load hotels from different source
     */
    function hotel_list($offset = 0) {
    	//error_reporting(E_ALL);
        $response ['data'] = '';
        $response ['msg'] = '';
        $response ['status'] = FAILURE_STATUS;
        $search_params = $this->input->get ();
        $limit = $this->config->item ( 'hotel_per_page_limit' );
        $safe_search_data = $this->hotel_model->get_safe_search_data($search_params ['search_id']);
         // debug($search_params);exit("hotel-search-905");
        $hotel_list2 = $hotel_list1 = $hotel_temp['total_result_count1']=$hotel_temp['filter_result_count1']=$hotel_temp['offset1']=$hotel_temp['total_result_count2']=$hotel_temp['filter_result_count2']=$hotel_temp['offset2']='';
        $filters1 = array();
        $star_rat = array();
        $check_crs_count = 0;
        $check_hotel_count = 0;
        $location_crs = array();

        // get price per nights
        $no_of_nights = 0;
        if($safe_search_data['status']){
            $no_of_nights = $safe_search_data['data']['no_of_nights'];
        }
        
        if ($search_params ['op'] == 'load' && intval ( $search_params ['search_id'] ) > 0 && isset ( $search_params ['source_id'] ) == true) {
            
            foreach ($search_params['source_id'] as $key => $value ){
                $booking_source_hotel =  $value;
                 // echo $booking_source_hotel;
                 // echo CRS_HOTEL_BOOKING_SOURCE;
                 // exit;
                load_hotel_lib ( $booking_source_hotel );

                switch ($booking_source_hotel) {
                    case PROVAB_HOTEL_BOOKING_SOURCE :
                        //Meaning hotels are loaded first time
                        $raw_hotel_list = $this->hotel_lib->get_hotel_list(abs($search_params['search_id']));
                        if ($raw_hotel_list['status']) {
                            //Converting API currency data to preferred currency
                            $currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
                            $raw_hotel_list = $this->hotel_lib->search_data_in_preferred_currency($raw_hotel_list, $currency_obj);
                            //Display 
                            $currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
                            //Update currency and filter summary appended
                            if (isset($search_params['filters']) == true and valid_array($search_params['filters']) == true) {
                                $filters = $search_params['filters'];
                            } else {
                                $filters = array();
                            }

                            $raw_hotel_list['data'] = $this->hotel_lib->format_search_response($raw_hotel_list['data'], $currency_obj, $search_params['search_id'], 'b2c', $filters);
                            $source_result_count = $raw_hotel_list['data']['source_result_count'];
                            $filter_result_count = $raw_hotel_list['data']['filter_result_count'];
                            $check_hotel_count = $source_result_count;
                            if (intval($offset) == 0) {
                                //Need filters only if the data is being loaded first time
                                $filters = $this->hotel_lib->filter_summary($raw_hotel_list['data']);
                                $response['filters'] = $filters['data'];
                            }

                            $raw_hotel_list['data'] = $this->hotel_lib->get_page_data($raw_hotel_list['data'], $offset, $limit);

                            $attr['search_id'] = abs($search_params['search_id']);
                            // debug($raw_hotel_list);exit;
                            $hotel_list1 = get_compressed_output($this->load->view('hotel/tbo/tbo_search_result',
                            array('currency_obj' => $currency_obj, 'raw_hotel_list' => $raw_hotel_list['data'],
                                        'search_id' => $search_params['search_id'], 'booking_source' => $booking_source_hotel,
                                        'attr' => $attr
                            ),true));
                            $response['status'] = SUCCESS_STATUS;
                            $hotel_temp['total_result_count1'] = $source_result_count;
                            $hotel_temp['filter_result_count1'] = $filter_result_count;
                            $hotel_temp['offset1'] = $offset+$limit;
                            
                        }
                        break;
                        case CRS_HOTEL_BOOKING_SOURCE :
                        $hotel_crs_markup = $this->private_management_model->get_markup('hotel');

                         //debug($hotel_crs_markup);exit('ex');
                        $raw_hotel_list = $this->hotel_lib->get_hotel_list(abs($search_params['search_id'])); 
                        // debug($raw_hotel_list);exit;
                        if ($raw_hotel_list['status']) {
                            $currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' =>get_application_currency_preference()));
                            // debug($currency_obj);exit;
                            
                            $api_request_result_count = 0;
                            /*if1 -starts*/
                            if($raw_hotel_list['data']->num_rows()>0){
                                $crs = $raw_hotel_list['data']->result_array();
                                //$filters_display = $this->hotel_lib->filter_summary ( $crs,$search_params ['search_id'] );
                                /*usort($myArray, function($a, $b) {
                                    return $a['hotel_name'] - $b['hotel_name'];
                                });*/
                                // debug($crs);exit;

                                $check_crs_count = count($crs);
                                if(!isset($key_index))
                                {
                                    $key_index = 0;
                                }
                                
                                /*foreach-1 starts*/
                                foreach($crs as $kk => $each_crs_hotels)
                                {
                                    //$hotel_count++; 
                                    
                                   $fetch_price = $this->hotels_model->gets_crs_topPrice_forSearch($each_crs_hotels['hotel_details_id']);
                                    if($fetch_price->num_rows()>=1)
                                    {   
                                         $key_index++; 

                                      // Added for Hotel and hotel CRS filter
                                      $star_rat[$each_crs_hotels['star_rating']] ['c'] = 1;
                                      $star_rat[$each_crs_hotels['star_rating']] ['v'] = $each_crs_hotels['star_rating'];
                                      $location_crs[] = empty ( $each_crs_hotels['city_details_id'] ) == true ? 'Others' : $each_crs_hotels['city_details_id'];

                                      //Added for Hotel and Hotel Code ended

                                      //Star Rating
                                      $stars[] =    $search_response[$key_index]['star_rating'] = ($each_crs_hotels['star_rating']=='' || $each_crs_hotels['star_rating']=='0') ? 'false' : $each_crs_hotels['star_rating'];  
                                         $category_array1   = array_unique($stars);
                                         foreach ($stars as $category_array11)
                                            $cat_array[] = $category_array11;
                                         //Star Rating End
                                         $fetch_price = $fetch_price->result_array()[0];

                                         $request = $this->hotel_lib->search_data ( abs ( $search_params ['search_id'] ) );

                                         $price_data = calculate_crs_roomPrice($fetch_price,$request,$crs[$kk],$each_crs_hotels['hotel_details_id'],$currency_obj); 
                                        
                                         $total_price = $price_data['adult_price'] + $price_data['child_price'];
                                         
                                         $original_price = $total_price;

                                         $admin_markup_hotelcrs = $this->domain_management_model->addHotelCrsMarkup($total_price, $hotel_crs_markup,$currency_obj);
                                       	 //debug($admin_markup_hotelcrs);die;
                                         $nationality = 'IN';
                                         //Markup
                                         //$admin_markup_details = get_Admin_Markup(1,$user_type,$user_id,$nationality);
                                         $admin_markup_details = '';
                                         $total_price = $total_price;
                                         $child_price_array = array();
                                         
                                         $adult_price_array = array();
                                       
                                          $days = array();
                                          
                                          $check = explode('/', $request['data']['from_date']); 
                                          $check = $check[2].'-'. $check[1].'-'.$check[0];
                                          $checkin_unix_time = strtotime($check);
                                         
                                          for($m=0;$m<=intval($request['data']['no_of_nights']) - 1;$m++)
                                              {
                                                if($m==0)
                                                {
                                                  $days[] = date('D',$checkin_unix_time);
                                                } else {
                                                  $days[] = date('D',strtotime('+'.$m.' day',$checkin_unix_time));
                                                }
                                              }
                                             
                                          
                                          $trace = json_decode($raw_hotel_list['search_request']['data']['request']);
                                          $TraceId = $trace->TokenId;
                                          // debug($trace);exit("555");

                                         $prices[] =  $search_response[$key_index]['total_price'] = $total_price;
                                         $search_response[$key_index]['admin_markup'] = $admin_markup_hotelcrs;
                                         $search_response[$key_index]['agent_markup'] = 0;
                                         $markup_prices[] = $total_price + $admin_markup_hotelcrs;
                                         //debug($markup_prices);die;
                                         $search_response[$key_index]['TraceId'] = $TraceId;
                                         $search_response[$key_index]['hotel_type_id'] = $each_crs_hotels['hotel_type_id'];
                                         $search_response[$key_index]['hotel_address'] = $each_crs_hotels['hotel_address'];
                                         $search_response[$key_index]['HotelCode'] = $each_crs_hotels['hotel_details_id'];
                                         $search_response[$key_index]['place'] = $request['data']['city_name'];
                                         $search_response[$key_index]['hotel_name'] = $each_crs_hotels['hotel_name'];
                                         $search_response[$key_index]['ResultIndex'] = $each_crs_hotels['hotel_details_id'];
                                         $search_response[$key_index]['description'] = $each_crs_hotels['hotel_info'];
                                         $search_response[$key_index]['source'] = 'crs';
                                         $search_response[$key_index]['request'] = $request;
                                         $search_response[$key_index]['api'] = CRS_HOTEL_BOOKING_SOURCE;
                                         $search_response[$key_index]['hotel_amenities'] = $each_crs_hotels['hotel_amenities'];
                                         $amenityIds = $each_crs_hotels['hotel_amenities'];
                                         $amenityIds_array = explode(',',$amenityIds);
                                         $am = array();
                                         // debug($search_response);exit("576");
                                         foreach ($amenityIds_array as $value) {
                                            $am[]= $this->hotels_model->get_hotel_amenities($value);
                                         }
                                        $search_response[$key_index]['hotel_amenities_name'] = $am;
                                        // debug($search_response);die("581");
                                        
                                         $search_response[$key_index]['latitude'] = $each_crs_hotels['latitude'];
                                         $search_response[$key_index]['longitude'] = $each_crs_hotels['longitude'];
                                         $search_response[$key_index]['hotel_code'] = $each_crs_hotels['hotel_code'];
                                         $search_response[$key_index]['price_breakup'] = (
                                                                array(  'weekdays_single_room_price' => $price_data['weekdays_single_room_price'],
                                                                        //'weekend_single_room_price' => $this->currencyconverter->convert('USD',$default_currency_array[0],$price_data['weekend_single_room_price']),
                                                                        'weekend_single_room_price' => $price_data['weekend_single_room_price'],
                                                                        'stay_days' => $price_data['stay_days'],
                                                                        'days' => $days,
                                                                        'child_price_array' => $child_price_array,
                                                                        'adult_price_array' => $adult_price_array,
                                                                        'weekdays_double_room_price' => $price_data['weekdays_double_room_price'],
                                                                        'weekend_double_room_price' => $price_data['weekend_double_room_price'],
                                                                        'weekdays_triple_room_price' => $price_data['weekdays_triple_room_price'],
                                                                        'weekend_triple_room_price' => $price_data['weekend_triple_room_price'],
                                                                        'weekdays_quad_room_price' => $price_data['weekdays_quad_room_price'],
                                                                        'weekend_quad_room_price' => $price_data['weekend_quad_room_price'],
                                                                         'weekdays_hex_room_price' => $price_data['weekdays_hex_room_price'],
                                                                        'weekend_hex_room_price' => $price_data['weekend_hex_room_price'],
                                                                        'require_weekend_price' => $price_data['require_weekend_price']
                                                                        )
                                                                                        );
                                         $search_response[$key_index]['source_code'] = '3';
                                         $search_response[$key_index]['unique_id'] = $unique_id = sha1(time().uniqid().'api'.'#%#sumeru');
                                       // debug($each_crs_hotels);exit("607");
                                         if($each_crs_hotels['thumb_image']==''){
                                     
                                            $pic = sizeof(fetch_crs_image($each_crs_hotels['hotel_images'])) > 0 ? fetch_crs_image($each_crs_hotels['hotel_images']) : 'false';
                                            $search_response[$key_index]['pic'] = $pic[0];
                                            if($pic=='false')
                                             {
                                                 $search_response[$key_index]['pic']= base_url().'assets/images/logo.png';
                                             }
                                         } else {
                                         
                                            $search_response[$key_index]['pic'] = ASSETS.'cpanel/uploads/hotel_images/'.urldecode($each_crs_hotels['thumb_image']);
                                         }
                                        
                                         $search_response[$key_index]['additional'] = serialize(array('hotel_id' => $each_crs_hotels['hotel_details_id'],'internal_code' => '0'));
                                         $search_response[$key_index]['sik'] = sha1(time().uniqid().'crs'.'#%#sumeru');
                     
                                         if(!isset($_SESSION))
                                         {

                                            session_start();
                                         } 
                                         // exit("2");
                                         $_SESSION[$search_response[$key_index]['sik']]['GET'] = @$GET;

                                         $_SESSION[$search_response[$key_index]['sik']]['markup_data'] = (@$markup==false) ? 'false' : json_encode($markup);
                                   }
                                   
                                } /*foreach-1 ends*/

                                $api_request_result_count++;
                            }/*if1 -ends*/
                           
                            $test = count($search_response); 
                            // debug($test);exit("640");
                            $unique_id = sha1(time().uniqid().'api'.'#%#oneclickk');
                            if($api_request_result_count==0){

                             if(isset($final_response['data']) && !is_null($final_response['data'])){
                                $data['final_response'] = $final_response = array( 'success' => 'false' , 'data' => $final_response['data']);
                             } else {
                                 $search_response = base_url()."assets/images/no_result.png";
                                 $count = 0;

                                 if ($test > 0) {
                                  $data['final_response']=  $final_response = array( 'success' => 'true' , 'data' => $search_response,'result_count' => sizeof($search_response),'request' => $request,'unique_id' => $unique_id);
                                 }else{
                                    $data['final_response'] = $final_response = array( 'success' => 'false' , 'data' => 'No search result found');
                                 }
                             }
                              
                        } 
                        else {
                            if(isset($search_params ['filters'])){
                                $filters_crs = $search_params ['filters'];
                                if(isset($filters_crs['sort_item']) && $filters_crs['sort_item'] == 'name' ){
                                    if($filters_crs['sort_type'] == 'asc'){
                                        sort_array_of_array($search_response,'hotel_name',SORT_ASC);
                                    }
                                    if($filters_crs['sort_type'] == 'desc'){
                                        sort_array_of_array($search_response,'hotel_name',SORT_DESC);
                                    }
                                }
                                if(isset($filters_crs['sort_item']) && $filters_crs['sort_item'] == 'star' ){
                                    if($filters_crs['sort_type'] == 'asc'){
                                        sort_array_of_array($search_response,'star_rating',SORT_ASC);
                                    }
                                    if($filters_crs['sort_type'] == 'desc'){
                                        sort_array_of_array($search_response,'star_rating',SORT_DESC);
                                    }
                                }
                                if(isset($filters_crs['sort_item']) && $filters_crs['sort_item'] == 'price' ){
                                    if($filters_crs['sort_type'] == 'asc'){
                                        sort_array_of_array($search_response,'total_price',SORT_ASC);
                                    }
                                    if($filters_crs['sort_type'] == 'desc'){
                                        sort_array_of_array($search_response,'total_price',SORT_DESC);
                                    }
                                }

                            }else{
                                sort_array_of_array($search_response,'total_price',SORT_ASC);
                            }
                            /*$max = max($prices);
                            $min = min($prices);*/
                            // debug($markup_prices);exit;
                            $max = (max($markup_prices));
                            $min = (min($markup_prices));

                            $filters1 = array("max"=>$max,"min"=>$min);

                            $data['final_response'] = $final_response = array( 'success' => 'true' , 'data' => $search_response,'result_count' => sizeof($search_response),'request' => $request,'unique_id' => $unique_id);                            
                            $attr ['search_id'] = abs ( $search_params ['search_id'] );
                            // debug($search_response);exit("asdsa");
                            $hotel_list2 =  get_compressed_output ( $this->load->view ( 'hotel/hotel_crs/crs_tbo_search_result', array (
                                    'currency_obj' => $currency_obj,
                                    'raw_hotel_list' => $search_response,
                                    'search_id' => $search_params ['search_id'],
                                    'booking_source' => $booking_source_hotel,
                                    'attr' => $attr,
                                    'view_type' => $search_params['view_type'],
                                    'no_of_nights' => $no_of_nights
                                    ),true ) );
                            $response ['status'] = SUCCESS_STATUS;
                            $hotel_temp ['total_result_count2'] = sizeof($search_response);
                            $hotel_temp ['filter_result_count2'] = sizeof($search_response);
                            $hotel_temp ['offset2'] = $offset + $limit;
                                //$response['filters']['p']=["max"=>$max,"min"=>$min];
                             }
                        }
                    break;
                    
                }
            }
            // Merging filters for both Hotel CRS and Hotel
                if($check_hotel_count > 0){
                    if($check_crs_count > 0){
                        if(isset($response['filters']['p'])){
                            $filter_new = array($response['filters']['p']['max'],$response['filters']['p']['min'],$filters1['max'],$filters1['min']);
                            //debug($filters);exit;
                            $response['filters']['p']['max'] = max($filter_new);
                            $response['filters']['p']['min'] = min($filter_new);

                            $response['filters']['star'] = $response['filters']['star'] + $star_rat;
                            //$response['filters']['loc']['Crs'] = ['c'=>$location_crs_count,'v'=>'Crs'];
                            $loc = array();
                            $loc_val = array_unique($location_crs);
                            for($ii = 0;$ii<count($loc_val);$ii++){
                                $loc_count = 0;
                                $location = $loc_val[$ii];
                                for($jj = 0;$jj<count($location_crs);$jj++){
                                    $location_val = $location_crs[$jj];
                                    if($location === $location_val){
                                        $loc_count++;
                                    }
                                }
                                $loc[$location] = array('c'=>$loc_count,'v'=>$location); 
                            }
                            $response['filters']['loc'] = $response['filters']['loc'] + $loc;
                        }else{

                            $filter_new = array($filters1['max'],$filters1['min']);
                            //debug($filters);exit;
                            $response['filters']['p']['max'] = max($filter_new);
                            $response['filters']['p']['min'] = min($filter_new);

                            $response['filters']['star'] = $star_rat;
                            //$response['filters']['loc']['Crs'] = ['c'=>$location_crs_count,'v'=>'Crs'];
                            $loc = array();
                            $loc_val = array_unique($location_crs);
                            for($ii = 0;$ii<count($loc_val);$ii++){
                                $loc_count = 0;
                                $location = $loc_val[$ii];
                                for($jj = 0;$jj<count($location_crs);$jj++){
                                    $location_val = $location_crs[$jj];
                                    if($location === $location_val){
                                        $loc_count++;
                                    }
                                }
                                $loc[$location] = array('c'=>$loc_count,'v'=>$location); 
                            }
                            $response['filters']['loc'] = $loc;

                        }
                    }

                }else{

                    if($check_crs_count > 0){
                        $filter_new = array($filters1['max'],$filters1['min']);
                        $response['filters']['p']['max'] = max($filter_new);
                        $response['filters']['p']['min'] = min($filter_new);

                        $response['filters']['star'] = $star_rat;
                        //$response['filters']['loc']['Crs'] = ['c'=>$location_crs_count,'v'=>'Crs'];
                        $loc = array();
                        $loc_val = array_unique($location_crs);
                        for($ii = 0;$ii<count($loc_val);$ii++){
                            $loc_count = 0;
                            $location = $loc_val[$ii];
                            for($jj = 0;$jj<count($location_crs);$jj++){
                                $location_val = $location_crs[$jj];
                                if($location === $location_val){
                                    $loc_count++;
                                }
                            }
                            $loc[$location] = array('c'=>$loc_count,'v'=>$location); 
                        }
                        $response['filters']['loc'] = $loc;
                    }

                }
                //Merging filters ends here

                $response['total_result_count'] = ($hotel_temp['total_result_count1']+$hotel_temp['total_result_count2']);
                $response['filter_result_count'] = ($hotel_temp['filter_result_count1']+$hotel_temp['filter_result_count2']);
                $response['offset'] = ($hotel_temp['offset1']+$hotel_temp['offset2']);

                $hotel_list = $hotel_list2.$hotel_list1;
               //  debug($hotel_list);exit("hotel-search-1311");
                $response['data'] = get_compressed_output($hotel_list);
        }
       // debug($response); exit("hotel-search-1314");
        $this->output_compressed_data ( $response );

    }


   
   function villa_list($offset = 0) {
        $response ['data'] = '';
        $response ['msg'] = '';
        $response ['status'] = FAILURE_STATUS;
        $search_params = $this->input->get ();
        $limit = $this->config->item ( 'hotel_per_page_limit' );
        $safe_search_data = $this->hotel_model->get_safe_search_data($search_params ['search_id']);
        // debug($search_params);exit;
        $hotel_list2 = $hotel_list1 = $hotel_temp['total_result_count1']=$hotel_temp['filter_result_count1']=$hotel_temp['offset1']=$hotel_temp['total_result_count2']=$hotel_temp['filter_result_count2']=$hotel_temp['offset2']='';
        $filters1 = array();
        $star_rat = array();
        $check_crs_count = 0;
        $check_hotel_count = 0;
        $location_crs = array();

        // get price per nights
        $no_of_nights = 0;
        if($safe_search_data['status']){
            $no_of_nights = $safe_search_data['data']['no_of_nights'];
        }
        
        if ($search_params ['op'] == 'load' && intval ( $search_params ['search_id'] ) > 0 && isset ( $search_params ['source_id'] ) == true) {
             //debug($search_params['source_id']);exit;
            foreach ($search_params['source_id'] as $key => $value ){
                $booking_source_hotel =  $value;
                 // ECHO $booking_source_hotel;
                 // echo CRS_HOTEL_BOOKING_SOURCE;
                 // EXIT;
                load_hotel_lib ( $booking_source_hotel );

                switch ($booking_source_hotel) {
                    
                        case CRS_HOTEL_BOOKING_SOURCE :
                        //echo "hi";exit();

                        //hotel_lib is hotels_expo.php
                        $hotel_crs_markup = $this->private_management_model->get_markup('hotel');
                         // debug($search_params['search_id']);die;
                        $raw_hotel_list = $this->hotel_lib->get_villa_list(abs($search_params['search_id'])); 
                        // debug($raw_hotel_list);exit;
                        if ($raw_hotel_list['status']) {
                            $currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' =>get_application_currency_preference()));
                            // debug($currency_obj);exit;
                            
                            $api_request_result_count = 0;
                            /*if1 -starts*/
                            if($raw_hotel_list['data']->num_rows()>0){
                                $crs = $raw_hotel_list['data']->result_array();
                                //$filters_display = $this->hotel_lib->filter_summary ( $crs,$search_params ['search_id'] );
                                /*usort($myArray, function($a, $b) {
                                    return $a['hotel_name'] - $b['hotel_name'];
                                });*/
                                //debug($crs);exit;

                                $check_crs_count = count($crs);
                                if(!isset($key_index))
                                {
                                    $key_index = 0;
                                }
                                
                                /*foreach-1 starts*/
                                foreach($crs as $kk => $each_crs_hotels)
                                {
                                    //$hotel_count++; 
                                    
                                   $fetch_price = $this->hotels_model->gets_crs_topPrice_forSearch($each_crs_hotels['hotel_details_id']);
                                    //debug($fetch_price);exit;
                                    if($fetch_price->num_rows()>=1)
                                    {   
                                         $key_index++; 

                                      // Added for Hotel and hotel CRS filter
                                      $star_rat[$each_crs_hotels['star_rating']] ['c'] = 1;
                                      $star_rat[$each_crs_hotels['star_rating']] ['v'] = $each_crs_hotels['star_rating'];
                                      $location_crs[] = empty ( $each_crs_hotels['city_details_id'] ) == true ? 'Others' : $each_crs_hotels['city_details_id'];

                                      //Added for Hotel and Hotel Code ended

                                      //Star Rating
                                      $stars[] =    $search_response[$key_index]['star_rating'] = ($each_crs_hotels['star_rating']=='' || $each_crs_hotels['star_rating']=='0') ? 'false' : $each_crs_hotels['star_rating'];  
                                         $category_array1   = array_unique($stars);
                                         foreach ($stars as $category_array11)
                                            $cat_array[] = $category_array11;
                                         //Star Rating End
                                         $fetch_price = $fetch_price->result_array()[0];
                                         $request = $this->hotel_lib->search_data ( abs ( $search_params ['search_id'] ) );

                                         $price_data = calculate_crs_roomPrice($fetch_price,$request,$crs[$kk],$each_crs_hotels['hotel_details_id'],$currency_obj); 
                                         //debug($price_data);exit;
                                        
                                         $total_price = $price_data['adult_price'] + $price_data['child_price'];
                                         
                                         $original_price = $total_price;

                                         $admin_markup_hotelcrs = $this->domain_management_model->addHotelCrsMarkup($total_price, $hotel_crs_markup,$currency_obj);
                                        // print_r($original_price); exit();
                                         $nationality = 'IN';
                                         //Markup
                                         //$admin_markup_details = get_Admin_Markup(1,$user_type,$user_id,$nationality);
                                         $admin_markup_details = '';
                                         $total_price = $total_price;
                                         $child_price_array = array();
                                         
                                         $adult_price_array = array();
                                       
                                          $days = array();
                                          
                                          $check = explode('/', $request['data']['from_date']); 
                                          $check = $check[2].'-'. $check[1].'-'.$check[0];
                                          $checkin_unix_time = strtotime($check);
                                         
                                          for($m=0;$m<=intval($request['data']['no_of_nights']) - 1;$m++)
                                              {
                                                if($m==0)
                                                {
                                                  $days[] = date('D',$checkin_unix_time);
                                                } else {
                                                  $days[] = date('D',strtotime('+'.$m.' day',$checkin_unix_time));
                                                }
                                              }
                                             
                                          
                                          $trace = json_decode($raw_hotel_list['search_request']['data']['request']);
                                          $TraceId = $trace->TokenId;
                                          

                                         $prices[] =  $search_response[$key_index]['total_price'] = $total_price;
                                         $search_response[$key_index]['admin_markup'] = $admin_markup_hotelcrs;
                                         $search_response[$key_index]['agent_markup'] = 0;
                                         $markup_prices[] = $total_price + $admin_markup_hotelcrs;
                                         $search_response[$key_index]['TraceId'] = $TraceId;
                                         $search_response[$key_index]['hotel_type_id'] = $each_crs_hotels['hotel_type_id'];
                                         $search_response[$key_index]['hotel_address'] = $each_crs_hotels['hotel_address'];
                                         $search_response[$key_index]['HotelCode'] = $each_crs_hotels['hotel_details_id'];
                                         $search_response[$key_index]['place'] = $request['data']['city_name'];
                                         $search_response[$key_index]['hotel_name'] = $each_crs_hotels['hotel_name'];
                                         $search_response[$key_index]['ResultIndex'] = $each_crs_hotels['hotel_details_id'];
                                         $search_response[$key_index]['description'] = $each_crs_hotels['hotel_info'];
                                         $search_response[$key_index]['source'] = 'crs';
                                         $search_response[$key_index]['request'] = $request;
                                         $search_response[$key_index]['api'] = CRS_HOTEL_BOOKING_SOURCE;
                                         $search_response[$key_index]['hotel_amenities'] = $each_crs_hotels['hotel_amenities'];
                                         $amenityIds = $each_crs_hotels['hotel_amenities'];
                                         $amenityIds_array = explode(',',$amenityIds);
                                         $am = array();
                                         foreach ($amenityIds_array as $value) {
                                            $am[]= $this->hotels_model->get_hotel_amenities($value);
                                         }
    $search_response[$key_index]['hotel_amenities_name'] = $am;
    // debug($price_data);die;

    $search_response[$key_index]['latitude'] = $each_crs_hotels['latitude'];
    $search_response[$key_index]['longitude'] = $each_crs_hotels['longitude'];
    $search_response[$key_index]['hotel_code'] = $each_crs_hotels['hotel_code'];
    $search_response[$key_index]['price_breakup'] = (
            array(  'weekdays_single_room_price' => $price_data['weekdays_single_room_price'],
                    //'weekend_single_room_price' => $this->currencyconverter->convert('USD',$default_currency_array[0],$price_data['weekend_single_room_price']),
                    'weekend_single_room_price' => $price_data['weekend_single_room_price'],
                    'stay_days' => $price_data['stay_days'],
                    'days' => $days,
                    'child_price_array' => $child_price_array,
                    'adult_price_array' => $adult_price_array,
                    'weekdays_double_room_price' => $price_data['weekdays_double_room_price'],
                    'weekend_double_room_price' => $price_data['weekend_double_room_price'],
                    'weekdays_triple_room_price' => $price_data['weekdays_triple_room_price'],
                    'weekend_triple_room_price' => $price_data['weekend_triple_room_price'],
                    'weekdays_quad_room_price' => $price_data['weekdays_quad_room_price'],
                    'weekend_quad_room_price' => $price_data['weekend_quad_room_price'],
                     'weekdays_hex_room_price' => $price_data['weekdays_hex_room_price'],
                    'weekend_hex_room_price' => $price_data['weekend_hex_room_price'],
                    'require_weekend_price' => $price_data['require_weekend_price']
                    )
                                    );
                     $search_response[$key_index]['source_code'] = '3';
                     $search_response[$key_index]['unique_id'] = $unique_id = sha1(time().uniqid().'api'.'#%#sumeru');
                   
                     if($each_crs_hotels['thumb_image']==''){
                        
                        $pic = sizeof(fetch_crs_image($each_crs_hotels['hotel_images'])) > 0 ? fetch_crs_image($each_crs_hotels['hotel_images']) : 'false';
                        $search_response[$key_index]['pic'] = $pic[0];
                        if($pic=='false')
                         {
                             $search_response[$key_index]['pic']= base_url().'assets/images/logo.png';
                         }
                     } else {
                        
                        
                        $search_response[$key_index]['pic'] = base_url().'cpanel/uploads/hotel_images/'.urldecode($each_crs_hotels['thumb_image']);
                     }
                    
                     $search_response[$key_index]['additional'] = serialize(array('hotel_id' => $each_crs_hotels['hotel_details_id'],'internal_code' => '0'));
                     $search_response[$key_index]['sik'] = sha1(time().uniqid().'crs'.'#%#sumeru');

                     if(!isset($_SESSION))
                     {
                        session_start();
                     } 
                     $_SESSION[$search_response[$key_index]['sik']]['GET'] = @$GET;

                     $_SESSION[$search_response[$key_index]['sik']]['markup_data'] = (@$markup==false) ? 'false' : json_encode($markup);
                }
               
            } /*foreach-1 ends*/

            $api_request_result_count++;
        }/*if1 -ends*/

                            $test = count($search_response); 
                            $unique_id = sha1(time().uniqid().'api'.'#%#oneclickk');
                            if($api_request_result_count==0){

                             if(isset($final_response['data']) && !is_null($final_response['data'])){
                                $data['final_response'] = $final_response = array( 'success' => 'false' , 'data' => $final_response['data']);
                             } else {
                                 $search_response = base_url()."assets/images/no_result.png";
                                 $count = 0;

                                 if ($test > 0) {
                                  $data['final_response']=  $final_response = array( 'success' => 'true' , 'data' => $search_response,'result_count' => sizeof($search_response),'request' => $request,'unique_id' => $unique_id);
                                 }else{
                                    $data['final_response'] = $final_response = array( 'success' => 'false' , 'data' => 'No search result found');
                                 }
                             }
                              
                        } 
                        else {
                            if(isset($search_params ['filters'])){
                                $filters_crs = $search_params ['filters'];
                                if(isset($filters_crs['sort_item']) && $filters_crs['sort_item'] == 'name' ){
                                    if($filters_crs['sort_type'] == 'asc'){
                                        sort_array_of_array($search_response,'hotel_name',SORT_ASC);
                                    }
                                    if($filters_crs['sort_type'] == 'desc'){
                                        sort_array_of_array($search_response,'hotel_name',SORT_DESC);
                                    }
                                }
                                if(isset($filters_crs['sort_item']) && $filters_crs['sort_item'] == 'star' ){
                                    if($filters_crs['sort_type'] == 'asc'){
                                        sort_array_of_array($search_response,'star_rating',SORT_ASC);
                                    }
                                    if($filters_crs['sort_type'] == 'desc'){
                                        sort_array_of_array($search_response,'star_rating',SORT_DESC);
                                    }
                                }
                                if(isset($filters_crs['sort_item']) && $filters_crs['sort_item'] == 'price' ){
                                    if($filters_crs['sort_type'] == 'asc'){
                                        sort_array_of_array($search_response,'total_price',SORT_ASC);
                                    }
                                    if($filters_crs['sort_type'] == 'desc'){
                                        sort_array_of_array($search_response,'total_price',SORT_DESC);
                                    }
                                }

                            }else{
                                sort_array_of_array($search_response,'total_price',SORT_ASC);
                            }
                            /*$max = max($prices);
                            $min = min($prices);*/
                            // debug($markup_prices);exit;
                            $max = (max($markup_prices));
                            $min = (min($markup_prices));

                            $filters1 = array("max"=>$max,"min"=>$min);

                            $data['final_response'] = $final_response = array( 'success' => 'true' , 'data' => $search_response,'result_count' => sizeof($search_response),'request' => $request,'unique_id' => $unique_id);                            
                            $attr ['search_id'] = abs ( $search_params ['search_id'] );
                            // debug($search_response);exit;
                            $hotel_list2 =  get_compressed_output ( $this->load->view ( 'hotel/hotel_crs/crs_tbo_search_result', array (
                                    'currency_obj' => $currency_obj,
                                    'raw_hotel_list' => $search_response,
                                    'search_id' => $search_params ['search_id'],
                                    'booking_source' => $booking_source_hotel,
                                    'attr' => $attr,
                                    'view_type' => $search_params['view_type'],
                                    'no_of_nights' => $no_of_nights
                                    ),true ) );
                            $response ['status'] = SUCCESS_STATUS;
                            $hotel_temp ['total_result_count2'] = sizeof($search_response);
                            $hotel_temp ['filter_result_count2'] = sizeof($search_response);
                            $hotel_temp ['offset2'] = $offset + $limit;
                                //$response['filters']['p']=["max"=>$max,"min"=>$min];
                             }
                        }
                    break;
                    
                }
            }
            // Merging filters for both Hotel CRS and Hotel
                if($check_hotel_count > 0){
                    if($check_crs_count > 0){
                        if(isset($response['filters']['p'])){
                            $filter_new = array($response['filters']['p']['max'],$response['filters']['p']['min'],$filters1['max'],$filters1['min']);
                            //debug($filters);exit;
                            $response['filters']['p']['max'] = max($filter_new);
                            $response['filters']['p']['min'] = min($filter_new);

                            $response['filters']['star'] = $response['filters']['star'] + $star_rat;
                            //$response['filters']['loc']['Crs'] = ['c'=>$location_crs_count,'v'=>'Crs'];
                            $loc = array();
                            $loc_val = array_unique($location_crs);
                            for($ii = 0;$ii<count($loc_val);$ii++){
                                $loc_count = 0;
                                $location = $loc_val[$ii];
                                for($jj = 0;$jj<count($location_crs);$jj++){
                                    $location_val = $location_crs[$jj];
                                    if($location === $location_val){
                                        $loc_count++;
                                    }
                                }
                                $loc[$location] = array('c'=>$loc_count,'v'=>$location); 
                            }
                            $response['filters']['loc'] = $response['filters']['loc'] + $loc;
                        }else{

                            $filter_new = array($filters1['max'],$filters1['min']);
                            //debug($filters);exit;
                            $response['filters']['p']['max'] = max($filter_new);
                            $response['filters']['p']['min'] = min($filter_new);

                            $response['filters']['star'] = $star_rat;
                            //$response['filters']['loc']['Crs'] = ['c'=>$location_crs_count,'v'=>'Crs'];
                            $loc = array();
                            $loc_val = array_unique($location_crs);
                            for($ii = 0;$ii<count($loc_val);$ii++){
                                $loc_count = 0;
                                $location = $loc_val[$ii];
                                for($jj = 0;$jj<count($location_crs);$jj++){
                                    $location_val = $location_crs[$jj];
                                    if($location === $location_val){
                                        $loc_count++;
                                    }
                                }
                                $loc[$location] = array('c'=>$loc_count,'v'=>$location); 
                            }
                            $response['filters']['loc'] = $loc;

                        }
                    }

                }else{

                    if($check_crs_count > 0){
                        $filter_new = array($filters1['max'],$filters1['min']);
                        $response['filters']['p']['max'] = max($filter_new);
                        $response['filters']['p']['min'] = min($filter_new);

                        $response['filters']['star'] = $star_rat;
                        //$response['filters']['loc']['Crs'] = ['c'=>$location_crs_count,'v'=>'Crs'];
                        $loc = array();
                        $loc_val = array_unique($location_crs);
                        for($ii = 0;$ii<count($loc_val);$ii++){
                            $loc_count = 0;
                            $location = $loc_val[$ii];
                            for($jj = 0;$jj<count($location_crs);$jj++){
                                $location_val = $location_crs[$jj];
                                if($location === $location_val){
                                    $loc_count++;
                                }
                            }
                            $loc[$location] = array('c'=>$loc_count,'v'=>$location); 
                        }
                        $response['filters']['loc'] = $loc;
                    }

                }
                //Merging filters ends here

                $response['total_result_count'] = $hotel_temp['total_result_count2'];
                $response['filter_result_count'] = $hotel_temp['filter_result_count2'];
                $response['offset'] = $hotel_temp['offset2'];

                $hotel_list = $hotel_list2;
                // debug($hotel_list);exit;
                $response['data'] = get_compressed_output($hotel_list);
        }
        //debug($response); exit();
        $this->output_compressed_data ( $response );

    }


	/**
	 * Compress and output data
	 * @param array $data
	 */
	private function output_compressed_data($data)
	{
		while (ob_get_level() > 0) { ob_end_clean() ; }
		ob_start("ob_gzhandler");
		header('Content-type:application/json');
		echo json_encode($data);
		ob_end_flush();
		exit;
	}

	/**
	 * Load hotels from different source
	 */
	function bus_list()
	{
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();
		if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
			load_bus_lib($search_params['booking_source']);
			switch($search_params['booking_source']) {
				case PROVAB_BUS_BOOKING_SOURCE :
					$raw_bus_list = $this->bus_lib->get_bus_list(abs($search_params['search_id']));

					if ($raw_bus_list['status']) {
						//Converting API currency data to preferred currency
						$currency_obj = new Currency(array('module_type' => 'bus','from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
						$raw_bus_list = $this->bus_lib->search_data_in_preferred_currency($raw_bus_list, $currency_obj);
						
						//Display Bus List
						$currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
						$response['data'] = get_compressed_output(
						$this->load->view('bus/travelyaari/travelyaari_search_result',
						array('currency_obj' => $currency_obj, 'raw_bus_list' => $raw_bus_list['data']['result'], 'search_id' => $search_params['search_id'], 'booking_source' => $search_params['booking_source']),true)
						);
						$response['status'] = SUCCESS_STATUS;
					}
					break;
			}
		}

		$this->output_compressed_data($response);
	}

	function get_bus_information()
	{
		$response['data'] = 'No Details Found';
		$response['status'] = false;
		//check params
		$params = $this->input->post();
		if (empty($params['booking_source']) == false and empty($params['search_id']) == false and intval($params['search_id']) > 0) {
			load_bus_lib($params['booking_source']);
			switch ($params['booking_source']) {
				case PROVAB_BUS_BOOKING_SOURCE :
					$currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
					$details = $this->bus_lib->get_bus_information($params['route_schedule_id'], $params['journey_date']);
					if ($details['status'] == SUCCESS_STATUS) {
						$response['stauts'] = SUCCESS_STATUS;
						$page_data['search_id'] = $params['search_id'];
						$page_data['details'] = @$details['data']['result'];
						$page_data['currency_obj'] = $currency_obj;
						$response['data'] = get_compressed_output($this->load->view('bus/travelyaari/travelyaari_bus_info', $page_data,true));
						$response['status'] = SUCCESS_STATUS;
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	/**
	 * Get Bus Booking List
	 */
	/**
	 * Get Bus Booking List
	 */
	function get_bus_details($filter_boarding_points=false)
	{
		// error_reporting(0);
		$this->load->model('bus_model');
		$response['data'] = 'No Details Found !! Try Later';
		$response['status'] = false;
		//check params
		$params = $this->input->post();
		$params = explode('*', $params['route_schedule_id']);
      
        $params['booking_source'] = $params[3];
        $params['search_id'] = $params[1];
        $params['route_schedule_id'] = $params[0];
        $params['route_code'] =  $params[2];
        $search_data = $this->bus_model->get_search_data($params['search_id']);
      
        $search_data = json_decode($search_data['search_data'], true);
          // debug($search_data);exit;
        $form_data = $this->bus_model->get_bus_station_data($search_data['from_station_id']);
        $to_data = $this->bus_model->get_bus_station_data($search_data['to_station_id']);
        
		if (empty($params['booking_source']) == false and empty($params['search_id']) == false and intval($params['search_id']) > 0) {
			load_bus_lib($params['booking_source']);
			switch ($params['booking_source']) {
				case PROVAB_BUS_BOOKING_SOURCE :
                    $bus_info_data = $this->bus_lib->get_route_details($params['search_id'], $params['route_schedule_id'], $params[2]);
                    $bus_info_data['bus_data']['Form_id'] = $form_data->station_id;
                    $bus_info_data['bus_data']['To_id'] = $to_data->station_id;
                    
                    $params['journey_date'] = $bus_info_data['bus_data']['DepartureTime'];
                    $params['ResultToken'] = $bus_info_data['bus_data']['ResultToken'];
					$details = $this->bus_lib->get_bus_details($params['route_schedule_id'], $params['journey_date'],$params['route_code'],$params['ResultToken'],$params['booking_source']);
					// debug($details);exit;
					if ($details['status'] == SUCCESS_STATUS) {
						//Converting API currency data to preferred currency
						$currency_obj = new Currency(array(
								'module_type' => 'bus',
								'from' => get_api_data_currency(), 
								'to' => get_application_currency_preference()
							));
						$details = $this->bus_lib->seatdetails_in_preferred_currency($details, $bus_info_data['bus_data'], $currency_obj);
						// debug($details);exit;
						//Display Bus Details
						$currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
						$page_data['search_id'] = $params['search_id'];
						$page_data['ResultToken'] = $params['ResultToken'];
						$this->bus_lib->update_seat_layout_commission($details['data']['result'], $currency_obj);
						$page_data['details'] = $details['data']['result'];
						$page_data['currency_obj'] = $currency_obj;

						// debug($page_data);exit;
  
						if ($filter_boarding_points == false) {
							$response['data'] = get_compressed_output($this->load->view('bus/travelyaari/travelyaari_bus_details', $page_data,true));
						} else {
							$response['data'] = get_compressed_output($this->load->view('bus/travelyaari/travelyaari_boarding_details', $page_data,true));
						}
						// debug($response);exit;
						$response['status'] = SUCCESS_STATUS;
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	    /**
     * Get Bus Booking List
     */
    // function get_bus_details($filter_boarding_points = false) {
    //     $this->load->model('bus_model');
    //     $response['data'] = 'No Details Found !! Try Later';
    //     $response['status'] = false;
    //     //check params
    //     $params = $this->input->post();
       
    //     $params = explode('*', $params['route_schedule_id']);
      
    //     $params['booking_source'] = $params[3];
    //     $params['search_id'] = $params[1];
    //     $params['route_schedule_id'] = $params[0];
    //     $params['route_code'] =  $params[2];
    //     $search_data = $this->bus_model->get_search_data($params['search_id']);
    //     $search_data = json_decode($search_data['search_data'], true);
    //     $form_data = $this->bus_model->get_bus_station_data($search_data['from_station_id']);
    //     $to_data = $this->bus_model->get_bus_station_data($search_data['to_station_id']);
    //     if (empty($params['booking_source']) == false and empty($params['search_id']) == false and intval($params['search_id']) > 0) {

    //         load_bus_lib($params['booking_source']);

    //         switch ($params['booking_source']) {
    //             case PROVAB_BUS_BOOKING_SOURCE :
    //                 $bus_info_data = $this->bus_lib->get_route_details($params['search_id'], $params['route_schedule_id'], $params[2]);
    //                 $bus_info_data['bus_data']['Form_id'] = $form_data->station_id;
    //                 $bus_info_data['bus_data']['To_id'] = $to_data->station_id;
                    
    //                 $params['journey_date'] = $bus_info_data['bus_data']['DepartureTime'];
    //                 $params['ResultToken'] = $bus_info_data['bus_data']['ResultToken'];
    //                 $details = $this->bus_lib->get_bus_details($params['route_schedule_id'], $params['journey_date'], $params['route_code'], $params['ResultToken'], $params['booking_source']);
                    
    //                 // debug($details);exit;
    //                 if ($details['status'] == SUCCESS_STATUS) {
    //                     //Converting API currency data to preferred currency
    //                     $currency_obj = new Currency(array(
    //                         'module_type' => 'bus',
    //                         'from' => get_api_data_currency(),
    //                         'to' => get_application_currency_preference()
    //                     ));
    //                     $details = $this->bus_lib->seatdetails_in_preferred_currency($details, $bus_info_data['bus_data'], $currency_obj);
    //                     // debug($details);exit;
    //                     //Display Bus Details
    //                     $currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
    //                     $response['stauts'] = SUCCESS_STATUS;
    //                     $page_data['search_id'] = $params['search_id'];
    //                     $page_data['ResultToken'] = $params['ResultToken'];
    //                     $page_data['details'] = $details['data']['result'];
    //                     $page_data['currency_obj'] = $currency_obj;
    //                     // debug($page_data);exit;
    //                     // debug($filter_boarding_points);exit;
    //                     if ($filter_boarding_points == false) {
    //                         $response['data'] = get_compressed_output($this->template->isolated_view('bus/travelyaari/travelyaari_bus_details', $page_data));
    //                     } else {
    //                         $response['data'] = get_compressed_output($this->template->isolated_view('bus/travelyaari/travelyaari_boarding_details', $page_data));
    //                     }
    //                     $response['status'] = SUCCESS_STATUS;
    //                 }
    //                 break;
    //         }
    //     }
    //     // debug($response);exit;
    //     $this->output_compressed_data($response);
    // }

	/**
	 * Load hotels from different source
	 */
	function get_room_details()
	{
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$params = $this->input->post();

		// debug($params);exit("1037");
		ini_set('memory_limit', '250M');
		if ($params['op'] == 'get_room_details' && intval($params['search_id']) > 0 && isset($params['booking_source']) == true) {
			$application_preferred_currency = get_application_currency_preference();
			$application_default_currency = get_application_currency_preference();
			load_hotel_lib($params['booking_source']);
			$this->hotel_lib->search_data($params['search_id']);
			$attr['search_id'] = intval($params['search_id']);
			switch($params['booking_source']) {
				case PROVAB_HOTEL_BOOKING_SOURCE :
					$raw_room_list = $this->hotel_lib->get_room_list(urldecode($params['ResultIndex']));
					$safe_search_data = $this->hotel_model->get_safe_search_data($params['search_id']);
					//debug($raw_room_list);exit;
					if ($raw_room_list['status']) {
						//Converting API currency data to preferred currency
						$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
						$raw_room_list = $this->hotel_lib->roomlist_in_preferred_currency($raw_room_list, $currency_obj);
						//Display
						$currency_obj = new Currency(array('module_type' => 'hotel','from' => $application_default_currency, 'to' => $application_preferred_currency));
						//debug($raw_room_list);exit;
						$response['data'] = get_compressed_output($this->load->view('hotel/tbo/tbo_room_list',
						array('currency_obj' => $currency_obj,
								'params' => $params, 'raw_room_list' => $raw_room_list['data'],
								'hotel_search_params'=>$safe_search_data['data'],
								'application_preferred_currency' => $application_preferred_currency,
								'application_default_currency' => $application_default_currency,
								'attr' => $attr
						),true
						) 
						);
						$response['status'] = SUCCESS_STATUS;
					}
					break;
					 case CRS_HOTEL_BOOKING_SOURCE :
                        $hotel_data = $this->hotels_model->get_crsHotels_byHotelId($params['HotelCode']);
                        // debug($hotel_data);exit;
                    $requ =  $this->hotel_lib->search_data ( $params ['search_id'] );
                     // debug($requ);
                     // exit("1075");
                     $checkin = explode('/', $requ['data']['from_date']);
                     $checkin = $checkin[2].'-'.$checkin[1].'-'.$checkin[0];
                     $checkout = explode('/', $requ['data']['to_date']);
                     $checkout = $checkout[2].'-'.$checkout[1].'-'.$checkout[0];
                    // Converting API currency data to preferred currency
                        $currency_obj = new Currency ( array (
                                'module_type' => 'hotel',
                                'from' => get_api_data_currency (),
                                'to' => get_application_currency_preference () 
                        ) );
                    
                    $rooms_required = intval($requ['data']['room_count']);
                        if($hotel_data->num_rows() > 0)
                        {//echo "asd";exit;
                $rooms_list = $this->hotels_model->get_crs_allRooms($params['HotelCode'],$requ); 

                if($rooms_list->num_rows()==0)
                {
                     $data['room_list'] = false;
                } else if($rooms_list->num_rows()>=1)
                {
                    $rooms_lists = array();
                   
                    foreach($rooms_list->result_array() as $key => $each_rooms)
                    { 
                      
                        //$hotel_rooms_count_info = $this->Hotel_Model->get_hotel_rooms_count_info($each_rooms['hotel_details_id'],$each_rooms['hotel_room_type_id'],$each_rooms['seasons_details_id']); //OLD Code
                        $hotel_rooms_count_info = $this->hotels_model->getAvailableRoomsHotel($checkin,$checkout,$each_rooms['hotel_room_type_id'],$each_rooms['seasons_details_id']);
                      
                       if($hotel_rooms_count_info->num_rows()>0)
                        {
                        //My Code 
                         $hotel_rooms_count = $this->hotels_model->getAvailableRooms($checkin,$checkout,$each_rooms['hotel_room_type_id'],$each_rooms['seasons_details_id']); 
                        
                         $room_booked = $hotel_rooms_count->rooms_booked_count;

                         $roombooked = $this->hotels_model->getBookedRooms($each_rooms['hotel_room_type_id'],$each_rooms['seasons_details_id']);
                         $numbers_of_room = $roombooked->no_of_room;

                       //End Of My Code
                        /*$hotel_rooms_count_info_a = $hotel_rooms_count_info->result_array()[0];
                        $room_booked = $hotel_rooms_count_info_a['no_of_room_booked'];
                        $numbers_of_room =  intval($hotel_rooms_count_info_a['no_of_room']);*/
                        $available_rooms = $numbers_of_room - $room_booked;
                        //echo '<pre>'; print_r($available_rooms); exit();
                        
                        if(is_null($room_booked) || $room_booked=='NULL')
                        {
                            $room_booked=0;
                        } else{
                            $room_booked=intval($room_booked);
                        }
                        $room_booked = is_numeric($room_booked) ? $room_booked : 0;
                        
                        if($each_rooms['season_type']=='3') // On requet
                        {
                            $rooms_available = true;
                        } else {
                             $rooms_available = ($rooms_required <= $available_rooms ) ? true : false;
                        }
                        
                        if($rooms_available)
                        {
                        $package_available= check_seasonPackage_availability($each_rooms,$requ);

                        if($package_available)
                        {

                      //  $stop_sale_data = $this->Hotel_Model->get_stop_sale_data($each_rooms['hotel_details_id'],$each_rooms['hotel_room_type_id']);
                       
                       // if($stop_sale_data->num_rows()==0)
                       // {
   
  
                            $price_data = $this->hotels_model->get_crs_topPrice_room($each_rooms['hotel_details_id'],$each_rooms['seasons_details_id'],$each_rooms['hotel_room_type_id']);
                           
                            $restrict_promotion_rate = false;
                            $pd = $price_data->result_array()[0];

                            $rate_type = $pd['rate_type'];
                            $hotel_room_rate_info_id = $price_data->result_array()[0]['hotel_room_rate_info_id'];
                        
                         if($price_data->num_rows()>0)
                         {
                            
                            
                        
                            foreach($each_rooms as $each_keys => $each_data)
                            {
                                $rooms_lists[$key][$each_keys] = $each_data;
                            }
                            //echo "<pre/>sanjay";print_r($each_rooms);exit;
                            $rooms_lists[$key]['price_data'] = ($price_data->num_rows==0) ? 'false' : $price_data->result_array()[0];
                            // print_r($rooms_list);exit;
                            $rooms_lists[$key]['price']      = is_array($rooms_lists[$key]['price_data']) ? calculate_crs_roomPrice($price_data->result_array()[0],$requ,$hotel_data->result_array()[0],$each_rooms['hotel_details_id'],$currency_obj) : 'false';
                            
                            $total_price_a                   = $rooms_lists[$key]['price']['child_price'] + $rooms_lists[$key]['price']['adult_price'];
                            
                            $nationality = 'IN';

                            $hotel_crs_markup = $this->private_management_model->get_markup('hotel');
                            // debug($hotel_crs_markup);exit;
                            $admin_markup_hotelcrs = $this->domain_management_model->addHotelCrsMarkup($total_price_a, $hotel_crs_markup,$currency_obj);

                            
                            // $rooms_lists[$key]['total_price'] = number_format($total_price_a, 2, '.', '');
                            $rooms_lists[$key]['total_price'] = $total_price_a;
                            $rooms_lists[$key]['admin_markup'] = $admin_markup_hotelcrs;
                            //number_format(floatval($rooms_lists[$key]['price']['child_price']) + floatval($rooms_lists[$key]['price']['adult_price']), 2, '.', '');
                            
                            $room_amenity = $this->hotels_model->get_hotel_room_details($each_rooms['hotel_details_id'],$each_rooms['hotel_room_type_id']);    
                           if($room_amenity->num_rows()>=1)
                            {
                                $room_amenity=$room_amenity->result_array()[0]['room_amenities'];
                                
                            } else {
                               $room_amenity = 'false'; 
                            }
                            $amenity = $room_amenity;

                            if(($room_amenity!='false'))
                               {
                                    if(strpos($amenity, ','))
                                    { 
                                                   
                                        $amenities = explode(',', $amenity);
                                        //echo '<pre>sanjay'; print_r($amenities); exit();
                                        foreach($amenities as $keys => $each_amenity)
                                        {
                                            if(is_numeric($each_amenity))
                                            {  
                                                $amenitiess = $this->hotels_model->get_hotel_amenities_byamenityId($each_amenity);
                                                
                                                if($amenitiess->num_rows()>=1)
                                                {
                                                    $am[$keys] = $amenitiess->result_array()[0];
                                                }

                                            }
                                        }
                                        $amenities = $am;
                                        //echo '<pre>sanjay'; print_r($amenities);
                                    } else {
                                        if(is_numeric($amenity))
                                        {
                                            //echo "<pre/>";print_r($amenity);exit;
                                            $amenitiess = $this->hotels_model->get_hotel_amenities_byamenityId($amenity);
                                            
                                            if($amenitiess->num_rows()>=1)
                                            {
                                                
                                                $amenities[0] = $amenitiess->result_array()[0];
                                            }
                                        }
                                    } 
                               } else {
                                    $amenities = 'false';
                               }
                            $rooms_lists[$key]['room_amenity'] = $amenities;
                            $rooms_lists[$key]['room_amenity_number'] = $room_amenity;
                        
                     } // end of price count
                     //} // end stop sale
                     } // end of pax combination and extra bed combination check 
                     } // count info room available count check 
                     } // count info num rows
                    }

                     sort_array_of_array($rooms_lists,'total_price');
                     $data['room_list'] = $rooms_lists;

                     $data['result'] = array('success' => 'true'); 

                }

                $source = '3';
                $response ['data'] = get_compressed_output ( $this->load->view ( 'hotel/hotel_crs/tbo_room_list_crs', array (
                                'currency_obj' => $currency_obj,
                                'params' => $params,
                                'raw_room_list' => $rooms_lists,
                                'application_preferred_currency' => $application_preferred_currency,
                                'application_default_currency' => $application_default_currency,
                                'hotel_code_id' => $hotel_code_id,
                                'requests'      => $requ,
                                'room_id' => $room_id,
                                'source' => $source,
                                'attr' => $attr,
                                'check_in' => $checkin,
                                'check_out' => $checkout
                        ),true ));
                 
                $response ['status'] = SUCCESS_STATUS;

           }

                    break;


			}
		}
		// debug($response);
  //                exit("1267");
		$this->output_compressed_data($response);
	}
	
	private function module_type() {
		$user_type=$this->session->userdata('user_type');
		if($user_type== Agent)
		{
			$module = 'b2b';
		}
		elseif ($user_type== Sub_Agent) {
			$module = 'b2b2b';
		}
		else
		{
			$module = 'b2c';
		}
		
		return $module;
	}
	/**
	 * Load Flight from different source
	 */
	function flight_list($search_id='')
	{
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();
		$page_params['search_id'] = $search_params['search_id'];
		if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
			load_flight_lib($search_params['booking_source']);

             $currency_obj = new Currency(array('module_type' => 'flight','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
			$raw_flight_list = $this->flight_lib->get_flight_list(abs($search_params['search_id']),$currency_obj);

			// debug($raw_flight_list);exit;

		if ($raw_flight_list['status']) {
			$page_params = array(
				'search_id' => $search_params['search_id'],
				'booking_source' => $search_params['booking_source'],
				'trip_type' => $this->flight_lib->master_search_data['trip_type'],
			);

			switch($search_params['booking_source']) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					$raw_search_result = $raw_flight_list['data']['Search']['FlightDataList'];
					
					$raw_search_result = $this->flight_lib->search_data_in_preferred_currency($raw_search_result, $currency_obj); 
					        
					$currency_obj = new Currency(array('module_type' => 'flight','from' => get_application_currency_preference(), 'to' => get_application_currency_preference())); 
					
					$formatted_search_data = $this->flight_lib->format_search_response($raw_search_result, $currency_obj, $search_params['search_id'], $this->user_type);
					$page_params['flight_section'] = "Travelomatix_API";
					$page_params['booking_url'] = $formatted_search_data['booking_url'];
					$raw_flight_list['data'] = $formatted_search_data['data'];
					$route_count = count($raw_flight_list['data']['Flights']);
					$domestic_round_way_flight = $raw_flight_list['data']['JourneySummary']['IsDomesticRoundway'];
					break;

				case TRAVELPORT_FLIGHT_BOOKING_SOURCE :
					//$raw_flight_list = $this->flight_lib->search_data_in_preferred_currency_TP($raw_flight_list['data'], $currency_obj);
					//debug($raw_flight_list['data']['Flights']);die("TRAVELPORT");
					$route_count = count($raw_flight_list['data']['Flights']);
					$domestic_round_way_flight = $raw_flight_list['data']['JourneySummary']['IsDomesticRoundway'];

					$page_params['travelport_s'] = "Travelport";
					$page_params['flight_section'] = "Travelport_API";
					$page_params['booking_url'] = @$raw_flight_list['booking_url'];
						
					//echo "<pre> TP search formatted_search_data";debug($raw_flight_list['data']);die;

					break; 
				case SABRE_FLIGHT_BOOKING_SOURCE :
					//debug($raw_flight_list);exit();
					//$raw_flight_list = $this->flight_lib->search_data_in_preferred_currency_TP($raw_flight_list['data'], $currency_obj);
					//debug($raw_flight_list);exit();
					$route_count = count($raw_flight_list['data']['Flights']);
					$domestic_round_way_flight = $raw_flight_list['data']['JourneySummary']['IsDomesticRoundway'];
					$page_params['travelport_s'] = "Sabre";
					$page_params['flight_section'] = "S*";
					$page_params['booking_url'] = @$raw_flight_list['booking_url'];
					break; 
			}
						/*if (($route_count > 0  && $domestic_round_way_flight == false) || ($route_count == 2 && $domestic_round_way_flight == true)) {
							$attr['search_id'] = abs($search_params['search_id']);
							$page_params = array(
							'raw_flight_list' => $raw_flight_list['data'],
							'search_id' => $search_params['search_id'],
							'booking_url' => $formatted_search_data['booking_url'],
							'booking_source' => $search_params['booking_source'],
							'trip_type' => $this->flight_lib->master_search_data['trip_type'],
							'attr' => $attr,
							'route_count' => $route_count,
							'IsDomestic' => $raw_flight_list['data']['JourneySummary']['IsDomestic']
							); 
							$page_params['domestic_round_way_flight'] = $domestic_round_way_flight;*/
							$attr['search_id'] = abs($search_params['search_id']);
				
							$page_params['route_count'] =	 $route_count;
							$page_params['IsDomestic'] = $raw_flight_list['data']['JourneySummary']['IsDomestic']; 
							$page_params['domestic_round_way_flight'] = @$domestic_round_way_flight;
							
							$page_params['twi_col_bs_val'] = $search_params['booking_source'];
							$twi_col_val_new = 1; // added for sub div section class
							$twi_col_val = 1; 

							foreach ($raw_flight_list['data']['Flights'] as $__key => $__jh_flight_result)
							{
								$page_params['raw_flight_list']['JourneySummary'] = $raw_flight_list['data']['JourneySummary'];
								$page_params['raw_flight_list']['Flights'][0] = force_multple_data_format($__jh_flight_result);
								$page_params['__root_indicator'] = $twi_col_val;
								$page_params['rotine'] = $twi_col_val_new++; 
								//$page_view_data = $this->load->view('flight/tbo/tbo_col2x_search_result', $page_params, true);
								//debug($page_params);die('new 1');
								$data_view_loader ['t-w-i-' . $twi_col_val++] = get_compressed_output($this->load->view('flight/tbo/tbo_col2x_search_result', $page_params, true));
							}							
			
							//$response['data'] = get_compressed_output($page_view_data);
							$response ['data'] ['col_x'] = ($data_view_loader);
							$response['status'] = SUCCESS_STATUS;
					
			} 
		}
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->output_compressed_data($response); 
	} 

	/**
	 * Get Data For Fare Calendar
	 * @param string $booking_source
	 */
	function puls_minus_days_fare_list($booking_source)
	{
		$response['data'] = array();
		$response['status'] = FAILURE_STATUS;

		$params = $this->input->get();
		load_flight_lib($booking_source);
		$search_data = $this->flight_lib->search_data(intval($params['search_id']));
		if ($search_data['status'] == SUCCESS_STATUS) {
			$date_array = array();
			$departure_date = $search_data['data']['depature'];
			$departure_date = strtotime(subtract_days_from_date(3, $departure_date));
			if (time() >= $departure_date) {
				$date_array[] = date('Y-m-d', strtotime(add_days_to_date(1)));
			} else {
				$date_array[] = date('Y-m-d', $departure_date);
			}
			$date_array[] = date('Y-m', strtotime($departure_date[0].' +1 month')).'-1';
			//Get Current Month And Next Month
			$day_fare_list = array();
			foreach ($date_array as $k => $v) {
				$search_data['data']['depature'] = $v;
				@$search = $this->flight_lib->calendar_safe_search_data($search_data['data']);
				if (valid_array($search) == true) {
					switch($booking_source) {
						case PROVAB_FLIGHT_BOOKING_SOURCE :
							$raw_fare_list = $this->flight_lib->get_fare_list($search);
							if ($raw_fare_list['status']) {
								$fare_calendar_list = $this->flight_lib->format_cheap_fare_list($raw_fare_list['data']);
								if ($fare_calendar_list['status'] == SUCCESS_STATUS) {
									$response['data']['departure'] = $search['depature'];
									$calendar_events = $this->get_fare_calendar_events($fare_calendar_list['data'], $raw_fare_list['data']['TraceId']);
									$day_fare_list = array_merge($day_fare_list, $calendar_events);
									$response['status'] = SUCCESS_STATUS;
								} else {
									$response['msg'] = 'Not Available!!! Please Try Later!!!!';
								}
							}
							break;
					}
				}
			}
			$response['data']['day_fare_list'] = $day_fare_list;
		}
		$this->output_compressed_data($response);
	}

	/**
	 * get fare list for calendar search - FLIGHT
	 */
	function fare_list($booking_source)
	{
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();
		load_flight_lib($booking_source);
		$search_params = $this->flight_lib->calendar_safe_search_data($search_params);
		if (valid_array($search_params) == true) {
			switch($booking_source) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					$raw_fare_list = $this->flight_lib->get_fare_list($search_params);
					if ($raw_fare_list['status']) {
						$fare_calendar_list = $this->flight_lib->format_cheap_fare_list($raw_fare_list['data']);
						if ($fare_calendar_list['status'] == SUCCESS_STATUS) {
							$response['data']['departure'] = $search_params['depature'];
							$calendar_events = $this->get_fare_calendar_events($fare_calendar_list['data'], $raw_fare_list['data']['TraceId']);
							$response['data']['day_fare_list'] = $calendar_events;
							$response['status'] = SUCCESS_STATUS;
						} else {
							$response['msg'] = 'Not Available!!! Please Try Later!!!!';
						}
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	/**
	 * Calendar Event Object
	 * @param $title
	 * @param $start
	 * @param $tip
	 * @param $href
	 * @param $event_date
	 * @param $session_id
	 * @param $add_class
	 */
	private function get_calendar_event_obj($title='', $start = '', $tip = '',$add_class = '', $href = '', $event_date = '', $session_id = '', $data_id='')
	{
		$event_obj = array();
		if (empty($data_id) == false) {
			$event_obj['data_id'] = $data_id;
		} else {
			$event_obj['data_id'] = '';
		}

		if (empty($title) == false) {
			$event_obj['title'] = $title;
		} else {
			$event_obj['title'] = '';
		}
		//start
		if (empty($start) == false) {
			$event_obj['start'] = $start;
			$event_obj['start_label'] = date('M d', strtotime($start));
		} else {
			$event_obj['start'] = '';
		}
		//tip
		if (empty($tip) == false) {
			$event_obj['tip'] = $tip;
		} else {
			$event_obj['tip'] = '';
		}
		//href
		if (empty($href) == false) {
			$event_obj['href'] = $href;
		} else {
			$event_obj['href'] = '';
		}
		//event_date
		if (empty($event_date) == false) {
			$event_obj['event_date'] = $event_date;
		}
		//session_id
		if (empty($session_id) == false) {
			$event_obj['session_id'] = $session_id;
		}
		//add_class
		if (empty($add_class) == false) {
			$event_obj['add_class'] = $add_class;
		} else {
			$event_obj['add_class'] = '';
		}
		return $event_obj;
	}

	function day_fare_list($booking_source)
	{
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();
		load_flight_lib($booking_source);
		$safe_search_params = $this->flight_lib->calendar_day_fare_safe_search_data($search_params);
		if ($safe_search_params['status'] == SUCCESS_STATUS) {
			switch($booking_source) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					$raw_day_fare_list = $this->flight_lib->get_day_fare($search_params);
					if ($raw_day_fare_list['status']) {
						$fare_calendar_list = $this->flight_lib->format_day_fare_list($raw_day_fare_list['data']);
						if ($fare_calendar_list['status'] == SUCCESS_STATUS) {
							$calendar_events = $this->get_fare_calendar_events($fare_calendar_list['data'], '');
							$response['data']['day_fare_list'] = $calendar_events;
							$response['data']['departure'] = $search_params['depature'];
							$response['status'] = SUCCESS_STATUS;
						} else {
							$response['msg'] = 'Not Available!!! Please Try Later!!!!';
						}
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	private function get_fare_calendar_events($events, $session_id='')
	{
		$currency_obj = new Currency(array('module_type' => 'flight','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		$index = 0;
		$calendar_events = array();
		foreach ($events as $k => $day_fare) {
			if (valid_array($day_fare) == true) {
				$fare_object = array('BaseFare' => $day_fare['BaseFare']);
				$BaseFare = $this->flight_lib->update_markup_currency($fare_object, $currency_obj);
				$tax = $currency_obj->get_currency($day_fare['tax'], false);
				$day_fare['price'] = floor($BaseFare['BaseFare']+$tax['default_value']);
				$event_obj = $this->get_calendar_event_obj(
				$currency_obj->get_currency_symbol(get_application_currency_preference()).' '.$day_fare['price'],
				$k, $day_fare['airline_name'].'-'.$day_fare['airline_code'], 'search-day-fare', '', $day_fare['departure'], '',
				$day_fare['airline_code']);
				$calendar_events[$index] = $event_obj;
			} else {
				$event_obj = $this->get_calendar_event_obj('Update', $k, 'Current Cheapest Fare Not Available. Click To Get Latest Fare.' ,
				'update-day-fare', '', $k, $session_id, '');
				$calendar_events[$index] = $event_obj;
			}
			$index++;
		}
		return $calendar_events;
	}




	/**
	 * Get Fare Details
	 */

	function get_fare_details_new()
	{
		//debug($_REQUEST); DIE;
		$response['status'] = false;
		$response['data'] = '';
		$response['msg'] = '<i class="fa fa-warning text-danger"></i> Fare Details Not Available';
		$params = $this->input->post();

		load_flight_lib($params['booking_source']);
		$data_access_key = $params['data_access_key'];
		$data_access['data_access_key'] =$params['data_access_key'];

		$params['data_access_key'] = unserialized_data($params['data_access_key']);

		if(empty($params['data_access_key']) == false || empty($params['data_access_key']) ==true)
		{
			switch($params['booking_source']) 
			{
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					$params['data_access_key'] = $this->flight_lib->read_token($data_access_key);
					$data_cat['data_cat'] = 'TBO';
				break;

				case TRAVELPORT_FLIGHT_BOOKING_SOURCE :
					$booking_source='PTBSID0000000016';
					$data_cat['data_cat'] = 'TP';
				break;

				case SABRE_FLIGHT_BOOKING_SOURCE :
					$booking_source='PTBSID0000000017';
					$data_cat['data_cat'] = 'SABRE';
				break;
			} 

			//echo "ddd ";debug($params); DIE;
			$data = $this->flight_lib->get_fare_details($params);
			//debug($data_cat['data_cat']);exit();
			if ($data['status'] == SUCCESS_STATUS) 
			{
				if($params['booking_source'] == TRAVELPORT_FLIGHT_BOOKING_SOURCE){
					
					$response['status']	= SUCCESS_STATUS;
					
					$response['data']	= $this->load->view('flight/travelport/fare_details_travelport', array('fare_rules' => $data['data']), true);
					$response['msg']	= 'Fare Details Available';
				}
				else if($params['booking_source'] != SABRE_FLIGHT_BOOKING_SOURCE){
					$response['status']	= SUCCESS_STATUS;
					$response['data']	= $this->load->view('flight/tbo/fare_details', array('fare_rules' => $data['data'],'fare_rules_category' => $data_cat['data_cat']));
					$response['msg']	= 'Fare Details Available';
				}else{
					$response['status']	= SUCCESS_STATUS;
					//debug($data);exit();
					$Rdata['AirFareRules'] 	= $data['resdata'];
					$response['data']	= $this->load->view('flight/tbo/air_rules_view
						',$Rdata);
					$response['msg']	= 'Fare Details Available';
				}
				//debug($response['data']);exit();
			}
		}
		$this->output_compressed_data($response);
	}
	function get_fare_details()
	{
		$response['status'] = false;
		$response['data'] = '';
		$response['msg'] = '<i class="fa fa-warning text-danger"></i> Fare Details Not Available';
		$params = $this->input->post();

		load_flight_lib($params['booking_source']);
		$data_access_key = $params['data_access_key'];
		$params['data_access_key'] = unserialized_data($params['data_access_key']);
		if (empty($params['data_access_key']) == false) {
			switch($params['booking_source']) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
				   // debug($data_access_key);  
					$params['data_access_key'] = $this->flight_lib->read_token($data_access_key);
					//debug($params['data_access_key']);die(" SERVER");  
					$data = $this->flight_lib->get_fare_details($params['data_access_key'], $params['search_access_key']); 
					//debug($data);die(" SERVER");    
					if ($data['status'] == SUCCESS_STATUS) {
						$response['status']	= SUCCESS_STATUS;
						$response['data']	= $this->load->view('flight/tbo/fare_details', array('fare_rules' => $data['data']), true);
						$response['msg']	= 'Fare Details Available';
					}
				break;
				case TRAVELPORT_FLIGHT_BOOKING_SOURCE :

				if(empty($params['Fareinfo']) == false || empty($params['air_fare_key']) ==false) {		
							$booking_source = TRAVELPORT_FLIGHT_BOOKING_SOURCE;	
							load_flight_lib($booking_source);
							//$params['data_access_key'] = $this->flight_lib->read_token($data_access_key);
							#echo "www ";debug($params);exit;
							$data = $this->flight_lib->get_fare_details($params);
							//debug($data);exit;
							if ($data['status'] == SUCCESS_STATUS) {
								$response['status']	= SUCCESS_STATUS;
								$response['data']	= $this->load->view('flight/travelport/fare_details_travelport', array('fare_rules' => $data['data']), true);
								$response['msg']	= 'Fare Details Available';
							}		
				}
				break;

				case SABRE_FLIGHT_BOOKING_SOURCE :

				$booking_source = SABRE_FLIGHT_BOOKING_SOURCE;	
				load_flight_lib($booking_source);
				$data = $this->flight_lib->get_fare_details($params);
				if ($data['status'] == SUCCESS_STATUS) {
					$Rdata['AirFareRules'] 	= $data['resdata'];
					$response['status']	= SUCCESS_STATUS;
					$response['data']	= $this->load->view('flight/tbo/air_rules_view',$Rdata,true);
					$response['msg']	= 'Fare Details Available';
				}
  				break;  
			}
		}
		$this->output_compressed_data($response);
	}

	function get_combined_booking_from()
	{
		$response['status']	= FAILURE_STATUS;
		$response['data']	= array();
		$params = $this->input->post();
		if (empty($params['search_id']) == false && empty($params['trip_way_1']) == false && empty($params['trip_way_2']) == false) {
			$tmp_trip_way_1	= json_decode($params['trip_way_1'], true);
			$tmp_trip_way_2	= json_decode($params['trip_way_2'], true);
			// echo '<pre>';print_r($tmp_trip_way_1);exit;
			$search_id	= $params['search_id'];
			foreach($tmp_trip_way_1 as $___v) {
				$trip_way_1[$___v['name']] = $___v['value'];
			}
			foreach($tmp_trip_way_2 as $___v) {
				$trip_way_2[$___v['name']] = $___v['value'];
			}
			$booking_source = $trip_way_1['booking_source'];
			// echo $booking_source;exit;
			switch($booking_source) {
				case PROVAB_FLIGHT_BOOKING_SOURCE : load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				$response['data']['booking_url']	= $this->flight_lib->booking_url(intval($params['search_id']));
				$response['data']['form_content']	= $this->flight_lib->get_form_content($trip_way_1, $trip_way_2);
				$response['status']					= SUCCESS_STATUS;
				break;
			}
		}
		$this->output_compressed_data($response);
	}
	/**
	 * Jaganath
	 * Get Traveller Details in Booking Page
	 */
	function user_traveller_details()
	{
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim($term);
		$result = array();
		$this->load->model('user_model');
		$traveller_details = $this->user_model->user_traveller_details($term)->result();
		$travllers_data = array();
		foreach($traveller_details as $traveller){
			$travllers_data['category'] = 'Travellers';
			$travllers_data['id'] = $traveller->origin;
			$travllers_data['label'] = trim($traveller->first_name.' '.$traveller->last_name);
			$travllers_data['value'] = trim($traveller->first_name);
			$travllers_data['first_name'] = trim($traveller->first_name);
			$travllers_data['last_name'] = trim($traveller->last_name);
			$travllers_data['date_of_birth'] = date('Y-m-d', strtotime(trim($traveller->date_of_birth)));
			$travllers_data['email'] = trim($traveller->email);
			$travllers_data['passport_user_name'] = trim($traveller->passport_user_name);
			$travllers_data['passport_nationality'] = trim($traveller->passport_nationality);
			$travllers_data['passport_expiry_day'] = trim($traveller->passport_expiry_day);
			$travllers_data['passport_expiry_month'] = trim($traveller->passport_expiry_month);
			$travllers_data['passport_expiry_year'] = trim($traveller->passport_expiry_year);
			$travllers_data['passport_number'] = trim($traveller->passport_number);
			$travllers_data['passport_issuing_country'] = trim($traveller->passport_issuing_country);
			array_push($result,$travllers_data);
		}
		$this->output_compressed_data($result);
	}
	/**
	 *
	 */
	function log_event_ip_info($eid)
	{
		$params = $this->input->post();
		if (empty($eid) == false) {
			$this->custom_db->update_record('exception_logger', array('client_info' => serialize($params)), array('exception_id' => $eid));
		}
	}
	// crs functions
	function get_crs_airport_code_list()
	{
		$this->load->model('flight_model');
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$result = array();

		$__airports = $this->flight_model->get_crs_airport_list($term)->result();
		if (valid_array($__airports) == false) {
			$__airports = $this->flight_model->get_crs_airport_list('')->result();
		}
		$airports = array();
		foreach($__airports as $airport){
			$airports['label'] = $airport->airport_city.', '.$airport->country.' ('.$airport->airport_code.')';
			$airports['value'] = $airport->airport_city.' ('.$airport->airport_code.')';
			$airports['id'] = $airport->origin;
			if (empty($airport->top_destination) == false) {
				$airports['category'] = 'Top cities';
				$airports['type'] = 'Top cities';
			} else {
				$airports['category'] = 'Search Results';
				$airports['type'] = 'Search Results';
			}

			array_push($result,$airports);
		}
		$this->output_compressed_data($result);
	}


	function flight_crs_list($search_id='')
	{
		//echo $search_id; die;
		error_reporting(0);
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get(); 
		$page_params['search_id'] = $search_params['search_id'];
		if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
		
		    $this->load->model ( 'flight_model' );
		 
		    $clean_search_details = $this->flight_model->get_safe_search_data($search_params['search_id']);
		    $from_loc=$clean_search_details['data']['from'];
		    $to_loc=$clean_search_details['data']['to'];
		    $depature=$clean_search_details['data']['depature'];
		    $depature=date("d-m-Y",strtotime($depature));
		    $trip_type=$clean_search_details['data']['trip_type'];
		    if($trip_type=='oneway')
			{
				$raw_flight_list=$this->flight_model->get_crs_oneflight($from_loc, $to_loc,$depature);	
				//$page_params['flights'] =$raw_flight_list;
				$page_params['flights'] =$raw_flight_list;
				$page_params['search_params']=$clean_search_details;
				$page_params['search_id']=$search_params['search_id'];
				$page_params['booking_source']=CRS_FLIGHT_BOOKING_SOURCE;

			}
			if($trip_type=='circle')
			{
				
				$return_to=$clean_search_details['data']['return'];
				$return_to=date("d-m-Y",strtotime($return_to));
				$raw_flight_list=$this->flight_model->get_crs_circleflight($from_loc, $to_loc,$depature,$return_to);
				$page_params['search_params']=$clean_search_details;
				$page_params['search_id']=$search_params['search_id'];
				$page_params['booking_source']=CRS_FLIGHT_BOOKING_SOURCE;
				$page_params['flights'] = $raw_flight_list;
			
			} 
		    
			$page_view_data = $this->load->view('flight/tbo/filght_crs_list', $page_params, true);
			//$page_view_data = $this->template->isolated_view('flight/tbo/filght_crs_list', $page_params);
			$response['data'] = get_compressed_output($page_view_data);
			$response['status'] = SUCCESS_STATUS;
			
		}
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->output_compressed_data($response);
	 }
	function set_flight_search_session_expiry($from_cache = false, $search_hash){

		$response = array();

		if($from_cache == false){

			$GLOBALS['CI']->session->set_userdata(array($search_hash => date("Y-m-d H:i:s")));
			$response['session_start_time'] = $GLOBALS ['CI']->config->item ('flight_search_session_expiry_period');

		}else{

			$start_time = $GLOBALS['CI']->session->userdata($search_hash);
			$current_time = date("Y-m-d H:i:s");
			$diff = strtotime($current_time) - strtotime($start_time);
			$response['session_start_time'] = $GLOBALS ['CI']->config->item ('flight_search_session_expiry_period') - $diff;
		}

		$response['search_hash'] = $search_hash;

		return $response;

	}

	/*
	*
	CODE ADDED ON 27/12/2017 
	*
	*/
	/**
	* Get Hotel Images by HotelCode
	*/
	function get_hotel_images(){
		$post_params = $this->input->post();
		if($post_params['hotel_code']){
			//debug($post_params['hotel_code']);exit;
			switch ($post_params['booking_source']) {

				case PROVAB_HOTEL_BOOKING_SOURCE:
					load_hotel_lib($post_params['booking_source']);
					$raw_hotel_images = $this->hotel_lib->get_hotel_images($post_params['hotel_code']);	
					
					if($raw_hotel_images['status']==true){
							$response['data'] = get_compressed_output(
							$this->load->view('hotel/tbo/tbo_hotel_images',
							array('hotel_images'=>$raw_hotel_images,'HotelCode'=>$post_params['hotel_code']
							),true));
					}
					
					break;
			}
			 $this->output_compressed_data($response);
		
		}
		exit;
	}

		/**
	*Load hotels for map
	*/
	function get_all_hotel_list(){
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();		
		$limit = $this->config->item('hotel_per_page_limit');
		if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
			load_hotel_lib($search_params['booking_source']);
			switch($search_params['booking_source']) {
				case PROVAB_HOTEL_BOOKING_SOURCE :
					//getting search params from table
					$safe_search_data = $this->hotel_model->get_safe_search_data($search_params['search_id']);
					//Meaning hotels are loaded first time
					$raw_hotel_list = $this->hotel_lib->get_hotel_list(abs($search_params['search_id']));
					//debug($raw_hotel_list);exit;
					if ($raw_hotel_list['status']) {
						//Converting API currency data to preferred currency
						$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
						$raw_hotel_list = $this->hotel_lib->search_data_in_preferred_currency($raw_hotel_list, $currency_obj);
						//Display 
						$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
						//Update currency and filter summary appended
						if (isset($search_params['filters']) == true and valid_array($search_params['filters']) == true) {
							$filters = $search_params['filters'];
						} else {
							$filters = array();
						}															
						$attr['search_id'] = abs($search_params['search_id']);			
						$raw_hotel_search_result = array();						
						$i=0;
						$counter=0;
				    	if ($max_lat == 0) {
							$max_lat = $min_lat = 0;
						}

						if ($max_lon == 0) {
							$max_lon = $min_lon = 0;
						}  
						if($raw_hotel_list['data']['HotelSearchResult']){
							foreach ($raw_hotel_list['data']['HotelSearchResult']['HotelResults'] as $key => $value) {
								$raw_hotel_search_result[$i] =$value;
								$raw_hotel_search_result[$i]['MResultToken']=urlencode($value['ResultToken']);
								 $lat = $value['Latitude'];
								 $lon = $value['Longitude'];
								if(($lat!='')&& ($counter<1)){
									$max_lat = $min_lat = $lat;
								}
								if(($lon!='')){
									$counter++;					
									$max_lon = $min_lon = $lon;
								}

								$i++;
							}
							$raw_hotel_list['data']['HotelSearchResult']['max_lat']  = $max_lat;
							$raw_hotel_list['data']['HotelSearchResult']['max_lon']  = $max_lon;
						}
						$raw_hotel_list['data']['HotelSearchResult']['HotelResults'] =$raw_hotel_search_result; 
						//debug($raw_hotel_list);exit;
						$response['data'] =$raw_hotel_list['data'];					
						$response['status'] = SUCCESS_STATUS;
						
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	/*
	** CODE ADDED ON 4/1/2018
	*/
		/**
	*Get Cancellation Policy based on Cancellation policy code
	*
	*/
	function get_cancellation_policy(){
		$get_params =$this->input->get();
		
		$application_preferred_currency = get_application_currency_preference();
		$application_default_currency = get_application_currency_preference();
		$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		$room_price = $get_params['room_price'];
		//debug($get_params);exit;
		if(isset($get_params['booking_source'])&&!empty($get_params['booking_source'])){
			load_hotel_lib($get_params['booking_source']);
			
			if(isset($get_params['policy_code'])&&!empty($get_params['policy_code'])){
				$safe_search_data = $this->hotel_model->get_safe_search_data($get_params['tb_search_id']);				
				$get_params['no_of_nights'] = $safe_search_data['data']['no_of_nights'];
				$get_params['room_count'] = $safe_search_data['data']['room_count'];
				$get_params['check_in'] = $safe_search_data['data']['from_date'];
 				$cancellation_details = $this->hotel_lib->get_cancellation_details($get_params);
 				
 				
 				$cancellatio_details =$cancellation_details['GetCancellationPolicy']['policy'][0]['policy'];
 				$policy_string ='';
 				$cancel_string='';
 				$cancel_count = count($cancellatio_details);
 				$cancel_reverse = $cancellatio_details;  	
 				//debug($cancellatio_details);exit;
 					foreach ($cancellatio_details as $key => $value) {
	 					$amount = 0;
	 					$policy_string ='';	 					
		 					if($value['Charge']==0){
		 						$policy_string .='No cancellation charges, if cancelled before '.date('d M Y',strtotime($value['ToDate']));
		 					}else{
		 						if($value['Charge']!=0){
		 							 if(isset($cancel_reverse[$key+1])){
		 							 		if($value['ChargeType']==1){ 					
						 						$amount =  $currency_obj->get_currency_symbol($currency_obj->to_currency)." ".get_converted_currency_value($currency_obj->force_currency_conversion(round($value['Charge'])));							 						
						 					}elseif($value['ChargeType']==2){
						 						$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)." ".$room_price;
						 					}
						 					$current_date = date('Y-m-d');
											$cancell_date = date('Y-m-d',strtotime($value['FromDate']));
											if($cancell_date >$current_date){
												//$value['FromDate'] = date('Y-m-d');
												$policy_string .='Cancellations made after '.date('d M Y',strtotime($value['FromDate'])).' to '.date('d M Y',strtotime($value['ToDate'])).', would be charged '.$amount;
											}
						 					//$policy_string .='Cancellations made after '.date('d M Y',strtotime($value['FromDate'])).' to '.date('d M Y',strtotime($value['ToDate'])).', would be charged '.$amount;
						                 
						             }else{
						             	if($value['ChargeType']==1){
						             		$amount =  $currency_obj->get_currency_symbol($currency_obj->to_currency)." ".get_converted_currency_value($currency_obj->force_currency_conversion(round($value['Charge'])));	
						          		}elseif ($value['ChargeType']==2) {
						             		$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)." ".$room_price;
						             	}
						             	$current_date = date('Y-m-d');
										$cancell_date = date('Y-m-d',strtotime($value['FromDate']));
										if($cancell_date > $current_date){
											$value['FromDate'] =$value['FromDate']; 
										}else{
											$value['FromDate'] = date('Y-m-d');
										}
						             	$policy_string .='Cancellations made after '.date('d M Y',strtotime($value['FromDate'])).', or no-show, would be charged '.$amount;
						             }
				 				
			 					}
		 					}		 					
		 				$cancel_string .= $policy_string.'<br/> ';
	 				}
 				
 				echo $cancel_string;
				//echo $cancellation_details['GetCancellationPolicy']['policy'][0];
			}else{
				$cancel_string ='';
					$cancellation_policy_details = json_decode(base64_decode($get_params['policy_details']));
					//debug($cancellation_policy_details);
					$cancel_count = count($cancellation_policy_details);					
					$cancellation_policy_details = json_decode(json_encode($cancellation_policy_details), True);
					$cancel_reverse = array_reverse($cancellation_policy_details);				
					//debug($cancel_reverse);
					if($cancellation_policy_details){
							foreach (array_reverse($cancellation_policy_details) as $key=>$value) {							
									$policy_string ='';								
										if($value['Charge']==0){
											$policy_string .='No cancellation charges, if cancelled before '.date('d M Y',strtotime($value['ToDate']));
										}else{
											if(isset($cancel_reverse[$key+1])){
												if($value['ChargeType']==1){
													$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)."  ".get_converted_currency_value($currency_obj->force_currency_conversion($value['Charge']));
													
												}elseif ($value['ChargeType']==2) {
													$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)."  ".$room_price;
												}
												$current_date = date('Y-m-d');
												$cancell_date = date('Y-m-d',strtotime($value['FromDate']));
												if($cancell_date >$current_date){
													$policy_string .='Cancellations made after '.date('d M Y',strtotime($value['FromDate'])).' to '.date('d M Y',strtotime($value['ToDate'])).', would be charged '.$amount;
												}
												
											}else{
												if($value['ChargeType']==1){
													$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)."  ".get_converted_currency_value($currency_obj->force_currency_conversion($value['Charge']));
													
												}elseif ($value['ChargeType']==2) {
													$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)."  ".$room_price;
												}
												$current_date = date('Y-m-d');
												$cancell_date = date('Y-m-d',strtotime($value['FromDate']));
												if($cancell_date >$current_date){
													$value['FromDate'] = $value['FromDate'];
												}else{
													$value['FromDate'] = date('Y-m-d');
												}
												$policy_string .='Cancellations made after '.date('d M Y',strtotime($value['FromDate'])).', or no-show, would be charged '.$amount;
											}
										}									
																	
									$cancel_string .= $policy_string.'<br/>';
									
							}
					}else{
						$cancel_string = 'This rate is non-refundable. If you cancel this booking you will not be refunded any of the payment.';
					}
					
				
				echo $cancel_string;
			}
				
			
		}else{
			echo "This rate is non-refundable. If you cancel this booking you will not be refunded any of the payment.";
		}
		exit;
	}

	public function sightseeing_list($offset=0){ 
	    $this->load->model('sightseeing_model');    
        $search_params = $this->input->get();
        $safe_search_data = $this->sightseeing_model->get_safe_search_data($search_params['search_id'],META_SIGHTSEEING_COURSE);
        $limit = $this->config->item('sightseeing_page_limit');
        if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
            load_sightseen_lib($search_params['booking_source']);
            switch($search_params['booking_source']) {
                case PROVAB_SIGHTSEEN_BOOKING_SOURCE : 
                 $raw_sightseeing_result = $this->sightseeing_lib->get_sightseeing_list($safe_search_data);                 
                    if ($raw_sightseeing_result['status']) {
                        //Converting API currency data to preferred currency
                        $currency_obj = new Currency(array('module_type' => 'sightseeing', 'from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
                        $raw_sightseeing_result = $this->sightseeing_lib->search_data_in_preferred_currency($raw_sightseeing_result, $currency_obj,$this->user_type);
                        //Display 
                        $currency_obj = new Currency(array('module_type' => 'sightseeing', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

                        $filters = array();                       

                        //Update currency and filter summary appended
                        if (isset($search_params['filters']) == true and valid_array($search_params['filters']) == true) {
                            $filters = $search_params['filters'];
                        } else {
                            $filters = array();
                        }
                      	
                        $raw_sightseeing_result['data'] = $this->sightseeing_lib->format_search_response($raw_sightseeing_result['data'], $currency_obj, $search_params['search_id'], $this->user_type, $filters);
                        //debug($raw_sightseeing_result);die('sdffs');
                        
                        $source_result_count = $raw_sightseeing_result['data']['source_result_count'];
                        $filter_result_count = $raw_sightseeing_result['data']['filter_result_count'];
                       
                        //debug($raw_hotel_list);exit;
                        if (intval($offset) == 0) {
                            //Need filters only if the data is being loaded first time
                            $filters = $this->sightseeing_lib->filter_summary($raw_sightseeing_result['data']);
                            $response['filters'] = $filters['data'];
                        }   
                        $attr['search_id'] = abs($search_params['search_id']);
             
                        $response['data'] = get_compressed_output(
                                $this->load->view('sightseeing/viator/viator_search_result', array('currency_obj' => $currency_obj, 'raw_sightseeing_list' => $raw_sightseeing_result['data'],
                                    'search_id' => $search_params['search_id'], 'booking_source' => $search_params['booking_source'],
                                    'attr' => $attr,
                                    'search_params' => $safe_search_data['data']
                        ), true));
   
                        $response['status'] = SUCCESS_STATUS;
                        $response['total_result_count'] = $source_result_count;
                        $response['filter_result_count'] = $filter_result_count;
                        $response['offset'] = $offset + $limit;
                    }else{
                        $response['status'] = FAILURE_STATUS;   
                    }
                break;
            }
        }
        //debug($response);exit('hi');
        $this->output_compressed_data($response);
    }

    /**
    * Get Sightsseeing Category List
    */
    function get_ss_category_list(){

       $get_params = $this->input->get();
        if($get_params){
             if($get_params['city_id']){          

                   load_sightseen_lib(PROVAB_SIGHTSEEN_BOOKING_SOURCE); 
                   $select_cate_id = 0;
                   if(isset($get_params['Select_cate_id'])){
                     $select_cate_id = $get_params['Select_cate_id'];
                   }else{
                    $get_params['Select_cate_id'] =0;
                   }
                   $category_list = $this->sightseeing_lib->get_category_list($get_params);
                   // debug($category_list);die;
                  if($category_list['status']==SUCCESS_STATUS){

                        $cate_response = $this->sightseeing_lib->format_category_response($category_list['data']['CategoryList'],$select_cate_id);

                       if($cate_response['status']==SUCCESS_STATUS){
                            echo json_encode($cate_response['data']);
                            exit;
                       }
                  }else{
                    echo "0";
                    exit;
                  }
                         
             }else{
                echo "0";
                exit;
             }

       }else{
        echo "0";
        exit;
       }
    }

    /**
	 * Karthick
	 * Get Bank Branches
	 */
	function get_bank_details($bank_origin)
	{
	   
		if(intval($bank_origin) > 0) {
			$data['status'] = false;
			$data['branches'] = false;
			$branch_details = $this->custom_db->single_table_records('bank_account_details', 'origin, en_branch_name, account_number,en_account_name,ifsc_code', array('origin' => intval($bank_origin), 'status' => ACTIVE));
			if($branch_details['status'] == true) {
				$data['status'] = true;
				$data['branch'] = $branch_details['data'][0]['en_branch_name'];
				$data['account_number'] = $branch_details['data'][0]['account_number'];
				$data['account_name'] = $branch_details['data'][0]['en_account_name'];
				$data['ifsc'] = $branch_details['data'][0]['ifsc_code'];
			}
		}
		
		$this->output_compressed_data($data);
	}
}
