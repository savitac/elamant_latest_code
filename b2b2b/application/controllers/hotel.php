<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @package    Provab
 * @subpackage Hotel
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */
error_reporting(0);
class Hotel extends CI_Controller {
	private $current_module;
	public function __construct()
	{
		parent::__construct();
		//we need to activate hotel api which are active for current domain and load those libraries
		$this->load->model('hotel_model');
		$this->load->model('user_model');
		$this->load->model('custom_db');
		$this->load->model('domain_management_model');
		$this->load->model('module_model');
		$this->load->model('module_model');
		$this->load->model('transaction');
		//$this->load->library('currency');
		$this->load->model ('General_Model'); 
		//$this->load->helper('db_cache_api');
		
		//$this->output->enable_profiler(TRUE);
		//$this->current_module = $this->config->item('current_module');
	}

	/**
	 * index page of application will be loaded here
	 */
	function index()
	{
		//	echo number_format(0, 2, '.', '');
	}

	/**
	 *  Arjun J Gowda
	 * Load Hotel Search Result 
	 * @param number $search_id unique number which identifies search criteria given by user at the time of searching
	 */
	function search($search_id)
	{
		/*echo '<pre>'; print_r($search_id); exit;
		*/
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		// debug($safe_search_data);
		// exit("50");
		// Get all the hotels bookings source which are active
		$active_booking_source = $this->hotel_model->active_booking_source();
		if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true && $safe_search_data['data']['villa'] == false) {
				// debug($safe_search_data);exit("47");
			$safe_search_data['data']['search_id'] = abs($search_id);
			$this->load->view('hotel/search_result_page', array('hotel_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
		}else if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true && $safe_search_data['data']['villa'] == true) {
				// debug($safe_search_data);exit("51");
			$safe_search_data['data']['search_id'] = abs($search_id);
			$this->load->view('villa/search_result_page_new', array('hotel_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
		} else {
			$this->load->view ( 'general/popup_redirect');
		}
	}
	function pre_hotel_search($search_id='')
	{
		//Global search Data
		$search_id = $this->save_pre_search(META_ACCOMODATION_COURSE);
		$this->save_search_cookie(META_ACCOMODATION_COURSE, $search_id);

		//Analytics
		$this->load->model('hotel_model');
		$searchParams = $this->input->get();
		//echo"<pre>";print_r($searchParams);die;
		// $searchParams = [];
		// $searchParams = [
		// 			'city' => $search_params['city'],
		// 			'hotel_destination' => $search_params['hotel_destination'],
		// 			'hotel_checkin' => $search_params['hotel_checkin'],
		// 			'hotel_checkout' => $search_params['hotel_checkout'],
		// 			'rooms' => $search_params['rooms'],
		// 			'adult' => [
		// 				0 => 1,
		// 			],
		// 			'child' => [
		// 				0 => 1,
		// 			],
		// 			'childAge_1' => [
		// 				0 => 10,
		// 			]  
		// 		];
		$params = http_build_query($searchParams);
		//echo"<pre>";print_r($search_params);die;
		// debug($search_params);die;
		$this->hotel_model->save_search_data($searchParams, META_ACCOMODATION_COURSE);

		//redirect('hotel/search/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
		redirect('hotel/search/'.$search_id.'?'.$params);
	}
	private function save_pre_search($search_type)
	{
		//Save data
		$searchParams = $this->input->get();
		// $searchParams = [];
		// $searchParams = [
		// 			'city' => $search_params['city'],
		// 			'hotel_destination' => $search_params['hotel_destination'],
		// 			'hotel_checkin' => $search_params['hotel_checkin'],
		// 			'hotel_checkout' => $search_params['hotel_checkout'],
		// 			'rooms' => $search_params['rooms'],
		// 			'adult' => [
		// 				0 => 1,
		// 			],
		// 			'child' => [
		// 				0 => 1,
		// 			],
		// 			'childAge_1' => [
		// 				0 => 10,
		// 			]  
		// 		];

		$search_data = json_encode($searchParams);
		$insert_id = $this->custom_db->insert_record('search_history', array('search_type' => $search_type, 'search_data' => $search_data, 'created_datetime' => date('Y-m-d H:i:s')));
		return $insert_id['insert_id'];
	}

	private function save_search_cookie($module, $search_id)
	{
		$sparam = array();
		$sparam = $this->input->cookie('sparam', TRUE);
		if (empty($sparam) == false) {
			$sparam = unserialize($sparam);
		}
		$sparam[$module] = $search_id;

		$cookie = array(
			'name' => 'sparam',
			'value' => serialize($sparam),
			'expire' => '86500',
			'path' => PROJECT_COOKIE_PATH
		);
		$this->set_cookie($cookie);
	}

	function set_cookie($name = '', $value = '', $expire = '', $domain = '', $path = '/', $prefix = '', $secure = FALSE)
	{
		if (is_array($name))
		{
			// always leave 'name' in last place, as the loop will break otherwise, due to $$item
			foreach (array('value', 'expire', 'domain', 'path', 'prefix', 'secure', 'name') as $item)
			{
				if (isset($name[$item]))
				{
					$$item = $name[$item];
				}
			}
		}

		if ($prefix == '' AND config_item('cookie_prefix') != '')
		{
			$prefix = config_item('cookie_prefix');
		}
		if ($domain == '' AND config_item('cookie_domain') != '')
		{
			$domain = config_item('cookie_domain');
		}
		if ($path == '/' AND config_item('cookie_path') != '/')
		{
			$path = config_item('cookie_path');
		}
		if ($secure == FALSE AND config_item('cookie_secure') != FALSE)
		{
			$secure = config_item('cookie_secure');
		}

		if ( ! is_numeric($expire))
		{
			$expire = time() - 86500;
		}
		else
		{
			$expire = ($expire > 0) ? time() + $expire : 0;
		}

		setcookie($prefix.$name, $value, $expire, $path, $domain, $secure);
	}

	function load_hotel_lib($source)
	{
		switch ($source) {
			case PROVAB_HOTEL_BOOKING_SOURCE :
				$this->load->library('hotel/provab_private', '', 'hotel_lib');
				break;
			default : redirect(base_url());
		}
	}

	function get_hotel_city_list()
	{
		$this->load->model('hotel_model');
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$data_list = $this->hotel_model->get_hotel_city_list($term);
		if (valid_array($data_list) == false) {
			$data_list = $this->hotel_model->get_hotel_city_list('');
		}
		$suggestion_list = array();
		$result = '';
		foreach($data_list as $city_list){
			$suggestion_list['label'] = $city_list['city_name'].', '.$city_list['country_name'].'';
			$suggestion_list['value'] = hotel_suggestion_value($city_list['city_name'], $city_list['country_name']);
			$suggestion_list['id'] = $city_list['origin'];
			if (empty($city_list['top_destination']) == false) {
				$suggestion_list['category'] = 'Top cities';
				$suggestion_list['type'] = 'Top cities';
			} else {
				$suggestion_list['category'] = 'Search Results';
				$suggestion_list['type'] = 'Search Results';
			}
			if (intval($city_list['cache_hotels_count']) > 0) {
				$suggestion_list['count'] = $city_list['cache_hotels_count'];
			} else {
				$suggestion_list['count'] = 0;
			}
			$result[] = $suggestion_list;
		}
		$this->output_compressed_data($result);
	}

	public function get_hotel_city_suggestions() {
        ini_set('memory_limit', '-1');
        $term = $this->input->get('term');
        $term = trim(strip_tags($term));
        $hotels = $this->hotel_model->get_hotel_city_list($term);
        /*echo "data: <pre>";print_r($hotels);exit;*/
        $result = array();
       
       foreach ($hotels as $hotel) {
       
            $apts['type'] = 'City';
            
            if($hotel['country_code'] != '') {
				$country = "(".$hotel['country_name'].")"; 
				$country_val = "(".$hotel['country_name'].")"; 
			}else{
			    $country = '';
			    $country_val ='';
			}
			
            $apts['label'] = $hotel['city_name'].' '.$country;
            $apts['value'] = $hotel['city_name'].' '.$country_val;
            $apts['id'] = $hotel['origin'];
            $result[] = $apts;
        }
        echo json_encode($result);
	}


	/**
	*  Arjun J Gowda
	* Load hotel details based on booking source
	*/
	function hotel_details($search_id)
	{
		$params = $this->input->get();
		
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		$safe_search_data['data']['search_id'] = abs($search_id);
		$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_api_data_currency(), 'to' => get_application_currency_preference())); 
		if (isset($params['booking_source']) == true) {

			//We will load different page for different API providers... As we have dependency on API for hotel details page
			load_hotel_lib($params['booking_source']);
			if ($params['booking_source'] == PROVAB_HOTEL_BOOKING_SOURCE && isset($params['ResultIndex']) == true and isset($params['op']) == true and
			$params['op'] == 'get_details' and $safe_search_data['status'] == true) {
				$params['ResultIndex']	= urldecode($params['ResultIndex']);
				$raw_hotel_details = $this->hotel_lib->get_hotel_details($params['ResultIndex']);
				//debug($raw_hotel_details);exit(" BOOKURFLIGHT");
				if ($raw_hotel_details['status']) {
					$this->load->view('hotel/tbo/tbo_hotel_details_page', array('currency_obj' => $currency_obj, 'hotel_details' => $raw_hotel_details['data'], 'hotel_search_params' => $safe_search_data['data'], 'active_booking_source' => $params['booking_source'], 'params' => $params));
				} else {
					redirect(base_url().'index.php/hotel/exception?op=Remote IO error @ Session Expiry&notification=session');
				}
			} else {
				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	}

	/**
	 *  Arjun J Gowda
	 * Passenger Details page for final bookings
	 * Here we need to run booking based on api
	 */
	function booking($search_id)
	{

		$pre_booking_params = $this->input->post();
		$domain_id=$this->session->userdata('domain_id'); 
		// debug($pre_booking_params);exit;
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		$safe_search_data['data']['search_id'] = abs($search_id);
		$page_data['active_payment_options'] = $this->module_model->get_active_payment_module_list();
		if (isset($pre_booking_params['booking_source']) == true) {
			//We will load different page for different API providers... As we have dependency on API for hotel details page
			$page_data['search_data'] = $safe_search_data['data'];
			load_hotel_lib($pre_booking_params['booking_source']);
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");

			if ($pre_booking_params['booking_source'] == PROVAB_HOTEL_BOOKING_SOURCE && isset($pre_booking_params['token']) == true and
			isset($pre_booking_params['op']) == true and $pre_booking_params['op'] == 'block_room' and $safe_search_data['status'] == true)
			{
				
				$pre_booking_params['token'] = unserialized_data($pre_booking_params['token'], $pre_booking_params['token_key']);
			
				if ($pre_booking_params['token'] != false) {


					$room_block_details = $this->hotel_lib->block_room($pre_booking_params);
				//	debug($room_block_details);exit;
					if ($room_block_details['status'] == false) {
						redirect(base_url().'index.php/hotel/exception?op='.$room_block_details['data']['msg']);
					}
					//Converting API currency data to preferred currency
					$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
					$room_block_details = $this->hotel_lib->roomblock_data_in_preferred_currency($room_block_details, $currency_obj);
					
					//Display
					$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
					$cancel_currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
					$pre_booking_params = $this->hotel_lib->update_block_details($room_block_details['data']['response']['BlockRoomResult'], $pre_booking_params,$cancel_currency_obj);
					/*
					 * Update Markup
					 */
					$pre_booking_params['markup_price_summary'] = $this->hotel_lib->update_booking_markup_currency($pre_booking_params['price_summary'], $currency_obj, $safe_search_data['data']['search_id']);
					if ($room_block_details['status'] == SUCCESS_STATUS) {
						if(!empty($this->entity_country_code)){
							$page_data['user_country_code'] = $this->entity_country_code;
						}
						else{
							$page_data['user_country_code'] = '';	
						}
						$page_data['booking_source'] = $pre_booking_params['booking_source'];
						$page_data['pre_booking_params'] = $pre_booking_params;
						$page_data['pre_booking_params']['default_currency'] = get_application_currency_preference();
						$page_data['iso_country_list']	= $this->db_cache_api->get_iso_country_list();
						$page_data['country_list']		= $this->db_cache_api->get_country_list();
						$page_data['currency_obj']		= $currency_obj;
						$page_data['total_price']		= $this->hotel_lib->total_price($pre_booking_params['markup_price_summary']);
						//debug($this->session->userdata);die;
						if($this->session->userdata('user_logged_in')==0 || $this->session->userdata('user_type') == 5)
						{
							
						$page_data['convenience_fees']  = $currency_obj->convenience_fees($page_data['total_price'], $page_data['search_data']['search_id'],$domain_id);
						}
						else
						{
							$page_data['convenience_fees']=0; 
						}
						$page_data['tax_service_sum']	=  $this->hotel_lib->tax_service_sum($pre_booking_params['markup_price_summary'], $pre_booking_params['price_summary']);
						//Traveller Details
						$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
						//Get the country phone code 
						$Domain_record = $this->custom_db->single_table_records('domain_list', '*');
						$page_data['active_data'] =$Domain_record['data'][0];
						$temp_record = $this->custom_db->single_table_records('api_country_list', '*');
						$page_data['phone_code'] =$temp_record['data'];
						//debug($page_data);exit;
						$this->load->view('hotel/tbo/tbo_booking_page', $page_data);
					}
				} else {
					redirect(base_url().'index.php/hotel/exception?op=Data Modification&notification=Data modified while transfer(Invalid Data received while validating tokens)');
				}
			} else {
				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	
	}

	/**
	 *  Arjun J Gowda
	 * Secure Booking of hotel
	 * 255 single adult static booking request 2310
	 * 261 double room static booking request 2308
	 */
	function pre_booking($search_id)
	{
		$site_name = str_replace('/','',$_SERVER['HTTP_HOST']);
      	$site_name = str_ireplace('www.','',$_SERVER['HTTP_HOST']); 
      	$site_details  =  $this->General_Model->get_site_details($site_name);

		$post_params = $this->input->post();
		//debug($post_params);exit;
		//Setting Static Data - Jaganath
		$post_params['billing_city'] = 'Bangalore';
		$post_params['billing_zipcode'] = '560100';
		$post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';
		

		//Make sure token and temp token matches
		$valid_temp_token = unserialized_data($post_params['token'], $post_params['token_key']);
		if ($valid_temp_token != false) {

			load_hotel_lib($post_params['booking_source']);
			/****Convert Display currency to Application default currency***/
			//After converting to default currency, storing in temp_booking
			$post_params['token'] = unserialized_data($post_params['token']);
			$currency_obj = new Currency ( array (
						'module_type' => 'hotel',
						'from' => get_application_currency_preference (),
						'to' => admin_base_currency () 
				));
			$post_params['token'] = $this->hotel_lib->convert_token_to_application_currency($post_params['token'], $currency_obj, $this->current_module);
			$post_params['token'] = serialized_data($post_params['token']);
			$temp_token = unserialized_data($post_params['token']);
			//Insert To temp_booking and proceed
			$temp_booking = $this->module_model->serialize_temp_booking_record($post_params, HOTEL_BOOKING); 
			$book_id = $temp_booking['book_id'];
			$book_origin = $temp_booking['temp_booking_origin'];
		
		
			if ($post_params['booking_source'] == PROVAB_HOTEL_BOOKING_SOURCE) {
				$amount	  = $this->hotel_lib->total_price($temp_token['markup_price_summary']);
				$currency = $temp_token['default_currency'];
			}
			$currency_obj = new Currency ( array (
						'module_type' => 'hotel',
						'from' => admin_base_currency (),
						'to' => admin_base_currency () 
			) );
			/********* Convinence Fees Start ********/
			$domain_id=$this->session->userdata('domain_id'); 
			$convenience_fees = $currency_obj->convenience_fees($amount, $search_id,$domain_id);
			/********* Convinence Fees End ********/
			 	
			/********* Promocode Start ********/
			$promocode_discount = $post_params['promo_code_discount_val'];
			/********* Promocode End ********/

			//details for PGI
			
			$email = $post_params ['billing_email'];
			$phone = $post_params ['passenger_contact'];
			$verification_amount = roundoff_number($amount+$convenience_fees-$promocode_discount);
			$firstname = $post_params ['first_name'] ['0'];
			$productinfo = META_ACCOMODATION_COURSE;
			//check current balance before proceeding further
			$domain_balance_status = $this->domain_management_model->verify_current_balance($verification_amount, $currency);
			if ($domain_balance_status == true) {
				switch($post_params['payment_method']) {
					case PAY_NOW :
						$this->load->model('transaction');
						$pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
						$this->transaction->create_payment_record($book_id, $verification_amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount, $pg_currency_conversion_rate);		
						//redirect(base_url().'index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin);  
						///debug($site_details);die(" BEFORE");
						$user_type=$this->session->userdata('user_type');
					// if($user_type == 5 && $site_details->payment_gateway_status == 0)
					// {
					// $this->offline_request($book_id,$book_origin);
					// exit ();
					if($this->session->userdata('user_logged_in') == 1){
					
							 redirect(base_url() . 'index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin.'/'.$post_params['card'].'/'.$post_params['month'].'/'.$post_params['year']);

					}
					else
					{
						echo ' Unauthorized access.';
								exit();
						redirect(base_url().'index.php/hotel/process_booking/'.$book_id.'/'.$book_origin);  						
					break; 
					}				
					case PAY_AT_BANK : echo 'Under Construction - Remote IO Error';exit;
					break; 
				}
			} else {
				redirect(base_url().'index.php/hotel/exception?op=Amount Hotel Booking&notification=insufficient_balance');
			}
		} else {
			redirect(base_url().'index.php/hotel/exception?op=Remote IO error @ Hotel Booking&notification=validation');
		}
	}

	/*
	PAWAN BAGGA
	*Offline booking 
	*/
	function offline_request($book_id, $temp_book_origin){
		if($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0){
			$page_data ['form_params'] ['book_id'] = $book_id;
			$page_data ['form_params'] ['temp_book_origin'] = $temp_book_origin;
			$this->session->set_userdata("uniqdata",$page_data);
			redirect('index.php/voucher/hotel_pay_later');
			//$this->template->view('share/loader/booking_process_loader', $page_data);	
			
		}else{
			exit("exit here");
			redirect(base_url().'index.php/hotel/exception?op=Invalid request&notification=validation'); 
		}
		
	}

	/*
		process booking in backend until show loader 
	*/
	function process_booking($book_id, $temp_book_origin){

		if($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0){
			$user_type=$this->session->userdata('user_type');
			if($user_type == 5)
			{ 
			$page_data['book_id']= $book_id;
			$page_data['temp_book_origin']= $temp_book_origin;
			$page_data['productinfo']= META_AIRLINE_COURSE; 
			$this->load->view('payment/PAYEESY/payment_gateway_card',$page_data); 			
			}
			else{
			$page_data ['form_url'] = base_url () . 'index.php/hotel/secure_booking';
			$page_data ['form_method'] = 'POST';
			$page_data ['form_params'] ['book_id'] = $book_id;
			$page_data ['form_params'] ['temp_book_origin'] = $temp_book_origin;

			$this->load->view('share/loader/booking_process_loader', $page_data); 	
			}
				

		}else{
			redirect(base_url().'index.php/hotel/exception?op=Invalid request&notification=validation');
		}
		
	}

	/*
	* PAWAN BAGGA
	* Deduct Balance From Agent & Sub- Agent  added date 11/01/2018
	*/
	function agent_balance_mangment($book_params,$temp_booking)
	{	  
		$agent_id= $this->session->userdata('user_details_id');  
		$agent_balance=$this->transaction->get_agent_balance($agent_id);	 
		$agent_account_balance=$agent_balance['balance_credit'];
		$total_fair=$book_params['fare']+$book_params['admin_markup']+$book_params['convinence']-
		$book_params['agent_markup'];  
		/*$total_fair=$fair_detail/*+$admin_markup*/
		$update_agent_balance=$agent_account_balance - $total_fair; 

		$transaction_logs =array(
		'user_id'         => $agent_id,
		'last_balance'    => $agent_balance['balance_credit'],
		'current_balance' => $update_agent_balance,
		'amount_debited'  => $total_fair,
		'app_ref'         => $temp_booking['book_id'], 
		'module'          => "HOTEL",
		'log_type'        => "BOOKING",
		);
		$this->transaction->update_transaction_logs($transaction_logs);  
		$this->transaction->update_agent_balance($update_agent_balance,$agent_id);   
	}
		/**
		*  Arjun J Gowda
		*Do booking once payment is successfull - Payment Gateway
		*and issue voucher
		*HB11-152109-443266/1
		*HB11-154107-854480/2
		*/
	function secure_booking()
	{
		$post_data = $this->input->post();
		//debug($post_data);exit;
		if(valid_array($post_data) == true && isset($post_data['book_id']) == true && isset($post_data['temp_book_origin']) == true &&
			empty($post_data['book_id']) == false && intval($post_data['temp_book_origin']) > 0){
			//verify payment status and continue
			$book_id = trim($post_data['book_id']);
			$temp_book_origin = intval($post_data['temp_book_origin']);
			$this->load->model('transaction');
			$booking_status = $this->transaction->get_payment_status($book_id);
			
			// if($booking_status['status'] !== 'accepted'){
			// 	redirect(base_url().'index.php/hotel/exception?op=Payment Not Done&notification=validation');
			// }
		} else{
			redirect(base_url().'index.php/hotel/exception?op=InvalidBooking&notification=invalid');
		}		
		//run booking request and do booking
		$temp_booking = $this->module_model->unserialize_temp_booking_record($book_id, $temp_book_origin);
		$book_params = $temp_booking ['book_attributes'];
		//Delete the temp_booking record, after accessing
		$this->module_model->delete_temp_booking_record ($book_id, $temp_book_origin);
		load_hotel_lib($temp_booking['booking_source']);
		//verify payment status and continue
		$total_booking_price = $this->hotel_lib->total_price($temp_booking['book_attributes']['token']['markup_price_summary']);
		$currency = $temp_booking['book_attributes']['token']['default_currency'];
		//also verify provab balance
		//check current balance before proceeding further
		$domain_balance_status = $this->domain_management_model->verify_current_balance($total_booking_price, $currency);
		
		if ($domain_balance_status) {
			//lock table
			if ($temp_booking != false) {
				switch ($temp_booking['booking_source']) {
					case PROVAB_HOTEL_BOOKING_SOURCE :
						//FIXME : COntinue from here - Booking request
						$booking = $this->hotel_lib->process_booking($book_id, $temp_booking['book_attributes']);
						//Save booking based on booking status and book id
						break;
				}
				if ($booking['status'] == SUCCESS_STATUS) {
					//debug($book_params);die(" CORRECT"); 
					$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => admin_base_currency(), 'to' => admin_base_currency()));
					$booking['data']['currency_obj'] = $currency_obj;  
					//Save booking based on booking status and book id
					//debug($booking['data']);die(" IN CTRL ONLY "); 
					$data = $this->hotel_lib->save_booking($book_id, $booking['data']);  

					//Deduct Balance From Agent & Sub-Agent.
					$this->agent_balance_mangment($data,$temp_booking); 
				 	$this->domain_management_model->update_transaction_details('hotel', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount'],$data['transaction_currency'], $data['currency_conversion_rate']); 

					redirect(base_url().'index.php/voucher/hotel/'.$book_id.'/'.$temp_booking['booking_source'].'/'.$data['booking_status'].'/show_voucher');
				} else {
					redirect(base_url().'index.php/hotel/exception?op=booking_exception&notification='.$booking['msg']);
				}
			}
			//release table lock
		} else {
			redirect(base_url().'index.php/hotel/exception?op=Remote IO error @ Insufficient&notification=validation');
		}
		//redirect(base_url().'index.php/hotel/exception?op=Remote IO error @ Hotel Secure Booking&notification=validation');
	}


	function test(){
		$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => admin_base_currency(), 'to' => admin_base_currency()));
		debug($currency_obj);
	}

	/**
	 *  Arjun J Gowda
	 *Process booking on hold - pay at bank
	 */
	function booking_on_hold($book_id)
	{

	}
	/**
	 * Jaganath
	 */
	function pre_cancellation($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source);

			if ($booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				// debug($page_data);exit;
				$this->load->view('hotel/pre_cancellation', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}

	/*
	 * Jaganath
	 * Process the Booking Cancellation
	 * Full Booking Cancellation
	 *
	 */
	function cancel_booking($app_reference, $booking_source)
	{
		if(empty($app_reference) == false) {
			$master_booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_hotel_booking_data($master_booking_details, 'b2c');
				$master_booking_details = $master_booking_details['data']['booking_details'][0];
				load_hotel_lib($booking_source);
				
				$cancellation_details = $this->hotel_lib->cancel_booking($master_booking_details);//Invoke Cancellation Methods
				// debug($cancellation_details);
				// exit("11");
				if($cancellation_details['status'] == false) {
					$query_string = '?error_msg='.$cancellation_details['msg'];
				} else {
					$query_string = '';
				}
				redirect('hotel/cancellation_details/'.$app_reference.'/'.$booking_source.$query_string);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	/**
	 * Jaganath
	 * Cancellation Details
	 * @param $app_reference
	 * @param $booking_source
	 */
	function cancellation_details($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $GLOBALS['CI']->hotel_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$page_data = array();
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_hotel_booking_data($master_booking_details, 'b2c');
				$page_data['data'] = $master_booking_details['data'];
				$this->load->view('hotel/cancellation_details', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}

	}
	function map()
	{
		$details = $this->input->get();
		$geo_codes['data']['latitude'] = $details['lat'];
		$geo_codes['data']['longtitude'] = $details['lon'];
		$geo_codes['data']['hotel_name'] = urldecode($details['hn']);
		$geo_codes['data']['star_rating'] = $details['sr'];
		$geo_codes['data']['city'] = urldecode($details['c']);
		$geo_codes['data']['hotel_image'] = urldecode($details['img']);
		echo $this->load->view('hotel/location_map', $geo_codes);
	}


	/**
	 * Arjun J Gowda
	 */
	function exception()
	{
		$module = META_ACCOMODATION_COURSE;
		$op = (empty($_GET['op']) == true ? '' : $_GET['op']);
		$notification = (empty($_GET['notification']) == true ? '' : $_GET['notification']);
		$eid = $this->module_model->log_exception($module, $op, $notification);
		//set ip log session before redirection
		$this->session->set_flashdata(array('log_ip_info' => true));
		redirect(base_url().'index.php/hotel/event_logger/'.$eid);
	}

	function event_logger($eid='')
	{
		$log_ip_info = $this->session->flashdata('log_ip_info');
		$this->load->view('hotel/exception', array('log_ip_info' => $log_ip_info, 'eid' => $eid));
	}

	
	/**
	 * Promo code
	 * Pradeep chinwan 
	 */
	function check_promo(){
		error_reporting(0);
		$params = $this->input->post();
		extract($params);
		
		$grand_total = $params['original']; 
		$return = $this->hotel_model->get_promo($params['promocode']);
		$data = $return->result_array();
	
		if(($data[0]['status'] == 1))
		{
			if(($data[0]['expiry_date'] >= date('Y-m-d')) || ($data[0]['expiry_date'] == '0000-00-00'))
			{ 
				if(($data[0]['module'] == "hotel"))
				{
					$promo_discount_value = $data[0]['value'];
					$promo_discount_type = $data[0]['value_type'];
					$promo_discount_expiry = $data[0]['expiry_date'];
					$promo_discount_status = $data[0]['status'];
					
					$currency = $this->session->userdata('currency');
					
					if(isset($currency) != 'USD')
					{
						$rate = $this->master_currency->getConversionRate(false,"USD",$currency);
						$total_promo_amount = ceil($promo_discount_value*$rate);
					}else{
						$total_promo_amount = ceil($promo_discount_value);
					}
				    
					if($promo_discount_type == 'plus'){
						$discount = $total_promo_amount;
						$value = $grand_total-$total_promo_amount;		
					}
					if($promo_discount_type == 'percentage'){
						$discount = $grand_total*$total_promo_amount/100;
						$value = $grand_total - $discount;

					}
					if($value < $grand_total)
					{
						$datas['message'] = "Accepted";
						$datas['promocode_discount']	= number_format($discount,2);
						$datas['total_amount']	= number_format($value,2);
						$datas['status']=1;
						echo json_encode($datas);
					}else{
						$datas['message'] = "Promocode Not Valid!!!";
						$datas['status']= 0;
						$datas['total_amount']	= number_format($grand_total,2);
						echo json_encode($datas);
					}
					
					
				}else{
					$datas['message'] = "Promocode Not Valid!!!";
					$datas['status']= 0;
					$datas['total_amount']	= number_format($grand_total,2);
					echo json_encode($datas);
				}
			}else{
				$datas['message'] = "Promocode expired !!!";
				$datas['status']= 0;
				$datas['total_amount']	= number_format($grand_total,2);
				echo json_encode($datas);
			}		
		}
		else{
			$datas['message'] = "Promocode Not Valid!!!";
			$datas['status']=0;
			$datas['total_amount']	= number_format($grand_total,2);
			echo json_encode($datas);
		}
	}

	/**
	 * Updates Cancellation Refund Details
	 */
	public function update_refund_details()
	{
		$post_data = $this->input->post();
		$redirect_url_params = array();
		$this->form_validation->set_rules('app_reference', 'app_reference', 'trim|required|xss_clean');
		$this->form_validation->set_rules('status', 'passenger_status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('status', 'passenger_status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('refund_payment_mode', 'refund_payment_mode', 'trim|required|xss_clean');
		$this->form_validation->set_rules('refund_amount', 'refund_amount', 'trim|numeric');
		$this->form_validation->set_rules('cancellation_charge', 'cancellation_charge', 'trim|numeric');
		$this->form_validation->set_rules('refund_status', 'refund_status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('refund_comments', 'refund_comments', 'trim|required');
		if ($this->form_validation->run()) {
			$app_reference = 				trim($post_data['app_reference']);
			$booking_source = 				trim($post_data['booking_source']);
			$status = 						trim($post_data['status']);
			$refund_payment_mode = 			trim($post_data['refund_payment_mode']);
			$refund_amount = 				floatval($post_data['refund_amount']);
			$cancellation_charge = 			floatval($post_data['cancellation_charge']);
			$refund_status = 				trim($post_data['refund_status']);
			$refund_comments = 				trim($post_data['refund_comments']);
			//Get Booking Details
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $status);
			if($booking_details['status'] == SUCCESS_STATUS){
				$master_booking_details = $booking_details['data']['booking_details'][0];
				$booking_currency = $master_booking_details['currency'];//booking currency
				$booked_user_id = intval($master_booking_details['created_by_id']);
				$user_condition[] = array('U.user_id' ,'=', $booked_user_id);
				$booked_user_details = $this->user_model->get_user_details($user_condition);
				$is_agent = false;
				if(valid_array($booked_user_details) == true && $booked_user_details[0]['user_type'] == B2B_USER){
					$is_agent = true;
				}
				//REFUND AMOUNT TO AGENT
				$currency_obj = new Currency(array('from' => get_application_default_currency() , 'to' => $booking_currency));
				$currency_conversion_rate = $currency_obj->currency_conversion_value(true, get_application_default_currency(), $booking_currency);
				if($refund_status == 'PROCESSED' && floatval($refund_amount) > 0 && $is_agent == true){
					//1.Crdeit the Refund Amount to Respective Agent
					$agent_refund_amount = ($currency_conversion_rate*$refund_amount);//converting to agent currency

					//2.Add Transaction Log for the Refund
					$fare = -($refund_amount);//dont remove: converting to negative
					$domain_markup=0;
					$level_one_markup=0;
					$convinence = 0;
					$discount = 0;
					$remarks = 'hotel Refund was Successfully done';
					$this->domain_management_model->save_transaction_details('hotel', $app_reference, $fare, $domain_markup, $level_one_markup, $remarks, $convinence, $discount, $booking_currency, $currency_conversion_rate, $booked_user_id);

					// update agent balance
					$this->domain_management_model->update_agent_balance($agent_refund_amount, $booked_user_id);
				}
				//UPDATE THE REFUND DETAILS
				//Update Condition
				$update_refund_condition = array();
				$update_refund_condition['app_reference'] =	$app_reference;
				//Update Data
				$update_refund_details = array();
				$update_refund_details['refund_payment_mode'] = 			$refund_payment_mode;
				$update_refund_details['refund_amount'] =					$refund_amount;
				$update_refund_details['cancellation_charge'] = 			$cancellation_charge;
				$update_refund_details['refund_status'] = 					$refund_status;
				$update_refund_details['refund_comments'] = 				$refund_comments;
				$update_refund_details['currency'] = 						$booking_currency;
				$update_refund_details['currency_conversion_rate'] = 		$currency_conversion_rate;
				if($refund_status == 'PROCESSED'){
					$update_refund_details['refund_date'] = 				date('Y-m-d H:i:s');
				}
				$this->custom_db->update_record('hotel_cancellation_details', $update_refund_details, $update_refund_condition);
				
				$redirect_url_params['app_reference'] = $app_reference;
				$redirect_url_params['booking_source'] = $master_booking_details['booking_source'];
				$redirect_url_params['status'] = $status;
			}
		}
		redirect('hotel/cancellation_refund_details?'.http_build_query($redirect_url_params));
	}
	/**
	 * Jaganath
	 * Get supplier cancellation status
	 */
	public function update_supplier_cancellation_status_details()
	{
		$get_data = $this->input->get();
		if(isset($get_data['app_reference']) == true && isset($get_data['booking_source']) == true && isset($get_data['status']) == true && $get_data['status'] == 'BOOKING_CANCELLED'){
			$app_reference = trim($get_data['app_reference']);
			$booking_source = trim($get_data['booking_source']);
			$status = trim($get_data['status']);
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $status);
			if($booking_details['status'] == SUCCESS_STATUS){
				$master_booking_details = $booking_details['data']['booking_details'];
				$booking_customer_details = $booking_details['data']['booking_customer_details'][0];
				$cancellation_details = $booking_details['data']['cancellation_details'][0];
				$ChangeRequestId =		$cancellation_details['ChangeRequestId'];
				load_hotel_lib($booking_source);
				$response = $this->hotel_lib->get_cancellation_refund_details($ChangeRequestId, $app_reference);
				if($response['status'] == SUCCESS_STATUS){
					$cancellation_details = $response['data'];
					$this->hotel_model->update_cancellation_refund_details($app_reference, $cancellation_details);
				}
			}
		}
	}

	public function cancellation_refund_details()
	{
		$get_data = $this->input->get();

		if(isset($get_data['app_reference']) == true && isset($get_data['booking_source']) == true && isset($get_data['status']) == true && $get_data['status'] == 'BOOKING_CANCELLED'){
			$app_reference = trim($get_data['app_reference']);
			$booking_source = trim($get_data['booking_source']);
			$status = trim($get_data['status']);

			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $status);

			if($booking_details['status'] == SUCCESS_STATUS){
				$booked_user_id = intval($booking_details['data']['booking_details'][0]['user_details_id']);
				$booked_user_details = array();
				$is_agent = false;
				$user_condition[] = array('U.user_details_id' ,'=', $booked_user_id);
				// debug($booking_details);exit;
				$booked_user_details = $this->user_model->get_user_details($user_condition);
			// 	echo $this->db->last_query();
			// exit;
				// debug($booked_user_details);
				// exit;	
				if(valid_array($booked_user_details) == true){
					$booked_user_details = $booked_user_details[0];
					if($booked_user_details['user_type'] == B2B_USER){
						$is_agent = true;
					}else{
						$is_agent =false;
					}
				}
				$page_data = array();
				$page_data['booking_data'] = 		$booking_details['data'];
				$page_data['booked_user_details'] =	$booked_user_details;
				$page_data['is_agent'] = 			$is_agent;
				$this->load->view('hotel/cancellation_refund_details', $page_data);
			} else {
				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	}

}
