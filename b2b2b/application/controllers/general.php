<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package    Provab
 * @subpackage General
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */

class General extends CI_Controller {

	/*function dxb_hotels()
	 {
		$HotelSearchResult = (json_decode(file_get_contents('DXB.json'), true));
		$table = '<table border=1>';
		$table .= '<tr>';
		$table .= '<th>Sno</th>';
		$table .= '<th>Hotel Name</th>';
		$table .= '<th>Location</th>';
		$table .= '</tr>';
		$hotel_list = array();
		foreach ($HotelSearchResult['HotelSearchResult']['HotelResults'] as $k => $v) {
		$hotel_list[$v['HotelName']] = $v['HotelAddress'];
		}
		file_put_contents('dxb_hotel.json', json_encode($hotel_list));
		$index = 1;
		ksort($hotel_list);
		foreach ($hotel_list as $k => $v) {
		$table .= '<tr>';
		$table .= '<td>'.($index++).'</td>';
		$table .= '<td>'.$k.'</td>';
		$table .= '<td>'.$v.'</td>';
		$table .= '</tr>';
		}
		$table .= '</table>';
		}*/
	/*
	 function static_hotel()
	 {
		$this->load->model('hotel_model');
		$search_response = $this->hotel_model->get_static_response(28);
		debug($search_response);
		exit;
		}

		function static_bus()
		{
		$this->load->model('bus_model');
		$search_response = $this->bus_model->get_static_response(1);
		debug($search_response);
		exit;
		}

		function static_bus_seats()
		{
		$this->load->model('bus_model');
		$search_response = $this->bus_model->get_static_response(2);
		debug($search_response);
		exit;
		}

		function static_flight()
		{
		$this->load->model('flight_model');
		$search_response = $this->flight_model->get_static_response(533);
		debug($search_response);
		exit;
		}*/
	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
		$this->load->model('user_model');
		$this->load->model('Package_Model');
		$this->load->model('custom_db');
	}
	
	function test1(){
		$post=$this->input->post();
		$post['lang']='hi';
		$this->session->set_userdata('lang', $post['lang']);
		//echo $this->session->userdata('some_name');
	}	

	/**
	 * index page of application will be loaded here
	 */
	function index($default_view='')
	{		/*Package Data*/
		// debug($default_view);die;
		$data['caption'] = $this->Package_Model->getPageCaption('tours_packages')->row();
		$data['packages'] = $this->Package_Model->getAllPackages();
		$data['countries'] = $this->Package_Model->getPackageCountries();
		$data['package_types'] = $this->Package_Model->getPackageTypes();
		/*Banner_Images */
		$domain_origin = get_domain_auth_id();
		$page_data['banner_images'] = $this->custom_db->single_table_records('banner_images', 'image', array('added_by' => $domain_origin));
		/*Package Data*/

		$page_data['default_view'] = @$_GET['default_view'];
		$page_data['holiday_data'] = $data; //Package Data

		if (is_active_airline_module()) {
			$this->load->model('flight_model');
		}
		if (is_active_bus_module()) {
			$this->load->model('bus_model');
		}
		if (is_active_hotel_module()) {
			$this->load->model('hotel_model');
			$page_data['top_destination_hotel'] = $this->hotel_model->hotel_top_destinations();
		}
		if(is_active_package_module()){
			$this->load->model('package_model');
			$top_package = $this->package_model->get_package_top_destination();
			$page_data['top_destination_package'] =$top_package['data'];
			$page_data['total'] = $top_package['total'];

		}
		/*header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		 header("Cache-Control: post-check=0, pre-check=0", false);
		 header("Pragma: no-cache");*/
		$this->template->view('general/index', $page_data);
	}

	/**
	  * Pre Search for SightSeen
	  */
	function pre_sight_seen_search($search_id=''){
		$search_id = $this->save_pre_search(META_SIGHTSEEING_COURSE);
		$this->save_search_cookie(META_SIGHTSEEING_COURSE, $search_id);
		//Analytics
		// debug("df");die;
		$this->load->model('sightseeing_model');
		$search_params = $this->input->get();
		// debug($search_params);
		// exit;
		$this->sightseeing_model->save_search_data($search_params, META_SIGHTSEEING_COURSE);

		redirect('sightseeing/search/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
	}

	/*
  *Pre Transfer Search
  */
  function pre_transferv1_search($search_id=''){
    $search_id = $this->save_pre_search(META_TRANSFERV1_COURSE);
    $this->save_search_cookie(META_TRANSFERV1_COURSE, $search_id);
    //Analytics
    $this->load->model('transferv1_model');
    $search_params = $this->input->get();
    
    $this->transferv1_model->save_search_data($search_params, META_TRANSFERV1_COURSE);
    
    redirect('transferv1/search/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
  }

	/**
	 * Set Search id in cookie
	 */
	private function save_search_cookie($module, $search_id)
	{
		$sparam = array();
		$sparam = $this->input->cookie('sparam', TRUE);
		if (empty($sparam) == false) {
			$sparam = unserialize($sparam);
		}
		$sparam[$module] = $search_id;

		$cookie = array(
			'name' => 'sparam',
			'value' => serialize($sparam),
			'expire' => '86500',
			'path' => PROJECT_COOKIE_PATH
		);
		$this->input->set_cookie($cookie);
	}

	/**
	 * Pre Search For Flight
	 */
	function pre_flight_search($search_id='')
	{
		$search_params = $this->input->get();
		
		//Global search Data
		$search_id = $this->save_pre_search(META_AIRLINE_COURSE);
		$this->save_search_cookie(META_AIRLINE_COURSE, $search_id);

		//Analytics
		$this->load->model('flight_model');
		$this->flight_model->save_search_data($search_params, META_AIRLINE_COURSE);
		redirect('flight/search/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
	}

	/**
	 * Pre Search For Hotel
	 */
	function pre_hotel_search($search_id='')
	{
		//Global search Data
		$search_id = $this->save_pre_search(META_ACCOMODATION_COURSE);
		$this->save_search_cookie(META_ACCOMODATION_COURSE, $search_id);

		//Analytics
		$this->load->model('hotel_model');
		$search_params = $this->input->get();
		$this->hotel_model->save_search_data($search_params, META_ACCOMODATION_COURSE);

		redirect('hotel/search/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
	}

	    /**
    *Pre Villa Search
    **/
      function pre_villa_search($search_id = '') {
       $search_params = $this->input->get();
       // debug($search_params);
       // exit("219");
       if($search_params['villa_checkin'] && $search_params['villa_checkout']){
        $search_params['hotel_checkin'] = $search_params['villa_checkin'];
        $search_params['hotel_checkout'] = $search_params['villa_checkout'];
        unset($search_params['villa_checkin']);
        unset($search_params['villa_checkout']);
       }
        //Global search Data
        //debug($this->session);exit;
        $search_id = $this->save_pre_search(META_ACCOMODATION_COURSE);
        $this->save_search_cookie(META_ACCOMODATION_COURSE, $search_id);
        // debug($search_id);exit("1212");
        //Analytics
        $this->load->model('hotel_model');
       
        // debug($search_params);exit;
        $this->hotel_model->save_search_data($search_params, META_ACCOMODATION_COURSE);

        redirect('hotel/search/' . $search_id . '?' . $_SERVER['QUERY_STRING']);
    }


	/**
	 * Pre Search For Bus
	 */
	function pre_bus_search($search_id='')
	{
		//Global search Data
		$search_id = $this->save_pre_search(META_BUS_COURSE);
		$this->save_search_cookie(META_BUS_COURSE, $search_id);

		//Analytics
		$this->load->model('bus_model');
		$search_params = $this->input->get();
		$this->bus_model->save_search_data($search_params, META_BUS_COURSE);

		redirect('bus/search/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
	}

	/**
	 * Pre Search For Packages
	 */
	function pre_package_search($search_id='')
	{
		//Global search Data
		$search_id = $this->save_pre_search(META_PACKAGE_COURSE);
		redirect('tours/search'.$search_id.'?'.$_SERVER['QUERY_STRING']);
		break;
	}

	/**
	 * Pre Search used to save the data
	 *
	 */
	private function save_pre_search($search_type)
	{
		//Save data
		$search_params = $this->input->get();
		if($search_params['hotel_destination'] ==8222){
        $search_params['hotel_destination'] = 715;
         $search_params['city'] = 'Bangkok (Thailand)';
       }
        if($search_params['villa_checkin'] && $search_params['villa_checkout']){
        $search_params['hotel_checkin'] = $search_params['villa_checkin'];
        $search_params['hotel_checkout'] = $search_params['villa_checkout'];
        unset($search_params['villa_checkin']);
        unset($search_params['villa_checkout']);
       }

		$search_data = json_encode($search_params);
		$insert_id = $this->custom_db->insert_record('search_history', array('search_type' => $search_type, 'search_data' => $search_data, 'created_datetime' => date('Y-m-d H:i:s')));
		return $insert_id['insert_id'];
	}

	/**
	 * oops page of application will be loaded here
	 */
	public function ooops()
	{
		$this->template->view('utilities/404.php');
	}
	/*
	 *Activating User Account.
	 *Account get activated only when the url is clicked from the account_activation_mail
	 */

	function activate_account_status()
	{
		$origin = $this->input->get('origin');
		$unsecure = substr($origin,3);
		$secure_id = base64_decode($unsecure);
		$status = ACTIVE;
		$this->user_model->activate_account_status($status, $secure_id);
		redirect(base_url());
	}

	/**
	 *Email Subscribtion
	 *
	 */
	public function email_subscription()
	{
		$data = $this->input->get();

		$mail = $data['email'];
		$domain_key = get_domain_auth_id();
		$inserted_id = $this->user_model->email_subscribtion($mail,$domain_key);
		if(isset($inserted_id) && $inserted_id != "already") {
			$this->application_logger->email_subscription($mail);
			echo "success";
		} elseif($inserted_id=="already") {
			echo "already";
		} else {
			echo "failed";
		}
	}
	function cms($page_position='Bottom', $id)
	{
		if($id > 0) {
			$data = $this->custom_db->single_table_records('cms_pages','page_title,page_description,page_seo_title,page_seo_keyword,page_seo_description', array('page_id' => $id, 'page_position' => $page_position, 'page_status' => 1));
			$this->template->view('cms/cms',$data);

		} else {
			redirect('general/index');
		}
	}
	function offline_payment(){
		$params = $this->input->post();
		$gotback = $this->user_model->offline_payment_insert($params);
		$url = base_url().'index.php/general/offline_approve/'.$gotback['refernce_code'];
		//unset($gotback['refernce_code']);

		//mail function
			/*$mail_template	=	'<!DOCTYPE html>
								<html>
								<head>
									<title>Offline payment Travelomatix</title>
								</head>
								<body style="background:#EBFCFD">
								<h3>Offline payment</h3>
								<strong>HI,'.$params["data"][0]["value"].' please click below link to confirm the offline payment.</strong>
								<p>'.$url.'</p>

								</body>
								</html>';
			$email = $params['data'][2]['value'];
			$this->load->library('provab_mailer');
			$this->provab_mailer->send_mail($email, 'OFFLINE PAYMENT CONFIRMATION', $mail_template);*/

		print_r (json_encode($gotback['refernce_code']));
	}
	function offline_approve($code){//apporval by mail

		$result['data'] = $this->user_model->offline_approval($code);
		$this->template->view('general/pay',$result);
	}

	function test()
	{
		$string = 'RL   349   8060  ';
		$string = preg_replace('/\s+/', '', $string);
		echo $string;exit;
	}
	public function get_country_code_by_country_iso($country_ISO)
		{
			$query="SELECT country_code FROM api_country_list WHERE iso_country_code='".$country_ISO."'";
			$country_code = $this->custom_db->get_result_by_query($query);
			$country_code = $country_code[0]->country_code; 
			echo $country_code;  
		}
function get_state_list_by_country_ISO($country_ISO)
	{
		$query="SELECT abbreviation,name FROM province WHERE country='".$country_ISO."'";
		$state_list = $this->custom_db->get_result_by_query($query);
		echo json_encode($state_list); 
	}
	function get_city_list_by_country_ISO($country_ISO)
	{
		$city_list = $this->custom_db->get_city_list_by_country($country_ISO);
		echo json_encode($city_list); 
		//print_r($city_list);
	}
}

