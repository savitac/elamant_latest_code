<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
error_reporting(E_ALL);
ob_start();
ini_set('memory_limit', '-1');
class Transfer extends CI_Controller {
	
	function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Hotel_Model');
		$this->load->model('Cart_Model');
		$this->load->model('Xml_Model');
		$this->load->model('Markup_Model');
		$this->load->model('Transfer_Model');
		$this->load->model('Hotel_Model');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
		
		$current_url = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
        $current_url = base_url().$this->uri->uri_string(). $current_url;
        $url =  array(
            'continue' => $current_url,
        );
        $this->session->set_userdata($url);
		
		define('CANCEL_BUFFER', '7');
		
		$this->load->library('Ajax_pagination_hotel');
        $this->perPage = 1000;
		$this->module_id="19";
		$this->api_mode ="TEST";
	}
	
	function index(){
	   $requests = $this->input->get();
	   $request = json_encode($requests['transfer_request']);
	   $req = base64_encode($request);
	   $data['req'] = $req;
	   $data['request'] = json_decode($request);
	   $data['session_data'] = $requests['sid'];
	   $data['transfer_countries'] = $this->General_Model->getTransferCountries()->result();
	   $data['tranfer_list_code'] = $this->General_Model->gatTranferListCode()->result();
	   $api_det = $this->General_Model->get_api_details_by_id($this->module_id, $this->api_mode); 
	  
	   for($k=0;$k<count($api_det);$k++)
		{
		  $api_c[] = "'".$api_det[$k]->api_details_id."'";
		}
	   $data['api_det'] = implode(",",$api_c);
	   $this->load->view('transfer/results', $data);
	 }
	
	function search(){
		//if($_GET['transfer_country'] == '' || $_GET['transfer_city'] =='' || $_GET['transfer_city'] =='' || $_GET['t_st_date'] =='' || $_GET['pickup_code'] =='' ||$_GET['dropoff_code'] =='' ){ redirect(base_url().'dashboard','refresh'); }
		$request = $this->input->get();
		//echo"<pre>";print_r($request);die;
	
		if(count($request) > 0){
			$country_details = $this->General_Model->getTransferCountries($request['transfer_country'])->row();
			$city_details = $this->General_Model->getTransferCities($request['transfer_country'], $request['transfer_city'])->row();
			$pickupcode_description = $this->Transfer_Model->TransferCodeDetails($request['pickup_code'])->row();
			$dropoffcode_description = $this->Transfer_Model->TransferCodeDetails($request['dropoff_code'])->row();
			
			$data['transfer_request'] = array("transfer_country_code" => $request['transfer_country'],
			                          "transfer_country" => $country_details->country_name,
			                          "transfer_city_code" => $request['transfer_city'],
			                          "transfer_city" => $city_details->city_name,
			                          "start_date" => $request['t_st_date'],
			                          "no_of_travellers" => $request['travellers'],
			                          "pickup_code" => $request['pickup_code'],
			                          "pickup_code_desc" => $pickupcode_description->english,
			                          "dropoff_code" => $request['dropoff_code'],
			                          "dropoff_code_desc" => $dropoffcode_description->english,
			                          "nationality" => $request['nationality'],
			                           );
			$data['days'] = floor($absDateDiff/(60*60*24));
            $data['sid'] = $this->generate_rand_no().date("mdHis"); */
			//$query = http_build_query($data);
			$query = http_build_query($request);
            $url = base_url().'transfer/?'.$query;
		redirect($url);
			
		}else{
			redirect(base_url().'dashboard','refresh');
		}
		
	}
	
	function call_api($api_id = ''){
		if($api_id != ''){
		$session_data = $_REQUEST['sessiondata'];
	    $request = base64_decode($_REQUEST['request']);
	    $data['request'] = $request = json_decode($request);
	    if($session_data != ''){
		$this->Transfer_Model->clear_search_transfers($session_data);
		$this->Transfer_Model->clear_old_search_transfers();
		}
	    $api_det = $this->General_Model->get_api_details_id($api_id);
	    if($api_det->api_helper!='') {
			 
			    $this->load->helper($api_det->api_helper);
				$availabilityrequest='';     
				$request = $_REQUEST['request'];
				TransferAvailRQ($request,$api_id,$session_data); 
			}
	   
	 // $session_data = 'FTODZ8WLU0SU9YK6JRXU3FIR0824093046';
	  $this->fetch_search_result($request,$api_id,$session_data);
     }else{
		echo json_encode(array("status" => 2));
	 }
	}
	
	function fetch_search_result($request,$api_id,$session_data){
		$request_data  = base64_encode(json_encode($request));
		$tmp_data = $this->Transfer_Model->fetch_search_result($request,$session_data, $api_id);
		$result_count = count($tmp_data);
		if($tmp_data != ''){
			$total_h_count = count($tmp_data);
			
			$config['first_link']  = 'First';
			$config['div']         = 'transfer_result'; //parent div tag id
			$config['base_url']    = base_url().'transfer/ajaxPaginationData/'.$session_data.'/'.$request_data.'/'.$api_id;
			$config['total_rows']  = count($tmp_data);
			$config['per_page']    = $this->perPage;
			$this->ajax_pagination_hotel->initialize($config);
			$data['api_id']	= $api_id;
			$data['transfer_result']	= $tmp_data;
			if(count($tmp_data) > 0){
			$len = count($tmp_data)-1;
			$min_price = $tmp_data[0]->total_cost;
			$max_price = $tmp_data[$len]->total_cost;
			$transfer_search_result = $this->load->view('transfer/transfer_list', $data, true);
			}else{
			$transfer_search_result = $this->load->view('transfer/transfer_list', $data, true);	
			$min_price = 0;
			$max_price = 0;
			}
			
			$trans_type_array = array();
			for($res =0; $res < count($tmp_data); $res++){
			   $trans_type_array[] = array("transfer_type" => $tmp_data[$res]->transfer_type_code.'|||'.$tmp_data[$res]->transfer_type ); 
			                                                    
			}
			
			$trans_types = array_unique($trans_type_array, SORT_REGULAR);
		    $result_array = array(
		        'status'                 => 1,
				'transfer_search_result' 	=> $transfer_search_result,
				'total_result' 			=> $result_count,
				'session_id'			=> $session_data,
				'requeststring'			=> $request,
				'api_id'				=> $api_id,
				'trans_types' => $trans_types,
				'min_val' => $min_price, 
				'max_val' => $max_price
          	); 
		   print json_encode($result_array); exit;
		}
     }
	
	function generate_rand_no($length = 24) {
        $alphabets = range('A','Z');
        $numbers = range('0','9');
        $final_array = array_merge($alphabets,$numbers);
         $id = '';
        while($length--) {
          $key = array_rand($final_array);
          $id .= $final_array[$key];
        }
        return $id;
    }
    
   function transfer_static_data(){
	 $this->db->select('city_code as "transfer_city_code"');
		$this->db->from('city_code_gta_transfer');
		 $this->db->group_by('city_code');
	 $result = $this->db->get()->result();
	  for($city =0; $city < count($result); $city++){
		         
			    $city1 = $result[$city]->transfer_city_code;
			    $this->load->helper('gtatransfer_static_helper');
				$availabilityrequest='';   
				TransferAvailRQ($city1,6);
				
	  }
	}
	
	function transfer_station_static_data(){
	 $this->db->select('city_code as "transfer_city_code", country_code');
	 $this->db->from('city_code_gta_transfer');
	 $this->db->group_by('city_code');
	 $result = $this->db->get()->result();
	  for($city =0; $city < count($result); $city++){
		        $city1 = $result[$city]->transfer_city_code;
			    $country_code = $result[$city]->country_code;
			    $this->load->helper('gtatransfer_static_helper');
				$availabilityrequest='';   
				TransferAvailStationRQ($city1, $country_code,6);
		 }
	}
	
	function add_to_cart($temp_result_id1, $item_code1){
		
		$temp_result_id = json_decode(base64_decode($temp_result_id1));
		$item_code = json_decode(base64_decode($item_code1));
		
		 if($temp_result_id != '' && $item_code != ''){
		 $temp_result = $this->Transfer_Model->fetch_temp_result($temp_result_id, $item_code);
		 if(count($temp_result)> 0){
			
			 $data['session_id'] = $session_id = $temp_result[0]->session_id;
			 $data['api_id'] = $temp_result[0]->api_id;
			 $data['item_code'] = $item_code = $temp_result[0]->item_code;
			 
			 $this->Transfer_Model->clear_temp_cart($session_id, $item_code);
			 $this->Transfer_Model->clear_temp_global($session_id);
			 $data['travel_start_date'] = $temp_result[0]->travel_start_date;
			 $data['travel_end_date'] = $temp_result[0]->travel_end_date;
			 $data['search_city_code'] = $temp_result[0]->city_code;
			 $data['search_city_name'] = $temp_result[0]->city_name;
			 $data['item_description'] = $temp_result[0]->item_description;
			 $data['pickup_code'] = $temp_result[0]->pickup_code;
			 $data['pickup_code_description'] = $temp_result[0]->pickup_code_description;
			 $data['pickup_city_code'] = $temp_result[0]->pickup_city_code;
			 $data['pickup_city_code_description'] = $temp_result[0]->pickup_city_code_description;
			 $data['dropoff_code'] = $temp_result[0]->dropoff_code;
			 $data['dropoff_code_description'] = $temp_result[0]->dropoff_code_description;
			 $data['dropoff_city_code'] = $temp_result[0]->dropoff_city_code;
			 $data['dropoff_city_code_description'] = $temp_result[0]->dropoff_city_code_description;
			 $data['approximate_time'] = $temp_result[0]->approximate_time;
			 $data['approximate_transfer_time'] = $temp_result[0]->approximate_transfer_time;
			 $data['vehicle_code'] = $temp_result[0]->vehicle_code;
			 $data['maximum_luggage'] = $temp_result[0]->maximum_luggage;
			 $data['maximum_passengers'] = $temp_result[0]->maximum_passengers;
			 $data['no_of_passengers'] = $temp_result[0]->no_of_passengers;
			 $data['no_of_vehicles'] = $temp_result[0]->no_of_vehicles;
			 $data['vehicle_name'] = $temp_result[0]->vehicle_name;
			 $data['vehicle_details'] = $temp_result[0]->vehicle_details;
			 $data['api_currecny'] = $temp_result[0]->api_currecny;
			 $data['net_rate'] = $temp_result[0]->api_price;
			 $data['total_cost'] = $temp_result[0]->total_cost;
			 $data['admin_markup'] = $temp_result[0]->admin_markup;
			 $data['admin_baseprice'] = $temp_result[0]->admin_baseprice;
			 $data['my_markup'] = $temp_result[0]->my_markup;
			 $data['agent_baseprice'] = $temp_result[0]->agent_baseprice;
			 $data['my_agent_Markup'] = $temp_result[0]->my_agent_Markup;
			 $data['sub_agent_baseprice'] = $temp_result[0]->sub_agent_baseprice;
			 $data['my_b2b2b_markup'] = $temp_result[0]->my_b2b2b_markup;
			 $data['b2b2b_baseprice'] = $temp_result[0]->b2b2b_baseprice;
			 $data['service_charge'] = $temp_result[0]->service_charge;
			 $data['gst_charge'] = $temp_result[0]->gst_charge;
			 $data['tax_charge'] = $temp_result[0]->tax_charge;
			 $data['caneclation_policy'] = $temp_result[0]->caneclation_policy;
			 $data['cancellation_commisionable_nettrate'] = $temp_result[0]->caneclation_policy;
			 $data['cancellation_till_date'] = $temp_result[0]->cancellation_till_date;
			 $data['transfer_request'] = $temp_result[0]->transfer_request;
			 $data['SITE_CURR'] =  BASE_CURRENCY;
			 $data['API_CURR'] = $temp_result[0]->api_currecny;
			 $transfer_cart_id = $this->Transfer_Model->add_transfer_cart($data);
			 if($this->session->userdata('user_details_id')){
				$user_type =$this->session->userdata('user_type');
				$user_id = $this->session->userdata('user_details_id');
			}
			else {
				$user_type = '';
				$user_id = '';
			}
			 $cart_global = array(
				'parent_cart_id' => 0,
				'referal_id' => $transfer_cart_id,
				'product_id' => 19,
				'user_type' => $user_type,
				'user_id' => $user_id,
				'session_id' => $session_id,
				'site_currency' => BASE_CURRENCY,
				'total_cost' => $temp_result[0]->total_cost,
				'ip_address' =>  $this->input->ip_address(),
				'timestamp' => date('Y-m-d H:i:s')
			);
			$cart_global_id = $this->Hotel_Model->insert_cart_global($cart_global);
			$URL = base_url().'booking/index/'.$session_id;
			$response_array = array("status"=>1, "redirect_url"=>$URL);
			echo json_encode($response_array);
		 }else{
			$URL = base_url();
			$response_array = array("status"=>2, "redirect_url"=>$URL);
			echo json_encode($response_array);
		 }
		 }else{
			$URL = base_url();
			$response_array = array("status"=>2, "redirect_url"=>$URL);
			echo json_encode($response_array);
		 }
	}
	
	function get_hotel_address($hotel_code, $city_code){
		$hotel_details = $this->Hotel_Model->get_hotel_address($hotel_code, $city_code);
		if(count($hotel_details) > 0){
			$address = explode(",",$hotel_details->Address);
			$data = array("address1" => $address[0],"address2" => $address[1],"city" => $address[1],"telephone" => $hotel_details->Telephone);
			echo  json_encode($data);
		}
	}
}
