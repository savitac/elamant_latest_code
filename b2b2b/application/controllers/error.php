<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
error_reporting(0);
class Error extends CI_Controller {	
	function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Validation_Model');
		$this->load->model('Email_Model');
		$this->load->model('Account_Model');
		if(isset($_SESSION['language']) && $_SESSION['language']!='') {
			$_SESSION['language'] = 'english';
		}else{
			$_SESSION['language'] = 'english';
		}
        $this->lang->load($_SESSION['language'],'Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	}
	

	public function access_denied(){
		$this->load->view('dashboard/access_denied');
	}

}
?>
