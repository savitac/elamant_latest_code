<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
// error_reporting(E_ALL);
ob_start();
class Support extends CI_Controller {
	
    public function __construct(){
		parent::__construct();
	    $this->check_isvalidated();	
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Account_Model');
		$this->load->model('Email_Model');
		$this->load->model('Settings_Model');
		$this->load->model('Support_Model');
		$this->load->model('Usermanagement_Model');
		$this->lang->load('english','Dynamic_Languages');
		$this->load->library('session'); 
		$this->TravelLights = $this->lang->line('TravelLights');
	}
	
	private function check_isvalidated()
	{
		if($this->session->userdata('user_details_id'))
		{
			$user_id = $this->session->userdata('user_details_id');
			$controller_name = $this->router->fetch_class();
			$function_name = $this->router->fetch_method();
		    $this->load->model('Privilege_Model');
            $sub_admin_id = $this->session->userdata('admin_id');
            $privilege_details = $this->Privilege_Model->get_user_privileges($user_id,$controller_name,$function_name, $function_parameter = 1)->row();
          
               if(count($privilege_details)  == 0)
				{	
				  redirect('error/access_denied');
				}
		}
	 }
	

	//Support
 function index()
  {

	$domain_id = 1;
	$b2b_type = $this->session->userdata('user_type');
	// echo $this->session->userdata('user_type');
	// exit("212");
	$b2b_id = $this->session->userdata('user_details_id');
	$data['support'] = $this->Support_Model->get_support_list($b2b_id, $b2b_type, $domain_id);
	// debug($data);
	// exit;
	$data['support_pending'] = $this->Support_Model->get_support_list_pending($b2b_id, $b2b_type, $domain_id);
	$data['support_sent'] = $this->Support_Model->get_support_list_sent($b2b_id, $b2b_type, $domain_id);
	$data['support_close'] = $this->Support_Model->get_support_list_close($b2b_id, $b2b_type, $domain_id);
	$data['support_ticket_subject'] = $this->Support_Model->get_support_subject_list();
	$this->load->view('dashboard/support/support',$data);  
  }

  	function view_ticket($id) {
        $data['status'] = '';
        $data['ticket'] = $this->Support_Model->get_support_list_id($id);
        $data['ticketrow'] = $this->Support_Model->get_support_list_id_row($id);
        $data['id'] = $id;
        $this->load->view('dashboard/support/view_ticket', $data);
    }
     public function download_file($file) {
        $this->load->helper('download');
        $name = base64_decode($file);
        $data = file_get_contents($name); // Read the file's contents
        $name1 = explode('support', $name); 
        $test = substr($name1[1],0); //print_r($name1); exit();
        if ($data != '') {
            force_download($test, $data);
        } else {
            redirect('support', 'refresh');
        }
    }
 	function reply_ticket($id) {
	        $domain = $this->input->post('domain');
	        $usertype = $this->input->post('user_type');
	        $user = $this->input->post('user_id');
	        $sub = $this->input->post('subject');
	        $message = $this->input->post('textcounter');
	        $support_ticket_id = $this->input->post('support_ticket_id');
	        $config['upload_path'] = 'cpanel/uploads/support/';
	        $config['allowed_types'] = '*';
	        $this->load->library('upload', $config);
	        if ($_FILES['file_name']['name'] != '') {
	
	            if (!$this->upload->do_upload('file_name')) {
	                $error = array('error' => $this->upload->display_errors());
	                $data['status'] = '<div class="alert alert-block alert-danger">
	                                          <a href="#" data-dismiss="alert" class="close">×</a>
	                                          <h4 class="alert-heading">Attachment File Failed!</h4>
	                                          ' . $error['error'] . '
	                                        </div>';
	                $data['ticket'] = $this->Support_Model->get_support_list_id($id);
	                $data['ticketrow'] = $this->Support_Model->get_support_list_id_row($id);
	                $data['id'] = $id;
	                $this->load->view('dashboard/support/view_ticket', $data);
	            }
	            $cc = $this->upload->data('file');
	
	            $image_path = base_url().'cpanel/uploads/support/'. $cc['file_name'];
	        } else {
	            $image_path = '';
	        }
	
	        
	        $users = $this->Usermanagement_Model->getDepositAgentList($user);
			$supportuser = $users[0]->user_email;
			$user_account_number = $users[0]->user_account_number;
			
            $this->Support_Model->add_new_support_ticket_updates($support_ticket_id, $domain, $usertype, $user, $sub, $message, $image_path);
            $this->Email_Model->sendAgentSupportAlertMailReply($_POST,$supportuser,$user_account_number);
	        redirect('support/view_ticket/' . $id, 'refresh');
    	}
	  function add_new_ticket() 
	  {
	  	
        ini_set("display_errors", "on");
	    
        $this->form_validation->set_rules('message', 'Message', 'required');

        if ($this->form_validation->run() == FALSE) {

            redirect(base_url().'support', 'refresh');
        } else {
          

            $domain = 0;
           
            if ($this->session->userdata('user_details_id')) 
            {
                $usertype = $this->session->userdata('user_type');
                $user = $this->session->userdata('user_details_id');
               

            }

            $sub = $this->input->post('sub');
            $message = $this->input->post('message');

            $config['upload_path'] ='../../b2b2b/uploads/support/';
            
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);

            if ($_FILES['file_name']['name'] != '') 
            {
                if (!$this->upload->do_upload('file_name')) 
                {
                    $error = array('error' => $this->upload->display_errors());
                    $data['status'] = '<div class="alert alert-block alert-danger">
                                          <a href="#" data-dismiss="alert" class="close">×</a>
                                          <h4 class="alert-heading">Attachment File Failed!</h4>
                                          ' . $error['error'] . '
                                        </div>';
                    $data['support_ticket_subject'] = $this->Support_Model->get_support_subject_list();
                   
                    redirect(base_url().'dashboard', 'refresh');
                }
                $cc = $this->upload->data('file');
                $image_path = ASSETS.'uploads/support/'. $cc['file_name'];
            } else {

                $image_path = '';
            }
            $users = $this->Usermanagement_Model->getDepositAgentList($user);
			$supportuser = $users[0]->user_email;
			$user_account_number = $users[0]->user_account_number;
			// echo '<pre>'; print_r($message); exit();
            $this->Support_Model->add_new_support_ticket($domain, $usertype, $user, $sub, $message, $image_path);
            // $this->Email_Model->sendAgentSupportAlertMail($_POST,$supportuser,$user_account_number);
            $data['status'] = '<div class="alert alert-block alert-success">
                                      <a href="#" data-dismiss="alert" class="close">×</a>
                                      <h4 class="alert-heading">Support Ticket Registered Successfully</h4></div>';
           
            redirect(base_url() . 'support');
        }
    }

    
    function close_ticket($sid) {
        $this->Support_Model->close_ticket($sid);
        redirect('support', 'refresh');
    }
	function delete_ticket($id) {
        $wheres = "support_ticket_id = '$id'";
        $this->db->delete('support_ticket', $wheres);
        $wheres = "support_ticket_id = '$id'";
        $this->db->delete('support_ticket_history', $wheres);
        redirect('support', 'refresh');
    }
	//End Of Support
	
}
?>
