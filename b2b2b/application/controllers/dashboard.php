<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
if(session_status() == PHP_SESSION_NONE){ session_start(); }
error_reporting(0);
class Dashboard extends CI_Controller {	
	function __construct(){
		parent::__construct();	
		$this->check_isvalidated();
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Account_Model');
		$this->load->model('Package_Model');
		$this->load->model('db_cache_api');
		$this->load->model('custom_db');
		$this->check_login();
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
		$this->current_module = $this->config->item('current_module');
		
	}
	
	private function check_login()
	{
		if($this->session->userdata('user_logged_in')!=1)
		{
			redirect(base_url()); 
		}  
	}
	private function check_isvalidated()
	{
		if($this->session->userdata('user_details_id'))
		{
			$user_id = $this->session->userdata('user_details_id');
			$controller_name = $this->router->fetch_class();
			$function_name = $this->router->fetch_method();
			$this->load->model('Privilege_Model');
            $sub_admin_id = $this->session->userdata('admin_id');
            $privilege_details = $this->Privilege_Model->get_user_privileges($user_id,$controller_name,$function_name)->row();
          
			   if(count($privilege_details)  == 0)
				{	
					redirect('error/permission_denied');
				}else{
					if($privilege_details->dashboard_url != 'dashboard'){
						redirect($privilege_details->dashboard_url);
					}
				}
		}
	 }
	 
	function index(){ 
	   $userid = $this->session->userdata('user_details_id'); 
	   $user_type = $this->session->userdata('user_type'); 
	   $domain_id = $this->session->userdata('domain_id'); 
		$dashboard['products'] = $this->Dashboard_Model->getProducts();
		$dashboard['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
		if($user_type==2)
		{	
		$dashboard['flight_booking']=$this->Dashboard_Model->get_all_flight_booking($userid,$user_type,$domain_id);  
		 $dashboard['hotel_booking']=$this->Dashboard_Model->get_all_hotel_booking($userid,$user_type,$domain_id); 
		 $dashboard['flight_booking_confirm']=$this->Dashboard_Model->get_all_flight_booking_confirm($userid,$user_type,$domain_id);
		 $dashboard['hotel_booking_confirm']=$this->Dashboard_Model->get_all_hotel_booking_confirm($userid,$user_type,$domain_id);  
		}
		else
		{
		$dashboard['flight_booking']=$this->Dashboard_Model->get_all_flight_booking_sub($userid,$user_type,$domain_id);
		$dashboard['hotel_booking']=$this->Dashboard_Model->get_all_hotel_booking_sub($userid,$user_type,$domain_id);
		$dashboard['flight_booking_confirm']=$this->Dashboard_Model->get_all_flight_booking_sub_confirm($userid,$user_type,$domain_id);
		$dashboard['hotel_booking_confirm']=$this->Dashboard_Model->get_all_hotel_booking_sub_confirm($userid,$user_type,$domain_id);
		} 
		$dashboard['transfer_countries'] = $this->General_Model->getTransferCountries()->result();
		$dashboard['tranfer_list_code'] = $this->General_Model->gatTranferListCode()->result();
		$dashboard['top_deals'] = $this->Dashboard_Model->getTopDeals($userid);
		$dashboard['inner_banner'] = $this->Dashboard_Model->getInnerBanner($userid);
		$dashboard['side_banner'] = $this->Dashboard_Model->getSideBanner($userid);
		/*Package Data*/
		$data['caption'] = $this->Package_Model->getPageCaption('tours_packages')->row();
		$data['packages'] = $this->Package_Model->getAllPackages($domain_id);
		$data['countries'] = $this->Package_Model->getPackageCountries($domain_id);
		$data['package_types'] = $this->Package_Model->getPackageTypes($domain_id); 
		$dashboard['holiday_data'] = $data; //Package Data
		$this->load->view('dashboard/index', $dashboard);
	}
	
	function roomCombination() {
		if(!empty($this->input->post())) {
			$data['RACDetails'] = $this->input->post();
			//~ echo "data: <pre>";print_r($data['RACDetails']);exit;
			$template = $this->load->view('hotel/room_adult_child_combination',$data,true);
		}
		else {
			$template = 'Not Found Template'; # Todo
		}
		echo json_encode($template);
	}
	
	function getTransferCities($country_code){
		if($country_code != ''){
			$cities = $this->General_Model->getTransferCities($country_code)->result();
			$options = '<option value="">Select Cities</option>';
			for($city =0; $city < count($cities); $city++){
			   $options .= '<option value="'.$cities[$city]->transfer_city_code.'">'.$cities[$city]->city_name.'</option>';
			}
			echo json_encode(array("option"=>$options)); exit;
		}
	}

	function Products(){
		$userid = $this->session->userdata('user_details_id');
		$products['products'] = $this->Account_Model->getHeaderProducts($userid);

		$this->load->view('dashboard/static/products',$products);
	}

	function Services(){
		$userid = $this->session->userdata('user_details_id');
		$service['services'] = $this->Account_Model->getHeaderServices($userid);
		
		$this->load->view('dashboard/static/services',$service);
	}

	function AboutUs(){
		$userid = $this->session->userdata('user_details_id');
		$service['aboutus'] = $this->Account_Model->getHeaderAboutus($userid);
		//echo '<pre>'; print_r($service['aboutus']); exit();
		$this->load->view('dashboard/static/aboutus',$service);
	}

	function Contact(){
		$userid = $this->session->userdata('user_details_id');
		$contact['contact'] = $this->Account_Model->getHeaderContact();
		//echo '<pre>'; print_r($service['aboutus']); 
		$this->load->view('dashboard/static/contact',$contact);
	}
	function carrers(){
		$userid = $this->session->userdata('user_details_id');
		$carrer['carrer'] = $this->Account_Model->getFooterCarrer($userid);
		//echo '<pre>'; print_r($carrer['carrer']); 
		$this->load->view('dashboard/static/carrers',$carrer);
	}

	function termsnconditions(){
		$userid = $this->session->userdata('user_details_id');
		$terms['terms'] = $this->Account_Model->getFootertermsnconditions($userid);
		//echo '<pre>'; print_r($service['aboutus']); 
		$this->load->view('dashboard/static/termsnconditions',$terms);
	}

	function createContact(){ //echo '<pre>'; print_r($_POST); exit();
		$this->Account_Model->addContactDetails($_POST);
		redirect('dashboard','refresh');
	}

	// Function to change language of the website
	public function change_language(){
		$language = $this->input->post('language');
		$_SESSION['TheChinaGap']['language'] = $language; //echo 'sanjay'; print_r($_SESSION['TheChinaGap']['language']); exit();
        $response = array(
        	'status' => 1
        );
        echo json_encode($response);
	}

	//End Of Language
	//currency
	public function change_currency(){
		$code = $this->input->post('code');
		//$icon = $this->input->post('icon');
		$_SESSION['currency']		= $code;
		$currency = $this->General_Model->getCurrencyList($code);
		$_SESSION['currency_value'] = $currency[0]->value; 
		
        $response = array(
        	'status' => 1
        );
        
        echo json_encode($response);
	}
}
?>
