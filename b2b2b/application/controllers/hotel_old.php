<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); } 
error_reporting(-1);
ini_set('memory_limit', '-1');
class Hotel extends CI_Controller {
    function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Hotel_Model');
		$this->load->model('Cart_Model');
		$this->load->model('Xml_Model');
		$this->load->model('Markup_Model');
		$this->lang->load('english','Dynamic_Languages');
		$this->load->model ( 'Sitemanagement_Model' );
		
        $this->TravelLights = $this->lang->line('TravelLights');
		
		$current_url = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
        $current_url = base_url().$this->uri->uri_string(). $current_url;
        $this->session->set_userdata(array('continue' => $current_url));
		
		$this->load->library('Ajax_pagination_hotel');
        $this->perPage = 1000;
		$this->module = "Hotels";
	}

	public function get_hotel_city_suggestions() {
        $term = $this->input->get('term');
        $term = trim(strip_tags($term));
        $hotels = $this->Hotel_Model->get_hotels_list($term);
        //~ echo "data: <pre>";print_r($hotels);exit;
        $result = array();
       
       foreach ($hotels as $hotel) {
            $apts['type'] = 'City';
            
            if($hotel->country_code != '') {
				$country = ",".$hotel->country; 
				$country_val = ",".$hotel->country; 
			}else{
			    $country = '';
			    $country_val ='';
			}
			if($hotel->state != '') {
				$state = ",".$hotel->state; 
				$state_val = ",".$hotel->state; 
			} else {
				$state ='';
				$state_val =""; 
			}
            $apts['label'] = $hotel->city.$state.$country;
            $apts['value'] = $hotel->city.$state_val.$country_val;
            $apts['id'] = $hotel->id;
            $result[] = $apts;
        }
        
        echo json_encode($result);
	}
	
	public function index(){ 
        $request = $this->input->get();
        $data['request_params'] = $request;
        $request_json = json_encode($request);
        $data['request'] = $request_json;
		$data['adult_count'] = $request['adult_count'];
        $data['child_count'] = $request['child_count'];
        $data['days'] = $request['days'];
        $data['city_data'] = array('city_origin'=>$request['city_origin'],
        						'city_name'=>$request['city_name'],
        						'city_code'=>$request['city_code'],
        						'country_name'=>$request['country_name'],
        						'country_code'=>$request['country_code']
        					);
		$data['session_data'] = $request['sid'];
		$api_det = $this->General_Model->get_api_details($this->module); 
        $data['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
        
        for($k=0;$k<count($api_det);$k++)
		{
		  $api_c[] = "'".$api_det[$k]->api_details_id."'";
		}

		$data['api_det'] = implode(",",$api_c);
		$data['hotel_checkin'] = $request['hotel_checkin'];
		$data['hotel_checkout'] = $request['hotel_checkout'];
		$this->load->view('hotel/results', $data);
    }
    
    public function search() { 
		
		if ($_GET['city'] == '' || $_GET['checkin_date'] =='' || $_GET['checkout_date'] =='' || 
			$_GET['rooms'] == '' || $_GET['adult'][0] =='' || $_GET['adult'][0] ==0) { 
			redirect(base_url().'home','refresh'); 
		}	
		$request = $this->input->get();
	    $check_in = $this->input->get('checkin_date');
        $check_out = $this->input->get('checkout_date');
		$city_data = $this->Hotel_Model->get_city_data(trim($this->input->get('city')))->row();    
	    
	    if(isset($city_data->id))
    	{ 
        	$data['city_origin'] =  $city_data->id;
         	$data['city_name'] =  $city_data->city_name;
         	  
	        if(!empty($city_data->gta_city_code)) {
				$data['city_code'] = $city_data->gta_city_code;
			}
			$data['country_code'] =  $city_data->country_code;
         	$data['country_name'] =  $city_data->country;
	        	
	        $data['hotel_checkin'] = date('Y-m-d' , strtotime($check_in) ) ;
	        $data['hotel_checkout'] = date('Y-m-d' , strtotime($check_out) );
	        
	        $data['rooms'] = $this->input->get('rooms');
	        $data['adult'] = $this->input->get('adult');
	        $data['child'] = $this->input->get('child');
	        $data['nationality'] = $this->input->get('nationality');

			$age1 = $this->input->get('child_0_Age');
			$age2 = $this->input->get('child_1_Age');
			$age3 = $this->input->get('child_2_Age');
			$age4 = $this->input->get('child_3_Age');


	        $data['childage1'] = (isset($request['child_0_Age']) && count($request['child_0_Age']) > 0) ? $request['child_0_Age'] : NULL ;
	        $data['childage2'] = (isset($request['child_1_Age']) && count($request['child_1_Age']) > 0) ? $request['child_1_Age'] : NULL ;
	        $data['childage3'] = (isset($request['child_2_Age']) && count($request['child_2_Age']) > 0) ? $request['child_2_Age'] : NULL ;
	        $data['childage4'] = (isset($request['child_3_Age']) && count($request['child_3_Age']) > 0) ? $request['child_3_Age'] : NULL ;


	        $data['childAges'] = array();
			if(isset($data['childage1'])) {
	            array_push($data['childAges'], $data['childage1']);           
	        } else {
	            array_push($data['childAges'], array(0));           
	        }

	        if(isset($data['childage2'])) {
	            array_push($data['childAges'], $data['childage2']);           
	        }else {
	            array_push($data['childAges'], array(0));           
	        }
	        
	        if(isset($data['childage3'])) {
	            array_push($data['childAges'], $data['childage3']);           
	        }else {
	            array_push($data['childAges'], array(0));           
	        }
	        
	        if(isset($data['childage4'])) {
	            array_push($data['childAges'], $data['childage4']);           
	        }else {
	            array_push($data['childAges'], array(0));           
	        }
			
			$data['adult_count'] = array_sum($request['adult']);
	        $data['child_count'] = array_sum($request['child']); 
	        $checkin_date = strtotime($check_in);
	        $checkout_date = strtotime($check_out);

	        $absDateDiff = abs($checkout_date - $checkin_date);

	        $data['days'] = floor($absDateDiff/(60*60*24));
	        $data['sid'] = $this->generate_rand_no().date("mdHis");
	      //  echo "data: base_url() <pre>";print_r($data);exit;
	        $query = http_build_query($data);
	        $url = base_url().'hotel/index?'.$query; 
	        redirect($url);
    	}
    }
    
	function call_api($api_id) {
		//echo "Api Id: <pre>";print_r($api_id);exit;
		$session_data = $_REQUEST['sessiondata'];
		
        $cache = $this->Hotel_Model->checkcache($session_data);
		$tmp_data = $this->Hotel_Model->fetch_search_result('',$session_data, $api_id);   
        
		// if($cache == 0){ 
			if($api_id == 1 || $api_id == 7){
			 $this->Hotel_Model->clear_prev_result();   
			}
				   
			$request = base64_decode($_REQUEST['request']);
			$data['request'] = $request = json_decode($request);
			$api_det = $this->General_Model->get_api_details_id($api_id);
			if($api_det->api_helper!='') {
				$this->load->helper($api_det->api_helper);
				$availabilityrequest='';     
				$request = $_REQUEST['request'];
				HotelValuedAvailRQ($request,$api_id,$session_data); 
			}
		//}
		$this->fetch_search_result($request,$api_id,$session_data);
    }
	public function fetch_search_result($request,$api_id,$session_data) {		
		$data['request'] = json_decode(base64_decode($request));
		$tmp_data = $this->Hotel_Model->fetch_search_result($data['request'],$session_data, $api_id);
		//~ echo "data: <pre>";print_r($tmp_data);exit;
		
		if($tmp_data!=''){
            $total_h_count = count($tmp_data['result']);
			$config['first_link']  = 'First';
			$config['div']         = 'hotel_result'; //parent div tag id
			$config['base_url']    = base_url().'hotel/ajaxPaginationData/'.$session_data.'/'.$request.'/'.$api_id;
			$config['total_rows']  = $total_h_count;
			$config['per_page']    = $this->perPage;
			$this->ajax_pagination_hotel->initialize($config);
			$data['api_id']	= $api_id;
			$data['result'] = $this->Hotel_Model->get_last_response($session_data,array('limit'=>$this->perPage));
			//~ echo "data: <pre>";print_r($data['result']);exit;
		   
			$data['hotel_count'] =  $total_h_count;
			if (!empty($data['result'])  ) {   
				$hotel_search_result = $this->load->view('hotel/hotel_list', $data, true);
			} else { 
				$data['image_url'] = base_url()."assets/images/no_result.png";
				$data['caption'] = "Sorry, no hotels found.";
				$data['message'] = "Please try after sometime.";
				$data['back_to_home_flag'] = false;
				$hotel_search_result = $this->load->view('general/error_info', $data, true);
			}
		   
			// Inclucsion List Point Related Part Begins
			$ca_airlines1 	= array();
			$loc_names 		= array();
			$ammenty_names 	= array();
			$resultType 	= $tmp_data['result'];
			 
			foreach ($resultType as $user) {
				$usernames  	= json_decode($user->locations);
				if($usernames!='')
				{
				foreach($usernames as $usernamesss){
					//$ammenty_names[] = $ammenity->NAME;
					$loc_names[] = $usernamesss;
				}
				}
				
				$categorytype[] = $user->star_rating;
				//$ammenties = $this->hotel_model->get_facility_details_hotel($user->hotel_code)->result(); 
				$ammenties = json_decode($user->hotel_facility);

                //var_dump( json_decode($user->hotel_facility ));
				
				if($ammenties!='')
				{
				foreach($ammenties as $ammenity){
					//$ammenty_names[] = $ammenity->NAME;
					$ammenty_names[] = $ammenity;
				}
				}
			}
			//~ echo "loc_names: <pre>";print_r($loc_names);
			$all_locations = array_values(array_filter($loc_names));
			$all_locations_array = array();
			for($loc_iter = 0; $loc_iter < count($all_locations);$loc_iter++) {
				$all_locations_array[] = ucwords(strtolower($all_locations[$loc_iter]));
			}
			
			
			//~ echo "all_locations: <pre>";print_r($all_locations);exit;
            $ammenity_array = array();
			$category_array1 	= array_unique($categorytype);
			$inclusion_array1 	= array_unique($loc_names);
			$inclusion_array2 	= $loc_names;
			$ammenities_array1 	= array_unique($ammenty_names);
			//~ $ammenities_array1 	= $ammenty_names;
			$inclusion_array=array();
			foreach ($inclusion_array1 as $inclusion_array11) {
				if (!in_array(ucwords(strtolower(trim($inclusion_array11)), $inclusion_array))) {
					$inclusion_array[] = ucwords(strtolower($inclusion_array11));
                }
			}
			//~ echo "inclusion_array: <pre>";print_r($inclusion_array);exit;
            $all_inclusion_array=array();
            foreach ($inclusion_array2 as $inclusion_array11) {
				if (!in_array(ucfirst(strtolower($inclusion_array11), $all_inclusion_array))) {
					$all_inclusion_array[] = ucwords(strtolower($inclusion_array11));
				}
			}
			
			foreach ($categorytype as $category_array11)
				$cat_array[] = $category_array11;
				
			foreach ($ammenities_array1 as $ammenities_array11)
				$ammenity_array[] = $ammenities_array11;   
				
			//~ echo "ammenity_array: <pre>";print_r($ammenity_array);
			//~ echo "inclusion_array: <pre>";print_r($inclusion_array);
			//~ echo "all_inclusion_array: <pre>";print_r($all_inclusion_array);exit; 
			
			$inclusion_array = array_values(array_unique($inclusion_array));
				
			$result_array = array(
				'hotel_search_result' 	=> $hotel_search_result,
				'total_result' 			=> $total_h_count,
				'min_val' 				=> floor($tmp_data['minVal']),
				'max_val' 				=> ceil($tmp_data['maxVal']),
				'inclusion' 			=> $inclusion_array,
				'areas' 				=> $all_inclusion_array,
				'locations' 			=> $all_locations_array,
				'ammenity' 				=> $ammenity_array,
				'category' 				=> $cat_array,
				'hotelRecommendatPrice' => '',
				'priceCount' 			=> '',
				'PriceFlight' 			=> '',
				'total_pages' 			=> '',
				'session_id'			=> $session_data,
				'requeststring'			=> $request,
				'api_id'				=> $api_id,
                'geomap'                => $tmp_data
			); 
			echo json_encode($result_array);
		} 
		else	{
			$data['image_url'] = ASSETS."assets/images/no_result.png";
			$data['caption'] = "Sorry, no hotels found.";
			$data['message'] = "Please try after sometime.";
			$data['back_to_home_flag'] = false;
			$hotel_search_result = $this->load->view('general/error_info', $data, true);
			echo json_encode(array('hotel_search_result' => $hotel_search_result ));
		}
	}
	
	function ajaxPaginationData($session_data, $session_request, $session_api) {
		
        $page = $this->input->post('page');
        $offset = (!$page) ? 0 : $page ;
        
        /*What ever the filtering data comes here*/
        $data = json_decode($this->input->post('filter'),true);
        //~ echo "data: <pre>";print_r($data);exit;
		$cond=array();
         if($data!='')
		 {
        list($from_amt, $to_amt) = explode('-',$data['amount']);
       $frm_amount =  trim(str_replace($_SESSION['currency'],'', $from_amt)) / $_SESSION['currency_value'];
       $to_amount =  trim(str_replace($_SESSION['currency'],'', $to_amt)) / $_SESSION['currency_value'];
       
       $from_amt = (int) $frm_amount;
       $to_amt  =  (int) $to_amount; 
        
        $amount_filter['amount >='] = $from_amt; 
        $amount_filter['amount <=']  = $to_amt;
        
        $hotel_name = isset($data['hotel_name']) ?  $data['hotel_name'] : NULL;
        
        $star_rating = (isset($data['starrate']) && count($data['starrate']) > 0) ?  $data['starrate'] : NULL;
        $accommodation = (isset($data['accommodation']) && count($data['accommodation']) > 0) ?  $data['accommodation'] : NULL;
        $ammenities = (isset($data['facility']) && count($data['facility']) > 0) ?  $data['facility'] : NULL;
        
        //~ echo "data star:<pre>";print_r($star_rating);
        
        $cond = array('amount_filter' => $amount_filter , 
                      'hotel_name' => $hotel_name,
                      'sort_col' => $data['sort']['column'],
                      'sort_val' => $data['sort']['value'],
                      'star_rating' => $star_rating,
                      'accommodation' => $accommodation,
                      'ammenities' => $ammenities
				); 
		}   
		
        //total rows count
		 $totalRec = $this->Hotel_Model->get_last_response_count($session_data,$cond);
		
		
		$data['request']	= json_decode(base64_decode($session_request));
		$data['api_id']	= $session_api;
			
		$data['hotel_count'] =  $totalRec;
					
    	 
        //pagination configuration
        $config['first_link']  = 'First';
        $config['div']         = 'hotel_result'; //parent div tag id
        $config['base_url']    = base_url().'hotel/ajaxPaginationData/'.$session_data.'/'.$session_request.'/'.$session_api;
        $config['total_rows']  =  $totalRec;
        $config['per_page']    = $this->perPage;
        
        $this->ajax_pagination_hotel->initialize($config);
        
        //get the posts data
      
	   $data['result'] =   $this->Hotel_Model->get_last_response($session_data,array('start'=>$offset,'limit'=>$this->perPage) ,$cond);
	   if(!$data['result']) {
            $data['message'] = "No Result Found."; 
            $data['image_url'] = base_url()."assets/images/no_result.png";
            $data['caption'] = "Sorry, no results found.";
            $data['message'] = "Plese try some different filtration criteria.";
            $data['back_to_home_flag'] = false;
            $filtered_theme = $this->load->view('general/error_info', $data, true);
        }
        else {
			$filtered_theme = $this->load->view('hotel/hotel_list', $data, true);
			
			$ca_airlines1 	= array();
			$loc_names 		= array();
			$ammenty_names 	= array();
			$resultType 	= $data['result'];
			 
		 	 foreach ($resultType as $user) {
				$usernames  	= json_decode($user->locations);
				if($usernames!='')
				{
				foreach($usernames as $usernamesss){
					//$ammenty_names[] = $ammenity->NAME;
					$loc_names[] = $usernamesss;
				}
				}
			
				
				
		    	$categorytype[] = $user->star_rating;
		    	
				//$ammenties = $this->hotel_model->get_facility_details_hotel($user->hotel_code)->result(); 
				$ammenties = json_decode($user->hotel_facility);
				
             
                //var_dump( json_decode($user->hotel_facility ));
				
				if($ammenties!='')
				{
				foreach($ammenties as $ammenity){
					//$ammenty_names[] = $ammenity->NAME;
					$ammenty_names[] = $ammenity;
				}
				}
			}
			//~ echo "loc_names: <pre>";print_r($loc_names);
			$all_locations = array_values(array_filter($loc_names));
			$all_locations_array = array();
			for($loc_iter = 0; $loc_iter < count($all_locations);$loc_iter++) {
				$all_locations_array[] = ucwords(strtolower($all_locations[$loc_iter]));
			}
			
			
			
			
			
            $ammenity_array = array();
			$category_array1 	= array_unique($categorytype);
			$inclusion_array1 	= array_unique($loc_names);
			$inclusion_array2 	= $loc_names;
			//~ $ammenities_array1 	= array_unique($ammenty_names);
			$ammenities_array1 	= $ammenty_names;
			$inclusion_array=array();
			foreach ($inclusion_array1 as $inclusion_array11)
				 if (!in_array(ucwords(strtolower($inclusion_array11), $inclusion_array)))
                {
                $inclusion_array[] = ucwords(strtolower($inclusion_array11));
                }
                
            $all_inclusion_array=array();
            foreach ($inclusion_array2 as $inclusion_array11) {
				if (!in_array(ucfirst(strtolower($inclusion_array11), $all_inclusion_array))) {
					$all_inclusion_array[] = ucwords(strtolower($inclusion_array11));
				}
			}
			
			foreach ($categorytype as $category_array11)
				$cat_array[] = $category_array11;
				
			foreach ($ammenities_array1 as $ammenities_array11)
				$ammenity_array[] = $ammenities_array11;    
		}
		$response = array(
						'hotel_search_result' 	=> $filtered_theme,
						'total_result' 			=> $totalRec,
						'inclusion' 			=> $inclusion_array,
						'areas' 				=> $all_inclusion_array,
						'locations' 			=> $all_locations_array,
						'ammenity' 				=> $ammenity_array,
						'category' 				=> $cat_array,
						'hotelRecommendatPrice' => '',
						'priceCount' 			=> '',
						'PriceFlight' 			=> '',
						'total_pages' 			=> '',
						'session_id'			=> $session_data,
						'requeststring'			=> $request,
						'api_id'				=> $api_id,
						'geomap'                => $tmp_data
					);
		
		echo json_encode($response);
    }
     function ajaxPaginationData__($session_data)
    {

        $page = $this->input->post('page');
        $offset = (!$page) ? 0 : $page ;
        
         /*What ever the filtering data comes here*/
        $data = json_decode($this->input->post('filter'),true);
        list($from_amt, $to_amt) = explode('-',$data['amount']);
        $from_amt = (int) trim(str_replace("$",'', $from_amt));
        $to_amt  =  (int) trim( str_replace("$",'', $to_amt));

        $amount_filter['price >='] = $from_amt; 
        $amount_filter['price <=']  = $to_amt;

        $start_rating = count($data['start_rating']) > 0 ?  $data['start_rating'] : NULL;
        $location = count($data['location']) > 0 ?  $data['location'] : NULL;
        $facility = count($data['facility']) > 0 ?  $data['facility'] : NULL;

        $cond = array(
                      'search_key' => trim($data['search_key']), 
                      'amount_filter' => $amount_filter , 
                      'start_rating' => $start_rating,
                      'sort_col' => $data['sort']['column'],
                      'sort_val' => $data['sort']['value'],
                      'location' => $location,
                      'facility' => $facility
                      ); 


        //debug($cond);

        
        //total rows count
        $dataresult['total_count'] = $this->hotel_model->get_last_result_count($session_data,$cond);
        
        //debug($dataresult['total_count']);         
    
        /*//pagination configuration
        $config['first_link']  = 'First';
        $config['div']         = 'flightsdata'; //parent div tag id
        $config['base_url']    = WEB_URL.'flight/ajaxPaginationData/'.$session_data;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        
        $this->ajax_pagination->initialize($config);*/
        
        //get the posts data

        $data['session_data'] = $session_data;
        $data['hotel_result'] =   $this->hotel_model->get_last_result($session_data,array('start'=>$offset,'limit'=>$this->perPage) ,$cond);
       if(!$data['hotel_result'])
        {
            $dataresult['result'] = "<center> <h4> No Result Found. </h4></center>";

        }
        $dataresult['result'] =  $this->load->view('hotel/hotel_list',$data,true);
   

     echo json_encode($dataresult);


    }
	
	public function hotel_details($hotelcode, $hotelid, $request, $api_id, $city_code){
		
		if(!empty($this->input->post('type')) && $this->input->post('type') != ''):
			$page_type = $this->input->post('type');
		else:
			$page_type = '';
		endif;
		
        $data['requests'] = $request;
        $data['city_code'] = $city_code;
        
        

		$hotel_code = json_decode((base64_decode($hotelcode)));
		
		$api_det = $this->General_Model->get_api_details_id($api_id);
		$hotel_det = $this->Hotel_Model->get_hotel_information_id($hotelid);
    	
        //$images =  $this->hotel_model->getHBHotelImages($hotel_code)->result();
			
        
		$data['hotel_code'] = $hotel_code;
		$data['request_data'] = json_decode((base64_decode($request)));
		$latitude = isset($hotel_det->lat) && $hotel_det->lat != ''  ?  $hotel_det->lat : 0;
        $longitude = isset($hotel_det->lon) && $hotel_det->lon != '' ? $hotel_det->lon : 0;


        $related_hotels=$this->Hotel_Model->get_nearby_hotels($latitude, $longitude);
        $data['related_hotels'] = $related_hotels;
		$data['api_det'] = $api_id ;

		$api_name = 	$this->General_Model->get_api_details_id($api_id);
		//~ echo "data: <pre>";print_r($api_name);exit;
	  
		$data['hotel_det'] = $hotel_det;

		if($page_type == 'ONEWAY' and $api_name->api_name=='GTA'):
			$data['hotel_details'] = $this->Hotel_Model->get_gta_hotel_data($hotel_det->hotel_code ,$hotel_det->sid);
			$template['theme'] = $this->load->view('hotel/room_list', $data, true);
			echo json_encode($template);
		elseif($page_type == '' and $api_name->api_name=='GTA'):
			$data['hotel_details'] = $this->Hotel_Model->get_gta_hotel_data($hotel_det->hotel_code ,$hotel_det->sid);
			$this->load->view('hotel/hotel_detail', $data);
		else:
		
		endif;
		if($api_name->api_name=='HOTELBEDS') {
			$hotel_fac_hotel_s = $this->Hotel_Model->getHBHotelAmenities($hotel_det->hotel_code)->result();
            $fac_h_s = array();
            $fac_h_Fee = array();
            $amenitie_s=array();
            if ($hotel_fac_hotel_s != '') {
                $fac_h_s = array();
                $fac_h_Fee = array();
                
                for ($l = 0; $l < count($hotel_fac_hotel_s); $l++) {
                    $h_code = $hotel_fac_hotel_s[$l]->CODE;
                    $count = $this->Hotel_Model->getHBHotelDesc($hotel_det->hotel_code, 'ENG')->num_rows();
                    //echo $count.'<br>';
                    if ($count > 0) {
                        $hotel_fac_hotel_des = $this->Hotel_Model->getHBHotelDesc($hotel_det->hotel_code, 'ENG')->row();
                        $fac_h_s[] = $hotel_fac_hotel_des->NAME;
                        $fac_h_Fee[] = $hotel_fac_hotel_s[$l]->FEE;
                            $amenitie_s[] = $hotel_fac_hotel_des->CLASSICON;
                    }
                }
                $fac_h_s = $fac_h_s;
            } else {
                $fac_h_s = array();
                $fac_h_s = $fac_h_s;
            }
            //echo '<pre>';print_r($hotel_fac_hotel_s);die;
            $hotel_fac_room_s = $this->Hotel_Model->getHBRoomAmenities($hotel_det->hotel_code)->result();
            $fac_r_s = array();
            $fac_r_Fee = array();
            if ($hotel_fac_room_s != '') {
                $fac_r_s = array();
                $fac_r_Fee = array();
                for ($ll = 0; $ll < count($hotel_fac_room_s); $ll++) {
                    $r_code = $hotel_fac_room_s[$ll]->CODE;
                    $hotel_fac_room_des = $this->Hotel_Model->getHBRoomDesc($r_code, 'ENG')->row();
                    $fac_r_s[] = $hotel_fac_room_des->NAME;
                    $fac_r_Fee[] = $hotel_fac_room_s[$ll]->FEE;
                }
                $fac_r_s = $fac_r_s;
            } else {
                $fac_r_s = array();
                $fac_r_s = $fac_r_s;
            }
            $data['amenitie_s']=$amenitie_s;

            $data['hotel_amenities'] = $fac_h_s;
            $data['room_amenities'] = $fac_r_s;
            $data['contact'] = $this->Hotel_Model->get_contact_details($hotel_det->hotel_code)->result();
			$data['hotel_details'] = $this->Hotel_Model->get_hb_hotel_data($hotel_det->hotel_code ,$hotel_det->sid); 
			//~ echo "hotel details: <pre>";print_r($data);exit;
			$this->load->view('hotel/hotel_detail_hb', $data); 
		}
    }
	
	public function get_room_details_gta($requests,$hotel_code,$api_id, $city_code)
	{//debug($requests);deubg($hotel_code);debug($api_id);exit;
		
		$currency = $_SESSION['currency'];
	    $exchange_rate = $_SESSION['currency_value'];
	    
		$data['api_id']=$api_id;
		$request_obj = json_decode((base64_decode($requests))); 
		$hotel_code = json_decode((base64_decode($hotel_code))); 
		$city_details = $this->General_Model->get_city_details_id($request_obj->city); 
		$data['city_code'] = $city_code;
		//$city_code = $city_details->gta_city_code;
		//$city_code = 'MIA';
		$hotel_details = $this->Hotel_Model->get_gta_hotel($hotel_code,$city_code); 
		$check_in_date_format = $request_obj->hotel_checkin;
		$country = explode(",", $request_obj->city);
		$user_nationality = trim($country[1]);
		
        $img = $hotel_details->ImageLinkThumbNail;
        $hotel_details->ImageLinkThumbNail;
       
        $imgs = explode(',',$hotel_details->ImageLinkThumbNail);
        
		$api_det = $this->General_Model->get_api_details_id($api_id);
	    $request_adt = $request_obj->adult;
        $adult_count = array_sum($request_adt); 
        if(isset($request_obj->child)) {
            $request_chd = $request_obj->child;
            $child_count = array_sum($request_chd);
        } else {
            $child_count = 0;
        }
        
        $user_nationality_details = $this->General_Model->getCountryByCountyCode($user_nationality);
		$user_nationality = $user_nationality_details->country_id;
		//~ echo "user_nationality".$user_nationality."<br/>";
		//~ echo "check_in_date_format".$check_in_date_format."<br/>";
		//~ echo "api_id".$api_id."<br/>";
		$admin_markup_details = $this->Markup_Model->get_admin_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);
		$b2b_markup_details = $this->Markup_Model->get_b2b_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);
		
			
		if($this->session->userdata('user_type') == 4){
			$sub_agent_markup_details = $this->Markup_Model->get_sub_agent_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);	
			$b2b2b_markup_details = $this->Markup_Model->get_b2b2b_markup_details($user_nationality, $check_in_date_format, $product_id ='1', $api_id);	
		}
		
		
		
		$cancel_buffer = $this->General_Model->getCancellationBuffer(1,$api_id)->row();
        if(empty($cancel_buffer) && $cancel_buffer == '') {
			$buffer_days = 0;
		}
		else {
			$buffer_days = $cancel_buffer->cancel_days;
		}
		
		//$buffer_days = 0;
		
		//~ End 
		
		$this->load->helper('gta_helper');  //new helper for gta travels
        $getHotelDetailsXml = getHotelDetailByCode($request_obj,$hotel_code, $city_code);  //gta helper
        // header("Content-Type: text/xml");echo $getHotelDetailsXml['GetHotelDetailRS']; die();            
            $HotelDetailsXmlRS = $getHotelDetailsXml['GetHotelDetailRS'];            
            //header("Content-Type: text/xml");echo $HotelDetailsXmlRS; die();
             
            $response_chk = substr($HotelDetailsXmlRS, 0, 5);  //check if the response is xml or not.
            if($response_chk != "<?xml") {
                           
            } else{
                $hotelDetailsObj = new SimpleXMLElement($HotelDetailsXmlRS);
                // echo "<pre>"; print_r($hotelDetailsObj); echo "</pre>"; die();
                if(isset($hotelDetailsObj->Errors)) {
                  
                }
                $hd_element = $hotelDetailsObj->ResponseDetails->SearchHotelPriceResponse;
                if( isset($hd_element->Errors) ) {
                   
                }
                else if(isset($hd_element->HotelDetails) && !empty($hd_element->HotelDetails) ) {
                     

                    $formatHotelDetailResponse_gta = $this->formatHotelDetailResponse_gta($hd_element,$imgs,$api_id); 

                    $hotelDetailsJson = json_encode($formatHotelDetailResponse_gta);          
					$data['hotel_details'] = json_decode($hotelDetailsJson);
					//~ echo "hotel details 1: <pre>"; print_r($data['hotel_details']);

                    $page_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    // echo "<pre>"; print_r($formatHotelDetailResponse_gta); echo "</pre>"; die();
                    /*Temp data storage*/
                    //echo "<pre>"; print_r($formatHotelDetailResponse_gta); echo "</pre>"; die();
                    
					
                  
                    foreach($formatHotelDetailResponse_gta as $tdk) {
					   $l = 0 ;
                        foreach($tdk['RoomCategory'] as $trk) { 
                            $images = $imgs[$l];
                            $temp_data['SITE_CURR'] = '';
                            $temp_data['API_CURR'] = $tdk['api_currency'];
                            $temp_data['session_id'] = $request_obj->sid;
                            $temp_data['api'] = 'GTA-H';
                            $temp_data['request'] = $requests;
                            $temp_data['hotel_code'] = $tdk['ItemCode'];
                            $temp_data['room_type'] = $trk['Description'];
                            $temp_data['room_name'] = $trk['Description'];
                            $temp_data['option_id'] = $trk['Id'];
                            $temp_data['hotel_name'] = $tdk['Item'];
                            $temp_data['hotel_rating'] = $tdk['StarRating'];
							$temp_data['hotel_description'] = $hotel_details->ReportInfo;
                            $temp_data['hotel_address'] = $hotel_details->Address;
                            $temp_data['hotel_contact'] = $hotel_details->Telephone;
                            $temp_data['hotel_api_images'] = $images;
							$temp_data['net_rate'] = $trk['net_rate'];
                            
                            $admin_markup = $b2b_markup = $b2b2_markup = $b2b_agent_markup = $b2b2b_agent_markup = 0;
							$admin_base_price = $b2b_base_price = $b2b2b_base_price = $b2b_agent_base_price = $b2b2b_agent_base_price = 0;
                            $total_price_with_markup = $trk['net_rate'];
                            
                            if(count($admin_markup_details) > 0 && $admin_markup_details != '' && !empty($admin_markup_details) ) {
								if($admin_markup_details[0]->markup_value_type == "Percentage") {
									$admin_markup =  $trk['net_rate'] * ($admin_markup_details[0]->markup_value / 100);
									$admin_base_price = $total_price_with_markup = $trk['net_rate'] + $admin_markup;
								}else{
									$admin_markup = $admin_markup_details[0]->markup_fixed;
									$admin_base_price = $total_price_with_markup = $trk['net_rate'] + $admin_markup;
								}
							}
							else {
								$admin_markup = 0;
								$admin_base_price = $total_price_with_markup = $trk['net_rate'] + $admin_markup;
							}
							if(count($b2b_markup_details) > 0 && $b2b_markup_details != '' && !empty($b2b_markup_details) ){
								if($b2b_markup_details[0]->markup_value_type == "Percentage") {
									$b2b_markup = $admin_base_price * ($b2b_markup_details[0]->markup_value / 100);
									$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
								} else {
									$b2b_markup = $b2b_markup_details[0]->markup_value;
									$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
								}
							}
							else {
								$b2b_markup = 0;
								$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
							}
						
							if($this->session->userdata('user_type') == 4){
								if(count($sub_agent_markup_details) > 0 && $sub_agent_markup_details != '' && !empty($sub_agent_markup_details) ){
									if($sub_agent_markup_details[0]->markup_value_type == "Percentage") {
										$b2b_agent_markup = $b2b_base_price* ($sub_agent_markup_details[0]->markup_value / 100);
										$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
									} 
									else {
										$b2b_agent_markup = $sub_agent_markup_details[0]->markup_value;
										$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
									}
								}
								else {
									$b2b_agent_markup = 0;
									$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
								}
								
								if(count($b2b2b_markup_details) > 0 && $b2b2b_markup_details != '' && !empty($b2b2b_markup_details) ){	  
									if($b2b2b_markup_details[0]->markup_value_type == "Percentage") {
										$b2b2b_agent_markup = $b2b_agent_base_price * ($b2b2b_markup_details[0]->markup_value / 100);
										$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
									} 
									else {
										$b2b2b_agent_markup = $b2b2b_markup_details[0]->markup_value;
										$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
									}
								}
								else {
									$b2b2b_agent_markup = 0;
									$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
								}
							}
							
							
							
							if(@$admin_markup == '' || @$admin_markup == null):
								$admin_markup = 0;
							endif;
							if(@$b2b_markup == '' || @$b2b_markup == null):
								$b2b_markup = 0;
							endif;
							if(@$b2b_agent_markup == '' || @$b2b_agent_markup == null):
								$b2b_agent_markup = 0;
							endif;
							if(@$b2b2b_agent_markup == '' || @$b2b2b_agent_markup == null):
								$b2b2b_agent_markup = 0;
							endif;
							
					$service_charge = 0;
				    $gst_charge = 0;
				    $tax_charge = 0;
				  
				    if($this->session->userdata('user_country_id') != 0){
						
						$country_name = $this->General_Model->get_country_name($this->session->userdata('user_country_id'))->row();
						$tax_details = $this->General_Model->get_tax_details($country_name->country_name, $this->session->userdata('user_state_id'))->row();
					    if(count($tax_details) > 0){
						 $price_with_markup_tax = $this->General_Model->tax_calculation($tax_details, $total_price_with_markup);
						 if(count($price_with_markup_tax) > 0){
							 $service_charge = $price_with_markup_tax['sc_tax'];
							 $gst_charge = $price_with_markup_tax['gst_tax'];
							 $tax_charge = $price_with_markup_tax['state_tax'];
							  $total_price_with_markup_tax = $price_with_markup_tax['amount'];
							}
						}else{
						 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
						}
					}else{
						 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
					} 
							
						
							  $Admin_Markup =$admin_markup;
							  $My_Markup = $b2b_markup;
							  $My_Agent_Markup = $b2b_agent_markup;
					          $My_B2B2B_Markup = $b2b2b_agent_markup;
							   
							  $Admin_BasePrice =$admin_base_price;
							  $agent_baseprice = $b2b_base_price;
							  $sub_agent_baseprice = $b2b_agent_base_price;
							  $b2b2b_baseprice = $b2b2b_agent_base_price;
							  
							
							  $TotalPrice = $total_price_with_markup_tax;
					 
                              $temp_data['admin_markup'] = $Admin_Markup;
                              $temp_data['my_markup'] = $My_Markup;
                              $temp_data['my_agent_Markup'] = $My_Agent_Markup ;
		                      $temp_data['my_b2b2b_markup'] = $My_B2B2B_Markup ;
                              
                              
							  $temp_data['admin_baseprice'] = $Admin_BasePrice;
							  $temp_data['agent_baseprice'] = $agent_baseprice;
							  $temp_data['sub_agent_baseprice'] = $sub_agent_baseprice ;
							  $temp_data['b2b2b_baseprice'] = $b2b2b_baseprice ;
							  
							   $temp_data['service_charge'] = $service_charge;
							  $temp_data['gst_charge'] = $gst_charge ;
							  $temp_data['tax_charge'] = $tax_charge ;
							  
							  $temp_data['total_cost'] = $TotalPrice;
							  
							  //~ Updating Item Price along with Markup
							  
							  $trk['ItemPrice'] = $TotalPrice;
							  $temp_data['room_data'] = json_encode($trk);
							  
							  $data['hotel_details']->Hotel->RoomCategory[$l] = $trk;
							  //~ echo "data 2: <pre>";print_r($data['hotel_details']);die();
							
                            //$temp_data['TotalPrice_API'] = $trk['ApiItemPrice'];
                            //$temp_data['profit_percent'] = $profit_percent;
                            $temp_data['city'] = $tdk['City'];
                            $temp_data['adult'] = $adult_count;
                            $temp_data['child'] = $child_count;
                            // $temp_data['MyMarkup'] = $myMarkup;

                            if(isset($trk['Meals'])){
                                $Meals = $trk['Meals'];
                                if($Meals['Basis']['code'] == 'B'){
                                    $meal_avail = "Breakfast Included";
                                } else {
                                    $meal_avail = "Room Only";
                                }
                            } else {
                                $meal_avail = "Room Only";
                            }
                            $temp_data['inclusion'] = $meal_avail;
                            $cancellation_policy = $trk['Condition'];
                            
                            $policy_string = "";
                            $cancelDates = array();

                            foreach($cancellation_policy as $cpk) {
                                //~ echo "data: <pre>";print_r($cpk);
                                if($cpk['Charge'] == "true"){
                                    if(isset($cpk['FromDate'])) {
                                        $fromday_string = $from_date = $cpk['FromDate'];
                                        if(strtotime($fromday_string) < strtotime('now')){
                                            $from_date = date('Y-m-d');
                                            $fromday_string = date('dS M Y');
                                        }
                                    } else {
                                        $from_date = date('Y-m-d');
                                        $fromday_string = 0;
                                    }
                                    if(isset($cpk['ToDate'])) {
                                        $toDay = $to_date = $cpk['ToDate'];
                                        
                                        if($toDay == "") {
                                            $to_date = date('Y-m-d'); 
                                            $today_string = "checkin date";
                                        } else {
                                            $today_string = $to_date = $cpk['ToDate'];
                                            if(strtotime($today_string) < strtotime('now') || $today_string == '0001-01-01'){
                                                $to_date = date('Y-m-d'); 
                                                $today_string = date('dS M Y');
                                            }
                                        }
                                    }
                                
                                    $api_curr = $tdk['api_currency'];
                                    $curr_display_icon = BASE_CURRENCY;
                                    $chargeable_amt = $TotalPrice_v = $cpk['ChargeAmount'];
                                    
                                    $admin_markup = $b2b_markup = $b2b2_markup = $b2b_agent_markup = $b2b2b_agent_markup = 0;
									$admin_base_price = $b2b_base_price = $b2b2b_base_price = $b2b_agent_base_price = $b2b2b_agent_base_price = 0;
									$total_price_with_markup = $trk['net_rate'];
									
									if(count($admin_markup_details) > 0 && $admin_markup_details != '' && !empty($admin_markup_details) ) {
										if($admin_markup_details[0]->markup_value_type == "Percentage") {
											$admin_markup =  $trk['net_rate'] * ($admin_markup_details[0]->markup_value / 100);
											$admin_base_price = $total_price_with_markup = $trk['net_rate'] + $admin_markup;
										}else{
											$admin_markup = $admin_markup_details[0]->markup_fixed;
											$admin_base_price = $total_price_with_markup = $TotalPrice_v + $admin_markup;
										}
									}
									else {
										$admin_markup = 0;
										$admin_base_price = $total_price_with_markup = $trk['net_rate'] + $admin_markup;
									}
									if(count($b2b_markup_details) > 0 && $b2b_markup_details != '' && !empty($b2b_markup_details) ){
										if($b2b_markup_details[0]->markup_value_type == "Percentage") {
											$b2b_markup = $admin_base_price * ($b2b_markup_details[0]->markup_value / 100);
											$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
										} else {
											$b2b_markup = $b2b_markup_details[0]->markup_value;
											$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
										}
									}
									else {
										$b2b_markup = 0;
										$b2b_base_price = $total_price_with_markup = $admin_base_price + $b2b_markup;
									}
								
									if($this->session->userdata('user_type') == 4){
										if(count($sub_agent_markup_details) > 0 && $sub_agent_markup_details != '' && !empty($sub_agent_markup_details) ){
											if($sub_agent_markup_details[0]->markup_value_type == "Percentage") {
												$b2b_agent_markup = $b2b_base_price* ($sub_agent_markup_details[0]->markup_value / 100);
												$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
											} 
											else {
												$b2b_agent_markup = $sub_agent_markup_details[0]->markup_value;
												$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
											}
										}
										else {
											$b2b_agent_markup = 0;
											$b2b_agent_base_price = $total_price_with_markup = $b2b_base_price + $b2b_agent_markup;
										}
										
										if(count($b2b2b_markup_details) > 0 && $b2b2b_markup_details != '' && !empty($b2b2b_markup_details) ){	  
											if($b2b2b_markup_details[0]->markup_value_type == "Percentage") {
												$b2b2b_agent_markup = $b2b_agent_base_price * ($b2b2b_markup_details[0]->markup_value / 100);
												$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
											} 
											else {
												$b2b2b_agent_markup = $b2b2b_markup_details[0]->markup_value;
												$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
											}
										}
										else {
											$b2b2b_agent_markup = 0;
											$b2b2b_agent_base_price = $total_price_with_markup = $b2b_agent_base_price + $b2b2b_agent_markup;
										}
									}
									
									
									
									if(@$admin_markup == '' || @$admin_markup == null):
										$admin_markup = 0;
									endif;
									if(@$b2b_markup == '' || @$b2b_markup == null):
										$b2b_markup = 0;
									endif;
									if(@$b2b_agent_markup == '' || @$b2b_agent_markup == null):
										$b2b_agent_markup = 0;
									endif;
									if(@$b2b2b_agent_markup == '' || @$b2b2b_agent_markup == null):
										$b2b2b_agent_markup = 0;
									endif;
							
									$service_charge = 0;
									$gst_charge = 0;
									$tax_charge = 0;
								  
									if($this->session->userdata('user_country_id') != 0){
										
										$country_name = $this->General_Model->get_country_name($this->session->userdata('user_country_id'))->row();
										$tax_details = $this->General_Model->get_tax_details($country_name->country_name, $this->session->userdata('user_state_id'))->row();
										if(count($tax_details) > 0){
										 $price_with_markup_tax = $this->General_Model->tax_calculation($tax_details, $total_price_with_markup);
										 if(count($price_with_markup_tax) > 0){
											 $service_charge = $price_with_markup_tax['sc_tax'];
											 $gst_charge = $price_with_markup_tax['gst_tax'];
											 $tax_charge = $price_with_markup_tax['state_tax'];
											  $total_price_with_markup_tax = $price_with_markup_tax['amount'];
											}
										}else{
										 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
										}
									}else{
										 $total_price_with_markup_tax = $total_price_with_markup + $service_charge + $gst_charge + $tax_charge;
									}
									$chargeable_amt = $total_price_with_markup_tax;
                                  
                                    //~ echo "data: <pre>";print_r($today_string);exit;
                                    
                                    $can_check_date = date('d-m-Y', strtotime($today_string));
									$can_check_datetime = date_create($can_check_date);
									$today = date('d-m-Y');
									$today_datetime = date_create($today);
									$diff=date_diff($can_check_datetime,$today_datetime);
									$interval = $diff->format("%a");
									
                                    if($today_string != "checkin date"){
										if($interval == 0 || $buffer_days == 0) {
											$today_string = date('dS M Y', strtotime($today_string));
											$fromday_string = date('dS M Y', strtotime($fromday_string));
										}
										elseif($interval >= $buffer_days) {
											$today_string = date('dS M Y', strtotime($today_string. "-".$buffer_days." days"));
											$fromday_string = date('dS M Y', strtotime($fromday_string. "-".$buffer_days." days"));
										}
										elseif($interval < $buffer_days) {
											$buffer_days = $interval;
											$today_string = date('dS M Y', strtotime($today_string. "-".$buffer_days." days"));
											$fromday_string = date('dS M Y', strtotime($fromday_string. "-".$buffer_days." days"));
										}
										else {
											$today_string = date('dS M Y', strtotime($today_string));
											$fromday_string = date('dS M Y', strtotime($fromday_string));
										}
                                        $policy_string .= "Cancellation between <b>".$today_string."</b> and <b>".$fromday_string."</b>, will incur cancellation charge <b>".$currency.' '.number_format($chargeable_amt*$exchange_rate,2).'</b>|';
                                        $cancelDates[] = array(
                                            'ToDate' => $to_date, 
                                            'FromDate' => $from_date, 
                                            'ChargeAmount' => $chargeable_amt, 
                                            'Type' => 1
                                        );
                                    } else {
                                        $curr_date = date('Y-m-d'); 
                                        //~ if($buffer_days == 0) {
											//~ $curr_date = date('dS M Y', strtotime($curr_date));
										//~ }
										//~ else {
											//~ $curr_date = date('dS M Y', strtotime($curr_date. "-".$buffer_days." days"));
										//~ }
										$curr_date = date('dS M Y', strtotime($curr_date));
										$fromday_string = $curr_date;
                                        $policy_string .= "Cancellation from <b>".$curr_date."</b>, will incur cancellation charge <b>".$currency.' '.$chargeable_amt*$exchange_rate.'</b>|';  
                                        $cancelDates[] = array(
                                            'ToDate' => $to_date, 
                                            'FromDate' => $from_date, 
                                            'ChargeAmount' => $chargeable_amt, 
                                            'Type' => 1
                                        ); 
                                    }
									$cancellation_till_date = date('Y-m-d', strtotime($fromday_string. "-1 days"));
                                    //~ echo 'if: <pre>';print_r($cancellation_till_date);die;
                                    //die();
                                    
                                } else if($cpk['Charge'] == "false") {
                                    if(isset($cpk['FromDate'])) {
                                        $fromday_string = $from_date = $cpk['FromDate'];
                                        if(strtotime($fromday_string) < strtotime('now')){
                                            $from_date = date('Y-m-d', strtotime('-1 days'));
                                            $fromday_string = date('dS M Y', strtotime("-1 days"));
                                        }
										$can_check_date = date('d-m-Y', strtotime($fromday_string));
										$can_check_datetime = date_create($can_check_date);
										$today = date('d-m-Y');
										$today_datetime = date_create($today);
										$diff=date_diff($can_check_datetime,$today_datetime);
										$interval = $diff->format("%a");
										
										if($interval == 0 || $buffer_days == 0) {
											$fromday_string = date('dS M Y', strtotime($fromday_string));
										}
										elseif($interval >= $buffer_days) {
											$fromday_string = date('dS M Y', strtotime($fromday_string. "-".$buffer_days." days"));
										}
										elseif($interval < $buffer_days) {
											$buffer_days = $interval;
											$fromday_string = date('dS M Y', strtotime($fromday_string. "-".$buffer_days." days"));
										}
										else {
											$fromday_string = date('dS M Y', strtotime($fromday_string));
										}
										
										
                                    } else {
                                        $from_date = date('Y-m-d'); 
                                        $fromday_string = 0;
                                    }
                                    $cancellation_till_date = date('Y-m-d', strtotime($fromday_string));
                                    //~ echo 'else: <pre>';print_r($cancellation_till_date);die;
                                    $policy_string .= "No cancellation fee (or Free cancellation) until ".$fromday_string.".|";
                                    $cancelDates[] = array(
                                        'ToDate' => $from_date, 
                                        'FromDate' => $from_date,
                                        'ChargeAmount' => 0,  
                                        'Type' => 0
                                    );
                                }
                            }
                            //~ echo '<pre>';print_r($cancellation_till_date);die;
                            
                            $temp_data['cancellation_data'] = json_encode($cancelDates); //store cancel dates
                            $temp_data['policy_description'] = $policy_string;  //store the policy strings.
                            $temp_data['cancellation_till_date'] = $cancellation_till_date;
                            $amendment_policy = $trk['amendment'];
                            $amendment_string = "";

                            foreach($amendment_policy as $apk) {
                                
                                if($apk['Charge'] == "true") {
                                    if(isset($apk['FromDate'])) {
                                        $fromday_string = $apk['FromDate'];
                                        if(strtotime($fromday_string) < strtotime('now')){
                                            $fromday_string = date('dS M Y');
                                        }
                                    } else {
                                        $fromday_string = 0;
                                    }
                                    if(isset($apk['ToDate'])) {
                                        $toDay = $apk['ToDate'];
                                        
                                        if($toDay == "") {
                                            $today_string = "checkin date";
                                        } else {
                                            $today_string = $apk['ToDate'];
                                            $today_string = date('dS M Y', strtotime($today_string)); 
                                            if(strtotime($today_string) < strtotime('now') || $today_string == '0001-01-01'){
                                                $today_string = date('dS M Y');
                                            }
                                        }
                                    }
                                    
                                
                                    $api_curr = $tdk['api_currency'];

                                    $curr_display_icon = BASE_CURRENCY;
                                    $chargeable_amt = $apk['ChargeAmount'];

                                    $today_string = date('dS M Y', strtotime($today_string));
                                    $fromday_string = date('dS M Y', strtotime($fromday_string));
                                    $amendment_string .= "Amendment between <b>".$today_string."</b> and <b>".$fromday_string."</b>, will incur amendment charge <b>".$curr_display_icon.$chargeable_amt.'</b>|';
                                    
                                }
                                else if($apk['Charge'] == "false") {
                                    if(isset($apk['FromDate'])) {
                                        $fromday_string = $frm_string = $apk['FromDate'];
                                        //$frm_string = strtotime($frm_string);
                                        if(strtotime($frm_string) < strtotime('now')){
                                            $fromday_string = date('dS M Y', strtotime("-1 days"));
                                        }
                                    } else {
                                        $fromday_string = 0;
                                    }

                                    $amendment_string .= "No amendment fee (or Free amendment) until ".$fromday_string.".|";
                                }else{
                                    $amendment_string .= "Amendments cannot be made against this booking once your booking has been requested";
                                }
                            }
                            $temp_data['amendment_policy'] = $amendment_string;

                            $temp_data['page_url'] = $page_link;
                            //~ echo '<pre>';print_r($temp_data);die;
                            $insert_temp_id = $this->Hotel_Model->storeHotelTempData($temp_data);
                            $room_index[] = $insert_temp_id; 
                            $l++;
                        } 
                        
                    } 
                   
                    /*Temp data storage*/
                    $data['room_index'] = $room_index;
                    $data['request'] = $request_obj;
                    $data['encoded_request'] = $requests;  //this request is used to get the similar listings from api through ajax request.
                    $data['gta_hotel_details'] = $this->Hotel_Model->get_gta_hotel_data($hotel_code,$city_code);
					$template['theme'] = $this->load->view('hotel/room_list', $data, true);
					echo json_encode($template);
                } 
                else{
                    
                }
            }
        
	 
	}
	
	public function get_room_details_hb($requests,$hotel_code,$api_id){
		$hotel_code = json_decode((base64_decode($hotel_code))); 
		$request =  json_decode((base64_decode($requests))); 
		$data['api_det']  = $api_id;
		$data['hotel_det'] = $this->Hotel_Model->get_hotel_information_all($hotel_code);
   
		$api_det = $this->General_Model->get_api_details_id($api_id);
		$data['session_data'] =    $session_data = $this->generate_rand_no().date("mdHis"); 
		$page_url = $this->input->post('page_url');
		if($api_det->api_helper!=''){
			$this->load->helper($api_det->api_helper);
			$availabilityrequest='';       
			HotelValuedRoomAvailRQ($request,$api_id,$session_data,$hotel_code); 
		}	 
		$data['hotel_code'] = $hotel_code;
		$data['request'] = $request;
		$data['request_data'] = $request;
		$data['encoded_request'] = $requests;
        
        $hotel_fac_room_s = $this->Hotel_Model->getHBRoomAmenities($hotel_code)->result();
        $fac_r_s = array();
		$fac_r_Fee = array();
		if ($hotel_fac_room_s != '') {
			$fac_r_s = array();
			$fac_r_Fee = array();
			for ($ll = 0; $ll < count($hotel_fac_room_s); $ll++) {
				$r_code = $hotel_fac_room_s[$ll]->CODE;
				$hotel_fac_room_des = $this->Hotel_Model->getHBRoomDesc($r_code, 'ENG')->row();
				$fac_r_s[] = $hotel_fac_room_des->NAME;
				$fac_r_Fee[] = $hotel_fac_room_s[$ll]->FEE;
			}
			$fac_r_s = $fac_r_s;
		} else {
			$fac_r_s = array();
			$fac_r_s = $fac_r_s;
		}
		$data['amenitie_s']=$amenitie_s;

		$data['hotel_amenities'] = $fac_h_s;
		$data['room_amenities'] = $fac_r_s;
        //echo "data: <pre>";print_r($data);exit;
        $template['theme'] = $this->load->view('hotel/room_list_hb', $data, true);
		echo json_encode($template);
    }
	
	
	public function get_room_details_dotw($requests,$hotel_code,$api_id){
		$data = array();
		$hotel_code = json_decode((base64_decode($hotel_code))); 
		$data['hotel_code'] = $hotel_code;
		$data['request'] = $request =  json_decode((base64_decode($requests))); 
		
		$data['api_det']  = $api_id;
		$data['hotel_det'] = $this->Hotel_Model->dotw_hotel_details($hotel_code)->row();
		
		$api_det = $this->General_Model->get_api_details_id($api_id);
		
	   $data['session_data'] =    $session_data = $this->generate_rand_no().date("mdHis");
		
		 //~ echo $page_url = $this->input->post('page_url');
		 //~ exit;
		if($api_det->api_helper!=''){
			$this->load->helper($api_det->api_helper);
			$availabilityrequest='';       
			HotelValuedRoomAvailRQ($request,$api_id,$session_data,$hotel_code); 
	
		$template['theme'] = $this->load->view('hotel/room_list_dotw', $data, true);
		echo json_encode($template);
			
		}else{
		 
		  	
		}
		
    }
    
	public function getCancellationPolicy() {
		$hotelCode = $this->input->post('hotelCode');
		$roomIndex = $this->input->post('roomIndex');
		$hotelRoomData = $this->Hotel_Model->getHotelRoomData($roomIndex,$hotelCode);
		$hotelRoomDataCount = $hotelRoomData->num_rows();
		
		if($hotelRoomDataCount == 1) {
			$data['hotelDataRow'] = $hotelDataRow = $hotelRoomData->row();
			//~ echo "data: <pre>";print_r($hotelDataRow);exit;
			$template['theme'] = $this->load->view('hotel/policy', $data, true);
		}
		else {
			$template['theme'] = "No Cancellation Policy";
		}
		echo json_encode($template);
	}
	public function getCancellationPolicy_hb() {
		
	}
	public function addToCart_gta($api_id='') {
		//gta starts here...
		
		
		$hotel_id = $this->input->post('hotel_id');
		$room_index = $this->input->post('room_index');
		$hotel_address = $this->input->post('hotel_address');
		$hotel_description = $this->input->post('description');
		$hotel_image = $this->input->post('image');
		$hotel_latitude = $this->input->post('lat');
		$hotel_longitude = $this->input->post('lon');
		$hotel_contact = $this->input->post('contact');
		$city_code = $this->input->post('city_code');
	
		//~ echo "data: <pre>";print_r($hotelRoomData);exit;

		$hotelRoomData = $this->Hotel_Model->getHotelRoomData($room_index, $hotel_id);
		
		$hotelRoomDataCount = $hotelRoomData->num_rows();
		
		
		if($hotelRoomDataCount == 1) {
			$hotelDataRow = $hotelRoomData->row();
			
			
			
			/* Clearing Cart - For Single Item Cart */
			
			$this->Cart_Model->clearCart($hotelDataRow->session_id);
			
			/* End */
			
           /*Check here for prices again from api*/
            $hotel_code = $hotelDataRow->hotel_code;
			$room_id = $hotelDataRow->option_id;
			$user_request = $hotelDataRow->request;

			$user_request_dec = json_decode((base64_decode($user_request)));
			$user_search_request = $user_request_dec;

			$hotel_search_string = http_build_query($user_search_request);                
			$this->load->helper('gta_helper');  //new helper for gta travels
			$hotel_search_url = base_url().'/hotel/?'.$hotel_search_string; //used for redirection to search page.                
			$hotelPriceRecheck = HotelPriceRecheck($hotel_code, $room_id, $user_request, $city_code);    //gta helper                
			$hotelPriceRecheck_xml = $hotelPriceRecheck['HotelReCheckRS'];
			
//			header("Content-Type: text/xml");
//			echo $hotelPriceRecheck_xml;
//			die();

			$HotelPriceRecheckRS = new SimpleXMLElement($hotelPriceRecheck_xml);
			$HotelPriceRecheckRS = $HotelPriceRecheckRS->children()->ResponseDetails->SearchHotelPriceResponse;

			if(isset($HotelPriceRecheckRS->HotelDetails)){
				$HotelPriceRecheckDetails = $HotelPriceRecheckRS->HotelDetails;
				$HotelPriceRecheckDetails = $HotelPriceRecheckDetails->Hotel;
				// die();
				if(isset($HotelPriceRecheckDetails->RoomCategories->RoomCategory->Confirmation)) {
					$confirmation_code = (string)$HotelPriceRecheckDetails->RoomCategories->RoomCategory->Confirmation['Code'];
					/*if($confirmation_code != 'IM') {
						$response_array = array("status"=>0, "redirect_url"=>$hotel_search_url);
						echo json_encode($response_array);
						return false;
					} */
				} else {
					$response_array = array("status"=>0, "redirect_url"=>$hotel_search_url);
					echo json_encode($response_array);
					return false;
				}

				if(isset($HotelPriceRecheckDetails->RoomCategories->RoomCategory->ItemPrice)) {                
					$CurrencyCode = (string)$HotelPriceRecheckDetails->RoomCategories->RoomCategory->ItemPrice['Currency'];
					$updatedPrice = $ApiTotalPrice = (string)$HotelPriceRecheckDetails->RoomCategories->RoomCategory->ItemPrice;
					
					//~ Commented as of now
					//~ $TotalPrice_v		= $this->general_model->convert_api_to_base_currecy_with_markup($updatedPrice,$CurrencyCode,$api_id);
					//$TotalPrice_v		= $updatedPrice;
					//$TotalPrice_v       = $this->general_model->Hgenerealmarkup($updatedPrice,$CurrencyCode,$api_id);
						 

					/*Adds Markup to charge amount*/
					
					/*Adds Markup to charge amount*/

				} else {
					$response_array = array("status"=>0, "redirect_url"=>$hotel_search_url);
					echo json_encode($response_array);
					return false;
				}
			} else {

				$response_array = array("status"=>0, "redirect_url"=>$hotel_search_url);
				echo json_encode($response_array);
				return false;
			}

			/*Check here for prices again from api*/

	   
			
			$data['session_id'] = $session_id = $hotelDataRow->session_id;

			$data['request'] = $request = $hotelDataRow->request;
			$data['hotel_code'] = $hotelDataRow->hotel_code;
			$data['token_id'] = $hotelDataRow->option_id;
			$data['room_name'] = $room_name = $hotelDataRow->room_name;
			$data['checkin'] = $user_search_request->hotel_checkin;
			$data['checkout'] = $user_search_request->hotel_checkout;
			$data['room_type'] = $hotelDataRow->room_name;
			$data['inclusion'] = $hotelDataRow->inclusion;
			$data['hotel_name'] = $hotelDataRow->hotel_name;
			$data['description'] = $hotelDataRow->hotel_description;
			$data['address'] = $hotelDataRow->hotel_address;
			$data['hotel_contact'] = $hotelDataRow->hotel_contact;
			$data['star'] = $hotelDataRow->hotel_rating;
			//  $data['contact'] = $hotel_contact;
			$data['thumb_image'] = $hotelDataRow->hotel_api_images;
			$data['images'] = $hotelDataRow->hotel_api_images;
			$data['lon'] = $hotel_longitude;
			$data['lat'] = $hotel_latitude;
			$data['star_rating'] = $hotelDataRow->hotel_rating;
			$data['hotel_facility'] = $hotelDataRow->hotel_facility;
			
			//~ Commented as of now
			//~ $data['total_cost'] = $TotalPrice_v['TotalPrice'];
			//~ $data['net_rate'] =  $TotalPrice_v['Netrate'];
			//~ $data['admin_markup'] = $TotalPrice_v['Admin_Markup'];
			//~ $data['admin_baseprice'] =  $TotalPrice_v['Admin_BasePrice'];
			//~ $data['my_markup'] = $TotalPrice_v['My_Markup'];
			
			$data['total_cost'] = $hotelDataRow->total_cost;
			
			$data['net_rate'] =  $hotelDataRow->net_rate;
			$data['admin_markup'] = $hotelDataRow->admin_markup;
			$data['admin_baseprice'] = $hotelDataRow->admin_baseprice;
			$data['my_markup'] = $hotelDataRow->my_markup;
			$data['agent_baseprice'] = $hotelDataRow->agent_baseprice;
			$data['my_agent_Markup'] = $hotelDataRow->my_agent_Markup;
			$data['sub_agent_baseprice'] = $hotelDataRow->sub_agent_baseprice;
			$data['my_b2b2b_markup'] = $hotelDataRow->my_b2b2b_markup;
			$data['b2b2b_baseprice'] = $hotelDataRow->b2b2b_baseprice;
			
			$data['service_charge'] = $hotelDataRow->service_charge;
			$data['gst_charge'] = $hotelDataRow->gst_charge;
			$data['tax_charge'] = $hotelDataRow->tax_charge;
			
			$data['SITE_CURR'] = BASE_CURRENCY;
			$data['API_CURR'] = $hotelDataRow->API_CURR;
					   
			$data['api_id'] = $api_id;
			$data['xml_log_id'] = '';

			$data['profit_percent'] = $hotelDataRow->profit_percent;
			
			
			$cancel_buffer = $this->General_Model->getCancellationBuffer(1,$api_id)->row();
			if(empty($cancel_buffer) && $cancel_buffer == '') {
				$buffer_days = 0;
			}
			else {
				$buffer_days = $cancel_buffer->cancel_days;
			}
		
			$cancel_details = json_decode($hotelDataRow->cancellation_data, true);
			
			if(!empty($cancel_details) && count($cancel_details) > 0){
				for($c=0; $c< count($cancel_details); $c++){
					if($buffer_days != 0){
					 $cancel_details[$c]['ToDate'] = date('Y-m-d', strtotime($cancel_details[$c]['ToDate']. "-".$buffer_days." days"));
					 $cancel_details[$c]['FromDate'] = date('Y-m-d', strtotime($cancel_details[$c]['FromDate']. "-".$buffer_days." days"));
					}
				}
				
			 $data['buffer_cancellation_policy'] = json_encode($cancel_details);
			 $data['cancellation_commisionable_nettrate'] = json_encode($cancel_details);
			}
		    $data['cancellation_data'] = $hotelDataRow->cancellation_data;
			$data['city'] = $hotelDataRow->city;
			$data['location'] = $hotelDataRow->city;
			$data['city_id'] = $cityId = $city_code;
			$data['room_count'] = $hotelDataRow->room_count;
			$data['room_data'] = $hotelDataRow->room_data;
			$data['policy_description'] = $hotelDataRow->policy_description;
			$data['amendment_policy'] = $hotelDataRow->amendment_policy;
			$data['cancel_policy'] = $hotelDataRow->policy_description;
			$data['cancellation_till_date'] = $hotelDataRow->cancellation_till_date;
			$data['adult'] = $hotelDataRow->adult;
			$data['child'] = $hotelDataRow->child;
			$data['page_url'] = $hotelDataRow->page_url;

			//echo '<pre>';print_r($data);die;
			$booking_cart_id = $this->Hotel_Model->addtocart($data);
			if($this->session->userdata('user_details_id')){
				$user_type =$this->session->userdata('user_type');
				$user_id = $this->session->userdata('user_details_id');
			}
			else {
				$user_type = '';
				$user_id = '';
			}
			$cart_global = array(
				'parent_cart_id' => 0,
				'referal_id' => $booking_cart_id,
				'product_id' => 1,
				'user_type' => $user_type,
				'user_id' => $user_id,
				'session_id' => $session_id,
				'site_currency' => BASE_CURRENCY,
				//~ 'total_cost' => $TotalPrice_v['TotalPrice'],
				'total_cost' => $hotelDataRow->total_cost,
				'ip_address' =>  $this->input->ip_address(),
				'timestamp' => date('Y-m-d H:i:s')
			);
			
			$cart_global_id = $this->Hotel_Model->insert_cart_global($cart_global);
			$URL = base_url().'booking/index/'.$session_id;
			
			$response_array = array("status"=>1, "redirect_url"=>$URL);
			echo json_encode($response_array);
		} else {
			$response_array = array("status"=>0, "redirect_url"=>base_url());
			echo json_encode($response_array);
			return false;
		}
    }
    
    public function formatHotelDetailResponse_gta($response,$imgs, $api_id) {
        $hotelsData = $response->HotelDetails;
        //foreach($hotelsData->Hotel as $hdk) {
            $hdk = $hotelsData->Hotel;
            $hs['Hotel']['API'] = 'GTA-H';
         
            $hs['Hotel']['HasExtraInfo'] = $extraInfo = (string)$hdk['HasExtraInfo'];
            $hs['Hotel']['HasMap'] = (string)$hdk['HasMap'];
            $hs['Hotel']['HasPictures'] = (string)$hdk['HasPictures'];
            $hs['Hotel']['HasPictures'] = (string)$hdk['HasPictures'];
            $hs['Hotel']['City'] = (string)$hdk->City;
            $hs['Hotel']['Item'] = (string)$hdk->Item;
            $hs['Hotel']['ItemCode'] = (string)$hdk->Item['Code'];



            $j=0;
            foreach($hdk->LocationDetails->Location as $lk) {
                $hs['Hotel']['LocationDetails'][$j]['LocationName'] = (string)$lk;
                $hs['Hotel']['LocationDetails'][$j]['LocationCode'] = (string)$lk['Code'];
                $j++;
            }

            $hs['Hotel']['StarRating'] = (string)$hdk->StarRating;

            // echo "<pre>"; print_r($hs); echo "</pre>"; die();
            
            $k = 0;
            foreach($hdk->RoomCategories->RoomCategory as $prk) {
                 $CurrencyCode = (string)$prk->ItemPrice['Currency'];

                $hs['Hotel']['RoomCategory'][$k]['Id'] = (string)$prk['Id'];
                $hs['Hotel']['RoomCategory'][$k]['Description'] = (string)$prk->Description;
                //~ Commented as of now 
                //~ $TotalPrice_v       = $this->general_model->convert_api_to_base_currecy_with_markup((string)$prk->ItemPrice,$CurrencyCode,$api_id);
                //~ $hs['Hotel']['RoomCategory'][$k]['ItemPrice'] = $TotalPrice_v['TotalPrice'];
                //~ $hs['Hotel']['RoomCategory'][$k]['ApiItemPrice'] = (string)$prk->ItemPrice;
                $hs['Hotel']['RoomCategory'][$k]['ItemPrice'] = (string)$prk->ItemPrice;
                $hs['Hotel']['RoomCategory'][$k]['ApiItemPrice'] = (string)$prk->ItemPrice;

       

                $hs['Hotel']['RoomCategory'][$k]['SharingBedding'] = (string)$prk->SharingBedding;
                $hs['Hotel']['RoomCategory'][$k]['ConfirmationCode'] = (string)$prk->Confirmation['Code'];
                $hs['Hotel']['RoomCategory'][$k]['Confirmation'] = (string)$prk->Confirmation;
                
                


                $meals = $prk->Meals;
               

                if(isset($meals->Basis)) {
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Basis']['name'] = (string)$meals->Basis;
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Basis']['code'] = (string)$meals->Basis['Code'];
                } else {
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Basis']['name'] = "";
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Basis']['code'] = "";
                }

                if(isset($meals->Breakfast)) {
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Breakfast']['name'] = (string)$meals->Breakfast;
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Breakfast']['code'] = (string)$meals->Breakfast['Code'];
                } else {
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Breakfast']['name'] = "";
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Breakfast']['code'] = "";
                }

                
                $hs['Hotel']['api_currency'] = $CurrencyCode;
                $org_amt = (string)$prk->ItemPrice;
                
                /*Adds Markup to current item price*/
                 //~ Commented as of now
				//~ $TotalPrice_v		= $this->general_model->convert_api_to_base_currecy_with_markup($org_amt,$CurrencyCode,$api_id);
				$TotalPrice_v		= $org_amt;
               // $TotalPrice_v       = $this->general_model->Hgenerealmarkup($org_amt,$CurrencyCode,$api_id);
               
					
               
					//~ $Netrate = $TotalPrice_v['Netrate'];
					//~ $Admin_Markup =$TotalPrice_v['Admin_Markup'];
					//~ $Admin_BasePrice =$TotalPrice_v['Admin_BasePrice'];
					//~ $My_Markup = $TotalPrice_v['My_Markup'];
					//~ $TotalPrice =$TotalPrice_v['TotalPrice'];
					
					$Netrate = $org_amt;
					$Admin_Markup = 0;
					$Admin_BasePrice = $org_amt;
					$My_Markup = 0;
					$TotalPrice = $org_amt;
		 
			
			
			        /*Adds Markup to current item price*/


                if(isset($prk->Offer)) {

                    $GrossWithoutDiscount = (string)$prk->ItemPrice['GrossWithoutDiscount'];   //price before discount
                    $IncludedOfferDiscount = (string)$prk->ItemPrice['IncludedOfferDiscount']; //price after discount
                    $Offer = (string)$prk->Offer;   //offer name
	
					//~ Commented as of now 
                    //~ $Offer_v       = $this->general_model->convert_api_to_base_currecy_with_markup($Offer,$CurrencyCode,$api_id);
                    $Offer_v       = $Offer;
                    /*Add Markup to price before offer*/
                    //~ Commented as of now 
					  //~ $OfferBeforePrice		= $this->general_model->convert_api_to_base_currecy_with_markup($GrossWithoutDiscount,$CurrencyCode,$api_id);
					  $OfferBeforePrice		= $GrossWithoutDiscount;
					 // $OfferBeforePrice       = $this->general_model->Hgenerealmarkup($GrossWithoutDiscount,$CurrencyCode,$api_id);
                    
                    /*Add Markup to price before offer*/

                    /*Add Markup to price after offer*/
                    //~ Commented as of now 
					//~ $OfferPrice		= $this->general_model->convert_api_to_base_currecy_with_markup($IncludedOfferDiscount,$CurrencyCode,$api_id);
					$OfferPrice		= $IncludedOfferDiscount;
					//$OfferPrice       = $this->general_model->Hgenerealmarkup($IncludedOfferDiscount,$CurrencyCode,$api_id);
					 
                    /*Add Markup to price after offer*/



                    $hs['Hotel']['RoomCategory'][$k]['GrossWithoutDiscount'] = $OfferBeforePrice['TotalPrice'];
                    $hs['Hotel']['RoomCategory'][$k]['IncludedOfferDiscount'] = $OfferPrice['TotalPrice'];
                    $hs['Hotel']['RoomCategory'][$k]['Offer'] = $Offer_v['TotalPrice'];
                }

                $hs['Hotel']['RoomCategory'][$k]['hotel_api_images'] = $imgs[$k];
                $hs['Hotel']['RoomCategory'][$k]['Currency'] = BASE_CURRENCY;
                $hs['Hotel']['RoomCategory'][$k]['amount'] = $TotalPrice;
                $hs['Hotel']['RoomCategory'][$k]['net_rate'] = $Netrate;
                $hs['Hotel']['RoomCategory'][$k]['admin_markup'] = $Admin_Markup;
				 $hs['Hotel']['RoomCategory'][$k]['admin_baseprice'] = $Admin_BasePrice;
				  $hs['Hotel']['RoomCategory'][$k]['my_markup'] = $My_Markup;


                if(isset($prk->ChargeConditions)) {

                    $chargeCondition = $prk->ChargeConditions;

                    $chargeConditionArray = array();
                    $chargeConditionString = "";

                    $ammendConditionArray = array();
                    $ammendConditionString = "";

                    foreach($chargeCondition->ChargeCondition as $cak){
                        $conditionType = (string)$cak['Type'];
                        if($conditionType == "cancellation") {
                            $cancelCondition = $cak->Condition;
                            $cond = 0;
                            foreach($cancelCondition as $cck) {

                                $Charge = (string)$cck['Charge'];
                                
                                if($Charge == "true") {
                                    $ChargeAmount = (string)$cck['ChargeAmount'];
                                    $Currency = (string)$cck['Currency'];
                                    $FromDate = (string)$cck['FromDate'];
                                    $ToDate = (string)$cck['ToDate'];

                                    $cancelString = 'From '.$FromDate.' to '.$ToDate.', chargeable amount is';
							//~ Commented as of now 
                              //~ $chargeAmount_c		= $this->general_model->convert_api_to_base_currecy_with_markup($ChargeAmount,$Currency,$api_id);
                              $chargeAmount_c		= $ChargeAmount;
							  //$chargeAmount_c      = $this->general_model->Hgenerealmarkup($ChargeAmount,$Currency,$api_id);
	 							     $ToDateDay = date('dS M Y', strtotime($ToDate)); 
                                    $FromDateDay = date('dS M Y', strtotime($FromDate)); 

                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['Charge'] = $Charge;
                                    //~ Commented as of now 
                                    //~ $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['ChargeAmount'] = $chargeAmount_c['TotalPrice'];
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['ChargeAmount'] = $ChargeAmount;
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['Currency'] = $Currency;
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['FromDate'] = $FromDate;
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['ToDate'] = $ToDate;

                                    /*$chargeConditionArray[] = "Cancellation between <b>".$ToDateDay."</b> and <b>".$FromDateDay."</b>, will incur cancellation charge <b>".$chargeAmountCurrency.$chargeAmount_c.'</b>';
                                    $chargeConditionString .= "Cancellation between <b>".$ToDateDay."</b> and <b>".$FromDateDay."</b>, will incur cancellation charge <b>".$chargeAmountCurrency.$chargeAmount_c.'</b>|';*/
                                    
                                } else {
                                    $FromDate = (string)$cck['FromDate'];
                                    $FromDateDay = date('dS M Y', strtotime($FromDate));
                                    if($buffer_days == 0) {
										$FromDateDay = date('dS M Y', strtotime($FromDateDay));
									}
									else {
										$FromDateDay = date('dS M Y', strtotime($FromDateDay. "-".$buffer_days." days"));
									}
                                     
                                    $chargeConditionArray[] = "No cancellation fee (or Free cancellation) until ".$FromDateDay.".";
                                    $chargeConditionString .= "No cancellation fee (or Free cancellation) until <b>".$FromDateDay."</b>.|";
                                
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['Charge'] = $Charge;
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['ChargeAmount'] = "";
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['Currency'] = "";
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['FromDate'] = $FromDateDay;

                                }                                
                                $cond++;
                            }

                        }
                        else if($conditionType == "amendment") {
                            $ammendCondition = $cak->Condition;
                            $amend = 0;
                            foreach($ammendCondition as $ak) {

                                $Charge = (string)$ak['Charge'];

                                if($Charge == "true") {
                                    $ChargeAmount = (string)$ak['ChargeAmount'];
                                    $Currency = (string)$ak['Currency'];
                                    $FromDate = (string)$ak['FromDate'];
                                    $ToDate = (string)$ak['ToDate'];

                                    $cancelString = 'From '.$FromDate.' to '.$ToDate.', chargeable amount is';
									//~ Commented as of now 
									//~ $chargeAmount_c		= $this->general_model->convert_api_to_base_currecy_with_markup($ChargeAmount,$Currency,$api_id);
									$chargeAmount_c		= $ChargeAmount;
						//$chargeAmount_c     = $this->general_model->Hgenerealmarkup($ChargeAmount,$Currency,$api_id);
						
 
                                    $ToDateDay = date('dS M Y', strtotime($ToDate)); 
                                    $FromDateDay = date('dS M Y', strtotime($FromDate)); 

                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['Charge'] = $Charge;
                                    //~ Commented as of now 
                                    //~ $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['ChargeAmount'] = $chargeAmount_c['TotalPrice'];
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['ChargeAmount'] = $ChargeAmount;
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['Currency'] = $Currency;
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['FromDate'] = $FromDate;
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['ToDate'] = $ToDate;

                                    $ammendConditionArray[] = "Amendment between <b>".$ToDateDay."</b> and <b>".$FromDateDay."</b>, will incur amendment charge <b>".BASE_CURRENCY.$chargeAmount_c.'</b>';
                                    $ammendConditionString .= "Amendment between <b>".$ToDateDay."</b> and <b>".$FromDateDay."</b>, will incur amendment charge <b>".BASE_CURRENCY.$chargeAmount_c.'</b>|';
                                } else {
                                    $FromDate = (string)$ak['FromDate'];
                                    $FromDateDay = date('dS M Y', strtotime($FromDate)); 

                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['Charge'] = $Charge;
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['ChargeAmount'] = "";
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['Currency'] = "";
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['FromDate'] = $FromDateDay;

                                    $ammendConditionArray[] = "No cancellation fee (or Free cancellation) until ".$FromDateDay.".";
                                    $ammendConditionString .= "No cancellation fee (or Free cancellation) until <b>".$FromDateDay."</b>.|";
                                }                                
                            $amend++;
                            }
                        }

                    }
                    
                }



                $n = 0;
                if(isset($prk->HotelRoomPrices->HotelRoom)) {
                    $pbd = $prk->HotelRoomPrices->HotelRoom;

                    $TotalGrossAmount = (string)$pbd->RoomPrice['Gross'];
                    
                    /*Adds Markup to total gross amount*/
                    //~ Commented as of now
					//~ $TotalPrice_v		= $this->general_model->convert_api_to_base_currecy_with_markup($TotalGrossAmount,$CurrencyCode,$api_id);
					$TotalPrice_v		= $TotalGrossAmount;
						 
		              //$TotalPrice_v       = $this->general_model->Hgenerealmarkup($TotalGrossAmount,$CurrencyCode,$api_id);
                    /*Adds Markup to total gross amount*/
                    
                    
                    $PriceRanges = $pbd->PriceRanges->PriceRange;
                    //echo '<pre>';print_r($PriceRanges);die;
                    $hs['Hotel']['RoomCategory'][$k]['PricebreakdownGross'] = $TotalPrice_v;
                    $pricebreak = 0;
                    foreach ($PriceRanges as $pkey => $PriceRange) {
                        //echo '<pre>';print_r($PriceRange);die;
                        $hs['Hotel']['RoomCategory'][$k]['Pricebreakdown'][$pricebreak]['FromDate'] =  (string)$PriceRange->DateRange->FromDate;
                        $hs['Hotel']['RoomCategory'][$k]['Pricebreakdown'][$pricebreak]['ToDate'] =  (string)$PriceRange->DateRange->ToDate;
                        
                        $BrokedAmount = (string)$PriceRange->Price['Gross'];
                        /*Adds Markup to total gross amount*/
						//$BrokedAmount		= $this->general_model->convert_api_to_base_currecy_with_markup($BrokedAmount,$CurrencyCode,$api_id);
					   //~ Commented as of now
					   //~ $TotalPrice_v     = $this->general_model->convert_api_to_base_currecy_with_markup($BrokedAmount,$CurrencyCode,$api_id);
					   $TotalPrice_v     = $BrokedAmount;
                      //  $TotalPrice_v       = $this->general_model->Hgenerealmarkup($BrokedAmount,$CurrencyCode,$api_id);
                        /*Adds Markup to total gross amount*/
                        
                        $hs['Hotel']['RoomCategory'][$k]['Pricebreakdown'][$pricebreak]['Price'] = $TotalPrice_v;
                        $hs['Hotel']['RoomCategory'][$k]['Pricebreakdown'][$pricebreak]['Nights'] =  (string)$PriceRange->Price['Nights'];
                        $pricebreak++;
                    }

                    //echo '<pre>';print_r($hs);die;                    
                }

                $essentialInfoString = "";
                if(isset($prk->EssentialInformation)) {
                   
                    foreach($prk->EssentialInformation->Information as $eik) {
                        $essentialInfoString .= (string)$eik->Text;
                        if(isset($eik->DateRange)) {
                            $fromDate = (string)$eik->DateRange->FromDate;
                            $toDate = (string)$eik->DateRange->ToDate;

                            if($fromDate != "" && $toDate != "" && $fromDate != '0001-01-01' && $toDate != '9999-12-31') {
                                $essentialInfoString .= 'from '.$fromDate.' to '.$toDate.'||';
                            } else {
                                $essentialInfoString .= '||';    
                            }

                        } else {
                            $essentialInfoString .= '||';
                        }
                    }
                    $hs['Hotel']['RoomCategory'][$k]['EssentialInfo'] = $essentialInfoString;
                }
                $k++;
            }
        // die();
    //   echo "<pre>"; print_r($hs); echo "</pre>"; die();
        return $hs;
    }

	public function generate_rand_no($length = 24) {
        $alphabets = range('A','Z');
        $numbers = range('0','9');
        $final_array = array_merge($alphabets,$numbers);
        //$id = date("ymd").date("His");
        $id = '';
        while($length--) {
          $key = array_rand($final_array);
          $id .= $final_array[$key];
        }
        return $id;
    }

	public function multiCurl() {
		
	}
}
?>
