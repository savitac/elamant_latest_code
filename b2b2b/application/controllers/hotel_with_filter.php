<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
ini_set('memory_limit', '-1');
class Hotel extends CI_Controller {
    function __construct(){
		parent::__construct();	
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Hotel_Model');
		$this->load->model('Xml_Model');
		
		if(isset($_SESSION['language']) && $_SESSION['language']!=''):
			$_SESSION['language'] = 'english';
		else:
			$_SESSION['language'] = 'english';
		endif;
		
		$current_url = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
        $current_url = base_url().$this->uri->uri_string(). $current_url;
        $url =  array(
            'continue' => $current_url,
        );
        $this->session->set_userdata($url);
		
		define('CANCEL_BUFFER', '7');
		
        $this->lang->load($_SESSION['language'],'Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
		
		$this->load->library('Ajax_pagination_hotel');
        $this->perPage = 1000;
		$this->module="Hotels";
	}

	public function get_hotel_city_suggestions() {
        ini_set('memory_limit', '-1');
        $term = $this->input->get('term'); //retrieve the search term that autocomplete sends
        $term = trim(strip_tags($term));
        $hotels = $this->Hotel_Model->get_hotels_list($term);
        $hotelsByNames = $this->Hotel_Model->get_hotels_list_ByName($term);
        $result = array();
       
        foreach ($hotels as $hotel) {
            $apts['type'] = 'City';
            $apts['label'] = $hotel->city;
            $apts['value'] = $hotel->city;
            $apts['id'] = $hotel->id;
           // $apts['id'] = $hotel->api_hotel_cities_id;
            $result[] = $apts;
        } 

        foreach ($hotelsByNames as $hotelname) {
            $apts['type'] = 'Hotels';
            $cityname = ucfirst(strtolower($hotelname->cityName));
            $apts['label'] = ucfirst(strtolower($hotelname->Name)).','.ucfirst($cityname).' ('.$hotelname->countryCode.')';
            $apts['value'] = ucfirst(strtolower($hotelname->Name)).','.ucfirst($cityname).' ('.$hotelname->countryCode.')';
            $apts['id'] = $hotelname->fly2escape_id;
           // $apts['id'] = $hotel->api_hotel_cities_id;
            $result[] = $apts;
        } 
        //print_r($result);
        echo json_encode($result);
	}

	public function index(){ 
        $request = $this->input->get();
    
        $data['request_array'] = $request;
        $request = json_encode($request);
        $data['req'] = $req = json_decode($request);
        $data['request'] = $request = base64_encode($request);
		 $dest_name = $req->city;
		 $c1  =  explode('-', $dest_name);
		
        $dest_name_array = explode(',', trim($c1[0]));


		 
        $country_name = end($dest_name_array);    //getting the last array element, as country will be in last.
        $country_name = trim($country_name);  //trim out any extra spaces.
    
        if(count($dest_name_array) > 1) {
            $country_pop = array_pop($dest_name_array);   //if the name str has more than 1 comma, pop out the country name. for ex: alexendria, lousiana, united states
            $city_name = implode(',', $dest_name_array);
        } else {
            //city-country combination is not present, do not search
        }
       
        $city_data = array('city_name' => $city_name, 'country_name' => $country_name, 'city_code' => $req->city_code);
        $city_data = json_encode($city_data);
		$city_data = json_decode($city_data);
        $data['adult_count'] = $req->adult_count;
        $data['child_count'] = $req->child_count;
        $data['days'] = $req->days;
        $data['city_data'] = $city_data;
		$data['session_data'] = $req->sid;
		
        $api_det = $this->General_Model->get_api_details($this->module); 
        $data['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
        
		//~ echo "<pre />"; print_r($api_det); exit;
		 
        for($k=0;$k<count($api_det);$k++)
		{
		  $api_c[] = "'".$api_det[$k]->api_details_id."'";
		}

		$data['api_det'] = implode(",",$api_c);
		$data['hotel_checkin'] = $req->hotel_checkin;
		$data['hotel_checkout'] = $req->hotel_checkout;
		$this->load->view('hotel/results', $data);
    }
    
    public function search() { 
       if ($_GET['city'] == '' || $_GET['checkin_date'] =='' || $_GET['checkout_date'] =='' || $_GET['rooms'] == '' || $_GET['adult'][0] =='' || $_GET['adult'][0] ==0) { redirect(base_url().'home','refresh'); }
		//echo "<pre>";print_r($_GET);exit;
        $request = $this->input->get();
	
       // echo "<pre>";print_r($request);exit;
        $check_in = $this->input->get('checkin_date');
        $check_out = $this->input->get('checkout_date');
      // $city_explode_data = substr($this->input->get('city'), 0, strrpos( $this->input->get('city'), '-') );
		 
        
         if(substr($_GET['city'], -1) == ')'){
         $hotelname = explode(',',$_GET['city']);
         $city_data = $this->Hotel_Model->get_hotel_name_data(trim($hotelname[0]))->row();  
         }
         else{
         $city_data = $this->Hotel_Model->get_city_data(trim($this->input->get('city')))->row();    
         } 
       
        
   // if(isset($city_data->api_hotel_cities_id))
   if(isset($city_data->id) || $hotelname != "")
    { 
         if(substr($_GET['city'], -1) == ')'){ 
         $hotelname = explode(',',$_GET['city']);
         $data['hotel_name'] = $hotelname[0]; 
         $data['city'] =  $city_data->cityName;
         }
         else{ 
        $data['city'] =      $this->input->get('city');
        }  
        if(!empty($city_data->city_code)) {
			$data['city_code'] = $city_data->city_code;
		}
				
        $data['hotel_checkin'] = date('Y-m-d' , strtotime($check_in) ) ;
        $data['hotel_checkout'] = date('Y-m-d' , strtotime($check_out) );
        
        $data['rooms'] = $this->input->get('rooms');
        $data['adult'] = $this->input->get('adult');
        $data['child'] = $this->input->get('child');

         $age1 = $this->input->get('child_0_Age');
         $age2 = $this->input->get('child_1_Age');
         $age3 = $this->input->get('child_2_Age');


        $data['childage1'] = (isset($request['child_0_Age']) && count($request['child_0_Age']) > 0) ? $request['child_0_Age'] : NULL ;
        $data['childage2'] = (isset($request['child_1_Age']) && count($request['child_1_Age']) > 0) ? $request['child_1_Age'] : NULL ;
        $data['childage3'] = (isset($request['child_2_Age']) && count($request['child_2_Age']) > 0) ? $request['child_2_Age'] : NULL ;


        $data['childAges'] = array();
      if(isset($data['childage1'])) {
            array_push($data['childAges'], $data['childage1']);           
        } else {
            array_push($data['childAges'], array(0));           
        }

        if(isset($data['childage2'])) {
            array_push($data['childAges'], $data['childage2']);           
        }else {
            array_push($data['childAges'], array(0));           
        }
        
        if(isset($data['childage3'])) {
            array_push($data['childAges'], $data['childage3']);           
        }else {
            array_push($data['childAges'], array(0));           
        }
		
        $data['nationality'] = $this->input->get('nationality');
        
        $data['adult_count'] = array_sum($request['adult']);
        $data['child_count'] = array_sum($request['child']); 
        $checkin_date = strtotime($check_in);
        $checkout_date = strtotime($check_out);

        $absDateDiff = abs($checkout_date - $checkin_date);

        $data['days'] = floor($absDateDiff/(60*60*24));
        $data['sid'] = $this->generate_rand_no().date("mdHis");
        $query = http_build_query($data);
        $url = base_url().'hotel/?'.$query;
        redirect($url);
    }
    else
    { 
        redirect('');
    }
    }
    
	function call_api($api_id) {
		$session_data = $_REQUEST['sessiondata'];
        $cache = $this->Hotel_Model->checkcache($session_data);
		$tmp_data = $this->Hotel_Model->fetch_search_result('',$session_data, $api_id);   
        
		// if($cache == 0){ 
			if($api_id == 3){
			// $this->hotel_model->clear_prev_result();   
			}
               
			$request = base64_decode($_REQUEST['request']);

			$data['request'] = $request = json_decode($request);
			
			$api_det = $this->General_Model->get_api_details_id($api_id);
			if(empty($tmp_data)):
				$this->load->helper('gta_helper');
				$availabilityrequest='';     
				$request = $_REQUEST['request'];
				HotelValuedAvailRQ($request,$api_id,$session_data);		
			endif;
			
		//}
		$this->fetch_search_result($request,$api_id,$session_data);
    }
	public function fetch_search_result($request,$api_id,$session_data) {		
		
         $tmp_data = $this->Hotel_Model->fetch_search_result('',$session_data, $api_id);   
		//~ echo "data: <pre>";print_r($tmp_data);exit;
		if($tmp_data!=''){
			// $total_h_count = count($tmp_data['result'])-1; //old one edit by sudhin

            $total_h_count = count($tmp_data['result']);

          
			$data['request'] = json_decode(base64_decode($request));

			$config['first_link']  = 'First';
			$config['div']         = 'hotel_result'; //parent div tag id
			$config['base_url']    = base_url().'hotel/ajaxPaginationData/'.$session_data.'/'.$request.'/'.$api_id;
			$config['total_rows']  = $total_h_count;
			$config['per_page']    = $this->perPage;
			$this->ajax_pagination_hotel->initialize($config);
			$data['api_id']	= $api_id;
			$data['result'] = $this->Hotel_Model->get_last_response($session_data,array('limit'=>$this->perPage));
		  // echo "data: <pre>";print_r($data['result']);exit;
			$data['hotel_count'] =  $total_h_count;

			if (!empty($data['result'])  ) {   

				$hotel_search_result = $this->load->view('hotel/hotel_list', $data, true);
			} else { 

                
				$hotel_search_result = $this->load->view('hotel/no_result', $data, true);
			}
		   
			// Inclucsion List Point Related Part Begins
			$ca_airlines1 	= array();
			$loc_names 		= array();
			$ammenty_names 		= array();
			$resultType 	= $tmp_data['result'];
			 
			foreach ($resultType as $user) {
				$usernames  	= json_decode($user->locations);
				if($usernames!='')
				{
				foreach($usernames as $usernamesss){
					//$ammenty_names[] = $ammenity->NAME;
					$loc_names[] = $usernamesss;
				}
				}
				
				$categorytype[] = $user->star_rating;
				//$ammenties = $this->hotel_model->get_facility_details_hotel($user->hotel_code)->result(); 
				$ammenties = json_decode($user->hotel_facility);

                //var_dump( json_decode($user->hotel_facility ));
				
				if($ammenties!='')
				{
				foreach($ammenties as $ammenity){
					//$ammenty_names[] = $ammenity->NAME;
					$ammenty_names[] = $ammenity;
				}
				}
			}
            $ammenity_array = array();
			$category_array1 	= array_unique($categorytype);
			$inclusion_array1 	= array_unique($loc_names);
			$ammenities_array1 	= array_unique($ammenty_names);
			$inclusion_array=array();
			foreach ($inclusion_array1 as $inclusion_array11)
				 if (!in_array(strtolower($inclusion_array11), $inclusion_array))
                {
                $inclusion_array[] = strtolower($inclusion_array11);
                }
				
			foreach ($categorytype as $category_array11)
				$cat_array[] = $category_array11;
				
			foreach ($ammenities_array1 as $ammenities_array11)
				$ammenity_array[] = $ammenities_array11;    
				
			$result_array = array(
				'hotel_search_result' 	=> $hotel_search_result,
				'total_result' 			=> $total_h_count,
				'min_val' 				=> ceil($tmp_data['minVal']),
				'max_val' 				=>  ceil($tmp_data['maxVal']),
				'inclusion' 			=> $inclusion_array,
				'ammenity' 				=> $ammenity_array,
				'category' 				=> $cat_array,
				'hotelRecommendatPrice' => '',
				'priceCount' 			=> '',
				'PriceFlight' 			=> '',
				'total_pages' 			=> '',
				'session_id'			=> $session_data,
				'requeststring'			=> $request,
				'api_id'				=> $api_id,
                'geomap'                => $tmp_data
			); 
			echo json_encode($result_array);
		} else	{

               // $hotel_search_result = $this->load->view(PROJECT_THEME.'/hotel/no_result', true, true);

			 echo json_encode(array('hotel_search_result' => '<center>No Result Found.</center>' ));
		}
	}
	public function hotel_details($hotelcode, $hotelid, $request, $api_id){
		
		if(!empty($this->input->post('type')) && $this->input->post('type') != ''):
			$page_type = $this->input->post('type');
		else:
			$page_type = '';
		endif;
		
        $data['requests'] = $request;

		$hotel_code = json_decode((base64_decode($hotelcode)));
		
		$api_det = $this->General_Model->get_api_details_id($api_id);
		$hotel_det = $this->Hotel_Model->get_hotel_information_id($hotelid);
    	
        //$images =  $this->hotel_model->getHBHotelImages($hotel_code)->result();
			
        
		$data['hotel_code'] = $hotel_code;
		$data['request_data'] = json_decode((base64_decode($request)));
		$latitude = isset($hotel_det->lat) && $hotel_det->lat != ''  ?  $hotel_det->lat : 0;
        $longitude = isset($hotel_det->lon) && $hotel_det->lon != '' ? $hotel_det->lon : 0;


        $related_hotels=$this->Hotel_Model->get_nearby_hotels($latitude, $longitude);
        $data['related_hotels'] = $related_hotels;
		$data['api_det'] = $api_id ;

		$api_name = 	$this->General_Model->get_api_details_id($api_id);
	  
		$data['hotel_det'] = $hotel_det;

		if($page_type == 'ONEWAY' and $api_name->api_name=='GTA'):
			$data['hotel_details'] = $this->Hotel_Model->get_gta_hotel_data($hotel_det->hotel_code ,$hotel_det->sid);
			$template['theme'] = $this->load->view('hotel/room_list', $data, true);
			echo json_encode($template);
		elseif($page_type == '' and $api_name->api_name=='GTA'):
			$data['hotel_details'] = $this->Hotel_Model->get_gta_hotel_data($hotel_det->hotel_code ,$hotel_det->sid);
			$this->load->view('hotel/hotel_detail', $data);
		else:
		
		endif;

		if($api_name->api_name=='HOTELBEDS') {
			$hotel_fac_hotel_s = $this->hotel_model->getHBHotelAmenities($hotel_det->hotel_code)->result();
            $fac_h_s = array();
            $fac_h_Fee = array();
            $amenitie_s=array();
            if ($hotel_fac_hotel_s != '') {
                $fac_h_s = array();
                $fac_h_Fee = array();
                
                for ($l = 0; $l < count($hotel_fac_hotel_s); $l++) {
                    $h_code = $hotel_fac_hotel_s[$l]->CODE;
                    $count = $this->hotel_model->getHBHotelDesc($hotel_det->hotel_code, 'ENG')->num_rows();
                    //echo $count.'<br>';
                    if ($count > 0) {
                        $hotel_fac_hotel_des = $this->hotel_model->getHBHotelDesc($hotel_det->hotel_code, 'ENG')->row();
                        $fac_h_s[] = $hotel_fac_hotel_des->NAME;
                        $fac_h_Fee[] = $hotel_fac_hotel_s[$l]->FEE;
                            $amenitie_s[] = $hotel_fac_hotel_des->CLASSICON;
                    }
                }
                $fac_h_s = $fac_h_s;
            } else {
                $fac_h_s = array();
                $fac_h_s = $fac_h_s;
            }
            //echo '<pre>';print_r($hotel_fac_hotel_s);die;
            $hotel_fac_room_s = $this->hotel_model->getHBRoomAmenities($hotel_det->hotel_code)->result();
            $fac_r_s = array();
            $fac_r_Fee = array();
            if ($hotel_fac_room_s != '') {
                $fac_r_s = array();
                $fac_r_Fee = array();
                for ($ll = 0; $ll < count($hotel_fac_room_s); $ll++) {
                    $r_code = $hotel_fac_room_s[$ll]->CODE;
                    $hotel_fac_room_des = $this->hotel_model->getHBRoomDesc($r_code, 'ENG')->row();
                    $fac_r_s[] = $hotel_fac_room_des->NAME;
                    $fac_r_Fee[] = $hotel_fac_room_s[$ll]->FEE;
                }
                $fac_r_s = $fac_r_s;
            } else {
                $fac_r_s = array();
                $fac_r_s = $fac_r_s;
            }
            $data['amenitie_s']=$amenitie_s;

            $data['hotel_amenities'] = $fac_h_s;
            $data['room_amenities'] = $fac_r_s;
            $data['contact'] = $this->hotel_model->get_contact_details($hotel_det->hotel_code)->result();
			$data['hotel_details'] = $this->hotel_model->get_hb_hotel_data($hotel_det->hotel_code ,$hotel_det->sid); 
       
			$this->load->view(PROJECT_THEME.'/hotel/hotel_details_hb', $data); 
		}
		else {
			
		}
	}
	public function get_room_details_gta($requests,$hotel_code,$api_id)
	{
		$data['api_id']=$api_id;
		$request_obj = json_decode((base64_decode($requests))); 
		$hotel_code = json_decode((base64_decode($hotel_code))); 
		$city_details = $this->General_Model->get_city_details_id($request_obj->city); 
		$city_code = $city_details->gta_city_code;
		$hotel_details = $this->Hotel_Model->get_gta_hotel($hotel_code,$city_code); 
           
        $img = $hotel_details->ImageLinkThumbNail;
        $imgs = explode(',',$hotel_details->ImageLinkThumbNail);
        
		$api_det = $this->General_Model->get_api_details_id($api_id);
	    $request_adt = $request_obj->adult;
        $adult_count = array_sum($request_adt); 
        if(isset($request_obj->child)) {
            $request_chd = $request_obj->child;
            $child_count = array_sum($request_chd);
        } else {
            $child_count = 0;
        }    
		$this->load->helper('gta_helper');  //new helper for gta travels
        $getHotelDetailsXml = getHotelDetailByCode($request_obj,$hotel_code);  //gta helper
        // header("Content-Type: text/xml");echo $getHotelDetailsXml['GetHotelDetailRS']; die();            
            $HotelDetailsXmlRS = $getHotelDetailsXml['GetHotelDetailRS'];            
            //header("Content-Type: text/xml");echo $HotelDetailsXmlRS; die();
             
            $response_chk = substr($HotelDetailsXmlRS, 0, 5);  //check if the response is xml or not.
            if($response_chk != "<?xml") {
                           
            } else{
                $hotelDetailsObj = new SimpleXMLElement($HotelDetailsXmlRS);
                // echo "<pre>"; print_r($hotelDetailsObj); echo "</pre>"; die();
                if(isset($hotelDetailsObj->Errors)) {
                  
                }
                $hd_element = $hotelDetailsObj->ResponseDetails->SearchHotelPriceResponse;
                if( isset($hd_element->Errors) ) {
                   
                }else if(isset($hd_element->HotelDetails) && !empty($hd_element->HotelDetails) ) {
                     

                    $formatHotelDetailResponse_gta = $this->formatHotelDetailResponse_gta($hd_element,$imgs,$api_id);                    
                    //echo "<pre>"; print_r($formatHotelDetailResponse_gta); echo "</pre>"; die();

                    $hotelDetailsJson = json_encode($formatHotelDetailResponse_gta);          
                    $data['hotel_details'] = json_decode($hotelDetailsJson);

                    $page_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    // echo "<pre>"; print_r($formatHotelDetailResponse_gta); echo "</pre>"; die();
                    /*Temp data storage*/
                    //echo "<pre>"; print_r($formatHotelDetailResponse_gta); echo "</pre>"; die();
                  
                    foreach($formatHotelDetailResponse_gta as $tdk) {
                        $l = 0 ;
                        foreach($tdk['RoomCategory'] as $trk) {  
                            $images = $imgs[$l];
                            $temp_data['SITE_CURR'] = '';
                            $temp_data['API_CURR'] = $tdk['api_currency'];
                            $temp_data['session_id'] = $request_obj->sid;
                            $temp_data['api'] = 'GTA-H';
                            $temp_data['request'] = $requests;
                            $temp_data['hotel_code'] = $tdk['ItemCode'];
                            $temp_data['room_data'] = json_encode($trk);
                            $temp_data['room_type'] = $trk['Description'];
                            $temp_data['room_name'] = $trk['Description'];
                            $temp_data['option_id'] = $trk['Id'];
                            $temp_data['hotel_name'] = $tdk['Item'];
                            $temp_data['total_cost'] = $trk['amount'];
                            $temp_data['hotel_rating'] = $tdk['StarRating'];
							$temp_data['hotel_description'] = $hotel_details->ReportInfo;
                            $temp_data['hotel_address'] = $hotel_details->Address;
                            $temp_data['hotel_contact'] = $hotel_details->Telephone;
                            $temp_data['hotel_api_images'] = $images;
						
							
                            $temp_data['net_rate'] = $trk['net_rate'];
                            $temp_data['admin_markup'] = $trk['admin_markup'];
							 $temp_data['admin_baseprice'] = $trk['admin_baseprice'];
							  $temp_data['my_markup'] = $trk['my_markup'];
							  
                            //$temp_data['TotalPrice_API'] = $trk['ApiItemPrice'];
                        //    $temp_data['profit_percent'] = $profit_percent;
                            $temp_data['city'] = $tdk['City'];
                            $temp_data['adult'] = $adult_count;
                            $temp_data['child'] = $child_count;
                            // $temp_data['MyMarkup'] = $myMarkup;

                            if(isset($trk['Meals'])){
                                $Meals = $trk['Meals'];
                                if($Meals['Basis']['code'] == 'B'){
                                    $meal_avail = "Breakfast Included";
                                } else {
                                    $meal_avail = "Room Only";
                                }
                            } else {
                                $meal_avail = "Room Only";
                            }
                            $temp_data['inclusion'] = $meal_avail;
                            $cancellation_policy = $trk['Condition'];
                            
                            $policy_string = "";
                            $cancelDates = array();

                            foreach($cancellation_policy as $cpk) {
                                
                                if($cpk['Charge'] == "true"){
                                    if(isset($cpk['FromDate'])) {
                                        $fromday_string = $from_date = $cpk['FromDate'];
                                        if(strtotime($fromday_string) < strtotime('now')){
                                            $from_date = date('Y-m-d');
                                            $fromday_string = date('dS M Y');
                                        }
                                    } else {
                                        $from_date = date('Y-m-d');
                                        $fromday_string = 0;
                                    }
                                    if(isset($cpk['ToDate'])) {
                                        $toDay = $to_date = $cpk['ToDate'];
                                        
                                        if($toDay == "") {
                                            $to_date = date('Y-m-d'); 
                                            $today_string = "checkin date";
                                        } else {
                                            $today_string = $to_date = $cpk['ToDate'];
                                            if(strtotime($today_string) < strtotime('now') || $today_string == '0001-01-01'){
                                                $to_date = date('Y-m-d'); 
                                                $today_string = date('dS M Y');
                                            }
                                        }
                                    }
                                
                                    $api_curr = $tdk['api_currency'];
                                    $curr_display_icon = BASE_CURRENCY;
                                    $chargeable_amt = $cpk['ChargeAmount'];
                                    
                                    if($today_string != "checkin date"){
                                        $today_string = date('dS M Y', strtotime($today_string. "-".CANCEL_BUFFER." days"));
                                        $fromday_string = date('dS M Y', strtotime($fromday_string. "-".CANCEL_BUFFER." days"));
                                        $policy_string .= "Cancellation between <b>".$today_string."</b> and <b>".$fromday_string."</b>, will incur cancellation charge <b>".$curr_display_icon.$chargeable_amt.'</b>|';
                                        $cancelDates[] = array(
                                            'ToDate' => $to_date, 
                                            'FromDate' => $from_date, 
                                            'ChargeAmount' => $chargeable_amt, 
                                            'Type' => 1
                                        );
                                    } else {
                                        $curr_date = date('Y-m-d');  
                                        $curr_date = date('dS M Y', strtotime($curr_date. "-".CANCEL_BUFFER." days"));
                                        $policy_string .= "Cancellation from <b>".$curr_date."</b>, will incur cancellation charge <b>".$curr_display_icon.$chargeable_amt.'</b>|';  
                                        $cancelDates[] = array(
                                            'ToDate' => $to_date, 
                                            'FromDate' => $from_date, 
                                            'ChargeAmount' => $chargeable_amt, 
                                            'Type' => 1
                                        ); 
                                    }

                                    //die();
                                    
                                } else if($cpk['Charge'] == "false") {
                                    if(isset($cpk['FromDate'])) {
                                        $fromday_string = $from_date = $cpk['FromDate'];
                                        if(strtotime($fromday_string) < strtotime('now')){
                                            $from_date = date('Y-m-d', strtotime('-1 days'));
                                            $fromday_string = date('dS M Y', strtotime("-1 days"));
                                        }
                                        $fromday_string = date('dS M Y', strtotime($fromday_string. "-".CANCEL_BUFFER." days"));
                                    } else {
                                        $from_date = date('Y-m-d'); 
                                        $fromday_string = 0;
                                    }
                                    $policy_string .= "No cancellation fee (or Free cancellation) until ".$fromday_string.".|";
                                    $cancelDates[] = array(
                                        'ToDate' => $from_date, 
                                        'FromDate' => $from_date,
                                        'ChargeAmount' => 0,  
                                        'Type' => 0
                                    );
                                }
                            }
                            //echo '<pre>';print_r($cancelDates);die;
                            
                            $temp_data['cancellation_data'] = json_encode($cancelDates); //store cancel dates
                            $temp_data['policy_description'] = $policy_string;  //store the policy strings.
                            $amendment_policy = $trk['amendment'];
                            $amendment_string = "";

                            foreach($amendment_policy as $apk) {
                                
                                if($apk['Charge'] == "true") {
                                    if(isset($apk['FromDate'])) {
                                        $fromday_string = $apk['FromDate'];
                                        if(strtotime($fromday_string) < strtotime('now')){
                                            $fromday_string = date('dS M Y');
                                        }
                                    } else {
                                        $fromday_string = 0;
                                    }
                                    if(isset($apk['ToDate'])) {
                                        $toDay = $apk['ToDate'];
                                        
                                        if($toDay == "") {
                                            $today_string = "checkin date";
                                        } else {
                                            $today_string = $apk['ToDate'];
                                            $today_string = date('dS M Y', strtotime($today_string)); 
                                            if(strtotime($today_string) < strtotime('now') || $today_string == '0001-01-01'){
                                                $today_string = date('dS M Y');
                                            }
                                        }
                                    }
                                    
                                
                                    $api_curr = $tdk['api_currency'];

                                    $curr_display_icon = BASE_CURRENCY;
                                    $chargeable_amt = $apk['ChargeAmount'];

                                    $today_string = date('dS M Y', strtotime($today_string));
                                    $fromday_string = date('dS M Y', strtotime($fromday_string));
                                    $amendment_string .= "Amendment between <b>".$today_string."</b> and <b>".$fromday_string."</b>, will incur amendment charge <b>".$curr_display_icon.$chargeable_amt.'</b>|';
                                    
                                }
                                else if($apk['Charge'] == "false") {
                                    if(isset($apk['FromDate'])) {
                                        $fromday_string = $frm_string = $apk['FromDate'];
                                        //$frm_string = strtotime($frm_string);
                                        if(strtotime($frm_string) < strtotime('now')){
                                            $fromday_string = date('dS M Y', strtotime("-1 days"));
                                        }
                                    } else {
                                        $fromday_string = 0;
                                    }

                                    $amendment_string .= "No amendment fee (or Free amendment) until ".$fromday_string.".|";
                                }else{
                                    $amendment_string .= "Amendments cannot be made against this booking once your booking has been requested";
                                }
                            }
                            $temp_data['amendment_policy'] = $amendment_string;

                            $temp_data['page_url'] = $page_link;
                            //echo '<pre>';print_r($temp_data);die;
                            $insert_temp_id = $this->Hotel_Model->storeHotelTempData($temp_data);
                            $room_index[] = $insert_temp_id; 
                            $l++;
                        } 
                        
                    } 
                   
                    /*Temp data storage*/
                    $data['room_index'] = $room_index;
                    $data['request'] = $request_obj;
                    $data['encoded_request'] = $requests;  //this request is used to get the similar listings from api through ajax request.
					$template['theme'] = $this->load->view('hotel/room_list', $data, true);
					echo json_encode($template);
                } 
                else{
                    
                }
            }
        
	 
	}
	public function addToCart_gta($api_id='') {
		//gta starts here...
		$hotel_id = $this->input->post('hotel_id');
		$room_index = $this->input->post('room_index');
		$hotel_address = $this->input->post('hotel_address');
		$hotel_description = $this->input->post('description');
		$hotel_image = $this->input->post('image');
		$hotel_latitude = $this->input->post('lat');
		$hotel_longitude = $this->input->post('lon');
		$hotel_contact = $this->input->post('contact');

	  

		$hotelRoomData = $this->Hotel_Model->getHotelRoomData($room_index, $hotel_id);

		$hotelRoomDataCount = $hotelRoomData->num_rows();
		//~ echo "data: <pre>";print_r($hotelRoomDataCount);exit;
		
		if($hotelRoomDataCount == 1) {
			$hotelDataRow = $hotelRoomData->row();

			/*Check here for prices again from api*/

			$hotel_code = $hotelDataRow->hotel_code;
			$room_id = $hotelDataRow->option_id;
			$user_request = $hotelDataRow->request;

			$user_request_dec = json_decode((base64_decode($user_request)));
			$user_search_request = $user_request_dec;

			$hotel_search_string = http_build_query($user_search_request);                
			$this->load->helper('gta_helper');  //new helper for gta travels
			$hotel_search_url = base_url().'/hotel/?'.$hotel_search_string; //used for redirection to search page.                
			$hotelPriceRecheck = HotelPriceRecheck($hotel_code, $room_id, $user_request);    //gta helper                
			$hotelPriceRecheck_xml = $hotelPriceRecheck['HotelReCheckRS'];
			/*
			header("Content-Type: text/xml");
			echo $hotelPriceRecheck_xml;
			die();*/

			$HotelPriceRecheckRS = new SimpleXMLElement($hotelPriceRecheck_xml);
			$HotelPriceRecheckRS = $HotelPriceRecheckRS->children()->ResponseDetails->SearchHotelPriceResponse;

			if(isset($HotelPriceRecheckRS->HotelDetails)){
				$HotelPriceRecheckDetails = $HotelPriceRecheckRS->HotelDetails;
				$HotelPriceRecheckDetails = $HotelPriceRecheckDetails->Hotel;
				// die();
				if(isset($HotelPriceRecheckDetails->RoomCategories->RoomCategory->Confirmation)) {
					$confirmation_code = (string)$HotelPriceRecheckDetails->RoomCategories->RoomCategory->Confirmation['Code'];
					/*if($confirmation_code != 'IM') {
						$response_array = array("status"=>0, "redirect_url"=>$hotel_search_url);
						echo json_encode($response_array);
						return false;
					} */
				} else {
					$response_array = array("status"=>0, "redirect_url"=>$hotel_search_url);
					echo json_encode($response_array);
					return false;
				}

				if(isset($HotelPriceRecheckDetails->RoomCategories->RoomCategory->ItemPrice)) {                
					$CurrencyCode = (string)$HotelPriceRecheckDetails->RoomCategories->RoomCategory->ItemPrice['Currency'];
					$updatedPrice = $ApiTotalPrice = (string)$HotelPriceRecheckDetails->RoomCategories->RoomCategory->ItemPrice;
					
					//~ Commented as of now
					//~ $TotalPrice_v		= $this->general_model->convert_api_to_base_currecy_with_markup($updatedPrice,$CurrencyCode,$api_id);
					$TotalPrice_v		= $updatedPrice;
					//$TotalPrice_v       = $this->general_model->Hgenerealmarkup($updatedPrice,$CurrencyCode,$api_id);
						 

					/*Adds Markup to charge amount*/
					
					/*Adds Markup to charge amount*/

				} else {
					$response_array = array("status"=>0, "redirect_url"=>$hotel_search_url);
					echo json_encode($response_array);
					return false;
				}
			} else {

				$response_array = array("status"=>0, "redirect_url"=>$hotel_search_url);
				echo json_encode($response_array);
				return false;
			}

			/*Check here for prices again from api*/

	   
			
			$data['session_id'] = $session_id = $hotelDataRow->session_id;

			$data['request'] = $request = $hotelDataRow->request;
			$data['hotel_code'] = $hotelDataRow->hotel_code;
			$data['token_id'] = $hotelDataRow->option_id;
			$data['room_name'] = $room_name = $hotelDataRow->room_name;
			$data['checkin'] = $user_search_request->hotel_checkin;
			$data['checkout'] = $user_search_request->hotel_checkout;
			$data['room_type'] = $hotelDataRow->room_name;
			$data['inclusion'] = $hotelDataRow->inclusion;
			$data['hotel_name'] = $hotelDataRow->hotel_name;
			$data['description'] = $hotelDataRow->hotel_description;
			$data['address'] = $hotelDataRow->hotel_address;
			$data['hotel_contact'] = $hotelDataRow->hotel_contact;
			$data['star'] = $hotelDataRow->hotel_rating;
			//  $data['contact'] = $hotel_contact;
			$data['thumb_image'] = $hotelDataRow->hotel_api_images;
			$data['images'] = $hotelDataRow->hotel_api_images;
			$data['lon'] = $hotel_longitude;
			$data['lat'] = $hotel_latitude;
			$data['star_rating'] = $hotelDataRow->hotel_rating;
			$data['hotel_facility'] = $hotelDataRow->hotel_facility;
			
			//~ Commented as of now
			//~ $data['total_cost'] = $TotalPrice_v['TotalPrice'];
			//~ $data['net_rate'] =  $TotalPrice_v['Netrate'];
			//~ $data['admin_markup'] = $TotalPrice_v['Admin_Markup'];
			//~ $data['admin_baseprice'] =  $TotalPrice_v['Admin_BasePrice'];
			//~ $data['my_markup'] = $TotalPrice_v['My_Markup'];
			
			$data['total_cost'] = $TotalPrice_v;
			$data['net_rate'] =  $TotalPrice_v;
			$data['admin_markup'] = 0;
			$data['admin_baseprice'] = $TotalPrice_v;
			$data['my_markup'] = 0;
			
			
			
			$data['SITE_CURR'] = BASE_CURRENCY;
			$data['API_CURR'] = $hotelDataRow->API_CURR;
					   
			$data['api_id'] = $api_id;
			$data['xml_log_id'] = '';

			$data['profit_percent'] = $hotelDataRow->profit_percent;
			$data['cancellation_data'] = $hotelDataRow->cancellation_data;
			$data['city'] = $hotelDataRow->city;
			$data['location'] = $hotelDataRow->city;
			$data['city_id'] = $cityId = $hotelDataRow->city_id;
			$data['room_count'] = $hotelDataRow->room_count;
			$data['room_data'] = $hotelDataRow->room_data;
			$data['policy_description'] = $hotelDataRow->policy_description;
			$data['amendment_policy'] = $hotelDataRow->amendment_policy;
			$data['cancel_policy'] = $hotelDataRow->policy_description;
			$data['adult'] = $hotelDataRow->adult;
			$data['child'] = $hotelDataRow->child;
			$data['page_url'] = $hotelDataRow->page_url;

			//echo '<pre>';print_r($data);die;
			$booking_cart_id = $this->Hotel_Model->addtocart($data);
			if($this->session->userdata('user_details_id')){
				$user_type =$this->session->userdata('user_type');
				$user_id = $this->session->userdata('user_details_id');
			}
			else {
				$user_type = '';
				$user_id = '';
			}
			$cart_global = array(
				'parent_cart_id' => 0,
				'referal_id' => $booking_cart_id,
				'product_id' => 1,
				'user_type' => $user_type,
				'user_id' => $user_id,
				'session_id' => $session_id,
				'site_currency' => BASE_CURRENCY,
				//~ 'total_cost' => $TotalPrice_v['TotalPrice'],
				'total_cost' => $TotalPrice_v,
				'ip_address' =>  $this->input->ip_address(),
				'timestamp' => date('Y-m-d H:i:s')
			);
			
			$cart_global_id = $this->Hotel_Model->insert_cart_global($cart_global);
			$URL = base_url().'booking/index/'.$session_id;
			
			$response_array = array("status"=>1, "redirect_url"=>$URL);
			echo json_encode($response_array);
		} else {
			$response_array = array("status"=>0, "redirect_url"=>base_url());
			echo json_encode($response_array);
			return false;
		}
    }

	public function formatHotelDetailResponse_gta($response,$imgs, $api_id) {
        $hotelsData = $response->HotelDetails;
        //foreach($hotelsData->Hotel as $hdk) {
            $hdk = $hotelsData->Hotel;
            $hs['Hotel']['API'] = 'GTA-H';
         
            $hs['Hotel']['HasExtraInfo'] = $extraInfo = (string)$hdk['HasExtraInfo'];
            $hs['Hotel']['HasMap'] = (string)$hdk['HasMap'];
            $hs['Hotel']['HasPictures'] = (string)$hdk['HasPictures'];
            $hs['Hotel']['HasPictures'] = (string)$hdk['HasPictures'];
            $hs['Hotel']['City'] = (string)$hdk->City;
            $hs['Hotel']['Item'] = (string)$hdk->Item;
            $hs['Hotel']['ItemCode'] = (string)$hdk->Item['Code'];



            $j=0;
            foreach($hdk->LocationDetails->Location as $lk) {
                $hs['Hotel']['LocationDetails'][$j]['LocationName'] = (string)$lk;
                $hs['Hotel']['LocationDetails'][$j]['LocationCode'] = (string)$lk['Code'];
                $j++;
            }

            $hs['Hotel']['StarRating'] = (string)$hdk->StarRating;

            // echo "<pre>"; print_r($hs); echo "</pre>"; die();
            
            $k = 0;
            foreach($hdk->RoomCategories->RoomCategory as $prk) {
                 $CurrencyCode = (string)$prk->ItemPrice['Currency'];

                $hs['Hotel']['RoomCategory'][$k]['Id'] = (string)$prk['Id'];
                $hs['Hotel']['RoomCategory'][$k]['Description'] = (string)$prk->Description;
                //~ Commented as of now 
                //~ $TotalPrice_v       = $this->general_model->convert_api_to_base_currecy_with_markup((string)$prk->ItemPrice,$CurrencyCode,$api_id);
                //~ $hs['Hotel']['RoomCategory'][$k]['ItemPrice'] = $TotalPrice_v['TotalPrice'];
                //~ $hs['Hotel']['RoomCategory'][$k]['ApiItemPrice'] = (string)$prk->ItemPrice;
                $hs['Hotel']['RoomCategory'][$k]['ItemPrice'] = (string)$prk->ItemPrice;
                $hs['Hotel']['RoomCategory'][$k]['ApiItemPrice'] = (string)$prk->ItemPrice;

       

                $hs['Hotel']['RoomCategory'][$k]['SharingBedding'] = (string)$prk->SharingBedding;
                $hs['Hotel']['RoomCategory'][$k]['ConfirmationCode'] = (string)$prk->Confirmation['Code'];
                $hs['Hotel']['RoomCategory'][$k]['Confirmation'] = (string)$prk->Confirmation;
                
                


                $meals = $prk->Meals;
               

                if(isset($meals->Basis)) {
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Basis']['name'] = (string)$meals->Basis;
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Basis']['code'] = (string)$meals->Basis['Code'];
                } else {
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Basis']['name'] = "";
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Basis']['code'] = "";
                }

                if(isset($meals->Breakfast)) {
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Breakfast']['name'] = (string)$meals->Breakfast;
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Breakfast']['code'] = (string)$meals->Breakfast['Code'];
                } else {
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Breakfast']['name'] = "";
                    $hs['Hotel']['RoomCategory'][$k]['Meals']['Breakfast']['code'] = "";
                }

                
                $hs['Hotel']['api_currency'] = $CurrencyCode;
                $org_amt = (string)$prk->ItemPrice;
                
                /*Adds Markup to current item price*/
                 //~ Commented as of now
				//~ $TotalPrice_v		= $this->general_model->convert_api_to_base_currecy_with_markup($org_amt,$CurrencyCode,$api_id);
				$TotalPrice_v		= $org_amt;
               // $TotalPrice_v       = $this->general_model->Hgenerealmarkup($org_amt,$CurrencyCode,$api_id);
					//~ $Netrate = $TotalPrice_v['Netrate'];
					//~ $Admin_Markup =$TotalPrice_v['Admin_Markup'];
					//~ $Admin_BasePrice =$TotalPrice_v['Admin_BasePrice'];
					//~ $My_Markup = $TotalPrice_v['My_Markup'];
					//~ $TotalPrice =$TotalPrice_v['TotalPrice'];
					$Netrate = $org_amt;
					$Admin_Markup = 0;
					$Admin_BasePrice = $org_amt;
					$My_Markup = 0;
					$TotalPrice = $org_amt;
		 
			
			
			        /*Adds Markup to current item price*/


                if(isset($prk->Offer)) {

                    $GrossWithoutDiscount = (string)$prk->ItemPrice['GrossWithoutDiscount'];   //price before discount
                    $IncludedOfferDiscount = (string)$prk->ItemPrice['IncludedOfferDiscount']; //price after discount
                    $Offer = (string)$prk->Offer;   //offer name
	
					//~ Commented as of now 
                    //~ $Offer_v       = $this->general_model->convert_api_to_base_currecy_with_markup($Offer,$CurrencyCode,$api_id);
                    $Offer_v       = $Offer;
                    /*Add Markup to price before offer*/
                    //~ Commented as of now 
					  //~ $OfferBeforePrice		= $this->general_model->convert_api_to_base_currecy_with_markup($GrossWithoutDiscount,$CurrencyCode,$api_id);
					  $OfferBeforePrice		= $GrossWithoutDiscount;
					 // $OfferBeforePrice       = $this->general_model->Hgenerealmarkup($GrossWithoutDiscount,$CurrencyCode,$api_id);
                    
                    /*Add Markup to price before offer*/

                    /*Add Markup to price after offer*/
                    //~ Commented as of now 
					//~ $OfferPrice		= $this->general_model->convert_api_to_base_currecy_with_markup($IncludedOfferDiscount,$CurrencyCode,$api_id);
					$OfferPrice		= $IncludedOfferDiscount;
					//$OfferPrice       = $this->general_model->Hgenerealmarkup($IncludedOfferDiscount,$CurrencyCode,$api_id);
					 
                    /*Add Markup to price after offer*/



                    $hs['Hotel']['RoomCategory'][$k]['GrossWithoutDiscount'] = $OfferBeforePrice['TotalPrice'];
                    $hs['Hotel']['RoomCategory'][$k]['IncludedOfferDiscount'] = $OfferPrice['TotalPrice'];
                    $hs['Hotel']['RoomCategory'][$k]['Offer'] = $Offer_v['TotalPrice'];
                }

                $hs['Hotel']['RoomCategory'][$k]['hotel_api_images'] = $imgs[$k];
                $hs['Hotel']['RoomCategory'][$k]['Currency'] = BASE_CURRENCY;
                $hs['Hotel']['RoomCategory'][$k]['amount'] = $TotalPrice;
                $hs['Hotel']['RoomCategory'][$k]['net_rate'] = $Netrate;
                $hs['Hotel']['RoomCategory'][$k]['admin_markup'] = $Admin_Markup;
				 $hs['Hotel']['RoomCategory'][$k]['admin_baseprice'] = $Admin_BasePrice;
				  $hs['Hotel']['RoomCategory'][$k]['my_markup'] = $My_Markup;


                if(isset($prk->ChargeConditions)) {

                    $chargeCondition = $prk->ChargeConditions;

                    $chargeConditionArray = array();
                    $chargeConditionString = "";

                    $ammendConditionArray = array();
                    $ammendConditionString = "";

                    foreach($chargeCondition->ChargeCondition as $cak){
                        $conditionType = (string)$cak['Type'];
                        if($conditionType == "cancellation") {
                            $cancelCondition = $cak->Condition;
                            $cond = 0;
                            foreach($cancelCondition as $cck) {

                                $Charge = (string)$cck['Charge'];
                                
                                if($Charge == "true") {
                                    $ChargeAmount = (string)$cck['ChargeAmount'];
                                    $Currency = (string)$cck['Currency'];
                                    $FromDate = (string)$cck['FromDate'];
                                    $ToDate = (string)$cck['ToDate'];

                                    $cancelString = 'From '.$FromDate.' to '.$ToDate.', chargeable amount is';
							//~ Commented as of now 
                              //~ $chargeAmount_c		= $this->general_model->convert_api_to_base_currecy_with_markup($ChargeAmount,$Currency,$api_id);
                              $chargeAmount_c		= $ChargeAmount;
							  //$chargeAmount_c      = $this->general_model->Hgenerealmarkup($ChargeAmount,$Currency,$api_id);
	 							     $ToDateDay = date('dS M Y', strtotime($ToDate)); 
                                    $FromDateDay = date('dS M Y', strtotime($FromDate)); 

                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['Charge'] = $Charge;
                                    //~ Commented as of now 
                                    //~ $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['ChargeAmount'] = $chargeAmount_c['TotalPrice'];
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['ChargeAmount'] = $ChargeAmount;
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['Currency'] = $Currency;
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['FromDate'] = $FromDate;
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['ToDate'] = $ToDate;

                                    /*$chargeConditionArray[] = "Cancellation between <b>".$ToDateDay."</b> and <b>".$FromDateDay."</b>, will incur cancellation charge <b>".$chargeAmountCurrency.$chargeAmount_c.'</b>';
                                    $chargeConditionString .= "Cancellation between <b>".$ToDateDay."</b> and <b>".$FromDateDay."</b>, will incur cancellation charge <b>".$chargeAmountCurrency.$chargeAmount_c.'</b>|';*/
                                    
                                } else {
                                    $FromDate = (string)$cck['FromDate'];
                                    $FromDateDay = date('dS M Y', strtotime($FromDate));
                                    $FromDateDay = date('dS M Y', strtotime($FromDateDay. "-".CANCEL_BUFFER." days")); 
                                    $chargeConditionArray[] = "No cancellation fee (or Free cancellation) until ".$FromDateDay.".";
                                    $chargeConditionString .= "No cancellation fee (or Free cancellation) until <b>".$FromDateDay."</b>.|";
                                
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['Charge'] = $Charge;
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['ChargeAmount'] = "";
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['Currency'] = "";
                                    $hs['Hotel']['RoomCategory'][$k]['Condition'][$cond]['FromDate'] = $FromDateDay;

                                }                                
                                $cond++;
                            }

                        }
                        else if($conditionType == "amendment") {
                            $ammendCondition = $cak->Condition;
                            $amend = 0;
                            foreach($ammendCondition as $ak) {

                                $Charge = (string)$ak['Charge'];

                                if($Charge == "true") {
                                    $ChargeAmount = (string)$ak['ChargeAmount'];
                                    $Currency = (string)$ak['Currency'];
                                    $FromDate = (string)$ak['FromDate'];
                                    $ToDate = (string)$ak['ToDate'];

                                    $cancelString = 'From '.$FromDate.' to '.$ToDate.', chargeable amount is';
									//~ Commented as of now 
									//~ $chargeAmount_c		= $this->general_model->convert_api_to_base_currecy_with_markup($ChargeAmount,$Currency,$api_id);
									$chargeAmount_c		= $ChargeAmount;
						//$chargeAmount_c     = $this->general_model->Hgenerealmarkup($ChargeAmount,$Currency,$api_id);
						
 
                                    $ToDateDay = date('dS M Y', strtotime($ToDate)); 
                                    $FromDateDay = date('dS M Y', strtotime($FromDate)); 

                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['Charge'] = $Charge;
                                    //~ Commented as of now 
                                    //~ $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['ChargeAmount'] = $chargeAmount_c['TotalPrice'];
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['ChargeAmount'] = $ChargeAmount;
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['Currency'] = $Currency;
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['FromDate'] = $FromDate;
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['ToDate'] = $ToDate;

                                    $ammendConditionArray[] = "Amendment between <b>".$ToDateDay."</b> and <b>".$FromDateDay."</b>, will incur amendment charge <b>".BASE_CURRENCY.$chargeAmount_c.'</b>';
                                    $ammendConditionString .= "Amendment between <b>".$ToDateDay."</b> and <b>".$FromDateDay."</b>, will incur amendment charge <b>".BASE_CURRENCY.$chargeAmount_c.'</b>|';
                                } else {
                                    $FromDate = (string)$ak['FromDate'];
                                    $FromDateDay = date('dS M Y', strtotime($FromDate)); 

                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['Charge'] = $Charge;
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['ChargeAmount'] = "";
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['Currency'] = "";
                                    $hs['Hotel']['RoomCategory'][$k]['amendment'][$amend]['FromDate'] = $FromDateDay;

                                    $ammendConditionArray[] = "No cancellation fee (or Free cancellation) until ".$FromDateDay.".";
                                    $ammendConditionString .= "No cancellation fee (or Free cancellation) until <b>".$FromDateDay."</b>.|";
                                }                                
                            $amend++;
                            }
                        }

                    }
                    
                }



                $n = 0;
                if(isset($prk->HotelRoomPrices->HotelRoom)) {
                    $pbd = $prk->HotelRoomPrices->HotelRoom;

                    $TotalGrossAmount = (string)$pbd->RoomPrice['Gross'];
                    
                    /*Adds Markup to total gross amount*/
                    //~ Commented as of now
					//~ $TotalPrice_v		= $this->general_model->convert_api_to_base_currecy_with_markup($TotalGrossAmount,$CurrencyCode,$api_id);
					$TotalPrice_v		= $TotalGrossAmount;
						 
		              //$TotalPrice_v       = $this->general_model->Hgenerealmarkup($TotalGrossAmount,$CurrencyCode,$api_id);
                    /*Adds Markup to total gross amount*/
                    
                    
                    $PriceRanges = $pbd->PriceRanges->PriceRange;
                    //echo '<pre>';print_r($PriceRanges);die;
                    $hs['Hotel']['RoomCategory'][$k]['PricebreakdownGross'] = $TotalPrice_v;
                    $pricebreak = 0;
                    foreach ($PriceRanges as $pkey => $PriceRange) {
                        //echo '<pre>';print_r($PriceRange);die;
                        $hs['Hotel']['RoomCategory'][$k]['Pricebreakdown'][$pricebreak]['FromDate'] =  (string)$PriceRange->DateRange->FromDate;
                        $hs['Hotel']['RoomCategory'][$k]['Pricebreakdown'][$pricebreak]['ToDate'] =  (string)$PriceRange->DateRange->ToDate;
                        
                        $BrokedAmount = (string)$PriceRange->Price['Gross'];
                        /*Adds Markup to total gross amount*/
						//$BrokedAmount		= $this->general_model->convert_api_to_base_currecy_with_markup($BrokedAmount,$CurrencyCode,$api_id);
					   //~ Commented as of now
					   //~ $TotalPrice_v     = $this->general_model->convert_api_to_base_currecy_with_markup($BrokedAmount,$CurrencyCode,$api_id);
					   $TotalPrice_v     = $BrokedAmount;
                      //  $TotalPrice_v       = $this->general_model->Hgenerealmarkup($BrokedAmount,$CurrencyCode,$api_id);
                        /*Adds Markup to total gross amount*/
                        
                        $hs['Hotel']['RoomCategory'][$k]['Pricebreakdown'][$pricebreak]['Price'] = $TotalPrice_v;
                        $hs['Hotel']['RoomCategory'][$k]['Pricebreakdown'][$pricebreak]['Nights'] =  (string)$PriceRange->Price['Nights'];
                        $pricebreak++;
                    }

                    //echo '<pre>';print_r($hs);die;                    
                }

                $essentialInfoString = "";
                if(isset($prk->EssentialInformation)) {
                   
                    foreach($prk->EssentialInformation->Information as $eik) {
                        $essentialInfoString .= (string)$eik->Text;
                        if(isset($eik->DateRange)) {
                            $fromDate = (string)$eik->DateRange->FromDate;
                            $toDate = (string)$eik->DateRange->ToDate;

                            if($fromDate != "" && $toDate != "" && $fromDate != '0001-01-01' && $toDate != '9999-12-31') {
                                $essentialInfoString .= 'from '.$fromDate.' to '.$toDate.'||';
                            } else {
                                $essentialInfoString .= '||';    
                            }

                        } else {
                            $essentialInfoString .= '||';
                        }
                    }
                    $hs['Hotel']['RoomCategory'][$k]['EssentialInfo'] = $essentialInfoString;
                }
                $k++;
            }
        // die();
    //   echo "<pre>"; print_r($hs); echo "</pre>"; die();
        return $hs;
    }

	public function generate_rand_no($length = 24) {
        $alphabets = range('A','Z');
        $numbers = range('0','9');
        $final_array = array_merge($alphabets,$numbers);
        //$id = date("ymd").date("His");
        $id = '';
        while($length--) {
          $key = array_rand($final_array);
          $id .= $final_array[$key];
        }
        return $id;
    }

	public function multiCurl() {
		
	}
	
	function getFilterList($session_id){
		//echo $session_id;
		//echo "<pre/>";print_r($_POST);exit;
		//error_reporting(E_ALL);
        $data['result'] = $this->Hotel_Model->get_filter_result($session_id,$_POST);
        //echo "<pre/>";print_r($data);exit;
        $data['hotel_count'] = $count = count($data['result']);
        if (!empty($data['result'])) {   

               $hotel_search_result = $this->load->view('hotel/hotel_list', $data, true);
            } else { 

                
                $hotel_search_result = 'No Result';
            }
            $array = array('count'               => $count,
                           'hotel_search_result' => $hotel_search_result);
        echo (json_encode($array));
    }
}
?>
