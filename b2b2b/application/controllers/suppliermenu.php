<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE){ session_start(); }
error_reporting(0);
ob_start();
class Suppliermenu extends CI_Controller {
	
    public function __construct(){
		parent::__construct();	
		$this->check_isvalidated();
		$this->load->model('General_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model('Security_Model');
		$this->load->model('Account_Model');
		$this->load->model('Staffmanagement_Model');
		$this->load->model('Email_Model');
		$this->load->model('Settings_Model');
		$this->load->model('Usermanagement_Model');
		$this->load->model('Crs_Supplier_Model');
		$this->load->model('Validation_Model');
		$this->lang->load('english','Dynamic_Languages');
		$this->TravelLights = $this->lang->line('TravelLights');
	}
	
	private function check_isvalidated()
	{
		if($this->session->userdata('user_details_id'))
		{
			$user_id = $this->session->userdata('user_details_id');
			$controller_name = $this->router->fetch_class();
			$function_name = $this->router->fetch_method();
			
			$this->load->model('Privilege_Model');
            $sub_admin_id = $this->session->userdata('admin_id');
            $privilege_details = $this->Privilege_Model->get_user_privileges($user_id,$controller_name,$function_name, $function_parameter = 1)->row();
            
    //            if(count($privilege_details)  == 0)
				// {	
				//   redirect('error/access_denied');
				// }
		}
	 }
	
	function index(){
		$user_id 	= $this->session->userdata('user_details_id');
		$users['currency'] = $this->General_Model->getCurrencyList();
		$users['products'] = $this->Dashboard_Model->getProducts();
		$users['agentadmin_list'] = $this->Account_Model->get_user_list();
		$users['depsoite'] 	= $this->Usermanagement_Model->getRequestedDepositList($user_id);
		$users['api'] = $this->General_Model->get_api_list();
		$users['agentadmin_list'] = $this->Usermanagement_Model->getAgentadminList($user_id);
		$users['agent_markups'] = $this->Settings_Model->getSubAgentMarkups($this->session->userdata('user_details_id'));
		$users['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
		$users['agentusers'] = $this->Usermanagement_Model->getAgentusersList();
		if(isset($_GET['class'])){

			$users['class'] = $_GET['class'];
		}
		//echo '<pre>'; print_r($users['agentusers']); exit();
		$this->load->view('dashboard/supplier_managment', $users);
	}
	
	function crs_flights()
	{
    $user_id 	= $this->session->userdata('user_details_id');
	$users['flight_details'] = $this->Crs_Supplier_Model->getcrs_List_model();
	$users['agent_list'] = $this->load->view('dashboard/suppliers/all_crs_flight_lists', $users, true);	
    print json_encode($users); 
    return;
	}
	
	function getuserList()
	{
		//b2b2c 4
    $user_id 	= 5;
	$users['users'] = $this->Usermanagement_Model->getusersList( $user_id );
	//$users['agent_list'] = $this->load->view('dashboard/management/agent_list', $users, true);
	$users['agent_list'] = $this->load->view('dashboard/management/users_list', $users, true);		
    print json_encode($users); 
    return;
	}
	
	/*function getuserList()
	{
		//b2b2c 4
    $user_id 	= 4;
	$users['users'] = $this->Usermanagement_Model->getusersList( $user_id );
	//$users['agent_list'] = $this->load->view('dashboard/management/agent_list', $users, true);
	$users['agent_list'] = $this->load->view('dashboard/management/b2b2b_list', $users, true);		
    print json_encode($users); 
    return;
	}*/
	
	
	
	function depositList($user_id1){
		$user_id 	= json_decode(base64_decode($user_id1));
		$users['user_list'] = $this->Usermanagement_Model->getSubAgentList($user_id);
			$users['depsoite'] 	= $this->Usermanagement_Model->getDepositList($user_id);
			$this->load->view('dashboard/management/subagent_deposite',$users);
		}

    function changeDeposit($user_id1,$deposit_id1,$data){
    	$data = base64_decode($data);
    	$user_id 	= json_decode(base64_decode($user_id1));
    	$deposit_id 	= json_decode(base64_decode($deposit_id1));
    	$deposit['user_id'] = $user_id;
    	$deposit['error'] = $data;
    	$deposit['deposit_id'] = $deposit_id;
    	$deposit['user_list'] = $this->Usermanagement_Model->getSubAgentList($user_id);
		$deposit['depsoite'] 	= $this->Usermanagement_Model->getAgentDepositList($user_id,$deposit_id);
    	
    	$this->load->view('dashboard/management/changeDeposit',$deposit);
    }	


	
	function addDeposit(){
		$user_id 	= $this->session->userdata('user_details_id');
		if($user_id != ''){
			if(count($_POST) > 0){
				if(isset($_POST['deposit_type']) && $_POST['deposit_type'] == ""){
					$data['error']['deposit-type-error'] = "Please select the deposit type";
				}
				
				if(isset($_POST['currencycode']) && $_POST['currencycode'] == ""){
					$data['error']['currencycode-error'] = "Please select the currency code";
				}
				
				if(isset($_POST['amount_credit']) && $_POST['amount_credit'] == ""){
					$data['error']['amount-credit-error'] = "Please enter the amount";
				}
				
				if(isset($_POST['amount_credit']) && $_POST['amount_credit'] != ""){
				   $amount_validation = $this->Validation_Model->amountValidation($_POST['amount_credit']);	
				    if(!$amount_validation){
				  $data['error']['amount-credit-error'] = "Please enter the correct amount (1000.00)";
			       }
				}
			
				if(isset($_POST['remarks']) && $_POST['remarks'] == ""){
					$data['error']['remarks-error'] = "Please enter the remarks";
				}
				
				if(isset($data['error']) && count($data['error']) > 0) {
				 $data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			   }

			
				//print_r($user_id); exit();
			   if ($this->session->userdata('user_type') == '4') {
			   	$users = $this->Usermanagement_Model->getDepositSubAgentList($user_id);
			   	$branch_id = $users[0]->branch_id;
			   	$branch = $this->Usermanagement_Model->getDepositAgentList($branch_id);
			   	$email = $branch[0]->user_email;
			   	
			   }else {
			   	$users = $this->Usermanagement_Model->getDepositAgentList($user_id);
			   	$email = 'Shashi@thechinagap.com';
			   }
				$user = $users[0]->user_email;
				$user_account_number = $users[0]->user_account_number;
				//echo '<pre>'; print_r($users); exit();
				//print_r($this->session->userdata('user_details_id')); exit();
				$currency_code = $_POST['currencycode'];
				$curreny =	$this->Usermanagement_Model->currenyAmount($currency_code);
			    $exchange = $curreny[0]->value;
			    
				$this->Usermanagement_Model->addDepositDetails($_POST,$exchange,$user_id);
				$this->Email_Model->sendAgentDepositAlertMail($_POST,$user,$email,$user_account_number);
				$data['status'] = 1;
				$data['url'] = base_url().'usermanagement/depositManagement';
				print json_encode($data);
				return;
			}else{
				redirect('usermanagement/depositManagement');
			}
		}else{
			redirect('usermanagement/depositManagement');
		}
	}
	
	function addAgentUsers(){
		if(count($_POST) > 0){
			
			$agent_validation = $this->agentUserFormValidation($_POST);
			
			if($agent_validation['status']['status'] == 3) {
				print  json_encode(array_merge($agent_validation['error'], $agent_validation['status']));
				return;
			}
			$email = $_POST['email'];
			$Query="select * from  user_details  where user_email ='".$email."' AND user_type_id='4' ";
			
			$query=$this->db->query($Query);
			if ($query->num_rows() > 0)
		    {
				$data['status'] = 3;
				$data['email-error'] = "Email Id Already exists";
				print  json_encode($data);
				return;
		  }else{
			try{
		    $this->General_Model->begin_transaction() ;
		    
		    $addressing = array( 
				'country_id'        => $_POST['country'],
				'address' => $_POST['address'],
				'city_name' => $_POST['cityname'],
				); 
			$address = $this->Usermanagement_Model->userAddress($addressing); 
		    $acc_no = 'TCG-'.rand(1,1000);
			$this->Usermanagement_Model->addUseragentDetails($_POST, $acc_no,$address);
				$user['user'] 		= "B2B User";
				$user['user_email'] = $_POST['email'];
				$user['name'] 	= $_POST['user_name'];
				//$data['user_name'] 	= $_POST['email'];
				$user['user_acc_no'] 	= $acc_no;
				$user['url'] 			= base_url();
				$user['profile_photo']= '';
			$user['email_template'] = $this->Email_Model->get_email_template('Registration')->row();
			$this->Email_Model->sendmail_reg($user);
			$this->General_Model->commit_transaction() ;
		        $data['status'] = 1;
				$data['message'] = "Agent added successfully";
				$data['url'] = base_url()."usermanagement";
				$data['active_div'] = "sub_agent";
				print  json_encode($data);
				return;
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction() ;
			return $e;
		} 
	}
   }
 }
 

 // for creating b2b ,b2bc users from usermanagement on dashborad
 function addUsers(){
		if(count($_POST) > 0){
			
			  $agent_validation = $this->agentUserFormValidation($_POST);
			  if($agent_validation['status']['status'] == 3) {
				   print  json_encode(array_merge($agent_validation['error'], $agent_validation['status']));
				   return;
			  }
			  $email = $_POST['email'];
			  $Query="select * from  user_details  where user_email ='".$email."' AND user_type_id=".$_POST['user_type'];
			 $query=$this->db->query($Query);
			 if ($query->num_rows() > 0) {
				$data['status'] = 3;
				$data['email-error'] = "Email Id Already exists";
				print  json_encode($data);
				return;
		     }else {
			    try{
		              $this->General_Model->begin_transaction() ;
		    
		             $addressing = array( 'country_id'        => $_POST['country'],
                                          'address'           => $_POST['address'],
				                          'city_name'         => $_POST['cityname'],
				                    ); 
			         $address = $this->Usermanagement_Model->userAddress($addressing); 
		             $acc_no = 'TRVL-'.rand(1,1000);
			         $this->Usermanagement_Model->addUseragentDetails($_POST, $acc_no,$address);
			         if($_POST['user_type'] == 4){
						
                         $user['user'] 		 = "B2B User";
		          	 }
		             if($_POST['user_type'] == 5){
				         $user['user'] 		 = "B2BC User";
			         }
				     $user['user_email']     = $_POST['email'];
				     $user['name'] 	         = $_POST['user_name'];
				     $user['user_acc_no'] 	 = $acc_no;
				     $user['url'] 			 = base_url();
				     $user['profile_photo']  = 'user-avatar.jpg';
			         $user['email_template'] = $this->Email_Model->get_email_template('Registration')->row();
			         $this->Email_Model->sendmail_reg($user);
			         $this->General_Model->commit_transaction() ;
		             $data['status'] = 1;
				     $data['message'] = "added successfully";
				     $data['url'] = base_url()."usermanagement";
				     $data['active_div'] = "sub_agent";
				     print  json_encode($data);
				     return;
		  } catch(Exception $e) {
			$this->General_Model->rollback_transaction() ;
			return $e;
		} 
	}
   }
 }
 
 function editAdminagent($agent_id1){
	 
	 $agent_id = json_decode(base64_decode($agent_id1));
	 $data['agentadmin_list'] =  $this->Account_Model->get_user_list($agent_id);
	 $data['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
	 //$data['agentadmin_list'] = $this->Account_Model->get_user_list();
	 //print_r($data['agentadmin_list']['user_info']);exit;
	 $data['editAgent'] = $this->load->view('dashboard/management/edit_agent', $data, true);
	 print json_encode($data); 
	 return;
	 
}


 function updateAgentUsers($agent_id1){
	   $agent_id = json_decode(base64_decode($agent_id1));
	 
		if(count($_POST) > 0){
			$agent_validation = $this->agentUserFormValidation($_POST);
			
			if($agent_validation['status']['status'] == 3) {
				print  json_encode(array_merge($agent_validation['error'], $agent_validation['status']));
				return;
			}
			$email = $_POST['email'];
			$Query="select * from  user_details  where user_email ='".$email."' AND user_type_id='4' and user_details_id !=".$agent_id;
			
			$query=$this->db->query($Query);
			if ($query->num_rows() > 0)
		    {
				$data['status'] = 3;
				$data['email-error'] = "Email Id Already exists";
				print  json_encode($data);
				return;
		  }else{
			try{
		    $this->General_Model->begin_transaction() ;
		    
			$this->Usermanagement_Model->updateUseragentDetails($_POST, $agent_id);
			$this->General_Model->commit_transaction() ;
		        $data['status'] = 1;
				$data['message'] = "Agent updated successfully";
				$data['url'] = base_url()."usermanagement";
				$data['active_div'] = "sub_agent";
				print  json_encode($data);
				return;
		} catch(Exception $e) {
			$this->General_Model->rollback_transaction() ;
			return $e;
		} 
	}
   }
 }
 
    function active_b2b_users($user_id1){
        $user_id    = json_decode(base64_decode($user_id1));
        $user = $this->Account_Model->get_user_list($user_id);
        if($user_id != ''){
            $this->Usermanagement_Model->active_users($user_id);
             $user['email_template'] = $this->Email_Model->get_email_template('add_user_active')->row();
            $this->Email_Model->send_add_user_active($user);
        }
        $data['tab'] = 'tab12';
        redirect('usermanagement');
    }
    
     function inactive_b2b_users($user_id1){
        $user_id    = json_decode(base64_decode($user_id1));
        $user = $this->Account_Model->get_user_list($user_id);
        if($user_id != ''){
            $this->Usermanagement_Model->inactive_users($user_id);
            $user['email_template'] = $this->Email_Model->get_email_template('user_deactivate')->row();
            $this->Email_Model->send_add_user_active($user);
        }
       redirect('usermanagement');
    }
    
    
	function addMarkup(){
		
		if(count($_POST) > 0) {
			if(isset($_POST['markup_type']) && $_POST['markup_type'] == "") {
					$data['error']['markup-type-error'] = "Please select valid markup type";
			}
			
			if(isset($_POST['sub_agent_id']) && $_POST['sub_agent_id'] == "") {
				$data['error']['sub-agent-id-error'] = "Please select valid sub agent id";
			}
			
			if(isset($_POST['product_id']) && $_POST['product_id'] == "") {
				$data['error']['markup-type-error'] = "Please select valid agent type";
			}
		
			
			if(isset($_POST['markup_value_type']) && $_POST['markup_value_type'] == "") {
				$data['error']['markup-value-type-error'] = "Please select valid agent type";
			}
			
			if(isset($_POST['markup_value']) && $_POST['markup_value'] == "") {
				$data['error']['markup-value-error'] = "Please enter the valid amount";
			}
		
		    if(isset($_POST['markup_value']) && $_POST['markup_value'] != "") {
				  $amount_validation = $this->Validation_Model->amountValidation($_POST['markup_value']);	
				    if(!$amount_validation){
				        $data['error']['markup-value-error'] = "Please enter the correct amount (1000.00)";
			       }
			}
			$created_id = $this->session->userdata('user_details_id');
			$branch_id = $this->session->userdata('branch_id');
			$user_id = $_POST['sub_agent_id'];
			if($_POST['markup_type'] == 'GENERAL') {
				$markup = $this->Settings_Model->get_available_general_markup($_POST['product_id'],$markup_value_type = '', $markupvalue = '', 'sub_agent_markup',$user_id,$created_id,$branch_id);
				if(count($markup) > 0){
					 $data['error']['markup-already-available'] = 'Entered markup is alrady available';
				}
		   }else{
				//$markup = $this->Settings_Model->get_available_specific_markup($_POST['product_id'],$markup_value_type = '', $markupvalue = '',  'sub_agent_markup', $markup_id='', $_POST['country_code'],  $_POST['sub_agent_id'] );
				$markup = $this->Settings_Model->get_available_specific_markup($_POST['product_id'],  $_POST['markup_type'], $markupvalue = '', $tablename='sub_agent_markup', $markup_id='', $_POST['country_code'], $_POST['start_date'],$_POST['end_date'], $_POST['expiry_date'],$_POST['expiry_date_to'],$user_id,$created_id,$branch_id);
				if(count($markup) > 0){
					 $data['error']['markup-already-available'] = 'Entered markup is alrady available';
				}
			}
		
		
			if(isset($data['error']) && count($data['error']) > 0) {
				 $data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			   }
			
			
		$this->Settings_Model->addSubAgentMarkup($_POST);
	    $data['status'] = 1;
        $data['url'] = base_url().'usermanagement';
        print json_encode($data);
		}else{
	    $data['status'] = 1;
        $data['url'] = base_url().'usermanagement';
        print json_encode($data);
		}
		
	}
	
	function deleteMarkup($markup_id1){
		$markup_id =json_decode(base64_decode($markup_id1));
		if($markup_id != ''){
			$this->Settings_Model->deletemarkups($markup_id);
			redirect('usermanagement');
		}else{
			redirect('usermanagement');
		}
	}
	
	function changeMarkupStatus($markup_id1 ,  $status1,$class){
		$markup_id =json_decode(base64_decode($markup_id1));
		$status = json_decode(base64_decode($status1));
		$class =json_decode(base64_decode($class));
		//print_r($markup_id); print_r($status); exit();
		if($markup_id != ''){
			$this->Settings_Model->changeSubAgentMarkupStatus($markup_id, $status);
			redirect('usermanagement?class='.$class.'');
		}else{
			redirect('usermanagement');
		}
	}
	
	function editMarkup($markup_id1){
		$markup_id1 = json_decode(base64_decode($markup_id1));

		$user_id 	= $this->session->userdata('user_details_id');
	    $dashboard['agentadmin_list'] = $this->Usermanagement_Model->getAgentadminList($user_id);
		$dashboard['products'] = $this->Dashboard_Model->getProducts();
		$dashboard['api'] = $this->General_Model->get_api_list();
		$dashboard['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
		$dashboard['agent_markups'] = $this->Settings_Model->getAgentMarkups($this->session->userdata('user_details_id'), $markup_id1, 'sub_agent_markup');
		$dashboard['updateMarkup'] = $this->load->view('dashboard/management/editb2b_markup', $dashboard, true);
		print json_encode($dashboard);
		return;
	}
	
	function updateMarkup($markup_id1){
		$markup_id = json_decode(base64_decode($markup_id1));
		if(count($_POST) > 0) {
			if(isset($_POST['markup_type']) && $_POST['markup_type'] == "") {
					$data['error']['markup-type-error'] = "Please select valid markup type";
			}
			
			if(isset($_POST['sub_agent_id']) && $_POST['sub_agent_id'] == "") {
				$data['error']['sub-agent-id-error'] = "Please select valid sub agent id";
			}
			
			if(isset($_POST['product_id']) && $_POST['product_id'] == "") {
				$data['error']['markup-type-error'] = "Please select valid agent type";
			}
		
			
			if(isset($_POST['markup_value_type']) && $_POST['markup_value_type'] == "") {
				$data['error']['markup-value-type-error'] = "Please select valid agent type";
			}
			
			if(isset($_POST['markup_value']) && $_POST['markup_value'] == "") {
				$data['error']['markup-value-error'] = "Please enter the valid amount";
			}
			
		    if(isset($_POST['markup_value']) && $_POST['markup_value'] != "") {
				  $amount_validation = $this->Validation_Model->amountValidation($_POST['markup_value']);	
				    if(!$amount_validation){
				        $data['error']['markup-value-error'] = "Please enter the correct amount (1000.00)";
			       }
			}
			
			if($_POST['markup_type'] == 'GENERAL') {
				$markup = $this->Settings_Model->get_available_general_markup($_POST['product_id'], $_POST['markup_value_type'],$_POST['markup_value'], 'sub_agent_markup', $markup_id);

				if(count($markup) > 0){
					 $data['error']['markup-already-available'] = 'Entered markup is alrady available';
				}
		   }else{
				//$markup = $this->Settings_Model->get_available_specific_markup($_POST['product_id'], $_POST['markup_value_type'],$_POST['markup_value'], 'sub_agent_markup', $markup_id, $_POST['country_code'], $_POST['sub_agent_id'] );
				$markup = $this->Settings_Model->get_available_specific_markup($_POST['product_id'],  $_POST['markup_type'], $markupvalue = '', $tablename='sub_agent_markup', $markup_id, $_POST['country_code'], $_POST['start_date'],$_POST['end_date'], $_POST['expiry_date'],$_POST['expiry_date_to']);
				//print_r(count($markup)); exit();
				if(count($markup) > 0){
					 $data['error']['markup-already-available'] = 'Entered markup is alrady available';
				}
			}
			
			if(isset($data['error']) && count($data['error']) > 0) {
				 $data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			   }
			   //print_r($markup_id); exit();
		$this->Settings_Model->updateSubAgentMarkup($_POST, $markup_id);
	    $data['status'] = 1;
        $data['url'] = base_url().'usermanagement';
        print json_encode($data);
		}else{
	    $data['status'] = 1;
        $data['url'] = base_url().'usermanagement';
        print json_encode($data);
		}
	}
	
	function editProfile(){
		$users = $this->Account_Model->get_user_details($this->session->userdata('user_details_id'));
		$users['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
		$this->load->view('dashboard/profile/profile_info', $users);
	}
	
	 public function updateProfile(){ 
		$ssid = $this->session->userdata('user_details_id');
		
		if(count($_POST) > 0) {
			
			
			if($_POST['agent_id'] == ''){
			   $data['error']['agentid-error'] = "Please enter valid agent id";
			}
			
			if($_POST['mobile_no'] == ''){
			   $data['error']['contact-error'] = "Please enter valid Contact Number";
			 }
			
			if(isset($_POST['mobile_no']) && $_POST['mobile_no'] != ''){
			  $contact_validation = $this->Validation_Model->numberWithSpecialCharacter($_POST['mobile_no']);	
			   if(!$contact_validation){
				 $data['error']['contact-error'] = "Please enter valid Contact Number";
			 }
			}
			
			if($_POST['address'] == ''){
			   $data['error']['address-error'] = "Please enter address";
			}
			
			if($_POST['city'] == ''){
			   $data['error']['city-error'] = "Please enter City";
			}
			
			if($_POST['state'] == ''){
			   $data['error']['state-error'] = "Please enter state";
			}
			
			if($_POST['postal_code'] == ''){
			   $data['error']['zip-error'] = "Please enter Zipcode";
			}
			if($_POST['country_id'] == ''){
			   $data['error']['country-error'] = "Please select country";
			}
		
			if(isset($data['error']) && count($data['error']) > 0) {
				 $data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			}
			
		//echo '<pre>'; print_r($this->input->post()); exit();
		 $postAddressData = array(
                'address'                        => $this->input->post('address'),
                'city_name'                      => $this->input->post('city'),
                'state_name'                     => $this->input->post('state'),
                'zip_code'                       => $this->input->post('postal_code'),
                'country_id'                     => $this->input->post('country_id'),
                );
         if($this->input->post('address_details_id') != 1){
			 $address_details_id  = $this->Usermanagement_Model->updateAgentAddressDetails($this->input->post('address_details_id'),$postAddressData);
		 }else{
			 $address_details_id = $this->Usermanagement_Model->insertAgentAddressDetails($postAddressData);
		 }
		$offers = json_encode($this->input->post('offers'));
        $postData = array(
                'user_name'                         => $this->input->post('user_name'),
                'agent_name'						=>$this->input->post('agent_name'),
                //'user_email'                        => $this->input->post('email'),
                'address_details_id'                => $address_details_id,
                'user_cell_phone'                   => $this->input->post('mobile_no'),
                'country_id'                        => $this->input->post('country_id'),
                'company_name'						=> $this->input->post('company_name'),
                'agent_tradingname'					=> $this->input->post('agencytrading'),
                'agent_licenseno'					=> $this->input->post('agencylicense'),
                'user_home_phone'					=> $this->input->post('companyphone'),
                'wechat_number'						=> $this->input->post('wechatid'),
                'qq'								=> $this->input->post('qq'),
                'grouptravel'						=> $this->input->post('grouptravel'),
                'fit'								=> $this->input->post('fit'),
                'bussiness'							=> $this->input->post('bussiness'),
                'offers' 							=> $offers,
                'user_updation_date_time'           => date('Y-m-d H:i:s'),
            );
        $this->Usermanagement_Model->updateAgentDetails($ssid,$postData);
        $data['status'] = 1;
        $data['url'] = base_url().'usermanagement/editProfile';
        print json_encode($data);
    }
}

 function agentUserFormValidation($post)
  {
	     if( $post['user_name'] == ""){
			 $data['error']['name-error'] = "Please enter valid name";
			}
			
			if(isset($post['user_name']) && $post['user_name'] != '') {
			 $name_validation = $this->Validation_Model->alphabetValidation($_POST['user_name']);
			 if(!$name_validation){
				$data['error']['name-error'] = "Please enter valid name";
			 }
			}
			/*if($post['company_name'] == ''){
			   $data['error']['company-error'] = "Please enter valid Agency Name";
			}
			*/
			if($post['email'] == ''){
			   $data['error']['email-error'] = "Please enter valid Email Address";
			 }
			 
			if(isset($post['email'])  && $post['email'] != ''){
			   $email_validation = $this->Validation_Model->emailIdFormat($post['email']);	
			    if(!$email_validation){
				  $data['error']['email-error'] = "Please enter valid Email";
			   }
			}
			
			if(isset($post['password']) && $post['password'] == ""){
				$data['error']['password-error'] = "Please enter password";
			}
			
			if(isset($post['password']) && $post['password'] != ''){
			   $password_validation = $this->Validation_Model->passwordValidation($post['password']);	
			    if(!$password_validation){
				   $data['error']['password-error'] = "Password should contain atleast on Capital letter, one Numbers and one Special Characters";
			 }
			}
			
			if(isset($post['cpassword']) && $post['cpassword'] == ""){
				$data['error']['password-error'] = "Please enter confirm password";
			}
			 
			 if($post['country'] == ""){
				$data['error']['password-error'] = "Please enter confirm password";
			}
			
			if(isset($post['cpassword']) && $post['cpassword'] != ''){
			   $password_validation = $this->Validation_Model->checkPasswords($post['password'], $post['cpassword']);	
			    if(!$password_validation){
				   $data['error']['password-error'] = "Password not valid";
			 }
			}
			
			if(isset($post['mob_number']) && $post['mob_number'] != ''){
			  $contact_validation = $this->Validation_Model->numberWithSpecialCharacter($post['mob_number']);	
			   if(!$contact_validation){
				 $data['error']['contact-error'] = "Please enter valid Contact Number";
			 }
			}
			
		    if(isset($data['error']) && count($data['error']) > 0) {
				$data['status']['status'] = 3;
			}else{
				$data['status']['status'] = 1;
			}
			
			return $data;
  }

function depositManagement(){
		$user_id 	= $this->session->userdata('user_details_id');
		$users['currency'] = $this->General_Model->getCurrencyList();
		$users['products'] = $this->Dashboard_Model->getProducts();
		$users['agentadmin_list'] = $this->Account_Model->get_user_list();
		$users['depsoite'] 	= $this->Usermanagement_Model->getDepositList($user_id);
		$users['api'] = $this->General_Model->get_api_list();
		$users['agentadmin_list'] = $this->Usermanagement_Model->getAgentadminList($user_id);
		$users['agent_markups'] = $this->Settings_Model->getSubAgentMarkups($this->session->userdata('user_details_id'));
		$users['nationality_countries'] = $this->General_Model->getNationalityCountries()->result();
		$this->load->view('dashboard/deposit/deposit', $users);
	}

function addAgentDeposit(){
		$user_id 	= $this->session->userdata('user_details_id');
		if($user_id != ''){
			if(count($_POST) > 0){
				if(isset($_POST['subagents']) && $_POST['subagents'] == ""){
					$data['error']['subagents-error'] = "Please select the SubAgents";
				}
				if(isset($_POST['deposit_type']) && $_POST['deposit_type'] == ""){
					$data['error']['deposit-type-error'] = "Please select the deposit type";
				}
				
				if(isset($_POST['currencycode']) && $_POST['currencycode'] == ""){
					$data['error']['currencycode-error'] = "Please select the currency code";
				}
				
				if(isset($_POST['amount_credit']) && $_POST['amount_credit'] == ""){
					$data['error']['amount-credit-error'] = "Please enter the amount";
				}
				
				if(isset($_POST['amount_credit']) && $_POST['amount_credit'] != ""){
				   $amount_validation = $this->Validation_Model->amountValidation($_POST['amount_credit']);	
				    if(!$amount_validation){
				  $data['error']['amount-credit-error'] = "Please enter the correct amount (1000.00)";
			       }
				}
				
				if(isset($_POST['remarks']) && $_POST['remarks'] == ""){
					$data['error']['remarks-error'] = "Please enter the remarks";
				}
				
				if(isset($data['error']) && count($data['error']) > 0) {
				 $data['status']['status'] = 3;
				print  json_encode(array_merge($data['error'], $data['status']));
				return ;
			   }
			
				$currency_code = $_POST['currencycode'];
				$curreny =	$this->Usermanagement_Model->currenyAmount($currency_code);
			    $exchange = $curreny[0]->value;
				$this->Usermanagement_Model->addAgentDepositDetails($_POST,$exchange,$user_id);
				$data['status'] = 1;
				$data['url'] = base_url().'usermanagement';
				print json_encode($data);
				return;
			}else{
				redirect('usermanagement');
			}
		}else{
			redirect('usermanagement');
		}
	}

	

function approvingDeposit($user_id1,$deposit_id1){  
		$user_id 		= json_decode(base64_decode($user_id1));
		$user = $this->Usermanagement_Model->getAgentusersList($user_id);
		$deposit_id 	= json_decode(base64_decode($deposit_id1));
		
		//print_r($user_id); exit();
		//print_r($deposit_id); exit();
		$deposit_type = $_POST['deposit_type'];
		$amount 		= $_POST['amount'];
		$sub_agent_id 		= $_POST['sub_agent_id'];
		$adminRemarks = $_POST['admin_remarks'];
		 $deposit = $this->Usermanagement_Model->getAgentDepsoiteList($deposit_id);
		if($user_id != ''){
			$acceptedornot = $this->Usermanagement_Model->getAgentDepsoiteList($deposit_id);
			
			//Currency Conversion Start
								$pdata = $acceptedornot[0]->currencycode;
                                $TotalPrice_Curr  = 'AUD';
                                $TotalPrice =  $acceptedornot[0]->amount_credit;
                                $amount = $this->General_Model->currency_convertor($TotalPrice,$pdata,$TotalPrice_Curr);
            //Currency Conversion End
                                
			if($acceptedornot[0]->status != "Accepted"){
				if ($deposit_type == "Accepted") {
					$subagentmarkup 	= $this->session->userdata('user_details_id');
					
					$parent_deposit_check = $this->Usermanagement_Model->get_agent_deposit_balance($subagentmarkup);
					
					//echo '<pre>sanjay'; print_r($parent_deposit_check[0]); exit();
					if ($parent_deposit_check[0]->balance_credit >= $amount) {
						
					$this->Usermanagement_Model->approveSubAgentDeposit($deposit_id,$adminRemarks);
					$balance_amount = $parent_deposit_check[0]->balance_credit - $amount;
					
					$this->Usermanagement_Model->update_agent_balance_debit($subagentmarkup,$amount,$balance_amount);
					
					$deposit_check = $this->Usermanagement_Model->get_agent_deposit_balance($sub_agent_id);
					//print_r($deposit_check[0]); exit();
					 if ($deposit_check == '') {
						  $this->Usermanagement_Model->insertAgentBalance($sub_agent_id,$amount);
					}else{
						$balance_amount = $deposit_check[0]->balance_credit;
						$update_balance_amount = $amount + $balance_amount;
						$this->Usermanagement_Model->updateAgentBalance($sub_agent_id,$amount,$update_balance_amount);
						
					}
					
					$user['email_template'] = $this->Email_Model->get_email_template('agent_deposit_accepted_mail')->row();
					$this->Email_Model->sendAgentDepositMail($user,$deposit);
				}else{
					$deposit = "Sorry, You Dont't have Enough balance in your Account To Approve";
					$deposit = base64_encode($deposit);
					redirect('usermanagement/changeDeposit/'.$user_id1.'/'.$deposit_id1.'/'.$deposit);
				} } else{
					$this->Usermanagement_Model->cancelSubAgentDepoist($deposit_id,$adminRemarks);
					$user['email_template'] = $this->Email_Model->get_email_template('agent_deposit_cancel_mail')->row();
					$this->Email_Model->sendAgentDepositMail($user,$deposit);
				}
			
		}
		
		
		redirect('usermanagement/depositList/'.$user_id1,'refresh');
		
	}
	}
	
	 function getUserPrivileges($user_id1){
	$user['user_id'] = $user_id1;
	   $user['privilege'] = $this->Dashboard_Model->get_sub_agent_dashboardmenu();
	   $user['privilege_template'] = $this->load->view("dashboard/management/user_privilege", $user, true);
	   $user['status'] = 1;
	   echo json_encode($user);
    }
    
    function update_staff_privilege(){
		if(count($_POST) > 0){
			$this->Usermanagement_Model->update_privileges($_POST);
			redirect('usermanagement');
		}else{
			redirect('usermanagement');
		}
	}
	
	
	function getUserProducts($user_id1){
		$user['user_id'] = $user_id1;
		$user['products'] = $this->Dashboard_Model->getProducts();
		$user['product_template'] = $this->load->view("dashboard/management/user_product", $user, true);
	    $user['status'] = 1;
	   echo json_encode($user);
	}
	
	
	
	 function update_product_privilege(){
		if(count($_POST) > 0){
			$this->Usermanagement_Model->update_product_privileges($_POST);
			redirect('usermanagement');
		}else{
			redirect('usermanagement');
		}
	}
}
?>

