<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(-1);
class Ajax extends CI_Controller {
	private $current_module;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('flight_model');
		$this->load->model('General_Model');
		$this->load->model('hotel_model');
		$this->current_module = $this->config->item('current_module');
		$this->load->model('db_cache_api');
		$this->load->library('currency');
		
		
	}
	/**
	 * get city list based on country
	 * @param $country_id
	 * @param $default_select
	 */
	function get_city_list($country_id=0, $default_select=0)
	{
		if (intval($country_id) != 0) {
			$condition = array('country' => $country_id);
			$order_by = array('destination' => 'asc');
			$option_list = $this->custom_db->single_table_records('api_city_list', 'origin as k, destination as v', $condition, 0, 1000000, $order_by);
			if (valid_array($option_list['data'])) {
				echo get_compressed_output(generate_options($option_list['data'], array($default_select)));
				exit;
			}
		}
	}

	/**
	 *
	 * @param $continent_id
	 * @param $default_select
	 * @param $zone_id
	 */
	function get_country_list($continent_id=array(), $default_select=0,$zone_id=0)
	{
		$this->load->model('general_model');
		$continent_id=urldecode($continent_id);
		if (intval($continent_id) != 0) {
			$option_list = $this->general_model->get_country_list($continent_id,$zone_id);
			if (valid_array($option_list['data'])) {
				echo get_compressed_output(generate_options($option_list['data'], array($default_select)));
			}
		}
	}

	/**
	 *Get Location List
	 */
	function location_list($limit=AUTO_SUGGESTION_LIMIT)
	{
		$chars = $_GET['term'];
		$list = $this->general_model->get_location_list($chars, $limit);
		$temp_list = '';
		if (valid_array($list) == true) {
			foreach ($list as $k => $v) {
				$temp_list[] = array('id' => $k, 'label' => $v['name'], 'value' => $v['origin']);
			}
		}
		$this->output_compressed_data($temp_list);
	}

	/**
	 *Get Location List
	 */
	function city_list($limit=AUTO_SUGGESTION_LIMIT)
	{
		$chars = $_GET['term'];
		$list = $this->general_model->get_city_list($chars, $limit);
		$temp_list = '';
		if (valid_array($list) == true) {
			foreach ($list as $k => $v) {
				$temp_list[] = array('id' => $k, 'label' => $v['name'], 'value' => $v['origin']);
			}
		}
		$this->output_compressed_data($temp_list);
	}

	/**
	 * Arjun J Gowda
	 * @param unknown_type $currency_origin origin of currency - default to USD
	 */
	function get_currency_value($currency_origin=0)
	{
		$data = $this->custom_db->single_table_records('currency_converter', 'value', array('id' => intval($currency_origin)));
		if (valid_array($data['data'])) {
			$response = $data['data'][0]['value'];
		} else {
			$response = 1;
		}
		header('Content-type:application/json');
		echo json_encode(array('value' => $response));
		exit;
	}

	/*
	 *
	 * Flight(Airport) auto suggest
	 *
	 */
	function get_airport_code_list()
	{
		
		
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$result = array();

		$__airports = $this->flight_model->get_airport_list($term)->result();
		if (valid_array($__airports) == false) {
			$__airports = $this->flight_model->get_airport_list('')->result();
		}
		$airports = array();
		foreach($__airports as $airport){
			$airports['label'] = $airport->airport_city.', '.$airport->country.' ('.$airport->airport_code.')';
			$airports['value'] = $airport->airport_city.' ('.$airport->airport_code.')';
			$airports['id'] = $airport->origin;
			if (empty($airport->top_destination) == false) {
				$airports['category'] = 'Top cities';
				$airports['type'] = 'Top cities';
			} else {
				$airports['category'] = 'Search Results';
				$airports['type'] = 'Search Results';
			}

			array_push($result,$airports);
		}
		$this->output_compressed_data($result);
	}

	/*
	 *
	 * Hotels City auto suggest
	 *
	 */
	function get_hotel_city_list()
	{
		$this->load->model('hotel_model');
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$data_list = $this->hotel_model->get_hotel_city_list($term);
		if (valid_array($data_list) == false) {
			$data_list = $this->hotel_model->get_hotel_city_list('');
		}
		$suggestion_list = array();
		$result = '';
		foreach($data_list as $city_list){
			$suggestion_list['label'] = $city_list['city_name'].', '.$city_list['country_name'].'';
			$suggestion_list['value'] = hotel_suggestion_value($city_list['city_name'], $city_list['country_name']);
			$suggestion_list['id'] = $city_list['origin'];
			if (empty($city_list['top_destination']) == false) {
				$suggestion_list['category'] = 'Top cities';
				$suggestion_list['type'] = 'Top cities';
			} else {
				$suggestion_list['category'] = 'Search Results';
				$suggestion_list['type'] = 'Search Results';
			}
			if (intval($city_list['cache_hotels_count']) > 0) {
				$suggestion_list['count'] = $city_list['cache_hotels_count'];
			} else {
				$suggestion_list['count'] = 0;
			}
			$result[] = $suggestion_list;
		}
		$this->output_compressed_data($result);
	}

	/**
	 * Auto Suggestion for bus stations
	 */
	function bus_stations()
	{
		$this->load->model('bus_model');
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$data_list = $this->bus_model->get_bus_station_list($term);
		if (valid_array($data_list) == false) {
			$data_list = $this->bus_model->get_bus_station_list('');
		}
		$suggestion_list = array();
		$result = '';
		foreach($data_list as $city_list){
			$suggestion_list['label'] = $city_list['name'];
			$suggestion_list['value'] = $city_list['name'];
			$suggestion_list['id'] = $city_list['origin'];
			if (empty($city_list['top_destination']) == false) {
				$suggestion_list['category'] = 'Top cities';
				$suggestion_list['type'] = 'Top cities';
			} else {
				$suggestion_list['category'] = 'Search Results';
				$suggestion_list['type'] = 'Search Results';
			}
			$result[] = $suggestion_list;
		}
		$this->output_compressed_data($result);
	}

	/**
	 * Load hotels from different source
	 */
	function hotel_list($offset=0)
	{
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();
		$limit = $this->config->item('hotel_per_page_limit');

		if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
			load_hotel_lib($search_params['booking_source']);
			switch($search_params['booking_source']) {
				case PROVAB_HOTEL_BOOKING_SOURCE :
					//getting search params from table
					$safe_search_data = $this->hotel_model->get_safe_search_data($search_params['search_id']);
					
					//Meaning hotels are loaded first time
					$raw_hotel_list = $this->hotel_lib->get_hotel_list(abs($search_params['search_id']));

					if ($raw_hotel_list['status']) {
						//Converting API currency data to preferred currency
						$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
						$raw_hotel_list = $this->hotel_lib->search_data_in_preferred_currency($raw_hotel_list, $currency_obj);

						//Display 
						$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
						//Update currency and filter summary appended
						if (isset($search_params['filters']) == true and valid_array($search_params['filters']) == true) {
							$filters = $search_params['filters'];
						} else {
							$filters = array();
						}
						$raw_hotel_list['data'] = $this->hotel_lib->format_search_response($raw_hotel_list['data'], $currency_obj, $search_params['search_id'], 'b2c', $filters);
                                                
						$source_result_count = $raw_hotel_list['data']['source_result_count'];
						$filter_result_count = $raw_hotel_list['data']['filter_result_count'];
						if (intval($offset) == 0) {
							//Need filters only if the data is being loaded first time
							$filters = $this->hotel_lib->filter_summary($raw_hotel_list['data']);
							$response['filters'] = $filters['data'];
						}
						$raw_hotel_list['data'] = $this->hotel_lib->get_page_data($raw_hotel_list['data'], $offset, $limit);

						$attr['search_id'] = abs($search_params['search_id']);

						$response['data'] = get_compressed_output(
						$this->load->view('hotel/tbo/tbo_search_result',
						array('currency_obj' => $currency_obj, 'raw_hotel_list' => $raw_hotel_list['data'],
									'search_id' => $search_params['search_id'], 'booking_source' => $search_params['booking_source'],							
									'attr' => $attr,
									'search_params'=>$safe_search_data
						),true));
						$response['status'] = SUCCESS_STATUS;
						$response['total_result_count'] = $source_result_count;
						$response['filter_result_count'] = $filter_result_count;
						$response['offset'] = $offset+$limit;
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	} 

	/**
	 * Compress and output data
	 * @param array $data
	 */
	private function output_compressed_data($data)
	{
		while (ob_get_level() > 0) { ob_end_clean() ; }
		ob_start("ob_gzhandler");
		header('Content-type:application/json');
		echo json_encode($data);
		ob_end_flush();
		exit;
	}

	/**
	 * Load hotels from different source
	 */
	function bus_list()
	{
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();
		if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
			load_bus_lib($search_params['booking_source']);
			switch($search_params['booking_source']) {
				case PROVAB_BUS_BOOKING_SOURCE :
					$raw_bus_list = $this->bus_lib->get_bus_list(abs($search_params['search_id']));
					if ($raw_bus_list['status']) {
						//Converting API currency data to preferred currency
						$currency_obj = new Currency(array('module_type' => 'bus','from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
						$raw_bus_list = $this->bus_lib->search_data_in_preferred_currency($raw_bus_list, $currency_obj);
						//Display Bus List
						$currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
						$response['data'] = get_compressed_output(
						$this->load->view('bus/travelyaari/travelyaari_search_result',
						array('currency_obj' => $currency_obj, 'raw_bus_list' => $raw_bus_list['data']['result'], 'search_id' => $search_params['search_id'], 'booking_source' => $search_params['booking_source']),true)
						);
						$response['status'] = SUCCESS_STATUS;
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	function get_bus_information()
	{
		$response['data'] = 'No Details Found';
		$response['status'] = false;
		//check params
		$params = $this->input->post();
		if (empty($params['booking_source']) == false and empty($params['search_id']) == false and intval($params['search_id']) > 0) {
			load_bus_lib($params['booking_source']);
			switch ($params['booking_source']) {
				case PROVAB_BUS_BOOKING_SOURCE :
					$currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
					$details = $this->bus_lib->get_bus_information($params['route_schedule_id'], $params['journey_date']);
					if ($details['status'] == SUCCESS_STATUS) {
						$response['stauts'] = SUCCESS_STATUS;
						$page_data['search_id'] = $params['search_id'];
						$page_data['details'] = @$details['data']['result'];
						$page_data['currency_obj'] = $currency_obj;
						$response['data'] = get_compressed_output($this->load->view('bus/travelyaari/travelyaari_bus_info', $page_data,true));
						$response['status'] = SUCCESS_STATUS;
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	/**
	 * Get Bus Booking List
	 */
	function get_bus_details($filter_boarding_points=false)
	{
		$response['data'] = 'No Details Found';
		$response['status'] = false;
		//check params
		$params = $this->input->post();
		if (empty($params['booking_source']) == false and empty($params['search_id']) == false and intval($params['search_id']) > 0) {
			load_bus_lib($params['booking_source']);
			switch ($params['booking_source']) {
				case PROVAB_BUS_BOOKING_SOURCE :
					$details = $this->bus_lib->get_bus_details($params['route_schedule_id'], $params['journey_date']);
					if ($details['status'] == SUCCESS_STATUS) {
						//Converting API currency data to preferred currency
						$currency_obj = new Currency(array(
								'module_type' => 'bus',
								'from' => get_application_currency_preference(), 
								'to' => get_application_currency_preference()
							));
						$details = $this->bus_lib->seatdetails_in_preferred_currency($details, $currency_obj);
						//Display Bus Details
						$currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
						$response['stauts'] = SUCCESS_STATUS;
						$page_data['search_id'] = $params['search_id'];
						$page_data['details'] = $details['data']['result'];
						$page_data['currency_obj'] = $currency_obj;
						if ($filter_boarding_points == false) {
							$response['data'] = get_compressed_output($this->load->view('bus/travelyaari/travelyaari_bus_details', $page_data,true));
						} else {
							$response['data'] = get_compressed_output($this->load->view('bus/travelyaari/travelyaari_boarding_details', $page_data,true));
						}
						$response['status'] = SUCCESS_STATUS;
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	/**
	 * Load hotels from different source
	 */
	function get_room_details()
	{
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$params = $this->input->post();

		if ($params['op'] == 'get_room_details' && intval($params['search_id']) > 0 && isset($params['booking_source']) == true) {
			$application_preferred_currency = get_application_currency_preference();
			$application_default_currency = get_application_currency_preference();
			load_hotel_lib($params['booking_source']);
			$this->hotel_lib->search_data($params['search_id']);
			$attr['search_id'] = intval($params['search_id']);
			switch($params['booking_source']) {
				case PROVAB_HOTEL_BOOKING_SOURCE :
					$raw_room_list = $this->hotel_lib->get_room_list(urldecode($params['ResultIndex']));
					$safe_search_data = $this->hotel_model->get_safe_search_data($params['search_id']);
					//debug($raw_room_list);exit;
					if ($raw_room_list['status']) {
						//Converting API currency data to preferred currency
						$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
						$raw_room_list = $this->hotel_lib->roomlist_in_preferred_currency($raw_room_list, $currency_obj);
						//Display
						$currency_obj = new Currency(array('module_type' => 'hotel','from' => $application_default_currency, 'to' => $application_preferred_currency));
						//debug($raw_room_list);exit;
						$response['data'] = get_compressed_output($this->load->view('hotel/tbo/tbo_room_list',
						array('currency_obj' => $currency_obj,
								'params' => $params, 'raw_room_list' => $raw_room_list['data'],
								'hotel_search_params'=>$safe_search_data['data'],
								'application_preferred_currency' => $application_preferred_currency,
								'application_default_currency' => $application_default_currency,
								'attr' => $attr
						),true
						) 
						);
						$response['status'] = SUCCESS_STATUS;
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}
	

	/**
	 * Load Flight from different source
	 */
	function flight_list($search_id='')
	{
		
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();
		$page_params['search_id'] = $search_params['search_id'];
		if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
			load_flight_lib($search_params['booking_source']);

             $currency_obj = new Currency(array('module_type' => 'flight','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
			$raw_flight_list = $this->flight_lib->get_flight_list(abs($search_params['search_id']),$currency_obj);

		if ($raw_flight_list['status']) {
					$page_params = array(
						'search_id' => $search_params['search_id'],
						'booking_source' => $search_params['booking_source'],
						'trip_type' => $this->flight_lib->master_search_data['trip_type'],
					);
			switch($search_params['booking_source']) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :

						$raw_search_result = $raw_flight_list['data']['Search']['FlightDataList'];
						
						$raw_search_result = $this->flight_lib->search_data_in_preferred_currency($raw_search_result, $currency_obj); 
						 
						//print_r($raw_search_result);exit;
						$currency_obj = new Currency(array('module_type' => 'flight','from' => get_application_currency_preference(), 'to' => get_application_currency_preference())); 
						$formatted_search_data = $this->flight_lib->format_search_response($raw_search_result, $currency_obj, $search_params['search_id'], $this->current_module);
						/*debug($formatted_search_data);
						exit;*/
						$page_params['flight_section'] = "Travelomatix_API";
						$page_params['booking_url'] = $formatted_search_data['booking_url'];
						$raw_flight_list['data'] = $formatted_search_data['data'];
						$route_count = count($raw_flight_list['data']['Flights']);
						$domestic_round_way_flight = $raw_flight_list['data']['JourneySummary']['IsDomesticRoundway'];
						//debug($formatted_search_data);die();
						break;

						case TRAVELPORT_FLIGHT_BOOKING_SOURCE :

						//$raw_flight_list = $this->flight_lib->search_data_in_preferred_currency_TP($raw_flight_list['data'], $currency_obj);
						//debug($raw_flight_list['data']['Flights']);die("TRAVELPORT");
						$route_count = count($raw_flight_list['data']['Flights']);
						$domestic_round_way_flight = $raw_flight_list['data']['JourneySummary']['IsDomesticRoundway'];

						$page_params['travelport_s'] = "Travelport";
						$page_params['flight_section'] = "Travelport_API";
						$page_params['booking_url'] = @$raw_flight_list['booking_url'];
							
						//echo "<pre> TP search formatted_search_data";debug($raw_flight_list['data']);die;

						break; 
						case SABRE_FLIGHT_BOOKING_SOURCE :
						//debug($raw_flight_list);exit();
						//$raw_flight_list = $this->flight_lib->search_data_in_preferred_currency_TP($raw_flight_list['data'], $currency_obj);
						//debug($raw_flight_list);exit();
						$route_count = count($raw_flight_list['data']['Flights']);
						$domestic_round_way_flight = $raw_flight_list['data']['JourneySummary']['IsDomesticRoundway'];
						$page_params['travelport_s'] = "Sabre";
						$page_params['flight_section'] = "S*";
						$page_params['booking_url'] = @$raw_flight_list['booking_url'];
						break; 
						}
						/*if (($route_count > 0  && $domestic_round_way_flight == false) || ($route_count == 2 && $domestic_round_way_flight == true)) {
							$attr['search_id'] = abs($search_params['search_id']);
							$page_params = array(
							'raw_flight_list' => $raw_flight_list['data'],
							'search_id' => $search_params['search_id'],
							'booking_url' => $formatted_search_data['booking_url'],
							'booking_source' => $search_params['booking_source'],
							'trip_type' => $this->flight_lib->master_search_data['trip_type'],
							'attr' => $attr,
							'route_count' => $route_count,
							'IsDomestic' => $raw_flight_list['data']['JourneySummary']['IsDomestic']
							); 
							$page_params['domestic_round_way_flight'] = $domestic_round_way_flight;*/
							$attr['search_id'] = abs($search_params['search_id']);
				
							$page_params['route_count'] =	 $route_count;
							$page_params['IsDomestic'] = $raw_flight_list['data']['JourneySummary']['IsDomestic']; 
							$page_params['domestic_round_way_flight'] = @$domestic_round_way_flight;
							
							$page_params['twi_col_bs_val'] = $search_params['booking_source'];
							$twi_col_val_new = 1; // added for sub div section class
							$twi_col_val = 1; 

							foreach ($raw_flight_list['data']['Flights'] as $__key => $__jh_flight_result)
							{
								$page_params['raw_flight_list']['JourneySummary'] = $raw_flight_list['data']['JourneySummary'];
								$page_params['raw_flight_list']['Flights'][0] = force_multple_data_format($__jh_flight_result);
								$page_params['__root_indicator'] = $twi_col_val;
								$page_params['rotine'] = $twi_col_val_new++; 
								//$page_view_data = $this->load->view('flight/tbo/tbo_col2x_search_result', $page_params, true);
								$data_view_loader ['t-w-i-' . $twi_col_val++] = get_compressed_output($this->load->view('flight/tbo/tbo_col2x_search_result', $page_params, true));
							}							
			
							//$response['data'] = get_compressed_output($page_view_data);
							$response ['data'] ['col_x'] = ($data_view_loader);
							$response['status'] = SUCCESS_STATUS;
					
			} 
		}
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->output_compressed_data($response); 
	} 

	/**
	 * Get Data For Fare Calendar
	 * @param string $booking_source
	 */
	function puls_minus_days_fare_list($booking_source)
	{
		$response['data'] = array();
		$response['status'] = FAILURE_STATUS;

		$params = $this->input->get();
		load_flight_lib($booking_source);
		$search_data = $this->flight_lib->search_data(intval($params['search_id']));
		if ($search_data['status'] == SUCCESS_STATUS) {
			$date_array = array();
			$departure_date = $search_data['data']['depature'];
			$departure_date = strtotime(subtract_days_from_date(3, $departure_date));
			if (time() >= $departure_date) {
				$date_array[] = date('Y-m-d', strtotime(add_days_to_date(1)));
			} else {
				$date_array[] = date('Y-m-d', $departure_date);
			}
			$date_array[] = date('Y-m', strtotime($departure_date[0].' +1 month')).'-1';
			//Get Current Month And Next Month
			$day_fare_list = array();
			foreach ($date_array as $k => $v) {
				$search_data['data']['depature'] = $v;
				@$search = $this->flight_lib->calendar_safe_search_data($search_data['data']);
				if (valid_array($search) == true) {
					switch($booking_source) {
						case PROVAB_FLIGHT_BOOKING_SOURCE :
							$raw_fare_list = $this->flight_lib->get_fare_list($search);
							if ($raw_fare_list['status']) {
								$fare_calendar_list = $this->flight_lib->format_cheap_fare_list($raw_fare_list['data']);
								if ($fare_calendar_list['status'] == SUCCESS_STATUS) {
									$response['data']['departure'] = $search['depature'];
									$calendar_events = $this->get_fare_calendar_events($fare_calendar_list['data'], $raw_fare_list['data']['TraceId']);
									$day_fare_list = array_merge($day_fare_list, $calendar_events);
									$response['status'] = SUCCESS_STATUS;
								} else {
									$response['msg'] = 'Not Available!!! Please Try Later!!!!';
								}
							}
							break;
					}
				}
			}
			$response['data']['day_fare_list'] = $day_fare_list;
		}
		$this->output_compressed_data($response);
	}

	/**
	 * get fare list for calendar search - FLIGHT
	 */
	function fare_list($booking_source)
	{
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();
		load_flight_lib($booking_source);
		$search_params = $this->flight_lib->calendar_safe_search_data($search_params);
		if (valid_array($search_params) == true) {
			switch($booking_source) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					$raw_fare_list = $this->flight_lib->get_fare_list($search_params);
					if ($raw_fare_list['status']) {
						$fare_calendar_list = $this->flight_lib->format_cheap_fare_list($raw_fare_list['data']);
						if ($fare_calendar_list['status'] == SUCCESS_STATUS) {
							$response['data']['departure'] = $search_params['depature'];
							$calendar_events = $this->get_fare_calendar_events($fare_calendar_list['data'], $raw_fare_list['data']['TraceId']);
							$response['data']['day_fare_list'] = $calendar_events;
							$response['status'] = SUCCESS_STATUS;
						} else {
							$response['msg'] = 'Not Available!!! Please Try Later!!!!';
						}
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	/**
	 * Calendar Event Object
	 * @param $title
	 * @param $start
	 * @param $tip
	 * @param $href
	 * @param $event_date
	 * @param $session_id
	 * @param $add_class
	 */
	private function get_calendar_event_obj($title='', $start = '', $tip = '',$add_class = '', $href = '', $event_date = '', $session_id = '', $data_id='')
	{
		$event_obj = array();
		if (empty($data_id) == false) {
			$event_obj['data_id'] = $data_id;
		} else {
			$event_obj['data_id'] = '';
		}

		if (empty($title) == false) {
			$event_obj['title'] = $title;
		} else {
			$event_obj['title'] = '';
		}
		//start
		if (empty($start) == false) {
			$event_obj['start'] = $start;
			$event_obj['start_label'] = date('M d', strtotime($start));
		} else {
			$event_obj['start'] = '';
		}
		//tip
		if (empty($tip) == false) {
			$event_obj['tip'] = $tip;
		} else {
			$event_obj['tip'] = '';
		}
		//href
		if (empty($href) == false) {
			$event_obj['href'] = $href;
		} else {
			$event_obj['href'] = '';
		}
		//event_date
		if (empty($event_date) == false) {
			$event_obj['event_date'] = $event_date;
		}
		//session_id
		if (empty($session_id) == false) {
			$event_obj['session_id'] = $session_id;
		}
		//add_class
		if (empty($add_class) == false) {
			$event_obj['add_class'] = $add_class;
		} else {
			$event_obj['add_class'] = '';
		}
		return $event_obj;
	}

	function day_fare_list($booking_source)
	{
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();
		load_flight_lib($booking_source);
		$safe_search_params = $this->flight_lib->calendar_day_fare_safe_search_data($search_params);
		if ($safe_search_params['status'] == SUCCESS_STATUS) {
			switch($booking_source) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					$raw_day_fare_list = $this->flight_lib->get_day_fare($search_params);
					if ($raw_day_fare_list['status']) {
						$fare_calendar_list = $this->flight_lib->format_day_fare_list($raw_day_fare_list['data']);
						if ($fare_calendar_list['status'] == SUCCESS_STATUS) {
							$calendar_events = $this->get_fare_calendar_events($fare_calendar_list['data'], '');
							$response['data']['day_fare_list'] = $calendar_events;
							$response['data']['departure'] = $search_params['depature'];
							$response['status'] = SUCCESS_STATUS;
						} else {
							$response['msg'] = 'Not Available!!! Please Try Later!!!!';
						}
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	private function get_fare_calendar_events($events, $session_id='')
	{
		$currency_obj = new Currency(array('module_type' => 'flight','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		$index = 0;
		$calendar_events = array();
		foreach ($events as $k => $day_fare) {
			if (valid_array($day_fare) == true) {
				$fare_object = array('BaseFare' => $day_fare['BaseFare']);
				$BaseFare = $this->flight_lib->update_markup_currency($fare_object, $currency_obj);
				$tax = $currency_obj->get_currency($day_fare['tax'], false);
				$day_fare['price'] = floor($BaseFare['BaseFare']+$tax['default_value']);
				$event_obj = $this->get_calendar_event_obj(
				$currency_obj->get_currency_symbol(get_application_currency_preference()).' '.$day_fare['price'],
				$k, $day_fare['airline_name'].'-'.$day_fare['airline_code'], 'search-day-fare', '', $day_fare['departure'], '',
				$day_fare['airline_code']);
				$calendar_events[$index] = $event_obj;
			} else {
				$event_obj = $this->get_calendar_event_obj('Update', $k, 'Current Cheapest Fare Not Available. Click To Get Latest Fare.' ,
				'update-day-fare', '', $k, $session_id, '');
				$calendar_events[$index] = $event_obj;
			}
			$index++;
		}
		return $calendar_events;
	}




	/**
	 * Get Fare Details
	 */

	function get_fare_details_new()
	{
		//debug($_REQUEST); DIE;
		$response['status'] = false;
		$response['data'] = '';
		$response['msg'] = '<i class="fa fa-warning text-danger"></i> Fare Details Not Available';
		$params = $this->input->post();

		load_flight_lib($params['booking_source']);
		$data_access_key = $params['data_access_key'];
		$data_access['data_access_key'] =$params['data_access_key'];

		$params['data_access_key'] = unserialized_data($params['data_access_key']);

		if(empty($params['data_access_key']) == false || empty($params['data_access_key']) ==true)
		{
			switch($params['booking_source']) 
			{
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					$params['data_access_key'] = $this->flight_lib->read_token($data_access_key);
					$data_cat['data_cat'] = 'TBO';
				break;

				case TRAVELPORT_FLIGHT_BOOKING_SOURCE :
					$booking_source='PTBSID0000000016';
					$data_cat['data_cat'] = 'TP';
				break;

				case SABRE_FLIGHT_BOOKING_SOURCE :
					$booking_source='PTBSID0000000017';
					$data_cat['data_cat'] = 'SABRE';
				break;
			}

			//echo "ddd ";debug($params); DIE;
			$data = $this->flight_lib->get_fare_details($params);
			//debug($data_cat['data_cat']);exit();
			if ($data['status'] == SUCCESS_STATUS) 
			{
				if($params['booking_source'] == TRAVELPORT_FLIGHT_BOOKING_SOURCE){
					
					$response['status']	= SUCCESS_STATUS;
					
					$response['data']	= $this->load->view('flight/travelport/fare_details_travelport', array('fare_rules' => $data['data']), true);
					$response['msg']	= 'Fare Details Available';
				}
				else if($params['booking_source'] != SABRE_FLIGHT_BOOKING_SOURCE){
					$response['status']	= SUCCESS_STATUS;
					$response['data']	= $this->load->view('flight/tbo/fare_details', array('fare_rules' => $data['data'],'fare_rules_category' => $data_cat['data_cat']));
					$response['msg']	= 'Fare Details Available';
				}else{
					$response['status']	= SUCCESS_STATUS;
					//debug($data);exit();
					$Rdata['AirFareRules'] 	= $data['resdata'];
					$response['data']	= $this->load->view('flight/tbo/air_rules_view
						',$Rdata);
					$response['msg']	= 'Fare Details Available';
				}
				//debug($response['data']);exit();
			}
		}
		$this->output_compressed_data($response);
	}
	function get_fare_details()
	{
		$response['status'] = false;
		$response['data'] = '';
		$response['msg'] = '<i class="fa fa-warning text-danger"></i> Fare Details Not Available';
		$params = $this->input->post();

		load_flight_lib($params['booking_source']);
		$data_access_key = $params['data_access_key'];
		$params['data_access_key'] = unserialized_data($params['data_access_key']);
		if (empty($params['data_access_key']) == false) {
			switch($params['booking_source']) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					$params['data_access_key'] = $this->flight_lib->read_token($data_access_key);
					$data = $this->flight_lib->get_fare_details($params['data_access_key'], $params['search_access_key']);
					debug($data);exit; 
					if ($data['status'] == SUCCESS_STATUS) {
						$response['status']	= SUCCESS_STATUS;
						$response['data']	= $this->load->view('flight/tbo/fare_details', array('fare_rules' => $data['data']), true);
						$response['msg']	= 'Fare Details Available';
					}
				break;
				case TRAVELPORT_FLIGHT_BOOKING_SOURCE :

				if(empty($params['Fareinfo']) == false || empty($params['air_fare_key']) ==false) {		
							$booking_source = TRAVELPORT_FLIGHT_BOOKING_SOURCE;	
							load_flight_lib($booking_source);
							//$params['data_access_key'] = $this->flight_lib->read_token($data_access_key);
							#echo "www ";debug($params);exit;
							$data = $this->flight_lib->get_fare_details($params);
							//debug($data);exit;
							if ($data['status'] == SUCCESS_STATUS) {
								$response['status']	= SUCCESS_STATUS;
								$response['data']	= $this->load->view('flight/travelport/fare_details_travelport', array('fare_rules' => $data['data']), true);
								$response['msg']	= 'Fare Details Available';
							}		
				}
				break;

				case SABRE_FLIGHT_BOOKING_SOURCE :

				$booking_source = SABRE_FLIGHT_BOOKING_SOURCE;	
				load_flight_lib($booking_source);
				$data = $this->flight_lib->get_fare_details($params);
				if ($data['status'] == SUCCESS_STATUS) {
					$Rdata['AirFareRules'] 	= $data['resdata'];
					$response['status']	= SUCCESS_STATUS;
					$response['data']	= $this->load->view('flight/tbo/air_rules_view',$Rdata,true);
					$response['msg']	= 'Fare Details Available';
				}
  				break;  
			}
		}
		$this->output_compressed_data($response);
	}

	function get_combined_booking_from()
	{
		$response['status']	= FAILURE_STATUS;
		$response['data']	= array();
		$params = $this->input->post();
		if (empty($params['search_id']) == false && empty($params['trip_way_1']) == false && empty($params['trip_way_2']) == false) {
			$tmp_trip_way_1	= json_decode($params['trip_way_1'], true);
			$tmp_trip_way_2	= json_decode($params['trip_way_2'], true);
			// echo '<pre>';print_r($tmp_trip_way_1);exit;
			$search_id	= $params['search_id'];
			foreach($tmp_trip_way_1 as $___v) {
				$trip_way_1[$___v['name']] = $___v['value'];
			}
			foreach($tmp_trip_way_2 as $___v) {
				$trip_way_2[$___v['name']] = $___v['value'];
			}
			$booking_source = $trip_way_1['booking_source'];
			// echo $booking_source;exit;
			switch($booking_source) {
				case PROVAB_FLIGHT_BOOKING_SOURCE : load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				$response['data']['booking_url']	= $this->flight_lib->booking_url(intval($params['search_id']));
				$response['data']['form_content']	= $this->flight_lib->get_form_content($trip_way_1, $trip_way_2);
				$response['status']					= SUCCESS_STATUS;
				break;
			}
		}
		$this->output_compressed_data($response);
	}
	/**
	 * Jaganath
	 * Get Traveller Details in Booking Page
	 */
	function user_traveller_details()
	{
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim($term);
		$result = array();
		$this->load->model('user_model');
		$traveller_details = $this->user_model->user_traveller_details($term)->result();
		$travllers_data = array();
		foreach($traveller_details as $traveller){
			$travllers_data['category'] = 'Travellers';
			$travllers_data['id'] = $traveller->origin;
			$travllers_data['label'] = trim($traveller->first_name.' '.$traveller->last_name);
			$travllers_data['value'] = trim($traveller->first_name);
			$travllers_data['first_name'] = trim($traveller->first_name);
			$travllers_data['last_name'] = trim($traveller->last_name);
			$travllers_data['date_of_birth'] = date('Y-m-d', strtotime(trim($traveller->date_of_birth)));
			$travllers_data['email'] = trim($traveller->email);
			$travllers_data['passport_user_name'] = trim($traveller->passport_user_name);
			$travllers_data['passport_nationality'] = trim($traveller->passport_nationality);
			$travllers_data['passport_expiry_day'] = trim($traveller->passport_expiry_day);
			$travllers_data['passport_expiry_month'] = trim($traveller->passport_expiry_month);
			$travllers_data['passport_expiry_year'] = trim($traveller->passport_expiry_year);
			$travllers_data['passport_number'] = trim($traveller->passport_number);
			$travllers_data['passport_issuing_country'] = trim($traveller->passport_issuing_country);
			array_push($result,$travllers_data);
		}
		$this->output_compressed_data($result);
	}
	/**
	 *
	 */
	function log_event_ip_info($eid)
	{
		$params = $this->input->post();
		if (empty($eid) == false) {
			$this->custom_db->update_record('exception_logger', array('client_info' => serialize($params)), array('exception_id' => $eid));
		}
	}
	// crs functions
	function get_crs_airport_code_list()
	{
		$this->load->model('flight_model');
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$result = array();

		$__airports = $this->flight_model->get_crs_airport_list($term)->result();
		if (valid_array($__airports) == false) {
			$__airports = $this->flight_model->get_crs_airport_list('')->result();
		}
		$airports = array();
		foreach($__airports as $airport){
			$airports['label'] = $airport->airport_city.', '.$airport->country.' ('.$airport->airport_code.')';
			$airports['value'] = $airport->airport_city.' ('.$airport->airport_code.')';
			$airports['id'] = $airport->origin;
			if (empty($airport->top_destination) == false) {
				$airports['category'] = 'Top cities';
				$airports['type'] = 'Top cities';
			} else {
				$airports['category'] = 'Search Results';
				$airports['type'] = 'Search Results';
			}

			array_push($result,$airports);
		}
		$this->output_compressed_data($result);
	}


	function flight_crs_list($search_id='')
	{
		//echo $search_id; die;
		error_reporting(0);
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get(); 
		$page_params['search_id'] = $search_params['search_id'];
		if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
		
		    $this->load->model ( 'flight_model' );
		 
		    $clean_search_details = $this->flight_model->get_safe_search_data($search_params['search_id']);
		    $from_loc=$clean_search_details['data']['from'];
		    $to_loc=$clean_search_details['data']['to'];
		    $depature=$clean_search_details['data']['depature'];
		    $depature=date("d-m-Y",strtotime($depature));
		    $trip_type=$clean_search_details['data']['trip_type'];
		    if($trip_type=='oneway')
			{
				$raw_flight_list=$this->flight_model->get_crs_oneflight($from_loc, $to_loc,$depature);	
				//$page_params['flights'] =$raw_flight_list;
				$page_params['flights'] =$raw_flight_list;
				$page_params['search_params']=$clean_search_details;
				$page_params['search_id']=$search_params['search_id'];
				$page_params['booking_source']=CRS_FLIGHT_BOOKING_SOURCE;

			}
			if($trip_type=='circle')
			{
				
				$return_to=$clean_search_details['data']['return'];
				$return_to=date("d-m-Y",strtotime($return_to));
				$raw_flight_list=$this->flight_model->get_crs_circleflight($from_loc, $to_loc,$depature,$return_to);
				$page_params['search_params']=$clean_search_details;
				$page_params['search_id']=$search_params['search_id'];
				$page_params['booking_source']=CRS_FLIGHT_BOOKING_SOURCE;
				$page_params['flights'] = $raw_flight_list;
			
			}
		    
			$page_view_data = $this->load->view('flight/tbo/filght_crs_list', $page_params, true);
			//$page_view_data = $this->template->isolated_view('flight/tbo/filght_crs_list', $page_params);
			$response['data'] = get_compressed_output($page_view_data);
			$response['status'] = SUCCESS_STATUS;
			
		}
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$this->output_compressed_data($response);
	}
	function set_flight_search_session_expiry($from_cache = false, $search_hash){

		$response = array();

		if($from_cache == false){

			$GLOBALS['CI']->session->set_userdata(array($search_hash => date("Y-m-d H:i:s")));
			$response['session_start_time'] = $GLOBALS ['CI']->config->item ('flight_search_session_expiry_period');

		}else{

			$start_time = $GLOBALS['CI']->session->userdata($search_hash);
			$current_time = date("Y-m-d H:i:s");
			$diff = strtotime($current_time) - strtotime($start_time);
			$response['session_start_time'] = $GLOBALS ['CI']->config->item ('flight_search_session_expiry_period') - $diff;
		}

		$response['search_hash'] = $search_hash;

		return $response;

	}

	/*
	*
	CODE ADDED ON 27/12/2017 
	*
	*/
	/**
	* Get Hotel Images by HotelCode
	*/
	function get_hotel_images(){
		$post_params = $this->input->post();
		if($post_params['hotel_code']){
			//debug($post_params['hotel_code']);exit;
			switch ($post_params['booking_source']) {

				case PROVAB_HOTEL_BOOKING_SOURCE:
					load_hotel_lib($post_params['booking_source']);
					$raw_hotel_images = $this->hotel_lib->get_hotel_images($post_params['hotel_code']);	
					
					if($raw_hotel_images['status']==true){
							$response['data'] = get_compressed_output(
							$this->load->view('hotel/tbo/tbo_hotel_images',
							array('hotel_images'=>$raw_hotel_images,'HotelCode'=>$post_params['hotel_code']
							),true));
					}
					
					break;
			}
			 $this->output_compressed_data($response);
		
		}
		exit;
	}

		/**
	*Load hotels for map
	*/
	function get_all_hotel_list(){
		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		$search_params = $this->input->get();		
		$limit = $this->config->item('hotel_per_page_limit');
		if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
			load_hotel_lib($search_params['booking_source']);
			switch($search_params['booking_source']) {
				case PROVAB_HOTEL_BOOKING_SOURCE :
					//getting search params from table
					$safe_search_data = $this->hotel_model->get_safe_search_data($search_params['search_id']);
					//Meaning hotels are loaded first time
					$raw_hotel_list = $this->hotel_lib->get_hotel_list(abs($search_params['search_id']));
					//debug($raw_hotel_list);exit;
					if ($raw_hotel_list['status']) {
						//Converting API currency data to preferred currency
						$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
						$raw_hotel_list = $this->hotel_lib->search_data_in_preferred_currency($raw_hotel_list, $currency_obj);
						//Display 
						$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
						//Update currency and filter summary appended
						if (isset($search_params['filters']) == true and valid_array($search_params['filters']) == true) {
							$filters = $search_params['filters'];
						} else {
							$filters = array();
						}															
						$attr['search_id'] = abs($search_params['search_id']);			
						$raw_hotel_search_result = array();						
						$i=0;
						$counter=0;
				    	if ($max_lat == 0) {
							$max_lat = $min_lat = 0;
						}

						if ($max_lon == 0) {
							$max_lon = $min_lon = 0;
						}  
						if($raw_hotel_list['data']['HotelSearchResult']){
							foreach ($raw_hotel_list['data']['HotelSearchResult']['HotelResults'] as $key => $value) {
								$raw_hotel_search_result[$i] =$value;
								$raw_hotel_search_result[$i]['MResultToken']=urlencode($value['ResultToken']);
								 $lat = $value['Latitude'];
								 $lon = $value['Longitude'];
								if(($lat!='')&& ($counter<1)){
									$max_lat = $min_lat = $lat;
								}
								if(($lon!='')){
									$counter++;					
									$max_lon = $min_lon = $lon;
								}

								$i++;
							}
							$raw_hotel_list['data']['HotelSearchResult']['max_lat']  = $max_lat;
							$raw_hotel_list['data']['HotelSearchResult']['max_lon']  = $max_lon;
						}
						$raw_hotel_list['data']['HotelSearchResult']['HotelResults'] =$raw_hotel_search_result; 
						//debug($raw_hotel_list);exit;
						$response['data'] =$raw_hotel_list['data'];					
						$response['status'] = SUCCESS_STATUS;
						
					}
					break;
			}
		}
		$this->output_compressed_data($response);
	}

	/*
	** CODE ADDED ON 4/1/2018
	*/
		/**
	*Get Cancellation Policy based on Cancellation policy code
	*
	*/
	function get_cancellation_policy(){
		$get_params =$this->input->get();
		
		$application_preferred_currency = get_application_currency_preference();
		$application_default_currency = get_application_currency_preference();
		$currency_obj = new Currency(array('module_type' => 'hotel','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		$room_price = $get_params['room_price'];
		//debug($get_params);exit;
		if(isset($get_params['booking_source'])&&!empty($get_params['booking_source'])){
			load_hotel_lib($get_params['booking_source']);
			
			if(isset($get_params['policy_code'])&&!empty($get_params['policy_code'])){
				$safe_search_data = $this->hotel_model->get_safe_search_data($get_params['tb_search_id']);				
				$get_params['no_of_nights'] = $safe_search_data['data']['no_of_nights'];
				$get_params['room_count'] = $safe_search_data['data']['room_count'];
				$get_params['check_in'] = $safe_search_data['data']['from_date'];
 				$cancellation_details = $this->hotel_lib->get_cancellation_details($get_params);
 				
 				
 				$cancellatio_details =$cancellation_details['GetCancellationPolicy']['policy'][0]['policy'];
 				$policy_string ='';
 				$cancel_string='';
 				$cancel_count = count($cancellatio_details);
 				$cancel_reverse = $cancellatio_details;  	
 				//debug($cancellatio_details);exit;
 					foreach ($cancellatio_details as $key => $value) {
	 					$amount = 0;
	 					$policy_string ='';	 					
		 					if($value['Charge']==0){
		 						$policy_string .='No cancellation charges, if cancelled before '.date('d M Y',strtotime($value['ToDate']));
		 					}else{
		 						if($value['Charge']!=0){
		 							 if(isset($cancel_reverse[$key+1])){
		 							 		if($value['ChargeType']==1){ 					
						 						$amount =  $currency_obj->get_currency_symbol($currency_obj->to_currency)." ".get_converted_currency_value($currency_obj->force_currency_conversion(round($value['Charge'])));							 						
						 					}elseif($value['ChargeType']==2){
						 						$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)." ".$room_price;
						 					}
						 					$current_date = date('Y-m-d');
											$cancell_date = date('Y-m-d',strtotime($value['FromDate']));
											if($cancell_date >$current_date){
												//$value['FromDate'] = date('Y-m-d');
												$policy_string .='Cancellations made after '.date('d M Y',strtotime($value['FromDate'])).' to '.date('d M Y',strtotime($value['ToDate'])).', would be charged '.$amount;
											}
						 					//$policy_string .='Cancellations made after '.date('d M Y',strtotime($value['FromDate'])).' to '.date('d M Y',strtotime($value['ToDate'])).', would be charged '.$amount;
						                 
						             }else{
						             	if($value['ChargeType']==1){
						             		$amount =  $currency_obj->get_currency_symbol($currency_obj->to_currency)." ".get_converted_currency_value($currency_obj->force_currency_conversion(round($value['Charge'])));	
						          		}elseif ($value['ChargeType']==2) {
						             		$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)." ".$room_price;
						             	}
						             	$current_date = date('Y-m-d');
										$cancell_date = date('Y-m-d',strtotime($value['FromDate']));
										if($cancell_date > $current_date){
											$value['FromDate'] =$value['FromDate']; 
										}else{
											$value['FromDate'] = date('Y-m-d');
										}
						             	$policy_string .='Cancellations made after '.date('d M Y',strtotime($value['FromDate'])).', or no-show, would be charged '.$amount;
						             }
				 				
			 					}
		 					}		 					
		 				$cancel_string .= $policy_string.'<br/> ';
	 				}
 				
 				echo $cancel_string;
				//echo $cancellation_details['GetCancellationPolicy']['policy'][0];
			}else{
				$cancel_string ='';
					$cancellation_policy_details = json_decode(base64_decode($get_params['policy_details']));
					//debug($cancellation_policy_details);
					$cancel_count = count($cancellation_policy_details);					
					$cancellation_policy_details = json_decode(json_encode($cancellation_policy_details), True);
					$cancel_reverse = array_reverse($cancellation_policy_details);				
					//debug($cancel_reverse);
					if($cancellation_policy_details){
							foreach (array_reverse($cancellation_policy_details) as $key=>$value) {							
									$policy_string ='';								
										if($value['Charge']==0){
											$policy_string .='No cancellation charges, if cancelled before '.date('d M Y',strtotime($value['ToDate']));
										}else{
											if(isset($cancel_reverse[$key+1])){
												if($value['ChargeType']==1){
													$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)."  ".get_converted_currency_value($currency_obj->force_currency_conversion($value['Charge']));
													
												}elseif ($value['ChargeType']==2) {
													$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)."  ".$room_price;
												}
												$current_date = date('Y-m-d');
												$cancell_date = date('Y-m-d',strtotime($value['FromDate']));
												if($cancell_date >$current_date){
													$policy_string .='Cancellations made after '.date('d M Y',strtotime($value['FromDate'])).' to '.date('d M Y',strtotime($value['ToDate'])).', would be charged '.$amount;
												}
												
											}else{
												if($value['ChargeType']==1){
													$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)."  ".get_converted_currency_value($currency_obj->force_currency_conversion($value['Charge']));
													
												}elseif ($value['ChargeType']==2) {
													$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency)."  ".$room_price;
												}
												$current_date = date('Y-m-d');
												$cancell_date = date('Y-m-d',strtotime($value['FromDate']));
												if($cancell_date >$current_date){
													$value['FromDate'] = $value['FromDate'];
												}else{
													$value['FromDate'] = date('Y-m-d');
												}
												$policy_string .='Cancellations made after '.date('d M Y',strtotime($value['FromDate'])).', or no-show, would be charged '.$amount;
											}
										}									
																	
									$cancel_string .= $policy_string.'<br/>';
									
							}
					}else{
						$cancel_string = 'This rate is non-refundable. If you cancel this booking you will not be refunded any of the payment.';
					}
					
				
				echo $cancel_string;
			}
				
			
		}else{
			echo "This rate is non-refundable. If you cancel this booking you will not be refunded any of the payment.";
		}
		exit;
	}
}
