//Hotel Maps
var gmarkers;
var loc, map, marker, infoWindow, data;
var bounds = new google.maps.LatLngBounds();
function loadMapMarkers(markers,geometry){ 
  gmarkers = new Array();

  var styles = [
      {
        featureType: 'road.highway',
        elementType: 'all',
        stylers: [
          { hue: '#e5e5e5' },
          { saturation: -100 },
          { lightness: 72 },
          { visibility: 'simplified' }
        ]
      },{
        featureType: 'water',
        elementType: 'all',
        stylers: [
          { hue: '#30a5dc' },
          { saturation: 47 },
          { lightness: -31 },
          { visibility: 'simplified' }
        ]
      },{
        featureType: 'road',
        elementType: 'all',
        stylers: [
          { hue: '#cccccc' },
          { saturation: -100 },
          { lightness: 44 },
          { visibility: 'on' }
        ]
      },{
        featureType: 'landscape',
        elementType: 'all',
        stylers: [
          { hue: '#ffffff' },
          { saturation: -100 },
          { lightness: 100 },
          { visibility: 'on' }
        ]
      },{
        featureType: 'poi.park',
        elementType: 'all',
        stylers: [
          { hue: '#d2df9f' },
          { saturation: 12 },
          { lightness: -4 },
          { visibility: 'on' }
        ]
      },{
        featureType: 'road.arterial',
        elementType: 'all',
        stylers: [
          { hue: '#e5e5e5' },
          { saturation: -100 },
          { lightness: 56 },
          { visibility: 'on' }
        ]
      },{
        featureType: 'administrative.locality',
        elementType: 'all',
        stylers: [
          { hue: '#000000' },
          { saturation: 0 },
          { lightness: 0 },
          { visibility: 'on' }
        ]
      }
    ];

  // Create a new StyledMapType object, passing it the array of styles,
  // as well as the name to be displayed on the map type control.
  var styledMap = new google.maps.StyledMapType(styles,{name: "TheChinaGap Map"});
  if(markers != '' && markers != '{}'){
    var mapOptions = {
      zoom: 16,
      center: new google.maps.LatLng(markers[markers.length-1].lat, markers[markers.length-1].lon),
      mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE, 'map_style']
      }
    };
  }else{
    var mapOptions = {
      zoom: 14,
      center: new google.maps.LatLng(geometry.latitude, geometry.longitude),
      mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
      }
    };
  }  
  var icon1 = ASSETS+"images/marker_out.png";
  var icon2 = ASSETS+"images/marker_over.png";
  
  //~ var icon1 = ASSETS+"images/mapred.png";
  //~ var icon2 = ASSETS+"images/maporg.png";

  infoWindow = new google.maps.InfoWindow();
  map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
  map.setTilt(90);
  google.maps.event.addDomListener(window, "resize", function() {
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
  });
  
  var weatherLayer = new google.maps.weather.WeatherLayer({
      temperatureUnits: google.maps.weather.TemperatureUnit.CELCIUS
  });
  weatherLayer.setMap(map);

  var cloudLayer = new google.maps.weather.CloudLayer();
  cloudLayer.setMap(map);
  
   var labelIndex,labelIndex_v1;
   var min = 1;
   var max = markers.length;
   var labels = [];
   for(var iter=1; iter <= markers.length; iter++) {
	   labels.push(iter);
   }
  //if(markers.length != 0 && markers.length > 0){
  if(markers != '' && markers.length > 0){
    if(!isEmpty(markers)){   
      var i = 0;
      var interval = setInterval(function () {
        data = markers[i];
        //console.log(labels[i].toString());
        var myLatlng = new google.maps.LatLng(data.lat, data.lon);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: icon1,
            title: data.title,
            animation: google.maps.Animation.DROP,
			//~ label: {
				//~ text: labels[i].toString(),
				//~ color: 'purple'
			//~ }
        });
        
        bounds.extend(marker.position);
        (function (marker, data) {
          //var info = data.description;
          var Aptimage;
          
          if(data.Image == null || data.Image == ''){
            Aptimage = '';        
          }else{
            Aptimage = data.Image;
          }
          

          if(data.inWishlist == 1) {
            var wishlistStyle = "style='color: red' ";
          } else {
            var wishlistStyle = "";
          }
         var ses_currency = $('#curr').val();
         var ses_currency_val = $('#curr_value').val();

			var info = '<div class="col-xs-12 nopad">'+
                      '<div class="sidenamedesc mapinside">'+
                        '<div class="celhtl width32 midlbord">'+
                          '<div class="hotel_image">'+ '<img src="'+data.hotel_api_images+'" alt="" />'+  '</div>'+
                        '</div>'+
                        '<div class="celhtl width50">'+
                          '<div class="waymensn">'+
                            '<div class="flitruo_hotel">'+
                              '<div class="hoteldist">'+ 
							  	'<a target="_blank" href="'+data.details+'" class="hotel_name">'+data.hotel_name+'</a>'+
                                '<div class="clearfix"></div>'+
                                '<div class="stra_hotel" data-star="'+data.star_rating+'"> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> </div>'+
                                '<div class="pricealso">Starting from <strong>'+ses_currency+' '+(data.amount * ses_currency_val).toFixed(2)+'</strong></div>'+
                                '<div class="sideprice_hotel"> '+ses_currency+' '+(data.amount * ses_currency_val).toFixed(2) +' </div>'+
                            '<div class="bookbtn_htl"><a class="booknow" target="_blank" href="'+data.details+'" >Select</a> </div>'+
                               
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        
                      '</div>'+
                    '</div>';
       
          google.maps.event.addListener(marker, "click", function (e) {
            //infoWindow.setOptions(myOptions);
            infoWindow.setContent(info);
            infoWindow.open(map, marker);
          });


            google.maps.event.addListener(marker, 'mouseover', function (e) {
                marker.setIcon(icon2);
            });
            google.maps.event.addListener(marker, 'mouseout', function (e) {
                marker.setIcon(icon1);
            });
        })(marker, data);
        
        i++;
        if (i == markers.length) {
          //setCenterPoint(gmarkers);
          clearInterval(interval);
        }
      },1);
    }
  }
  //Associate the styled map with the MapTypeId and set it to display.
  //map.mapTypes.set('map_style', styledMap);
  //map.setMapTypeId('map_style');  
 // map.setMapTypeId(google.maps.MapTypeId.SATELLITE); 

  //setCenterPoint(bounds);
  
  var listener = google.maps.event.addListener(map, "idle", function () {
    map.setZoom(11);
    google.maps.event.removeListener(listener);
  }); 
  var center = map.getCenter();
  google.maps.event.trigger(map, "resize");
  map.setCenter(center);      
}

function setCenterPoint(bounds){
  //console.log(bounds.getCenter());
  google.maps.event.trigger(map, 'resize');
  map.setCenter(bounds.getCenter());
  map.fitBounds(bounds); //  Fit these bounds to the map
}

function changeMarker(type,that) {
    var id = $(that).data('id');
    marker = gmarkers[id];
    var icon1 = ASSETS+"images/marker_out.png";
    var icon2 = ASSETS+"images/marker_over.png";
    var varLat = $(that).data('lat');
    var varLong = $(that).data('lng');
    var latLng = new google.maps.LatLng(varLat, varLong); //Makes a latlng
    map.panTo(latLng);//Make map global
    if(type == 'over'){
      marker.setIcon(icon2);
      //infoWindow.setContent(info);
      //infoWindow.open(map,marker);
    }else if(type == 'out'){
      //infoWindow.close();
      marker.setIcon(icon1);
    }
    //marker.setIcon(icon);
}

// Speed up calls to hasOwnProperty
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}
