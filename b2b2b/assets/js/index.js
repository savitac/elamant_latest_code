$(function($) {
var check_in = db_date(7);
var check_out = db_date(10); 
    $('.htd-wrap').on('click', function(e) { 
        e.preventDefault();
        var curr_destination = $('.top-des-val', this).val();    
        $('#hotel_destination_search_name').val(curr_destination);
        $('#hotel_checkin').val(check_in);
        $('#hotel_checkout').val(check_out);
        $('#hotel_search').submit()
    });
    $.supersized({
        slide_interval: 5000,
        transition: 1,
        transition_speed: 700,
        slide_links: 'blank',
		slides: tmpl_imgs
    })
});     