/* ================================================
----------- Travelite ---------- */
(function ($) {
	"use strict";
	
	//slider start
	 /******************************************
    -	PREPARE PLACEHOLDER FOR SLIDER	-
    ******************************************/

 /*   var tpj = jQuery;
    var revapi116;
    tpj(document).ready(function() {
        if (tpj("#rev_slider_116_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_116_1");
        } else {
            revapi116 = tpj("#rev_slider_116_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "../../revolution/js/",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 9000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 600,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        }
                    }
                },
                viewPort: {
                    enable: true,
                    outof: "pause",
                    visible_area: "80%"
                },
                gridwidth: 1240,
                gridheight: 645,
                lazyType: "none",
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }
	});
	

	

	//slider End*/


	// Ion.RangeSlider
// version 2.1.7 Build: 371
// Â© Denis Ineshin, 2017
// https://github.com/IonDen
//
// Project page:    http://ionden.com/a/plugins/ion.rangeSlider/en.html
// GitHub page:     https://github.com/IonDen/ion.rangeSlider
//
// Released under MIT licence:
// http://ionden.com/a/plugins/licence-en.html
// =====================================================================================================================

;(function(factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], function (jQuery) {
            return factory(jQuery, document, window, navigator);
        });
    } else if (typeof exports === "object") {
        factory(require("jquery"), document, window, navigator);
    } else {
        factory(jQuery, document, window, navigator);
    }
} (function ($, document, window, navigator, undefined) {
    "use strict";

    // =================================================================================================================
    // Service

    var plugin_count = 0;

    // IE8 fix
    var is_old_ie = (function () {
        var n = navigator.userAgent,
            r = /msie\s\d+/i,
            v;
        if (n.search(r) > 0) {
            v = r.exec(n).toString();
            v = v.split(" ")[1];
            if (v < 9) {
                $("html").addClass("lt-ie9");
                return true;
            }
        }
        return false;
    } ());
    if (!Function.prototype.bind) {
        Function.prototype.bind = function bind(that) {

            var target = this;
            var slice = [].slice;

            if (typeof target != "function") {
                throw new TypeError();
            }

            var args = slice.call(arguments, 1),
                bound = function () {

                    if (this instanceof bound) {

                        var F = function(){};
                        F.prototype = target.prototype;
                        var self = new F();

                        var result = target.apply(
                            self,
                            args.concat(slice.call(arguments))
                        );
                        if (Object(result) === result) {
                            return result;
                        }
                        return self;

                    } else {

                        return target.apply(
                            that,
                            args.concat(slice.call(arguments))
                        );

                    }

                };

            return bound;
        };
    }
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(searchElement, fromIndex) {
            var k;
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }
            var O = Object(this);
            var len = O.length >>> 0;
            if (len === 0) {
                return -1;
            }
            var n = +fromIndex || 0;
            if (Math.abs(n) === Infinity) {
                n = 0;
            }
            if (n >= len) {
                return -1;
            }
            k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
            while (k < len) {
                if (k in O && O[k] === searchElement) {
                    return k;
                }
                k++;
            }
            return -1;
        };
    }



    // =================================================================================================================
    // Template

    var base_html =
        '<span class="irs">' +
        '<span class="irs-line" tabindex="-1"><span class="irs-line-left"></span><span class="irs-line-mid"></span><span class="irs-line-right"></span></span>' +
        '<span class="irs-min">0</span><span class="irs-max">1</span>' +
        '<span class="irs-from">0</span><span class="irs-to">0</span><span class="irs-single">0</span>' +
        '</span>' +
        '<span class="irs-grid"></span>' +
        '<span class="irs-bar"></span>';

    var single_html =
        '<span class="irs-bar-edge"></span>' +
        '<span class="irs-shadow shadow-single"></span>' +
        '<span class="irs-slider single"></span>';

    var double_html =
        '<span class="irs-shadow shadow-from"></span>' +
        '<span class="irs-shadow shadow-to"></span>' +
        '<span class="irs-slider from"></span>' +
        '<span class="irs-slider to"></span>';

    var disable_html =
        '<span class="irs-disable-mask"></span>';



    // =================================================================================================================
    // Core

    /**
     * Main plugin constructor
     *
     * @param input {Object} link to base input element
     * @param options {Object} slider config
     * @param plugin_count {Number}
     * @constructor
     */
    var IonRangeSlider = function (input, options, plugin_count) {
        this.VERSION = "2.1.7";
        this.input = input;
        this.plugin_count = plugin_count;
        this.current_plugin = 0;
        this.calc_count = 0;
        this.update_tm = 0;
        this.old_from = 0;
        this.old_to = 0;
        this.old_min_interval = null;
        this.raf_id = null;
        this.dragging = false;
        this.force_redraw = false;
        this.no_diapason = false;
        this.is_key = false;
        this.is_update = false;
        this.is_start = true;
        this.is_finish = false;
        this.is_active = false;
        this.is_resize = false;
        this.is_click = false;

        options = options || {};

        // cache for links to all DOM elements
        this.$cache = {
            win: $(window),
            body: $(document.body),
            input: $(input),
            cont: null,
            rs: null,
            min: null,
            max: null,
            from: null,
            to: null,
            single: null,
            bar: null,
            line: null,
            s_single: null,
            s_from: null,
            s_to: null,
            shad_single: null,
            shad_from: null,
            shad_to: null,
            edge: null,
            grid: null,
            grid_labels: []
        };

        // storage for measure variables
        this.coords = {
            // left
            x_gap: 0,
            x_pointer: 0,

            // width
            w_rs: 0,
            w_rs_old: 0,
            w_handle: 0,

            // percents
            p_gap: 0,
            p_gap_left: 0,
            p_gap_right: 0,
            p_step: 0,
            p_pointer: 0,
            p_handle: 0,
            p_single_fake: 0,
            p_single_real: 0,
            p_from_fake: 0,
            p_from_real: 0,
            p_to_fake: 0,
            p_to_real: 0,
            p_bar_x: 0,
            p_bar_w: 0,

            // grid
            grid_gap: 0,
            big_num: 0,
            big: [],
            big_w: [],
            big_p: [],
            big_x: []
        };

        // storage for labels measure variables
        this.labels = {
            // width
            w_min: 0,
            w_max: 0,
            w_from: 0,
            w_to: 0,
            w_single: 0,

            // percents
            p_min: 0,
            p_max: 0,
            p_from_fake: 0,
            p_from_left: 0,
            p_to_fake: 0,
            p_to_left: 0,
            p_single_fake: 0,
            p_single_left: 0
        };



        /**
         * get and validate config
         */
        var $inp = this.$cache.input,
            val = $inp.prop("value"),
            config, config_from_data, prop;

        // default config
        config = {
            type: "single",

            min: 10,
            max: 100,
            from: null,
            to: null,
            step: 1,

            min_interval: 0,
            max_interval: 0,
            drag_interval: false,

            values: [],
            p_values: [],

            from_fixed: false,
            from_min: null,
            from_max: null,
            from_shadow: false,

            to_fixed: false,
            to_min: null,
            to_max: null,
            to_shadow: false,

            prettify_enabled: true,
            prettify_separator: " ",
            prettify: null,

            force_edges: false,

            keyboard: false,
            keyboard_step: 5,

            grid: false,
            grid_margin: true,
            grid_num: 4,
            grid_snap: false,

            hide_min_max: false,
            hide_from_to: false,

            prefix: "",
            postfix: "",
            max_postfix: "",
            decorate_both: true,
            values_separator: " â€” ",

            input_values_separator: ";",

            disable: false,

            onStart: null,
            onChange: null,
            onFinish: null,
            onUpdate: null
        };


        // check if base element is input
        if ($inp[0].nodeName !== "INPUT") {
            console && console.warn && console.warn("Base element should be <input>!", $inp[0]);
        }


        // config from data-attributes extends js config
        config_from_data = {
            type: $inp.data("type"),

            min: $inp.data("min"),
            max: $inp.data("max"),
            from: $inp.data("from"),
            to: $inp.data("to"),
            step: $inp.data("step"),

            min_interval: $inp.data("minInterval"),
            max_interval: $inp.data("maxInterval"),
            drag_interval: $inp.data("dragInterval"),

            values: $inp.data("values"),

            from_fixed: $inp.data("fromFixed"),
            from_min: $inp.data("fromMin"),
            from_max: $inp.data("fromMax"),
            from_shadow: $inp.data("fromShadow"),

            to_fixed: $inp.data("toFixed"),
            to_min: $inp.data("toMin"),
            to_max: $inp.data("toMax"),
            to_shadow: $inp.data("toShadow"),

            prettify_enabled: $inp.data("prettifyEnabled"),
            prettify_separator: $inp.data("prettifySeparator"),

            force_edges: $inp.data("forceEdges"),

            keyboard: $inp.data("keyboard"),
            keyboard_step: $inp.data("keyboardStep"),

            grid: $inp.data("grid"),
            grid_margin: $inp.data("gridMargin"),
            grid_num: $inp.data("gridNum"),
            grid_snap: $inp.data("gridSnap"),

            hide_min_max: $inp.data("hideMinMax"),
            hide_from_to: $inp.data("hideFromTo"),

            prefix: $inp.data("prefix"),
            postfix: $inp.data("postfix"),
            max_postfix: $inp.data("maxPostfix"),
            decorate_both: $inp.data("decorateBoth"),
            values_separator: $inp.data("valuesSeparator"),

            input_values_separator: $inp.data("inputValuesSeparator"),

            disable: $inp.data("disable")
        };
        config_from_data.values = config_from_data.values && config_from_data.values.split(",");

        for (prop in config_from_data) {
            if (config_from_data.hasOwnProperty(prop)) {
                if (config_from_data[prop] === undefined || config_from_data[prop] === "") {
                    delete config_from_data[prop];
                }
            }
        }


        // input value extends default config
        if (val !== undefined && val !== "") {
            val = val.split(config_from_data.input_values_separator || options.input_values_separator || ";");

            if (val[0] && val[0] == +val[0]) {
                val[0] = +val[0];
            }
            if (val[1] && val[1] == +val[1]) {
                val[1] = +val[1];
            }

            if (options && options.values && options.values.length) {
                config.from = val[0] && options.values.indexOf(val[0]);
                config.to = val[1] && options.values.indexOf(val[1]);
            } else {
                config.from = val[0] && +val[0];
                config.to = val[1] && +val[1];
            }
        }



        // js config extends default config
        $.extend(config, options);


        // data config extends config
        $.extend(config, config_from_data);
        this.options = config;



        // validate config, to be sure that all data types are correct
        this.update_check = {};
        this.validate();



        // default result object, returned to callbacks
        this.result = {
            input: this.$cache.input,
            slider: null,

            min: this.options.min,
            max: this.options.max,

            from: this.options.from,
            from_percent: 0,
            from_value: null,

            to: this.options.to,
            to_percent: 0,
            to_value: null
        };



        this.init();
    };

    IonRangeSlider.prototype = {

        /**
         * Starts or updates the plugin instance
         *
         * @param [is_update] {boolean}
         */
        init: function (is_update) {
            this.no_diapason = false;
            this.coords.p_step = this.convertToPercent(this.options.step, true);

            this.target = "base";

            this.toggleInput();
            this.append();
            this.setMinMax();

            if (is_update) {
                this.force_redraw = true;
                this.calc(true);

                // callbacks called
                this.callOnUpdate();
            } else {
                this.force_redraw = true;
                this.calc(true);

                // callbacks called
                this.callOnStart();
            }

            this.updateScene();
        },

        /**
         * Appends slider template to a DOM
         */
        append: function () {
            var container_html = '<span class="irs js-irs-' + this.plugin_count + '"></span>';
            this.$cache.input.before(container_html);
            this.$cache.input.prop("readonly", true);
            this.$cache.cont = this.$cache.input.prev();
            this.result.slider = this.$cache.cont;

            this.$cache.cont.html(base_html);
            this.$cache.rs = this.$cache.cont.find(".irs");
            this.$cache.min = this.$cache.cont.find(".irs-min");
            this.$cache.max = this.$cache.cont.find(".irs-max");
            this.$cache.from = this.$cache.cont.find(".irs-from");
            this.$cache.to = this.$cache.cont.find(".irs-to");
            this.$cache.single = this.$cache.cont.find(".irs-single");
            this.$cache.bar = this.$cache.cont.find(".irs-bar");
            this.$cache.line = this.$cache.cont.find(".irs-line");
            this.$cache.grid = this.$cache.cont.find(".irs-grid");

            if (this.options.type === "single") {
                this.$cache.cont.append(single_html);
                this.$cache.edge = this.$cache.cont.find(".irs-bar-edge");
                this.$cache.s_single = this.$cache.cont.find(".single");
                this.$cache.from[0].style.visibility = "hidden";
                this.$cache.to[0].style.visibility = "hidden";
                this.$cache.shad_single = this.$cache.cont.find(".shadow-single");
            } else {
                this.$cache.cont.append(double_html);
                this.$cache.s_from = this.$cache.cont.find(".from");
                this.$cache.s_to = this.$cache.cont.find(".to");
                this.$cache.shad_from = this.$cache.cont.find(".shadow-from");
                this.$cache.shad_to = this.$cache.cont.find(".shadow-to");

                this.setTopHandler();
            }

            if (this.options.hide_from_to) {
                this.$cache.from[0].style.display = "none";
                this.$cache.to[0].style.display = "none";
                this.$cache.single[0].style.display = "none";
            }

            this.appendGrid();

            if (this.options.disable) {
                this.appendDisableMask();
                this.$cache.input[0].disabled = true;
            } else {
                this.$cache.cont.removeClass("irs-disabled");
                this.$cache.input[0].disabled = false;
                this.bindEvents();
            }

            if (this.options.drag_interval) {
                this.$cache.bar[0].style.cursor = "ew-resize";
            }
        },

        /**
         * Determine which handler has a priority
         * works only for double slider type
         */
        setTopHandler: function () {
            var min = this.options.min,
                max = this.options.max,
                from = this.options.from,
                to = this.options.to;

            if (from > min && to === max) {
                this.$cache.s_from.addClass("type_last");
            } else if (to < max) {
                this.$cache.s_to.addClass("type_last");
            }
        },

        /**
         * Determine which handles was clicked last
         * and which handler should have hover effect
         *
         * @param target {String}
         */
        changeLevel: function (target) {
            switch (target) {
                case "single":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_single_fake);
                    break;
                case "from":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_from_fake);
                    this.$cache.s_from.addClass("state_hover");
                    this.$cache.s_from.addClass("type_last");
                    this.$cache.s_to.removeClass("type_last");
                    break;
                case "to":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_to_fake);
                    this.$cache.s_to.addClass("state_hover");
                    this.$cache.s_to.addClass("type_last");
                    this.$cache.s_from.removeClass("type_last");
                    break;
                case "both":
                    this.coords.p_gap_left = this.toFixed(this.coords.p_pointer - this.coords.p_from_fake);
                    this.coords.p_gap_right = this.toFixed(this.coords.p_to_fake - this.coords.p_pointer);
                    this.$cache.s_to.removeClass("type_last");
                    this.$cache.s_from.removeClass("type_last");
                    break;
            }
        },

        /**
         * Then slider is disabled
         * appends extra layer with opacity
         */
        appendDisableMask: function () {
            this.$cache.cont.append(disable_html);
            this.$cache.cont.addClass("irs-disabled");
        },

        /**
         * Remove slider instance
         * and ubind all events
         */
        remove: function () {
            this.$cache.cont.remove();
            this.$cache.cont = null;

            this.$cache.line.off("keydown.irs_" + this.plugin_count);

            this.$cache.body.off("touchmove.irs_" + this.plugin_count);
            this.$cache.body.off("mousemove.irs_" + this.plugin_count);

            this.$cache.win.off("touchend.irs_" + this.plugin_count);
            this.$cache.win.off("mouseup.irs_" + this.plugin_count);

            if (is_old_ie) {
                this.$cache.body.off("mouseup.irs_" + this.plugin_count);
                this.$cache.body.off("mouseleave.irs_" + this.plugin_count);
            }

            this.$cache.grid_labels = [];
            this.coords.big = [];
            this.coords.big_w = [];
            this.coords.big_p = [];
            this.coords.big_x = [];

            cancelAnimationFrame(this.raf_id);
        },

        /**
         * bind all slider events
         */
        bindEvents: function () {
            if (this.no_diapason) {
                return;
            }

            this.$cache.body.on("touchmove.irs_" + this.plugin_count, this.pointerMove.bind(this));
            this.$cache.body.on("mousemove.irs_" + this.plugin_count, this.pointerMove.bind(this));

            this.$cache.win.on("touchend.irs_" + this.plugin_count, this.pointerUp.bind(this));
            this.$cache.win.on("mouseup.irs_" + this.plugin_count, this.pointerUp.bind(this));

            this.$cache.line.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
            this.$cache.line.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));

            if (this.options.drag_interval && this.options.type === "double") {
                this.$cache.bar.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "both"));
                this.$cache.bar.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "both"));
            } else {
                this.$cache.bar.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                this.$cache.bar.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
            }

            if (this.options.type === "single") {
                this.$cache.single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
                this.$cache.s_single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
                this.$cache.shad_single.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));

                this.$cache.single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
                this.$cache.s_single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
                this.$cache.edge.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                this.$cache.shad_single.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
            } else {
                this.$cache.single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, null));
                this.$cache.single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, null));

                this.$cache.from.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
                this.$cache.s_from.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
                this.$cache.to.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
                this.$cache.s_to.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
                this.$cache.shad_from.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                this.$cache.shad_to.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));

                this.$cache.from.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
                this.$cache.s_from.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
                this.$cache.to.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
                this.$cache.s_to.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
                this.$cache.shad_from.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                this.$cache.shad_to.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
            }

            if (this.options.keyboard) {
                this.$cache.line.on("keydown.irs_" + this.plugin_count, this.key.bind(this, "keyboard"));
            }

            if (is_old_ie) {
                this.$cache.body.on("mouseup.irs_" + this.plugin_count, this.pointerUp.bind(this));
                this.$cache.body.on("mouseleave.irs_" + this.plugin_count, this.pointerUp.bind(this));
            }
        },

        /**
         * Mousemove or touchmove
         * only for handlers
         *
         * @param e {Object} event object
         */
        pointerMove: function (e) {
            if (!this.dragging) {
                return;
            }

            var x = e.pageX || e.originalEvent.touches && e.originalEvent.touches[0].pageX;
            this.coords.x_pointer = x - this.coords.x_gap;

            this.calc();
        },

        /**
         * Mouseup or touchend
         * only for handlers
         *
         * @param e {Object} event object
         */
        pointerUp: function (e) {
            if (this.current_plugin !== this.plugin_count) {
                return;
            }

            if (this.is_active) {
                this.is_active = false;
            } else {
                return;
            }

            this.$cache.cont.find(".state_hover").removeClass("state_hover");

            this.force_redraw = true;

            if (is_old_ie) {
                $("*").prop("unselectable", false);
            }

            this.updateScene();
            this.restoreOriginalMinInterval();

            // callbacks call
            if ($.contains(this.$cache.cont[0], e.target) || this.dragging) {
                this.callOnFinish();
            }
            
            this.dragging = false;
        },

        /**
         * Mousedown or touchstart
         * only for handlers
         *
         * @param target {String|null}
         * @param e {Object} event object
         */
        pointerDown: function (target, e) {
            e.preventDefault();
            var x = e.pageX || e.originalEvent.touches && e.originalEvent.touches[0].pageX;
            if (e.button === 2) {
                return;
            }

            if (target === "both") {
                this.setTempMinInterval();
            }

            if (!target) {
                target = this.target || "from";
            }

            this.current_plugin = this.plugin_count;
            this.target = target;

            this.is_active = true;
            this.dragging = true;

            this.coords.x_gap = this.$cache.rs.offset().left;
            this.coords.x_pointer = x - this.coords.x_gap;

            this.calcPointerPercent();
            this.changeLevel(target);

            if (is_old_ie) {
                $("*").prop("unselectable", true);
            }

            this.$cache.line.trigger("focus");

            this.updateScene();
        },

        /**
         * Mousedown or touchstart
         * for other slider elements, like diapason line
         *
         * @param target {String}
         * @param e {Object} event object
         */
        pointerClick: function (target, e) {
            e.preventDefault();
            var x = e.pageX || e.originalEvent.touches && e.originalEvent.touches[0].pageX;
            if (e.button === 2) {
                return;
            }

            this.current_plugin = this.plugin_count;
            this.target = target;

            this.is_click = true;
            this.coords.x_gap = this.$cache.rs.offset().left;
            this.coords.x_pointer = +(x - this.coords.x_gap).toFixed();

            this.force_redraw = true;
            this.calc();

            this.$cache.line.trigger("focus");
        },

        /**
         * Keyborard controls for focused slider
         *
         * @param target {String}
         * @param e {Object} event object
         * @returns {boolean|undefined}
         */
        key: function (target, e) {
            if (this.current_plugin !== this.plugin_count || e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
                return;
            }

            switch (e.which) {
                case 83: // W
                case 65: // A
                case 40: // DOWN
                case 37: // LEFT
                    e.preventDefault();
                    this.moveByKey(false);
                    break;

                case 87: // S
                case 68: // D
                case 38: // UP
                case 39: // RIGHT
                    e.preventDefault();
                    this.moveByKey(true);
                    break;
            }

            return true;
        },

        /**
         * Move by key. Beta
         * @todo refactor than have plenty of time
         *
         * @param right {boolean} direction to move
         */
        moveByKey: function (right) {
            var p = this.coords.p_pointer;

            if (right) {
                p += this.options.keyboard_step;
            } else {
                p -= this.options.keyboard_step;
            }

            this.coords.x_pointer = this.toFixed(this.coords.w_rs / 100 * p);
            this.is_key = true;
            this.calc();
        },

        /**
         * Set visibility and content
         * of Min and Max labels
         */
        setMinMax: function () {
            if (!this.options) {
                return;
            }

            if (this.options.hide_min_max) {
                this.$cache.min[0].style.display = "none";
                this.$cache.max[0].style.display = "none";
                return;
            }

            if (this.options.values.length) {
                this.$cache.min.html(this.decorate(this.options.p_values[this.options.min]));
                this.$cache.max.html(this.decorate(this.options.p_values[this.options.max]));
            } else {
                this.$cache.min.html(this.decorate(this._prettify(this.options.min), this.options.min));
                this.$cache.max.html(this.decorate(this._prettify(this.options.max), this.options.max));
            }

            this.labels.w_min = this.$cache.min.outerWidth(false);
            this.labels.w_max = this.$cache.max.outerWidth(false);
        },

        /**
         * Then dragging interval, prevent interval collapsing
         * using min_interval option
         */
        setTempMinInterval: function () {
            var interval = this.result.to - this.result.from;

            if (this.old_min_interval === null) {
                this.old_min_interval = this.options.min_interval;
            }

            this.options.min_interval = interval;
        },

        /**
         * Restore min_interval option to original
         */
        restoreOriginalMinInterval: function () {
            if (this.old_min_interval !== null) {
                this.options.min_interval = this.old_min_interval;
                this.old_min_interval = null;
            }
        },



        // =============================================================================================================
        // Calculations

        /**
         * All calculations and measures start here
         *
         * @param update {boolean=}
         */
        calc: function (update) {
            if (!this.options) {
                return;
            }

            this.calc_count++;

            if (this.calc_count === 10 || update) {
                this.calc_count = 0;
                this.coords.w_rs = this.$cache.rs.outerWidth(false);

                this.calcHandlePercent();
            }

            if (!this.coords.w_rs) {
                return;
            }

            this.calcPointerPercent();
            var handle_x = this.getHandleX();


            if (this.target === "both") {
                this.coords.p_gap = 0;
                handle_x = this.getHandleX();
            }

            if (this.target === "click") {
                this.coords.p_gap = this.coords.p_handle / 2;
                handle_x = this.getHandleX();

                if (this.options.drag_interval) {
                    this.target = "both_one";
                } else {
                    this.target = this.chooseHandle(handle_x);
                }
            }

            switch (this.target) {
                case "base":
                    var w = (this.options.max - this.options.min) / 100,
                        f = (this.result.from - this.options.min) / w,
                        t = (this.result.to - this.options.min) / w;

                    this.coords.p_single_real = this.toFixed(f);
                    this.coords.p_from_real = this.toFixed(f);
                    this.coords.p_to_real = this.toFixed(t);

                    this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);

                    this.coords.p_single_fake = this.convertToFakePercent(this.coords.p_single_real);
                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);

                    this.target = null;

                    break;

                case "single":
                    if (this.options.from_fixed) {
                        break;
                    }

                    this.coords.p_single_real = this.convertToRealPercent(handle_x);
                    this.coords.p_single_real = this.calcWithStep(this.coords.p_single_real);
                    this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max);

                    this.coords.p_single_fake = this.convertToFakePercent(this.coords.p_single_real);

                    break;

                case "from":
                    if (this.options.from_fixed) {
                        break;
                    }

                    this.coords.p_from_real = this.convertToRealPercent(handle_x);
                    this.coords.p_from_real = this.calcWithStep(this.coords.p_from_real);
                    if (this.coords.p_from_real > this.coords.p_to_real) {
                        this.coords.p_from_real = this.coords.p_to_real;
                    }
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, "from");
                    this.coords.p_from_real = this.checkMaxInterval(this.coords.p_from_real, this.coords.p_to_real, "from");

                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);

                    break;

                case "to":
                    if (this.options.to_fixed) {
                        break;
                    }

                    this.coords.p_to_real = this.convertToRealPercent(handle_x);
                    this.coords.p_to_real = this.calcWithStep(this.coords.p_to_real);
                    if (this.coords.p_to_real < this.coords.p_from_real) {
                        this.coords.p_to_real = this.coords.p_from_real;
                    }
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, "to");
                    this.coords.p_to_real = this.checkMaxInterval(this.coords.p_to_real, this.coords.p_from_real, "to");

                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);

                    break;

                case "both":
                    if (this.options.from_fixed || this.options.to_fixed) {
                        break;
                    }

                    handle_x = this.toFixed(handle_x + (this.coords.p_handle * 0.001));

                    this.coords.p_from_real = this.convertToRealPercent(handle_x) - this.coords.p_gap_left;
                    this.coords.p_from_real = this.calcWithStep(this.coords.p_from_real);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, "from");
                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);

                    this.coords.p_to_real = this.convertToRealPercent(handle_x) + this.coords.p_gap_right;
                    this.coords.p_to_real = this.calcWithStep(this.coords.p_to_real);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, "to");
                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);

                    break;

                case "both_one":
                    if (this.options.from_fixed || this.options.to_fixed) {
                        break;
                    }

                    var real_x = this.convertToRealPercent(handle_x),
                        from = this.result.from_percent,
                        to = this.result.to_percent,
                        full = to - from,
                        half = full / 2,
                        new_from = real_x - half,
                        new_to = real_x + half;

                    if (new_from < 0) {
                        new_from = 0;
                        new_to = new_from + full;
                    }

                    if (new_to > 100) {
                        new_to = 100;
                        new_from = new_to - full;
                    }

                    this.coords.p_from_real = this.calcWithStep(new_from);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);

                    this.coords.p_to_real = this.calcWithStep(new_to);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);

                    break;
            }

            if (this.options.type === "single") {
                this.coords.p_bar_x = (this.coords.p_handle / 2);
                this.coords.p_bar_w = this.coords.p_single_fake;

                this.result.from_percent = this.coords.p_single_real;
                this.result.from = this.convertToValue(this.coords.p_single_real);

                if (this.options.values.length) {
                    this.result.from_value = this.options.values[this.result.from];
                }
            } else {
                this.coords.p_bar_x = this.toFixed(this.coords.p_from_fake + (this.coords.p_handle / 2));
                this.coords.p_bar_w = this.toFixed(this.coords.p_to_fake - this.coords.p_from_fake);

                this.result.from_percent = this.coords.p_from_real;
                this.result.from = this.convertToValue(this.coords.p_from_real);
                this.result.to_percent = this.coords.p_to_real;
                this.result.to = this.convertToValue(this.coords.p_to_real);

                if (this.options.values.length) {
                    this.result.from_value = this.options.values[this.result.from];
                    this.result.to_value = this.options.values[this.result.to];
                }
            }

            this.calcMinMax();
            this.calcLabels();
        },


        /**
         * calculates pointer X in percent
         */
        calcPointerPercent: function () {
            if (!this.coords.w_rs) {
                this.coords.p_pointer = 0;
                return;
            }

            if (this.coords.x_pointer < 0 || isNaN(this.coords.x_pointer)  ) {
                this.coords.x_pointer = 0;
            } else if (this.coords.x_pointer > this.coords.w_rs) {
                this.coords.x_pointer = this.coords.w_rs;
            }

            this.coords.p_pointer = this.toFixed(this.coords.x_pointer / this.coords.w_rs * 100);
        },

        convertToRealPercent: function (fake) {
            var full = 100 - this.coords.p_handle;
            return fake / full * 100;
        },

        convertToFakePercent: function (real) {
            var full = 100 - this.coords.p_handle;
            return real / 100 * full;
        },

        getHandleX: function () {
            var max = 100 - this.coords.p_handle,
                x = this.toFixed(this.coords.p_pointer - this.coords.p_gap);

            if (x < 0) {
                x = 0;
            } else if (x > max) {
                x = max;
            }

            return x;
        },

        calcHandlePercent: function () {
            if (this.options.type === "single") {
                this.coords.w_handle = this.$cache.s_single.outerWidth(false);
            } else {
                this.coords.w_handle = this.$cache.s_from.outerWidth(false);
            }

            this.coords.p_handle = this.toFixed(this.coords.w_handle / this.coords.w_rs * 100);
        },

        /**
         * Find closest handle to pointer click
         *
         * @param real_x {Number}
         * @returns {String}
         */
        chooseHandle: function (real_x) {
            if (this.options.type === "single") {
                return "single";
            } else {
                var m_point = this.coords.p_from_real + ((this.coords.p_to_real - this.coords.p_from_real) / 2);
                if (real_x >= m_point) {
                    return this.options.to_fixed ? "from" : "to";
                } else {
                    return this.options.from_fixed ? "to" : "from";
                }
            }
        },

        /**
         * Measure Min and Max labels width in percent
         */
        calcMinMax: function () {
            if (!this.coords.w_rs) {
                return;
            }

            this.labels.p_min = this.labels.w_min / this.coords.w_rs * 100;
            this.labels.p_max = this.labels.w_max / this.coords.w_rs * 100;
        },

        /**
         * Measure labels width and X in percent
         */
        calcLabels: function () {
            if (!this.coords.w_rs || this.options.hide_from_to) {
                return;
            }

            if (this.options.type === "single") {

                this.labels.w_single = this.$cache.single.outerWidth(false);
                this.labels.p_single_fake = this.labels.w_single / this.coords.w_rs * 100;
                this.labels.p_single_left = this.coords.p_single_fake + (this.coords.p_handle / 2) - (this.labels.p_single_fake / 2);
                this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single_fake);

            } else {

                this.labels.w_from = this.$cache.from.outerWidth(false);
                this.labels.p_from_fake = this.labels.w_from / this.coords.w_rs * 100;
                this.labels.p_from_left = this.coords.p_from_fake + (this.coords.p_handle / 2) - (this.labels.p_from_fake / 2);
                this.labels.p_from_left = this.toFixed(this.labels.p_from_left);
                this.labels.p_from_left = this.checkEdges(this.labels.p_from_left, this.labels.p_from_fake);

                this.labels.w_to = this.$cache.to.outerWidth(false);
                this.labels.p_to_fake = this.labels.w_to / this.coords.w_rs * 100;
                this.labels.p_to_left = this.coords.p_to_fake + (this.coords.p_handle / 2) - (this.labels.p_to_fake / 2);
                this.labels.p_to_left = this.toFixed(this.labels.p_to_left);
                this.labels.p_to_left = this.checkEdges(this.labels.p_to_left, this.labels.p_to_fake);

                this.labels.w_single = this.$cache.single.outerWidth(false);
                this.labels.p_single_fake = this.labels.w_single / this.coords.w_rs * 100;
                this.labels.p_single_left = ((this.labels.p_from_left + this.labels.p_to_left + this.labels.p_to_fake) / 2) - (this.labels.p_single_fake / 2);
                this.labels.p_single_left = this.toFixed(this.labels.p_single_left);
                this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single_fake);

            }
        },



        // =============================================================================================================
        // Drawings

        /**
         * Main function called in request animation frame
         * to update everything
         */
        updateScene: function () {
            if (this.raf_id) {
                cancelAnimationFrame(this.raf_id);
                this.raf_id = null;
            }

            clearTimeout(this.update_tm);
            this.update_tm = null;

            if (!this.options) {
                return;
            }

            this.drawHandles();

            if (this.is_active) {
                this.raf_id = requestAnimationFrame(this.updateScene.bind(this));
            } else {
                this.update_tm = setTimeout(this.updateScene.bind(this), 300);
            }
        },

        /**
         * Draw handles
         */
        drawHandles: function () {
            this.coords.w_rs = this.$cache.rs.outerWidth(false);

            if (!this.coords.w_rs) {
                return;
            }

            if (this.coords.w_rs !== this.coords.w_rs_old) {
                this.target = "base";
                this.is_resize = true;
            }

            if (this.coords.w_rs !== this.coords.w_rs_old || this.force_redraw) {
                this.setMinMax();
                this.calc(true);
                this.drawLabels();
                if (this.options.grid) {
                    this.calcGridMargin();
                    this.calcGridLabels();
                }
                this.force_redraw = true;
                this.coords.w_rs_old = this.coords.w_rs;
                this.drawShadow();
            }

            if (!this.coords.w_rs) {
                return;
            }

            if (!this.dragging && !this.force_redraw && !this.is_key) {
                return;
            }

            if (this.old_from !== this.result.from || this.old_to !== this.result.to || this.force_redraw || this.is_key) {

                this.drawLabels();

                this.$cache.bar[0].style.left = this.coords.p_bar_x + "%";
                this.$cache.bar[0].style.width = this.coords.p_bar_w + "%";

                if (this.options.type === "single") {
                    this.$cache.s_single[0].style.left = this.coords.p_single_fake + "%";

                    this.$cache.single[0].style.left = this.labels.p_single_left + "%";
                } else {
                    this.$cache.s_from[0].style.left = this.coords.p_from_fake + "%";
                    this.$cache.s_to[0].style.left = this.coords.p_to_fake + "%";

                    if (this.old_from !== this.result.from || this.force_redraw) {
                        this.$cache.from[0].style.left = this.labels.p_from_left + "%";
                    }
                    if (this.old_to !== this.result.to || this.force_redraw) {
                        this.$cache.to[0].style.left = this.labels.p_to_left + "%";
                    }

                    this.$cache.single[0].style.left = this.labels.p_single_left + "%";
                }

                this.writeToInput();

                if ((this.old_from !== this.result.from || this.old_to !== this.result.to) && !this.is_start) {
                    this.$cache.input.trigger("change");
                    this.$cache.input.trigger("input");
                }

                this.old_from = this.result.from;
                this.old_to = this.result.to;

                // callbacks call
                if (!this.is_resize && !this.is_update && !this.is_start && !this.is_finish) {
                    this.callOnChange();
                }
                if (this.is_key || this.is_click) {
                    this.is_key = false;
                    this.is_click = false;
                    this.callOnFinish();
                }

                this.is_update = false;
                this.is_resize = false;
                this.is_finish = false;
            }

            this.is_start = false;
            this.is_key = false;
            this.is_click = false;
            this.force_redraw = false;
        },

        /**
         * Draw labels
         * measure labels collisions
         * collapse close labels
         */
        drawLabels: function () {
            if (!this.options) {
                return;
            }

            var values_num = this.options.values.length,
                p_values = this.options.p_values,
                text_single,
                text_from,
                text_to;

            if (this.options.hide_from_to) {
                return;
            }

            if (this.options.type === "single") {

                if (values_num) {
                    text_single = this.decorate(p_values[this.result.from]);
                    this.$cache.single.html(text_single);
                } else {
                    text_single = this.decorate(this._prettify(this.result.from), this.result.from);
                    this.$cache.single.html(text_single);
                }

                this.calcLabels();

                if (this.labels.p_single_left < this.labels.p_min + 1) {
                    this.$cache.min[0].style.visibility = "hidden";
                } else {
                    this.$cache.min[0].style.visibility = "hidden";
                }

                if (this.labels.p_single_left + this.labels.p_single_fake > 100 - this.labels.p_max - 1) {
                    this.$cache.max[0].style.visibility = "hidden";
                } else {
                    this.$cache.max[0].style.visibility = "hidden";
                }

            } else {

                if (values_num) {

                    if (this.options.decorate_both) {
                        text_single = this.decorate(p_values[this.result.from]);
                        text_single += this.options.values_separator;
                        text_single += this.decorate(p_values[this.result.to]);
                    } else {
                        text_single = this.decorate(p_values[this.result.from] + this.options.values_separator + p_values[this.result.to]);
                    }
                    text_from = this.decorate(p_values[this.result.from]);
                    text_to = this.decorate(p_values[this.result.to]);

                    this.$cache.single.html(text_single);
                    this.$cache.from.html(text_from);
                    this.$cache.to.html(text_to);

                } else {

                    if (this.options.decorate_both) {
                        text_single = this.decorate(this._prettify(this.result.from), this.result.from);
                        text_single += this.options.values_separator;
                        text_single += this.decorate(this._prettify(this.result.to), this.result.to);
                    } else {
                        text_single = this.decorate(this._prettify(this.result.from) + this.options.values_separator + this._prettify(this.result.to), this.result.to);
                    }
                    text_from = this.decorate(this._prettify(this.result.from), this.result.from);
                    text_to = this.decorate(this._prettify(this.result.to), this.result.to);

                    this.$cache.single.html(text_single);
                    this.$cache.from.html(text_from);
                    this.$cache.to.html(text_to);

                }

                this.calcLabels();

                var min = Math.min(this.labels.p_single_left, this.labels.p_from_left),
                    single_left = this.labels.p_single_left + this.labels.p_single_fake,
                    to_left = this.labels.p_to_left + this.labels.p_to_fake,
                    max = Math.max(single_left, to_left);

                if (this.labels.p_from_left + this.labels.p_from_fake >= this.labels.p_to_left) {
                    this.$cache.from[0].style.visibility = "hidden";
                    this.$cache.to[0].style.visibility = "hidden";
                    this.$cache.single[0].style.visibility = "visible";

                    if (this.result.from === this.result.to) {
                        if (this.target === "from") {
                            this.$cache.from[0].style.visibility = "visible";
                        } else if (this.target === "to") {
                            this.$cache.to[0].style.visibility = "visible";
                        } else if (!this.target) {
                            this.$cache.from[0].style.visibility = "visible";
                        }
                        this.$cache.single[0].style.visibility = "hidden";
                        max = to_left;
                    } else {
                        this.$cache.from[0].style.visibility = "hidden";
                        this.$cache.to[0].style.visibility = "hidden";
                        this.$cache.single[0].style.visibility = "visible";
                        max = Math.max(single_left, to_left);
                    }
                } else {
                    this.$cache.from[0].style.visibility = "visible";
                    this.$cache.to[0].style.visibility = "visible";
                    this.$cache.single[0].style.visibility = "hidden";
                }

                if (min < this.labels.p_min + 1) {
                    this.$cache.min[0].style.visibility = "hidden";
                } else {
                    this.$cache.min[0].style.visibility = "visible";
                }

                if (max > 100 - this.labels.p_max - 1) {
                    this.$cache.max[0].style.visibility = "hidden";
                } else {
                    this.$cache.max[0].style.visibility = "visible";
                }

            }
        },

        /**
         * Draw shadow intervals
         */
        drawShadow: function () {
            var o = this.options,
                c = this.$cache,

                is_from_min = typeof o.from_min === "number" && !isNaN(o.from_min),
                is_from_max = typeof o.from_max === "number" && !isNaN(o.from_max),
                is_to_min = typeof o.to_min === "number" && !isNaN(o.to_min),
                is_to_max = typeof o.to_max === "number" && !isNaN(o.to_max),

                from_min,
                from_max,
                to_min,
                to_max;

            if (o.type === "single") {
                if (o.from_shadow && (is_from_min || is_from_max)) {
                    from_min = this.convertToPercent(is_from_min ? o.from_min : o.min);
                    from_max = this.convertToPercent(is_from_max ? o.from_max : o.max) - from_min;
                    from_min = this.toFixed(from_min - (this.coords.p_handle / 100 * from_min));
                    from_max = this.toFixed(from_max - (this.coords.p_handle / 100 * from_max));
                    from_min = from_min + (this.coords.p_handle / 2);

                    c.shad_single[0].style.display = "block";
                    c.shad_single[0].style.left = from_min + "%";
                    c.shad_single[0].style.width = from_max + "%";
                } else {
                    c.shad_single[0].style.display = "none";
                }
            } else {
                if (o.from_shadow && (is_from_min || is_from_max)) {
                    from_min = this.convertToPercent(is_from_min ? o.from_min : o.min);
                    from_max = this.convertToPercent(is_from_max ? o.from_max : o.max) - from_min;
                    from_min = this.toFixed(from_min - (this.coords.p_handle / 100 * from_min));
                    from_max = this.toFixed(from_max - (this.coords.p_handle / 100 * from_max));
                    from_min = from_min + (this.coords.p_handle / 2);

                    c.shad_from[0].style.display = "block";
                    c.shad_from[0].style.left = from_min + "%";
                    c.shad_from[0].style.width = from_max + "%";
                } else {
                    c.shad_from[0].style.display = "none";
                }

                if (o.to_shadow && (is_to_min || is_to_max)) {
                    to_min = this.convertToPercent(is_to_min ? o.to_min : o.min);
                    to_max = this.convertToPercent(is_to_max ? o.to_max : o.max) - to_min;
                    to_min = this.toFixed(to_min - (this.coords.p_handle / 100 * to_min));
                    to_max = this.toFixed(to_max - (this.coords.p_handle / 100 * to_max));
                    to_min = to_min + (this.coords.p_handle / 2);

                    c.shad_to[0].style.display = "block";
                    c.shad_to[0].style.left = to_min + "%";
                    c.shad_to[0].style.width = to_max + "%";
                } else {
                    c.shad_to[0].style.display = "none";
                }
            }
        },



        /**
         * Write values to input element
         */
        writeToInput: function () {
            if (this.options.type === "single") {
                if (this.options.values.length) {
                    this.$cache.input.prop("value", this.result.from_value);
                } else {
                    this.$cache.input.prop("value", this.result.from);
                }
                this.$cache.input.data("from", this.result.from);
            } else {
                if (this.options.values.length) {
                    this.$cache.input.prop("value", this.result.from_value + this.options.input_values_separator + this.result.to_value);
                } else {
                    this.$cache.input.prop("value", this.result.from + this.options.input_values_separator + this.result.to);
                }
                this.$cache.input.data("from", this.result.from);
                this.$cache.input.data("to", this.result.to);
            }
        },



        // =============================================================================================================
        // Callbacks

        callOnStart: function () {
            this.writeToInput();

            if (this.options.onStart && typeof this.options.onStart === "function") {
                this.options.onStart(this.result);
            }
        },
        callOnChange: function () {
            this.writeToInput();

            if (this.options.onChange && typeof this.options.onChange === "function") {
                this.options.onChange(this.result);
            }
        },
        callOnFinish: function () {
            this.writeToInput();

            if (this.options.onFinish && typeof this.options.onFinish === "function") {
                this.options.onFinish(this.result);
            }
        },
        callOnUpdate: function () {
            this.writeToInput();

            if (this.options.onUpdate && typeof this.options.onUpdate === "function") {
                this.options.onUpdate(this.result);
            }
        },




        // =============================================================================================================
        // Service methods

        toggleInput: function () {
            this.$cache.input.toggleClass("irs-hidden-input");
        },

        /**
         * Convert real value to percent
         *
         * @param value {Number} X in real
         * @param no_min {boolean=} don't use min value
         * @returns {Number} X in percent
         */
        convertToPercent: function (value, no_min) {
            var diapason = this.options.max - this.options.min,
                one_percent = diapason / 100,
                val, percent;

            if (!diapason) {
                this.no_diapason = true;
                return 0;
            }

            if (no_min) {
                val = value;
            } else {
                val = value - this.options.min;
            }

            percent = val / one_percent;

            return this.toFixed(percent);
        },

        /**
         * Convert percent to real values
         *
         * @param percent {Number} X in percent
         * @returns {Number} X in real
         */
        convertToValue: function (percent) {
            var min = this.options.min,
                max = this.options.max,
                min_decimals = min.toString().split(".")[1],
                max_decimals = max.toString().split(".")[1],
                min_length, max_length,
                avg_decimals = 0,
                abs = 0;

            if (percent === 0) {
                return this.options.min;
            }
            if (percent === 100) {
                return this.options.max;
            }


            if (min_decimals) {
                min_length = min_decimals.length;
                avg_decimals = min_length;
            }
            if (max_decimals) {
                max_length = max_decimals.length;
                avg_decimals = max_length;
            }
            if (min_length && max_length) {
                avg_decimals = (min_length >= max_length) ? min_length : max_length;
            }

            if (min < 0) {
                abs = Math.abs(min);
                min = +(min + abs).toFixed(avg_decimals);
                max = +(max + abs).toFixed(avg_decimals);
            }

            var number = ((max - min) / 100 * percent) + min,
                string = this.options.step.toString().split(".")[1],
                result;

            if (string) {
                number = +number.toFixed(string.length);
            } else {
                number = number / this.options.step;
                number = number * this.options.step;

                number = +number.toFixed(0);
            }

            if (abs) {
                number -= abs;
            }

            if (string) {
                result = +number.toFixed(string.length);
            } else {
                result = this.toFixed(number);
            }

            if (result < this.options.min) {
                result = this.options.min;
            } else if (result > this.options.max) {
                result = this.options.max;
            }

            return result;
        },

        /**
         * Round percent value with step
         *
         * @param percent {Number}
         * @returns percent {Number} rounded
         */
        calcWithStep: function (percent) {
            var rounded = Math.round(percent / this.coords.p_step) * this.coords.p_step;

            if (rounded > 100) {
                rounded = 100;
            }
            if (percent === 100) {
                rounded = 100;
            }

            return this.toFixed(rounded);
        },

        checkMinInterval: function (p_current, p_next, type) {
            var o = this.options,
                current,
                next;

            if (!o.min_interval) {
                return p_current;
            }

            current = this.convertToValue(p_current);
            next = this.convertToValue(p_next);

            if (type === "from") {

                if (next - current < o.min_interval) {
                    current = next - o.min_interval;
                }

            } else {

                if (current - next < o.min_interval) {
                    current = next + o.min_interval;
                }

            }

            return this.convertToPercent(current);
        },

        checkMaxInterval: function (p_current, p_next, type) {
            var o = this.options,
                current,
                next;

            if (!o.max_interval) {
                return p_current;
            }

            current = this.convertToValue(p_current);
            next = this.convertToValue(p_next);

            if (type === "from") {

                if (next - current > o.max_interval) {
                    current = next - o.max_interval;
                }

            } else {

                if (current - next > o.max_interval) {
                    current = next + o.max_interval;
                }

            }

            return this.convertToPercent(current);
        },

        checkDiapason: function (p_num, min, max) {
            var num = this.convertToValue(p_num),
                o = this.options;

            if (typeof min !== "number") {
                min = o.min;
            }

            if (typeof max !== "number") {
                max = o.max;
            }

            if (num < min) {
                num = min;
            }

            if (num > max) {
                num = max;
            }

            return this.convertToPercent(num);
        },

        toFixed: function (num) {
            num = num.toFixed(20);
            return +num;
        },

        _prettify: function (num) {
            if (!this.options.prettify_enabled) {
                return num;
            }

            if (this.options.prettify && typeof this.options.prettify === "function") {
                return this.options.prettify(num);
            } else {
                return this.prettify(num);
            }
        },

        prettify: function (num) {
            var n = num.toString();
            return n.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + this.options.prettify_separator);
        },

        checkEdges: function (left, width) {
            if (!this.options.force_edges) {
                return this.toFixed(left);
            }

            if (left < 0) {
                left = 0;
            } else if (left > 100 - width) {
                left = 100 - width;
            }

            return this.toFixed(left);
        },

        validate: function () {
            var o = this.options,
                r = this.result,
                v = o.values,
                vl = v.length,
                value,
                i;

            if (typeof o.min === "string") o.min = +o.min;
            if (typeof o.max === "string") o.max = +o.max;
            if (typeof o.from === "string") o.from = +o.from;
            if (typeof o.to === "string") o.to = +o.to;
            if (typeof o.step === "string") o.step = +o.step;

            if (typeof o.from_min === "string") o.from_min = +o.from_min;
            if (typeof o.from_max === "string") o.from_max = +o.from_max;
            if (typeof o.to_min === "string") o.to_min = +o.to_min;
            if (typeof o.to_max === "string") o.to_max = +o.to_max;

            if (typeof o.keyboard_step === "string") o.keyboard_step = +o.keyboard_step;
            if (typeof o.grid_num === "string") o.grid_num = +o.grid_num;

            if (o.max < o.min) {
                o.max = o.min;
            }

            if (vl) {
                o.p_values = [];
                o.min = 0;
                o.max = vl - 1;
                o.step = 1;
                o.grid_num = o.max;
                o.grid_snap = true;

                for (i = 0; i < vl; i++) {
                    value = +v[i];

                    if (!isNaN(value)) {
                        v[i] = value;
                        value = this._prettify(value);
                    } else {
                        value = v[i];
                    }

                    o.p_values.push(value);
                }
            }

            if (typeof o.from !== "number" || isNaN(o.from)) {
                o.from = o.min;
            }

            if (typeof o.to !== "number" || isNaN(o.to)) {
                o.to = o.max;
            }

            if (o.type === "single") {

                if (o.from < o.min) o.from = o.min;
                if (o.from > o.max) o.from = o.max;

            } else {

                if (o.from < o.min) o.from = o.min;
                if (o.from > o.max) o.from = o.max;

                if (o.to < o.min) o.to = o.min;
                if (o.to > o.max) o.to = o.max;

                if (this.update_check.from) {

                    if (this.update_check.from !== o.from) {
                        if (o.from > o.to) o.from = o.to;
                    }
                    if (this.update_check.to !== o.to) {
                        if (o.to < o.from) o.to = o.from;
                    }

                }

                if (o.from > o.to) o.from = o.to;
                if (o.to < o.from) o.to = o.from;

            }

            if (typeof o.step !== "number" || isNaN(o.step) || !o.step || o.step < 0) {
                o.step = 1;
            }

            if (typeof o.keyboard_step !== "number" || isNaN(o.keyboard_step) || !o.keyboard_step || o.keyboard_step < 0) {
                o.keyboard_step = 5;
            }

            if (typeof o.from_min === "number" && o.from < o.from_min) {
                o.from = o.from_min;
            }

            if (typeof o.from_max === "number" && o.from > o.from_max) {
                o.from = o.from_max;
            }

            if (typeof o.to_min === "number" && o.to < o.to_min) {
                o.to = o.to_min;
            }

            if (typeof o.to_max === "number" && o.from > o.to_max) {
                o.to = o.to_max;
            }

            if (r) {
                if (r.min !== o.min) {
                    r.min = o.min;
                }

                if (r.max !== o.max) {
                    r.max = o.max;
                }

                if (r.from < r.min || r.from > r.max) {
                    r.from = o.from;
                }

                if (r.to < r.min || r.to > r.max) {
                    r.to = o.to;
                }
            }

            if (typeof o.min_interval !== "number" || isNaN(o.min_interval) || !o.min_interval || o.min_interval < 0) {
                o.min_interval = 0;
            }

            if (typeof o.max_interval !== "number" || isNaN(o.max_interval) || !o.max_interval || o.max_interval < 0) {
                o.max_interval = 0;
            }

            if (o.min_interval && o.min_interval > o.max - o.min) {
                o.min_interval = o.max - o.min;
            }

            if (o.max_interval && o.max_interval > o.max - o.min) {
                o.max_interval = o.max - o.min;
            }
        },

        decorate: function (num, original) {
            var decorated = "",
                o = this.options;

            if (o.prefix) {
                decorated += o.prefix;
            }

            decorated += num;

            if (o.max_postfix) {
                if (o.values.length && num === o.p_values[o.max]) {
                    decorated += o.max_postfix;
                    if (o.postfix) {
                        decorated += " ";
                    }
                } else if (original === o.max) {
                    decorated += o.max_postfix;
                    if (o.postfix) {
                        decorated += " ";
                    }
                }
            }

            if (o.postfix) {
                decorated += o.postfix;
            }

            return decorated;
        },

        updateFrom: function () {
            this.result.from = this.options.from;
            this.result.from_percent = this.convertToPercent(this.result.from);
            if (this.options.values) {
                this.result.from_value = this.options.values[this.result.from];
            }
        },

        updateTo: function () {
            this.result.to = this.options.to;
            this.result.to_percent = this.convertToPercent(this.result.to);
            if (this.options.values) {
                this.result.to_value = this.options.values[this.result.to];
            }
        },

        updateResult: function () {
            this.result.min = this.options.min;
            this.result.max = this.options.max;
            this.updateFrom();
            this.updateTo();
        },


        // =============================================================================================================
        // Grid

        appendGrid: function () {
            if (!this.options.grid) {
                return;
            }

            var o = this.options,
                i, z,

                total = o.max - o.min,
                big_num = o.grid_num,
                big_p = 0,
                big_w = 0,

                small_max = 4,
                local_small_max,
                small_p,
                small_w = 0,

                result,
                html = '';



            this.calcGridMargin();

            if (o.grid_snap) {

                if (total > 50) {
                    big_num = 50 / o.step;
                    big_p = this.toFixed(o.step / 0.5);
                } else {
                    big_num = total / o.step;
                    big_p = this.toFixed(o.step / (total / 100));
                }

            } else {
                big_p = this.toFixed(100 / big_num);
            }

            if (big_num > 4) {
                small_max = 3;
            }
            if (big_num > 7) {
                small_max = 2;
            }
            if (big_num > 14) {
                small_max = 1;
            }
            if (big_num > 28) {
                small_max = 0;
            }

            for (i = 0; i < big_num + 1; i++) {
                local_small_max = small_max;

                big_w = this.toFixed(big_p * i);

                if (big_w > 100) {
                    big_w = 100;

                    local_small_max -= 2;
                    if (local_small_max < 0) {
                        local_small_max = 0;
                    }
                }
                this.coords.big[i] = big_w;

                small_p = (big_w - (big_p * (i - 1))) / (local_small_max + 1);

                for (z = 1; z <= local_small_max; z++) {
                    if (big_w === 0) {
                        break;
                    }

                    small_w = this.toFixed(big_w - (small_p * z));

                    html += '<span class="irs-grid-pol small" style="left: ' + small_w + '%"></span>';
                }

                html += '<span class="irs-grid-pol" style="left: ' + big_w + '%"></span>';

                result = this.convertToValue(big_w);
                if (o.values.length) {
                    result = o.p_values[result];
                } else {
                    result = this._prettify(result);
                }

                html += '<span class="irs-grid-text js-grid-text-' + i + '" style="left: ' + big_w + '%">' + result + '</span>';
            }
            this.coords.big_num = Math.ceil(big_num + 1);



            this.$cache.cont.addClass("irs-with-grid");
            this.$cache.grid.html(html);
            this.cacheGridLabels();
        },

        cacheGridLabels: function () {
            var $label, i,
                num = this.coords.big_num;

            for (i = 0; i < num; i++) {
                $label = this.$cache.grid.find(".js-grid-text-" + i);
                this.$cache.grid_labels.push($label);
            }

            this.calcGridLabels();
        },

        calcGridLabels: function () {
            var i, label, start = [], finish = [],
                num = this.coords.big_num;

            for (i = 0; i < num; i++) {
                this.coords.big_w[i] = this.$cache.grid_labels[i].outerWidth(false);
                this.coords.big_p[i] = this.toFixed(this.coords.big_w[i] / this.coords.w_rs * 100);
                this.coords.big_x[i] = this.toFixed(this.coords.big_p[i] / 2);

                start[i] = this.toFixed(this.coords.big[i] - this.coords.big_x[i]);
                finish[i] = this.toFixed(start[i] + this.coords.big_p[i]);
            }

            if (this.options.force_edges) {
                if (start[0] < -this.coords.grid_gap) {
                    start[0] = -this.coords.grid_gap;
                    finish[0] = this.toFixed(start[0] + this.coords.big_p[0]);

                    this.coords.big_x[0] = this.coords.grid_gap;
                }

                if (finish[num - 1] > 100 + this.coords.grid_gap) {
                    finish[num - 1] = 100 + this.coords.grid_gap;
                    start[num - 1] = this.toFixed(finish[num - 1] - this.coords.big_p[num - 1]);

                    this.coords.big_x[num - 1] = this.toFixed(this.coords.big_p[num - 1] - this.coords.grid_gap);
                }
            }

            this.calcGridCollision(2, start, finish);
            this.calcGridCollision(4, start, finish);

            for (i = 0; i < num; i++) {
                label = this.$cache.grid_labels[i][0];

                if (this.coords.big_x[i] !== Number.POSITIVE_INFINITY) {
                    label.style.marginLeft = -this.coords.big_x[i] + "%";
                }
            }
        },

        // Collisions Calc Beta
        // TODO: Refactor then have plenty of time
        calcGridCollision: function (step, start, finish) {
            var i, next_i, label,
                num = this.coords.big_num;

            for (i = 0; i < num; i += step) {
                next_i = i + (step / 2);
                if (next_i >= num) {
                    break;
                }

                label = this.$cache.grid_labels[next_i][0];

                if (finish[i] <= start[next_i]) {
                    label.style.visibility = "visible";
                } else {
                    label.style.visibility = "hidden";
                }
            }
        },

        calcGridMargin: function () {
            if (!this.options.grid_margin) {
                return;
            }

            this.coords.w_rs = this.$cache.rs.outerWidth(false);
            if (!this.coords.w_rs) {
                return;
            }

            if (this.options.type === "single") {
                this.coords.w_handle = this.$cache.s_single.outerWidth(false);
            } else {
                this.coords.w_handle = this.$cache.s_from.outerWidth(false);
            }
            this.coords.p_handle = this.toFixed(this.coords.w_handle  / this.coords.w_rs * 100);
            this.coords.grid_gap = this.toFixed((this.coords.p_handle / 2) - 0.1);

            this.$cache.grid[0].style.width = this.toFixed(100 - this.coords.p_handle) + "%";
            this.$cache.grid[0].style.left = this.coords.grid_gap + "%";
        },



        // =============================================================================================================
        // Public methods

        update: function (options) {
            if (!this.input) {
                return;
            }

            this.is_update = true;

            this.options.from = this.result.from;
            this.options.to = this.result.to;
            this.update_check.from = this.result.from;
            this.update_check.to = this.result.to;

            this.options = $.extend(this.options, options);
            this.validate();
            this.updateResult(options);

            this.toggleInput();
            this.remove();
            this.init(true);
        },

        reset: function () {
            if (!this.input) {
                return;
            }

            this.updateResult();
            this.update();
        },

        destroy: function () {
            if (!this.input) {
                return;
            }

            this.toggleInput();
            this.$cache.input.prop("readonly", false);
            $.data(this.input, "ionRangeSlider", null);

            this.remove();
            this.input = null;
            this.options = null;
        }
    };

    $.fn.ionRangeSlider = function (options) {
        return this.each(function() {
            if (!$.data(this, "ionRangeSlider")) {
                $.data(this, "ionRangeSlider", new IonRangeSlider(this, options, plugin_count++));
            }
        });
    };



    // =================================================================================================================
    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

    // requestAnimationFrame polyfill by Erik MÃ¶ller. fixes from Paul Irish and Tino Zijdel

    // MIT license

    (function() {
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                || window[vendors[x]+'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function(callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                    timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };

        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = function(id) {
                clearTimeout(id);
            };
    }());

}));



// Trigger

$(function () {
  
var $range = $(".js-range-slider"),
    $inputFrom = $(".js-input-from"),
    $inputTo = $(".js-input-to"),
    instance,
    min = 5000,
    max = 1000000,
    from = 0,
    to = 0;

$range.ionRangeSlider({
    type: "double",
    min: min,
    max: max,
    from: 0,
    to: 40000,
  prefix: 'Rp. ',
    onStart: updateInputs,
    onChange: updateInputs,
    step: 1000,
    prettify_enabled: true,
    prettify_separator: ".",
  values_separator: " - ",
  force_edges: true
  

});

instance = $range.data("ionRangeSlider");

function updateInputs (data) {
    from = data.from;
    to = data.to;
    
    $inputFrom.prop("value", from);
    $inputTo.prop("value", to); 
}

$inputFrom.on("input", function () {
    var val = $(this).prop("value");
    
    // validate
    if (val < min) {
        val = min;
    } else if (val > to) {
        val = to;
    }
    
    instance.update({
        from: val
    });
});

$inputTo.on("input", function () {
    var val = $(this).prop("value");
    
    // validate
    if (val < from) {
        val = from;
    } else if (val > max) {
        val = max;
    }
    
    instance.update({
        to: val
    });
});

    });
	

















	var Travelite = {
		initialised: false,
		version: 1.0,
		mobile: false,
		container : $('#portfolio-item-container'),
		blogContainer: $('#blog-item-container'),
		productContainer: $('#product-container'),
		init: function () {

			if(!this.initialised) {
				this.initialised = true;
			} else {
				return;
			}













			// Call Travelite Functions
			this.RTL();
			this.pageLoading();
			this.MenuToggle();
			this.ParallaxPageTitle();
			this.VisibleSpaceTopmenu();
			this.VisibleSpaceMainmenu();
			this.AccordionSinglecollapse();
			this.LoaderPostLoad();
			this.CheckDropdownVisibility();
			this.SearchWidgetFooter();
			this.packagedetailstab();
			this.packagebookingtab();
			this.paymentverticaltabs();
			this.featuretabssider();
			this.DateTimePickerAll();
			this.CommentsLastChild();
			this.CustomerSays();
			this.EassyPieChart();
			this.counterjs();
			this.Eventslide();
			this.testimonialslider();
			this.packagedetails();
			this.partnerscrousel();
			this.loginform();
			this.countdownjs();
			this.videopopup();
			this.videoplayer();
			this.slickslider();
			this.seemorelessbutton();
			this.moretextwidget();
			this.pricefilterslider();
			this.spinnerselect();
			this.portfoliocolumnthree();
			
	
		
			// this.checkMobile();
			this.fullHeight();
			// this.filterColorBg();
			this.tooltip();
			
			
			
			/* Call function if Owl Carousel plugin is included */
			if ( $.fn.owlCarousel ) {
				this.owlCarousels();
			}

			/* Call function if Magnific Popup plugin is included */
			if ( $.fn.magnificPopup) {
				this.newsletterPopup();
				this.lightBox();
			}

			/* Call function if Media element plugin is included */
			if ($.fn.mediaelementplayer) {
				this.mediaElement();
			}

			/* Call function if Media noUiSlider plugin is included */
			if ($.fn.noUiSlider) {
				this.priceSlider();
			}

			var self = this;
			/* Imagesloaded plugin included in isotope.pkgd.min.js */
			/* Portfolio isotope + Blog masonry with images loaded plugin */
			if (typeof imagesLoaded === 'function') {
				/* */
				imagesLoaded(self.container, function() {
					self.isotopeActivate();
					// recall for plugin support
					self.isotopeFilter();
				});

				/* check images for blog masonry/grid */
				imagesLoaded(self.blogContainer, function() {
					self.blogMasonry();
				});

				/* check images for product masonry/grid index11 */
				imagesLoaded(self.productContainer, function() {
					self.productMasonry();
				});
			}

		},
		
		RTL: function () {
			// On Right-to-left(RTL) add class 
			var rtl_attr = $("html").attr('dir');
			if(rtl_attr){
				$('html').find('body').addClass("right-to-left");	
			}		
		},
		pageLoading: function(){
		   var value = 1;
		   var interval = setInterval(function(){

			if(value <= 100){
			 $('.pointer_val').html(value+'%');
			 $('.travel_loading_bar_overlay').css('width', value+'%').animate();
			}else{
			 clearInterval(interval);
			}
			value++;
			
			if(value == 7){
			 $('.pointer').css('opacity','1');
			 $('.travel_loading_bar > span.from svg path').css('fill','#86B940');
			 $('.travel_loading_bar > span.to').addClass('jump');
			}
			if(value == 99){
			 $('.pointer').css('opacity','0');
			 $('.travel_loading_bar > span.to svg path').css('fill','#FCB715');
			 $('.travel_loading_bar > span.to').removeClass('jump');
			}
			if(value == 100){
			 $('.travel_page_loader').fadeOut();
			}
			
		   },80);
		   
		   
		   //page load animation
		   if ($('body').hasClass('travel_home')){
				setTimeout(function(){
				  $('#travel_wrapper').addClass('travel_body_loaded'); 
			    }, 8000);
		
			} else{
			    setTimeout(function(){
				  $('#travel_wrapper').addClass('travel_body_loaded'); 
			    }, 100);
			}
			
			
		},
		ManuDropdownToggle: function () {
			/*Add dropdown toggle html on tablat/mobile width for open dropdown*/
			var width = $(document).width();
			if (width < 767) {
				$("li.travel_dropdown").append("<span class='dropdown_toggle'></span>");	
				
				$('.main_menu > ul > li').children('.dropdown_toggle').on('click', function(){
					$(this).prev('ul.sub-menu').slideToggle();	

				});
				
				
				
				$('.main_menu > ul > li > ul.sub-menu > li > .dropdown_toggle').on('click', function(){
					$('.main_menu > ul > li > ul.sub-menu > li > ul.sub-menu').slideToggle();	
				});
				
			}
			
		},
		ServiceIsotope: function () {
			/*Service Isotoped section on service page(09_Services.html) */
			var $container = $('.service_wrapper');
			$container.isotope({
				itemSelector: '.item',
				masonry: {
				  gutter: 0,
				  isFitWidth: true
				}
			});
		},
		OfferIsotope: function () {
			/*Offer Isotoped section on offer page(15_Offers.html) */
			var $container = $('.offer_wrapper');
			$container.isotope({
				itemSelector: '.item',
				masonry: {
				  gutter: 0,
				  isFitWidth: true
				}
			});
		},
		MenuToggle:function () {
			/* Menu toggle on mobile/tablat/mobile width */
			var $menuToggle = $(".menu-toggle");
			$menuToggle.on('click',function() {
				$('.main_menu').toggleClass('open');
				$(this).toggleClass("menu-toggle--open");
			});
		},
		ParallaxPageTitle:function () {
			/* parallax for page title */
			$('.page_title').parallax("50%", 0);
		},
		VisibleSpaceTopmenu:function () {
			/* Submenu Check Visible Space on top menu */
			$(".top_menu ul li").hover(function() {
				if($(window).width() < 700){
					return;
					} 
				var subMenu = $(this).find("ul.sub-menu");
				
				if(!subMenu.get(0)){
					return;
					} 
				var screenWidth = $(window).width(),
					subMenuOffset = subMenu.offset(),
					subMenuWidth = subMenu.width(),
					subMenuParentWidth = subMenu.parents("ul.sub-menu").width(),
					subMenuPosRight = subMenu.offset().left + subMenu.width();
					

				if(subMenuPosRight > screenWidth) {
					subMenu.css("margin-left", "-" + (subMenuParentWidth + subMenuWidth + 10) + "px");
				}else {
					subMenu.css("margin-left", 0);
				}
			
			});
		},
		VisibleSpaceMainmenu:function () {
			/* Submenu Check Visible Space on main menu */
			$(".main_menu ul li").hover(function() {
				if($(window).width() < 700){
					return;
					} 
				var subMenu = $(this).find("ul.sub-menu");
				
				if(!subMenu.get(0)){
					return;
					} 
				var screenWidth = $(window).width(),
					subMenuOffset = subMenu.offset(),
					subMenuWidth = subMenu.width(),
					subMenuParentWidth = subMenu.parents("ul.sub-menu").width(),
					subMenuPosRight = subMenu.offset().left + subMenu.width();
					

				if(subMenuPosRight > screenWidth) {
					subMenu.css("margin-left", "-" + (subMenuParentWidth + subMenuWidth + 10) + "px");
				}else {
					subMenu.css("margin-left", 0);
				}
			});
		},
		AccordionSinglecollapse:function () {
			/* accordion single-collapse */
			var active1 = $('#accordion .panel-collapse.in').prev().addClass('active');
			active1.find('a').prepend('<i class="fa fa-angle-up"></i>');
			$('#accordion .panel-heading').not(active1).find('a').prepend('<i class="fa fa-angle-down"></i>');
			$('#accordion').on('show.bs.collapse', function (e) {
				$('#accordion .panel-heading.active').removeClass('active').find('.fa').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
				$(e.target).prev().addClass('active').find('.fa').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
			});
		},
		LoaderPostLoad:function () {
			/* load post loader effect */
			$(".widget_categories ul li input").click( function(){
			   //if( $(this).is(':checked') )
			   $(".post_wrapper").addClass('load_post');
				setTimeout(function(){ 
					$(".post_wrapper").removeClass('load_post');	
			   }, 3000);
			});
		},
		CheckDropdownVisibility:function () {
			/* check dropdown is visible or not */
			$('li').has('ul.sub-menu').addClass('travel_dropdown');
		},
		
		SearchWidgetFooter:function () {
			/*  widget search availability tab */
			$('.wsa_tab > ul').each(function(){
				var $active, $content, $links = $(this).find('a');
				$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
				$active.addClass('active');

				$content = $($active[0].hash);

				// Hide the remaining content
				$links.not($active).each(function () {
				  $(this.hash).hide();
				});

				// Bind the click event handler
				$(this).on('click', 'a', function(e){
					// Make the old tab inactive.
					$active.removeClass('active');
					$content.hide();

					// Update the variables with the new link and content
					$active = $(this);
					$content = $(this.hash);

					// Make the tab active.
					$active.addClass('active');
					$content.show();

					// Prevent the anchor's default click action
					e.preventDefault();
				});
			});
			
		},
		
		//packagedetailstab
		packagedetailstab:function(){
			$( "#travelite_middle_tabs" ).tabs();
			
		},
		//packagedetailstab end
		
		
		
		//packagebookingtab
		packagebookingtab:function(){
			$( "#tour_booking_tabs" ).tabs();
			
		},
		//paymentverticaltabs
		paymentverticaltabs:function(){
			$( "#payment_vertical_tabs" ).tabs();
			
		},
		
		
	
		//feature tabs slider
		featuretabssider:function(){
		
        $(".slide_tabs a").on("click", function() {
            $(".slide_tabs").find(".active_tab").removeClass("active_tab");
            $(this).parent().addClass("active_tab");
        });
		
		//tabsjs
		$('.feature_tabs > ul').each(function(){
		var $active, $content, $links = $(this).find('a');
		$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
		$active.addClass('active');
		$content = $($active[0].hash);
		$links.not($active).each(function () {
		  $(this.hash).hide();
		});
		$(this).on('click', 'a', function(e){
		  $active.removeClass('active');
		  $content.hide();
		  $active = $(this);
		  $(window).trigger('resize');
		  $content = $(this.hash);
		  
		  $active.addClass('active');
		  
		  $content.show();
		  e.preventDefault();
		});
		});
		
		//featuretabcrousel
		// $('.feature_tab_crousel').bxSlider({
		// maxSlides: 3,
		// minSlides: 1,
		// slideWidth: 282,
		// slideMargin: 0,
		// pager:true,
		// controllers:false,
		// responsive:{
					// 480:{
						// minSlides:1,
						
					// },
					// 767: {
						// minSlides:2,
						
					// },
					
				// }
		// });
			
		},

		DateTimePickerAll:function () {
			//datetime picker event based on calendar 
			   $('#event_based_on_cal').datetimepicker({
				yearOffset:0,
				inline: true,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'2014/01/01', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			   });

			//datetime picker footer widget global search
			$('#Check_in_date_global_search').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});


			//datetime picker footer widget global search 
			$('#Check_out_date_global_search').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});	


			//datetime picker footer widget hotal search
			$('#Check_in_date_hotal_search').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});


			//datetime picker footer widget hotal search 
			$('#Check_out_date_hotal_search').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});		

			//datetime picker footer widget local search
			$('#Check_in_date_local_search').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});


			//datetime picker footer widget local search 
			$('#Check_out_date_local_search').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
		
			//event from date
			$('#select_date_event').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			
			
			//tab form date check in
			$('#Check_in_date_tab').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			$('#Check_in_date_tab_flight').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			
			$('#Check_in_date_tab_car').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			$('#Check_in_date_tab_hospital').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			
			$('#flights_Check_in_date_tab').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'m/d/Y',
				formatDate:'m/d/y',
				minDate:'-1970/01/02', // yesterday is minimum date
			});
			
			//tab form date check out
			$('#Check_out_date_tab').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			$('#flights_Check_out_date_tab').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'m/d/Y',
				formatDate:'m/d/y',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			$('#Check_out_date_tab_flight').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			$('#Check_out_date_tab_car').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			$('#Check_out_date_tab_hospital').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			
			
			//Home 3 slider check checkin
			$('#booking_checkin_date').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
			
			//Home 3 slider check checkout
			$('#booking_checkout_date').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
				//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
			});
              
			  //slider check in date
			$('#slider_Check_in_date').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
			});
			
			//slider check out date
			$('#slider_Check_out_date').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
			});
			
			 //slider check in date
			$('#slider_Check_in_date_hotal_search').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
			});
			
			//slider check out date
			$('#slider_Check_out_date_hotal_search').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
			});
			
			
			 //slider check in date
			$('#slider_Check_in_date_local_search').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
			});
			
			//slider check out date
			$('#slider_Check_out_date_local_search').datetimepicker({
				yearOffset:0,
				timepicker:false,
				format:'d/m/Y',
				formatDate:'Y/m/d',
				minDate:'-1970/01/02', // yesterday is minimum date
			});
			
		},
		CommentsLastChild:function () {
			/* comment - check child ul  */
			$('#comments ol li:last-child').has('ul.children').addClass('last-with-ul');	
		},
		CustomerSays:function () {
			/* Customer Says slider and click function */
			$('.customer_says_slider .owl-carousel').owlCarousel({
				items:5,
				margin:10,
				nav:true,
				navText: [ '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 197.402 197.402" style="enable-background:new 0 0 197.402 197.402;" xml:space="preserve"><g><g><g><polygon style="fill:#808b8d;" points="146.883,197.402 45.255,98.698 146.883,0 152.148,5.418 56.109,98.698 152.148,191.98 "/></g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>', '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 223.413 223.413" style="enable-background:new 0 0 223.413 223.413;" xml:space="preserve"><g><g><g><polygon style="fill:#808b8d;" points="57.179,223.413 51.224,217.276 159.925,111.71 51.224,6.127 57.179,0 172.189,111.71 "/></g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>' ],
				responsive:{
					0:{
						items:1
					},
					480:{
						items:2
					},
					600:{
						items:4
					},
					1000:{
						items:5
					}
				}
			});
			
			/* Customer Says test first element will be show */
			$('.customer_says_text #cst_1').show();
			/* Customer Says detail first element will be show */
			$('.customer_says_detail #csd_1').show();
			
			/* Customer Says image click function */
			$('.customer_says_slider .item > img').on('click', function(){
				var cst_id =$(this).attr("id");
				var cst_id2 = cst_id.split("_")[1];
				console.log(cst_id2);
				$('#cst_'+cst_id2).show();
				var cur_cst_id='cst_'+cst_id2;
				$('.customer_says_text > div').each(function(){
					if($(this).attr("id")!=cur_cst_id){
						$(this).hide();
					}
				});
				
				
				var csd_id =$(this).attr("id");
				var csd_id2 = csd_id.split("_")[1];
				console.log(csd_id2);
				$('#csd_'+csd_id2).show();
				var cur_csd_id='csd_'+csd_id2;
				$('.customer_says_detail > div').each(function(){
					if($(this).attr("id")!=cur_csd_id){
						$(this).hide();
					}
				});
				
			});
			
			$('.customer_says_slider .item').on('click', function(event){
				$('.customer_says_slider .item').removeClass('active');
				$(this).addClass('active');
			});
			
			
		},
		
		EassyPieChart: function(){
			$(".counter_section").mouseenter(function() {
			//pie-chart start
	       $('.percentage-light').easyPieChart({
			barColor: function(percent) {
				return "#ff5ba0";
			},
			trackColor: '#f8f8f8',
			scaleColor: false,
			lineCap: 'butt', 
			animate: 6000,
			lineWidth: 10,
			trackWidth: 10,
			onStep: function(value) {
				this.$el.find('span').text(~~value);
			}
		 });	
		 //pie-chart start
	       $('.percentage_blue').easyPieChart({
			barColor: function(percent) {
				return "#6054aa";
			},
			trackColor: '#f8f8f8',
			scaleColor: false,
			lineCap: 'butt', 
			animate: 6000,
			lineWidth: 10,
			trackWidth: 10,
			onStep: function(value) {
				this.$el.find('span').text(~~value);
			}
		 });
		 //pie-chart start
	       $('.percentage_red').easyPieChart({
			barColor: function(percent) {
				return "#e12e45";
			},
			trackColor: '#f8f8f8',
			scaleColor: false,
			lineCap: 'butt', 
			animate: 6000,
			lineWidth: 10,
			trackWidth: 10,
			onStep: function(value) {
				this.$el.find('span').text(~~value);
			}
		 });
		 //pie-chart start
	       $('.percentage_skyblue').easyPieChart({
			barColor: function(percent) {
				return "#07b7b5";
			},
			trackColor: '#f8f8f8',
			scaleColor: false,
			lineCap: 'butt', 
			animate: 6000,
			lineWidth: 10,
			trackWidth: 10,
			onStep: function(value) {
				this.$el.find('span').text(~~value);
			}
		 });
		 
		 
		});	
		
		},
		
		counterjs: function(){
			
			//counter js
			$(window).scroll(function() {
          $('.timer').each(count);

          function count(options) {
            var $this = $(this);
            options = $.extend({}, options || {}, $this.data('countToOptions') || {});
            $this.countTo(options);
        }
		});

		},
		//eventslider
		Eventslide:function(){
			
		$("#syncro_slide").owlCarousel({
		items:1,
      nav : true,
	  dots:false,
	  navText:['<i class="fa fa-arrow-left"></i>','<i class="fa fa-arrow-right"></i>'],
	  autoPlay: 4000,
       paginationSpeed : 1500,
       slideSpeed : 3000,
	   smartSpeed:1000,
      singleItem:true,
	  transitionStyle : "fade",
	   animateIn: 'fadeIn'
 
  });
			
		},
		
		
		
		//testimonials-slider
		testimonialslider:function(){
			$("#home_testimonials").owlCarousel({
		items:1,
       nav : true,
	   dots:false,
	   navText:['<i class="fa fa-chevron-right"></i>','<i class="fa fa-chevron-left"></i>'],
	   autoPlay: true,
       singleItem:true,
	   animateIn: 'fadeIn',
	   mouseDrag:false,
	   activeClass:'active_navs',
	   transitionStyle : "fade",
 
  });
			
		},
		
		//packagedetails-slider
		packagedetails:function(){
			$("#package_details_slider").owlCarousel({
			items:1,
			nav : true,
			auto:true,
			dots:false,
			navText:['<i class="fa fa-arrow-left"></i>','<i class="fa fa-arrow-right"></i>'],
			paginationSpeed : 1500,
			slideSpeed : 3000,
			smartSpeed:1000,
			singleItem:true,
			transitionStyle : "fade",
			

 
  });
			
		},
		//packagedetails-slider-End
	
		
		//partner_crousel_slide start
		partnerscrousel:function(){
			
			$("#partner_crousel_slide").owlCarousel({
 
			  autoPlay: 3000, 
			  items : 5,
			  dots:false,
			  nav:true,
				navText:['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
				responsive:{
					0:{
						items:1
					},
					480:{
						items:1
					},
					600:{
						items:2
					},
					1000:{
						items:4
					},
					1100:{
						items:4
					},
					1200:{
						items:5
					}
				}
 
  });
		},
		
		//loginform start		
		loginform: function(){
		$(".travelite_login_alert").on('click', function(){
			$(".travelite_login_form").fadeIn();
			
		});
		$(".close_btn").on('click', function(){
			$(".travelite_login_form").fadeOut();
			
		});
		$(".travelite_login_alert").on('click', function(){
			$(".travelite_signup_form").fadeOut();
			
		});
		
		//signup form
		$(".travelite_signup_alert").on('click', function(){
			$(".travelite_signup_form").fadeIn();
			
		});
		$(".close_btn").on('click', function(){
			$(".travelite_signup_form").fadeOut();
			
		});
		
		$(".travelite_signup_alert").on('click', function(){
			$(".travelite_login_form").fadeOut();
			
		});

	},
		countdownjs: function(){
			$('[data-countdown]').each(function() {
			   var $this = $(this), finalDate = $(this).data('countdown');
			   $this.countdown(finalDate, function(event) {
				 $this.html(event.strftime(''
					+ '<div class="counter-container"><div class="counter-box first"><div class="number_time_c">%-D</div><span>Day%!d</span></div>'
					+ '<div class="counter-box"><div class="number_time_c">%H</div><span>Hours</span></div>'
					+ '<div class="counter-box"><div class="number_time_c">%M</div><span>Minutes</span></div>'
					+ '<div class="counter-box last"><div class="number_time_c">%S</div><span>Seconds</span></div></div>'
				   ));
			   });
			 });	
		},
		
		//videopopup start
		videopopup:function(){
		$('.popup_video_swm').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});
			
		
		},
		//videopopup end
		
		//videoplayer start
		videoplayer:function(){
			
	   $('audio,video').mediaelementplayer({
	   features: ['playpause','progress','volume','postroll']
       });
		},
		//videoplayer end
		
		//slickslider start
		slickslider:function(){
			
			$('.slick_crousel').slick({
  centerMode: true,
  centerPadding: '0px',
  slidesToShow: 3,
  arrows: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: true,
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: true,
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 1
      }
    }
  ]
});

$('.content_1').show();
		$('.slick_crousel').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			$('.cls').hide();
 			$('.content_'+( parseInt(nextSlide) + 1 )).show();
  		});
		},
		
		
		//seemoreless
		seemorelessbutton:function(){
			 var showChar = 287; 
    var ellipsestext = "...";
    var moretext = "See More Details";
    var lesstext = "less Details";
    

    $('.more_text').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
		},
		
		//moretext widget start
		moretextwidget:function(){
			var showChar = 32; 
    var ellipsestext = "-";
    var moretext = "More";
    var lesstext = "less";
    

    $('.more_text_widget').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink_w">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink_w").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
			
		},
		
		// moretext widget end
		
		//pricefilterslider
		   pricefilterslider: function (){
			
	 $( "#slider" ).slider({
      range: true,
      min: 0,
      max: 1000,
      values: [ 25, 900 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider" ).slider( "values", 0 ) +
      " - $" + $( "#slider" ).slider( "values", 1 ) );
			 
		},
		//pricefilterslider End
		
		//spinnerselect
		spinnerselect:function(){
			$( ".input_spinner" ).spinner();
		},
		
		//portfoliocolumnthree
		portfoliocolumnthree:function(){
			$('.portfolio_column_3_popup .portfolio-meta > a').magnificPopup({
				type: 'image',
				tLoading: 'Loading image #%curr%...',
				mainClass: 'mfp-img-mobile',
				gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
				},
				image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title') + '<small>by Travelite @ 2016. All Right Reserved</small>';
				}
				}
			});
		},
		//portfoliocolumnthree end
		
		toggleClass: function () {
			$('#mobile-menu, #mobile-menu-btn').toggleClass('opened');
			$('body').toggleClass('no-scroll');
		},
		fullHeight: function () {
			/* make a section full window height with predefined class */
			$('.fullheight').each(function () {
				var winHeight = $(window).height();

				$(this).css('height', winHeight);
			});
		},
		menuScrollbar: function () {
			if ($.fn.slimScroll) {
				/* For Side Menu*/
				if ( $('.side-menu').hasClass('dark') ) {
					/* check for dark side menu and change color of scrollbar */
					var bgColor = '#606060';
				}

				$('.side-menu-wrapper').slimScroll({
					height: 'auto',
					color: (bgColor) ? bgColor : '#2e2e2e',
					opacity: 0.6,
					size: '3px',
					alwaysVisible: false
				});

				/* Mobile menu*/
				$('#mobile-menu-wrapper').slimScroll({
					height: 'auto',
					color: '#fff',
					opacity: 0.2,
					size: '4px',
					alwaysVisible: false,
					distance: '2px'
				});
			}
		},
		owlCarousels: function () {
			var self = this;
			
			/* portfolio item slider */
			var owl2 = $(".portfolio_item_slider");
			owl2.owlCarousel({
				margin: 0,
				loop:true,
				responsiveClass: true,
				nav: false,
				dots: false,
				items: 1,
				autoplayTimeout: 8000,
				autoPlay : false,
				mouseDrag:false,
				animateIn: 'fadeIn',
				animateOut: 'fadeOut',
			});
			// Go to the next item
			$('.portfolionext').on("click",function() {
				owl2.trigger('next.owl.carousel');
			});
			// Go to the previous item
			$('.portfolioprev').on("click",function() {
				owl2.trigger('prev.owl.carousel', [300]);
			});
			
			
			
			/* Product newarrivals carousel (shoes)  - (index.html - homepage) */
			$('.owl-carousel.home-newarrivals-carousel').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480: {
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					},
					1200:{
						items:5
					}
				}
			});

			/* Latest Blog Posts Carousels - (index.html - homepage) */
			$('.owl-carousel.home-latestblog-carousel').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480: {
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					},
					1200:{
						items:5
					}
				}
			});

			/* index14.html - Lookbook carousel */
			$('.owl-carousel.lookbook-carousel').owlCarousel({
	            loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				autoplay: true,
				autoplayTimeout: 10000,
				center: true,
				responsive:{
					0:{
						items:1
					},
					520:{
						items:2
					},
					768: {
						items:3
					},
					992: {
						items:4
					}
				}
	        });

	        /* Latest Blog Posts Carousels - (index20.html - homepage) */
			$('.owl-carousel.home-latestposts-carousel').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					520:{
						items:2
					},
					768: {
						items:3
					},
					992: {
						items:4
					}
				}
			});

			/* Latest Blog Posts Carousels - (index8html - homepage) */
			$('.owl-carousel.home-latestposts-carousel-sm').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					520:{
						items:2
					},
					768: {
						items:3
					}
				}
			});

			/* Product.html -  Product carousel to zoom product section */
			$('.owl-carousel.product-gallery').owlCarousel({
	            loop:false,
				margin:15,
				responsiveClass:true,
				nav:false,
				dots: false,
				autoplay: true,
				autoplayTimeout: 10000,
				responsive:{
					0:{
						items:3
					},
					480: {
						items:4
					}
				}
	        });

			/* Portfolio - Related Projects Carousel - (single-portfolio.html) */
			$('.owl-carousel.portfolio-related-carousel').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:true,
				navText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
				dots: false,
				autoplay: true,
				autoplayTimeout: 10000,
				responsive:{
					0:{
						items:1
					},
					600: {
						items:2
					},
					992:{
						items:3
					}
				}
			});


			/* Product featured carousel  - (product.html - homepages) */
			$('.owl-carousel.product-featured-carousel').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					},
					1200:{
						items:5
					}
				}
			});

			/* Product popular carousel  - (product.html - homepages) */
			$('.owl-carousel.product-popular-carousel').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					},
					1200:{
						items:5
					}
				}
			});

			/* Product newarrivals carousel  - (product.html - homepages) */
			$('.owl-carousel.product-newarrivals-carousel').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					},
					1200:{
						items:5
					}
				}
			});

			/* Product featured carousel  - (index2.html - homepages) */
			$('.owl-carousel.product-featured-carousel-sm').owlCarousel({
				loop:false,
				margin:24,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					}
				}
			});

			/* Product popular carousel  - (index2.html - homepages) */
			$('.owl-carousel.product-popular-carousel-sm').owlCarousel({
				loop:false,
				margin:24,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					}
				}
			});

			/* Product newarrivals carousel  - (index2.html - homepages) */
			$('.owl-carousel.product-newarrivals-carousel-sm').owlCarousel({
				loop:false,
				margin:24,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					}
				}
			});

			/* Sale products carousel  - (index2.html - homepages) */
			$('.owl-carousel.product-sale-carousel').owlCarousel({
				loop:false,
				margin:24,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					}
				}
			});

			/* Testimonial Slider Sidebar - widget  - (index2.html) */
			$('.owl-carousel.testimonials-slider').owlCarousel({
				loop:true,
				margin: 0,
				responsiveClass: true,
				nav: false,
				dots: false,
				items: 1,
				autoplay: true,
				autoplayTimeout: 8000
			});

			/* Product featured carousel  - (index16.html - homepages) */
			$('.owl-carousel.product-featured-carousel-lg').owlCarousel({
				loop:false,
				margin:45,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:2,
						margin:30
					},
					768:{
						items:3,
						margin:30
					},
					992:{
						items:4
					},
					1200:{
						items:5
					}
				}
			});

			/* Product popular carousel  - (index16.html - homepages) */
			$('.owl-carousel.product-popular-carousel-lg').owlCarousel({
				loop:false,
				margin:45,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:2,
						margin:30
					},
					768:{
						items:3,
						margin:30
					},
					992:{
						items:4
					},
					1200:{
						items:5
					}
				}
			});

			/* Product newarrivals carousel  - (index16.html - homepages) */
			$('.owl-carousel.product-newarrivals-carousel-lg').owlCarousel({
				loop:false,
				margin:45,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:2,
						margin:30
					},
					768:{
						items:3,
						margin:30
					},
					992:{
						items:4
					},
					1200:{
						items:5
					}
				}
			});

			/* Product clearance carousel  - (index16.html - homepages) */
			$('.owl-carousel.product-clearance-carousel').owlCarousel({
				loop:false,
				margin:45,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:2,
						margin:30
					},
					768:{
						items:3,
						margin:30
					},
					992:{
						items:4
					},
					1200:{
						items:5
					}
				}
			});

			/* Product featured carousel  - (index5.html - homepages) */
			$('.owl-carousel.product-featured-carousel-xlg').owlCarousel({
				loop:false,
				margin:25,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					},
					1200:{
						items:5
					},
					1400: {
						items:6
					}
				}
			});

			/* Product popular carousel  - (index5.html - homepages) */
			$('.owl-carousel.product-popular-carousel-xlg').owlCarousel({
				loop:false,
				margin:25,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					},
					1200:{
						items:5
					},
					1400: {
						items:6
					}
				}
			});

			/* Product newarrivals carousel  - (index5.html - homepages) */
			$('.owl-carousel.product-newarrivals-carousel-xlg').owlCarousel({
				loop:false,
				margin:25,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					},
					992:{
						items:4
					},
					1200:{
						items:5
					},
					1400: {
						items:6
					}
				}
			});

			/* Product newarrivals carousel  - (index18.html - homepages) */
			$('.owl-carousel.presentation-newarrivals-carousel').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					}
				}
			});

			/* Product featured carousel  - (index18.html - homepages) */
			$('.owl-carousel.presentation-featured-carousel').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					768:{
						items:3
					}
				}
			});

			/* Banner row first carousel  - (index6.html - homepages) */
			$('.owl-carousel.banner-row-carousel-first').owlCarousel({
				loop:false,
				margin:0,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480: {
						items:2
					},
					768:{
						items:2
					},
					992:{
						items:3
					},
					1400: {
						items:4
					},
					1650: {
						items:5
					}
				}
			});

			/* Banner row second carousel  - (index6.html - homepages) */
			$('.owl-carousel.banner-row-carousel-second').owlCarousel({
				loop:false,
				margin:0,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480: {
						items:2
					},
					768:{
						items:2
					},
					992:{
						items:3
					},
					1400: {
						items:4
					},
					1650: {
						items:5
					}
				}
			});

			/* Banner row third carousel  - (index6.html - homepages) */
			$('.owl-carousel.banner-row-carousel-third').owlCarousel({
				loop:false,
				margin:0,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480: {
						items:2
					},
					768:{
						items:2
					},
					992:{
						items:3
					},
					1400: {
						items:4
					},
					1650: {
						items:5
					}
				}
			});

			/* Product featured carousel  - (index12.html - homepages) */
			$('.owl-carousel.product-featured-carousel-6col').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					480:{
						items:3
					},
					768:{
						items:4
					},
					992:{
						items:5
					},
					1200:{
						items:6
					}
				}
			});

			/* Product popular carousel  - (index12.html - homepages) */
			$('.owl-carousel.product-popular-carousel-6col').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					480:{
						items:3
					},
					768:{
						items:4
					},
					992:{
						items:5
					},
					1200:{
						items:6
					}
				}
			});

			/* Product newarrivals carousel  - (index12.html - homepages) */
			$('.owl-carousel.product-newarrivals-carousel-6col').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:2
					},
					480:{
						items:3
					},
					768:{
						items:4
					},
					992:{
						items:5
					},
					1200:{
						items:6
					}
				}
			});

			/* Product featured carousel  - (index17.html - homepages) */
			$('.owl-carousel.product-featured-carousel-side').owlCarousel({
				loop:false,
				margin:23,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:2
					},
					768:{
						items:3
					},
					1200:{
						items:4
					}
				}
			});

			/* Product popular carousel  - (index17.html - homepages) */
			$('.owl-carousel.product-popular-carousel-side').owlCarousel({
				loop:false,
				margin:23,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:2
					},
					768:{
						items:3
					},
					1200:{
						items:4
					}
				}
			});

			/* Product newarrivals carousel  - (index17.html - homepages) */
			$('.owl-carousel.product-newarrivals-carousel-side').owlCarousel({
				loop:false,
				margin:23,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:2
					},
					768:{
						items:3
					},
					1200:{
						items:4
					}
				}
			});

			/* Latest Blog Posts Carousels - (index17.html - homepage) */
			$('.owl-carousel.home-blog-post-carousel').owlCarousel({
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:false,
				dots: false,
				responsive:{
					0:{
						items:1
					},
					520:{
						items:2
					},
					768: {
						items:3
					}
				}
			});


			/*Caution This carousel function has to be called after the function above
			You must call this function after the carousel inside of tabs (for example product.html lava tab)*/
			/* Product - Products Carousel */
			var productCarousel = $('.owl-carousel.product-slider').owlCarousel({
				loop:false,
				margin:0,
				items:1,
				responsiveClass:true,
				animateOut: 'fadeOut', // Choose a calls form animated.css and change then tada
				nav:true,
				navText: ['Previous', 'Next'],
				dots: false
			});

			
			//about slider
			  $('.about_slider').bxSlider({
				  
				  pager:false,
				  auto:true,
				  
			  });
			  

			/* index.html - Clients -partners carousel  */
			$('.owl-carousel.our-partners').owlCarousel({
				loop:false,
				margin:0,
				responsiveClass:true,
				nav:true,
				navText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
				dots: false,
				responsive:{
					0:{
						items:2,
						nav:false
					},
					420: {
						items:3,
						nav:false
					},
					520: {
						items:4
					},
					992:{
						items:5,
					},
					1199:{
						items:6,
					}
				}
			});
			
		},
		scrollTopBtnAppear: function () {
			// This will be triggered at the bottom of code with window scroll event
			var windowTop = $(window).scrollTop(),
		            scrollTop = $('#scroll-top');

	        if (windowTop >= 300) {
	            scrollTop.addClass('fixed');
	        } else {
	            scrollTop.removeClass('fixed');
	        }
		    
		},
		scrollToAnimation: function (speed, offset, e) {
			/* General scroll to function */
			var targetEl = $(this).attr('href'),
				toTop = false;

			if (!$(targetEl).length) {
				if (targetEl === '#header' || targetEl === '#top' || targetEl === '#wrapper') {
					targetPos = 0;
					toTop = true;
				} else {
					return;
				}
			} else {
				var elem = $(targetEl),
					targetPos = offset ? ( elem.offset().top + offset ) : elem.offset().top;
			}
			
			if (targetEl || toTop) {
				$('html, body').animate({
		            'scrollTop': targetPos
		        }, speed || 1200);
		        e.preventDefault();
			}
		},
		priceSlider:function () {
			// Slider For category pages / filter price
			$('#price-range').noUiSlider({
				start: [0, 2990],
				handles: 2,
				connect: true,
				range: {
					'min': 0,
					'max': 4000
				}
			});

			$("#price-range").Link('lower').to( $('#slider-low-value') )
			$("#price-range").Link('upper').to( $('#slider-high-value') );
		},
		// filterColorBg: function () {
			// /* Category-item filter color box background */
			// $('.filter-color-box').each(function() {
				// var $this = $(this),
					// bgColor = $this.data('bgcolor');

					// $this.css('background-color', bgColor);
			// });
		// },
		tooltip: function () {
			// Bootstrap tooltip
			if($.fn.tooltip) {
				$('.add-tooltip').tooltip();
			}
		},
		newsletterPopup : function () {
			// Newsletter form popup - require magnific-popup plugin on page load

			if ( ! document.getElementById('newsletter-popup-form') ) {
				return;
			}

			jQuery.magnificPopup.open({
				items: {
					src: '#newsletter-popup-form'
				},
				type: 'inline'
			}, 0);
		},
		lightBox: function () {
			/* Popup for gallery items and videso and etc.. */
			/* magnific-popup.css and jquery.magnific.popup.mi.js files need to be included */

			/* This is for gallery images */
			$('.popup-gallery').magnificPopup({
				delegate: '.zoom-item',
				type: 'image',
				closeOnContentClick: false,
				closeBtnInside: true,
				mainClass: 'mfp-fade',
				removalDelay: 100,
				gallery: {
					enabled: true
				}
			});


			/* This is for iframe - youtube - vimeo videos - goole maps  with fade animation */
			$('.popup-iframe').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,
				fixedContentPos: false
			});

		},
		animateKnob: function() {
			// Animate knob
			if ($.fn.knob) {
				$('.knob').each(function() {
					var $this = $(this),
						container = $this.closest('.progress-animate'),
						animateTo = $this.data('animateto'),
						animateSpeed = $this.data('animatespeed')
					$this.animate(
			                { value: animateTo }, 
			                {   duration: animateSpeed,
			                    easing: 'swing',
		                    progress: function() {
		                      $this.val(Math.round(this.value)).trigger('change');
		                    },
		                    complete: function () {
		                    	container.removeClass('progress-animate');
		                    }
	               		});

				});
			}
		},
		mediaElement: function () {
			/* Media element plugin for video and audio support and styling */
			$('video, audio').mediaelementplayer();
		},
		scrollAnimations: function () {

			/* 	// Wowy Plugin
				Add Html elements wow and animation class 
				And you can add duration via data attributes
				data-wow-duration: Change the animation duration
				data-wow-delay: Delay before the animation starts
				data-wow-offset: Distance to start the animation (related to the browser bottom)
				data-wow-iteration: Number of times the animation is repeated
			*/

			// Check for class WOW // You need to call wow.min.js and animate.css for scroll animations to work
			if (typeof WOW === 'function') {
				new WOW({
					boxClass:     'wow',      // default
					animateClass: 'animated', // default
					offset:       0          // default
				}).init();
			}

		},
		isotopeActivate: function() {
			// Trigger for isotope plugin
			if($.fn.isotope) {
				var container = this.container,
					layoutMode = container.data('layoutmode');

				container.isotope({
                	itemSelector: '.portfolio-item',
                	layoutMode: (layoutMode) ? layoutMode : 'masonry',
                	transitionDuration: 0
            	});

            	
			}
		},
		isotopeReinit: function () {
			// Recall for isotope plugin
			if($.fn.isotope) {
				this.container.isotope('destroy');
				this.isotopeActivate();
			}
		},
		isotopeFilter: function () {
			// Isotope plugin filter handle
			var self = this,
				filterContainer = $('#portfolio-filter');

			filterContainer.find('a').on('click', function(e) {
				var $this = $(this),
					selector = $this.attr('data-filter');

				filterContainer.find('.active').removeClass('active');

				// And filter now
				self.container.isotope({
					filter: selector,
					transitionDuration: '0.8s'
				});
				
				$this.closest('li').addClass('active');
				e.preventDefault();
			});
		},
		blogMasonry: function () {
			/* Masonry - Grid for blog pages with isotope.pkgd.min.js file */

			// This is defined at the top of the this file
			var blogContainer = this.blogContainer;

			blogContainer.isotope({
				itemSelector: '.entry',
				masonry: {
					gutter: 30
				}
			});
		},
		productMasonry: function () {
			/* Masonry - Grid for product homepages with isotope.pkgd.min.js file */
			var productContainer = this.productContainer;

			productContainer.isotope({
				itemSelector: '.product',
				layoutmode: 'fitRows'
			});
		},
		

	};

	Travelite.init();

	// Load Event
	$(window).on('load', function() {
		/* Trigger side menu scrollbar */
		Travelite.menuScrollbar();

		/* Trigger Scroll Animations */
		Travelite.scrollAnimations();
		Travelite.ManuDropdownToggle();
		Travelite.ServiceIsotope();
		Travelite.OfferIsotope();
	});

	// Scroll Event
	$(window).on('scroll', function () {
		/* Display Scrol to Top Button */
		Travelite.scrollTopBtnAppear();

	});

	// Resize Event 
	// Smart resize if plugin not found window resize event
	if($.event.special.debouncedresize) {
		$(window).on('debouncedresize', function() {

			/* Full Height recall */
			Travelite.fullHeight();

	    });
	} else {
		$(window).on('resize', function () {
			
			/* Full Height recall */
			Travelite.fullHeight();

		});
	}

	/* Do not delete - this is trigger for owl carousels which used in bootstrap tab plugin */
	/* This is update for carousels  example (product.html) */
    $('.nav-lava').find('a[data-toggle="tab"]').on('shown.bs.tab', function (event) {
		/* Trigger resize event for to owl carousels fit */
		var evt = document.createEvent('UIEvents');
		evt.initUIEvent('resize', true, false,window,0);
		window.dispatchEvent(evt);
    });
	
	//hotelthumbnailslider
			//hotelthumbnail slider start
		$('.thumbnail_hotel_slider').bxSlider({
			pagerCustom: '#bx-pager',
			mode:'fade',
			auto:true,
			autoControls: true,
			pause: 3000,
			controls: false,
			touchEnabled: true,
			prevText: '<i class="fa fa-angle-left"></i>',
            nextText: '<i class="fa fa-angle-right"></i>',
			responsive: true
        });
	
   
		 $('.hotel_thumbnail_crousel').bxSlider({
		  minSlides:3,
		  maxSlides: 5,
		  auto:true,
		  autoControls: true,
		  pause: 4000,
		  moveSlides: 1,
		  controls: true,
		  pager:false,
		  responsive: true,
		  touchEnabled: true
        });
		//hotelthumbnail crousel end
				
		 //menu scroll fixed
      $(window).bind('scroll', function() {
        if ($(window).scrollTop() > 70) {
            $('.header_bottom').addClass('fixed_top_menu').slideDown('slow');


        } else {
            $('.header_bottom').removeClass('fixed_top_menu');
        }

    });
	
})(jQuery);