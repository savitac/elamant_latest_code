function show_result_pre_loader() {
    var t = setInterval(function() {
        var e = $("#bar"),
            r = parseInt($(".result-pre-loader-container").width()),
            a = bar_width = parseInt(e.width()),
            o = parseInt(r / 10);
        bar_width >= r ? (e.text("Please Wait ... 100%"), clearInterval(t)) : (a = bar_width + o, e.width(a), e.text(parseInt(a / 10) + "%"))
    }, 1e3)
}

function hide_result_pre_loader() { $(".result-pre-loader-wrapper").hide();
	                                $(".all_loading").hide(); }
$(document).on("focus", "input:not([readonly],[type=submit],[type=button],[type=reset],button)", function() { $(this).select() }), $.widget("custom.catcomplete", $.ui.autocomplete, {
    _create: function() { this._super(), this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)") },
    _renderMenu: function(t, e) {
        var r = this,
            a = "";
        $.each(e, function(e, o) {
            var n;
            o.category != a && (t.append("<li class='ui-autocomplete-category'>" + o.category + "</li>"), a = o.category), n = r._renderItemData(t, o), o.category && n.attr("aria-label", o.category + " : " + o.label)
        })
    }
});

//-----------OFFLINE PAYMENT ---------

$(document).on('click', '.btn-offline-pay', function(){
	var serial = $('#offline_form').serializeArray();
	$.post(app_base_url+'index.php/general/offline_payment',{data:serial}, function(data){
	
        // $('.offline-msg').html('Process Successfull , We have sent mail to confirm offline payment');
        window.location.href = app_base_url+'index.php/general/offline_approve/'+data;
	},'json');
})
