﻿$(document).ready(function() {
	alert("new one");
	var user_details_confirmed = 0;

	// make payment 
	$('#make-payment-btn').on('click', function(){
		
		//Hiding Continue Button
		$('.continue_booking_button').attr('disabled', true);
		$('<button class="btn btn-success" disabled="disabled"> Processing....</button>').insertAfter($('.continue_booking_button:eq(0)'));
		$('.continue_booking_button').hide();
		$('.alert-wrapper').addClass('hide');
		$('.alert-content').text('');
		
		user_details_confirmed = 1;
		$('#pre-booking-form').submit();
		$('#passenger-confirm-modal').modal('hide');
		user_details_confirmed = 0;	
	});
 
	$('#billing-country').on('change', function() {
		if (this.value in city_list_cache) {
			update_city_options(this.value);
		} else {
			fill_city_list(this.value);
		}
	});

	/**
	*Cache city list based on country
	*/
	var city_list_cache = {};
	function fill_city_list(country_name)
	{
		$.get(app_base_url+"index.php/ajax/get_city_list/"+country_name, function(city_list) {
			city_list_cache[country_name] = city_list;
			update_city_options(country_name)
		});
	}

	/**
	*update city list based on country from cache
	*/
	function update_city_options(country_name)
	{
		$('#billing-city').html(city_list_cache[country_name]);
	}
	//fill_city_list($('#billing-country').val());

	$('[type="submit"]').on('click', function(e) {
		
		var module_name = $(this).attr('name');
		var continue_book_button = $(this);
		var _status = true;
		var _focus = '';
		$('select:required').each(function() {
			if (this.value == 'INVALIDIP') {
				$(this).addClass('invalid-ip');
				if (_status == true) {
					_status = false;
					_focus = this;
				}
			} else if ($(this).hasClass('invalid-ip')) {
				$(this).removeClass('invalid-ip');
			}
		});
		if(! validate_mobile($('#passenger-contact').val())) {
			$('#passenger-contact').addClass('invalid-ip');
			$('#invalid_mobile_msg').remove();
			$('#passenger-contact').after('<p id="invalid_mobile_msg" class="text-danger">Mobile Number Should be 10 digits</p>');
			_status = false;
			_focus = '';
		} else {
			$('#invalid_mobile_msg').empty();
		}

		

		$('#pre-booking-form').find(':input[required]:visible').each(function(){
			if($(this).is(':visible') && $(this).val() == ''){
				_status = false;
			}			
		});

		$('#invalid_cond_msg').remove();
		if(!$('#terms_cond1').is(':checked')){
			_status = false;
			$('#clikagre').after('<p class="text-danger" id="invalid_cond_msg">Please select terms and conditions.</p>');
		}

		var billing_email = $('#billing-email').val();
		if(billing_email =='' || validate_email(billing_email) == false) {
			$('#billing-email').addClass('invalid-ip');
			$('#invalid_email_msg').remove();
			$('#billing-email').after('<p id="invalid_email_msg" class="text-danger">Invalid EmailId</p>');
			_status = false;
			_focus = '';
		} else {
			$('#invalid_email_msg').empty();
		}
		if (_status == false) {
			$('.alert-content').text('Please Fill All The Data To Continue');
			$('.alert-wrapper').removeClass('hide');
			$(_focus).focus();
			e.preventDefault();
		} else {

			if(user_details_confirmed == 0 && module_name != '' && module_name != undefined && module_name != null){

				build_confirm_modal(module_name); 
				return false;
			}
			
			//console.log('success');
			/*continue_book_button.submit();
			continue_book_button.attr('disabled', true);
			$('<button class="btn btn-success" disabled="disabled"> Processing....</button>').insertAfter(continue_book_button);
			continue_book_button.hide();
			$('.alert-wrapper').addClass('hide');
			$('.alert-content').text('');*/
		}
	});
	//Tobin
	$('.review_tab_marker').click(function(){
		if($(this).hasClass('review_tab_marker')) {
			$('.rondsts').removeClass('active');
			$(this).parent('.rondsts').addClass('active');
			$('.bktab2, .bktab3').fadeOut(500,function(){$('.bktab1').fadeIn(500)});
		}
	});
	//Tobin
	$('.travellers_tab_marker').click(function(){
		$(this).parent('.rondsts').addClass('active');
		$('#stepbk1').parent('.rondsts').addClass('success');
		$('.bktab1, .bktab3').fadeOut(500,function(){$('.bktab2').fadeIn(500)});
	});
	//Tobin
	$('#alreadyacnt').click(function(){
		show_alert_content('');
		if($(this).prop("checked") == true){
			$('.cntgust').fadeOut(500,function(){$('.alrdyacnt').fadeIn(500)});
		}

		else if($(this).prop("checked") == false){
			$('.alrdyacnt').fadeOut(500,function(){$('.cntgust').fadeIn(500)});
		}
	});
	//User Login - Jaganath
	$('#continue_as_user').click(function(){
		show_alert_content('');
		var username = $('#booking_user_name').val().trim();
		var password = $('#booking_user_password').val().trim();
		if(username !='' && password !='') {
			var login_data = {'username':username, 'password':password};
			$('#book_login_auth_loading_image').show();
			$.post(app_base_url+'index.php/auth/login', login_data, function(response){
				$('#book_login_auth_loading_image').hide();
				if(response['status'] == true) {
					location.reload();
				} else {
					show_alert_content(response['data']);
				}
			});
		}
	});
	//Add Guest User Data - Jaganath
	$('#continue_as_guest').click(function(){
		var username = $('#booking_user_name').val().trim();
		var mobile_number = $('#booking_user_mobile').val().trim();
		var count = 0;
		$('._guest_validate').each( function () {
			if(this.value.trim() == '') {
	           count++;
	           $(this).addClass('invalid-ip');
			}
		});
		if(username!='') {
			$('#booking_user_name').addClass('invalid-ip').attr('placeholder', 'Invalid Email ID');
			count++;
		}
		if(mobile_number!='') {
			$('#booking_user_mobile').addClass('invalid-ip').attr('placeholder', 'Invalid Mobile Number');
			count++;
		}
		var count =0;

		if(count == 0) {
			var login_data = {'username':username, 'mobile_number':mobile_number};
			$.post(app_base_url+'index.php/auth/register_guest_user', login_data, function(response){
				if(response['status'] == true) {
					$('#billing-email').val(username);
					$('#passenger-contact').val(mobile_number);
					show_travellers_tab();
				}
			});
		}
	});
	//Guest User Data Validation
	//validation
	$('._guest_validate').focus( function () {
		$(this).removeClass('invalid-ip');
	});
	$('._guest_validate').blur( function () {
		if(this.value.trim() == '')
		$(this).addClass('invalid-ip');
	});
	$('.name_title').change(function(e){
		var name_title = $(this).val().trim();
		var gender = get_gender(name_title);
		$(this).closest('div._passenger_hiiden_inputs').find('.hidden_pax_details').find('.pax_gender').val(gender);
	});
	//Jaganath
	//After Continue as a guest, hide review tab and show travellers tab
	function show_travellers_tab()
	{
		$('.core_travellers_tab').removeClass('inactive_travellers_tab_marker').addClass('travellers_tab_marker');
		$('.travellers_tab_marker').parent('.rondsts').addClass('active');
		$('#stepbk1').parent('.rondsts').addClass('success');
		$('.core_review_tab').parent('.rondsts').removeClass('active');
		$('.core_review_tab').removeClass('review_tab_marker').addClass('inactive_review_tab_marker');//Inactive Review Tab
		$('.bktab1, .bktab3').fadeOut(500,function(){$('.bktab2').fadeIn(500)});
	}
	$('._numeric_only').on('keydown focus blur keyup change cut copy paste', function (e) {
		isNumber(e, e.keyCode, e.ctrlKey, e.metaKey, e.shiftKey);
	});
	//Jaganath
	//Shows an error Message for User Login
	function show_alert_content(content, container)
	{
		if(typeof(container) == 'undefined') {
			container = '.alert-danger';
		}
		$(container).text(content);
		if (content.length > 0) {
			$(container).removeClass('hide');
		} else {
			$(container).addClass('hide');
		}
	}
	//Jaganath
	//Returns Gender Based on Pax Title
	function get_gender(name_title)
	{
		var gender = 1;
		if(name_title !='') {
			name_title = parseInt(name_title);
			var male_titles = [1];
			var female_titles = [2,3,5];
			if($.inArray(name_title, male_titles) != -1) {
				gender = 1;
			} else if($.inArray(name_title, female_titles) != -1) {
				gender = 2;
			}
		}
		return gender;
	}
	function validate_mobile(number){
		return /^[1-9][0-9]{9}$/.test(number);
	}

	/*
		builds passenger confirmation popup content
	*/
	function build_confirm_modal(module_name){

		var popup_html = '';
		if(module_name == 'bus'){
			popup_html =  build_bus_modal_content();
		}else if(module_name == 'flight'){
			popup_html =  build_flight_modal_content();
		}
		$('#passenger-confirm-header').text('Confirm Passenger Details');
		$('#passenger-confirm-body').html(popup_html);
		var column_count = hide_empty_columns($('#passenger-confirm-table'));
		if(column_count < 2){
			$('#passenger-confirm-modal div:first').removeClass('modal-lg large-details').addClass('modal-sm small-details');
		}else if(column_count == 2 || column_count == 3){
			$('#passenger-confirm-modal div:first').removeClass('modal-lg large-details').addClass('modal-md medium-details');
		}
		$('#passenger-confirm-modal').modal('show');

	}

	/*
		builds passenger confirmation popup content for bus
	*/
	function build_flight_modal_content(){

		var popup_html = '';
		var passenger_count = 1;

		popup_html += '<div class="table-responsive"> ';
			popup_html += '<table class="table" id="passenger-confirm-table">';
				popup_html += '<thead>';
					popup_html += '<tr>';
						popup_html += '<th>Passenger Name</th>';
						popup_html += '<th>Date of Birth</th>';
						popup_html += '<th>Passport Number</th>';
						popup_html += '<th>Issuing Country</th>';
						popup_html += '<th>Date of Expiry</th>';
					popup_html += '</tr>';
				popup_html += '</thead>';
				popup_html += '<tbody>';

				$('#pre-booking-form').find('.pasngrinput').each(function(){

					popup_html += '<tr>';

						popup_html += '<td>';
							popup_html += $(this).find('.name_title option:selected').text()+'. '+$(this).find('#passenger-first-name-'+passenger_count).val()+' '+$(this).find('#passenger-last-name-'+passenger_count).val();
						popup_html += '</td>';

						if($(this).find('#infant-date-picker-'+passenger_count).is(':visible')){

							popup_html += '<td>';
								popup_html += $(this).find('#infant-date-picker-'+passenger_count).val();
							popup_html += '</td>';

						}else{
							popup_html += '<td>';
							popup_html += '</td>';
						}

						popup_html += '<td>';
						if($(this).find('#passenger_passport_number_'+passenger_count).is(':visible')){
							popup_html += $(this).find('#passenger_passport_number_'+passenger_count).val();
						}else{
							popup_html += '';
						}
						popup_html += '</td>';

						popup_html += '<td>';
						if($(this).find('#passenger_passport_issuing_country_'+passenger_count).is(':visible')){
							popup_html += $(this).find('#passenger_passport_issuing_country_'+passenger_count+' option:selected').text();
						}else{
							popup_html += '';
						}
						popup_html += '</td>';

						popup_html += '<td>';
							if($(this).find('#passenger_passport_expiry_day_'+passenger_count).is(':visible')){
								popup_html += $(this).find('#passenger_passport_expiry_day_'+passenger_count+' option:selected').text()+' '+$(this).find('#passenger_passport_expiry_month_'+passenger_count+' option:selected').text()+' '+$(this).find('#passenger_passport_expiry_year_'+passenger_count+' option:selected').text();
							}else{
								popup_html += '';
							}
						popup_html += '</td>';

					popup_html += '</tr>';

					passenger_count++;
				});


				popup_html += '</tbody>';	
			popup_html += '</table">';		
		popup_html += '</div">';

		return popup_html;
	}


	// function to hide empty column header and body if all column data is empty
	function hide_empty_columns(table) {
		var column_count = 0;
	    table.each(function(a, tbl) {
	        $(tbl).find('th').each(function(i) {
	        	column_count++;
	            var remove = true;
	            var currentTable = $(this).parents('table');
	            var tds = currentTable.find('tr td:nth-child(' + (i + 1) + ')');
	            tds.each(function(j) { if (this.innerHTML != '') remove = false; });
	            if (remove) {
	            	column_count--;
	                $(this).hide();
	                tds.hide();
	            }
	        });
	    });

	    return column_count;
	}	

	/*
		builds passenger confirmation popup content for bus
	*/

	function build_bus_modal_content(){

		var popup_html = '';

		popup_html += '<div class="table-responsive"> ';
			popup_html += '<table class="table" id="passenger-confirm-table">';
				popup_html += '<thead>';
					popup_html += '<tr>';
						popup_html += '<th>Seat Details</th>';
						popup_html += '<th>Passenger Name</th>';
						popup_html += '<th>Age</th>';
					popup_html += '</tr>';
				popup_html += '</thead>';
				popup_html += '<tbody>';

		$('#pre-booking-form').find('.pasngrinput_secnrews').each(function(){
					popup_html += '<tr>';

						popup_html += '<td>';
							popup_html += $(this).find('.seat_number').children().html();
						popup_html += '</td>';

						popup_html += '<td>';
							popup_html += $(this).find('.name_title option:selected').text()+'. '+$(this).find('#contact-name').val();
						popup_html += '</td>';

						popup_html += '<td>';
							popup_html += $(this).find('.age').val();
						popup_html += '</td>';

					popup_html += '</tr>';

		});

				popup_html += '</tbody>';	
			popup_html += '</table">';		
		popup_html += '</div">';

		return popup_html;
	}
	//Show Booking Not Allowed ALert
	//$('body').append(booking_not_allowed_alert());
	//$('#booking_not_allowed_modal').modal('show');
	function booking_not_allowed_alert()
	{
		var modal_content = '';
		modal_content += '<div class="modal fade" id="booking_not_allowed_modal" tabindex="-1" role="dialog">';
		modal_content += '<div class="modal-dialog" role="document">';
		modal_content += '<div class="modal-content">';
		modal_content += '<div class="modal-header">';
		modal_content += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
		modal_content += '<h3 class="modal-title">Booking Not Allowed !!</h3>';
		modal_content += '</div>';
		modal_content += '<div class="modal-body">';
		modal_content += '<h4 class="text-danger">Dear customer, booking is not allowed, this is a demo site !!!!</h4>';
		modal_content += '</div>';
		modal_content += '<div class="modal-footer">';
		modal_content += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
		modal_content += '</div>';
		modal_content += '</div>';
		modal_content += '</div>';
		modal_content += '</div>';
		return modal_content;
	}
});
