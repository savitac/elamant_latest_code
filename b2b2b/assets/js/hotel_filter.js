 
$(document).on("click",".filter_airline",function() {
  filter();
});

//~ $(document).on("keyup","#hotel_namefilter",function() {
  //~ filter();
//~ });

var typingTimer;
var doneTypingInterval = 1500;
var $input = $('#hotel_namefilter');
$input.on('keyup',function () {
	clearTimeout(typingTimer);
	typingTimer = setTimeout(doneTyping, doneTypingInterval);
});
$input.on('keydown', function () {
	clearTimeout(typingTimer);
});
function doneTyping () {
	filter();
}

/*$(".sorta").click(function(){

    var val = $(this).attr('val');
    $(".sorta").each(function(){

      if ($(this).attr('class').match(/active|des/)) 
      {
        $(this).removeClass("active des");
      } 
      if ($(this).attr('class').match(/active|ase/)) 
      {
        $(this).removeClass("active ase");
      } 

    });
    //$(this).addClass('active des');
    if( val == 'desc')
    {
		$(this).addClass('active des');
        $(this).attr('val' ,'asc');
    }
    else
    {
		$(this).addClass('active ase');
      $(this).attr('val',"desc");
    }
    filter();

  });*/

	$("#sorting").change(function(){

	   /* var val = $(this).val();
		var option = $('option:selected', this).attr('type');
		$(this).attr('val' ,'asc');*/
		filter();
	});
	$(".sort_wrap").click(function() {
		filter($(this));
	});

$(".filter_depart_btn").mouseup(function(){
  if($(this).attr('type') == '1_star')
  {
     if ($('#1_starrat').is(':checked')) 
     {
      $('#1_starrat').prop('checked', false);
     }
     else
     {
      $('#1_starrat').prop('checked', true);
     }
  }

if($(this).attr('type') == '2_star')
  { 
     if ($('#2_starrat').is(':checked')) 
     { 
      $('#2_starrat').prop('checked', false);

     }
     else
     {
      $('#2_starrat').prop('checked', true);
     }
  }
  if($(this).attr('type') == '3_star')
  {
     if ($('#3_starrat').is(':checked')) 
     {
      $('#3_starrat').prop('checked', false);

     }
     else
     {
      $('#3_starrat').prop('checked', true);
     }
  }

  if($(this).attr('type') == '4_star')
  {
     if ($('#4_starrat').is(':checked')) 
     {
      $('#4_starrat').prop('checked', false);

     }
     else
     {
      $('#4_starrat').prop('checked', true);
     }
  }
  if($(this).attr('type') == '5_star')
  {
     if ($('#5_starrat').is(':checked')) 
     {
      $('#5_starrat').prop('checked', false);

     }
     else
     {
      $('#5_starrat').prop('checked', true);
     }
  }
    filter();
});
$(".filter_depart_btn_new").mouseup(function(){

  if($(this).attr('type') == '1_star')
  {
    $('#1_starrat').trigger('click');
  }

if($(this).attr('type') == '2_star')
  { 
    $('#2_starrat').trigger('click');
  }
  if($(this).attr('type') == '3_star')
  {
     $('#3_starrat').trigger('click');
  }

  if($(this).attr('type') == '4_star')
  {
     $('#4_starrat').trigger('click');
  }
  if($(this).attr('type') == '5_star')
  {
    $('#5_starrat').trigger('click');
  }
    filter();
});
$(document).on("click",".filter_accommodation",function() {
  filter();
});
$(document).on("click",".filter_ammenity",function() {
  filter();
});

function filter(obj)
{
	var sorting_type ;
	var sorting_value;
	if(obj == null) {
		sorting_value = $("#last_sorting_history").attr('data-value');
		sorting_type = $("#last_sorting_history").attr('data-type');
	}
	else {
		if($(obj).attr('data-value') =='asc') {
			sorting_value = 'desc';
			$(".sort_wrap a").removeClass('active');
			$(obj).find('a.sorta').removeClass('ase').addClass('active des');
			$(".sort_wrap").removeClass('highlight');
			$(obj).addClass('highlight');
		}
		else {
			sorting_value = 'asc';
			$(".sort_wrap a").removeClass('active');
			$(obj).find('a.sorta').removeClass('des').addClass('active ase');
			$(".sort_wrap").removeClass('highlight');
			$(obj).addClass('highlight');
		}
		sorting_type = $(obj).attr('data-type');
		$(obj).attr('data-value',sorting_value);
		
		$(obj).find('a.sorta').attr('data-value',sorting_value);
		
		$("#last_sorting_history").attr('data-type',sorting_type);
		$("#last_sorting_history").attr('data-value',sorting_value);
	}

    var data = {};
    var sort = {};
    var facilities = [];
    var star = [];
    var session_id  = $("#session_id").val();
    var requeststring = $("#request_string").val();
    var api_id = $("#api_id").val();
    console.log($("#amount").val());
    data['amount'] = $("#amount").val();
    data['hotel_name'] = $("#hotel_namefilter").val();
    sort['column'] = sorting_type;
    sort['value'] = sorting_value;
    data['sort'] = sort;
    //data['request'] = requeststring;
    //data['api_id'] = api_id;
    
    var matches = [];
    $(".filter_accommodation:checked").each(function() {
        matches.push(this.value);
    });
    data['accommodation'] = matches;


    $(".filter_ammenity:checked").each(function() {
        facilities.push(this.value);
    });

    data['facility'] = facilities;

   $(".filter_depart:checked").each(function() { 
        star.push(this.value);
    });

    data['starrate'] = star;

    $.ajax({
		dataType: 'json',
		type:'POST', 
		url: WEB_URL+'hotel/ajaxPaginationData/'+session_id+'/'+requeststring+'/'+api_id,
		data: { filter: JSON.stringify(data) },
		beforeSend: function(XMLHttpRequest){
			//~ $('.imgLoader').fadeIn();
			var load_container = '<div class="loadcenter"><img src="../assets/images/loader.gif" alt=""/><span class="filter_text" >Please wait..</span></div>';
			$('.hotels_results').html(load_container);
		}, 
		success: function(response) { 
			get_start_counts(response);
			$('.hotels_results').html(response.hotel_search_result);
			
			$('#total_result_count').html(response.total_result);
			
			
			
			var myObj = [];
			$(".mapcnticon").each(function() {
				myObj.push(JSON.parse($(this).attr('data-marker')));
			});
			var geometry = {"latitude":25.2048493,"longitude":55.2707828,"status":"OK"};
			loadMapMarkers(myObj,geometry); 
			//~ $('.imgLoader').fadeOut();
		},
		async: true
    });
} 

$(document).on("click",".pricedates",function() {
  var sorting_type ;
   var sorting_value;
   $(".sorta").each(function(){
      if ($(this).attr('class').match(/active|des/)) 
      {
          sorting_type = $(this).attr('type');
          sorting_value = $(this).attr('val');
      } 
    });

    var data = {};
    var sort = {};
    var stops = [];
    var depart_time = [];
    var return_time = [];
    var session_id  = $("#session_id").val();

    data['amount'] = $("#amount").val();
    sort['column'] = sorting_type;
    sort['value'] = sorting_value;
    data['sort'] = sort;
    
    var matches = [];
    if($(this).attr('id') != 'all'){
        matches.push($(this).attr('id'));
    };
    data['airline'] = matches;


    $(".filter_stop:checked").each(function() {
        stops.push(this.value);
    });

    data['stops'] = stops;

   $(".filter_depart:checked").each(function() {
        depart_time.push(this.value);
    });

    data['depart_time'] = depart_time;

    $(".filter_return:checked").each(function() {
        return_time.push(this.value);
    });

    data['return_time'] = return_time;

    $.ajax({
    type:'POST', 
    url: WEB_URL+'hotel/ajaxPaginationData/'+session_id,
    data: { filter: JSON.stringify(data) },
    beforeSend: function(XMLHttpRequest){
      $('.flight_fliter_loader').fadeIn();
      }, 
      success: function(response) {
      $('.hotels_results').html(response);
       $('.flight_fliter_loader').fadeOut();
      }
    });
});


