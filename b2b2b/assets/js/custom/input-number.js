$(document).ready(function(){		
    //reCall();

    //Hotel Room validation starts
    var roomLength;
    $('.HroomP').click(function (event) {
        event.preventDefault();
        roomLength = $( "div.roomss" ).length;
        var co = roomLength+1;
        if(co <= 3){}else{return false;}
        if(co < 3){}else{$('div.AddRm').fadeOut();}
        var room = '<div class="roomss repeatedroom Aroom'+co+'" data-roomid="'+co+'">'+                        
                        '<div class="col-lg-2 col-md-4 col-xs-4 maronly hafwdth haftofull">'+
                            '<span class="formlabel nonelabel">&nbsp;</span>'+
                            '<div class="roomnum">'+
                                '<span class="numroom">Room '+co+'</span>'+
                            '</div>'+
                        '</div>'+                                                        
                        '<div class="col-lg-2 col-md-4 col-xs-4 maronly hafwdth">'+
                            '<span class="formlabel">Adult<strong>(12+ yrs)</strong></span>'+
                            '<div class="input-group countmore">'+
                                '<span class="input-group-btn">'+
                                    '<button type="button" class="btn btn-default btn-number btnpot" disabled="disabled" data-type="minus" data-field="adult['+roomLength+']">'+
                                        '<span class="fa fa-minus"></span>'+
                                    '</button>'+
                                '</span>'+
                                '<input type="text" name="adult[]" class="form-control input-number centertext" value="1" min="1" max="3" data-name="adult['+roomLength+']" readonly required>'+
                                '<span class="input-group-btn">'+
                                    '<button type="button" class="btn btn-default btn-number btnpot" data-type="plus" data-field="adult['+roomLength+']">'+
                                        '<span class="fa fa-plus"></span>'+
                                    '</button>'+
                                '</span>'+
                            '</div>'+
                        '</div>'+                                                        
                        '<div class="col-lg-2 col-md-4 col-xs-4 maronly hafwdth">'+
                            '<span class="formlabel">Children<strong>(2-12 yrs)</strong></span>'+
                            '<div class="input-group countmore">'+
                                '<span class="input-group-btn">'+
                                    '<button type="button" class="btn btn-default btn-number btnpot ChildAgeRemove" disabled="disabled" data-child="child" data-type="minus" data-field="child['+roomLength+']">'+
                                        '<span class="fa fa-minus"></span>'+
                                    '</button>'+
                                '</span>'+
                                '<input type="text" name="child[]" class="form-control input-number centertext" value="0" min="0" max="2" data-name="child['+roomLength+']" readonly required>'+
                                '<span class="input-group-btn">'+
                                    '<button type="button" class="btn btn-default btn-number btnpot AdChild" data-child="child" data-type="plus" data-field="child['+roomLength+']">'+
                                        '<span class="fa fa-plus"></span>'+
                                    '</button>'+
                                '</span>'+
                            '</div>'+
                        '</div>'+                                                                                    
                        '<div class="col-lg-6 col-md-12 col-xs-12  fivefull nopad">'+
                            '<div class="ChildAges"></div>'+                                                                                    
                            '<div class="colchkcst2 maronly">'+
                                '<span class="formlabel nonelabel">&nbsp;</span>'+
                                '<div class="clss HroomRemove">'+
                                    'Remove'+
                                '</div>'+
                            '</div>'+                                    
                        '</div>'+
                    '</div>';
        if(co <= 3){
          $('.addedRooms').append(room).fadeIn(); // end append  
        }else{
          $('div.AddRm').fadeOut();
          return false;
        }
        $('.HroomRemove').on('click', function (e) {
          e.preventDefault(); 
          $(this).closest('div.repeatedroom').remove().fadeOut('slow'); // remove the textbox
          //$(this).next().remove (); // remove the <br>
          //$(this).remove(); // remove the button
          roomLength = $( "div.roomss" ).length;
          co = roomLength+1;
            if(co <= 4){
              $('div.AddRm').fadeIn();
            }else{
              $('div.AddRm').fadeOut();
              return false;
            }
            reArrangeNames();
        });
        //reCall();
        reArrangeNames();
    });    
});

var childAgeLength;
$(document).on('click', '.AdChild', function(e){
    //childAgeLength = $(this).parents('.repeatedroom').closest( "div.cAges" ).length;
    fieldName = $(this).attr('data-field');
    var input = $("input[data-name='"+fieldName+"']");
    var childAgeLength = parseInt(input.val());
    var roomid = $(this).closest('.repeatedroom').data('roomid');
   
    var ca = childAgeLength+1;
    var co = childAgeLength;
    if(co <= 2){}else{return false;}
    var Age = '<div class="colchkcst cldwdt maronly cAges ChildA'+roomid+ca+'">'+
                    '<span class="formlabel">Children age</span>'+
                    '<div class="input-group countmore">'+
                        '<span class="input-group-btn">'+
                            '<button type="button" class="btn btn-default btn-number btnpot" disabled="disabled" data-type="minus" data-field="childAge_'+roomid+'['+co+']">'+
                                '<span class="fa fa-minus"></span>'+
                            '</button>'+
                        '</span>'+
                        '<input type="text" name="childAge_'+roomid+'[]" class="form-control input-number centertext cag" value="1" min="1" max="12" data-name="childAge_'+roomid+'['+co+']" readonly required>'+
                        '<span class="input-group-btn">'+
                            '<button type="button" class="btn btn-default btn-number btnpot" data-type="plus" data-field="childAge_'+roomid+'['+co+']">'+
                                '<span class="fa fa-plus"></span>'+
                            '</button>'+
                        '</span>'+
                    '</div>'+
                '</div>';
    if(co <= 2){
      $(this).parents('.repeatedroom').find('.ChildAges').append(Age).fadeIn(); // end append  
    }else{
      return false;
    }

    
});
$('.ChildAgeRemove').on('click', function (e) {
      e.preventDefault();
      var roomid = $(this).closest('.repeatedroom').data('roomid');
      fieldName = $(this).attr('data-field');
      
      var input = $("input[data-name='"+fieldName+"']");
     // console.log(input);
      
      var AgecurrentVal = parseInt(input.val()); 
      $("div.ChildA"+roomid+AgecurrentVal+"").remove().fadeOut(); // remove the textbox
      //alert(AgecurrentVal-1);
      //input.val(AgecurrentVal-1);
      //$(this).val(AgecurrentVal-1);
      //$(this).next().remove (); // remove the <br>
      //$(this).remove(); // remove the button      
    });
function reArrangeNames(){
    var current = 1;
    $(".repeatedroom").each(function() {
        $(this).attr('data-roomid', current); 
        $(this).find('.numroom').text('Room '+current);
        $(this).find('.cag').each(function() {
             $(this).attr('name', 'childAge_'+current+'[]');
        });
        current++;
    });
    $('#Rooms').val(current-1);
}

$(document).on('click', '.btn-number', function(e){
	
//$('.btn-number').click(function(e){
    e.preventDefault();    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[data-name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
        


    if (!isNaN(currentVal)) {
        if(type == 'minus') {            
            if(currentVal > input.attr('min')) {
                if(fieldName == 'adult' || fieldName == 'child'){
                    //flight
                    var max = 9;
                    var chd = parseInt($('#child').val()); 
                    var adt = parseInt($('#adult').val());
                    var inf = parseInt($('#infant').val());
                    var persons = adt + chd;
                    
                    if(persons < max){
                        input.val(currentVal - 1).change();
                    }

                    if(fieldName == 'adult'){
                        //console.log(inf +'>='+ adt);
                        if(inf >= adt){
                            $('#infant').val(adt-1).change();
                        } 
                    }
                                        
                }if(fieldName == 'infant'){
                    var adt = parseInt($('#adult').val());
                    var inf = parseInt($('#infant').val());
                    if(inf <= adt){
                        input.val(currentVal - 1).change();
                    }                    
                }else{
                    input.val(currentVal - 1).change();
                } 
                //input.val(currentVal - 1).change();               
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }
        } else if(type == 'plus') {
            if(currentVal < input.attr('max')) {
                if(fieldName == 'adult' || fieldName == 'child'){
                    //flight
                    var max = 9;
                    var chd = parseInt($('#child').val());
                    var adt = parseInt($('#adult').val());
                    var persons = adt + chd;
                    if(persons < max){
                        //console.log(persons +'<'+ max);
                        input.val(currentVal + 1).change();
                    }                    
                }else if(fieldName == 'infant'){
                    var adt = parseInt($('#adult').val());
                    var inf = parseInt($('#infant').val());
                    if(inf < adt){
                        input.val(currentVal + 1).change();
                    }
                }else{
                    input.val(currentVal + 1).change();
                }                                
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }
        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$(document).on('change', '.input-number', function(e){
//$('.input-number').change(function() {    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('data-name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }        
});

$(".input-number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) || 
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)){
        e.preventDefault();
    }
});

$('.HroomRemove').on('click', function (e) {
  e.preventDefault(); 
  $(this).closest('div.repeatedroom').remove().fadeOut('slow'); // remove the textbox
  //$(this).next().remove (); // remove the <br>
  //$(this).remove(); // remove the button
  roomLength = $( "div.roomss" ).length;
  co = roomLength+1;
    if(co <= 4){
      $('div.AddRm').fadeIn();
    }else{
      $('div.AddRm').fadeOut();
      return false;
    }
    reArrangeNames();
});
