
	var windowidth = $(window).width();
	if(windowidth > 768){
		var NUMBER_OF_MONTH = 2
	}else{
		var NUMBER_OF_MONTH = 1 ;
	}
	
$("#t_st_date").datepicker({
	numberOfMonths: 1,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: 1,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate()); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#en_date").datepicker("setDate", d1);
		$("#en_date").datepicker("option", "minDate", d1);
	},
	onClose: function( selectedDate ) {
		$( "#en_date" ).datepicker( "option", "minDate", selectedDate );
		$( '#en_date' ).focus();
	}
});

$("#st_date").datepicker({
	numberOfMonths: 1,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	numberOfMonths: NUMBER_OF_MONTH,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate()); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#en_date").datepicker("setDate", d1);
		$("#en_date").datepicker("option", "minDate", d1);
	},
	onClose: function( selectedDate ) {
		$( "#en_date" ).datepicker( "option", "minDate", selectedDate );
		$( '#en_date' ).focus();
	}
});


$("#en_date").datepicker({
	numberOfMonths: 2,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: NUMBER_OF_MONTH,
	onClose: function( selectedDate ) {
		$( "#st_date" ).datepicker( "option", "maxDate", selectedDate );
		
	}
	
});

$( "#hotel_checkin" ).datepicker({
	numberOfMonths: 2,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: NUMBER_OF_MONTH,
	onSelect: function(dateStr) {
		var d1 = $(this).datepicker("getDate");
		d1.setDate(d1.getDate() + 1); // change to + 1 if necessary
		var d2 = $(this).datepicker("getDate");
		d2.setDate(d2.getDate() + 30); // change to + 29 if necessary
		$("#hotel_checkout").datepicker("setDate", d1);
		$("#hotel_checkout").datepicker("option", "minDate", d1);
		$("#to").datepicker("option", "maxDate", d2);
		updateNightText();
	},
	onClose: function( selectedDate ) {
		$( "#hotel_checkout" ).datepicker( "option", "minDate", selectedDate );
		$( '#hotel_checkout' ).focus();
	}
});


$( "#hotel_checkout" ).datepicker({
	numberOfMonths: 2,
	minDate: 0,
	dateFormat: 'dd-mm-yy',
	maxDate: "+1y",
	numberOfMonths: NUMBER_OF_MONTH,
	onClose: function( selectedDate ) {
		$( "#hotel_checkin" ).datepicker( "option", "maxDate", selectedDate );
		updateNightText();
	}
});
$("#hotel_checkin").datepicker("setDate",check_in);
$("#hotel_checkout").datepicker("setDate",check_out);
//check_in.setDate(check_in.getDate() + 1); // change to + 1 if necessary
$("#hotel_checkout").datepicker("option", "minDate", check_out);
function selectNights(c, b) {
	var a = $(b).val();
	if (a == "30+") {
		a = 31;
		$(".night_number").text("31+")
	} else {
		$(".night_number").text(a)
	}
	$(".nights-dropdown-menu").hide();
	var d = $("#hotel_checkin").datepicker("getDate");
	d.setDate(d.getDate() + parseInt(a));
	$("#hotel_checkout").datepicker("setDate", d);
	//updateDateTexts1($("#hotel_checkout"));
	if (c.stopPropagation) {
		c.stopPropagation()
	} else {
		c.cancelBubble = true
	}
	$("[tabindex=5]").focus()
}
function updateNightText() {
	var a = $('#hotel_checkin').datepicker("getDate");
	var b = $('#hotel_checkout').datepicker("getDate");
	var c = Math.ceil((b.getTime() - a.getTime()) / (1000 * 60 * 60 * 24));
	if (c > 30) {
		c = 31;
	}
	//$("#nights").val(c);
	$('#nights option[value="'+c+'"]').attr('selected', 'selected');
	// $('[name=nights] option').filter(function() { 
	//     return ($(this).val() == c); //To select Blue
	// }).prop('selected', true);
}

//Hotel Autocomplete starts

$.widget( "custom.catcomplete", $.ui.autocomplete, {
    _create: function() {
      this._super();
      this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
    },
    // _resizeMenu: function() {
    //      this.menu.element.outerWidth(470).outerHeight(300);
    // },
    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) {
        var li;
        if ( item.category != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category "+item.category+"'>" + item.category + "</li>" );
          currentCategory = item.category;
        }
        li = that._renderItemData( ul, item );
        if ( item.category ) {
          li.attr( "aria-label", item.category + " : " + item.label );
        }
      });
    },
    _renderItem: function( ul, item ) {
    	if(item.category == 'Cities'){
			return $( "<li>" )
			//.addClass(item.category)
			.attr( "data-value", item.value )			
			.append( $( "<a>" ).html('<span class="ctynme">'+item.label + '</span><span class="ctycnt">'+ item.HotelCount +' Hotels</span>') )
			.appendTo( ul );
		}else{
			return $( "<li>" )
			.attr( "data-value", item.value )			
			.append( $( "<a class='htlnnme'>" ).text( item.label ) )
			.appendTo( ul );
		}
	}
});

$(function() {
    var data = [
      { label: "annhhx10", category: "Products" },
      { label: "annk K12", category: "Products" },
      { label: "annttop C13", category: "Products" },
      { label: "anders andersson", category: "People" },
      { label: "andreas andersson", category: "People" },
      { label: "andreas johnson", category: "People" }
    ];
 
    $( "#city" ).catcomplete({
		delay: 0,
		minLength: 3,
		source: WEB_URL+'home/get_hotel_cities',
		select: function(event, ui){
			$('#CityID').val(ui.item.CityId);
			$('#HotelId').val(ui.item.HotelCode);
			$('#SearchType').val(ui.item.category);
		}
    });
});

//Currency Convertor
function ChangeCurrency(that){
  var code = $(that).data('code');
  var icon = $(that).data('icon');
  //$('.currencychange').hide();
  var data = {};
  data['code'] = code;
  data['icon'] = icon;
  
  $.ajax({
      type: 'POST',
      url: WEB_URL + "home/change_currency",
      async: true,
      dataType: 'json',
      data: data,
      success: function(data) {
        location.reload();
      }
  });
}

function dontSubmit (event){
   if (event.keyCode == 13) {
      return false;
   }
}

$(document).on('click','.dshbrdLnk',function() {
  var curUrl = $(this).data('link');
  history.pushState("", "", curUrl);
});

$( "#datepicker3" ).datepicker({
	dateFormat: 'mm/dd/yy',
	maxDate: 0
});

//Flight Starts
jQuery( "#depature" ).datepicker({
  numberOfMonths: 2,
  minDate: 0,
  dateFormat: 'dd-mm-yy',
  numberOfMonths: NUMBER_OF_MONTH,
  maxDate: "+1y",
  onClose: function( selectedDate ) {
    $( "#return" ).datepicker( "option", "minDate", selectedDate );
    //var type = $("input:radio[name=trip_type]").val();
    $( '#return' ).focus();
  }
});
jQuery( "#return" ).datepicker({
  numberOfMonths: 2,
  minDate: 0,
  dateFormat: 'dd-mm-yy',
  maxDate: "+1y",
  numberOfMonths: NUMBER_OF_MONTH,
  onClose: function( selectedDate ) {
    $( "#depature" ).datepicker( "option", "maxDate", selectedDate );
  }
});
jQuery("#fcheckin_m1" ).datepicker({
				numberOfMonths: 1,
				dateFormat: 'dd-mm-yy',			
				minDate: 0,
				firstDay: 1,
				numberOfMonths: NUMBER_OF_MONTH,
				maxDate: "+361D",
				onClose: function( selectedDate ) {
					var date2 = $('#fcheckin_m1').datepicker('getDate');
					date2.setDate(date2.getDate()+361);
					$( "#fcheckin_m2" ).datepicker( "option", "minDate", selectedDate );
					$( "#fcheckin_m2" ).datepicker( "option", "maxDate", date2 );
					//$( "#fcheckin_m2" ).focus();
				  }
			});
jQuery("#fcheckin_m2" ).datepicker({			
				numberOfMonths: 1,
				dateFormat: 'dd-mm-yy',
				minDate: 0,
				numberOfMonths: NUMBER_OF_MONTH,
				firstDay: 1,
				maxDate: "+1Y",			
			});
			
jQuery("#fcheckin_m3" ).datepicker({			
				numberOfMonths: 1,
				dateFormat: 'dd-mm-yy',
				minDate: 0,
				numberOfMonths: NUMBER_OF_MONTH,
				firstDay: 1,
				maxDate: "+1Y",			
			});

jQuery("#fcheckin_m4" ).datepicker({			
				numberOfMonths: 1,
				dateFormat: 'dd-mm-yy',
				minDate: 0,
				numberOfMonths: NUMBER_OF_MONTH,
				firstDay: 1,
				maxDate: "+1Y",			
			});

jQuery("#fcheckin_m5" ).datepicker({			
				numberOfMonths: 1,
				dateFormat: 'dd-mm-yy',
				minDate: 0,
				numberOfMonths: NUMBER_OF_MONTH,
				firstDay: 1,
				maxDate: "+1Y",			
			});
function swap(){
  var from = $('#fromflight').val();
  var destination = $('#departflight').val();
  $('#fromflight').val(destination);
  $('#departflight').val(from);
}

$(function() {
	$(".fromflight").autocomplete({
		source: WEB_URL+"home/get_airports",
		minLength: 2,//search after two characters
		autoFocus: true, // first item will automatically be focused
		select: function(event,ui){
			$(".departflight").focus();
			//$(".flighttoo").focus();
		}
	});
	
	$(".departflight").autocomplete({
		source: WEB_URL+"home/get_airports",
		minLength: 2,//search after two characters
		autoFocus: true, // first item will automatically be focused
		select: function(event,ui){
			$("#depature").focus();
		}
	});
	$(".p-airline").autocomplete({
		source: WEB_URL+"home/get_airways",
		minLength: 2,//search after two characters
		autoFocus: true, // first item will automatically be focused
		select: function(event,ui){
			$(".p-airline").focus();
			//$(".flighttoo").focus();
		}
	});
});

function mail_voucher(that){
	
  //var that = that;
  var pnr = $('#mail_pnr').val();
  var email = $('#mail_email').val();
  var module = $('#mail_module').val();
    
  if(module == 'HOTEL'){
    //var url = WEB_URL + "hotel/hotel_mail_voucher/"+pnr;
    var url = WEB_URL + "support/hotel_mail_voucher/"+pnr;
  } 
  if(module == 'FLIGHT'){
	  
	  var url = WEB_URL + "support/flight_mail_voucher/"+pnr;
	  }
  
  if(validateEmail(email)){
    $('span.chkmail').html("");
    $.ajax({
        type: 'POST',
        url: url,
        data: {'fvemail': email},
        dataType: 'json',
        beforeSend: function(){
          $(that).children('span.loadr').show();
        },
        success: function(data) {
          if(data.status == 1){
            $(that).children('span.loadr').hide();
            $('span.success_msg').show();
          } else if(data.status == 0){
            $(that).children('span.loadr').hide();
            $('span.chkmail').html("Invalid email address!");
          }
        }
    }); 
  } else {
    $('span.chkmail').html("Invalid email address!");
  }
}

function validateEmail(email){
  var emailReg = new RegExp(/^(\s*,?\s*[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})+\s*$/);
  var valid = emailReg.test(email);
  if(!valid) {
    return false;
  } else {
    return true;
  }
}

function hotel_cancel_booking(that){
  var that = that;
  var pnr = $(that).data('pnr');
  
  var res = confirm("Are you sure to cancel the booking?");
  if (res == true) {
    $.ajax({
        type: 'GET',
        url: WEB_URL + "hotel/cancel/"+pnr,
        dataType: 'json',
        beforeSend: function(){
          $(that).children('span.loadr').toggle();
        },
        success: function(data) {
          if(data.status == 1){
            location.reload();
            $(that).children('span.loadr').toggle();
            $(that).parent().parent('tr').find('td.status').text('CANCELLED');
            $(that).remove();
          }else{
            $(that).children('span.loadr').toggle();
            $(that).text('** Cancellation Pending **');
          }
        }
    }); 
  }
}

function flight_cancel_booking(that){
  var that = that;
  var pnr = $(that).data('pnr');
  
  var res = confirm("Are you sure to cancel the booking?");
  if (res == true) {
    $.ajax({
        type: 'GET',
        url: WEB_URL + "flight/cancel/"+pnr,
        dataType: 'json',
        beforeSend: function(){
          $(that).children('span.loadr').toggle();
        },
        success: function(data) {
			//alert(data);
          if(data.status == 1){
            location.reload();
            $(that).children('span.loadr').toggle();
            $(that).parent().parent('tr').find('td.status').text('CANCELLED');
            $(that).remove();
          }else{
            $(that).children('span.loadr').toggle();
            $(that).text('** Cancellation Pending **');
          }
        }
    }); 
  }
}
function airports_latest(id){
	$('#'+id).autocomplete({
		source: WEB_URL+"home/get_airports",
		minLength: 2,//search after two characters
		autoFocus: true, // first item will automatically be focused
		});	
	
	}
var flights=0;
	function add_flights()
		{
			cnt = $("#f_count").val();
			ft_cnt = $("#ft_count").val();
			if (flights !=3)
			{
				id 		= parseInt(cnt);
				art_id 	= parseInt(ft_cnt);
				art_id1 = art_id + 1;		
				id1 	= id + 1; 
				id2 	= id - 1; 
				selectedDateValue = $("#fcheckin_m"+id2).val();
				if(selectedDateValue == '')
				{
					alert("Please choose the Date");
					$("#fcheckin_m"+id2).focus();
					return false;
				}
				$("#rowsFlight").append('<div class="appenddiv"><div class="col-xs-12 nopad mobilestr"><div class="col-xs-5  marginbotom10 marginbotomdty fivefull"><span class="formlabel">Leaving From</span><div class="relativemask"><span class="maskimg mto"></span><input type="text" id="m_from_city_'+id+'" name="m_from_city[]" class="ft" onkeypress="airports_latest(this.id)" placeholder="Type Departure City"/></div></div><div class="col-xs-5  marginbotom10 marginbotomdty fivefull"><span class="formlabel">To</span><div class="relativemask"><span class="maskimg mto"></span><input type="text" id="m_to_city_'+id+'"  name="m_to_city[]" class="ft" onkeypress="airports_latest(this.id)" placeholder="Type Destination City"/></div></div><div class="col-xs-2 fivefull"><span class="formlabel">Departing</span><div class="relativemask"><span class="maskimg caln"></span><input  value="" name="m_sdt[]" id="fcheckin_m'+id+'"  placeholder="Select Date" class="forminput" type="text" onClick="this.select();"  /></div></div></div></div>');
				$(function() {
					$("#fcheckin_m"+id).datepicker({
						numberOfMonths: 1,
						dateFormat: 'dd-mm-yy',
						minDate: $("#fcheckin_m"+id2).datepicker('getDate'),
						firstDay: 1,
						maxDate: '+1Y',
					}).on('changeDate', function(ev) {
						dat_id.hide();
					}).data('datepicker');
				});
				
				flights += 1;
				art_id1 += 1;
				$("#f_count").val(id1);
				$("#ft_count").val(art_id1);
			}
			else{
				$("#rowsFlight1").empty().html('<div class="alertcity"><span class="formlabel">Only 6 Multi Cities allowed.</span></div>');
				alert('Only 6 Multi Cities allowed.');
			}
		}	
		var flights = 0;
		function closeFlights()
		{
			cnt = $("#f_count").val();
			id1 = (cnt - 1); 
			$("#f_count").val(id1);
			
			ft_cnt = $("#ft_count").val();
			art_id 	= parseInt(ft_cnt);
			art_id1 = art_id - 2;
			$("#ft_count").val(art_id1);
			
			$('#rowsFlight .appenddiv').last().remove();
		}
		
		
	$(document).ready(function() {
		
			$('.cancelhtl').click(function(){
				$(".htlmod").slideUp("slow");
			});
			
			$('#modhtl').click(function(){
				$(".htlmod").slideToggle("slow");
			});
        
    });
   

