$(document).ready(function() {
	$('.alpha').keyup(function() {
        var $th = $(this);
        $th.val( $th.val().replace(/[^a-zA-Z\s]/g, function(str) { return ''; } ) );
    });

});

$("#createAccount").validate({
      rules: {
		     password: "password1",
		     
            },
	submitHandler: function(form) { 
		var action = $("#createAccount").attr('action');
		//$("#signup").attr("disabled", "disabled");
		$.ajax({
			type: "POST",
			url: action,
			data: $("#createAccount").serialize(),
			dataType: "json",
			success: function(data){
				if(data.status == 3) {
					//$("#signup").attr("disabled", "");
					$.each(data, function(key, value) {
						
					 $('#'+key).text(value).css('display','block');;
					});
				}
				
				
				if(data.status == 1){
					window.location.href = data.success_url;
				}else{
				    //$("#signup").attr("disabled", "");
				    $.each(data, function(key, value) {
					 $('#'+key).text(value);
					});  	
				}
			}
		}); 
		return false; 
	}  
});

$("#updateProfile").validate({
	
	submitHandler: function(form) { 
		var action = $("#updateProfile").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#updateProfile").serialize(),
			dataType: "json",
			success: function(data){
				
				if(data.status == 3) {
					$.each(data, function(key, value) {
					 $('#'+key).text(value);
					});
				}
				if(data.status == 1){
					window.location.href = data.url;
				}else{
					$('#loginLdrReg_reg').hide();
					$('div.reg_popuperror').html(data.msg);
					$('div.reg_popuperror').show();
				}
			}
		}); 
		return false; 
	}  
});


function form_submits(formName) {
	
	$("#"+formName).validate({
	submitHandler: function(form) {
		var action = $("#"+formName).attr('action');
	
		$.ajax({
			type: "POST",
			url: action,
			data: $("#"+formName).serialize(),
			dataType: "json",
			success: function(data){
				
				if(data.status == 3) {
					$.each(data, function(key, value) {
					 $('#'+key).text(value);
					});
				}
				if(data.status == 1){
					//window.location.href = data.url;
					$("#user")[0].reset();
					$('.user_create_suc').removeClass('hide');
					setTimeout(function(){ $('.user_create_suc').addClass('hide'); }, 10000);
					
				}else{
					$('#loginLdrReg_reg').hide();
					$('div.reg_popuperror').html(data.msg);
					$('div.reg_popuperror').show();
				}
			}
		}); 
		return false; 
	}  
});
}






	
	

$("#changePwd").validate({
	rules: {
		password: "password1",
		cpassword: {
			equalTo: "#password"
		}
	},
	submitHandler: function(form) { 
		var action = $("#changePwd").attr('action');
		
		$.ajax({
			type: "POST",
			url: action,
			data: $("#changePwd").serialize(),
			dataType: "json",
			success: function(data){
				
				if(data.status == 3) {
					$.each(data, function(key, value) {
					 $('#'+key).text(value);
					});
				}
				if(data.status == 1){
					$("#current_password").val('');
					$("#password").val('');
					$("#c_password").val('');
				    alert(data.msg);
				}
			}
		}); 
		return false; 
	}  
});

$("#login").validate({
	submitHandler: function(form) {
		
		p = $('#password').val();
		if(!p) {
			$('.errMsg').empty().append('<label id="pswd-error" class="error" for="pswd">This field is required.</label>');
			return false;
		}
		var action = $("#login").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#login").serialize(),
			dataType: "json",
			success: function(data){
				
				if(data.status == 1){
					
						window.location.href = data.url
				} else if(data.status == 2) {
					var curUrl = document.URL; //encode to base64
					window.location.href = WEB_URL+'/security/verifytwostep?url='+curUrl;
				} else{
					$.each(data, function(key, value) {
					 $('#'+key).text(value);
					});
				}
			}
		});
		return false; 
	}
});

// validation for forgot password
$("#forgetpwd").validate({
	submitHandler: function(form) { 
		var action = $("#forgetpwd").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#forgetpwd").serialize(),
			dataType: "json",
			success: function(data){
				 //alert(data.status);
				if(data.status == 1){
					alert("Please check your email to reset the password");
				}else{
				   $('div.popuperror').html(data.msg);
				   alert("Please Enter Valid Mail-Id");
				}
			}
		});
		
		return false; 
	}
});
		
// validation for Reset password form
$("#resetpwd").validate({
	rules: {
		newpassword: "required",
		confirm_password: {
			equalTo: "#newpassword"
		}
	},
	submitHandler: function(form) { 
		$('#loginLdrReg_resetpwd').show();
		var action = $("#resetpwd").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#resetpwd").serialize(),
			dataType: "json",
			success: function(data){
				if(data.status == 1){
					$('#loginLdrReg_resetpwd').hide();
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
					//setTimeout(function(){$('#fadeandscalechange').popup('hide');}, 1000);
					//doLogin(data);
				}else{
					$('#loginLdrReg_resetpwd').hide();
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
				}
			}
		}); 
		return false; 
	}  
});
			
// validation for profile edit
$("#profile").validate({
	submitHandler: function(form) { 
		$('#loginLdrReg').show();
		var action = $("#profile").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#profile").serialize(),
			dataType: "json",
			success: function(data){
				if(data.status == 1){
					$('#loginLdrReg').hide();
					doLogin(data);
				}else{
					$('#loginLdrReg').hide();
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
				}
			}
		});
		return false; 
	}
});

// validation for passport form
$("#passport_form_profile").validate({
	submitHandler: function(form) { 
		$('#loginLdrReg_passport').show();
		var action = $("#passport_form_profile").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#passport_form_profile").serialize(),
			dataType: "json",
			success: function(data){
				if(data.status == 1){
					$('#loginLdrReg_passport').hide();
					doLogin(data);
				}else{
					$('#loginLdrReg_passport').hide();
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
				}
			}
		});
		form.submit();
		return false; 
	}
});

// validation for visa form
$("#visa_form").validate({
	submitHandler: function(form) { 
		$('#loginLdrReg_visa').show();
		var action = $("#visa_form").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#visa_form").serialize(),
			dataType: "json",
			success: function(data){
				if(data.status == 1){
					$('#loginLdrReg_visa').hide();
					doLogin(data);
				}else{
					$('#loginLdrReg_visa').hide();
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
				}
			}
		});
		form.submit();
		return false; 
	}
});

// validation for frequent traveller form
$("#membership_form").validate({
	submitHandler: function(form) { 
		$('#loginLdrReg_membership').show();
		var action = $("#membership_form").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#membership_form").serialize(),
			dataType: "json",
			success: function(data){
				if(data.status == 1){
					$('#loginLdrReg_membership').hide();
					doLogin(data);
				}else{
					$('#loginLdrReg_membership').hide();
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
				}
			}
		});
		form.submit();
		return false; 
	}
});

// validation for traveller form
$("#traveller_form").validate({
	submitHandler: function(form) { 
		$('#loginLdrReg_traveller').show();
		form.preventDefault();
		var action = $("#traveller_form").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#traveller_form").serialize(),
			dataType: "json",
			success: function(data){
				if(data.status == 1){
					$('#loginLdrReg_traveller').hide();
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
				}else{
					$('#loginLdrReg_traveller').hide();
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
				}
			}
		});
		form.preventDefault();
		form.submit();
		return false; 
	}
});

// validation for change password form
$("#chnagepwd").validate({
	rules: {
		npassword: "required",
		cpassword: {
			equalTo: "#npassword"
		}
	},
	submitHandler: function(form) { 
		$('#loginLdrReg_cpwd').show();
		var action = $("#chnagepwd").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#chnagepwd").serialize(),
			dataType: "json",
			success: function(data){
				if(data.status == 1){
					$('#loginLdrReg_cpwd').hide();
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
					setTimeout(function(){$('#fadeandscalechange').popup('hide');}, 1000);
					//doLogin(data);
				}else{
					$('#loginLdrReg_cpwd').hide();
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
				}
			}
		}); 
		return false; 
	}  
});
$("#login2").validate({
	submitHandler: function() { 
		var action = $("#login2").attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#login2").serialize(),
			dataType: "json",
			success: function(data){
				
				if(data.status == 1){
					$('#fadeandscale').popup('hide');
					window.location.href = data.continue;
				} else if(data.status == 2) {
					var curUrl = document.URL; //encode to base64
					window.location.href = base_url()+'/security/verifytwostep?url='+curUrl;
				} else{
					$('div.popuperror').html(data.msg);
					$('div.popuperror').show();
				}
			}
		});
		return false; 
	}
});

function validateCheckbox(id) {
	if (!jQuery("#"+id).is(":checked")) {
		var response = {
						'status': false,
						'message': 'Before you proceed, please do agree to our terms &amp; conditions.',
					};
	}
	else {
		var response = {
						'status': true,
						'message': '',
					};
	}
	return response;
}
function isEmptyAndValue(val) {
	if(val.trim() == '') {
		var response = {
						'status': false,
						'message': 'This field is required!',
					};
	}
	else if(val <= 0) {
		var response = {
						'status': false,
						'message': 'Deposit amount should be more than zero!',
					};
	}
	else {
		var response = {
						'status': true,
						'message': '',
					};
	}
	return response;
}

$("#checkout-apartment").validate({
	submitHandler: function() {
		var flagStorage = [];
		var response = validateCheckbox("squaredThree1");
		if(response.status == true) {
			if($("#indicator").next().hasClass('error')) {
				$("#indicator").next().remove(".error");
			}
		}
		else {
			if($("#indicator").next().hasClass('error')) {
				$("#indicator").next().remove(".error");
			}
			var error_contents = "<span class='error'>"+response.message+"</span>"
			$("#indicator").after(error_contents);
			flagStorage.push(false);
		}
		
		var action = $("#checkout-apartment").attr('action');
		
		var flag = $("#checkout-apartment").attr('data-flag');
		if(flag == 'COMMISSIONABLE') {
			var payment_type = $('input[name=type_of_payment]:checked').val();
			if(payment_type == 'BOTH') {
				var deposit_payment = $('#commissionable_both_payment').val();
				var response = isEmptyAndValue(deposit_payment);
				if(response.status == true) {
					if($("#commissionable_both_payment").next().hasClass('error')) {
						$("#commissionable_both_payment").next().remove(".error");
					}
				}
				else {
					if($("#commissionable_both_payment").next().hasClass('error')) {
						$("#commissionable_both_payment").next().remove(".error");
					}
					var error_contents = "<span class='error'>"+response.message+"</span>"
					$("#commissionable_both_payment").after(error_contents);
					flagStorage.push(false);
				}
			}
			else {
				var deposit_payment = null;
			}
		}
		else if(flag == 'NET') {
			var payment_type = $('input[name=type_of_payment1]:checked').val();
			if(payment_type == 'BOTH') {
				var deposit_payment = $('#net_both_payment').val();
				var response = isEmptyAndValue(deposit_payment);
				if(response.status == true) {
					if($("#net_both_payment").next().hasClass('error')) {
						$("#net_both_payment").next().remove(".error");
					}
				}
				else {
					if($("#net_both_payment").next().hasClass('error')) {
						$("#net_both_payment").next().remove(".error");
					}
					var error_contents = "<span class='error'>"+response.message+"</span>"
					$("#net_both_payment").after(error_contents);
					flagStorage.push(false);
				}
			}
			else {
				var deposit_payment = null;
			}
		}
		else {
			flagStorage.push(false);
		}
		
		if($.inArray(false,flagStorage) === -1) {}
		else {
			return false;
		}
		
		var contents = '<input type="hidden" name="payment_logic" value="'+flag+'" />'+
						'<input type="hidden" name="payment_type" value="'+payment_type+'" />'+
						'<input type="hidden" name="deposit_payment" value="'+deposit_payment+'" />';
		$("#checkout-apartment").append(contents);
		$('#checkout_continue').attr('disabled','disabled');
		$.ajax({
			type: "POST",
			url: action,
			data: $("#checkout-apartment").serialize(),
			dataType: "json",
			beforeSend: function(){
		        $('.lodrefrentwhole').show();
		        $('.carttoloadr').fadeIn();
		    },
			success: function(data){
				$('.carttoloadr').fadeOut();
				if(data.status == 1){
					setTimeout(function(){ $('div.lodrefrentwhole').hide();}, 2000);
					window.location.href = data.voucher_url; 	  
				}else if(data.status == -1){
					window.location.href = data.signup_login;
				}else if(data.status == -2){
					$('div.lodrefrentwhole').hide();
					//~ inform();
					alert('You have insufficient balance!');
					console.log('There is no enough amount in your account');
				}else if(data.status == 555){
					console.log('Gate');
					window.location.href = data.GateURL;
				}
			}
		});
		return false; 
	}
});
