$(document).ready(function() {
/*Bishnu*/
/*$('.done1').click(function() {
	var child = $('input[name="child[]"]');
	console.log(child);
});*/
	var flight_adult_child_max_count = 9;
	var flight_adult_child_max_count_group = 30;
	$('.advncebtn').click(function() {
		$(this).parent('.togleadvnce').toggleClass('open')
	});
	$('.totlall').click(function() {
		$('.roomcount').toggleClass("fadeinn")
	});
	$('.totlall, .roomcount').click(function(e) {
		e.stopPropagation()
	});
	$('.done1').click(function() {
       $('.roomcount').removeClass("fadeinn")
    });
	$(document).click(function() {
		$('.roomcount').removeClass("fadeinn")
	});
	$('.alladvnce').click(function() {
		$('.advncedown').removeClass("fadeinn");
		$(this).children('.advncedown').toggleClass("fadeinn")
	});
	$('.alladvnce, .advncedown').click(function(e) {
		e.stopPropagation()
	});
	$(document).click(function() {
		$('.advncedown').removeClass("fadeinn")
	});
	$('.btn-number').click(function(e) {
		e.preventDefault();
		fieldName = $(this).attr('data-field');
		type = $(this).attr('data-type');
		var current_pax_count_wrapper = $(this).closest('.pax-count-wrapper');
		var input = $("input[name='" + fieldName + "']", current_pax_count_wrapper);
		var currentVal = parseInt(input.val());
		// console.log(currentVal);
		if (!isNaN(currentVal)) {
			if (type == 'minus') {
				if (currentVal > input.attr('min')) {
					input.val(currentVal - 1).change()
				}
				if (parseInt(input.val()) == input.attr('min')) {}
			} else if (type == 'plus') {
				if (currentVal < input.attr('max')) {
					input.val(currentVal + 1).change()
				}
				if (parseInt(input.val()) == input.attr('max')) {}
			}
		} else {
			input.val(0)
		}
//		if (fieldName == 'infant' || (fieldName == 'adult' && $('[data-field="infant"]').length > 0)) {
//			manage_infant_count(fieldName)
//		}
		
		var myElem = document.getElementById('OWT_infant');
		if (myElem != null) {
			manage_infant_count(fieldName);
		}
		var form_id = $(this).closest('form').attr('id');
		total_pax_count(form_id)
	});
	
	function manage_infant_count(pax_type) 
	{
		var _content = '';
	    var adult_count = parseInt($('#OWT_adult').val().trim());
	    // console.log(adult_count);
	    var child_count = parseInt($('#OWT_child').val().trim());
	    var infant_count = parseInt($('#OWT_infant').val().trim());
	    var total_adult_child_count = (adult_count+child_count);
	    if (pax_type == 'infant' && infant_count > 0) {
	        var temp_infant_count = (infant_count - 1);
	        if (temp_infant_count >= adult_count) {
	            $('#OWT_infant').val(temp_infant_count);
	            $('#OWT_infant').parent('.infant_count_div').find('button[data-type=plus]').attr('disabled', 'disabled');
	            _content = '1 Infant Per Adult Allowed'
	        }
	    }
	    if (pax_type == 'adult') {//Adult
	    		var temp_adult_child_count = (total_adult_child_count	 - 1);
	    		if (temp_adult_child_count >= flight_adult_child_max_count) {
	                $('#OWT_adult').val(adult_count-1);
	                $('#OWT_adult').parent('.adult_count_div').find('button[data-type=plus]').attr('disabled', 'disabled');
	                _content = '<small>Max 9 Passenger(Adult+Child) Allowed</small>'
	            } else {
	            	$('#OWT_adult').parent('.adult_count_div').find('button[data-type=plus]').removeAttr('disabled');
	            	$('#OWT_child').parent('.child_count_div').find('button[data-type=plus]').removeAttr('disabled');
	            }
	    	//Infant
	        if (infant_count > 0 && infant_count > adult_count) {
	            $('#OWT_infant').val(0)
	        }
	        $('#OWT_infant').parent('.infant_count_div').find('button[data-type=plus]').removeAttr('disabled')
	    }else if (pax_type == 'child') {//Child
	    	var temp_adult_child_count = (total_adult_child_count	 - 1);
			if (temp_adult_child_count >= flight_adult_child_max_count) {
	            $('#OWT_child').val(child_count-1);
	            $('#OWT_child').parent('.child_count_div').find('button[data-type=plus]').attr('disabled', 'disabled');
	            _content = '<small>Max 9 Passenger(Adult+Child) Allowed</small>'
	        } else {
	        	$('#OWT_adult').parent('.adult_count_div').find('button[data-type=plus]').removeAttr('disabled');
	        	$('#OWT_child').parent('.child_count_div').find('button[data-type=plus]').removeAttr('disabled');
	        }
	    }
	    show_alert_content(_content)
	}
	/** group booking flight js**/
	$('.btn-number-group').click(function(e) {
		//alert();
		e.preventDefault();
		fieldName = $(this).attr('data-field');
		type = $(this).attr('data-type');
		var current_pax_count_wrapper = $(this).closest('.pax-count-wrapper');
		var input = $("input[name='" + fieldName + "']", current_pax_count_wrapper);
		var currentVal = parseInt(input.val());
		// console.log(currentVal);
		if (!isNaN(currentVal)) {
			if (type == 'minus') {
				if (currentVal > input.attr('min')) {
					input.val(currentVal - 1).change()
				}
				if (parseInt(input.val()) == input.attr('min')) {}
			} else if (type == 'plus') {
				if (currentVal < input.attr('max')) {
					input.val(currentVal + 1).change()
				}
				if (parseInt(input.val()) == input.attr('max')) {}
			}
		} else {
			input.val(0)
		}
//		if (fieldName == 'infant' || (fieldName == 'adult' && $('[data-field="infant"]').length > 0)) {
//			manage_infant_count(fieldName)
//		}
		
		var myElem = document.getElementById('OWT_infant');
		if (myElem != null) {
			manage_infant_count_group(fieldName);
		}
		var form_id = $(this).closest('form').attr('id');
		total_pax_count(form_id)
	});
	$('.btn-number-hotel').click(function(e) {
		//alert();
		e.preventDefault();
		fieldName = $(this).attr('data-field');
		type = $(this).attr('data-type');
		var current_pax_count_wrapper = $(this).closest('.pax-count-wrapper');
		var input = $("input[name='" + fieldName + "']", current_pax_count_wrapper);
		var currentVal = parseInt(input.val());
		// console.log(currentVal);
		if (!isNaN(currentVal)) {
			if (type == 'minus') {
				if (currentVal > input.attr('min')) {
					input.val(currentVal - 1).change()
				}
				if (parseInt(input.val()) == input.attr('min')) {}
			} else if (type == 'plus') {
				if (currentVal < input.attr('max')) {
					input.val(currentVal + 1).change()
				}
				if (parseInt(input.val()) == input.attr('max')) {}
			}
		} else {
			input.val(0)
		}
//		if (fieldName == 'infant' || (fieldName == 'adult' && $('[data-field="infant"]').length > 0)) {
//			manage_infant_count(fieldName)
//		}
		
		var myElem = document.getElementById('OWT_infant');
		if (myElem != null) {
			manage_infant_count_hotel(fieldName);
		}
		var form_id = $(this).closest('form').attr('id');
		total_pax_count_hotel(form_id)
	});
	
	function manage_infant_count_group(pax_type) 
	{
		var _content = '';
	    var adult_count = parseInt($('#OWT_adult').val().trim());
	    // console.log(adult_count);
	    var child_count = parseInt($('#OWT_child').val().trim());
	    var infant_count = parseInt($('#OWT_infant').val().trim());
	    var total_adult_child_count = (adult_count+child_count);
	    if (pax_type == 'infant' && infant_count > 0) {
	        var temp_infant_count = (infant_count - 1);
	        if (temp_infant_count >= adult_count) {
	            $('#OWT_infant').val(temp_infant_count);
	            $('#OWT_infant').parent('.infant_count_div').find('button[data-type=plus]').attr('disabled', 'disabled');
	            _content = '1 Infant Per Adult Allowed'
	        }
	    }
	    if (pax_type == 'adult') {//Adult
	    		var temp_adult_child_count = (total_adult_child_count	 - 1);
	    		if (temp_adult_child_count >= flight_adult_child_max_count_group) {
	                $('#OWT_adult').val(adult_count-1);
	                $('#OWT_adult').parent('.adult_count_div').find('button[data-type=plus]').attr('disabled', 'disabled');
	                _content = '<small>Max 30 Passenger(Adult+Child) Allowed</small>'
	            } else {
	            	$('#OWT_adult').parent('.adult_count_div').find('button[data-type=plus]').removeAttr('disabled');
	            	$('#OWT_child').parent('.child_count_div').find('button[data-type=plus]').removeAttr('disabled');
	            }
	    	//Infant
	        if (infant_count > 0 && infant_count > adult_count) {
	            $('#OWT_infant').val(0)
	        }
	        $('#OWT_infant').parent('.infant_count_div').find('button[data-type=plus]').removeAttr('disabled')
	    }else if (pax_type == 'child') {//Child
	    	var temp_adult_child_count = (total_adult_child_count	 - 1);
			if (temp_adult_child_count >= flight_adult_child_max_count_group) {
	            $('#OWT_child').val(child_count-1);
	            $('#OWT_child').parent('.child_count_div').find('button[data-type=plus]').attr('disabled', 'disabled');
	            _content = '<small>Max 30 Passenger(Adult+Child) Allowed</small>'
	        } else {
	        	$('#OWT_adult').parent('.adult_count_div').find('button[data-type=plus]').removeAttr('disabled');
	        	$('#OWT_child').parent('.child_count_div').find('button[data-type=plus]').removeAttr('disabled');
	        }
	    }
	    show_alert_content(_content)
	}

	function manage_infant_count_hotel(pax_type) 
	{	
	//	alert(pax_type);
		var _content = '';
	    var adult_count = parseInt($('#ht_adult').val().trim());
	    // console.log(adult_count);
	    var child_count = parseInt($('#ht_child').val().trim());
	 
	    var total_adult_child_count = (adult_count+child_count);
	   	
	    if (pax_type == 'adult') {//Adult
	    
	    		var temp_adult_child_count = (total_adult_child_count	 - 1);
	    	
	    		if (temp_adult_child_count >= flight_adult_child_max_count_group) {
	    		
	                $('#ht_adult').val(adult_count-1);
	                $('#ht_adult').parent('.adult_count_div').find('button[data-type=plus]').attr('disabled', 'disabled');
	                _content = '<small>Max 30 Passenger(Adult+Child) Allowed</small>'
	            } else {
	            	$('#ht_adult').parent('.adult_count_div').find('button[data-type=plus]').removeAttr('disabled');
	            	$('#ht_child').parent('.child_count_div').find('button[data-type=plus]').removeAttr('disabled');
	            }
	    	//Infant
	      
	    }else if (pax_type == 'child') {//Child
	    	var temp_adult_child_count = (total_adult_child_count	 - 1);
			if (temp_adult_child_count >= flight_adult_child_max_count_group) {
	            $('#ht_child').val(child_count-1);
	            $('#ht_child').parent('.child_count_div').find('button[data-type=plus]').attr('disabled', 'disabled');
	            _content = '<small>Max 30 Passenger(Adult+Child) Allowed</small>'
	        } else {
	        	$('#ht_adult').parent('.adult_count_div').find('button[data-type=plus]').removeAttr('disabled');
	        	$('#ht_child').parent('.child_count_div').find('button[data-type=plus]').removeAttr('disabled');
	        }
	    }
	    show_alert_content_hotel(_content)
	}
	function show_alert_content(content, container) {
		 if (typeof container == "undefined") {
		        container = '.alert-content'
		    }
		    $(container).html(content);
		    if (content.length > 0) {
		        $('.alert-wrapper').removeClass('hide')
		    } else {
		        $('.alert-wrapper').addClass('hide')
		    }
	}
	function show_alert_content_hotel(content, container) {
		 if (typeof container == "undefined") {
		        container = '.alert-content'
		    }
		    $(container).html(content);
		    if (content.length > 0) {
		        $('.alert-wrapper-hotel').removeClass('hide')
		    } else {
		        $('.alert-wrapper-hotel').addClass('hide')
		    }
	}
	$('.input-number').focusin(function() {
		$(this).data('oldValue', $(this).val())
	});
	$('.input-number').change(function() {
		minValue = parseInt($(this).attr('min'));
		maxValue = parseInt($(this).attr('max'));
		valueCurrent = parseInt($(this).val());
		var current_pax_count_wrapper = $(this).closest('.pax-count-wrapper');
		name = $(this).attr('name');
		if (valueCurrent >= minValue) {
			$(".btn-number[data-type='minus'][data-field='" + name + "']", current_pax_count_wrapper).removeAttr('disabled')
		} else {
			alert('Sorry, the minimum value was reached');
			$(this).val($(this).data('oldValue'))
		}
		if (valueCurrent <= maxValue) {
			$(".btn-number[data-type='plus'][data-field='" + name + "']", current_pax_count_wrapper).removeAttr('disabled')
		} else {
			alert('Sorry, the maximum value was reached');
			$(this).val($(this).data('oldValue'))
		}
	});
	$(".input-number").keydown(function(e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
			return
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault()
		}
	})
});

function total_pax_count(form_id) {
	if (form_id != '') {
		var pax_count = $('input.pax_count_value', 'form#' + form_id + ' div.pax_count_div').map(function() {
			return parseInt(this.value)
		}).get();
		var total_pax_count = 0;
		$.each(pax_count, function() {
			total_pax_count += this
		});
		$('.total_pax_count', 'form#' + form_id).empty().text(total_pax_count)
	}

}
function total_pax_count_hotel(form_id) {
	//alert(form_id);
	if (form_id != '') {
		var pax_count = $('input.pax_count_value_hotel', 'form#' + form_id + ' div.pax_count_div_hotel').map(function() {
			return parseInt(this.value)
		}).get();
		//alert(pax_count);
		var total_pax_count = 0;
		$.each(pax_count, function() {
			total_pax_count += this
		});
	//	alert(total_pax_count);
		$('.total_pax_count', 'form#' + form_id).empty().text(total_pax_count)
	}
}