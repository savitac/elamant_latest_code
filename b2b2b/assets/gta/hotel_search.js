$(document).ready(function() {
	$('.loader-image').show();
	pre_load_audio();
	var _search_id = document.getElementById('pri_search_id').value;
	var _fltr_r_cnt = 0; //Filter Result count
	var _total_r_cnt = 0; //Total Result count
	var _offset = 0; //Offset to load results
	var dynamic_page_data = true;

	var processLazyLoad = function() {
			//check if your div is visible to user
			//CODE ONLY CHECKS VISIBILITY FROM TOP OF THE PAGE
			/*if (_next_page == true && $('#npl_img').get(0) && $('#npl_img').get(0).scrollHeight != 0) {
				//console.log('Lazy loaded flexible');
				if ($(window).scrollTop() + $(window).height() >= $('#npl_img').get(0).scrollHeight) {
					if (!$('#npl_img').attr('loaded')) {
						_next_page == false;
						//not in ajax.success due to multiple sroll events
						$('#npl_img').attr('loaded', true);
						//ajax goes here
						var booking_source = $('#pri_active_source').val();
						var booking_source = jQuery.parseJSON(booking_source);
						load_hotels(process_result_update,booking_source[0], _offset, _ini_fil);
						load_hotels(process_result_update,booking_source[1], _offset, _ini_fil);
					}
				}
			}*/

			if (_next_page == true && $('#npl_img').get(0) && $('#npl_img').get(0).scrollHeight != 0) {
				//console.log('Lazy loaded flexible');
				if ($(window).scrollTop() + $(window).height() >= $('#npl_img').get(0).scrollHeight) {
					if (!$('#npl_img').attr('loaded')) {
						_next_page == false;
						//not in ajax.success due to multiple sroll events
						$('#npl_img').attr('loaded', true);
						//ajax goes here
						var booking_source = $('#pri_active_source').val();
						var booking_source = jQuery.parseJSON(booking_source);

						var result_booking_source = $('#pri_result_source').val();
						var total_api_count = $('#total_api_count').val();

						/*$.each(booking_source, function(index, element) {
				            load_hotels(process_result_update,element, _offset, _ini_fil);
				        });*/

						
						load_hotels(process_result_update,result_booking_source,total_api_count,total_api_count,_offset, _ini_fil);
					}
				}
			}

		}
		/**
		 * Offset and total records needed for pagination
		 */
	var _next_page = false;
	function ini_pagination(api_results) {
		//fixme - here
		//console.log(_offset+' offset : '+_fltr_r_cnt+ ' Filter Count '+ _total_r_cnt+ ' Total Count');
		if (_offset >= _fltr_r_cnt && _fltr_r_cnt < _total_r_cnt) {
			_next_page = false;
			//console.log('Filters are applied and all the results are loaded');
			$('#npl_img').hide();
		} else if (_offset < _fltr_r_cnt && _fltr_r_cnt < _total_r_cnt) {
			_next_page = true;
			//console.log('Filters are applied and all the results are not loaded');
			$('#npl_img').show();
		} else if (_offset > _total_r_cnt && _fltr_r_cnt == _total_r_cnt && dynamic_page_data == true) {
			_next_page = false;
			//all data loaded, remove scroll event handler as we no longer need lazy loading of data
			//console.log('No More Records so can activate JS filter and disable pagination');
			$(window).unbind('scroll', window, processLazyLoad);
			var total_api_count = $('#total_api_count').val();
			//if(total_api_count == api_results ){
				$('#npl_img').hide();
			//}
			dynamic_page_data = true;
			//enable sorting via javascript
			enable_sort();
		} else if (_offset < _fltr_r_cnt) {
			_next_page = true;
			//console.log('More Records are available to load');
			$('#npl_img').show();
		}
	}

	window.process_result_update = function(result,api_results) {
		var ___data_r = Date.now();
		$('.loader-image').hide();
		hide_result_pre_loader();
		if (result.hasOwnProperty('status') == true && result.status == true) {
			$('#pri_result_source').val(result.booking_source);
			var prev_cnt = $('#filter_records').text();
			var total_filte_result_cnt = result.filter_result_count;
			if(prev_cnt != '') {
				total_result_cnt = prev_cnt + result.filter_result_count;
			}
			//set_total_summary(result.total_result_count, result.filter_result_count, result.offset);
			set_total_summary(result.total_result_count, total_filte_result_cnt, result.offset);
			update_total_count_summary();
			disable_facilities(result.filters.facility);
			disable_accomodation(result.filters.a_type);
			disable_location(result.filters.loc);
			if($('.star-wrapper').hasClass('active') == false){
				enable_star_wrapper(result.filters.star);
			}
			$('#npl_img').removeAttr('loaded');
			if (_offset == 0) {
				//console.log('Data loaded with offset 0');
				$('#hotel_search_result').html(result.data);
				//No Result Found
			} else {
				if(_offset == 10){
					$('#hotel_search_result').html(result.data);
				}
				else{
					//console.log('Pagination data loaded with offset');
					$('#hotel_search_result').append(result.data);	
				}
				
			}
			ini_pagination(api_results);
			//Set time out to lazy load images
			lazy_img();
		}else{

			$('#npl_img').hide();
			$('.loader-image').hide();
			$('#empty_hotel_search_result').show();
		}
	}

	window.ini_result_update = function(result) {
		post_load_audio();
		//console.log(result);
		if (result.hasOwnProperty('status') == true && result.status == true) {
			//update_range_slider(parseInt(result.filters.p.min), parseInt(result.filters.p.max));
			update_range_slider(result.filters.p.min, result.filters.p.max);
			enable_location_selector(result.filters.loc);
			//console.log(result.filters.a_type);
			enable_accomodation_selector(result.filters.a_type);
			enable_star_wrapper(result.filters.star);
			enable_facility_selector(result.filters.facility);
			inif();
			$(window).on('scroll', processLazyLoad);
		}
		check_empty_search_result();
	}

	/**
	 * Set total result summary
	 */
	function set_total_summary(total_count, fltr_count, offset) {
		_fltr_r_cnt = parseInt(fltr_count); //visible 
		_total_r_cnt = parseInt(total_count); //total
		_offset = parseInt(offset);
	}

	function lazy_img() {
		$("img.lazy").lazy({
			threshold: 200
		});
	}

	function check_empty_search_result() {
		if ($('.r-r-i:first').index() == -1) {
			$('#empty-search-result').show();
			$('#page-parent').remove();
		}
	}

	var room_list_cache = {};
	$(document).on('click', ".room-btn", function(e) {
		e.preventDefault();
		var _cur_row_index = $(this).closest('.r-r-i');
		var _hotel_room_list = $(".room-list", _cur_row_index);
		var _result_index = $('[name="ResultIndex"]:first', _cur_row_index).val();
		if (_hotel_room_list.is(':visible') == false) {
			_hotel_room_list.show();
			if ((_result_index in room_list_cache) == false) {
				var _hotel_name = $('.h-name', _cur_row_index).text();
				var _hotel_star_rating = $('.h-sr', _cur_row_index).text();
				var _hotel_image = $('.h-img', _cur_row_index).attr('src');
				var _hotel_address = $('.h-adr', _cur_row_index).text();
				$('.loader-image', _cur_row_index).show();
				$.post(app_base_url + "index.php/ajax/get_room_details", $('.room-form', _cur_row_index).serializeArray(), function(response) {
					if (response.hasOwnProperty('status') == true && response.status == true) {
						room_list_cache[_result_index] = true;
						$('.loader-image', _cur_row_index).hide();
						$(".room-summ", _cur_row_index).html(response.data);
						//update star rating and hotel name
						$('[name="HotelName"]', _cur_row_index).val(_hotel_name);
						$('[name="StarRating"]', _cur_row_index).val(_hotel_star_rating);
						$('[name="HotelImage"]').val(_hotel_image); //Jaganath
						$('[name="HotelAddress"]').val(_hotel_address); //Jaganath
					}
				});
			} else {
				$(".room-summ", _cur_row_index).show();
			}
		} else {
			_hotel_room_list.hide();
		}
	});

	//Load hotels from active source
	show_result_pre_loader();
	$(document).on('click', '.location-map', function() {
		$('#map-box-modal').modal();
	});


	

	//************************** **********/

	$('.toglefil').click(function() {
		$(this).toggleClass('active');
	});


	/*  Mobile Filter  */
	$('.filter_tab').click(function() {
		$('.resultalls').stop(true, true).toggleClass('open');
		$('.coleft').stop(true, true).slideToggle(500);
	});

	var widowwidth = $(window).width();
	if (widowwidth < 991) {
		$('.resultalls.open #hotel_search_result').on('click', function() {
			$('.resultalls').removeClass('open');
			$('.coleft').slideUp(500);
		});
	}


	var application_preference_currency = document.getElementById('pri_app_pref_currency').value;

	/** -------------------------SORT LIST DATA---------------------- **/
	function unset_sorters_array()
	{
		_ini_fil['sort_item'] = undefined;
		_ini_fil['sort_type'] = undefined;
	}
	function enable_sort() {
		/*$(".price-l-2-h").click(function() {
			$(this).addClass('hide');
			$('.price-h-2-l').removeClass('hide');
			$("#hotel_search_result").jSort({
				sort_by: '.h-p',
				item: '.r-r-i',
				order: 'asc',
				is_num: true
			});
		});

		$(".price-h-2-l").click(function() {
			$(this).addClass('hide');
			$('.price-l-2-h').removeClass('hide');
			$("#hotel_search_result").jSort({
				sort_by: '.h-p',
				item: '.r-r-i',
				order: 'desc',
				is_num: true
			});
		});

		$(".name-l-2-h").click(function() {
			$(this).addClass('hide');
			$('.name-h-2-l').removeClass('hide');
			$("#hotel_search_result").jSort({
				sort_by: '.h-name',
				item: '.r-r-i',
				order: 'asc',
				is_num: false
			});
		});

		$(".name-h-2-l").click(function() {
			$(this).addClass('hide');
			$('.name-l-2-h').removeClass('hide');
			$("#hotel_search_result").jSort({
				sort_by: '.h-name',
				item: '.r-r-i',
				order: 'desc',
				is_num: false
			});
		});


		$(".star-l-2-h").click(function() {
			$(this).addClass('hide');
			$('.star-h-2-l').removeClass('hide');
			$("#hotel_search_result").jSort({
				sort_by: '.h-sr',
				item: '.r-r-i',
				order: 'asc',
				is_num: true
			});
		});

		$(".star-h-2-l").click(function() {
			$(this).addClass('hide');
			$('.star-l-2-h').removeClass('hide');
			$("#hotel_search_result").jSort({
				sort_by: '.h-sr',
				item: '.r-r-i',
				order: 'desc',
				is_num: true
			});
		});*/

		$(".price-l-2-h").unbind('click').click(function() {
			//if(dynamic_page_data == true) {

				default_view();
				$('.loader-image').show();
				$('#npl_img').hide();

				unset_sorters_array();
				_ini_fil['sort_item'] = 'price';
				_ini_fil['sort_type'] = 'asc';
				dynamic_sorter_rom();
				if($('.price-l-2-h').hasClass('hide') == false) {
					$('.price-l-2-h').addClass('hide');
				}
				if($('.price-h-2-l').hasClass('hide') == true) {
					$('.price-h-2-l').removeClass('hide');
				}
			//}
		});
		$(".price-h-2-l").unbind('click').click(function() {
			//if(dynamic_page_data == true) {

				default_view();
				$('.loader-image').show();
				$('#npl_img').hide();

				unset_sorters_array();
				_ini_fil['sort_item'] = 'price';
				_ini_fil['sort_type'] = 'desc';
				dynamic_sorter_rom();
				if($('.price-h-2-l').hasClass('hide') == false) {
					$('.price-h-2-l').addClass('hide');
				}
				if($('.price-l-2-h').hasClass('hide') == true) {
					$('.price-l-2-h').removeClass('hide');
				}
			//}
		});

		$(".name-l-2-h").unbind('click').click(function() {

			default_view();
			$('.loader-image').show();
			$('#npl_img').hide();

			unset_sorters_array();
			_ini_fil['sort_item'] = 'name';
			_ini_fil['sort_type'] = 'asc';
			dynamic_sorter_rom();
			if($('.name-l-2-h').hasClass('hide') == false) {
				$('.name-l-2-h').addClass('hide');
			}
			if($('.name-h-2-l').hasClass('hide') == true) {
				$('.name-h-2-l').removeClass('hide');
			}
		});

		$(".name-h-2-l").unbind('click').click(function() {
			default_view();
			$('.loader-image').show();
			$('#npl_img').hide();

			unset_sorters_array();
			_ini_fil['sort_item'] = 'name';
			_ini_fil['sort_type'] = 'desc';
			dynamic_sorter_rom();
			if($('.name-h-2-l').hasClass('hide') == false) {
				$('.name-h-2-l').addClass('hide');
			}
			if($('.name-l-2-h').hasClass('hide') == true) {
				$('.name-l-2-h').removeClass('hide');
			}
		});
		$(".star-l-2-h").unbind('click').click(function() {

			default_view();
			$('.loader-image').show();
			$('#npl_img').hide();

			unset_sorters_array();
			_ini_fil['sort_item'] = 'star';
			_ini_fil['sort_type'] = 'asc';
			dynamic_sorter_rom();
			if($('.star-l-2-h').hasClass('hide') == false) {
				$('.star-l-2-h').addClass('hide');
			}
			if($('.star-h-2-l').hasClass('hide') == true) {
				$('.star-h-2-l').removeClass('hide');
			}
		});
		$(".star-h-2-l").unbind('click').click(function() {

			default_view();
			$('.loader-image').show();
			$('#npl_img').hide();
			
			unset_sorters_array();
			_ini_fil['sort_item'] = 'star';
			_ini_fil['sort_type'] = 'desc';
			dynamic_sorter_rom();
			if($('.star-h-2-l').hasClass('hide') == false) {
				$('.star-h-2-l').addClass('hide');
			}
			if($('.star-l-2-h').hasClass('hide') == true) {
				$('.star-l-2-h').removeClass('hide');
			}
		});

	}
	enable_sort();
	/** -------------------------SORT LIST DATA---------------------- **/

	/**
	 * Toggle active class to highlight current applied sorting
	 **/
	$(document).on('click', '.sorta', function(e) {
		e.preventDefault();
		loader();
		$(this).closest('.sortul').find('.active').removeClass('active');
		//Add to sibling
		$(this).siblings().addClass('active');
	});

	$('.loader').on('click', function(e) {
		e.preventDefault();
		loader();
	});

	$('#hotel-name-search-btn').on('click', function(e) {
		loader();
		e.preventDefault();
		$('.loader-image').show();
		$('#npl_img').hide();
		ini_hotel_namef();
		filter_rom();
	});

	$(document).on('change', 'input.acc-type', function(e) {
		loader();
		$('.loader-image').show();
		$('#npl_img').hide();
		window.scrollTo(500, 10);
		ini_hotel_accf();
		filter_rom();
	});
	
	$(document).on('change', 'input.hotel-location', function(e) {
		loader();
		$('.loader-image').show();
		$('#npl_img').hide();
		window.scrollTo(500, 10);
		ini_hotel_locf();
		filter_rom();
	});
	

	$('.deal-status-filter').on('change', function(e) {
		loader();
		$('.loader-image').show();
		$('#npl_img').hide();
		ini_dealf();
		filter_rom();
	});

	$(document).on('change', '.star-filter', function(e) {
		loader();
		$('.loader-image').show();
		$('#npl_img').hide();
		window.scrollTo(500, 10);
		var thisEle = this;
		var _filter = '';
		var attr = {};
		attr['checked'] = $(thisEle).is(':checked');
		ini_starf();
		filter_rom('star-filter', _filter, attr);
	});

	$(document).on('change', '.hotel-facility', function(e) {
		loader();
		$('.loader-image').show();
		$('#npl_img').hide();
		window.scrollTo(500, 10);
		var thisEle = this;
		var _filter = '';
		var attr = {};
		attr['checked'] = $(thisEle).is(':checked');
		ini_facilityf();
		filter_rom('hotel-facility', _filter, attr);
	});



	//Jaganath - Setting minimum and maximum price for slider range
	function update_range_slider(minPrice, maxPrice) {
		$('#core_minimum_range_value', '#core_min_max_slider_values').val(minPrice);
		$('#core_maximum_range_value', '#core_min_max_slider_values').val(maxPrice);
		//price-refine
		enable_price_range_slider(minPrice, maxPrice);
	}
	//Reset the filters -- Jaganath
	$(document).on('click', '#reset_filters', function() {
		loader();
		$('.loader-image').show();
		$('#npl_img').hide();
		//Reset the Star,and Location Filters
		$('#starCountWrapper .active').each(function() {
			$(this).removeClass('active');
			$('.star-filter', this).prop('checked', false);
		});
		
		$('input.acc-type').prop('checked', false);
		$('input.hotel-facility').prop('checked', false);
		$('input.hotel-location').prop('checked', false);
		
		$('input.hotel-location').prop('checked', false);
		//HotelName
		$('#hotel-name').val(''); //Hotel Name
		set_slider_label(min_amt, max_amt);
		var minPrice = $('#core_minimum_range_value', '#core_min_max_slider_values').val();
		var maxPrice = $('#core_maximum_range_value', '#core_min_max_slider_values').val();
		$("#price-range").slider("option", "values", [minPrice, maxPrice]);
		inif();
		filter_rom();
	});

	// default result views
	function default_view(){
		$('#hotel_search_result .item').addClass('list-group-item');
    	$('#hotel_search_result .item').removeClass('grid-group-item');
    	$('.map_view').hide();
    	$('.rowresult1').addClass("col-xs-4");
    	$('.allresult').removeClass("map_open");
    	$('#hotel_search_result .item').show();
    	$('.view_sty_btn').removeClass("active");
    	$('.list_click').addClass("active");
	}

	/**
	 * Show loader images
	 */
	function loader() {
		$('.container').css({
			'opacity': '1'
		});
		setTimeout(function() {
			$('.container').css({
				'opacity': '1'
			}, 'slow');
		}, 1000);
	}

	//------ INI F
	var _ini_fil = {};
	_ini_fil['_sf'] = [];

	function ini_starf() {
		_ini_fil['_sf'] = $('input.star-filter:checked').map(function() {
			return this.value;
		}).get();
	}

	//facility
	_ini_fil['_fac'] = [];

	function ini_facilityf() {
		_ini_fil['_fac'] = $('input.hotel-facility:checked').map(function() {
			return this.value;
		}).get();
	}

	_ini_fil['hl'] = [];

	function ini_hotel_locf() {
		_ini_fil['hl'] = $('.hotel-location:checked', '#hotel-location-wrapper').map(function() {
			return this.value;
		}).get();
	}
	
	_ini_fil['at'] = [];

	function ini_hotel_accf() {
		_ini_fil['at'] = $('.acc-type:checked', '#hotel-acc-wrapper').map(function() {
			return this.value;
		}).get();
	}

	_ini_fil['min_price'] = 0;
	_ini_fil['max_price'] = 0;

	function ini_pricef() {
		_ini_fil['min_price'] = parseFloat($("#price-range").slider("values")[0]);
		_ini_fil['max_price'] = parseFloat($("#price-range").slider("values")[1]);
	}

	_ini_fil['hn_val'] = '';

	function ini_hotel_namef() {
		_ini_fil['hn_val'] = $('#hotel-name').val().trim().toLowerCase();
	}

	_ini_fil['dealf'] = false;

	function ini_dealf() {
		if ($('.deal-status-filter:checked').val() == 'filter') {
			_ini_fil['dealf'] = true;
		} else {
			_ini_fil['dealf'] = false;
		}
	}

	function inif() {
		ini_starf();
		ini_hotel_locf();
		ini_hotel_accf();
		ini_pricef();
		ini_hotel_namef();
		ini_dealf();
		ini_facilityf();
	}
	//------ INI F

	/**
	 * _filter_trigger	==> element which caused fliter to be triggered
	 * _filter			==> default filter settings received from filter trigger
	 */
	function filter_rom(_filter_trigger, _filter, attr) {
		inif();
		default_view();
		//console.log(_offset+' offset : '+_fltr_r_cnt+ ' Filter Count '+ _total_r_cnt+ ' Total Count');
		//if (_fltr_r_cnt == _total_r_cnt && _offset >= _total_r_cnt) {
		if (dynamic_page_data == false) {
			//console.log('if');
			//if (_filter_trigger == 'star-filter') {
			//	if ((attr['checked'] == false && _ini_fil['_sf'].length > 0) || (attr['checked'] == true && _ini_fil['_sf'].length == 1)) {
			//		_filter = ':visible';
			//	} else {
			//		_filter = ':hidden';
			//	}
			//} else {
			//	_filter = '';
			//}

			_fltr_r_cnt = 0;
			//$('.r-r-i' + _filter) : FIXME
			$('.r-r-i').each(function(key, value) {
				var _rmp = parseInt($('.h-p', this).text());
				var _rhn = $('.h-name', this).text().trim().toLowerCase();
				var _rhl = $('.h-loc', this).text();
				var _rde = $('.deal-status', this).data('deal');
				var _fclty = $('.m-f-l', this).val();
				var star = $('.h-sr', this).text();
				var _acc = $('.a-t', this).data('cstr');
				/*console.log(_rhl);
				console.log(_ini_fil['hl']);
				console.log($.inArray(_ini_fil['hl'],(_rhl)));*/
				if (
					((_ini_fil['_sf'].length == 0) || ($.inArray((star), _ini_fil['_sf']) != -1)) &&
					(_rmp >= _ini_fil['min_price'] && _rmp <= _ini_fil['max_price']) &&
					((_rhn == "" || _ini_fil['hn_val'] == "") || (_rhn.search(_ini_fil['hn_val']) > -1)) &&
					((_rhl == "" || _ini_fil['hl'].length == 0) || ($.inArray((_rhl), _ini_fil['hl']) != -1)) &&
					((_acc == "" || _ini_fil['at'].length == 0) || ($.inArray((_acc), _ini_fil['at']) != -1)) &&
					(_ini_fil['dealf'] == false || (_ini_fil['dealf'] == true && _rde == true)) &&
					((_ini_fil['_fac'].length == 0) || (has_all_facility(_fclty) == true))
				){
					++_fltr_r_cnt;
					$(this).removeClass('hide');
					$('.loader-image').hide();
					//console.log('remove');
				} else {
					//console.log('add');
					$(this).addClass('hide');
				}

				if(_fltr_r_cnt <= 0){
					$('.loader-image').hide();
					$('#empty_hotel_search_result').show();
				}
				//console.log((has_any_facility(_fclty)));
				set_marker_visibility(this, null, null, _rhn, star);
			});
		} else {
			//console.log('else');
			//filter from backend
			dynamic_filter_rom();
		}
		update_total_count_summary();
	}

	function has_all_facility(_fclty) {
		/*console.log('hi');
		console.log(_ini_fil['_fac']);
		console.log(_fclty);*/
		var has = false;
		$.each(_ini_fil['_fac'], function(k, v) {
			var str_lnth = parseInt(_fclty.search(v));
			if (str_lnth > 0) {
				//_fclty.search(set_null);
				console.log(_fclty.search(v));
				console.log('-----------');
				has = true;
				//return false
			}else{
				console.log(_fclty.search(v));
				console.log('-----------');
				has = false;
				return false;
			}
		});
		return has;
	}

	function dynamic_filter_rom() {
		var booking_source = $('#pri_active_source').val();
		var booking_source = jQuery.parseJSON(booking_source);
		var result_booking_source = $('#pri_result_source').val();
		var total_api_count = $('#total_api_count').val();
		
		//-- empty results and show loader
		show_result_pre_loader();
		$('#hotel_search_result').empty();
		_offset = 0;
		load_hotels(process_result_update,result_booking_source,total_api_count,total_api_count,_offset, _ini_fil);
		//load_hotels(process_result_update,booking_source[1], _offset, _ini_fil);
	}

	//Jaganath
	function dynamic_sorter_rom()
	{
		var booking_source = $('#pri_active_source').val();
		var booking_source = jQuery.parseJSON(booking_source);
		var result_booking_source = $('#pri_result_source').val();
		var total_api_count = $('#total_api_count').val();

		
		//-- empty results and show loader
		show_result_pre_loader();
		$('#hotel_search_result').empty();
		_offset = 0;
		load_hotels(process_result_update,result_booking_source,total_api_count,total_api_count,_offset, _ini_fil);
	}

	/**
	 *Update Hotel Count Details
	 */
	function update_total_count_summary() {
		$('#hotel_search_result').show();
		//if($.active == 0) {
			if (isNaN(_fltr_r_cnt) == true || _fltr_r_cnt < 1) {
				_fltr_r_cnt = 0;
				//display warning
				$('#hotel_search_result').hide();
				$('#empty_hotel_search_result').show();
			} else {
				$('#hotel_search_result').show();
				$('#empty_hotel_search_result').hide();
			}
		//}
		$('#total_records').text(_total_r_cnt);
		$('.total-row-record-count').text(_total_r_cnt);
		$('#filter_records').text(_fltr_r_cnt);
	}
	var sliderCurrency = application_preference_currency;
	//application_preference_currency
	var min_amt = 0;
	var max_amt = 0;

	function enable_price_range_slider(minPrice, maxPrice) {
		min_amt = minPrice;
		max_amt = maxPrice;
		$("#price-range").empty();
		/**** PRICE SLIDER START ****/
		$("#price-range").slider({
			range: true,
			min: minPrice,
			max: maxPrice,
			values: [minPrice, maxPrice],
			slide: function(event, ui) {
				set_slider_label(ui.values[0], ui.values[1]);
			},
			change: function(e) {
				if ('originalEvent' in e) {
					$('.loader-image').show();
		            $('#npl_img').hide();
					ini_pricef();
					filter_rom();
				}
			}
		});
		set_slider_label(minPrice, maxPrice);
		/**** PRICE SLIDER END ****/
	}

	function set_slider_label(val1, val2) {
		$("#hotel-price").text(sliderCurrency + val1.toFixed(2) + " - " + sliderCurrency + val2.toFixed(2));
	}

	function enable_location_selector(locs) {
		var _location_option_list = '';
		var i = 0;
		$.each(locs, function(k, v) {
			_location_option_list += '<li>';
			_location_option_list += '<div class="squaredThree llocSquaredThree '+k+'">';
			_location_option_list += '<input id="llocSquaredThree' + i + '" class="hotel-location" type="checkbox" name="check" value="' + v['v'] + '">';
			_location_option_list += '<label for="llocSquaredThree' + i + '"></label>';
			_location_option_list += '</div>';
			//_location_option_list += '<label class="lbllbl llocSquaredThree '+k+'" for="llocSquaredThree' + i + '">' + v['v'] + ' (' + v['c'] + ')</label>';
			_location_option_list += '<label class="lbllbl llocSquaredThree '+k+'" for="llocSquaredThree' + i + '">' + v['v'] + '</label>';
			_location_option_list += '</li>';
			i++;
		});
		$('#hotel-location-wrapper').html(_location_option_list);
	}
	
	function enable_accomodation_selector(locs) {
		var _option_list = '';
		var i = 0;
		$.each(locs, function(k, v) {
			
			if(v['v'] == 'Apart'){
				var ACDT = v['v'].replace("Apart", "Apartment");
			}else if(v['v'] == 'Apthotel'){
				var ACDT = v['v'].replace("Apthotel", " Hotel Apartment");
			}else{
				var ACDT = v['v'];
			}

			if(v['v'] != null) {
				_option_list += '<li>';
				_option_list += '<div class="squaredThree alocSquaredThree '+k+'">';
				_option_list += '<input id="alocSquaredThree' + i + '" class="acc-type" type="checkbox" name="check" value="' + v['v'] + '">';
				_option_list += '<label for="alocSquaredThree' + i + '"></label>';
				_option_list += '</div>';
				//_option_list += '<label class="lbllbl alocSquaredThree '+k+'" for="alocSquaredThree' + i + '">' + ACDT + ' (' + v['c'] + ')</label>';
				_option_list += '<label class="lbllbl alocSquaredThree '+k+'" for="alocSquaredThree' + i + '">' + ACDT + '</label>';
				_option_list += '</li>';
				i++;
			}
		});
		$('#hotel-acc-wrapper').html(_option_list);
	}

	function enable_facility_selector(facilities) {
		//console.log('yes');
		var _facility_option_list = '';
		var i = 0;
		$.each(facilities, function(k, v) {
			_facility_option_list += '<li>';
			_facility_option_list += '<div class="squaredThree '+k+' flocSquaredThree">';
			_facility_option_list += '<input id="flocSquaredThree' + i + '" class="hotel-facility" type="checkbox" name="facility[]" value="' + v['cstr_inf'] + '">';
			_facility_option_list += '<label for="flocSquaredThree' + i + '"></label>';
			_facility_option_list += '</div>';
			//_facility_option_list += '<label class="lbllbl '+k+' flocSquaredThree" for="flocSquaredThree' + i + '">' + v['v'] + ' (' + v['c'] + ')</label>';
			_facility_option_list += '<label class="lbllbl '+k+' flocSquaredThree" for="flocSquaredThree' + i + '">' + v['v'] + '</label>';
			//_facility_option_list += '<label class="lbllbl" for="flocSquaredThree' + i + '">' + v['v'] + '</label>';
			_facility_option_list += '</li>';
			i++;
		});
		$('#hotel-facility-wrapper').html(_facility_option_list);
	}


	function enable_star_wrapper(star_count_array) {
		var starCat = 0;
		$('#starCountWrapper .star-filter').each(function(key, value) {
			starCat = parseInt($(this).val());
			if ($.isEmptyObject(star_count_array[starCat]) == true) {
				//disabled
				//$(this).attr('disabled', 'disabled');
				$(this).closest('.star-wrapper').removeClass('enabled');
				$(this).closest('.star-wrapper').addClass('disabled');
			} else {
				$(this).closest('.star-wrapper').removeClass('disabled');
				$(this).closest('.star-wrapper').addClass('enabled');
			}
		});
	}

	function disable_facilities(facilities){
		//console.log('no');
		$('.flocSquaredThree').addClass('disabled');
		//console.log(facilities);
		/*$.each(function(key, value) {
			console.log(key);
		});*/

		$.each( facilities, function( key, value ) {
		  $('.'+key).removeClass('disabled');
		});
	}

	function disable_accomodation(a_type){
		$('.alocSquaredThree').addClass('disabled');
		$.each( a_type, function( key, value ) {
		  $('.'+key).removeClass('disabled');
		});
	}

	function disable_location(loc){
		$('.llocSquaredThree').addClass('disabled');
		$.each( loc, function( key, value ) {
		  $('.'+key).removeClass('disabled');
		});
	}

	function unique_array_values(array_values) {
		var _unique_array_values = [];
		$.each(array_values, function(k, v) {
			if (_unique_array_values.indexOf(v) == -1) {
				_unique_array_values.push(v);
			}
		});
		return _unique_array_values;
	}

	function get_location_list() {
		return $('.h-loc').map(function() {
			return $(this).text();
		});
	}
	$('.vlulike').click(function() {
		$('.vlulike').removeClass('active');
		$(this).addClass('active');
	});

	//MAP
	/*Map view click function*/
	$('.map_click').click(function() {
		resizeMap();
		reset_ini_map_view();//set up master map markers
		$('.allresult').addClass('map_open');
		$('.view_type').removeClass('active');
		$(this).addClass('active');

		$(".hotels_results").niceScroll({
			styler: "fb",
			cursorcolor: "#4ECDC4",
			cursorwidth: '3',
			cursorborderradius: '10px',
			background: '#404040',
			spacebarenabled: false,
			cursorborder: ''
		});

		google.maps.event.trigger(map, 'resize');
		resizeMap();
	});
	
	$('.list_click').click(function() {
		$('.allresult').removeClass('map_open');
		$('.view_type').removeClass('active');
		$(this).addClass('active');
	});
	
	var map;        
	var myCenter=new google.maps.LatLng(53, -1.33);
	var marker=new google.maps.Marker({
	    position:myCenter
	});

	function initialize() {
	  var mapProp = {
	      center:myCenter,
	      zoom: 11,
	      draggable: true,
	      scrollwheel: true,
	      mapTypeId:google.maps.MapTypeId.ROADMAP
	  };
	  
	  map=new google.maps.Map(document.getElementById("map"),mapProp); 

	  marker.setMap(map);
	    
	  google.maps.event.addListener(marker, 'click', function() {
	      
	    infowindow.setContent(contentString);
	    infowindow.open(map, marker);
	    
	  }); 
	};
	google.maps.event.addDomListener(window, 'load', initialize);

	google.maps.event.addDomListener(window, "resize", resizingMap());

	$('#map_view_hotel').on('show.bs.modal', function() {
		//Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)
		resizeMap();
	})

	function resizeMap() {
		if (typeof map == "undefined") return;
		setTimeout(function() {
			resizingMap();
		}, 600);
	}

	function resizingMap() {
		if (typeof map == "undefined") return;
		var center = map.getCenter();
		google.maps.event.trigger(map, "resize");
		map.setCenter(center);
	}
	
	/**
	 * Set value are data on map
	 */
	var marker;
	//var markers = [];
	var min_lat = 0;
	var max_lat = 0;
	var min_lon = 0;
	var max_lon = 0;
	//initialize master map object
	function reset_ini_map_view()
	{
		ini_map_view();
	}
	
	function ini_map_view() {
		var counter=0;
		if (max_lat == 0) {
			max_lat = min_lat = 0;
		}

		if (max_lon == 0) {
			max_lon = min_lon = 0;
		}
		$('#hotel_search_result .r-r-i').each(function() {
			set_marker_visibility(this);
			var lat = parseFloat($(this).find('.hotel_location').data('lat'));
			var lon = parseFloat($(this).find('.hotel_location').data('lon'));
			if((typeof(lat)!='undefined') && (lat!='') && !isNaN(lat) && (counter<1)){
				max_lat = min_lat = lat;
			}
			if((typeof(lon)!='undefined') && (lon!='') && !isNaN(lon)){
				counter++;					
				max_lon = min_lon = lon;
			}
		});
		//set center
		var mid_lat = parseFloat(max_lat + min_lat) / 2;
		var mid_lon = parseFloat(max_lon + min_lon) / 2;
		map.setCenter(new google.maps.LatLng(mid_lat, mid_lon));
	}
	
	//function show_on_map()
	
	function get_map_attr_obj(thisRef, lat, lon, name, star)
	{
		var object = {};
		object['lat'] = parseFloat(lat) || parseFloat($('.hotel_location', thisRef).data('lat'));
		object['lon'] = parseFloat(lon) || parseFloat($('.hotel_location', thisRef).data('lon'));
		object['name'] = name || $('.h-name', thisRef).text();
		object['star'] = star || $('.h-sr', thisRef).text();
		object['details_url'] = $('.booknow', thisRef).attr('href');
		object['img'] = $('.h-img', thisRef).data('src');
		object['acc_key'] = marker_access_key(object['lat'], object['lon']);
		if (object['img'] == '') {
			$('.h-img', thisRef).attr('src');
		}
		object['curr'] = $('.currency_symbol', thisRef).text();
		object['price'] = $('.h-p', thisRef).text();
		return object;
	}
	
	function marker_access_key(lat, lon)
	{
		return lat+'_'+lon;
	}
	
	function set_marker_visibility(thisRef, lat, lon, name, star)
	{ //alert(lat);
		var lat = parseFloat($('.hotel_location', thisRef).data('lat'));
		var lon = parseFloat($('.hotel_location', thisRef).data('lon'));
		var access_key = marker_access_key(lat, lon);
		var visibility = true;
		if ($(thisRef).is(':visible') == false) {
			visibility = false;
			//console.log('data is hidden');
		}

		if ($.isEmptyObject(markers[access_key]) == true) {
			//console.log('Access key not found '+access_key);
			$(thisRef).attr('access_key', access_key);
			var object = get_map_attr_obj(thisRef, lat, lon, name, star);
			create_marker(object, visibility);
		} else {
			//toggle visibility
			//console.log('already present so setting visibility');
			markers[access_key].setVisible(visibility);
		}
	}

	/**
	 * Add marker
	 */
	function create_marker(obj, visibility)
	{
		max_lon = (max_lon < obj['lon'] ? obj['lon'] : max_lon);
		min_lon = (min_lon < obj['lon'] ? obj['lon'] : min_lon);
		max_lat = (max_lat < obj['lat'] ? obj['lat'] : max_lat);
		min_lat = (min_lat < obj['lat'] ? obj['lat'] : min_lat);
		var contentString =
		'<div class="map_box">'+
			'<div class="in_map_box">'+
				'<div class="map_image">'+
					'<img alt="" src="'+obj['img']+'">'+
					'<div class="map_htl_price"><span>'+obj['curr']+'</span> <span>'+obj['price']+'</span></div>'+
				'</div>'+
				'<div class="map_details">'+
					'<div class="col-xs-8 nopad">'+
						'<div class="map_name_dets">'+
							'<h4 class="map_name_htl">'+obj['name']+'</h4>'+
							'<div data-star="'+obj['star']+'" class="stra_hotel"> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> </div>'+
						'</div>'+
					'</div>'+
					'<div class="col-xs-4 nopad"> <a class="book_map" href="'+obj['details_url']+'">Book</a> </div>'+
				'</div>'+
			'</div>'+
		'</div>';

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		var marker = new google.maps.Marker({
			map: map,
			draggable: false,
			position: {lat: obj['lat'], lng: obj['lon']},
			title: obj['name'],
			visible: visibility
		});

		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});
		//console.log('Access key created for '+obj['acc_key']);
		markers[obj['acc_key']] = marker;
	}
});
var markers = {};
