$(function($) {
var check_in = db_date(5);
var check_out = db_date(6);
    $('.htd-wrap').on('click', function(e) {
        e.preventDefault();
        var curr_destination = $('.top-des-val', this).val();
        var city_id = $('.top_des_id',this).val();

        $('#hotel_destination_search_name').val(curr_destination);
        $(".loc_id_holder").val(city_id);
        $('#hotel_checkin').val(check_in);
        $('#hotel_checkout').val(check_out);
        $('#hotel_search').submit();
    });

        //for holiday 
     $('.hol-wrap').on('click', function(e) {
        e.preventDefault();
        var curr_destination = $('.country_holiday', this).val();
        var package_type = $('.package_type', this).val();
        $('#country').val(curr_destination);
        $('#package_type').val(package_type);
        $('#duration').val("");
        $('#holiday_search').submit()
    });

        //for flight
        $('.flight-wrap').on('click', function(e) {
        e.preventDefault();
        // var from_city = $('.from_city', this).val();
        // var to_city = $('.to_city', this).val();
        // var start_date = $('.start_date', this).val();
        // var trip_type = $('.trip_type', this).val();
        var fromlocid = $('.from_loc_id', this).val();
        var tolocid = $('.to_loc_id', this).val();
   
        // $("#from").val(from_city);
        // $("#from_loc_id").val(fromlocid);
        // $("#flight_datepicker1").val(start_date);
        // $("#to").val(to_city);
        // $("#to_loc_id").val(tolocid);
        // $("#trip_type").val(trip_type);
        // $("#choosen_airline_class").val('Economy');
        // $('#OWT_adult').val('1');
           // alert(trip_type);
           // alert(from_city);
           // alert(to_city);
           // alert(fromlocid);
           // alert(tolocid);
           // alert(start_date);

        $('#flight_form-home_'+fromlocid+'_'+tolocid).submit();
    });


      //for flight
    $('.flight-wrapper-data').on('click', function(e) {
        // alert("sdfsdf");
        e.preventDefault();

        var triptype = $(this).data('triptype');
        var from_city = $(this).data('fromcity');
        var to_city = $(this).data('tocity');
        var fromlocid = $(this).data('fromlocid');
        var tolocid = $(this).data('tolocid');
        var to = $(this).data('to');
        var departure = $(this).data('departure');
         var end = $(this).data('returndate');
       //alert(triptype);
       //alert(from_city);
       //alert(to_city);
       //alert(fromlocid);
       //alert(tolocid);
       //alert(departure);

        $("#from").val(from_city);
        $("#from_loc_id").val(fromlocid);
        $("#to").val(to_city);
        $("#to_loc_id").val(tolocid);
        $("#flight_datepicker1").val(departure);
        $("#trip_type").val(triptype);
        // $("#choosen_airline_class").val('Economy');
        // $('#OWT_adult').val('1');
        // alert('ok');
         $('#flight-form-submit').trigger('click');
    });

    $('.activity-search').on("click",function(e){
        //alert("hiii");
        e.preventDefault();
        var curr_destination = $('.destination_name',this).val();
        //alert(curr_destination);
        //console.log("curr_destination"+curr_destination);
        var city_id = $('.destination_id',this).val();
        //alert("city_id"+city_id);
        //console.log("city_id"+city_id);
        var category_id = $('.category_id',this).val();
        $("#activity_destination_search_name").val(curr_destination);
        $(".loc_id_holder").val(city_id);
        $("#select_cate").val(category_id);
        //$("#name-search").val(curr_destination);
        $("#activity_search").submit();
    });
    $("#owl-demo2").owlCarousel({
        items: 3,
        itemsDesktop: [991, 2],
        itemsDesktopSmall: [767, 2],
        itemsTablet: [600, 1],
        itemsMobile: [479, 1],
        navigation: true,
        pagination: true
    });      
    $("#TopAirLine").owlCarousel({
        items:5,
        loop:true,
        margin:10,
        autoplay:true,
        navigation: true,
        pagination: false,
        autoplayTimeout:1000,
        autoplayHoverPause:true
    });
    $("#all_deal").owlCarousel({
    items : 1, 
    autoplay:true,
    autoplayTimeout:1000,
    itemsDesktop : [1000,1],
    itemsDesktopSmall : [991,1], 
    itemsTablet: [767,1], 
    itemsMobile : [480,1], 
        navigation : true,
    pagination : false
    });
    
    $.supersized({
        slide_interval: 5000,
        transition: 1,
        transition_speed: 700,
        slide_links: 'blank',
		slides: tmpl_imgs
    })
});

$(document).ready(function() {
  //carousel options
  $('#quote-carousel').carousel({
    pause: true, interval: 10000,
  });
});