<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once BASEPATH . 'libraries/Common_Api_Grind.php';
/**
 *
 * @package    Provab
 * @subpackage API
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */
class Provab_private extends Common_Api_Grind
{
    private $ClientId;
    private $UserName;
    private $Password;
    private $service_url;
    private $Url;

    private $LoginType = '2'; //(For API Users, value should be ‘2’)
    private $EndUserIp = '127.0.0.1';
    private $TokenId; //    Token ID that needs to be echoed back in every subsequent request
    public $master_search_data;
    public $search_hash;
    public function __construct()
    {
        $this->CI = &get_instance();
        $GLOBALS['CI']->load->library('Api_Interface');
        $GLOBALS['CI']->load->model('hotel_model');
        $this->TokenId = $GLOBALS['CI']->session->userdata('tb_auth_token');
        $this->set_api_credentials();
        //if found in session then token will not be replaced
        //$this->set_authenticate_token();
    }

    private function get_header()
    {
        $hotel_engine_system   = $this->CI->config->item('hotel_engine_system');
        $response['UserName']  = $this->CI->config->item($hotel_engine_system . '_username');
        $response['Password']  = $this->CI->config->item($hotel_engine_system . '_password');
        $response['DomainKey'] = $this->CI->config->item('domain_key');
        $response['system']    = $hotel_engine_system;
        return $response;
    }

    private function set_api_credentials()
    {

        $hotel_engine_system = $this->CI->config->item('hotel_engine_system');
        $this->system        = $hotel_engine_system;
        $this->UserName      = $this->CI->config->item($hotel_engine_system . '_username');
        $this->Password      = $this->CI->config->item($hotel_engine_system . '_password');
        $this->Url           = $this->CI->config->item('hotel_url');
        $this->ClientId      = $this->CI->config->item('domain_key');
        //$this->UserName = 'test';
        //$this->Password = 'password'; // miles@123 for b2b

    }

    public function credentials($service)
    {
        switch ($service) {
            case 'Authenticate':

                $this->service_url = $this->Url . 'Authenticate';

                break;
            case 'GetHotelResult':

                $this->service_url = $this->Url . 'Search';

                break;
            case 'GetHotelInfo':

                $this->service_url = $this->Url . 'HotelDetails';

                break;
            case 'GetHotelRoom':

                $this->service_url = $this->Url . 'RoomList';

                break;
            case 'BlockRoom':

                $this->service_url = $this->Url . 'BlockRoom';

                break;
            case 'Book':

                $this->service_url = $this->Url . 'CommitBooking';
                break;
            case 'CancelBooking':
                $this->service_url = $this->Url . 'CancelBooking';
                break;
            case 'GetHotelImages':
                $this->service_url = $this->Url . 'GetHotelImages';
                break;
            case 'GetCancellationCode':
                $this->service_url = $this->Url . 'GetCancellationPolicy';
                break;

            case 'SendChangeRequest':

                //$this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/SendChangeRequest/';
                $this->service_url = $this->Url . 'SendChangeRequest';

                break;
            case 'GetChangeRequestStatus':

                $this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetChangeRequestStatus/';
                $this->service_url = $this->Url . 'GetChangeRequestStatus';
                break;

            case 'GetBookingDetail':

                $this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetBookingDetail/';

                break;

            case 'GenerateVoucher':

                $this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GenerateVoucher/';

                break;
        }
    }
    /*
    Orignal Credentials
    function credentials($service)
    {
    switch ($service) {
    case 'Authenticate':
    if ($this->credential_type == 'live') {
    //$this->service_url = 'http://tboapi.travelboutiqueonline.com/SharedAPI/SharedData.svc/rest/Authenticate';
    $this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/Authenticate';
    } else {
    //$this->service_url = 'http://api.tektravels.com/SharedServices/SharedData.svc/rest/Authenticate';
    $this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/Authenticate';
    }
    break;
    case 'GetHotelResult':
    if ($this->credential_type == 'live') {
    $this->service_url = 'http://tboapi.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetHotelResult/';
    //$this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/GetHotelResult';
    } else {
    //$this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelResult/';
    $this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/GetHotelResult';
    }
    break;
    case 'GetHotelInfo':
    if ($this->credential_type == 'live') {
    $this->service_url = 'http://tboapi.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetHotelInfo/';
    //$this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/GetHotelInfo';
    } else {
    //$this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelInfo/';
    $this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/GetHotelInfo';
    }
    break;
    case 'GetHotelRoom':
    if ($this->credential_type == 'live') {
    $this->service_url = 'http://tboapi.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetHotelRoom/';
    //$this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/GetHotelRoom';
    } else {
    //$this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelRoom/';
    $this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/GetHotelRoom';
    }
    break;
    case 'BlockRoom':
    if ($this->credential_type == 'live') {
    $this->service_url = 'http://tboapi.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/BlockRoom/';
    //$this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/BlockRoom';
    } else {
    //$this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/BlockRoom/';
    $this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/BlockRoom';
    }
    break;
    case 'Book':
    if ($this->credential_type == 'live') {
    $this->service_url = 'http://tboapi.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/Book/';
    //$this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/Book';
    } else {
    //$this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/Book/';
    $this->service_url = 'http://192.168.0.63/provab/webservices/hotel_server/Book';
    }
    break;
    case 'SendChangeRequest':
    if ($this->credential_type == 'live') {
    $this->service_url = '';
    } else {
    $this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/SendChangeRequest/';
    }
    break;
    case 'GetChangeRequestStatus':
    if ($this->credential_type == 'live') {
    $this->service_url = 'http://tboapi.travelboutiqueonline.com/HotelAPI_V10/hotelservice.svc/rest/GetChangeRequestStatus/';
    } else {
    $this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetChangeRequestStatus/';
    }
    break;

    case 'GetBookingDetail':
    if ($this->credential_type == 'live') {
    $this->service_url = 'http://tboapi.travelboutiqueonline.com/HotelAPI_V10/hotelservice.svc/rest/GetBookingDetail/';
    } else {
    $this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetBookingDetail/';
    }
    break;

    case 'GenerateVoucher':
    if ($this->credential_type == 'live') {
    $this->service_url = 'http://tboapi.travelboutiqueonline.com/HotelAPI_V10/hotelservice.svc/rest/GenerateVoucher/';
    } else {
    $this->service_url = 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GenerateVoucher/';
    }
    break;
    }
    }

     */

    /**
     *  Arjun J Gowda
     *
     * This will get the "TokenId" and refresh token id
     * Keeping static as this should work for all the objects
     * @param boolean $override_token to decide if the token has to be overriden in case if token has to be refreshed
     */
    public function set_authenticate_token($override_token = false)
    {
        $header = $this->get_header();
        if (empty($this->TokenId) == true || $override_token == true) {
            $this->credentials('Authenticate');
            $service_url          = $this->service_url;
            $request['ClientId']  = $this->ClientId;
            $request['UserName']  = $this->UserName;
            $request['Password']  = $this->Password;
            $request['LoginType'] = $this->LoginType;
            $request['EndUserIp'] = $this->EndUserIp;
            //$GLOBALS['CI']->custom_db->generate_static_response(json_encode($request));
            $response = $GLOBALS['CI']->api_interface->get_json_response($service_url, json_encode($request), $header);

            //$GLOBALS['CI']->custom_db->generate_static_response(json_encode($response));
            if (valid_array($response) == true && empty($response['Status']) == false && $response['Status'] == ACTIVE) {
                //validate response and create session
                $authenticate_token = $response['TokenId'];
                $GLOBALS['CI']->session->set_userdata(array('tb_auth_token' => $authenticate_token));
                $this->TokenId = $authenticate_token;
            } else {
                //FIXME : handle all the failure conditions
                //redirect(base_url());
            }
        }
    }

    /**
     *  Arjun J Gowda
     *
     * TBO auth token will be returned
     */
    public function get_authenticate_token()
    {
        return $GLOBALS['CI']->session->userdata('tb_auth_token');
    }

    public function mobile_auth_token_id()
    {
        $header = $this->get_header();
        if (empty($this->TokenId) == true || $override_token == true) {
            $this->credentials('Authenticate');
            $service_url          = $this->service_url;
            $request['ClientId']  = $this->ClientId;
            $request['UserName']  = $this->UserName;
            $request['Password']  = $this->Password;
            $request['LoginType'] = $this->LoginType;
            $request['EndUserIp'] = $this->EndUserIp;
            //$GLOBALS['CI']->custom_db->generate_static_response(json_encode($request));
            $response = $GLOBALS['CI']->api_interface->get_json_response($service_url, json_encode($request), $header);

            //$GLOBALS['CI']->custom_db->generate_static_response(json_encode($response));
            if (valid_array($response) == true && empty($response['Status']) == false && $response['Status'] == ACTIVE) {
                //validate response and create session
                $authenticate_token = $response['TokenId'];
                return $authenticate_token;

            } else {
                return false;
            }
        }
    }

    /**
     *  Arjun J Gowda
     *
     *get hotel search request details
     *@param array $search_params data to be used while searching of hotels
     */
    private function hotel_search_request($search_params)
    {

        //$this->set_authenticate_token(true);
        $response['status'] = true;
        $response['data']   = array();
        $currency_obj       = new Currency(array('module_type' => 'hotel'));
        /** Request to be formed for search **/

        $request['BookingMode']       = 5; //value to be 5 for api users
        $request['CheckInDate']       = $search_params['from_date']; //dd/mm/yyyy
        $request['NoOfNights']        = $search_params['no_of_nights']; //Min 1
        $request['CountryCode']       = $search_params['country_code']; //ISO Country Code of Destination
        $request['CityId']            = intval($search_params['location_id']);
        $request['PreferredCurrency'] = get_application_default_currency(); //INR only
        $request['GuestNationality']  = $search_params['country_code']; //ISO Country Code
        $request['NoOfRooms']         = intval($search_params['room_count']);

        $room_index = $temp_child_index = 0;
        for ($room_index = 0; $room_index < $request['NoOfRooms']; $room_index++) {
            $temp_room_config               = '';
            $temp_room_config['NoOfAdults'] = intval($search_params['adult_config'][$room_index]);
            $temp_room_config['NoOfChild']  = intval($search_params['child_config'][$room_index]);
            if ($search_params['child_config'][$room_index] > 0) {
                $temp_room_config['ChildAge'] = array_slice($search_params['child_age'], $temp_child_index, intval($search_params['child_config'][$room_index]));
                $temp_child_index += intval($search_params['child_config'][$room_index]);
            }
            $request['RoomGuests'][] = $temp_room_config;
        }
        $request['PreferredHotel']        = '';
        $request['MinRating']             = 0;
        $request['MaxRating']             = 5;
        $request['IsNearBySearchAllowed'] = true;
        $request['SortBy']                = 0;
        $request['OrderBy']               = 0;
        $request['ResultCount']           = 0;
        $request['ReviewScore']           = 0;
        //debug($request);exit;
        $response['data']['request'] = json_encode($request);
        $this->credentials('GetHotelResult');
        $response['data']['service_url'] = $this->service_url;
        if ($request['NoOfNights'] > 29) {
            $response['status'] = false;
        }
        return $response;
    }

    /**
     *  Arjun J Gowda
     *
     * Hotel Details Request
     * @param string $TraceId
     * @param string $ResultIndex
     * @param string $HotelCode
     */
    private function hotel_details_request($TraceId, $ResultIndex, $HotelCode)
    {
        $response['status']     = true;
        $response['data']       = array();
        $request['TokenId']     = $this->get_authenticate_token();
        $request['TraceId']     = $TraceId;
        $request['ResultIndex'] = $ResultIndex;
        $request['HotelCode']   = $HotelCode;
        $request['EndUserIp']   = '127.0.0.1';

        $response['data']['request'] = json_encode($request);
        $this->credentials('GetHotelInfo');
        $response['data']['service_url'] = $this->service_url;
        return $response;
    }

    /**
     *  Arjun J Gowda
     *
     * Room Details Request
     * @param string $TraceId
     * @param string $ResultIndex
     * @param string $HotelCode
     */
    private function room_list_request($TraceId, $ResultIndex, $HotelCode)
    {
        $response['status']     = true;
        $response['data']       = array();
        $request['TokenId']     = $this->get_authenticate_token();
        $request['TraceId']     = $TraceId;
        $request['ResultIndex'] = $ResultIndex;
        $request['HotelCode']   = $HotelCode;
        $request['EndUserIp']   = '127.0.0.1';

        $response['data']['request'] = json_encode($request);
        $this->credentials('GetHotelRoom');
        $response['data']['service_url'] = $this->service_url;
        return $response;
    }

    /**
     *  Arjun J Gowda
     *
     * get room block request
     * @param array $booking_parameters
     */
    private function get_block_room_request($booking_params)
    {
        $number_of_nights     = $booking_params['search_data']['no_of_nights'];
        $response['status']   = true;
        $response['data']     = array();
        $request['TokenId']   = $this->get_authenticate_token();
        $request['TraceId']   = $booking_params['TraceId'];
        $request['EndUserIp'] = '127.0.0.1';

        $request['ResultIndex']       = intval($booking_params['ResultIndex']);
        $request['HotelCode']         = $booking_params['HotelCode'];
        $request['HotelName']         = $booking_params['HotelName'];
        $request['GuestNationality']  = $booking_params['GuestNationality'];
        $request['NoOfRooms']         = abs($booking_params['search_data']['room_count']);
        $request['IsVoucherBooking']  = true;
        $request['ClientReferenceNo'] = 0;
        $request['NoOfNights']        = $number_of_nights; //Jaganath
        $request['HotelRoomsDetails'] = '';
        $room_token_list              = $booking_params['token'];
        foreach ($room_token_list as $__room_key => $__room_value) {
            $request['HotelRoomsDetails'][$__room_key]['RoomIndex']         = intval($__room_value['RoomIndex']);
            $request['HotelRoomsDetails'][$__room_key]['RoomTypeCode']      = $__room_value['RoomTypeCode'];
            $request['HotelRoomsDetails'][$__room_key]['RoomTypeName']      = $__room_value['RoomTypeName'];
            $request['HotelRoomsDetails'][$__room_key]['RatePlanCode']      = $__room_value['RatePlanCode'];
            $request['HotelRoomsDetails'][$__room_key]['SmokingPreference'] = $__room_value['SmokingPreference'];
            $request['HotelRoomsDetails'][$__room_key]['RatePlanName']      = $__room_value['RatePlanName'];
            $request['HotelRoomsDetails'][$__room_key]['BedTypeCode']       = '';
            $request['HotelRoomsDetails'][$__room_key]['Supplements']       = '';

            //PRICE Details
            $request['HotelRoomsDetails'][$__room_key]['Price']['CurrencyCode']             = $__room_value['CurrencyCode'];
            $request['HotelRoomsDetails'][$__room_key]['Price']['RoomPrice']                = number_format($__room_value['RoomPrice'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['PublishedPrice']           = number_format($__room_value['PublishedPrice'], 3, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['PublishedPriceRoundedOff'] = abs($__room_value['PublishedPriceRoundedOff']);
            $request['HotelRoomsDetails'][$__room_key]['Price']['OfferedPrice']             = number_format($__room_value['OfferedPrice'], 3, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['OfferedPriceRoundedOff']   = abs($__room_value['OfferedPriceRoundedOff']);
            $request['HotelRoomsDetails'][$__room_key]['Price']['ServiceTax']               = number_format($__room_value['ServiceTax'], 2, '.', '');

            $request['HotelRoomsDetails'][$__room_key]['Price']['Tax']              = number_format(@$__room_value['Tax'], 3, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['ExtraGuestCharge'] = number_format($__room_value['ExtraGuestCharge'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['ChildCharge']      = number_format($__room_value['ChildCharge'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['OtherCharges']     = number_format($__room_value['OtherCharges'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['Discount']         = number_format($__room_value['Discount'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['AgentCommission']  = number_format($__room_value['AgentCommission'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['AgentMarkUp']      = number_format($__room_value['AgentMarkUp'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['TDS']              = number_format($__room_value['TDS'], 2, '.', '');
        }
        $response['data']['request'] = json_encode($request);
        $this->credentials('BlockRoom');
        $response['data']['service_url'] = $this->service_url;
        //debug($response); die;
        return $response;
    }

    /**
     *
     *Form Book Request
     *
     */

    public function get_book_request($booking_params, $booking_id, $auth_token)
    {
        /*echo 'Not Authorized To Do Booking!!!';
        exit;*/
        /*print_r($booking_params);
        exit;*/
        $search_id = $booking_params['search_id'];

        $safe_search_data = $GLOBALS['CI']->hotel_model->get_search_data($search_id);
        //debug($safe_search_data);exit;
        $search_data      = json_decode($safe_search_data['search_data'], true);
        $number_of_nights = get_date_difference(date('Y-m-d', strtotime($search_data['hotel_checkin'])), date('Y-m-d', strtotime($search_data['hotel_checkout'])));
        $NO_OF_ROOMS      = $search_data['rooms'];

        $search_params = $this->search_data($search_id);
        //debug($search_params);exit;
        $search_params = $search_params['data'];
        /*************Re-Assign the Pax Room Wise Strats******************************/
        // debug($booking_params);
        // echo "----";
        // echo "---";

        $room_wise_passenger_info = array();
        for ($i = 0; $i < $NO_OF_ROOMS; $i++) {

            $room_adult_count = $search_params['adult_config'][$i];
            $room_child_count = $search_params['child_config'][$i];

            foreach ($booking_params['name_title'] as $bk => $bv) {
                $pax_type = trim($booking_params['passenger_type'][$bk]);

                $assigned_pax_type_count = $this->get_assigned_pax_type_count(@$room_wise_passenger_info[$i]['passenger_type'], $pax_type);

                if (intval($pax_type) == 1 && intval($assigned_pax_type_count) < intval($room_adult_count)) {
//Adult
                    $room_wise_passenger_info[$i]['name_title'][]        = $booking_params['name_title'][$bk];
                    $room_wise_passenger_info[$i]['first_name'][]        = $booking_params['first_name'][$bk];
                    $room_wise_passenger_info[$i]['middle_name'][]       = $booking_params['middle_name'][$bk];
                    $room_wise_passenger_info[$i]['last_name'][]         = $booking_params['last_name'][$bk];
                    $room_wise_passenger_info[$i]['passenger_contact'][] = $booking_params['passenger_contact'];
                    $room_wise_passenger_info[$i]['billing_email'][]     = $booking_params['billing_email'];
                    $room_wise_passenger_info[$i]['passenger_type'][]    = $booking_params['passenger_type'][$bk];
                    $room_wise_passenger_info[$i]['date_of_birth'][]     = $booking_params['date_of_birth'][$bk];

                    //Remove the pax data from array
                    unset($booking_params['name_title'][$bk]);

                } else if (intval($pax_type) == 2 && intval($assigned_pax_type_count) < intval($room_child_count)) {
//Child
                    $room_wise_passenger_info[$i]['name_title'][]        = $booking_params['name_title'][$bk];
                    $room_wise_passenger_info[$i]['first_name'][]        = $booking_params['first_name'][$bk];
                    $room_wise_passenger_info[$i]['middle_name'][]       = $booking_params['middle_name'][$bk];
                    $room_wise_passenger_info[$i]['last_name'][]         = $booking_params['last_name'][$bk];
                    $room_wise_passenger_info[$i]['passenger_contact'][] = $booking_params['passenger_contact'];
                    $room_wise_passenger_info[$i]['billing_email'][]     = $booking_params['billing_email'];
                    $room_wise_passenger_info[$i]['passenger_type'][]    = $booking_params['passenger_type'][$bk];
                    $room_wise_passenger_info[$i]['date_of_birth'][]     = $booking_params['date_of_birth'][$bk];

                    //Remove the pax data from array
                    unset($booking_params['name_title'][$bk]);
                }
            }
        }

        /*************Re-Assign the Pax Room Wise Ends******************************/

        /* Counting No of adults and childs per room wise */

        for ($i = 0; $i < $NO_OF_ROOMS; $i++) {

            $booking_params['token']['token'][$i]['no_of_pax'] = $search_data['adult'][$i] + $search_data['child'][$i];
        }
        //debug($booking_params['token'] ['ResultIndex']);exit;
        /* Forming Request */
        $response['status']      = true;
        $response['data']        = array();
        $request['ResultToken']  = $booking_params['token']['ResultIndex'];
        $request['BlockRoomId']  = $booking_params['BlockRoomId'];
        $request['AppReference'] = trim($booking_id); // Jaganath
        $room_details            = array();
        $k                       = 0;

        for ($i = 0; $i < $NO_OF_ROOMS; $i++) {
            for ($j = 0; $j < $booking_params['token']['token'][$i]['no_of_pax']; $j++) {

                $pax_list = array(); // Reset Pax List Array

                $pax_list['Title']      = $room_wise_passenger_info[$i]['name_title'][$j];
                $pax_list['FirstName']  = $room_wise_passenger_info[$i]['first_name'][$j];
                $pax_list['MiddleName'] = $room_wise_passenger_info[$i]['middle_name'][$j];
                $pax_list['LastName']   = $room_wise_passenger_info[$i]['last_name'][$j];
                $pax_list['Phoneno']    = $room_wise_passenger_info[$i]['passenger_contact'][$j];
                $pax_list['Email']      = $room_wise_passenger_info[$i]['billing_email'][$j];
                $pax_list['PaxType']    = $room_wise_passenger_info[$i]['passenger_type'][$j];

                $pax_lead = false;

                if ($j == 0) {
                    $pax_lead = true;
                }
                $pax_list['LeadPassenger'] = $pax_lead;
                /* Age Calculation of Pax */
                $from                                               = new DateTime($room_wise_passenger_info[$i]['date_of_birth'][$j]);
                $to                                                 = new DateTime('today');
                $pax_age                                            = $from->diff($to)->y;
                $pax_list['Age']                                    = $pax_age;
                $request['RoomDetails'][$i]['PassengerDetails'][$j] = $pax_list;
                $k++;
            }
        }
//debug($request);exit;
        $response['data']['request'] = json_encode($request);
        $this->credentials('Book');
        $response['data']['service_url'] = $this->service_url;
        return $response;
    }
    /**
     * Jagnath
     * Cancellation Request:SendChangeRequest
     */
    private function send_change_request_params($BookingId)
    {
        $this->set_authenticate_token(true);
        $response['status']          = true;
        $response['data']            = array();
        $request['RequestType']      = 4;
        $request['Remarks']          = 'Cancellation in Provab Environnment';
        $request['BookingId']        = $BookingId;
        $request['EndUserIp']        = '127.0.0.1';
        $request['TokenId']          = $this->get_authenticate_token();
        $response['data']['request'] = json_encode($request);
        $this->credentials('SendChangeRequest');
        $response['data']['service_url'] = $this->service_url;
        return $response;
    }
    /**
     * Jagnath
     * Cancellation Status:GetChangeRequestStatus
     */
    private function get_change_request_status_params($ChangeRequestId)
    {
        $this->set_authenticate_token(true);
        $response['status']          = true;
        $response['data']            = array();
        $request['ChangeRequestId']  = $ChangeRequestId;
        $request['EndUserIp']        = '127.0.0.1';
        $request['TokenId']          = $this->get_authenticate_token();
        $response['data']['request'] = json_encode($request);
        $this->credentials('GetChangeRequestStatus');
        $response['data']['service_url'] = $this->service_url;
        return $response;
    }
    /**
     *  Arjun J Gowda
     * get search result from tbo
     * @param number $search_id unique id which identifies search details
     */
    public function get_hotel_list($search_id = '')
    {
        $this->CI->load->driver('cache');
        $header             = $this->get_header();
        $response['data']   = array();
        $response['status'] = true;
        $search_data        = $this->search_data($search_id);

        $cache_search = $this->CI->config->item('cache_hotel_search');
        $search_hash  = $this->search_hash;
        if ($cache_search) {
            $cache_contents = $this->CI->cache->file->get($search_hash);
        }

        if ($search_data['status'] == true) {
            if ($cache_search === false || ($cache_search === true && empty($cache_contents) == true)) {
                $search_request = $this->hotel_search_request($search_data['data']);
                if ($search_request['status']) {
                    $search_response = $GLOBALS['CI']->api_interface->get_json_response($search_request['data']['service_url'], $search_request['data']['request'], $header);
                    //debug($search_response);exit;
                    if ($this->valid_search_result($search_response)) {
                        $response['data'] = $search_response['Search'];
                        if ($cache_search) {
                            $cache_exp = $this->CI->config->item('cache_hotel_search_ttl');
                            $this->CI->cache->file->save($search_hash, $response['data'], $cache_exp);
                        }
                        //Log Hotels Count
                        $this->cache_result_hotel_count($search_response);
                    } else {
                        $response['status'] = false;
                    }
                } else {
                    $response['status'] = false;
                }
            } else {
                //read from cache
                $response['data'] = $cache_contents;
            }
        } else {
            $response['status'] = false;
        }
        //debug($response);exit;
        return $response;
    }

    /**
     * Get Cache list of hotels
     * @param number $search_id
     */
    /*    function get_cache_hotel_list($search_id)
    {
    $data = array();
    $status = false;
    $this->CI->load->driver('cache');

    $search_data = $this->search_data($search_id);
    $search_hash = $this->search_hash;
    $cache_contents = $this->CI->cache->file->get($search_hash);
    if (valid_array($cache_contents) == true) {
    $data = $cache_contents;
    $status = true;
    }
    return array('data' => $data, 'status' => $status);
    }*/

    public function cache_result_hotel_count($response)
    {
        $CI          = &get_instance();
        $city_id     = intval(@$response['HotelSearchResult']['CityId']);
        $hotel_count = intval(count(@$response['HotelSearchResult']['HotelResults']));
        if ($hotel_count > 0 && $city_id > 0) {
            $CI->custom_db->update_record('hotels_city', array('cache_hotels_count' => $hotel_count), array('origin' => $city_id));
        }
    }

    /**
     *  Arjun J Gowda
     * get Room List for selected hotel
     * @param string $TraceId
     * @param number $ResultIndex
     * @param string $HotelCode
     */
    public function get_room_list($TraceId, $ResultIndex, $HotelCode)
    {
        $header             = $this->get_header();
        $response['data']   = array();
        $response['status'] = false;
        $hotel_room_request = $this->room_list_request($TraceId, $ResultIndex, $HotelCode);
        if ($hotel_room_request['status']) {
            //get the response for hotel details
            $hotel_room_list_response = $GLOBALS['CI']->api_interface->get_json_response($hotel_room_request['data']['service_url'], $hotel_room_request['data']['request'], $header);
            //$GLOBALS['CI']->custom_db->generate_static_response(json_encode($hotel_room_list_response));

            /*$static_search_result_id = 813;//106;//68;//52;
            $hotel_room_list_response = $GLOBALS['CI']->hotel_model->get_static_response($static_search_result_id);*/
            if ($this->valid_room_details_details($hotel_room_list_response)) {
                $response['data']   = $hotel_room_list_response;
                $response['status'] = true;
            } else {
                //Need the complete data so that later we can use it for redirection
                $response['data'] = $hotel_room_list_response;
            }
        }
        return $response;
    }

    /**
     *  Arjun J Gowda
     *Load Hotel Details
     *
     * @param string $TraceId        Trace ID of hotel found in search result response
     * @param number $ResultIndex    Result index generated for each hotel by hotel search
     * @param string $HotelCode        unique id which identifies hotel
     *
     * @return array having status of the operation and resulting data in case if operaiton is successfull
     */
    public function get_hotel_details($TraceId, $ResultIndex, $HotelCode)
    {
        $header                = $this->get_header();
        $response['data']      = array();
        $response['status']    = false;
        $hotel_details_request = $this->hotel_details_request($TraceId, $ResultIndex, $HotelCode);
        if ($hotel_details_request['status']) {
            //get the response for hotel details
            $hotel_details_response = $GLOBALS['CI']->api_interface->get_json_response($hotel_details_request['data']['service_url'], $hotel_details_request['data']['request'], $header);
            //$GLOBALS['CI']->custom_db->generate_static_response(json_encode($hotel_details_response));
            /*$static_search_result_id = 812;//105;//67;//49;
            $hotel_details_response = $GLOBALS['CI']->hotel_model->get_static_response($static_search_result_id);*/
            if ($this->valid_hotel_details($hotel_details_response)) {
                $response['data']   = $hotel_details_response;
                $response['status'] = true;
            } else {
                //Need the complete data so that later we can use it for redirection
                $response['data'] = $hotel_details_response;
            }
        }
        return $response;
    }

    /**
     * Arjun J Gowda
     * Block Room Before Going for payment and showing final booking page to user - TBO rule
     * @param array $pre_booking_params    All the necessary data required in block room request - fetched from roomList and hotelDetails Request
     */
    public function block_room($pre_booking_params)
    {
        $header                            = $this->get_header();
        $response['status']                = false;
        $response['data']                  = array();
        $search_data                       = $this->search_data($pre_booking_params['search_id']);
        $run_block_room_request            = true;
        $block_room_request_count          = 0;
        $pre_booking_params['search_data'] = $search_data['data'];
        //debug($pre_booking_params); die;
        $block_room_request = $this->get_block_room_request($pre_booking_params);
        //debug($block_room_request);exit('block room RQ');
        $application_default_currency = get_application_default_currency();
        if ($block_room_request['status'] == ACTIVE) {
            while ($run_block_room_request) {
                //$GLOBALS['CI']->custom_db->generate_static_response(json_encode($block_room_request['data']['request']));
                $block_room_response = $GLOBALS['CI']->api_interface->get_json_response($block_room_request['data']['service_url'], $block_room_request['data']['request'], $header);

                //$GLOBALS['CI']->custom_db->generate_static_response(json_encode($block_room_response)); //release this

                /*$static_search_result_id = 309;//161;//184;//169;//197; -- SINGLE*/
                /*$static_search_result_id = 815;//202;// -- MULTIPLE
                $block_room_response = $GLOBALS['CI']->hotel_model->get_static_response($static_search_result_id); //cmt this*/

                if ($this->valid_response($block_room_response['BlockRoomResult']['ResponseStatus']) == false) {
                    $run_block_room_request  = false;
                    $response['status']      = false; // Indication for room block
                    $response['data']['msg'] = 'Some Problem Occured. Please Search Again to continue';
                } elseif ($this->is_room_blocked($block_room_response) == true) {
                    $run_block_room_request = false;
                    $response['status']     = true; // Indication for room block
                } else {
                    //UPDATE RECURSSION
                    $_HotelRoomsDetails = get_room_index_list($block_room_response['BlockRoomResult']['HotelRoomsDetails']);
                    //Reset pre booking params token and get new values
                    $dynamic_params_url = '';
                    foreach ($_HotelRoomsDetails as $___tk => $___tv) {
                        $dynamic_params_url[] = get_dynamic_booking_parameters($___tk, $___tv, $application_default_currency);
                    }
                    //update token key
                    $pre_booking_params['token']     = $dynamic_params_url;
                    $pre_booking_params['token_key'] = md5(serialized_data($dynamic_params_url));
                    $block_room_request              = $this->get_block_room_request($pre_booking_params);
                }
                $block_room_request_count++; //Increment number of times request is run
                if ($block_room_request_count == 3 && $run_block_room_request == true) {
                    //try max 3times to block the room
                    $run_block_room_request = false;
                }
            }
            $response['data']['response'] = $block_room_response;
        }

        return $response;
    }

    /**
     *
     * @param array $booking_params
     */
    public function process_booking($book_id, $booking_params, $auth_token)
    {
        $header             = $this->get_header();
        $response['status'] = FAILURE_STATUS;
        $response['data']   = array();

        //form request    done
        //run request to get response and store response in db for dev done
        $book_request = $this->get_book_request($booking_params, $book_id, $auth_token);
        //echo 'Hi';
        //debug($booking_params);exit;
        $block_data_array    = $book_request['data']['request'];
        $tmp                 = array();
        $tmp['ResultToken']  = $booking_params['ProvabAuthKey'];
        $tmp['BlockRoomId']  = $booking_params['BlockRoomId'];
        $tmp['AppReference'] = $book_id;
        for ($i = 0; $i < count($booking_params['passenger_type']); $i++) {
            $tmp['RoomDetails'][0]['PassengerDetails'][$i]['Title']      = $booking_params['name_title'][$i];
            $tmp['RoomDetails'][0]['PassengerDetails'][$i]['FirstName']  = $booking_params['first_name'][$i];
            $tmp['RoomDetails'][0]['PassengerDetails'][$i]['MiddleName'] = '';
            $tmp['RoomDetails'][0]['PassengerDetails'][$i]['LastName']   = $booking_params['last_name'][$i];
            $tmp['RoomDetails'][0]['PassengerDetails'][$i]['Phoneno']    = $booking_params['passenger_contact'];
            $tmp['RoomDetails'][0]['PassengerDetails'][$i]['Email']      = $booking_params['billing_email'];
            $tmp['RoomDetails'][0]['PassengerDetails'][$i]['PaxType']    = $booking_params['passenger_type'][$i];
            if ($booking_params['lead_passenger'][$i] == 1) {
                $tmp['RoomDetails'][0]['PassengerDetails'][$i]['LeadPassenger'] = true;

            } else {
                $tmp['RoomDetails'][0]['PassengerDetails'][$i]['LeadPassenger'] = false;
            }
            //$tmp['RoomDetails'][0]['PassengerDetails'][$i]['LeadPassenger'] = $booking_params['lead_passenger'][$i];
            $tmp['RoomDetails'][0]['PassengerDetails'][$i]['Age'] = 30;
        }
        /*print_r($tmp);
        exit();*/
        if ($book_request['status'] == SUCCESS_STATUS) {
            $GLOBALS['CI']->custom_db->generate_static_response($block_data_array); //release this
            $book_response = $GLOBALS['CI']->api_interface->get_json_response($book_request['data']['service_url'], json_encode($tmp), $header);

            //debug($book_response);exit;
            $GLOBALS['CI']->custom_db->generate_static_response(json_encode($book_response));

            $api_book_response_status = $book_response['Status'];

            if ($api_book_response_status == SUCCESS_STATUS) {
                $book_response['BookResult']                       = $book_response['CommitBooking']['BookingDetails'];
                $book_response['BookResult']['HotelBookingStatus'] = $book_response['Status'];
                /**    PROVAB LOGGER **/
                $GLOBALS['CI']->private_management_model->provab_xml_logger('Book_Room', $book_id, 'hotel', $block_data_array, json_encode($book_response));
                //validate response
                if (valid_array($book_response)) {
                    $response['status']                 = SUCCESS_STATUS;
                    $response['data']['book_response']  = $book_response;
                    $response['data']['booking_params'] = $booking_params;

                    // Convert Room Book Data in Application Currency
                    $block_data_array                    = $book_request['data']['request'];
                    $room_book_data                      = json_decode($block_data_array, true);
                    $room_book_data['HotelRoomsDetails'] = $this->formate_hotel_room_details($booking_params);

                    $response['data']['room_book_data'] = $this->convert_roombook_data_to_application_currency($room_book_data);

                }
            }
        }

        return $response;
    }
    /**
     * Jaganath
     * Convert Room Book Data in Application Currency
     *
     * @param
     *            $currency_obj
     */
    private function convert_roombook_data_to_application_currency($room_book_data)
    {
        $application_default_currency = admin_base_currency();
        $currency_obj                 = new Currency(array(
            'module_type' => 'hotel',
            'from'        => get_api_data_currency(),
            'to'          => admin_base_currency(),
        ));
        $master_room_book_data = array();
        $HotelRoomsDetails     = array();
        foreach ($room_book_data['HotelRoomsDetails'] as $hrk => $hrv) {
            $HotelRoomsDetails[$hrk]          = $hrv;
            $HotelRoomsDetails[$hrk]['Price'] = $this->preferred_currency_fare_object($hrv['Price'], $currency_obj, $application_default_currency);
        }
        $master_room_book_data                      = $room_book_data;
        $master_room_book_data['HotelRoomsDetails'] = $HotelRoomsDetails;
        return $master_room_book_data;
    }
    /**
     * Jaganath
     *
     * @param unknown_type $room_list
     * @param unknown_type $currency_obj
     */
    public function roomlist_in_preferred_currency($room_list, $currency_obj, $search_id, $module_type = 'b2c', $user_id = 0)
    {
        $application_currency_preference = get_application_currency_preference();
        //echo $application_currency_preference;exit;
        $hotel_room_details = $room_list['data']['GetHotelRoomResult']['HotelRoomsDetails'];
        $hotel_room_result  = array();
        foreach ($hotel_room_details as $hr_k => $hr_v) {
            $hotel_room_result[$hr_k] = $hr_v;
            // Price
            $API_raw_price  = $hr_v['Price'];
            $level_one      = true;
            $current_domain = true;
            if ($module == 'b2c') {
                $level_one      = false;
                $current_domain = true;
            } else if ($module == 'b2b') {
                $level_one      = true;
                $current_domain = true;
            }
            // debug($hr_v ['Price']);
            $hr_v['Price'] = $this->update_search_markup_currency($hr_v['Price'], $currency_obj, $search_id, $level_one, $current_domain, $module_type, $user_id);
            // debug($hr_v['Price']);exit;
            $hr_v['Price']['Tax'] = $hr_v['Price']['PublishedPriceRoundedOff'] - $hr_v['Price']['RoomPrice'];
            $Price                = $this->preferred_currency_fare_object($hr_v['Price'], $currency_obj);

            //echo "fsdfs";exit;
            // CancellationPolicies
            $CancellationPolicies = array();
            foreach ($hr_v['CancellationPolicies'] as $ck => $cv) {
                //add cancellation charge in markup

                $Charge = $this->update_cancellation_markup_currency($cv['Charge'], $currency_obj, $search_id);

                $CancellationPolicies[$ck]             = $cv;
                $CancellationPolicies[$ck]['Currency'] = $application_currency_preference;
                //$CancellationPolicies [$ck] ['Charge'] = get_converted_currency_value ( $currency_obj->force_currency_conversion ( $Charge ) );
                $CancellationPolicies[$ck]['Charge'] = $Charge;

            }
            $hotel_room_result[$hr_k]['API_raw_price']        = $API_raw_price;
            $hotel_room_result[$hr_k]['Price']                = $Price;
            $hotel_room_result[$hr_k]['CancellationPolicies'] = $CancellationPolicies;
            // CancellationPolicy:FIXME: convert the INR price to preferred currency
        }
        $room_list['data']['GetHotelRoomResult']['HotelRoomsDetails'] = $hotel_room_result;
        //debug($room_list);exit;
        return $room_list;
    }
    /**
     * Jaganath
     *
     * @param unknown_type $fare_details
     * @param unknown_type $currency_obj
     */
    private function preferred_currency_fare_object($fare_details, $currency_obj, $default_currency = '')
    {
        $price_details = array();
        //debug($fare_details);

        $price_details['CurrencyCode'] = empty($default_currency) == false ? $default_currency : get_application_currency_preference();

        $price_details['RoomPrice'] = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['RoomPrice']));

        $price_details['Tax'] = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['Tax']));

        $price_details['ExtraGuestCharge'] = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['ExtraGuestCharge']));

        $price_details['ChildCharge']              = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['ChildCharge']));
        $price_details['OtherCharges']             = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['OtherCharges']));
        $price_details['Discount']                 = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['Discount']));
        $price_details['PublishedPrice']           = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['PublishedPrice']));
        $price_details['PublishedPriceRoundedOff'] = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['PublishedPriceRoundedOff']));
        $price_details['OfferedPrice']             = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['OfferedPrice']));
        $price_details['OfferedPriceRoundedOff']   = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['OfferedPriceRoundedOff']));
        $price_details['AgentCommission']          = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['AgentCommission']));
        $price_details['AgentMarkUp']              = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['AgentMarkUp']));
        $price_details['ServiceTax']               = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['ServiceTax']));
        $price_details['TDS']                      = get_converted_currency_value($currency_obj->force_currency_conversion($fare_details['TDS']));
        //debug($price_details);exit;
        return $price_details;
    }
    /**
     * Formates Hotel Room Details
     * @param unknown_type $booking_params
     */
    private function formate_hotel_room_details($booking_params)
    {
        $search_id        = $booking_params['token']['search_id'];
        $safe_search_data = $GLOBALS['CI']->hotel_model->get_search_data($search_id);
        $search_data      = json_decode($safe_search_data['search_data'], true);
        $number_of_nights = get_date_difference(date('Y-m-d', strtotime($search_data['hotel_checkin'])), date('Y-m-d', strtotime($search_data['hotel_checkout'])));
        $NO_OF_ROOMS      = $search_data['rooms'];
        $k                = 0;
        //    debug($booking_params);exit;

        $HotelRoomsDetails = array();
        /* Counting No of adults and childs per room wise */
        for ($i = 0; $i < $NO_OF_ROOMS; $i++) {
            $booking_params['token']['token'][$i]['no_of_pax'] = $search_data['adult'][$i] + $search_data['child'][$i];
        }
        $currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
        for ($i = 0; $i < $NO_OF_ROOMS; $i++) {
            $room_detail                      = array();
            $room_detail['RoomIndex']         = $booking_params['token']['token'][$i]['RoomIndex'];
            $room_detail['RatePlanCode']      = $booking_params['token']['token'][$i]['RatePlanCode'];
            $room_detail['RatePlanName']      = $booking_params['token']['token'][$i]['RatePlanName'];
            $room_detail['RoomTypeCode']      = $booking_params['token']['token'][$i]['RoomTypeCode'];
            $room_detail['RoomTypeName']      = $booking_params['token']['token'][$i]['RoomTypeName'];
            $room_detail['SmokingPreference'] = 0;

            $room_detail['Price']['CurrencyCode']             = $booking_params['token']['token'][$i]['CurrencyCode'];
            $room_detail['Price']['CurrencySymbol']           = $currency_obj->get_currency_symbol($currency_obj->to_currency);
            $room_detail['Price']['RoomPrice']                = $booking_params['token']['token'][$i]['RoomPrice'];
            $room_detail['Price']['Tax']                      = $booking_params['token']['token'][$i]['Tax'];
            $room_detail['Price']['ExtraGuestCharge']         = $booking_params['token']['token'][$i]['ExtraGuestCharge'];
            $room_detail['Price']['ChildCharge']              = $booking_params['token']['token'][$i]['ChildCharge'];
            $room_detail['Price']['OtherCharges']             = $booking_params['token']['token'][$i]['OtherCharges'];
            $room_detail['Price']['Discount']                 = $booking_params['token']['token'][$i]['Discount'];
            $room_detail['Price']['PublishedPrice']           = $booking_params['token']['token'][$i]['PublishedPrice'];
            $room_detail['Price']['PublishedPriceRoundedOff'] = $booking_params['token']['token'][$i]['PublishedPriceRoundedOff'];
            $room_detail['Price']['OfferedPrice']             = $booking_params['token']['token'][$i]['OfferedPrice'];
            $room_detail['Price']['OfferedPriceRoundedOff']   = $booking_params['token']['token'][$i]['OfferedPriceRoundedOff'];
            $room_detail['Price']['SmokingPreference']        = $booking_params['token']['token'][$i]['SmokingPreference'];
            $room_detail['Price']['ServiceTax']               = $booking_params['token']['token'][$i]['ServiceTax'];
            $room_detail['Price']['Tax']                      = $booking_params['token']['token'][$i]['Tax'];
            $room_detail['Price']['ExtraGuestCharge']         = $booking_params['token']['token'][$i]['ExtraGuestCharge'];
            $room_detail['Price']['ChildCharge']              = $booking_params['token']['token'][$i]['ChildCharge'];
            $room_detail['Price']['OtherCharges']             = $booking_params['token']['token'][$i]['OtherCharges'];
            $room_detail['Price']['Discount']                 = $booking_params['token']['token'][$i]['Discount'];
            $room_detail['Price']['AgentCommission']          = $booking_params['token']['token'][$i]['AgentCommission'];
            $room_detail['Price']['AgentMarkUp']              = $booking_params['token']['token'][$i]['AgentMarkUp'];
            $room_detail['Price']['TDS']                      = $booking_params['token']['token'][$i]['TDS'];
            $HotelRoomsDetails[$i]                            = $room_detail;

            //debug($room_detail);exit;

            for ($j = 0; $j < $booking_params['token']['token'][$i]['no_of_pax']; $j++) {
                $pax_list = array(); // Reset Pax List Array
                //$pax_title = get_enum_list ( 'title', $booking_params ['name_title'] [$k] );
                $pax_title              = $booking_params['name_title'][$k];
                $pax_list['Title']      = $pax_title;
                $pax_list['FirstName']  = $booking_params['first_name'][$k];
                $pax_list['MiddleName'] = $booking_params['last_name'][$k];
                $pax_list['LastName']   = $booking_params['last_name'][$k];
                $pax_list['Phoneno']    = $booking_params['passenger_contact'];
                $pax_list['Email']      = $booking_params['billing_email'];
                $pax_list['PaxType']    = $booking_params['passenger_type'][$k];

                $pax_lead = false;
                // temp
                if ($j == 0) {
                    $pax_lead = true;
                }
                $pax_list['LeadPassenger'] = $pax_lead;
                /* Age Calculation of Pax */
                $from                                        = new DateTime($booking_params['date_of_birth'][$k]);
                $to                                          = new DateTime('today');
                $pax_age                                     = $from->diff($to)->y;
                $pax_list['Age']                             = $pax_age;
                $HotelRoomsDetails[$i]['HotelPassenger'][$j] = $pax_list;
                $k++;
            }
        }
        return $HotelRoomsDetails;
    }
    /**
     * Reference number generated for booking from application
     * @param $app_booking_id
     * @param $params
     */
    public function save_booking($app_booking_id, $params, $module = 'b2c', $user_id = '')
    {
        /*ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);

        debug(array(
            'app_booking_id'=>$app_booking_id,
            'params'=>$params,
            'module'=>$module,
            'user_id'=>$user_id,
        ));
        exit();*/
        //Need to return following data as this is needed to save the booking fare in the transaction details

        $response['fare']       = $response['domain_markup']       = $response['level_one_markup']       = 0;
        $domain_origin          = get_domain_auth_id();
        $master_search_id       = $params['booking_params']['token']['search_id'];
        $search_data            = $this->search_data($master_search_id);
        $status                 = get_booking_status($params['book_response']['BookResult']['HotelBookingStatus']);
        $app_reference          = $app_booking_id;
        $booking_source         = $params['booking_params']['token']['booking_source'];
        $booking_id             = $params['book_response']['BookResult']['BookingId'];
        $booking_reference      = $params['book_response']['BookResult']['BookingRefNo'];
        $confirmation_reference = $params['book_response']['BookResult']['ConfirmationNo'];
        $no_of_nights           = intval($search_data['data']['no_of_nights']);
        $HotelRoomsDetails      = force_multple_data_format($params['room_book_data']['HotelRoomsDetails']);
        $total_room_count       = count($HotelRoomsDetails);

        $book_total_fare   = $params['booking_params']['token']['price_summary']['OfferedPriceRoundedOff']; //(TAX+ROOM PRICE)
        $currency_obj      = $params['currency_obj'];

        $loyality_points = $params['booking_params']['loyality_points'];

        $deduction_cur_obj = clone $currency_obj;

        $room_price = $params['booking_params']['token']['price_summary']['RoomPrice'];

        if ($module == 'b2c') {
            $markup_total_fare = $currency_obj->get_currency($book_total_fare, true, false, true, $no_of_nights * $total_room_count); //(ON Total PRICE ONLY)
            $ded_total_fare    = $deduction_cur_obj->get_currency($book_total_fare, true, true, false, $no_of_nights * $total_room_count); //(ON Total PRICE ONLY)
            $admin_markup      = sprintf("%.2f", $markup_total_fare['default_value'] - $ded_total_fare['default_value']);
            $agent_markup      = sprintf("%.2f", $ded_total_fare['default_value'] - $book_total_fare);
        } else {
            //B2B Calculation
            $markup_total_fare = $currency_obj->get_currency($book_total_fare, true, true, true, $no_of_nights * $total_room_count, [], $module, $user_id); //(ON Total PRICE ONLY)
            $ded_total_fare    = $deduction_cur_obj->get_currency($book_total_fare, true, false, true, $no_of_nights * $total_room_count, [], $module, $user_id); //(ON Total PRICE ONLY)
            $admin_markup      = sprintf("%.2f", $markup_total_fare['default_value'] - $ded_total_fare['default_value']);
            $agent_markup      = sprintf("%.2f", $ded_total_fare['default_value'] - $book_total_fare);
        }
        $currency         = $params['booking_params']['token']['default_currency'];
        $hotel_name       = $params['booking_params']['token']['HotelName'];
        $star_rating      = $params['booking_params']['token']['StarRating'];
        $hotel_code       = $params['booking_params']['token']['HotelCode'];
        $phone_number     = $params['booking_params']['passenger_contact'];
        $alternate_number = 'NA';
        $email            = $params['booking_params']['billing_email'];
        $hotel_check_in   = db_current_datetime(str_replace('/', '-', $search_data['data']['from_date']));
        $hotel_check_out  = db_current_datetime(str_replace('/', '-', $search_data['data']['to_date']));
        $payment_mode     = $params['booking_params']['payment_method'];

        $country_name = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $params['booking_params']['billing_country']));
        //$city_name = $GLOBALS['CI']->db_cache_api->get_city_list(array('k' => 'origin', 'v' => 'destination'), array('origin' => $params['booking_params']['billing_city']));
        $attributes = array(
            'address'            => @$params['booking_params']['billing_address_1'],
            'billing_country'    => @$country_name[$params['booking_params']['billing_country']],
            //'billing_city' => $city_name[$params['booking_params']['billing_city']],
            'billing_city'       => @$params['booking_params']['billing_city'],
            'billing_zipcode'    => @$params['booking_params']['billing_zipcode'],
            'HotelCode'          => @$params['booking_params']['token']['HotelCode'],
            'search_id'          => @$params['booking_params']['token']['search_id'],
            'TraceId'            => @$params['booking_params']['token']['TraceId'],
            'HotelName'          => @$params['booking_params']['token']['HotelName'],
            'StarRating'         => @$params['booking_params']['token']['StarRating'],
            'HotelImage'         => @$params['booking_params']['token']['HotelImage'],
            'HotelAddress'       => @$params['booking_params']['token']['HotelAddress'],
            'CancellationPolicy' => @$params['booking_params']['token']['CancellationPolicy'],
        );
        //$created_by_id        = intval(@$GLOBALS['CI']->entity_user_id);
        if (isset($user_id) && $user_id != '') {

            $created_by_id = $user_id;
        } else {
            // $created_by_id = intval(@$GLOBALS['CI']->entity_user_id);
            $created_by_id = ($params['booking_params']['user_id'])?$params['booking_params']['user_id']:intval(@$GLOBALS['CI']->entity_user_id);

        }
        //$created_by_id        = intval(trim(@$params['booking_params']['customer_id']));
        // echo $created_by_id; exit();
        $transaction_currency     = get_application_currency_preference();
        $application_currency     = admin_base_currency();
        $currency_conversion_rate = $currency_obj->transaction_currency_conversion_rate();

        //SAVE Booking details
        $GLOBALS['CI']->hotel_model->save_booking_details(
            $domain_origin, $status, $app_reference, $booking_source, $booking_id, $booking_reference, $confirmation_reference,
            $hotel_name, $star_rating, $hotel_code, $phone_number,
            $alternate_number, $email, $hotel_check_in, $hotel_check_out, $payment_mode, json_encode($attributes), $created_by_id, $transaction_currency, $currency_conversion_rate,$loyality_points);

        $check_in  = db_current_datetime(str_replace('/', '-', $search_data['data']['from_date']));
        $check_out = db_current_datetime(str_replace('/', '-', $search_data['data']['to_date']));

        $location = $search_data['data']['location'];
        //loop token of token
        foreach ($HotelRoomsDetails as $k => $v) {
            $room_type_name = $v['RoomTypeName'];
            if (isset($v['BedTypeCode'])) {
                $bed_type_code = $v['BedTypeCode'];
            } else {
                $bed_type_code = '';
            }

            $smoking_preference = get_smoking_preference($v['SmokingPreference']);
            $smoking_preference = $smoking_preference['label'];
            $total_fare         = $v['Price']['OfferedPriceRoundedOff'];
            $room_price         = $v['Price']['RoomPrice'];

            if ($module == 'b2c') {
                $markup_total_fare = $currency_obj->get_currency($total_fare, true, false, true, $no_of_nights); //(ON Total PRICE ONLY)
                $ded_total_fare    = $deduction_cur_obj->get_currency($total_fare, true, true, false, $no_of_nights); //(ON Total PRICE ONLY)
                $admin_markup      = sprintf("%.2f", $markup_total_fare['default_value'] - $ded_total_fare['default_value']);
                $agent_markup      = sprintf("%.2f", $ded_total_fare['default_value'] - $total_fare);
            } else {
                //B2B Calculation - Room wise price
                $markup_total_fare = $currency_obj->get_currency($total_fare, true, true, true, $no_of_nights); //(ON Total PRICE ONLY)
                $ded_total_fare    = $deduction_cur_obj->get_currency($total_fare, true, false, true, $no_of_nights); //(ON Total PRICE ONLY)
                $admin_markup      = sprintf("%.2f", $markup_total_fare['default_value'] - $ded_total_fare['default_value']);
                $agent_markup      = sprintf("%.2f", $ded_total_fare['default_value'] - $total_fare);
            }

            $attributes = '';
            //SAVE Booking Itinerary details
            $GLOBALS['CI']->hotel_model->save_booking_itinerary_details(
                $app_reference, $location, $check_in, $check_out, $room_type_name, $bed_type_code, $status, $smoking_preference,
                $total_fare, $admin_markup, $agent_markup, $currency, $attributes,
                @$v['RoomPrice'], @$v['Tax'], @$v['ExtraGuestCharge'], @$v['ChildCharge'], @$v['OtherCharges'],
                @$v['Discount'], @$v['ServiceTax'], @$v['AgentCommission'], @$v['AgentMarkUp'], @$v['TDS']
            );
            $passengers = force_multple_data_format($v['HotelPassenger']);
            if (valid_array($passengers) == true) {
                foreach ($passengers as $passenger) {
                    $title         = $passenger['Title'];
                    $first_name    = $passenger['FirstName'];
                    $middle_name   = $passenger['MiddleName'];
                    $last_name     = $passenger['LastName'];
                    $phone         = $passenger['Phoneno'];
                    $email         = $passenger['Email'];
                    $pax_type      = $passenger['PaxType'];
                    $date_of_birth = array_shift($params['booking_params']['date_of_birth']); //

                    $passenger_nationality_id    = array_shift($params['booking_params']['passenger_nationality']); //
                    $passport_issuing_country_id = array_shift($params['booking_params']['passenger_passport_issuing_country']); //

                    $passenger_nationality    = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passenger_nationality_id));
                    $passport_issuing_country = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $passport_issuing_country_id));

                    $passenger_nationality    = $passenger_nationality[$passenger_nationality_id];
                    $passport_issuing_country = $passport_issuing_country[$passport_issuing_country_id];
                    $passport_number          = array_shift($params['booking_params']['passenger_passport_number']); //
                    $passport_expiry_date     = array_shift($params['booking_params']['passenger_passport_expiry_year']) . '-' . array_shift($params['booking_params']['passenger_passport_expiry_month']) . '-' . array_shift($params['booking_params']['passenger_passport_expiry_day']); //
                    $attributes               = array();

                    //SAVE Booking Pax details
                    $GLOBALS['CI']->hotel_model->save_booking_pax_details(
                        $app_reference, $title, $first_name, $middle_name, $last_name, $phone, $email, $pax_type, $date_of_birth,
                        $passenger_nationality, $passport_number, $passport_issuing_country, $passport_expiry_date, $status, serialize($attributes)
                    );
                }
            }
        }

        /************** Update Convinence Fees And Other Details Start ******************/
        //Convinence_fees to be stored and discount
        $convinence         = 0;
        $discount           = 0;
        $convinence_value   = 0;
        $convinence_type    = 0;
        $convinence_per_pax = 0;
        $promo_code         = '';

        if ($module == 'b2c') {
            $convinence         = $currency_obj->convenience_fees($book_total_fare, $master_search_id);
            $convinence_row     = $currency_obj->get_convenience_fees();
            $convinence_value   = $convinence_row['value'];
            $convinence_type    = $convinence_row['type'];
            $convinence_per_pax = $convinence_row['per_pax'];
            $discount           = @$params['booking_params']['promo_code_discount_val'];
            $promo_code         = @$params['booking_params']['promo_code'];
        } elseif ($module == 'b2b') {
            $discount = 0;
        }
        $GLOBALS['CI']->load->model('transaction');
        //SAVE Booking convinence_discount_details details
        $GLOBALS['CI']->transaction->update_convinence_discount_details('hotel_booking_details', $app_reference, $discount, $promo_code, $convinence, $convinence_value, $convinence_type, $convinence_per_pax, $promo_code);
        /************** Update Convinence Fees And Other Details End ******************/

        $response['fare']                     = $book_total_fare;
        $response['currency_conversion_rate'] = $currency_conversion_rate;
        $response['admin_markup']             = $admin_markup;
        $response['agent_markup']             = $agent_markup;
        $response['convinence']               = $convinence;
        $response['discount']                 = $discount;
        return $response;
    }
    /**
     *Get hotel images
     * @param hotel_code
     */
    public function get_hotel_images($hotel_code)
    {
        $header             = $this->get_header();
        $response['data']   = array();
        $response['status'] = true;
        if ($hotel_code != '') {
            $this->credentials('GetHotelImages');
            $url            = $this->service_url;
            $request        = json_encode(array('hotel_code' => $hotel_code));
            $image_response = $GLOBALS['CI']->api_interface->get_json_response($url, $request, $header);
            if ($image_response['Status'] == true) {
                $response['data'] = $image_response['GetHotelImages'];
            } else {
                $response['status'] = false;
            }
        } else {
            $response['status'] = false;
        }
        return $response;
    }
    /**
     * Cancel Booking
     */
    public function cancel_booking_mobile($booking_details)
    {
        $header                 = $this->get_header();
        $response['data']       = array();
        $response['status']     = FAILURE_STATUS;
        $resposne['msg']        = 'Remote IO Error';
        $BookingId              = $booking_details['booking_id'];
        $app_reference          = $booking_details['app_reference'];
        $cancel_booking_request = $this->cancel_booking_request_params($app_reference);
        if ($cancel_booking_request['status']) {
            // 1.SendChangeRequest
            $cancel_booking_response = $GLOBALS['CI']->api_interface->get_json_response($cancel_booking_request['data']['service_url'], $cancel_booking_request['data']['request'], $header);
            $GLOBALS['CI']->custom_db->generate_static_response(json_encode($cancel_booking_response));

            // $cancel_booking_response = $GLOBALS['CI']->hotel_model->get_static_response(3317);
            if (valid_array($cancel_booking_response) == true && $cancel_booking_response['Status'] == SUCCESS_STATUS) {

                // Save Cancellation Details
                $hotel_cancellation_details = $cancel_booking_response['CancelBooking']['CancellationDetails'];
                $GLOBALS['CI']->hotel_model->update_cancellation_details($app_reference, $hotel_cancellation_details);
                $response['status'] = SUCCESS_STATUS;

            } else {
                $response['msg'] = $cancel_booking_response['Message'];
            }
        }
        return $response;
    }
    /**
     * Jaganath
     * Cancel Booking
     */
    public function cancel_booking($booking_details)
    {
        //1.SendChangeRequest
        //2.GetChangeRequestStatus
        $header              = $this->get_header();
        $response['data']    = array();
        $response['status']  = FAILURE_STATUS;
        $resposne['msg']     = 'Remote IO Error';
        $BookingId           = $booking_details['booking_id'];
        $app_reference       = $booking_details['app_reference'];
        $send_change_request = $this->send_change_request_params($BookingId);
        if ($send_change_request['status']) {
            //1.SendChangeRequest
            $send_change_request_response = $GLOBALS['CI']->api_interface->get_json_response($send_change_request['data']['service_url'], $send_change_request['data']['request'], $header);
            $GLOBALS['CI']->custom_db->generate_static_response(json_encode($send_change_request_response));

            /*$send_change_request_response = $GLOBALS['CI']->hotel_model->get_static_response(1755);//1624 //(pocessed-1783, 1755)*/
            if (valid_array($send_change_request_response) == true && $send_change_request_response['HotelChangeRequestResult']['ResponseStatus'] == 1) {
                //2.GetChangeRequestStatus
                return $this->get_change_request_status($send_change_request_response['HotelChangeRequestResult']['ChangeRequestId'], $app_reference);
            } else {
                $response['msg'] = $send_change_request_response['HotelChangeRequestResult']['Error']['ErrorMessage'];
            }
        }
        return $response;
    }
    /**
     *Get cancellation details by cancellation policy code
     */
    public function get_cancellation_policy($get_params)
    {
        $response['status'] = false;
        $response['data']   = array();
        if ($get_params) {
            $request_rate_data = json_encode($get_params);
            //echo $request_rate_data;
            $header = $this->get_header();
            $this->credentials('GetCancellationCode');
            $response['data']     = array();
            $cancel_url           = $this->service_url;
            $cancel_data_arr      = array();
            $cancellation_details = array();
            $cancellation_policy  = $GLOBALS['CI']->api_interface->get_json_response($cancel_url, $request_rate_data, $header);
            if ($cancellation_policy['Status'] == true) {
                if ($cancellation_policy['GetCancellationPolicy']['policy'][0]['policy']) {
                    $response['status'] = true;
                    $response['data']   = $cancellation_policy['GetCancellationPolicy']['policy'][0]['policy'];
                } else {
                    $response['status'] = false;
                }
            } else {
                $response['status'] = false;
            }
            return $response;
        }
    }
    /**
     * Jagnath
     * Cancellation Request:SendChangeRequest
     */
    private function cancel_booking_request_params($app_reference)
    {
        $response['status']      = true;
        $response['data']        = array();
        $request['AppReference'] = trim($app_reference);

        $response['data']['request'] = json_encode($request);
        $this->credentials('CancelBooking');
        $response['data']['service_url'] = $this->service_url;
        return $response;
    }
    /**
     * Jaganath
     * Cancellation Request Status
     */
    public function get_change_request_status($ChangeRequestId, $app_reference)
    {
        $header                    = $this->get_header();
        $response['data']          = array();
        $response['status']        = FAILURE_STATUS;
        $resposne['msg']           = 'Remote IO Error';
        $get_change_request_status = $this->get_change_request_status_params($ChangeRequestId);
        if ($get_change_request_status['status']) {
            $get_change_request_status_response = $GLOBALS['CI']->api_interface->get_json_response($get_change_request_status['data']['service_url'], $get_change_request_status['data']['request'], $header);
            $GLOBALS['CI']->custom_db->generate_static_response(json_encode($get_change_request_status_response));

            /*$get_change_request_status_response = $GLOBALS['CI']->hotel_model->get_static_response(1853);//(pocessed-1927,1950)*/
            if (valid_array($get_change_request_status_response) == true && isset($get_change_request_status_response['HotelChangeRequestStatusResult']['ResponseStatus']) == true &&
                $get_change_request_status_response['HotelChangeRequestStatusResult']['ResponseStatus'] == 1) {
                //Save Cancellation Details
                $this->save_cancellation_data($app_reference, $get_change_request_status_response);
                $response['status'] = SUCCESS_STATUS;
            } else {
                $resposne['msg'] = $get_change_request_status_response['HotelChangeRequestStatusResult']['Error']['ErrorMessage'];
            }
        }
        return $response;
    }
    /*
     * Jaganath
     * Save the Cancellation Details into Database
     */
    public function save_cancellation_data($app_reference, $cancellation_details)
    {
        $HotelChangeRequestStatusResult = $cancellation_details['HotelChangeRequestStatusResult'];
        $$app_reference                 = trim($app_reference);
        $ChangeRequestId                = $HotelChangeRequestStatusResult['ChangeRequestId'];
        $ChangeRequestStatus            = $HotelChangeRequestStatusResult['ChangeRequestStatus'];
        $status_description             = $this->ChangeRequestStatusDescription($ChangeRequestStatus);
        $TraceId                        = $HotelChangeRequestStatusResult['TraceId'];
        $API_RefundedAmount             = @$HotelChangeRequestStatusResult['RefundedAmount'];
        $API_CancellationCharge         = @$HotelChangeRequestStatusResult['CancellationCharge'];
        $attr                           = json_encode($cancellation_details);
        //Update Booking Status
        if ($ChangeRequestStatus != 0) {
            /* STATUS CODES
            NotSet = 0,
            Pending = 1,
            InProgress = 2,
            Processed = 3,
            Rejected = 4*/
            $booking_status = 'BOOKING_CANCELLED';
            //Update Master Booking Status
            $GLOBALS['CI']->hotel_model->update_master_booking_status($app_reference, $booking_status);
            //Update Pax Booing Status
            $GLOBALS['CI']->hotel_model->update_pax_booking_status($app_reference, $booking_status);
        }
        $GLOBALS['CI']->hotel_model->save_cancellation_data($app_reference, $ChangeRequestId, $ChangeRequestStatus, $status_description, $TraceId, $API_RefundedAmount, $API_CancellationCharge, $attr);
        //Update Transaction Details
        //$this->domain_management_model->update_transaction_details('hotel', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount'] );
    }
    /**
     *  Sawood
     * check and return status is success or not
     * @param unknown_type $response_status
     */

    public function valid_book_response($response_status)
    {
        $status = false;
        if (is_array($response_status) and !empty($response_status) and is_array($response_status['BookResult']) and
            !empty($response_status['BookResult']) and $response_status['BookResult']['ResponseStatus'] == SUCCESS_STATUS and
            isset($response_status['BookResult']['HotelBookingStatus']) and $response_status['BookResult']['HotelBookingStatus'] != '' and
            ($response_status['BookResult']['HotelBookingStatus'] != 'Pending' || $response_status['BookResult']['HotelBookingStatus'] != 'Vouchered' ||
                $response_status['BookResult']['HotelBookingStatus'] != 'Confirmed')) {
            $status = true;
        }
        return $status;
    }

    /**
     *  Arjun J Gowda
     * check and return status is success or not
     * @param unknown_type $response_status
     */
    public function valid_response($response_status)
    {
        $status = true;
        if ($response_status != SUCCESS_STATUS) {
            $status = false;
        }
        return $status;
    }

    /**
     *  Arjun J Gowda
     *
     * Check if the room was blocked successfully
     * @param array $block_room_response block room response
     */
    private function is_room_blocked($block_room_response)
    {
        $room_blocked = false;
        if (isset($block_room_response['BlockRoomResult']) == true and $block_room_response['BlockRoomResult']['IsPriceChanged'] == false and
            $block_room_response['BlockRoomResult']['IsCancellationPolicyChanged'] == false) {
            $room_blocked = true;
        }
        return $room_blocked;
    }

    /**
     *  Arjun J Gowda
     * check if the room list is valid or not
     * @param $room_list
     */
    private function valid_room_details_details($room_list)
    {
        $status = false;
        if (valid_array($room_list) == true and isset($room_list['Status']) == true and $room_list['Status'] == SUCCESS_STATUS) {
            $status = true;
        }
        return $status;
    }

    /**
     *  Arjun J Gowda
     * check if the hotel response which is received from server is valid or not
     * @param $hotel_details
     */
    private function valid_hotel_details($hotel_details)
    {
        $status = false;
        if (valid_array($hotel_details) == true and isset($hotel_details['Status']) == true and $hotel_details['Status'] == SUCCESS_STATUS) {
            $status = true;
        }
        return $status;
    }

    /**
     *  Arjun J Gowda
     * check if the search response is valid or not
     * @param array $search_result search result response to be validated
     */
    private function valid_search_result($search_result)
    {
        if (valid_array($search_result) == true and isset($search_result['Status']) == true and $search_result['Status'] == SUCCESS_STATUS) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *  Arjun J Gowda
     * Update and return price details
     */
    public function update_block_details($room_details, $booking_parameters)
    {

        $booking_parameters['TraceId']     = $room_details['BlockRoomId'];
        $booking_parameters['BlockRoomId'] = $room_details['BlockRoomId'];
        $room_details['HotelRoomsDetails'] = get_room_index_list($room_details['HotelRoomsDetails']);
        $application_default_currency      = get_application_default_currency();
        $booking_parameters['token']       = ''; // Remove all the token details
        $total_OfferedPriceRoundedOff      = $Tax      = '';
        foreach ($room_details['HotelRoomsDetails'] as $__rc_key => $__rc_value) {
            $booking_parameters['token'][]       = get_dynamic_booking_parameters($__rc_key, $__rc_value, $application_default_currency);
            $booking_parameters['price_token'][] = $__rc_value['Price'];
            $booking_parameters['HotelCode']     = $__rc_value['HotelCode'];
        }
        $booking_parameters['CancellationPolicy'] = $room_details['HotelRoomsDetails'][0]['CancellationPolicies'];
        if (isset($room_details['HotelRoomsDetails'][0]['RoomTypeName'])) {
            $booking_parameters['RoomTypeName'] = $room_details['HotelRoomsDetails'][0]['RoomTypeName'];
        }
        if (isset($room_details['HotelRoomsDetails'][0]['Boarding_details'])) {
            $booking_parameters['Boarding_details'] = $room_details['HotelRoomsDetails'][0]['Boarding_details'];
        }
        $booking_parameters['price_summary'] = tbo_summary_room_combination($room_details['HotelRoomsDetails']);

        return $booking_parameters;
    }

    /**
     * parse data according to voucher needs
     * @param array $data
     */
    public function parse_voucher_data($data)
    {
        $response = $data;
        return $response;
    }

    /**
     *  Arjun J Gowda
     * convert search params to format
     */
    public function search_data($search_id)
    {
        $response['status'] = true;
        $response['data']   = array();
        if (empty($this->master_search_data) == true and valid_array($this->master_search_data) == false) {
            $clean_search_details = $GLOBALS['CI']->hotel_model->get_safe_search_data($search_id);
            //debug($clean_search_details);exit;
            if ($clean_search_details['status'] == true) {
                $response['status'] = true;
                $response['data']   = $clean_search_details['data'];
                //28/12/2014 00:00:00 - date format
                $response['data']['from_date'] = date('Y-m-d', strtotime($clean_search_details['data']['from_date']));
                $response['data']['to_date']   = date('Y-m-d', strtotime($clean_search_details['data']['to_date']));
                //get city id based
                //$location_details = $GLOBALS['CI']->hotel_model->tbo_hotel_city_id($clean_search_details['data']['city_name'], $clean_search_details['data']['country_name']);
                $location_details = $GLOBALS['CI']->custom_db->single_table_records('all_api_city_master', 'country_code,origin', array('origin' => $clean_search_details['data']['hotel_destination']), 0, 1);
                //echo $GLOBALS['CI']->db->last_query();
                //debug($location_details);exit;
                if ($location_details['status']) {
                    $response['data']['country_code'] = $location_details['data'][0]['country_code'];
                    $response['data']['location_id']  = $location_details['data'][0]['origin'];
                } else {
                    $response['data']['country_code'] = $response['data']['location_id'] = '';
                }
                $this->master_search_data = $response['data'];
            } else {
                $response['status'] = false;
            }
        } else {
            $response['data'] = $this->master_search_data;
        }
        $this->search_hash = md5(serialized_data($response['data']));
        return $response;
    }

    /**
     * Markup for search result
     * @param array $price_summary
     * @param object $currency_obj
     * @param number $search_id
     */
    public function update_search_markup_currency(&$price_summary, &$currency_obj, $search_id, $level_one_markup = false, $current_domain_markup = true, $module_type = 'b2c', $user_id = 0)
    {
        $search_data  = $this->search_data($search_id);
        $no_of_nights = $this->master_search_data['no_of_nights'];
        $no_of_rooms  = $this->master_search_data['room_count'];
        $multiplier   = ($no_of_nights * $no_of_rooms);
        return $this->update_markup_currency($price_summary, $currency_obj, $multiplier, $level_one_markup, $current_domain_markup, $module_type, $user_id);
    }

    /**
     * Markup for Room List
     * @param array $price_summary
     * @param object $currency_obj
     * @param number $search_id
     */
    public function update_room_markup_currency(&$price_summary, &$currency_obj, $search_id, $level_one_markup = false, $current_domain_markup = true, $module_type = 'b2c', $user_id = 0)
    {

        $search_data  = $this->search_data($search_id);
        $no_of_nights = $this->master_search_data['no_of_nights'];
        $no_of_rooms  = 1;
        $multiplier   = ($no_of_nights * $no_of_rooms);
        return $this->update_markup_currency($price_summary, $currency_obj, $multiplier, $level_one_markup, $current_domain_markup, $module_type, $user_id);
    }

    /**
     * Markup for Booking Page List
     * @param array $price_summary
     * @param object $currency_obj
     * @param number $search_id
     */
    public function update_booking_markup_currency(&$price_summary, &$currency_obj, $search_id, $level_one_markup = false, $current_domain_markup = true, $module_type = 'b2c', $user_id = 0)
    {
        return $this->update_search_markup_currency($price_summary, $currency_obj, $search_id, $level_one_markup, $current_domain_markup, $module_type, $user_id);

    }
    /**
     *Update Markup currency for Cancellation Charge
     */
    public function update_cancellation_markup_currency(&$cancel_charge, &$currency_obj, $search_id, $level_one_markup = false, $current_domain_markup = true)
    {
        $search_data = $this->search_data($search_id);

        $no_of_nights = $this->master_search_data['no_of_nights'];
        $temp_price   = $currency_obj->get_currency($cancel_charge, true, $level_one_markup, $current_domain_markup, $no_of_nights);

        return round($temp_price['default_value']);
    }
    /**
     * update markup currency and return summary
     * $attr needed to calculate number of nights markup when its plus based markup
     */
    public function update_markup_currency(&$price_summary, &$currency_obj, $no_of_nights = 1, $level_one_markup = false, $current_domain_markup = true, $module_type = 'b2c', $user_id = 0)
    {
        $tax_service_sum = 0;
        /*$tax_service_sum = $this->tax_service_sum($price_summary);*/
        //Remove Tax and Service Tax While Adding markup
        /*$tax_removal_list = array('PublishedPrice', 'PublishedPriceRoundedOff', 'OfferedPrice', 'OfferedPriceRoundedOff');
        $markup_list = array('RoomPrice', 'PublishedPrice', 'PublishedPriceRoundedOff', 'OfferedPrice', 'OfferedPriceRoundedOff');*/
        $tax_removal_list = array();
        $markup_list      = array('PublishedPrice', 'PublishedPriceRoundedOff', 'OfferedPrice', 'OfferedPriceRoundedOff');

        //Removing Tax and Service Tax so no need to add markup
        /*foreach($tax_removal_list as $__mk => $__mv) {
        $price_summary[$__mv] = $price_summary[$__mv]-($tax_service_sum);
        }*/
        $markup_summary = array();
        foreach ($price_summary as $__k => $__v) {

            $ref_cur             = $currency_obj->force_currency_conversion($__v); //Passing Value By Reference so dont remove it!!!
            $price_summary[$__k] = $ref_cur['default_value']; //If you dont understand then go and study "Passing value by reference"
            if (in_array($__k, $markup_list)) {
                $temp_price = $currency_obj->get_currency($__v, true, $level_one_markup, $current_domain_markup, $no_of_nights, [], $module_type, $user_id);
            } else {
                $temp_price = $currency_obj->force_currency_conversion($__v);
            }

            //adding service tax and tax to total
            if (in_array($__k, $tax_removal_list)) {
                $markup_summary[$__k] = $temp_price['default_value'] + $tax_service_sum;
            } else {
                $markup_summary[$__k] = $temp_price['default_value'];
            }
            $markup_summary['CurrencySymbol'] = $currency_obj->get_currency_symbol($currency_obj->to_currency);
        }
        return $markup_summary;
    }

    /**
     *Tax price is the price for which markup should not be added
     */
    public function tax_service_sum($markup_price_summary, $api_price_summary)
    {
        //sum of tax and service ;
        return ($api_price_summary['ServiceTax'] + $api_price_summary['Tax'] + ($markup_price_summary['PublishedPrice'] - $api_price_summary['PublishedPrice']));
    }

    /**
     * calculate and return total price details
     */
    public function total_price($price_summary)
    {
        return ($price_summary['OfferedPriceRoundedOff']);
    }

    public function booking_url($search_id)
    {
        return base_url() . 'index.php/hotel/booking/' . intval($search_id);
    }
    /**
     * Jaganath
     * @param $ChangeRequestStatus
     */
    private function ChangeRequestStatusDescription($ChangeRequestStatus)
    {
        $status_description = '';
        switch ($ChangeRequestStatus) {
            case 0:
                $status_description = 'NotSet';
                break;
            case 1:
                $status_description = 'Pending';
                break;
            case 2:
                $status_description = 'InProgress';
                break;
            case 3:
                $status_description = 'Processed';
                break;
            case 4:
                $status_description = 'Rejected';
                break;
        }
        return $status_description;
    }

    /**
     * Get Filter Params - fliter_params
     */
    public function format_search_response($hl, $cobj, $sid, $module = 'b2c', $fltr = array(), $user_id = 0)
    {
        $level_one      = true;
        $current_domain = true;
        if ($module == 'b2c') {
            $level_one      = false;
            $current_domain = true;
        } else if ($module == 'b2b') {
            $level_one      = true;
            $current_domain = true;
        }
        $h_count      = 0;
        $HotelResults = array();
        /*    if (isset($fltr['hl']) == true) {
        foreach ($fltr['hl'] as $tk => $tv) {
        $fltr['hl'][urldecode($tk)] = strtolower(urldecode($tv));
        }
        }*/
        //Creating closures to filter data
        /* check_filters = function ($hd) use ($fltr) {
        if (
        (
        valid_array(@$fltr['hl']) == false ||
        (valid_array(@$fltr['hl']) == true && in_array(strtolower($hd['HotelLocation']), $fltr['hl']))
        ) &&
        (
        valid_array(@$fltr['_sf']) == false ||
        (valid_array(@$fltr['_sf']) == true && in_array($hd['StarRating'], $fltr['_sf']))
        ) &&
        (
        @$fltr['min_price'] <= ceil($hd['Price']['RoomPrice']) &&
        (@$fltr['max_price'] != 0 && @$fltr['max_price'] >= floor($hd['Price']['RoomPrice']))
        ) && (
        (string)$fltr['dealf'] == 'false' || empty($hd['HotelPromotion']) == false
        ) &&
        (
        empty($fltr['hn_val']) == true ||
        (empty($fltr['hn_val']) == false && stripos(strtolower($hd['HotelName']), (urldecode($fltr['hn_val']))) > -1)
        )
        ) {
        return true;
        } else {
        return false;
        }
        };*/
        $hc  = 0;
        $frc = 0;

        foreach ($hl['HotelSearchResult']['HotelResults'] as $hr => $hd) {
            $hc++;

            //default values
            $hd['StarRating'] = intval($hd['StarRating']);
            if (empty($hd['HotelLocation']) == true) {
                $hd['HotelLocation'] = 'Others';
            }
            if (isset($hd['Latitude']) == false) {
                $hd['Latitude'] = 0;
            }
            if (isset($hd['Longitude']) == false) {
                $hd['Longitude'] = 0;
            }
            //markup
            $hd['Price'] = $this->update_search_markup_currency($hd['Price'], $cobj, $sid, $level_one, $current_domain, $module, $user_id);

            unset($hd['HotelCategory']);
            unset($hd['HotelDescription']);
            unset($hd['HotelPolicy']);
            unset($hd['ImageOrder']);
            //unset($hd['HotelAddress']);
            //unset($hd['HotelContactNo']);
            unset($hd['HotelMap']);
            unset($hd['CITY_CODE']);
            unset($hd['DEST_CODE']);
            unset($hd['COUNTRY_CODE']);
            unset($hd['SupplierPrice']);
            unset($hd['RoomDetails']);
            unset($hd['Free_cancel_date']);
            unset($hd['trip_adv_url']);
            unset($hd['trip_rating']);
            $hd['Price']['Tax'] = $hd['Price']['PublishedPriceRoundedOff'] - $hd['Price']['RoomPrice'];
            unset($hd['Price']['PublishedPrice']);
            unset($hd['Price']['PublishedPriceRoundedOff']);
            unset($hd['Price']['OfferedPrice']);
            unset($hd['Price']['OfferedPriceRoundedOff']);
            // unset($hd['Price']['Tax']);
            unset($hd['Price']['ExtraGuestCharge']);
            unset($hd['Price']['ChildCharge']);
            unset($hd['Price']['OtherCharges']);
            unset($hd['Price']['Discount']);
            unset($hd['Price']['AgentCommission']);
            unset($hd['Price']['AgentMarkUp']);
            unset($hd['Price']['ServiceTax']);
            unset($hd['Price']['TDS']);
            unset($hd['Price']['GSTPrice']);

            $selected_amenities = array('wireless', 'Wi-Fi', 'breakfast', 'park', 'pool', 'Swim');
            $Amenities_List     = array();
            $API_Amenities_List = $hd['HotelAmenities'];

            for ($t = 0; $t < count($API_Amenities_List); $t++) {
                for ($r = 0; $r < count($selected_amenities); $r++) {
                    if (strpos(strtolower($API_Amenities_List[$t]), strtolower($selected_amenities[$r])) !== false) {
                        if (count($Amenities_List) < 6) {
                            $Amenities_List[] = $API_Amenities_List[$t];
                        } else {
                            break;}
                    }
                }
            }

            $hd['HotelAmenities'] = $Amenities_List;
            $HotelResults[$hr]    = $hd;
            $frc++;
        }
        //SORTING STARTS
        /*    if(isset($fltr['sort_item']) == true && empty($fltr['sort_item']) == false &&
        isset($fltr['sort_type']) == true && empty($fltr['sort_type']) == false) {
        $sort_item = array();
        foreach ($HotelResults as $key => $row)
        {
        if($fltr['sort_item'] == 'price') {
        $sort_item[$key] = floatval($row['Price']['RoomPrice']);
        } else if($fltr['sort_item'] == 'star') {
        $sort_item[$key] = floatval($row['StarRating']);
        } else if($fltr['sort_item'] == 'name') {
        $sort_item[$key] = trim($row['HotelName']);
        }
        }
        if($fltr['sort_type'] == 'asc') {
        $sort_type = SORT_ASC;
        } else if($fltr['sort_type'] == 'desc') {
        $sort_type = SORT_DESC;
        }
        if(valid_array($sort_item) == true && empty($sort_type) == false) {
        array_multisort($sort_item, $sort_type, $HotelResults);
        }
        }*///SORTING ENDS
        $hl['HotelSearchResult']['HotelResults'] = $HotelResults;
        $hl['source_result_count']               = $hc;
        $hl['filter_result_count']               = $frc;
        return $hl;
    }

    public function get_page_data($hl, $offset, $limit)
    {
        $hl['HotelSearchResult']['HotelResults'] = array_slice($hl['HotelSearchResult']['HotelResults'], $offset, $limit);
        return $hl;
    }

    /**
     * Break data into pages
     * @param $data
     * @param $offset
     * @param $limit
     */
    public function age_data($hl, $offset, $limit)
    {
        $hl['HotelSearchResult']['HotelResults'] = array_slice($hl['HotelSearchResult']['HotelResults'], $offset, $limit);
        return $hl;
    }

    /**
     * Get Filter Summary of the data list
     * @param array $hl
     */
    public function filter_summary($hl)
    {
        $h_count          = 0;
        $filt['p']['max'] = false;
        $filt['p']['min'] = false;
        $filt['loc']      = array();
        $filt['star']     = array();
        $filters          = array();
        foreach ($hl['HotelSearchResult']['HotelResults'] as $hr => $hd) {
            //filters
            $StarRating    = intval(@$hd['StarRating']);
            $HotelLocation = empty($hd['HotelLocation']) == true ? 'Others' : $hd['HotelLocation'];

            if (isset($filt['star'][$StarRating]) == false) {
                $filt['star'][$StarRating]['c'] = 1;
                $filt['star'][$StarRating]['v'] = $StarRating;
            } else {
                $filt['star'][$StarRating]['c']++;
            }

            if (($filt['p']['max'] != false && $filt['p']['max'] < $hd['Price']['RoomPrice']) || $filt['p']['max'] == false) {
                $filt['p']['max'] = roundoff_number($hd['Price']['RoomPrice']);
            }
            if (($filt['p']['min'] != false && $filt['p']['min'] > $hd['Price']['RoomPrice']) || $filt['p']['min'] == false) {
                $filt['p']['min'] = roundoff_number($hd['Price']['RoomPrice']);
            }

            if (($filt['p']['min'] != false && $filt['p']['min'] > $hd['Price']['RoomPrice']) || $filt['p']['min'] == false) {
                $filt['p']['min'] = $hd['Price']['RoomPrice'];
            }
            $hloc = ucfirst(strtolower($HotelLocation));
            if (isset($filt['loc'][$hloc]) == false) {
                $filt['loc'][$hloc]['c'] = 1;
                $filt['loc'][$hloc]['v'] = $hloc;
            } else {
                $filt['loc'][$hloc]['c']++;
            }

            $filters['data'] = $filt;
            $h_count++;
        }
        ksort($filters['data']['loc']);
        $filters['hotel_count'] = $h_count;
        return $filters;

    }

    public function block_room_mobile($pre_booking_params, $Token__id)
    {
        $Token__id = $Token__id;

        $header                            = $this->get_header();
        $response['status']                = false;
        $response['data']                  = array();
        $search_data                       = $this->search_data($pre_booking_params['search_id']);
        $run_block_room_request            = true;
        $block_room_request_count          = 0;
        $pre_booking_params['search_data'] = $search_data['data'];
        //debug($pre_booking_params); die;
        $block_room_request = $this->get_block_room_request_mobile($pre_booking_params, $Token__id);

        $application_default_currency = get_application_default_currency();
        if ($block_room_request['status'] == ACTIVE) {
            while ($run_block_room_request) {
                $GLOBALS['CI']->custom_db->generate_static_response(json_encode($block_room_request['data']['request']));
                $block_room_response = $GLOBALS['CI']->api_interface->get_json_response($block_room_request['data']['service_url'], $block_room_request['data']['request'], $header);

                //debug($block_room_response);exit;
                $GLOBALS['CI']->custom_db->generate_static_response(json_encode($block_room_response)); //release this
                /*$static_search_result_id = 309;//161;//184;//169;//197; -- SINGLE*/
                /*$static_search_result_id = 815;//202;// -- MULTIPLE
                $block_room_response = $GLOBALS['CI']->hotel_model->get_static_response($static_search_result_id); //cmt this*/
                $api_block_room_response_status = $block_room_response['Status'];
                if ($api_block_room_response_status) {
                    $block_room_response = $block_room_response['BlockRoom'];
                    if ($this->valid_response($api_block_room_response_status) == false) {
                        $run_block_room_request  = false;
                        $response['status']      = false; // Indication for room block
                        $response['data']['msg'] = 'Some Problem Occured. Please Search Again to continue';
                    } elseif ($this->is_room_blocked($block_room_response) == true) {
                        $run_block_room_request = false;
                        $response['status']     = true; // Indication for room block
                    } else {
                        //UPDATE RECURSSION
                        $_HotelRoomsDetails = get_room_index_list($block_room_response['BlockRoomResult']['HotelRoomsDetails']);
                        //Reset pre booking params token and get new values
                        $dynamic_params_url = '';
                        foreach ($_HotelRoomsDetails as $___tk => $___tv) {
                            $dynamic_params_url[] = get_dynamic_booking_parameters($___tk, $___tv, $application_default_currency);
                        }
                        //update token key
                        $pre_booking_params['token']     = $dynamic_params_url;
                        $pre_booking_params['token_key'] = md5(serialized_data($dynamic_params_url));
                        $block_room_request              = $this->get_block_room_request_mobile($pre_booking_params, $Token__id);
                    }
                } else {
                    $response['data']['status'] = false;

                }

                $block_room_request_count++; //Increment number of times request is run
                if ($block_room_request_count == 3 && $run_block_room_request == true) {
                    //try max 3times to block the room
                    $run_block_room_request = false;
                }
            }
            $response['data']['response'] = $block_room_response;
        }
        //debug($response);exit;
        return $response;
    }
    private function get_block_room_request_mobile($booking_params, $Token__id)
    {

        $number_of_nights       = $booking_params['search_data']['no_of_nights'];
        $response['status']     = true;
        $response['data']       = array();
        $request['ResultToken'] = urldecode($booking_params['ResultIndex']);
        //debug($booking_params);
        foreach ($booking_params['token'] as $tk => $tv) {
            $request['RoomUniqueId'][] = $tv;
        }

        //debug($request);
        //exit;
        $response['data']['request'] = json_encode($request);
        $this->credentials('BlockRoom');
        $response['data']['service_url'] = $this->service_url;
        return $response;
    }
    private function get_block_room_request_mobile_old($booking_params, $Token__id)
    {

        $number_of_nights     = $booking_params['search_data']['no_of_nights'];
        $response['status']   = true;
        $response['data']     = array();
        $request['TokenId']   = $Token__id;
        $request['TraceId']   = $booking_params['TraceId'];
        $request['EndUserIp'] = '127.0.0.1';

        $request['ResultIndex']       = intval($booking_params['ResultIndex']);
        $request['HotelCode']         = $booking_params['HotelCode'];
        $request['HotelName']         = $booking_params['HotelName'];
        $request['GuestNationality']  = ISO_INDIA; //$booking_params['GuestNationality'];
        $request['NoOfRooms']         = abs($booking_params['search_data']['room_count']);
        $request['IsVoucherBooking']  = true;
        $request['ClientReferenceNo'] = 0;
        $request['NoOfNights']        = $number_of_nights; //Jaganath
        $request['HotelRoomsDetails'] = '';
        $room_token_list              = $booking_params['token'];
        foreach ($room_token_list as $__room_key => $__room_value) {
            $request['HotelRoomsDetails'][$__room_key]['RoomIndex']         = intval($__room_value['RoomIndex']);
            $request['HotelRoomsDetails'][$__room_key]['RoomTypeCode']      = $__room_value['RoomTypeCode'];
            $request['HotelRoomsDetails'][$__room_key]['RoomTypeName']      = $__room_value['RoomTypeName'];
            $request['HotelRoomsDetails'][$__room_key]['RatePlanCode']      = $__room_value['RatePlanCode'];
            $request['HotelRoomsDetails'][$__room_key]['SmokingPreference'] = $__room_value['SmokingPreference'];
            $request['HotelRoomsDetails'][$__room_key]['RatePlanName']      = $__room_value['RatePlanName'];
            $request['HotelRoomsDetails'][$__room_key]['BedTypeCode']       = '';
            $request['HotelRoomsDetails'][$__room_key]['Supplements']       = '';

            //PRICE Details
            $request['HotelRoomsDetails'][$__room_key]['Price']['CurrencyCode']             = $__room_value['CurrencyCode'];
            $request['HotelRoomsDetails'][$__room_key]['Price']['RoomPrice']                = number_format($__room_value['RoomPrice'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['PublishedPrice']           = number_format($__room_value['PublishedPrice'], 3, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['PublishedPriceRoundedOff'] = abs($__room_value['PublishedPriceRoundedOff']);
            $request['HotelRoomsDetails'][$__room_key]['Price']['OfferedPrice']             = number_format($__room_value['OfferedPrice'], 3, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['OfferedPriceRoundedOff']   = abs($__room_value['OfferedPriceRoundedOff']);
            $request['HotelRoomsDetails'][$__room_key]['Price']['ServiceTax']               = number_format($__room_value['ServiceTax'], 2, '.', '');

            $request['HotelRoomsDetails'][$__room_key]['Price']['Tax']              = number_format(@$__room_value['Tax'], 3, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['ExtraGuestCharge'] = number_format($__room_value['ExtraGuestCharge'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['ChildCharge']      = number_format($__room_value['ChildCharge'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['OtherCharges']     = number_format($__room_value['OtherCharges'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['Discount']         = number_format($__room_value['Discount'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['AgentCommission']  = number_format($__room_value['AgentCommission'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['AgentMarkUp']      = number_format($__room_value['AgentMarkUp'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['Price']['TDS']              = number_format($__room_value['TDS'], 2, '.', '');
            $request['HotelRoomsDetails'][$__room_key]['AccessKey']                 = $__room_value['AccessKey'];
        }
        $response['data']['request'] = json_encode($request);
        $this->credentials('BlockRoom');
        $response['data']['service_url'] = $this->service_url;
        return $response;
    }

    public function get_hotel_details_mobile($TraceId, $ResultIndex, $HotelCode, $search_id, $module_type = 'b2c', $user_id = 0)
    {
        $header = $this->get_header();

        $response['data']      = array();
        $response['status']    = false;
        $hotel_details_request = $this->hotel_details_request_mobile($TraceId, $ResultIndex, $HotelCode);
        if ($hotel_details_request['status']) {
            //get the response for hotel details
            $hotel_details_response = $GLOBALS['CI']->api_interface->get_json_response($hotel_details_request['data']['service_url'], $hotel_details_request['data']['request'], $header);
            // debug($hotel_details_response['HotelDetails']['HotelInfoResult']['HotelDetails']['first_room_details']['Price']);
            $currency_obj   = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
            $level_one      = true;
            $current_domain = true;
            if ($module == 'b2c') {
                $level_one      = false;
                $current_domain = true;
            } else if ($module == 'b2b') {
                $level_one      = true;
                $current_domain = true;
            }
            $temp_data = $this->update_search_markup_currency($hotel_details_response['HotelDetails']['HotelInfoResult']['HotelDetails']['first_room_details']['Price'], $currency_obj, $search_id, $level_one, $current_domain, $module_type, $user_id);

            $hotel_details_response['HotelDetails']['HotelInfoResult']['HotelDetails']['first_room_details']['Price']        = $temp_data;
            $hotel_details_response['HotelDetails']['HotelInfoResult']['HotelDetails']['first_room_details']['Price']['Tax'] = $hotel_details_response['HotelDetails']['HotelInfoResult']['HotelDetails']['first_room_details']['Price']['PublishedPriceRoundedOff'] - $hotel_details_response['HotelDetails']['HotelInfoResult']['HotelDetails']['first_room_details']['Price']['RoomPrice'];
            //debug($temp_data);exit;
            //$GLOBALS['CI']->custom_db->generate_static_response(json_encode($hotel_details_response));
            /*$static_search_result_id = 812;//105;//67;//49;
            $hotel_details_response = $GLOBALS['CI']->hotel_model->get_static_response($static_search_result_id);*/
            if ($this->valid_hotel_details($hotel_details_response)) {
                $response['data']   = $hotel_details_response;
                $response['status'] = true;
            } else {
                //Need the complete data so that later we can use it for redirection
                $response['data'] = $hotel_details_response;
            }
        }
        return $response;
    }

    private function hotel_details_request_mobile($TraceId, $ResultIndex, $HotelCode)
    {
        $response['status'] = true;
        $response['data']   = array();
        //$request['TokenId']        = $Token_id;
        //$request['TraceId']        = $TraceId;
        //$request['ResultIndex']    = $ResultIndex;
        //$request['HotelCode']    = $HotelCode;
        //$request['EndUserIp']    = '127.0.0.1';
        $request['ResultToken']      = $TraceId;
        $response['data']['request'] = json_encode($request);
        $this->credentials('GetHotelInfo');
        $response['data']['service_url'] = $this->service_url;
        return $response;
    }

    public function get_room_list_mobile($TraceId, $ResultIndex, $HotelCode)
    {
        $header             = $this->get_header();
        $response['data']   = array();
        $response['status'] = false;
        $hotel_room_request = $this->room_list_request_mobile($TraceId, $ResultIndex, $HotelCode);
        if ($hotel_room_request['status']) {
            //get the response for hotel details
            $hotel_room_list_response = $GLOBALS['CI']->api_interface->get_json_response($hotel_room_request['data']['service_url'], $hotel_room_request['data']['request'], $header);
            //debug($hotel_room_list_response);exit;
            //$GLOBALS['CI']->custom_db->generate_static_response(json_encode($hotel_room_list_response));

            /*$static_search_result_id = 813;//106;//68;//52;
            $hotel_room_list_response = $GLOBALS['CI']->hotel_model->get_static_response($static_search_result_id);*/
            if ($this->valid_room_details_details($hotel_room_list_response)) {
                $response['data']   = $hotel_room_list_response;
                $response['status'] = true;
            } else {
                //Need the complete data so that later we can use it for redirection
                $response['data'] = $hotel_room_list_response;
            }
        }
        return $response;
    }

    private function room_list_request_mobile($TraceId, $ResultIndex, $HotelCode)
    {
        $response['status']     = true;
        $response['data']       = array();
        $request['ResultToken'] = $TraceId;
        // $request['TokenId']        = $TokenId;
        // $request['TraceId']        = $TraceId;
        // $request['ResultIndex']    = $ResultIndex;
        // $request['HotelCode']    = $HotelCode;
        // $request['EndUserIp']    = '127.0.0.1';

        $response['data']['request'] = json_encode($request);
        $this->credentials('GetHotelRoom');
        $response['data']['service_url'] = $this->service_url;
        return $response;
    }
    /**
     * Roomwise Assigned Passenger Count
     * @param unknown_type $pax_type_arr
     * @param unknown_type $pax_type
     */
    public function get_assigned_pax_type_count($pax_type_arr, $pax_type)
    {
        $pax_type_count = 0;
        if (valid_array($pax_type_arr) == true) {
            foreach ($pax_type_arr as $k => $v) {
                if ($pax_type == $v) {
                    $pax_type_count++;
                }
            }
        }
        return $pax_type_count;
    }

    /*php check array unique*/
    /**/
    public function php_arrayUnique($array, $key)
    {
        $temp_array = array();
        $i          = 0;
        $key_array  = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i]  = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }
}
