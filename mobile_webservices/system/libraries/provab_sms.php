<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

/**
 * provab
 *
 * Travel Portal Application
 *
 * @package provab
 * @author Arjun J<arjun.provab@gmail.com>
 * @copyright Copyright (c) 2013 - 2014
 * @link http://provab.com
 */
class Provab_Sms {
	public $CI; // instance of codeigniter super object
	public $sms_configuration; // sms configurations defined by user
	public function __construct($data = '') {
		if (valid_array ( $data ) == true and intval ( $data ['id'] ) > 0) {
			$id = intval ( $data ['id'] );
		} else {
			$id = GENERAL_SMS;
		}
		$this->CI = & get_instance ();
		$return_data = $this->CI->user_model->sms_configuration ( $id );
		
		$this->sms_configuration = $return_data;
	}
	/**
	 * switch statement to select sms-gateway
	 */
	public function send_msg($phone, $msg) {
		$gateway = $this->sms_configuration->gateway;
		
		switch ($gateway) {
			case "pay4sms" :
				$this->smslogin ( $phone, $msg );
				break;
			default :
				$status = false;
				return array (
						'status' => $status 
				);
				break;
		}
	}
	/**
	 * send sms to the user based on gateway from switch statement
	 */
	public function smslogin($phone, $msg) {
		$username = $this->sms_configuration->username;
		$password = $this->sms_configuration->password;
		$senderid = $this->sms_configuration->from_name;
		$api_key = $this->sms_configuration->api_key;
		$phone = trim($phone);

		//http://pay4sms.in/sendsms/?token=416312cc82094ed73e8950a54b3620ab&credit=2&sender=XXXXXX&message=XXXXXX&number=XXXXXXXXXX
		$msg_link = 'http://pay4sms.in/sendsms/?token='.$api_key.'&credit=2&sender='.$senderid.'&number='.$phone.'&message='.urlencode($msg);
		$url = $msg_link;
		//echo $msg_link; die;
		file_get_contents($url);
		
	}

	public function booking_sms_tempalte($course, $app_reference, $booking_source, $module = 'b2c')
	{
		switch($course)
		{
			case META_AIRLINE_COURSE:
				$sms_template = $this->flight_sms_tempalte($app_reference, $booking_source, $module);
				break;
			case META_ACCOMODATION_COURSE:
				$sms_template = $this->hotel_sms_tempalte($app_reference, $booking_source, $module);
				break;
			case META_BUS_COURSE:
				$sms_template = $this->bus_sms_tempalte($app_reference, $booking_source, $module);
				break;
			// case META_RECHARGE_COURSE:
			// 	$sms_template = $this->recharge_sms_tempalte($app_reference, $booking_source, $module);
			// 	break;
			case META_PACKAGE_COURSE:
				$sms_template = $this->package_sms_tempalte($app_reference, $booking_source, $module);
				break;
			default:
			$sms_template['status'] = false;
		}
		return $sms_template;
	}

	private function flight_sms_tempalte($app_reference, $booking_source, $module)
	{
		$data = array();
		$data['status'] = false;
		$data['sms_tempalte'] = '';
		$GLOBALS['CI']->load->model('flight_model');
		$GLOBALS['CI']->load->library('booking_data_formatter');
		$booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
		if ($booking_details['status'] == SUCCESS_STATUS) {
			//Assemble Booking Data
			$assembled_booking_details = $GLOBALS['CI']->booking_data_formatter->format_flight_booking_data($booking_details, $module);
			$booking_details = $assembled_booking_details['data']['booking_details'][0];
			$booking_itinerary_details = $booking_details['booking_itinerary_details'];
			$booking_transaction_details = $booking_details['booking_transaction_details'];
			$phone_number = trim($booking_details['phone']);
			$departure_city = explode('(', trim($booking_details['journey_from']));
			$departure_city = trim($departure_city[0]);
			$arrival_city = explode('(', trim($booking_details['journey_to']));
			$arrival_city = trim($arrival_city[0]);
			$departure_date = trim($booking_details['journey_start']);
			$flight_details = $booking_itinerary_details[0]['airline_name'].'  '.$booking_itinerary_details[0]['airline_code'].'  '.$booking_itinerary_details[0]['flight_number'];//FIXME: For Round Way
			foreach ($booking_transaction_details as $k => $v) {
				$pnr = trim($v['pnr']);
				$flight = trim($v['source']);
				$sms_tempalte = "The PNR for your ".$flight_details."  for-   ".$departure_city." -  ".$arrival_city."  on  ".date('d M y', strtotime($departure_date))." at  ".local_time($departure_date)." hrs is ".$pnr." Thank You";
				$data['status'] = true;
				$data['phone_number'] = $phone_number;
				$data['sms_tempalte'][$k] = $sms_tempalte;
				$data['type'] =META_AIRLINE_COURSE;
			}
		}
		return $data;
	}
	private function hotel_sms_tempalte($app_reference, $booking_source, $module)
	{
		$data = array();
		$data['status'] = false;
		$data['sms_tempalte'] = '';
		$GLOBALS['CI']->load->model('hotel_model');
		$GLOBALS['CI']->load->library('booking_data_formatter');
		$booking_details = $GLOBALS['CI']->hotel_model->get_booking_details($app_reference, $booking_source);
		if ($booking_details['status'] == SUCCESS_STATUS) {
			//Assemble Booking Data
			$assembled_booking_details = $GLOBALS['CI']->booking_data_formatter->format_hotel_booking_data($booking_details, $module);
			$booking_details = $assembled_booking_details['data']['booking_details'] [0];
			
			$phone_number = trim($booking_details['phone_number']);
			$hotel_city = explode('(', trim($booking_details['hotel_location']));
			$hotel_city = trim($hotel_city[0]);
			$hotel_name = trim($booking_details['hotel_name']);
			$check_in = trim($booking_details['hotel_check_in']);
			$check_out = trim($booking_details['hotel_check_out']);
			$total_nights = trim($booking_details['total_nights']);
			$total_rooms = trim($booking_details['total_rooms']);
			$confirmation_number = trim($booking_details['confirmation_reference']);
			$sms_tempalte = "Welcome! Your hotel booking with ".$hotel_name." , ".$hotel_city."  is confirmed  Confirmation number- ".$confirmation_number." check-in ".date('d-M-Y', strtotime($check_in))." nights- ".$total_nights." rooms- ".$total_rooms." Thank You";

			$data['status'] = true;
			$data['phone_number'] = $phone_number;
			$data['sms_tempalte'][0] = $sms_tempalte;
			$data['type'] =META_ACCOMODATION_COURSE;
		}
		return $data;
	}
	private function bus_sms_tempalte($app_reference, $booking_source, $module)
	{
		$data = array();
		$data['status'] = false;
		$data['sms_tempalte'] = '';
		$GLOBALS['CI']->load->model('bus_model');
		$GLOBALS['CI']->load->library('booking_data_formatter');
		$booking_details = $GLOBALS['CI']->bus_model->get_booking_details($app_reference, $booking_source);
		if ($booking_details['status'] == SUCCESS_STATUS) {
			//Assemble Booking Data
			$assembled_booking_details = $GLOBALS['CI']->booking_data_formatter->format_bus_booking_data($booking_details, $module);
			$booking_details = $assembled_booking_details['data']['booking_details'] [0];
			
			$phone_number = trim($booking_details['phone_number']);
			$departure_city = ucfirst(trim($booking_details['departure_from']));
			$arrival_city = ucfirst(trim($booking_details['arrival_to']));
			$departure_date = trim($booking_details['journey_datetime']);
			$pnr = trim($booking_details['pnr']);
			$seats = trim($booking_details['seat_numbers']);
			$grand_total = trim($booking_details['grand_total']);
			$boarding_details = trim($booking_details['booking_itinerary_details'][0]['boarding_from']);
			$sms_tempalte = "Welcome! Your ticket is confirmed for DOJ - ".strtotime($departure_date)." from- ".$departure_city." to ".$arrival_city." Seat No - ".$seats." Fare -  ".$grand_total." Boarding Point - ".$boarding_details." PNR No - ".$pnr." Thank You";
			$data['status'] = true;
			$data['phone_number'] = $phone_number;
			$data['sms_tempalte'][0] = $sms_tempalte;
			$data['type'] =META_BUS_COURSE;
		}
		return $data;
	}
	private function recharge_sms_tempalte($app_reference, $booking_source, $module)
	{
		$data = array();
		$data['status'] = false;
		$data['sms_tempalte'] = '';
		$GLOBALS['CI']->load->model('recharge_model');
		$recharge = $page_data['recharge']=$GLOBALS['CI']->custom_db->single_table_records('recharge_details','*',array('app_reference' => $app_reference));
		if ($recharge['status'] == SUCCESS_STATUS) {
			$details=$recharge['data'][0];
			$phone_number=$details['mobile'];
			$refnumber=$details['refnumber'];
			$transaction_id=$details['transaction_id'];
			$recharge_type=$details['type'];
			$common_time=$details['common_time'];
			$operator=$details['operator'];
			$circle=$details['circle'];
			$status=$details['status'];
			$name=strstr($details['email'],'@',true);
			$amount=$details['amount'];
			
			$sms_tempalte = 'Dear '.$name.', Your recharge is '.$status.' for Rs. '.$amount.' of '.$phone_number. ' with '.$operator.' and TransID-'.$transaction_id.' Reference No-'.$refnumber.' date-'.$common_time;
			$sms_tempalte .= ' Thank You';
			$data['status'] = true;
			$data['phone_number'] = $phone_number;
			$data['sms_tempalte'][0] = $sms_tempalte;
		}
		return $data;
	}

	private function package_sms_tempalte($app_reference, $booking_source, $module)
	{
		$data = array();
		$data['status'] = false;
		$data['sms_tempalte'] = '';
		$GLOBALS['CI']->load->model('package_model');
		$GLOBALS['CI']->load->library('booking_data_formatter');
		$booking_details = $GLOBALS['CI']->package_model->get_booking_details($app_reference, $booking_source);
		if ($booking_details['status'] == SUCCESS_STATUS) {
			//Assemble Booking Data
			$assembled_booking_details = $GLOBALS['CI']->booking_data_formatter->format_package_booking_data($booking_details, $module);
			$booking_details = $assembled_booking_details['data']['booking_details'] [0];
			//debug($booking_details); die;
			$app_reference = trim($booking_details['app_reference']);
			$phone_number = trim($booking_details['phone_number']);
			$package_location = trim($booking_details['package_location']);
			$package_name = trim($booking_details['package_name']);
			$durations = trim($booking_details['durations']);
			$total_passengers = trim($booking_details['total_passenger']);
			$confirmation_number = trim($booking_details['booking_id']);
			$sms_tempalte = "Welcome! Your package booking with ".$package_name." , ".$package_location."  is confirmed  Confirmation number- ".$confirmation_number." and Reference number - ".$app_reference." package durations - ".$durations." Days No of Passenger's - ".$total_passengers." . Thank You";

			$data['status'] = true;
			$data['phone_number'] = $phone_number;
			$data['sms_tempalte'][0] = $sms_tempalte;
			$data['type'] =META_PACKAGE_COURSE;
		}
		return $data;
	}

	function agent_balance_sms_tempalte($data)
	{
		debug($data); die;
	}
}
?>
