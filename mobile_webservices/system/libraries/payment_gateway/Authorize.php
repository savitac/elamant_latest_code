<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
/**
 *
 * @package Provab
 * @subpackage Payu
 * @author Pravinkumar P <balu.provab@gmail.com>
 * @version V1
 */
class Authorize {
	
	public $authorizeNetPayment;
	public $card;
	public $month;
	public $year;
	public $amount;

	public function __construct() {
		$this->CI = &get_instance ();
		include APPPATH . 'third_party/authorize/AuthorizeNetPayment.php';
		$this->authorizeNetPayment = new AuthorizeNetPayment();
	}

	function initialize($data)
	{
		$this->card = $data['card-number'];
		$this->month = $data['month'];
		$this->year = $data['year'];
		$this->amount = $data['pgi_amount'];
	}
	function process_payment(){
		$message = '';
		$array = array(
			'card-number'=>$this->card,
			'month'=>$this->month,
			'year'=>$this->year,
			'amount'=>$this->amount,
		);
		// debug($array); exit();
		$response = $this->authorizeNetPayment->chargeCreditCard($array);
		// echo "<pre>";print_r($response);exit();
		if ($response != null) {

			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode() == "Ok") {

				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
					$reponseType = "success";
					$message.= " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
					$message.= " Transaction Response Code: " . $tresponse->getResponseCode() . "\n";
					//$message.=  " Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";
					// $message.=  " Auth Code: " . $tresponse->getAuthCode() . "\n";
					//$message.=  " Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";
	                $data = array(
	                		'payment_status' => true,
	                		'status' => 'Success',
	                		'message' => $message,
	                		'auth_code' => $tresponse->getAuthCode(),
	                		'description' => $tresponse->getMessages()[0]->getDescription(),
	                		'message_code' => $tresponse->getMessages()[0]->getCode(),
	                		'transaction_response_code' => $tresponse->getResponseCode(),
	                		'trans_id' => $tresponse->getTransId()
	                );
	                return $data;
            	}else{
            		$reponseType = "error";
            		$message.= "Transaction Failed \n";
            		if ($tresponse->getErrors() != null) {
            			$message.= " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
                    	$message.= " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                	}
                	$data = array(
	                		'payment_status' => false,
	                		'status' => 'fail',
	                		'message' => $message,
	                	);
                	return $data;
           		}
            	// Or, print errors if the API request wasn't successful
        	}else{
        		$reponseType = "error";
        		$message.= "Transaction Failed \n";
        		$tresponse = $response->getTransactionResponse();

        		if ($tresponse != null && $tresponse->getErrors() != null) {
        			$message.= " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
                	$message.= " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
            	} else {
                	$message.= " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
                	$message.= " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
            	}

            	$data = array(
	                		'payment_status' => false,
	                		'status' => 'fail',
	                		'message' => $message,
	                	);
                	return $data;
            }
        } else {
        	$reponseType = "error";
        	$message.= "No response returned \n";
        	$data = array(
	                		'payment_status' => false,
	                		'status' => 'fail',
	                		'message' => $message,
	                	);
                	return $data;
    	}
  		echo $message;
	}
}
