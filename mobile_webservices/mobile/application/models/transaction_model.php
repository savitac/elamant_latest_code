<?php
require_once 'abstract_management_model.php';
/**
 * @package    Provab Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class Transaction_Model extends CI_Model
{
	/**
	 *
	 */
	function logs($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		if ($count) {
			$query = 'select count(*) as total_records from transaction_log where domain_origin='.get_domain_auth_id().' and created_by_id='.$this->entity_user_id;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			
			$query = 'select "INR" as currency, TL.system_transaction_id,TL.transaction_type,TL.domain_origin,TL.app_reference,
				TL.fare,TL.domain_markup as admin_markup,TL.level_one_markup as agent_markup,TL.convinence_fees as convinence_amount,TL.promocode_discount as discount,
				TL.remarks,TL.created_datetime,concat(U.first_name, " ", U.last_name) as username 
			from transaction_log TL 
			LEFT JOIN user U ON TL.created_by_id=U.user_id where TL.domain_origin='.get_domain_auth_id().' and TL.created_by_id='.$user_id.'
			order by TL.origin desc limit '.$offset.', '.$limit;
			return $this->db->query($query)->result_array();
		}
		
	}
	function get_journey_details($app_reference)
	{
		$query='select app_reference,journey_from,journey_to from flight_booking_details where app_reference="'.$app_reference.'"  ';//and status="BOOKING_CONFIRMED"
		$data=$this->db->query($query)->result_array();
		return $data;
	}
	
	function get_city_loc($city_name)
	{
		$query='select city,lat,lng from city_loc_list where city LIKE "%'.$city_name.'%"';
		$data=$this->db->query($query)->result_array();
	
		return $data;
	}

	function get_city_loc2($city_name)
	{
		$query='select city,lat,lng from city_loc_list where city LIKE "%'.$city_name.'%"';
		$data=$this->db->query($query)->result_array();
		
		return $data;
	}
}