<?php
/**
 * Library which has generic functions to get data
 *
 * @package    Provab Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class User_Model extends CI_Model
{
	public function getDevices($user_id='',$type = '',$title='',$description='')
	{
		$android         = $this->db->select('distinct(device_id) as devices')->where('type', $type)->where('device_type', '1')->where('user_id', $user_id)->get('device_ids')->result_array();
       	$data['android'] = array_column($android, 'devices');

        $ios         = $this->db->select('distinct(device_id) as devices')->where('type', $type)->where('device_type', '2')->where('user_id', $user_id)->get('device_ids')->result_array();
        $data['ios'] = array_column($ios, 'devices');

        $data2 = array(
        	'user_id' => $user_id,
        	'type' => $type,
        	'title' => $title,
        	'description' => $description,
        	'added_on' => date('Y-m-d H:i:s')
        );
        $this->db->insert('notification_list',$data2);
        return $data;
	}

	function create_user($email, $password, $first_name='Customer', $last_name='', $phone='', $device_type='', $device_id='', $creation_source='portal', $country_code=92)
	{
		//$data['email'] = provab_encrypt(trim($email));
		$third_party2['email'] = $third_party['email'] = $data['email'] = trim($email);
		//$data['user_name'] = provab_encrypt(trim($email));
		$third_party2['user_name'] = $third_party['user_name'] = $data['user_name'] = trim($email);
		
		if (empty($password) == false and strlen($password) > 3) {
			$data['password'] = provab_encrypt(md5(trim($password)));
			$third_party2['password'] = md5(trim($password));
		}
		$third_party['first_name'] = $data['first_name'] = $first_name;
		$third_party['last_name'] = $data['last_name'] = $last_name;
		$third_party['mobile'] = $data['phone'] = $phone;
		// $data['user_name'] = $first_name;
		$data['domain_list_fk'] = get_domain_auth_id();
		$data ['uuid'] = time () . rand ( 1, 1000 );
		$data ['country_code'] = $country_code;
		$data['creation_source'] = $creation_source;
		if ($creation_source == 'portal') {
			$data['status'] = ACTIVE;
		} else {
			$data['status'] = ACTIVE;
		}
		$third_party['reg_date'] = $data['created_datetime'] = date('Y-m-d H:i:s');
		//$data['modified_datetime'] = date('Y-m-d H:i:s');
		$data['created_by_id'] = intval(@$GLOBALS['CI']->entity_user_id);
		$data['language_preference'] = 'english';
		//$data['user_type'] = B2C_USER;
		$data['user_type'] = B2B_USER;
		// debug($data);exit;
		$insert_id = $this->custom_db->insert_record('user', $data);
		$third_party2['oc_customer_ref_id'] = $third_party['user_id'] = $insert_id = $insert_id['insert_id'];
		$third_party2['user_type'] = 'B2B_USER';
		$third_party2['user_role'] = 'user';

		$third_party_insert_id = $this->custom_db->insert_record('23594_infinite_user_registration_details', $third_party);
		$third_party_insert_id2 = $this->custom_db->insert_record('23594_ft_individual', $third_party2);
		$user_data = $this->custom_db->single_table_records('user', '*', array('user_id' => $insert_id));
		$remarks = $email.' Has Registered From B2B Portal';
		$notification_users = $this->get_admin_user_id();
		$action_query_string = array();
		$action_query_string['user_id'] = $insert_id;
		$action_query_string['uuid'] = $data['uuid'];
		//$action_query_string['user_type'] = B2C_USER;
		$action_query_string['user_type'] = B2B_USER;
			$this->application_logger->registration ( $email, $email . ' Has Registered From B2B Portal', $insert_id, array (
				'user_id' => $insert_id,
				'uuid' => $data ['uuid'] 
		) );
		$b2b_user_details = array();
		$get_admin_currency = $this->custom_db->single_table_records('domain_list','currency_converter_fk',array('domain_key'=>CURRENT_DOMAIN_KEY));
		$b2b_user_details['currency_converter_fk'] = $get_admin_currency['data'][0]['currency_converter_fk'];
		
		
		$image = '';
		$b2b_user_details['user_oid'] = $insert_id;
		$b2b_user_details['balance'] = 0;
		$b2b_user_details['created_datetime'] = date('Y-m-d H:i:s');
		$this->custom_db->insert_record('b2b_user_details', $b2b_user_details);

		if(isset($device_id)){
			$this->update_device_details(array('device_id' => $device_id, 'device_type' => $device_type, 'user_id' => $insert_id, 'type' => '1'));
		}

		/*$data1=array(
			'device_type'=>$device_type,
			'device_id'=>$device_id,
			'user_id'=>$insert_id,
			'type'=>1,
			'created_on'=>date('Y-m-d H:i:s'),
			'modified_on'=>date('Y-m-d H:i:s')
		);						
		$this->db->insert('udid',$data1);*/
		//$this->application_logger->registration($email, $remarks, $insert_id, $action_query_string, array(), $notification_users);
		return $user_data;
	}
	public function update_device_details($params = array())
    {
        $this->db->where('device_id', $params['device_id']);
        $this->db->where('device_type', $params['device_type']);
        //$this->db->where('student_id', $params['student_id']);
        $this->db->where('type', $params['type']);
        $result = $this->db->from('device_ids')->get()->row();
        if (empty($result)) {

            $this->db->set('user_id', $params['user_id']);
            $this->db->set('type', $params['type']);
            $this->db->set('device_id', $params['device_id']);
            $this->db->set('device_type', $params['device_type']);
            $this->db->set('created_on', date('Y-m-d H:i:s'));
            $this->db->insert('device_ids');
            //return true;
        } else {

            $this->db->where('user_id', $params['user_id']);
            $this->db->where('type', $params['type']);
            $this->db->set('device_id', $params['device_id']);
            $this->db->set('device_type', $params['device_type']);
            $this->db->set('created_on', date('Y-m-d H:i:s'));
            $this->db->update('device_ids');
        }
        //return true;
    }
	//sms configuration
	function sms_configuration($sms)
	{
		$tmp_data = $this->db->select('*')->get_where('sms_configuration', array('domain_origin' => $sms));
		//echo $this->db->last_query();exit;
		return $tmp_data->row();
	}
	//social network configuration
	function fb_network_configuration($id,$social)
	{
		//$tmp_data = $this->db->select('config')->get_where('social_login', array('domain_origin' => $id,'social_login_name' => $social));
		//echo $this->db->last_query();exit;
		$social_links = $this->db_cache_api->get_active_social_network_list();
		return isset($social_links[$social]) ? $social_links[$social]['config'] : false;
	}
	
	function google_network_configuration($id,$social)
	{
		$social_links = $this->db_cache_api->get_active_social_network_list();
		return isset($social_links[$social]) ? $social_links[$social]['config'] : false;
	}

	//Global SMS Checkpoint
	function sms_checkpoint($name)
	{
		$result = $this->db->select('status')->get_where('sms_checkpoint', array('condition' => $name))->row();
		//echo $this->db->last_query();exit;
		//echo $result->status;exit;
		return $result->status;
	}
	/***
	 * Registered new user Activation Point
	 */
	function activate_account_status($status, $user_id)
	{
		$data = array(
				'status' => $status
		);
		$this->db->where('user_id', $user_id);
		$this->db->update('user', $data);
	}

	/**
	 * Return current user details who has logged in
	 */
	public function get_user_id($book_id='')
	{
		$result = $this->db->select('created_by_id as user_id')
            ->from('flight_booking_details')
            ->where('app_reference', $book_id)
            ->get();
        return $result->row_array();
	}
	function get_current_user_details()
	{
		if (intval(@$this->entity_user_id) > 0) {
			$cond = array(array('U.user_id', '=', intval($this->entity_user_id)));
				
			$user = $this->get_user_details($cond);
			return $user;
		} else {
			return false;
		}
	}
	public function update_user_loyality_point($points='', $user_id='')
	{
		$this->db->set('loyality_points', (int) $points, false);
        $this->db->where('user_oid', $user_id);
        $this->db->update('b2b_user_details');
        /*echo $this->db->last_query();
        echo "<br>";*/
        return 0;
	}
    public function getUserLoyalityPoint($user_id)
    {
        $result = $this->db->select('*')
            ->from('b2b_user_details')
            //->where('user_oid', $this->entity_user_id)
            ->where('user_oid', $user_id)
            //->where('user_oid', 1355)
            ->get();
        //echo $this->db->last_query();exit;
        //echo $result->status;exit;
        return $result->row_array();
    }

    public function get_point_from_booking_detailsTransfer($app_reference)
    {
        $result = $this->db->select('*')
            ->from('transferv1_booking_details')
            ->where('app_reference', $app_reference)
            ->get();
        //echo $this->db->last_query();exit;
        //echo $result->status;exit;
        return $result->row_array();
    }

    public function log_loyality_point($data)
    {
        $this->db->insert('loyality_points_history', $data);
        /*echo $this->db->last_query();
        echo "<br>";*/
        return 0;
    }

    public function add_loyality_point($points,$user_id)
    {
        $this->db->set('loyality_points', 'loyality_points + ' . (int) $points, false);
        $this->db->where('user_oid', $user_id);
        $this->db->update('b2b_user_details');
        /*echo $this->db->last_query();
        echo "<br>";*/
        return 0;
    }

    public function deduct_loyality_point($points,$user_id)
    {
        $this->db->set('loyality_points', 'loyality_points - ' . (int) $points, false);
        //$this->db->where('user_oid', $this->entity_user_id);
        $this->db->where('user_oid', $user_id);
        $this->db->update('b2b_user_details');
        /*echo $this->db->last_query();
        echo "<br>";*/
        return 0;
    }

    public function get_point_from_booking_details($app_reference)
    {
        $result = $this->db->select('*')
            ->from('flight_booking_details')
            ->where('app_reference', $app_reference)
            ->get();
        //echo $this->db->last_query();exit;
        //echo $result->status;exit;
        return $result->row_array();
    }


    public function get_point_from_transfer_booking_details($app_reference)
    {
        $result = $this->db->select('*')
            ->from('transferv1_booking_details')
            ->where('app_reference', $app_reference)
            ->get();
        //echo $this->db->last_query();exit;
        //echo $result->status;exit;
        return $result->row_array();
    }


    public function get_point_from_hotel_booking_details($app_reference)
    {
        $result = $this->db->select('*')
            ->from('hotel_booking_details')
            ->where('app_reference', $app_reference)
            ->get();
        //echo $this->db->last_query();exit;
        //echo $result->status;exit;
        return $result->row_array();
    }
	/**
	 * Get Active User Details - B2C Only
	 * @param string $username
	 * @param string $password
	 */
	function active_b2c_user($username, $password)
	{
		$condition[] = array('U.status', '=', ACTIVE);
		$condition[] = array('U.domain_list_fk', '=',1);// get_domain_auth_id()
		$condition[] = array('U.user_type', '=', B2C_USER);
		$condition[] = array('U.email', '=',  $this->db->escape(provab_encrypt($username)));
		$condition[] = array('U.password', '=', $this->db->escape(provab_encrypt(md5(trim($password)))));
		return $this->get_user_details($condition);

	}

	/**
	 *verify is the user credentials are valid
	 *
	 *@param string $email    email of the user
	 *@param string @password password of the user
	 *
	 *return boolean status of the user credentials
	 */
	public function get_user_details($condition=array(), $count=false, $offset=0, $limit=10000000000, $order_by=array())
	{

		$filter_condition = ' and ';
		if (valid_array($condition) == true) {
			foreach ($condition as $k => $v) {
				$filter_condition .= implode($v).' and ';
			}
		}

		if (valid_array($order_by) == true) {
			$filter_order_by = 'ORDER BY';
			foreach ($order_by as $k => $v) {
				$filter_order_by .= implode($v).',';
			}
		} else {
			$filter_order_by = '';
		}
		$filter_condition = rtrim($filter_condition, 'and ');
		$filter_order_by = rtrim($filter_order_by, ',');
		if (!$count) {
			return $this->db->query('SELECT U.*, UT.user_type as user_profile_name, ACL.country_code as country_code_value
			FROM user AS U, user_type AS UT, api_country_list AS ACL
		 	WHERE U.user_type=UT.origin 
		 	AND U.country_code=ACL.origin'.$filter_condition.' limit '.$limit.' offset '.$offset.' '.$filter_order_by)->result_array();
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U, user_type AS UT, api_country_list AS ACL
		 WHERE U.user_type=UT.origin 
		 AND U.country_code=ACL.origin'.$filter_condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}
	/**
	 * get Domain user list in the system
	 */
	function get_domain_user_list($condition=array(), $count=false, $offset=0, $limit=10000000000, $order_by=array())
	{
		$filter_condition = ' and ';
		if (valid_array($condition) == true) {
			foreach ($condition as $k => $v) {
				$filter_condition .= implode($v).' and ';
			}
		}
		if(is_domain_user() == false) {
			//PROVAB ADMIN
			//GET ALL DOMAIN ADMINS DETAILS
			$filter_condition .= ' U.domain_list_fk > 0 and U.user_type = '.ADMIN.' and U.user_id != '.intval($this->entity_user_id).' and ';
		} else if(is_domain_user() == true) {
			//DOMAIN ADMIN
			//GET ALL DOMAIN USERS DETAILS
			$filter_condition .= ' U.domain_list_fk ='.get_domain_auth_id().' and U.user_type != '.ADMIN.' and U.user_id != '.intval($this->entity_user_id).' and ';
		}
		if (valid_array($order_by) == true) {
			$filter_order_by = 'ORDER BY';
			foreach ($order_by as $k => $v) {
				$filter_order_by .= implode($v).',';
			}
		} else {
			$filter_order_by = '';
		}
		$filter_condition = rtrim($filter_condition, 'and ');
		$filter_order_by = rtrim($filter_order_by, ',');
		if (!$count) {
			return $this->db->query('SELECT U.*, UT.user_type, ACL.country_code as country_code_value FROM user AS U, user_type AS UT, api_country_list AS ACL
		 WHERE U.user_type=UT.origin 
		 AND U.country_code=ACL.origin'.$filter_condition.' limit '.$limit.' offset '.$offset.' '.$filter_order_by)->result_array();
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U, user_type AS UT, api_country_list AS ACL
		 WHERE U.user_type=UT.origin 
		 AND U.country_code=ACL.origin'.$filter_condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}

	/**
	 * get Logged in Users
	 Jaganath (25-05-2015) - 25-05-2015
	 */
	function get_logged_in_users($condition=array(), $count=false, $offset=0, $limit=10000000000)
	{
		$filter_condition = ' and ';
		if (valid_array($condition) == true) {
			foreach ($condition as $k => $v) {
				$filter_condition .= implode($v).' and ';
			}
		}
		if(is_domain_user() == false) {
			//PROVAB ADMIN
			//GET ALL DOMAIN ADMINS DETAILS
			$filter_condition .= ' U.domain_list_fk > 0 and U.user_type = '.ADMIN.' and U.user_id != '.intval($this->entity_user_id).' and ';
		} else if(is_domain_user() == true) {
			//DOMAIN ADMIN
			//GET ALL DOMAIN USERS DETAILS
			$filter_condition .= ' U.domain_list_fk ='.get_domain_auth_id().' and U.user_type != '.ADMIN.' and U.user_id != '.intval($this->entity_user_id).' and ';
		}
		$filter_condition = rtrim($filter_condition, 'and ');
		$current_date = date('Y-m-d', time());
		if (!$count) {
			return $this->db->query('SELECT U.*, UT.user_type, LM.login_date_time as login_time,LM.logout_date_time as logout_time,LM.login_ip
			FROM user AS U
			JOIN user_type AS UT ON U.user_type=UT.origin
			JOIN api_country_list AS ACL ON U.country_code=ACL.origin
			JOIN login_manager AS LM ON U.user_type=LM.user_type and U.user_id=LM.user_id
		    WHERE LM.login_date_time >="'.$current_date.' 00:00:00"
			and (LM.logout_date_time = "0000-00-00 00:00:00" or LM.logout_date_time >= "'.$current_date.' 00:00:00")
			 '.$filter_condition.' order by LM.logout_date_time asc limit '.$limit.' offset '.$offset)->result_array();
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U
			JOIN user_type AS UT ON U.user_type=UT.origin
			JOIN api_country_list AS ACL ON U.country_code=ACL.origin
			JOIN login_manager AS LM ON U.user_type=LM.user_type and U.user_id=LM.user_id
		    WHERE LM.login_date_time >="'.$current_date.' 00:00:00"
			and (LM.logout_date_time = "0000-00-00 00:00:00" or LM.logout_date_time >= "'.$current_date.' 00:00:00")'.$filter_condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}

	/**
	 * get Domain List present in the system
	 */
	function get_domain_details()
	{
		$query = 'select DL.*,CONCAT(U.first_name, " ", U.last_name) as created_user_name from domain_list DL join user U on DL.created_by_id=U.user_id';
		return $this->db->query($query)->result_array();
	}

	/**
	 *update logout time
	 *
	 *@param number $LID unique login id which has to be updated
	 *
	 *@return status;
	 */
	function update_login_manager($user_id, $login_id)
	{
		$condition = array(
				'user_id' => $user_id,
				'origin' => $login_id
		);
		//update all the logout session in login manager
		$this->custom_db->update_record('login_manager', array('logout_date_time' => date('Y-m-d H:i:s', time())), $condition);
		$this->application_logger->logout($this->entity_name, $this->entity_user_id, array('user_id' => $this->entity_user_id, 'uuid' => $this->entity_uuid));
	}


	/**
	 * Create Login Manager
	 */
	function create_login_auth_record($user_id, $user_type, $user_origin=0, $username='customer')
	{
		$login_details['browser'] = $_SERVER['HTTP_USER_AGENT'];
		$remote_ip = $_SERVER['REMOTE_ADDR'];
		$this->update_auth_record_expiry($user_id, $user_type, $remote_ip, $user_origin, $username);
		//logout of same user from same ip
		$login_details['info'] = file_get_contents("http://ipinfo.io/".$remote_ip."/json");
		$data['user_id'] = $user_id;
		$data['user_type'] = $user_type;
		$data['login_date_time'] = date('Y-m-d H:i:s');
		$data['login_ip'] = $remote_ip;
		$data['attributes'] = json_encode($login_details);

		$login_id = $this->custom_db->insert_record('login_manager', $data);
		$this->application_logger->login($username, $user_origin, array('user_id' => $user_origin, 'uuid' => $user_id));
		return $login_id['insert_id'];
	}

	/**
	 * Update logout
	 * @param $user_id
	 * @param $user_type
	 * @param $remote_ip
	 * @param $browser
	 */
	function update_auth_record_expiry($user_id, $user_type, $remote_ip, $user_origin, $username)
	{
		$cond['user_id'] = $user_id;
		$cond['user_type'] = $user_type;
		$cond['login_ip'] = $remote_ip;
		$auth_exp = $this->custom_db->update_record('login_manager', array('logout_date_time' => date('Y-m-d H:i:s')), $cond);
		if ($auth_exp == true) {
			//update application logger
			$this->application_logger->logout($username, $user_origin, array('user_id' => $user_origin, 'uuid' => $user_id));
		}
	}

	public function email_subscribtion($email,$domain_key)
	{
		$query = $this->db->get_where('email_subscribtion', array('email_id' => $email));
		if($query->num_rows() > 0){
			return "already";
		}else{
			$insert_id = $this->custom_db->insert_record('email_subscribtion',array('email_id' => $email,'domain_list_fk' => $domain_key));
			return $insert_id['insert_id'];
		}
	}
	/**
	 * Jaganath
	 */
	public function user_traveller_details($search_chars)
	{
		$raw_search_chars = $this->db->escape($search_chars);
		$r_search_chars = $this->db->escape($search_chars.'%');
		$search_chars = $this->db->escape('%'.$search_chars.'%');
		$query = 'select * from user_traveller_details where created_by_id='.intval($this->entity_user_id).' and (first_name like '.$search_chars.'
		OR 	last_name like '.$search_chars.')
		ORDER BY first_name ASC	LIMIT 0, 20';
		return $this->db->query($query);
	}
	/**
	 * Jaganath
	 */
	function get_user_traveller_details()
	{
		$query = 'select * from user_traveller_details 
		where created_by_id='.intval(@$this->entity_user_id).' ORDER BY first_name ASC';
		return $this->db->query($query)->result_array();
	}
	/**
	sudheep offline payment
	**/
	function offline_payment_insert($params){
		//$query = "INSERT INTO `offline_payment`(`id`, `company_name`, `name`, `email`, `phone`, `amount`, `remarks`, `created_date`, `refernce_code`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9])";

		$created_date = date('Y-m-d H:i:s');
		$coded = str_shuffle($params['data'][0]['value'].$params['data'][3]['value']);

		$insert_id = $this->custom_db->insert_record('offline_payment', array($params['data'][0]['name']=> $params['data'][0]['value'], $params['data'][1]['name']=> $params['data'][1]['value'], $params['data'][2]['name']=> $params['data'][2]['value'], $params['data'][3]['name']=> $params['data'][3]['value'], $params['data'][4]['name']=> $params['data'][4]['value'], $params['data'][5]['name']=> $params['data'][5]['value'],'created_date' =>$created_date,'refernce_code'=>$coded	));

			return array('db'=>$insert_id, 'refernce_code'=>$coded);

	}
	function offline_approval($cd){
		$query = "SELECT * FROM `offline_payment` WHERE `refernce_code` = '$cd' ";

	$ret = $this->db->query($query)->result_array();
	return $ret;
	}
	/**
	 * Jaganath
	 */
	function get_admin_user_id()
	{
		$admin_user_id = array();
		$cond[] = array('U.user_type', '=', ADMIN);
		$cond[] = array('U.status', '=', ACTIVE);
		$cond[] = array('U.domain_list_fk', '=', get_domain_auth_id());
		$user_details = $this->get_user_details($cond);
		foreach($user_details as $k => $v){
			$admin_user_id[$k] = $v['user_id'];
		}
		return $admin_user_id;
	}

	function flight_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) AS total_records from flight_booking_details BD
					where domain_origin='.get_domain_auth_id().' AND BD.created_by_id ='.$user_id.' '.$condition;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			//Booking Details
			$bd_query = 'select *,total_fare,BTD.origin as trans_id
			 from flight_booking_details AS BD JOIN  flight_booking_transaction_details AS BTD
						WHERE BD.app_reference = BTD.app_reference and BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition.'
						order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;
			// echo $bd_query;
			$booking_details	= $this->db->query($bd_query)->result_array();
			// debug($booking_details); exit('aaaaa');
			
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				//Itinerary Details
				$id_query = 'select * from flight_booking_itinerary_details AS ID
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				//Transaction Details
				$td_query = 'select * from flight_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.')';
				//Customer and Ticket Details
				$cd_query = 'select CD.*,FPTI.TicketId,FPTI.TicketNumber,FPTI.IssueDate,FPTI.Fare,FPTI.SegmentAdditionalInfo
							from flight_booking_passenger_details AS CD
							left join flight_passenger_ticket_info FPTI on CD.origin=FPTI.passenger_fk
							WHERE CD.flight_booking_transaction_details_fk IN 
							(select TD.origin from flight_booking_transaction_details AS TD 
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//Cancellation Details
				$cancellation_details_query = 'select FCD.*
						from flight_booking_passenger_details AS CD
						left join flight_cancellation_details AS FCD ON FCD.passenger_fk=CD.origin
						WHERE CD.flight_booking_transaction_details_fk IN 
						(select TD.origin from flight_booking_transaction_details AS TD 
						WHERE TD.app_reference IN ('.$app_reference_ids.'))';
					
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$booking_transaction_details = $this->db->query($td_query)->result_array();
				$cancellation_details = $this->db->query($cancellation_details_query)->result_array();
			}
			$response['data']['booking_details']			= $booking_details;
			//$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			// $response['data']['booking_transaction_details']	= $booking_transaction_details;
			$response['data']['booking_customer_details']	= $booking_customer_details;
			//$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}

	function hotel_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) as total_records 
					from hotel_booking_details BD
					join hotel_booking_itinerary_details AS HBID on BD.app_reference=HBID.app_reference
					join payment_option_list AS POL on BD.payment_mode=POL.payment_category_code 
					where BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$cancellation_details = array();
			$bd_query = 'select *,total_fare,BD.attributes from hotel_booking_details AS BD JOIN hotel_booking_itinerary_details as HBI
						WHERE HBI.app_reference = BD.app_reference and BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition.'
						order by BD.origin desc limit '.$offset.', '.$limit;
			$booking_details = $this->db->query($bd_query)->result_array();
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				$id_query = 'select * from hotel_booking_itinerary_details AS ID 
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				$cd_query = 'select * from hotel_booking_pax_details AS CD 
							WHERE CD.app_reference IN ('.$app_reference_ids.')';
				$cancellation_details_query = 'select * from hotel_cancellation_details AS HCD 
							WHERE HCD.app_reference IN ('.$app_reference_ids.')';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$cancellation_details	= $this->db->query($cancellation_details_query)->result_array();
			}
			//$response['data']['booking_details']			= $booking_details;
			$response			= $booking_details;
			//$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			//$response['data']['booking_customer_details']	= $booking_customer_details;
			//$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}

	function sightseeing_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
        //BT, CD, ID
        if ($count) {
            $query = 'select count(distinct(BD.app_reference)) as total_records 
                    from sightseeing_booking_details BD
                    join sightseeing_booking_itinerary_details AS HBID on BD.app_reference=HBID.app_reference
                    join payment_option_list AS POL on BD.payment_mode=POL.payment_category_code 
                    where BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.' '.$condition;
            //echo $query;exit;
            $data = $this->db->query($query)->row_array();
            return $data['total_records'];
        } else {
            $this->load->library('booking_data_formatter');
            $response['status'] = SUCCESS_STATUS;
            $response['data'] = array();
            $booking_itinerary_details  = array();
            $booking_customer_details   = array();
            $cancellation_details = array();
            $bd_query = 'select BD.*,BID.total_fare from sightseeing_booking_details AS BD,sightseeing_booking_itinerary_details AS BID WHERE
                        BD.app_reference=BID.app_reference and BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.' '.$condition.'
                        order by BD.origin desc limit '.$offset.', '.$limit;
                        // debug($bd_query);exit;
            $booking_details = $this->db->query($bd_query)->result_array();
            $app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
            if(empty($app_reference_ids) == false) {
                $id_query = 'select * from sightseeing_booking_itinerary_details AS ID 
                            WHERE ID.app_reference IN ('.$app_reference_ids.')';
                $cd_query = 'select * from  sightseeing_booking_pax_details AS CD 
                            WHERE  CD.app_reference IN ('.$app_reference_ids.') ';
                $cancellation_details_query = 'select * from sightseeing_cancellation_details AS HCD 
                            WHERE HCD.app_reference IN ('.$app_reference_ids.')';
                $booking_itinerary_details  = $this->db->query($id_query)->result_array();
                $booking_customer_details   = $this->db->query($cd_query)->result_array();
                $cancellation_details   = $this->db->query($cancellation_details_query)->result_array();
            }
            $response['data']['booking_details']            = $booking_details;
            $response['data']['booking_itinerary_details']  = $booking_itinerary_details;
            $response['data']['booking_customer_details']   = $booking_customer_details;
            $response['data']['cancellation_details']   = $cancellation_details;
            return $response;
        }
	}
	

	/**
	 * Shubham Jain
 	*/

 	function transfers_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//print_r($condition);exit();
        //BT, CD, ID
        if ($count) {
            $query = 'select count(distinct(BD.app_reference)) as total_records 
                    from transferv1_booking_details BD
                    join transferv1_booking_itinerary_details AS HBID on BD.app_reference=HBID.app_reference
                    join payment_option_list AS POL on BD.payment_mode=POL.payment_category_code 
                    where BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.' '.$condition;
            //echo $query;exit;
            $data = $this->db->query($query)->row_array();
            return $data['total_records'];
        } else {
            $this->load->library('booking_data_formatter');
            $response['status'] = SUCCESS_STATUS;
            $response['data'] = array();
            $booking_itinerary_details  = array();
            $booking_customer_details   = array();
            $cancellation_details = array();
            $bd_query = 'select BD.*,BID.total_fare from transferv1_booking_details AS BD,transferv1_booking_itinerary_details AS BID WHERE
                        BD.app_reference=BID.app_reference and BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.' '.$condition.'
                        order by BD.origin desc limit '.$offset.', '.$limit;
                        // debug($bd_query);exit;
            $booking_details = $this->db->query($bd_query)->result_array();
            //echo $this->db->last_query();
            $app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
            if(empty($app_reference_ids) == false) {
                $id_query = 'select * from transferv1_booking_itinerary_details AS ID 
                            WHERE ID.app_reference IN ('.$app_reference_ids.')';
                $cd_query = 'select * from  transferv1_booking_pax_details AS CD 
                            WHERE  CD.app_reference IN ('.$app_reference_ids.') ';
                $cancellation_details_query = 'select * from transferv1_cancellation_details AS HCD 
                            WHERE HCD.app_reference IN ('.$app_reference_ids.')';
                $booking_itinerary_details  = $this->db->query($id_query)->result_array();
                $booking_customer_details   = $this->db->query($cd_query)->result_array();
                //debug($booking_customer_details); exit();
                $cancellation_details   = $this->db->query($cancellation_details_query)->result_array();
            }

            $response['data']['booking_details']            = $booking_details;
            $response['data']['booking_itinerary_details']  = $booking_itinerary_details;
            $response['data']['booking_customer_details']   = $booking_customer_details;
            //debug($response['data']['booking_customer_details']); exit();
            $response['data']['cancellation_details']   = $cancellation_details;
            return $response;
        }
	}

	function sightseeing_shared_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
        //BT, CD, ID
        if ($count) {
            $query = 'select count(distinct(BD.app_reference)) as total_records 
                    from sightseeing_booking_details BD
                    join sightseeing_booking_itinerary_details AS HBID on BD.app_reference=HBID.app_reference
                    join payment_option_list AS POL on BD.payment_mode=POL.payment_category_code 
                    where BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.' '.$condition;
            //echo $query;exit;
            $data = $this->db->query($query)->row_array();
            return $data['total_records'];
        } else {
            $this->load->library('booking_data_formatter');
            $response['status'] = SUCCESS_STATUS;
            $response['data'] = array();
            $booking_itinerary_details  = array();
            $booking_customer_details   = array();
            $cancellation_details = array();
            $tagged_bookings= $this->get_tagged_bookings($user_id);
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($tagged_bookings);
			// debug($tagged_bookings);exit;
			//Booking Details
			if(empty($app_reference_ids) == false) {
	            $bd_query = 'select * from sightseeing_booking_details AS BD 
	                        WHERE BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.' '.$condition.'
	                        order by BD.origin desc limit '.$offset.', '.$limit;
	            $booking_details = $this->db->query($bd_query)->result_array();
	            $app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
	            if(empty($app_reference_ids) == false) {
	                $id_query = 'select * from sightseeing_booking_itinerary_details AS ID 
	                            WHERE ID.app_reference IN ('.$app_reference_ids.')';
	                $cd_query = 'select * from  sightseeing_booking_pax_details AS CD 
	                            WHERE  CD.app_reference IN ('.$app_reference_ids.') ';
	                $cancellation_details_query = 'select * from sightseeing_cancellation_details AS HCD 
	                            WHERE HCD.app_reference IN ('.$app_reference_ids.')';
	                $booking_itinerary_details  = $this->db->query($id_query)->result_array();
	                $booking_customer_details   = $this->db->query($cd_query)->result_array();
	                $cancellation_details   = $this->db->query($cancellation_details_query)->result_array();
	            }
	            $response['data']['booking_details']            = $booking_details;
	            $response['data']['booking_itinerary_details']  = $booking_itinerary_details;
	            $response['data']['booking_customer_details']   = $booking_customer_details;
	            $response['data']['cancellation_details']   = $cancellation_details;
	        }
            return $response;
        }
	}


	function flight_shared_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) AS total_records from flight_booking_details BD
					where domain_origin='.get_domain_auth_id().' AND BD.created_by_id ='.$user_id.' '.$condition;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			$tagged_bookings= $this->get_tagged_bookings($user_id);
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($tagged_bookings);
			// debug($tagged_bookings);exit;
			//Booking Details
			if(empty($app_reference_ids) == false) {
				$bd_query = 'select *,total_fare,BTD.origin as trans_id,
				 (SELECT latitude  FROM `flight_airport_list` WHERE `airport_code` LIKE BD.from_loc) as from_loc_latitude,
				 (SELECT longitude  FROM `flight_airport_list` WHERE `airport_code` LIKE BD.from_loc) as from_loc_longitude,
				 (SELECT latitude  FROM `flight_airport_list` WHERE `airport_code` LIKE BD.to_loc) as to_loc_latitude,
				 (SELECT longitude  FROM `flight_airport_list` WHERE `airport_code` LIKE BD.to_loc) as to_loc_longitude
				 from flight_booking_details AS BD JOIN  flight_booking_transaction_details AS BTD
							WHERE BD.app_reference = BTD.app_reference and BD.domain_origin='.get_domain_auth_id().' and BTD.app_reference IN ('.$app_reference_ids.') '.$condition.'
							order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;
				// echo $bd_query;
				$booking_details	= $this->db->query($bd_query)->result_array();
				// debug($booking_details); exit('aaaaa');
				$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($tagged_bookings);
				if(empty($app_reference_ids) == false) {
					//Itinerary Details
					$id_query = 'select * from flight_booking_itinerary_details AS ID
								WHERE ID.app_reference IN ('.$app_reference_ids.')';
					//Transaction Details
					$td_query = 'select * from flight_booking_transaction_details AS TD
								WHERE TD.app_reference IN ('.$app_reference_ids.')';
					//Customer and Ticket Details
					$cd_query = 'select CD.*,FPTI.TicketId,FPTI.TicketNumber,FPTI.IssueDate,FPTI.Fare,FPTI.SegmentAdditionalInfo
								from flight_booking_passenger_details AS CD
								left join flight_passenger_ticket_info FPTI on CD.origin=FPTI.passenger_fk
								WHERE CD.flight_booking_transaction_details_fk IN 
								(select TD.origin from flight_booking_transaction_details AS TD 
								WHERE TD.app_reference IN ('.$app_reference_ids.'))';
					//Cancellation Details
					$cancellation_details_query = 'select FCD.*
							from flight_booking_passenger_details AS CD
							left join flight_cancellation_details AS FCD ON FCD.passenger_fk=CD.origin
							WHERE CD.flight_booking_transaction_details_fk IN 
							(select TD.origin from flight_booking_transaction_details AS TD 
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
						
					$booking_itinerary_details	= $this->db->query($id_query)->result_array();
					$booking_customer_details	= $this->db->query($cd_query)->result_array();
					$booking_transaction_details = $this->db->query($td_query)->result_array();
					$cancellation_details = $this->db->query($cancellation_details_query)->result_array();
				}
				$response['data']['booking_details']			= $booking_details;
				//$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
				// $response['data']['booking_transaction_details']	= $booking_transaction_details;
				$response['data']['booking_customer_details']	= $booking_customer_details;
			}
			//$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}

	function hotel_shared_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) as total_records 
					from hotel_booking_details BD
					join hotel_booking_itinerary_details AS HBID on BD.app_reference=HBID.app_reference
					join payment_option_list AS POL on BD.payment_mode=POL.payment_category_code 
					where BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$cancellation_details = array();
			$tagged_bookings= $this->get_tagged_bookings($user_id);
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($tagged_bookings);
			if(empty($app_reference_ids) == false) {
				$bd_query = 'select *,total_fare,BD.attributes from hotel_booking_details AS BD JOIN hotel_booking_itinerary_details as HBI
							WHERE HBI.app_reference = BD.app_reference and BD.domain_origin='.get_domain_auth_id().' and BD.app_reference IN ('.$app_reference_ids.')'.$condition.'
							order by BD.origin desc limit '.$offset.', '.$limit;
				$booking_details = $this->db->query($bd_query)->result_array();
				$tagged_bookings= $this->user_model->get_tagged_bookings($user_id);
				// debug($tagged_bookings);exit;
				$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($tagged_bookings);
				if(empty($app_reference_ids) == false) {
					$id_query = 'select * from hotel_booking_itinerary_details AS ID 
								WHERE ID.app_reference IN ('.$app_reference_ids.')';
					$cd_query = 'select * from hotel_booking_pax_details AS CD 
								WHERE CD.app_reference IN ('.$app_reference_ids.')';
					$cancellation_details_query = 'select * from hotel_cancellation_details AS HCD 
								WHERE HCD.app_reference IN ('.$app_reference_ids.')';
					$booking_itinerary_details	= $this->db->query($id_query)->result_array();
					$booking_customer_details	= $this->db->query($cd_query)->result_array();
					$cancellation_details	= $this->db->query($cancellation_details_query)->result_array();
				}
				$response['data']['booking_details']			= $booking_details;
			}
			//$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			//$response['data']['booking_customer_details']	= $booking_customer_details;
			//$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}

	


	function get_stories($user_id='',$start=0,$limit=200,$count=0,$hash='',$story_id='',$profile=0,$profile_user_id='')
	{
		if($count==0)
		{
			if($user_id==''){
				if($story_id!=''){

					$story_query='select * from reviews where review_id="'.$story_id.'" ORDER by posted_date DESC LIMIT '.$start.','.$limit.'';

				}else{
					$story_query='select * from reviews where user_id in (select user_id from user where user_id in (select user_id from review_friends where follower_id="'.$user_id.'") or user_id in (select follower_id from review_friends where user_id="'.$user_id.'") or user_id="'.$user_id.'" or privacy="public" and user_type="4") or review_id in (select post_id from review_tags where tag_friend_id="'.$user_id.'")  or user_id = "31" ORDER by posted_date DESC LIMIT '.$start.','.$limit.'';
				}
			}else{
				if($hash!='')
				{
				// debug($hash);exit;
					$story_query='select * from reviews where ((hashtags like "%'.$hash.'%")) ORDER by posted_date DESC LIMIT '.$start.','.$limit.'';
					// debug($story_query);exit;
				}else 
				if($story_id!='')
				{
					$story_query='select * from reviews where review_id="'.$story_id.'" ORDER by posted_date DESC LIMIT '.$start.','.$limit.'';

				}else if($profile==1 ){
					if(isset($profile_user_id))
					{

					$story_query='select * from reviews where user_id="'.$profile_user_id.'" ORDER by posted_date DESC LIMIT '.$start.','.$limit.'';
					}else{
					$story_query='select * from reviews where user_id="'.$user_id.'" ORDER by posted_date DESC LIMIT '.$start.','.$limit.'';
						
					}
				}else{
					$story_query='select * from reviews where user_id in (select user_id from user where user_id in (select user_id from review_friends where follower_id="'.$user_id.'") or user_id in (select follower_id from review_friends where user_id="'.$user_id.'") or user_id="'.$user_id.'" or privacy="public" and user_type="4") or review_id in (select post_id from review_tags where tag_friend_id="'.$user_id.'")  or user_id = "31" ORDER by posted_date DESC LIMIT '.$start.','.$limit.'';
				}
			}
			// debug($story_query);exit;
			$story_data=$this->db->query($story_query)->result_array();
			// debug($story_data);
			// echo 'in'; exit;
			// $story_data['unread']=0;
			// $story_data['read']=0;
			foreach ($story_data as $key => $value) {
				$story_data[$key]['remarks']=preg_replace('/<\/?a[^>]*>/','',$value['remarks']);
				// debug($story_data[$key]['posted_date']);
				$story_data[$key]['posted_date']=date('Y-m-d H-m-s',strtotime($story_data[$key]['posted_date']));
				// debug($story_data[$key]['posted_date']);
				// exit;
				// debug($value);exit;
		// debug($story_data);exit;
				$img_data=$this->format_img_data($story_data[$key]['images'],$story_data[$key]['thumbnail']);
				if(!empty($img_data) && $img_data!='null')
				{

					$story_data[$key]['image_data']=$img_data;
				}else{
					$story_data[$key]['image_data']="[]";
				}
				if(isset($value['hashtags']) && (is_string($value['hashtags']) && ($value['hashtags']=='null'))){
					$story_data[$key]['hashtags'] = '[]';
				}
				$user_query='select user_id,first_name,last_name,email,phone,image,user_id,login_status,last_activity_ts,policies,privacy from user where user_id="'.$story_data[$key]['user_id'].'"';
				$user_data=$this->db->query($user_query)->result_array();
				// debug($user_data);exit;
				$friend_status=$this->get_friend_status($user_id,$story_data[$key]['user_id']);
				$story_data[$key]['user_data']=$user_data[0];
				$story_data[$key]['user_data']['friend_status']=$friend_status;
				if($story_data[$key]['user_data']['login_status']==1){
					$datetime1 = date_create(date('Y-m-d H:i:s'));
					$date_time = DateTime::createFromFormat('Y-m-d H:i:s', $story_data[$key]['user_data'][0]['last_activity_ts']);
					// $date2 = $date_time->format('Y-m-d H:i:s');
					$datetime2 = date_create($date_time);
					// debug($datetime2);exit;
					$interval = date_diff($datetime1,$datetime2);
					$interval=json_decode(json_encode($interval),true);
					// debug($interval);exit;
					$year=$interval['y'];
					$month=$interval['m'];
					$day=$interval['d'];
					$hour=$interval['h'];
					$mins=$interval['i'];
					if(!($year==0 && $month==0 && $day==0 && $hour==0 && $mins<3) || $story_data[$key]['user_data'][0]['last_activity_ts']=='0000-00-00 00:00:00.000000')
					{
						// debug($story_data[$key]);exit;
						$story_data[$key]['user_data'][0]['login_status']=0;
						$this->update_login_status('',$story_data[$key]['user_data'][0]['user_id'],false);
					}
				}
				
				$comments_query='select * from `review_comments` where review_id="'.$story_data[$key]['review_id'].'" order by comment_date DESC ';
				// debug($comments_query);
				$comments_data=$this->db->query($comments_query)->result_array();
				$comments_count=0;
				foreach ($comments_data as $k => $v) {
					$comments_data[$k]['posted_date']=app_friendly_absolute_date($comments_data[$k]['comment_date']);
					$user_query='select user_id,first_name,last_name,email,phone,image,login_status,last_activity_ts,policies,privacy from user where user_id="'.$comments_data[$k]['user_id'].'"';
					$user_data=$this->db->query($user_query)->result_array();
					$comments_data[$k]['user_data']=$user_data[0];

					if($comments_data[$k]['user_data']['login_status']==1){
						$datetime1 = date_create(date('Y-m-d H:i:s'));
						$date_time = DateTime::createFromFormat('Y-d-m H:i:s', $comments_data[$k]['user_data'][0]['last_activity_ts']);
						// $date2 = $date_time->format('Y-m-d H:i:s');
						$datetime2 = date_create($date_time);
						// debug($datetime2);exit;
						$interval = date_diff($datetime1,$datetime2);
						$interval=json_decode(json_encode($interval),true);
						// debug($interval);exit;
						$year=$interval['y'];
						$month=$interval['m'];
						$day=$interval['d'];
						$hour=$interval['h'];
						$mins=$interval['i'];
						if(!($year==0 && $month==0 && $day==0 && $hour==0 && $mins<3) || $comments_data[$k]['user_data'][0]['last_activity_ts']=='0000-00-00 00:00:00.000000')
						{
							// echo "inside";exit;
							$comments_data[$k]['user_data'][0]['login_status']=0;
							$this->update_login_status('',$comments_data[$k]['user_data'][0]['user_id'],false);
						}
					}
					$comments_count+=1;
				}
				// debug($story_data[$key]['review_id']);debug($user_id);exit;
				$likes_query='select * from review_likes where review_id="'.$story_data[$key]['review_id'].'" and user_id="'.$user_id.'" ';
				$like_data=$this->db->query($likes_query)->result_array();
				if(valid_array($like_data))
				{
					$story_data[$key]['like_status']=1;
				}else{
					$story_data[$key]['like_status']=0;
				}


				// debug($like_data);exit;
				/*$likes_count_query='select count(id) as count from review_likes where review_id="'.$story_data[$key]['review_id'].'"';
				$like_count=$this->db->query($likes_count_query)->result_array();*/
				// debug($like_count);exit;
				$likes_data=$this->get_likes_list($user_id,$story_data[$key]['review_id']);
				if(valid_array($likes_data))
				{
					$story_data[$key]['likes_count']=count($likes_data);
					$story_data[$key]['likes_data']=$likes_data;
				}else{
					$story_data[$key]['likes_count']=0;
				}
				$story_data[$key]['comments_count']=$comments_count;
				$story_data[$key]['comments_data']=$comments_data;
				$story_data[$key]['image_path']=$GLOBALS['CI']->template->domain_images();
				$story_data[$key]['default_profile_img']=$GLOBALS['CI']->template->template_images('user-pic.png');
				if($story_data[$key]['share_status']==1)
				{

					$shared_user_query='select user_id,first_name,last_name,email,phone,image,login_status,last_activity_ts,policies,privacy from user where user_id="'.$story_data[$key]['parent_user_id'].'"';
					$parent_user_data=$this->db->query($shared_user_query)->result_array();
					$story_data[$key]['parent_user_data']=$parent_user_data;
				}else{
					$story_data[$key]['share_status']=0;
				}

				$tagged_friends_query='select * from review_tags where post_id="'.$story_data[$key]['review_id'].'"';
				$tagged_data=$this->db->query($tagged_friends_query)->result_array();
				// debug($tagged_data);exit;
				if(isset($tagged_data) && !empty($tagged_data))
				{
					foreach ($tagged_data as $t_key => $t_value) {
						$tagged_user_query='select first_name,user_id,last_name,email,phone,image,login_status,last_activity_ts,policies,privacy  from user where user_id="'.$tagged_data[$t_key]['tag_friend_id'].'"';
						// debug($tagged_user_query);
						$tagged_user_data=$this->db->query($tagged_user_query)->result_array();
						// debug($tagged_user_data);exit;
						$tagged_user_list[$t_key]=$tagged_user_data[0];
					}
					$story_data[$key]['tagged_friends']=$tagged_user_list;
					$story_data[$key]['tag_friend_status']=1;
				}else{
					$story_data[$key]['tag_friend_status']=0;
				}
			}
			
			}else{
				if($user_id==''){
					$story_query='select count(*) as story_count from reviews where user_id in (select user_id from user where user_id in (select user_id from review_friends where follower_id="'.$user_id.'") or user_id in (select follower_id from review_friends where user_id="'.$user_id.'") or user_id="'.$user_id.'" or privacy="public" and user_type="4") or user_id = "31" ORDER by posted_date DESC LIMIT '.$start.','.$limit.'';
				}else{
					$story_query='select count(*) as story_count from reviews where user_id="'.$user_id.'" ORDER by posted_date DESC LIMIT '.$start.','.$limit.'';
				}
				// debug($story_query);
				$story_data=$this->db->query($story_query)->result_array();
			
			}

		return $story_data;
	}

	function get_all_replies($story_id)
	{
		$comments_query='select * from review_comments where review_id="'.$story_id.'" LIMIT 20';
		$comments_data=$this->db->query($comments_query)->result_array();
		foreach ($comments_data as $key => $value) {
			$comments_data[$key]['posted_date']=app_friendly_absolute_date($comments_data[$key]['comment_date']);
			$user_query='select first_name,last_name,email,phone,image from user where user_id="'.$comments_data[$key]['user_id'].'"';
			$user_data=$this->db->query($user_query)->result_array();
			$comments_data[$key]['user_data']=$user_data;
		}

		return $comments_data;
	}



	function get_all_images($story_id)
	{
		$comments_query='select images from reviews where review_id="'.$story_id.'"';
		$comments_data=$this->db->query($comments_query)->result_array();
		return $comments_data;
	}

	function check_view_exist($user_id,$story_id)
	{
		$exist_query='select * from review_views where review_id="'.$story_id.'" and user_id="'.$user_id.'"';
		$view_exist_data=$this->db->query($exist_query)->result_array();
		if(valid_array($view_exist_data))
		{
			return 1;
		}else{
			return 0;
		}
	}

	function datalist_by_key($key)
	{
		$query="select concat(first_name,' ',last_name)as name from user where first_name like '%$key%' OR last_name like '%$key%'";
		
		$results=$this->db->query($query)->result_array();
		if(valid_array($results))
		{
			return $results;
		}else{
			return false;
		}
	}

	function get_friend_list($user_id,$offset='0',$limit='10',$count=0)
	{
		if($count==0){
			$query='select first_name,last_name,email,address,user_id,image FROM `user` where user_id in (select follower_id from review_friends where user_id="'.$user_id.'" and follower_id not in ('.$user_id.') and status="1")  limit '.$offset.','.$limit.'';
		
			$followers_results=$this->db->query($query)->result_array();
			$query='select first_name,last_name,email,address,user_id,image FROM `user` where user_id in (select user_id from review_friends where follower_id="'.$user_id.'" and status="1" and user_id not in ('.$user_id.'))  limit '.$offset.','.$limit.'';

			$following_results=$this->db->query($query)->result_array();
			return array_merge($followers_results,$following_results);
		}else{
			$query='select count(first_name) as friends_count FROM `user` where user_id in (select follower_id from review_friends where user_id="'.$user_id.'" and follower_id not in ('.$user_id.') and status="1")  limit '.$offset.','.$limit.'';
			$followers_results=$this->db->query($query)->result_array();
			$query='select count(first_name) as friends_count FROM `user` where user_id in (select user_id from review_friends where follower_id="'.$user_id.'" and status="1" and user_id not in ('.$user_id.'))  limit '.$offset.','.$limit.'';
			$following_results=$this->db->query($query)->result_array();
			// debug($following_results);exit;

			return $followers_results[0]['friends_count']+$following_results[0]['friends_count'];
		}
		
		// return $results;
	}

	function get_friend_suggestion($user_id,$offset='0',$limit='10')
	{
		$query='select * from user where user_id!="'.$user_id.'" and user_type=4 and status=1';

		$friends_available=$this->db->query($query)->result_array();
		// debug($friends_available);
	
		foreach ($friends_available as $key => $value) {
			$user_id_query='select * from review_friends where user_id="'.$friends_available[$key]['user_id'].'" and follower_id="'.$user_id.'"';
			$user_id_results=$this->db->query($user_id_query)->result_array();
			// debug($user_id_results);exit;
			if(valid_array($user_id_results) && isset($user_id_results))
			{
				// found as user
				if($user_id_results[0]['status']=='0')
				{
					$friends_available[$key]['friend_status']='got_friend_request';//got_friend_request
					
				}else if($user_id_results[0]['status']=='2'){
					$friends_available[$key]['friend_status']='no_friend';
				
				}else if($user_id_results[0]['status']=='1'){
					$friends_available[$key]['friend_status']='friend';
				
				}
				

			}else{
				//not found as user
				// debug($friends_available);exit;
				$follower_id_query='select * from review_friends where follower_id="'.$friends_available[$key]['user_id'].'" and user_id="'.$user_id.'" ';
				$follower_id_results=$this->db->query($follower_id_query)->result_array();
				// if($key=='6')
				// {
					
				// debug($follower_id_results);exit;
				// }
				if(valid_array($follower_id_results) && isset($follower_id_results))
				{
					// found as follower
					if($follower_id_results[0]['status']=='0')
					{
						$friends_available[$key]['friend_status']='friend_request_sent';//got_friend_request
						
					}else if($follower_id_results[0]['status']=='2'){
						$friends_available[$key]['friend_status']='no_friend';
						
					}else if($follower_id_results[0]['status']=='1'){
						$friends_available[$key]['friend_status']='friend';
						
					}
				}else{
						$friends_available[$key]['friend_status']='no_friend';
						
				}
			}
		}
		// debug($friends_available);exit;
		return $friends_available;
	}

	function cancel_friend_request($user_id,$follower_id)
	{
		$query='delete from review_friends where user_id="'.$user_id.'" and follower_id="'.$follower_id.'"';
		$status=$this->db->query($query);
		$query='delete from review_notifications where user_id="'.$user_id.'" and notifier_id="'.$follower_id.'"';
		$status=$this->db->query($query);
		return $status;
	}

	function get_notifications($user_id)
	{
		$query='select * from review_notifications where notifier_id="'.$user_id.'" order by date desc';
		$results=$this->db->query($query)->result_array();

		// debug($results);exit;
		foreach ($results as $key => $value) {
			$notifier_query='select first_name,last_name,email,phone,image,user_id,policies,privacy from user where user_id="'.$results[$key]['user_id'].'"';
			$notifier_data=$this->db->query($notifier_query)->result_array();
			$results[$key]['notifier_data']=$notifier_data;
			switch ($results[$key]['notification_type']) {
			case '1':
				$message=$notifier_data[0]['first_name'].' '.$notifier_data[0]['last_name'].' liked your post';
				$results[$key]['notification_type']='like';
				break;
			case '2':
				$message=$notifier_data[0]['first_name'].' '.$notifier_data[0]['last_name'].' Commented on your post';
				$results[$key]['notification_type']='comment';
				break;
			case '3':
				$message=$notifier_data[0]['first_name'].' '.$notifier_data[0]['last_name'].' wants to follow you';
				$results[$key]['notification_type']='follow_request';
				break;
			case '4':
				$message=$notifier_data[0]['first_name'].' '.$notifier_data[0]['last_name'].' has accepted your follow request';
				$results[$key]['notification_type']='request_accepted';
				break;
			case '5':
				$message=$notifier_data[0]['first_name'].' '.$notifier_data[0]['last_name'].' has Tagged you';
				$results[$key]['notification_type']='tagged_in';
				break;
			case '6':
				$message=$notifier_data[0]['first_name'].' '.$notifier_data[0]['last_name'].' Shared your post';
				$results[$key]['notification_type']='shared_your_post';
				break;
			
			default:
				
				break;
		}
			
			$results[$key]['date']=app_friendly_absolute_date($results[$key]['date']);
			
			// debug($results[$key]['notification_type']);exit;
			
		$results[$key]['message']=$message;
		}
// debug($results);exit;
		return $results;
	}

	function accept_follow($notifier_id,$user_id)
	{
		$accept_query='update review_notifications set notification_type=5 where user_id="'.$notifier_id.'" and notifier_id="'.$user_id.'"';
		$results=$this->db->query($accept_query);
		$data['user_id']=$user_id;
		$data['notifier_id']=$notifier_id;
		$data['notification_type']=4;

		$this->custom_db->insert_record('review_notifications',$data);
		$friends_query='update review_friends set status=1 where follower_id="'.$user_id.'" and user_id="'.$notifier_id.'"';

		$friends_results=$this->db->query($friends_query);
		// echo $this->db->last_query();exit;
		return $friends_results;

	}

	function ignore_follow($notifier_id,$user_id)
	{
		$ignore_query='delete from review_notifications where notifier_id="'.$user_id.'" and user_id="'.$notifier_id.'"';
		$results=$this->db->query($ignore_query);
		$ignore_friends_query='delete from review_friends where follower_id="'.$user_id.'" and user_id="'.$notifier_id.'"';
		$ignore_status=$this->db->query($ignore_friends_query);
		return $ignore_status;
	}

	function get_user_data($user_id)
	{

		$story_query='select * from user where user_id="'.$user_id.'"';
		
		// debug($story_query);
		$story_data=$this->db->query($story_query)->result_array();
		return $story_data;
	}

	function get_images($review_id)
	{
		$query='select images from reviews where review_id="'.$review_id.'"'; 
		$image_data=$this->db->query($query)->result_array();
		// debug($review_id);debug($image_data);
		return $image_data[0];
	}

	function get_likes_list($user_id,$review_id)
	{
		$query='select * from user where user_id in (select user_id from review_likes where review_id="'.$review_id.'")'; 
		$likes_list=$this->db->query($query)->result_array();

		foreach ($likes_list as $l_key => $l_val) {
			// debug($user_id);
			// debug($likes_list[$l_key]['user_id']);
		// debug($likes_list);exit;
			# code...
			// $likes_list[$l_key]['like_status']==1;
			// echo "hello";
		$friend_status=$this->get_friend_status($user_id,$likes_list[$l_key]['user_id']);

		$likes_list[$l_key]['friend_status']=$friend_status;
			if($likes_list[$l_key]['login_status']==1){
				$datetime1 = date_create(date('Y-m-d H:i:s'));
				$date_time = DateTime::createFromFormat('Y-m-d H:i:s', $likes_list[$l_key]['last_activity_ts']);
				$datetime2 = date_create($date_time);
				$interval = date_diff($datetime1,$datetime2);
				$interval=json_decode(json_encode($interval),true);
				$year=$interval['y'];
				$month=$interval['m'];
				$day=$interval['d'];
				$hour=$interval['h'];
				$mins=$interval['i'];
				if(!($year==0 && $month==0 && $day==0 && $hour==0 && $mins<3) || $likes_list[$l_key]['last_activity_ts']=='0000-00-00 00:00:00.000000')
				{

					$likes_list[$l_key]['login_status']=0;
					$this->update_login_status('',$likes_list[$l_key]['user_id'],false);
				}
			}
			// $likes_list[$l_key]['like_status']=1;
		
		}
		// debug($likes_list);exit;
		// debug($review_id);debug($image_data);
		return $likes_list;
	}

	function get_ads_data($user_id='',$start=0,$limit=20,$count=0,$hash='')
	{
		if($count==0)
		{
			
			
			$story_query='select * from advertisement' ;
			$story_data=$this->db->query($story_query)->result_array();
			foreach ($story_data as $key => $value) {
				$user_query='select first_name,last_name,email,phone,image,policies,privacy from user where user_id="'.$story_data[$key]['user_id'].'"';
				$user_data=$this->db->query($user_query)->result_array();
				$story_data[$key]['user_data']=$user_data;

				$comments_query='select * from `review_comments` where review_id="'.$story_data[$key]['review_id'].'" order by comment_date DESC ';
				// debug($comments_query);
				$comments_data=$this->db->query($comments_query)->result_array();
				$comments_count=0;
				foreach ($comments_data as $k => $v) {
					$comments_data[$k]['posted_date']=app_friendly_absolute_date($comments_data[$k]['comment_date']);
					$user_query='select first_name,last_name,email,phone,image from user where user_id="'.$comments_data[$k]['user_id'].'"';
					$user_data=$this->db->query($user_query)->result_array();
					$comments_data[$k]['user_data']=$user_data;
					$comments_count+=1;
				}

				$likes_query='select * from review_likes where review_id="'.$story_data[$key]['review_id'].'" and user_id="'.$user_id.'" ';
				$like_data=$this->db->query($likes_query)->result_array();
				if(valid_array($like_data))
				{
					$story_data[$key]['like_status']=1;
				}else{
					$story_data[$key]['like_status']=0;
				}
				// debug($like_data);exit;
				$likes_count_query='select count(id) as count from review_likes where review_id="'.$story_data[$key]['review_id'].'"';
				$like_count=$this->db->query($likes_count_query)->result_array();
				// debug($like_count);exit;
				$likes_data=$this->get_likes_list($story_data[$key]['review_id']);
				// debug($likes_data);exit;
				if(valid_array($like_count))
				{
					$story_data[$key]['likes_count']=$like_count[0]['count'];
				}else{
					$story_data[$key]['likes_count']=0;
				}
				if(valid_array($likes_data))
				{
					$story_data[$key]['likes_data']=$likes_data;
				}
				$story_data[$key]['comments_count']=$comments_count;
				$story_data[$key]['comments_data']=$comments_data;
			}
			
			}else{
				
					$story_query='select count(*) as story_count from advertisement where user_id="'.$user_id.'" ORDER by posted_date DESC LIMIT '.$start.','.$limit.'';
				
				// debug($story_query);
				$story_data=$this->db->query($story_query)->result_array();
			
			}

		
		return $story_data;
	}

	function get_hit_count($review_id)
	{
		$query='select ad_hits from reviews where review_id="'.$review_id.'" ';
		$hits_count=$this->db->query($query)->result_array();
		return $hits_count;
	}



	function get_policies()
	{
		$query='select * from privacy_policies ';
		$policy_data=$this->db->query($query)->result_array();
		return $policy_data;
	}


	function update_login_status($user_data='',$user_id='',$status)
	{
		if(isset($user_data[0]['user_id']))
		{
			$id=$user_data[0]['user_id'];
		}else if($user_id!='')
		{
			$id=$user_id;
		}
		if($status=true)
		{
			$query='update user set login_status="1" where user_id="'.$user_data[0]['user_id'].'"';
		}else if($status=false){
			$query='update user set login_status="0" where user_id="'.$user_data[0]['user_id'].'"';
		}
		return $this->db->query($query);
	}

	function update_ts($user_id)
	{
		// $query='update user set last_activity_ts=now() where user_id="'.$user_id.'"';
		// // debug($query);exit;
		//   $this->db->query($query);

		  $condition = array(
				'user_id' => $user_id,
				
		);
		//update all the logout session in login manager
		 // echo date('Y-m-d H:i:s', strtotime('+5:30')); exit('time');
		$this->custom_db->update_record('user', array('last_activity_ts' => date('Y-m-d H:i:s', strtotime('+5:30'))), $condition);
		  return true;
	}


	function check_friend_status($user_id,$follower_id)
	{
		$query='select user_id from review_friends where (user_id="'.$user_id.'" and follower_id="'.$follower_id.'") or (follower_id="'.$user_id.'" and user_id="'.$follower_id.'")';
		// debug($query);exit;
		$data=$this->db->query($query)->result_array();
		// debug($data);exit;
		if(isset($data[0]['user_id']) && !empty($data[0]['user_id']))
		{
			return 1;
		}else{
			return 0;
		}
	}

	function get_post_id_on_comment_id($comment_id)
	{
		$query='select review_id from review_comments where comment_id="'.$comment_id.'"';
		$data=$this->db->query($query)->result_array();
		return $data[0]['review_id'];
	}

	function get_friend_status($user_id,$follower_id)
	{
		if($user_id!=$follower_id)
		{
			$user_id_query='select * from review_friends where user_id="'.$user_id.'" and follower_id="'.$follower_id.'"';
			$user_id_results=$this->db->query($user_id_query)->result_array();
			// debug($user_id_results);exit;
			if(valid_array($user_id_results) && isset($user_id_results))
			{
				// found as user
				if($user_id_results[0]['status']=='0')
				{
					$friends_available['friend_status']='got_friend_request';//got_friend_request
					
				}else if($user_id_results[0]['status']=='2'){
					$friends_available['friend_status']='no_friend';
				
				}else if($user_id_results[0]['status']=='1'){
					$friends_available['friend_status']='friend';
				
				}
				

			}else{
				//not found as user
				// debug($friends_available);exit;
				$follower_id_query='select * from review_friends where follower_id="'.$user_id.'" and user_id="'.$follower_id.'" ';
				$follower_id_results=$this->db->query($follower_id_query)->result_array();
				// if($key=='6')
				// {
					
				// debug($follower_id_results);exit;
				// }
				if(valid_array($follower_id_results) && isset($follower_id_results))
				{
					// found as follower
					if($follower_id_results[0]['status']=='0')
					{
						$friends_available['friend_status']='friend_request_sent';//got_friend_request
						
					}else if($follower_id_results[0]['status']=='2'){
						$friends_available['friend_status']='no_friend';
						
					}else if($follower_id_results[0]['status']=='1'){
						$friends_available['friend_status']='friend';
						
					}
				}else{
						$friends_available['friend_status']='no_friend';
						
				}
			}
		}else
		{
			$friends_available['friend_status']="same_user";
		}
			return $friends_available['friend_status'];
		
	}

	function format_img_data($img_data,$thumbnail='')
	{
		// debug();
		if(isset($thumbnail) && !empty($thumbnail))
		{
			$thumb_data=json_decode($thumbnail,true);
		}
		$img_data=json_decode($img_data,true);

		foreach ($img_data as $key => $value) {
			# code...
			$file_data[$key]['file_url']=$value;
			$file_name=explode('.',$value );
			// debug($file_name);
			if($file_name[1]=='MP4' || $file_name[1]=='AVI' || $file_name[1]=='FLV' || $file_name[1]=='WMV' || $file_name[1]=='MOV' || $file_name[1]=='mp4' || $file_name[1]=='avi' || $file_name[1]=='flv' || $file_name[1]=='wmv' || $file_name[1]=='mov' )
			{

				$file_data[$key]['media_type']='video';
				$fname=$file_name[0];
				// debug($value);
				// debug($thumb_data[$value]);
				if(isset($thumb_data[$value][0]))
				{

					$file_data[$key]['thumbnail']=$thumb_data[$value][0];
				}else{
					// $file_data[$key]['thumbnail']=$GLOBALS['CI']->template->domain_images('thumbnail_default.jpg');
					$file_data[$key]['thumbnail']='thumbnail_default.jpg';
				}
			}else{
				$file_data[$key]['media_type']='image';
				// debug($value);exit;
				$file_name[0]=$file_name[0].'_mobile';
				$file_name=implode('.', $file_name);
				// debug($file_name);exit;
				$file_data[$key]['thumbnail']=$file_name;	
			}
		}
		// debug($file_data);exit;
		return $file_data;
	}

	function get_user_uploads($user_id)
	{
		$query='select images,thumbnail from reviews where user_id="'.$user_id.'" ORDER BY review_id DESC';
		$review_data=$this->db->query($query)->result_array();
		// debug($review_data);exit;
		foreach ($review_data as $rkey => $rvalue) {
			// debug($review_data[$rkey]['images']);
			$img_data=$this->format_img_data($review_data[$rkey]['images'],$review_data[$rkey]['thumbnail']);
			// debug($img_data);exit;
			$image_data[]['images']=json_encode($img_data);
		// debug($review_data);exit;
		}
		// debug($image_data);exit;
		return $image_data;
	}

	function user_data($user_id){
		$user_query='select * from user where user_id="'.$user_id.'"';
		$user_data=$this->db->query($user_query)->result_array();
		return $user_data[0];
	}

	function get_tagged_bookings($user_id)
	{
		$tagged_query='select * from review_book_tags where user_id="'.$user_id.'"';
		$tagged_data=$this->db->query($tagged_query)->result_array();
		return $tagged_data;
	}


	
       /*     if ($_POST) {

            if ($_FILES["file"]["error"] > 0) {
            $error = $_FILES["file"]["error"];
        }
            else if (($_FILES["file"]["type"] == "image/gif") ||
            ($_FILES["file"]["type"] == "image/jpeg") ||
            ($_FILES["file"]["type"] == "image/png") ||
            ($_FILES["file"]["type"] == "image/pjpeg")) {

            $url = 'C:/Users/admin/Downloads/compressed.jpg';
            $filename = compress_image($_FILES["file"]["tmp_name"], $url, 80);
        }else {
            $error = "Uploaded image should be jpg or gif or png";
        }
        }*/


	 function compress_image($source_url, $destination_url, $quality) {

      $info = getimagesize($source_url);

          if ($info['mime'] == 'image/jpeg')
          $image = imagecreatefromjpeg($source_url);

          elseif ($info['mime'] == 'image/gif')
          $image = imagecreatefromgif($source_url);

          elseif ($info['mime'] == 'image/png')
          $image = imagecreatefrompng($source_url);

          imagejpeg($image, $destination_url, $quality);
          return $destination_url;
        }
        function bus_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) as total_records 
					from bus_booking_details BD
					join bus_booking_itinerary_details AS BBID on BD.app_reference=BBID.app_reference
					join payment_option_list AS POL on BD.payment_mode=POL.payment_category_code 
					where BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$cancellation_details = array();
			$bd_query = 'select *,BBCD.fare from bus_booking_details AS BD JOIN bus_booking_itinerary_details as BBI join bus_booking_customer_details as BBCD on BD.app_reference = BBCD.app_reference 
						WHERE BBI.app_reference = BD.app_reference and BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition.'
						order by BD.origin desc limit '.$offset.', '.$limit;
			
			$booking_details = $this->db->query($bd_query)->result_array();
	
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				$id_query = 'select * from bus_booking_itinerary_details AS ID 
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				$cd_query = 'select * from bus_booking_customer_details AS CD 
							WHERE CD.app_reference IN ('.$app_reference_ids.')';
				$cancellation_details_query = 'select * from bus_cancellation_details AS BCD 
							WHERE BCD.app_reference IN ('.$app_reference_ids.')';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$cancellation_details	= $this->db->query($cancellation_details_query)->result_array();
			}
			$response['data']['booking_details']			= $booking_details;
			//$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			//$response['data']['booking_customer_details']	= $booking_customer_details;
			//$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}


	function get_wallet_log($user_id)
	{
		if(isset($user_id))
		{
			$query='select * from wallet_transaction_log where user_id="'.$user_id.'" order by id DESC';
			$wallet_log=$this->db->query($query)->result_array();
			if(count($wallet_log)>0)
			{
				return $wallet_log;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function save_wallet_log($user_id,$book_id,$amount,$credit_status,$opening_balance)
	{
		$data['app_reference']=$book_id;
		$data['amount']=$amount;
		$data['user_id']=intval(@$user_id);
		$data['opening_balance']=$opening_balance;
		if($credit_status==true){
			$data['closing_balance']=$opening_balance;
			$data['credit']=$amount;
			$data['debit']=0;
			$data['status']=0;
		}else{
			$data['closing_balance']=$opening_balance-$amount;
			$data['debit']=$amount;
			$data['credit']=0;
			$data['status']=1;
		}
		$this->custom_db->insert_record('wallet_transaction_log', $data);
		return true;
	}


	function active_b2b_user($username, $password)
	{
		//$condition[] = array('U.status', '=', ACTIVE);
		$condition[] = array('U.domain_list_fk', '=', get_domain_auth_id());
		$condition[] = array('U.user_type', '=', B2B_USER);
		//$condition[] = array('U.user_name', '=', $this->db->escape(provab_encrypt(trim($username))));
		$condition[] = array('U.user_name', '=', $this->db->escape((trim($username))));
		//$condition[] = array('U.phone', '=', $this->db->escape($username));
		$condition[] = array('U.password', '=', $this->db->escape(provab_encrypt(md5(trim($password)))));

		$filter_condition = ' and ';

		if (valid_array($condition) == true) {
			foreach ($condition as $k => $v) {
				if ($k < count($condition) - 1) {
					$filter_condition .= implode($v).' and ';
				} else {
					$filter_condition .= implode($v);
			}
				}
		}
		//$query = 'SELECT U.*, UT.user_type as user_profile_name, ACL.country_code as country_code_value, B2B.balance as balance, B2B.credit_limit as credit_limit, B2B.due_amount as due_amount
		//	FROM user AS U, user_type AS UT, api_country_list AS ACL, b2b_user_details AS B2B
		$query = 'SELECT U.*, UT.user_type as user_profile_name, ACL.country_code as country_code_value
			FROM user AS U, user_type AS UT, api_country_list AS ACL
		 	WHERE U.user_type=UT.origin
		 	AND ACL.origin=U.country_code' . $filter_condition;
		return $this->db->query($query)->result_array();

	}
	public function get_price_from_payment_details($book_id='')
	{
		 $result = $this->db->select('amount,currency')
            ->from('payment_gateway_details')
            //->where('user_oid', $this->entity_user_id)
            ->where('app_reference', $book_id)
            //->where('user_oid', 1355)
            ->get();
        //echo $this->db->last_query();exit;
        //echo $result->status;exit;
        return $result->row_array();
	}
	public function loyality_points_history($user_id='')
	{
		if(isset($user_id))
		{
			$query='select *, IFNULL(credit,0) as credit, IFNULL(debit,0) as debit from loyality_points_history where user_id="'.$user_id.'" order by id DESC';
			$points_log=$this->db->query($query)->result_array();

			$query='select SUM(credit) as credited, SUM(debit) as debited from loyality_points_history where user_id="'.$user_id.'" GROUP BY user_id order by id DESC';
			$credit_debit=$this->db->query($query)->row_array();

			$query1='select loyality_points from b2b_user_details where user_oid="'.$user_id.'"';
			$available_points=$this->db->query($query1)->row_array();
			if(empty($available_points)){
				$total_available_points = 0; 
			}else{
				$total_available_points = $available_points['loyality_points']; 
			}
			//echo $this->db->last_query();exit();
			$data['points_log'] = $points_log;
			$data['total_available_points'] = $total_available_points;
			$data['total_reedemed'] = $credit_debit['debited'];
			$data['total_earned'] = $credit_debit['credited'];
			/*if(count($points_log)>0)
			{
				return $points_log;
			}else{
				return false;
			}*/
			return $data;
		}else{
			return false;
		}
	}

	public function notification_list($user_id='')
	{
		if(isset($user_id))
		{
			$query='select * from notification_list where user_id="'.$user_id.'" order by id DESC';
			$list=$this->db->query($query)->result_array();
			if(count($list)>0)
			{
				return $list;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
}// main class end*************
