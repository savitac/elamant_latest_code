<?php
class Package_Model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
	public function getAllPackages(){
		$this->db->select('*');
		$this->db->where('status',ACTIVE);
		$query = $this->db->get('package');
		if ( $query->num_rows > 0 ) {
	 		return $query->result();
		}else{
			return array();
		}		
	}


	/**
	 * return Top Destination Packages mobile
	 */
	function get_package_top_destination_mobile()
	{
		$query = 'Select package_id,package_code,package_name,image from package where top_destination = '.ACTIVE;
		$data = $this->db->query($query)->result_array();
		return $data;
	}
	public function getAllPackages_countrywise($country) {
		$this->db->select ( '*' );			
		$this->db->join('country', 'country.country_id = package.package_country', 'right');
		$this->db->where ( 'package.status', ACTIVE );
		if(!empty($country)){
			$this->db->where ( 'country.name', $country);
		}else{
			$this->db->where ( 'country.name !=', 'India');
		}
		
		$query = $this->db->get ('package');
		


		
		if ($query->num_rows > 0) {
			return $query->result ();
		} else {
			return array ();
		}
	}


	/**
	 *@param Top Destination Packages
	 */
	public function get_package_top_destination(){
		$this->db->select('*');
		$this->db->where('top_destination',ACTIVE);
		$query = $this->db->get('package');
		if ( $query->num_rows > 0 ) {
			$data['data'] = $query->result();
			$data['total'] = $query->num_rows;
			return $data;
		}else{
			return array('data' => '', 'total' => 0);
		}
	}

	public function getPageCaption($page_name) {
		$this->db->where('page_name', $page_name);
		return $this->db->get('page_captions');
	}
	public function get_contact(){
		$contact = $this->db->get('contact_details');
		return $contact->row();
	}
	/**
	*get country name
	**/
	public function getCountryName($id){
		$this->db->select("*");
		$this->db->from("country");
		$this->db->where('country_id',$id);
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->row();
		}else{
			return array();
		}
	}
  
   /**
    * get package itinerary
    */
   public function getPackageItinerary($package_id){
   		$this->db->select("*");
		$this->db->from("package_itinerary");
		$this->db->where('package_id',$package_id);
		$this->db->order_by('day','ASC');
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->result_array();
		}else{
			return array();
		}
   }
  
   /**
    * get package pricing policy
    */
   public function getPackagePricePolicy($package_id){
  		$this->db->select("*");
		$this->db->from("package_pricing_policy");
		$this->db->where('package_id',$package_id);
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->row();
		}else{
			return array();
		}
   }
	
   /**
    * get package traveller photos
    */
   public function getTravellerPhotos($package_id){
   		$this->db->select("*");
		$this->db->from("package_traveller_photos");
		$this->db->where('package_id',$package_id);
		$this->db->where('status','1');
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->result();
		}else{
			return array();
		}
   }
   /*8
    * get getPackageCancelPolicy
    */
   public function getPackageCancelPolicy($package_id){
   		$this->db->select("*");
		$this->db->from("package_cancellation");
		$this->db->where('package_id',$package_id);
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->row();
		}else{
			return array();
		}
   }
   /**
    * getPackage
    */
   public function getPackage($package_id){
   		$this->db->select("*");
		$this->db->from("package");
		$this->db->where('package_id',$package_id);
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->row();
		}else{
			return array();
		}
   }
  
    public function saveEnquiry($data){    	
    	$this->db->insert('package_enquiry',$data);
    	return $this->db->insert_id();
    }

    public function getPackageCountries(){
    	$data = 'select package_country, C.name AS country_name FROM package P, country C WHERE P.package_country=C.country_id';
    	return $this->db->query($data)->result();
    	/*$this->db->select('package_country');
    	 $this->db->from('package'); 
    	 $this->db->group_by('package_country'); 
		$query = $this->db->get();
		if($query->num_rows()){
			return $query->result();
		}else{
			return array();
		}*/
    }
    public function getPackageTypes(){
    	$this->db->select("*");
		$this->db->from("package_types");
		$query=$this->db->get();
		if($query->num_rows()){
			return $query->result();
		}else{
			return array();
		}
    }
  /*  public function search($c,$p,$d,$b,$dmn_list_fk){
    	$this->db->select("*");
		$this->db->from("package");
		$this->db->like('package_country', $c,'both'); 
		$this->db->like('package_type', $p,'both'); 
		if($d){
			$this->db->where($d);
		}else{
			$this->db->like('duration', $d,'both');
		}
		if($b){
			$this->db->where($b);
		}else{
			$this->db->like('price', $b,'both'); 
		}
		$this->db->where('status',ACTIVE);
		$this->db->where('domain_list_fk',$dmn_list_fk);
		$query=$this->db->get();
		//echo $this->db->last_query();
		//exit;
		if($query->num_rows()){
			return $query->result();
		}else{
			return array();
		}
    }*/

     public function search($c,$p,$d,$b,$dmn_list_fk,$expirydate,$holiday_type){
//debug($holiday_type);exit;
    	
    	$this->db->select("*");
		$this->db->from("package");
		$this->db->like('holiday_type', $holiday_type,'both'); 
		$this->db->where('status',ACTIVE);
		$this->db->where('domain_list_fk',$dmn_list_fk);
		//$this->db->where('expirydate >=', $expirydate);

		$query=$this->db->get();
		echo $this->db->last_query();exit;
		if($query->num_rows()){
			return $query->result();
		}else{
			return array();
		}
    }
    
    
     function add_user_rating($arr_data)
     {   
        $pkg_id=$arr_data['package_id']; 
          $res=$this->db->insert('package_rating',$arr_data);        
         if($res==true){             
           $this->db->select('rating');
           $this->db->where('package_id',$pkg_id);
           $res1=$this->db->get('package_rating');
           	$sum='';
             if($res1->num_rows() > 0)
             {  // print_r($res1);
                  $tot_no=count($res1->result());
                  $results=$res1->result();
               //   sum=0;
                foreach($results as $r)
                 {
                      $sum+=$r->rating;
                 }
                $rating=$sum/$tot_no;
                $da=array('rating'=> ceil($rating));
                $this->db->where('package_id',$pkg_id);
                $this->db->update('package',$da);
             }
         }
    }

    function getPackageCountry($term)
    {
    	$query = "select package_country, C.name AS country_name FROM package P, country C WHERE P.package_country=C.country_id and C.name like '%".$term."%' GROUP BY C.name";    //echo $query; die;
    	return $this->db->query($query)->result_array();
    }
    
     public function getPackageCity($term){
     		$data = "select DISTINCT package_city FROM package where package_city like '%".$term."%'";

    	return $this->db->query($data)->result_array();
    	
    }

    function getPackageType($id)
    {
   //  	$query = "select package_types_name as PackageName, package_types_id as PackageTypeId ,duration as Duration, package_country as Country_id FROM package P JOIN package_types C on P.package_type = C.package_types_id WHERE package_country = ".$id." GROUP BY C.package_types_name";
   //  	//debug($query); die;
 		// return $this->db->query($query)->result_array();


 		$query = "select package_types_name , package_types_id ,duration , package_country  FROM package P JOIN package_types C on P.package_type = C.package_types_id WHERE package_country = ".$id." GROUP BY C.package_types_name";
    	//debug($query); die;
 		return $this->db->query($query)->result_array();
    }

    function getPackageDurations($id)
    {
    	$query = "select MIN(duration) as min_days, MAX(duration) as max_days FROM package P, package_types C WHERE P.package_type= ".$id;
 		return $this->db->query($query)->result_array();
    }

      function mobile_search($c,$p,$d,$b,$dmn_list_fk){

    	if($b == "LH"){
			$oder_by  = $this->db->order_by('price','ASC');
		}elseif($b == "HL"){
			 $this->db->order_by('price','DESC');
		}elseif($b == "NAME"){
			$this->db->order_by('TRIM(package_name)','ASC');
		}elseif($b =="STAR"){
			 $this->db->order_by('rating DESC, TRIM(package_name) ASC');
		}

    	$this->db->select("*");
		$this->db->from("package");
		$this->db->like('package_country', $c,'both'); 
		$this->db->like('package_type', $p,'both'); 
		if($d){
			$this->db->where($d);
		}else{
			$this->db->like('duration', $d,'both');
		}
		$this->db->where('status',ACTIVE);
		$this->db->where('domain_list_fk',$dmn_list_fk);
		//$order_by;
		$query=$this->db->get();
		$result = $query->result();

		//echo $this->db->last_query();exit();
		//exit;
		if($query->num_rows()){
			return $query->result_array();
		}else{
			return array();
		}
    }

    function getPackageTypeName($id)
    {
    	$query = "select package_types_name as PackageName FROM package P, package_types C WHERE P.package_type=C.package_types_id and C.package_types_id = ".$id." GROUP BY C.package_types_name";
 		$result = $this->db->query($query)->result_array();
 		return $result['0']['PackageName'];
    }

     function save_package_booking_transaction_details(
	$app_reference, $transaction_status, $status_description, $pnr, $book_id, $source, $ref_id, $attributes,
	 $currency, $total_fare)
	{
		$data['app_reference'] = $app_reference;
		$data['status'] = $transaction_status;
		$data['status_description'] = $status_description;
		$data['pnr'] = $pnr;
		$data['book_id'] = $book_id;
		$data['source'] = $source;
		$data['ref_id'] = $ref_id;
		$data['attributes'] = $attributes;
		

		$data['total_fare'] = $total_fare;
		
		$data['currency'] = $currency;

		
		
		return $this->custom_db->insert_record('package_booking_transaction_details', $data);
	}
	function save_package_booking_passenger_details(
	$app_reference, $passenger_type, $is_lead, $first_name,$last_name,
	$gender, $passenger_nationality, $status,
	$attributes, $flight_booking_transaction_details_fk)
	{
		$data['app_reference'] = $app_reference;
		$data['passenger_type'] = $passenger_type;
		$data['is_lead'] = $is_lead;
		
		$data['first_name'] = $first_name;
		
		$data['last_name'] = $last_name;
		
		$data['gender'] = $gender;
		$data['passenger_nationality'] = $passenger_nationality;
		
		$data['status'] = $status;
		$data['attributes'] = $attributes;
		$data['flight_booking_transaction_details_fk'] = $flight_booking_transaction_details_fk;
		return $this->custom_db->insert_record('package_booking_passenger_details', $data);
	}

	function save_package_booking_details(
	$domain_origin, $status, $app_reference, $booking_source, $phone, $alternate_number, $email,$payment_mode,	$attributes, $created_by_id, 
	$transaction_currency, $currency_conversion_rate,$pack_id,$date_of_travel)
	{
		$data['domain_origin'] = $domain_origin;
		$data['status'] = $status;
		$data['app_reference'] = $app_reference;
		$data['booking_source'] = $booking_source;
		$data['phone'] = $phone;
		$data['package_type'] = $pack_id;
		
		$data['email'] = $email;
		
		$data['payment_mode'] = $payment_mode;
		$data['attributes'] = $attributes;
		$data['created_by_id'] = $created_by_id;
		$data['created_datetime'] = date('Y-m-d H:i:s');
		$data['date_of_travel'] = $date_of_travel;
		
		
		$data['currency'] = $transaction_currency;
		$data['currency_conversion_rate'] = $currency_conversion_rate;
		$this->custom_db->insert_record('package_booking_details', $data);
	}
	public function process_booking($book_id, $booking_params)
	{

		//debug($book_id); exit;

		$response['status'] ='BOOKING_CONFIRMED';
		$condition = array();
		$condition['app_reference']=$book_id;
		$this->custom_db->update_record('package_booking_details',$response,$condition);
		$this->custom_db->update_record('package_booking_transaction_details', $response, $condition);
		$this->custom_db->update_record('package_booking_passenger_details', $response, $condition);

		$response['data']['book_id'] = $book_id;
		$response['data']['booking_params'] = $booking_params;
		$response['status'] = $response['status'];
		//debug($response); exit;
		return $response;
	}

	function get_booking_details($app_reference, $booking_source='', $booking_status='')
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = array();
		//Booking Details
		$bd_query = 'select * from package_booking_details AS BD
		   WHERE BD.app_reference like '.$this->db->escape($app_reference);
		if (empty($booking_source) == false) {
			$bd_query .= '	AND BD.booking_source = '.$this->db->escape($booking_source);
		}
		if (empty($booking_status) == false) {
			$bd_query .= ' AND BD.status = '.$this->db->escape($booking_status);
		}
		//Itinerary Details
		
		$td_query = 'select * from package_booking_transaction_details
    AS CD WHERE CD.app_reference='.$this->db->escape($app_reference).'  order by origin asc';
		//Customer and Ticket Details
		$cd_query = 'select CD.*
						from package_booking_passenger_details CD

						
						WHERE CD.flight_booking_transaction_details_fk IN 
						(select TD.origin from package_booking_transaction_details AS TD 
						WHERE TD.app_reference ='.$this->db->escape($app_reference).') order by origin asc';
		//Cancellation Details
		

		$response['data']['booking_details']			= $this->db->query($bd_query)->result_array();
		
		$response['data']['booking_transaction_details']	= $this->db->query($td_query)->result_array();
		$response['data']['booking_customer_details']	= $this->db->query($cd_query)->result_array();
		
		if (valid_array($response['data']['booking_details']) == true and valid_array($response['data']['booking_customer_details']) == true) {
			$response['status'] = SUCCESS_STATUS;
		}
		return $response;
	}

}