<?php
/**
 * Library which has generic functions to get data
 *
 * @package    Provab Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class User_Model extends CI_Model
{

	function create_user($email, $password, $first_name='Customer', $phone='', $creation_source='portal')
	{
		$data['email'] = $email;
		$data['user_name'] = $email;

		if (empty($password) == false and strlen($password) > 3) {
			$data['password'] = md5($password);
		}

		$data['user_type'] = B2C_USER;
		$data['first_name'] = $first_name;
		$data['phone'] = $phone;
		$data['domain_list_fk'] = get_domain_auth_id();
		$data['uuid'] = time().rand(1, 1000);
		$data['creation_source'] = $creation_source;
		if ($creation_source == 'portal') {
			$data['status'] = ACTIVE;
		} else {
			$data['status'] = ACTIVE;
		}
		$data['created_datetime'] = date('Y-m-d H:i:s');
		$data['created_by_id'] = intval(@$GLOBALS['CI']->entity_user_id);
		$data['language_preference'] = 'english';
		$insert_id = $this->custom_db->insert_record('user', $data);
		$insert_id = $insert_id['insert_id'];
		$user_data = $this->custom_db->single_table_records('user', '*', array('user_id' => $insert_id));
		$remarks = $email.' Has Registered From B2C Portal';
		$notification_users = $this->get_admin_user_id();
		$action_query_string = array();
		$action_query_string['user_id'] = $insert_id;
		$action_query_string['uuid'] = $data['uuid'];
		$action_query_string['user_type'] = B2C_USER;
		$this->application_logger->registration($email, $remarks, $insert_id, $action_query_string, array(), $notification_users);
		return $user_data;
	}

	//sms configuration
	function sms_configuration($sms)
	{
		$tmp_data = $this->db->select('*')->get_where('sms_configuration', array('domain_origin' => $sms));
		//echo $this->db->last_query();exit;
		return $tmp_data->row();
	}
	//social network configuration
	function fb_network_configuration($id,$social)
	{
		//$tmp_data = $this->db->select('config')->get_where('social_login', array('domain_origin' => $id,'social_login_name' => $social));
		//echo $this->db->last_query();exit;
		$social_links = $this->db_cache_api->get_active_social_network_list();
		return isset($social_links[$social]) ? $social_links[$social]['config'] : false;
	}
	
	function google_network_configuration($id,$social)
	{
		$social_links = $this->db_cache_api->get_active_social_network_list();
		return isset($social_links[$social]) ? $social_links[$social]['config'] : false;
	}

	//Global SMS Checkpoint
	function sms_checkpoint($name)
	{
		$result = $this->db->select('status')->get_where('sms_checkpoint', array('condition' => $name))->row();
		//echo $this->db->last_query();exit;
		//echo $result->status;exit;
		return $result->status;
	}
	/***
	 * Registered new user Activation Point
	 */
	function activate_account_status($status, $user_id)
	{
		$data = array(
				'status' => $status
		);
		$this->db->where('user_id', $user_id);
		$this->db->update('user', $data);
	}

	/**
	 * Return current user details who has logged in
	 */
	function get_current_user_details()
	{
		if (intval(@$this->entity_user_id) > 0) {
			$cond = array(array('U.user_id', '=', intval($this->entity_user_id)));
				
			$user = $this->get_user_details($cond);
			return $user;
		} else {
			return false;
		}
	}

	/**
	 * Get Active User Details - B2C Only
	 * @param string $username
	 * @param string $password
	 */
	function active_b2c_user($username, $password)
	{
		$condition[] = array('U.status', '=', ACTIVE);
		$condition[] = array('U.domain_list_fk', '=',1);// get_domain_auth_id()
		$condition[] = array('U.user_type', '=', B2C_USER);
		$condition[] = array('U.email', '=', $this->db->escape($username));
		$condition[] = array('U.password', '=', $this->db->escape(md5($password)));
		return $this->get_user_details($condition);

	}

	/**
	 *verify is the user credentials are valid
	 *
	 *@param string $email    email of the user
	 *@param string @password password of the user
	 *
	 *return boolean status of the user credentials
	 */
	public function get_user_details($condition=array(), $count=false, $offset=0, $limit=10000000000, $order_by=array())
	{

		$filter_condition = ' and ';
		if (valid_array($condition) == true) {
			foreach ($condition as $k => $v) {
				$filter_condition .= implode($v).' and ';
			}
		}

		if (valid_array($order_by) == true) {
			$filter_order_by = 'ORDER BY';
			foreach ($order_by as $k => $v) {
				$filter_order_by .= implode($v).',';
			}
		} else {
			$filter_order_by = '';
		}
		$filter_condition = rtrim($filter_condition, 'and ');
		$filter_order_by = rtrim($filter_order_by, ',');
		if (!$count) {
			return $this->db->query('SELECT U.*, UT.user_type as user_profile_name, ACL.country_code as country_code_value
			FROM user AS U, user_type AS UT, api_country_list AS ACL
		 	WHERE U.user_type=UT.origin 
		 	AND U.country_code=ACL.origin'.$filter_condition.' limit '.$limit.' offset '.$offset.' '.$filter_order_by)->result_array();
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U, user_type AS UT, api_country_list AS ACL
		 WHERE U.user_type=UT.origin 
		 AND U.country_code=ACL.origin'.$filter_condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}
	/**
	 * get Domain user list in the system
	 */
	function get_domain_user_list($condition=array(), $count=false, $offset=0, $limit=10000000000, $order_by=array())
	{
		$filter_condition = ' and ';
		if (valid_array($condition) == true) {
			foreach ($condition as $k => $v) {
				$filter_condition .= implode($v).' and ';
			}
		}
		if(is_domain_user() == false) {
			//PROVAB ADMIN
			//GET ALL DOMAIN ADMINS DETAILS
			$filter_condition .= ' U.domain_list_fk > 0 and U.user_type = '.ADMIN.' and U.user_id != '.intval($this->entity_user_id).' and ';
		} else if(is_domain_user() == true) {
			//DOMAIN ADMIN
			//GET ALL DOMAIN USERS DETAILS
			$filter_condition .= ' U.domain_list_fk ='.get_domain_auth_id().' and U.user_type != '.ADMIN.' and U.user_id != '.intval($this->entity_user_id).' and ';
		}
		if (valid_array($order_by) == true) {
			$filter_order_by = 'ORDER BY';
			foreach ($order_by as $k => $v) {
				$filter_order_by .= implode($v).',';
			}
		} else {
			$filter_order_by = '';
		}
		$filter_condition = rtrim($filter_condition, 'and ');
		$filter_order_by = rtrim($filter_order_by, ',');
		if (!$count) {
			return $this->db->query('SELECT U.*, UT.user_type, ACL.country_code as country_code_value FROM user AS U, user_type AS UT, api_country_list AS ACL
		 WHERE U.user_type=UT.origin 
		 AND U.country_code=ACL.origin'.$filter_condition.' limit '.$limit.' offset '.$offset.' '.$filter_order_by)->result_array();
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U, user_type AS UT, api_country_list AS ACL
		 WHERE U.user_type=UT.origin 
		 AND U.country_code=ACL.origin'.$filter_condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}

	/**
	 * get Logged in Users
	 Jaganath (25-05-2015) - 25-05-2015
	 */
	function get_logged_in_users($condition=array(), $count=false, $offset=0, $limit=10000000000)
	{
		$filter_condition = ' and ';
		if (valid_array($condition) == true) {
			foreach ($condition as $k => $v) {
				$filter_condition .= implode($v).' and ';
			}
		}
		if(is_domain_user() == false) {
			//PROVAB ADMIN
			//GET ALL DOMAIN ADMINS DETAILS
			$filter_condition .= ' U.domain_list_fk > 0 and U.user_type = '.ADMIN.' and U.user_id != '.intval($this->entity_user_id).' and ';
		} else if(is_domain_user() == true) {
			//DOMAIN ADMIN
			//GET ALL DOMAIN USERS DETAILS
			$filter_condition .= ' U.domain_list_fk ='.get_domain_auth_id().' and U.user_type != '.ADMIN.' and U.user_id != '.intval($this->entity_user_id).' and ';
		}
		$filter_condition = rtrim($filter_condition, 'and ');
		$current_date = date('Y-m-d', time());
		if (!$count) {
			return $this->db->query('SELECT U.*, UT.user_type, LM.login_date_time as login_time,LM.logout_date_time as logout_time,LM.login_ip
			FROM user AS U
			JOIN user_type AS UT ON U.user_type=UT.origin
			JOIN api_country_list AS ACL ON U.country_code=ACL.origin
			JOIN login_manager AS LM ON U.user_type=LM.user_type and U.user_id=LM.user_id
		    WHERE LM.login_date_time >="'.$current_date.' 00:00:00"
			and (LM.logout_date_time = "0000-00-00 00:00:00" or LM.logout_date_time >= "'.$current_date.' 00:00:00")
			 '.$filter_condition.' order by LM.logout_date_time asc limit '.$limit.' offset '.$offset)->result_array();
		} else {
			return $this->db->query('SELECT count(*) as total FROM user AS U
			JOIN user_type AS UT ON U.user_type=UT.origin
			JOIN api_country_list AS ACL ON U.country_code=ACL.origin
			JOIN login_manager AS LM ON U.user_type=LM.user_type and U.user_id=LM.user_id
		    WHERE LM.login_date_time >="'.$current_date.' 00:00:00"
			and (LM.logout_date_time = "0000-00-00 00:00:00" or LM.logout_date_time >= "'.$current_date.' 00:00:00")'.$filter_condition.' limit '.$limit.' offset '.$offset)->row();
		}
	}

	/**
	 * get Domain List present in the system
	 */
	function get_domain_details()
	{
		$query = 'select DL.*,CONCAT(U.first_name, " ", U.last_name) as created_user_name from domain_list DL join user U on DL.created_by_id=U.user_id';
		return $this->db->query($query)->result_array();
	}

	/**
	 *update logout time
	 *
	 *@param number $LID unique login id which has to be updated
	 *
	 *@return status;
	 */
	function update_login_manager($user_id, $login_id)
	{
		$condition = array(
				'user_id' => $user_id,
				'origin' => $login_id
		);
		//update all the logout session in login manager
		$this->custom_db->update_record('login_manager', array('logout_date_time' => date('Y-m-d H:i:s', time())), $condition);
		$this->application_logger->logout($this->entity_name, $this->entity_user_id, array('user_id' => $this->entity_user_id, 'uuid' => $this->entity_uuid));
	}


	/**
	 * Create Login Manager
	 */
	function create_login_auth_record($user_id, $user_type, $user_origin=0, $username='customer')
	{
		$login_details['browser'] = $_SERVER['HTTP_USER_AGENT'];
		$remote_ip = $_SERVER['REMOTE_ADDR'];
		$this->update_auth_record_expiry($user_id, $user_type, $remote_ip, $user_origin, $username);
		//logout of same user from same ip
		$login_details['info'] = file_get_contents("http://ipinfo.io/".$remote_ip."/json");
		$data['user_id'] = $user_id;
		$data['user_type'] = $user_type;
		$data['login_date_time'] = date('Y-m-d H:i:s');
		$data['login_ip'] = $remote_ip;
		$data['attributes'] = json_encode($login_details);

		$login_id = $this->custom_db->insert_record('login_manager', $data);
		$this->application_logger->login($username, $user_origin, array('user_id' => $user_origin, 'uuid' => $user_id));
		return $login_id['insert_id'];
	}

	/**
	 * Update logout
	 * @param $user_id
	 * @param $user_type
	 * @param $remote_ip
	 * @param $browser
	 */
	function update_auth_record_expiry($user_id, $user_type, $remote_ip, $user_origin, $username)
	{
		$cond['user_id'] = $user_id;
		$cond['user_type'] = $user_type;
		$cond['login_ip'] = $remote_ip;
		$auth_exp = $this->custom_db->update_record('login_manager', array('logout_date_time' => date('Y-m-d H:i:s')), $cond);
		if ($auth_exp == true) {
			//update application logger
			$this->application_logger->logout($username, $user_origin, array('user_id' => $user_origin, 'uuid' => $user_id));
		}
	}

	public function email_subscribtion($email,$domain_key)
	{
		$query = $this->db->get_where('email_subscribtion', array('email_id' => $email));
		if($query->num_rows() > 0){
			return "already";
		}else{
			$insert_id = $this->custom_db->insert_record('email_subscribtion',array('email_id' => $email,'domain_list_fk' => $domain_key));
			return $insert_id['insert_id'];
		}
	}
	/**
	 * Jaganath
	 */
	public function user_traveller_details($search_chars)
	{
		$raw_search_chars = $this->db->escape($search_chars);
		$r_search_chars = $this->db->escape($search_chars.'%');
		$search_chars = $this->db->escape('%'.$search_chars.'%');
		$query = 'select * from user_traveller_details where created_by_id='.intval($this->entity_user_id).' and (first_name like '.$search_chars.'
		OR 	last_name like '.$search_chars.')
		ORDER BY first_name ASC	LIMIT 0, 20';
		return $this->db->query($query);
	}
	/**
	 * Jaganath
	 */
	function get_user_traveller_details()
	{
		$query = 'select * from user_traveller_details 
		where created_by_id='.intval(@$this->entity_user_id).' ORDER BY first_name ASC';
		return $this->db->query($query)->result_array();
	}
	/**
	sudheep offline payment
	**/
	function offline_payment_insert($params){
		//$query = "INSERT INTO `offline_payment`(`id`, `company_name`, `name`, `email`, `phone`, `amount`, `remarks`, `created_date`, `refernce_code`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9])";

		$created_date = date('Y-m-d H:i:s');
		$coded = str_shuffle($params['data'][0]['value'].$params['data'][3]['value']);

		$insert_id = $this->custom_db->insert_record('offline_payment', array($params['data'][0]['name']=> $params['data'][0]['value'], $params['data'][1]['name']=> $params['data'][1]['value'], $params['data'][2]['name']=> $params['data'][2]['value'], $params['data'][3]['name']=> $params['data'][3]['value'], $params['data'][4]['name']=> $params['data'][4]['value'], $params['data'][5]['name']=> $params['data'][5]['value'],'created_date' =>$created_date,'refernce_code'=>$coded	));

			return array('db'=>$insert_id, 'refernce_code'=>$coded);

	}
	function offline_approval($cd){
		$query = "SELECT * FROM `offline_payment` WHERE `refernce_code` = '$cd' ";

	$ret = $this->db->query($query)->result_array();
	return $ret;
	}
	/**
	 * Jaganath
	 */
	function get_admin_user_id()
	{
		$admin_user_id = array();
		$cond[] = array('U.user_type', '=', ADMIN);
		$cond[] = array('U.status', '=', ACTIVE);
		$cond[] = array('U.domain_list_fk', '=', get_domain_auth_id());
		$user_details = $this->get_user_details($cond);
		foreach($user_details as $k => $v){
			$admin_user_id[$k] = $v['user_id'];
		}
		return $admin_user_id;
	}

	function flight_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) AS total_records from flight_booking_details BD
					where domain_origin='.get_domain_auth_id().' AND BD.created_by_id ='.$user_id.' '.$condition;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$booking_transaction_details = array();
			$cancellation_details = array();
			//Booking Details
			$bd_query = 'select *,total_fare,BTD.origin as trans_id from flight_booking_details AS BD JOIN  flight_booking_transaction_details AS BTD
						WHERE BD.app_reference = BTD.app_reference and BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition.'
						order by BD.created_datetime desc, BD.origin desc limit '.$offset.', '.$limit;
			// echo $bd_query;exit;
			$booking_details	= $this->db->query($bd_query)->result_array();
			
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				//Itinerary Details
				$id_query = 'select * from flight_booking_itinerary_details AS ID
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				//Transaction Details
				$td_query = 'select * from flight_booking_transaction_details AS TD
							WHERE TD.app_reference IN ('.$app_reference_ids.')';
				//Customer and Ticket Details
				$cd_query = 'select CD.*,FPTI.TicketId,FPTI.TicketNumber,FPTI.IssueDate,FPTI.Fare,FPTI.SegmentAdditionalInfo
							from flight_booking_passenger_details AS CD
							left join flight_passenger_ticket_info FPTI on CD.origin=FPTI.passenger_fk
							WHERE CD.flight_booking_transaction_details_fk IN 
							(select TD.origin from flight_booking_transaction_details AS TD 
							WHERE TD.app_reference IN ('.$app_reference_ids.'))';
				//Cancellation Details
				$cancellation_details_query = 'select FCD.*
						from flight_booking_passenger_details AS CD
						left join flight_cancellation_details AS FCD ON FCD.passenger_fk=CD.origin
						WHERE CD.flight_booking_transaction_details_fk IN 
						(select TD.origin from flight_booking_transaction_details AS TD 
						WHERE TD.app_reference IN ('.$app_reference_ids.'))';
					
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$booking_transaction_details = $this->db->query($td_query)->result_array();
				$cancellation_details = $this->db->query($cancellation_details_query)->result_array();
			}
			$response['data']['booking_details']			= $booking_details;
			//$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			// $response['data']['booking_transaction_details']	= $booking_transaction_details;
			$response['data']['booking_customer_details']	= $booking_customer_details;
			//$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}

	function hotel_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) as total_records 
					from hotel_booking_details BD
					join hotel_booking_itinerary_details AS HBID on BD.app_reference=HBID.app_reference
					join payment_option_list AS POL on BD.payment_mode=POL.payment_category_code 
					where BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$cancellation_details = array();
			$bd_query = 'select *,total_fare,BD.attributes from hotel_booking_details AS BD JOIN hotel_booking_itinerary_details as HBI
						WHERE HBI.app_reference = BD.app_reference and BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition.'
						order by BD.origin desc limit '.$offset.', '.$limit;
			$booking_details = $this->db->query($bd_query)->result_array();
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				$id_query = 'select * from hotel_booking_itinerary_details AS ID 
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				$cd_query = 'select * from hotel_booking_pax_details AS CD 
							WHERE CD.app_reference IN ('.$app_reference_ids.')';
				$cancellation_details_query = 'select * from hotel_cancellation_details AS HCD 
							WHERE HCD.app_reference IN ('.$app_reference_ids.')';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$cancellation_details	= $this->db->query($cancellation_details_query)->result_array();
			}
			$response['data']['booking_details']			= $booking_details;
			//$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			//$response['data']['booking_customer_details']	= $booking_customer_details;
			//$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}

	function bus_booking($condition=array(), $count=false, $offset=0, $limit=100000000000,$user_id)
	{
		$condition = $this->custom_db->get_custom_condition($condition);
		//BT, CD, ID
		if ($count) {
			$query = 'select count(distinct(BD.app_reference)) as total_records 
					from bus_booking_details BD
					join bus_booking_itinerary_details AS BBID on BD.app_reference=BBID.app_reference
					join payment_option_list AS POL on BD.payment_mode=POL.payment_category_code 
					where BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition;
			$data = $this->db->query($query)->row_array();
			return $data['total_records'];
		} else {
			$this->load->library('booking_data_formatter');
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = array();
			$booking_itinerary_details	= array();
			$booking_customer_details	= array();
			$cancellation_details = array();
			$bd_query = 'select *,BBCD.fare from bus_booking_details AS BD JOIN bus_booking_itinerary_details as BBI join bus_booking_customer_details as BBCD on BD.app_reference = BBCD.app_reference 
						WHERE BBI.app_reference = BD.app_reference and BD.domain_origin='.get_domain_auth_id().' and BD.created_by_id ='.$user_id.''.$condition.'
						order by BD.origin desc limit '.$offset.', '.$limit;
			
			$booking_details = $this->db->query($bd_query)->result_array();
	
			$app_reference_ids = $this->booking_data_formatter->implode_app_reference_ids($booking_details);
			if(empty($app_reference_ids) == false) {
				$id_query = 'select * from bus_booking_itinerary_details AS ID 
							WHERE ID.app_reference IN ('.$app_reference_ids.')';
				$cd_query = 'select * from bus_booking_customer_details AS CD 
							WHERE CD.app_reference IN ('.$app_reference_ids.')';
				$cancellation_details_query = 'select * from bus_cancellation_details AS BCD 
							WHERE BCD.app_reference IN ('.$app_reference_ids.')';
				$booking_itinerary_details	= $this->db->query($id_query)->result_array();
				$booking_customer_details	= $this->db->query($cd_query)->result_array();
				$cancellation_details	= $this->db->query($cancellation_details_query)->result_array();
			}
			$response['data']['booking_details']			= $booking_details;
			//$response['data']['booking_itinerary_details']	= $booking_itinerary_details;
			//$response['data']['booking_customer_details']	= $booking_customer_details;
			//$response['data']['cancellation_details']	= $cancellation_details;
			return $response;
		}
	}
	
}// main class end*************
