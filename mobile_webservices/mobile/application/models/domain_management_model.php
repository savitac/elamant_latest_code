<?php
require_once 'abstract_management_model.php';
/**
 * @package    current domain Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V2
 */
Class Domain_Management_Model extends Abstract_Management_Model
{
	private $airline_markup;
	private $hotel_markup;
	private $bus_markup;
	var $verify_domain_balance;

	function __construct() {
		parent::__construct('level_2');
		$this->verify_domain_balance = $this->config->item('verify_domain_balance');
	}

	/**
	 * Arjun J Gowda
	 * Get markup based on different modules
	 * @return array('value' => 0, 'type' => '')
	 */
	function get_markup($module_name, $module_type = 'b2c', $user_id = 0)
	{
		$markup_data = '';
		if ($module_type == 'b2c') {
			switch ($module_name) {
				case 'flight' : $markup_data = $this->airline_markup();
				break;
				case 'hotel' : $markup_data = $this->hotel_markup();
				break;
				case 'bus' : $markup_data = $this->bus_markup();
				break;
				case 'sightseeing' : $markup_data = $this->sightseeing_markup();
				break;

				case 'transferv1':$markup_data = $this->transferv1_markup();
				break;
			}
		} else {
			switch ($module_name) {
				case 'flight' : $markup_data = $this->airline_markup_b2b($user_id);
				break;
				case 'hotel' : $markup_data = $this->hotel_markup_b2b($user_id);
				break;
				case 'bus' : $markup_data = $this->bus_markup_b2b($user_id);
				break;
				case 'sightseeing' : $markup_data = $this->sightseeing_markup_b2b($user_id);
				break;

				case 'transferv1':$markup_data = $this->transferv1_markup_b2b($user_id);
				break;
			}
		}
		return $markup_data;
	}

	/**
	 * 
	 * Arjun J Gowda
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function airline_markup()
	{
		if (empty($this->airline_markup) == true) {
			$response['specific_markup_list'] = array();
			$specific_ailine_markup_list = $this->specific_airline_markup('b2c_flight');
			$response['specific_markup_list'] = $specific_ailine_markup_list;
			$response['generic_markup_list'] = $this->generic_domain_markup('b2c_flight');
			$this->airline_markup = $response;
		} else {
			$response = $this->airline_markup;
		}
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function hotel_markup()
	{
		if (empty($this->hotel_markup) == true) {
			$response['specific_markup_list'] = '';
			$response['generic_markup_list'] = $this->generic_domain_markup('b2c_hotel');
			$this->hotel_markup = $response;
		} else {
			$response = $this->hotel_markup;
		}
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function bus_markup()
	{
		if (empty($this->bus_markup) == true) {
			$response['specific_markup_list'] = '';
			$response['generic_markup_list'] = $this->generic_domain_markup('b2c_bus');
			$this->bus_markup = $response;
		} else {
			$response = $this->bus_markup;
		}
		return $response;
	}

	function airline_markup_b2b($user_id)
	{
		if (empty($this->airline_markup) == true) {
			$response['specific_markup_list'] = $this->specific_airline_markup_b2b('b2b_flight');
			$response['generic_markup_list'] = $this->generic_domain_markup_b2b('b2b_flight', $user_id);

			$this->airline_markup = $response;
		} else {
			$response = $this->airline_markup;
		}
		return $response;
	}

	/**
	 * Balu A
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function hotel_markup_b2b($user_id)
	{
		if (empty($this->hotel_markup) == true) {
			$response['specific_markup_list'] = '';
			$response['generic_markup_list'] = $this->generic_domain_markup_b2b('b2b_hotel', $user_id);
			$this->hotel_markup = $response;
		} else {
			$response = $this->hotel_markup;
		}
		return $response;
	}
	/**
	 * Balu A
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function bus_markup_b2b($user_id)
	{
		if (empty($this->bus_markup) == true) {
			$response['specific_markup_list'] = '';
			$response['generic_markup_list'] = $this->generic_domain_markup_b2b('b2b_bus', $user_id);
			$this->bus_markup = $response;
		} else {
			$response = $this->bus_markup;
		}
		return $response;
	}

	/**
	 * Arjun J Gowda
	 * Get generic markup based on the module type
	 * @param $module_type
	 * @param $markup_level
	 */
	function generic_domain_markup($module_type)
	{
		$query = 'SELECT ML.origin AS markup_origin, ML.type AS markup_type, ML.reference_id, ML.value, ML.value_type, ML.markup_currency AS markup_currency
		FROM markup_list AS ML where ML.value != "" and ML.module_type = "'.$module_type.'" and
		ML.markup_level = "'.$this->markup_level.'" and ML.type="generic" and ML.domain_list_fk='.get_domain_auth_id();
		$generic_data_list = $this->db->query($query)->result_array();
		return $generic_data_list;
	}

	function generic_domain_markup_b2b($module_type, $user_id = 0)
	{
		$query = 'SELECT ML.origin AS markup_origin, ML.type AS markup_type, ML.reference_id, ML.value, ML.value_type,  ML.markup_currency AS markup_currency
		FROM markup_list AS ML where ML.module_type = "'.$module_type.'" and
		ML.markup_level = "level_4" and ML.type="generic" and ML.domain_list_fk='.get_domain_auth_id().' and ML.user_oid=' . $user_id;

		$generic_data_list = $this->db->query($query)->result_array();
		return $generic_data_list;
	}

	function specific_airline_markup_b2b($module_type, $user_id = 0)
	{
		$markup_list = '';
		$query = 'SELECT AL.origin AS airline_origin, AL.name AS airline_name, AL.code AS airline_code,
		ML.origin AS markup_origin, ML.type AS markup_type, ML.reference_id, ML.value, ML.value_type, ML.markup_currency AS markup_currency
		FROM airline_list AS AL JOIN markup_list AS ML where ML.value != "" and
		ML.module_type = "'.$module_type.'" and ML.markup_level = "level_4" and AL.origin=ML.reference_id and ML.type="specific"
		and ML.domain_list_fk != 0  and ML.domain_list_fk='.get_domain_auth_id().' and ML.user_oid='. $user_id .' order by AL.name ASC';
		$specific_data_list = $this->db->query($query)->result_array();
		if (valid_array($specific_data_list)) {
			foreach ($specific_data_list as $__k => $__v) {
				$markup_list[$__v['airline_code']] = $__v;
			}
		}
		return $markup_list;
	}

	/**
	 *  Arjun J Gowda
	 * Get specific markup based on module type
	 * @param string $module_type	Name of the module for which the markup has to be returned
	 * @param string $markup_level	Level of markup
	 */
	function specific_airline_markup($module_type)
	{
		$markup_list = '';
		$query = 'SELECT AL.origin AS airline_origin, AL.name AS airline_name, AL.code AS airline_code,
		ML.origin AS markup_origin, ML.type AS markup_type, ML.reference_id, ML.value, ML.value_type, ML.markup_currency AS markup_currency
		FROM airline_list AS AL JOIN markup_list AS ML where ML.value != "" and
		ML.module_type = "'.$module_type.'" and ML.markup_level = "'.$this->markup_level.'" and AL.origin=ML.reference_id and ML.type="specific"
		and ML.domain_list_fk != 0  and ML.domain_list_fk='.get_domain_auth_id().' order by AL.name ASC';
		$specific_data_list = $this->db->query($query)->result_array();
		if (valid_array($specific_data_list)) {
			foreach ($specific_data_list as $__k => $__v) {
				$markup_list[$__v['airline_code']] = $__v;
			}
		}
		return $markup_list;
	}

	/**
	 * Check if the Booking Amount is allowed on Client Domain
	 */
	function verify_current_balance($amount, $currency)
	{
		$status = FAILURE_STATUS;
		if ($this->verify_domain_balance == true) {
			//OBSELETE - NO USE OF THIS
			if ($amount > 0) {
				$query = 'SELECT DL.balance, CC.country as currency, CC.value as conversion_value from domain_list as DL, currency_converter AS CC where CC.id=DL.currency_converter_fk
			AND DL.status='.ACTIVE.' and DL.origin='.$this->db->escape(get_domain_auth_id()).' and DL.domain_key = '.$this->db->escape(get_domain_key());
				$balance_record = $this->db->query($query)->row_array();
				if ($currency == $balance_record['currency']) {
					$balance = $balance_record['balance'];
					if ($balance >= $amount) {
						$status = SUCCESS_STATUS;
					} else {
						//Notify User about current balance problem
						//FIXME - send email, notification for less balance to domain admin and current domain admin
						$this->application_logger->balance_status('Your Balance Is Very Low To Make Booking Of '.$amount.' '.$currency);
					}
				} else {
					echo 'Under Construction';
					exit;
				}
			}
		} else {
			$status = SUCCESS_STATUS;
		}
		return $status;
	}


	function verify_current_balance_b2b($amount, $currency, $user_id)
	{
		$status = FAILURE_STATUS;
		if ($amount > 0) {
			$query = 'SELECT BU.balance, BU.credit_limit, BU.due_amount, CC.country as currency, CC.value as conversion_value
						from user as U
						JOIN b2b_user_details as BU ON U.user_id=BU.user_oid
						JOIN domain_list as DL ON U.domain_list_fk = DL.origin
						JOIN currency_converter CC ON CC.id=BU.currency_converter_fk
						WHERE U.status='.ACTIVE.' and U.user_id='.intval($user_id).' and 
						DL.status='.ACTIVE.' and DL.origin='.$this->db->escape(get_domain_auth_id()).' and DL.domain_key = '.$this->db->escape(get_domain_key());
			$balance_record = $this->db->query($query)->row_array();
			if ($currency == $balance_record['currency']) {
				// Due is always stored with -ve symbol
                $balance = $balance_record['balance'] + floatval($balance_record ['credit_limit']) + floatval($balance_record ['due_amount']);
				// $balance = $balance_record['balance'];
				if ($balance >= $amount) {
					$status = SUCCESS_STATUS;
				} else {
					//Notify User about current balance problem
					//FIXME - send email, notification for less balance to domain admin and current domain admin
				}
			} else {
				echo 'Under Construction--Currency mismatch';
				exit;
			}
		}
		return $status;
	}
	/**
	 *
	 * @param $fare
	 * @param $domain_markup
	 * @param $level_one_markup this is 0 as default as it is not mandatory to keep level one markup
	 */
	function update_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup=0, $convinence=0, $discount=0)
	{
		$status = FAILURE_STATUS;
		$amount = floatval($fare+$level_one_markup+$convinence-$discount);
		$remarks = $transaction_type.' Transaction was Successfully done';
		$notification_users = $this->user_model->get_admin_user_id();
		$action_query_string = array('app_reference' => $app_reference, 'type' => $transaction_type, 'module' => $this->config->item('current_module'));
		if ($this->verify_domain_balance == true) {
			echo 'We Dont Support This';
			exit;
			if ($amount > 0) {
				//deduct balance and continue
				$this->private_management_model->update_domain_balance(get_domain_auth_id(), (-$amount));
				//Log transaction details

				$this->save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $convinence, $discount, $remarks);
				$this->application_logger->transaction_status($remarks.'('.$amount.')', $action_query_string, $notification_users);
			}
		} else {
			$this->save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $convinence, $discount, $remarks);
			$this->application_logger->transaction_status($remarks.'('.$amount.')', $action_query_string, $notification_users);
			$status = SUCCESS_STATUS;
		}
		return $status;
	}

	function agent_buying_price($transaction_type, $app_reference)
	{
		$this->load->library('booking_data_formatter');
		switch($transaction_type) {
			case 'flight':
				$this->load->model('flight_model');
				$booking_data = $this->flight_model->get_booking_details($app_reference, '');
				$booking_data = $this->booking_data_formatter->format_flight_booking_data($booking_data, 'b2b');
				$amount = $booking_data['data']['booking_details'][0]['agent_buying_price'];
				break;
			case 'hotel':
				$this->load->model('hotel_model');
				$booking_data = $this->hotel_model->get_booking_details($app_reference, '');
				$booking_data = $this->booking_data_formatter->format_hotel_booking_data($booking_data, 'b2b');
				$amount = $booking_data['data']['booking_details'][0]['agent_buying_price'];
				break;
			case 'bus':
				$this->load->model('bus_model');
				$booking_data = $this->bus_model->get_booking_details($app_reference, '');
				$booking_data = $this->booking_data_formatter->format_bus_booking_data($booking_data, 'b2b');
				$amount = $booking_data['data']['booking_details'][0]['agent_buying_price'];
				break;
			case 'car':
				$this->load->model('car_model');
				$booking_data = $this->car_model->get_booking_details($app_reference, '');
				$booking_data = $this->booking_data_formatter->format_car_booking_datas($booking_data, 'b2b');
                                
				$amount = $booking_data['data']['booking_details'][0]['agent_buying_price'];
				break;
			case 'sightseeing':
				$this->load->model('sightseeing_model');
				$booking_data = $this->sightseeing_model->get_booking_details($app_reference, '');
				$booking_data = $this->booking_data_formatter->format_sightseeing_booking_data($booking_data, 'b2b');
                                
				$amount = $booking_data['data']['booking_details'][0]['agent_buying_price'];
				break;
			case 'transferv1':
			    $this->load->model('transferv1_model');
				$booking_data = $this->transferv1_model->get_booking_details($app_reference, '');
				$booking_data = $this->booking_data_formatter->format_transferv1_booking_data($booking_data, 'b2b');
                                
				$amount = $booking_data['data']['booking_details'][0]['agent_buying_price'];
				break;

		}
		return floatval($amount);
	}

	function update_transaction_details_b2b($transaction_type, $app_reference, $fare, $domain_markup=0, $level_one_markup=0,$convinence=0, $discount=0, $currency='INR', $currency_conversion_rate=1, $user_id = '')
	{
		$status = FAILURE_STATUS;
		$remarks = $transaction_type.' Transaction was Successfully done';
		$amount = $this->agent_buying_price($transaction_type, $app_reference);
		$notification_users = $this->user_model->get_admin_user_id();
		$action_query_string = array('app_reference' => $app_reference, 'type' => $transaction_type, 'module' => $this->config->item('current_module'));
		if ($this->verify_domain_balance == false) {
			//$amount = floatval($fare+$domain_markup);
			if ($amount > 0) {
				//Log transaction details
				$this->save_transaction_details($transaction_type, $app_reference, $amount, $domain_markup, $level_one_markup, $remarks, $convinence, $discount, $currency, $currency_conversion_rate);

				$this->update_agent_balance((-$amount), $user_id);
				//$this->save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $remarks);
				$this->application_logger->transaction_status($remarks.'('.$amount.')', $action_query_string, $notification_users);
			}
		} else {
			$this->save_transaction_details($transaction_type, $app_reference, $amount, $domain_markup, $level_one_markup, $remarks, $convinence, $discount, $currency, $currency_conversion_rate);
			$this->application_logger->transaction_status($remarks.'('.$amount.')', $action_query_string, $notification_users);
			$status = SUCCESS_STATUS;
		}
		return $status;
	}

	function update_agent_balance($amount, $user_id)
	{
		$current_balance = 0;
		$cond = array('user_oid' => $user_id);
		$details = $this->custom_db->single_table_records('b2b_user_details', 'balance,due_amount,credit_limit', $cond);
		if ($details['status'] == true) {
			$details ['data'] [0] ['balance'] = $current_balance = ($details ['data'] [0] ['balance'] + $amount);
                if ($details ['data'] [0] ['balance'] < 0) {
					$details ['data'] [0] ['due_amount'] += $details ['data'] [0] ['balance'];
                    $details ['data'] [0] ['balance'] = 0;
                }
                // debug($details);exit;
			// $details['data'][0]['balance'] = $current_balance = ($details['data'][0]['balance'] + $amount);
			$this->custom_db->update_record('b2b_user_details', $details['data'][0], $cond);
			$this->balance_notification($current_balance);
		}
		return $current_balance;
	}

	function balance_notification($current_balance)
	{
		$condition = array('agent_fk' => intval($this->entity_user_id));
		$details = $this->custom_db->single_table_records('agent_balance_alert_details', '*', $condition);
		if ($details['status'] == true) {
			$threshold_amount = $details['data'][0]['threshold_amount'];
			$mobile_number = trim($details['data'][0]['mobile_number']);
			$email_id = trim($details['data'][0]['email_id']);
			$enable_sms_notification = $details['data'][0]['enable_sms_notification'];
			$enable_email_notification = $details['data'][0]['enable_email_notification'];
			if($current_balance <= $threshold_amount) {
				//FIXME:Send Notification
				//SMS ALERT
				if($enable_sms_notification == ACTIVE && empty($mobile_number) == false) {
					//Send SMS Alert for Low Balance
				}
				//EMAIL NOTIFICATION
				if($enable_email_notification == ACTIVE && empty($email_id) == false) {
					//Send Email Notification for Low Balance
					$subject = $this->agency_name.'- Low Balance Alert';
					$message = 'Dear '.$this->entity_name.'<br/> <h1>Your Agent Balance is Low.</h1><br/><h2>Agent Balance as on '.date("Y-m-d h:i:sa").'is : '.COURSE_LIST_DEFAULT_CURRENCY_VALUE.' '.$threshold_amount.'/-</h2><h3>Please Recharge Your Account to enjoy UnInterrupted Bookings. :)</h3>';
					$this->load->library('provab_mailer');
					$mail_status = $this->provab_mailer->send_mail($email_id, $subject, $message);
				}
			}
		}
	}

	/**
	 * Save transaction logging for security purpose
	 * @param string $transaction_type
	 * @param string $app_reference
	 * @param number $fare
	 * @param number $domain_markup
	 * @param number $level_one_markup
	 * @param string $remarks
	 */
	function save_transaction_details($transaction_type, $app_reference, $fare, $domain_markup, $level_one_markup, $convinence, $discount, $remarks)
	{
		$transaction_log['system_transaction_id']	= date('Ymd-His').'-S-'.rand(1, 10000);
		$transaction_log['transaction_type']		= $transaction_type;
		$transaction_log['domain_origin']			= get_domain_auth_id();
		$transaction_log['app_reference']			= $app_reference;
		$transaction_log['fare']					= $fare;
		$transaction_log['level_one_markup']		= $level_one_markup;
		$transaction_log['domain_markup']			= $domain_markup;
		$transaction_log['convinence_fees']			= $convinence;
		$transaction_log['promocode_discount']		= $discount;
		$transaction_log['remarks']					= $remarks;
		$transaction_log['created_by_id']			= intval(@$this->entity_user_id) ;
		$transaction_log['created_datetime']		= date('Y-m-d H:i:s', time());
		$this->custom_db->insert_record('transaction_log', $transaction_log);
	}

	function transferv1_markup(){
		if (empty($this->transferv1_markup) == true) {
			$response['specific_markup_list'] = '';
			$response['generic_markup_list'] = $this->generic_domain_markup('b2c_transferv1');
			
			$this->transferv1_markup = $response;
		} else {
			$response = $this->transferv1_markup;
		}
		return $response;
	}

	/**
	 * Elavarasi
	 * Manage domain markup for current domain - Domain wise and module wise
	 */
	function sightseeing_markup()
	{
		if (empty($this->sightseeing_markup) == true) {
			$response['specific_markup_list'] = '';
			$response['generic_markup_list'] = $this->generic_domain_markup('b2c_sightseeing');
			
			$this->sightseeing_markup = $response;
		} else {
			$response = $this->sightseeing_markup;
		}
		return $response;
	}
	
}
