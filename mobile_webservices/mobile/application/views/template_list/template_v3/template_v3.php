<?php
$___favicon_ico = $GLOBALS ['CI']->template->domain_images ( 'favicon/favicon.ico' );
//lib
//mod
//pg
$active_domain_modules = $GLOBALS ['CI']->active_domain_modules;
$master_module_list = $GLOBALS ['CI']->config->item ( 'master_module_list' );
if (empty ( $default_view )) {
	$default_view = $GLOBALS ['CI']->uri->segment ( 1 );
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
		<meta name="keywords" content="Travelomatix, Flights, Hotels, Busses, Packages, Low Cost Flights">
		<meta name="description" content="Flight Bookings, Hotel Bookings, Bus Bookings, Package bookings system.">
		<meta name="author" content="travelomatix">
		<script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
		<link rel="shortcut icon" href="<?=$___favicon_ico?>" type="image/x-icon">

		<link rel="icon" href="<?=$___favicon_ico?>" type="image/x-icon">
		<title><?php echo get_app_message('AL001'). ' '.HEADER_TITLE_SUFFIX; ?></title>
		<link href='https://fonts.googleapis.com/css?family=Roboto'	rel='stylesheet' type='text/css' />
		<link href='https://fonts.googleapis.com/css?family=Roboto:300'	rel='stylesheet' type='text/css' />
		<link href='https://fonts.googleapis.com/css?family=Roboto:500'	rel='stylesheet' type='text/css' />
		<?php
			// Loading Common CSS and JS
			$GLOBALS ['CI']->current_page->header_css_resource ();
			Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('front_end.css'), 'media' => 'screen');
			$GLOBALS ['CI']->current_page->header_js_resource ();
			echo $GLOBALS ['CI']->current_page->css ();
			?>
		<!-- Custom CSS -->
		<link href="<?php echo $GLOBALS['CI']->template->template_css_dir('media.css'); ?>"	rel="stylesheet" />
		<script>
		var app_base_url = "<?=base_url()?>";
		var tmpl_img_url = '<?=$GLOBALS['CI']->template->template_images(); ?>';
		var _lazy_content;
		</script>
	</head>
	<body class="<?php echo (isset($body_class) == false ? 'index_page' : $body_class) ?>">
		<div class="allpagewrp">
			<!-- Header Start -->
			<header>
				<div class="section_top">
					<div class="container">
						<div class="topalstn">
							<div class="socila hidesocial">
								<?php
									//echo $this->CI->session('phone');
									$temp = $this->custom_db->single_table_records ( 'social_links' );
									if ($temp ['data'] ['0'] ['status'] == ACTIVE) {
									?>
								<a href="<?php echo $temp['data']['0']['url_link'];?>"><i class="fa fa-facebook"></i></a>
								<?php } if ($temp ['data'] ['1'] ['status'] == ACTIVE) { ?>
								<a href="<?php echo $temp['data']['1']['url_link'];?>"><i class="fa fa-twitter"></i></a>
								<?php } if ($temp['data']['2']['status'] == ACTIVE) {?>
								<a href="<?php echo $temp['data']['2']['url_link'];?>"> <i class="fa fa-google-plus"></i></a>
								<?php } if ($temp['data']['3']['status'] == ACTIVE) {?>
								<a href="<?php echo $temp['data']['3']['url_link'];?>"><i class="fa fa-youtube"></i></a>
								<?php } ?>
							</div>
							<div class="toprit">
								<div class="sectns">
									<a class="phnumr" href="tel:<?=$this->entity_domain_phone?>">
										<span class="sprte indnum samestl"></span>
										<span class="numhide"><?=$this->entity_domain_phone?></span>
										<div class="fa cliktocl fa-phone"></div>
									</a>
								</div>
								<div class="sectns">
									<a class="mailadrs" href="mailto:<?=$this->entity_domain_mail?>">
									<span class="fa fa-paper-plane"></span>
									<?=$this->entity_domain_mail?>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="topssec">
					<div class="container">
						<div class="bars_menu fa fa-bars menu_brgr"></div>
						<a class="logo" href="<?=base_url()?>">
						<img class="tab_logo" src="<?php echo $GLOBALS['CI']->template->domain_images('mobile_logo.png'); ?>" alt="Logo" /> 
						<img class="ful_logo" src="<?php echo $GLOBALS['CI']->template->domain_images($GLOBALS['CI']->template->get_domain_logo()); ?>"	alt="" />
						</a>
						<div class="menuandall">
							<div class="sepmenus">
								<ul class="exploreall">
									<?php
										foreach ( $master_module_list as $k => $v ) {
											if (in_array ( $k, $active_domain_modules )) {
									?>
									<li
										class="<?=((@$default_view == $k || $default_view == $v) ? 'active' : '')?>"><a
										href="<?php echo base_url()?>index.php/general/index/<?php echo ($v)?>?default_view=<?php echo $k?>">
										<span
											class="sprte cmnexplor <?=module_spirit_img(strtolower($v))?>"></span>
										<strong><?php echo ucfirst($v)?></strong>
										</a>
									</li>
									<?php
										}
									}
									?>
								</ul>
							</div>
							<div class="ritsude">
								<div class="sidebtn">
									<a class="topa logindown">
										<div class="reglog">
											<div class="userimage">
												<?php
													if (is_logged_in_user() == true && empty($GLOBALS['CI']->entity_image) == false) { 
														$profile_image = $GLOBALS['CI']->template->domain_images($GLOBALS['CI']->entity_image);
													} else {
														$profile_image = $GLOBALS['CI']->template->template_images('user.png');
													}
													?>
												<img src="<?php echo $profile_image;?>" alt="" />
											</div>
											<?php if (is_logged_in_user() == false) { ?>
											<div class="userorlogin">My Account</div>
											<?php } else {?>
											<div class="userorlogin"><?php echo $GLOBALS['CI']->entity_name?></div>
											<?php }?>
											<b class="caret cartdown"></b>
										</div>
									</a>
									<?=$GLOBALS['CI']->template->isolated_view('general/login')?>
								</div>
								<div class="sidebtn flagss">
									<a class="topa dropdown-toggle" data-toggle="dropdown">
										<div class="reglognorml">
											<div class="flag_images">
												<?php
													$curr = get_application_currency_preference();
													echo '<span class="curncy_img sprte '.strtolower($curr).'"></span>'?>
											</div>
											<div class="flags">
												<?php
													echo $curr;
													?>
											</div>
											<b class="caret cartdown"></b>
										</div>
									</a>
									<ul class="dropdown-menu exploreul explorecntry logdowndiv">
										<?=$this->template->isolated_view('utilities/multi_currency')?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<!-- Header End -->
			<div class="clearfix"></div>
			<!-- UTILITY NAV For Application MESSAGES START -->
			<div class="container-fluid utility-nav clearfix">
				<!-- ROW --> <?php
					if ($this->session->flashdata ( 'message' ) != "") {
						$message = $this->session->flashdata ( 'message' );
						$msg_type = $this->session->flashdata ( 'type' );
						$show_btn = TRUE;
						if ($this->session->flashdata ( 'override_app_msg' ) != "") {
							$override_app_msg = $this->session->flashdata ( 'override_app_msg' );
						} else {
							$override_app_msg = FALSE;
						}
						
						echo get_message ( $message, $msg_type, $show_btn, $override_app_msg );
					}
					?> <!-- /ROW -->
			</div>
			<!-- UTILITY NAV For Application MESSAGES END -->
			<!-- Body Printed Here -->
			<div class="fromtopmargin">
				<?=$body?>
			</div>
			<div class="clearfix"></div>
			<!-- Footer Start -->
			<footer>
				<div class="fstfooter">
					<div class="container">
						<div class="reftr">
							<div class="col-xs-12 col-sm-4 nopad fulnine">
								<div class="frtbest centertio">
									<h4 class="ftrhd">Let's Social</h4>
									<div class="signupfm">
										<?php
											if ($temp ['data'] ['0'] ['status'] == ACTIVE) {
												?>
										<li><a href="<?php echo $temp['data']['0']['url_link'];?>"><i
											class="faftrsoc fa fa-facebook"></i></a></li>
										<?php } ?>
										<?php if ($temp['data']['1']['status'] == ACTIVE) {?>
										<li><a href="<?php echo $temp['data']['1']['url_link'];?>"><i
											class="faftrsoc fa fa-twitter"></i></a></li>
										<?php } ?>
										<?php if ($temp['data']['2']['status'] == ACTIVE) {?>
										<li><a href="<?php echo $temp['data']['2']['url_link'];?>">
										<i class="faftrsoc fa fa-google-plus"></i></a></li>
										<?php } ?>
										<?php if ($temp['data']['3']['status'] == ACTIVE) {?>
										<li><a href="<?php echo $temp['data']['3']['url_link'];?>">
										<i class="faftrsoc fa fa-youtube"></i></a></li>
										<?php } ?>
									</div>
									<div class="footrlogo">
										<img src="<?php echo $GLOBALS['CI']->template->domain_images('footer_'.$GLOBALS['CI']->template->get_domain_logo()); ?>" alt="" />
									</div>
								</div>
							</div>
							<div class="col-xs-8 nopad fulnine">
								<div class="col-xs-4 nopad">
									<div class="frtbest">
										<h4 class="ftrhd arimo">About Us</h4>
										<ul>
											<?php
												$cond = array (
														'page_status' => ACTIVE 
												);
												$cms_data = $this->custom_db->single_table_records ( 'cms_pages', '', $cond );
												foreach ( $cms_data ['data'] as $keys => $values ) {
													echo '<li class="frteli"><a href="' . base_url () . 'index.php/general/cms/Bottom/' . $values ['page_id'] . '">' . $values ['page_title'] . ' <br> </a></li>';
												}
												?>
										</ul>
									</div>
								</div>
								<div class="col-xs-4 col-sm-4 nopad">
									<div class="frtbest">
										<h4 class="ftrhd arimo">Quick Links</h4>
										<ul>
											<li class="frteli"><a>Flight</a></li>
											<li class="frteli"><a>Hotel</a></li>
											<li class="frteli"><a>Transfers</a></li>
											<li class="frteli"><a> Excursions</a></li>
											<li class="frteli"><a>Deals</a></li>
											<li class="frteli"><a>Destinations</a></li>
										</ul>
									</div>
								</div>
								<div class="col-xs-4 nopad">
									<div class="frtbest">
										<h4 class="ftrhd arimo">Legal</h4>
										<ul>
											<li class="frteli"><a>Privacy Policy</a></li>
											<li class="frteli"><a>Terms & Conditions</a></li>
											<li class="frteli"><a>Taxes & Fees</a></li>
											<li class="frteli"><a>Fare Rules</a></li>
											<li class="frteli"><a>User Agreement</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
									<button type="button" class="btn btn-warning btn-xs pull-right col-md-3" data-toggle="modal" data-target="#myModal">Offline Payment</button>
				</div>
				<div class="clearfix"></div>
				

			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">

			    <!-- Modal content-->
			    <form action="<?=base_url()?>/index.php/general/offline_payment" method="post" name="offline" id="offline_form">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Offline Payment</h4>
			      </div>
			      			    <span class="text-success offline-msg"></span>

			      <div class="modal-body">
			       	<div class="form-group">
					  <label for="usr">Company Name:</label>
					  <input type="text" class="form-control" id="company_name" name="company_name">
					</div>
					<div class="form-group">
					  <label for="usr">Customer Name:</label>
					  <input type="text" class="form-control" id="name" name="name">
					</div>
					<div class="form-group">
					  <label for="usr">Customer Email:</label>
					  <input type="text" class="form-control" id="email" name="email">
					</div>
					<div class="form-group">
					  <label for="usr">Customer Contact No:</label>
					  <input type="text" class="form-control" id="phone" name="phone">
					</div>
					<div class="form-group">
					  <label for="usr">Amount:</label>
					  <input type="text" class="form-control" id="amount" name="amount">
					</div>
					<div class="form-group">
					  <label for="usr">Remarks:</label>
					  <textarea class="form-control" rows="5" id="remarks" name="remarks"></textarea>
					</div>

			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-success btn-offline-pay" >Submit</button>
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			   </form> 
			  </div>
			</div>


				<div class="btmfooter">
					<div class="container">
						<div class="acceptimg">
							<img class="img-responsive"
								src="<?php echo $GLOBALS['CI']->template->template_images('payment.png'); ?>"
								alt="" />
						</div>
						<div class="copyrit">
							© 2016 <a>travelomatix.com</a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- Footer End -->
		</div>
		<!-- Notification --> 
		<!--<div id="" class="fixed_pop hide">
			<div id="facebook_alert" class="inside_alert">
				<div class="alert_box">
					<div class="close_alert fa fa-times"></div>
					<div class="left_sidemage">
						<div class="matix_image">
							<span class="fa fa-facebook"></span>
						</div>
					</div>
					<div class="contentposps">
						<h3 class="noti_heading">You are signed in with Facebook</h3>
						<span class="message_alert">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet.</span>
					</div>
				</div>
			</div>
			<div id="google_alert" class="inside_alert">
				<div class="alert_box">
					<div class="close_alert fa fa-times"></div>
					<div class="left_sidemage">
						<div class="matix_image">
							<span class="fa fa-google-plus"></span>
						</div>
					</div>
					<div class="contentposps">
						<h3 class="noti_heading">You are signed in with Google Plus</h3>
						<span class="message_alert">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet.</span>
					</div>
				</div>
			</div>
			<div id="common_alert" class="inside_alert">
				<div class="alert_box">
					<div class="close_alert fa fa-times"></div>
					<div class="left_sidemage">
						<div class="matix_image">
							<span class="fa fa-exclamation-triangle"></span>
						</div>
					</div>
					<div class="contentposps">
						<h3 class="noti_heading">Warning</h3>
						<span class="message_alert">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet.</span>
					</div>
				</div>
			</div>
			<div id="success_alert" class="inside_alert">
				<div class="alert_box">
					<div class="close_alert fa fa-times"></div>
					<div class="left_sidemage">
						<div class="matix_image">
							<span class="fa fa-check"></span>
						</div>
					</div>
					<div class="contentposps">
						<h3 class="noti_heading">Success</h3>
						<span class="message_alert">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet.</span>
					</div>
				</div>
			</div>
			<div  class="inside_alert">
				<div class="alert_box">
					<div class="close_alert fa fa-times"></div>
					<div class="left_sidemage">
						<div class="matix_image">
							<img src=".." alt="" />
						</div>
					</div>
					<div class="contentposps">
						<h3 class="noti_heading">Preffered Airline</h3>
						<span class="message_alert">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet.</span>
					</div>
				</div>
			</div>
		</div>
		--><?php
		// Dynamic Loading of all the files needed in the application
		Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/datepicker.js'), 'defer' => 'defer');
		Provab_Page_Loader::load_core_resource_files();
		$GLOBALS ['CI']->current_page->footer_js_resource ();
		echo $GLOBALS ['CI']->current_page->js ();
		?>
		<script	src="<?php echo $GLOBALS['CI']->template->template_js_dir('modernizr.custom.js'); ?>" defer></script>
	</body>
</html>
