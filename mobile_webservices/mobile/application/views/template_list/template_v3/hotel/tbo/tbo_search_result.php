<?php
$template_images = $GLOBALS['CI']->template->template_images();
$mini_loading_image = '<div class="text-center loader-image">Please Wait</div>';
$trace_id = $raw_hotel_list['HotelSearchResult']['TraceId'];
foreach ($raw_hotel_list['HotelSearchResult']['HotelResults'] as $hd) {
	$current_hotel_rate = $hd['StarRating'];
?>
<div class="rowresult r-r-i">
	<div class="madgrid forhtlpopover">
		<div class="col-xs-3 nopad listimage">
			<div class="imagehtldis">
				<img src="" alt="Hotel img" data-src="<?=$hd['HotelPicture'] ?>" class="lazy h-img">
				<a class="mapviewhtlhotl fa fa-map-marker"></a>  
				<a href="<?php echo base_url().'index.php/hotel/map?lat='.$hd['Latitude'].'&lon='.$hd['Longitude'].'&hn='.urlencode($hd['HotelName']).'&sr='.intval($hd['StarRating']).'&c='.urlencode($hd['HotelLocation']).'&img='.urlencode($hd['HotelPicture'])?>" class="location-map mapviewhtlhotl fa fa-map-marker" target="map_box_frame"></a>
			</div>
		</div>
		<div class="col-xs-9 nopad listfull">
			<div class="sidenamedesc">
				<div class="celhtl width70">
					<div class="innd">
						<div class="property-type" data-property-type="hotel"></div>
						<div class="shtlnamehotl">
							<span class="h-name"><?php echo $hd['HotelName']?></span>
							<div class="starrtinghotl rating-no">
								<span class="h-sr hide"><?php echo $current_hotel_rate?></span>
								<?php echo print_star_rating($current_hotel_rate);?>
							</div>
						</div>
						<div class="hoteloctnf">
							<span class="fa fa-map-marker"></span>
							<span class="h-loc"><?=$hd['HotelLocation']?></span>
						</div>
						<div class="adreshotle h-adr"><?php echo $hd['HotelAddress']?></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<?php
					/**
					 * HOTEL PRICE SECTION With Markup price will be returned
					 * 
					 */
					$search_id = intval($attr['search_id']);
					//$PublishedPrice				= $hd['Price']['PublishedPrice'];
					//$PublishedPriceRoundedOff	= $hd['Price']['PublishedPriceRoundedOff'];
					//$OfferedPrice				= $hd['Price']['OfferedPrice'];
					//$OfferedPriceRoundedOff		= $hd['Price']['OfferedPriceRoundedOff'];
					$RoomPrice					= $hd['Price']['RoomPrice'];
					?>
				<div class="celhtl width30">
					<div class="sidepricewrp">
						<?php
							if (isset($hd['HotelPromotion']) == true and empty($hd['HotelPromotion']) == false) {?>
						<a href="#" class="dealicons" data-trigger="hover" data-placement="left" data-toggle="popover" data-deal="<?php echo ACTIVE?>" title="<?=$hd['HotelPromotion']?>" data-content="<?=$hd['HotelPromotion']?>"></a>
						<span class="deal-status hide" data-deal="<?php echo ACTIVE?>"></span>
					<?php } else {?>
						<span class="deal-status hide" data-deal="<?php echo INACTIVE?>"></span>
						<?php
							}?>
						<div class="priceflights">
							<div class="prcstrtingt">starting @ </div>
							<strong> <?php echo $currency_obj->get_currency_symbol($currency_obj->to_currency); ?> </strong>
							<span class="h-p"><?php echo roundoff_number($RoomPrice); ?></span>
						</div>
						<form action="<?php echo base_url().'index.php/hotel/hotel_details/'.($search_id)?>">
							<input type="hidden" value="<?=urlencode($hd['ResultIndex'])?>" name="ResultIndex" class="result-index">
							<input type="hidden" value="<?=urlencode($hd['HotelCode'])?>" name="HotelCode" class="hotel-code">
							<input type="hidden" value="<?=urlencode($trace_id)?>" name="TraceId" class="trace-id">
							<input type="hidden" value="<?=urlencode($booking_source)?>" name="booking_source" class="booking_source">
							<input type="hidden" value="get_details" name="op" class="operation">
							<button class="confirmBTN b-btn bookallbtn splhotltoy" type="submit"> Book Now</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<form class="room-form hide">
			<input type="hidden" value="<?=urlencode($hd['ResultIndex'])?>" name="ResultIndex" class="result-index">
			<input type="hidden" value="<?=urlencode($hd['HotelCode'])?>" name="HotelCode" class="hotel-code">
			<input type="hidden" value="<?=urlencode($trace_id)?>" name="TraceId" class="trace-id">
			<input type="hidden" value="<?=urlencode($booking_source)?>" name="booking_source" class="booking_source">
			<input type="hidden" name="op" value="get_room_details">
			<input type="hidden" name="search_id" value="<?=$search_id?>">
		</form>
		<div class="viewhotlrmtgle">
			<button class="vwrums room-btn" type="button">View Rooms</button>
		</div>
		<div class="clearfix"></div>
		<div class="room-list" style="display:none">
			<div class="room-summ romlistnh">
				<?=$mini_loading_image?>
			</div>
		</div>
	</div>
</div>
<?php
}