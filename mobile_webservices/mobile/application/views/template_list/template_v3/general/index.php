<?php
/*$image = $banner_images ['data'] [0] ['image'];*/
$active_domain_modules = $this->active_domain_modules;
$default_active_tab = $default_view;
/**
 * set default active tab
 *
 * @param string $module_name
 *        	name of current module being output
 * @param string $default_active_tab
 *        	default tab name if already its selected otherwise its empty
 */
function set_default_active_tab($module_name, &$default_active_tab) {
	if (empty ( $default_active_tab ) == true || $module_name == $default_active_tab) {
		if (empty ( $default_active_tab ) == true) {
			$default_active_tab = $module_name; // Set default module as current active module
		}
		return 'active';
	}
}

//add to js of loader
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('backslider.css'), 'media' => 'screen');
Js_Loader::$css[] = array('href' => $GLOBALS['CI']->template->template_css_dir('owl.carousel.min.css'), 'media' => 'screen');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('owl.carousel.min.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('backslider.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/index.js'), 'defer' => 'defer');
Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('page_resource/pax_count.js'), 'defer' => 'defer');
?>

<div class="searcharea">
	<div class="srchinarea">
		<div class="container">
			<div class="captngrp">
				<div id="big1" class="bigcaption">BOOK NOW THE BEST DEALS</div>
				<div id="desc" class="smalcaptn">Book securely and with confidence</div>
			</div>
		</div>
		<div class="allformst">
			<div class="container">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs tabstab">
					<?php if (is_active_airline_module()) { ?>
					<li
						class="<?php echo set_default_active_tab(META_AIRLINE_COURSE, $default_active_tab)?>"><a
						href="#flight" role="tab" data-toggle="tab"><span
						class="sprte iconcmn icnhnflight"></span><label>Flight</label></a></li>
					<?php } ?>
					<?php if (is_active_hotel_module()) { ?>
					<li
						class="<?php echo set_default_active_tab(META_ACCOMODATION_COURSE, $default_active_tab)?>"><a
						href="#hotel" role="tab" data-toggle="tab"><span
						class="sprte iconcmn icnhtl"></span><label>Hotel</label></a></li>
					<?php } ?>
					<?php if (is_active_bus_module()) { ?>
					<li
						class="<?php echo set_default_active_tab(META_BUS_COURSE, $default_active_tab)?>"><a
						href="#bus" role="tab" data-toggle="tab"><span
						class="sprte iconcmn icnhnbus"></span><label>Bus</label></a></li>
					<?php } ?>
					<?php if (is_active_package_module()) { ?>
					<li
						class="<?php echo set_default_active_tab(META_PACKAGE_COURSE, $default_active_tab)?>"><a
						href="#holiday" role="tab"
						data-toggle="tab"><span class="sprte iconcmn icnhnhlydy"></span><label>Holiday</label></a></li>
					<?php } ?>
				</ul>
			</div>
			<!-- Tab panes -->
			<div class="secndblak">
				<div class="container">
					<div class="tab-content custmtab">
						<?php if (is_active_airline_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_AIRLINE_COURSE, $default_active_tab)?>"
							id="flight">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/flight_search')?>
						</div>
						<?php } ?>
						<?php if (is_active_hotel_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_ACCOMODATION_COURSE, $default_active_tab)?>"
							id="hotel">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/hotel_search')?>
						</div>
						<?php } ?>
						<?php if (is_active_bus_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_BUS_COURSE, $default_active_tab)?>"
							id="bus">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/bus_search')?>
						</div>
						<?php } ?>
						<?php if (is_active_package_module()) { ?>
						<div
							class="tab-pane <?php echo set_default_active_tab(META_PACKAGE_COURSE, $default_active_tab)?>"
							id="holiday">
							<?php echo $GLOBALS['CI']->template->isolated_view('share/holiday_search',$holiday_data)?>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="perhldys">
<div class="container">
	<div class="">
		<div class="pagehdwrap">
			<h2 class="pagehding">Perfect Holidays</h2>
		</div>
		<div class="retmnus">
			<div id="owl-demo2" class="owl-carousel owlindex2">
				<?php
				
					//debug($top_destination_package[0]);exit; 
					$k = 0;
					while ( $total > 0 ) {
						?>	
				<div class="item">
					<div class="col-xs-12 nopad">
						<?php for($i=1;$i<=2;$i++) { 
							if(isset($top_destination_package[$k])){
								?>
						<div class="topone">
							<div class="inspd2">
								<div class="imgeht2">
									<img
										class="lazy lazy_loader"
										data-src="<? echo $GLOBALS['CI']->template->domain_upload_pckg_images(basename($top_destination_package[$k]->image)); ?>"
										alt="<?php echo $top_destination_package[$k]->package_name; ?>"
										src="<?=LAZY_IMG_ENCODED_STR?>"
										/>
									<?php if(($k % 2) == 0) {?>
									<div class="absint2 absintcol1 ">
										<?php } else {?>
										<div class="absint2 absintcol2 ">
											<?php } ?>
											<div class="absinn">
												<div class="smilebig2">
													<h3><?php echo $top_destination_package[$k]->package_name; ?> </h3>
												</div>
												<div class="clearfix"></div>
												<a class="package_dets_btn" href="<?=base_url().'index.php/tours/details/'.$top_destination_package[$k]->package_id?>">
												View details
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php } $k++ ;	} $total = $total-2;
								?>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="htldeals">
	<div class="container">
		<div class="pagehdwrap">
			<h2 class="pagehding">Top Hotel Destinations</h2>
		</div>
		<div class="tophtls">
			<div class="grid">
				<?php
					if (in_array ( META_ACCOMODATION_COURSE, $active_domain_modules ) and valid_array ( $top_destination_hotel ) == true) : // TOP DESTINATION
						foreach ( $top_destination_hotel as $tk => $tv ) :
							?>
				<?php if(($tk-0)%10 == 0){?>
				<div class="col-sm-8 col-xs-12 nopad htd-wrap">
					<div class="effect-marley figure">
						<img
							class="lazy lazy_loader"
							src="<?=LAZY_IMG_ENCODED_STR?>"
							data-src="<?php echo $GLOBALS['CI']->template->domain_images($tv['image']); ?>"
							alt="<?=$tv['destination']?>" />
						<div class="figcaption">
							<h3 class="clasdstntion"><?=$tv['destination']?></h3>
							<p>(<?=rand(99, 500)?> Hotels)</p>
							<input type="hidden"
								class="top-des-val hand-cursor"
								value="<?=hotel_suggestion_value($tv['destination'], $tv['country'])?>">
							<a href="#">View more</a>
						</div>
					</div>
				</div>
				<?php } elseif (($tk-6)%10 == 0){ ?>
				<div class="col-sm-8 col-xs-6 nopad htd-wrap">
					<div class="effect-marley figure">
						<img
							class="lazy lazy_loader"
							src="<?=LAZY_IMG_ENCODED_STR?>"
							data-src="<?php echo $GLOBALS['CI']->template->domain_images($tv['image']); ?>"
							alt="<?=$tv['destination']?>" />
						<div class="figcaption">
							<h3 class="clasdstntion"><?=$tv['destination']?></h3>
							<p>(<?=rand(99, 500)?> Hotels)</p>
							<input type="hidden"
								class="top-des-val hand-cursor"
								value="<?=hotel_suggestion_value($tv['destination'], $tv['country'])?>">
							<a href="#">View more</a>
						</div>
					</div>
				</div>
				<?php } else {?>
				<div class="col-sm-4 col-xs-6 nopad htd-wrap">
					<div class="effect-marley figure">
						<img
							class="lazy lazy_loader"
							src="<?=LAZY_IMG_ENCODED_STR?>"
							data-src="<?php echo $GLOBALS['CI']->template->domain_images($tv['image']); ?>"
							alt="<?=$tv['destination']?>" />
						<div class="figcaption">
							<h3 class="clasdstntion"><?=$tv['destination']?></h3>
							<p>(<?=rand(99, 500)?> Hotels)</p>
							<input type="hidden"
								class="top-des-val hand-cursor"
								value="<?=hotel_suggestion_value($tv['destination'], $tv['country'])?>">
							<a href="#">View more</a>
						</div>
					</div>
				</div>
				<?php
					}
					endforeach;
					endif; // TOP DESTINATION
				?>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="ychoose">
	<div class="container">
		<div class="allys">
			<div class="col-xs-4 nopad">
				<div class="threey">
					<div class="sprte apritopty sppricegu"></div>
					<div class="dismany">
						<div class="hedsprite">Best Price<br /> Guaranteed</div>
					</div>
				</div>
			</div>
			<div class="col-xs-4 nopad">
				<div class="threey">
					<div class="sprte apritopty spsatis"></div>
					<div class="dismany">
						<div class="hedsprite">99.9% Customer<br /> Satisfaction</div>
					</div>
				</div>
			</div>
			<div class="col-xs-4 nopad">
				<div class="threey">
					<div class="sprte apritopty spsupprt"></div>
					<div class="dismany">
						<div class="hedsprite">24/7 Customer<br /> Support</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?=$this->template->isolated_view('share/js/lazy_loader')?>
