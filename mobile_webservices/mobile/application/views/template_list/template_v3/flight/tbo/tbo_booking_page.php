<title>SSRE</title>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<meta name="viewport" content="width=device-width, initial-scale=1" /> 

<script  src="<?=Base_URL?>/extras/system/library/bootstrap/js/bootstrap.min.js" defer="defer" charset="UTF-8"></script>
<!-- <script  src="<?=Base_URL?>/extras/system/template_list/template_v3/javascript/javascript.js" defer="defer" charset="UTF-8"></script> -->

<script  src="<?=Base_URL?>extras/system/template_list/template_v3/javascript/page_resource/flight_extra_services.js" defer="defer" charset="UTF-8"></script>

<link  href="<?=Base_URL?>/extras/system/template_list/template_v3/css/flight_extra_services.css" media="screen" rel="stylesheet" type="text/css" hreflang="en" />
<link  href="<?=Base_URL?>/extras/system/template_list/template_v3/css/theme_style.css" media="screen" rel="stylesheet" type="text/css" hreflang="en" />
<link  href="<?=Base_URL?>/extras/system/template_list/template_v3/css/front_end.css" media="screen" rel="stylesheet" type="text/css" hreflang="en" />
<link  href="<?=Base_URL?>/extras/system/template_list/template_v3/css/media.css" media="screen" rel="stylesheet" type="text/css" hreflang="en" />
<link  href="<?=Base_URL?>/extras/system/library/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" hreflang="en" />
<?php
    // Loading Common CSS and JS
    $GLOBALS ['CI']->current_page->header_css_resource ();
   /* Js_Loader::$css [] = array (
        'href' => $GLOBALS ['CI']->template->template_css_dir ( 'page_resource/index.css' ),
        'media' => 'screen' 
    ); */
    // Js_Loader::$css [] = array (
    //     'href' => $GLOBALS ['CI']->template->template_css_dir ( 'front_end.css' ),
    //     'media' => 'screen' 
    // );
    // $GLOBALS ['CI']->current_page->header_js_resource ();
    echo $GLOBALS ['CI']->current_page->css ();

   

    ?>
    <!-- <link href="<?php echo $GLOBALS['CI']->template->template_css_dir('media.css'); ?>" rel="stylesheet" /> -->
<?php 

// debug($passenger_data);exit;
//	include_once 'process_tbo_response.php';
	//$template_images = $GLOBALS['CI']->template->template_images();
	//Js_Loader::$js[] = array('src' => $GLOBALS['CI']->template->template_js_dir('provablib.js'), 'defer' => 'defer');
	/*Flight Booking Summary
	$FareDetails = $pre_booking_summery['FareDetails']['b2c_PriceDetails'];
	//debug($FareDetails);exit;
	$PassengerFareBreakdown = $pre_booking_summery['PassengerFareBreakdown'];
	$SegmentDetails = $pre_booking_summery['SegmentDetails'];
	$SegmentSummary = $pre_booking_summery['SegmentSummary'];
	/********************************* Convenience Fees *****************************
	$flight_total_amount = $FareDetails['TotalFare']+$convenience_fees;
	/********************************* Convenience Fees ********************************
	$currency_symbol = $FareDetails['CurrencySymbol'];
	$currency = $FareDetails['Currency'];

	$is_domestic = $pre_booking_params['is_domestic'];
	//Segment Summary and Details
	$flight_segment_details =  flight_segment_details($SegmentDetails, $SegmentSummary);
	if ($is_domestic != true) {
		$pass_mand = '<sup class="text-danger">*</sup>';
		$pass_req = 'required="required"';
	} else {
		$pass_mand = '';
		$pass_req = '';
	}
	$mandatory_filed_marker = '<sup class="text-danger">*</sup>';
	//Jaganath
	$is_domestic_flight = $search_data['is_domestic_flight'];
	if($is_domestic_flight) {
		$temp_passport_expiry_date = date('Y-m-d', strtotime('+5 years'));
		$static_passport_details = array();
		$static_passport_details['passenger_passport_expiry_day'] = date('d', strtotime($temp_passport_expiry_date));
		$static_passport_details['passenger_passport_expiry_month'] = date('m', strtotime($temp_passport_expiry_date));
		$static_passport_details['passenger_passport_expiry_year'] = date('Y', strtotime($temp_passport_expiry_date));
	}
	if(is_logged_in_user()) {
		$review_active_class = ' success ';
		$review_tab_details_class = '';
		$review_tab_class = ' inactive_review_tab_marker ';
		$travellers_active_class = ' active ';
		$travellers_tab_details_class = ' gohel ';
		$travellers_tab_class = ' travellers_tab_marker ';
	} else {
		$review_active_class = ' active ';
		$review_tab_details_class = ' gohel ';
		$review_tab_class = ' review_tab_marker ';
		$travellers_active_class = '';
		$travellers_tab_details_class = '';
		$travellers_tab_class = ' inactive_travellers_tab_marker ';
	} */
	
	$total_adult_count	= is_array($search_data['adult_config']) ? array_sum($search_data['adult_config']) : intval($search_data['adult_config']);
								$total_child_count	= is_array($search_data['child_config']) ? array_sum($search_data['child_config']) : intval($search_data['child_config']);
								$total_infant_count	= is_array($search_data['infant_config']) ? array_sum($search_data['infant_config']) : intval($search_data['infant_config']);
		$total_pax_count = $total_adult_count+$total_child_count+$total_infant_count;
		$adult_enum = $child_enum = get_enum_list('title');
								$gender_enum = get_enum_list('gender');
								unset($adult_enum[MASTER_TITLE]); // Master is for child so not required
								unset($child_enum[MASTER_TITLE]); // Master is not supported in TBO list
		function pax_type($pax_index, $total_adult, $total_child, $total_infant)
								{
									if ($pax_index <= $total_adult) {
										$pax_type = 'adult';
									} elseif ($pax_index <= ($total_adult+$total_child)) {
										$pax_type = 'child';
									} else {
										$pax_type = 'infant';
									}
									return $pax_type;
								}
		function is_lead_pax($pax_count)
								{
									return ($pax_count == 1 ? true : false);
								}
		function is_adult($pax_index, $total_adult)
								{
									return ($pax_index>$total_adult ?	false : true);
								}
		function pax_type_count($pax_index, $total_adult, $total_child, $total_infant)
								{
									if ($pax_index <= $total_adult) {
										$pax_count = ($pax_index);
									} elseif ($pax_index <= ($total_adult+$total_child)) {
										$pax_count = ($pax_index-$total_adult);
										
									} else {
										$pax_count = ($pax_index-($total_adult+$total_child));
									}
									return $pax_count;
								}
	function diaplay_phonecode($phone_code,$active_data, $user_country_code)
								{
									
									
									$list='';
									foreach($phone_code as $code){
									if(!empty($user_country_code)){
										if($user_country_code==$code['country_code']){
											$selected ="selected";
										}
										else {
											$selected="";
										}
									}
									else{
										
										if($active_data['api_country_list_fk']==$code['origin']){
											$selected ="selected";
										}
										else {
											$selected="";
										}
									}
										$list .="<option value=".$code['name']." ".$code['country_code']."  ".$selected." >".$code['name']." ".$code['country_code']."</option>";
									
									}
									 return $list;
									
								}
								
		$adult_title_options = generate_options($adult_enum, false, true);
								$child_title_options = generate_options($child_enum, false, true);
								$gender_options	= generate_options($gender_enum);
	$book_login_auth_loading_image	 = '<div class="text-center loader-image"><img src="'.$GLOBALS['CI']->template->template_images('loader_v3.gif').'" alt="please wait"/></div>';
?>
<style>
	.topssec::after{display:none;}
</style>
<!--<div class="fldealsec">
	<div class="container">
		<div class="tabcontnue">
			<div class="col-xs-4 nopadding">
				<div class="rondsts <?=$review_active_class?>">
					<a class="taba core_review_tab <?=$review_tab_class?>" id="stepbk1">
						<div class="iconstatus fa fa-eye"></div>
						<div class="stausline">Review</div>
					</a>
				</div>
			</div>
			<div class="col-xs-4 nopadding">
				<div class="rondsts <?=$travellers_active_class?>">
					<a class="taba core_travellers_tab <?=$travellers_tab_class?>" id="stepbk2">
						<div class="iconstatus fa fa-group"></div>
						<div class="stausline">Travellers</div>
					</a>
				</div>
			</div>
			<div class="col-xs-4 nopadding">
				<div class="rondsts">
					<a class="taba" id="stepbk3">
						<div class="iconstatus fa fa-money"></div>
						<div class="stausline">Payments</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div> -->
<div class="clearfix"></div>
<form action="<?=Base_URL.'mobile_webservices/mobile/index.php/flight/process_additional_ssr/'.@$search_data['search_id']?>" method="POST" autocomplete="off" id="pre-booking-form">
								<div class="hide">
									<input type="hidden" required="required" name="search_id"		value="<?=$search_data['search_id'];?>" />
									<?php $dynamic_params_url = serialized_data($pre_booking_params);?>
									<input type="hidden" required="required" name="token"		value="<?=$dynamic_params_url;?>" />
									<input type="hidden" required="required" name="token_key"	value="<?=md5($dynamic_params_url);?>" />
									<input type="hidden" required="required" name="passenger_contact" value="<?=$ContactNo?>" >
									<input type="hidden" required="required" name="passenger_data" value="<?=$passenger_data?>" >
									<input type="hidden" required="required" name="billing_email"	value="<?=$Email?>">
									<input type="hidden" required="required" name="first_name" value="<?=$FirstName?>" >
									<input type="hidden" required="required" name="last_name"	value="<?=$LastName?>">
									<input type="hidden" required="required" name="op"			value="book_flight">
									<input type="hidden" required="required" name="booking_source"		value="<?=$booking_source?>" readonly>
									<input type="hidden" required="required" name="promo_code_discount_val" id="promo_code_discount_val" value="0.00" readonly>
									<input type="hidden" required="required" name="promo_code" id="promocode_val" value="" readonly>
									<input type="hidden" required="required" name="wallet_bal" id="wallet_bal" value="<?=$wallet_bal?>" readonly>
									<!--<input type="hidden" required="required" name="provab_auth_key" value="?=$ProvabAuthKey ?>" readonly>
								--></div>
								<div class="flitab1">
									<!--<div class="moreflt boksectn">
										<div class="ontyp">
											<div class="labltowr arimobold">Please enter names as on passport. </div>
											<?php
												$pax_index = 1;
												$lead_pax_details = @$pax_details[0];
												if(is_logged_in_user()) {
												$traveller_class = ' user_traveller_details ';
												} else {
												$traveller_class = '';
												}
													for($pax_index=1; $pax_index <= $total_pax_count; $pax_index++) {//START FOR LOOP FOR PAX DETAILS
													$cur_pax_info = is_array($pax_details) ? array_shift($pax_details) : array();
													$pax_type = pax_type($pax_index, $total_adult_count, $total_child_count, $total_infant_count);
													$pax_type_count = pax_type_count($pax_index, $total_adult_count, $total_child_count, $total_infant_count);
													
													if($pax_type != 'infant') {
														$extract_pax_name_cls = ' extract_pax_name_cls ';
													} else {
														$extract_pax_name_cls = '';
													}
													?>
											<div class="pasngr_input pasngrinput _passenger_hiiden_inputs">
												<div class="hide hidden_pax_details">
													<input type="hidden" name="passenger_type[]" value="<?=ucfirst($pax_type)?>">
													<input type="hidden" name="lead_passenger[]" value="<?=(is_lead_pax($pax_index) ? true : false)?>">
													<input type="hidden" name="gender[]" value="1" class="pax_gender">
													<input type="hidden" required="required" name="passenger_nationality[]" id="passenger-nationality-<?=$pax_index?>" value="92">
												</div>
												<div class="col-xs-1 nopadding full_dets_aps">
													<div class="adltnom"><?=ucfirst($pax_type)?><?=$pax_type_count?><?=$mandatory_filed_marker?></div>
												</div>
												<div class="col-xs-11 nopadding full_dets_aps">
												
													<div class="inptalbox">
														<div class="col-xs-3 spllty">
															<div class="selectedwrap">
																<select class="mySelectBoxClass flyinputsnor name_title" name="name_title[]" required>
																<?php echo (is_adult($pax_index, $total_adult_count) ? $adult_title_options : $child_title_options)?>
																</select>
															</div>
														</div>
														
														<?php if($pax_type == 'infant') {//Only For Infant?>
														<div class="col-xs-6 spllty infant_dob_div">
															<div class="col-xs-4 nopadding"><span class="fmlbl">Date of Birth <?=$mandatory_filed_marker?></span></div>
															<div class="col-xs-8 nopadding">
																<input placeholder="DOB" type="text" class="clainput"  name="date_of_birth[]" readonly <?=(is_adult($pax_index, $total_adult_count) ? 'required="required"' : 'required="required"')?> id="<?=strtolower(pax_type($pax_index, $total_adult_count, $total_child_count, $total_infant_count))?>-date-picker-<?=$pax_index?>">
															</div>
														</div>
														<?php } else{ //Adult/Child
															if($pax_type == 'adult') {
																$static_date_of_birth = date('Y-m-d', strtotime('-30 years'));;
															} else if($pax_type == 'child') {
																$static_date_of_birth = date('Y-m-d', strtotime('-8 years'));;
															}
															?>
														<div class="adult_child_dob_div hide">
															<input type="hidden" name="date_of_birth[]" value="<?=$static_date_of_birth?>">
														</div>
														<?php } ?>
														<div class="clearfix"></div>
														<!-- Passport Section Starts
														<div class="passport_content_div">
															<?php if($is_domestic_flight == false) { //For Internatinal Travel?>
															<div class="international_passport_content_div">
																<div class="col-xs-4 spllty">
																	<span class="formlabel">Passport Number <?=$pass_mand?></span>
																	<div class="relativemask"> 
																		<input type="text" name="passenger_passport_number[]" <?=$pass_req?> id="passenger_passport_number_<?=$pax_index?>" class="clainput" maxlength="10" placeholder="Passport Number" />
																	</div>
																</div>
																<div class="col-xs-3 spllty">
																	<span class="formlabel">Issuing Country <?=$pass_mand?></span>
																	<div class="selectedwrap">
																		<select name="passenger_passport_issuing_country[]" <?=$pass_req?> id="passenger_passport_issuing_country_<?=$pax_index?>" class="mySelectBoxClass flyinputsnor">
																			<option value="INVALIDIP">Please Select</option>
																			<?=$passport_issuing_country_options?>
																		</select>
																	</div>
																</div>
																<div class="col-xs-5 spllty">
																	<span class="formlabel">Date of Expire <?=$pass_mand?></span>
																	<div class="relativemask">
																		<div class="col-xs-4 splinmar">
																			<div class="selectedwrap">
																				<select name="passenger_passport_expiry_day[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_day" data-expiry-type="day" id="passenger_passport_expiry_day_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
																					<option value="INVALIDIP">DD</option>
																					<?=$day_options;?>
																				</select>
																			</div>
																		</div>
																		<div class="col-xs-4 splinmar">
																			<div class="selectedwrap">
																				<select name="passenger_passport_expiry_month[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_month" data-expiry-type="month" id="passenger_passport_expiry_month_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
																					<option value="INVALIDIP">MM</option>
																					<?=$month_options;?>
																				</select>
																			</div>
																		</div>
																		<div class="col-xs-4 splinmar">
																			<div class="selectedwrap">
																				<select name="passenger_passport_expiry_year[]" <?=$pass_req?> class="mySelectBoxClass flyinputsnor passport_expiry_year" data-expiry-type="year" id="passenger_passport_expiry_year_<?=$pax_index?>" data-row-id="<?=($pax_index);?>">
																					<option value="INVALIDIP">YYYY</option>
																					<?=$year_options;?>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
															<div class="pull-right text-danger hide" id="passport_error_msg_<?=$pax_index?>"></div>
															</div>
															<?php } else { //For Domestic Travel, Set Static Passport Data
																$passport_number = rand(1111111111,9999999999);
																$passport_issuing_country = 92;
																?>
															<div class="domestic_passport_content_div hide">
																<input type="hidden" name="passenger_passport_number[]" value="<?=$passport_number?>" id="passenger_passport_number_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_issuing_country[]" value="<?=$passport_issuing_country?>" id="passenger_passport_issuing_country_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_expiry_day[]" value="<?=$static_passport_details['passenger_passport_expiry_day']?>" id="passenger_passport_expiry_day_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_expiry_month[]" value="<?=$static_passport_details['passenger_passport_expiry_month']?>" id="passenger_passport_expiry_month_<?=$pax_index?>">
																<input type="hidden" name="passenger_passport_expiry_year[]" value="<?=$static_passport_details['passenger_passport_expiry_year']?>" id="passenger_passport_expiry_year_<?=$pax_index?>">
															</div>
															<?php }?>
														</div>
														<!-- Passport Section Ends-
														
													</div><!-- inptalbox class ends 
												</div>
											</div>
											<?php
												}//END FOR LOOP FOR PAX DETAILS
												?>
										</div>
									</div> -->
									<input type="hidden" name="total_amount_val" id="total_amount_val" class="promocode" value="<?=@$FareDetails['TotalFare'];?>" />
										<!--	<div class="ontyp">
								<div class="kindrest">
										<div class="cartlistingbuk">
													<div class="cartitembuk">
														<div class="col-md-12">
															<div class="payblnhmxm">Have an e-coupon or a deal-code ? (Optional)</div>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="cartitembuk prompform">
														<form name="promocode" id="promocode" novalidate>
									                    <div class="col-md-4 col-xs-8 nopadding_right">
									                      <div class="cartprc">
									                        <div class="payblnhm singecartpricebuk ritaln">
									                    	 <input type="text" placeholder="Enter Promo" name="code" id="code" class="promocode" aria-required="true" />
									                          <input type="hidden" name="module_type" id="module_type" class="promocode" value="<?=@$module_value;?>" />
									                         
									                          <input type="hidden" name="convenience_fee" id="convenience_fee" class="promocode" value="<?=@$convenience_fees;?>" />
									                          <input type="hidden" name="currency_symbol" id="currency_symbol" value="<?=@$currency_symbol;?>" />
									                          <input type="hidden" name="currency" id="currency" value="<?=@$currency;?>" />
									                         
									                          <p class="error_promocode text-danger"></p>                     
									                        </div>
									                      </div>
									                    </div>
									                    <div class="col-md-2 col-xs-3 nopadding_left">
									                      <input type="button" value="Apply" name="apply" id="apply" class="promosubmit">
									                    </div>
									                  </form>
													</div>
													<div class="clearfix"></div>
													<div class="savemessage"></div>
												</div>
										</div> 
											</div>-->
									<div class="sepertr"></div>
									<div class="clearfix"></div>
									<!--<div class="contbk">
										<div class="contcthdngs">CONTACT DETAILS</div>
										<div class="col-xs-12 nopad full_smal_forty">
										<div class="col-xs-6 nopad full_smal_forty">
											<div class="col-xs-3 nopadding">
												<div class="hide">
													<input type="hidden" name="billing_country" value="92">
													<input type="hidden" name="billing_city" value="test">
													<input type="hidden" name="billing_zipcode" value="test">
													<input type="hidden" name="billing_address_1" value="test">

												</div>
												<select name="country_code" class="newslterinput nputbrd _numeric_only" id="after_country_code" required>
											<?php echo diaplay_phonecode($phone_code,$active_data, $user_country_code); ?>
										</select> 
											</div>
											<div class="col-xs-2">
												<div class="sidepo">-</div>
											</div>
											<div class="col-xs-7 nopadding">
												<input value="<?=@$lead_pax_details['phone'] == 0 ? '' : @$lead_pax_details['phone'];?>" type="text" name="passenger_contact" id="passenger-contact" placeholder="Mobile Number" class="newslterinput nputbrd _numeric_only" maxlength="10" required="required">
											</div>
                                             </div>

											
											<div class="emailperson col-xs-6 nopad full_smal_forty">
												<input value="<?=@$lead_pax_details['email']?>" type="text" maxlength="80" required="required" id="billing-email" class="newslterinput nputbrd" placeholder="Email" name="billing_email">
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="notese">Your mobile number will be used only for sending flight related communication.</div>
									</div> 
									<div class="clikdiv">
										<div class="squaredThree">
											<input id="terms_cond1" type="checkbox" name="tc" checked="checked" required="required">
											<label for="terms_cond1"></label>
										</div>
										<span class="clikagre" id="clikagre">
										Terms and Conditions
										</span>
									</div>-->
										<div class="clearfix"></div>
									<div class="sepertr"></div>
									<div class="clearfix"></div>
									<!-- Dyanamic Baggage&Meals Section Starts -->
									<?php
									// debug($extra_services);exit;
										if(valid_array($extra_services) == true){
											if(isset($extra_services['ExtraServiceDetails']['Baggage'])){
												$baggage_meal_seat_details['baggage_meal_details']['Baggage'] = $extra_services['ExtraServiceDetails']['Baggage'];
											}
											if(isset($extra_services['ExtraServiceDetails']['Meals'])){
												$baggage_meal_seat_details['baggage_meal_details']['Meals'] = $extra_services['ExtraServiceDetails']['Meals'];
											}
											if(isset($extra_services['ExtraServiceDetails']['Seat'])){
												$baggage_meal_seat_details['baggage_meal_details']['Seat'] = $extra_services['ExtraServiceDetails']['Seat'];
											}
											$baggage_meal_seat_details['total_adult_count'] = $total_adult_count;
											$baggage_meal_seat_details['total_child_count'] = $total_child_count;
											$baggage_meal_seat_details['total_infant_count'] = $total_infant_count;
											$baggage_meal_seat_details['total_pax_count'] = $total_pax_count;
											echo $GLOBALS['CI']->template->isolated_view('flight/dynamic_baggage_meal_seat_details',$baggage_meal_seat_details);
										}
									?>
									<!-- Dyanamic Baggage&Meals Section Ends -->
									<!-- Seats&Meals Preference Section Starts -->
									<?php
										if(valid_array($extra_services) == true){
											if(isset($extra_services['ExtraServiceDetails']['MealPreference'])){
												$seat_meal_preference_details['seat_meal_preference_details']['MealPreference'] = $extra_services['ExtraServiceDetails']['MealPreference'];
											}
											if(isset($extra_services['ExtraServiceDetails']['SeatPreference'])){
												$seat_meal_preference_details['seat_meal_preference_details']['SeatPreference'] = $extra_services['ExtraServiceDetails']['SeatPreference'];
											}
											$seat_meal_preference_details['total_adult_count'] = $total_adult_count;
											$seat_meal_preference_details['total_child_count'] = $total_child_count;
											$seat_meal_preference_details['total_infant_count'] = $total_infant_count;
											$seat_meal_preference_details['total_pax_count'] = $total_pax_count;
											echo $GLOBALS['CI']->template->isolated_view('flight/seat_meal_preference_details',$seat_meal_preference_details);
										}
									?>
									<!-- Seats&Meals Preference Section Ends -->
									<div class="clearfix"></div>
									<div class="loginspld">
										<div class="collogg">
											<?php
												//If single payment option then hide selection and select by default
												if (count($active_payment_options) == 1) {
													$payment_option_visibility = 'hide';
													$default_payment_option = 'checked="checked"';
												} else {
													$payment_option_visibility = 'show';
													$default_payment_option = '';
												}
												
												?>
											<div class="row <?=$payment_option_visibility?>">
												<?php if (in_array(PAY_NOW, $active_payment_options)) {?>
												<div class="col-md-3">
													<div class="form-group">
														<label for="payment-mode-<?=PAY_NOW?>">
														<input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_NOW?>" id="payment-mode-<?=PAY_NOW?>" class="form-control b-r-0" placeholder="Payment Mode">
														Pay Now
														</label>
													</div>
												</div>
												<?php } ?>
												<?php if (in_array(PAY_AT_BANK, $active_payment_options)) {?>
												<div class="col-md-3">
													<div class="form-group">
														<label for="payment-mode-<?=PAY_AT_BANK?>">
														<input <?=$default_payment_option?> name="payment_method" type="radio" required="required" value="<?=PAY_AT_BANK?>" id="payment-mode-<?=PAY_AT_BANK?>" class="form-control b-r-0" placeholder="Payment Mode">
														Pay At Bank
														</label>
													</div>
												</div>
												<?php } ?>
											</div>
											<div class="continye col-sm-4 col-xs-6 nopad">
												<button type="submit" id="make-payment-btn" name="flight"  value="continue" class="bookcont make-payment-btn continue_booking_button">Continue</button>
											</div>
											<div class="clearfix"></div>
											<div class="sepertr"></div>
											<!--<div class="temsandcndtn">
												Most countries require travellers to have a passport valid for more than 3 to 6 months from the date of entry into or exit from the country. Please check the exact rules for your destination country before completing the booking.
											</div> -->
										</div>
									</div>
								</div>
							</form>
<span class="hide">
<input type="hidden" id="pri_passport_min_exp" value="<?=$passport_minimum_expiry_date?>">
</span>

<?php echo $GLOBALS['CI']->template->isolated_view('share/flight_session_expiry_popup');?>
<?php echo $GLOBALS['CI']->template->isolated_view('share/passenger_confirm_popup');?>

<?php
/*
 * Jaganath
 * Flight segment details
 * Outer summary and Inner Summary
 */
function flight_segment_details($SegmentDetails, $SegmentSummary)
{
	$loc_dir_icon = '<span class="fadr fa fa-long-arrow-right textcntr"></span>';
	$inner_summary = $outer_summary = '';
	//Inner Summary
	foreach($SegmentDetails as $__segment_k => $__segment_v) {
		$segment_summary = $SegmentSummary[$__segment_k];
		//Calculate Total Duration of Onward/Return Journey
		$inner_summary .= '<div class="ontyp">';
		//Way Summary in one line - Start
		$inner_summary .= '<div class="labltowr arimobold">';
		$inner_summary .= $segment_summary['OriginDetails']['CityName'].' to '.$segment_summary['DestinationDetails']['CityName'];
		$inner_summary .= '</div>';
		//Way Summary in one line - End
		foreach ($__segment_v as $__stop => $__segment_flight) {
			//Summary of Way - Start
			$inner_summary .= '<div class="allboxflt">';
				//airline
				$inner_summary .= '<div class="col-xs-3 nopadding width_adjst">
									<div class="jetimg">
									<img  alt="'.$__segment_flight['AirlineDetails']['AirlineCode'].'" src="'.Base_URL.'/extras/system/library/images/airline_logo/'.$__segment_flight['AirlineDetails']['AirlineCode'].'.gif" >
									</div>
									<div class="alldiscrpo">
									'.$__segment_flight['AirlineDetails']['AirlineName'].'
									<span class="sgsmal">'.$__segment_flight['AirlineDetails']['AirlineCode'].' 
									'.$__segment_flight['AirlineDetails']['FlightNumber'].'</span>
									</div>
								  </div>';
				//depart
				$inner_summary .= '<div class="col-xs-7 nopadding width_adjst">';
				$inner_summary .= '<div class="col-xs-6">
									<span class="airlblxl">'.($__segment_flight['OriginDetails']['DateTime']).'</span>
									<span class="portnme">'.$__segment_flight['OriginDetails']['AirportName'].'</span>
									</div>';
				//direction indicator
				// $inner_summary .= '<div class="col-xs-2">
				// '.$loc_dir_icon.'</div>';
				//arrival
				$inner_summary .= '<div class="col-xs-6">
									<span class="airlblxl">'.($__segment_flight['DestinationDetails']['DateTime']).'</span>
									<span class="portnme">'.$__segment_flight['DestinationDetails']['AirportName'].'</span>
									</div>';
				$inner_summary .= '</div>';
					
				//Between Content -----
				$inner_summary .= '<div class="col-xs-2 nopadding width_adjst">
								<span class="portnme textcntr">'.$__segment_flight['SegmentDuration'].'</span>
								<span class="portnme textcntr">Stop : '.($__stop).'</span>
								</div>';
				//Summary of Way - End
				$inner_summary .= '</div>';
			if (isset($__segment_v['WaitingTime']) == true) {
				$next_seg_info = $seg_v[$seg_details_k+1];
				$waiting_time = $__segment_v['WaitingTime'];
				$inner_summary .= '
			<div class="clearfix"></div>
			<div class="connectnflt">
				<div class="conctncentr">
				<span class="fa fa-plane"></span>Change of planes at '.$next_seg_info['OriginDetails']['AirportName'].' | <span class="fa fa-clock-o"></span> Waiting : '.$waiting_time.'
			</div>
			</div>
			<div class="clearfix"></div>';
			}
		}
		$inner_summary .= '</div>';
	}
	//Outer Summry
	$total_stop_count = 0;
	$outer_summary .='<div class="moreflt spltopbk">';
	foreach ($SegmentSummary as $__segment_k => $__segment_v) {
		$total_segment_travel_duration = $__segment_v['TotalDuaration'];
		$__stop_count = $__segment_v['TotalStops'];
		$total_stop_count	+= $__stop_count;
		$outer_summary .= '<div class="ontypsec">
						<div class="allboxflt">';
		//airline
		$outer_summary .= '<div class="col-xs-3 nopadding width_adjst">
							<div class="jetimg">
							<img class="airline-logo" alt="'.$__segment_v['AirlineDetails']['AirlineCode'].'" src="'.Base_URL.'/extras/system/library/images/airline_logo/'.$__segment_v['AirlineDetails']['AirlineCode'].'.gif">
							</div>
							<div class="alldiscrpo">
									<span class="air_name">'.$__segment_v['AirlineDetails']['AirlineName'].'</span>
									<span class="sgsmal"> '.$__segment_v['AirlineDetails']['AirlineCode'].''.$__segment_v['AirlineDetails']['FlightNumber'].'</span>
							</div>
						  </div>';
		$outer_summary .= '<div class="col-xs-7 nopadding width_adjst">';
		//depart
		$outer_summary .= '<div class="col-xs-6">
									<span class="airlblxl">'.$__segment_v['OriginDetails']['AirportName'].'</span>
									<span class="portnme">'.($__segment_v['OriginDetails']['DateTime']).'</span>
							</div>';
		//direction indicator
		//$outer_summary .= '<div class="col-xs-2"><span class="fadr fa fa-long-arrow-right textcntr"></span></div>';
		//arrival
		$outer_summary .= '<div class="col-xs-6">
									<span class="airlblxl">'.$__segment_v['DestinationDetails']['AirportName'].'</span>
									<span class="portnme">'.($__segment_v['DestinationDetails']['DateTime']).'</span>
								</div>
								</div>';
		//Stops/Class details
		$outer_summary .= '<div class="col-xs-2 nopadding width_adjst">
								<span class="portnme textcntr">'.($total_segment_travel_duration).'</span>
								<span class="portnme textcntr" >Stop:'.($__stop_count).'</span>
						</div>';
		$outer_summary .= '</div></div>';
	}
	$outer_summary .='</div>';
	return array('segment_abstract_details' => $outer_summary, 'segment_full_details' => $inner_summary);
}
function get_fare_summary($FareDetails, $PassengerFareBreakdown, $convenience_fees)
{

	$total_tax = $FareDetails['TotalTax'];
	$flight_total_price = number_format(($FareDetails['TotalFare']+$convenience_fees), 2);
	$currency_symbol = $FareDetails['CurrencySymbol'];
	$currency = $FareDetails['Currency'];
	$fare_summary = '<div class="insiefare">
				<div class="farehd arimobold">Fare Summary</div>
				<div class="fredivs">';
				$pax_base_fare_details = '<div class="kindrest">
							<div class="freshd">Base Fare</div>';
				$pax_tax_details = '<div class="kindrest">
								<div class="freshd">Taxes</div>';
				foreach($PassengerFareBreakdown as $k => $v) {
					$pax_type = $v['PassengerType'];
					$pax_base_fare = $v['BaseFare'];
					$pax_count = $v['Count'];
					$pax_base_fare_details .= '<div class="reptallt">
						<div class="col-xs-8 nopadding">
							<div class="faresty">'.$pax_count.' '.$pax_type.'(s) ‎(1 X '.(@$pax_base_fare/$pax_count).')</div>
						</div>
						<div class="col-xs-4 nopadding">
							<div class="amnter">'.$currency_symbol.' '.$pax_base_fare.' </div>
						</div>
					</div>';
					}
					$pax_tax_details .= '<div class="reptallt">
						<div class="col-xs-8 nopadding">
							<div class="faresty">Taxes & Fees</div>
						</div>
						<div class="col-xs-4 nopadding">
							<div class="amnter arimobold">'.$currency_symbol.' '.$total_tax.' </div>
						</div>
						<div class="col-xs-8 nopadding">
							<div class="faresty">Convenience Fees</div>
						</div>
						<div class="col-xs-4 nopadding">
							<div class="amnter arimobold">'.$currency_symbol.' '.$convenience_fees.' </div>
						</div>
						<div class="col-xs-8 nopadding promo_code_discount hide">
							<div class="faresty">Promo Code Discount</div>
						</div>
						<div class="col-xs-4 nopadding promo_code_discount hide">
							<div class="amnter arimobold promo_discount_val"> </div>
						</div>
					</div>';
					$pax_base_fare_details .= '</div>';
					$pax_tax_details .= '</div>';
					$fare_summary .= $pax_base_fare_details;
					$fare_summary .= $pax_tax_details;
					$fare_summary .='
					<div class="clearfix"></div>
						<div class="reptalltftr">
							<div class="col-xs-6 nopadding">
								<div class="farestybig">Grand Total</div>
							</div>
							<div class="col-xs-6 nopadding ">
								<div class="amnterbig arimobold grandtotal">'.$currency_symbol.' '.$flight_total_price.'  </div>
							</div>
						</div>
					</div>
					</div>';
	return $fare_summary;
}


?>
<script  src="<?=Base_URL?>extras/system/template_list/template_v1/javascript/page_resource/booking_script.js" defer="defer" charset="UTF-8"></script>
<script  src="<?=Base_URL?>extras/system/template_list/template_v3/javascript/page_resource/flight_extra_services.js" defer="defer" charset="UTF-8"></script>
<!--<script  src="/flytripnow/mobile_webservices/extras/system/template_list/template_v3/javascript/page_resource/flight_booking.js" defer="defer" charset="UTF-8"></script> -->

<script type="text/javascript">
	/*
		session time out variables defined
	*/
	var  search_session_expiry = "<?php echo $GLOBALS ['CI']->config->item ( 'flight_search_session_expiry_period' ); ?>";
	var search_session_alert_expiry = "<?php echo $GLOBALS ['CI']->config->item ( 'flight_search_session_expiry_alert_period' ); ?>";
	var search_hash = "<?php echo $session_expiry_details['search_hash']; ?>";
	var start_time = "<?php echo $session_expiry_details['session_start_time']; ?>";
	var session_time_out_function_call = 1; 
</script>
