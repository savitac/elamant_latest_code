
<?php 
$booking_details = $data['booking_details'][0]; 
$from_airport_name = $this->custom_db->single_table_records('flight_airport_list','airport_city',array('airport_code'=>$booking_details['journey_from']));
$to_airport_name = $this->custom_db->single_table_records('flight_airport_list','airport_city',array('airport_code'=>$booking_details['journey_to']));
?>

<table style="border-collapse: collapse; background: #ffffff;font-size: 12pt; margin: 0 auto; font-family: arial;" width="100%" cellpadding="0" cellspacing="0" border="0">
  <tbody>
    
    <!--Logo and confirmation-->
    <tr>
      <td style="padding: 10px;">
        <table cellpadding="5" cellspacing="0" border="0" width="100%" style="border-collapse: collapse;">
          <tbody>
            <tr>
              <td align="center" style="-webkit-text-size-adjust: none;border-collapse: collapse; padding:0 10px;">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0;mso-table-rspace: 0">
                  <tbody>
                    <tr>
                          <td bgcolor="#f6f6f6" colspan="2" align="center" style="-webkit-text-size-adjust: none;border-collapse: collapse; padding:0 10px;"><img width="150" alt="" style="display: block;outline: none;text-decoration: none;border: none;-ms-interpolation-mode: bicubic" src="<?= $GLOBALS['CI']->template->domain_images($GLOBALS['CI']->template->get_domain_logo());?>" class="CToWUd"/></td>
                    </tr>
                    <tr>
                      <td bgcolor="#f6f6f6">
                      
                  <table width="100%" cellpadding="5" style="padding: 10px 20px;font-size: 11pt;">
                          <tbody>
                            <tr>
                              <td valign="top" height="40" align="left" style="color: #2b303d;font-weight: bold;line-height: 22px;font-size: 22px;text-decoration: none;vertical-align: middle;-webkit-text-size-adjust: none;border-collapse: collapse;padding:0 10px;"> Booking</td>
                            </tr>
                            <tr>
                              <td style="text-align:left;color: #666;font-size: 14px;padding: 0 10px;"> Booking No :<strong> <?php echo $booking_details['pnr'];?> </strong></td>
                            </tr>
                            <tr>
                            <td style="text-align:left;color: #666;font-size: 14px;padding: 0 10px;" > Booking Status  :<strong
                          class=""  >
                      <?php
                      switch (@$booking_details['status']) {
                        case 'BOOKING_CONFIRMED' :
                          echo 'CONFIRMED';
                          break;
                        case 'BOOKING_CANCELLED' :
                          echo 'CANCELLED';
                          break;
                        case 'BOOKING_FAILED' :
                          echo 'FAILED';
                          break;
                        case 'BOOKING_INPROGRESS' :
                          echo 'INPROGRESS';
                          break;
                        case 'BOOKING_INCOMPLETE' :
                          echo 'INCOMPLETE';
                          break;
                        case 'BOOKING_HOLD' :
                          echo 'HOLD';
                          break;
                        case 'BOOKING_PENDING' :
                          echo 'PENDING';
                          break;
                        case 'BOOKING_ERROR' :
                          echo 'ERROR';
                          break;
                      }
                      
                      ?>
                      </strong></td></tr>
                            <tr>
                              <td style="text-align:left;color: #666;font-size: 14px;padding: 0 10px;"> Booking Date :<strong> <?= str_replace('-', ' ', $booking_details['booked_date']);?> </strong></td>
                            </tr>
                          </tbody>
                        </table></td>
                        <td bgcolor="#f6f6f6">
                      <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0;mso-table-rspace: 0">
                          <tbody>
                        <tr>
                        <td  style="color: #666;font-size: 14px;border-collapse: collapse;padding:10px;" align="right"></td>
                       </tr>
                        </tbody>
                        </table>
                        </td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr>
      <td height="20" style="line-height: 3px;font-size: 3px;-webkit-text-size-adjust: none;border-collapse: collapse"></td>
    </tr>
          </tbody>
        </table></td>
    </tr>
    
    <!--Logo and confirmation end--> 
    
  
    
    <!--Itinerary content -->
    <tr>
      <td bgcolor="#ffffff" align="center" style="-webkit-text-size-adjust: none;border-collapse: collapse"><table width="600" cellspacing="0" cellpadding="0" border="0" style="border:1px solid #ddd; border-bottom:none; border-top:none;border-collapse: collapse;mso-table-lspace: 0;mso-table-rspace: 0">
          <tbody>
          <tr>
      <td height="20" style="line-height: 3px;font-size: 3px;-webkit-text-size-adjust: none;border-collapse: collapse"></td>
    </tr>
            <tr>
              <td style="text-align:left;padding: 0 10px;-webkit-text-size-adjust: none;border-collapse: collapse"><table style="width: 100%;border-collapse: collapse;mso-table-lspace: 0;mso-table-rspace: 0" class="insideone">
                  <tr>
                    <td style="width: 33.33%;padding: 5px 0 5px 0;-webkit-text-size-adjust: none;border-collapse: collapse">To </td>
                    <td style="color: #333;padding: 5px 0 5px 0;-webkit-text-size-adjust: none;border-collapse: collapse">: <?php echo $booking_details['lead_pax_name'];?></td>
                  </tr>
                  <tr>
                    <td style="-webkit-text-size-adjust: none;border-collapse: collapse">Order ID </td>
                    <td style="color: #333;padding: 5px 0 5px 0;-webkit-text-size-adjust: none;border-collapse: collapse">: <?= $booking_details['app_reference'];?></td>
                  </tr>
                  <tr>
                    <td style="-webkit-text-size-adjust: none;border-collapse: collapse">From Location </td>
                    <td style="color: #333;padding: 5px 0 5px 0;-webkit-text-size-adjust: none;border-collapse: collapse">: <?= @$from_airport_name['data'][0]['airport_city'];?></td>
                  </tr>
                  <tr>
                    <td style="-webkit-text-size-adjust: none;border-collapse: collapse">To Location </td>
                    <td style="color: #333;padding: 5px 0 5px 0;-webkit-text-size-adjust: none;border-collapse: collapse">: <?= @$to_airport_name['data'][0]['airport_city'];?></td>
                  </tr>
                  <tr>
                    <td style="-webkit-text-size-adjust: none;border-collapse: collapse">Date & Time of Travel </td>
                    <td style="color: #333;padding: 5px 0 5px 0;-webkit-text-size-adjust: none;border-collapse: collapse">: <?= $booking_details['journey_start'];?></td>
                  </tr>
                  <tr>
                    <td style="-webkit-text-size-adjust: none;border-collapse: collapse">Flight Name </td>
                    <td style="color: #333;padding: 5px 0 5px 0;-webkit-text-size-adjust: none;border-collapse: collapse">: <?= $booking_details['booking_itinerary_details'][0]['airline_name'];?></td>
                  </tr>
                  <tr>
                    <td style="-webkit-text-size-adjust: none;border-collapse: collapse">Total Amount </td>
                    <td style="color: #333;padding: 5px 0 5px 0;-webkit-text-size-adjust: none;border-collapse: collapse">: <?= 'Rs  '.$booking_details['grand_total'];?></td>
                  </tr>
                </table></td>
            </tr>
            <tr>
      <td height="20" style="line-height: 3px;font-size: 3px;-webkit-text-size-adjust: none;border-collapse: collapse"></td>
    </tr>
          </tbody>
        </table></td>
    </tr>
   
    
    <tr>
      <td bgcolor="#ffffff" align="center" style="-webkit-text-size-adjust: none;border-collapse: collapse">
      <table bgcolor="#2b303d" width="600" cellspacing="0" cellpadding="0" border="0" style="border:1px solid #ddd; border-top:none;border-collapse: collapse;mso-table-lspace: 0;mso-table-rspace: 0">
          <tbody>
            <tr>
              <td align="center" style="-webkit-text-size-adjust: none;border-collapse: collapse"><table width="600" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0;mso-table-rspace: 0">
                  <tbody>
                    <tr>
                      <td style="padding:0 10px;"><table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0;mso-table-rspace: 0">
                          <tr>
                            <td align="center" style="padding: 10px 5px;background-color: #2b303d;font-size: 13px;color: #fff;line-height: 16px;-webkit-text-size-adjust: none;border-collapse: collapse">© <?= date('Y');?> Simplyjounrney.co.in All Rights Reserved </td>
                          </tr>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
