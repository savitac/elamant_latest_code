<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// echo "test"; exit();
/**
 *
 * @package    Provab
 * @subpackage General
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */
// echo "sadasd"; exit();
class General extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		$this->load->library('Api_Interface');

		$this->load->model('Package_Model');
		$this->load->model('custom_db');
		$this->load->model('flight_model');
		$this->load->model('hotel_model');
		$this->load->model('user_model');
		$this->load->library('booking_data_formatter');
	}


	function index()
	{
		$data = array('status'=>0,"message"=>"Invalid URL");
		echo json_encode($data);
		exit;
	}

	public function ooops()
	{
		$this->template->view('utilities/404.php');
	}

	function Authentication()
	{
		$post_data = $this->input->post('auth_data');
		//debug($post_data);exit;
		$var = json_decode($post_data);		
		$mobile_auth = json_decode(json_encode($var), True);
		
		$flight_url = $this->config->item('flight_url').'Authenticate/';
		if(!empty($mobile_auth['system']) && !empty($mobile_auth['domainkey']) && !empty($mobile_auth['username']) && !empty($mobile_auth['password'])){
			$auth_key = $this->connect_server($flight_url,$mobile_auth);
			if(valid_array($auth_key))
			{
				echo json_encode($auth_key);
			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] ="Unable Fetch data";
				$data['data']  = '';
				echo json_encode($data);
			}
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Required fields Are Missing";
			$data['data'] = '';
			echo json_encode($data);
		}
		
		
	}

	public function connect_server($flight_url,$mobile_auth)
	{
		$request ='';
		$header_info['system'] = $mobile_auth['system'];
		$header_info['DomainKey'] = $mobile_auth['domainkey'];
		$header_info['UserName'] = $mobile_auth['username'];
		$header_info['Password'] = $mobile_auth['password'];

		$url = $flight_url;
		$data = $this->api_interface->get_json_response($url, $request, $header_info);
		return $data;
	}

	function pre_flight_search_mobile($search_id='')
	{ 
		// echo "flight"; exit("exit");
		$var = $this->input->post('flight_search');
		$user_type = $this->input->post('user_type');
		$user_id = !empty($this->input->post('user_id')) ? $this->input->post('user_id') : 0;

		$mobile_search = json_decode($var,true);	
		//echo "<pre>";print_r($mobile_search);exit();
		foreach ($mobile_search['Segments'] as $seg) {
			
			$from1 	= $seg['Origin'];
			$from[] 	= $seg['Origin'];
			$from_id = $GLOBALS ['CI']->custom_db->single_table_records ( 'flight_airport_list', 'origin', array ('airport_code' => $from1));
			$from_loc_id[] = $from_id['data'][0]['origin'];
			/*	$from_name = $GLOBALS ['CI']->custom_db->single_table_records ( 'flight_airport_list', 'airport_city', array ('airport_code' => $from1));
			$from_city_name = $from_name['data'][0]['airport_city'];
				*/
			$to1 	= $seg['Destination'];
			$to[] 	= $seg['Destination'];
			$to_id = $GLOBALS ['CI']->custom_db->single_table_records ( 'flight_airport_list', 'origin', array ('airport_code' => $to1));
			$to_loc_id[] = $to_id['data'][0]['origin'];
			/*	$to_name = $GLOBALS ['CI']->custom_db->single_table_records ( 'flight_airport_list', 'airport_city', array ('airport_code' => $to1));
			$to_city_name = $to_name['data'][0]['airport_city'];
			*/
			$v_class 	= @$seg['CabinClass'];
			$depature[] 	=$seg['DepartureDate'];
			if(isset($seg['return_date'])){
				$return 	= $seg['return_date'];
			}else{
				$return = '';
			}
				

		}
		// debug($mobile_search); exit();
		if($mobile_search['JourneyType'] == 'oneway' || $mobile_search['JourneyType'] == 'circle'){
			$search_params = array(
			'carrier'  		=>	array($mobile_search['PreferredAirlines']),
			'from'			=> 	$from[0],
			'from_loc_id'	=>	$from_loc_id[0],
			'to'			=> 	$to[0],
			'to_loc_id'		=> 	$to_loc_id[0],
			'v_class'		=> 	$v_class,
			'depature'		=> 	$depature[0],
			'return'		=>	$return,
			'adult'			=> 	$mobile_search['AdultCount'],
			'child'			=> 	$mobile_search['ChildCount'],
			'infant'		=> 	$mobile_search['InfantCount'],
			 'ApiToken' =>rand(0,1000),
			'trip_type'		=>  $mobile_search['JourneyType']
			);
		}	
		else{
			$search_params = array(
			'carrier'  		=>	array($mobile_search['PreferredAirlines']),
			'from'			=> 	$from,
			'from_loc_id'	=>	$from_loc_id,
			'to'			=> 	$to,
			'to_loc_id'		=> 	$to_loc_id,
			'v_class'		=> 	$v_class,
			'depature'		=> 	$depature,
			'adult'			=> 	$mobile_search['AdultCount'],
			'child'			=> 	$mobile_search['ChildCount'],
			'infant'		=> 	$mobile_search['InfantCount'],
			'ApiToken' =>rand(0,1000),
			'trip_type'		=>  $mobile_search['JourneyType']
			);	
		}
		

		$search_id = $this->save_pre_search_mobile(META_AIRLINE_COURSE,$search_params);
		
		$Token_Id = $search_params['ApiToken'];

		$this->flight_model->save_search_data($search_params, META_AIRLINE_COURSE);
		// /echo "sadasd"; exit();
		// debug($Token_Id); die;
	
		$flight_search_mobile = self::search_mobile($search_id, $Token_Id, $user_type, $user_id);
		// debug($search_id); exit();

		if($flight_search_mobile)
		{
			echo json_encode($flight_search_mobile);	
		}else{
			$data = array('status'=>0,'message'=>'Unable to fetch data');
			echo json_encode($data);
		}
		
	}
	/* Added by badri */
	function search_mobile($search_id, $Token_Id, $user_type, $user_id = 0)
	{
		error_reporting(0);
		$safe_search_data = $this->flight_model->get_safe_search_data($search_id);
		// Get all the FLIGHT bookings source which are active
		$active_booking_source = $this->flight_model->active_booking_source();

		if (valid_array($active_booking_source) == true and $safe_search_data['status'] == true) {
			$safe_search_data['data']['search_id'] = abs($search_id);
			$page_params = array (
				'flight_search_params' => $safe_search_data['data'],
				'active_booking_source' => $active_booking_source 
				);
			$page_params['from_currency'] = get_application_default_currency();
			$page_params['to_currency'] = get_application_currency_preference();

			//Need to check if its domestic travel
			$from_loc = $safe_search_data['data']['from_loc'];
			$to_loc = $safe_search_data['data']['to_loc'];
			$page_params['is_domestic_one_way_flight'] = false;
			if ($safe_search_data['data']['trip_type'] == 'oneway') {
				$page_params['is_domestic_one_way_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);
			}

			$page_params['airline_list'] = $this->db_cache_api->get_airline_code_list();
			$response['data'] = '';
			$response['msg'] = '';
			$response['status'] = FAILURE_STATUS;
			$search_params['search_id'] = $search_id;
			$search_params['op'] = 'load';
			$search_params['booking_source'] = PROVAB_FLIGHT_BOOKING_SOURCE;

			$page_params['search_id'] = $search_params['search_id'];

			if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
				load_flight_lib($search_params['booking_source']);

				switch($search_params['booking_source']) { //get_application_default_currency
					case PROVAB_FLIGHT_BOOKING_SOURCE :
						//Converting API currency data to preferred currency
						/*$currency_obj = new Currency(array('module_type' => 'flight','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
                                                 
						$raw_search_result = $this->flight_lib->search_data_in_preferred_currency($raw_search_result, $currency_obj);*/
						
					$currency_obj = new Currency(array('module_type' => 'flight','from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
					//echo "<pre>";print_r($currency_obj);exit();

					$search_file_data = $this->flight_lib->search_data(($search_params['search_id']));
					$search_file_key = 	$search_file_data['search_hash'];

					// create cache file for formated data
					$this->CI = &get_instance();
					$this->CI->load->driver('cache');

					$cache_search = $this->CI->config->item('cache_flight_search');
					$search_hash = $search_file_key.'_formated';
					$search_hash_flight = $search_file_key.'_flight_details';

					if ($cache_search) {
						$cache_contents = $this->CI->cache->file->get($search_hash);
					}
					#$cache_contents='';			
					if ($cache_search === false || ($cache_search === true && empty($cache_contents) == true)){

						$raw_flight_list = $this->flight_lib->get_flight_list_mobile(abs($search_params['search_id']),$Token_Id);

						if(valid_array($raw_flight_list['data'])){
							$raw_search_result = $raw_flight_list['data']['Search']['FlightDataList'];
							
							/*$this->load->library('common_flight');
							$raw_flight_list ['data'] = $this->common_flight->update_markup_and_insert_cache_key_to_token ( $raw_flight_list ['data'], $carry_cache_key, $search_id );	
							*/
								
							$raw_search_result = $this->flight_lib->search_data_in_preferred_currency($raw_search_result, $currency_obj);
							$user_type = !empty($user_type) ? $user_type : 'b2c';
							//markup is adding here

							$formatted_search_data = $this->flight_lib->format_search_response($raw_search_result, $currency_obj, $search_params['search_id'], $user_type, $user_id);
							//debug($formatted_search_data); die;
							$FlightsSegmentDetails = $formatted_search_data['FlightsSegmentDetails'];

							unset($formatted_search_data['FlightsSegmentDetails']);
							$cache_exp = $this->CI->config->item('cache_flight_search_ttl');
							
							$this->CI->cache->file->save($search_hash, $formatted_search_data, $cache_exp);
							$this->CI->cache->file->save($search_hash_flight,$FlightsSegmentDetails , $cache_exp);
						}

					} else {
						//read from cache
						$formatted_search_data = $cache_contents;
					}

					//debug($active_booking_source); exit();
					if(valid_array($formatted_search_data['data'])){
						
						$format_mobile_data = $formatted_search_data['data'];
						$format_mobile_data['airline_list'] = $formatted_search_data['airline_list'];
						$format_mobile_data['price'] = $formatted_search_data['price'];
						$route_count = count($formatted_search_data['data']['Flights']);
						$domestic_round_way_flight = $formatted_search_data['data']['JourneySummary']['IsDomesticRoundway'];
						if (($route_count > 0  && $domestic_round_way_flight == false) || ($route_count == 2 && $domestic_round_way_flight == true)) {
							$format_mobile_data['IsDomestic'] = $formatted_search_data['data']['JourneySummary']['IsDomestic'];
							$response['domestic_round_way_flight'] = $domestic_round_way_flight;
							$format_mobile_data['search_id'] = $search_params['search_id'];
							$format_mobile_data['booking_url']  = $formatted_search_data['booking_url'];
							$format_mobile_data['booking_source'] = $search_params['booking_source'];
							$format_mobile_data['trip_type']  = $this->flight_lib->master_search_data['trip_type'];
							$format_mobile_data['airline_img_url']  = SYSTEM_IMAGE_DIR . 'airline_logo/' . @$fli_det['operator_code'];
							$format_mobile_data['cache_file_name']  = $formatted_search_data['cache_file_name'];
						}

					}else{
						$format_mobile_data = array('status'=>0,
							'message'=>"Unable to fetch the data");
					}
					
				//debug($format_mobile_data);die;
					//echo json_encode($format_mobile_data);
					$this->output_compressed_data($format_mobile_data); 
					exit;
				}
			}
		} else {
			return false;
		}
	}

	function flight_details_mobile(){
		$post_data = $this->input->post();
		$search_file_key = $post_data['search_key'];
		// create cache file for formated data
		$this->CI = &get_instance();
		$this->CI->load->driver('cache');
		//debug($search_file_key);die;
		$cache_search = $this->CI->config->item('cache_flight_search');

		$search_hash_flight = $search_file_key.'_flight_details';
		if ($cache_search) {
			$cache_flight_details = $this->CI->cache->file->get($search_hash_flight);
		}
		echo json_encode($cache_flight_details); 
		exit;
	}

	private function save_pre_search_mobile($search_type,$search_params)
	{	
		//Save data
		$search_data = json_encode($search_params);
		//debug($search_data); exit();
		$insert_id = $this->custom_db->insert_record('search_history', array('search_type' => $search_type, 'search_data' => $search_data, 'created_datetime' => date('Y-m-d H:i:s')));
		return $insert_id['insert_id'];
	}
	/**********      Hotel Search API   ***********/
	
	function pre_hotel_search_mobile()
	{
		error_reporting(0);
		//$hotel_data = '{"EndUserIp":"127.0.0.1","TokenId":"f82aa408-f0a7-4c48-87b4-50647742d92b","BookingMode":"5","CheckInDate":"25-02-2017","CheckOutDate":"26-02-2017","NoOfNights":1,"CountryCode":"IN","CityId":"10438","PreferredCurrency":"INR","GuestNationality":"IN","NoOfRooms":1,"PreferredHotel":"","MinRating":0,"MaxRating":5,"IsNearBySearchAllowed":true,"SortBy":0,"OrderBy":0,"ResultCount":0,"ReviewScore":0,"RoomGuests":[{"NoOfAdults":1,"NoOfChild":0}],"ChildAge":[]}';
		$hotel_data = $this->input->post('hotel_search');
		$user_type = $this->input->post('user_type');
		$user_id = !empty($this->input->post('user_id')) ? $this->input->post('user_id') : 0; 
		if(empty($hotel_data))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to search";
			$this->output_compressed_data($data);
			exit;
		}
		
		$hotel_search_para = json_decode($hotel_data);
		$hotel_search_p = json_decode(json_encode($hotel_search_para), True);
		$hotel_search_room= $hotel_search_p['RoomGuests'];
	
		$CheckInDate = $hotel_search_p['CheckInDate'];
		$CheckOutDate = $hotel_search_p['CheckOutDate'];
	
		if(valid_array($hotel_search_p)){			
			$country_city_name_array 	= $from_id = $GLOBALS ['CI']->custom_db->single_table_records ( 'all_api_city_master', 'city_name,country_name', array ('origin' => $hotel_search_p['CityId']));
			$city_name 			= $country_city_name_array['data'][0]['city_name'];
					$country_name 		= $country_city_name_array['data'][0]['country_name'];
			$city 				=  $city_name."(".$country_name.")";

			$adult = array();
			$child = array();
			
			$search_params = array();
			foreach ($hotel_search_p['RoomGuests'] as $pax_info) {
				$adult [] =$pax_info['NoOfAdults'];
				$child [] = $child_count = $pax_info['NoOfChild'];
			}
			
			// exit;
			
			$search_params = array(
				'city'					=>  $city,
				'hotel_destination'		=>  $hotel_search_p['CityId'],
				'hotel_checkin'			=>	$CheckInDate,
				'hotel_checkout'		=>	$CheckOutDate,
				'rooms'					=>	$hotel_search_p['NoOfRooms'],
				'adult'					=> 	$adult,
				'child'					=>  $child,
			);
			$i = 1;
			foreach ($hotel_search_p['RoomGuests'] as $pax_info) {
				$child_count = $pax_info['NoOfChild'];
				$search_params['childAge_'.$i] = @$pax_info['childAge_'.$i];
				$i++;
			}
			 //debug($search_params);exit;
		    // debug($hotel_data);exit;
		
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Requested field is missing";
			$this->output_compressed_data($data);
			exit;
		}
		// debug($search_params);exit;
		$search_id = $this->save_pre_search_hotel_mobile(META_ACCOMODATION_COURSE,$search_params);
		//debug($search_id); exit();
		$this->load->model('hotel_model');
		$this->hotel_model->save_search_data($search_params, META_ACCOMODATION_COURSE);
		$hotel_search_mobile = self::search_hotel_mobile($search_id, $hotel_search_room, $user_type, $user_id);
		//debug($hotel_search_mobile); exit();
		if($hotel_search_mobile) {
			$this->output_compressed_data($hotel_search_mobile);	
		} else {
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Unable to fetch data";
			$this->output_compressed_data($data);
			exit;
		}
		
	}


	function search_hotel_mobile($search_id, $hotel_search_room, $user_type = 'b2c', $user_id = 0)
	{
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);

		$safe_search_data['status'] = '1';
		// Get all the hotels bookings source which are active
		$active_booking_source = $this->hotel_model->active_booking_source();
		//print_r($search_id);exit();
		if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
			$safe_search_data['data']['search_id'] = abs($search_id);
			$response['data'] = '';
			$response['msg'] = '';
			$response['status'] = FAILURE_STATUS;			
		//	$limit = $this->config->item('hotel_per_page_limit');
			$limit = '10000';
			$search_params['op'] = 'load';
			$search_params['booking_source'] = PROVAB_HOTEL_BOOKING_SOURCE;
			$search_params['search_id'] = $search_id;
			//debug($search_params); exit();
			if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
				
				load_hotel_lib($search_params['booking_source']);
				switch($search_params['booking_source']) {
					case PROVAB_HOTEL_BOOKING_SOURCE :
					$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
						//Meaning hotels are loaded first time
						//debug($currency_obj);exit;
					$raw_hotel_list = $this->hotel_lib->get_hotel_list(abs($search_params['search_id']));
					//debug($raw_hotel_list); exit();
					if ($raw_hotel_list['status']) {
							//Update currency 
						/* $hlist = $raw_hotel_list['data']['Search']['HotelSearchResult']['HotelResults'];
					
						foreach($hlist as $hl)
							
						{
							$price[] = $hl['Price']['RoomPrice'];
						}
						//debug($price);exit;
						$price = $price;
						$min_price = min($price);
						$max_price = max($price);
						
						$raw_hotel_list['Price_filter'] = array('min_price'=>$min_price,'max_price'=>$max_price);*/

						$raw_hotel_list['data'] = $this->hotel_lib->format_search_response($raw_hotel_list['data'], $currency_obj, $search_params['search_id'], $user_type, $filters);
						// debug($raw_hotel_list['data']); exit();
						/*	$source_result_count = $raw_hotel_list['data']['source_result_count'];
						$filter_result_count = $raw_hotel_list['data']['filter_result_count'];
						if (intval($offset) == 0) {
								//Need filters only if the data is being loaded first time
							$filters = $this->hotel_lib->filter_summary($raw_hotel_list['data']);
							$response['filters'] = $filters['data'];
						}*/
						$raw_hotel_list['search_id']	= $search_id;
						$raw_hotel_list['booking_source']	= $search_params['booking_source']; 

						$raw_hotel_list['data'] = $this->hotel_lib->get_page_data($raw_hotel_list['data'], $offset, $limit);
						//$raw_hotel_list['data']['HotelSearchResult']['HotelResults'] = $raw_hotel_list['data']['HotelSearchResult']['HotelResults'];
						$raw_hotel_list['data']['HotelSearchResult']['CityId'] = $raw_hotel_list['data']['HotelSearchResult']['CityId'];
						$raw_hotel_list['data']['HotelSearchResult']['ResponseStatus'] = 1;
						$raw_hotel_list['data']['HotelSearchResult']['CheckInDate'] = $safe_search_data['data']['from_date'];
						$raw_hotel_list['data']['HotelSearchResult']['CheckOutDate'] = $safe_search_data['data']['to_date'];
						//$raw_hotel_list['data']['HotelSearchResult']['PreferredCurrency'] = $raw_hotel_list['data']['HotelSearchResult']['HotelResults'][0]['Price']['CurrencyCode'];
						$raw_hotel_list['data']['HotelSearchResult']['NoOfRooms'] = $safe_search_data['data']['room_count'];
						$raw_hotel_list['data']['HotelSearchResult']['RoomGuests'] = $hotel_search_room;

						//unset($raw_hotel_list['data']['Search']);
							//debug($raw_hotel_list['data']); exit();
						$attr['search_id'] = abs($search_params['search_id']);
							// debug($raw_hotel_list); exit();
						//echo "hiiii"; 
						$this->output_compressed_data($raw_hotel_list);
						exit();
							/*$response['data'] = get_compressed_output(
							$this->template->isolated_view('hotel/tbo/tbo_search_result',
							array('currency_obj' => $currency_obj, 'raw_hotel_list' => $raw_hotel_list['data'],
										'search_id' => $search_params['search_id'], 'booking_source' => $search_params['booking_source'],
										'attr' => $attr
							)));
							$response['status'] = SUCCESS_STATUS;
							$response['total_result_count'] = $source_result_count;
							$response['filter_result_count'] = $filter_result_count;
							$response['offset'] = $offset+$limit;
						*/
						}
						break;
					}
				}

			//$this->output_compressed_data($response);


			} else {
				echo json_encode("Redirect PopUP");
			}
		}

		function pre_mobile_search_hotel()
		{
			error_reporting(0);

		//Old Hotel Request 
			//$hotel_data = '{"EndUserIp":"127.0.0.1","TokenId":"d2abbc89-f797-4eb6-ac2e-4bd9a776d6a1","BookingMode":"5","CheckInDate":"25-05-2017","CheckOutDate":"26-05-2017","NoOfNights":1,"CountryCode":"IN","CityId":"10409","PreferredCurrency":"INR","GuestNationality":"IN","NoOfRooms":1,"PreferredHotel":"","MinRating":0,"MaxRating":5,"IsNearBySearchAllowed":true,"SortBy":0,"OrderBy":0,"ResultCount":0,"ReviewScore":0,"RoomGuests":[{"NoOfAdults":1,"NoOfChild":0}],"ChildAge":[]}';
		//Single Rooom
			//$hotel_data = '{"EndUserIp":"127.0.0.1","TokenId":"3c20c2a8-b690-448a-a759-6d272e56ab9b","BookingMode":"5","CheckInDate":"25-05-2017","CheckOutDate":"26-05-2017","NoOfNights":1,"CountryCode":"IN","CityId":"10409","PreferredCurrency":"INR","GuestNationality":"IN","NoOfRooms":1,"PreferredHotel":"","MinRating":0,"MaxRating":5,"IsNearBySearchAllowed":true,"SortBy":0,"OrderBy":0,"ResultCount":0,"ReviewScore":0,"RoomGuests":[{"NoOfAdults":1,"NoOfChild":0,"ChildAge":[0]}]}';
		//multiple Rooms
			//$hotel_data = '{"EndUserIp":"127.0.0.1","TokenId":"3c20c2a8-b690-448a-a759-6d272e56ab9b","BookingMode":"5","CheckInDate":"25-05-2017","CheckOutDate":"26-05-2017","NoOfNights":1,"CountryCode":"IN","CityId":"25921","PreferredCurrency":"INR","GuestNationality":"IN","NoOfRooms":2,"PreferredHotel":"","MinRating":0,"MaxRating":5,"IsNearBySearchAllowed":true,"SortBy":0,"OrderBy":0,"ResultCount":0,"ReviewScore":0,"RoomGuests":[{"NoOfAdults":1,"NoOfChild":1,"ChildAge_1":["1"]},{"NoOfAdults":1,"NoOfChild":1,"ChildAge_2":["1"]}]}';

			$hotel_data = $this->input->post('hotel_search');
			if(empty($hotel_data))
			{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Please enter the details to search";
				echo json_encode($data);
				exit;
			}
		//$hotel_data = $hotel_data['hotel_data'];
			$hotel_search_para = json_decode($hotel_data);
			$hotel_search_p = json_decode(json_encode($hotel_search_para), True);
		//debug($hotel_search_p); die;
			$CheckInDate = $hotel_search_p['CheckInDate'];
			$CheckOutDate = $hotel_search_p['CheckOutDate'];
		//echo $CheckInDate; die;
		//$date = DateTime::createFromFormat('d/m/Y', $CheckInDate);
			$CheckInDate = date("Y-m-d", strtotime($CheckInDate));
			$CheckOutDate = date("Y-m-d", strtotime($CheckOutDate));
			if(valid_array($hotel_search_p)) {
				foreach ($hotel_search_p as $hote_cou =>$hotel_search) {

					$city_name_array 	= $from_id = $GLOBALS ['CI']->custom_db->single_table_records ( 'hotels_city', 'city_name', array ('origin' => $hotel_search_p['CityId']));
					$country_name_array = $from_id = $GLOBALS ['CI']->custom_db->single_table_records ( 'hotels_city', 'country_name', array ('origin' => $hotel_search_p['CityId']));
					$city_name 			= $city_name_array['data'][0]['city_name'];
					$country_name 		= $country_name_array['data'][0]['country_name'];
					$city 				=  $city_name."(".$country_name.")";				
				}
				$pax_xou = 1;
				foreach ($hotel_search_p['RoomGuests'] as $pax_info) {

					$adult[]			 = $pax_info['NoOfAdults'];
					$child[]		     = $pax_info['NoOfChild'];
					$search_params['childAge_'.$pax_xou]           = $pax_info['ChildAge_'.$pax_xou];
					$pax_xou ++;	
				}
			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Requested field is missing";
				echo json_encode($data);
				exit;
			}

			$search_params['city'] = $city;
			$search_params['hotel_destination'] = $hotel_search_p['CityId'];
			$search_params['hotel_checkin'] = $CheckInDate;
			$search_params['hotel_checkout'] = $CheckOutDate;
			$search_params['rooms'] = $hotel_search_p['NoOfRooms'];
			$search_params['adult'] = $adult;
			$search_params['child'] = $child;

		//debug($search_params); exit();
			$search_id = $this->save_pre_search_hotel_mobile(META_ACCOMODATION_COURSE,$search_params);
			$this->load->model('hotel_model');
			$this->hotel_model->save_search_data($search_params, META_ACCOMODATION_COURSE);
			$hotel_search_mobile = self::search_hotel_mobile($search_id);
			if($hotel_search_mobile)
			{
				echo json_encode($hotel_search_mobile);	
			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to fetch data";
				echo json_encode($data);
				exit;

			}

		}

		public function save_pre_search_hotel_mobile($search_type,$search_params)
		{

		//Save data
			$search_data = json_encode($search_params);
		//debug($search_data); exit();
			$insert_id = $this->custom_db->insert_record('search_history', array('search_type' => $search_type, 'search_data' => $search_data, 'created_datetime' => date('Y-m-d H:i:s')));
			return $insert_id['insert_id'];
		}

		function get_airport_code_list()
		{
		//$term = "ban";
		$term = $this->input->post('term'); //retrieve the search term that autocomplete sends
		if(empty($term))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to search";
			echo json_encode($data);
			exit;
		}
		$term = trim(strip_tags($term));
		$result = array();

		$__airports = $this->flight_model->get_airport_list($term)->result_array();
		
		if (valid_array($__airports) == false) {
			$__airports = $this->flight_model->get_airport_list('')->result_array();
		}
		$airports = array();
		foreach($__airports as $airport){
			
			$airports['label'] = $airport['airport_city'].', '.$airport['country'].' ('.$airport['airport_code'].')';
			//$airports['value'] = $airport->airport_city.', '.$airport->country.' ('.$airport->airport_code.')';
			$airports['id'] = $airport['origin'];
			$airports['code'] = $airport['airport_code'];

			array_push($result,$airports);
		}
		$results['status'] = SUCCESS_STATUS;
		array_push($result,$results);
		echo (json_encode($result)); 
		exit;
	}

	function get_hotel_city_code_list()
	{
		$this->load->model('hotel_model');
		$term = $this->input->post('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$data_list = $this->hotel_model->get_hotel_city_list($term);
		if (valid_array($data_list) == false) {
			$data_list = $this->hotel_model->get_hotel_city_list('');
		}
		$suggestion_list = array();
		$result = '';
		foreach($data_list as $city_list){
			$suggestion_list['label'] = $city_list['city_name'].', '.$city_list['country_name'].'';
			$suggestion_list['value'] = hotel_suggestion_value($city_list['city_name'], $city_list['country_name']);
			$suggestion_list['id'] = $city_list['origin'];
			
			$result[] = $suggestion_list;
		}
		echo json_encode($result);
		exit;
	}

	function get_all_hotel_cities()
	{
		$this->load->model('hotel_model');
		$data_list = $this->hotel_model->get_all_hotel_city_list('');
		//echo count($data_list); die;
		if (valid_array($data_list) == false) {
			$data_list = $this->hotel_model->get_all_hotel_city_list('');
		}
		$suggestion_list = array();
		$result = '';
		foreach($data_list as $city_list){
			$suggestion_list['label'] = $city_list['city_name'].', '.$city_list['country_name'].'';
			$suggestion_list['value'] = hotel_suggestion_value($city_list['city_name'], $city_list['country_name']);
			$suggestion_list['id'] = $city_list['origin'];
			$result[] = $suggestion_list;
		}
		echo json_encode($result);
	}

	function get_all_airline_cities()
	{

		$this->load->model('flight_model');
		$__airports = $this->flight_model->get_all_flight_city_list('');
		//echo count($data_list); die;
		if (valid_array($__airports) == false) {
			$__airports = $this->hotel_model->get_all_flight_city_list('');
		}
		$airports = array();
		$result = array();
		if(valid_array($__airports)){
			foreach($__airports as $airport){
				
				$airports['label'] = $airport['airport_city'].', '.$airport['country'].' ('.$airport['airport_code'].')';
				//$airports['value'] = $airport->airport_city.', '.$airport->country.' ('.$airport->airport_code.')';
				$airports['id'] = $airport['origin'];
				$airports['code'] = $airport['airport_code'];

				array_push($result,$airports);
			}
			$results['status'] = SUCCESS_STATUS;
		}else{
			$results['status'] = FAILURE_STATUS;
		}
		array_push($result,$results);
		echo (json_encode($result)); 
		exit;
		
	}

	function airline_list()
	{
		$airline = $this->flight_model->all_airline_list();
		$list =""; 
		$air = array();
		if(valid_array($airline)){
			$data['status'] = SUCCESS_STATUS;
			foreach ($airline as $air_key => $air_val) {
				$air['name'] = $air_val['name'];
				$air['code'] = $air_val['code'];
				$air['image'] = SYSTEM_IMAGE_DIR.'airline_logo/'.$air['code'].'.gif';
				$list[] = $air;
			}
			$data['list'] = $list;
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "No Airline Found !!";
			$data['list'] = array();
		}
		
		echo json_encode($data);
		exit;

	}

	function mobile_top_destination_list()
	{
		//error_reporting(E_ALL);
		$image_path = $GLOBALS['CI']->template->domain_images();
	
		$response = array();
		$response['status'] = SUCCESS_STATUS;

		$data['package']['list'] = array();
		$data['hotel']['list'] = array();
		$data['flight']['list'] = array();
		$data['promocode']['list'] = array();
		$data['why_choose_us']['list'] = array();

		//if(is_active_package_module()){
			//Jun 6 2018
			$page_data['holiday_data'] ['india_packages'] = $this->Package_Model->getAllPackages_countrywise ('India');		
			#debug($this->db->last_query()); die;
			$page_data['holiday_data'] ['international_packages'] = $this->Package_Model->getAllPackages_countrywise (@$country);
			//$package_data = $this->Package_Model->get_package_top_destination_mobile();
			if(valid_array($page_data))
			{
				$data['package']['list'] = 	$page_data;
				$data['package']['img_url'] = DOMAIN_PCKG_UPLOAD_DIR;
			}
		//}
		//if (is_active_hotel_module()) {
			$hotel_data = $this->hotel_model->hotel_top_destinations();
			//debug($hotel_data);exit();
			if(valid_array($hotel_data))
			{
				$data['hotel']['list'] = $hotel_data;
				$data['hotel']['img_url'] = $image_path;
			}

			$flight_data = $this->flight_model->flight_top_destinations();
			/*debug($flight_data);exit();*/
			if(valid_array($flight_data))
			{
				//echo "sad"; exit();
				$data['flight']['list'] = $flight_data;
				$data['flight']['img_url'] = $image_path;
			}
			
			$why_choose_us = $this->custom_db->single_table_records('why_choose_us', '*', array('status' => '1'));
			if(valid_array($why_choose_us))
			{
				$data['why_choose_us']['list'] = $why_choose_us;
			}

			$qry = $this->db->query('select * from promo_code_list where status ='.ACTIVE.' and display_home_page="Yes" and expiry_date >="'.date('Y-m-d').'"');
			// echo $this->db->last_query(); exit();
			$get_promo = $qry->result_array();
			// debug($get_promo); exit();
			if(valid_array($get_promo)){
				$data['promocode']['list'] = $get_promo;
				$data['promocode']['img_url'] = DOMAIN_PROMOCODE_UPLOAD_DIR;
			}
		//}
		$response['status'] = SUCCESS_STATUS;
		$response['data'] = $data;
		//echo json_encode($response);
		$this->output_compressed_data($response);
		exit;
	}




	function mobile_hotel_topdestination()
	{
		$image_path = $GLOBALS['CI']->template->domain_images();
		$response = array();
		$response['status'] = SUCCESS_STATUS;
		if (is_active_hotel_module()) {
			$this->load->model('hotel_model');
			$get_data = $this->hotel_model->hotel_top_destinations();
		
			if(valid_array($get_data))
			{
				foreach ($get_data as $key => $value) {

					$data[$key] =$value;
					$data[$key]['image'] = $image_path.$value['image'];
					
				}

				$response['status'] = SUCCESS_STATUS;
				$response['data'] = $data;
				echo json_encode($response);
				exit;
				
			}else{
				$response['status'] = FAILURE_STATUS;
				$response['message'] = "No Details Found";
				echo json_encode($response);
				exit;
			}
			
		}else{
			$response['status'] = FAILURE_STATUS;
			$response['message'] = "You don't have permission to access this URL please conatact our admin";
			echo json_encode($response);
			exit;
		}
	}

	function mobile_hotel_topcities()
	{
		$this->load->model('hotel_model');
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$data_list = $this->hotel_model->get_hotel_city_list($term);
		if (valid_array($data_list) == false) {
			$data_list = $this->hotel_model->get_hotel_city_list('');
		}
		$suggestion_list = array();
		$result = '';
		foreach($data_list as $city_list){
			$suggestion_list['label'] = $city_list['city_name'].', '.$city_list['country_name'].'';
			$suggestion_list['value'] = hotel_suggestion_value($city_list['city_name'], $city_list['country_name']);
			$suggestion_list['id'] = $city_list['origin'];
			if (empty($city_list['top_destination']) == false) {
				$suggestion_list['category'] = 'Top cities';
			}
			$result[] = $suggestion_list;
		}
		$this->output_compressed_data($result);
	}

	function mobile_airline_topcities()
	{
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$result = array();

		$__airports = $this->flight_model->get_airport_list($term)->result();
		if (valid_array($__airports) == false) {
			$__airports = $this->flight_model->get_airport_list('')->result();
		}
		$airports = array();
		foreach($__airports as $airport){
			$airports['label'] = $airport->airport_city.', '.$airport->country.' ('.$airport->airport_code.')';
			$airports['id'] = $airport->origin;
			$airports['code'] = $airport->airport_code;
			if (empty($airport->top_destination) == false) {
				$airports['category'] = 'Top cities';
			} 
			array_push($result,$airports);
		}
		$this->output_compressed_data($result);
	}

	/**
	 * Compress and output data
	 * @param array $data
	 */
	private function output_compressed_data($data)
	{
		while (ob_get_level() > 0) { ob_end_clean() ; }
		ob_start("ob_gzhandler");
		header('Content-type:application/json');
		echo json_encode($data);
		ob_end_flush();
		exit;
	}

	function get_all_bus_cities()
	{
		$this->load->model('bus_model');
		$term = $this->input->get('term'); //retrieve the search term that autocomplete sends
		$term = trim(strip_tags($term));
		$data_list = $this->custom_db->single_table_records('bus_stations','origin as id,station_id as Station_id, name as Station_name');
		

		$suggestion_list = array();
		$result = '';
		if($data_list['status'] == SUCCESS_STATUS && valid_array($data_list['data'])){
			
			foreach($data_list['data'] as $city_list){
				$suggestion_list['label'] = $city_list['Station_name'];
				$suggestion_list['value'] = $city_list['Station_name'];
				$suggestion_list['id'] = $city_list['id'];
				
				$result[] = $suggestion_list;
			}
			$this->output_compressed_data($result);
		}else{
			$result['status'] = FAILURE_STATUS;
			$result['message'] ="unable to fetch the cities";

		}
		$this->output_compressed_data($result);
	}


	function pre_bus_search_mobile()
	{
		$bus_data = $this->input->post('bus_search');
		if(empty($bus_data))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to search";
			echo json_encode($data);
			exit;
		}
		$search_params = json_decode($bus_data,true );

		$search_id = $this->save_pre_search_mobile(META_BUS_COURSE,$search_params);

		$this->load->model('bus_model');
		$this->bus_model->save_search_data($search_params, META_BUS_COURSE);

		$safe_search_data = $this->bus_model->get_safe_search_data($search_id);
		$active_booking_source = $this->bus_model->active_booking_source();		
		if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
			$safe_search_data['data']['search_id'] = abs($search_id);
		
			# Taken From ajax/mobile_bus_list
			$response['data'] = '';
			$response['msg'] = '';
			$response['status'] = FAILURE_STATUS;
		
			$search_params = array(
				'op' => 'load', 
				'booking_source' => $active_booking_source[0]['source_id'],
				'search_id' => abs($search_id)
				);

			if ($search_params['op'] == 'load' && intval($search_params['search_id']) > 0 && isset($search_params['booking_source']) == true) {
				load_bus_lib($search_params['booking_source']);
				switch($search_params['booking_source']) {
					case PROVAB_BUS_BOOKING_SOURCE :
					$currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));

					$this->CI = &get_instance();
					$this->CI->load->driver('cache');

					$search_file_data = $this->bus_lib->search_data(intval($search_params['search_id']));
					$search_hash = $search_file_data['search_hash'];
					// debug($search_file_data);die;
					$cache_search = $this->CI->config->item('cache_bus_search');
					// debug($cache_search);die;
					if ($cache_search) {
                        $cache_contents = $this->CI->cache->file->get($search_hash);
                    }
					// debug($cache_contents);die;
                    $raw_bus_list = array();
                    if ($cache_search === false || ($cache_search === true && empty($cache_contents) == true)){

                        $raw_bus_list = $this->bus_lib->get_bus_list(abs($search_params['search_id']));

                        if(valid_array($raw_bus_list)){
                        	$raw_bus_list = $this->bus_lib->search_data_in_preferred_currency($raw_bus_list, $currency_obj);
							$raw_bus_list['data']['result'] = force_multple_data_format($raw_bus_list['data']['result']);

                        	if ($cache_search) {
	                            $cache_exp = $this->CI->config->item('cache_bus_search_ttl');
	                            $this->CI->cache->file->save($search_hash, $raw_bus_list, $cache_exp);
	                        }
                        }
                    }else {
                        //read from cache
                        $raw_bus_list = $cache_contents;
                    }
                    // debug($raw_bus_list);die;
                    // $resp['operator']=array();
					foreach($raw_bus_list['data']['result'] as $k=>$v)
					{
						$CompanyId=$v['CompanyId'];
						$CompanyName=$v['CompanyName'];
						$Fare=$v['BusStatus']['BaseFares'][0];
						$API_Raw_Fare=$v['API_Raw_Fare'];
					}
					if ($raw_bus_list['status']) {

							$resp['status'] =  SUCCESS_STATUS;
							$resp['message']= "Data Available";
							$resp['data'] = $raw_bus_list['data']['result'];
							foreach ($raw_bus_list['data']['result'] as $rbkey => $rbvalue)  {
								if(isset($resp['operator']))
								{
									$flag=0;
									foreach ($resp['operator'] as $rokey => $rovalue) {
										if($rbvalue['CompanyId']==$rovalue['code'])
										{
											$flag=1;
										}
									}
									if($flag==0)
									{
										$ke=count($resp['operator'])+1;
										// debug($ke);
										$operator['code']=$rbvalue['CompanyId'];
										$operator['name']=$rbvalue['CompanyName'];
										$resp['operator'][]=$operator;
										
										// debug($resp['operator']);exit;
									}
								}else{
									// debug("else");
									$resp['operator'][0]['code'] = $rbvalue['CompanyId'];
									$resp['operator'][0]['name'] = $rbvalue['CompanyName'];
								}

							}
							// debug($resp['operator']);exit;
							// $resp['operator']=json_encode($resp['operator']);
							$resp['filter']['p']['max'] = $Fare;
							$resp['filter']['p']['min'] = $API_Raw_Fare;
							$resp['search_id'] = $search_params['search_id'];
							$resp['booking_source'] = $search_params['booking_source'];
							
							// debug($resp);exit;
							// debug($raw_bus_list);die;
							echo $this->output_compressed_data($resp);
							exit;
							
						}else{
							$response = array('status' => FAILURE_STATUS, 'message' => 'No Results Found');
							$response = $this->output_compressed_data($response);
							exit;
						}
						// debug($response);die;
						echo $response;
						break;
					}
				}

			} else {
				$response = array('status' => false, 'msg' => 'Invalid Data Format');
				$response = $this->output_compressed_data($response);
				echo $response;
			}	
		}
/*
		function terms()
		{
			$terms = $this->db->query('select * from cms_pages where page_title like "%terms%"')->result_array();

			if( valid_array($terms)){
				$data['status'] = SUCCESS_STATUS;
				$data['data'] = $terms[0]['page_description'];
				echo json_encode($data); 
				exit;
			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to get the data";
				echo json_encode($data);
				exit;
			}
		}*/

		function contact_us()
		{
			$terms = $this->db->query('select * from cms_pages where page_title like "%contact%"')->result_array();

			//$terms = $this->db->query('select * from domain_list')->result_array();
			if( valid_array($terms)){
				$data['status'] = SUCCESS_STATUS;
				$data['data'] = $terms[0]['page_description'];
				echo json_encode($data); 
				exit;
			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to get the data";
				echo json_encode($data);
				exit;
			}
		}

		function about_us()
		{
			$terms = $this->db->query('select * from cms_pages where page_title like "%about%"')->result_array();
// echo $this->db->last_query();
			//$terms = $this->db->query('select * from domain_list')->result_array();
			if( valid_array($terms)){
				$data['status'] = SUCCESS_STATUS;
				$data['data'] = $terms[0]['page_description'];
				$data['data1'] = $terms[0]['mobile_page_desc'];
				echo json_encode($data); 
				exit;
			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to get the data";
				echo json_encode($data);
				exit;
			}
		}

		function privacy()
		{
			$terms = $this->db->query('select * from cms_pages where page_title like "%privacy%"')->result_array();

			//$terms = $this->db->query('select * from domain_list')->result_array();
			if( valid_array($terms)){
				$data['status'] = SUCCESS_STATUS;
				$data['data'] = $terms[0]['page_description'];
				echo json_encode($data); 
				exit;
			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to get the data";
				echo json_encode($data);
				exit;
			}
		}

		function terms()
		{
			$terms = $this->db->query('select * from cms_pages where page_title like "%terms%"')->result_array();

			//$terms = $this->db->query('select * from domain_list')->result_array();
			if( valid_array($terms)){
				$data['status'] = SUCCESS_STATUS;
				$data['data'] = $terms[0]['page_description'];
				echo json_encode($data); 
				exit;
			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to get the data";
				echo json_encode($data);
				exit;
			}
		}

		function forgot_password_mobile()
		{

			$post_data = $this->input->post('forgot_details');
		//$post_data = '{"email":"jeeva.provab@gmail.com","phone":"1234567890"}';

			if(empty($post_data))
			{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Please enter the details to Change password";
				echo json_encode($data);
				exit;
			}
		//$hotel_data = $hotel_data['hotel_data'];
			$forgot_params = json_decode($post_data);
			$details_params = json_decode(json_encode($forgot_params), True);

			extract($details_params);
		//email, phone
			$condition['email'] = $email;
			$condition['phone'] = $phone;
			$condition['status'] = ACTIVE;
			$condition['user_type'] = B2C_USER;
			$user_record = $this->custom_db->single_table_records('user', 'email, password, user_id, first_name, last_name', $condition);
			if ($user_record['status'] == true and valid_array($user_record['data']) == true) {

				$user_record['data'][0]['password'] = time();
			//send email
				$mail_template = $this->template->isolated_view('user/forgot_password_template', $user_record['data'][0]);
				$user_record['data'][0]['password'] = md5($user_record['data'][0]['password']);
				$this->custom_db->update_record('user', $user_record['data'][0], array('user_id' => intval($user_record['data'][0]['user_id'])));
				$this->load->library('provab_sms');
			//echo $mail_template;exit;
			//$this->provab_mailer->send_mail($email, 'Password Reset', $mail_template);
			//$this->provab_mailer->send_mail($email, 'Password Reset', $mail_template);

				$message = 'Password Has Been Reset Successfully and Sent To Your Email ID';
				$data = array();
				$status = SUCCESS_STATUS;
			} else {
				$message = 'Please Provide Correct Data To Identify Your Account';
				$status = FAILURE_STATUS;
				$data = array();
			}
			header('content-type:application/json');
			echo json_encode(array('status' => $status, 'message' => $message, 'data' => $data));
			exit;
		}

		function update_profile_mobile()
		{
		//$post_data = '{"first_name":"Jeeva","last_name":"nandam","phone":"1234567890"}';
		//$user_id = "909";
			$post_data = $this->input->post('update_profile');
			$user_id = $this->input->post('user_id');
			if(empty($post_data))
			{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Please enter the details to Update Profile";
				echo json_encode($data);
				exit;
			}

			$update_params = json_decode($post_data);
			$details_params = json_decode(json_encode($update_params), True);

			if(empty($user_id) == false){

				$set = $details_params;
				$where =array('user_id'=>$user_id);
				$response = $this->custom_db->update_record('user',$set,$where);

				if($response == SUCCESS_STATUS){
					$res['status'] = SUCCESS_STATUS;
					$res['message']= 'Successfully Updated';
					$res['data']   = array();
					echo json_encode($res);
					exit;
				}else{
					$res['status'] = FAILURE_STATUS;
					$res['message']= 'Something went wrong please try again';
					$res['data']   = array();
					echo json_encode($res);
					exit;
				}

			}else{
				$res['status'] = FAILURE_STATUS;
				$res['message']= 'Please Provide Correct Data To Identify Your Account';
				$res['data']   = array();
				echo json_encode($res);
				exit;
			}

		}

		function get_passport_country()
		{
			$country = $this->db_cache_api->get_iso_country_code ();
			echo json_encode($country);
			exit;
		}

		function cms_mobile($terms)
		{
			error_reporting(0);
			$terms = $this->db->query('select * from cms_pages where page_title like "%'.$terms.'%"')->result_array();

			if( valid_array($terms)){
				$data['status'] = SUCCESS_STATUS;
				$data['data'] = $terms[0]['page_description'];
				echo json_encode($data); 
				exit;
			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to get the data";
				echo json_encode($data);
				exit;
			}
		}


		function getHashes()
		{
			$txnid =$_POST['txnid'];
			$amount =$_POST['amount'];
			$productinfo =$_POST['productinfo'];
			$firstname =$_POST['firstname'];
			$email =$_POST['email'];
			$user_credentials =$_POST['user_credentials'];
			$udf1 =$_POST['udf1'];
			$udf2 =$_POST['udf2'];
			$udf3 =$_POST['udf3'];
			$udf4 =$_POST['udf4'];
			$udf5 =$_POST['udf5'];
			$offerKey =$_POST['offerKey'];
			$cardBin = $_POST['cardBin'];

      // $firstname, $email can be "", i.e empty string if needed. Same should be sent to PayU server (in request params) also.
      	$key = 'IoH6cS'; //gtKFFx
      	$salt = 'nd35G1Wo'; //eCwWELxi

      	$payhash_str = $key . '|' .$this->checkNull($txnid) . '|' .$this->checkNull($amount)  . '|' .$this->checkNull($productinfo)  . '|' . $this->checkNull($firstname) . '|' .$this->checkNull($email) . '|' .$this->checkNull($udf1) . '|' .$this->checkNull($udf2) . '|' . $this->checkNull($udf3) . '|' . $this->checkNull($udf4) . '|' . $this->checkNull($udf5) . '||||||' . $salt;

      	$paymentHash = strtolower(hash('sha512', $payhash_str));
      	$arr['payment_hash'] = $paymentHash;

      	$cmnNameMerchantCodes = 'get_merchant_ibibo_codes';
      	$merchantCodesHash_str = $key . '|' . $cmnNameMerchantCodes . '|default|' . $salt ;
      	$merchantCodesHash = strtolower(hash('sha512', $merchantCodesHash_str));
      	$arr['get_merchant_ibibo_codes_hash'] = $merchantCodesHash;

      	$cmnMobileSdk = 'vas_for_mobile_sdk';
      	$mobileSdk_str = $key . '|' . $cmnMobileSdk . '|default|' . $salt;
      	$mobileSdk = strtolower(hash('sha512', $mobileSdk_str));
      	$arr['vas_for_mobile_sdk_hash'] = $mobileSdk;
      	
      	$cmnPaymentRelatedDetailsForMobileSdk1 = 'payment_related_details_for_mobile_sdk';
      	$detailsForMobileSdk_str1 = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk1 . '|default|' . $salt ;
      	$detailsForMobileSdk1 = strtolower(hash('sha512', $detailsForMobileSdk_str1));
      	$arr['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk1;

	      //used for verifying payment(optional)  
      	$cmnVerifyPayment = 'verify_payment';
      	$verifyPayment_str = $key . '|' . $cmnVerifyPayment . '|'.$txnid .'|' . $salt;
      	$verifyPayment = strtolower(hash('sha512', $verifyPayment_str));
      	$arr['verify_payment_hash'] = $verifyPayment;

      	if($user_credentials != NULL && $user_credentials != '')
      	{
      		$cmnNameDeleteCard = 'delete_user_card';
      		$deleteHash_str = $key  . '|' . $cmnNameDeleteCard . '|' . $user_credentials . '|' . $salt ;
      		$deleteHash = strtolower(hash('sha512', $deleteHash_str));
      		$arr['delete_user_card_hash'] = $deleteHash;

      		$cmnNameGetUserCard = 'get_user_cards';
      		$getUserCardHash_str = $key  . '|' . $cmnNameGetUserCard . '|' . $user_credentials . '|' . $salt ;
      		$getUserCardHash = strtolower(hash('sha512', $getUserCardHash_str));
      		$arr['get_user_cards_hash'] = $getUserCardHash;

      		$cmnNameEditUserCard = 'edit_user_card';
      		$editUserCardHash_str = $key  . '|' . $cmnNameEditUserCard . '|' . $user_credentials . '|' . $salt ;
      		$editUserCardHash = strtolower(hash('sha512', $editUserCardHash_str));
      		$arr['edit_user_card_hash'] = $editUserCardHash;

      		$cmnNameSaveUserCard = 'save_user_card';
      		$saveUserCardHash_str = $key  . '|' . $cmnNameSaveUserCard . '|' . $user_credentials . '|' . $salt ;
      		$saveUserCardHash = strtolower(hash('sha512', $saveUserCardHash_str));
      		$arr['save_user_card_hash'] = $saveUserCardHash;

      		$cmnPaymentRelatedDetailsForMobileSdk = 'payment_related_details_for_mobile_sdk';
      		$detailsForMobileSdk_str = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk . '|' . $user_credentials . '|' . $salt ;
      		$detailsForMobileSdk = strtolower(hash('sha512', $detailsForMobileSdk_str));
      		$arr['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk;

      	}


      	if ($offerKey!=NULL && !empty($offerKey)) {
      		$cmnCheckOfferStatus = 'check_offer_status';
      		$checkOfferStatus_str = $key  . '|' . $cmnCheckOfferStatus . '|' . $offerKey . '|' . $salt ;
      		$checkOfferStatus = strtolower(hash('sha512', $checkOfferStatus_str));
      		$arr['check_offer_status_hash']=$checkOfferStatus;
      	}


      	if ($cardBin!=NULL && !empty($cardBin)) {
      		$cmnCheckIsDomestic = 'check_isDomestic';
      		$checkIsDomestic_str = $key  . '|' . $cmnCheckIsDomestic . '|' . $cardBin . '|' . $salt ;
      		$checkIsDomestic = strtolower(hash('sha512', $checkIsDomestic_str));
      		$arr['check_isDomestic_hash']=$checkIsDomestic;
      	}

      	echo json_encode($arr); exit;
      }

      function checkNull($value) {
      	if ($value == null) {
      		return '';
      	} else {
      		return $value;
      	}
      }

      function mobile_success_payment()
      {
      	$response = $_REQUEST;
      	if(empty($response) == true){
      		$response['status'] = FAILURE_STATUS;
      		$response['data'] = array();
      	}else{
      		$response['status'] = SUCCESS_STATUS;
      		$response['data'] = $response;
      	}
      	echo json_encode($response);
      }

      function mobile_failure_payment()
      {
      	$response = $_REQUEST;
      	if(empty($response) == true){
      		$response['status'] = FAILURE_STATUS;
      		$response['data'] = array();
      	}else{
      		$response['status'] = SUCCESS_STATUS;
      		$response['data'] = $response;
      	}
      	echo json_encode($response);
      }

      function mobile_oldforgotpassword()
      {
      	$phone = $this->input->post('phone');
      	$email = $this->input->post('email_id');

      	if(empty($email) == false && empty($phone) == false ){
      		$get_user = $this->custom_db->single_table_records('user','*',array('user_name'=>$email,'phone'=>$phone));

      		if($get_user['status'] == true && valid_array($get_user['data']) && isset($get_user['data'][0]['user_id'])){
      			$otp = rand(10034,423234);
      			$update_otp = $this->custom_db->update_record('user',array('otp'=>$otp),array('user_id'=>$get_user['data'][0]['user_id']));
      			if (active_sms_checkpoint('forget_password')) {	
      				$msg =  "";
      				$msg .= "Dear Customer, ".$otp." is your one time password (OTP). Please enter the OTP to proceed.";
      				$msg .= " Thank you,";
      				$msg .= " Team Simplyjourney";
      				$this->load->library('provab_sms');
      				$sms_status = $this->provab_sms->send_msg ( $phone, $msg );	
      				$res['status'] = SUCCESS_STATUS;
      				$res['message'] = "OTP send to your mobile number !!";
      				$res['data'] = array('otp'=>$otp);
      				echo json_encode($res); exit;
      			}else{
      				$res['status'] = FAILURE_STATUS;
      				$res['message'] = "Please active the sms option !!!";
      				$res['data'] = array();
      				echo json_encode($res); exit;
      			}
      		}else{
      			$res['status'] = FAILURE_STATUS;
      			$res['message'] = "User Details Not Found !!";
      			$res['data'] = array();
      			echo json_encode($res); exit;
      		}
      	}else{
	      	$res['status'] = FAILURE_STATUS;
	      	$res['message'] = "Request Fileds are Missing !!";
	      	$res['data'] = array();
	      	echo json_encode($res); exit;
     	 }
  	}

function mobile_forgotpassword() {
		error_reporting(0);
		$post_data = $this->input->post();
		// debug($post_data); exit();
		extract ( $post_data );
		// email, phone
		//echo $email; exit();
		//$condition ['email'] = provab_encrypt($email_id);
		$condition ['email'] = $email_id;
		//$condition ['phone'] = $phone;
		$condition ['status'] = ACTIVE;
		$condition ['user_type'] = B2B_USER;
		$user_record = $this->custom_db->single_table_records ( 'user', 'email, password, user_id, first_name, last_name', $condition );
		// echo $this->db->last_query();exit();
		// debug($user_record); exit();
		if ($user_record ['status'] == true and valid_array ( $user_record ['data'] ) == true) {
			
			// Sms config & Checkpoint			
			 if(active_sms_checkpoint('forget_password'))
			 {
			 $msg = "Dear ".$user_record['data'][0]['first_name']." Your Password details has been sent to your email id";
			 //print($msg); exit;
			 /*$this->load->library ( 'provab_sms' );	
			 $msg = urlencode($msg);
			 $this->provab_sms->send_msg($phone,$msg);*/
			 }			
			// sms will be sent
			
			$user_record ['data'] [0] ['password'] = time ();
			// send email
			$mail_template = $this->template->isolated_view ( 'user/forgot_password_template', $user_record ['data'] [0] );
			$user_record ['data'] [0] ['password'] = provab_encrypt(md5(trim( $user_record ['data'] [0] ['password'] )));
			$this->custom_db->update_record ( 'user', $user_record ['data'] [0], array (
					'user_id' => intval ( $user_record ['data'] [0] ['user_id'] ) 
			) );
			//echo $this->db->last_query(); exit();
			$this->load->library ( 'provab_mailer' );
			// /echo $email_id; exit();			
			$this->provab_mailer->send_mail($email_id, 'Password Reset', $mail_template);
		// debug($mail_template); exit();	
			$data = 'Password Has Been Reset Successfully and Sent To Your Email ID';
			$status = true;
		} else {
			$data = 'Please Provide the Correct Data To Identify Your Account!';
			$status = false;
		}
		header ( 'content-type:application/json' );
		echo json_encode ( array (
				'status' => $status,
				'data' => $data 
		) );
		exit ();
	}

  	function mobile_forgotpassword_checkold()
  	{
  		$otp = $this->input->post('otp');
      	$email = $this->input->post('email_id');
      	$password = $this->input->post('password');
		if(empty($email) == false && empty($otp) == false && empty($password) ==false){
      		$get_user = $this->custom_db->single_table_records('user','*',array('user_name'=>$email));
      		if($get_user['status'] == true && valid_array($get_user['data']) && isset($get_user['data'][0]['user_id'])){
      			
      			if($get_user['data'][0]['otp'] == trim($otp)){
      			$update_otp = $this->custom_db->update_record('user',array('password'=>md5($password)),array('user_id'=>$get_user['data'][0]['user_id']));
      				$res['status'] = SUCCESS_STATUS;
      				$res['message'] = "Password Changed Successfully !!";
      				echo json_encode($res); exit;
      			}else{
      				$res['status'] = FAILURE_STATUS;
	      			$res['message'] = "Invalid OTP !!";
	      			$res['data'] = array();
	      			echo json_encode($res); exit;
      			}
      		}else{
				$res['status'] = FAILURE_STATUS;
      			$res['message'] = "User Details Not Found !!";
      			$res['data'] = array();
      			echo json_encode($res); exit;
      		}
      	}else{
	      	$res['status'] = FAILURE_STATUS;
	      	$res['message'] = "Request Fileds are Missing !!";
	      	$res['data'] = array();
	      	echo json_encode($res); exit;
     	}
  	}
  	function mobile_forgotpassword_check()
  	{
  		$otp = $this->input->post('otp');
      	$email = $this->input->post('email_id');
      	$password = $this->input->post('password');
		if(empty($email) == false && empty($otp) == false && empty($password) ==false){
      		$get_user = $this->custom_db->single_table_records('user','*',array('email'=>$email));
      		if($get_user['status'] == true && valid_array($get_user['data']) && isset($get_user['data'][0]['user_id'])){
      			
      			if($get_user['data'][0]['otp'] == trim($otp)){
      			$update_otp = $this->custom_db->update_record('user',array('password'=>md5($password)),array('user_id'=>$get_user['data'][0]['user_id']));
      				$res['status'] = SUCCESS_STATUS;
      				$res['message'] = "Password Changed Successfully !!";
      				echo json_encode($res); exit;
      			}else{
      				$res['status'] = FAILURE_STATUS;
	      			$res['message'] = "Invalid OTP !!";
	      			$res['data'] = array();
	      			echo json_encode($res); exit;
      			}
      		}else{
				$res['status'] = FAILURE_STATUS;
      			$res['message'] = "User Details Not Found !!";
      			$res['data'] = array();
      			echo json_encode($res); exit;
      		}
      	}else{
	      	$res['status'] = FAILURE_STATUS;
	      	$res['message'] = "Request Fileds are Missing !!";
	      	$res['data'] = array();
	      	echo json_encode($res); exit;
     	}
  	}


  	function mobile_change_password()
  	{
  		$current_password = $this->input->post('current_password');
  		$new_password = $this->input->post('new_password');
  		$user_id       = $this->input->post('user_id');

  		if(empty($current_password)==false && empty($new_password) ==false && empty($user_id) == false){

  			$get_user = $this->custom_db->single_table_records('user','password',array('user_id'=>$user_id));
  			if($get_user['status'] == true && isset($get_user['data'][0]['password'])){

  				$db_password = $get_user['data'][0]['password'];
  				if($db_password == provab_encrypt(md5($current_password))){
  					$this->custom_db->update_record('user',array('password'=>provab_encrypt(md5($new_password))),array('user_id'=>$user_id));
  					$resp['status'] = SUCCESS_STATUS;
		  			$resp['message'] = "Password Changed Successfully";
		  			$resp['data']= array();
		  			echo json_encode($resp); exit;
  				}else{
	  				$resp['status'] = FAILURE_STATUS;
		  			$resp['message'] = "Current Password Not Matching";
		  			$resp['data']= array();
		  			echo json_encode($resp); exit;
  				}
  			}else{
  				$resp['status'] = FAILURE_STATUS;
	  			$resp['message'] = "User Details Not Found";
	  			$resp['data']= array();
	  			echo json_encode($resp); exit;
  			}
  		}else{
  			$resp['status'] = FAILURE_STATUS;
  			$resp['message'] = "Request Fields are Missing";
  			$resp['data']= array();
  			echo json_encode($resp); exit;

  		}
  	}

  	function flight_invoice_mobile($app_reference, $booking_source='', $booking_status='', $operation='show_invoice')
	{
		// $app_reference  = $this->input->post('app_reference');
		// $booking_source = $this->input->post('booking_source');
		// $booking_status = $this->input->post('booking_status');

		if(empty($app_reference)==false && empty($booking_source)==false && empty($booking_status)==false){
			$this->load->model('flight_model');
			if (empty($app_reference) == false) {
				$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
				
				if ($booking_details['status'] == SUCCESS_STATUS) {
					load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
					//Assemble Booking Data
					$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2c');
					$page_data['data'] = $assembled_booking_details['data'];
					$operation="show_pdf";
					switch ($operation) {
						case 'show_invoice' : 
							$this->template->view('voucher/flight_invoice', $page_data);
						break;
						case 'show_pdf' :
							$this->load->library('provab_pdf');
							$create_pdf = new Provab_Pdf();
							$get_view=$this->template->isolated_view('voucher/flight_invoice_pdf', $page_data);
							//debug($get_view); die;
							$s= $create_pdf->create_pdf_link($get_view,'D'); 
						    $res = substr($s, 2);
						    $resp['status'] = SUCCESS_STATUS;
						    $resp['message'] = "";
						    $resp['data'] = array('pdf_url'=>$res);
						    echo json_encode($resp);exit;
						break;
					}
				}
			}
		}else{
			$resp['status'] =FAILURE_STATUS;
			$resp['message'] = "Request Fields are missing";
			$resp['data'] = array();
			echo json_encode($resp); exit;
		}
	}
	function fare_list($booking_source) {
        $response['data'] = '';
        $response['msg'] = '';
        $response['status'] = FAILURE_STATUS;
        $search_params = $this->input->get();
     //   debug($search_params); exit;
        load_flight_lib($booking_source);
        $search_params = $this->flight_lib->calendar_safe_search_data($search_params);
        print_r($search_params);exit();
        if (valid_array($search_params) == true) {
            switch ($booking_source) {
                case PROVAB_FLIGHT_BOOKING_SOURCE :
                    $raw_fare_list = $this->flight_lib->get_fare_list($search_params);
                    if ($raw_fare_list['status']) {
                        $fare_calendar_list = $this->flight_lib->format_cheap_fare_list($raw_fare_list['data']);
                        if ($fare_calendar_list['status'] == SUCCESS_STATUS) {
                            $response['data']['departure'] = $search_params['depature'];
                            $calendar_events = $this->get_fare_calendar_events($fare_calendar_list['data'], $raw_fare_list['data']['TraceId']);
                            $response['data']['day_fare_list'] = $calendar_events;
                            $response['status'] = SUCCESS_STATUS;
                        } else {
                            $response['msg'] = 'Not Available!!! Please Try Later!!!!';
                        }
                    }
                    break;
            }
        }
        $this->output_compressed_data($response);
    }
	function hotel_invoice_mobile($app_reference, $booking_source='', $booking_status='', $operation='show_invoice')
	{
		// $app_reference  = $this->input->post('app_reference');
		// $booking_source = $this->input->post('booking_source');
		// $booking_status = $this->input->post('booking_status');

		if(empty($app_reference)==false && empty($booking_source)==false && empty($booking_status)==false){
			$this->load->model('hotel_model');
			if (empty($app_reference) == false) {
				$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $booking_status);
				
				if ($booking_details['status'] == SUCCESS_STATUS) {
					load_hotel_lib(PROVAB_HOTEL_BOOKING_SOURCE);
					//Assemble Booking Data
					$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, 'b2c');
					$page_data['data'] = $assembled_booking_details['data'];
					$operation="show_pdf";
					switch ($operation) {
						case 'show_invoice' : 
							$this->template->view('voucher/hotel_invoice', $page_data);
						break;
						case 'show_pdf' :
							$this->load->library('provab_pdf');
							$create_pdf = new Provab_Pdf();
							$get_view=$this->template->isolated_view('voucher/hotel_invoice_pdf', $page_data);
							//debug($get_view); die;
							$s= $create_pdf->create_pdf_link($get_view,'D'); 
						    $res = substr($s, 2);
						    $resp['status'] = SUCCESS_STATUS;
						    $resp['message'] = "";
						    $resp['data'] = array('pdf_url'=>$res);
						    echo json_encode($resp);exit;
						break;
					}
				}
			}
		}else{
			$resp['status'] =FAILURE_STATUS;
			$resp['message'] = "Request Fields are missing";
			$resp['data'] = array();
			echo json_encode($resp); exit;
		}
	}

	function bus_invoice_mobile($app_reference, $booking_source='', $booking_status='', $operation='show_invoice')
	{
		// $app_reference  = $this->input->post('app_reference');
		// $booking_source = $this->input->post('booking_source');
		// $booking_status = $this->input->post('booking_status');

		if(empty($app_reference)==false && empty($booking_source)==false && empty($booking_status)==false){
			$this->load->model('bus_model');
			if (empty($app_reference) == false) {
				$booking_details = $this->bus_model->get_booking_details($app_reference, $booking_source, $booking_status);
				if ($booking_details['status'] == SUCCESS_STATUS) {
					load_bus_lib(PROVAB_BUS_BOOKING_SOURCE);
					//Assemble Booking Data
					$assembled_booking_details = $this->booking_data_formatter->format_bus_booking_data($booking_details, 'b2c');
				
					$page_data['data'] = $assembled_booking_details['data'];
					$operation="show_pdf";
					switch ($operation) {
						case 'show_invoice' : 
							$this->template->view('voucher/bus_invoice', $page_data);
						break;
						case 'show_pdf' :
							$this->load->library('provab_pdf');
							$create_pdf = new Provab_Pdf();
							$get_view=$this->template->isolated_view('voucher/bus_invoice_pdf', $page_data);
							
							$s= $create_pdf->create_pdf_link($get_view,'D'); 
						    $res = substr($s, 2);
						    $resp['status'] = SUCCESS_STATUS;
						    $resp['message'] = "";
						    $resp['data'] = array('pdf_url'=>$res);
						    echo json_encode($resp);exit;
						break;
					}
				}
			}
		}else{
			$resp['status'] =FAILURE_STATUS;
			$resp['message'] = "Request Fields are missing";
			$resp['data'] = array();
			echo json_encode($resp); exit;
		}
	}

	function mobile_promo()
	{
		$promo = $this->input->post('get_promo');
		//$promo = '{"promo_code":"SIMPLY100","module":"flight","price":"4000"}';
		if(empty($promo))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to Update Profile";
			echo json_encode($data);
			exit;
		}
		$update_params = json_decode($promo);
		$details_params = json_decode(json_encode($update_params), True);

		$return = $this->flight_model->get_promo($details_params['promo_code']);
		$data = $return->result_array();
		if(valid_array($data)){
			$module = trim($details_params['module']);
			if(($data[0]['status'] == 1))
			{
				if(($data[0]['expiry_date'] >= date('Y-m-d')))
				{ 
					if(($data[0]['module'] == $module))
					{
						$domain_balance = current_application_balance();

						$promo_discount_value = $data[0]['value'];
						$promo_discount_type = $data[0]['value_type'];
						$promo_discount_expiry = $data[0]['expiry_date'];
						$promo_discount_status = $data[0]['status'];
					    $total_promo_amount = ceil($promo_discount_value);
						
						if(empty($details_params['price']) ==true){
							$datas['message'] = "Price Not Found";
							$datas['status']= 0;
							echo json_encode($datas); exit;

						}else{
							$grand_total = trim($details_params['price']);
						}
						if($promo_discount_type == 'plus'){
							//$promocode_discount = $promo_discount_value;
							$value = $total_promo_amount;
								
						}
						if($promo_discount_type == 'percentage'){
							//$promocode_discount = $promo_discount_value;
							$value = $grand_total*($total_promo_amount/100);
						}
						if($value >= $grand_total){
							$datas['message'] = "Promocode Not Valid!!!";
							$datas['status']= 0;
							echo json_encode($datas);
							exit;
						}
						$value = $value;
						$datas['message'] = "Promocode Accepted";
						$datas['data']['promo_price']	= $value;
						$datas['status']=1;
						echo json_encode($datas);exit;
										
					}else{
						$datas['message'] = "Promocode Not Valid!!!";
						$datas['status']= 0;
						echo json_encode($datas);exit;
					}
				}else{
					$datas['message'] = "Promocode expired !!!";
					$datas['status']= 0;
					echo json_encode($datas);exit;
				}		
			}else{
				$datas['message'] = "Promocode Not Valid!!!";
				$datas['status']=0;
				echo json_encode($datas);exit;
			}
		}else{
			$datas['message'] = "Promocode Not Valid!!!";
				$datas['status']=0;
				echo json_encode($datas);exit;
		}
	}


	function get_promo_info()
	{
		error_reporting(E_ALL);
		$promo = $this->input->post('promo_code');
		//$promo = '{"promo_code":"SIMPLY100","module":"flight","price":"4000"}';
		if(empty($promo))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to Update Profile";
			echo json_encode($data);
			exit;
		}
		$return = $this->flight_model->get_promo($promo);
		$data = $return->result_array();
		if(valid_array($data)){
			// $module = trim($details_params['module']);
			if(($data[0]['status'] == 1))
			{
				if(($data[0]['expiry_date'] >= date('Y-m-d')))
				{ 
					
						// debug($data[0]);exit;
						$datas['data']	= $data[0];
						$datas['status']=1;
						echo json_encode($datas);exit;
										
					
				}else{
					$datas['message'] = "Promocode expired !!!";
					$datas['status']= 0;
					echo json_encode($datas);exit;
				}		
			}else{
				$datas['message'] = "Promocode Not Valid!!!";
				$datas['status']=0;
				echo json_encode($datas);exit;
			}
		}else{
			$datas['message'] = "Promocode Not Valid!!!";
				$datas['status']=0;
				echo json_encode($datas);exit;
		}
	}



	function all_promo()
	{
		$module = $this->input->post('module');
		//$module = "flight";
		$resp['status'] = FAILURE_STATUS;
		$resp['message'] = "";
		$resp['data'] = array();
		if(empty($module)== false){
			// $get_promo = $this->custom_db->single_table_records('promo_code_list','*',array('module'=>$module,'expiry_date'=> '>='.date('Y-m-d')));
			$qry = $this->db->query('select * from promo_code_list where module = "'.$module.'" and  display_home_page="Yes" and status ='.ACTIVE.' and expiry_date >="'.date('Y-m-d').'"');
			$get_promo = $qry->result_array();
			// debug($_SERVER);exit;
			foreach ($get_promo as $gpkey => $gpvalue) {
				$get_promo[$gpkey]['promo_code_image']=B_URL.'/extras/system/template_list/template_v1/images/promocode/'.$get_promo[$gpkey]['promo_code_image'];
			}
			if(valid_array($get_promo)){
				$resp['status'] = SUCCESS_STATUS;
				$resp['data'] = $get_promo;
			}else{
				$resp['message'] = "No Promocode found for this module";
			}
			
		}else{

			$qry = $this->db->query('select * from promo_code_list where status ='.ACTIVE.' and display_home_page="Yes" and expiry_date >="'.date('Y-m-d').'"');
			$get_promo = $qry->result_array();
				// debug($get_promo);exit;
			foreach ($get_promo as $gpkey => $gpvalue) {
				$get_promo[$gpkey]['promo_code_image']=B_URL.'/extras/system/template_list/template_v1/images/promocode/'.$get_promo[$gpkey]['promo_code_image'];
			}
			if(valid_array($get_promo)){
				$resp['status'] = SUCCESS_STATUS;
				$resp['data'] = $get_promo;
			}else{
				$resp['message'] = "No Promocode found for this module";
			}
		}
		// echo $this->db->last_query();exit;
		echo json_encode($resp); exit;

	}
	public function mobile_promocode() {
		$all_post=$this->input->post('get_promo');	
		error_reporting(0);	
		if(empty($all_post))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Promocode details are incorrect";
			echo json_encode($data);
			exit;
		}
		$post_promo_code = json_decode($all_post);
		$post_promo_code = json_decode(json_encode($post_promo_code), True);
	
		if(valid_array($post_promo_code)){
			$application_default_currency = admin_base_currency();
			$currency_obj = new Currency ( array ('module_type' => 'flight','from' => admin_base_currency (),'to' => $post_promo_code['currency']));
			
			// debug($all_post); exit();
			$condition['promo_code'] = $post_promo_code['promo_code'];
			$condition['status'] = 1;
			$promo_code_res=$this->custom_db->single_table_records('promo_code_list', '*', $condition );
			
			if($promo_code_res['status']==1)
			{
				$promo_code=$promo_code_res['data'][0];
				if($promo_code['module']!=$post_promo_code['module'])
				{
					$data['status']=0;
					$data['message']='Invalid Promo Code';
				}elseif($promo_code['expiry_date']<=date('Y-m-d')){
					
					$data['status']=0;
					$data['message']='Promo Code Expired';
				}else{
					
					if($promo_code['module']=='car')
					{
						$booking_table = 'Car_booking_details';
						$booking_user_table = 'Car_user_details';
					}elseif($promo_code['module']=='hotel')
					{
						$booking_table = 'hotel_booking_details';
					}elseif($promo_code['module']=='flight')
					{
						$booking_table = 'flight_booking_details';
					}elseif($promo_code['module']=='bus')
					{
						$booking_table = 'bus_booking_details';
					}
					###################################################################################
					if($post_promo_code['user_id']){
						$query = "SELECT BD.origin FROM payment_gateway_details AS PGD RIGHT JOIN ".$booking_table." AS BD ON PGD.app_reference = BD.app_reference WHERE BD.created_by_id='".$post_promo_code['user_id']."' ";
					}else{
						$email = $post_promo_code['email'];
						if($promo_code['module']!='car')
						{
							$query = "SELECT BD.origin FROM payment_gateway_details AS PGD RIGHT JOIN ".$booking_table." AS BD ON PGD.app_reference = BD.app_reference WHERE BD.email='".$email."' ";
						}else{
							$query = "SELECT BD.origin FROM payment_gateway_details AS PGD RIGHT JOIN ".$booking_table." AS BD ON PGD.app_reference = BD.app_reference RIGHT JOIN ".$booking_user_table." AS BUD ON BD.app_reference=BUD.app_reference  WHERE BUD.email='".$email."' ";
						}
					}
					###################################################################################
					// debug($query);exit;
					$user_promocode_check=$this->custom_db->get_result_by_query($query);
					$user_promocode_check = 0;
					
					if($user_promocode_check > 0){ 					
						$data['status']=0;
						$data['message']='Already used';
					}else{
						if($promo_code['value_type']=='percentage'){
							$data['value']=($post_promo_code['total_amount_val']*round($promo_code['value']))/100;
							$data['value'] = number_format($data['value'],2);
							$data['actual_value']= number_format($data['value'],2);
						}else
						{
							$data['value']= $promo_code['value'];
							$data['actual_value']= number_format($promo_code['value'],2);
							$data['value'] = get_converted_currency_value($currency_obj->force_currency_conversion($data['value']));
							$data['value'] = number_format($data['value'],2);
						}					
						
						$total_amount_val=($post_promo_code['total_amount_val']+$post_promo_code['convenience_fee'])-$data['value'];
						$total_amount_val=($total_amount_val>0)? $total_amount_val: 0;
						$data['total_amount_val'] = number_format($total_amount_val, 2);
						// $result['value'] = sprintf("%.2f", ceil($result['value']));
						//$result['value'] = round($result['value']).'.00';
						$data['total_amount_data'] =number_format($total_amount_val, 2);
						$data['convenience_fee']=$post_promo_code['convenience_fee'];
						$data['promo_code']=$post_promo_code['promo_code'];	
						//$data['discount_value']= $post_promo_code['currency_symbol']." ".number_format($data['value'],2);
						$data['discount_value']= number_format($data['value'],2);
						$data['module']=$post_promo_code['module'];
						$data['currency'] = $post_promo_code['currency'];
						$data['status']=1;
						$data['How do you get it?']='1)To avail discounts users have to register signup for booking tickets for their preferred destination by applying coupon code:'.$data['promo_code'].' 2)This offer is valid for limited period 3)The offer is valid on '. $data['module'].' bookings 4)The offer is valid for bookings made on MyTripBazaar website, Mobile site, Android & iOS App';
						$data['What else do you need to know?']='1)Convenience fee will be charged as per the applicability 2)The offer can’t be clubbed with any other promotional offers 3) Bookings with the valid promo codes will be only eligible for this offer 4)This offer is applicable for only new customers, to be redeemed only once 5)This offer cannot be clubbed with any other offer running on MyTripBazar 6)To avail this offer customer need to process his booking with registered mail id 7)In case of partial/full cancellation the offer stands void and discount will be rolled back before processing the refunds. 8)Child / infant discount, date or '. $data['module'].' change, refund charges, weekend surcharge, black out period, travel restrictions and / or '. $data['module'].' restriction will be also applicable as per the fare rule 9)Changes in '. $data['module'].' and dates are allowed with change fees and fare difference 10)Changes in names are not allowed';
						$data['Terms & Conditions']='1)In the event of any misuse or abuse of the offer, Mytrip Bazaar reserves the right to deny the offer to the customers. 2)MytripBazaar is the sole authority for interpretation of these terms. 3)In addition, Mytrip Bazaar standard booking and privacy policy on www.mytripbazaar.com shall apply. 4)In the event of any dispute, Courts of New Delhi will have jurisdiction. 5)MytripBazaar reserves the right, at any time, without prior notice and liability and without assigning any reason whatsoever, to add/alter/modify/change or vary all of these terms and conditions or to replace, wholly or in part, this offer by another offer, whether similar to this offer or not, or to extend or withdraw it altogether. 6)MytripBazaar shall not be liable for any loss or damage arising due to force majeure.';
					}

				}
			}
			else{
				$data['status']=$promo_code_res['status'];
				$data['message']='Invalid Promo Code';
			}
			echo json_encode($data);
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Promocode not in correct format";
			echo json_encode($data);
			exit;
		}
		
	}

	private function save_pre_search($search_type)
	{
		//Save data
		$search_params = $this->input->get();
		$search_data = json_encode($search_params);
		$insert_id = $this->custom_db->insert_record('search_history', array('search_type' => $search_type, 'search_data' => $search_data, 'created_datetime' => date('Y-m-d H:i:s')));
		return $insert_id['insert_id'];
	}
	private function save_search_cookie($module, $search_id)
	{
		$sparam = array();
		$sparam = $this->input->cookie('sparam', TRUE);
		if (empty($sparam) == false) {
			$sparam = unserialize($sparam);
		}
		$sparam[$module] = $search_id;

		$cookie = array(
			'name' => 'sparam',
			'value' => serialize($sparam),
			'expire' => '86500',
			'path' => PROJECT_COOKIE_PATH
		);
		$this->input->set_cookie($cookie);
	}
// 	function pre_sight_seen_search($search_id='')
// 	{
// 		$search_id = $this->save_pre_search(META_SIGHTSEEING_COURSE);
// 		// echo "hello";exit;
// 		// debug($search_id);exit;
// 		$this->save_search_cookie(META_SIGHTSEEING_COURSE, $search_id);
// 		//Analytics
// 		$this->load->model('sightseeing_model');
// 		$search_params = $this->input->get();
// 		// debug($search_params);exit;
// 		$response=$this->sightseeing_model->save_search_data($search_params, META_SIGHTSEEING_COURSE);
// 		if($response['status']==1)
// 		{
// 			$search_response['status']=1;
// 			$search_response['search_id']=$search_id;
// 		}else{
// 			$search_response['status']=0;
// 		}
// 		echo json_encode($search_response);
// // debug($response);exit;
// 		// redirect('sightseeing/search/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
// 	}
	
	function pre_sight_seen_search($search_id=''){

	    $search_id = $this->save_pre_search(META_SIGHTSEEING_COURSE);
	    $this->save_search_cookie(META_SIGHTSEEING_COURSE, $search_id);
	    //Analytics
	    $this->load->model('sightseeing_model');
	    $search_params = $this->input->get();
	    // debug($search_params);
	    // exit;
	    $response=$this->sightseeing_model->save_search_data($search_params, META_SIGHTSEEING_COURSE);
	    // $this->sightseeing_model->save_search_data($search_params, META_SIGHTSEEING_COURSE);
		if($response['status']==1)
		{
			$search_response['status']=1;
			$search_response['search_id']=$search_id;
		}else{
			$search_response['status']=0;
		}
		echo json_encode($search_response);
	    
	    // redirect('sightseeing/search/'.$search_id.'?'.$_SERVER['QUERY_STRING']);
	  }

	  function test_response()
	  {
	  	$this->template->view('flight/tbo/test_seatmap.php');
	  }

	 public function currency_converter()
	{
		$currency_data = $this->custom_db->single_table_records('currency_converter');
		$data['converter'] = $currency_data['data'];
		if(valid_array($data['converter'])){
			echo json_encode($data); exit;
		}else{
			$data = array('status'=>0,"message"=>"No Data Found.");
			echo json_encode($data); exit;
		}
		
	}

			/*
  *Pre Transfer Search
  */
  function pre_transferv1_search_mobile($search_id=''){

  	$transfer_search = $this->input->post('transfer_search');
  	// debug($transfer_search); exit();
  	$data['status'] = SUCCESS_STATUS;
	if(!empty($transfer_search))
	{
		$search_params = json_decode($transfer_search,true );
		if(!isset($search_params['from']) or empty($search_params['from'])){
			$data['message'][] = "Please pass the destination name in 'from' parameter.";
			$data['status'] = FAILURE_STATUS;
		}
		if(!isset($search_params['destination_id']) or empty($search_params['destination_id'])){
			$data['message'][] = "Please pass the destination ID name in 'destination_id' parameter.";
			$data['status'] = FAILURE_STATUS;
		}
	}else{
		
		$data['message'][] = "Please enter the details to search";
		$data['request_format'] = 'key ->transfer_search - value->{"from": "Bangalore","destination_id": "1262","from_date": "","to_date": ""}';
		$data['status'] = FAILURE_STATUS;
	}
	if($data['status']==FAILURE_STATUS){
		echo json_encode($data);
		exit;
	}


    $search_id = $this->save_pre_search_mobile(META_TRANSFERV1_COURSE,$search_params);
  //  $this->save_search_cookie(META_TRANSFERV1_COURSE, $search_id);
    //Analytics
    $this->load->model('transferv1_model');
  
    $this->transferv1_model->save_search_data($search_params, META_TRANSFERV1_COURSE);
    
   $transfer_search_mobile = self::search_transferv1_mobile($search_id);
	
	if($transfer_search_mobile)
	{
		$this->output_compressed_data($transfer_search_mobile);	
	}else{
		$data['status'] = FAILURE_STATUS;
		$data['message'] = "Unable to fetch data";
		$this->output_compressed_data($data);
		exit;
	}
  }
  function search_transferv1_mobile($search_id){ error_reporting(E_ALL);
  	$safe_search_data = $this->transferv1_model->get_safe_search_data($search_id,META_TRANSFERV1_COURSE);
  
	$active_booking_source = $this->transferv1_model->active_booking_source();
	
	if (valid_array($active_booking_source) == false) {
		$data['status'] = FAILURE_STATUS;
		$data['message'] = "Please active the API.";
		echo json_encode($data);
		exit;
	}

	$safe_search_data['data']['search_id'] = abs($search_id);
	$response['data'] = '';
	$response['msg'] = '';
	$response['status'] = FAILURE_STATUS;			
	$limit = '10000';
	$search_params['op'] = 'load';
	$search_params['booking_source'] = PROVAB_TRANSFERV1_BOOKING_SOURCE;
	$search_params['search_id'] = $search_id;

    if ($search_params['op'] == 'load' && intval($search_id) > 0 && isset($search_params['booking_source']) == true) {
        load_transferv1_lib($search_params['booking_source']);

        switch($search_params['booking_source']) {

            case PROVAB_TRANSFERV1_BOOKING_SOURCE :
           	
                $raw_transferv1_result = $this->transferv1_lib->get_transfer_list($safe_search_data);
             
                if ($raw_transferv1_result['status']) {
                    //Converting API currency data to preferred currency
                    $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
                    // debug($currency_obj);
                    $raw_transferv1_result = $this->transferv1_lib->search_data_in_preferred_currency($raw_transferv1_result, $currency_obj,'b2c');
                   // debug($raw_transferv1_result);exit;
                    //Display 
                    $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
                    // debug($currency_obj);
                    $filters = array();
    
                    $raw_transferv1_result['data'] = $this->transferv1_lib->format_search_response($raw_transferv1_result['data'], $currency_obj, $search_params['search_id'], 'b2c', $filters);
                    // debug($raw_transferv1_result['data']);exit;
                    if(valid_array($raw_transferv1_result['data']['TransferSearchResult']['TransferResults'])){
                    	$response['status'] = SUCCESS_STATUS;
	                  //  $response['data'] = $raw_transferv1_result['data']['TransferSearchResult']['TransferResults'];
	                     $response['data'] = array('transfer_list' => $raw_transferv1_result['data'],
				                                    'search_id' => $search_id, 
				                                    'booking_source' =>PROVAB_TRANSFERV1_BOOKING_SOURCE,
				                                    'search_params' => $safe_search_data['data']
                        );
	                }

                }else{
                    $response['status'] = FAILURE_STATUS;
                    
                }
            break;

        }
    }
    $this->output_compressed_data($response);
  }


  function top_airlines(){
		$page_data = array ();
		$data_list = $this->custom_db->single_table_records ( 'top_airlines', '*', '', 0, 100000 );
		// debug($page_data);exit;
		foreach ($data_list ['data'] as $key => $value) {
			$data_list ['data'][$key]['logo'] = $GLOBALS['CI']->template->domain_top_airline_images($value['logo']);
		}
		if(count($data_list['data'])>1)
		{
			$response['status'] = SUCCESS_STATUS;
			$response['data'] = @$data_list ['data'];
		}else{
			$response['status'] = FAILURE_STATUS;
			$response['message'] = 'Top Airlines not set by Admin';
		}
		$this->output_compressed_data($response);
  
	}


	function get_wallet_info()
	{
		$user_id=$this->input->post('user_id');

		if(isset($user_id) && !empty($user_id))
		{

			$wallet_log=$this->user_model->get_wallet_log($user_id);
			if($wallet_log!=false && !empty($wallet_log))
			{
				$response['status']=SUCCESS_STATUS;
				$response['wallet_balance']=$wallet_log[0]['closing_balance'];
				$response['trnsaction_log']=$wallet_log;
			}else{
				$response['status']=FAILURE_STATUS;
				$response['message']='No transactions Found for this user';
			}
		}else{
			$response['status']=FAILURE_STATUS;
			$response['message']='No transactions Found for this user';
		}
		
		$this->output_compressed_data($response);
	}

	public function notification_list($value='') 
	{
		$user_id=$this->input->post('user_id');

		if(isset($user_id) && !empty($user_id))
		{

			$list=$this->user_model->notification_list($user_id);
			if($list!=false && !empty($list))
			{
				$response['status']=SUCCESS_STATUS;
				$response['notification_list']=$list;
			}else{
				$response['status']=FAILURE_STATUS;
				$response['message']='No notfication found for this user';
			}
		}else{
			$response['status']=FAILURE_STATUS;
			$response['message']='User Id is required for this user';
		}
		
		$this->output_compressed_data($response);
	}

	public function loyality_points_history()
	{
		$user_id=$this->input->post('user_id');

		if(isset($user_id) && !empty($user_id))
		{

			$points_log=$this->user_model->loyality_points_history($user_id);
			/*if($points_log!=false && !empty($points_log))
			{
				$response['status']=SUCCESS_STATUS;
				$response['loyality_points']=$points_log;
			}else{
				$response['status']=FAILURE_STATUS;
				$response['message']='No points found for this user';
			}*/
			$response['status']=SUCCESS_STATUS;
			$response['loyality_points']=$points_log;
		}else{
			$response['status']=FAILURE_STATUS;
			$response['message']='User Id is Required';
		}
		
		$this->output_compressed_data($response);
	}

	public function testing($value='')
	{
		$url = explode('/mobile_webservices', base_url());
		echo "<pre>";print($url[0]);//exit();
		echo base_url();
	}

}

