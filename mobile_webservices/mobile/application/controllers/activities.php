<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (session_status() == PHP_SESSION_NONE) { session_start(); } 
error_reporting(0);
class Activities extends CI_Controller {
	//DO : Setting Current website url in session, Purpose : For keeping the page on login/logout.
	public function __construct(){
		parent::__construct();
		#error_reporting(0);
		$current_url 	= $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
        $current_url 	= WEB_URL.$this->uri->uri_string(). $current_url;
		$url 			= array('continue' => $current_url);
		$this->perPage 	= 100;
        $this->session->set_userdata($url);   
        $this->load->model('Home_Model');
		$this->load->model('Flight_Model');   
        $this->load->model('cart_model');
        $this->load->model('booking_model');  
        $this->load->model('sightseeing_model');  
        $this->load->model('email_model');  
		$this->load->library('encrypt');
        $this->load->library('session');
        $this->load->library('Ajax_pagination'); 
        $this->load->helper('sightseeing/sightseeing_helper');     
        $this->load->helper('string');
	}
    
    public function index(){
        $request 			= $this->input->get();

       
        #print_r($request);die;
        $data['req'] 		= json_decode(json_encode($request));
        $data['request'] 	= base64_encode(json_encode($request));
        $data['city']    = $request['city'];
        $data['category_id'] =0;
        if(isset($request['category_id'])){
            if($request['category_id']){
                 $data['category_id'] =$request['category_id'];
            }
        }
		if($request['city'] != ''){
			$this->load->view(PROJECT_THEME.'/activity/search_result_page', array('sight_seen_search_params' => $data));
		}else{
			redirect('home');
		}
	}
 	
	public function GetResults($Req_before_decode = '',$search_id=''){	

		$search_request =  json_decode(base64_decode($Req_before_decode));
		#print_r($search_request);die;
		$rand_id 					= md5(time() . rand() . crypt(time()));
      
		$xml_response=vitoor_search_request($search_request->activity_selection_id,$search_request->category_id);
		#echo "<pre/>";print_r($xml_response);die;
		$xml_response['request']=$search_request;
      
        $search_result_array = array();       
		if($xml_response['Status']==true)
		{     

            #print_r($xml_response);die;
            foreach($xml_response['Search']['SSSearchResult']['SightSeeingResults'] as $sight_results){
                $search_result_array[] = $sight_results;
                $mprice[]=$sight_results['Price']['TotalDisplayFare'];
            }
           
            $xml_response['Search']['SSSearchResult']['SightSeeingResults']=$search_result_array;
            #print_r($mprice);die;
            $dataresult['max']=max($mprice);
            $dataresult['min']=min($mprice);
            $dataresult['filter_result_count']=count($xml_response['Search']['SSSearchResult']['SightSeeingResults']);
            $xml_response['search_id'] = $search_id;
			$dataresult['data'] =  $this->load->view(PROJECT_THEME.'/activity/ajax_flight_result',$xml_response,true);
		}
		else
		{
			$dataresult['result'] =  $this->load->view(PROJECT_THEME.'/flight/ajax_result',$data,true);
		}
		echo json_encode($dataresult);
    }
    function transfer_details(){

        $get=$this->input->get();
         $data_condition['origin'] = $get['search_id'];
         $data_condition['search_type'] = 'ACTIVITIES';
          //getting search data
         $search_data = $this->custom_db->single_table_records('search_history','*',$data_condition);
         
        $xml_response=vitoor_details_request($get);
        
        $xml_response['request']=$search_request;
        if ($xml_response['Status']) {
            $calendar_availability_date = $this->enable_calendar_availability($xml_response['ProductDetails']['SSInfoResult']['Product_available_date']);
            $xml_response['ProductDetails']['SSInfoResult']['Calendar_available_date'] = $calendar_availability_date;
            

            if($search_data['status']==1){
                $search_req = json_decode($search_data['data'][0]['search_data']);

            }
            
            $xml_response['search_req'] = $search_req;
            $xml_response['search_id'] =  $get['search_id'];
        $this->load->view(PROJECT_THEME.'/activity/search_details_page',$xml_response);
        }else{
            redirect('home');
        }
    }
    function select_tourgrade(){
        $get=$this->input->post();
       
        $xml_response=vitoor_triplist_request($get);
        #print_r($xml_response);die;
        if ($xml_response['Status']) {
            $xml_response['search_params'] = $get;
            $xml_response['search_params']['search_id'] = $get['search_id'];
            $response['status']=true;
            #$xml_response['search_id']=$get['search_id'];
            $response['data']=$this->load->view(PROJECT_THEME.'/activity/select_tourgrade',$xml_response,true);
         header('Content-type:application/json');
         echo json_encode($response);
        }else{
            redirect('home');
        }
    }
    function booking(){
        $get=$this->input->post();
        #print_r($get);die;
        $xml_response=vitoor_triplist_block_request($get);

       
      // print_r($xml_response);
        if ($xml_response['Status']) {
            $xml_response['search_params'] = $get;
            $xml_response['room_response'] = $get['room_response'];
            $conf=$this->sightseeing_model->Add_cart($xml_response);
        redirect('booking/'.$conf.'/'.$get['search_id'].'/ACTIVITIES','refresh');
        }else{
            redirect('home');
        }
    }
    public function enable_calendar_availability($calendar_availability_date){
        
        if(valid_array($calendar_availability_date)){
            $available_str=array();
             foreach ($calendar_availability_date as $m_key => $m_value) {
                $avail_month_str = $m_key;
                
                foreach ($m_value as $d_key => $d_value) {
                    //j- to remove 0 from date, n to remove 0 from month
                    $date_str = $avail_month_str.'-'.$d_value;
                    $available_str[] = date('j-n-Y',strtotime($date_str));
                    
                }
             }
            
            return $available_str;
        }else{
            return '';
        }
    }
    
    function voucher(){
        $this->load->view(PROJECT_THEME.'/transferv1/voucher');
    }
    function CancelPnrBooking(){
      
       $this->load->model('booking_model');
       if($_POST){
            $pnr_no             =  base64_decode(base64_decode($_POST['pnr_no']));  
            $b_data = $this->booking_model->getBookingPnr($pnr_no)->result_array();  
            if($b_data){
                $app_reference  = $b_data[0]['con_pnr_no'];
                $sightseeing_request['AppReference'] = $app_reference;
                $sightseeing_request['CancelCode'] = "00";
                $sightseeing_request['CancelDescription']='Testing';
                $xml_response = viator_cancel_booking_request_params($app_reference,"00","Testing");
               
                if($xml_response['Status']==true){
                    if($xml_response['CancelBooking']['CancellationDetails']){
                        $update_booking = array(
                                    'booking_status'            => 'CANCELED',
                                    'cancellation_status'       => 'SUCCESS',
                                    'cancel_request'            => json_encode($sightseeing_request),
                                    'cancel_response'           => json_encode($xml_response)
                                );
                        $this->sightseeing_model->update_global_booking_data($pnr_no,$update_booking);
                        $data['response'] ='Your Cancellation  Request Done';
                        $data['status'] ='Success';
                    }
                }else{
                     $data['response'] ='Booking not cancelled from API';
                     $data['status'] ='Failure';
                }
            }else{
                $data['response'] ='Unable to Process your Request';
                $data['status'] ='Failure';
            }
       }else{
          $data['response'] ='PNR number is missing';
          $data['status'] ='Failure';
       }
       
        echo json_encode($data);
        exit;
    }
}
?>
