<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 *
 * @package    Provab
 * @subpackage Hotel
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */

class Hotel extends CI_Controller {
	public function __construct() {
		parent::__construct();
		//we need to activate hotel api which are active for current domain and load those libraries
		$this->load->model('hotel_model');
		$this->load->model('user_model');
		$this->load->library('social_network/facebook'); //Facebook Library to enable login button
		//$this->output->enable_profiler(TRUE);
		error_reporting(0);
	}

	/**
	 * index page of application will be loaded here
	 */
	function index() {
		//	echo number_format(0, 2, '.', '');
	}

	/**
	 *  Arjun J Gowda
	 * Load Hotel Search Result
	 * @param number $search_id unique number which identifies search criteria given by user at the time of searching
	 */
	function search($search_id) {
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		// Get all the hotels bookings source which are active
		$active_booking_source = $this->hotel_model->active_booking_source();

		if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
			$safe_search_data['data']['search_id'] = abs($search_id);
			$this->template->view('hotel/search_result_page', array('hotel_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
		} else {
			$this->template->view('general/popup_redirect');
		}
	}

	/**
	 *  Arjun J Gowda
	 * Load hotel details based on booking source
	 */
	function hotel_details($search_id) {
		$params = $this->input->get();
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		$safe_search_data['data']['search_id'] = abs($search_id);
		$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));

		if (isset($params['booking_source']) == true) {

			//We will load different page for different API providers... As we have dependency on API for hotel details page
			load_hotel_lib($params['booking_source']);
			if ($params['booking_source'] == PROVAB_HOTEL_BOOKING_SOURCE && isset($params['ResultIndex']) == true and
				isset($params['HotelCode']) == true and isset($params['TraceId']) == true and isset($params['op']) == true and
				$params['op'] == 'get_details' and $safe_search_data['status'] == true) {

				$params['TraceId'] = urldecode($params['TraceId']);
				$params['ResultIndex'] = urldecode($params['ResultIndex']);
				$params['HotelCode'] = urldecode($params['HotelCode']);
				$raw_hotel_details = $this->hotel_lib->get_hotel_details($params['TraceId'], $params['ResultIndex'], $params['HotelCode']);

				if ($raw_hotel_details['status']) {

					$this->template->view('hotel/tbo/tbo_hotel_details_page', array('currency_obj' => $currency_obj, 'hotel_details' => $raw_hotel_details['data'], 'hotel_search_params' => $safe_search_data['data'], 'active_booking_source' => $params['booking_source'], 'params' => $params));
				} else {
					redirect(base_url() . 'index.php/hotel/exception?op=Remote IO error @ Session Expiry&notification=session');
				}
			} else {
				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	}

	/**
	 *  Arjun J Gowda
	 * Passenger Details page for final bookings
	 * Here we need to run booking based on api
	 */
	function booking($search_id) {
		$pre_booking_params = $this->input->post();
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		$safe_search_data['data']['search_id'] = abs($search_id);
		$page_data['active_payment_options'] = $this->module_model->get_active_payment_module_list();
		if (isset($pre_booking_params['booking_source']) == true) {
			$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_default_currency()));
			//We will load different page for different API providers... As we have dependency on API for hotel details page
			$page_data['search_data'] = $safe_search_data['data'];
			load_hotel_lib($pre_booking_params['booking_source']);
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");

			if ($pre_booking_params['booking_source'] == PROVAB_HOTEL_BOOKING_SOURCE && isset($pre_booking_params['ResultIndex']) == true and isset($pre_booking_params['HotelCode']) == true and
				isset($pre_booking_params['op']) == true and $pre_booking_params['op'] == 'block_room' and $safe_search_data['status'] == true) {
				$pre_booking_params['token'] = unserialized_data($pre_booking_params['token'], $pre_booking_params['token_key']);
				if ($pre_booking_params['token'] != false) {

					$room_block_details = $this->hotel_lib->block_room($pre_booking_params);
					//debug($room_block_details); exit;
					if ($room_block_details['status'] == false) {
						redirect(base_url() . 'index.php/hotel/exception?op=' . $room_block_details['data']['msg']);
					}
					$pre_booking_params = $this->hotel_lib->update_block_details($room_block_details['data']['response']['BlockRoomResult'], $pre_booking_params);
					/*
						 * Update Markup
					*/
					$pre_booking_params['markup_price_summary'] = $this->hotel_lib->update_booking_markup_currency($pre_booking_params['price_summary'], $currency_obj, $safe_search_data['data']['search_id']);

					if ($room_block_details['status'] == SUCCESS_STATUS) {
						$page_data['booking_source'] = $pre_booking_params['booking_source'];
						$page_data['pre_booking_params'] = $pre_booking_params;
						$page_data['pre_booking_params']['default_currency'] = get_application_default_currency();
						$page_data['iso_country_list'] = $this->db_cache_api->get_iso_country_list();
						$page_data['country_list'] = $this->db_cache_api->get_country_list();
						$page_data['currency_obj'] = $currency_obj;
						$page_data['total_price'] = $this->hotel_lib->total_price($pre_booking_params['markup_price_summary']);
						$page_data['convenience_fees'] = ceil($currency_obj->convenience_fees($page_data['total_price'], $page_data['search_data']['search_id']));
						$page_data['tax_service_sum'] = $this->hotel_lib->tax_service_sum($pre_booking_params['markup_price_summary'], $pre_booking_params['price_summary']);
						//Traveller Details
						$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
						//debug($page_data); die;
						$this->template->view('hotel/tbo/tbo_booking_page', $page_data);
					}
				} else {
					redirect(base_url() . 'index.php/hotel/exception?op=Data Modification&notification=Data modified while transfer(Invalid Data received while validating tokens)');
				}
			} else {
				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	}

	/**
	 *  Arjun J Gowda
	 * Secure Booking of hotel
	 * 255 single adult static booking request 2310
	 * 261 double room static booking request 2308
	 */
	function pre_booking($search_id = 2310, $static_search_result_id = 255) {
		$post_params = $this->input->post();
		//Setting Static Data - Jaganath
		$post_params['billing_city'] = 'Bangalore';
		$post_params['billing_zipcode'] = '560100';
		$post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';

		$this->custom_db->generate_static_response(json_encode($post_params));
		//Insert To temp_booking and proceed
		/*$post_params = $this->hotel_model->get_static_response($static_search_result_id);*/
		$temp_booking = $this->module_model->serialize_temp_booking_record($post_params, HOTEL_BOOKING);
		$book_id = $temp_booking['book_id'];
		$book_origin = $temp_booking['temp_booking_origin'];

		//Make sure token and temp token matches
		$temp_token = unserialized_data($post_params['token'], $post_params['token_key']);
		if ($temp_token != false) {
			load_hotel_lib($post_params['booking_source']);
			if ($post_params['booking_source'] == PROVAB_HOTEL_BOOKING_SOURCE) {
				$amount = $this->hotel_lib->total_price($temp_token['markup_price_summary']);
				$currency = $temp_token['default_currency'];
			}
			$currency_obj = new Currency(array(
				'module_type' => 'hotel',
				'from' => get_application_default_currency(),
				'to' => get_application_default_currency(),
			));
			/********* Convinence Fees Start ********/
			$convenience_fees = ceil($currency_obj->convenience_fees($amount, $search_id));
			/********* Convinence Fees End ********/

			/********* Promocode Start ********/
			$promocode_discount = 0;
			/********* Promocode End ********/

			//details for PGI
			$email = $post_params['billing_email'];
			$phone = $post_params['passenger_contact'];
			$verification_amount = ceil($amount + $convenience_fees - $promocode_discount);
			$firstname = $post_params['first_name']['0'];
			$productinfo = META_ACCOMODATION_COURSE;
			if (isset($post_params['promo_code_discount_val']) && !empty($post_params['promo_code_discount_val'])) {
				$amount = $amount - $post_params['promo_code_discount_val'];
			}
			//check current balance before proceeding further
			$domain_balance_status = $this->domain_management_model->verify_current_balance($verification_amount, $currency);

			if ($domain_balance_status == true) {
				switch ($post_params['payment_method']) {
				case PAY_NOW:
					$this->load->model('transaction');
					$this->transaction->create_payment_record($book_id, $amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount);
					redirect(B_URL . 'index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin);
					break;
				case PAY_AT_BANK:echo 'Under Construction - Remote IO Error';exit;
					break;
				}
			} else {
				redirect(base_url() . 'index.php/hotel/exception?op=Amount Hotel Booking&notification=insufficient_balance');
			}
		} else {
			redirect(base_url() . 'index.php/hotel/exception?op=Remote IO error @ Hotel Booking&notification=validation');
		}
	}

	/**
	 *  Arjun J Gowda
	 *Do booking once payment is successfull - Payment Gateway
	 *and issue voucher
	 *HB11-152109-443266/1
	 *HB11-154107-854480/2
	 */
	function secure_booking($book_id, $temp_book_origin, $user_type, $user_id) {
		error_reporting(0);
		// $book_id =trim(base64_decode($book_id));
		// $temp_book_origin = trim(base64_decode($temp_book_origin));
		//$auth_token = $this->input->post('auth_token');
		$auth_token = '';
		if (empty($book_id) == false && empty($temp_book_origin) == false && $temp_book_origin > 0) {
			//verify payment status and continue
			$book_id = $book_id;
			$temp_book_origin = intval($temp_book_origin);
			$this->load->model('transaction');

			$booking_status = $this->transaction->get_payment_status($book_id);

			// if($booking_status['status'] !== 'accepted'){
			// 	$data['status']=false;
			// 	$data['message'] ='Payment not done';
			// 	echo json_encode($data);
			// 	exit;
			// }
		} else {
			$data['status'] = false;
			$data['message'] = 'Invalid Booking Details';
			$this->output_compressed_data($data);
			exit;
		}
		//run booking request and do booking
		$temp_booking = $this->module_model->unserialize_temp_booking_record($book_id, $temp_book_origin);
		//debug($temp_booking);exit;
		//Delete the temp_booking record, after accessing
		//$this->module_model->delete_temp_booking_record ($book_id, $temp_book_origin);
		load_hotel_lib($temp_booking['booking_source']);
		//verify payment status and continue
		$total_booking_price = $this->hotel_lib->total_price($temp_booking['book_attributes']['token']['markup_price_summary']);
		$currency = $temp_booking['book_attributes']['token']['default_currency'];
        $user_id = $temp_booking['book_attributes']['user_id'];

        $loyality_points = $temp_booking['book_attributes']['markup_ad'];

		//also verify provab balance
		//check current balance before proceeding further
		$domain_balance_status = $this->domain_management_model->verify_current_balance($total_booking_price, $currency);
		if ($domain_balance_status) {
			//lock table
			if ($temp_booking != false) {
				switch ($temp_booking['booking_source']) {
				case PROVAB_HOTEL_BOOKING_SOURCE:
					//FIXME : COntinue from here - Booking request
					$booking = $this->hotel_lib->process_booking($book_id, $temp_booking['book_attributes'], $auth_token);
					//Save booking based on booking status and book id
					break;
				}

				/*print_r($booking);
				exit();*/

				if ($booking['status'] == SUCCESS_STATUS) {
					$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_default_currency()));
					$booking['data']['currency_obj'] = $currency_obj;
                    $booking['data']['loyality_points'] = $loyality_points;
					//Save booking based on booking status and book id
					//debug($booking['data']); exit();
					$data = $this->hotel_lib->save_booking($book_id, $booking['data'],'b2b',$user_id);
					//debug($data); exit();

					if ($user_type == 'b2c') {
						$this->domain_management_model->update_transaction_details('hotel', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount']);
					} else {
						$this->domain_management_model->update_transaction_details_b2b('hotel', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount'], $data['currency_conversion_rate'], $user_id);
					}
					$this->applyLoyalityPoints($total_booking_price, $book_id, $user_id);

					//deduct balance and continue
					$resp['status'] = SUCCESS_STATUS;
					$resp['data'] = array('app_reference' => $book_id, 'booking_source' => $temp_booking['booking_source'], 'status' => 'BOOKING_CONFIRMED');
					$url = explode('/mobile_webservices', base_url());
					redirect(base_url().'mobile' . '/index.php/voucher/hotel/' . $book_id . '/' . $temp_booking['booking_source'] . '/BOOKING_CONFIRMED/show_voucher');
					$res["status"] = 1;
					$res["url"] = base_url() . 'index.php/voucher/hotel/' . $book_id . '/' . $temp_booking['booking_source'] . '/BOOKING_CONFIRMED/show_voucher';
					//die(json_encode($res));

					$this->output_compressed_data($resp);exit;
				} else {
					//redirect(base_url().'index.php/hotel/exception?op=booking_exception&notification='.$booking['msg']);
					$resp['status'] = FAILURE_STATUS;
					$resp['message'] = "Booking Failed";
					$this->output_compressed_data($resp);exit;
				}
			}
			//release table lock
		} else {
			// redirect(base_url().'index.php/hotel/exception?op=Remote IO error @ Insufficient&notification=validation');
			$resp['status'] = FAILURE_STATUS;
			$resp['message'] = "Insuffient Balance";
			echo json_encode($resp);exit;
		}
		//redirect(base_url().'index.php/hotel/exception?op=Remote IO error @ Hotel Secure Booking&notification=validation');
	}


    private function applyLoyalityPoints($total_booking_price=0, $app_reference=0, $user_id='') {
        
        //echo $total_booking_price;
        //$total_booking_price = 10000; 
        //$app_reference = 'FB18-200416-717494';
        // get booking loyality price
        $price = $this->user_model->get_point_from_hotel_booking_details($app_reference);
        $booking_price = $this->user_model->get_price_from_payment_details($app_reference);
        $loyality_points_config = $this->user_model->getUserLoyalityPoint($user_id);
        $available_loyality_points = $loyality_points_config['loyality_points'];

        /*echo $this->db->last_query();
        echo "<pre>";
        print_r($loyality_points_config);exit();*/
        $total_points = ($price['loyality_points']/$loyality_points_config['loyality_points_coversion_rate']);
        //$this->user_model->deduct_loyality_point($total_points, $user_id);
        $total_booking_price = 0;
        if(!empty($booking_price)){
            $total_booking_price = $booking_price['amount'];
        }
        $data1 = array(
            'booking_ref' => $app_reference,
            'debit' => $total_points,
            'equivalent_amount' => $price['loyality_points'],
            //'user_id' => $this->entity_user_id
            'user_id' => $user_id,
            'remarks' => $total_booking_price
        );
        $this->user_model->log_loyality_point($data1);
         

        $credit = (int)(($total_booking_price*$loyality_points_config['max_percent_loyality_point_can_earn'])/100);

        $data2 = array(
            'booking_ref' => $app_reference,
            'credit' => $credit,
            'equivalent_amount' => 0,
            //'user_id' => $this->entity_user_id,
            'user_id' => $user_id,
            'remarks' => $total_booking_price
        );
        $this->user_model->log_loyality_point($data2);

        //deducting debit amount
        $available_loyality_points = $available_loyality_points - $price['loyality_points'];

        //updating credit amount
        $available_loyality_points = $available_loyality_points + $credit; 

        //$this->user_model->add_loyality_point($credit,$user_id);
        $this->user_model->update_user_loyality_point($available_loyality_points,$user_id);

    }
	/*
		process booking in backend until show loader
	*/
	function process_booking($book_id, $temp_book_origin, $user_type, $user_id='') {
		// echo $book_id; exit();

		if($user_id ==''){
            $user_id = $this->entity_user_id;
        }
        
		if ($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0) {

			// $page_data ['form_url'] = base_url () . 'index.php/hotel/secure_booking';
			// $page_data ['form_method'] = 'POST';
			// $page_data ['form_params'] ['book_id'] = $book_id;
			// $page_data ['form_params'] ['temp_book_origin'] = $temp_book_origin;
			$form_url = base_url() . 'mobile/index.php/hotel/secure_booking';
			$book_id = $book_id;
			$temp_book_origin = $temp_book_origin;
			if ($user_type == 'b2b') {
				redirect($form_url . '/' . $book_id . '/' . $temp_book_origin . '/' . $user_type . '/' . $user_id);
			} else {
				redirect($form_url . '/' . $book_id . '/' . $temp_book_origin. '/b2b/' . $user_id);
			}
		} else {

		}

	}

	function test() {
		$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_default_currency()));
		debug($currency_obj);
	}

	/**
	 *  Arjun J Gowda
	 *Process booking on hold - pay at bank
	 */
	function booking_on_hold($book_id) {

	}
	/**
	 * Jaganath
	 */
	function pre_cancellation($app_reference, $booking_source) {
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				$this->template->view('hotel/pre_cancellation', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	/**
	 *Elavarasi
	 *@param $app_reference
	 *@param $book_source
	 */
	public function cancel_booking_mobile() {
		//	error_reporting(E_ALL);
		$app_reference = $this->input->post('book_id');
		$booking_source = $this->input->post('booking_source');
		if (empty($app_reference) && empty($booking_source)) {
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please Provide correct details";
			echo json_encode($data);
			exit;
		}
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source);

			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_hotel_booking_data($master_booking_details, 'b2c');
				$master_booking_details = $master_booking_details['data']['booking_details'][0];
				load_hotel_lib($booking_source);
				$cancellation_details = $this->hotel_lib->cancel_booking_mobile($master_booking_details); //Invoke Cancellation Methods
				if ($cancellation_details['status'] == false) {
					$data['message'] = $cancellation_details['msg'];
					$data['status'] = FAILURE_STATUS;
				} else {
					$data['status'] = SUCCESS_STATUS;
					$data['message'] = 'Your Cancellation Request has been sent';
				}
				echo json_encode($data);
				exit;
			}

		} else {
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please Provide correct details";
			echo json_encode($data);
			exit;
		}

	}
	public function hotel_images() {
		error_reporting(0);
		$hotel_code = $this->input->post('hotel_code');
		$booking_source = $this->input->post('booking_source');
		if (empty($hotel_code) && empty($booking_source)) {
			$data['status'] = false;
			$data['message'] = 'Please Provide Hotel Code';
			echo json_encode($data);
			exit;
		}
		if (empty($hotel_code) == false && empty($booking_source) == false) {
			load_hotel_lib($booking_source);
			$raw_hotel_images = $this->hotel_lib->get_hotel_images($hotel_code);
			if ($raw_hotel_images['status'] == SUCCESS_STATUS) {
				$data['hotel_images'] = $raw_hotel_images['data'];
				$data['status'] = true;
			} else {
				$data['message'] = 'No hotel image found';
				$data['status'] = false;
			}
			echo json_encode($data);
			exit;
		} else {
			$data['status'] = false;
			$data['message'] = 'Hotel Code and booking source cannot be empty';
			echo json_encode($data);
			exit;
		}
	}
	/**
	 *Get Cancellation Policy by cancellation policy code
	 */
	public function cancellation_policy() {
		$post_params = $this->input->post('policy_params');
		if (empty($post_params)) {
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to get cancellation policy";
			echo json_encode($data);
			exit;
		}
		$policy_params = json_decode($post_params);
		$policy_params = json_decode(json_encode($policy_params), True);
		if (valid_array($policy_params)) {
			if ($policy_params['booking_source']) {
				load_hotel_lib($policy_params['booking_source']);
				$get_cancellation_policy = $this->hotel_lib->get_cancellation_policy($policy_params);
				if ($get_cancellation_policy['status'] == true) {
					$data['policy_data'] = $get_cancellation_policy['data'];
					$data['status'] = true;
				} else {
					$data['status'] = false;
				}
				echo json_encode($data);
				exit;
			}
		} else {
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "cancellation policy params are incorrect";
			echo json_encode($data);
			exit;
		}
	}
	/*
		 * Jaganath
		 * Process the Booking Cancellation
		 * Full Booking Cancellation
		 *
	*/
	function cancel_booking($app_reference, $booking_source) {
		if (empty($app_reference) == false) {
			$master_booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_hotel_booking_data($master_booking_details, 'b2c');
				$master_booking_details = $master_booking_details['data']['booking_details'][0];
				load_hotel_lib($booking_source);
				$cancellation_details = $this->hotel_lib->cancel_booking($master_booking_details); //Invoke Cancellation Methods
				if ($cancellation_details['status'] == false) {
					$query_string = '?error_msg=' . $cancellation_details['msg'];
				} else {
					$query_string = '';
				}
				redirect('hotel/cancellation_details/' . $app_reference . '/' . $booking_source . $query_string);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}

	/**
	 * Jaganath
	 * Cancellation Details
	 * @param $app_reference
	 * @param $booking_source
	 */
	function cancellation_details($app_reference, $booking_source) {
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $GLOBALS['CI']->hotel_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$page_data = array();
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_hotel_booking_data($master_booking_details, 'b2c');
				$page_data['data'] = $master_booking_details['data'];
				$this->template->view('hotel/cancellation_details', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}

	}
	function map() {
		$details = $this->input->get();
		$geo_codes['data']['latitude'] = $details['lat'];
		$geo_codes['data']['longtitude'] = $details['lon'];
		$geo_codes['data']['hotel_name'] = urldecode($details['hn']);
		$geo_codes['data']['star_rating'] = $details['sr'];
		$geo_codes['data']['city'] = urldecode($details['c']);
		$geo_codes['data']['hotel_image'] = urldecode($details['img']);
		echo $this->template->isolated_view('hotel/location_map', $geo_codes);
	}

	/**
	 * Arjun J Gowda
	 */
	function exception() {
		$module = META_ACCOMODATION_COURSE;
		$op = (empty($_GET['op']) == true ? '' : $_GET['op']);
		$notification = (empty($_GET['notification']) == true ? '' : $_GET['notification']);
		$eid = $this->module_model->log_exception($module, $op, $notification);
		//set ip log session before redirection
		$this->session->set_flashdata(array('log_ip_info' => true));
		redirect(base_url() . 'index.php/hotel/event_logger/' . $eid);
	}

	function event_logger($eid = '') {
		$log_ip_info = $this->session->flashdata('log_ip_info');
		$this->template->view('hotel/exception', array('log_ip_info' => $log_ip_info, 'eid' => $eid));
	}

	function mobile_hotel_details() {
		error_reporting(0);
		// $params = '{"TokenId":"0bcdd43e-7598-477f-88b1-bf0902e701b2","ResultIndex":"101","HotelCode":"985738","TraceId":"8a67e85a-33d5-43b6-bebd-f75ad9aa8d96","booking_source":"PTBSID0000000001","op":"get_details","search_id":"41"}';
		//echo "resr";
		$params = $this->input->post('hotel_details');
		// debug($params);exit;
		$hotel_details = json_decode($params);
		$hotel_details = json_decode(json_encode($hotel_details), True);
		//debug($hotel_details['search_id']); die;

		//debug($hotel_details); exit();
		$safe_search_data = $this->hotel_model->get_safe_search_data($hotel_details['search_id']);
		$safe_search_data['data']['search_id'] = abs($hotel_details['search_id']);
		$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));

		//We will load different page for different API providers... As we have dependency on API for hotel details page

		load_hotel_lib($hotel_details['booking_source']);
		if ($hotel_details['booking_source'] == PROVAB_HOTEL_BOOKING_SOURCE && isset($hotel_details['ResultIndex']) == true and
			isset($hotel_details['HotelCode']) == true and isset($hotel_details['op']) == true and
			$hotel_details['op'] == 'get_details' and $safe_search_data['status'] == true) {

			$hotel_details['TraceId'] = urldecode($hotel_details['TraceId']);
			$hotel_details['ResultIndex'] = urldecode($hotel_details['ResultIndex']);
			$hotel_details['HotelCode'] = urldecode($hotel_details['HotelCode']);
			//$hotel_details['TokenId']		= urldecode($hotel_details['TokenId']);

			//debug($hotel_details); exit();
			$raw_hotel_details = $this->hotel_lib->get_hotel_details_mobile($hotel_details['TraceId'], $hotel_details['ResultIndex'], $hotel_details['HotelCode'], $safe_search_data['data']['search_id']);
			$raw_hotel_details['data']['HotelInfoResult'] = $raw_hotel_details['data']['HotelDetails']['HotelInfoResult'];

			if (isset($raw_hotel_details['data']['HotelDetails']['HotelInfoResult']['HotelDetails']['first_room_details']['Room_data']['RoomUniqueId'])) {
				$dynamic_params_url = $raw_hotel_details['data']['HotelDetails']['HotelInfoResult']['HotelDetails']['first_room_details']['Room_data']['RoomUniqueId'];
			}
			$dynamic_params_url = serialized_data($dynamic_params_url);
			$ls_param = array();

			$ls_param['token'] = $dynamic_params_url;
			$ls_param['token_key'] = md5($dynamic_params_url);

			$raw_hotel_details['data']['HotelInfoResult']['Custom'] = $ls_param;

			// debug($raw_hotel_details['data']['HotelDetails']['HotelInfoResult']['HotelDetails']['first_room_details']['Room_data']['RoomUniqueId']);exit;
			// debug($safe_search_data);exit;

			$raw_hotel_details['data']['HotelInfoResult']['TraceId'] = urldecode($hotel_details['TraceId']);
			unset($raw_hotel_details['data']['HotelDetails']);
			// debug($raw_hotel_details);exit;
			if (valid_array($raw_hotel_details) && $raw_hotel_details['data']['Status'] == ACTIVE) {
				$raw_hotel_details['status'] = SUCCESS_STATUS;
				$this->output_compressed_data($raw_hotel_details);exit();
			} else {
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to fetch the hotel details";
				$data['data'] = '';
				$this->output_compressed_data($data);
			}

		} else {
			//echo "sdfd";exit;
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "something went wrong Please try again";
			$data['data'] = '';
			$this->output_compressed_data($data);
		}
	}

	/**
	 * Compress and output data
	 * @param array $data
	 */
	private function output_compressed_data($data) {
		while (ob_get_level() > 0) {ob_end_clean();}
		ob_start("ob_gzhandler");
		header('Content-type:application/json');
		echo json_encode($data);
		ob_end_flush();
		exit;
	}
	function generate_combinations(array $data, array &$all = array(), array $group = array(), $value = null, $i = 0) {
		$keys = array_keys($data);
		if (isset($value) === true) {
			array_push($group, $value);
		}

		if ($i >= count($data)) {
			array_push($all, $group);
		} else {
			$currentKey = $keys[$i];
			$currentElement = $data[$currentKey];
			foreach ($currentElement as $val) {
				$this->generate_combinations($data, $all, $group, $val, $i + 1);
			}
		}

		return $all;
	}

	function get_hotel_room_list() {
		//$params = '{"TokenId":"a3feda00-ddea-466e-bf62-78f2715ce281","ResultIndex":"167","HotelCode":"985738","TraceId":"1277a4f2-4ab3-409f-aa08-6049644fcb03","booking_source":"PTBSID0000000001","op":"get_room_details","search_id":"45"}';
		$params = $this->input->post('room_list');
		$user_type = $this->input->post('user_type');
		$user_id = $this->input->post('user_id');

		$hotel_details = json_decode($params);
		$params = json_decode(json_encode($hotel_details), True);
		//debug($params); die;

		$search_id = $params['search_id'];

		$response['data'] = '';
		$response['msg'] = '';
		$response['status'] = FAILURE_STATUS;
		//$params = $this->input->post();
		//debug($params); exit();
		if ($params['op'] == 'get_room_details' && intval($params['search_id']) > 0 && isset($params['booking_source']) == true) {

			$application_preferred_currency = get_application_currency_preference();
			$application_default_currency = get_application_default_currency();
			//$currency_obj = new Currency(array('module_type' => 'hotel','from' => $application_default_currency, 'to' => $application_preferred_currency));
			$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
			//debug($currency_obj);exit;
			load_hotel_lib($params['booking_source']);
			$this->hotel_lib->search_data($params['search_id']);
			$attr['search_id'] = intval($params['search_id']);
			//debug($params); die;
			switch ($params['booking_source']) {
			case PROVAB_HOTEL_BOOKING_SOURCE:
				$raw_room_list = $this->hotel_lib->get_room_list_mobile(urldecode($params['TraceId']), urldecode($params['ResultIndex']), urldecode($params['HotelCode']));
				//debug($raw_room_list);exit;
				if ($raw_room_list['data']['Status']) {
					//Converting API currency data to preferred currency
					//echo "dfsdf";exit;
					$raw_room_list = $this->hotel_lib->roomlist_in_preferred_currency($raw_room_list, $currency_obj, $search_id, $user_type, $user_id);
				}
				//debug($raw_room_list);exit;
				break;
			}
		}
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		$safe_search_data['data']['search_id'] = abs($search_id);
		//$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
		$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		$params['op'] = 'get_details';

		load_hotel_lib($params['booking_source']);
		if ($params['booking_source'] == PROVAB_HOTEL_BOOKING_SOURCE && isset($params['ResultIndex']) == true and
			isset($params['HotelCode']) == true and isset($params['TraceId']) == true and isset($params['op']) == true and
			$params['op'] == 'get_details' and $safe_search_data['status'] == true) {

			$params['TraceId'] = urldecode($params['TraceId']);
			$params['ResultIndex'] = urldecode($params['ResultIndex']);
			$params['HotelCode'] = urldecode($params['HotelCode']);
			//$params['TokenId']	    = urldecode($params['TokenId']);
			//debug($params);exit;
			$raw_hotel_details = $this->hotel_lib->get_hotel_details_mobile($params['TraceId'], $params['ResultIndex'], $params['HotelCode'], $search_id, $user_type, $user_id);

			$hotel_room_details['raw_room_list'] = $raw_room_list;
			//debug($hotel_room_details); die;
			$booking_url = $GLOBALS['CI']->hotel_lib->booking_url($params['search_id']);

			if (valid_array($raw_room_list) && $raw_room_list['data']['Status'] == SUCCESS_STATUS) {
				$raw_room_list = $raw_room_list['data'];
				$rm_list = "";
				$clean_room_list = ''; //HOLD DATA TO BE RETURNED
				$_HotelRoomsDetails = get_room_index_list($raw_room_list['RoomList']['GetHotelRoomResult']['HotelRoomsDetails']);
				$_RoomCombinations = $raw_room_list['RoomList']['GetHotelRoomResult']['RoomCombinations'];
				//$_TraceId					 = $raw_room_list['RoomList']['GetHotelRoomResult']['TraceId'];
				$_TraceId = $params['TraceId'];

				//$_IsUnderCancellationAllowed = $raw_room_list['RoomList']['GetHotelRoomResult']['IsUnderCancellationAllowed'];
				$_IsUnderCancellationAllowed = '';
				$_InfoSource = $raw_room_list['RoomList']['GetHotelRoomResult']['RoomCombinations']['InfoSource'];
				$common_params_url = '';
				$last_params = array();

				$last_params['HotelCode'] = $params['HotelCode']; //This URL has to be carried forward
				$last_params['ResultIndex'] = $params['ResultIndex'];
				$last_params['booking_source'] = $params['booking_source'];
				$last_params['search_id'] = $params['search_id'];

				/**
				 * Forcing room combination to appear in multiple list format
				 */
				if (isset($_RoomCombinations['RoomCombination'][0]) == false) {
					$_RoomCombinations['RoomCombination'][0] = $_RoomCombinations['RoomCombination'];
				}

				$generate_rm_cm = array();
				if ($_InfoSource != 'FixedCombination') {

					//print_r($_RoomCombinations['RoomCombination']);
					foreach ($_RoomCombinations['RoomCombination'] as $key => $value) {
						$rm_com = array();
						/*echo "key "; print_r($key);
							echo "<br>value "; print_r($value['RoomIndex']);*/
						$rm_com = $value['RoomIndex'];
						$generate_rm_cm[] = $rm_com;
					}

					$_RoomComb = $this->generate_combinations($generate_rm_cm);
					//echo "<br><pre>";print_r($_RoomComb);
					$RoomComb_fin = array();
					foreach ($_RoomComb as $key => $value) {
						$RoomComb_fin[$key]['RoomIndex'] = $value;
						/*echo"<br>key"; print_r($key);
							 echo "<bR> VALUE";
							 print_r($value);*/
					}
					$_RoomCombinations['RoomCombination'] = $RoomComb_fin;
					//echo "<pre>";	print_r($RoomComb_fin);
					/*	debug($_RoomCombinations);
						exit;*/
				}

				/**
				 * Forcing room combination to appear in multiple list format
				 */
				if (isset($_RoomCombinations['RoomCombination'][0]) == false) {
					$_RoomCombinations['RoomCombination'][0] = $_RoomCombinations['RoomCombination'];
				}

				/**
				 * Forcing Room list to appear in multiple list format
				 */
				if (isset($_HotelRoomsDetails[0]) == false) {
					$_HotelRoomsDetails[0] = $_HotelRoomsDetails;
				}

				//---------------------------------------------------------------------------
				//Print Combination - START
				foreach ($_RoomCombinations['RoomCombination'] as $__rc_key => $__rc_value) {
					/**
					 * Forcing Combination to appear in multiple format
					 */
					if (valid_array($__rc_value['RoomIndex']) == false) {
						$current_combination_wrapper = array($__rc_value['RoomIndex']);
					} else {
						$current_combination_wrapper = $__rc_value['RoomIndex'];
					}

					$temp_current_combination_count = count($current_combination_wrapper);
					$room_panel_details = $room_panel_summary = $dynamic_params_url = ''; //SUPPORT DETAILS
					foreach ($current_combination_wrapper as $__room_index_key => $__room_index_value) {
						//NOTE : PRINT ROOM DETAILS OF EACH ROOM INDEX VALUE
						$temp_room_details = $_HotelRoomsDetails[$__room_index_value];

						$common_params_url .= '<input type="hidden" name="CancellationPolicy[]"	value="' . $temp_room_details['CancellationPolicy'] . '">'; //Jaganath

						$last_params['CancellationPolicy'] = $temp_room_details['CancellationPolicy'];
						$SmokingPreference = get_smoking_preference(@$temp_room_details['SmokingPreference']);

						$last_params['RoomTypeName'] = ucfirst(strtolower($temp_room_details['RoomTypeName']));

						/*
							new cancellation policy
							*/

						$cancel_string = '';
						$cancellation_policy_details = $temp_room_details['CancellationPolicies'];
						//debug($cancellation_policy_details);
						$cancel_count = count($cancellation_policy_details);

						$cancel_reverse = $this->hotel_lib->php_arrayUnique(array_reverse($cancellation_policy_details), 'Charge');

						$cancellation_policy_details = $this->hotel_lib->php_arrayUnique(array_reverse($cancellation_policy_details), 'Charge');

						if ($cancellation_policy_details) {
							//$cancellation_policy_details = array_reverse($cancellation_policy_details);
							foreach ($cancellation_policy_details as $key => $value) {
								$policy_string = '';
								if ($value['Charge'] == 0) {
									$policy_string .= 'No cancellation charges, if cancelled before ' . date('d M Y', strtotime($value['ToDate']));
								} else {
									if (isset($cancel_reverse[$key + 1])) {
										if ($value['ChargeType'] == 1) {
											$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency) . "  " . $value['Charge'];
										} elseif ($value['ChargeType'] == 2) {
											$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency) . "  " . $room_price;
										}
										$current_date = date('Y-m-d');
										$cancell_date = date('Y-m-d', strtotime($value['FromDate']));
										if ($cancell_date > $current_date) {
											$policy_string .= 'Cancellations made after ' . date('d M Y', strtotime($value['FromDate'])) . ' to ' . date('d M Y', strtotime($value['ToDate'])) . ', would be charged ' . $amount;
										}
									} else {
										if ($value['ChargeType'] == 1) {
											$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency) . "  " . $value['Charge'];
										} elseif ($value['ChargeType'] == 2) {
											$amount = $currency_obj->get_currency_symbol($currency_obj->to_currency) . "  " . $room_price;
										}
										$current_date = date('Y-m-d');
										$cancell_date = date('Y-m-d', strtotime($value['FromDate']));
										if ($cancell_date > $current_date) {
											$value['FromDate'] = $value['FromDate'];
										} else {
											$value['FromDate'] = date('Y-m-d');
										}
										$policy_string .= 'Cancellations made after ' . date('d M Y', strtotime($value['FromDate'])) . ', or no-show, would be charged ' . $amount;
									}
								}

								$cancel_string .= $policy_string . '<br/>';
							}
						} else {
							$cancel_string = 'This rate is non-refundable. If you cancel this booking you will not be refunded any of the payment.';
						}

						$last_params['CancellationPolicy'] = $cancel_string;

						$last_params['LastCancellationDate'] = local_date($temp_room_details['LastCancellationDate']);
						$last_params['LastCancellationDate'] = local_date($temp_room_details['LastCancellationDate']);

						//$last_params['AccessKey'] = $temp_room_details['AccessKey'];
						$last_params['AccessKey'] = 'xyz';
						$AM = array();
						if (isset($temp_room_details['Amenities'])) {

							foreach ($temp_room_details['Amenities'] as $__amenity) {

								$AM[] = ucfirst(strtolower($__amenity));
							}

							$last_params['Amenities'] = $AM;
						}

						$search_id = $attr['search_id'];

						$rslt_temp_room_details = $temp_room_details;
						$temp_price_details = $GLOBALS['CI']->hotel_lib->update_room_markup_currency($temp_room_details['Price'], $currency_obj, $search_id);
						// debug($temp_price_details);exit;
						$tax = $temp_price_details['PublishedPriceRoundedOff'] - $temp_price_details['RoomPrice'];
						$PublishedPrice = $temp_price_details['PublishedPrice'];
						$PublishedPriceRoundedOff = $temp_price_details['PublishedPriceRoundedOff'];
						$OfferedPrice = $temp_price_details['OfferedPrice'];
						$OfferedPriceRoundedOff = $temp_price_details['OfferedPriceRoundedOff'];
						$RoomPrice = $temp_price_details['RoomPrice'];

						$last_params['Currency'] = get_application_currency_preference();
						$last_params['Currency_symbol'] = $currency_obj->get_currency_symbol($currency_obj->to_currency);
						$last_params['RoomPrice'] = $RoomPrice;

						$last_params['CancellationPolicy_old'] = $rslt_temp_room_details['CancellationPolicy'];

						$rslt_temp_room_details['RoomTypeName'] = ucfirst(strtolower($rslt_temp_room_details['RoomTypeName']));
						if (intval($__room_index_key) == 0) {
							$temp_book_now_button = '';
							$temp_summary_room_list = array($rslt_temp_room_details['RoomTypeName']);
							$temp_summary_price_list = array($RoomPrice);
						} else {
							$temp_summary_room_list[] = $rslt_temp_room_details['RoomTypeName'];
							$temp_summary_price_list[] = $RoomPrice;
						}

						if (intval($temp_current_combination_count) == intval($__room_index_key + 1)) {
							//PIN Summary
							if (valid_array($temp_summary_room_list)) {
								$temp_summary_room_list = implode(' <i class="fa fa-plus"></i> ', $temp_summary_room_list);
							}
							if (valid_array($temp_summary_price_list)) {
								$temp_summary_price_list = array_sum($temp_summary_price_list);
							}
							$room_panel_summary = '';

						}
						// debug($temp_summary_price_list);exit;
						$last_params['RoomPrice'] = $temp_summary_price_list;
						$last_params['Tax'] = $tax;
						$last_params['Rate_key'] = $temp_room_details['rate_key'];
						$policy_code = '';
						if (isset($temp_room_details['cancellation_policy_code'])) {
							$policy_code = $temp_room_details['cancellation_policy_code'];
						}
						$last_params['Policy_code'] = $policy_code;

						$last_params['API_SEARCH_ID'] = $temp_room_details['SEARCH_ID'];
						// if(isset($temp_room_details[''])){

						// }
						// $last_params['']
						//$dynamic_params_url[] = get_dynamic_booking_parameters($__room_index_value, $rslt_temp_room_details, $application_default_currency);
						$dynamic_params_url[] = $temp_room_details['RoomUniqueId'];

					} //END INDIVIDUAL COMBINATION LOOPING
					$dynamic_params_url = serialized_data($dynamic_params_url);
					$ls_param = array();

					$ls_param['token'] = $dynamic_params_url;
					$ls_param['token_key'] = md5($dynamic_params_url);

					$rm_list['room_data'] = $last_params;
					//$rm_list['room_data']['RoomUniqueId'] = $temp_room_details['RoomUniqueId'];
					$rm_list['room_data']['custom'] = $ls_param;

					$rm_s['data'][] = $rm_list;
				} //END COMBINATION LOOPING

				$resp['status'] = SUCCESS_STATUS;
				$resp['data'] = $rm_s;
				$this->output_compressed_data($resp);exit;
				//---------------------------------------------------------------------------Print Combination - END

			} else {
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to process your request please try again";
				$this->output_compressed_data($data);exit();

			}

			$this->output_compressed_data($hotel_room_details);exit();

		} else {
			$this->output_compressed_data("No Hotel Found");
		}

	}

	function mobile_booking() {

		$params = $this->input->post('details');
		//$params = '{"HotelCode":"985738","ResultIndex":"176","search_id":"90","TraceId":"435608a6-40c3-4dad-8f75-f7696def0216","HotelName":"Hotel Elite In","StarRating":"2","HotelAddress":"Kalpana Cinema Building","HotelImage":"http:\/\/www.travelboutiqueonline.com\/imageresource.aspx?img=lLdKYi18YEgkGZK2iagaFjGxYML67342J7Ha3JCjPVShKxc\/WIpL6DyinagDOCagq5IsIEZHu\/0OYsFvEsJlkIfqIvaeLx46x5s+I6Z1uSuY9FT6v2ib4w==","CancellationPolicy":"test"}';
		//$params = $this->input->post('details');
		$params = json_decode($params);
		$params = json_decode(json_encode($params), True);

		$HotelCode = $params['HotelCode'];
		$ResultIndex = $params['ResultIndex'];
		$search_id = $params['search_id'];
		$TraceId = $params['TraceId'];
		$HotelName = $params['HotelName'];
		$StarRating = $params['StarRating'];
		$HotelAddress = $params['HotelAddress'];
		$HotelImage = $params['HotelImage'];
		$CancellationPolicy = array($params['CancellationPolicy']);
		$user_type = $params['user_type'];
		$user_id = isset($params['user_id']) ? $params['user_id'] : '';

		$token = $this->input->post('token');
		//echo $token;exit;
		// $token 				= json_decode($token);
		// $token 				= json_decode(json_encode($token), True);
		// $token              = $token['token'];
		//$token = array($token);
		//$dynamic_params_url = serialized_data($token);
		$Token = $token;

		$token_key = md5($Token);

		//$Token__ID 			=  $this->input->post('TokenId');
		//$Auth_Token         =  $this->input->post('auth_token');
		$Auth_Token = '';
		$pre_booking_params = array(
			'HotelCode' => $HotelCode,
			'ResultIndex' => $ResultIndex,
			'booking_source' => 'PTBSID0000000001',
			'search_id' => $search_id,
			'TraceId' => $TraceId,
			'op' => 'block_room',
			'HotelName' => $HotelName,
			'StarRating' => $StarRating,
			'HotelImage' => $HotelImage,
			'HotelAddress' => $HotelAddress,
			'CancellationPolicy' => $CancellationPolicy,
			'token' => $Token,
			'token_key' => $token_key,
		);

		//debug($pre_booking_params);exit;
		//$pre_booking_params = $this->input->post();
		//debug($pre_booking_params); exit;
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		$safe_search_data['data']['search_id'] = abs($search_id);
		$page_data['active_payment_options'] = $this->module_model->get_active_payment_module_list();


		$loyality_point = $this->user_model->getUserLoyalityPoint($params['user_id']);
        if(!empty($loyality_point)){
            $max_point_can_use_in_one_time = $loyality_point['max_point_can_use_in_one_time'];
            $loyality_points               = (int) (($loyality_point['loyality_points'] * $loyality_point['percent_can_use']) / 100);
            $loyality_points_usable        = (min($max_point_can_use_in_one_time, $loyality_points)) * $loyality_point['loyality_points_coversion_rate'];

            $page_data['loyality_points_usable'] = (int) $loyality_points_usable;
        }else{
            $page_data['loyality_points_usable'] = 0;
        }

		if (isset($pre_booking_params['booking_source']) == true) {
			$currency_obj = new Currency(array('module_type' => 'hotel', 'from' => get_application_default_currency(), 'to' => get_application_default_currency()));
			//We will load different page for different API providers... As we have dependency on API for hotel details page
			$page_data['search_data'] = $safe_search_data['data'];
			load_hotel_lib($pre_booking_params['booking_source']);
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			//debug($pre_booking_params);exit;
			if ($pre_booking_params['booking_source'] == PROVAB_HOTEL_BOOKING_SOURCE && isset($pre_booking_params['ResultIndex']) == true and isset($pre_booking_params['HotelCode']) == true and
				isset($pre_booking_params['op']) == true and $pre_booking_params['op'] == 'block_room' and $safe_search_data['status'] == true) {

				$pre_booking_params['token'] = unserialized_data($pre_booking_params['token'], $pre_booking_params['token_key']);
				//debug($pre_booking_params); die;
				if ($pre_booking_params['token'] != false) {
					//debug($Token__ID); die;
					$room_block_details = $this->hotel_lib->block_room_mobile($pre_booking_params, $Auth_Token);

					if ($room_block_details['status'] == false) {
						//redirect(base_url().'index.php/hotel/exception?op='.$room_block_details['data']['msg']);
						$resp['status'] = FAILURE_STATUS;
						$resp['message'] = "room block status fail";
						$resp['data'] = array();
						echo json_encode($resp);exit;
					}

					$pre_booking_params = $this->hotel_lib->update_block_details($room_block_details['data']['response']['BlockRoomResult'], $pre_booking_params);

					/*
						 * Update Markup
					*/
					$pre_booking_params['markup_price_summary'] = $this->hotel_lib->update_booking_markup_currency($pre_booking_params['price_summary'], $currency_obj, $safe_search_data['data']['search_id'], $user_type, $user_id);
					$pre_booking_params['default_currency'] = get_application_default_currency();
					$dynamic_params_url = serialized_data($pre_booking_params);

					$Token = $dynamic_params_url;
					$token_details['Token'] = $dynamic_params_url;
					$token_details['Token_key'] = md5($Token);

					if ($room_block_details['status'] == SUCCESS_STATUS) {
						$page_data['booking_source'] = $pre_booking_params['booking_source'];
						$page_data['pre_booking_params'] = $pre_booking_params;
						$page_data['pre_booking_params']['default_currency'] = get_application_default_currency();
						//$page_data['iso_country_list']	= $this->db_cache_api->get_iso_country_list();
						//$page_data['country_list']		= $this->db_cache_api->get_country_list();
						$page_data['currency_obj'] = $currency_obj;
						$page_data['total_price'] = $this->hotel_lib->total_price($pre_booking_params['markup_price_summary']);
						$page_data['convenience_fees'] = ceil($currency_obj->convenience_fees($page_data['total_price'], $page_data['search_data']['search_id']));
						$page_data['tax_service_sum'] = $this->hotel_lib->tax_service_sum($pre_booking_params['markup_price_summary'], $pre_booking_params['price_summary']);
						$page_data['search_id'] = $search_id;
						//Traveller Details
						$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
						//$id = $GLOBALS['CI']->entity_user_id ;
						//$dynamic_params_url = serialized_data($pre_booking_params);
						$page_data['token'] = $token_details['Token'];
						$page_data['token_key'] = $token_details['Token_key'];
						$response['status'] = SUCCESS_STATUS;
						//debug($page_data);exit;
						$response['data'] = $page_data;
						$this->output_compressed_data($response);exit();
					}
				} else {
					$resp['status'] = FAILURE_STATUS;
					$resp['message'] = "room block status fail";
					$resp['data'] = array();
					$this->output_compressed_data($resp);exit;
					//redirect(base_url().'index.php/hotel/exception?op=Data Modification&notification=Data modified while transfer(Invalid Data received while validating tokens)');
				}
			} else {
				$resp['status'] = FAILURE_STATUS;
				$resp['message'] = "Requested fields are missing";
				$resp['data'] = array();
				$this->output_compressed_data($resp);exit;
			}
		} else {
			$resp['status'] = FAILURE_STATUS;
			$resp['message'] = "Something went wrong please try again";
			$resp['data'] = array();
			$this->output_compressed_data($resp);exit;
		}
	}

	function pre_booking_api($search_id = '') {

		$wallet_bal = $this->input->post('wallet_bal');
		$passenger_data = json_decode($this->input->post('hotel_params'), true);
		$Token = $this->input->post('Token');
		$Token_key = $this->input->post('Token_key');
		//$GLOBALS['CI']->entity_user_id = $customer_id;
		$user_id = $passenger_data['user_id'];
		$api_token = $passenger_data['ApiToken'];
		$TokenId = $api_token['TokenId'];

		$search_id = $passenger_data['search_id'];
		$search_id = $search_id;
		//debug($passenger_data);exit;
		foreach ($passenger_data['Passengers'] as $passengers_details) {
			$Title[] = $passengers_details['Title'];
			$FirstName[] = $passengers_details['FirstName'];
			$LastName[] = $passengers_details['LastName'];

			if ($passengers_details['passenger_type'] == "Adult") {
				$passenger_type[] = "1";
			} else {
				$passenger_type[] = "2";
			}

			$Gender[] = $passengers_details['Gender'];
			$DateOfBirth[] = $passengers_details['DateOfBirth'];
			$PassportNumber = array($passengers_details['PassportNumber']);
			$PassportExpiry = $passengers_details['PassportExpiry'];
			$CountryCode[] = $passenger_data['CountryCode'];
			$CountryName[] = $passenger_data['CountryName'];
			$ContactNo = $passenger_data['ContactNo'];
			$City = $passenger_data['City'];
			$PinCode = $passenger_data['PinCode'];
			$AddressLine1 = $passenger_data['AddressLine1'];
			$Email = $passenger_data['Email'];
			$phone = $passenger_data['ContactNo'];
			$payment_method = $passenger_data['payment_method'];
			$customer_id = $passenger_data['customer_id'];
		}

		$PassportExpiry = explode('-', $PassportExpiry);

		$post_params = array(
			'search_id' => $search_id,
			'token' => $Token,
			'token_key' => $Token_key,
			'op' => 'book_flight',
			'booking_source' => PROVAB_HOTEL_BOOKING_SOURCE,
			'passenger_type' => $passenger_type,
			'lead_passenger' => array('1'),
			'gender' => $Gender,
			'passenger_nationality' => array('92'),
			'name_title' => $Title,
			'first_name' => $FirstName,
			'last_name' => $LastName,
			'date_of_birth' => $DateOfBirth,
			'passenger_passport_number' => $PassportNumber,
			'passenger_passport_issuing_country' => array('92'),
			'billing_email' => $Email,
			'passenger_contact' => $phone,
			'passenger_passport_expiry_day' => array(date('d')),
			'passenger_passport_expiry_month' => array(date('m')),
			'passenger_passport_expiry_year' => array(date('Y') + 10),
			'pay_amount' => $passenger_data['final_fare'],
			'payment_method' => ($payment_method) ? $payment_method : 'PNHB1',
			'customer_id' => $customer_id,
			'wallet_bal' => $wallet_bal,
			'promo_code' => $passenger_data['promo_code'],
			'promo_code_discount_val' => $passenger_data['promo_code_discount_val'],
			'ProvabAuthKey' => $passenger_data['ProvabAuthKey'],
			'BlockRoomId' => $passenger_data['BlockRoomId'],
			'card' => $this->input->post('card'),
			'month' => $this->input->post('month'),
			'year' => $this->input->post('year'),
		);
		if($passenger_data['markup_ad']!=0 && isset($passenger_data['markup_ad'])){
            $post_params['loyality_points']  = $passenger_data['markup_ad'];
        }else{
            $post_params['loyality_points']  = 0;
        }
		if (valid_array($post_params) == false) {
			$resp['status'] = FAILURE_STATUS;
			$resp['data'] = array();

		}

		$post_params['billing_city'] = 'Bangalore';
		$post_params['billing_zipcode'] = '560100';
		$post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';
		$post_params['billing_country'] = 'India';

		$booking_source = HOTEL_BOOKING;
		$temp_booking = $this->module_model->serialize_temp_booking_record($post_params, HOTEL_BOOKING);
//debug($temp_booking);exit;
		$book_id = $temp_booking['book_id'];
		$book_origin = $temp_booking['temp_booking_origin'];

		$user_id = '';

		if (isset($post_params['tageduser']) && isset($post_params['customer_id'])) {
			$tagged_users = json_decode($post_params['tageduser'], true);
			$tagged_data = $this->db_cache_api->insert_tagged_user($book_id, $tagged_users, $post_params['customer_id']);
			$user_id = $post_params['customer_id'];
		}

		$this->custom_db->generate_static_response(json_encode($post_params));

		//Make sure token and temp token matches
		$temp_token = unserialized_data($post_params['token']);
		load_hotel_lib($post_params['booking_source']);
		if ($post_params['booking_source'] == PROVAB_HOTEL_BOOKING_SOURCE) {
			$amount = $this->hotel_lib->total_price($temp_token['markup_price_summary']);
			$currency = $temp_token['default_currency'];
		}
		$currency_obj = new Currency(array(
			'module_type' => 'hotel',
			'from' => get_application_default_currency(),
			'to' => get_application_default_currency(),
		));
		/********* Convinence Fees Start ********/
		$convenience_fees = ceil($currency_obj->convenience_fees($amount, $search_id));
		/********* Convinence Fees End ********/

		/********* Promocode Start ********/
		$promocode_discount = 0;
		/********* Promocode Start ********/

		/********* Promocode End ********/
		/********* Promocode End ********/

		//details for PGI
		$email = $post_params['billing_email'];
		$phone = $post_params['passenger_contact'];
		$verification_amount = roundoff_number($amount);
		$firstname = $post_params['first_name']['0'];
		$productinfo = META_ACCOMODATION_COURSE;

		//check current balance before proceeding further
		$domain_balance_status = $this->domain_management_model->verify_current_balance($verification_amount, $currency);
		$payment_method = $post_params['payment_method'];
		//$new_session_amount = $post_params['new_session_amount'];
		$book_id = $temp_booking['book_id'];

		$amount = $passenger_data['final_fare'] + $passenger_data['convenience_fee'];
		$wallet_bal = 'off';
		if (trim($wallet_bal) == 'on') {
			// echo "in";
			$wallet_amount = 0;
			$wallet_data = $this->user_model->get_wallet_log($GLOBALS['CI']->entity_user_id);
			// debug($wallet_data);exit;
			if (isset($wallet_data[0]) && !empty($wallet_data[0])) {

				$wallet_amount = $wallet_data[0]['closing_balance'];
				// debug($wallet_amount);
				if (($amount) > $wallet_amount) {
					// $this->flight_model->save_wallet_log($book_id,$amount,false,$wallet_amount);
					$amount = ($amount) - $wallet_amount;
				} else if (($amount) < $wallet_amount) {
					// $this->flight_model->save_wallet_log($book_id,$amount,false,$wallet_amount);
					$amount = 0;
				} else if (($amount) == $wallet_amount) {
					// $this->flight_model->save_wallet_log($book_id,$amount,false,$wallet_amount);
					$amount = 0;
				}
			}
		}
		
		$convenience_fees = 0;



		if ($domain_balance_status == true) {
			//debug($post_params['payment_method']);exit;
			switch ($post_params['payment_method']) {
			case PAY_NOW:

				$this->load->model('transaction');

				$this->load->model('transaction');
				$this->transaction->create_payment_record($book_id, $amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount);
				$pre_booking_status['status'] = SUCCESS_STATUS;
				$pre_booking_status['book_origin'] = $book_origin;
				$pre_booking_status['return_url'] = base_url() . 'index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin . '/' . $post_params['card'] . '/' . $post_params['month'] . '/' . $post_params['year'];
				//echo base_url() . 'index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin . '/' . $post_params['card'] . '/' . $post_params['month'] . '/' . $post_params['year'];exit;
				$this->output_compressed_data($pre_booking_status);
				break;

			case PAY_AT_BANK:echo 'Under Construction - Remote IO Error';exit;
				break;
			}
		} else {

			$pre_booking_status['status'] = FAILURE_STATUS;
			$pre_booking_status['data'] = array(); 
			$this->output_compressed_data($pre_booking_status);
			exit;
			
		}

	}

}
