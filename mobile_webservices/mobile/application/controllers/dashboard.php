<?php
ob_start();if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
error_reporting(0);
class Dashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
		//DO : Setting Current website url in session, 
		//Purpose : For keeping the page on login/logout.
		//Begin
		$current_url = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
        $current_url = WEB_URL.$this->uri->uri_string(). $current_url;
		$url =  array(
            'continue' => $current_url,
        );
        $this->session->set_userdata($url);
		//End
		$this->load->model('home_model');
		$this->load->model('verification_model');
		$this->load->model('account_model');
		$this->load->model('support_model');
		$this->load->model('cart_model');
		$this->load->model('Flight_Model'); 
		 $this->load->model('booking_model');  
        $this->load->model('email_model');    
		$this->load->model('general_model');
		$this->load->library('New_Ajax');
        $this->perPage = 5;
		if (!$this->session->userdata('user_id')) {
            redirect(WEB_URL);
        }
	}
		 
	
	public function index($page=''){


		if(isset($_POST['email_v']))
		{
			$data['email_v'] = $_POST['email_v'];
		}
		
		 $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
        
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);

			$data['flight_bookings'] = $this->general_model->get_bookings($user_id,$user_type,'1');
			$data['hotel_bookings'] = $this->general_model->get_bookings($user_id,$user_type,'2');

			$data['car_bookings'] = $this->general_model->get_bookings($user_id,$user_type,'7');
			$data['activity_bookings'] = $this->general_model->get_bookings($user_id,$user_type,'4');//activity

			$data['transfer_bookings'] = $this->general_model->get_bookings($user_id,$user_type,'8');//transfers

			$data['bundle_booking_count'] = count($this->account_model->get_bundle_booking_ids($user_id,$user_type));
			
			if(isset($_GET['id'])){
				$data['header_product'] = ucfirst($_GET['id']);	
			}else{
				$data['header_product'] = 'Flights';
			}
			

			
			$usertypename = $this->account_model->usertype_name($user_type);
		$data['notices'] = $this->general_model->get_notices($usertypename[0]->user_type_name);


		$this->load->view(PROJECT_THEME.'/dashboard/index',$data);	
	}
	public function get_overall_reports()
	{
		if($this->session->userdata('user_id'))
		{
			$data['user_id'] = $user_id = $this->session->userdata('user_id');
			$data['user_type']=$user_type =  $this->session->userdata('user_type');
			$data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type,1);
			$data['bookings_reports'] = $this->general_model->get_overall_reports_graph($user_id,$user_type);
			$data['recent_sales_profit'] = $this->general_model->get_recent_sales_profit($user_id,$user_type);
			$data['recent_product_sales'] = $this->general_model->get_recent_product_sales($user_id,$user_type);
			$data['get_overall_pds_graph'] = $this->general_model->get_overall_pds_graph($user_id,$user_type);
			
			 
			echo $this->load->view(PROJECT_THEME.'/dashboard/overall_graph_reports',$data);	
		}
	}
	
	public function markup($id=0){
		$post_params = $this->input->post();
		//debug($post_params);
		//exit;
		
		$this->load->model('markup_model');
		if(valid_array($post_params)){
			if($id==0){	
				//insert
				$post_params['module']='B2B';
				if($post_params['markup_type']=='gen'){
					$post_params['type']='GENERIC';
				}else{
					$post_params['type']='SPECIFIC';
				}
				
				$post_params['update_id'] = $id;
				$markup_status = $this->markup_model->add_update_markup_value($post_params);
				redirect(base_url().'dashboard/markup');
			}else{
				//update
				$post_params['module']='B2B';
				if($post_params['markup_type']=='gen'){
					$post_params['type']='GENERIC';
				}else{
					$post_params['type']='SPECIFIC';
				}
				$post_params['update_id'] = $id;
				$markup_status = $this->markup_model->add_update_markup_value($post_params);
				redirect(base_url().'dashboard/markup');
			}
		}else{
			$edit_markup_details = $this->markup_model->get_b2b_user_markup($id);
			if($edit_markup_details){
				$data['edit_markup'] = $edit_markup_details[0];
			}

		}
		
		 $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');

         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);

		 $data['product'] = $this->general_model->get_product_details();
		 // debug($data['product']);
		 // exit;

		 $data['get_markup'] = $this->general_model->get_markup_details($user_id,'B2B');
  	 	
		
	  // $data['getSMSalertList'] = $this->verification_model->getSMSalertList($user_id, $user_type); //get list of all the list available in sms_alert table
     //  $data['getSMSalertData'] = $this->verification_model->getSMSalertData($user_id, $user_type); //get joined table between sms_alert and sms_alert_enabled.
       	$data['airline_list'] = $this->Flight_Model->get_airline_list();
		$data['ID'] = $id;
		
		$this->load->view(PROJECT_THEME.'/dashboard/markup',$data);	
	}
	public function delete_markup($id=0){
		$condition=array();
		$condition['markup_id'] = $id;
		$condition['user_id'] = $this->session->userdata('user_id');
		if($this->custom_db->delete_record('markup',$condition)){
			redirect(base_url().'dashboard/markup');
		}
	}
	public function settings(){
		 $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		 $data['products'] = $this->general_model->get_product_details();
		 $data['get_markup'] = $this->general_model->get_markup_details($user_id,$user_type);
  	 
			
	   $data['getSMSalertList'] = $this->verification_model->getSMSalertList($user_id, $user_type); //get list of all the list available in sms_alert table
       $data['getSMSalertData'] = $this->verification_model->getSMSalertData($user_id, $user_type); //get joined table between sms_alert and sms_alert_enabled.
		
		$this->load->view(PROJECT_THEME.'/dashboard/settings',$data);	
	}
	public function sms_alerts(){
		 $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		 $data['products'] = $this->general_model->get_product_details();
		 $data['get_markup'] = $this->general_model->get_markup_details($user_id,$user_type);
  	 
			
	   $data['getSMSalertList'] = $this->verification_model->getSMSalertList($user_id, $user_type); //get list of all the list available in sms_alert table
       $data['getSMSalertData'] = $this->verification_model->getSMSalertData($user_id, $user_type); //get joined table between sms_alert and sms_alert_enabled.
		
		$this->load->view(PROJECT_THEME.'/dashboard/sms_alerts',$data);	
	}
	public function change_pwd($id=''){

		 $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		 //$data['products'] = $this->general_model->get_product_details();
		 //$data['get_markup'] = $this->general_model->get_markup_details($user_id,$user_type);
  	 
			
	   //$data['getSMSalertList'] = $this->verification_model->getSMSalertList($user_id, $user_type); //get list of all the list available in sms_alert table
     //  $data['getSMSalertData'] = $this->verification_model->getSMSalertData($user_id, $user_type); //get joined table between sms_alert and sms_alert_enabled.
		$data['Status'] = $id;
		$this->load->view(PROJECT_THEME.'/dashboard/change_pwd',$data);	
	}
	public function cancel_account(){
		 $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		 $data['products'] = $this->general_model->get_product_details();
		 $data['get_markup'] = $this->general_model->get_markup_details($user_id,$user_type);
  	 	
			
	   $data['getSMSalertList'] = $this->verification_model->getSMSalertList($user_id, $user_type); //get list of all the list available in sms_alert table
       $data['getSMSalertData'] = $this->verification_model->getSMSalertData($user_id, $user_type); //get joined table between sms_alert and sms_alert_enabled.
		
		$this->load->view(PROJECT_THEME.'/dashboard/cancel_account',$data);	
	}
	public function bookings($module = '')
	{
        
        $totalRec = $this->account_model->getallBookings();

        if($module != ''){
			$data['module'] = json_decode(base64_decode($module));
		} else {
			$data['module'] = '';
		}
		
        //pagination configuration
        $config['first_link']  = 'First';
        $config['div']         = 'BookingList'; //parent div tag id
        $config['base_url']    = WEB_URL.'dashboard/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
       
        $this->new_ajax->initialize($config);

		 $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
         //$data['booking']=$this->general_model->get_booking_detail($user_id);
		 $data['getoverallBookings'] = $this->account_model->getoverallBookings($user_id,$user_type)->result();
		 #echo $user_id.'<br/>';
		 #echo $user_type.'<br/>';
		 $bundle_booking_details = $this->account_model->getbundleoverallBookings($user_id,$user_type);
			
		$data['getbundleoverallBookings'] = $bundle_booking_details['bundle_booking'];

		// debug($data['getbundleoverallBookings']);
		// exit;
		$data['booking_module'] = $bundle_booking_details['module_ids'];
	     #echo "data['module']: <pre/>";print_r($data);exit();

		 $this->load->view(PROJECT_THEME.'/dashboard/bookings',$data);	
	}
    function ajaxPaginationData()
    {
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec =  $this->account_model->getallBookings();
        
        //pagination configuration
        $config['first_link']  = 'First';
        $config['div']         = 'BookingList'; //parent div tag id
        $config['base_url']    = WEB_URL.'dashboard/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        
        $this->new_ajax->initialize($config);
        
        //get the posts data
        $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
         $data['getoverallBookings'] = $this->account_model->getoverallBookings($user_id,$user_type,array('start'=>$offset,'limit'=>$this->perPage))->result();
        
        //load the view
        $this->load->view(PROJECT_THEME.'/dashboard/bookings_pagination', $data, false);
    }
	public function profile($subpage='')
	{
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
        $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		$data['address_details'] = $this->account_model->get_address_details($user_id,$user_type);
		$data['baddress_details'] = $this->account_model->get_billing_address_details($user_id,$user_type);
		
		$data['baddress_details'] = $this->account_model->get_billing_address_details($user_id,$user_type);
		
		$data['getoverallBookings'] = $this->account_model->getoverallBookings($user_id,$user_type)->result();
		
		$data['getCountry'] = $this->general_model->getCountryList();
		if($subpage=='')
		{
		 	$subpage = 'profile';
		}
  	 	$data['subpage']=$subpage;
		$this->load->view(PROJECT_THEME.'/dashboard/profile',$data);		
	}
	public function edit_profile($subpage=''){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
        $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
       
		$data['address_details'] = $this->account_model->get_address_details($user_id,$user_type);
		$data['baddress_details'] = $this->account_model->get_billing_address_details($user_id,$user_type);
		
		$data['baddress_details'] = $this->account_model->get_billing_address_details($user_id,$user_type);
		
		$data['getoverallBookings'] = $this->account_model->getoverallBookings($user_id,$user_type)->result();
		
		$data['getCountry'] = $this->general_model->getCountryList();
		if($subpage=='')
		{
		 	$subpage = 'profile';
		}
  	 	$data['subpage']=$subpage;
  	 	$admin_details = $this->booking_model->get_admin_details();
  	 	$data['admin_country_code'] = $admin_details->country_code;
		$this->load->view(PROJECT_THEME.'/dashboard/profile/edit_profile',$data);		
	}
	
	public function verification($subpage=''){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
        $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		$data['address_details'] = $this->account_model->get_address_details($user_id,$user_type);
		$data['baddress_details'] = $this->account_model->get_billing_address_details($user_id,$user_type);
		
		$data['baddress_details'] = $this->account_model->get_billing_address_details($user_id,$user_type);
		
		$data['getoverallBookings'] = $this->account_model->getoverallBookings($user_id,$user_type)->result();
		
		$data['getCountry'] = $this->general_model->getCountryList();
		if($subpage=='')
		{
		 	$subpage = 'profile';
		}
  	 	$data['subpage']=$subpage;
		$this->load->view(PROJECT_THEME.'/dashboard/profile/verifications',$data);		
	}
	
	public function newsletter($subpage=''){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
        $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		
		   $data['getNewsletterStatus'] = $this->account_model->getNewsletterStatus($user_id,$user_type)->num_rows();
		   
	 
		if($subpage=='')
		{
		 	$subpage = 'newsletter';
		}
  	 	$data['subpage']=$subpage;
		$this->load->view(PROJECT_THEME.'/dashboard/newsletter',$data);		
	}
	  
    public function updateProfile(){
       $data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
        $data['userInfo'] = $userInfo = $this->general_model->get_user_details($user_id,$user_type);
            
       
        
       /* if($this->input->post('em_name') != '' || $this->input->post('em_phone') != '' || $this->input->post('em_email') != '' || $this->input->post('em_rel') != ''){
            $count = $this->account_model->getUserEmergency($user_type,$user_id)->num_rows();
            if($count == 1){
                $emergency = array(
                    'name' => $this->input->post('em_name'),
                    'phone' => $this->input->post('em_phone'),
                    'email' => $this->input->post('em_email'),
                    'relation' => $this->input->post('em_rel')
                );
                $this->account_model->update_emergency_contact($emergency, $user_type, $user_id);
            }else{
                $emergency = array(
                    'user_type' => $user_type,
                    'user_id' => $user_id,
                    'name' => $this->input->post('em_name'),
                    'phone' => $this->input->post('em_phone'),
                    'email' => $this->input->post('em_email'),
                    'relation' => $this->input->post('em_rel')
                );
                $this->account_model->createEmergency($emergency);
            }
        }*/
        
         $update = array(
                'user_name' => $this->input->post('fname'),
                'mobile_phone'=>$this->input->post('phone')
            );
            $this->account_model->update_user($update, $user_id);
         $update2 = array(
                
                'address' => $this->input->post('address'),
                'city_name' => $this->input->post('city_name'),
                'state_name' => $this->input->post('state_name'),
                'zip_code' => $this->input->post('zip_code'),
                'country_code' => $this->input->post('country'),
            );
            $this->account_model->update_user_address($update2, $userInfo->address_details_id);
        

        echo json_encode(array('status' => '1', 'msg' => 'Changes has been saved!!'));
        //redirect(WEB_URL.'dashboard/profile');
    }

	 public function update_user_profile_image(){
	 
	 	$file_type_arr = explode(".",$_FILES['profilePhoto']['name']);

	 	$data =array();
	 	if(count($file_type_arr)>=3){
	 		$data['status']=false;
	 		$data['message'] ='Incorrect Image types';
	 		$data['img'] = '';
	 		echo json_encode($data);

	 	}elseif(count($file_type_arr)==2){
	 		
	 		if($_FILES['profilePhoto']['error'] == 0 && $_FILES['profilePhoto']['size'] > 0){

	 			if(function_exists("check_mime_image_type")){

	 				if(!check_mime_image_type($_FILES["profilePhoto"]["tmp_name"])){
	 					$data['status']=false;
				 		$data['message'] ='You have selected incorrect image file';
				 		$data['img'] = '';
				 		echo json_encode($data);
				 		exit;
	 				}
	 				$ext = end(explode('/', $_FILES["profilePhoto"]["type"]));
			        $tmp_name = $_FILES["profilePhoto"]["tmp_name"];       
					$newlogoname = date("dmHis").rand(1,99999) . '.' .$ext;
				    move_uploaded_file($tmp_name, 'photo/users/'.$newlogoname);
					$image = $newlogoname;
			        
			        if($this->session->userdata('user_id')){    //identify the session
			            $data['user_type']=$user_type =$this->session->userdata('user_type');
			            $data['user_id']=$user_id = $this->session->userdata('user_id');
			        } 

			        $userInfo =  $this->general_model->get_user_details($user_id,$user_type);
			        $email = $userInfo->user_email; 
					$old_image = $userInfo->profile_picture;
					
					$update = array('profile_picture' => $image);
			        $profile_photo = $this->account_model->update_user($update, $user_id );

			        echo json_encode(array('status'=>true,'message'=>'upload success','img' => UPLOAD_PATH."users/".$image));
	 			}

 				
	 		}

	 		
	 	}
	 
	 	exit;
	 
        
    }

	public function deposit($msg_value=0){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
        $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
        $data['deposit'] = $this->account_model->agent_deposit_details($user_id)->result();
        $data['deposit_amount'] = $this->account_model->get_deposit_amount($user_id)->row();
        if($msg_value==2){
        	$data['err_v'] = 'Image Not uploaded,please select correct image type';
        }elseif($msg_value==1){
        	$data['scs_v'] = 'Deposit Updated Succcess';
        }
		$this->load->view(PROJECT_THEME.'/dashboard/deposit',$data);		
	}
	public function deposit_control(){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);

            
            $data['deposit'] = $this->account_model->agent_deposit_details($user_id)->result();
            $data['deposit_amount'] = $this->account_model->get_deposit_amount($user_id)->row();

		$this->load->view(PROJECT_THEME.'/dashboard/deposit_control',$data);		
	}
	public function support_conversation(){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
        $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
        $data['support_ticket_subject'] = $this->support_model->get_support_subject_list(); 
        $data['support'] = $this->support_model->get_support_list($user_id); 
        $data['support_pending'] = $this->support_model->get_support_list_pending($user_id); 
        $data['support_sent'] = $this->support_model->get_support_list_sent($user_id); 
        $data['support_close'] = $this->support_model->get_support_list_close($user_id); 
            $this->load->view(PROJECT_THEME.'/dashboard/support_conversation',$data);
	
	}
	public function ticket_inbox(){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
        $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
        $data['support_ticket_subject'] = $this->support_model->get_support_subject_list(); 
        $data['support'] = $this->support_model->get_support_list($user_id); 
        $data['support_pending'] = $this->support_model->get_support_list_pending($user_id); 
        $data['support_sent'] = $this->support_model->get_support_list_sent($user_id); 
        $data['support_close'] = $this->support_model->get_support_list_close($user_id); 
            $this->load->view(PROJECT_THEME.'/dashboard/ticket_inbox',$data);
	
	}
	public function sent_ticket(){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
        $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
        $data['support_ticket_subject'] = $this->support_model->get_support_subject_list(); 
        $data['support'] = $this->support_model->get_support_list($user_id); 
        $data['support_pending'] = $this->support_model->get_support_list_pending($user_id); 
        $data['support_sent'] = $this->support_model->get_support_list_sent($user_id); 
        $data['support_close'] = $this->support_model->get_support_list_close($user_id); 
            $this->load->view(PROJECT_THEME.'/dashboard/sent_ticket',$data);
	
	}
	public function closed_tickets(){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
        $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
        $data['support_ticket_subject'] = $this->support_model->get_support_subject_list(); 
        $data['support'] = $this->support_model->get_support_list($user_id); 
        $data['support_pending'] = $this->support_model->get_support_list_pending($user_id); 
        $data['support_sent'] = $this->support_model->get_support_list_sent($user_id); 
        $data['support_close'] = $this->support_model->get_support_list_close($user_id); 
            $this->load->view(PROJECT_THEME.'/dashboard/closed_tickets',$data);
	
	}
	

    function add_new_ticket(){
		if($this->session->userdata('user_id')){
			$data['user_type']=$user_type = $this->session->userdata('user_type');
			$data['user_id']=$user_id = $this->session->userdata('user_id');
		} else {
			 redirect(WEB_URL,'refresh');
		}

		$sub = $this->input->post('subject');
		$message = $this->input->post('message');
		$config['upload_path'] = './admin-panel/uploads/support_ticket/';
		$config['allowed_types'] = '*';
		$this->load->library('upload', $config);
		if($_FILES['file_name']['name']!=''){
			if ( ! $this->upload->do_upload('file_name')){
				$error = array('error' => $this->upload->display_errors());
				$data['status'] ='<div class="alert alert-block alert-danger">
				<a href="#" data-dismiss="alert" class="close">×</a>
				<h4 class="alert-heading">Attachment File Failed!</h4>
				'.$error['error'].'</div>';
				$data['user_id'] = $user_id = $this->session->userdata('user_id');
				$data['user_type']=$user_type =  $this->session->userdata('user_type');
				$data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
				$data['support_ticket_subject'] = $this->support_model->get_support_subject_list(); 
				$data['support'] = $this->support_model->get_support_list($user_id); 
				$data['support_pending'] = $this->support_model->get_support_list_pending($user_id); 
				$data['support_sent'] = $this->support_model->get_support_list_sent($user_id); 
				$data['support_close'] = $this->support_model->get_support_list_close($user_id); 
				$this->load->view(PROJECT_THEME.'/dashboard/support_conversation',$data);
			}
			$cc = $this->upload->data('file');
			$image_path = 'uploads/support_ticket/'.$cc['file_name'];
		} else {
			$image_path = '';
		}
		$this->support_model->add_new_support_ticket( $user_id,$sub,$message,$image_path);
		
		echo json_encode(array('status' => 1));exit;
		// redirect(WEB_URL."dashboard/support_conversation",'refresh');
	}
   public  function download_file($file){
        $this->load->helper('download');
            $name=  base64_decode($file);
       
        $data = file_get_contents($name); // Read the file's contents
       
        $a = explode('support_ticket', $name);
        $name1 = substr($a[1],2);
        if($data != ''){
            force_download($name1, $data); 
        }else{
            redirect(WEB_URL.'dashboard/support_conversation/','refresh');
        }
    }
  public function close_ticket($sid){
        $this->support_model->close_ticket($sid); 
        $response = array('status'=> '1');
        echo json_encode($response);
       // redirect("dashboard/support_conversation","refresh");
    }
    
    public function view_ticket($id){

  			$data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);

       
        $data['status']='';
        $data['ticket'] = $this->support_model->get_support_list_id($user_id,$id); 
        $data['ticketrow'] = $this->support_model->get_support_list_id_row($user_id,$id); 
        $data['id']=$id;
// echo '<pre/>';
// print_r($data);exit;
        $tickets = $this->load->view(PROJECT_THEME.'/dashboard/support_view_ticket',$data,TRUE);
        $response = array('tickets' => $tickets,'status'=> '1');
        echo json_encode($response);

    }
	function reply_ticket($id){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
		$ticketrow = $this->support_model->get_support_list_id_row($user_id,$id); 
		$user = $ticketrow->user_id;
		$message = $this->input->post('message');
		$support_ticket_id = $ticketrow->support_ticket_id;
		$config['upload_path'] = './admin-panel/uploads/support_ticket/';
		$config['allowed_types'] = '*';
		$this->load->library('upload', $config);
		/* $config['upload_path'] = './uploads/tickets';
		 * $config['allowed_types'] = 'gif|jpg|png';
		 * $config['max_size'] = '100';
		 * $config['max_width']  = '0';
		 * $config['max_height']  = '0';
		 * $this->load->library('upload', $config);*/
		foreach($_FILES as $field => $file){
			if($file['error'] == 0){
				if ($this->upload->do_upload($field)){
					$data = $this->upload->data();
					$cc = $this->upload->data('file');
					$image_path = 'uploads/support_ticket/'.$cc['file_name'];
				}else{
					$image_path = '';
				}
			}else{
				$image_path = '';
			}
		}
		/*if($_FILES['rfile_name']['name']!=''){
		 * if ( ! $this->upload->do_upload('rfile_name')){
		 * $error = array('error' => $this->upload->display_errors());
		 * $data['status'] ='<div class="alert alert-block alert-danger">
		 * <a href="#" data-dismiss="alert" class="close">×</a>
		 * <h4 class="alert-heading">Attachment File Failed!</h4>'.$error['error'].'</div>';
		 * $data['ticket'] = $this->Support_Model->get_support_list_id($id); 
		 * $data['ticketrow'] = $this->Support_Model->get_support_list_id_row($id); 
		 * $data['id']=$id;
		 * $this->load->view('dashboard/support_conversation',$data);
		 * }
		 * $cc = $this->upload->data('file');
		 * $image_path = 'uploads/support_ticket/'.$domain.'/'.$cc['file_name'];
		 }else{ $image_path = ''; }*/
            
            $last_id = $this->support_model->add_new_support_ticket_updates($support_ticket_id,$message,$image_path);
            $data['ticket'] = $this->support_model->getSupportHistoryRow($last_id);
            $ticketrow = $this->support_model->get_support_list_id_row($ticketrow->user_id,$id);
				$data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
			$data['user_details'] = $this->general_model->get_user_details($user_id,$user_type);
			 
            $tickets = $this->load->view(PROJECT_THEME.'/dashboard/support_view_ticket_ajax',$data,TRUE);
            $response = array('tickets' => $tickets,'status'=> '1','id'=>$id);
            echo json_encode($response);
            //redirect('dashboard/support_conversation/'.$id,'refresh');
    }
    
	function check_b2b_user(){
		 $user_id = $this->session->userdata('user_id');
		 $user_type =  $this->session->userdata('user_type');
		$userInfo = $this->general_model->get_user_details($user_id,$user_type);
		if($userInfo->user_type_name != 'B2B')
		{
			redirect(WEB_URL.'dashboard');
		}
		
		
	}
	public function account_statement(){
		$this->check_b2b_user();
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
		$data['user_type']=$user_type =  $this->session->userdata('user_type');
		$data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		$data['deposit'] = $this->account_model->agent_deposit_details($user_id)->result();
		$data['deposit_amount'] = $this->account_model->get_deposit_amount($user_id)->row();
		$data['account_statment_data'] = $this->account_model->get_account_statment($user_id)->result();
		// debug($data['account_statment_data']);
		// exit;
		$this->load->view(PROJECT_THEME.'/dashboard/account_statement',$data);		
	}
	  public function booking_statement(){
		$this->check_b2b_user();
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);

            
        $data['deposit'] = $this->account_model->agent_deposit_details($user_id)->result();
 		$data['deposit_amount'] = $this->account_model->get_deposit_amount($user_id)->row();

       
  		$data['Bookings'] =  $this->account_model->getoverallBookings($user_id, $user_type)->result();
	   	
	   	// debug($data['Bookings']);
	   	// exit;
		$this->load->view(PROJECT_THEME.'/dashboard/booking_statement',$data);		
	}
	 public function profit_matrix(){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
        $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
        $data['deposit'] = $this->account_model->agent_deposit_details($user_id)->result();
 		$data['deposit_amount'] = $this->account_model->get_deposit_amount($user_id)->row();

       
  		$data['Bookings'] = $this->account_model->getoverallBookings($user_id, $user_type)->result();
		
		$data['products'] = $this->account_model->get_products();
	   	
	   
		$this->load->view(PROJECT_THEME.'/dashboard/profit_matrix',$data);		
	}
	public function traveller(){
		$this->load->view(PROJECT_THEME.'/dashboard/traveller');	
	}
	public function b2b_dashboard(){
		$this->load->view(PROJECT_THEME.'/dashboard/b2b_dashboard');	
	}
	 
	 public function get_booking_data(){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		 $data['booking'] = $this->account_model->getBookingbyPnr($_REQUEST['pnr'],$_REQUEST['module'])->row();
		 $this->load->view(PROJECT_THEME.'/dashboard/booking_details',$data);
	 }
	 
	 public function add_employee(){
		
		 $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		$this->load->view(PROJECT_THEME.'/dashboard/b2b2b/add_employee',$data);	
	}
	 public function employee_list(){
		
		 $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
         $data['employee_list'] = $this->general_model->get_employees($user_id);
		$this->load->view(PROJECT_THEME.'/dashboard/b2b2b/employee_list',$data);	
	}
	 public function employee_bookings(){
		
		 $data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		$this->load->view(PROJECT_THEME.'/dashboard/b2b2b/employee_bookings',$data);	
	}
	function call_iternary(){
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
         $data['user_type']=$user_type =  $this->session->userdata('user_type');
         $data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
         $data['module']	= $_REQUEST['module'];
		 $data['booking']  = $booking = $this->account_model->getBookingbyPnr($_REQUEST['pnr'],$_REQUEST['module'])->row();
		 if($_REQUEST['module'] == 'HOTEL')
		 $data['cart']	= $cart = $this->cart_model->getBookingTemphotel($booking->shopping_cart_hotel_id);
		$this->load->view(PROJECT_THEME.'/dashboard/booking_details',$data); 
   	}
   	
   	public function update_user_status(){
		$id = $_GET['id'];
		$status = $_GET['status'];

		//$this->email_model->send_status_email($id, $status);
        if($this->account_model->update_user_status($id, $status)){
			$response = array('status' => 1);
        	echo json_encode($response);
		} else {
			$response = array('status' => 0);
        	echo json_encode($response);
		}
	}	
	public function view_profile($user_id, $user_type){
		$data['userInfo'] = $this->general_model->get_user_details($user_id,$user_type);
		$data['address_details'] = $this->account_model->get_address_details($user_id,$user_type);
		$data['baddress_details'] = $this->account_model->get_billing_address_details($user_id,$user_type);		
		$data['baddress_details'] = $this->account_model->get_billing_address_details($user_id,$user_type);		
		$data['getoverallBookings'] = $this->account_model->getoverallBookings($user_id,$user_type)->result();		
		$data['getCountry'] = $this->general_model->getCountryList();
		
		$this->load->view(PROJECT_THEME.'/dashboard/b2b2b/employee_profile',$data);
	}




	public function mail($pnr_no='',$con_pnr_no='',$booking_global_id=''){	
		$this->load->library('provab_mailer');

		
		if($booking_global_id){

		 $count = $this->booking_model->getBookingPnr($pnr_no,$con_pnr_no,$booking_global_id)->num_rows();	 
		}else{
			$count = $this->booking_model->getBookingPnr($pnr_no,$con_pnr_no)->num_rows();	
		}
		
        if($count == 1){

        	if($booking_global_id){
        		 $b_data = $this->booking_model->getBookingPnr($pnr_no,$con_pnr_no,$booking_global_id)->row();	
        	}else{
        		 $b_data = $this->booking_model->getBookingPnr($pnr_no,$con_pnr_no)->row();
        	}

          	$admin_details = $this->booking_model->get_admin_details();
         	$data['admin_details'] = $admin_details;
            if($b_data->product_name == 'FLIGHT'){
            //  $data['terms_conditions'] = $this->booking_model->get_terms_conditions($product_id);
        	if($booking_global_id){
        		$data['b_data'] = $this->booking_model->getBookingPnr($pnr_no,$con_pnr_no,$booking_global_id)->row();   
        	}else{
        		$data['b_data'] = $this->booking_model->getBookingPnr($pnr_no,$con_pnr_no)->row();   
        	}
             
	           $booking_global_id=$b_data->booking_global_id;
	           $billing_address_id=$b_data->billing_address_id;
             
                $data['Passenger'] = $passenger = $this->booking_model->getPassengerbyid($booking_global_id)->result();
                $data['booking_agent'] = $passenger = $this->booking_model->getagentbyid($billing_address_id)->result();
                $data['message'] = $this->load->view(PROJECT_THEME.'/booking/mail_voucher', $data,TRUE);

                $data['to'] = $data['booking_agent'][0]->billing_email;           
                //$data['to'] = 'elavarasi.k@provabmail.com';
                $data['booking_status'] = $b_data->booking_status;
                $data['pnr_no'] = $pnr_no;
                $data['email_access'] = $this->email_model->get_email_acess()->row();
                $email_type = 'FLIGHT_BOOKING_VOUCHER';
                 // echo "b_data:<pre/>";print_r($data);exit();
                
                $Response = $this->email_model->sendmail_flightVoucher($data);
               
              //   echo "b_data:<pre/>";print_r($Response);exit();
                $response = array('status' => 1);
                //echo json_encode($response);
            }
            
            else if($b_data->product_name == 'HOTEL'){
            //  $data['terms_conditions'] = $this->booking_model->get_terms_conditions($product_id);
            	if($booking_global_id){
            		$data['b_data'] = $this->booking_model->getBookingPnr($pnr_no,$con_pnr_no,$booking_global_id)->row();
            	}else{
            		$data['b_data'] = $this->booking_model->getBookingPnr($pnr_no,$con_pnr_no)->row();
            	}
	                

	           $booking_global_id=$b_data->booking_global_id;
	           $billing_address_id=$b_data->billing_address_id;
             
                 $data['Passenger'] = $passenger = $this->booking_model->getPassengerbyid($booking_global_id)->result();
                 $data['booking_agent'] = $passenger = $this->booking_model->getagentbyid($billing_address_id)->result();
                 

                 $data['Booking'] = $bookinginfo = $this->booking_model->getBookingbyPnr($pnr_no,$b_data->product_name,$b_data->con_pnr_no)->row();

                 $data['cart']	= $cart = $this->cart_model->getBookingTemphotel($bookinginfo->shopping_cart_hotel_id);

                 $data['message'] = $this->load->view(PROJECT_THEME.'/booking/hotel_mail_voucher', $data,TRUE);

                
                 
              //   print_r($data);  exit;
               
                $data['to'] = $data['booking_agent'][0]->billing_email;               
                $data['booking_status'] = $b_data->booking_status;
                $data['pnr_no'] = $pnr_no;
                $data['email_access'] = $this->email_model->get_email_acess()->row();
                $email_type = 'HOTEL_BOOKING_VOUCHER';
                $data['mail_from'] = 'Hotel';

                $Response = $this->email_model->sendmail_hotelVoucher($data);
                $response = array('status' => 1);
                //echo json_encode($response);
            }
            elseif ($b_data->product_name =='ACTIVITY') {
            	$this->load->model('payment_model');
            	  $data['b_data'] =  $this->booking_model->getBookingPnr($pnr_no)->row();  
        	   	$data['Booking'] =$this->booking_model->getBookingbyPnr($pnr_no,$b_data->product_name)->row();
	           $booking_global_id=$b_data->booking_global_id;
	           $billing_address_id=$b_data->billing_address_id;

                 $data['Passenger'] = $passenger = $this->booking_model->getPassengerbyid($booking_global_id)->result_array();
                 $data['booking_agent'] = $passenger = $this->booking_model->getagentbyid($billing_address_id)->result();
                 
                 $data['booking_info'] = $bookinginfo = $this->account_model->getBookingbyPnr($pnr_no,$b_data->product_name)->row();
                 $data['cart']	= $cart = $this->cart_model->getBookingTemp_SIGHTSEEING($bookinginfo->referal_id);

				$data['pnr_nos'] = $this->booking_model->getBookingByParentPnr($data['Booking']->parent_pnr_no)->result();
				$global_ids = $this->payment_model->validate_order_id_org($data['Booking']->parent_pnr_no)->result();
				$data['cart'] = $this->db->get_where('booking_activity', array('booking_sightseeing_id' => $global_ids[0]->referal_id))->row();
                 $data['mail_voucher'] = true;
                 $data['message'] = $this->load->view(PROJECT_THEME.'/transferv1/voucher', $data,TRUE);
                $data['to'] = $data['booking_agent'][0]->billing_email;               
                $data['booking_status'] = $b_data->booking_status;
                $data['pnr_no'] = $pnr_no;
                $data['email_access'] = $this->email_model->get_email_acess()->row();
                $email_type = 'ACTIVITY_BOOKING_VOUCHER';
                $data['mail_from'] = 'Activity';
        		
                $Response = $this->email_model->sendmail_ActivityTransferVoucher($data,'Activity');
                
                $response = array('status' => 1);

            }else if($b_data->product_name =='TRANSFER'){
            	$this->load->model('payment_model');
            	  $data['b_data'] =  $this->booking_model->getBookingPnr($pnr_no)->row();  
        	   	$data['Booking'] =$this->booking_model->getBookingbyPnr($pnr_no,$b_data->product_name)->row();
	           $booking_global_id=$b_data->booking_global_id;
	           $billing_address_id=$b_data->billing_address_id;

                 $data['Passenger'] = $passenger = $this->booking_model->getPassengerbyid($booking_global_id)->result_array();
                 $data['booking_agent'] = $passenger = $this->booking_model->getagentbyid($billing_address_id)->result();
                 
                 $data['booking_info'] = $bookinginfo = $this->account_model->getBookingbyPnr($pnr_no,$b_data->product_name)->row();
                 $data['cart']	= $cart = $this->cart_model->getBookingTemp_transfer($bookinginfo->referal_id);

				$data['pnr_nos'] = $this->booking_model->getBookingByParentPnr($data['Booking']->parent_pnr_no)->result();
				$global_ids = $this->payment_model->validate_order_id_org($data['Booking']->parent_pnr_no)->result();
				$data['cart'] = $this->db->get_where('booking_transfer', array('booking_transfer_id' => $global_ids[0]->referal_id))->row();
                 $data['mail_voucher'] = true;
                 $data['message'] = $this->load->view(PROJECT_THEME.'/transferv1/voucher', $data,TRUE);
                 
               
               
                $data['to'] = $data['booking_agent'][0]->billing_email;               
                $data['booking_status'] = $b_data->booking_status;
                $data['pnr_no'] = $pnr_no;
                $data['email_access'] = $this->email_model->get_email_acess()->row();
                $email_type = 'TRANSFER_BOOKING_VOUCHER';
                $data['mail_from'] = 'Transfer';
        		
                $Response = $this->email_model->sendmail_ActivityTransferVoucher($data,'Transfer');
                
                $response = array('status' => 1);
            }
            elseif ($b_data->product_name =='CAR') {
            	if($booking_global_id){
            		 $data['Booking']= $data['b_data']= $this->booking_model->getBookingPnr($pnr_no,$con_pnr_no,$booking_global_id)->row();
            		}else{
            			 $data['Booking']= $data['b_data']= $this->booking_model->getBookingPnr($pnr_no,$con_pnr_no)->row();
            		}
            	

		         $booking_global_id=$data['Booking']->booking_global_id;
		          $billing_address_id=$data['Booking']->billing_address_id;

		         $data['Passenger'] = $passenger = $this->booking_model->getPassengerbyid($booking_global_id)->result();
		         $data['booking_agent'] = $passenger = $this->booking_model->getagentbyid($billing_address_id)->result();
		          $data['mail_voucher'] = true;
		          $data['message'] =  $this->load->view(PROJECT_THEME.'/booking/car_voucher_view', $data,TRUE);
		       
		         $data['to'] = $data['booking_agent'][0]->billing_email;               
	                $data['booking_status'] = $b_data->booking_status;
	                $data['pnr_no'] = $pnr_no;
	                $data['email_access'] = $this->email_model->get_email_acess()->row();
	                $email_type = 'CAR_BOOKING_VOUCHER';
	                $data['mail_from'] = 'Car';
                 $Response = $this->email_model->sendmail_ActivityTransferVoucher($data,'Car');
                
                $response = array('status' => 1);


            }
        }else{
            $response = array('status' => 0);
            echo json_encode($response);
        }

        echo json_encode($response);
        exit;

    }

	public function inner_header($product){

		$data['header_product'] 	= 	$product;
		
		redirect(base_url().'dashboard?id='.strtolower($product));
	}

	 
}


?>
