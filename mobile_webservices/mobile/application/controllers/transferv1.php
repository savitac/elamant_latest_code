<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 *
 * @package    Provab
 * @subpackage Transfers
 * @author     Elavarasi<elavarasi.k@provabmial.com>
 * @version    V1
 */
error_reporting(0);
class Transferv1 extends CI_Controller
{
    private $country_code = '1';
    private $current_module;
    public function __construct()
    {
        parent::__construct();
        //we need to activate transfer api which are active for current domain and load those libraries
        $this->load->model('transferv1_model');
        $this->load->library('social_network/facebook'); //Facebook Library to enable login button
        //$this->output->enable_profiler(TRUE);
        $this->current_module = $this->config->item('current_module');
    }

    /**
     * index page of application will be loaded here
     */
    public function index()
    {
        //    echo number_format(0, 2, '.', '');
    }

    /**
     * Elavarasi
     * Load Transfers Search Result
     * @param number $search_id unique number which identifies search criteria given by user at the time of searching
     */
    public function search($search_id)
    {
        $safe_search_data = $this->transferv1_model->get_safe_search_data($search_id, META_TRANSFERV1_COURSE);

        // Get all the hotels bookings source which are active
        $active_booking_source = $this->transferv1_model->active_booking_source();

        if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
            $safe_search_data['data']['search_id'] = abs($search_id);
            $this->template->view('transferv1/search_result_page', array('sight_seen_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
        } else {
            $this->template->view('general/popup_redirect');
        }
    }

    /**
     *  Elavarasi
     * Get Product Details
     */
    public function mobile_transfer_details()
    {
        $transfer_details = $this->input->post('transfer_details');
        if (empty($transfer_details)) {
            $data['status']  = FAILURE_STATUS;
            $data['message'] = "Please enter the details to search";
            echo json_encode($data);
            exit;
        }
        //debug($transfer_details); exit;
        //echo PROVAB_TRANSFERV1_BOOKING_SOURCE;exit;
        $params = json_decode($transfer_details, true);
        #debug($params);exit;
        $safe_search_data                      = $this->transferv1_model->get_safe_search_data($params['search_id'], META_TRANSFERV1_COURSE);
        $safe_search_data['data']['search_id'] = abs($params['search_id']);
        $currency_obj                          = new Currency(array('module_type' => 'transferv1', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
        $search_id                             = $params['search_id'];
        if (isset($params['booking_source']) == true) {
            //We will load different page for different API providers... As we have dependency on API for hotel details page
            load_transferv1_lib($params['booking_source']);
            if ($params['booking_source'] == PROVAB_TRANSFERV1_BOOKING_SOURCE && isset($params['result_token']) == true and isset($params['op']) == true and
                $params['op'] == 'get_details' and $safe_search_data['status'] == true) {

                $params['result_token'] = urldecode($params['result_token']);

                $raw_product_deails = $this->transferv1_lib->get_product_details($params);

                if ($raw_product_deails['status']) {

                    $calendar_availability_date = $this->enable_calendar_availability($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Product_available_date']);

                    $raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Calendar_available_date'] = $calendar_availability_date;

                    if ($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Price']) {
                        //details data in preffered currency
                        $Price = $this->transferv1_lib->details_data_in_preffered_currency($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Price'], $currency_obj, 'b2c');

                        $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

                        //calculation Markup
                        $raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Price'] = $this->transferv1_lib->update_booking_markup_currency($Price, $currency_obj, $search_id);

                        $token['AdditionalInfo']   = base64_encode(json_encode($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['AdditionalInfo']));
                        $token['Inclusions']       = base64_encode(json_encode($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Inclusions']));
                        $token['Exclusions']       = base64_encode(json_encode($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Exclusions']));
                        $token['ShortDescription'] = base64_encode(json_encode($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['ShortDescription']));
                        $token['voucher_req']      = base64_encode(json_encode($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['voucher_req']));

                        $response['status'] = SUCCESS_STATUS;
                        $response['data']   = array('currency_obj' => $currency_obj,
                            'product_details'                          => $raw_product_deails['data']['ProductDetails']['TransferInfoResult'],
                            'tokens_data'                              => $token,
                            'search_params'                            => $safe_search_data['data'],
                            'search_id'                                => $search_id,
                            'active_booking_source'                    => $params['booking_source'],
                            'params'                                   => $params);
                    }

                    /*        $this->template->view('transferv1/viator/sightseeing_details', array('currency_obj' => $currency_obj, 'product_details' => $raw_product_deails['data']['ProductDetails']['TransferInfoResult'], 'search_params' => $safe_search_data['data'], 'search_id'=>$search_id,'active_booking_source' => $params['booking_source'], 'params' => $params));*/
                } else {
                    $response['status'] = FAILURE_STATUS;
                    $response['data']   = $raw_product_deails['Message'];
                }
            } else {
                $response['status'] = FAILURE_STATUS;
                $response['data']   = 'invalid request';
            }
        } else {
            $response['status'] = FAILURE_STATUS;
            $response['data']   = 'invalid booking source';
        }

        $this->output_compressed_data($response);
    }

    /**
     *  Elavarasi
     * Get Product Details
     */
    public function transfer_details()
    {
        $params = $this->input->get();
        #debug($params);exit;
        $safe_search_data                      = $this->transferv1_model->get_safe_search_data($params['search_id'], META_TRANSFERV1_COURSE);
        $safe_search_data['data']['search_id'] = abs($params['search_id']);
        $currency_obj                          = new Currency(array('module_type' => 'transferv1', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
        $search_id                             = $params['search_id'];
        if (isset($params['booking_source']) == true) {
            //We will load different page for different API providers... As we have dependency on API for hotel details page
            load_transferv1_lib($params['booking_source']);
            if ($params['booking_source'] == PROVAB_TRANSFERV1_BOOKING_SOURCE && isset($params['result_token']) == true and isset($params['op']) == true and
                $params['op'] == 'get_details' and $safe_search_data['status'] == true) {

                $params['result_token'] = urldecode($params['result_token']);

                $raw_product_deails = $this->transferv1_lib->get_product_details($params);
                // debug($raw_product_deails);
                // exit;
                if ($raw_product_deails['status']) {

                    $calendar_availability_date = $this->enable_calendar_availability($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Product_available_date']);

                    $raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Calendar_available_date'] = $calendar_availability_date;

                    if ($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Price']) {
                        //details data in preffered currency
                        $Price = $this->transferv1_lib->details_data_in_preffered_currency($raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Price'], $currency_obj, 'b2c');

                        $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

                        //calculation Markup
                        $raw_product_deails['data']['ProductDetails']['TransferInfoResult']['Price'] = $this->transferv1_lib->update_booking_markup_currency($Price, $currency_obj, $search_id);

                    }

                    $this->template->view('transferv1/viator/sightseeing_details', array('currency_obj' => $currency_obj, 'product_details' => $raw_product_deails['data']['ProductDetails']['TransferInfoResult'], 'search_params' => $safe_search_data['data'], 'search_id' => $search_id, 'active_booking_source' => $params['booking_source'], 'params' => $params));
                } else {

                    $msg = $raw_product_deails['Message'];

                    redirect(base_url() . 'index.php/transferv1/exception?op=' . $msg . '&notification=session');
                }
            } else {
                redirect(base_url());
            }
        } else {
            redirect(base_url());
        }
    }
    /**
     *Elavarasi
     *Enable Particular day only in calendar
     */
    public function enable_calendar_availability($calendar_availability_date)
    {

        if (valid_array($calendar_availability_date)) {
            $available_str = array();
            foreach ($calendar_availability_date as $m_key => $m_value) {
                $avail_month_str = $m_key;

                foreach ($m_value as $d_key => $d_value) {
                    //j- to remove 0 from date, n to remove 0 from month
                    $date_str        = $avail_month_str . '-' . $d_value;
                    $available_str[] = date('j-n-Y', strtotime($date_str));

                }
            }

            return $available_str;
        } else {
            return '';
        }
    }

    /**
     *Elavarasi
     *Getting Available Tourgrade Based on Passenger Mix
     */
    public function mobile_select_tourgrade()
    {
        $post_params = $this->input->post('select_tourgrade');
        if (empty($post_params)) {
            $data['status']  = FAILURE_STATUS;
            $data['message'] = "Please enter the details to search";
            echo json_encode($data);
            exit;
        }
        //echo PROVAB_TRANSFERV1_BOOKING_SOURCE;exit;
        $post_params        = json_decode($post_params, true);
        $response['data']   = '';
        $response['msg']    = '';
        $response['status'] = FAILURE_STATUS;
        $safe_search_data   = $this->transferv1_model->get_safe_search_data($post_params['search_id'], META_TRANSFERV1_COURSE);
        //debug($safe_search_data);exit;
        if (trim($post_params['product_code']) != '') {
            //print_r($post_params);exit();
            load_transferv1_lib($post_params['booking_source']);
            if (trim($post_params['op'] == 'check_tourgrade') && $post_params['product_code'] != '') {
                $search_product_tourgrade = $this->transferv1_lib->get_tourgrade_list($post_params);
                //debug($search_product_tourgrade); exit();

                $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));

                if ($search_product_tourgrade['status']) {

                    $module_currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

                    $raw_product_list = $this->transferv1_lib->tourgrade_in_preferred_currency($search_product_tourgrade['data'], $currency_obj, $module_currency_obj, 'b2c');

                    $search_product_tourgrade['data'] = $raw_product_list;
                    $ageBand_token                    = base64_encode(json_encode($search_product_tourgrade['data']['Trip_list'][0]['AgeBands']));
                    $response['data']                 = array('tourgrade_list' => $search_product_tourgrade['data'],
                        'ageBand_token'                                            => $ageBand_token,
                        'search_params'                                            => $post_params,
                        'currency_obj'                                             => $currency_obj,
                        'booking_source'                                           => $post_params['booking_source']);
                    $response['msg']    = 'success';
                    $response['status'] = SUCCESS_STATUS;

                } else {
                    $search_product_tourgrade['Message'] = $search_product_tourgrade['Message'];

                    if (!valid_array($search_product_tourgrade['data'])) {

                        $response['msg'] = $search_product_tourgrade['Message'];

                    }
                }

            } else {

            }
        } else {

        }
        $this->output_compressed_data($response);

    }

    /**
     *Elavarasi
     *Getting Available Tourgrade Based on Passenger Mix
     */
    public function select_tourgrade()
    {
        $post_params = $this->input->post();
        // debug($post_params);
        // exit;
        //debug($get_params)
        $response['data']   = '';
        $response['msg']    = '';
        $response['status'] = FAILURE_STATUS;
        $safe_search_data   = $this->transferv1_model->get_safe_search_data($post_params['search_id'], META_TRANSFERV1_COURSE);
        //debug($get_params);exit;
        if (trim($post_params['product_code']) != '') {

            load_transferv1_lib($post_params['booking_source']);
            if (trim($post_params['op'] == 'check_tourgrade') && $post_params['product_code'] != '') {

                $search_product_tourgrade = $this->transferv1_lib->get_tourgrade_list($post_params);

                $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));

                if ($search_product_tourgrade['status']) {

                    $module_currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

                    $raw_product_list = $this->transferv1_lib->tourgrade_in_preferred_currency($search_product_tourgrade['data'], $currency_obj, $module_currency_obj, 'b2c');

                    $search_product_tourgrade['data'] = $raw_product_list;
                    $response['data']                 = get_compressed_output($this->template->isolated_view('transferv1/viator/select_tourgrade',
                        array('tourgrade_list' => $search_product_tourgrade['data'], 'search_params' => $post_params,
                            'currency_obj'         => $currency_obj,
                            'booking_source'       => $post_params['booking_source'])));
                    $response['msg']    = 'success';
                    $response['status'] = SUCCESS_STATUS;

                } else {
                    $search_product_tourgrade['Message'] = $search_product_tourgrade['Message'];

                    if (!valid_array($search_product_tourgrade['data'])) {

                        $response['msg'] = $search_product_tourgrade['Message'];

                    }
                }

            } else {

            }
        } else {

        }
        $this->output_compressed_data($response);

    }
    /**
     * Compress and output data
     * @param array $data
     */
    private function output_compressed_data($data)
    {

        while (ob_get_level() > 0) {
            ob_end_clean();
        }
        ob_start("ob_gzhandler");
        header('Content-type:application/json');
        echo json_encode($data);
        ob_end_flush();
        exit;
    }
    /**
     *Ealvarasi check the available date for the product
     **/
    public function select_date()
    {
        $post_params = $this->input->post();

        $selected_date = trim($post_params['selected_date']);
        //echo $selected_date;exit;
        //debug($get_params);
        if (!empty($selected_date)) {

            $product_get_booking_date = json_decode(base64_decode($post_params['available_date']), true);
            if (valid_array($product_get_booking_date)) {

                $selected_date_details = $product_get_booking_date;
                //debug($selected_date_details);exit;
                $options = '';

                //debug($selected_date_details[$selected_date]);exit;
                foreach ($selected_date_details[$selected_date] as $key => $value) {
                    $selected = '';
                    if (isset($post_params['s_date'])) {

                        if ($value == $post_params['s_date']) {
                            $selected = ' selected';
                        }
                    }

                    $options .= '<option value=' . $value . ' ' . $selected . ' >' . ($value) . '</option>';
                }

                echo $options;
                exit;

            }
        }
    }

    /**
     *  Elavarasi
     * Passenger Details page for final bookings
     * Here we need to run booking based on api
     */
    public function block_trip()
    {
        $post_params = $this->input->post('block_params');
        if (empty($post_params)) {
            $data['status']  = FAILURE_STATUS;
            $data['message'] = "Please enter the details to search";
            echo json_encode($data);
            exit;
        }
        //echo PROVAB_TRANSFERV1_BOOKING_SOURCE;exit;
        $pre_booking_params = json_decode($post_params, true);
        //    debug($pre_booking_params);exit;
        $search_id        = $pre_booking_params['search_id'];
        $safe_search_data = $this->transferv1_model->get_safe_search_data($pre_booking_params['search_id'], META_TRANSFERV1_COURSE);

        $safe_search_data['data']['search_id'] = abs($search_id);
        $page_data['active_payment_options']   = $this->module_model->get_active_payment_module_list();

        $loyality_point = $this->user_model->getUserLoyalityPoint($pre_booking_params['user_id']);
        if(!empty($loyality_point)){
            $max_point_can_use_in_one_time = $loyality_point['max_point_can_use_in_one_time'];
            $loyality_points               = (int) (($loyality_point['loyality_points'] * $loyality_point['percent_can_use']) / 100);
            $loyality_points_usable        = (min($max_point_can_use_in_one_time, $loyality_points)) * $loyality_point['loyality_points_coversion_rate'];

            $page_data['loyality_points_usable'] = (int) $loyality_points_usable;
        }else{
            $page_data['loyality_points_usable'] = 0;
        }

        if (isset($pre_booking_params['booking_source']) == true) {

            //We will load different page for different API providers... As we have dependency on API for tourgrade details page
            $page_data['search_data'] = $safe_search_data['data'];
            load_transferv1_lib($pre_booking_params['booking_source']);

            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");

            if ($pre_booking_params['booking_source'] == PROVAB_TRANSFERV1_BOOKING_SOURCE && isset($pre_booking_params['tour_uniq_id']) == true and
                isset($pre_booking_params['op']) == true and $pre_booking_params['op'] == 'block_trip' and $safe_search_data['status'] == true) {

                if ($pre_booking_params['tour_uniq_id'] != false) {

                    $trip_block_details = $this->transferv1_lib->block_trip($pre_booking_params);
                    //debug($trip_block_details);

                    if ($trip_block_details['status'] == false) {
                        $data['status']  = FAILURE_STATUS;
                        $data['message'] = $trip_block_details['data']['msg'];
                        echo json_encode($data);
                        // debug("ok"); exit;
                        exit;
                        //redirect(base_url().'index.php/transferv1/exception?op='.$trip_block_details['data']['msg']);
                    }
                    //Converting API currency data to preferred currency
                    $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));

                    $trip_block_details = $this->transferv1_lib->tripblock_data_in_preferred_currency($trip_block_details, $currency_obj, 'b2c');

                    //Display
                    $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

                    $cancel_currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));

                    $pre_booking_params = $this->transferv1_lib->update_block_details($trip_block_details['data']['BlockTrip'], $pre_booking_params, $currency_obj, 'b2c');
                    // debug($pre_booking_params);
                    // exit;
                    $pre_booking_params['markup_price_summary'] = $this->transferv1_lib->update_booking_markup_currency($pre_booking_params['price_summary'], $currency_obj, $safe_search_data['data']['search_id'], false, true, 'b2c');
                    $pre_booking_params['tax_service_sum']      = $this->transferv1_lib->tax_service_sum($pre_booking_params['markup_price_summary'], $pre_booking_params['price_summary']);

                    if ($trip_block_details['status'] == SUCCESS_STATUS) {
                        if (!empty($this->entity_country_code)) {
                            $page_data['user_country_code'] = $this->entity_country_code;
                        } else {
                            $page_data['user_country_code'] = '';
                        }
                        $page_data['booking_source']                         = $pre_booking_params['booking_source'];
                        $page_data['pre_booking_params']                     = $pre_booking_params;
                        $page_data['pre_booking_params']['default_currency'] = get_application_currency_preference();

                        $page_data['iso_country_list'] = $this->db_cache_api->get_iso_country_list();
                        $page_data['country_list']     = $this->db_cache_api->get_country_list();
                        $page_data['currency_obj']     = $currency_obj;
                        $page_data['total_price']      = $this->transferv1_lib->total_price($pre_booking_params['markup_price_summary']);

                        //calculate convience fees by pax wise
                        $ageband_details = $trip_block_details['data']['BlockTrip']['AgeBands'];
                        // debug($ageband_details); exit;
                        $page_data['convenience_fees'] = $currency_obj->convenience_fees($page_data['total_price'], $ageband_details);

                        $page_data['tax_service_sum'] = $this->transferv1_lib->tax_service_sum($pre_booking_params['markup_price_summary'], $pre_booking_params['price_summary']);

                        $page_data['search_id']    = $search_id;
                        $page_data['token']        = serialized_data($pre_booking_params);
                        $page_data['token_key']    = md5($page_data['token']);
                        $page_data['module_value'] = md5('transfers');

                        //$this->template->view('transferv1/viator/viator_booking_page', $page_data);
                        $response['status'] = SUCCESS_STATUS;
                        $response['data']   = $page_data;
                        //    debug($page_data); exit;
                        echo json_encode($response);exit;
                    } else {
                        $response['status']  = FAILURE_STATUS;
                        $response['message'] = 'Tour id is missing';
                    }
                } else {
                    $response['status'] = FAILURE_STATUS;
                    $response['data']   = 'Data modified while transfer(Invalid Data received while validating tokens)';
                    //redirect(base_url().'index.php/transferv1/exception?op=Data Modification&notification=Data modified while transfer(Invalid Data received while validating tokens)');
                }
            } else {
                $response['status']  = FAILURE_STATUS;
                $response['message'] = 'request invalid';
                //redirect(base_url());
            }
        } else {
            $response['status']  = FAILURE_STATUS;
            $response['message'] = 'booking_source invalid';
            //redirect(base_url());
        }
        $this->output_compressed_data($response);
    }

    /**
     *  Elavarasi
     * Passenger Details page for final bookings
     * Here we need to run booking based on api
     */
    public function booking()
    {

        $pre_booking_params = $this->input->post();

        $search_id        = $pre_booking_params['search_id'];
        $safe_search_data = $this->transferv1_model->get_safe_search_data($pre_booking_params['search_id'], META_TRANSFERV1_COURSE);

        $safe_search_data['data']['search_id'] = abs($search_id);
        $page_data['active_payment_options']   = $this->module_model->get_active_payment_module_list();

        if (isset($pre_booking_params['booking_source']) == true) {

            //We will load different page for different API providers... As we have dependency on API for tourgrade details page
            $page_data['search_data'] = $safe_search_data['data'];
            load_transferv1_lib($pre_booking_params['booking_source']);
            //Need to fill pax details by default if user has already logged in
            $this->load->model('user_model');
            $page_data['pax_details'] = $this->user_model->get_current_user_details();

            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");

            if ($pre_booking_params['booking_source'] == PROVAB_TRANSFERV1_BOOKING_SOURCE && isset($pre_booking_params['tour_uniq_id']) == true and
                isset($pre_booking_params['op']) == true and $pre_booking_params['op'] == 'block_trip' and $safe_search_data['status'] == true) {

                if ($pre_booking_params['tour_uniq_id'] != false) {

                    $trip_block_details = $this->transferv1_lib->block_trip($pre_booking_params);
                    //debug($trip_block_details);

                    if ($trip_block_details['status'] == false) {
                        redirect(base_url() . 'index.php/transferv1/exception?op=' . $trip_block_details['data']['msg']);
                    }
                    //Converting API currency data to preferred currency
                    $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));

                    $trip_block_details = $this->transferv1_lib->tripblock_data_in_preferred_currency($trip_block_details, $currency_obj, 'b2c');

                    //Display
                    $currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

                    $cancel_currency_obj = new Currency(array('module_type' => 'transferv1', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));

                    $pre_booking_params = $this->transferv1_lib->update_block_details($trip_block_details['data']['BlockTrip'], $pre_booking_params, $currency_obj, 'b2c');

                    // debug($pre_booking_params);
                    // exit;
                    /*
                     * Update Markup
                     */
                    $pre_booking_params['markup_price_summary'] = $this->transferv1_lib->update_booking_markup_currency($pre_booking_params['price_summary'], $currency_obj, $safe_search_data['data']['search_id']);

                    // debug($pre_booking_params['markup_price_summary'] );
                    // exit;
                    if ($trip_block_details['status'] == SUCCESS_STATUS) {
                        if (!empty($this->entity_country_code)) {
                            $page_data['user_country_code'] = $this->entity_country_code;
                        } else {
                            $page_data['user_country_code'] = '';
                        }
                        $page_data['booking_source']                         = $pre_booking_params['booking_source'];
                        $page_data['pre_booking_params']                     = $pre_booking_params;
                        $page_data['pre_booking_params']['default_currency'] = get_application_currency_preference();

                        $page_data['iso_country_list'] = $this->db_cache_api->get_iso_country_list();
                        $page_data['country_list']     = $this->db_cache_api->get_country_list();
                        $page_data['currency_obj']     = $currency_obj;
                        $page_data['total_price']      = $this->transferv1_lib->total_price($pre_booking_params['markup_price_summary']);

                        //calculate convience fees by pax wise
                        $ageband_details = $trip_block_details['data']['BlockTrip']['AgeBands'];

                        $page_data['convenience_fees'] = $currency_obj->convenience_fees($page_data['total_price'], $ageband_details);

                        $page_data['tax_service_sum'] = $this->transferv1_lib->tax_service_sum($pre_booking_params['markup_price_summary'], $pre_booking_params['price_summary']);

                        //Traveller Details
                        $page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
                        //Get the country phone code
                        $Domain_record            = $this->custom_db->single_table_records('domain_list', '*');
                        $page_data['active_data'] = $Domain_record['data'][0];
                        $temp_record              = $this->custom_db->single_table_records('api_country_list', '*');
                        $page_data['phone_code']  = $temp_record['data'];

                        $page_data['search_id'] = $search_id;
                        $this->template->view('transferv1/viator/viator_booking_page', $page_data);
                    }
                } else {
                    redirect(base_url() . 'index.php/transferv1/exception?op=Data Modification&notification=Data modified while transfer(Invalid Data received while validating tokens)');
                }
            } else {

                redirect(base_url());
            }
        } else {
            redirect(base_url());
        }
    }

    /**
     *  Elavarasi
     * sending for booking
     */
    public function pre_booking($search_id)
    {
        $post_params = $this->input->post();

        $post_params['billing_city']      = 'Bangalore';
        $post_params['billing_zipcode']   = '560100';
        $post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';

        //Make sure token and temp token matches
        $valid_temp_token = unserialized_data($post_params['token'], $post_params['token_key']);

        if ($valid_temp_token != false) {

            load_transferv1_lib($post_params['booking_source']);
            /****Convert Display currency to Application default currency***/
            //After converting to default currency, storing in temp_booking
            $post_params['token'] = unserialized_data($post_params['token']);
            $currency_obj         = new Currency(array(
                'module_type' => 'transferv1',
                'from'        => get_application_currency_preference(),
                'to'          => get_application_default_currency(),
            ));

            $post_params['token'] = $this->transferv1_lib->convert_token_to_application_currency($post_params['token'], $currency_obj, $this->current_module);
            // debug($post_params['token']);
            // exit;
            $post_params['token'] = serialized_data($post_params['token']);

            $temp_token = unserialized_data($post_params['token']);
            //Insert To temp_booking and proceed
            $temp_booking = $this->module_model->serialize_temp_booking_record($post_params, TRANSFER_BOOKING);
            $book_id      = $temp_booking['book_id'];
            $book_origin  = $temp_booking['temp_booking_origin'];

            // debug($post_params);
            // exit;

            if ($post_params['booking_source'] == PROVAB_TRANSFERV1_BOOKING_SOURCE) {
                $amount   = $this->transferv1_lib->total_price($temp_token['markup_price_summary']);
                $currency = $temp_token['default_currency'];
            }
            $currency_obj = new Currency(array(
                'module_type' => 'transferv1',
                'from'        => admin_base_currency(),
                'to'          => admin_base_currency(),
            ));
            /********* Convinence Fees Start ********/
            $search_data = $temp_token['AgeBands'];

            $convenience_fees = $currency_obj->convenience_fees($amount, $search_data);
            /********* Convinence Fees End ********/

            /********* Promocode Start ********/
            $promocode_discount = $post_params['promo_actual_value'];
            /********* Promocode End ********/

            //details for PGI

            $email = $post_params['billing_email'];
            $phone = $post_params['passenger_contact'];
//            $verification_amount = roundoff_number($amount+$convenience_fees-$promocode_discount);
            $verification_amount = roundoff_number($amount);
            $firstname           = $post_params['first_name']['0'];
            $productinfo         = META_TRANSFERV1_COURSE;
            //check current balance before proceeding further
            $domain_balance_status = $this->domain_management_model->verify_current_balance($verification_amount, $currency);
            if ($domain_balance_status == true) {
                switch ($post_params['payment_method']) {
                    case PAY_NOW:
                        $this->load->model('transaction');
                        $pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
                        $this->transaction->create_payment_record($book_id, $verification_amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount, $pg_currency_conversion_rate);
                        //redirect(base_url().'index.php/transferv1/process_booking/'.$book_id.'/'.$book_origin);
                        redirect(base_url() . 'index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin);
                        break;
                    case PAY_AT_BANK:echo 'Under Construction - Remote IO Error';exit;
                        break;
                }
            } else {
                redirect(base_url() . 'index.php/transferv1/exception?op=Amount Transfers Booking&notification=insufficient_balance');
            }
        } else {
            redirect(base_url() . 'index.php/transferv1/exception?op=Remote IO error @ Transfers Booking&notification=validation');
        }
    }

    /**
     *  Elavarasi
     * sending for booking
     */
    public function mobile_pre_booking()
    {
        error_reporting(E_ALL);
        $post_params = $this->input->post('booking_params');
		//debug($post_params );exit;

        $wallet_bal = $this->input->post('wallet_bal');
        if (empty($post_params)) {
            $data['status']  = FAILURE_STATUS;
            $data['message'] = "Please enter valid request";
            echo json_encode($data);
            exit;
        }

        //echo "hi";exit();
        //echo PROVAB_TRANSFERV1_BOOKING_SOURCE;exit;
        $post_params = json_decode($post_params, true);

        $token     = $this->input->post('token');
        $token_key = $post_params['token_key'];
        $card      = $this->input->post('card');
        $month     = $this->input->post('month');
        $year      = $this->input->post('year');
        //echo $token;
        //echo $token_key;exit;
        $post_params['wallet_bal'] = $wallet_bal;
        $this->country_code        = str_replace("+", "", $post_params['country_code']);

        $this->session->set_userdata('country_code', str_replace("+", "", $post_params['country_code']));

        // $post_params['total_amount_val'];
        // $post_params['currency'];
        // $post_params['currency_symbol'];

        //Make sure token and temp token matches
        $valid_temp_token = unserialized_data($token, $token_key);
        
		//echo "fdgdfhgf";

		//debug($valid_temp_token); exit;
        if ($valid_temp_token != false) {

            load_transferv1_lib($post_params['booking_source']);

            /****Convert Display currency to Application default currency***/

            //After converting to default currency, storing in temp_booking
            $post_params['token'] = unserialized_data($token);

            $currency_obj = new Currency(array(
                'module_type' => 'transferv1',
                'from'        => get_application_currency_preference(),
                'to'          => get_application_default_currency(),
            ));

            if($post_params['markup_ad']!=0 && isset($post_params['markup_ad'])){
                $post_params['loyality_points']  = $post_params['markup_ad'];
            }else{
                $post_params['loyality_points']  = 0;
            }

            #debug($post_params['token']);exit;
            $post_params['token'] = $this->transferv1_lib->convert_token_to_application_currency($post_params['token'], $currency_obj, $this->current_module);

            //debug($post_params['token']);
            // exit;
            $post_params['token'] = serialized_data($post_params['token']);

            $temp_token = unserialized_data($post_params['token']);

            //Insert To temp_booking and proceed
            //echo "<pre>";print_r($post_params);exit();
            $temp_booking                  = $this->module_model->serialize_temp_booking_record($post_params, TRANSFER_BOOKING);
            $book_id                       = $temp_booking['book_id'];
            $book_origin                   = $temp_booking['temp_booking_origin'];
            $post_params['payment_method'] = 'PNHB1';
            // debug($post_params);
            // exit;

            if ($post_params['booking_source'] == PROVAB_TRANSFERV1_BOOKING_SOURCE) {
                $amount   = $post_params['total_amount_val']; //$this->transferv1_lib->total_price($temp_token['markup_price_summary']);
                $currency = $temp_token['default_currency'];
            }
            $currency_obj = new Currency(array(
                'module_type' => 'transferv1',
                'from'        => admin_base_currency(),
                'to'          => admin_base_currency(),
            ));
            /********* Convinence Fees Start ********/
            $search_data = $temp_token['AgeBands'];

            $convenience_fees = $currency_obj->convenience_fees($amount, $search_data);
            /********* Convinence Fees End ********/

            /********* Promocode Start ********/
            $promocode_discount = $post_params['promo_code_discount_val'];
            /********* Promocode End ********/

            //details for PGI

            $email                         = $post_params['billing_email'];
            $phone                         = $post_params['passenger_contact'];
            $GLOBALS['CI']->entity_user_id = $post_params['user_id'];

//            $verification_amount = roundoff_number($amount+$convenience_fees-$promocode_discount);
            $verification_amount = roundoff_number($amount);

            // debug($verification_amount);
            if ($wallet_bal == 'on') {
                $wallet_amount = 0;
                $wallet_data   = $this->user_model->get_wallet_log($GLOBALS['CI']->entity_user_id);
                // debug($wallet_data);
                if (isset($wallet_data[0]) && !empty($wallet_data[0])) {

                    $wallet_amount = $wallet_data[0]['closing_balance'];
                    // debug($wallet_amount);
                    if (($verification_amount) > $wallet_amount) {
                        // $this->flight_model->save_wallet_log($book_id,$amount,false,$wallet_amount);
                        $verification_amount = ($verification_amount) - $wallet_amount;
                    } else if (($verification_amount) < $wallet_amount) {
                        // $this->flight_model->save_wallet_log($book_id,$amount,false,$wallet_amount);
                        $verification_amount = 0;
                    } else if (($verification_amount) == $wallet_amount) {
                        // $this->flight_model->save_wallet_log($book_id,$amount,false,$wallet_amount);
                        $verification_amount = 0;
                    }
                }
            }
            // debug($verification_amount);exit;
            $firstname   = $post_params['first_name']['0'];
            $productinfo = META_TRANSFERV1_COURSE;
            //check current balance before proceeding further
            $domain_balance_status = $this->domain_management_model->verify_current_balance($verification_amount, $currency);
            if ($domain_balance_status == true) {
                switch ($post_params['payment_method']) {
                    case PAY_NOW:

                        $this->load->model('transaction');

                        //$this->transaction->create_payment_record($book_id, $post_params['total_amount_val'], $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount);
                        //redirect(base_url() . 'index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin . '/' . $post_params['card'] . '/' . $post_params['month'] . '/' . $post_params['year']);
                        //break;exit();

                        $payment_gateway_method      = (isset($post_params['payment_type'])) ? $post_params['payment_type'] : 'STRIPE';
                        $pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
                        // $this->transaction->create_payment_record($book_id, $verification_amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount, $post_params['currency'], $this->country_code);

                        $this->transaction->create_payment_record($book_id, $post_params['total_amount_val'], $firstname, $email, $phone, $productinfo, 0, 0, $post_params['currency'], $this->country_code);

                        $url = explode('/mobile_webservices', base_url());

                        if ($verification_amount > 0) {
                            $payment_gateway_url = $url[0] . '/mobile_webservices/mobile/index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin . '/' . $card . '/' . $month . '/' . $year;

                            $pre_booking_status['data']   = array('app_reference' => $book_id, 'temp_book_origin' => $book_origin, 'return_url' => $payment_gateway_url);
                            $pre_booking_status['status'] = SUCCESS_STATUS;
                            // debug($pre_booking_status);exit;
                            $this->output_compressed_data($pre_booking_status);
                            exit();
                        } else {

                            $pre_booking_status['status']          = SUCCESS_STATUS;
                            $pre_booking_status['response_params'] = 'wallet_amount_used';
                            $this->transaction->update_payment_record_status($book_id, ACCEPTED, $response_params);
                            $pre_booking_status['data']['return_url'] = $url[0] . '/index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin . '/' . $post_params['card'] . '/' . $post_params['month'] . '/' . $post_params['year'];
                            //debug($pre_booking_status);exit;
                            $this->output_compressed_data($pre_booking_status);
                            exit();
                            // redirect(base_url().'index.php/flight/process_booking/'.$book_id.'/'.$book_origin);
                        }

                        //redirect(base_url().'index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin);
                        break;
                    case PAY_AT_BANK:echo 'Under Construction - Remote IO Error';exit;
                        break;
                }
            } else {
                $response['status']  = FAILURE_STATUS;
                $response['message'] = 'insufficient_balance';
                $this->output_compressed_data($response);
                //return $response;
                //    redirect(base_url().'index.php/transferv1/exception?op=Amount Transfers Booking&notification=insufficient_balance');
            }
        } else {
            $response['status']  = FAILURE_STATUS;
            $response['message'] = 'token mismatched';

            $this->output_compressed_data($response);
            //redirect(base_url().'index.php/transferv1/exception?op=Remote IO error @ Transfers Booking&notification=validation');
        }

    }

    /*
    process booking in backend until show loader
     */
    public function process_booking($book_id, $temp_book_origin, $user_type = 'b2c', $user_id = '')
    {
        if($user_id == ''){
            $user_id = $GLOBALS['CI']->entity_user_id;
        }

        if ($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0) {

            $page_data['form_url']                        = base_url() . 'index.php/transferv1/secure_booking/' . $book_id . '/' . $temp_book_origin;
            $page_data['form_method']                     = 'POST';
            $page_data['form_params']['book_id']          = $book_id;
            $page_data['form_params']['temp_book_origin'] = $temp_book_origin;
            //redirect(base_url() . 'index.php/transferv1/secure_booking/' . $book_id . '/' . $temp_book_origin);

            if ($user_type == 'b2b') {
                redirect(base_url() . 'mobile/index.php/transferv1/secure_booking/' . $book_id . '/' . $temp_book_origin . '/' . $user_type . '/' . $user_id);
            } else {
                redirect(base_url() . 'index.php/transferv1/secure_booking/' . $book_id . '/' . $temp_book_origin);
            }
            // $this->template->view('share/loader/booking_process_loader', $page_data);

        } else {
            redirect(base_url() . 'index.php/transferv1/exception?op=Invalid request&notification=validation');
        }
        /*
    if($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0){

    $page_data ['form_url'] = base_url () . 'index.php/transferv1/secure_booking';
    $page_data ['form_method'] = 'POST';
    $page_data ['form_params'] ['book_id'] = $book_id;
    $page_data ['form_params'] ['temp_book_origin'] = $temp_book_origin;

    $this->template->view('share/loader/booking_process_loader', $page_data);

    }else{
    redirect(base_url().'index.php/transferv1/exception?op=Invalid request&notification=validation');
    }
     */
    }

    public function sendmail($app_reference, $booking_source, $booking_status)
    {

        $booking_status = 'BOOKING_CONFIRMED';
        $this->load->library('booking_data_formatter');
        $this->load->library('provab_mailer');
        $this->load->model('transferv1_model');
        if (empty($app_reference) == false) {
            $booking_details = $this->transferv1_model->get_booking_details($app_reference, $booking_source, $booking_status);

            if ($booking_details['status'] == SUCCESS_STATUS) {
                load_transferv1_lib(PROVAB_TRANSFERV1_BOOKING_SOURCE);

                $assembled_booking_details = $this->booking_data_formatter->format_transferv1_booking_data($booking_details, 'b2b');

                $page_data['data'] = $assembled_booking_details['data'];
                //debug($page_data);exit;
                $email = $booking_details['data']['booking_customer_details']['0']['email'];
                $this->load->library('provab_pdf');
                // $create_pdf = new Provab_Pdf();
                $mail_template = $this->template->isolated_view('voucher/transferv1_voucher', $page_data);
                // $pdf = $create_pdf->create_pdf($mail_template,'');
                // $this->provab_mailer->send_mail($email, domain_name().' E-Ticket',$mail_template ,$pdf);

/************ for sms ********/
                $sendSMS = '
Booking status : ' . $booking_details['data']['booking_itinerary_details']['0']['status'] . '
App reference : ' . $booking_details['data']['booking_itinerary_details']['0']['app_reference'] . '
travel date : ' . date('d-m-Y', strtotime($booking_details['data']['booking_itinerary_details']['0']['travel_date'])) . '
Grade Code : ' . $booking_details['data']['booking_itinerary_details']['0']['grade_code'] . '
Grade Description : ' . $booking_details['data']['booking_itinerary_details']['0']['grade_desc'] . '';
                $user_number = $booking_details['data']['booking_customer_details']['0']['phone'];
// $country_code = (int)$booking_details['data']['booking_customer_details']['0']['country_code'];
                $numbb      = ($this->session->userdata('country_code')) ? (int) $this->session->userdata('country_code') : 91;
                $sms_number = $numbb . $user_number;

                $this->load->library('twilio_sms');
                $sid = "AC7e7cf0942f1f0bc8e301ae5aeb7fb871"; // Your Account SID from www.twilio.com/console
                // $sid = "AC871f5edd766e632472b28a0e7150fa1c";
                $token = "a87efa072190b5a141548d7ba6a0c95d"; // Your Auth Token from www.twilio.com/console
                // $token = "f8ec9dd30b634758015d19eb07286f7d";
                $client  = new Twilio\Rest\Client($sid, $token);
                $message = $client->messages->create(
                    // '+919738767357', // Text this number
                    //    '+919742068789',
                    //    $telephone_code.$booking ['data'] ['booking_params']['passenger_contact'],
                    "+" . $sms_number,
                    array(
                        'from' => '+14422693612', // From a valid Twilio number
                        // 'from' => '+15005550006',
                        'body' => $sendSMS,
                    )
                );

/************ for sms ********/

                $this->provab_mailer->send_mail($email, 'topfly.co E-Ticket', $mail_template);

                // $this->provab_mailer->send_mail('provab.himanshu@gmail.com', 'Topfly.co E-Ticket','test');

            }
            return true;
        }
        return false;
    }

    /**
     * Elavarasi
     *Do booking once payment is successfull - Payment Gateway
     *and issue voucher
     */
    public function secure_booking($book_id, $temp_book_origin, $user_type, $user_id)
    {
        //error_reporting(E_ALL);
        //$post_data = $this->input->post();
        //echo "hi";exit;
        $resp['status']  = 0;
        $resp['message'] = '';
        if (!empty($book_id) && !empty($temp_book_origin) && intval($temp_book_origin) > 0) {

            //verify payment status and continue
            $book_id          = trim($book_id);
            $temp_book_origin = intval($temp_book_origin);
            $this->load->model('transaction');
            $booking_status = $this->transaction->get_payment_status($book_id);

            /*if ($booking_status['status'] !== 'accepted') {
        //    redirect(base_url().'index.php/transferv1/exception?op=Payment Not Done&notification=validation');
        }*/

        } else {

            $resp['message'] = 'InvalidBooking';
            $this->output_compressed_data($resp);
            //redirect(base_url().'index.php/transferv1/exception?op=InvalidBooking&notification=invalid');
        }

        //run booking request and do booking
        $temp_booking = $this->module_model->unserialize_temp_booking_record($book_id, $temp_book_origin);
        //debug($temp_booking);exit;
        //Delete the temp_booking record, after accessing
        //$this->module_model->delete_temp_booking_record($book_id, $temp_book_origin);
        load_transferv1_lib(PROVAB_TRANSFERV1_BOOKING_SOURCE);
        //verify payment status and continue

        $total_booking_price = $this->transferv1_lib->total_price($temp_booking['book_attributes']['token']['markup_price_summary']);
        //debug($temp_booking);
        //exit;
        $currency = $temp_booking['book_attributes']['token']['default_currency'];
        $loyality_points = $temp_booking['book_attributes']['markup_ad'];
        $user_id = $temp_booking['book_attributes']['user_id'];
        //also verify provab balance
        //check current balance before proceeding further
        $domain_balance_status = $this->domain_management_model->verify_current_balance($total_booking_price, $currency);
        // debug($temp_booking);exit;

        if ($domain_balance_status) {
        //echo '1';exit();
            //lock table
            if ($temp_booking != false) {
                switch ($temp_booking['booking_source']) {
                    case PROVAB_TRANSFERV1_BOOKING_SOURCE:

                        //FIXME : COntinue from here - Booking request
                        $booking = $this->transferv1_lib->process_booking($book_id, $temp_booking['book_attributes']);
                        //Save booking based on booking status and book id
                        break;
                }
                //debug($booking);exit;
                if ($booking['status'] == SUCCESS_STATUS) {

                    $currency_obj                          = new Currency(array('module_type' => 'transferv1', 'from' => admin_base_currency(), 'to' => admin_base_currency()));
                    $promo_currency_obj                    = new Currency(array('module_type' => 'transferv1', 'from' => get_application_currency_preference(), 'to' => admin_base_currency()));
                    $booking['data']['currency_obj']       = $currency_obj;
                    $booking['data']['promo_currency_obj'] = $promo_currency_obj;
                    $booking['data']['loyality_points'] = $loyality_points;
                    //Save booking based on booking status and book id
                    $data = $this->transferv1_lib->save_booking($book_id, $booking['data'], 'b2b', $user_id);
                    //echo "gfnnnnhgf";exit;
                    $this->domain_management_model->update_transaction_details('transferv1', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount'], $data['transaction_currency'], $data['currency_conversion_rate']);

                    // $mail_value = self::sendmail($book_id,$temp_booking['booking_source'],$data['booking_status']);
                    //$mail_value = self::sendmail($book_id, $temp_booking['booking_source'], 'accepted');

                    //echo redirect(base_url() . 'index.php/voucher/transferv1/' . $book_id . '/' . $temp_booking['booking_source'] . '/' . $data['booking_status'] . '/show_voucher');exit;

                    //redirect(base_url() . 'index.php/voucher/transfers/' . $book_id . '/' . $temp_booking['booking_source'] . '/' . $data['booking_status'] . '/show_voucher');exit;

                        //echo "savita";
                    $this->applyLoyalityPoints($total_booking_price, $book_id, $user_id);

                    exit();
                    redirect(base_url() . 'index.php/voucher/transfers/' . $book_id . '/' . $temp_booking['booking_source'] . '/' . $data['booking_status'] . '/show_voucher');
                    // $resp['status'] =1;
                    // $resp['data']['voucher_url'] =base_url().'index.php/voucher/transferv1/'.$book_id.'/'.$temp_booking['booking_source'].'/'.$data['booking_status'].'/show_voucher';
                    // $this->output_compressed_data($resp);

                } else {
                    // $resp['message']=$booking ['message'];
                    // $this->output_compressed_data($resp);

                    redirect(base_url() . 'index.php/transferv1/exception?op=booking_exception&notification=' . $booking['Message']);
                }
            }
            //release table lock
        } else {
            // $resp['message']='Remote IO error @ Insufficient';
            // $this->output_compressed_data($resp);
            redirect(base_url() . 'index.php/transferv1/exception?op=Remote IO error @ Insufficient&notification=validation');
        }
        redirect(base_url() . 'index.php/hotel/exception?op=Remote IO error @ Hotel Secure Booking&notification=validation');
    }

    private function applyLoyalityPoints($total_booking_price=0, $app_reference=0, $user_id='') {
        
        //echo $total_booking_price;
        //$total_booking_price = 10000; 
        //$app_reference = 'FB18-200416-717494';
        // get booking loyality price
        $price = $this->user_model->get_point_from_transfer_booking_details($app_reference);
        $booking_price = $this->user_model->get_price_from_payment_details($app_reference);
        $loyality_points_config = $this->user_model->getUserLoyalityPoint($user_id);
        $available_loyality_points = $loyality_points_config['loyality_points'];

        /*echo $this->db->last_query();
        echo "<pre>";
        print_r($loyality_points_config);exit();*/
        $total_points = ($price['loyality_points']/$loyality_points_config['loyality_points_coversion_rate']);
        //$this->user_model->deduct_loyality_point($total_points, $user_id);
        $total_booking_price = 0;
        if(!empty($booking_price)){
            $total_booking_price = $booking_price['amount'];
        }
        $data1 = array(
            'booking_ref' => $app_reference,
            'debit' => $total_points,
            'equivalent_amount' => $price['loyality_points'],
            //'user_id' => $this->entity_user_id
            'user_id' => $user_id,
            'remarks' => $total_booking_price
        );
        $this->user_model->log_loyality_point($data1);
         

        $credit = (int)(($total_booking_price*$loyality_points_config['max_percent_loyality_point_can_earn'])/100);

        $data2 = array(
            'booking_ref' => $app_reference,
            'credit' => $credit,
            'equivalent_amount' => 0,
            //'user_id' => $this->entity_user_id,
            'user_id' => $user_id,
            'remarks' => $total_booking_price
        );
        $this->user_model->log_loyality_point($data2);

        //deducting debit amount
        $available_loyality_points = $available_loyality_points - $price['loyality_points'];

        //updating credit amount
        $available_loyality_points = $available_loyality_points + $credit; 

        //$this->user_model->add_loyality_point($credit,$user_id);
        $this->user_model->update_user_loyality_point($available_loyality_points,$user_id);

    }
    public function test()
    {
        $currency_obj = new Currency(array('module_type' => 'sightseeing', 'from' => admin_base_currency(), 'to' => admin_base_currency()));
        debug($currency_obj);
    }

    /**
     * Elavarasi
     */
    public function pre_cancellation($app_reference, $booking_source)
    {
        if (empty($app_reference) == false && empty($booking_source) == false) {
            $page_data       = array();
            $booking_details = $this->transferv1_model->get_booking_details($app_reference, $booking_source);
            if ($booking_details['status'] == SUCCESS_STATUS) {
                $this->load->library('booking_data_formatter');
                //Assemble Booking Data
                $assembled_booking_details = $this->booking_data_formatter->format_transferv1_booking_data($booking_details, 'b2c');
                $page_data['data']         = $assembled_booking_details['data'];
                $this->template->view('transferv1/pre_cancellation', $page_data);
            } else {
                redirect('security/log_event?event=Invalid Details');
            }
        } else {
            redirect('security/log_event?event=Invalid Details');
        }
    }
    /*
     * Elavarasi
     * Process the Booking Cancellation
     * Full Booking Cancellation
     *
     */
    public function cancel_booking($app_reference, $booking_source)
    {
        if (empty($app_reference) == false) {
            $get_params = $this->input->get();

            $master_booking_details = $this->transferv1_model->get_booking_details($app_reference, $booking_source);
            if ($master_booking_details['status'] == SUCCESS_STATUS) {
                $this->load->library('booking_data_formatter');
                $master_booking_details = $this->booking_data_formatter->format_sightseeing_booking_data($master_booking_details, 'b2c');
                $master_booking_details = $master_booking_details['data']['booking_details'][0];
                load_sightseen_lib($booking_source);
                $cancellation_details = $this->sightseeing_lib->cancel_booking($master_booking_details, $get_params); //Invoke Cancellation Methods
                if ($cancellation_details['status'] == false) {
                    $query_string = '?error_msg=' . $cancellation_details['msg'];
                } else {
                    $query_string = '';
                }
                redirect('sightseeing/cancellation_details/' . $app_reference . '/' . $booking_source . $query_string);
            } else {
                redirect('security/log_event?event=Invalid Details');
            }
        } else {
            redirect('security/log_event?event=Invalid Details');
        }
    }
    /**
     * Elavarasi
     * Cancellation Details
     * @param $app_reference
     * @param $booking_source
     */
    public function cancellation_details($app_reference, $booking_source)
    {
        if (empty($app_reference) == false && empty($booking_source) == false) {
            $master_booking_details = $GLOBALS['CI']->transferv1_model->get_booking_details($app_reference, $booking_source);
            if ($master_booking_details['status'] == SUCCESS_STATUS) {
                $page_data = array();
                $this->load->library('booking_data_formatter');
                $master_booking_details = $this->booking_data_formatter->format_sightseeing_booking_data($master_booking_details, 'b2c');
                $page_data['data']      = $master_booking_details['data'];
                $this->template->view('sightseeing/cancellation_details', $page_data);
            } else {
                redirect('security/log_event?event=Invalid Details');
            }
        } else {
            redirect('security/log_event?event=Invalid Details');
        }

    }

    /**
     * Elavarasi
     */
    public function exception($redirect = true)
    {
        $module       = META_TRANSFERV1_COURSE;
        $op           = (empty($_GET['op']) == true ? '' : $_GET['op']);
        $notification = (empty($_GET['notification']) == true ? '' : $_GET['notification']);
        if ($op == 'Some Problem Occured. Please Search Again to continue') {
            $op = 'Some Problem Occured. ';
        }
        if ($notification == 'In Sufficiant Balance') {

            $notification = 'In Sufficiant Balance For Transfers';
        }

        $eid = $this->module_model->log_exception($module, $op, $notification);

        //set ip log session before redirection
        $this->session->set_flashdata(array('log_ip_info' => true));

        if ($redirect) {
            redirect(base_url() . 'index.php/transferv1/event_logger/' . $eid);
        }

    }
    public function event_logger($eid = '')
    {

        $log_ip_info    = $this->session->flashdata('log_ip_info');
        $exception_data = $this->custom_db->single_table_records('exception_logger', '*', array('exception_id' => $eid), 0, 1);
        $exception      = $exception_data['data'][0];
        $this->template->view('transferv1/exception', array('log_ip_info' => $log_ip_info, 'eid' => $eid, 'exception' => $exception));
    }

    public function get_transfer_city_list_mobile()
    {
        // echo"hello";exit;
        $this->load->model('transferv1_model');
        $term = $this->input->post('char'); //retrieve the search term that autocomplete sends
        $term = trim(strip_tags($term));
        // $term = '';
        $data['status'] = 0;
        $data_list      = $this->transferv1_model->get_sightseen_city_list($term);
        // debug($data_list);exit;
        if (valid_array($data_list) == true) {
            $data['status'] = 1;

            $suggestion_list = array();
            $result          = '';
            foreach ($data_list as $city_list) {
                $suggestion_list['city_name'] = $city_list['city_name'];
                $suggestion_list['id']        = $city_list['origin'];

                $result[] = $suggestion_list;
            }
            $data['list'] = $result;
        }
        // debug($data);exit;
        echo json_encode($data);exit;
        // $this->output_compressed_data($result);
    }

}
