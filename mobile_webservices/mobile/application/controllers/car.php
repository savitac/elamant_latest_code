<?php
ob_start();if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (session_status() == PHP_SESSION_NONE) { session_start(); } 

class Car extends CI_Controller {
	//DO : Setting Current website url in session, Purpose : For keeping the page on login/logout.
	public function __construct(){
		parent::__construct();
		$current_url 	= $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
        $current_url 	= WEB_URL.$this->uri->uri_string(). $current_url;
		$url 			= array('continue' => $current_url);
		    $this->perPage 	= 10;
        $this->session->set_userdata($url);               
        $this->load->model('Home_Model');
		    $this->load->model('Car_Model');   
        $this->load->model('cart_model');
        $this->load->model('booking_model');  
        $this->load->library('session');
        $this->load->model('email_model');  
		    $this->load->library('encrypt');
        $this->load->library('flight/amedus_xml_to_array');
        $this->load->library('Ajax_pagination'); 
        $this->load->helper('car/car_helper');        
	}
    public function index(){
        $request 			= $this->input->get();
       # debug($request);die;
        $data['req'] 		= json_decode(json_encode($request));
        $data['request'] 	= base64_encode(json_encode($request)); 
        #print_r($data);die;
        $bundle_search_id = 0;
        if(isset($request['id'])){
          $bundle_search_id  = $request['id'];
        }
        $data['bundle_search_id'] = $bundle_search_id;
        $this->load->view(PROJECT_THEME.'/car/results', $data);
	}
	
	public function search(){      
        $request 	= $this->input->get();
        
        
        if($request['drop_down_date1']){
          $request['drop_down_date1'] = date('d-m-Y',strtotime($request['drop_down_date1']));
        }
        if($request['drop_down_date2']){
          $request['drop_down_date2'] = date('d-m-Y',strtotime($request['drop_down_date2']));
        }
       

        $request['type']= $request['trip_type_car'];

        $request['drop_down_loc'] = substr(chop(substr($request['drop_down_loc'], -5), ')'), -3);
        $request['pick_up_loc'] = substr(chop(substr($request['pick_up_loc'], -5), ')'), -3);
        $request['pick_up_date'] = $request['drop_down_date1'];
       
        $request['drop_down_time2'] = $request['drop_out_time2'];
        $request['trav'] = $request['adult'];

        $insert_data['search_data'] = json_encode($request);
        $insert_data['search_type'] = 'CAR';

        $search_insert_data = $this->custom_db->insert_record('search_history',$insert_data);

        $this->save_search_cookie('CAR', $search_insert_data['insert_id']);
        $search_id = $search_insert_data['insert_id'];
        $drop_down_date = '';
      		if($request['trip_type_car'] == 'same_loc'){
      			$type 	= 'type=same_loc';   
                  $trav   = '&trav='.$request['adult'];
                  $drop_up_loc= '&drop_down_loc='. substr(chop(substr($request['pick_up_loc'], -5), ')'), -3);
                  $query  = $type.'&pick_up_loc='.substr(chop(substr($request['pick_up_loc'], -5), ')'), -3).$drop_up_loc.'&pick_up_date='.$request['drop_down_date1'].'&drop_down_time1='.$request['drop_down_time1'].'&drop_down_date2='.$request['drop_down_date2'].'&drop_down_time2='.$request['drop_out_time2'].$trav.'&search_id='.$search_id;
      		}else if($request['trip_type_car'] == 'diff_loc'){
      			$type 	= 'type=diff_loc'; 
            $drop_down_date2 = '&drop_down_date2='.$request['drop_down_date2'];
            $drop_down_time2 = '&drop_down_time2='.$request['drop_out_time2']; 
            $trav   = '&trav='.$request['adult'];
                  $drop_up_loc= '&drop_down_loc='.substr(chop(substr($request['drop_down_loc'], -5), ')'), -3);
                  $query  = $type.'&pick_up_loc='.substr(chop(substr($request['pick_up_loc'], -5), ')'), -3).$drop_up_loc.'&pick_up_date='.$request['drop_down_date1'].'&drop_down_time1='.$request['drop_down_time1'].$drop_down_date2.$drop_down_time2.$trav.'&search_id='.$search_id;
          }
         
          redirect(WEB_URL.'car/?'.$query);
    }
 
 private function save_search_cookie($module, $search_id) {
        $sparam = array();
        $sparam = $this->input->cookie('sparam', TRUE);
        if (empty($sparam) == false) {
            $sparam = unserialize($sparam);
        }
        $sparam[$module] = $search_id;

        $cookie = array(
            'name' => 'sparam',
            'value' => serialize($sparam),
            'expire' => '86500',
            'path' => PROJECT_COOKIE_PATH
        );

        $this->input->set_cookie($cookie);
}

	public function GetResults($Req_before_decode = ''){	
   
		$data['request'] 			= $search_request =  json_decode(base64_decode($Req_before_decode));
   
		$rand_id 					= md5(time() . rand() . time());
        $xml_response 				= $this->Car_Model->insertInputParameters($search_request,$rand_id);

    //debug($search_request);die;
    if($xml_response['xmlNameRes'] == ''){
			$VehicleSearchAvailabilityReq 	= VehicleSearchAvailabilityReq($search_request,$xml_response);
		}else{
			$rand_id 					= $xml_response['InputrandId'];            
			$result 					= file_get_contents("ChinaEasterXML/Car/".$xml_response['xmlNameRes']);
			$xml_query 					= file_get_contents("ChinaEasterXML/Car/".$xml_response['xmlNameReq']);
			$VehicleSearchAvailabilityReq= array(
											'Car_AvailabilityReq' => $xml_query,
											'Car_AvailabilityRes' => $result
										);
		}
   
		$results 				= $this->renderApiResponse($VehicleSearchAvailabilityReq['Car_AvailabilityRes'],$rand_id);
    
          $data['car_result']=$results;

        $dataresult['session_data'] 	= $data['session_data'] = $session_data = $this->generate_rand_no().date("mdHis");
        // echo "<pre/>";print_r($dataresult);die;
        $this->Car_Model->save_result($results,$session_data,$search_request);
        $car_data       = $this->Car_Model->get_last_response_data($session_data);
        $supplier_data       = $this->Car_Model->getSuppliers($session_data);
        $supplier_data_html = "";
        foreach ($supplier_data as $key => $value) {
          $supplier_data_html .= '<li>
                 <div class="squaredThree"><input id="squaredThree'.$key.'" class="filter_airline" type="checkbox" name="airline" value="'.$value["company_name"].'"><label for="squaredThree'.$key.'"></label></div>
                 <label class="lbllbl" for="squaredThree'.$key.'">'.ucfirst(strtolower($value["company_name"])).'</label>
              </li>';
        }
		$car_data		    				= $this->Car_Model->get_last_response_data($session_data);
        $dataresult['min_rate'] 			=  floor((float)$car_data['result_data']->min_rate);
		$dataresult['max_rate']       		=  ceil((float)$car_data['result_data']->max_rate);
		$dataresult['supplier_data_html'] 	=  $supplier_data_html;
		$data['car_data'] 				    =  $car_data['car_data'];
		$data['car_count'] 				    =  $car_data['result_data']->car_count;
		$totalRec 							=  $car_data['result_data']->car_count;
		$config['first_link']  				= 'First';
		$config['div']        				= 'flightsdata'; //parent div tag id
    #debug($data['request']);

		$config['base_url']    				= WEB_URL.'car/ajaxPaginationData/'.$session_data;
    #echo $config['base_url'] ;
    #exit;
		$config['total_rows']  				= $totalRec;
		$config['per_page']    				= $this->perPage;
    
		$this->ajax_pagination->initialize($config);
    #debug($this->ajax_pagination);die;

        $carmatrixdata= $CarFilter = '';
        $data['car_result']['car_result'] = $this->Car_Model->get_last_response($session_data,array('limit'=>$this->perPage));
        // debug(  $data['car_result']['car_result'] );
        // exit;
		#debug($data['car_result']);die;
		if(!$data['car_result']){
			$data['message'] = "No Result Found"; 
		}
		$dataresult['airlinematrix'] = $airlinematrixdata;
		$dataresult['AirlineFilter'] = $AirlineFilter;
        $data['Req_before_decode'] =$Req_before_decode;
        $data['all_results'] = $results;
        $data['search_id'] = $data['request']->search_id; 
        
       // $data['request']->id = $data['request']->search_id; 
        $dataresult['result'] =  $this->load->view(PROJECT_THEME.'/car/ajax_result',$data,TRUE);

  
		echo json_encode($dataresult);
    }
	public function renderApiResponse($SearchResponse,$rand_id){
            $car_result = xmlstr_to_array($SearchResponse);           
				$hs = array ();
                $company_name=array();
                $car_rate=array();
                $car_model=array();
                $car_img=array();
                $data=array();
                $i = 0;

               
            if($car_result != ''){
			$data['session_data'] = array (
                'SessionId' => $car_result['soapenv:Header'] ['awsse:Session'] ['awsse:SessionId'],
                'SequenceNumber' => $car_result ['soapenv:Header'] ['awsse:Session'] ['awsse:SequenceNumber'],
                'SecurityToken' => $car_result ['soapenv:Header']['awsse:Session'] ['awsse:SecurityToken']
             );
           //debug($car_result);die;
            if(!isset($car_result['soapenv:Body']['Car_AvailabilityReply']['errorData'])){
            $car_data=$car_result['soapenv:Body']['Car_AvailabilityReply']['availabilityDetails']['rates'];

            if(!isset($car_data[0])){
                $car_data=array($car_data);
            }
            foreach($car_data as $key =>$car_details){
           $hs[$i]['travelSector']= $car_details['carCompanyData']['travelSector']; 
           $hs[$i]['company_name']= $car_details['carCompanyData']['companyName'];
           $hs[$i]['company_code']= $car_details['carCompanyData']['companyCode'];
           $hs[$i]['company_access_level']= $car_details['carCompanyData']['accessLevel'];
           $hs[$i]['fareType']= $car_details['loyaltyNumbersList']['discountNumbers']['customerReferenceInfo']['referenceNumber'];
           $hs[$i]['referenceQualifier']= $car_details['loyaltyNumbersList']['discountNumbers']['customerReferenceInfo']['referenceQualifier'];           
           $hs[$i]['car_rate']=$car_details['rateDetailsInfo']['tariffInfo'][0]['rateAmount']; 
           if(isset($car_details['rateDetailsInfo']['tariffInfo'][0]['rateIdentifier']) && ($car_details['rateDetailsInfo']['tariffInfo'][0]['rateIdentifier'])){
            $hs[$i]['car_rateType']=$car_details['rateDetailsInfo']['tariffInfo'][0]['rateIdentifier'];  
           }   
           $hs[$i]['rate_currency']=$car_details['rateDetailsInfo']['tariffInfo'][0]['rateCurrency'];
           $hs[$i]['rate_category']=$car_details['rateDetailsInfo']['extraRateTypeInfo']['rateCategory'];
           $hs[$i]['car_vehicleTypeOwner']=$car_details['vehicleTypeInfo']['vehicleCharacteristic']['vehicleTypeOwner'];
           $hs[$i]['car_vehicleRentalPrefType']=$car_details['vehicleTypeInfo']['vehicleCharacteristic']['vehicleRentalPrefType'];
           if(isset($car_details['vehicleTypeInfo']['carModel']) && ($car_details['vehicleTypeInfo']['carModel'])!= "")
           {
            $car_model=$car_details['vehicleTypeInfo']['carModel'];
           }else{
             $car_model="";
           }
           $hs[$i]['car_model']=$car_model;
           if(isset($car_details['sizedPictures']) && ($car_details['sizedPictures'])!="")
           {
           $car_image=$car_details['sizedPictures']['imageURL']['address']['identifier'];
           }else{
             $car_image="";
           }
            $hs[$i]['car_image']=$car_image;
           $hs[$i]['car_pick_up']=$car_details['pickupDropoffLocations'][0]['locationType'];
           $hs[$i]['car_pick_up_ld_code']=$car_details['pickupDropoffLocations'][0]['locationDescription']['code'];
           $hs[$i]['car_pick_up_ld_name']=$car_details['pickupDropoffLocations'][0]['locationDescription']['name'];
           $hs[$i]['car_drop_off']=$car_details['pickupDropoffLocations'][1]['locationType'];
           $hs[$i]['car_drop_off_ld_code']=$car_details['pickupDropoffLocations'][1]['locationDescription']['code'];
           $hs[$i]['car_drop_off_ld_name']=$car_details['pickupDropoffLocations'][1]['locationDescription']['name'];
           $hs[$i]['car_pick_up_dt']=$car_details['pickupDropoffTime']['beginDateTime']['day'].'-'.
                                                 $car_details['pickupDropoffTime']['beginDateTime']['month'].'-'.
                                                 $car_details['pickupDropoffTime']['beginDateTime']['year'].'( '.
                                                 $car_details['pickupDropoffTime']['beginDateTime']['hour'].')';
           $hs[$i]['car_drop_off_dt']=$car_details['pickupDropoffTime']['endDateTime']['day'].'-'.
                                                 $car_details['pickupDropoffTime']['endDateTime']['month'].'-'.
                                                 $car_details['pickupDropoffTime']['endDateTime']['year'].'( '.
                                                 $car_details['pickupDropoffTime']['endDateTime']['hour'].')';
            $hs[$i]['car_pick_day']=$car_details['pickupDropoffTime']['beginDateTime']['day'];
            $hs[$i]['car_pick_month']=$car_details['pickupDropoffTime']['beginDateTime']['month'];
            $hs[$i]['car_pick_year']=$car_details['pickupDropoffTime']['beginDateTime']['year'];
            $hs[$i]['car_pick_hour']=$car_details['pickupDropoffTime']['beginDateTime']['hour'];
            $hs[$i]['car_drop_day']=$car_details['pickupDropoffTime']['endDateTime']['day'];
            $hs[$i]['car_drop_month']= $car_details['pickupDropoffTime']['endDateTime']['month'];
            $hs[$i]['car_drop_year']=$car_details['pickupDropoffTime']['endDateTime']['year'];
            $hs[$i]['car_drop_hour']=$car_details['pickupDropoffTime']['endDateTime']['hour'];
            $i++;
	       	}
            $data['car_result'] = $hs;
            return $data;
        }
        }
    }
    function xml2array($xmlStr, $get_attributes = 1, $priority = 'tag'){
        //print_r($xmlStr);die();
        // I renamed $url to $xmlStr, $url was the first parameter in the method if you
        // want to load from a URL then rename $xmlStr to $url everywhere in this method
        $contents = "";
        if (!function_exists('xml_parser_create')) {
            return array();
        }
        $parser = xml_parser_create('');
        
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($xmlStr), $xml_values);
        xml_parser_free($parser);
        if (!$xml_values)
            return; //Hmm...
        //echo "<pre>"; print_r($xml_values); echo "</pre>";  die();
        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();
        $current = & $xml_array;
        $repeated_tag_index = array();
        foreach ($xml_values as $data) {
            unset($attributes, $value);
            //echo "<pre>"; print_r($get_attributes); echo "</pre>";  die();
            extract($data);
            $result = array();
            $attributes_data = array();
            if (isset($value)) {
                if ($priority == 'tag')
                    $result = $value;
                else
                    $result['value'] = $value;
            }
            if (isset($attributes) and $get_attributes) {
                foreach ($attributes as $attr => $val) { 
                    if ($priority == 'tag')
                        $attributes_data[$attr] = $val;
                    else
                        $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
                }
            }
            if ($type == "open") {
                $parent[$level - 1] = & $current; 
                if (!is_array($current) or (!in_array($tag, array_keys($current)))) {
                    $current[$tag] = $result;
                    if ($attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    $current = & $current[$tag];
                }
                else {
                    if (isset($current[$tag][0])) {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        $repeated_tag_index[$tag . '_' . $level]++;
                    } else {
                        $current[$tag] = array(
                            $current[$tag],
                            $result
                        );
                        $repeated_tag_index[$tag . '_' . $level] = 2;
                        if (isset($current[$tag . '_attr'])) {
                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset($current[$tag . '_attr']);
                        }
                    }
                    $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                    $current = & $current[$tag][$last_item_index];
                }
            } elseif ($type == "complete") {
                if (!isset($current[$tag])) {
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                }
                else {
                    if (isset($current[$tag][0]) and is_array($current[$tag])) {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        if ($priority == 'tag' and $get_attributes and $attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag . '_' . $level]++;
                    } else {
                        $current[$tag] = array(
                            $current[$tag],
                            $result
                        );
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        if ($priority == 'tag' and $get_attributes) {
                            if (isset($current[$tag . '_attr'])) {
                                $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                                unset($current[$tag . '_attr']);
                            }
                            if ($attributes_data) {
                                $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
                    }
                }
            } elseif ($type == 'close') {
                $current = & $parent[$level - 1];
            }
        }
        //echo "<pre>"; print_r($xml_array); echo "</pre>";  die();
        return ($xml_array);
    }






function dateformatAMD($dateval,$timeval){  
    $day=substr($dateval,0,2);  
    $mon=substr($dateval,2,2);
    $year="20".substr($dateval,4,2);
    $hour=substr($timeval,0,2); 
    $min=substr($timeval,2,2);
    $newdate=$year.'-'.$mon.'-'.$day.'T'.$hour.':'.$min.':00';
    return $newdate;
}
// function call_iternary($idval)
// {
//     $data['result'] = $this->Flight_Model->get_flight_data_segments($idval);
// 	// echo '<pre/>';print_r($data);exit;
//     $this->load->view(PROJECT_THEME.'/flight/flight_details',$data);
// }

public function addToCart(){
   $uid=@$_REQUEST['uid'];
  
    if($uid!=''){
        $uid_v1 = json_decode(base64_decode($uid));     
        $session_id  = $uid_v1->sessionid;
        $SequenceNumber  = $uid_v1->SequenceNumber;
        $SecurityToken  = $uid_v1->SecurityToken;
        $result = $uid_v1->data;
        $request=$uid_v1->request;
        if($result!=''){
         $car_xml_response = VehicleSearchAvailabilityLiveReq($uid_v1->request);
         $car_xml_response = xmlstr_to_array($car_xml_response['Car_AvailabilityRes']);
         $_SESSION['car_amadeus_SessionId'] = $car_xml_response['soapenv:Header']['awsse:Session']['awsse:SessionId'];
         $_SESSION['car_amadeus_SequenceNumber'] = $car_xml_response['soapenv:Header']['awsse:Session']['awsse:SequenceNumber'];
         $_SESSION['car_amadeus_SecurityToken'] = $car_xml_response['soapenv:Header']['awsse:Session']['awsse:SecurityToken'];
            $cart_car = array(
                'session_id'                  => $session_id,
                'SequenceNumber'              => $SequenceNumber,
                'SecurityToken'               => $SecurityToken,
                'round_type'                  => $request->type,
                'travler'                     => $request->trav,
                'pick_up_loc'                 => $this->Car_Model->get_airport_cityname($request->pick_up_loc),
                'drop_down_loc'               => $this->Car_Model->get_airport_cityname($request->drop_down_loc),
              	'travelSector'                => $result->travelSector,
				'company_name'                => $result->company_name,
				'company_code'                => $result->company_code,
				'company_access_level'        => $result->company_access_level,
				'fareType'                    => $result->fareType,
				'referenceQualifier'          => $result->referenceQualifier,
				'car_rate'                    => $result->car_rate,
				'car_rateType'                => $result->car_rateType,
				'rate_currency'               => $result->rate_currency,
				'rate_category'               => $result->rate_category,
				'car_vehicleTypeOwner'        => $result->car_vehicleTypeOwner,   
				'car_vehicleRentalPrefType'   => $result->car_vehicleRentalPrefType,
				'car_model'                   => $result->car_model,
				'car_image'                   => $result->car_image,				
				'car_pick_up_ld_code'         => $result->car_pick_up_ld_name,
				'car_drop_off_ld_code'        => $result->car_drop_off_ld_name,
				'car_rate'                    => $result->car_rate,
				'car_drop_off_dt'             => $result->car_drop_off_dt,
				'car_pick_day'                => $result->car_pick_day,
				'car_pick_month'              => $result->car_pick_month,
				'car_pick_year'               => $result->car_pick_year,
				'car_pick_hour'               => $result->car_pick_hour,
				'car_drop_day'                => $result->car_drop_day,
				'car_drop_month'              => $result->car_drop_month,
				'car_drop_year'               => $result->car_drop_year,
				'car_drop_hour'               => $result->car_drop_hour,
				'segment_data'                => base64_encode(json_encode($result)),
				'admin_markup' 			      => '1',            
                'my_markup' 			      => '1',
				'site_currency' 		      => BASE_CURRENCY,
                'api_currency' 			      => $result->rate_currency,
                'request_scenario'		      => base64_encode(json_encode($request)),             
                'api_id' 				      => '1',
                'bundle_search_id'            => @$request->id 
                );
                if($request->id!=''||$request->id!=0){
                    $booking_cart_id = $this->Car_Model->delete_cart_car(@$request->id);
                   }
				 $booking_cart_id = $this->Car_Model->insert_cart_car($cart_car);
               if($this->session->userdata('user_id')){
					$user_type =$this->session->userdata('user_type');
					$user_id = $this->session->userdata('user_id');
				}else{
					$user_type = '';
					$user_id = '';
				}
				$cart_global = array(
					'parent_cart_id' => 0,
					'referal_id' => $booking_cart_id,
					'product_id' => '7',
					'user_type' => $user_type,
					'user_id' => $user_id,
					'session_id' => $session_id,
					'site_currency' => BASE_CURRENCY,
					'total_cost' => $result->car_rate,
					'ip_address' =>  $this->input->ip_address(),
					'timestamp' => date('Y-m-d H:i:s'),
                    'bundle_search_id'=> @$request->id 
					);  
                    if($request->id!=''||$request->id!=0){
                    $booking_cart_id = $this->Car_Model->delete_cart_global($request->id);
                   }              
            $cart_global_id = $this->Car_Model->insert_cart_global($cart_global);
            if(@$request->id!=''||@$request->id!=0){
                    $request = $this->custom_db->single_table_records('search_history', '', array('origin' => $request->id));
                    $fs = json_decode($request['data'][0]['search_data'], true);
                    if($fs['deal_type']=="flight_hotel"){

                    }elseif($fs['deal_type']=="flight_car"){
                        $data['status'] = 1;
                        $data['isCart'] = true;
                        $data['C_URL'] = WEB_URL.'bundle/'.base64_encode(json_encode($request['data'][0]['origin']));
                        echo json_encode($data);die;
                    }elseif($fs['deal_type']=="hotel_car"){
                        $data['status'] = 1;
                        $data['isCart'] = true;
                        $data['C_URL'] = WEB_URL.'bundle/'.base64_encode(json_encode($request['data'][0]['origin']));
                        echo json_encode($data);die;
                    }elseif($fs['deal_type']=="flight_hotel_car"){
                        $data['status'] = 1;
                        $data['isCart'] = true;
                        $data['C_URL'] = WEB_URL.'bundle/'.base64_encode(json_encode($request['data'][0]['origin']));
                        echo json_encode($data);die;
                    }else{
            $data['cart_status'] = 1;
            $data['isCart'] = true;
            $data['C_URL'] = WEB_URL.'booking/'.$session_id;
            }
    }else{
        $data['isCart'] = false;
    }
}else{
    $data['isCart'] = false;
}
$search_id = $_REQUEST['search_id'];
$search_module = 'CAR';
header('Location: '.WEB_URL.'booking/'.$session_id.'/'.$search_id.'/'.$search_module);
	//echo json_encode($data);    
}
}
 public function generate_rand_no($length = 24) {
        $alphabets = range('A','Z');
        $numbers = range('0','9');
        $final_array = array_merge($alphabets,$numbers);
        //$id = date("ymd").date("His");
        $id = '';
        while($length--) {
          $key = array_rand($final_array);
          $id .= $final_array[$key];
        }
        return $id;
    }

function ajaxPaginationData($session_data){
            // echo "<pre>"; print_r($session_data); echo "</pre>"; die();
        

        $page = $this->input->post('page');
        $offset = (!$page) ? 0 : $page ;
        
         //What ever the filtering data comes here
        $data = json_decode($this->input->post('filter'),true);
        $search_id = $this->input->post('SEARCH_ID');
        // echo "<pre/>";print_r($data);die;
        list($from_amt, $to_amt) = explode('-',$data['amount']);
        $from_amt = (int) trim(str_replace("$",'', $from_amt));
        $to_amt  =  (int) trim( str_replace("$",'', $to_amt));

        $amount_filter['car_rate >='] = $from_amt; 
        $amount_filter['car_rate <=']  = $to_amt;
        $supplier_list = array();
        if (isset($data['airline'][0])) {
          foreach ($data['airline'] as $key => $value) {
            $supplier_list[$key]  = $value;
        }
      }
      $supplier_list = count($supplier_list) > 0 ?  $supplier_list : NULL;
       /* echo "<pre>";
        print_r($data);*/

        $cond = array('amount_filter' => $amount_filter,
                       'sort_col' => isset($data['sort']['column']) ? $data['sort']['column'] : NULL,
                       'sort_val' => isset($data['sort']['value']) ? $data['sort']['value'] : NULL,
                       'supplier_list' => $supplier_list
                       ); 
        /*echo "<pre>";
        print_r($cond);
  
        exit;*/

        //total rows count
        $totalRec = $this->Car_Model->get_last_response_count($session_data,$cond);
        
        // echo $this->db->last_query();
        // echo $totalRec;
        // exit;
        $data['car_count'] =  $totalRec;
                     
        $flight_data = $this->Car_Model->get_last_response_data($session_data);
        

        //pagination configuration
        $config['first_link']  = 'First';
        $config['div']         = 'flightsdata'; //parent div tag id
        $config['base_url']    = WEB_URL.'car/ajaxPaginationData/'.$session_data;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
    
       $data['car_result']['car_result'] =   $this->Car_Model->get_last_response_flt($session_data,array('start'=>$offset,'limit'=>$this->perPage),$cond);
       $car_seg = $this->Car_Model->get_last_response_flt_more($session_data,$cond);
  // echo '<pre/>';print_r(json_decode($data['car_result']['car_result'][0]['request_scenario'], true));exit;
       $data['request'] = json_decode($car_seg[0]['request_scenario']);
       $data['car_result']['session_data'] = json_decode($car_seg[0]['segment_data'], true)['session_data'];
       if(!$data['car_result'])
        {
            $data['message'] = "No Result Found."; 
        }
        $data['search_id'] = $search_id;       
        // echo '<pre/>';print_r($data);exit;
      $this->load->view(PROJECT_THEME.'/car/ajax_result',$data);
    }

function format_time($time_f)
{
     switch ($time_f) {
        case '12_6A':
                $time_v1 = '0';$time_v2 = '0600';
            break;
             case '6_12A':
                  $time_v1 = '0600';$time_v2 = '1200';
            break;
             case '12_6P':
                   $time_v1 = '1200';$time_v2 = '1800';
            break;
             case '6_12P':
                 $time_v1 = '1800';$time_v2 = '2400';
            break;
    }

    return array('min' => $time_v1 , 'max' => $time_v2);

}


public function flight_mail_voucher($pnr_no){
  

        $count = $this->booking_model->getBookingPnr($pnr_no)->num_rows();

        if($count == 1){
            $b_data = $this->booking_model->getBookingPnr($pnr_no)->row();
            if($b_data->product_name == 'FLIGHT'){
            //  $data['terms_conditions'] = $this->booking_model->get_terms_conditions($product_id);
           $data['b_data'] = $this->booking_model->getBookingPnr($pnr_no)->row();     
           $booking_global_id=$b_data->booking_global_id;
           $billing_address_id=$b_data->billing_address_id;
             
                 $data['Passenger'] = $passenger = $this->booking_model->getPassengerbyid($booking_global_id)->result();
                 $data['booking_agent'] = $passenger = $this->booking_model->getagentbyid($billing_address_id)->result();
                $data['message'] = $this->load->view(PROJECT_THEME.'/booking/mail_voucher', $data,TRUE);
               
                $data['to'] = $data['booking_agent'][0]->billing_email;               
                $data['booking_status'] = $b_data->booking_status;
                $data['pnr_no'] = $pnr_no;
                $data['email_access'] = $this->email_model->get_email_acess()->row();
                $email_type = 'FLIGHT_BOOKING_VOUCHER';

                
                $Response = $this->email_model->sendmail_flightVoucher($data);
                $response = array('status' => 1);
               // echo"aschva";
                //echo json_encode($response);
            }
        }else{
            $response = array('status' => 0);
            echo json_encode($response);
        }
    }

public function rental($id){
    //$data['id11']=$id;

    $data['car_result']=$this->Car_Model->get_cardetailbyid($id);
     $data['rental_info'] = $this->load->view(PROJECT_THEME.'/car/carpopup',$data,true);
      echo json_encode($data);

}
public function CancelPnrBooking(){
  if($_POST){
        $pnr_no = base64_decode(base64_decode($_POST['pnr_no']));
       $count = $this->booking_model->getBookingPnr($pnr_no)->num_rows(); 
        if($count==1){
          $booking_details = $this->booking_model->getBookingPnr($pnr_no)->row();               
          $billing_address_id=$booking_details->billing_address_id;
          $booking_agent  = $this->booking_model->getagentbyid($billing_address_id)->result();
         
          $to_email_id = $booking_agent[0]->billing_email;
         
          $this->load->library('provab_mailer');
           if($booking_details->product_name=='CAR' && $booking_details->booking_status =='CONFIRMED' ){

                  $booking_status = "CANCEL_HOLD";
                  $this->db->set('booking_status', $booking_status);
                  $this->db->where('pnr_no', $pnr_no);
                  $this->db->update('booking_global');
                  $mail_data['firstname']  = $booking_details->leadpax;
                  $mail_subject =ucwords(PROJECT_TITLE).' - '.$pnr_no.' Cancellation Request Car';
                  $mail_message = $this->load->view(PROJECT_THEME.'/booking/cancellation_email', $mail_data,TRUE);
                  //send email to user
                  $this->provab_mailer->send_mail($to_email_id, $mail_subject, $mail_message);
                  $data['status'] = true;
                  $data['response'] ='Cancellation request sent';

           }else{
              $data['response'] ='Not Confirmed booking';
              $data['status'] =false;
           }
        }else{
          $data['response'] ='Booking not found';
              $data['status'] =false;
        }
  }else{
        $data['response'] ='PNR number is missing';
        $data['status'] =false;
  }
  echo json_encode($data);
  exit;
}








}
?>
