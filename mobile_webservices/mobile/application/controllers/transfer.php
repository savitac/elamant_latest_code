<?php
ob_start();if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Transfer extends CI_Controller {

    private $current_module;

    public function __construct() {
        parent::__construct();
        //we need to activate hotel api which are active for current domain and load those libraries
        $current_url    = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
        $current_url    = WEB_URL.$this->uri->uri_string(). $current_url;
        $url            = array('continue' => $current_url);
        $this->perPage  = 100;
        $this->session->set_userdata($url); 
        $this->load->model('Home_Model');
        $this->load->model('Flight_Model');   
        $this->load->model('cart_model');
        $this->load->model('booking_model');  
        $this->load->model('transferv1_model');  
        $this->load->model('email_model');  
        $this->load->library('encrypt');
        $this->load->library('session');
        $this->load->library('Ajax_pagination'); 
        $this->load->helper('transfer/transfer_helper');     
        $this->load->helper('string');
    }

    /**
     * index page of application will be loaded here
     */
    public function index(){
        $request            = $this->input->get();
        #print_r($request);die;
        $data['req']        = json_decode(json_encode($request));
        $data['request']    = base64_encode(json_encode($request));
        $data['city']    = $request['city'];
        if($request['city'] != ''){
            $this->load->view(PROJECT_THEME.'/transferv1/search_result_page', array('sight_seen_search_params' => $data));
        }else{
            redirect('home');
        }
    }
    
    public function GetResults($Req_before_decode = '',$search_id=''){        
        $search_request =  json_decode(base64_decode($Req_before_decode));
        #print_r($search_request);die;
        $rand_id                    = md5(time() . rand() . crypt(time()));
        $xml_response=vitoor_search_request($search_request->activity_selection_id);
        #echo "<pre/>";print_r($xml_response);die;
        $xml_response['request']=$search_request;
        $search_result_array = array(); 
        if($xml_response['Status']==true)
        {
            #print_r($xml_response);die;
            foreach($xml_response['Search']['TransferSearchResult']['TransferResults'] as $sight_results){
                 $search_result_array[] = $sight_results;
                $mprice[]=$sight_results['Price']['TotalDisplayFare'];
            }
            $xml_response['Search']['TransferSearchResult']['TransferResults'] = $search_result_array;
            #print_r($mprice);die;
            $dataresult['max']=max($mprice);
            $dataresult['min']=min($mprice);
            $dataresult['filter_result_count']=count($xml_response['Search']['TransferSearchResult']['TransferResults']);
            $xml_response['search_id'] = $search_id;
            $dataresult['data'] =  $this->load->view(PROJECT_THEME.'/transferv1/ajax_flight_result',$xml_response,true);
        }
        else
        {
            $dataresult['result'] =  $this->load->view(PROJECT_THEME.'/flight/ajax_result',$data,true);
        }
        echo json_encode($dataresult);
    }
    function transfer_details(){
        $get=$this->input->get();
        $xml_response=vitoor_details_request($get);
        $data_condition['origin'] = $get['search_id'];
         $data_condition['search_type'] = 'TRANSFERS';
          $search_data = $this->custom_db->single_table_records('search_history','*',$data_condition);
       # echo "<pre/>";print_r($xml_response);die;
        $xml_response['request']=$search_request;
        if ($xml_response['Status']) {
            $calendar_availability_date = $this->enable_calendar_availability($xml_response['ProductDetails']['TransferInfoResult']['Product_available_date']);
            $xml_response['ProductDetails']['TransferInfoResult']['Calendar_available_date'] = $calendar_availability_date;
            if($search_data['status']==1){
                $search_req = json_decode($search_data['data'][0]['search_data']);

            }
            
            $xml_response['search_req'] = $search_req;
            $xml_response['search_id'] =  $get['search_id'];

        $this->load->view(PROJECT_THEME.'/transferv1/search_details_page',$xml_response);
        }else{
            redirect('home');
        }
    }
    function select_tourgrade(){
        $get=$this->input->post();
        $xml_response=vitoor_triplist_request($get);
        #print_r($xml_response);die;
        if ($xml_response['Status']) {
            $xml_response['search_params'] = $get;
            $xml_response['search_params']['search_id'] = $get['search_id'];
            $response['status']=true;
            $response['data']=$this->load->view(PROJECT_THEME.'/transferv1/select_tourgrade',$xml_response,true);
        echo json_encode($response);
        }else{
            redirect('home');
        }
    }
    function booking(){
        $get=$this->input->post();
        #print_r($get);die;
        $xml_response=vitoor_triplist_block_request($get);

       
      #print_r($xml_response);die;
        if ($xml_response['Status']) {
            $xml_response['search_params'] = $get;
            $xml_response['room_response'] = $get['room_response'];
            $conf=$this->transferv1_model->Add_cart($xml_response);
        redirect('booking/'.$conf.'/'.$get['search_id'].'/TRANSFERS','refresh');
        }else{
            redirect('home');
        }
    }
    public function enable_calendar_availability($calendar_availability_date){
        
        if(valid_array($calendar_availability_date)){
            $available_str=array();
             foreach ($calendar_availability_date as $m_key => $m_value) {
                $avail_month_str = $m_key;
                
                foreach ($m_value as $d_key => $d_value) {
                    //j- to remove 0 from date, n to remove 0 from month
                    $date_str = $avail_month_str.'-'.$d_value;
                    $available_str[] = date('j-n-Y',strtotime($date_str));
                    
                }
             }
            
            return $available_str;
        }else{
            return '';
        }
    }

    /**
     *  Arjun J Gowda
     * Load Hotel Search Result
     * @param number $search_id unique number which identifies search criteria given by user at the time of searching
     */
    function search($search_id) {
        $safe_search_data = $this->transfer_model->get_safe_search_data($search_id);
        // Get all the hotels bookings source which are active
        $active_booking_source = $this->transfer_model->active_booking_source();

        if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
            $safe_search_data['data']['search_id'] = abs($search_id);
            $this->template->view('transfer/search_result_page', array('hotel_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
        } else {
            $this->template->view('general/popup_redirect');
        }
    }

 
    function CancelPnrBooking(){
      
       $this->load->model('booking_model');
       if($_POST){
            $pnr_no             =  base64_decode(base64_decode($_POST['pnr_no']));  
            $b_data = $this->booking_model->getBookingPnr($pnr_no)->result_array();  
            if($b_data){
                $app_reference  = $b_data[0]['con_pnr_no'];
                $sightseeing_request['AppReference'] = $app_reference;
                $sightseeing_request['CancelCode'] = "00";
                $sightseeing_request['CancelDescription']='Testing';
                $xml_response = viator_cancel_booking_request_params($app_reference,"00","Testing");
               
                if($xml_response['Status']==true){
                    if($xml_response['CancelBooking']['CancellationDetails']){
                        $update_booking = array(
                                    'booking_status'            => 'CANCELED',
                                    'cancellation_status'       => 'SUCCESS',
                                    'cancel_request'            => json_encode($sightseeing_request),
                                    'cancel_response'           => json_encode($xml_response)
                                );
                        $this->transferv1_model->update_global_booking_data($pnr_no,$update_booking);
                        $data['response'] ='Your Cancellation  Request Done';
                        $data['status'] ='Success';
                    }
                }else{
                     $data['response'] ='Booking not cancelled from API';
                     $data['status'] ='Failure';
                }
            }else{
                $data['response'] ='Unable to Process your Request';
                $data['status'] ='Failure';
            }
       }else{
          $data['response'] ='PNR number is missing';
          $data['status'] ='Failure';
       }
       
        echo json_encode($data);
        exit;
    }
}
