<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
include_once BASEPATH . 'libraries/payment_gateway/Crypt_RC4.php';
include_once BASEPATH . 'libraries/payment_gateway/Stripe.php';
/**
 *
 * @package Provab
 * @subpackage Transaction
 * @author Pravinkuamr P <pravinkumar.provab@gmail.com>
 * @version V1
 */
class Payment_gateway extends CI_Controller {
	/**
	 */
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'module_model' );
		$this->load->model ( 'transaction' );
	}

	function test() {
		$ebs_config = $this->config->item ( 'ebs_config' );
	}
	
	/**
	 * Redirection to payment gateway
	 *
	 * @param string $book_id
	 *        	Unique string to identify every booking - app_reference
	 * @param number $book_origin
	 *        	Unique origin of booking
	 */
	public function payment_old($search_id) {
		$params = $this->input->post ();
		
		if (valid_array ( $params ) == false) {
			redirect ( base_url () );
		}
		$app_reference = $params ['app_reference'];
		
		// Redirect to IRCTC
		// load_train_lib ( IRCTC_TRAIN_BOOKING_SOURCE );
		// $page_params = $this->train_lib->booking_form ( $app_reference );
		// echo $this->template->isolated_view ( 'share/dynamic_js_form_submission', $page_params );
		
		$this->load->model ( 'transaction' );
		
		$pg_record = $this->transaction->read_payment_record ( $app_reference );
		$temp_booking = $this->transaction->read_temp_booking_record ( $app_reference );
		$amount = $pg_record['amount'];
		$book_origin = $temp_booking ['id'];
		$response = array (
			'status' => FAILURE_STATUS 
			);
		
		if (empty ( $pg_record ) == false and valid_array ( $pg_record ) == true) {
			switch ($temp_booking ['course_id']) {
				case META_TRAIN_COURSE :

					// verify balance and deduct
				$response = $this->agent_model->verify_train_balance ( $app_reference );
				if ($response ['status'] == SUCCESS_STATUS) {
					$status = $this->agent_model->process_train_fare_deduction ( $app_reference, $response );
					$response ['status'] = $status;
						// update
					$page_params ['form_url'] = base_url () . 'index.php/payment_gateway/success/' . $search_id;
					$page_params ['form_method'] = 'POST';
					$page_params ['form_params'] ['app_reference'] = $app_reference;
					$page_params ['form_params'] ['product_type'] = $temp_booking ['course_id'];
					echo $this->template->isolated_view ( 'share/dynamic_js_form_submission', $page_params );
					exit ();
				} else {
					echo 'You Dont Have sufficient balance to book ticket';
					exit ();
				}
				break;
				case META_AIRLINE_COURSE :
				$this->set_pg_params ( $app_reference, $amount );
				$ebs_config = $this->config->item ( 'ebs_config' );
				$page_params['url'] = $ebs_config['url'];
				$page_params['hash_data'] = $ebs_config['hash_data'];
				echo $this->template->isolated_view ( 'general/ebs', $page_params);
				break;
				case META_TRANSFERS_COURSE :
				/* 	$params = json_decode($pg_record['request_params'], true);
					debug($pg_record);
					debug($params);
					
					$pg_initialize_data = array (
							'txnid' => $params['txnid'],
							'pgi_amount' => $params['booking_fare'] * 100,
							'firstname' => $params['firstname'],
							'email'=>$params['email'],
							'phone'=>$params['phone'],
							'curency'=>$params['curency'],
							'productinfo'=> $params['productinfo']
					);
					$payment_gateway_status = $this->config->item('enable_payment_gateway');
					debug($payment_gateway_status);
					exit; */
					$this->set_pg_params ( $app_reference, $amount );
					$ebs_config = $this->config->item ( 'ebs_config' );
					$page_params['url'] = $ebs_config['url'];
					$page_params['hash_data'] = $ebs_config['hash_data'];
					echo $this->template->isolated_view ( 'general/ebs', $page_params);
					break;
					default :
					echo '==>Under Construction - exit';
					exit ();
				}
			}
		}

	/**
	 * Redirection to payment gateway
	 * @param string $app_reference		Unique string to identify every booking - app_reference
	 * @param number $book_origin	Unique origin of booking
	 */
	// public function payment($app_reference){
	public function payment($app_reference,$book_origin, $card='', $month='', $year=''){
		//redirect('payment_gateway/demo_booking_blocked');//Blocked the payment gateway temporarly
		
		$this->load->model('transaction');
		$PG = $this->config->item('active_payment_gateway');
		
		load_pg_lib ( $PG );

		$pg_record = $this->transaction->read_payment_record($app_reference);
                //debug($pg_record);exit;
		//Converting Application Payment Amount to Pyment Gateway Currency
		//debug($pg_record); exit;

		$pg_record['amount'] = roundoff_number($pg_record['amount']);
		//debug($pg_record);exit;
		if (empty($pg_record) == false and valid_array($pg_record) == true) {
			$params = json_decode($pg_record['request_params'], true);
			$pg_initialize_data = array (
				'txnid' => $params['txnid'],
				'pgi_amount' => $pg_record['amount'],
				'firstname' => $params['firstname'],
				'email'=>$params['email'],
				'phone'=>$params['phone'],
				'productinfo'=> $params['productinfo'],
				'url' => 'mobile/index.php/payment_gateway/ebs_success_payment',
				'card-number'=> $card,
				'month'=> $month,
				'year'=> $year,
				);
		} else {
			echo 'Under Construction :payment gatway';
			exit;
		}

		//defined in provab_config.php
		$payment_gateway_status = $this->config->item('enable_payment_gateway');
		if ($payment_gateway_status == true) {
			$this->pg->initialize ( $pg_initialize_data );
			$page_data['pay_data'] = $this->pg->process_payment ($app_reference);
			/*print_r($page_data);
			exit();*/
			$this->auth_success($page_data['pay_data']['description'],$app_reference,$page_data['pay_data']['status']);
		} else {
			//directly going to process booking
//			echo 'Booking Can Not Be Done!!!';
//			exit;
			redirect('flight/secure_booking/'.$app_reference);
			//redirect('hotel/secure_booking/'.$app_reference.'/'.$book_origin);
			//redirect('bus/secure_booking/'.$app_reference.'/'.$book_origin);
		}
	}

	public function paypal_payment($app_reference){
		//redirect('payment_gateway/demo_booking_blocked');//Blocked the payment gateway temporarly
		
		$this->load->model('transaction');
		$PG = $this->config->item('active_payment_gateway1');
		
		load_pg_lib ( $PG );

		$pg_record = $this->transaction->read_payment_record($app_reference);
		//debug($pg_record);exit;
		//Converting Application Payment Amount to Pyment Gateway Currency
		//debug($pg_record); exit;

		$pg_record ['amount'] = roundoff_number ( $pg_record ['amount'] * $pg_record ['currency_conversion_rate'] );
		//debug($pg_record);exit;
		if (empty($pg_record) == false and valid_array($pg_record) == true) {
			$params = json_decode($pg_record['request_params'], true);
			$pg_initialize_data = array (
				'txnid' => $params['txnid'],
				'pgi_amount' => $pg_record['amount'],
				'firstname' => $params['firstname'],
				'email'=>$params['email'],
				'phone'=>$params['phone'],
				'productinfo'=> $params['productinfo'],
				);
		} else {
			echo 'Under Construction :payment gatway';
			exit;
		}
		//defined in provab_config.php
		$payment_gateway_status = $this->config->item('enable_payment_gateway');
		if ($payment_gateway_status == true) {
			$this->pg->initialize ( $pg_initialize_data );
			$page_data['pay_data'] = $this->pg->process_payment ($app_reference);
			//echo $PG; exit;
			//debug($page_data['pay_data']);exit;
			//Not to show cache data in browser
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");

			//debug($page_data); exit;
			echo $this->template->isolated_view('payment/'.$PG.'/pay', $page_data);
			// echo $this->template->isolated_view('payment/'.$PG.'/paypal', $page_data);
		} else {
			//directly going to process booking
//			echo 'Booking Can Not Be Done!!!';
//			exit;
			redirect('flight/secure_booking/'.$app_reference);
			//redirect('hotel/secure_booking/'.$app_reference.'/'.$book_origin);
			//redirect('bus/secure_booking/'.$app_reference.'/'.$book_origin);
		}
	}

	/**
	 * 
	 * @param unknown $book_id
	 * @param unknown $book_origin
	 */
	public function payment_process($book_id, $book_origin) {
		$this->load->model ( 'transaction' );
		$PG = $this->config->item ( 'active_payment_gateway' );
		load_pg_lib ( $PG );

		$pg_record = $this->transaction->read_payment_record ( $book_id );
		// Converting Application Payment Amount to Pyment Gateway Currency
		$pg_record ['amount'] = roundoff_number ( $pg_record ['amount']);
		if (empty ( $pg_record ) == false and valid_array ( $pg_record ) == true) {
			$params = json_decode ( $pg_record ['request_params'], true );
			$pg_initialize_data = array (
				'txnid' => $params ['txnid'],
				'pgi_amount' => $pg_record ['amount'],
				'firstname' => $params ['firstname'],
				'email' => $params ['email'],
				'phone' => $params ['phone'],
				'productinfo' => $params ['productinfo'],
				'url' => 'mobile/index.php/payment_gateway/response_crs/'
				);
		} else {
			echo 'Under Construction :p';
			exit ();
		}
		// defined in provab_config.php
		$payment_gateway_status = $this->config->item ( 'enable_payment_gateway' );
		if ($payment_gateway_status == true) {
			$this->pg->initialize ( $pg_initialize_data );
			$page_data ['pay_data'] = $this->pg->process_payment ($book_id);
			// Not to show cache data in browser
			header ( "Cache-Control: no-store, no-cache, must-revalidate, max-age=0" );
			header ( "Cache-Control: post-check=0, pre-check=0", false );
			header ( "Pragma: no-cache" );
			echo $this->template->isolated_view ( 'payment/' . $PG . '/ebs', $page_data );
		} else {
			redirect ();
			// redirect('hotel/secure_booking/'.$book_id.'/'.$book_origin);
			// redirect('bus/secure_booking/'.$book_id.'/'.$book_origin);
		}
	}
	/**
	 *
	 */

	public function auth_success($info, $txn_id, $status)
	{
		$this->load->model('transaction');
		$product = $info; // $_REQUEST ['productinfo'];
		$book_id = $txn_id; //$_REQUEST ['txnid'];
		
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array ('book_id' => $book_id) );
		$pg_status = lcfirst($status); //$_REQUEST['status'];
		$pg_record = $this->transaction->read_payment_record($book_id);
		$book_respd = substr($book_id,0,1);
		switch ($book_respd[0]) {
			case 'F':
				$product = META_AIRLINE_COURSE;
				break;
			case 'H':
				$product = META_ACCOMODATION_COURSE;
				break;
			case 'T':
				$product = META_TRANSFERS_COURSE;
				break;
			
			default:
				echo 'error in type of travel'; exit();
				break;
		}
	//echo $product;exit;

		/*print_r($pg_status);
		exit();*/
		/*if ($pg_status == 'Success' and empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {*/
		if ($pg_status == 'success') {
			//echo 'asd';
			//update payment gateway status
			$response_params = $_REQUEST;
			$status = $this->transaction->update_payment_record_status($book_id, ACCEPTED, $response_params);
			$book_origin = $temp_booking ['data'] ['0'] ['id'];
			$this->redirect_booking($product, $book_id, $book_origin);

		}else{
$this->redirect_booking($product, $book_id, $book_origin);
			echo 'Else';
			exit();
			$this->auth_cancel($product,$book_id );
		}
	}

	function success() {
		$this->load->model('transaction');
		$product = $_REQUEST ['productinfo'];
		$book_id = $_REQUEST ['txnid'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
			'book_id' => $book_id 
			) );
		/*echo $book_id; echo '<br>';
		debug($temp_booking); die();*/
		$pg_status = $_REQUEST['status'];
		$pg_record = $this->transaction->read_payment_record($book_id);
		if ($pg_status == 'success' and empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {
			//update payment gateway status
			$response_params = $_REQUEST;
			$this->transaction->update_payment_record_status($book_id, ACCEPTED, $response_params);
			$book_origin = $temp_booking ['data'] ['0'] ['id'];
			switch ($product) {
				case META_AIRLINE_COURSE :
				redirect ( base_url () . 'index.php/flight/secure_booking/' . $book_id );
				break;
				case META_BUS_COURSE :
				redirect ( base_url () . 'index.php/mobile/bus/secure_booking/' . $book_id . '/' . $book_origin );
				break;
				case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'index.php/accomodation/secure_booking/' . $book_id );
					//redirect ( base_url () . 'index.php/accomodation/secure_booking/' . $book_id . '/' . $book_origin );
				break;
				case META_TRANSFERS_COURSE : 
				redirect ( base_url () . 'index.php/transfer/secure_booking/'.$book_id);
				break;
				case META_ACTIVITY_COURSE :
				redirect ( base_url () . 'index.php/activity/secure_booking/'.$book_id. '/' . $book_origin);
				default :
				redirect ( base_url().'index.php/transaction/cancel' );
				break;
			}
		}
	}
	
	private function redirect_booking($product, $book_id, $book_origin)
	{


		switch ($product) {
			case META_AIRLINE_COURSE :
				redirect ( base_url () . 'mobile/index.php/flight/process_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_BUS_COURSE :
				redirect ( base_url () . 'mobile/index.php/bus/process_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'mobile/index.php/hotel/process_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_TRANSFERS_COURSE:
				redirect ( base_url () . 'mobile/index.php/transferv1/process_booking/' . $book_id . '/' . $book_origin );
				break;
			case META_CAR_COURSE :
				redirect ( base_url () . 'mobile/index.php/car/process_booking/' . $book_id . '/' . $book_origin );
				break;
			default :
				redirect ( base_url().'mobile/index.php/transaction/cancel' );
				break;
		}
	}
	
		function success_mobile() {
		//echo "sfffs";exit;
		$this->load->model('transaction');
		//$product = $_REQUEST ['productinfo'];
		//$book_id = $_REQUEST ['txnid'];

	    $product = $this->input->post('product');
	    $book_id = $this->input->post('txnid');
	    $pg_status = $this->input->post('status');

		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
			'book_id' => $book_id 
			) );

		$pg_record = $this->transaction->read_payment_record($book_id);
		//debug($temp_booking);exit;
		if ($pg_status == 'success' and valid_array ( $temp_booking ['data'] )) {
			//echo "dfds";exit;
			//update payment gateway status
			//$response_params = $_REQUEST;
			$this->transaction->update_payment_record_status($book_id, ACCEPTED);
			$book_origin = $temp_booking ['data'] ['0'] ['id'];
			
			//debug($book_origin);exit;
			switch ($product) {

				case META_AIRLINE_COURSE :
               //secho $book_id;exit;
				redirect ( base_url () . 'mobile_webservices/mobile/index.php/flight/secure_booking/' . $book_id.'/'.$book_origin );
				break;
	
				case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'mobile_webservices/mobile/index.php/accomodation/secure_booking/' . $book_id.'/'.$book_origin );
					//redirect ( base_url () . 'index.php/accomodation/secure_booking/' . $book_id . '/' . $book_origin );
				break;
				case META_TRANSFERS_COURSE : 
				redirect ( base_url () . 'mobile_webservices/mobile/index.php/transfer/secure_booking/'. $book_id.'/'.$book_origin );
				break;
				case META_ACTIVITY_COURSE :
				redirect ( base_url () . 'mobile_webservices/mobile/index.php/activity/secure_booking/'.$book_id. '/' . $book_origin);
				default :
				redirect ( base_url () . 'mobile_webservices/mobile/index.php/transaction/cancel_mobile' );
				break;
			}
		}
	}

	/**
	 *
	 */


	function auth_cancel($prod, $txn_id ) {
		$this->load->model('transaction');
		$product = $prod;//$_REQUEST ['productinfo'];
		$book_id = $txn_id; //$_REQUEST ['txnid'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
				'book_id' => $book_id 
		) );
		$pg_record = $this->transaction->read_payment_record($book_id);
		if (empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {
			$response_params = array($product,$book_id);
			$this->transaction->update_payment_record_status($book_id, DECLINED, $response_params);
			$msg = "Payment Unsuccessful, Please try again.";
			switch ($product) {
				case META_AIRLINE_COURSE :
					redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_BUS_COURSE :
					redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_ACCOMODATION_COURSE :
					redirect ( base_url () . 'index.php/hotel/exception?op=booking_exception&notification=' . $msg );
					break;
			}
		}
	}

	function cancel() {
		$this->load->model('transaction');
		$product = $_REQUEST ['productinfo'];
		$book_id = $_REQUEST ['txnid'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
			'book_id' => $book_id 
			) );
		$pg_record = $this->transaction->read_payment_record($book_id);
		if (empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {
			$response_params = $_REQUEST;
			$this->transaction->update_payment_record_status($book_id, DECLINED, $response_params);
			$msg = "Payment Unsuccessful, Please try again.";
			switch ($product) {
				case META_AIRLINE_COURSE :
				redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_BUS_COURSE :
				redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'index.php/accomodation/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_ACTIVITY_COURSE :
				redirect ( base_url () . 'index.php/activity/exception?op=booking_exception&notification=' . $msg );
				break;

			}
		}
	}
	
	function cancel_mobile() {
		$this->load->model('transaction');
	    $product = $this->input->post('product');
	    $book_id = $this->input->post('txnid');
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
				'book_id' => $book_id 
		) );
		//debug($book_id);exit;
		$pg_record = $this->transaction->read_payment_record($book_id);
		if (empty($pg_record) == false and valid_array($pg_record) == true && valid_array ( $temp_booking ['data'] )) {
			$response_params = $_REQUEST;
			$this->transaction->update_payment_record_status($book_id, DECLINED);
			$msg = "Payment Unsuccessful, Please try again.";
			switch ($product) {
				case META_AIRLINE_COURSE :
					redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_BUS_COURSE :
					redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_ACCOMODATION_COURSE :
					redirect ( base_url () . 'index.php/accomodation/exception?op=booking_exception&notification=' . $msg );
					break;
				case META_HAJJ_UMRAH_PACKAGE:
					redirect ( base_url () . 'index.php/trips/exception?op=booking_exception&notification=' . $msg );
					break;				
			}
		}
	}
	

	
	/**
	 */
	function success_old($search_id) {
		$this->load->model ( 'transaction' );
		$product = $this->input->post ( 'product_type' );
		$book_id = $this->input->post ( 'app_reference' );
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
			'book_id' => $book_id 
			) );
		$pg_status = 'success';
		$pg_record = $this->transaction->read_payment_record ( $book_id );
		if ($pg_status == 'success' and empty ( $pg_record ) == false and valid_array ( $pg_record ) == true && valid_array ( $temp_booking ['data'] )) {
			// update payment gateway status
			$response_params = $this->input->post ();
			$this->transaction->update_payment_record_status ( $book_id, ACCEPTED, $response_params );
			
			$page_params ['form_url'] = $this->redirect_process_url ( $product, $book_id, $search_id );
			$page_params ['form_method'] = 'POST';
			$page_params ['form_params'] ['app_reference'] = $book_id;
			echo $this->template->isolated_view ( 'share/dynamic_js_form_submission', $page_params );
		}
	}
	private function redirect_process_url($product, $book_id, $search_id = '') {
		$url = '';
		switch ($product) {
			case META_AIRLINE_COURSE :
			$url = (base_url () . 'index.php/flight/secure_booking/' . $book_id);
			break;
			case META_BUS_COURSE :
			$url = (base_url () . 'index.php/bus/secure_booking/' . $book_id);
			break;
			case META_ACCOMODATION_COURSE :
			$url = (base_url () . 'index.php/hotel/secure_booking/' . $book_id);
			break;
			case META_TRAIN_COURSE :
			$url = (base_url () . 'index.php/train/pre_booking/' . $search_id);
			break;
			case INSTANT_RECHARGE :
			$url = (base_url () . 'index.php/management/confirm_payment/' . $book_id);
			break;
			case META_TRANSFERS_COURSE :
			echo "ok";exit;
				//FIXME
				//$booking_reference = explode("_", $_POST['module_name']);
			redirect ( base_url () . 'index.php/transfer/secure_booking/'.$book_id.'/'. $booking_reference[1]);
			break;
			default :
			redirect ( base_url () . 'index.php/transaction/cancel' );
			break;
		}
		return $url;
	}
	
	/**
	 */
	function cancel_old() {
		$this->load->model ( 'transaction' );
		$product = $_REQUEST ['product_type'];
		$book_id = $_REQUEST ['app_reference'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
			'book_id' => $book_id 
			) );
		$pg_record = $this->transaction->read_payment_record ( $book_id );
		if (empty ( $pg_record ) == false and valid_array ( $pg_record ) == true && valid_array ( $temp_booking ['data'] )) {
			$response_params = $_REQUEST;
			$this->transaction->update_payment_record_status ( $book_id, 'failed', $response_params );
			$msg = "Payment Unsuccessful, Please try again.";
			switch ($product) {
				case META_AIRLINE_COURSE :
				redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_BUS_COURSE :
				redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'index.php/hotel/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_TRAIN_COURSE :
				redirect ( base_url () . 'index.php/train/exception?op=booking_exception&notification=' . $msg );
				break;
			}
		}
	}
	
	/**
	 * Do instant recharge
	 */
	function instant_recharge($payment_id) {
		$transaction = $this->domain_management_model->master_transaction_request_list ( $payment_id );
		if (valid_array ( $transaction ) == true && strcmp ( $transaction [0] ['status'], 'pending' ) == 0) {
			$transaction = $transaction [0];
			$book_id = $reference_id = $transaction ['system_transaction_id'];
			$verification_amount = $transaction ['amount'];
			$firstname = $this->entity_agency_name;
			$email = $this->entity_email;
			$phone = $this->entity_phone;
			$productinfo = INSTANT_RECHARGE;
			$convenience_fees = $transaction ['transaction_charges'];
			$promocode_discount = 0;
			
			$amount = $verification_amount + $convenience_fees;
			// load model
			$this->load->model ( 'transaction' );
			$this->transaction->create_payment_record ( $book_id, $verification_amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount );
			$attributes = json_decode($transaction['attributes'], true);
			$this->set_pg_params ( $reference_id, $amount, @$attributes['card_type'], @$attributes['payment_mode'] );
			$ebs_config = $this->config->item ( 'ebs_config' );
			echo $this->template->isolated_view ( 'general/ebs', $ebs_config );
		} else {
			// failure
		}
	}
	private function set_pg_params($reference_id, $amount, $card_brand='', $payment_mode='') {
		$ebs_config = $this->config->item ( 'ebs_config' );
		$_POST ['account_id'] = trim ( $ebs_config ['account_id'] );
		$_POST ['address'] = 'Bangalore';
		$_POST ['amount'] = $amount;
		$_POST ['bank_code'] = "";
		$_POST ['card_brand'] = $card_brand;
		$_POST ['channel'] = 0;
		$_POST ['city'] = "Bang";
		$_POST ['country'] = "IN";
		$_POST ['currency'] = "INR";
		$_POST ['description'] = "Payment Transaction";
		$_POST ['display_currency'] = "INR";
		$_POST ['display_currency_rates'] = 1; // remove in case of issue
		$_POST ['email'] = isset($this->entity_email) ? $this->entity_email : 'b2c@neptune.com';
		$_POST ['emi'] = "";
		$_POST ['mode'] = trim ( $ebs_config ['mode'] );
		$_POST ['name'] = $this->entity_domain_name;
		$_POST ['page_id'] = "";
		$_POST ['payment_mode'] = $payment_mode;
		$_POST ['payment_option'] = "";
		$_POST ['phone'] = isset($this->entity_phone) ? $this->entity_phone : '9743937617';
		$_POST ['postal_code'] = "560078";
		$_POST ['reference_no'] = $reference_id;
		$_POST ['return_url'] = base_url () . "payment_gateway/response";
		$_POST ['ship_address'] = "Bang";
		$_POST ['ship_city'] = "";
		$_POST ['ship_country'] = "";
		$_POST ['ship_name'] = "";
		$_POST ['ship_phone'] = "";
		$_POST ['ship_postal_code'] = "";
		$_POST ['ship_state'] = "";
		$_POST ['state'] = "Karnataka";
		unset( $_POST['app_reference']);
	}
	function response() {
		$this->load->model ( 'flight_model' );
		$this->load->model ( 'transaction' );
		// $response = $this->flight_model->get_static_response (17);
		// $response_array = json_decode( $response , TRUE);
		
		$response_array = $_REQUEST;
		
		$book_id = @$response_array ['MerchantRefNo'];
		$pg_record = $this->transaction->read_payment_record ( $book_id );
		if (empty ( $pg_record ) == true || valid_array ( $pg_record ) == false) {
			echo 'Unable to process transaction-001';
			exit ();
		}
		$request_params = json_decode ( $pg_record ['request_params'], TRUE );
		debug($request_params ['productinfo']);
		$product = $request_params ['productinfo'];
		if (valid_array ( $response_array ) && intval ( $response_array ['ResponseCode'] ) == 0) {
			// update payment gateway status
			$response_params = $this->input->post ();
			$this->transaction->update_payment_record_status ( $book_id, ACCEPTED, $response_array );
			
			$page_params ['form_url'] = $this->redirect_process_url ( $product, $book_id );
			$page_params ['form_method'] = 'POST';
			$page_params ['form_params'] ['app_reference'] = $book_id;
			echo $this->template->isolated_view ( 'share/dynamic_js_form_submission', $page_params );
		} else {
			$this->transaction->update_payment_record_status ( $book_id, 'failed', $response_array );
			$msg = "Payment Unsuccessful, Please try again.";
			switch ($product) {
				case META_AIRLINE_COURSE :
				redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_BUS_COURSE :
				redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'index.php/hotel/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_TRAIN_COURSE :
				redirect ( base_url () . 'index.php/train/exception?op=booking_exception&notification=' . $msg );
				break;
				default :
				echo 'unable to process your transction';
				exit ();
				break;
			}
		}
		exit ();
	}

	public function ebs_success_payment() {
		error_reporting(0);
		$PG = $this->config->item ( 'active_payment_gateway' );
		
		load_pg_lib ( $PG );

		$secret_key = $this->config->item ( 'ebs_key' );
		//echo"kkkk";echo $secret_key;exit;
		
/*		define ( 'EBS_KEY', 'ebskey' );*/
		$secret_key = 'ebskey';

		if (isset ( $_REQUEST ['DR'] )) {
			$DR = preg_replace ( "/\s/", "+", $_GET ['DR'] );
				
			$rc4 = new Crypt_RC4 ( $secret_key );
			//	debug( $rc4); exit;
			$QueryString = base64_decode ( $DR );
			//debug($QueryString);
			$rc4->decrypt ( $QueryString );
			$QueryString = explode ( '&', $QueryString );
			
			$response = array (); // $QueryString3=decrypt($QueryString[0]);
			//print_r($QueryString);
			//print_r($QueryString);exit;
			foreach ( $QueryString as $param ) {
				$param = explode ( '=', $param );
				$response [$param [0]] = urldecode ( $param [1] );
			}
		}	
		$this->load->model ( 'transaction' );
		//$product = 'VHCID1420613748';// $response ['Description'];
		
	//	debug( $response); exit;
		$book_id = $response ['MerchantRefNo'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
			'book_id' => $book_id 
			) );
		$pg_status = $response ['ResponseMessage'];
		$pg_record = $this->transaction->read_payment_record ( $book_id );
		if ($pg_status == 'Transaction Successful' and empty ( $pg_record ) == false and valid_array ( $pg_record ) == true && valid_array ( $temp_booking ['data'] )) {
			// update payment gateway status

			$request_params  = json_decode($pg_record['request_params'],true);
			$product = $request_params['productinfo'];

			$response_params = $response;
			$this->transaction->update_payment_record_status ( $book_id, ACCEPTED, $response_params );
			$book_origin = $temp_booking ['data'] ['0'] ['id'];
			switch ($product) {
				case META_AIRLINE_COURSE :
			//	echo  base_url () . 'mobile/index.php/flight/secure_booking/' . $book_id . '/' . $book_origin ;exit;
				redirect ( base_url () . 'mobile/index.php/flight/secure_booking/' . $book_id . '/' . $book_origin );
				break;
				case META_BUS_COURSE :
				redirect ( base_url () . 'mobile/index.php/bus/secure_booking/' . $book_id . '/' . $book_origin );
				break;
				case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'mobile/index.php/hotel/secure_booking/' . $book_id . '/' . $book_origin );
				break;
				default :
				redirect ( base_url () . 'mobile/index.php/payment_gateway/ebs_cancel_payment' );
				break;
			}
		}
	}
	function ebs_cancel_payment() {
		$PG = $this->config->item ( 'active_payment_gateway' );
		load_pg_lib ( $PG );
		
		if ($_SERVER ['HTTP_HOST'] == '192.168.0.25' || $_SERVER ['HTTP_HOST'] == 'localhost') {
			define ( 'EBS_KEY', 'ebskey' ); // MAKE DYNAMIC for Test/Live
				                                // define ( 'EBS_KEY', '547a286e2f88072b88c97d46690864bc' ); // Live Secret Key
		} else {
			define ( 'EBS_KEY', 'ebskey' );
		}
		$secret_key = EBS_KEY;
		if (isset ( $_REQUEST ['DR'] )) {
			$DR = preg_replace ( "/\s/", "+", $_GET ['DR'] );
			
			$rc4 = new Crypt_RC4 ( $secret_key );
			$QueryString = base64_decode ( $DR );
			$rc4->decrypt ( $QueryString );
			$QueryString = explode ( '&', $QueryString );
			
			$response = array (); // $QueryString3=decrypt($QueryString[0]);print_r($QueryString3);
			foreach ( $QueryString as $param ) {
				$param = explode ( '=', $param );
				$response [$param [0]] = urldecode ( $param [1] );
			}
		}
		$this->load->model ( 'transaction' );
		$product = $response ['Description'];
		$book_id = $response ['MerchantRefNo'];
		$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
			'book_id' => $book_id 
			) );
		$pg_record = $this->transaction->read_payment_record ( $book_id );
		if (empty ( $pg_record ) == false and valid_array ( $pg_record ) == true && valid_array ( $temp_booking ['data'] )) {
			$response_params = $response;
			$this->transaction->update_payment_record_status ( $book_id, DECLINED, $response_params );
			$msg = "Payment Unsuccessful, Please try again.";
			switch ($product) {
				case META_AIRLINE_COURSE :
				redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_BUS_COURSE :
				redirect ( base_url () . 'index.php/bus/exception?op=booking_exception&notification=' . $msg );
				break;
				case META_ACCOMODATION_COURSE :
				redirect ( base_url () . 'index.php/hotel/exception?op=booking_exception&notification=' . $msg );
				break;
			}
		}
	}

	function transaction_log() {
		load_pg_lib ( 'PAYU' );
		echo $this->template->isolated_view ( 'payment/PAYU/pay' );
	}
	/**
	 * 
	 * @param payment gateway Response for Hotel Crs
	 */
	function response_crs() {	
		$this->load->library('payment_gateway/Crypt_RC4');		
		$status = $this->config->item('active_payment_system');
		if ($status == 'test') {
			$secret_key = 'ebskey';					
		} else {
			$secret_key = 'ebskey';		
		}
		
		$book_id = $_GET['book_id'];
		if (isset ( $_REQUEST ['DR'] )) {
			$DR = preg_replace ( "/\s/", "+", $_GET ['DR'] );

			$rc4 = new Crypt_RC4 ( $secret_key );
			$QueryString = base64_decode ( $DR );
			$rc4->decrypt ( $QueryString );
			$QueryString = explode ( '&', $QueryString );

			$response = array (); // $QueryString3=decrypt($QueryString[0]);print_r($QueryString3);
			foreach ( $QueryString as $param ) {
				$param = explode ( '=', $param );
				$response [$param [0]] = urldecode ( $param [1] );
			}
		}

		if ($response ['ResponseCode'] == 0) {
			$this->load->model ( 'transaction' );

			// redirect(base_url().'Payment_gateway/success')
			$temp_booking = $this->custom_db->single_table_records ( 'temp_booking', '', array (
				'book_id' => $book_id
				) );
			$pg_record = $this->transaction->read_payment_record ( $book_id );
			if (empty ( $pg_record ) == false and valid_array ( $pg_record ) == true && valid_array ( $temp_booking ['data'] )) { 
			// $pg_status == 'success' and
				// update payment gateway status
				$response_params = $response;
				$this->transaction->update_payment_record_status ( $book_id, ACCEPTED, $response_params );
				$book_origin = $temp_booking ['data'] ['0'] ['id'];
				$request_params = json_decode ( $pg_record ['request_params'] );
				switch ($request_params->productinfo) {

					case META_ACCOMODATION_COURSE :
					redirect ( base_url () . 'index.php/hotel/secure_booking_crs/' . $book_id . '/' . $book_origin );
					break;
					default :
					redirect ( base_url () . 'index.php/transaction/cancel' );
					break;
				}
			}
		}
	}
	
	function stripe_payment(){

			if ($_POST) {
				Stripe::setApiKey("sk_live_f4VDDXiXAMBNWFYvw1GevYNJ");
				//pk_test_gxEaG8CYNYvXAyQwnuA5OmgX
				$error = '';
				$success = '';

			
					if (!isset($_POST['source'])){
						throw new Exception("The Stripe Token was not generated correctly");
					}

					$abc = array("amount" => $_POST['amount'],
						"currency" => $_POST['currency'],
						"source" => $_POST['source'],
					"receipt_email" => $_POST['receipt_email']);

					$charge = Stripe::create($abc);
					//debug($charge);exit('dsfds');
					$result["msg"] = 'Your payment was successful.';
				// catch (Exception $e) {
				// 	$error = $e->getMessage();
				// }
			}
			echo json_encode($charge); exit;
	}

}
