<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package    Provab
 * @subpackage General
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */

class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('module_model');
		$this->load->library('booking_data_formatter');
		//$this->output->enable_profiler(TRUE);
	}

	function create_default_domain($domain_key_name='192.168.0.26')
	{
		include_once DOMAIN_CONFIG.'default_domain_configuration.php';
	}

	/**
	 * index page of application will be loaded here
	 */
	function index()
	{
		if (is_logged_in_user()) {
			redirect('menu/index');
		}
	}

	/**
	 * User Profile Management
	 */
	function profile()
	{
		validate_user_login();
		$op_data = $this->input->post();
		$page_data = '';
		$this->load->model('transaction_model');
		$currency_obj = new Currency();
		$page_data['currency_obj'] = $currency_obj;
		if (valid_array($op_data) == true && empty($op_data['title']) == false && empty($op_data['first_name']) == false && empty($op_data['last_name']) == false &&
			empty($op_data['country_code']) == false && empty($op_data['phone']) == false && empty($op_data['address']) == false) {
			//Application Logger
			$notification_users = $this->user_model->get_admin_user_id();
			$remarks = $op_data['first_name'].' Updated Profile Details';
			$action_query_string = array();
			$action_query_string['user_id'] = $this->entity_user_id;
			$action_query_string['uuid'] = $this->entity_uuid;
			$action_query_string['user_type'] = B2C_USER;
			
			$this->application_logger->profile_update($op_data['first_name'], $remarks, $action_query_string, array(), $this->entity_user_id, $notification_users);
			
			$this->custom_db->update_record('user', $op_data, array('user_id' => $this->entity_user_id, 'uuid' => $this->entity_uuid));
			//PROFILE IMAGE UPLOAD
			if (valid_array($_FILES) == true and $_FILES['image']['error'] == 0 and $_FILES['image']['size'] > 0) {
				$config['upload_path'] = $this->template->domain_image_upload_path();//FIXME: Jaganath get Correct Path
				
				$config['allowed_types'] = '*';
				$config['file_name'] = time();
				$config['max_size'] = '1000000';
				$config['max_width']  = '';
				$config['max_height']  = '';
				$config['remove_spaces']  = false;
				//UPDATE
				$temp_record = $this->custom_db->single_table_records('user', 'image', array('user_id' => $this->entity_user_id));
				$icon = $temp_record['data'][0]['image'];
				//DELETE OLD FILES
				if (empty($icon) == false) {
					if (file_exists($config['upload_path'].$icon)) {
						unlink($config['upload_path'].$icon);
					}
				}
				//UPLOAD IMAGE
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('image')) {
					//debug($config);
					echo $this->upload->display_errors();exit;
				} else {
					$image_data =  $this->upload->data();
				}
				$this->custom_db->update_record('user', array('image' => @$image_data['file_name']), array('user_id' => $this->entity_user_id));
			}
			$this->session->set_flashdata(array('message' => 'AL004', 'type' => SUCCESS_MESSAGE));
			if(empty($_SERVER['QUERY_STRING']) == false) {
				$query_string = '?'.$_SERVER['QUERY_STRING'];
			} else {
				$query_string = '';
			}
			redirect('user/profile'.$query_string);
		} else {
			$page_data['title'] = $this->entity_title;
			$page_data['first_name'] = $this->entity_first_name;
			$page_data['last_name'] = $this->entity_last_name;
			$page_data['full_name'] = $this->entity_name;
			$page_data['user_country_code'] = $this->entity_country_code;
			$page_data['date_of_birth'] = date('d-m-Y', strtotime($this->entity_date_of_birth));
			$page_data['address'] = $this->entity_address;
			$page_data['phone'] = $this->entity_phone;
			$page_data['email'] = $this->entity_email;
			$page_data['profile_image'] = $this->entity_image;
			$page_data['signature'] = $this->entity_signature;
		}
		$this->load->library('booking_data_formatter');
		$booking_counts = $this->booking_data_formatter->get_booking_counts();
		$page_data['booking_counts'] = $booking_counts['data'];
		$page_data['country_code'] = $this->db_cache_api->get_country_code_list();
		$latest_transaction = $this->transaction_model->logs(array(), false, 0, 5);
		$latest_transaction = $this->booking_data_formatter->format_recent_transactions($latest_transaction, 'b2c');
		$page_data['latest_transaction'] = $latest_transaction['data']['transaction_details'];
		$traveller_details = $this->traveller_details();
		$page_data['user_passport_visa_details'] = $traveller_details['user_passport_visa_details'];
		$page_data['traveller_details'] = $traveller_details['traveller_details'];
		$page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_code ();
		$page_data ['country_list'] = $this->db_cache_api->get_iso_country_code ();
		$this->template->view('user/profile', $page_data);
	}

	/**
	 * Logout function for logout from account and unset all the session variables
	 */
	function initilize_logout(){
		redirect('auth/initilize_logout');
		if (is_logged_in_user()) {
			$this->general_model->update_login_manager($this->session->userdata(LOGIN_POINTER));
			$this->session->unset_userdata(array(AUTH_USER_POINTER => '',LOGIN_POINTER => '') );
			// added by nithin for unseting the email username
			$this->session->unset_userdata('mail_user');
		}
	}

	/**
	 * oops page of application will be loaded here
	 */
	public function ooops()
	{
		$this->template->view('utilities/404.php');
	}
	

	/**
	 * Function to Change the Password of a User
	 */
	public function change_password()
	{
		validate_user_login();
		$data=array();
		$get_data = $this->input->get();
		if(isset($get_data['uid'])) {
			$user_id = intval($this->encrypt->decode($get_data['uid']));
		} else {
			redirect("general/initilize_logout");
		}
		$page_data['form_data'] = $this->input->post();
		if(valid_array($page_data['form_data'])==TRUE) {
			$this->current_page->set_auto_validator();
			if ($this->form_validation->run()) {
				$table_name="user";
				/** Checking New Password and Old Password Are Same OR Not **/
				$condition['password'] = md5($this->input->post('new_password'));
				$condition['user_id'] = $user_id;
				$check_pwd = $this->custom_db->single_table_records($table_name,'password',$condition);
				if(!$check_pwd['status']) {
					$condition['password'] = md5($this->input->post('current_password'));
					$condition['user_id'] = $user_id;
					$data['password'] = md5($this->input->post('new_password'));
					$update_res=$this->custom_db->update_record($table_name, $data, $condition);
					if($update_res)	{
						$this->session->set_flashdata(array('message' => 'UL0010', 'type' => SUCCESS_MESSAGE));
						refresh();
					} else {
						$this->session->set_flashdata(array('message' => 'UL0011', 'type' => ERROR_MESSAGE));
						refresh();
						/*$data['msg'] = 'UL0011';
						 $data['type'] = ERROR_MESSAGE;*/
					}
				} else {
					$this->session->set_flashdata(array('message' => 'UL0012', 'type'=>WARNING_MESSAGE));
					refresh();
					//redirect('general/change_password?uid='.urlencode($get_data['uid']));
				}
			}
		}
		$this->template->view('user/change_password', $data);
	}
	/**
	 * Jaganath
	 * Add Traveller
	 */
	function add_traveller()
	{
		//FIXME:Make Codeigniter Validations -- Jaganath
		validate_user_login();
		$post_data = $this->input->post();
		if(valid_array($post_data) == true && isset($post_data['traveller_first_name']) == true && empty($post_data['traveller_first_name']) == false && isset($post_data['traveller_date_of_birth']) == true && empty($post_data['traveller_date_of_birth']) == false 
		&& isset($post_data['traveller_email']) == true && isset($post_data['traveller_last_name']) == true) {
			$user_traveller_details = array();
			$user_traveller_details['first_name'] = trim($post_data['traveller_first_name']);
			$user_traveller_details['last_name'] = trim($post_data['traveller_last_name']);
			$user_traveller_details['date_of_birth'] = date('Y-m-d', strtotime(trim($post_data['traveller_date_of_birth'])));
			$user_traveller_details['email'] = trim($post_data['traveller_email']);
			$user_traveller_details['created_by_id'] = $this->entity_user_id;
			$user_traveller_details['created_datetime'] = date('Y-m-d H:i:s');
			$this->custom_db->insert_record('user_traveller_details', $user_traveller_details);
		}
		if(empty($_SERVER['QUERY_STRING']) == false) {
			$query_string = '?'.$_SERVER['QUERY_STRING'];
		} else {
			$query_string = '';
		}
		redirect('user/profile'.$query_string);
	}
	/**
	 * Jaganath
	 */
	function update_traveller_details()
	{
		//FIXME:Make Codeigniter Validations -- Jaganath
		$post_data = $this->input->post();
		if(valid_array($post_data) == true && isset($post_data['origin']) == true && intval($post_data['origin']) > 0 &&
		isset($post_data['traveller_first_name']) == true && empty($post_data['traveller_first_name']) == false && isset($post_data['traveller_date_of_birth']) == true && empty($post_data['traveller_date_of_birth']) == false 
		&& isset($post_data['traveller_email']) == true && isset($post_data['traveller_last_name']) == true) {
			$user_traveller_details = array();
			$user_traveller_details['first_name'] = trim($post_data['traveller_first_name']);
			$user_traveller_details['last_name'] = trim($post_data['traveller_last_name']);
			$user_traveller_details['date_of_birth'] = date('Y-m-d', strtotime(trim($post_data['traveller_date_of_birth'])));
			$user_traveller_details['email'] = trim($post_data['traveller_email']);
			
			$user_traveller_details['passport_user_name'] = trim($post_data['passport_user_name']);
			$user_traveller_details['passport_nationality'] = trim($post_data['passport_nationality']);
			$user_traveller_details['passport_expiry_day'] = trim($post_data['passport_expiry_day']);
			$user_traveller_details['passport_expiry_month'] = trim($post_data['passport_expiry_month']);
			$user_traveller_details['passport_expiry_year'] = trim($post_data['passport_expiry_year']);
			$user_traveller_details['passport_number'] = trim($post_data['passport_number']);
			$user_traveller_details['passport_issuing_country'] = trim($post_data['passport_issuing_country']);
			$user_traveller_details['updated_by_id'] = $this->entity_user_id;
			$user_traveller_details['updated_datetime'] = date('Y-m-d H:i:s');
			$this->custom_db->update_record('user_traveller_details', $user_traveller_details, array('origin' => intval($post_data['origin'])));
		}
		if(empty($_SERVER['QUERY_STRING']) == false) {
			$query_string = '?'.$_SERVER['QUERY_STRING'];
		} else {
			$query_string = '';
		}
		redirect('user/profile'.$query_string);
	}
	/**
	 * Jaganath
	 */
	function traveller_details()
	{
		$data = array();
		$data['user_passport_visa_details'] = array();
		$data['traveller_details'] = array();
		//traveller details
		$traveller_details = $this->custom_db->single_table_records('user_traveller_details', '*', array('created_by_id' => $this->entity_user_id, 'user_id' => 0));
		if($traveller_details['status'] == true) {
			$data['traveller_details'] = $traveller_details['data'];
		}
		//User PassportVisa details
		$user_passport_visa_details = $this->custom_db->single_table_records('user_traveller_details', '*', array('created_by_id' => $this->entity_user_id, 'user_id' => $this->entity_user_id));
		if($user_passport_visa_details['status'] == true) {
			$data['traveller_details'] = $user_passport_visa_details['data'][0];
		}
		return $data;
	}

	public function flight_transcation_history()
	{
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		if(!empty($user_id)){
				$flight_transcation_details 	=	$this->user_model->flight_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
				if(valid_array($flight_transcation_details['data'])){

					$flight_booking_counts = count($flight_transcation_details['data']['booking_details']);
					$flight_transcation_details		= json_decode(json_encode($flight_transcation_details['data']['booking_details']), True);
					$transcation_details['status'] = SUCCESS_STATUS;
					$transcation_details['flights']	= 	$flight_transcation_details;
					echo json_encode($transcation_details);exit();
				}else{
					$transcation_details['status'] = FAILURE_STATUS;
					$transcation_details['flights']	= 	array();
					echo json_encode($transcation_details);exit();
				}
				
			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
			echo json_encode($transcation_details);exit();
		}
		
	}


	public function hotel_transcation_history()
	{
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		if(!empty($user_id)){
			
				$hotel_transcation_details 	=	$this->user_model->hotel_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
				if(valid_array($hotel_transcation_details)){
					$hotel_transcation_details		= json_decode(json_encode($hotel_transcation_details), True);
				}else{
					$transcation_details['hotels']	= 	array();
				}
				$transcation_details['hotels']	= 	$hotel_transcation_details;
			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
		}
		echo json_encode($transcation_details);exit();
	}

	public function bus_transcation_history()
	{
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		if(!empty($user_id)){
			
				$bus_transcation_details 	=	$this->user_model->bus_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
				if(valid_array($bus_transcation_details)){
					$bus_transcation_details		= json_decode(json_encode($bus_transcation_details), True);
				}else{
					$transcation_details['hotels']	= 	array();
				}
				$transcation_details['hotels']	= 	$bus_transcation_details;
			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
		}
		echo json_encode($transcation_details);exit();
	}

	public function hotel_ticket()
	{
		$this->load->model('hotel_model');
		$ticket = $this->input->post('ticket_details');

		$hotel_ticket_details = json_decode($ticket);
		$ticket_details = json_decode(json_encode($hotel_ticket_details), True);

		if(valid_array($ticket_details))
		{
			extract($ticket_details);
			if (empty($app_reference) == false) {
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $booking_status);
				if ($booking_details['status'] == SUCCESS_STATUS) {
					//Assemble Booking Data
					$data['status'] = SUCCESS_STATUS;
					$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, 'b2c');
					$data['ticket'] = $assembled_booking_details['data'];	
				}else{
					$data['status'] = FAILURE_STATUS;
					$data['message'] = "Ticket Detail Not Found !!";
				}
			}
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Invalid Request !!";
		}
		echo json_encode($data);
		exit;	
	}

	function flight_ticket()
	{
		$this->load->model('flight_model');
		$ticket = $this->input->post('ticket_details');

		$flight_ticket_details = json_decode($ticket);
		$ticket_details = json_decode(json_encode($flight_ticket_details), True);

		if(valid_array($ticket_details))
		{
			extract($ticket_details);

			if (empty($app_reference) == false) {
				$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
				
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$data['status'] = SUCCESS_STATUS;
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2c');	
				$data['ticket'] = $assembled_booking_details['data'];

			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Ticket Detail Not Found !!";
			}
		}
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Invalid Request !!";
		}
		echo json_encode($data);
		exit;	
	}

	function bus_ticket()
	{
		$this->load->model('bus_model');
		$ticket = $this->input->post('ticket_details');

		$bus_ticket_details = json_decode($ticket);
		$ticket_details = json_decode(json_encode($bus_ticket_details), True);

		if(valid_array($ticket_details))
		{
			extract($ticket_details);
			if (empty($app_reference) == false) {
				$booking_details = $this->bus_model->get_booking_details($app_reference, $booking_source, $booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$data['status'] = SUCCESS_STATUS;
				$assembled_booking_details = $this->booking_data_formatter->format_bus_booking_data($booking_details, 'b2c');	
				$data['ticket'] = $assembled_booking_details['data'];

			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Ticket Detail Not Found !!";
			}
		}
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Invalid Request !!";
		}
		echo json_encode($data);
		exit;	
	}

	/***** Login with facebook in mobile ****/

	function mob_login_facebook()
	{
		//$post_data = '{"user_logged_in":"0","email":"jeeva.provab@gmail.com","first_name":"jeeva","last_name":"k"}';
		$post_data = $this->input->post('get_fb_detail');
		if(empty($post_data)){
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Request fields are missing";
			$res['data'] = array();
			echo json_encode($res);
			exit;
		}
		$var = json_decode($post_data);		
		$data = json_decode(json_encode($var), True);
		if(isset($data) && valid_array($data)){
			if($data['user_logged_in'] == false){
				$user_exists = $this->username_check($data['email']);
				if($user_exists['status'] == FAILURE_STATUS){
					$user_record = $this->user_model->create_user($data['email'], "password", $data['first_name'],'', $creation_source='facebook', $data['last_name']," ", $country_code="212", $country_name="United Arab Emirates");
					if ($user_record != '' and valid_array($user_record) == true) {
						$status = true;
						//create login pointer
						$user_record = $user_record['data'];
						$res['user_type'] = $user_record[0]['user_type'];
						$res['auth_user_pointer'] = $user_record[0]['uuid'];
						$res['user_id'] = $user_record[0]['user_id'];
						$res['email'] =  $user_record[0]['email'];
						$res['phone'] = $user_record[0]['phone'];
						$res['first_name'] = $user_record[0]['first_name'];
						$res['last_name'] = $user_record[0]['last_name'];
						$res['message'] = 'Login Successful';	
						echo json_encode($res);
						exit;			
					}else{
						$res['status'] = FAILURE_STATUS;
						$res['message'] = "User Details Not Found !!";
						$res['data'] = array();
						echo json_encode($res);
						exit;
					}
				} else {
					$userdata = $this->custom_db->single_table_records('user','*',array('user_name'=>$data['email']));
					$userdata = $userdata['data'];
					$res['user_type'] = $userdata[0]['user_type'];
					$res['auth_user_pointer'] = $userdata[0]['uuid'];
					$res['user_id'] = $userdata[0]['user_id'];
					$res['email'] =  $userdata[0]['email'];
					$res['phone'] = $userdata[0]['phone'];
					$res['first_name'] = $userdata[0]['first_name'];
					$res['last_name'] = $userdata[0]['last_name'];
					$res['message'] = 'Login Successful';	
					echo json_encode($res);
					exit;			
				}
			}else{
				$res['status'] = FAILURE_STATUS;
				$res['message'] = "Please Logout to Register !!";
				$res['data'] = array();
				echo json_encode($res);
				exit;
			}
		}
	}

	/***** Login with Google in mobile ****/

	function mob_login_google()
	{
		//$post_data = '{"user_logged_in":"0","email":"jeeva.provab@gmail.com","first_name":"jeeva","last_name":"k"}';
		$post_data = $this->input->post('get_google_detail');
		if(empty($post_data)){
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Request fields are missing";
			$res['data'] = array();
			echo json_encode($res);
			exit;
		}
		$var = json_decode($post_data);		
		$data = json_decode(json_encode($var), True);
		if(isset($data) && valid_array($data)){
			if($data['user_logged_in'] == false){
				$user_exists = $this->username_check($data['email']);
				if($user_exists['status'] == FAILURE_STATUS){
					$user_record = $this->user_model->create_user($data['email'], "password", $data['first_name'],'', $creation_source='google', $data['last_name']," ", $country_code="212", $country_name="United Arab Emirates");
					if ($user_record != '' and valid_array($user_record) == true) {
						$status = true;
						//create login pointer
						$user_record = $user_record['data'];
						$res['user_type'] = $user_record[0]['user_type'];
						$res['auth_user_pointer'] = $user_record[0]['uuid'];
						$res['user_id'] = $user_record[0]['user_id'];
						$res['email'] =  $user_record[0]['email'];
						$res['phone'] = $user_record[0]['phone'];
						$res['first_name'] = $user_record[0]['first_name'];
						$res['last_name'] = $user_record[0]['last_name'];
						$res['message'] = 'Login Successful';	
						echo json_encode($res);
						exit;			
					}else{
						$res['status'] = FAILURE_STATUS;
						$res['message'] = "User Details Not Found !!";
						$res['data'] = array();
						echo json_encode($res);
						exit;
					}
				} else {
					$userdata = $this->custom_db->single_table_records('user','*',array('user_name'=>$data['email']));
					$userdata = $userdata['data'];
					$res['user_type'] = $userdata[0]['user_type'];
					$res['auth_user_pointer'] = $userdata[0]['uuid'];
					$res['user_id'] = $userdata[0]['user_id'];
					$res['email'] =  $userdata[0]['email'];
					$res['phone'] = $userdata[0]['phone'];
					$res['first_name'] = $userdata[0]['first_name'];
					$res['last_name'] = $userdata[0]['last_name'];
					$res['message'] = 'Login Successful';	
					echo json_encode($res);
					exit;			
				}
			}else{
				$res['status'] = FAILURE_STATUS;
				$res['message'] = "Please Logout to Register !!";
				$res['data'] = array();
				echo json_encode($res);
				exit;
			}
		}
	}

	/**
	 * Call back function to check username availability
	 * @param string $name
	 */
	public function username_check($name) {
		$condition['email'] = $name;
		$condition['user_type'] = B2C_USER;
		$condition['domain_list_fk'] = intval(get_domain_auth_id());
		$data = $this -> custom_db -> single_table_records('user', array('user_id', 'user_name'), $condition);
		
		if ($data['status'] == SUCCESS_STATUS and valid_array($data['data']) == true) {
			//$this -> form_validation -> set_message('username_check', $name . ' Is Not Available!!!');
			return $data;
		} else {
			$data['status'] = FAILURE_STATUS;
			$data['data'] = array();
		}
	}

	public function get_profile_details()
	{
		$user_id		=$this->input->post('user_id');
		if(empty($user_id)==false){
			$get_data = $this->custom_db->single_table_records('user','*',array('user_id'=>$user_id));
			if($get_data['status'] == SUCCESS_STATUS && valid_array($get_data['data'])){

				$data['user_id']      = $get_data['data'][0]['user_id'];
				$data['uuid']         = $get_data['data'][0]['uuid'];
				$data['user_type']    = $get_data['data'][0]['user_type'];
				$data['email']        = $get_data['data'][0]['email'];
				$data['user_name']    = $get_data['data'][0]['user_name'];
				$data['status']       = $get_data['data'][0]['status'];
				$data['image']        = $GLOBALS['CI']->template->domain_images($get_data['data'][0]['image']);
				$data['title']        = $get_data['data'][0]['title'];
				$data['first_name']   = $get_data['data'][0]['first_name'];
				$data['last_name']    = $get_data['data'][0]['last_name'];
				$data['address']      = $get_data['data'][0]['address'];
				$data['city']         = $get_data['data'][0]['city'];
				$data['state']        = $get_data['data'][0]['state'];
				$data['pin_code']     = $get_data['data'][0]['pin_code'];
				$data['country_name'] = $get_data['data'][0]['country_name'];
				$data['phone']        = $get_data['data'][0]['phone'];

				echo json_encode($data);
				exit;

			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to get the user details";
				$data['data'] = array();
				echo json_encode($data);
				exit;
			}
			
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please Login to see your details";
			$data['data'] = array();
			echo json_encode($data);
			exit;
		}
	}







	/**
	 * Badri Nath Nayak
	 * Add Traveller
	 */
	function add_traveller_mobile()
	{
		//FIXME:Make Codeigniter Validations 
		// {"traveller_first_name":"fghfgh","traveller_last_name":"fghfgh","traveller_date_of_birth":"15-03-2018","traveller_email":"fghfg@drtgf.jhghgf","created_by_id":"123"}
	//	validate_user_login();
		$post_data = $this->input->post('traveller_info');
		$post_data = json_decode($post_data,true);
	
		if(valid_array($post_data) == true && isset($post_data['traveller_first_name']) == true && empty($post_data['traveller_first_name']) == false && isset($post_data['traveller_date_of_birth']) == true && empty($post_data['traveller_date_of_birth']) == false 
		&& isset($post_data['traveller_email']) == true && isset($post_data['traveller_last_name']) == true) {
			$user_traveller_details = array();
			$user_traveller_details['first_name'] = trim($post_data['traveller_first_name']);
			$user_traveller_details['last_name'] = trim($post_data['traveller_last_name']);
			$user_traveller_details['date_of_birth'] = date('Y-m-d', strtotime(trim($post_data['traveller_date_of_birth'])));
			$user_traveller_details['email'] = trim($post_data['traveller_email']);
			$user_traveller_details['created_by_id'] = trim($post_data['created_by_id']);
			$user_traveller_details['created_datetime'] = date('Y-m-d H:i:s');
				//debug($user_traveller_details); exit;
			$this->custom_db->insert_record('user_traveller_details', $user_traveller_details);

			$data['status'] = SUCCESS_STATUS;
			$data['message'] = "Traveller details saved successful.";
			$data['data'] = $this->traveller_details_mobile($post_data['created_by_id']);
			echo json_encode($data);
			exit;


		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Unable to save traveller details.";
			$data['data'] = array();
			echo json_encode($data);
			exit;
		}
	}
	/**
	 * Badri
	 */
	function update_traveller_details_mobile()
	{
		//FIXME:Make Codeigniter Validations 
		$post_data = $this->input->post();
		if(valid_array($post_data) == true && isset($post_data['origin']) == true && intval($post_data['origin']) > 0 &&
		isset($post_data['traveller_first_name']) == true && empty($post_data['traveller_first_name']) == false && isset($post_data['traveller_date_of_birth']) == true && empty($post_data['traveller_date_of_birth']) == false 
		&& isset($post_data['traveller_email']) == true && isset($post_data['traveller_last_name']) == true) {
			$user_traveller_details = array();
			$user_traveller_details['first_name'] = trim($post_data['traveller_first_name']);
			$user_traveller_details['last_name'] = trim($post_data['traveller_last_name']);
			$user_traveller_details['date_of_birth'] = date('Y-m-d', strtotime(trim($post_data['traveller_date_of_birth'])));
			$user_traveller_details['email'] = trim($post_data['traveller_email']);
			
			$user_traveller_details['passport_user_name'] = trim($post_data['passport_user_name']);
			$user_traveller_details['passport_nationality'] = trim($post_data['passport_nationality']);
			$user_traveller_details['passport_expiry_day'] = trim($post_data['passport_expiry_day']);
			$user_traveller_details['passport_expiry_month'] = trim($post_data['passport_expiry_month']);
			$user_traveller_details['passport_expiry_year'] = trim($post_data['passport_expiry_year']);
			$user_traveller_details['passport_number'] = trim($post_data['passport_number']);
			$user_traveller_details['passport_issuing_country'] = trim($post_data['passport_issuing_country']);
			$user_traveller_details['updated_by_id'] = $this->entity_user_id;
			$user_traveller_details['updated_datetime'] = date('Y-m-d H:i:s');
			$this->custom_db->update_record('user_traveller_details', $user_traveller_details, array('origin' => intval($post_data['origin'])));
		}
		if(empty($_SERVER['QUERY_STRING']) == false) {
			$query_string = '?'.$_SERVER['QUERY_STRING'];
		} else {
			$query_string = '';
		}
		redirect('user/profile'.$query_string);
	}
	/**
	 * Badri
	 */
	function traveller_details_mobile($entity_user_id)
	{
		$data = array();
		$data['user_passport_visa_details'] = array();
		$data['traveller_details'] = array();
		if(isset($entity_user_id)){
			//traveller details
			$traveller_details = $this->custom_db->single_table_records('user_traveller_details', '*', array('created_by_id' => $entity_user_id, 'user_id' => 0));
			if($traveller_details['status'] == true) {
				$data['traveller_details'] = $traveller_details['data'];
			}
			//User PassportVisa details
			$user_passport_visa_details = $this->custom_db->single_table_records('user_traveller_details', '*', array('created_by_id' => $entity_user_id, 'user_id' => $entity_user_id));
			if($user_passport_visa_details['status'] == true) {
				$data['traveller_details'] = $user_passport_visa_details['data'][0];
			}

			$res['status'] = SUCCESS_STATUS;
			$res['message'] = "Traveller details.";
			$res['data'] = $data;
			echo json_encode($res);
			exit;
		}else{
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Unable to find traveller details.";
			$res['data'] = array();
			echo json_encode($res);
			exit;
		}
		
		//return $data;
	}
}
