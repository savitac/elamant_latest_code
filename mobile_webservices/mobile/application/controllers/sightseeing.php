<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @package    Provab
 * @subpackage Sightseeing
 * @author     Elavarasi<elavarasi.k@provabmial.com>
 * @version    V1
 */
//error_reporting(E_ALL);
class Sightseeing extends CI_Controller {
	private $current_module;
	public function __construct()
	{
		parent::__construct();
		//we need to activate hotel api which are active for current domain and load those libraries
		$this->load->model('sightseeing_model');
		$this->load->model('db_cache_api');
		$this->load->library('social_network/facebook');//Facebook Library to enable login button		
		//$this->output->enable_profiler(TRUE);
		$this->current_module = $this->config->item('current_module');
	}

	/**
	 * index page of application will be loaded here
	 */
	function index()
	{
		//	echo number_format(0, 2, '.', '');
	}

	/**
	 *  Arjun J Gowda
	 * Load Hotel Search Result
	 * @param number $search_id unique number which identifies search criteria given by user at the time of searching
	 */
	function search($search_id)
	{	
		$safe_search_data = $this->sightseeing_model->get_safe_search_data($search_id,META_SIGHTSEEING_COURSE);
		
		// Get all the hotels bookings source which are active
		$active_booking_source = $this->sightseeing_model->active_booking_source();
		
		if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
			$safe_search_data['data']['search_id'] = abs($search_id);
			$this->template->view('sightseeing/search_result_page', array('sight_seen_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
		} else {
			$this->template->view ( 'general/popup_redirect');
		}
	}

	/**
	 *  Elavarasi
	 * Get Product Details
	 */
	// function sightseeing_details()
	// {
	// 	$params = $this->input->get();
	// 	$safe_search_data = $this->sightseeing_model->get_safe_search_data($params['search_id'],META_SIGHTSEEING_COURSE);
	// 	$safe_search_data['data']['search_id'] = abs($params['search_id']);
	// 	$currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
	// 	$search_id = $params['search_id'];
	// 	if (isset($params['booking_source']) == true) {
	// 		//We will load different page for different API providers... As we have dependency on API for hotel details page
	// 		load_sightseen_lib($params['booking_source']);
	// 		if ($params['booking_source'] == PROVAB_SIGHTSEEN_BOOKING_SOURCE && isset($params['result_token']) == true and isset($params['op']) == true and
	// 		$params['op'] == 'get_details' and $safe_search_data['status'] == true) {

	// 			$params['result_token']	= urldecode($params['result_token']);


	// 			$raw_product_deails = $this->sightseeing_lib->get_product_details($params);


	// 			if ($raw_product_deails['status']) {

	// 				if($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price']){

	// 					//details data in preffered currency


	// 					$Price = $this->sightseeing_lib->details_data_in_preffered_currency($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price'],$currency_obj,'b2c');


	// 					//calculation Markup 
	// 					$raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price'] = $this->sightseeing_lib->update_booking_markup_currency($Price,$currency_obj,$search_id);	

	// 				}
	// 				$response['status']=1;
	// 				$response['data']=$raw_product_deails['data']['ProductDetails']['SSInfoResult'];
	// 				echo json_encode($response);exit;
	// 				// $this->template->view('sightseeing/viator/sightseeing_details', array('currency_obj' => $currency_obj, 'product_details' => $raw_product_deails['data']['ProductDetails']['SSInfoResult'], 'search_params' => $safe_search_data['data'], 'search_id'=>$search_id,'active_booking_source' => $params['booking_source'], 'params' => $params));
	// 			} else {

	// 				$msg= $raw_product_deails['Message'];
	// 				echo $msg;exit;
	// 				// redirect(base_url().'index.php/sightseeing/exception?op='.$msg.'&notification=session');
	// 			}
	// 		} else {
	// 			echo "status:0";exit;
	// 			// redirect(base_url());
	// 		}
	// 	} else {
	// 			echo "status:0";exit;

	// 		// redirect(base_url());
	// 	}
	// }

	function sightseeing_details()
	{

		$params = $this->input->get();
		#debug($params);exit;
		$safe_search_data = $this->sightseeing_model->get_safe_search_data($params['search_id'],META_SIGHTSEEING_COURSE);
		$safe_search_data['data']['search_id'] = abs($params['search_id']);
		$currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
		$search_id = $params['search_id'];
		if (isset($params['booking_source']) == true) {
			//We will load different page for different API providers... As we have dependency on API for hotel details page
			load_sightseen_lib($params['booking_source']);
			if ($params['booking_source'] == PROVAB_SIGHTSEEN_BOOKING_SOURCE && isset($params['result_token']) == true and isset($params['op']) == true and
				$params['op'] == 'get_details' and $safe_search_data['status'] == true) {

				$params['result_token']	= urldecode($params['result_token']);


			$raw_product_deails = $this->sightseeing_lib->get_product_details($params);

				//debug($raw_product_deails);
				//exit;
			if ($raw_product_deails['status']) {


				$calendar_availability_date = $this->enable_calendar_availability($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Product_available_date']);


				$raw_product_deails['data']['ProductDetails']['SSInfoResult']['Calendar_available_date'] = $calendar_availability_date;

				if($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price']){

						//details data in preffered currency


					$Price = $this->sightseeing_lib->details_data_in_preffered_currency($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price'],$currency_obj,'b2c');

					$currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

						//calculation Markup 
					$raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price'] = $this->sightseeing_lib->update_booking_markup_currency($Price,$currency_obj,$search_id);	

					$token['AdditionalInfo']=base64_encode(json_encode($raw_product_deails['data']['ProductDetails']['SSInfoResult']['AdditionalInfo']));
					$token['Inclusions']=base64_encode(json_encode($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Inclusions']));
					$token['Exclusions']=base64_encode(json_encode($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Exclusions']));
					$token['ShortDescription']=base64_encode($raw_product_deails['data']['ProductDetails']['SSInfoResult']['ShortDescription']);					
					$token['voucher_req']=base64_encode($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Voucher_req']);
					$token['API_Price'] = base64_encode(json_encode($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price']));
					$raw_product_deails['data']['ProductDetails']['SSInfoResult']['tokens_data'] = $token;


					
						// debug($raw_product_deails['data']['ProductDetails']['SSInfoResult']['Price']);
						// exit;

				}
				$response['status']=1;
				$response['data']=$raw_product_deails['data']['ProductDetails']['SSInfoResult'];
				echo json_encode($response);exit;
					// $this->template->view('sightseeing/viator/sightseeing_details', array('currency_obj' => $currency_obj, 'product_details' => $raw_product_deails['data']['ProductDetails']['SSInfoResult'], 'search_params' => $safe_search_data['data'], 'search_id'=>$search_id,'active_booking_source' => $params['booking_source'], 'params' => $params));
			} else {

				$msg= $raw_product_deails['Message'];
				echo $msg;exit;
					// redirect(base_url().'index.php/sightseeing/exception?op='.$msg.'&notification=session');
			}
		} else {
			echo "status:0";exit;
				// redirect(base_url());
		}
	} else {
		echo "status:0";exit;

			// redirect(base_url());
	}
}

public function enable_calendar_availability($calendar_availability_date){

	if(valid_array($calendar_availability_date)){
		$available_str=array();
		foreach ($calendar_availability_date as $m_key => $m_value) {
			$avail_month_str = $m_key;

			foreach ($m_value as $d_key => $d_value) {
			 		//j- to remove 0 from date, n to remove 0 from month
				$date_str = $avail_month_str.'-'.$d_value;
				$available_str[] = date('j-n-Y',strtotime($date_str));

			}
		}

		return $available_str;
	}else{
		return '';
	}
}

	/**
	*Elavarasi
	*Getting Available Tourgrade Based on Passenger Mix
	*/
	public function select_tourgrade(){
		$post_params = $this->input->post();
		// debug($post_params);
		// exit;
		//debug($get_params)
		$safe_search_data = $this->sightseeing_model->get_safe_search_data($post_params['search_id'],META_SIGHTSEEING_COURSE);
		#debug($safe_search_data);exit;
		if(trim($post_params['product_code'])!=''){

			load_sightseen_lib($post_params['booking_source']);
			#debug($post_params);exit;
			if(trim($post_params['op']=='check_tourgrade')&&$post_params['product_code']!=''){

				$search_product_tourgrade = $this->sightseeing_lib->get_tourgrade_list($post_params);


				$currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_application_default_currency(), 'to' => get_application_currency_preference()));


					// debug($search_product_tourgrade);exit;
				//$currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));


				if($search_product_tourgrade['status']){

					$module_currency_obj =  new Currency(array('module_type' => 'sightseeing','from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
					$raw_product_list = $this->sightseeing_lib->tourgrade_in_preferred_currency($search_product_tourgrade['data'], $currency_obj,$module_currency_obj,'b2c');

					$search_product_tourgrade['data'] = $raw_product_list;

				}else{
						//$this->exception(false)
					$search_product_tourgrade['Message'] = $search_product_tourgrade['Message'];

					if(!valid_array($search_product_tourgrade['data'])){


						echo json_encode($search_product_tourgrade);exit;
							// debug( $search_product_tourgrade;exit;
							// redirect(base_url().'index.php/sightseeing/exception?op=exception&notification='.$search_product_tourgrade['Message']);
					}						
				}

				$response['status']=1;
				$ageBand_token = base64_encode(json_encode($search_product_tourgrade['data']['Trip_list'][0]['AgeBands']));

				$search_product_tourgrade['data']['ageBand_token'] =$ageBand_token;						 

				$response['data']=$search_product_tourgrade['data'];
				echo json_encode($response);exit;
					// $this->template->view('sightseeing/viator/select_tourgrade',
					// 		array('tourgrade_list'=>$search_product_tourgrade['data'],'search_params'=>$post_params,
					// 			'currency_obj'=>$currency_obj,
					// 			'booking_source'=>$post_params['booking_source']));

			}else{
				echo "status:1";exit;
				redirect(base_url());
			}
		}else{
			echo "status:2";exit;
			redirect(base_url());
		}
		
	}
	/**
	*Ealvarasi check the available date for the product
	**/
	public function select_date(){
		$post_params = $this->input->post();

		$selected_date = trim($post_params['selected_date']);
		//echo $selected_date;exit;
		//debug($get_params);
		if(!empty($selected_date)){
			
			$product_get_booking_date = json_decode(base64_decode($post_params['available_date']),true);
			if(valid_array($product_get_booking_date)){

				$selected_date_details = $product_get_booking_date;
				//debug($selected_date_details);exit;
				$options = '';

				//debug($selected_date_details[$selected_date]);exit;
				foreach ($selected_date_details[$selected_date] as $key => $value) {
					$selected = '';
					if(isset($post_params['s_date'])){

						if($value==$post_params['s_date']){
							$selected =' selected';
						}
					}
					
					$options .='<option value='.$value.' '.$selected.' >'.($value).'</option>';
				}
				
				echo $options;
				exit;
				
			}
		}
	}
	/**
	 *  Elavarasi
	 * Passenger Details page for final bookings
	 * Here we need to run booking based on api
	 */
	function booking()
	{
		
		$pre_booking_params = $this->input->post();
		// $pre_booking_params['age_band']=json_encode($pre_booking_params['age_band']);
		// debug($pre_booking_params);exit;
		$age_band_details_arr = array('Adult','Youth','Senior','Child','Infant');
		
		$agen_band = array();
		$i=0;
		foreach ($age_band_details_arr as $key => $value) {			
			if(isset($pre_booking_params[$value.'_Band_ID'])&&isset($pre_booking_params['no_of_'.$value])){
				$agen_band[$i]['bandId'] = 	$pre_booking_params[$value.'_Band_ID'];
				$agen_band[$i]['count'] = $pre_booking_params['no_of_'.$value];
				$i++;
			}
		}
		$pre_booking_params['age_band']=json_encode($agen_band);
		// debug($pre_booking_params);exit;
		$search_id = $pre_booking_params['search_id'];
		$safe_search_data = $this->sightseeing_model->get_safe_search_data($pre_booking_params['search_id'],META_SIGHTSEEING_COURSE);

		$safe_search_data['data']['search_id'] = abs($search_id);
		$page_data['active_payment_options'] = $this->module_model->get_active_payment_module_list();

		if (isset($pre_booking_params['booking_source']) == true) {
			
			//We will load different page for different API providers... As we have dependency on API for tourgrade details page
			$page_data['search_data'] = $safe_search_data['data'];
			load_sightseen_lib($pre_booking_params['booking_source']);
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");

			if ($pre_booking_params['booking_source'] == PROVAB_SIGHTSEEN_BOOKING_SOURCE && isset($pre_booking_params['tour_uniq_id']) == true and
				isset($pre_booking_params['op']) == true and $pre_booking_params['op'] == 'block_trip' and $safe_search_data['status'] == true)
			{
				
				if ($pre_booking_params['tour_uniq_id'] != false) {

					$trip_block_details = $this->sightseeing_lib->block_trip($pre_booking_params);
					// debug($trip_block_details);
					// exit;
					if ($trip_block_details['status'] == false) {
						echo json_encode($trip_block_details['data']);exit;
						redirect(base_url().'index.php/sightseeing/exception?op='.$trip_block_details['data']['msg']);
					}
					//Converting API currency data to preferred currency
					// $currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));

					//Converting API currency data to preferred currency
					$currency_obj = new Currency(array('module_type' => 'sightseeing','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
					
					$trip_block_details = $this->sightseeing_lib->tripblock_data_in_preferred_currency($trip_block_details, $currency_obj,'b2c');
					
					// debug($trip_block_details);
					// exit;
					//Display
					$currency_obj = new Currency(array('module_type' => 'sightseeing', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));

					//$pre_booking_params = $this->sightseeing_lib->update_block_details($trip_block_details['data']['BlockTrip'], $pre_booking_params,$cancel_currency_obj);

					$pre_booking_params = $this->sightseeing_lib->update_block_details($trip_block_details['data']['BlockTrip'], $pre_booking_params,$currency_obj,'b2c');
					#debug($pre_booking_params);
					$pre_booking_params['markup_price_summary'] = $this->sightseeing_lib->update_booking_markup_currency($pre_booking_params['price_summary'], $currency_obj, $safe_search_data['data']['search_id'], false, true, 'b2c');
					$pre_booking_params['Price']['_Markup'] = $pre_booking_params['markup_price_summary']['_Markup'];
					$pre_booking_params['Price']['TotalDisplayFare'] = $pre_booking_params['markup_price_summary']['TotalDisplayFare'];
					$pre_booking_params['tax_service_sum']	=  $this->sightseeing_lib->tax_service_sum($pre_booking_params['markup_price_summary'], $pre_booking_params['price_summary']);
					/*
					 * Update Markup
					 */					
					//$pre_booking_params = $this->sightseeing_lib->update_booking_markup_currency($pre_booking_params['price_summary'], $currency_obj, $safe_search_data['data']['search_id']);
					

					if ($trip_block_details['status'] == SUCCESS_STATUS) {
						if(!empty($this->entity_country_code)){
							$page_data['user_country_code'] = $this->entity_country_code;
						}
						else{
							$page_data['user_country_code'] = '';	
						}
						$page_data['booking_source'] = $pre_booking_params['booking_source'];
						$page_data['pre_booking_params'] = $pre_booking_params;
						$page_data['pre_booking_params']['default_currency'] = get_application_currency_preference();
						
						$page_data['iso_country_list']	= $this->db_cache_api->get_iso_country_list();
						$page_data['country_list']		= $this->db_cache_api->get_country_list();
						$page_data['currency_obj']		= $currency_obj;
						$page_data['total_price']		= $this->sightseeing_lib->total_price($pre_booking_params['markup_price_summary']);
						
						//calculate convience fees by pax wise
						$ageband_details = $trip_block_details['data']['BlockTrip']['AgeBands'];

						$page_data['convenience_fees']  = $currency_obj->convenience_fees($page_data['total_price'], $ageband_details);
						
						$page_data['tax_service_sum']	=  $this->sightseeing_lib->tax_service_sum($pre_booking_params['markup_price_summary'], $pre_booking_params['price_summary']);

						//Traveller Details
						$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
						//Get the country phone code 
						$Domain_record = $this->custom_db->single_table_records('domain_list', '*');
						$page_data['active_data'] =$Domain_record['data'][0];
						$temp_record = $this->custom_db->single_table_records('api_country_list', '*');
						$page_data['phone_code'] =$temp_record['data'];
						
						$page_data['search_id'] = $search_id;
						$page_data['token'] = serialized_data($pre_booking_params);
						$page_data['token_key'] = md5(serialized_data($pre_booking_params));
						echo json_encode($page_data);exit;
						// $this->template->view('sightseeing/viator/viator_booking_page', $page_data);
					}
				} else {
					echo "1";exit;
					redirect(base_url().'index.php/sightseeing/exception?op=Data Modification&notification=Data modified while transfer(Invalid Data received while validating tokens)');
				}
			} else {
				echo "2";exit;

				redirect(base_url());
			}
		} else {
			echo "3";exit;

			redirect(base_url());
		}
	}

	/**
	 *  Elavarasi
	 * sending for booking	 
	 */
	function pre_booking()
	{
		$post_params = $this->input->post();
		$search_id=$this->input->post('search_id');
		
		
		// debug($search_id);debug($post_params);exit;
		$post_params['billing_city'] = 'Bangalore';
		$post_params['billing_zipcode'] = '560100';
		$post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';
		
		$valid_temp_token = unserialized_data($post_params['token'], $post_params['token_key']);

		//Make sure token and temp token matches
		//debug($post_params);exit;
		if ($valid_temp_token != false) {

			load_sightseen_lib($post_params['booking_source']);
			/****Convert Display currency to Application default currency***/
			//After converting to default currency, storing in temp_booking
			$post_params['token'] = unserialized_data($post_params['token']);
			$currency_obj = new Currency ( array (
				'module_type' => 'sightseeing',
				'from' => get_application_currency_preference (),
				'to' => admin_base_currency () 
				));

			$post_params['token'] = $this->sightseeing_lib->convert_token_to_application_currency($post_params['token'], $currency_obj, $this->current_module);
			// debug($post_params);
			// exit;
			$post_params['token'] = serialized_data($post_params['token']);
		
			$temp_token = unserialized_data($post_params['token']);
		
			
			/*debug($post_params);exit;
			if(isset($post_params['tageduser']) && isset($post_params['customer_id']))
			{

				$tagged_users=json_decode($post_params['tageduser'],true);
				 //debug($tagged_users);exit;
				$tagged_data=$this->db_cache_api->insert_tagged_user($book_id,$tagged_users,$post_params['customer_id']);
				// debug($tagged_data);exit;
				$user_id=$post_params['customer_id'];
			}*/

			if ($post_params['booking_source'] == PROVAB_SIGHTSEEN_BOOKING_SOURCE) {
				//echo"fggdf";exit;
				$amount	  = $this->sightseeing_lib->total_price($temp_token['markup_price_summary']);
				$currency = $temp_token['default_currency'];
			}
			$currency_obj = new Currency ( array (
				'module_type' => 'sightseeing',
				'from' => admin_base_currency(),
				'to' => admin_base_currency() 
				) );
			//debug($currency_obj);exit;
			/********* Convinence Fees Start ********/
			$convenience_fees = $currency_obj->convenience_fees($amount, $search_id);
			/********* Convinence Fees End ********/

			/********* Promocode Start ********/
			$promocode_discount = 0;//$post_params['promo_code_discount_val'];
			/********* Promocode End ********/

			//details for PGI
			
			$email = $post_params ['Email'];
			$phone = $post_params ['ContactNo'];
			
			if(isset($post_params['BookingQuestions'])){
				$booking_question = json_decode($post_params['BookingQuestions'],true);
				
				if(isset($booking_question['pax_question'])){
					$post_params['pax_question'] = $booking_question['pax_question'];
						
				}
				if(isset($booking_question['Questions'])){
					$question_Id = array();
					$question = array();
					if($booking_question['Questions']){
						foreach ($booking_question['Questions'] as $key => $value) {
							$question_Id[$key][0] = $value['Question_id'];
							$question[$key][0] = $value['Answer'][0];
						}
					}
					$post_params['question_Id'] = $question_Id;
					$post_params['question'] = $question;
				}
				
			}			

//			$verification_amount = roundoff_number($amount+$convenience_fees-$promocode_discount);
			//debug($post_params);exit;	
			$post_params['Passengers']=json_decode($post_params['Passengers'],true);

			foreach ($post_params['Passengers'] as $p_key => $p_value) {
				$post_params ['first_name'] [$p_key]=$post_params['Passengers'][$p_key]['firstName'];
				$post_params ['last_name'] [$p_key]=$post_params['Passengers'][$p_key]['lastName'];
				$post_params['passenger_type'][$p_key] = $post_params['Passengers'][$p_key]['Pax_Type'];
				$post_params['name_title'][$p_key] = $post_params['Passengers'][$p_key]['Title'];
	 			}

	 		$post_params['passenger_contact'] =$post_params['ContactNo'];
	 		$post_params['billing_email'] =$post_params['Email'];
	 		$post_params['hotelPickup_name']  = $post_params['Hotelpickupname'];
	 		$post_params['hotelPickupId']  = $post_params['HotelPickupId'];

	 		//$post_params['hotel_pickup_list_name']  = 
	 		//$post_params['hotelPickup_name']  = $post_params['Hotelpickupname'];


			//Insert To temp_booking and proceed
			$temp_booking = $this->module_model->serialize_temp_booking_record($post_params, SIGHTSEEING_BOOKING);
		
			$book_id = $temp_booking['book_id'];
			$book_origin = $temp_booking['temp_booking_origin'];

			$verification_amount = roundoff_number($amount);

			// echo $verification_amount;
			// exit;
			$firstname = $post_params ['first_name'] ['0'];
			$productinfo = META_SIGHTSEEING_COURSE;
			//check current balance before proceeding further
			$domain_balance_status = $this->domain_management_model->verify_current_balance($verification_amount, $currency);
			
			if ($domain_balance_status == true) {
				switch($post_params['payment_method']) {
					case PAY_NOW :
					$this->load->model('transaction');
					$pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
					$this->transaction->create_payment_record($book_id, $verification_amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount, $pg_currency_conversion_rate);
					$response['status']=1;
					$response['data']['book_id']=$book_id;
					$response['data']['book_origin']=$book_origin;
					$replace_url = str_replace("mobile/","", base_url());


					$url=explode('/mobile_webservices', base_url());
		
					$response['data']['payment_url']=$url[0].'/index.php/payment_gateway/payment/'.$book_id.'/'.$book_id.'/'.$book_origin;
					echo json_encode($response);exit;
						// redirect(base_url().'index.php/sightseeing/process_booking/'.$book_id.'/'.$book_origin);		
						//redirect(base_url().'index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin);						
					break;
					case PAY_AT_BANK : echo 'Under Construction - Remote IO Error';exit;
					break;
				}
			} else {
				$response['status']=1;
				echo json_encode($response);exit;
				redirect(base_url().'index.php/hotel/exception?op=Amount Hotel Booking&notification=insufficient_balance');
			}
		} else {
			$response['status']=2;
			echo json_encode($response);exit;
			redirect(base_url().'index.php/hotel/exception?op=Remote IO error @ Hotel Booking&notification=validation');
		}
	}


	/*
		process booking in backend until show loader 
	*/
		function process_booking($book_id, $temp_book_origin){

			if($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0){

				$page_data ['form_url'] = base_url () . 'index.php/sightseeing/secure_booking';
				$page_data ['form_method'] = 'POST';
				$page_data ['form_params'] ['book_id'] = $book_id;
				$page_data ['form_params'] ['temp_book_origin'] = $temp_book_origin;

				$this->template->view('share/loader/booking_process_loader', $page_data);	

			}else{
				redirect(base_url().'index.php/sightseeing/exception?op=Invalid request&notification=validation');
			}

		}

	/**
	 * Elavarasi
	 *Do booking once payment is successfull - Payment Gateway
	 *and issue voucher
	 */
	function secure_booking()
	{
		$post_data = $this->input->post();
		
		// debug($post_data);exit;
		if(valid_array($post_data) == true && isset($post_data['book_id']) == true && isset($post_data['temp_book_origin']) == true &&
			empty($post_data['book_id']) == false && intval($post_data['temp_book_origin']) > 0){
			//verify payment status and continue
			$book_id = trim($post_data['book_id']);
		$temp_book_origin = intval($post_data['temp_book_origin']);
		$this->load->model('transaction');
		$booking_status = $this->transaction->get_payment_status($book_id);

			// if($booking_status['status'] !== 'accepted'){
			// 	redirect(base_url().'index.php/hotel/exception?op=Payment Not Done&notification=validation');
			// }
	} else{
		// echo "invalid transaction";exit;
	//	redirect(base_url().'index.php/sightseeing/exception?op=InvalidBooking&notification=invalid');
		$res["status"] = 0;
		$res["url"] = base_url().'index.php/sightseeing/exception?op=InvalidBooking&notification=invalid';
		die(json_encode($res));
	}		
		//run booking request and do booking
		// debug($book_id);debug($temp_book_origin);
	$temp_booking = $this->module_model->unserialize_temp_booking_record($book_id, $temp_book_origin);
		// debug($temp_booking);exit('428');
		//Delete the temp_booking record, after accessing
	$this->module_model->delete_temp_booking_record ($book_id, $temp_book_origin);
	load_sightseen_lib($temp_booking['booking_source']);
		//verify payment status and continue


	$total_booking_price = $this->sightseeing_lib->total_price($temp_booking['book_attributes']['token']['markup_price_summary']);


	$currency = $temp_booking['book_attributes']['token']['default_currency'];
		//also verify provab balance
		//check current balance before proceeding further
	$domain_balance_status = $this->domain_management_model->verify_current_balance($total_booking_price, $currency);
		// debug($temp_booking);exit;
	if ($domain_balance_status) {
			//lock table
		if ($temp_booking != false) {
			switch ($temp_booking['booking_source']) {
				case PROVAB_SIGHTSEEN_BOOKING_SOURCE :

						//FIXME : COntinue from here - Booking request
				$booking = $this->sightseeing_lib->process_booking($book_id, $temp_booking['book_attributes']);
						//Save booking based on booking status and book id
				break;
			}

			if ($booking['status'] == SUCCESS_STATUS) {

				$currency_obj = new Currency(array('module_type' => 'sightseeing', 'from' => admin_base_currency(), 'to' => admin_base_currency()));

				$booking['data']['currency_obj'] = $currency_obj;
					//Save booking based on booking status and book id
				$data = $this->sightseeing_lib->save_booking($book_id, $booking['data']);

				$this->domain_management_model->update_transaction_details('sightseeing', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount'],$data['transaction_currency'], $data['currency_conversion_rate'] );
				$response['status']=1;
				$response['booking_source']=$temp_booking['booking_source'];
				$response['book_id']=$book_id;
				$response['data']=$data;
				echo json_encode($response);
				//exit;
				
				$res["status"] = 1;
				$res["url"] =  base_url () . 'index.php/voucher/sightseeing/' . $book_id . '/' . $temp_booking ['booking_source'] . '/'  .$data['booking_status'].'/show_voucher';
				die(json_encode($res));
					// redirect(base_url().'index.php/voucher/sightseeing/'.$book_id.'/'.$temp_booking['booking_source'].'/'.$data['booking_status'].'/show_voucher');
			} else {
				echo json_encode($booking);
				//exit;
				$res["status"] = 0;
				$res["url"] =  base_url () . 'index.php/sightseeing/exception?op=booking_exception&notification=' . $booking ['message'];
				die(json_encode($res));
					// redirect(base_url().'index.php/sightseeing/exception?op=booking_exception&notification='.$booking['msg']);
			}
		}
			//release table lock
	} else {
		echo "Remote IO error";exit;

			// redirect(base_url().'index.php/sightseeing/exception?op=Remote IO error @ Insufficient&notification=validation');
	}
		//redirect(base_url().'index.php/hotel/exception?op=Remote IO error @ Hotel Secure Booking&notification=validation');
}

function test(){
	$currency_obj = new Currency(array('module_type' => 'sightseeing', 'from' => admin_base_currency(), 'to' => admin_base_currency()));
	debug($currency_obj);
}


	/**
	 * Elavarasi
	 */
	function pre_cancellation($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$booking_details = $this->sightseeing_model->get_booking_details($app_reference, $booking_source);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_sightseeing_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				$this->template->view('sightseeing/pre_cancellation', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	/*
	 * Elavarasi
	 * Process the Booking Cancellation
	 * Full Booking Cancellation
	 *
	 */
	function cancel_booking($app_reference, $booking_source)
	{
		if(empty($app_reference) == false) {
			$get_params = $this->input->get();

			$master_booking_details = $this->sightseeing_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_sightseeing_booking_data($master_booking_details, 'b2c');
				$master_booking_details = $master_booking_details['data']['booking_details'][0];
				load_sightseen_lib($booking_source);
				$cancellation_details = $this->sightseeing_lib->cancel_booking($master_booking_details,$get_params);//Invoke Cancellation Methods
				if($cancellation_details['status'] == false) {
					$query_string = '?error_msg='.$cancellation_details['msg'];
				} else {
					$query_string = '';
				}
				redirect('sightseeing/cancellation_details/'.$app_reference.'/'.$booking_source.$query_string);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	/**
	 * Elavarasi
	 * Cancellation Details
	 * @param $app_reference
	 * @param $booking_source
	 */
	function cancellation_details($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $GLOBALS['CI']->sightseeing_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$page_data = array();
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_sightseeing_booking_data($master_booking_details, 'b2c');
				$page_data['data'] = $master_booking_details['data'];
				$this->template->view('sightseeing/cancellation_details', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}

	}
	


	/**
	 * Elavarasi
	 */
	function exception($redirect=true)
	{
		$module = META_SIGHTSEEING_COURSE;
		$op = (empty($_GET['op']) == true ? '' : $_GET['op']);
		$notification = (empty($_GET['notification']) == true ? '' : $_GET['notification']);
		$eid = $this->module_model->log_exception($module, $op, $notification);

		//set ip log session before redirection
		$this->session->set_flashdata(array('log_ip_info' => true));
		$is_session = false;
		
		if($notification=='session'){
			$is_session =true;
		}
		if($redirect){
			redirect(base_url().'index.php/sightseeing/event_logger/'.$eid.'/'.$is_session);
		}
		
	}

	function event_logger($eid='',$is_session='')
	{
		
		$log_ip_info = $this->session->flashdata('log_ip_info');
		$this->template->view('sightseeing/exception', array('log_ip_info' => $log_ip_info, 'eid' => $eid,'is_session'=>$is_session));
	}

	
}
