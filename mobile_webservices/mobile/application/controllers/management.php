<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package    Provab - Provab Application
 * @subpackage Travel Portal
 * @author     Arjun J<arjunjgowda260389@gmail.com> on 01-06-2015
 * @version    V2
 */

class Management extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('management_model');
	}
}