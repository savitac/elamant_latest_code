<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
		// error_reporting(E_ALL);

/**
 *
 * @package    Provab
 * @subpackage General
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */

class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('module_model');
		$this->load->model('transaction_model');
		$this->load->library('booking_data_formatter');
		//$this->output->enable_profiler(TRUE);
	}

	function create_default_domain($domain_key_name='192.168.0.26')
	{
		include_once DOMAIN_CONFIG.'default_domain_configuration.php';
	}

	/**
	 * index page of application will be loaded here
	 */
	function index()
	{
		if (is_logged_in_user()) {
			redirect('menu/index');
		}
	}

	/**
	 * User Profile Management
	 */
	function edit_profile()
	{
		// error_reporting(0);
		$op_data = $this->input->post();
		// debug($_FILES);
		// debug($op_data);exit;
		$currency_obj = new Currency();
		$page_data['currency_obj'] = $currency_obj;
		if (valid_array($op_data) == true && empty($op_data['title']) == false && empty($op_data['first_name']) == false && empty($op_data['last_name']) == false &&
			empty($op_data['country_code']) == false && empty($op_data['phone']) == false && empty($op_data['address']) == false) {
			$post_data['title']=$op_data['title'];
			$post_data['first_name']=$op_data['first_name'];
			$post_data['last_name']=$op_data['last_name'];
			$post_data['country_code']=$op_data['country_code'];
			$post_data['phone']=$op_data['phone'];
			$post_data['address']=$op_data['address'];
			$post_data['privacy']=$op_data['privacy'];
			if($op_data['map_view']==1)
			{
				$policy[]='map_view';
			}
			if($op_data['image_view']==1)
			{
				$policy[]='image_view';
			}
			$post_data['policies']=json_encode($policy);
			$post_data['title']=$op_data['title'];
			$user_id=$op_data['user_id'];
			$this->custom_db->update_record('user', $post_data, array('user_id' => $user_id));
			//PROFILE IMAGE UPLOAD
			// debug($_FILES);
			if (valid_array($_FILES) == true and $_FILES['image']['error'] == 0 and $_FILES['image']['size'] > 0) {
				$config['upload_path'] = $this->template->domain_image_upload_path();//FIXME: Jaganath get Correct Path
				
				$config['allowed_types'] = '*';
				$config['file_name'] = time();
				$config['max_size'] = '1000000';
				$config['max_width']  = '';
				$config['max_height']  = '';
				$config['remove_spaces']  = false;
				//UPDATE
				// $temp_record = $this->custom_db->single_table_records('user', 'image', array('user_id' => $user_id));
				// $icon = $temp_record['data'][0]['image'];
				// //DELETE OLD FILES
				// if (empty($icon) == false) {
				// 	if (file_exists($config['upload_path'].$icon)) {
				// 		unlink($config['upload_path'].$icon);
				// 	}
				// }
				//UPLOAD IMAGE
				// debug($config);exit;
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('image')) {
					//debug($config);
					echo $this->upload->display_errors();exit;
				} else {
					$image_data =  $this->upload->data();
				}
				// debug($image_data);exit;
				$this->custom_db->update_record('user', array('image' => @$image_data['file_name']), array('user_id' => $user_id));
			}
			$user_data= $this->user_model->get_user_data($user_id);
			$page_data['title'] = '';
			$page_data['first_name'] = ucfirst($user_data[0]['first_name']);
			$page_data['last_name'] = ucfirst($user_data[0]['last_name']);
			$page_data['full_name'] = ucfirst($user_data[0]['first_name']).' '.ucfirst($user_data[0]['last_name']);
			$page_data['user_country_code'] = $user_data[0]['country_code'];
			$page_data['date_of_birth'] = date('d-m-Y', strtotime($user_data[0]['date_of_birth']));
			$page_data['address'] = $user_data[0]['address'];
			if($user_data[0]['phone']=='0')
			{
				$user_data[0]['phone']='';
			}
			$page_data['phone'] = $user_data[0]['phone'];
			$page_data['email'] = provab_decrypt($user_data[0]['email']);
			$page_data['email_prof'] = provab_decrypt($user_data[0]['email']);
			$page_data['profile_image'] = $user_data[0]['image'];
			$page_data['signature'] = $user_data[0]['signature'];
			$page_data['user_policies'] = $user_data[0]['policies'];
			$page_data['privacy'] = $user_data[0]['privacy'];
			
			$page_data['user_id_profile'] = $user_id;
			$return_data['status']="true";
			$return_data['data']=$page_data;
			echo json_encode($return_data);exit;
		} else {
			$return_data['status']="false";
			$return_data['data']="Invalid Parameters";
			echo json_encode($return_data);exit;
		}

		
	}
	function profile()
	{	
		error_reporting(0);
		$get_data = $this->input->post();
		$reponse['status'] =FAILURE_STATUS;
		$response['msg'] ="Datas are empty";

		if(valid_array($get_data) && !empty($get_data['user_id']) && !empty($get_data['first_name']) && !empty($get_data['last_name']) && !empty($get_data['phone'])  && !empty($get_data['country_code'])  && !empty($get_data['address']) && !empty($get_data['title'])) {

			$temp_record  =  $this->custom_db->single_table_records ( 'user', '*', array (
						'user_id' => $get_data['user_id']
				) );
			$user_data=$temp_record['data'];
			if($temp_record['status'] != FAILURE_STATUS){			
			//debug($_FILES); die;
				// PROFILE IMAGE UPLOAD
			if (valid_array ( $_FILES ) == true and $_FILES ['image'] ['error'] == 0 and $_FILES ['image'] ['size'] > 0) {
				$config['upload_path'] = $this->template->domain_image_upload_path(); // FIXME: Jaganath get Correct Path
				// debug($config['upload_path']); exit();
				$config ['allowed_types'] = '*';
				$config ['file_name'] = time ();
				$config ['max_size'] = '1000000';
				$config ['max_width'] = '';
				$config ['max_height'] = '';
				$config ['remove_spaces'] = false;
				// UPDATE
				$temp_record = $this->custom_db->single_table_records ( 'user', 'image', array (
						'user_id' => $this->entity_user_id 
				) );
				$icon = $temp_record ['data'] [0] ['image'];
				// DELETE OLD FILES
				if (empty ( $icon ) == false) {
					if (file_exists ( $config ['upload_path'] . $icon )) {
						unlink ( $config ['upload_path'] . $icon );
					}
				}
				// UPLOAD IMAGE
				$this->load->library ( 'upload', $config );
					if (! $this->upload->do_upload ( 'image' )) {
				//	debug($config);exit();
						echo $this->upload->display_errors ();
						exit ();
					} else {
							$image_data = $this->upload->data ();
					}
				// debug($image_data); die;
					$update_image=	$this->custom_db->update_record ( 'user', array (
							'image' => @$image_data ['file_name'] ), array ('user_id' => $get_data['user_id']) );
		} else{
			
			$reponse['status'] =SUCCESS_STATUS;
			$response['msg'] ="Problem in Image uploading!";
		}	
	
		$update_status = $this->custom_db->update_record ( 'user', $get_data, array ('user_id' => $get_data['user_id']));		
		// debug($update_status); exit();
				if($update_status || $update_image){							
					
					$user_record  =  $this->custom_db->single_table_records ( 'user', '*', array (
					'user_id' => $get_data['user_id']) );
					
					$user_data=$user_record['data'];
					if(!empty($user_data[0]['image'])){
				
					$image =$user_data[0]['image'];
					}else{
						$image='user.png';
					}
					if($user_data[0]['phone']=='0')
					{
						$user_data[0]['phone']='';
					}
					$user_details = array('user_type'=>$user_data[0]['user_type'],
					'auth_user_pointer'=>$user_data[0]['uuid'],
					'user_id'=>$user_data[0]['user_id'],
					'first_name'=>$user_data[0]['first_name'],
					'last_name'=>$user_data[0]['last_name'],
					'user_name'=>$user_data[0]['first_name'].$user_data[0]['last_name'],
					'email_id'=>provab_decrypt($user_data[0]['email']),
					'title'=>$user_data[0]['title'],	
					'phone'=>$user_data[0]['phone'],	
					'address'=>$user_data[0]['address'],	
					'country_code'=>$user_data[0]['country_code'], 
					'image' =>'https://'.$_SERVER['HTTP_HOST'].APP_ROOT_DIR.'/extras/custom/'.CURRENT_DOMAIN_KEY.'/images/'.$image
					);		
					
					//debug($user_details); die;
					$response['status'] =SUCCESS_STATUS;	
					$response['data'] =$user_details;
					$response['msg'] ="Updated successfully";
				}else{
					$reponse['status'] =SUCCESS_STATUS;
					$response['msg'] ="No changes.";
				}
		}else{
				$reponse['status'] =FAILURE_STATUS;
				$response['msg'] ="No record found for this user id.";
		}

		}else{
			$reponse['status'] =FAILURE_STATUS;
			$response['msg'] ="Mandatory Datas are empty (Title, first_name, last_name, country_code, phone, user_id, address)";
		}
		echo json_encode($response);
	}

	/**
	 * Logout function for logout from account and unset all the session variables
	 */
	function initilize_logout(){
		redirect('auth/initilize_logout');
		if (is_logged_in_user()) {
			$this->general_model->update_login_manager($this->session->userdata(LOGIN_POINTER));
			$this->session->unset_userdata(array(AUTH_USER_POINTER => '',LOGIN_POINTER => '') );
			// added by nithin for unseting the email username
			$this->session->unset_userdata('mail_user');
		}
	}

	/**
	 * oops page of application will be loaded here
	 */
	public function ooops()
	{
		$this->template->view('utilities/404.php');
	}
	

	/**
	 * Function to Change the Password of a User
	 */
	public function change_password()
	{
		validate_user_login();
		$data=array();
		$get_data = $this->input->get();
		if(isset($get_data['uid'])) {
			$user_id = intval($this->encrypt->decode($get_data['uid']));
		} else {
			redirect("general/initilize_logout");
		}
		$page_data['form_data'] = $this->input->post();
		if(valid_array($page_data['form_data'])==TRUE) {
			$this->current_page->set_auto_validator();
			if ($this->form_validation->run()) {
				$table_name="user";
				/** Checking New Password and Old Password Are Same OR Not **/
				$condition['password'] = md5($this->input->post('new_password'));
				$condition['user_id'] = $user_id;
				$check_pwd = $this->custom_db->single_table_records($table_name,'password',$condition);
				if(!$check_pwd['status']) {
					$condition['password'] = md5($this->input->post('current_password'));
					$condition['user_id'] = $user_id;
					$data['password'] = md5($this->input->post('new_password'));
					$update_res=$this->custom_db->update_record($table_name, $data, $condition);
					if($update_res)	{
						$this->session->set_flashdata(array('message' => 'UL0010', 'type' => SUCCESS_MESSAGE));
						refresh();
					} else {
						$this->session->set_flashdata(array('message' => 'UL0011', 'type' => ERROR_MESSAGE));
						refresh();
						/*$data['msg'] = 'UL0011';
						 $data['type'] = ERROR_MESSAGE;*/
					}
				} else {
					$this->session->set_flashdata(array('message' => 'UL0012', 'type'=>WARNING_MESSAGE));
					refresh();
					//redirect('general/change_password?uid='.urlencode($get_data['uid']));
				}
			}
		}
		$this->template->view('user/change_password', $data);
	}
	/**
	 * Jaganath
	 * Add Traveller
	 */
	function add_traveller()
	{
		//FIXME:Make Codeigniter Validations -- Jaganath
		validate_user_login();
		$post_data = $this->input->post();
		if(valid_array($post_data) == true && isset($post_data['traveller_first_name']) == true && empty($post_data['traveller_first_name']) == false && isset($post_data['traveller_date_of_birth']) == true && empty($post_data['traveller_date_of_birth']) == false 
		&& isset($post_data['traveller_email']) == true && isset($post_data['traveller_last_name']) == true) {
			$user_traveller_details = array();
			$user_traveller_details['first_name'] = trim($post_data['traveller_first_name']);
			$user_traveller_details['last_name'] = trim($post_data['traveller_last_name']);
			$user_traveller_details['date_of_birth'] = date('Y-m-d', strtotime(trim($post_data['traveller_date_of_birth'])));
			$user_traveller_details['email'] = trim($post_data['traveller_email']);
			$user_traveller_details['created_by_id'] = $user_id;
			$user_traveller_details['created_datetime'] = date('Y-m-d H:i:s');
			$this->custom_db->insert_record('user_traveller_details', $user_traveller_details);
		}
		if(empty($_SERVER['QUERY_STRING']) == false) {
			$query_string = '?'.$_SERVER['QUERY_STRING'];
		} else {
			$query_string = '';
		}
		redirect('user/profile'.$query_string);
	}
	/**
	 * Jaganath
	 */
	function update_traveller_details()
	{
		//FIXME:Make Codeigniter Validations -- Jaganath
		$post_data = $this->input->post();
		if(valid_array($post_data) == true && isset($post_data['origin']) == true && intval($post_data['origin']) > 0 &&
		isset($post_data['traveller_first_name']) == true && empty($post_data['traveller_first_name']) == false && isset($post_data['traveller_date_of_birth']) == true && empty($post_data['traveller_date_of_birth']) == false 
		&& isset($post_data['traveller_email']) == true && isset($post_data['traveller_last_name']) == true) {
			$user_traveller_details = array();
			$user_traveller_details['first_name'] = trim($post_data['traveller_first_name']);
			$user_traveller_details['last_name'] = trim($post_data['traveller_last_name']);
			$user_traveller_details['date_of_birth'] = date('Y-m-d', strtotime(trim($post_data['traveller_date_of_birth'])));
			$user_traveller_details['email'] = trim($post_data['traveller_email']);
			
			$user_traveller_details['passport_user_name'] = trim($post_data['passport_user_name']);
			$user_traveller_details['passport_nationality'] = trim($post_data['passport_nationality']);
			$user_traveller_details['passport_expiry_day'] = trim($post_data['passport_expiry_day']);
			$user_traveller_details['passport_expiry_month'] = trim($post_data['passport_expiry_month']);
			$user_traveller_details['passport_expiry_year'] = trim($post_data['passport_expiry_year']);
			$user_traveller_details['passport_number'] = trim($post_data['passport_number']);
			$user_traveller_details['passport_issuing_country'] = trim($post_data['passport_issuing_country']);
			$user_traveller_details['updated_by_id'] = $user_id;
			$user_traveller_details['updated_datetime'] = date('Y-m-d H:i:s');
			$this->custom_db->update_record('user_traveller_details', $user_traveller_details, array('origin' => intval($post_data['origin'])));
		}
		if(empty($_SERVER['QUERY_STRING']) == false) {
			$query_string = '?'.$_SERVER['QUERY_STRING'];
		} else {
			$query_string = '';
		}
		redirect('user/profile'.$query_string);
	}
	public function bus_transcation_history()
	{
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		if(!empty($user_id)){
			
				$bus_transcation_details 	=	$this->user_model->bus_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);

				if(valid_array($bus_transcation_details)){

				/*	$booking_details = $bus_transcation_details['data']['booking_details'];
					if(valid_array($booking_details)){
						$i=0;
						foreach($booking_details as $detaisl){
							$detaisl[$i] = $detaisl;
							$detaisl[$i]['departure_date'] = date("d-m-y", strtotime($detaisl['departure_datetime']));
							$detaisl[$i]['departure_time'] = date("h:i", strtotime($detaisl['departure_datetime']));
							//$detaisl[$i]['arrival_date'] = date("d-m-y", strtotime($detaisl['arrival_datetime']));
							$detaisl[$i]['arrival_time'] = date("h:i", strtotime($detaisl['arrival_datetime']));

						$i++;
						}
				$booking_details= $detaisl;
				$bus_transcation_details['data']['booking_details'] =$booking_details;
					} */
					
					$bus_transcation_details		= json_decode(json_encode($bus_transcation_details), True);
					// debug($bus_transcation_details);exit;
					foreach ($bus_transcation_details['data']['booking_details'] as $fkey => $fvalue) {
						$bus_transcation_details['data']['booking_details'][$fkey]['fare']=$fvalue['fare']+$fvalue['admin_markup']+$fvalue['convinence_amount'];
					}

				}else{
					$transcation_details['hotels']	= 	array();
				}
				$transcation_details['hotels']	= 	$bus_transcation_details;
				
			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
		}
		echo json_encode($transcation_details);exit();
	}

	/**
	 * Shubham Jain
 	*/

	public function sightseeing_transcation_history()
	{
		//echo "etdsf"; exit();
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		// debug($user_id); exit();
		if(!empty($user_id)){
			$sightseeing_transcation_details 	=	$this->user_model->sightseeing_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
			if(valid_array($sightseeing_transcation_details['data'])){
					// debug($sightseeing_transcation_details['data']); exit;

				$sightseeing_booking_counts = count($sightseeing_transcation_details['data']['booking_details']);
				$sightseeing_transcation_details		= json_decode(json_encode($sightseeing_transcation_details['data']), True);
				$transcation_details['status'] = SUCCESS_STATUS;
				$transcation_details['sightseeing']	= 	$sightseeing_transcation_details;
				echo json_encode($transcation_details);exit();
			}else{
				$transcation_details['status'] = FAILURE_STATUS;
				$transcation_details['sightseeing']	= 	array();
				echo json_encode($transcation_details);exit();
			}

			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
			echo json_encode($transcation_details);exit();
		}
	}



	/**
	 * Shubham Jain
 	*/
	public function transfers_transcation_history()
	{
		//echo "etdsf"; exit();
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		// debug($user_id); exit();
		if(!empty($user_id)){
			$transfers_transcation_details 	=	$this->user_model->transfers_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
			if(valid_array($transfers_transcation_details['data'])){
					// debug($sightseeing_transcation_details['data']); exit;

				$transfers_booking_counts = count($transfers_transcation_details['data']['booking_details']);
				$transfers_transcation_details		= json_decode(json_encode($transfers_transcation_details['data']), True);	
				$transcation_details['status'] = SUCCESS_STATUS;
				$transcation_details['transfer']	= 	$transfers_transcation_details;
				echo json_encode($transcation_details);exit();
			}else{
				$transcation_details['status'] = FAILURE_STATUS;
				$transcation_details['transfer']	= 	array();
				echo json_encode($transcation_details);exit();
			}

			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
			echo json_encode($transcation_details);exit();
		}
	}




	/**
	 * Jaganath
	 */
	function traveller_details()
	{
		$data = array();
		$data['user_passport_visa_details'] = array();
		$data['traveller_details'] = array();
		//traveller details
		$traveller_details = $this->custom_db->single_table_records('user_traveller_details', '*', array('created_by_id' => $user_id, 'user_id' => 0));
		if($traveller_details['status'] == true) {
			$data['traveller_details'] = $traveller_details['data'];
		}
		//User PassportVisa details
		$user_passport_visa_details = $this->custom_db->single_table_records('user_traveller_details', '*', array('created_by_id' => $user_id, 'user_id' => $user_id));
		if($user_passport_visa_details['status'] == true) {
			$data['traveller_details'] = $user_passport_visa_details['data'][0];
		}
		return $data;
	}

	public function flight_transcation_history()
	{
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		if(!empty($user_id)){
				$flight_transcation_details 	=	$this->user_model->flight_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
				//debug($flight_transcation_details);exit;
				if(valid_array($flight_transcation_details['data'])){
					// debug($flight_transcation_details['data']); exit;
					$flight_booking_counts = count($flight_transcation_details['data']['booking_details']);
					
					$flight_transcation_details		= json_decode(json_encode($flight_transcation_details['data']['booking_details']), True);
					foreach ($flight_transcation_details as $fkey => $fvalue) {
						$flight_transcation_details[$fkey]['total_fare']=$fvalue['total_fare']-$fvalue['admin_commission']+$fvalue['admin_tds']+$fvalue['convinence_value'];
						$flight_transcation_details[$fkey]['attributes']= json_decode($fvalue['attributes'], true);
					}
					$transcation_details['status'] = SUCCESS_STATUS;
					$transcation_details['flights']	= 	$flight_transcation_details;
					echo json_encode($transcation_details);exit();
				}else{
					$transcation_details['status'] = FAILURE_STATUS;
					$transcation_details['flights']	= 	array();
					echo json_encode($transcation_details);exit();
				}
				
			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
			echo json_encode($transcation_details);exit();
		}
		
	}


	public function flight_shared_transcation_history()
	{
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		if(!empty($user_id)){
				$flight_transcation_details 	=	$this->user_model->flight_shared_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
				if(valid_array($flight_transcation_details['data'])){
					// debug($flight_transcation_details['data']); exit;

					$flight_booking_counts = count($flight_transcation_details['data']['booking_details']);
					$flight_transcation_details		= json_decode(json_encode($flight_transcation_details['data']['booking_details']), True);
					$transcation_details['status'] = SUCCESS_STATUS;
					$transcation_details['flights']	= 	$flight_transcation_details;
					echo json_encode($transcation_details);exit();
				}else{
					$transcation_details['status'] = FAILURE_STATUS;
					$transcation_details['flights']	= 	array();
					echo json_encode($transcation_details);exit();
				}
				
			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
			echo json_encode($transcation_details);exit();
		}
		
	}


	public function hotel_transcation_history()
	{
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		if(!empty($user_id)){
			
				$hotel_transcation_details 	=	$this->user_model->hotel_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
				if(valid_array($hotel_transcation_details)){
					$hotel_transcation_details		= json_decode(json_encode($hotel_transcation_details), True);
					// debug($hotel_transcation_details);exit;
					foreach ($hotel_transcation_details as $fkey => $fvalue) {
						$hotel_transcation_details[$fkey]['total_fare']=$fvalue['total_fare']+$fvalue['admin_markup']+$fvalue['convinence_amount'];
					}


				}else{
					$transcation_details['data']	= 	array();
				}
				$transcation_details['data']	= 	$hotel_transcation_details;
				$transcation_details['status'] = SUCCESS_STATUS;
			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
		}
		echo json_encode($transcation_details);exit();
	}

	public function hotel_shared_transcation_history()
	{
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		if(!empty($user_id)){
			
				$hotel_transcation_details 	=	$this->user_model->hotel_shared_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
				if(valid_array($hotel_transcation_details)){
					$hotel_transcation_details		= json_decode(json_encode($hotel_transcation_details), True);
				}else{
					$transcation_details['hotels']	= 	array();
				}
				$transcation_details['hotels']	= 	$hotel_transcation_details;
			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
		}
		echo json_encode($transcation_details);exit();
	}

/*	public function sightseeing_transcation_history()
	{
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		if(!empty($user_id)){
				$sightseeing_transcation_details 	=	$this->user_model->sightseeing_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
				if(valid_array($sightseeing_transcation_details['data'])){
					// debug($sightseeing_transcation_details['data']); exit;

					$sightseeing_booking_counts = count($sightseeing_transcation_details['data']['booking_details']);
					$sightseeing_transcation_details		= json_decode(json_encode($sightseeing_transcation_details['data']['booking_details']), True);
					$transcation_details['status'] = SUCCESS_STATUS;
					$transcation_details['sightseeing']	= 	$sightseeing_transcation_details;
					echo json_encode($transcation_details);exit();
				}else{
					$transcation_details['status'] = FAILURE_STATUS;
					$transcation_details['sightseeing']	= 	array();
					echo json_encode($transcation_details);exit();
				}
				
			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
			echo json_encode($transcation_details);exit();
		}
	}*/

	public function sightseeing_shared_transcation_history()
	{
		$user_id 						= 	$this->input->post('user_id');
		//$user_id						=	'909';
		if(!empty($user_id)){
				$sightseeing_transcation_details 	=	$this->user_model->sightseeing_shared_booking($condition=array(),$count=0,$offset=0,$limit=1000000,$user_id);
				if(valid_array($sightseeing_transcation_details['data'])){
					// debug($sightseeing_transcation_details['data']); exit;

					$sightseeing_booking_counts = count($sightseeing_transcation_details['data']['booking_details']);
					$sightseeing_transcation_details		= json_decode(json_encode($sightseeing_transcation_details['data']['booking_details']), True);
					$transcation_details['status'] = SUCCESS_STATUS;
					$transcation_details['sightseeing']	= 	$sightseeing_transcation_details;
					echo json_encode($transcation_details);exit();
				}else{
					$transcation_details['status'] = FAILURE_STATUS;
					$transcation_details['sightseeing']	= 	array();
					echo json_encode($transcation_details);exit();
				}
				
			
		}else{
			$transcation_details['status'] = FAILURE_STATUS;
			$transcation_details['message'] = "User Id Not Found !!";
			echo json_encode($transcation_details);exit();
		}
	}

	public function hotel_ticket()
	{
		$this->load->model('hotel_model');
		$ticket = $this->input->post('ticket_details');

		$hotel_ticket_details = json_decode($ticket);
		$ticket_details = json_decode(json_encode($hotel_ticket_details), True);

		if(valid_array($ticket_details))
		{
			extract($ticket_details);
			if (empty($app_reference) == false) {
			$booking_details = $this->hotel_model->get_booking_details($app_reference, $booking_source, $booking_status);
				if ($booking_details['status'] == SUCCESS_STATUS) {
					//Assemble Booking Data
					$data['status'] = SUCCESS_STATUS;
					$assembled_booking_details = $this->booking_data_formatter->format_hotel_booking_data($booking_details, 'b2c');
					$data['ticket'] = $assembled_booking_details['data'];	
				}else{
					$data['status'] = FAILURE_STATUS;
					$data['message'] = "Ticket Detail Not Found !!";
				}
			}
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Invalid Request !!";
		}
		echo json_encode($data);
		exit;	
	}

	function flight_ticket()
	{
		$this->load->model('flight_model');
		$ticket = $this->input->post('ticket_details');

		$flight_ticket_details = json_decode($ticket);
		$ticket_details = json_decode(json_encode($flight_ticket_details), True);

		if(valid_array($ticket_details))
		{
			extract($ticket_details);

			if (empty($app_reference) == false) {
				$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
				
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$data['status'] = SUCCESS_STATUS;
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2c');	
				$data['ticket'] = $assembled_booking_details['data'];

			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Ticket Detail Not Found !!";
			}
		}
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Invalid Request !!";
		}
		echo json_encode($data);
		exit;	
	}

	function bus_ticket()
	{
		$this->load->model('bus_model');
		$ticket = $this->input->post('ticket_details');

		$bus_ticket_details = json_decode($ticket);
		$ticket_details = json_decode(json_encode($bus_ticket_details), True);

		if(valid_array($ticket_details))
		{
			extract($ticket_details);
			if (empty($app_reference) == false) {
				$booking_details = $this->bus_model->get_booking_details($app_reference, $booking_source, $booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$data['status'] = SUCCESS_STATUS;
				$assembled_booking_details = $this->booking_data_formatter->format_bus_booking_data($booking_details, 'b2c');	
				$data['ticket'] = $assembled_booking_details['data'];

			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Ticket Detail Not Found !!";
			}
		}
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Invalid Request !!";
		}
		echo json_encode($data);
		exit;	
	}

	/***** Login with facebook in mobile ****/

	function mob_login_facebook()
	{
		//$post_data = '{"user_logged_in":"0","email":"jeeva.provab@gmail.com","first_name":"jeeva","last_name":"k"}';
		$post_data = $this->input->post('get_fb_detail');
		if(empty($post_data)){
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Request fields are missing";
			$res['data'] = array();
			echo json_encode($res);
			exit;
		}
		$var = json_decode($post_data);		
		$data = json_decode(json_encode($var), True);
		if(isset($data) && valid_array($data)){
			if($data['user_logged_in'] == false){
				$user_exists = $this->username_check($data['email']);
				if($user_exists['status'] == FAILURE_STATUS){
					$user_record = $this->user_model->create_user($data['email'], "password", $data['first_name'],0, $data['last_name'], $creation_source='facebook', $country_code=212, $country_name="United Arab Emirates");
					if ($user_record != '' and valid_array($user_record) == true) {
						$status = true;
						//create login pointer
						$user_record = $user_record['data'];
						$res['user_type'] = $user_record[0]['user_type'];
						$res['auth_user_pointer'] = $user_record[0]['uuid'];
						$res['user_id'] = $user_record[0]['user_id'];
						$res['email'] =  provab_decrypt($user_record[0]['email']);
						// debug($user_record[0]['phone']);exit;
						if($user_record[0]['phone']=='0'||$user_record[0]['phone']==0)
						{
							$user_record[0]['phone']='';
						}
						$res['phone'] = $user_record[0]['phone'];
						$res['first_name'] = $user_record[0]['first_name'];
						$res['last_name'] = $user_record[0]['last_name'];
						$res['message'] = 'Login Successful';	
						echo json_encode($res);
						exit;			
					}else{
						$res['status'] = FAILURE_STATUS;
						$res['message'] = "User Details Not Found !!";
						$res['data'] = array();
						echo json_encode($res);
						exit;
					}
				} else {
					$userdata = $this->custom_db->single_table_records('user','*',array('email'=>provab_encrypt($data['email'])));
					$userdata = $userdata['data'];
					$res['user_type'] = $userdata[0]['user_type'];
					$res['auth_user_pointer'] = $userdata[0]['uuid'];
					$res['user_id'] = $userdata[0]['user_id'];
					$res['email'] =  provab_decrypt($userdata[0]['email']);
					if($user_data[0]['phone']==0)
					{
						$user_data[0]['phone']='';
					}
					$res['phone'] = $userdata[0]['phone'];
					$res['first_name'] = $userdata[0]['first_name'];
					$res['last_name'] = $userdata[0]['last_name'];
					$res['message'] = 'Login Successful';	
					echo json_encode($res);
					exit;			
				}
			}else{
				$res['status'] = FAILURE_STATUS;
				$res['message'] = "Please Logout to Register !!";
				$res['data'] = array();
				echo json_encode($res);
				exit;
			}
		}
	}

	/***** Login with Google in mobile ****/

	function mob_login_google()
	{
		//$post_data = '{"user_logged_in":"0","email":"jeeva.provab@gmail.com","first_name":"jeeva","last_name":"k"}';
		$post_data = $this->input->post('get_google_detail');
		if(empty($post_data)){
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Request fields are missing";
			$res['data'] = array();
			echo json_encode($res);
			exit;
		}
		$var = json_decode($post_data);		
		$data = json_decode(json_encode($var), True);
		if(isset($data) && valid_array($data)){
			if($data['user_logged_in'] == false){
				$user_exists = $this->username_check($data['email']);
				// debug($user_exists);exit;
				if($user_exists['status'] == FAILURE_STATUS){
					$user_record = $this->user_model->create_user($data['email'], "password", $data['first_name'],0, $data['last_name'], $creation_source='google', $country_code=212, $country_name="United Arab Emirates");
					if ($user_record != '' and valid_array($user_record) == true) {
						$status = true;
						//create login pointer
						$user_record = $user_record['data'];
						$res['user_type'] = $user_record[0]['user_type'];
						$res['auth_user_pointer'] = $user_record[0]['uuid'];
						$res['user_id'] = $user_record[0]['user_id'];
						$res['email'] =  provab_decrypt($user_record[0]['email']);
						if($user_record[0]['phone']=='0')
						{
							$user_record[0]['phone']='';
						}
						$res['phone'] = $user_record[0]['phone'];
						$res['first_name'] = $user_record[0]['first_name'];
						$res['last_name'] = $user_record[0]['last_name'];
						$res['message'] = 'Login Successful';	
						// debug($res);exit;
						echo json_encode($res);
						exit;			
					}else{
						$res['status'] = FAILURE_STATUS;
						$res['message'] = "User Details Not Found !!";
						$res['data'] = array();
						echo json_encode($res);
						exit;
					}
				} else {
					// debug(provab_encrypt($data['email']));
					$userdata = $this->custom_db->single_table_records('user','*',array('email'=>provab_encrypt($data['email'])));
					// echo $this->db->last_query();
					// debug($userdata);exit;
					$userdata = $userdata['data'];
					$res['user_type'] = $userdata[0]['user_type'];
					$res['auth_user_pointer'] = $userdata[0]['uuid'];
					$res['user_id'] = $userdata[0]['user_id'];
					$res['email'] =  provab_decrypt($userdata[0]['email']);
					if($userdata[0]['phone']==0)
					{
						$userdata[0]['phone']='';
					}
					$res['phone'] = $userdata[0]['phone'];
					$res['first_name'] = $userdata[0]['first_name'];
					$res['last_name'] = $userdata[0]['last_name'];
					$res['message'] = 'Login Successful';	
					echo json_encode($res);
					exit;			
				}
			}else{
				$res['status'] = FAILURE_STATUS;
				$res['message'] = "Please Logout to Register !!";
				$res['data'] = array();
				echo json_encode($res);
				exit;
			}
		}
	}

	/**
	 * Call back function to check username availability
	 * @param string $name
	 */
	public function username_check($name) {
		$condition['email'] = provab_encrypt($name);
		$condition['user_type'] = B2C_USER;
		$condition['domain_list_fk'] = intval(get_domain_auth_id());
		$data = $this -> custom_db -> single_table_records('user', array('user_id', 'user_name'), $condition);
		
		if ($data['status'] == SUCCESS_STATUS and valid_array($data['data']) == true) {
			//$this -> form_validation -> set_message('username_check', $name . ' Is Not Available!!!');
			return $data;
		} else {
			$data['status'] = FAILURE_STATUS;
			$data['data'] = array();
		}
	}

	public function get_profile_details()
	{
		$user_id		=$this->input->post('user_id');
		if(empty($user_id)==false){
			$get_data = $this->custom_db->single_table_records('user','*',array('user_id'=>$user_id));
			if($get_data['status'] == SUCCESS_STATUS && valid_array($get_data['data'])){

				$data['user_id']      = $get_data['data'][0]['user_id'];
				$data['uuid']         = $get_data['data'][0]['uuid'];
				$data['user_type']    = $get_data['data'][0]['user_type'];
				$data['email']        = $get_data['data'][0]['email'];
				$data['user_name']    = $get_data['data'][0]['user_name'];
				$data['status']       = $get_data['data'][0]['status'];
				$data['image']        = $GLOBALS['CI']->template->domain_images($get_data['data'][0]['image']);
				$data['title']        = $get_data['data'][0]['title'];
				$data['first_name']   = $get_data['data'][0]['first_name'];
				$data['last_name']    = $get_data['data'][0]['last_name'];
				$data['address']      = $get_data['data'][0]['address'];
				$data['city']         = $get_data['data'][0]['city'];
				$data['state']        = $get_data['data'][0]['state'];
				$data['pin_code']     = $get_data['data'][0]['pin_code'];
				$data['country_name'] = $get_data['data'][0]['country_name'];
				$data['phone']        = $get_data['data'][0]['phone'];

				echo json_encode($data);
				exit;

			}else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Unable to get the user details";
				$data['data'] = array();
				echo json_encode($data);
				exit;
			}
			
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please Login to see your details";
			$data['data'] = array();
			echo json_encode($data);
			exit;
		}
	}
function get_traveller_lat_long($app_reference)//$app_reference
	{
		// $app_reference='FB07-104411-520971';
		
		$journey_details=$this->transaction_model->get_journey_details($app_reference);
		// debug($journey_details);exit;
		$from_city=explode(' ', $journey_details['0']['journey_from']);
		$to_city=explode(' ', $journey_details['0']['journey_to']);
		$journey_details['0']['journey_from']=$from_city['0'];
		$journey_details['0']['journey_to']=$to_city['0'];
		$journey_from_loc=$this->transaction_model->get_city_loc($journey_details['0']['journey_from']);
		$journey_details['0']['journey_from']=$journey_from_loc['0']['city'];
		$journey_details['0']['journey_from_lat']=$journey_from_loc['0']['lat'];
		$journey_details['0']['journey_from_lon']=$journey_from_loc['0']['lng'];
		$journey_to_loc=$this->transaction_model->get_city_loc($journey_details['0']['journey_to']);
		$journey_details['0']['journey_to']=$journey_to_loc['0']['city'];
		$journey_details['0']['journey_to_lat']=$journey_to_loc['0']['lat'];
		$journey_details['0']['journey_to_lon']=$journey_to_loc['0']['lng'];
		return $journey_details;

	}


	function get_traveller_lat_long2($app_reference)//$app_reference
	{
		// $app_reference='FB07-104411-520971';
		$journey_details=$this->transaction_model->get_journey_details($app_reference);
		//debug($journey_details);exit;
		$from_city=explode(' ', $journey_details['0']['journey_from']);
		$to_city=explode(' ', $journey_details['0']['journey_to']);
		$journey_details['0']['journey_from']=$from_city['0'];
		$journey_details['0']['journey_to']=$to_city['0'];
		$journey_from_loc=$this->transaction_model->get_city_loc2($journey_details['0']['journey_from']);
		$journey_details['0']['journey_from']=$journey_from_loc['0']['city'];
		$journey_details['0']['journey_from_lat']=$journey_from_loc['0']['lat'];
		$journey_details['0']['journey_from_lon']=$journey_from_loc['0']['lng'];
		$journey_to_loc=$this->transaction_model->get_city_loc2($journey_details['0']['journey_to']);
		$journey_details['0']['journey_to']=$journey_to_loc['0']['city'];
		$journey_details['0']['journey_to_lat']=$journey_to_loc['0']['lat'];
		$journey_details['0']['journey_to_lon']=$journey_to_loc['0']['lng'];

		$locations[0]['lat']= $journey_details[0]['journey_from_lat'];
		$locations[0]['lng']= $journey_details[0]['journey_from_lon'];
		$locations[0]['title']= $journey_details[0]['journey_from'];
		$locations[0]['target']['lat']= $journey_details[0]['journey_to_lat'];
	    $locations[0]['target']['lng']= $journey_details[0]['journey_to_lon'];
	    $locations[0]['target']['title']= $journey_details[0]['journey_to'];

	 //    ob_end_flush();
		header('Content-Type: application/json');
		// debug($locations);exit;
		echo json_encode($locations);
		exit;

	}

	function add_story()
	{
		// error_reporting(E_ALL);
		$post_data = $this->input->post();
		// debug($post_data);
		// echo json_encode($post_data);
		$post_data['tageduser']=json_decode($post_data['tageduser'],true);
		// debug($post_data['tageduser']);exit;
		// debug();exit;
		$hash= explode(' ', $post_data['remarks']);
		$i=0;
		foreach ($hash as $key => $value) {
			if (preg_match("/[#]/", $hash[$key], $match))
			{
				$hash_variable[$i]=$hash[$key];
				$i+=1;
				$hash_text=explode('#',$hash[$key]);
				$hash[$key]='<a id="hash_id" href="'. get_host().'/index.php/user/story_board/home/'.$hash_text['1'].'">'.$hash[$key].'</a>';
				
			}
		}

		$post_data['remarks']=implode(" ",$hash);
		$post_data['hashtags']=json_encode($hash_variable);
		
		$data['title']=$post_data['title'];
		$data['remarks']=$post_data['remarks'];
		$data['hashtags']=$post_data['hashtags'];
		$data['user_id']=$post_data['user_id'];
		$insert_id=$this->custom_db->insert_record('reviews',$data);
		$review_id=$insert_id['insert_id'];
	    $tagged_user=$this->insert_tagged_friends($review_id,$post_data['tageduser'],$post_data['user_id']);
		$img=[];
		$removed_images=json_decode($post_data['removed_images'][0],true);
		$response['story_data']=$post_data;
		$response['image_data']=$_FILES;
		// debug($_FILES);exit;
		// echo json_encode($_FILES);exit;
		$tmp_files=$_FILES;
		$files_data=$this->format_files_data($_FILES);
		$_FILES=$files_data;
		// debug($_FILES);exit;
		// echo json_encode($response);exit;
		if (valid_array($_FILES)) 
		{
			foreach ($_FILES as $f_key => $f_value) {
				if(isset($_FILES[$f_key]) && !empty($_FILES[$f_key]['name'][0]))
				{
					if(isset($removed_images ))
							{
								foreach ($removed_images as $r_k => $r_v) {
									foreach ($_FILES[$f_key]['name'] as $f_r_k => $f_r_v) 
									{
										if($_FILES[$f_key]['name'][$f_r_k]==$removed_images[$r_k])
										{
											unset($_FILES[$f_key]['name'][$f_r_k]);
											unset($_FILES[$f_key]['type'][$f_r_k]);
											unset($_FILES[$f_key]['tmp_name'][$f_r_k]);
											unset($_FILES[$f_key]['error'][$f_r_k]);
											unset($_FILES[$f_key]['size'][$f_r_k]);
										}
									}
								}
							}
					// debug($_FILES);exit;
					foreach ($_FILES[$f_key]['name'] as $key => $value) 
					{

							$files['name'][$key]	= $_FILES[$f_key]['name'][$key];
							$files['type'][$key]	= $_FILES[$f_key]['type'][$key];
							$files['tmp_name'][$key]	= $_FILES[$f_key]['tmp_name'][$key];
							$files['error'][$key]	= $_FILES[$f_key]['error'][$key];
							$files['size'][$key]	= $_FILES[$f_key]['size'][$key];
									
					}
					// debug($files);exit;


					$images=$this->upload_images($files,$post_data);
					foreach ($images as $push_key => $push_value) {
						array_push($img, $images[$push_key]);
					}
				}
				
			}

		}
		// debug($img);exit;
		// debug($tmp_files);exit;
		foreach ($img as $imkey => $imvalue) {
			$k='thumbnail'.($imkey+1);
			// $img_name='images'.($imkey+1);
			// debug($k);exit;
			if(isset($tmp_files[$k]))
			{
				// debug($tmp_files[$k]);
				// $filess=$tmp_files[$k];
				$filess['name'][0]=$tmp_files[$k]['name'];
				$filess['type'][0]=$tmp_files[$k]['type'];
				$filess['tmp_name'][0]=$tmp_files[$k]['tmp_name'];
				$filess['error'][0]=$tmp_files[$k]['error'];
				$filess['size'][0]=$tmp_files[$k]['size'];
				// debug($filess);
				$thumb=$this->upload_images($filess,$post_data);
				// debug($thumb);
				$thumbnail[$imvalue]=$thumb;
			}
		}	
		// debug($thumbnail);exit;

		// if(isset($tmp_img[))
		$data['images']=json_encode($img);
		$data['thumbnail']=json_encode($thumbnail);
		$condition = array('review_id' => $review_id );
		$update_status= $this->custom_db->update_record('reviews',$data,$condition);
		// debug($insert_id);debug($update_status);exit;
		 if($update_status==1 && !empty($insert_id))
		 {
		 	$return_data['status']="true";
		 }else{
		 	$return_data['status']="false";
		 }
		 $return_data['data']=$this->user_model->get_stories($post_data['user_id'],0,200,0);
		 echo json_encode($return_data);exit;
		 // redirect('user/story_board');

		
	}


	function format_files_data($files)
	{
		$i=0;
		foreach ($files as $f_i_f_k => $f_i_f_v) {
			// debug($f_i_f_k);exit;
			// if()
			$key='images'.($i+1);
			$i=$i+1;
			if(isset($files[$key]))
			{

				$images['images']['name'][]=$files[$key]['name'];
				$images['images']['type'][]=$files[$key]['type'];
				$images['images']['tmp_name'][]=$files[$key]['tmp_name'];
				$images['images']['error'][]=$files[$key]['error'];
				$images['images']['size'][]=$files[$key]['size'];
			}
			// debug($key);
		}
		// debug($images);exit;
		return $images;

	}

	function format_files_data_edit($files)
	{
		$i=0;
		foreach ($files as $f_i_f_k => $f_i_f_v) {
			// debug($f_i_f_k);exit;
			// if()
			$key='images'.($i+1);
			$i=$i+1;
			if(isset($files[$f_i_f_k]))
			{

				$images['images']['name'][]=$files[$f_i_f_k]['name'];
				$images['images']['type'][]=$files[$f_i_f_k]['type'];
				$images['images']['tmp_name'][]=$files[$f_i_f_k]['tmp_name'];
				$images['images']['error'][]=$files[$f_i_f_k]['error'];
				$images['images']['size'][]=$files[$f_i_f_k]['size'];
			}
			// debug($key);
		}
		// debug($images);exit;
		return $images;

	}

	function upload_images($files,$post_data)
	{
		// print_r("sdfdf");
		$config['upload_path'] = $this->template->domain_image_upload_path();//FIXME: Jaganath get Correct Path
		$config['allowed_types'] = '*';
		$config['file_name'] = time();
		$config['max_size'] = '1000000';
		$config['max_width']  = '';
		$config['max_height']  = '';
		$config['remove_spaces']  = false;
		//UPDATE
		
        $this->load->library('upload', $config);

        $images = [];
        // debug($images);exit;
        // debug($files['name']);exit;
        foreach ($files['name'] as $key => $image) {
        	// echo "hello";exit;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];
            $img_title='';
            $title=explode(' ',$post_data['title']);
            foreach ($title as $key => $value) {
            	$img_title.=$title[$key];
            	$img_title.='_';
            }
            $t=time();
            $img_p_title='';
            $imgtitle=explode(' ',$image);
            foreach ($imgtitle as $tkey => $tvalue) {
            	if($tkey<(sizeof($imgtitle)-1))
            	{

	            	$img_p_title.=$imgtitle[$tkey];
	            	$img_p_title.='_';
            	}else if($tkey==(sizeof($imgtitle)-1)){
            		$img_p_title.=$imgtitle[$tkey];
            	}
            }
            $image=$img_p_title;
            // debug($image);exit;
            $fileName = $t.'_'.$image;

            $images[] = $fileName;
            // debug($iamges);
            $config['file_name'] = $fileName;
            // debug($images);debug($config);exit;
            $this->upload->initialize($config);
            $up_data=$this->upload->do_upload('images[]');
            // debug($up_data);
            if ($up_data) {
                $this->upload->data();
                $f_name=explode('.',$images[0] );
                $f_name[0]=$f_name[0].'_mobile';
                $f_name=implode('.', $f_name);
				$url = $this->template->domain_image_upload_path().$f_name;

				$filename = $this->compress_image($_FILES['images[]']['tmp_name'], $url, 50);
				// debug($filename);exit;
			} else {
            	// echo "exit";
                return false;
            }
        }
        // debug($images);exit;
        return $images;
	}

	function share_post()
	{
		$post_api_data=$this->input->post();
		// debug($post_api_data);
		$post_data=$this->get_story_on_story_id($post_api_data['review_id'],0);
		$post_data=$post_data[0];
		// echo "hello";debug($post_data);exit;
		$data['title']=$post_data['title'];
		$data['remarks']=$post_data['remarks'];
		$data['hashtags']=$post_data['hashtags'];
		$data['images']=$post_data['images'];
		$data['user_id']=$post_api_data['user_id'];
		$data['share_status']=1;
		$data['parent_post_id']=$post_data['review_id'];
		$data['parent_user_id']=$post_data['user_id'];
		// debug($data);exit;
		$insert_id=$this->custom_db->insert_record('reviews',$data);
		// debug($insert_id);exit;
		if($insert_id['status']==1)
		{
			$not_data['review_id']=$post_data['review_id'];
			$not_data['notifier_id']=$post_data['review_id'];
			$not_data['user_id']=$post_api_data['user_id'];
			$not_data['notification_type']=6;//shared_post
			$insert_id=$this->custom_db->insert_record('review_notifications',$not_data);
			$this->send_notification($not_data['user_id'],$not_data['notifier_id'],$not_data['notification_type']);
			// debug($insert_id);exit;
		}
		 $this->update_user_status();

		if($insert_id['status']==1)
		{
			$story_data=$this->user_model->get_stories($post_api_data['user_id'],0,200,0);
			echo json_encode($story_data);exit;
		}else{
			echo json_encode($insert_id);exit;
			
		}

	}

	function update_user_status($user_id='')
	{
		if($user_id=='')
		{
			$this->user_model->update_ts($this->entity_user_id);

		}else{
			$this->user_model->update_ts($user_id);
			
		}
		return true;
	}
	function add_comment()
	{
		$post_data = $this->input->post();
		 // debug($post_data);exit;
		$data['review_id']=$post_data['story_id'];
		$data['comment']=$post_data['comment_area'];
		$data['user_id']=$post_data['user_id'];
		$this->custom_db->insert_record('review_comments',$data);
		$condition = array('review_id' => $data['review_id'] );
		$review_user_id=$this->custom_db->single_table_records('reviews','user_id',$condition);
		// debug($review_user_id);exit;
		if($review_user_id['status']==1)
		{
			if($review_user_id['data'][0]['user_id']!=$data['user_id'])
			{
				$notification_data['review_id']=$data['review_id'];
				$notification_data['notifier_id']=$review_user_id['data'][0]['user_id'];
				$notification_data['user_id']=$data['user_id'];
				$notification_data['notification_type']=2;
				$notification_status=$this->custom_db->insert_record('review_notifications',$notification_data);
				$this->send_notification($notification_data['user_id'],$notification_data['notifier_id'],$notification_data['notification_type']);
			}
		}
		// debug($review_user_id);debug($notification_status);exit;
		if($review_user_id['status']==1)
		{
			$return_data['status']=true;
		 	$return_data['data']=$this->user_model->get_stories($post_data['user_id'],0,200,0,'',$post_data['story_id']);
		}else{
			$return_data['status']=false;
		}
		echo json_encode($return_data);exit;
		// $this->session->set_flashdata(array('message' => 'AL009', 'type' => SUCCESS_MESSAGE));
		 // redirect('user/story_board');
	}

	function view_all_replies()
	{
		$post_data = $this->input->post();
		// debug($post_data);exit;
		$comments_data = $this->user_model->get_all_replies($post_data['story_id']);
		 // debug($comments_data); exit;
		// ob_end_flush();
		if(valid_array($comments_data))
		{

		header('Content-Type: application/json');
		echo json_encode($comments_data);
		}else
		{
			return false;
		}
		exit;
	}

	function view_all_images()
	{
		$post_data = $this->input->post();
		// debug($post_data);exit;
		$image_data = $this->user_model->get_all_images($post_data['story_id']);
		 // debug($comments_data); exit;
		// ob_end_flush();
		$data=json_decode($image_data['0']['images'],true);
		
		if(valid_array($data))
		{
			header('Content-Type: application/json');
			echo json_encode($data);
		}else
		{
			return false;
		}
		exit;
	}

	function set_viewed()
	{
		$post_data = $this->input->post();
		$data['user_id']=$user_id;
		$data['review_id']=$post_data['story_id'];
		$exist_data=$this->user_model->check_view_exist($data['user_id'],$data['review_id']);
		if($exist_data==0)
		{

			$this->custom_db->insert_record('review_views',$data);
		}
		return true;
	}

	function datalist_by_key()
	{
		$post_data = $this->input->post();
		$results=$this->user_model->datalist_by_key($post_data['key']);
		
	    if(valid_array($results))
		{
			header('Content-Type: application/json');
			echo json_encode($results);
		}else
		{
			return false;
		}
		exit;
	}

	function like_post()
	{
		$post_data = $this->input->post();
		
		 if($post_data['status']=='like')//like=1 dislike=0
		 {
		 	$data['review_id']=$post_data['review_id'];
		 	$data['user_id']=$post_data['user_id'];
		 	$like_status=$this->custom_db->insert_record('review_likes',$data);
		 	$condition = array('review_id' => $data['review_id'] );
		 	$review_user_id=$this->custom_db->single_table_records('reviews','user_id',$condition);
			 // debug($review_user_id);exit;
			if($review_user_id['status']==1)
			{
				if($review_user_id['data'][0]['user_id']!=$data['user_id'])
				{
					$notification_data['review_id']=$data['review_id'];
					$notification_data['notifier_id']=$review_user_id['data'][0]['user_id'];
					$notification_data['user_id']=$data['user_id'];
					$notification_data['notification_type']=1;
					$notification_status=$this->custom_db->insert_record('review_notifications',$notification_data);
					$this->send_notification($notification_data['user_id'],$notification_data['notifier_id'],$notification_data['notification_type']);
				}
			}
			// debug($like_status);
			if($like_status['status']==1)
			{
				$lk_status=1;
			}else{
				$lk_status=0;
			}

		 }else if($post_data['status']=='dislike'){
		 	$data['review_id']=$post_data['review_id'];
		 	$data['user_id']=$post_data['user_id'];
		 	$dislike_status=$this->custom_db->delete_record('review_likes',$data);
		 	// debug($dislike_status);
		 	if($dislike_status==1)
		 	{
		 		$lk_status=1;
		 	}else
		 	{
		 		$lk_status=0;
		 	}
		 }
		   
		 $likes_data['status']=$lk_status;
		 // $likes_data['likes_data']=$this->user_model->get_likes_list($post_data['review_id']);
		 $likes_data['data']=$this->user_model->get_stories($post_data['user_id'],0,200,0,'',$post_data['review_id']);
		  echo json_encode($likes_data);
		
	}

	function follow_friend()
	{
		// echo "hello break";exit;
		$post_data = $this->input->post();
		// debug($post_data);exit;
		$data['user_id']=$post_data['user_id'];
		$data['follower_id']=$post_data['follow_id'];
		$data['status']='0';
		$friend_status=$this->user_model->check_friend_status($data['user_id'],$data['follower_id']);
		// debug($friend_status);exit;
		if($friend_status==0)
		{
			$this->custom_db->insert_record('review_friends',$data);
			$notification_data['user_id']=$post_data['user_id'];
			$notification_data['notifier_id']=$post_data['follow_id'];
			$notification_data['notification_type']='3';
			$notification_data['view_status']='0';
			$this->custom_db->insert_record('review_notifications',$notification_data);
			$this->send_notification($notification_data['user_id'],$notification_data['notifier_id'],$notification_data['notification_type']);
			$return_data['status']='true';
			// $friend_suggestion=$this->user_model->get_friend_suggestion($post_data['user_id'],0,100);
		}else{
			$return_data['status']='false';
		}
			$friend_suggestion=$this->user_model->get_friend_suggestion($post_data['user_id'],0,100);
			$return_data['friend_suggestion']=$friend_suggestion;
			echo json_encode($return_data);
	}
		
	function unfollow_friend()
	{
		$post_data = $this->input->post();
		// debug($post_data);exit;
		$data['user_id']=$post_data['user_id'];
		$data['follower_id']=$post_data['follow_id'];
		// debug($data);exit;
		$friend_status=$this->user_model->check_friend_status($data['user_id'],$data['follower_id']);
		// debug($friend_status);exit;
		if($friend_status==1)
		{
			$return = $this->custom_db->delete_record('review_friends',$data);
			// echo $this->db->last_query();
			$data['follower_id']=$post_data['user_id'];
			$data['user_id']=$post_data['follow_id'];
			$return = $this->custom_db->delete_record('review_friends',$data);
			// echo $this->db->last_query();

			$notification_data['user_id']=$post_data['user_id'];
			$notification_data['notifier_id']=$post_data['follow_id'];
			$return = $this->custom_db->delete_record('review_notifications',$notification_data);
			// echo $this->db->last_query();

			$notification_data['notifier_id']=$post_data['user_id'];
			$notification_data['user_id']=$post_data['follow_id'];
			$return = $this->custom_db->delete_record('review_notifications',$notification_data);
			// echo $this->db->last_query();
			// exit;
			$return_data['status']='true';
			
		}else{
			$return_data['status']='false';
		}
		// debug($return);exit;
		$friend_suggestion=$this->user_model->get_friend_list($post_data['user_id'],0,100);
		$return_data['friend_list']=$friend_suggestion;
		echo json_encode($return_data);
	}

	function get_notifications()
	{
		$user_id=$this->input->post('user_id');
		// debug($user_id);
		$notification_data=$this->user_model->get_notifications($user_id);
		$return_data['status']=true;
		$return_data['notification_data']=$notification_data;
		echo json_encode($return_data);exit;
		// return $notification_data;
	}
	function accept_follow()
	{
		$post_data = $this->input->post();
		// debug($post_data);exit;
		$accept_follow=$this->user_model->accept_follow($post_data['notifier_id'],$post_data['user_id']);
		$this->send_notification($post_data['user_id'],$post_data['notifier_id'],4);
		$friend_suggestion['friend_suggestion']=$this->user_model->get_friend_suggestion($post_data['user_id'],0,100);
		echo json_encode($friend_suggestion);
	}

	function ignore_follow()
	{
		$post_data = $this->input->post();
		$ignore_follow=$this->user_model->ignore_follow($post_data['notifier_id'],$post_data['user_id']);
		$friend_suggestion['friend_suggestion']=$this->user_model->get_friend_suggestion($post_data['user_id'],0,100);
		echo json_encode($friend_suggestion);
	}	
	
	function remove_friend()
		{
			$post_data = $this->input->post();
			$ignore_follow=$this->user_model->ignore_follow($post_data['notifier_id'],$post_data['user_id']);
			$friend_list['friend_list']=$this->user_model->get_friend_list($post_data['user_id'],0,100);
			echo json_encode($friend_list);
		}	

	function get_more_stories()
	{
		$post_data = $this->input->post();
		if(isset($post_data['user_id']))
		{
			$new_stories = $this->user_model->get_stories($post_data['user_id'],$post_data['start_from']);
		}else{
			$new_stories = $this->user_model->get_stories('',$post_data['start_from']);
		}
		$new_story_html_template ='';
		foreach ($new_stories as $key => $value) {
			$new_story_html_template .= $this->template->isolated_view('user/user_posts', array('new_stories'=>$new_stories[$key]));

		}
		$new_story_html_template .= '<script type="text/javascript" src="<?= base_url() ?>extras/system/template_list/template_v3/javascript/html5gallery.js"></script>';
		// debug($new_story_html_template);exit;
		echo get_json_compressed_data(array('data' => $new_story_html_template));
	}

	function delete_data()
	{
		$post_data = $this->input->post();
		$id=$post_data['id'];
		$post=$post_data['post_type'];
		$delete_status=0;
		$return_data='';
		if($post=='post')//delete post
		{
			$data['review_id']=$id;
		 	$delete_status=$this->custom_db->delete_record('reviews',$data);
		 	// $this->session->set_flashdata(array('message' => 'AL011', 'type' => SUCCESS_MESSAGE));

		}else if($post=='comment')//delete_comment
		{
			$data['comment_id']=$id;
			$post_id=$this->user_model->get_post_id_on_comment_id($data['comment_id']);
			// debug($post_id);exit;
			if(isset($post_id) && !empty($post_id))
			{

		 		$delete_status=$this->custom_db->delete_record('review_comments',$data);
			}
		 	// echo $this->db->last_query();exit;
		 	// debug($delete_status);exit;
		 	// $this->session->set_flashdata(array('message' => 'AL010', 'type' => SUCCESS_MESSAGE));
		}
		// debug($delete_status);exit;
		if($delete_status==1)
		{
			$return_data['status']="true";
		 	$return_data['data']=$this->user_model->get_stories('',0,200,0,'',$post_id);


		}else
		{
			$return_data['status']="false";
		}
		echo json_encode($return_data);exit;
			
	}
	function edit_comment()
	{
		$post_data=$this->input->post();

		$data['comment']=$post_data['comment_area'];
		$condition['review_id']=$post_data['story_id'];
		$condition['comment_id']=$post_data['comment_id'];
		$comment_status=$this->custom_db->update_record('review_comments',$data,$condition);
		if($comment_status==1)
		{
			$return_data['status']="true";
			$return_data['data']=$this->user_model->get_stories('',0,200,0,'',$post_data['story_id']);
		}else{
			$return_data['status']="false";
		}
		echo json_encode($return_data);exit;
	}

	function get_story_on_story_id($review_id='',$type=1)
	{
		if(isset($review_id) && $review_id!='')
		{
			$review_id=$review_id;
		}else{
			$post_data=$this->input->post();
			$review_id=$post_data['review_id'];
		}
		// $post_data['review_id']=70;
		// $condition = array('review_id' =>  $review_id);
		// debug($review_id);exit;
	 	$story_data=$this->user_model->get_stories('',0,200,0,'', $review_id);
	 	// echo $this->db->last_query();exit;
	 	// debug($story_data); exit;
	 	// $remarks=strip_tags($story_data['data'][0]['remarks']);
	 	// $story_data['data'][0]['remarks']=$remarks;

	 	if($type==1)
	 	{
	 		header('Content-Type: application/json');

	 		echo json_encode($story_data['data'][0]);
	 	}else{
	 		return $story_data;
	 	}
	}

	function edit_story()
	{
		$post_data=$this->input->post();
		// debug($post_data);exit;
		$hash= explode(' ', $post_data['remarks']);
		$i=0;
		$hash_variable=[];
		foreach ($hash as $key => $value) {
			if (preg_match("/[#]/", $hash[$key], $match))
			{
				$hash_variable[$i]=$hash[$key];
				$i+=1;
				$hash_text=explode('#',$hash[$key]);
				$hash[$key]='<a id="hash_id" href="'. base_url().'index.php/user/story_board/home/'.$hash_text['1'].'">'.$hash[$key].'</a>';
				
			}
		}
		$story_data=$this->get_story_on_story_id($post_data['review_id'],0);
		// debug($story_data);
		$img=json_decode($story_data[0]['images'],true);
		// debug($img);
		// debug($_FILES);
		$files_data=$this->format_files_data_edit($_FILES);
		// debug($files_data);exit;
		$_FILES=$files_data;
		if (valid_array($_FILES)) 
		{
			foreach ($_FILES as $f_key => $f_value) {
				if(isset($_FILES[$f_key]) && !empty($_FILES[$f_key]['name'][0]))
				{
					if(isset($removed_images ))
							{
								foreach ($removed_images as $r_k => $r_v) {
									foreach ($_FILES[$f_key]['name'] as $f_r_k => $f_r_v) 
									{
										if($_FILES[$f_key]['name'][$f_r_k]==$removed_images[$r_k])
										{
											unset($_FILES[$f_key]['name'][$f_r_k]);
											unset($_FILES[$f_key]['type'][$f_r_k]);
											unset($_FILES[$f_key]['tmp_name'][$f_r_k]);
											unset($_FILES[$f_key]['error'][$f_r_k]);
											unset($_FILES[$f_key]['size'][$f_r_k]);
										}
									}
								}
							}
					// debug($_FILES);exit;
					foreach ($_FILES[$f_key]['name'] as $key => $value) 
					{

						if(isset($_FILES[$f_key]['name'][$key]) && !empty($_FILES[$f_key]['name'][$key]) )
						{

							$files['name'][$key]	= $_FILES[$f_key]['name'][$key];
							$files['type'][$key]	= $_FILES[$f_key]['type'][$key];
							$files['tmp_name'][$key]	= $_FILES[$f_key]['tmp_name'][$key];
							$files['error'][$key]	= $_FILES[$f_key]['error'][$key];
							$files['size'][$key]	= $_FILES[$f_key]['size'][$key];
							
						}			
					}

					// debug($files);
					$images=$this->upload_images($files,$post_data);
					// debug($images);exit;
					foreach ($images as $push_key => $push_value) {
						// debug($img);debug($images[$push_key]);
						if($img==null)
						{
							$img[]=$images[$push_key];
						}else{

							array_push($img, $images[$push_key]);
						}
						// debug($img);exit;	
					}
				}
				
			}

		}
		// debug($img);exit;
		$del_data['post_id']=$post_data['review_id'];
	 	$dislike_status=$this->custom_db->delete_record('review_tags',$del_data);
	 	
	 	// debug($post_data);exit;
	 	$post_data['tageduser']=json_decode($post_data['tageduser'],true);
		$inser_id=$this->insert_tagged_friends($post_data['review_id'],$post_data['tageduser'],$post_data['user_id']);
			// debug($inser_id);exit;

		$post_data['remarks']=implode(" ",$hash);
		// debug($img);exit;
		$data['images']=json_encode($img);
		// debug($data['images']);exit;		
		$post_data['hashtags']=json_encode($hash_variable);
		$data['title']=$post_data['title'];
		$data['remarks']=$post_data['remarks'];
		$data['hashtags']=$post_data['hashtags'];
		$condition['review_id']=$post_data['review_id'];
		// debug($condition);exit;
		$update_status=$this->custom_db->update_record('reviews',$data,$condition);
		// echo $this->db->last_query();exit;
		// debug($update_status);exit;
		if($update_status==1)
		{
			$return_data['status']="true";
			$return_data['data']=$this->user_model->get_stories($post_data['user_id'],0,200,0);
		}else{
			$return_data['status']="false";
		}
		echo json_encode($return_data); exit;
	}

	function get_stories_on_hash()
	{
		$hasht=$GLOBALS['CI']->uri->segment(3);
		debug($hasht);
	}

	function remove_image()
	{
		$post_data=$this->input->post();
		$review_id=$post_data['review_id'];
		$image_key=$post_data['image_key'];
		// exit("hello");

		$image_data=$this->user_model->get_images($review_id);
		// debug($image_data);exit;
		$image_list=json_decode($image_data['images'],true);
		
		foreach ($image_list as $key => $value) {
			if($key!=$image_key)
			{
				$updated_image_list[]=$image_list[$key];
			}
		}
		// debug($updated_image_list);
		$condition['review_id']=$review_id;
		$data['images']=json_encode($updated_image_list);
		$update_id= $this->custom_db->update_record('reviews',$data,$condition);
		if($update_id==1)
		{
			echo "true";
		}else{
			echo "false";
		}exit;
		// return $update_id;
	}

	function get_likes_list()
	{
		$post_data=$this->input->post();
		$review_id=$post_data['review_id'];
		// $review_id='84';
		$likes_list=$this->user_model->get_likes_list($review_id);
		// debug($likes_list);
		// $template='';
		// foreach ($likes_list as $key => $value) {
		// 	$name=($key+1).'.'.$likes_list[$key]['first_name'].$likes_list[0][$key]['last_name'];
		// 	// $template.='<a href='.base_url().'index.php/user/profile/profile_stry/'.$likes_list[$key]['user_id'].'>'.$name.'</a><br/>';
		// 	$template.=$name;
		// }

		echo json_encode($likes_list);
	}

	function story_board()
	{	

		$user_id=$this->input->post('user_id');
		$tab=$this->input->post('tab');
		$post_id=$this->input->post('post_id');
		$hashtag=$this->input->post('hashtag');
		$profile_user_id=$this->input->post('profile_user_id');
		// debug($post_id);exit;
		if($user_id!=''){
					$user_data= $this->user_model->get_user_data($user_id);
					$page_data['title'] = '';
					$page_data['first_name'] = ucfirst($user_data[0]['first_name']);
					$page_data['last_name'] = ucfirst($user_data[0]['last_name']);
					$page_data['full_name'] = ucfirst($user_data[0]['first_name']).' '.ucfirst($user_data[0]['last_name']);
					$page_data['user_country_code'] = $user_data[0]['country_code'];
					$page_data['date_of_birth'] = date('d-m-Y', strtotime($user_data[0]['date_of_birth']));
					$page_data['address'] = $user_data[0]['address'];
					$page_data['phone'] = $user_data[0]['phone'];
					$page_data['email'] = $user_data[0]['email'];
					$page_data['email_prof'] = $user_data[0]['email'];
					$page_data['profile_image'] = $user_data[0]['image'];
					$page_data['signature'] = $user_data[0]['signature'];
					$page_data['user_policies'] = $user_data[0]['policies'];
					
					$page_data['user_id_profile'] = $user_id;
					
				
					
				}
				$friends_list=[];
				// debug($tab);exit;
				if($tab=='home' && $user_id!='' && $post_id=='' && $hashtag=='')
				{
					$story_data=$this->user_model->get_stories($user_id,0,200,0);
					$page_data['stories']=$story_data;
					$page_data['friends_list']=$this->user_model->get_friend_list($user_id,0,100);


				}else if($tab=='profile_stry' && isset($profile_user_id)){
					$profile_story_count=$this->user_model->get_stories($user_id,0,20,1,'','',1,$profile_user_id);
					$profile_story_data=$this->user_model->get_stories($user_id,0,20,'','','',1,$profile_user_id);
					$friends_count=$this->user_model->get_friend_list($profile_user_id,0,100,1);
					$page_data['profile_stories']=$profile_story_data;
					$page_data['profile_stories_count']=$profile_story_count[0]['story_count'];
					$page_data['policies']=$this->user_model->get_policies();
					$page_data['policies']=$this->user_model->get_policies(1);
				}else if($tab=='friends_list' && $user_id!='')
				{
					// debug($user_id);exit;
					$friends_count=$this->user_model->get_friend_list($user_id,0,10,1);
					$friends_list=$this->user_model->get_friend_list($user_id,0,100);
					// debug($friends_list);exit;
					$page_data['friends_list']=$friends_list;
					$page_data['friends_count']=$friends_count;
				}else if($tab=='friend_suggestion')
				{
					$friend_suggestion=$this->user_model->get_friend_suggestion($user_id,0,100);
					$page_data['friend_suggestion']=$friend_suggestion;

				}else if($tab=='notifications')
				{
					$notification_data=$this->user_model->get_notifications($user_id);
					$page_data['notification_data']=$notification_data;
				}else if($tab=='home' && $user_id!='' && $post_id!='')
				{
					// debug($post_data);exit;
					$story_data=$this->get_story_on_story_id($post_id,0);
					// debug($story_data);exit;
					$page_data['stories']=$story_data;
					// $page_data['friends_list']=$this->user_model->get_friend_list($user_id,0,100);
				}else if($tab=='home' && $user_id=='' && $post_id!='' && $hashtag=='')
				{
					// debug($post_id);exit;

					$story_data=$this->get_story_on_story_id($post_id,0);
					// debug($story_data);exit;
					$page_data['stories']=$story_data;
					// $page_data['friends_list']=$this->user_model->get_friend_list($user_id,0,100);
				}else if($tab=='home' && $hashtag!='' && $user_id!='' && $post_id=='')
				{
					// debug($hashtag);exit;

					$story_data=$this->user_model->get_stories($user_id,0,20,0,$hashtag,'',1);
					// debug($story_data);exit;
					$page_data['stories']=$story_data;
					// $page_data['friends_list']=$this->user_model->get_friend_list($user_id,0,100);
				}
				// debug($friends_list);exit;	
				// foreach ($page_data['latest_transaction'] as $i => $value) {

				// 	$locations[$i]['lat']= $page_data['latest_transaction'][$i]['from_lat'];
		  //   		$locations[$i]['lng']= $page_data['latest_transaction'][$i]['from_lon'];
		  //   		$locations[$i]['title']= $page_data['latest_transaction'][$i]['from_city'];
		  //   		$locations[$i]['target'][0]['lat']= $page_data['latest_transaction'][$i]['to_lat'];
				//     $locations[$i]['target'][0]['lng']= $page_data['latest_transaction'][$i]['to_lon'];
				//     $locations[$i]['target'][0]['title']= $page_data['latest_transaction'][$i]['to_city'];
		  //   			$k=1;

				// 	foreach ($page_data['latest_transaction'] as $j => $value) {
				// 		if(!($i==$j)){
				//         	if($page_data['latest_transaction'][$i]['from_lat']==$page_data['latest_transaction'][$j]['from_lat'])
				//         	{
				//         		$locations[$i]['target'][$k]['lat']= $page_data['latest_transaction'][$j]['to_lat'];
				//         		$locations[$i]['target'][$k]['lng']= $page_data['latest_transaction'][$j]['to_lon'];
				//         		$k+=1;
				//         	}
			 //        	}
						
				// 	}
				// }
			// $page_data['country_code'] = $this->db_cache_api->get_country_code_list();
			// $page_data['locations']=json_encode($locations);
			
			$this->user_model->update_ts($user_id);
			 // debug($page_data);exit;

			echo json_encode($page_data);exit;			
		
	}
	function update_hits()
	{
		$post_data=$this->input->post();
		$hit_data=$this->user_model->get_hit_count($post_data['review_id']);
		// debug($hit_data);exit;
		$hit_count=$hit_data[0]['hits']+1;
		$data['ad_hits']=$hit_count;
		$condition = array('review_id' => $post_data['review_id']);
		$this->custom_db->update_record('reviews',$data,$condition);
	}

	function set_profile_pic()
	{
		$user_id=$this->input->post('user_id');
		$image=$this->input->post('image');
		$data['image']=$image;
		// debug($data);exit;
		$condition = array('user_id' => $user_id );
		// debug($data);debug($condition);
		$update=$this->custom_db->update_record('user',$data,$condition);
		// debug($update);exit;
		if($update==1)
		{
			echo "true";
		}else{
			echo "false";
		}exit;
		// redirect($_SERVER['HTTP_REFERER']);
	}

	function get_user_images($user_id=0)
	{
		$post_data=$this->input->post();
		// debug($post_data);exit;
		$user_id=$user_id;

		if($post_data!='' && valid_array($post_data))
		{
			$user_id=$post_data['id'];
		}
		
		// debug($user_id);exit;
		$temp_data=[];
		$upload_data=$this->user_model->get_user_uploads($user_id);
		if(valid_array($upload_data) && !empty($upload_data))
		{

			foreach ($upload_data as $u_key => $u_value) {
				// debug($upload_data[$u_key]['images']);exit;
				$img_data=json_decode($upload_data[$u_key]['images'],true);
				// debug($img_data);exit;
				foreach ($img_data as $i_key => $i_value) {
				
					array_push($temp_data,$img_data[$i_key]);
				}
			}
			$upload_datas['status']=1;
			$upload_datas['data']=$temp_data;
		}else{
			$upload_datas['status']=0;
			$upload_datas['data']='';
		}
		// debug($temp_data);exit;
		// echo $this->db->last_query();exit;
		// debug($upload_data);
		echo json_encode($upload_datas);

	}

	function cancel_friend_request()
	{
		$post_data=$this->input->post();
		$follower_id=$post_data['follower_id'];
		// debug($follower_id);
		$user_id=$post_data['user_id'];

		$cancelled_data=$this->user_model->cancel_friend_request($user_id,$follower_id);
		if($cancelled_data==1)
		{
			$friend_suggestion=$this->user_model->get_friend_suggestion($user_id,0,100);
			$return_data['status']=true;
			$return_data['friend_suggestion']=$friend_suggestion;
		}
		echo json_encode($return_data);
		// debug($cancelled_data);exit;
	}


	function stories_for_no_login()
	{
		$story_data=$this->user_model->get_stories();
		$page_data['stories']=$story_data;
		echo json_encode($page_data);exit;
	}

	function insert_tagged_friends($review_id,$friends_list,$user_id)
	{
		foreach ($friends_list as $f_key => $f_value) {
			$data['post_id']=$review_id;
			$data['tag_friend_id '] = $friends_list[$f_key];
			$this->custom_db->insert_record('review_tags', $data);
			$not_data['review_id']=$review_id;
			$not_data['notifier_id']=$friends_list[$f_key];
			$not_data['user_id']=$user_id;
			$not_data['notification_type']=5;//tagged_in
			$this->custom_db->insert_record('review_notifications', $not_data);
			$this->send_notification($not_data['user_id'],$not_data['notifier_id'],$not_data['notification_type']);

		}
	}

	function report_post(){

		$post_data=$this->input->post();
		$data['remarks']=$post_data['remarks'];
		$data['review_id']=$post_data['review_id'];
		$data['date'] = date("Y-m-d h:i:s");
		$data['complained_user_id'] = $post_data['user_id'];

		$insert_status=$this->custom_db->insert_record('abusive_reviews', $data);
		// redirect('user/story_board');
		if($insert_status)
		{
			$return_data['status']='true';
			$return_data['data']=$this->user_model->get_stories($user_id,0,20);
		}else{
			$return_data['status']='false';	
		}
		echo json_encode($return_data);exit;
	}

	function cms()
	{
		$slno=$this->input->post('id');
		$page_position='Bottom';
		if(isset($slno)) {
			$data = $this->custom_db->single_table_records('cms_pages','page_title,page_description,page_seo_title,page_seo_keyword,page_seo_description', array('page_id'=>$slno, 'page_position' => $page_position, 'page_status' => 1));
			$return_data['status']='true';
			$return_data['data']=$data;
			echo json_encode($return_data);
			
		} else {
			$return_data['status']='false';
			$return_data['data']="Invalid Input";
			echo json_encode($return_data);
		}
	}

	function format_img_data()
	{
		$img_data='["1531476099_hotel.jpg","1531476099_VID-20180629-WA0014.mp4","1531476099_VID-20180709-WA0006.mp4"]';
		$thumbnail='{"1531476099_VID-20180629-WA0014.mp4":["1531476099_hotel.jpg"],"1531476099_VID-20180709-WA0006.mp4":["1531476099_holiday-inn-hotel-and-suites-oakland-2533422671-4x3.jpeg"]}';
		$this->user_model->format_img_data($img_data,$thumbnail);
	}

	function sendFCM($mess='',$id='') {
		if($mess!='' && $id!='')
		{

			$url = 'https://fcm.googleapis.com/fcm/send';
			$fields = array (
			        'to' => $id,
			        'notification' => array (
			                "body" => $mess,
			                "title" => "Title"
			        )
			);
			$fields = json_encode ( $fields );
			$headers = array (
			        'Authorization: key=AAAA01tSD8o:APA91bGMJKS9SEeMRzaym2Glir1drr9Ce5uNJ85A6nSoKWIvYiumWUNAotcN9hwOwdN5VEVzZV0woPXIfCa7O9clNLfl71Oaw185qKTHyVFmfJJwNDry-cPqwGtsFPKN_gvbr1sUL1gi',
			        'Content-Type: application/json'
			);

			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_POST, true );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

			$result = curl_exec ( $ch );
			curl_close ( $ch );
			// debug($result);exit;
			return $result;
		}else{
			return false;
		}
	}

	function send_notification($user_id,$notifier_id,$type)
	{
		// $notifier_id=500;
		$notifier_data=$this->user_model->user_data($notifier_id);
		$user_data=$this->user_model->user_data($user_id);
		// debug($user_data);exit;
		$message='';
		switch ($type) {
			case '1':
				$message.=$user_data['first_name'].' '.$user_data['last_name'].' liked your post';
				break;
			case '2':
				$message.=$user_data['first_name'].' '.$user_data['last_name'].' Commented on your post';
				break;
			case '3':
				$message.=$user_data['first_name'].' '.$user_data['last_name'].' wants to follow you';
				break;
			case '4':
				$message.=$user_data['first_name'].' '.$user_data['last_name'].' has accepted your follow request';
				break;
			case '5':
				$message.=$user_data['first_name'].' '.$user_data['last_name'].' has Tagged you';
				break;
			case '6':
				$message.=$user_data['first_name'].' '.$user_data['last_name'].' Shared your post';
				break;
			
			default:
				
				break;
		}
		// debug($message);exit;
		$device_id=$notifier_data['device_id'];
		$this->sendFCM($message,$device_id);
		return true;
		// debug($user_data);
		// debug($notifier_data);exit;

	}

	// $name = ''; $type = ''; $size = ''; $error = '';
   function compress_image($source_url, $destination_url, $quality) {

      $info = getimagesize($source_url);

          if ($info['mime'] == 'image/jpeg')
          {
          	// echo "hello";exit;
          	// debug($source_url);exit;
          $image = imagecreatefromjpeg($source_url);
          }

          elseif ($info['mime'] == 'image/gif')
          $image = imagecreatefromgif($source_url);

          elseif ($info['mime'] == 'image/png')
          $image = imagecreatefrompng($source_url);
      	// debug($image);exit;
      	// debug($image);debug($destination_url);debug($quality);exit;
         $response= imagejpeg($image, $destination_url, $quality);
         // echo "hello";debug($response);exit;
          return $destination_url;
        }

    function add_wallet()
	{
		error_reporting(E_ALL);
		//FIXME:Make Codeigniter Validations -- Balu A
		$post_data = $this->input->post();
		//debug($post_data);exit;
		$this->entity_user_id=$post_data['user_id'];
		if(valid_array($post_data) == true && isset($post_data['user_id']) == true && empty($post_data['user_id']) == false)
		{
			if(valid_array($post_data) == true && isset($post_data['amount_wallet']) == true && empty($post_data['amount_wallet']) == false)
			{
				$payment_gateway_details = array();
				$payment_gateway_details['amount'] = trim($post_data['amount_wallet']);
				
				//$payment_gateway_details['domain_origin'] = $this->entity_user_id;
				$payment_gateway_details['booking_ip'] = $_SERVER['REMOTE_ADDR'];
				$payment_gateway_details['created_datetime'] = date('Y-m-d H:i:s');
				//$payment_gateway_details['book_id'] = 'DEP-'.$this->entity_user_id.time();

						$amount = trim($post_data['amount_wallet']);
				
				;

				$book_id = 'DEP-'.$this->entity_user_id.time();
				$temp_record  =  $this->custom_db->single_table_records ( 'user', '*', array (
							'user_id' => $post_data['user_id']
					) );

					$this->load->model('transaction');
					
				$firstname=$temp_record['data'][0]['first_name'];
				$email=provab_decrypt($temp_record['data'][0]['email']);
				$phone=$temp_record['data'][0]['phone'];
				$productinfo=WALLET_PAYMENT;
				// debug($email);exit;
				$payment_gateway_details['domain_list_fk'] = get_domain_auth_id();
					// $this->custom_db->insert_record('wallet_transaction_log', $payment_gateway_details);
				$wallet_data=$this->user_model->get_wallet_log($this->entity_user_id);
				if(valid_array($wallet_data) && !empty($wallet_data) && isset($wallet_data)){
					$opening_balance=$wallet_data['0']['closing_balance'];
				}else{
					$opening_balance=0;
				}
				$this->user_model->save_wallet_log($this->entity_user_id,$book_id,$amount,true,$opening_balance);

						$this->transaction->create_payment_record($book_id, $amount, $firstname, $email, $phone, $productinfo,0,0,1);
						$url=explode('/mobile_webservices', base_url());
						$payment_gateway_url = $url[0].'/index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;

						$pre_booking_status['data'] = array('app_reference'=>$book_id,'temp_book_origin'=>$book_origin,'return_url'=>$payment_gateway_url);
						$pre_booking_status['status']   = SUCCESS_STATUS;
						$this->output_compressed_data($pre_booking_status);
						exit;
						
			}else{
				$pre_booking_status['status']   = FAILURE_STATUS;
				$pre_booking_status['message']   = 'Invalid amount';
				$this->output_compressed_data($pre_booking_status);
				exit();
			}
			
		}else{
			$pre_booking_status['status']   = FAILURE_STATUS;
			$pre_booking_status['message']   = 'Please send User id';
			$this->output_compressed_data($pre_booking_status);
			exit();
		}
	}

	private function output_compressed_data($data)
	{
		while (ob_get_level() > 0) { ob_end_clean() ; }
		ob_start("ob_gzhandler");
		header('Content-type:application/json');
		echo json_encode($data);
		ob_end_flush();
		exit;
	}
	
}
