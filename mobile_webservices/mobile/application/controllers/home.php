<?php
ob_start();if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (session_status() == PHP_SESSION_NONE) { session_start(); }
//ini_set("display_errors",1);
error_reporting(0);
class Home extends CI_Controller {
	
	//DO : Setting Current website url in session, Purpose : For keeping the page on login/logout.
	public function __construct(){
		parent::__construct();
		$current_url 	= $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
        $current_url 	= WEB_URL.$this->uri->uri_string(). $current_url;
		$url 			=  array('continue' => $current_url);
        $this->session->set_userdata($url);
		$this->load->model('home_model');
		$this->load->model('General_Model');
		if(!isset($_SESSION['ses_id'])){
			$sec_res = session_id();
	    	$_SESSION['ses_id'] = $sec_res;
		}
		// debug($_SESSION);exit;
		$current_url 	= $this->config->site_url().$this->uri->uri_string(). ($_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '');
        $url 			=  array('continue' => $current_url);
		$this->session->set_userdata($url);
		//No such file or directory
		$URIS = explode('/',$this->uri->uri_string());
		// debug($_SESSION['Flymein']['language_code']);
		// debug($URIS);exit;
        if($URIS[0] != $_SESSION['Flymein']['language_code']){
            $_SESSION['Flymein']['language_code'] = $URIS[0];
            if($URIS[0] == 'ar'){
                $_SESSION['Flymein']['language'] = 'arabic';
                $_SESSION['Flymein']['language_code'] = 'ar';
            }
            elseif($URIS[0] == 'en'){
                $_SESSION['Flymein']['language'] = 'english';
                $_SESSION['Flymein']['language_code'] = 'en';
            } 
           } 
     
		if($_SESSION['Flymein']['language'] == "arabic"){ 
			$this->lang->load('arab','Dynamic_Languages');
		}else{
			$this->lang->load('english','Dynamic_Languages');
		} 
        $this->Flymein = $this->lang->line('Flymein');
        // echo "<pre/>";print_r($this->Al_Shualah);exit;
	}
	
	public function index($type=''){

		$data['top_destionation']	=	$this->home_model->get_top_destionation();
		//$data['top_deals']			=	$this->home_model->get_top_deals();
		$data['top_deals']			=	$this->home_model->get_promo_deals();

		$data['top_flightdeals']	=	$this->home_model->get_flight_deals();

		$data['top_airliners'] = $this->home_model->get_topairliners();

		$data['tour_style'] = $this->home_model->get_tourstyle();

$banners = $this->home_model->home_banners();
// debug($banners);exit;
		#echo '<script>if(confirm("Do you want to cancel?")){ alert("yes")}else{ alert("no")}</script>';
		// echo "ela";
		// exit;
		if($type!=''){

			$data['header_product']=@$type;
			
		}	
		// debug($data);
		// exit;

		$this->load->view(PROJECT_THEME.'/index', $data);	
    }
    
    //~ public function create_pagination(){
		//~ $this->load->library('pagination');
		//~ $config['base_url'] = base_url().'home/create_pagination';
		//~ $config['total_rows'] = 200;
		//~ $config['per_page'] = 20;
		//~ $this->pagination->initialize($config);
		//~ echo $this->pagination->create_links();
	//~ }
    
	public function change_currency(){
		$code = $this->input->post('code');
		$icon = $this->input->post('icon');
		if($this->input->cookie('currency')){
            $cookie = array( 'name'   => 'currency', 'value'  => $code, 'expire' => '86500');
            $this->input->set_cookie($cookie);
            $cookie = array( 'name'   => 'icon', 'value'  => $icon, 'expire' => '86500');
			$this->input->set_cookie($cookie);
			$this->display_currency = $code;
			$this->display_icon = $icon;
        }else{
        	$cookie = array( 'name'   => 'currency', 'value'  => BASE_CURRENCY, 'expire' => '86500');
            $this->input->set_cookie($cookie);
            $cookie = array( 'name'   => 'icon','value'  => BASE_CURRENCY_ICON, 'expire' => '86500');
            $this->input->set_cookie($cookie);
            $this->display_currency = BASE_CURRENCY;
            $this->display_icon = BASE_CURRENCY_ICON;
        }
        $this->curr_val = $this->general_model->get_curr_val($this->display_currency);
        $response = array('status' => 1);
        echo json_encode($response);
	}

	public function get_hotel_cities(){
		ini_set('memory_limit', '-1');
        $term = $this->input->get('term'); //retrieve the search term that autocomplete sends
        $term = trim(strip_tags($term));
        $cities = $this->home_model->get_hotel_cities_list($term)->result();
        $result = array();
        foreach ($cities as $city) {
            $apts['label'] = $city->city;
            $apts['value'] = $city->city;
            $apts['id'] = $city->id;
            $result[] = $apts;
        }
        echo json_encode($result);
	}
	
	/*  16-03-2016 */
	// For Inner pages header functionality
	public function inner_header($product){
		
		

		$data['header_product'] 	= 	$product;
		$data['top_destionation']	=	$this->home_model->get_top_destionation();
		//$data['top_deals']			=	$this->home_model->get_top_deals();
		;

		$data['top_flightdeals']	=	$this->home_model->get_flight_deals();

		$data['top_deals']			=	$this->home_model->get_promo_deals();

		$data['top_flightdeals']	=	$this->home_model->get_flight_deals();
		$data['tour_style'] = $this->home_model->get_tourstyle();
		$data['top_airliners'] = $this->home_model->get_topairliners();


		
		$this->load->view(PROJECT_THEME.'/index', $data);	
	}


	public function get_top_destination()
	{
		$data['top_destionation']=$this->home_model->get_top_destionation();
		$this->load->view(PROJECT_THEME.'/new_theme/top_destination'); 

	}


	public function topsearch($flight_id)
	{
		$data      				=	$this->home_model->get_flight_dealsbyid($flight_id);
		$adult					=1;
		 $today_date			=date("Y-m-d");		
		 $from					=$data[0]->deal_from_place;
		 $to					=$data[0]->deal_to_place;
		 $type					=$type 	= 'type=oneway';   
		 $child				    =0;
		 $infant				=0;
		 $class                 ="All";
		 $date = date("Y-m-d");// current date
         $date1 = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . " +1 day"));
   
	$query 	= $type.'&origin='.substr(chop(substr($from, -5), ')'), -3).'&destination='.substr(chop(substr($to, -5), ')'), -3).'&depart_date='.$date1.'&ADT='.$adult.'&CHD='.$child.'&INF='.$infant.'&class='.$class ;
	redirect(WEB_URL.'flight/?'.$query);  
	}


	public function tophotelsearch($last_deal_id){
	$data =	$this->home_model->get_last_dealsbyid($last_deal_id);
	$city1= explode('(', $data[0]->city);
	$city=str_replace(" ","+",$city1[0]);
	$date = date("Y-m-d");// current date
	$hotel_checkin =  $date1 = date("d-m-Y",strtotime(date("d-m-Y", strtotime($date)) . " +1 day"));
	$hotel_checkout=  $date2 = date("d-m-Y",strtotime(date("d-m-Y", strtotime($date1)) . " +1 day"));
	$rooms=1;
	$adult=1; 
	$child=0; 
	//$query 	= 'city='.$city.'&hotel_checkin='.$hotel_checkin.'&hotel_checkout='.$hotel_checkout.'&adult='.$adult.'&child='.$child ;
	$query='city='.$city.'&hotel_checkin='.$hotel_checkin.'&hotel_checkout='.$hotel_checkout.'&rooms='."1".'&adult[0]='."1".'&child='."0";
redirect(WEB_URL.'hotel/search?'.$query);  
}


	function page($id){		
		$data['content'] = $this->home_model->get_page_content($id);
		$data['top_airliners'] = $this->home_model->get_topairliners();

		//echo 'hi'; print_r($data['content']); exit();
		$this->load->view(PROJECT_THEME.'/pages/pages',$data);
	}

	
}

?>
