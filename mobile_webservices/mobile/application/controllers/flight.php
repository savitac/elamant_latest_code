<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 *
 * @package Provab
 * @subpackage Flight
 * @author Arjun J<arjunjgowda260389@gmail.com>
 * @version V1
 */
class Flight extends CI_Controller
{
    private $current_module;
    public function __construct()
    {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);
        $this->load->model('flight_model');
        $this->load->library('session');
        $this->load->model('user_model'); // we need to load user model to access provab sms library
        $this->load->library('provab_sms'); // we need this provab_sms library to send sms.
        $this->load->library('social_network/facebook'); //Facebook Library to enable share button
        $this->current_module = $this->config->item('current_module');

        $this->load->library('session');
    }
    /**
     * App Validation and reset of data
     */
    public function pre_calendar_fare_search()
    {
        $params           = $this->input->get();
        $safe_search_data = $this->flight_model->calendar_safe_search_data($params);
        //Need to check if its domestic travel
        $from_loc                                       = $safe_search_data['from_loc'];
        $to_loc                                         = $safe_search_data['to_loc'];
        $safe_search_data['is_domestic_one_way_flight'] = false;

        $safe_search_data['is_domestic_one_way_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);
        if ($safe_search_data['is_domestic_one_way_flight'] == false) {
            $page_params['from'] = '';
            $page_params['to']   = '';
        } else {
            $page_params['from'] = $safe_search_data['from'];
            $page_params['to']   = $safe_search_data['to'];
        }

        $page_params['depature'] = $safe_search_data['depature'];
        $page_params['carrier']  = $safe_search_data['carrier'];
        $page_params['adult']    = $safe_search_data['adult'];
        redirect(base_url() . 'index.php/flight/calendar_fare?' . http_build_query($page_params));
    }

    /**
     * Airfare calendar
     */
    public function calendar_fare()
    {
        $params                = $this->input->get();
        $active_booking_source = $this->flight_model->active_booking_source();
        if (valid_array($active_booking_source) == true) {
            $safe_search_data = $this->flight_model->calendar_safe_search_data($params);
            $page_params      = array(
                'flight_search_params'  => $safe_search_data,
                'active_booking_source' => $active_booking_source,
            );
            $page_params['from_currency'] = get_application_default_currency();
            $page_params['to_currency']   = get_application_currency_preference();
            $this->template->view('flight/calendar_fare_result', $page_params);
        }
    }
    /**
     * Jaganaath
     */
    public function add_days_todate()
    {
        $get_data = $this->input->get();
        if (isset($get_data['search_id']) == true && intval($get_data['search_id']) > 0 && isset($get_data['new_date']) == true && empty($get_data['new_date']) == false) {
            $search_id        = intval($get_data['search_id']);
            $new_date         = trim($get_data['new_date']);
            $safe_search_data = $this->flight_model->get_safe_search_data($search_id);

            $day_diff = get_date_difference($safe_search_data['data']['depature'], $new_date);
            if (valid_array($safe_search_data) == true && $safe_search_data['status'] == true) {
                $safe_search_data           = $safe_search_data['data'];
                $search_params              = array();
                $search_params['trip_type'] = trim($safe_search_data['trip_type']);
                $search_params['from']      = trim($safe_search_data['from']);
                $search_params['to']        = trim($safe_search_data['to']);
                $search_params['depature']  = date('d-m-Y', strtotime($new_date)); //Adding new Date
                if (isset($safe_search_data['return'])) {
                    $search_params['return'] = add_days_to_date($day_diff, $safe_search_data['return']); //Check it
                }
                $search_params['adult']         = intval($safe_search_data['adult_config']);
                $search_params['child']         = intval($safe_search_data['child_config']);
                $search_params['infant']        = intval($safe_search_data['infant_config']);
                $search_params['search_flight'] = 'search';
                $search_params['v_class']       = trim($safe_search_data['v_class']);
                $search_params['carrier']       = $safe_search_data['carrier'];
                redirect(base_url() . 'index.php/general/pre_flight_search/?' . http_build_query($search_params));
            } else {
                $this->template->view('general/popup_redirect');
            }
        } else {
            $this->template->view('general/popup_redirect');
        }
    }
    /**
     * Jaganath
     * Search Request from Fare Calendar
     */
    public function pre_fare_search_result()
    {
        $get_data = $this->input->get();
        if (isset($get_data['from']) == true && empty($get_data['from']) == false &&
            isset($get_data['to']) == true && empty($get_data['to']) == false &&
            isset($get_data['depature']) == true && empty($get_data['depature']) == false) {
            $from             = trim($get_data['from']);
            $to               = trim($get_data['to']);
            $depature         = trim($get_data['depature']);
            $from_loc_details = $this->custom_db->single_table_records('flight_airport_list', '*', array('airport_code' => $from));
            $to_loc_details   = $this->custom_db->single_table_records('flight_airport_list', '*', array('airport_code' => $to));
            if ($from_loc_details['status'] == true && $to_loc_details['status'] == true) {
                $depature     = date('Y-m-d', strtotime($depature));
                $airport_code = trim($from_loc_details['data'][0]['airport_code']);
                $airport_city = trim($from_loc_details['data'][0]['airport_city']);
                $from         = $airport_city . ' (' . $airport_code . ')';
                //To
                $airport_code = trim($to_loc_details['data'][0]['airport_code']);
                $airport_city = trim($to_loc_details['data'][0]['airport_city']);
                $to           = $airport_city . ' (' . $airport_code . ')';

                //Forming Search Request
                $search_params                  = array();
                $search_params['trip_type']     = 'oneway';
                $search_params['from']          = $from;
                $search_params['to']            = $to;
                $search_params['depature']      = $depature;
                $search_params['adult']         = 1;
                $search_params['child']         = 0;
                $search_params['infant']        = 0;
                $search_params['search_flight'] = 'search';
                $search_params['v_class']       = 'All';
                $search_params['carrier']       = array('');
                redirect(base_url() . 'index.php/general/pre_flight_search/?' . http_build_query($search_params));
            } else {
                $this->template->view('general/popup_redirect');
            }
        } else {
            $this->template->view('general/popup_redirect');
        }
    }
    /**
     * Search Result
     * @param number $search_id
     */
    public function search($search_id)
    {
        $safe_search_data = $this->flight_model->get_safe_search_data($search_id);
        // Get all the FLIGHT bookings source which are active
        $active_booking_source = $this->flight_model->active_booking_source();
        if (valid_array($active_booking_source) == true and $safe_search_data['status'] == true) {
            $safe_search_data['data']['search_id'] = abs($search_id);
            $page_params                           = array(
                'flight_search_params'  => $safe_search_data['data'],
                'active_booking_source' => $active_booking_source,
            );
            $page_params['from_currency'] = get_application_default_currency();
            $page_params['to_currency']   = get_application_currency_preference();

            //Need to check if its domestic travel
            $from_loc                                  = $safe_search_data['data']['from_loc'];
            $to_loc                                    = $safe_search_data['data']['to_loc'];
            $page_params['is_domestic_one_way_flight'] = false;
            if ($safe_search_data['data']['trip_type'] == 'oneway') {
                $page_params['is_domestic_one_way_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);
            }
            $page_params['airline_list'] = $this->db_cache_api->get_airline_code_list(); //Jaganath
            $this->template->view('flight/search_result_page', $page_params);
        } else {
            if ($safe_search_data['status'] == true) {
                $this->template->view('general/popup_redirect');
            } else {
                $this->template->view('flight/exception');
            }
        }
    }
    /**
     * Arjun J Gowda
     * Passenger Details page for final bookings
     * Here we need to run farequote/booking based on api
     * View Page for booking
     */
    public function booking($search_id)
    {
        /*
         * FIXME
         * The length of Passenger first name and last name combine cannot more
         * than 35 characters
         */
        $pre_booking_params          = $this->input->post();
        $page_data['loyality_point'] = $this->user_model->getUserLoyalityPoint();
        $booking_type                = $pre_booking_params['booking_type'];

        load_flight_lib($pre_booking_params['booking_source']);
        $safe_search_data                      = $this->flight_lib->search_data($search_id);
        $safe_search_data['data']['search_id'] = intval($search_id);
        $token                                 = $this->flight_lib->unserialized_token($pre_booking_params['token'], $pre_booking_params['token_key']);

        if ($token['status'] == SUCCESS_STATUS) {
            $pre_booking_params['token'] = $token['data']['token'];
        }
        if (isset($pre_booking_params['booking_source']) == true && $safe_search_data['status'] == true) {
            //Jaganath - Check Travel is Domestic or International
            $from_loc                                       = $safe_search_data['data']['from_loc'];
            $to_loc                                         = $safe_search_data['data']['to_loc'];
            $safe_search_data['data']['is_domestic_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);

            $page_data['active_payment_options'] = $this->module_model->get_active_payment_module_list();
            $page_data['search_data']            = $safe_search_data['data'];
            $currency_obj                        = new Currency(array(
                'module_type' => 'flight',
                'from'        => get_application_default_currency(),
                'to'          => get_application_default_currency(),
            ));
            // We will load different page for different API providers... As we have dependency on API for Flight details
            $page_data['search_data'] = $safe_search_data['data'];
            //Need to fill pax details by default if user has already logged in
            $this->load->model('user_model');
            $page_data['pax_details'] = $this->user_model->get_current_user_details();

            //Not to show cache data in browser
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            switch ($pre_booking_params['booking_source']) {
                case PROVAB_FLIGHT_BOOKING_SOURCE:
                    switch ($booking_type) {
                        case 'process_fare_quote':
                            // upate fare details
                            $quote_update = $this->fare_quote_booking($pre_booking_params);
                            if ($quote_update['status'] == FAILURE_STATUS) {
                                redirect(base_url() . 'index.php/flight/exception?op=Remote IO error @ Session Expiry&notification=session');
                            } else {
                                $pre_booking_params = $quote_update['data'];
                            }
                            break;
                        case 'process_booking':
                            break;
                        default:
                            redirect(base_url());
                    }
                    // Load View
                    $page_data['booking_source']                         = $pre_booking_params['booking_source'];
                    $page_data['pre_booking_params']['default_currency'] = get_application_default_currency();
                    $page_data['iso_country_list']                       = $this->db_cache_api->get_iso_country_code();
                    $page_data['country_list']                           = $this->db_cache_api->get_iso_country_code();
                    $page_data['currency_obj']                           = $currency_obj;
                    //Traveller Details
                    $page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
                    //Extracting Segment Summary and Fare Details
                    $updated_flight_details = $pre_booking_params['token'];
                    $is_price_Changed       = false;
                    $flight_details         = array();
                    foreach ($updated_flight_details as $k => $v) {
                        if ($is_price_Changed == false && $v['IsPriceChanged'] == true) {
                            $is_price_Changed = true;
                        }
                        $temp_flight_details = $this->flight_lib->extract_flight_segment_fare_details($v['FlightDetails'], $currency_obj, $search_id, $this->current_module);
                        unset($temp_flight_details[0]['BookingType']); //Not needed in Next page
                        $flight_details[$k] = $temp_flight_details[0];
                    }
                    //Merge the Segment Details and Fare Details For Printing Purpose
                    $flight_pre_booking_summary       = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
                    $pre_booking_params['token']      = $flight_details;
                    $page_data['pre_booking_params']  = $pre_booking_params;
                    $page_data['pre_booking_summery'] = $flight_pre_booking_summary;
                    $TotalPrice                       = $flight_pre_booking_summary['FareDetails'][$this->current_module . '_PriceDetails']['TotalFare'];
                    $page_data['convenience_fees']    = $currency_obj->convenience_fees($TotalPrice, $search_id);
                    $page_data['is_price_Changed']    = $is_price_Changed;
                    $this->template->view('flight/tbo/tbo_booking_page', $page_data);
                    break;
            }
        } else {
            redirect(base_url());
        }
    }

    /**
     * Fare Quote Booking
     * This will be used for TBO LCC carrier
     */
    private function fare_quote_booking($flight_booking_details)
    {
        // LCCSpecialReturn vs LCCNormalReturn
        //currency conversion
        $fare_quote_details = $this->flight_lib->fare_quote_details($flight_booking_details);
        if ($fare_quote_details['status'] == SUCCESS_STATUS && valid_array($fare_quote_details) == true) {
            //Converting API currency data to preferred currency
            $currency_obj       = new Currency(array('module_type' => 'flight', 'from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
            $fare_quote_details = $this->flight_lib->farequote_data_in_preferred_currency($fare_quote_details, $currency_obj);
        }

        return $fare_quote_details;
        //return $this->flight_lib->fare_quote_details ( $flight_booking_details );

    }
    /**
     * Get Extra Services
     */
    private function get_extra_services($flight_booking_details)
    {
        $extra_service_details = $this->flight_lib->get_extra_services($flight_booking_details);
        return $extra_service_details;
    }

    /**
     * Arjun J Gowda
     * Secure Booking of FLIGHT
     * Process booking no view page
     */
    public function pre_booking($search_id)
    {
        $post_params = $this->input->post();
        if (valid_array($post_params) == false) {
            redirect(base_url());
        }
        //Setting Static Data - Jaganath
        $post_params['billing_city']      = 'Bangalore';
        $post_params['billing_zipcode']   = '560100';
        $post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';

        $temp_booking = $this->module_model->serialize_temp_booking_record($post_params, FLIGHT_BOOKING);
        $book_id      = $temp_booking['book_id'];
        $book_origin  = $temp_booking['temp_booking_origin'];
        // Make sure token and temp token matches
        $temp_token = unserialized_data($post_params['token'], $post_params['token_key']);

        if ($temp_token != false) {
            load_flight_lib($post_params['booking_source']);
            $amount   = 0;
            $currency = '';
            if ($post_params['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {
                $currency_obj = new Currency(array(
                    'module_type' => 'flight',
                    'from'        => get_application_default_currency(),
                    'to'          => get_application_default_currency(),
                ));
                $flight_details         = $temp_token['token'];
                $flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
                $fare_details           = $flight_booking_summary['FareDetails'][$this->current_module . '_PriceDetails'];
                $amount                 = $fare_details['TotalFare'];
                $currency               = $fare_details['CurrencySymbol'];
            }
            /********* Convinence Fees Start ********/
            $convenience_fees = ceil($currency_obj->convenience_fees($amount, $search_id));
            /********* Convinence Fees End ********/

            /********* Promocode Start ********/
            $promocode_discount = 0;
            /********* Promocode End ********/

            $email               = $post_params['billing_email'];
            $phone               = $post_params['passenger_contact'];
            $verification_amount = ($amount + $convenience_fees - $promocode_discount);
            $firstname           = $post_params['first_name']['0'] . " " . $post_params['last_name']['0'];
            $book_id             = $book_id;
            $productinfo         = META_AIRLINE_COURSE;

            // check current balance before proceeding further
            $domain_balance_status = $this->domain_management_model->verify_current_balance($verification_amount, $currency);
            if ($domain_balance_status == true) {
                //Save the Booking Data
                $booking_data = $this->module_model->unserialize_temp_booking_record($book_id, $book_origin);
                $book_params  = $booking_data['book_attributes'];
                $data         = $this->flight_lib->save_booking($book_id, $book_params, $currency_obj, $this->current_module);
                switch ($post_params['payment_method']) {
                    case PAY_NOW:
                        $this->load->model('transaction');
                        $this->transaction->create_payment_record($book_id, $amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount);
                        redirect('https://www.bundletrip.com/index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin);
                        break;
                    case PAY_AT_BANK:
                        echo 'Under Construction - Remote IO Error';
                        exit();
                        break;
                }
            } else {
                redirect(base_url() . 'index.php/flight/exception?op=Amount Flight Booking&notification=insufficient_balance');
            }
        }
        redirect(base_url() . 'index.php/flight/exception?op=Remote IO error @ FLIGHT Booking&notification=validation');
    }
    /*
    process booking in backend until show loader
     */

    public function process_booking($book_id, $temp_book_origin, $user_type = 'b2c', $user_id = '')
    {
        if ($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0) {

            $page_data['form_url']                        = base_url() . '/index.php/flight/secure_booking/' . $book_id . '/' . $temp_book_origin;
            $page_data['form_method']                     = 'POST';
            $page_data['form_params']['book_id']          = $book_id;
            $page_data['form_params']['temp_book_origin'] = $temp_book_origin;
            if ($user_type == 'b2b') {
                redirect(base_url() . 'mobile/index.php/flight/secure_booking/' . $book_id . '/' . $temp_book_origin . '/' . $user_type . '/' . $user_id);
            } else {
                redirect(base_url() . 'index.php/flight/secure_booking/' . $book_id . '/' . $temp_book_origin . '/b2b/' . $user_id);
            }
            // $this->template->view('share/loader/booking_process_loader', $page_data);

        } else {
            redirect(base_url() . 'index.php/flight/exception?op=Invalid request&notification=validation');
        }

    }
    /**
     * Arjun J Gowda
     * Do booking once payment is successfull - Payment Gateway
     * and issue voucher
     */
    public function secure_booking($book_id, $temp_book_origin, $user_type = 'b2c', $user_id = '')
    {
        
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(0);
        // $post_data = $this->input->post();
        $user_info = $this->user_model->get_user_id($book_id);
        if(!empty($user_info)){
            $user_id = $user_info['user_id'];
        }
        /*echo 'book_id=>'.$book_id;
        echo 'User_id=>'.$user_id;exit();*/

        // if(valid_array($post_data) == true && isset($post_data['book_id']) == true && isset($post_data['temp_book_origin']) == true &&
        // empty($post_data['book_id']) == false && intval($post_data['temp_book_origin']) > 0){
        if (!empty($book_id) && !empty($temp_book_origin) && intval($temp_book_origin) > 0) {
            //verify payment status and continue
            $book_id          = trim($book_id);
            $temp_book_origin = intval($temp_book_origin);
            $this->load->model('transaction');

            $booking_status = $this->transaction->get_payment_status($book_id);
            // debug($booking_status);exit;
            // $booking_status['status'] = 'accepted';
            // debug($booking_status);exit;
            if ($booking_status['status'] !== 'accepted') {
                redirect(base_url() . '/index.php/flight/exception?op=Payment Not Done&notification=validation');
            }
        } else {
            redirect(base_url() . '/index.php/flight/exception?op=InvalidBooking&notification=invalid');
        }

        //run booking request and do booking
        $temp_booking = $this->module_model->unserialize_temp_booking_record($book_id, $temp_book_origin);
        //debug($temp_booking);exit;
        //Delete the temp_booking record, after accessing

        // $this->module_model->delete_temp_booking_record ($book_id, $temp_book_origin);

        load_flight_lib($temp_booking['booking_source']);
        if ($temp_booking['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {

            $currency_obj = new Currency(array(
                'module_type' => 'flight',
                'from'        => admin_base_currency(),
                'to'          => admin_base_currency(),
            ));
            $flight_details         = $temp_booking['book_attributes']['token']['token'];
            $flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
            $fare_details           = $flight_booking_summary['FareDetails'][$this->current_module . '_PriceDetails'];
            $total_booking_price    = $fare_details['_AgentBuying'];
            $currency               = $fare_details['Currency'];
        }
        // verify payment status and continue
        if ($temp_booking != false) {
            switch ($temp_booking['booking_source']) {
                case PROVAB_FLIGHT_BOOKING_SOURCE:
                    try {
                        //echo "testng";exit;
                        // till here its working check in library
                        $booking = $this->flight_lib->process_booking($book_id, $temp_booking['book_attributes']);
                    } catch (Exception $e) {
                        $booking['status'] = BOOKING_ERROR;
                    }
                    // Save booking based on booking status and book id
                    break;
            }

            if (in_array($booking['status'], array(SUCCESS_STATUS, BOOKING_CONFIRMED, BOOKING_PENDING, BOOKING_FAILED, BOOKING_ERROR, BOOKING_HOLD, FAILURE_STATUS)) == true) {
                $currency_obj = new Currency(array(
                    'module_type' => 'flight',
                    'from'        => admin_base_currency(),
                    'to'          => admin_base_currency(),
                ));

                $booking['data']['booking_params']['currency_obj'] = $currency_obj;
                //Update the booking Details
                $ticket_details                          = @$booking['data']['ticket'];
                $ticket_details['master_booking_status'] = $booking['status'];

                $data = $this->flight_lib->update_booking_details($book_id, $booking['data']['booking_params'], $ticket_details, $this->current_module);
                //Update Transaction Details+
                if ($user_type == 'b2c') {
                    $this->domain_management_model->update_transaction_details('flight', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount'], $data['transaction_currency'], $data['currency_conversion_rate']);
                } else {
                    $this->domain_management_model->update_transaction_details_b2b('flight', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount'], $data['transaction_currency'], $data['currency_conversion_rate'], $user_id);
                }

                $url = explode('/mobile_webservices', base_url());

                if (in_array($data['status'], array(
                    'BOOKING_CONFIRMED',
                    'BOOKING_PENDING',
                    'BOOKING_HOLD',
                ))) {
                    //echo "savita";
                    if($data['status']=='BOOKING_CONFIRMED'){
                        $this->applyLoyalityPoints($total_booking_price, $book_id, $user_id);
                    }

                    $title = 'Flight Booking';
                    $description = 'Dear User, your flight has confirmed.';

                    $device_details= $this->user_model->getDevices($user_id,$type='1',$title,$description);
                    $this->load->library('fcm');
                    $this->fcm->set_title($title);
                    $this->fcm->set_message($description);

                    if ($device_details['ios']) {
                        //echo "<pre>";print_r($device_details['ios']);
                        $this->fcm->set_device_type('ios');
                        $device_ids = $device_details['ios'];
                        $this->fcm->add_multiple_recipients($device_ids);
                        $fcm_result = $this->fcm->send();
                        //echo "<pre>";print_r($fcm_result);exit();
                    }
                    if ($device_details['android']) {
                        //$device_details['android'] = array("d9IoRc3N-Qo:APA91bEcZWcMIH8R0t8v3E6SebglIEPpFTAaagqUgAlO_tWJJxQoc0j_9gRnMTdj55TVGXz0tvrUru7LQinaoT1aSi7eNVi2YFzCYDCGqviO15SOCxwUF7Hw3ngpWxMt2sS--jOSkmU-");
                        $this->fcm->set_device_type('android');
                        $device_ids = $device_details['android'];
                        $this->fcm->add_multiple_recipients($device_ids);
                        $fcm_result = $this->fcm->send();
                        //echo "<pre>";print_r($fcm_result);exit();
                    }

                    redirect(base_url() . '/index.php/voucher/flight/' . $book_id . '/' . $temp_booking['booking_source'] . '/' . $data['status'] . '/show_voucher');
                } else {
                    //debug($booking);exit;
                    $res["status"] = 0;
                    $res["url"]    = base_url() . 'index.php/flight/exception?op=booking_exception&notification=' . $booking['message'];
                }

            } else {
                $res["status"] = 0;
                $res["url"]    = base_url() . 'index.php/flight/exception?op=booking_exception&notification=' . $booking['message'];
            }
        }
        die(json_encode($res));
    }
    private function applyLoyalityPoints($total_booking_price=0, $app_reference=0, $user_id='') {
		
        //echo $total_booking_price;
    	//$total_booking_price = 10000;	
    	//$app_reference = 'FB18-200416-717494';
    	// get booking loyality price
        $price = $this->user_model->get_point_from_booking_details($app_reference);
    	$booking_price = $this->user_model->get_price_from_payment_details($app_reference);
    	$loyality_points_config = $this->user_model->getUserLoyalityPoint($user_id);
        $available_loyality_points = $loyality_points_config['loyality_points'];

        /*echo $this->db->last_query();
    	echo "<pre>";
    	print_r($loyality_points_config);exit();*/
    	$total_points = ($price['loyality_points']/$loyality_points_config['loyality_points_coversion_rate']);
    	//$this->user_model->deduct_loyality_point($total_points, $user_id);
        $total_booking_price = 0;
    	if(!empty($booking_price)){
            $total_booking_price = $booking_price['amount'];
        }
    	$data1 = array(
    		'booking_ref' => $app_reference,
    		'debit' => $total_points,
    		'equivalent_amount' => $price['loyality_points'],
            //'user_id' => $this->entity_user_id
    		'user_id' => $user_id,
            'remarks' => $total_booking_price
    	);
    	$this->user_model->log_loyality_point($data1);
         

        $credit = (int)(($total_booking_price*$loyality_points_config['max_percent_loyality_point_can_earn'])/100);

        $data2 = array(
    		'booking_ref' => $app_reference,
    		'credit' => $credit,
    		'equivalent_amount' => 0,
            //'user_id' => $this->entity_user_id,
    		'user_id' => $user_id,
    		'remarks' => $total_booking_price
    	);
    	$this->user_model->log_loyality_point($data2);

        //deducting debit amount
        $available_loyality_points = $available_loyality_points - $price['loyality_points'];

        //updating credit amount
        $available_loyality_points = $available_loyality_points + $credit; 

        //$this->user_model->add_loyality_point($credit,$user_id);
        $this->user_model->update_user_loyality_point($available_loyality_points,$user_id);


    }
    /**
     * Arjun J Gowda
     * Do booking once payment is successfull - Payment Gateway
     * and issue voucher
     */
    // function secure_booking_old()
    // {
    //     $book_id = $this->input->post('book_id');
    //     $temp_book_origin  = $this->input->post('temp_book_origin');
    //     //run booking request and do booking
    //     $temp_booking = $this->module_model->unserialize_temp_booking_record ( $book_id, $temp_book_origin );
    //     // FIXME Delete record from database
    //     load_flight_lib ( $temp_booking ['booking_source'] );
    //     if ($temp_booking ['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {
    //         $currency_obj = new Currency ( array (
    //                 'module_type' => 'flight',
    //                 'from' => get_application_default_currency (),
    //                 'to' => get_application_default_currency ()
    //         ) );
    //         $flight_details = $temp_booking ['book_attributes'] ['token'] ['token'];
    //         $flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
    //         $fare_details = $flight_booking_summary['FareDetails'][$this->current_module.'_PriceDetails'];
    //         $total_booking_price = $fare_details['TotalFare'];
    //         $currency = $fare_details['Currency'];
    //     }
    //     // verify payment status and continue
    //     // Flight_Model::lock_tables();
    //     $domain_balance_status = $this->domain_management_model->verify_current_balance ( $total_booking_price, $currency );
    //     if ($domain_balance_status) {
    //         if ($temp_booking != false) {
    //             switch ($temp_booking ['booking_source']) {
    //                 case PROVAB_FLIGHT_BOOKING_SOURCE :
    //                     try {
    //                         $booking = $this->flight_lib->process_booking ( $book_id, $temp_booking ['book_attributes'] );

    //                     }catch (Exception $e) {
    //                         $booking ['status'] = BOOKING_ERROR;
    //                     }
    //                     // Save booking based on booking status and book id
    //                     break;
    //             }
    //             if (in_array($booking ['status'], array(SUCCESS_STATUS, BOOKING_CONFIRMED, BOOKING_PENDING, BOOKING_FAILED, BOOKING_ERROR)) == true) {
    //                 $currency_obj = new Currency ( array (
    //                         'module_type' => 'flight',
    //                         'from' => get_application_default_currency (),
    //                         'to' => get_application_default_currency ()
    //                 ) );
    //                 $booking ['data'] ['booking_params'] ['currency_obj'] = $currency_obj;
    //                 //Update the booking Details
    //                 $icket_details = @$booking ['data'] ['ticket'];
    //                 $icket_details['master_booking_status'] = $booking ['status'];
    //                 $data = $this->flight_lib->update_booking_details( $book_id, $booking ['data'] ['booking_params'], $icket_details, @$booking ['data'] ['book'], $this->current_module);
    //                 $this->domain_management_model->update_transaction_details ( 'flight', $book_id, $data ['fare'], $data ['admin_markup'], $data ['agent_markup'], $data['convinence'], $data['discount'] );
    //                 if (in_array ( $data ['status'], array (
    //                         'BOOKING_CONFIRMED',
    //                         'BOOKING_PENDING'
    //                         ) )) {
    //                             // Sms config & Checkpoint
    //                             if (active_sms_checkpoint ( 'booking' )) {
    //                                 $msg = "Dear " . $data ['name'] . " Thank you for Booking your ticket with us.Ticket Details will be sent to your email id";
    //                                 $this->provab_sms->send_msg ( $data ['phone'], $msg );
    //                                     // return $sms_status;
    //                             }

    //                             $app_reference   = $book_id;
    //                             $booking_source  = $temp_booking ['booking_source'];
    //                             $booking_status  = $data ['status'];
    //                             $this->sendmail($app_reference,$booking_source,$booking_status);

    //                             /*redirect ( base_url () . 'index.php/voucher/flight/' . $book_id . '/' . $temp_booking ['booking_source'] . '/' . $data ['status'] . '/show_voucher' );*/
    //                             $resp['status'] = SUCCESS_STATUS;
    //                             $resp['message'] = "Booking Confirmed";
    //                             $resp['data'] =array('app_reference'=>$book_id,'booking_source'=>$temp_booking ['booking_source'],'booking_status'=>$data ['status']);
    //                             echo json_encode($resp); exit;
    //                         } else {
    //                             // redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $booking ['msg'] );
    //                             $resp['status'] = FAILURE_STATUS;
    //                             $resp['message'] = "Booking Failed";
    //                             $resp['data'] = array();
    //                             echo json_encode($resp); exit;
    //                         }
    //             } else {
    //                 redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $booking ['msg'] );
    //             }
    //         }
    //         // release table lock
    //         Flight_Model::release_locked_tables ();
    //     } else {
    //         // release table lock
    //         Flight_Model::release_locked_tables ();
    //         //echo base_url () . 'index.php/flight/exception?op=Remote IO error @ Insufficient&notification=validation';
    //         $response['status'] = FAILURE_STATUS;
    //         $response['message'] = "Insufficient Balance, Please contact admin";
    //         echo json_encode($response); exit;

    //     }
    //     // redirect(base_url().'index.php/flight/exception?op=Remote IO error @ FLIGHT Secure Booking&notification=validation');
    // }

    /**
     * Arjun J Gowda
     * Process booking on hold - pay at bank
     * Issue Ticket Later
     */
    public function booking_on_hold($book_id)
    {

    }
    /**
     * Jaganath
     */
    public function pre_cancellation($app_reference, $booking_source)
    {
        if (empty($app_reference) == false && empty($booking_source) == false) {
            $page_data       = array();
            $booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source);
            if ($booking_details['status'] == SUCCESS_STATUS) {
                $this->load->library('booking_data_formatter');
                //Assemble Booking Data
                $assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);
                $page_data['data']         = $assembled_booking_details['data'];
                $this->template->view('flight/pre_cancellation', $page_data);
            } else {
                redirect('security/log_event?event=Invalid Details');
            }
        } else {
            redirect('security/log_event?event=Invalid Details');
        }
    }
    public function cancel_booking_mobile()
    {
        error_reporting(E_ALL);
        $flight_data = $this->input->post('flight_cancel');
        //debug($flight_data);exit();
        // debug($flight_data);exit;
        $flight_data = json_decode($flight_data);
        $flight_data = json_decode(json_encode($flight_data), true);

        $app_reference      = trim($flight_data['app_reference']);
        $booking_source     = trim($flight_data['booking_source']);
        $transaction_origin = $flight_data['transaction_origin'];
        $passenger_data     = $this->flight_model->get_passenger_info($app_reference);
        //debug($passenger_data);exit();
        foreach ($passenger_data as $key => $pass) {
            $passenger_origin[$key] = $pass['origin'];
        }
        //debug($passenger_origin);exit;
        // debug($passenger_origin);exit;
        $booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
        // debug($booking_details);exit;
        if ($booking_details['status'] == SUCCESS_STATUS) {
            load_flight_lib($booking_source);
            //Formatting the Data
            $this->load->library('booking_data_formatter');
            $booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);
            $booking_details = $booking_details['data'];

            //Grouping the Passenger Ticket Ids
            $grouped_passenger_ticket_details = $this->flight_lib->group_cancellation_passenger_ticket_id($booking_details, $passenger_origin);
            $passenger_origin                 = $grouped_passenger_ticket_details['passenger_origin'];
            //debug($grouped_passenger_ticket_details);exit;
            $passenger_ticket_id  = $grouped_passenger_ticket_details['passenger_ticket_id'];
            $cancellation_details = $this->flight_lib->cancel_booking($booking_details, $passenger_origin, $passenger_ticket_id);
            //debug($cancellation_details);exit();
            if ($cancellation_details['status'] == 1) {
                $data['status']  = SUCCESS_STATUS;
                $data['message'] = "Your Cancellation Request has been sent";
            } else {
                $data['status']  = FAILURE_STATUS;
                $data['message'] = "Some thing went wrong Please Try Again !!! ";
            }
            echo json_encode($data);
        }

    }
    /**
     * Jaganath
     * @param $app_reference
     */
    public function cancel_booking($app_reference, $booking_source)
    {
        if (empty($app_reference) == false && empty($booking_source) == false) {
            $master_booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
            if ($master_booking_details['status'] == SUCCESS_STATUS) {
                $this->load->library('booking_data_formatter');
                $master_booking_details = $this->booking_data_formatter->format_flight_booking_data($master_booking_details, $this->current_module);
                $master_booking_details = $master_booking_details['data'];
                load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
                $cancellation_details = $this->flight_lib->cancel_full_booking($master_booking_details);
                redirect('flight/cancellation_details/' . $app_reference . '/' . $booking_source);
            } else {
                redirect('security/log_event?event=Invalid Details');
            }
        } else {
            redirect('security/log_event?event=Invalid Details');
        }
    }
    public function cancellation_details($app_reference, $booking_source)
    {
        if (empty($app_reference) == false && empty($booking_source) == false) {
            $master_booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
            if ($master_booking_details['status'] == SUCCESS_STATUS) {
                $page_data = array();
                $this->load->library('booking_data_formatter');
                $master_booking_details = $this->booking_data_formatter->format_flight_booking_data($master_booking_details, $this->current_module);
                $page_data['data']      = $master_booking_details['data'];
                $this->template->view('flight/cancellation_details', $page_data);
            } else {
                redirect('security/log_event?event=Invalid Details');
            }
        } else {
            redirect('security/log_event?event=Invalid Details');
        }

    }
    /**
     * Import Temp Booking data
     */
    public function import_temp_booking_data($book_id = 'FB03-125019-840625', $temp_book_origin = 140)
    {
        $temp_booking = $this->module_model->unserialize_temp_booking_record($book_id, $temp_book_origin);
        if (valid_array($temp_booking) == true) {
            //debug($temp_booking);
            //echo 'transaction log tabel wrt app_reference';
            load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
            $currency_obj = new Currency(array(
                'module_type' => 'flight',
                'from'        => get_application_default_currency(),
                'to'          => get_application_default_currency(),
            ));
            $book_params                 = $temp_booking['book_attributes'];
            $book_params['currency_obj'] = $currency_obj;

            $data = $this->flight_lib->save_booking($book_id, $book_params);
            //debug($data);
            $this->domain_management_model->update_transaction_details('flight', $book_id, $data['fare'], $data['admin_markup'], $data['agent_markup'], $data['convinence'], $data['discount']);
            echo 'data saved';exit;
        } else {
            echo 'No data';exit;
        }
    }
    /**
     * Arjun J Gowda
     */
    public function exception()
    {
        $module       = META_AIRLINE_COURSE;
        $op           = @$_GET['op'];
        $notification = @$_GET['notification'];
        $eid          = $this->module_model->log_exception($module, $op, $notification);
        // set ip log session before redirection
        $this->session->set_flashdata(array(
            'log_ip_info' => true,
        ));
        redirect(base_url() . 'index.php/flight/event_logger/' . $eid);
    }
    public function event_logger($eid = '')
    {
        $log_ip_info    = $this->session->flashdata('log_ip_info');
        $data['eid']    = $eid;
        $data['status'] = FAILURE_STATUS;
        echo json_encode($data);die;
        $this->template->view('flight/exception', array(
            'log_ip_info' => $log_ip_info,
            'eid'         => $eid,
        ));
    }
    public function test_server()
    {
        $data     = $this->custom_db->single_table_records('test', '*', array('origin' => 851));
        $response = json_decode($data['data'][0]['test'], true);
    }
    public function booking_mobile()
    {
        $post_params = $_REQUEST['farequote_data'];
        $var         = json_decode($post_params);

        $pre_booking_params_post                     = json_decode(json_encode($var), true);
        $pre_booking_params                          = array();

        $loyality_point = $this->user_model->getUserLoyalityPoint($pre_booking_params_post['user_id']);
        //echo $this->db->last_query();
        //echo "<pre>";print_r($loyality_point);exit();
        //echo  $this->db->last_query();

        if(!empty($loyality_point)){
            $max_point_can_use_in_one_time = $loyality_point['max_point_can_use_in_one_time'];
            $loyality_points               = (int) (($loyality_point['loyality_points'] * $loyality_point['percent_can_use']) / 100);
            $loyality_points_usable        = (min($max_point_can_use_in_one_time, $loyality_points)) * $loyality_point['loyality_points_coversion_rate'];

            $page_data['loyality_points_usable'] = (int) $loyality_points_usable;
        }else{
            $page_data['loyality_points_usable'] = 0;
        }
        //echo "<pre>";print_r($page_data);exit();
        $pre_booking_params['is_domestic']           = $pre_booking_params_post['is_domestic'];
        $pre_booking_params['token']                 = $pre_booking_params_post['token'];
        $pre_booking_params['token_key']             = $pre_booking_params_post['token_key'];
        $pre_booking_params['search_access_key']     = $pre_booking_params_post['search_access_key'];
        $pre_booking_params['is_lcc']                = '1';
        $pre_booking_params['promotional_plan_type'] = '0';
        $pre_booking_params['booking_type']          = 'process_fare_quote';
        $pre_booking_params['booking_source']        = 'PTBSID0000000002';

        $booking_type = $pre_booking_params['booking_type'];
        $search_id    = $pre_booking_params_post['search_id'];
        // echo 'herre'.$search_id;exit;
        load_flight_lib($pre_booking_params['booking_source']);
        $safe_search_data                      = $this->flight_lib->search_data($search_id);
        $safe_search_data['data']['search_id'] = intval($search_id);
        // debug($pre_booking_params ['token_key']); exit();
        $token = $this->flight_lib->unserialized_token($pre_booking_params['token'], $pre_booking_params['token_key']);
        // debug($token);exit;
        // debug($token);exit;
        if ($token['status'] == SUCCESS_STATUS) {
            $pre_booking_params['token'] = $token['data']['token'];
        }
        if (isset($pre_booking_params['booking_source']) == true && $safe_search_data['status'] == true) {
            //Jaganath - Check Travel is Domestic or International
            $from_loc                                       = $safe_search_data['data']['from_loc'];
            $to_loc                                         = $safe_search_data['data']['to_loc'];
            $safe_search_data['data']['is_domestic_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);

            $page_data['active_payment_options'] = $this->module_model->get_active_payment_module_list();
            $page_data['search_data']            = $safe_search_data['data'];
            $currency_obj                        = new Currency(array(
                'module_type' => 'flight',
                'from'        => get_application_default_currency(),
                'to'          => get_application_default_currency(),
            ));
            // We will load different page for different API providers... As we have dependency on API for Flight details
            $page_data['search_data'] = $safe_search_data['data'];
            //Need to fill pax details by default if user has already logged in
            $this->load->model('user_model');
            $page_data['pax_details'] = $this->user_model->get_current_user_details();

            //Not to show cache data in browser
            /*    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");*/

            switch ($pre_booking_params['booking_source']) {
                case PROVAB_FLIGHT_BOOKING_SOURCE:
                    switch ($booking_type) {
                        case 'process_fare_quote':
                            // upate fare details
                            //debug($pre_booking_params);exit;
                            $quote_update = $this->fare_quote_booking($pre_booking_params);
                            //debug($quote_update ); exit;
                            if ($quote_update['status'] == FAILURE_STATUS) {
                                $data['status'] = FAILURE_STATUS;
                                //$data['message'] ="Unable to Process your request please try again";
                                $data['message'] = "Session expired! Please try again";
                                $this->output_compressed_data($data);
                            } else {
                                $pre_booking_params = $quote_update['data'];
                                $extra_services     = $this->get_extra_services($pre_booking_params);
                                if ($extra_services['status'] == SUCCESS_STATUS) {
                                    $page_data['extra_services'] = $extra_services['data'];
                                } else {
                                    $page_data['extra_services'] = array();
                                }
                            }
                            break;
                        case 'process_booking':
                            break;

                    }

                    // Load View
                    $page_data['booking_source']                         = $pre_booking_params['booking_source'];
                    $page_data['pre_booking_params']['default_currency'] = get_application_default_currency();

                    $page_data['currency_obj'] = $currency_obj;
                    //Traveller Details
                    $page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
                    //Extracting Segment Summary and Fare Details
                    $updated_flight_details = $pre_booking_params['token'];
                    $flight_details         = array();
                    //debug($updated_flight_details);exit;
                    $empty_array = array();
                    foreach ($updated_flight_details as $k => $v) {
                        $temp_flight_details = $this->flight_lib->extract_flight_segment_fare_details($v, $currency_obj, $search_id, $this->current_module, false, $empty_array, $empty_array, $empty_array, 0, true);
                        unset($temp_flight_details[0]['BookingType']); //Not needed in Next page
                        $flight_details[$k] = $temp_flight_details[0];
                    }
                    //Merge the Segment Details and Fare Details For Printing Purpose

                    $flight_pre_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);

                    $pre_booking_params['token']          = $flight_details;
                    $pre_booking_params['extra_services'] = $extra_services;
                    $page_data['pre_booking_params']      = $pre_booking_params;
                    $page_data['pre_booking_summery']     = $flight_pre_booking_summary;
                    $TotalPrice                           = $flight_pre_booking_summary['FareDetails'][$this->current_module . '_PriceDetails']['TotalFare'];
                    $page_data['convenience_fees']        = $currency_obj->convenience_fees($TotalPrice, $search_id);

                    $page_data['session_expiry_details'] = $this->flight_lib->set_flight_search_session_expiry(true, $search_hash);
                    $page_data['trip_type']              = $safe_search_data['data']['trip_type'];
                    $page_data['search_id']              = $search_id;
                    $dynamic_params_url                  = serialized_data($pre_booking_params);

                    $response['status']            = SUCCESS_STATUS;
                    $response['data']              = $page_data;
                    $response['data']['token']     = $dynamic_params_url;
                    $response['data']['token_key'] = md5($dynamic_params_url);
                    if (!empty($dynamic_params_url) && !empty(md5($dynamic_params_url))) {
                        $indata['token_key']   = $response['data']['token_key'];
                        $indata['token']       = $response['data']['token'];
                        $flight_token_table_id = $this->custom_db->insert_record('flight_token', $indata);
                        if (!empty($flight_token_table_id) && intval($flight_token_table_id['insert_id']) > 0) {
                            $response['data']['flight_token_table_id'] = $flight_token_table_id['insert_id'];
                        }
                    }

                    $this->CI = &get_instance();
                    $this->CI->load->driver('cache');
                    $cache_search = $this->CI->config->item('cache_flight_ssr_search');
                    $search_hash  = md5($pre_booking_params_post['search_id']);

                    if ($cache_search) {
                        $cache_exp = $this->CI->config->item('cache_flight_search_ttl');
                        $this->CI->cache->file->save($search_hash, $page_data, $cache_exp);
                    }
                    if ($cache_search) {
                        $cache_contents = $this->CI->cache->file->get($search_hash);

                    }
                    $response['data']['search_hash_ssr'] = $search_hash;

                    $this->output_compressed_data($response);
                    exit;
            }
        } else {
            $response['status'] = FAILURE_STATUS;
            $response['msg']    = 'Failed in Farequote';
            //echo json_encode($response);
            $this->output_compressed_data($response);
        }
    }

    public function pre_booking_mobile($search_id = '')
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $passenger_data  = $_REQUEST['flight_book'];
        $booking_step    = @$_REQUEST['booking_step'];
        $search_ssr_hash = $_REQUEST['search_ssr_hash'];
        $wallet_bal      = !empty($_REQUEST['wallet_bal']) ? $_REQUEST['wallet_bal'] : '0';
        $user_type       = !empty($_REQUEST['user_type']) ? $_REQUEST['user_type'] : 'b2c';

        if (empty($passenger_data) && empty($key_token) && empty($token)) {
            $data['status']  = FAILURE_STATUS;
            $data['message'] = "Please enter the details to process your request";
            echo json_encode($data);
            exit;
        }

        $passenger_data = json_decode($passenger_data);
        $passenger_data = json_decode(json_encode($passenger_data), true);

        $key_token = $passenger_data['token_key'];
        $token     = json_decode($_REQUEST['token']);
        $token     = json_decode(json_encode($token), true);
        //$token = $token['token_id'];
        //debug($token);exit();
        // debug($token);
        // exit;
        // $flight_token_table_id =$token['flight_token_table_id'];
        // $flight_token_table_id =$token['flight_token_table_id'];
        $flight_token_table_id = $_REQUEST['flight_token_table_id'];

        //$flight_token_table_id = 8;
        $post_params1                      = array();
        $post_params1['billing_email']     = (isset($passenger_data['Email'])) ? $passenger_data['Email'] : '';
        $post_params1['passenger_contact'] = $passenger_data['ContactNo'];
        $post_params1['total_amount_val']  = $passenger_data['total_amount_val'];

        if($passenger_data['markup_ad']!=0 && isset($passenger_data['markup_ad'])){
            $post_params1['loyality_points']  = $passenger_data['markup_ad'];
        }else{
            $post_params1['loyality_points']  = 0;
        }

        $post_params1['convenience_fee']   = $passenger_data['convenience_fee'];
        $post_params1['currency_symbol']   = $passenger_data['currency_symbol'];
        $post_params1['currency']          = $passenger_data['currency'];
        $post_params1['country_code']      = $passenger_data['CountryCode'];
        $currencysymbol                    = $passenger_data['currency'];
        //    debug($post_params1); die;
        foreach ($passenger_data['Passengers'] as $key => $value) {
            $post_params1['passenger_type'][$key]                     = $value['passenger_type'];
            $post_params1['lead_passenger'][$key]                     = $value['lead_passenger'];
            $post_params1['lead_passenger'][$key]                     = $value['Gender'];
            $post_params1['passenger_nationality'][$key]              = '92';
            $post_params1['gender'][$key]                             = $value['Gender'];
            $post_params1['name_title'][$key]                         = $value['Title'];
            $post_params1['first_name'][$key]                         = $value['FirstName'];
            $post_params1['last_name'][$key]                          = $value['LastName'];
            $post_params1['passenger_passport_number'][$key]          = $value['PassportNumber'];
            $post_params1['date_of_birth'][$key]                      = $value['DateOfBirth'];
            $post_params1['passenger_passport_issuing_country'][$key] = $value['PassportIssueCountry'];
            $pass_exp                                                 = explode('-', $value['PassportExpiry']);
            $post_params1['passenger_passport_expiry_day'][$key]      = (isset($pass_exp['0'])) ? $pass_exp['0'] : '';
            $post_params1['passenger_passport_expiry_month'][$key]    = (isset($pass_exp['1'])) ? $pass_exp['1'] : '';
            $post_params1['passenger_passport_expiry_year'][$key]     = (isset($pass_exp['2'])) ? $pass_exp['2'] : '';

        }

        if (isset($passenger_data['payment_method'])) {
            $payment_method = $passenger_data['payment_method'];
        } else {
            $payment_method = 'PNHB1';
        }

        $post_params1['payment_method']          = $payment_method;
        $post_params1['customer_id']             = (isset($passenger_data['customer_id'])) ? $passenger_data['customer_id'] : '';
        $post_params1['op']                      = 'book_room';
        $post_params1['booking_source']          = PROVAB_FLIGHT_BOOKING_SOURCE;
        $post_params1['promo_code_discount_val'] = '0.00';
        $post_params1['wallet_bal']              = $wallet_bal;
        $post_params1['promo_code']              = '';
        $post_params1['search_id']               = $passenger_data['search_id'];

        if (isset($passenger_data['baggage_0'])) {
            foreach ($passenger_data['baggage_0'] as $key => $value) {
                $post_params1['baggage_0'][$key] = @$value[$key];
            }
        }
        if (isset($passenger_data['meal_0'])) {
            foreach ($passenger_data['meal_0'] as $key => $value) {
                $post_params1['meal_0'][$key] = @$value[$key];
            }
        }
        if (isset($passenger_data['seat_0'])) {
            foreach ($passenger_data['seat_0'] as $key => $value) {
                $post_params1['seat_0'][$key] = @$value[$key];
            }
        }
        $post_params1['billing_city']      = 'Bangalore';
        $post_params1['billing_country']   = '92';
        $post_params1['billing_zipcode']   = '560100';
        $post_params1['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue, Konnappana Agrahara, Electronic city';
        $post_params1['tc']                = 'on';
        //debug($post_params1); die;
        /*$post_params1['token'] = $token;
        $post_params1['token_key'] = $key_token;*/
        if (valid_array($post_params1) == false) {
            $data['status']  = FAILURE_STATUS;
            $data['message'] = "please enter the Valid data to book the ticket";
            $this->output_compressed_data($data);
            exit;
        }
        #debug($post_params1); die;
        //for ssr webview storing passenger details in session
        $GLOBALS['CI']->session->set_userdata('flight_ss_post_data', base64_encode(json_encode($post_params1)));
        #debug($GLOBALS['CI']->session->all_userdata()); die;
        //debug($flight_token_table_id); die;
        if (intval($flight_token_table_id) > 0) {
            $token_table_data = $this->custom_db->single_table_records('flight_token', '*', array('origin' => $flight_token_table_id));
            //debug($token_table_data);exit();
            $post_params1['token']     = $token_table_data['data']['0']['token'];
            $post_params1['token_key'] = $token_table_data['data']['0']['token_key'];
        }

        //debug($post_params1); die;
        $valid_temp_token = unserialized_data($post_params1['token'], $post_params1['token_key']);
        //echo "<pre>";print_r($post_params1);exit();
        //echo "<pre>";print_r($valid_temp_token);exit();
        /*if($passenger_data['markup_ad']!=0 && $valid_temp_token != false)
        {*/
            /*$valid=&$valid_temp_token['token'][0]['FareDetails']['b2b_PriceDetails'];
            //debug($valid);
            $valid['_AgentMarkup']= $valid['_AgentMarkup'] + $post_params ['markup_ad'];
            $valid['_CustomerBuying']= $valid['_CustomerBuying'] + $post_params['markup_ad'];
            $valid['_AgentEarning'] = $valid['_AgentEarning'] + $post_params ['markup_ad'];
            $valid['_TaxSum'] = $valid['_TaxSum'] + $post_params ['markup_ad'];
            $valid['_TotalPayable'] =$valid['_TotalPayable'] + $post_params ['markup_ad'];
            //debug($valid_temp_token);exit;
            // debug($valid['_AgentBuying']);exit();
            $token=serialized_data($valid_temp_token);
            $post_params ['token']=$token;*/
            //debug($token);exit;
        //}

        //debug($valid_temp_token);exit();
        // echo '<pre>';
        // print_r(array(
        //     'flight_token_table_id' => $flight_token_table_id,
        //     'post_params1'=>$post_params1,
        //     'valid_temp_token'=>$valid_temp_token
        // ));
        // exit();
        if ($valid_temp_token != false) {
            //webview additional ssr starts
            if ($booking_step == 'additional_ssr') {
                // echo 'flight/additional_ssr/'.$search_ssr_hash;exit;
                $this->additional_ssr($search_ssr_hash);
            }
            //webview additional ssr ends
            load_flight_lib($post_params1['booking_source']);
            $amount   = 0;
            $currency = '';

            $serealize_post = $post_params1;
            $temp_token1    = unserialized_data($post_params1['token']);
            // debug($temp_token1);
            // exit;
            $post_params1['token'] = $temp_token1;
            $temp_token            = $post_params1;
            //debug($temp_token);exit();
            if ($temp_token != false) {
                $currency_obj = new Currency(array(
                    'module_type' => 'flight',
                    'from'        => get_application_currency_preference(),
                    'to'          => get_application_default_currency(),
                ));

                $post_params1['token']['token'] = $this->flight_lib->convert_token_to_application_currency($post_params1['token']['token'], $currency_obj, $this->current_module, 'web');
                if (isset($post_params1['token']['extra_services']) == true) {
                    $post_params1['token']['extra_services'] = $extra_services = $this->flight_lib->convert_extra_services_to_application_currency($post_params1['token']['extra_services'], $currency_obj);
                }
                // $temp_token1['extra_services'] = $extra_services;
                $token_se              = serialized_data($post_params1['token']);
                $post_params1['token'] = $token_se;
                $temp_booking          = $this->module_model->serialize_temp_booking_record($post_params1, FLIGHT_BOOKING);
                $book_id               = $temp_booking['book_id'];
                $book_origin           = $temp_booking['temp_booking_origin'];
                if ($post_params1['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {
                    $currency_obj = new Currency(array(
                        'module_type' => 'flight',
                        'from'        => get_application_default_currency(),
                        'to'          => get_application_default_currency(),
                    ));

                    $flight_details = unserialized_data($post_params1['token']);
                    $flight_details = $flight_details['token'];

                    $flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);

                    $fare_details = $flight_booking_summary['FareDetails'][$user_type . '_PriceDetails'];
                    // echo "here";
                    $amount        = $fare_details['TotalFare'];
                    $currency      = $currencysymbol;
                    $book_currency = $fare_details['Currency'];
                }
                //echo $amount; exit();
                /********* Convinence Fees Start ********/
                //$convenience_fees = ceil($currency_obj->convenience_fees($amount, $search_id));
                $convenience_fees = 0.00;
                /********* Convinence Fees End ********/

                /********* Promocode Start ********/
                $promocode_discount = 0;
                /********* Promocode End ********/

                $email               = $post_params1['billing_email'];
                $phone               = $post_params1['passenger_contact'];
                $verification_amount = ($amount + $convenience_fees - $promocode_discount);
                $firstname           = $post_params1['first_name']['0'] . " " . $post_params1['last_name']['0'];
                $book_id             = $book_id;
                $productinfo         = META_AIRLINE_COURSE;

                // check current balance before proceeding further
                $domain_balance_status = $this->domain_management_model->verify_current_balance($verification_amount, $currency);
                if ($domain_balance_status == true) {
                    //Save the Booking Data
                    $booking_data                                               = $this->module_model->unserialize_temp_booking_record($book_id, $book_origin);
                    $booking_data['book_attributes']['token']['booking_source'] = $booking_data['book_attributes']['booking_source'];
                    $booking_data['book_attributes']['search_id']               = $passenger_data['search_id'];
                    //debug($booking_data); die;
                    $book_params = $booking_data['book_attributes'];
                    //debug($book_params); exit();
                    $GLOBALS['CI']->entity_user_id = $passenger_data['user_id'];
                    $data                          = $this->flight_lib->save_booking($book_id, $book_params, $currency_obj, $user_type, $post_params1['customer_id']);
                    //Add Extra Service Price to Booking Amount
                    /*echo 'fare'.$data['fare'];
                    echo 'admin'.$data['admin_markup'];
                    echo 'con'.$data['convinence']; exit();*/

                    $amount                     = roundoff_number($data['fare']) + roundoff_number($data['admin_markup']) + roundoff_number($data['convinence']);
                    $extra_services_total_price = $this->flight_model->get_extra_services_total_price($book_id);
                    if (isset($passenger_data['promo_code_discount_val']) && !empty($passenger_data['promo_code_discount_val'])) {
                        $amount = $amount - $passenger_data['promo_code_discount_val'] + $extra_services_total_price;
                    } else {
                        $amount = $amount + $extra_services_total_price;
                    }
                    switch ($post_params1['payment_method']) {
                        case PAY_NOW:
                            $this->load->model('transaction');
                            $this->transaction->create_payment_record($book_id, $amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount);
                            //$pre_booking_status['redirect'] = base_url().'index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;
                            $pre_booking_status['data'] = array('app_reference' => $book_id, 'book_origin' => $book_origin);

                            $url = explode('/mobile_webservices', base_url());

                            if ($amount > 0) {
                                // $pre_booking_status['data']['return_url']   = $url[0].'/index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin.'/'.$passenger_data['card'].'/'.$passenger_data['month'].'/'.$passenger_data['year'];
                                $pre_booking_status['data']['return_url'] = base_url() . 'index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin . '/' . $passenger_data['card'] . '/' . $passenger_data['month'] . '/' . $passenger_data['year'];
                                $pre_booking_status['status']             = SUCCESS_STATUS;
                                $this->output_compressed_data($pre_booking_status);
                                exit();
                            }
                            /*elseif ($amount > 0 && $user_type == "b2b") {
                            $domain_balance_status_b2b = $this->domain_management_model->verify_current_balance_b2b ( $amount, $post_params1['currency'], $post_params1['customer_id']);

                            if ($domain_balance_status_b2b == false) {
                            $pre_booking_status['status'] = FAILURE_STATUS;
                            $pre_booking_status['message'] = "Insufficient Balance";
                            $this->output_compressed_data($pre_booking_status);
                            exit();
                            }

                            $pre_booking_status['status'] = SUCCESS_STATUS;
                            $pre_booking_status['data']['return_url']   = base_url().'mobile/index.php/flight/process_booking/'.$book_id.'/'.$book_origin . '/' . $user_type . '/' . $post_params1['customer_id'];
                            $this->output_compressed_data($pre_booking_status);
                            exit();
                            } */
                            else {
                                // $pre_booking_status['status']   = SUCCESS_STATUS;
                                // $pre_booking_status['response_params'] ='wallet_amount_used';
                                // $this->transaction->update_payment_record_status($book_id, ACCEPTED, $response_params);
                                // $pre_booking_status['data']['return_url']   = $url[0].'/index.php/flight/process_booking/'.$book_id.'/'.$book_origin;
                                // $this->output_compressed_data($pre_booking_status);
                                echo "please check amount";
                                exit();
                            }

                        case PAY_AT_BANK:
                            echo 'Under Construction - Remote IO Error';
                            exit();
                            break;
                    }
                } else {
                    $data['status']  = FAILURE_STATUS;
                    $data['message'] = "We can't process your request please contact our Customer Support";
                    $this->output_compressed_data($data);
                    exit;
                }
            } else {
                $data['status']  = FAILURE_STATUS;
                $data['message'] = "Invalid Request";
                $this->output_compressed_data($data);
                exit;
            }
        } else {
            $data['status']  = FAILURE_STATUS;
            $data['message'] = "Invalid Token (Token Key Mismatch)";
            $this->output_compressed_data($data);
            exit;
        }
    }
// Jun 7 2018,(generating web view for meal, seat, bag details)
    //after entering pax name details, before payment gateway page, they have to call this api.
    // function additional_ssr($search_ssr_hash){
    //     $response['status'] = false;
    //     $response['msg'] = 'Failed in  additional SSr';
    //     //$search_ssr_hash =$_REQUEST['search_ssr_hash'];
    //     $this->CI = &get_instance();
    //     $this->CI->load->driver('cache');
    //     if(!empty($search_ssr_hash)){
    //     $passenger_data_json=$_REQUEST['flight_book'];

//     $passenger_data = json_decode($passenger_data_json);
    //     $passenger_data = json_decode(json_encode($passenger_data), True);
    //     $cache_search = $this->CI->config->item('cache_flight_ssr_search');
    //     //fetching ssr from cache
    //     if ($cache_search) {
    //     $page_data = $this->CI->cache->file->get($search_ssr_hash);

//         if(!empty($page_data)){

//             $page_data['passenger_data'] = base64_encode($passenger_data_json);
    //             $page_data['Email'] =$passenger_data['Email'];
    //             $page_data['ContactNo'] =$passenger_data['ContactNo'];
    //             $page_data['FirstName'] =$passenger_data['Passengers'][0]['FirstName'];
    //             $page_data['LastName']=$passenger_data['Passengers'][0]['LastName'];
    //             //debug($page_data); die;
    //             load_flight_lib ( 'PTBSID0000000002' );
    //             echo $this->template->isolated_view ( 'flight/tbo/tbo_booking_page', $page_data);
    //             exit;
    //             //echo $this->template->view ( 'flight/tbo/tbo_booking_page', $page_data);

//         }else{
    //         $response['status'] = false;
    //         $response['msg'] = 'Page data is empty, Failed in  additional SSr';
    //         $this->output_compressed_data($response);
    //         die;
    //         }
    //     }

//     }
    //     //$this->output_compressed_data($response);
    // }

    public function additional_ssr($search_ssr_hash)
    {
        // $search_ssr_hash = "878d5691c824ee2aaf770f7d36c151d6";
        $response['status'] = false;
        $response['msg']    = 'Failed in  additional SSr';
        //$search_ssr_hash = $_REQUEST['search_ssr_hash'];

        $this->CI = &get_instance();
        $this->CI->load->driver('cache');
        if (!empty($search_ssr_hash)) {

            $passenger_data_json = $_REQUEST['flight_book'];
            //debug($passenger_data_json); exit();
            $wallet_bal     = $_REQUEST['wallet_bal'];
            $passenger_data = json_decode($passenger_data_json);
            $passenger_data = json_decode(json_encode($passenger_data), true);
            $cache_search   = $this->CI->config->item('cache_flight_ssr_search');
            //fetching ssr from cache
            if ($cache_search) {
                $page_data = $this->CI->cache->file->get($search_ssr_hash);

                if (!empty($page_data)) {

                    $page_data['passenger_data'] = base64_encode($passenger_data_json);
                    $page_data['pax_details']    = $passenger_data;
                    $page_data['Email']          = $passenger_data['Email'];
                    $page_data['ContactNo']      = $passenger_data['ContactNo'];
                    $page_data['FirstName']      = $passenger_data['Passengers'][0]['FirstName'];
                    $page_data['LastName']       = $passenger_data['Passengers'][0]['LastName'];
                    $page_data['wallet_bal']     = $wallet_bal;
                    //debug($page_data); exit();
                    load_flight_lib('PTBSID0000000002');
                    //echo $this->template->view ( 'flight/tbo/tbo_booking_page', $page_data);
                    echo $this->template->isolated_view('flight/tbo/tbo_booking_page', $page_data);
                    exit;

                } else {
                    $response['status'] = false;
                    $response['msg']    = 'Page data is empty, Failed in  additional SSr';
                    $this->output_compressed_data($response);
                    die;
                }
            }

        }
        //$this->output_compressed_data($response);
    }

// Jun 7 2018, after clicking continue,(after selecting meal, seat, bag details)
    public function process_additional_ssr($search_id)
    {
        //error_reporting(E_ALL);
        $post_params = $_POST;
        // debug($post_params); die;
        if (valid_array($post_params) == false) {
            redirect(base_url());

        }
        $post_params['billing_city']      = 'Bangalore';
        $post_params['billing_country']   = '92';
        $post_params['billing_zipcode']   = '560100';
        $post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';

        $valid_temp_token = unserialized_data($post_params['token'], $post_params['token_key']);
        if ($valid_temp_token != false) {
            load_flight_lib($post_params['booking_source']);
            $amount               = 0;
            $currency             = '';
            $post_params['token'] = unserialized_data($post_params['token']);
            $currency_obj         = new Currency(array(
                'module_type' => 'flight',
                'from'        => get_application_currency_preference(),
                'to'          => get_application_default_currency(),
            ));

            $post_params['token']['token'] = $flight_data = $this->flight_lib->convert_token_to_application_currency($post_params['token']['token'], $currency_obj, $this->current_module, 'web');
            if (isset($post_params['token']['extra_services']) == true) {
                $post_params['token']['extra_services'] = $extra_SSR = $this->flight_lib->convert_extra_services_to_application_currency($post_params['token']['extra_services'], $currency_obj);
            }

            $post_params['token'] = serialized_data($post_params['token']);
            //fetchicn passenger information from app(from session)
            //debug($this->session->all_userdata()); die;
            $session_data        = $GLOBALS['CI']->session->all_userdata();
            $flight_ss_post_data = @$session_data['flight_ss_post_data'];
            $post_passenger      = json_decode(base64_decode($flight_ss_post_data), true);

            $post_passenger['search_id']               = $post_params['search_id'];
            $post_passenger['token']                   = $post_params['token'];
            $post_passenger['token_key']               = $post_params['token_key'];
            $post_passenger['op']                      = $post_params['op'];
            $post_passenger['booking_source']          = $post_params['booking_source'];
            $post_passenger['promo_code_discount_val'] = $post_params['promo_code_discount_val'];
            $post_passenger['promo_code']              = $post_params['promo_code'];
            $post_passenger['total_amount_val']        = $post_params['total_amount_val'];
            $post_passenger['passenger_contact']       = ((!empty($post_params['passenger_contact'])) ? $post_params['passenger_contact'] : '9489026310');
            $post_passenger['billing_email']           = ((!empty($post_params['billing_email'])) ? $post_params['billing_email'] : 'info@flytripnow.com');
            $post_passenger['first_name']              = ((!empty($post_params['first_name'])) ? $post_params['first_name'] : 'User');
            $post_passenger['last_name']               = ((!empty($post_params['last_name'])) ? $post_params['last_name'] : '');

            $passenger_data = json_decode(base64_decode($post_params['passenger_data']), true);

            foreach ($passenger_data['Passengers'] as $key => $value) {
                $post_passenger['passenger_type'][$key]                     = $value['passenger_type'];
                $post_passenger['lead_passenger'][$key]                     = $value['lead_passenger'];
                $post_passenger['lead_passenger'][$key]                     = $value['Gender'];
                $post_passenger['passenger_nationality'][$key]              = '92';
                $post_passenger['gender'][$key]                             = $value['Gender'];
                $post_passenger['name_title'][$key]                         = $value['Title'];
                $post_passenger['first_name'][$key]                         = $value['FirstName'];
                $post_passenger['last_name'][$key]                          = $value['LastName'];
                $post_passenger['passenger_passport_number'][$key]          = $value['PassportNumber'];
                $post_passenger['date_of_birth'][$key]                      = $value['DateOfBirth'];
                $post_passenger['passenger_passport_issuing_country'][$key] = $value['PassportIssueCountry'];
                $pass_exp                                                   = explode('-', $value['PassportExpiry']);
                $post_passenger['passenger_passport_expiry_day'][$key]      = $pass_exp['0'];
                $post_passenger['passenger_passport_expiry_month'][$key]    = $pass_exp['1'];
                $post_passenger['passenger_passport_expiry_year'][$key]     = $pass_exp['2'];
            }

            // $flight_count = count($flight_data);
            /* for($i=0; $i<$flight_count; $i++){
            }*/
            $bag_count  = count($extra_SSR['data']['ExtraServiceDetails']['Baggage']) - 1;
            $meal_count = count($extra_SSR['data']['ExtraServiceDetails']['Meals']) - 1;
            $seat_count = count($extra_SSR['data']['ExtraServiceDetails']['Seat']) - 1;

            for ($i = 0; $i <= $bag_count; $i++) {
                if (isset($post_params["baggage_$i"])) {
                    $post_passenger["baggage_$i"] = $post_params["baggage_$i"];
                }
            }
            for ($i = 0; $i <= $meal_count; $i++) {
                if (isset($post_params["meal_$i"])) {
                    $post_passenger["meal_$i"] = $post_params["meal_$i"];
                }
            }

            for ($i = 0; $i <= $meal_count; $i++) {
                if (isset($post_params["meal_pref$i"])) {
                    $post_passenger["meal_pref$i"] = $post_params["meal_pref$i"];
                }
            }

            for ($i = 0; $i <= $seat_count; $i++) {
                if (isset($post_params["seat_$i"])) {
                    $post_passenger["seat_$i"] = $post_params["seat_$i"];
                }
            }
            //  debug($_POST); debug($post_passenger); debug($meal_count); debug($seat_count); die;
            $post_passenger['payment_method']    = $post_params['payment_method'];
            $post_passenger['billing_city']      = $post_params['billing_city'];
            $post_passenger['billing_zipcode']   = $post_params['billing_zipcode'];
            $post_passenger['billing_address_1'] = $post_params['billing_address_1'];

            #error_reporting(E_ALL);
            if (valid_array($post_passenger)) {
                //$post_params['passenger_passport_expiry_month'] = $this->flight_lib->reindex_passport_expiry_month($post_passenger['passenger_passport_expiry_month'], $search_id);
                $temp_booking = $this->module_model->serialize_temp_booking_record($post_passenger, FLIGHT_BOOKING);

                $book_id     = $temp_booking['book_id'];
                $book_origin = $temp_booking['temp_booking_origin'];
                if ($post_passenger['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {
                    $currency_obj = new Currency(array(
                        'module_type' => 'flight',
                        'from'        => admin_base_currency(),
                        'to'          => admin_base_currency(),
                    ));
                    $temp_token             = unserialized_data($post_passenger['token']);
                    $flight_details         = $temp_token['token'];
                    $flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
                    $fare_details           = $flight_booking_summary['FareDetails'][$this->current_module . '_PriceDetails'];
                    $amount                 = $fare_details['TotalFare'];
                    $currency               = $fare_details['CurrencySymbol'];
                }

                /********* Promocode Start ********/
                $promocode_discount = $post_passenger['promo_code_discount_val'];
                /********* Promocode End ********/

                $email       = $post_passenger['billing_email'];
                $phone       = $post_passenger['passenger_contact'];
                $firstname   = $post_passenger['first_name'] . " " . $post_passenger['last_name'];
                $book_id     = $book_id;
                $productinfo = META_AIRLINE_COURSE;

                /********* Convinence Fees Start ********/
                $convenience_fees = ceil($currency_obj->convenience_fees($amount, $search_id));
                /********* Convinence Fees End ********/
                $currency = $fare_details['CurrencySymbol'];
                //Save the Booking Data
                $booking_data = $this->module_model->unserialize_temp_booking_record($book_id, $book_origin);
                $book_params  = $booking_data['book_attributes'];
                #    debug('ds');die;
                $data = $this->flight_lib->save_booking($book_id, $book_params, $currency_obj, $user_type);

                //Add Extra Service Price to Booking Amount
                $extra_services_total_price = $this->flight_model->get_extra_services_total_price($book_id);
                if ($extra_services_total_price > 0) {
                    $amount += $extra_services_total_price;
                }

                #debug($amount); die;
                $domain_balance_status = $this->domain_management_model->verify_current_balance($amount, $currency);
                if ($domain_balance_status == true) {

                    switch ($post_passenger['payment_method']) {
                        case PAY_NOW:
                            $this->load->model('transaction');
                            $this->transaction->create_payment_record($book_id, $amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount);
                            //$pre_booking_status['redirect'] = base_url().'index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;
                            $pre_booking_status['data'] = array('app_reference' => $book_id, 'book_origin' => $book_origin, 'extra_services_total_price' => $extra_services_total_price);

                            $pre_booking_status['retun_url'] = B_URL . '/index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin;
                            $pre_booking_status['status']    = SUCCESS_STATUS;
                            echo json_encode($pre_booking_status);
                            exit();

                            break;
                        case PAY_AT_BANK:
                            echo 'Under Construction - Remote IO Error';
                            exit();
                            break;
                    }
                } else {
                    $data['status']  = FAILURE_STATUS;
                    $data['message'] = "We can't process your request please contact our Customer Support";
                    $this->output_compressed_data($data);
                    exit;
                }

            } else {

                $data['status']  = FAILURE_STATUS;
                $data['message'] = "Passenger post data is empty";
                $this->output_compressed_data($data);
                exit;
            }

        } else {
            $data['status']  = FAILURE_STATUS;
            $data['message'] = "Token data is invalid";
            $this->output_compressed_data($data);
            exit;
        }

    }

    public function get_fare_details()
    {
        // echo "sdfsd"; exit();
        $response['status'] = FAILURE_STATUS;
        $response['data']   = '';
        $params             = $this->input->post('farerules');
        //$params = '{"data_access_key":"czoyMToiMTQ4NTgzODc1NzEzMTAqXypPQjEwIjs=","booking_source":"PTBSID0000000002","search_access_key":"MTQ4NTgzODc1NzIwNTE0MzEyNTQxODc1ODE3ODUxMTY3MzI5MzI0MDIxMDk4NDIwMTJfX18wUjBfX181NGQ2OTYzMC1kYjg4LTQzZjUtYTMwMS04MTBjNzRhNTVjZjNfX19QVEJTSUQwMDAwMDAwMDAy","provab_auth_key":""}';

        if (empty($params)) {
            $data['status']  = FAILURE_STATUS;
            $data['message'] = "Invalid Request";
            echo json_encode($data);
            exit;
        }

        $tokens = json_decode($params);
        $params = json_decode(json_encode($tokens), true);

        load_flight_lib($params['booking_source']);
        $data_access_key           = $params['data_access_key'];
        $params['data_access_key'] = unserialized_data($params['data_access_key']);
        if (empty($params['data_access_key']) == false) {
            switch ($params['booking_source']) {
                case PROVAB_FLIGHT_BOOKING_SOURCE:
                    $params['data_access_key'] = $this->flight_lib->read_token($data_access_key);
                    $data                      = $this->flight_lib->get_fare_details($params['data_access_key'], $params['search_access_key']);

                    if ($data['status'] == SUCCESS_STATUS) {
                        $response['status'] = SUCCESS_STATUS;
                        $response['data']   = $data['data'];
                        $response['msg']    = 'Fare Details Available';
                    }
            }
        }
        $response['important'] = "<b style='color: red'>Important - This information is indicative. Wizfair does not guarantee the accuracy of the information. Kindly check with the airline website for cancellation, baggage policy, date change and other travel policies. Contact our 24/7 customer service for further assistance.</b>
                                <ul class='fair-details'>
                                    <li>Name change is not permitted.</li>
                                    <li>Date changes will incur penalty and fees (airline penalty plus any fare difference plus our processing service fee) and is based on availability of flight at the time of change.</li>
                                    <li>Date change after departure must be done by the airline directly (airline penalty plus any fare difference will apply and is based on availability of flight at the time of change).</li>
                                    <li>Routing change will incur penalty and fees (airline penalty plus any fare difference plus our processing service fee will apply and is based on availability of flight at the time of change).</li>
                                    <li>Contact our 24/7 customer service to make any changes.</li>
                                    <li>Prior to completing the booking, please review our </li>
                                </ul>";
        //  echo json_encode($respo,true);

        $this->output_compressed_data($response);
    }

    private function output_compressed_data($data)
    {
        while (ob_get_level() > 0) {ob_end_clean();}
        ob_start("ob_gzhandler");
        header('Content-type:application/json');
        echo json_encode($data);
        ob_end_flush();
        exit;
    }

    public function test_mobile_sms()
    {
        $data['name']  = "Jeeva";
        $data['phone'] = "8867161115";
        // Sms config & Checkpoint
        if (active_sms_checkpoint('booking')) {
            $msg = "Dear " . $data['name'] . " Thank you for Booking your ticket with us.Ticket Details will be sent to your email id";
            $this->provab_sms->send_msg($data['phone'], $msg);
            // return $sms_status;
        }
    }

    public function sendmail($app_reference, $booking_source, $booking_status)
    {
        $this->load->library('booking_data_formatter');
        $this->load->library('provab_mailer');
        $this->load->model('flight_model');
        if (empty($app_reference) == false) {
            $booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
            if ($booking_details['status'] == SUCCESS_STATUS) {
                load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
                //Assemble Booking Data
                $assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2b');
                $page_data['data']         = $assembled_booking_details['data'];
                //debug($page_data);exit;
                $email = $booking_details['data']['booking_details']['0']['email'];
                $this->load->library('provab_pdf');
                $create_pdf    = new Provab_Pdf();
                $mail_template = $this->template->isolated_view('voucher/flight_pdf', $page_data);
                $pdf           = $create_pdf->create_pdf($mail_template, '');
                $this->provab_mailer->send_mail($email, domain_name() . ' E-Ticket', $mail_template, $pdf);
            }
        }
    }

    public function country_list()
    {
        //$page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_code ();
        $query                  = "select * from api_country_list where name != ''";
        $resutl['country_list'] = $GLOBALS['CI']->db->query($query)->result_array();
        $this->output_compressed_data($resutl);
    }

    public function fare_details_mobile()
    {

        $response['important1'] = "Important - This information is indicative. Wizfair does not guarantee the accuracy of the information. Kindly check with the airline website for cancellation, baggage policy, date change and other travel policies. Contact our 24/7 customer service for further assistance.";

        $response['important2'] = "Name change is not permitted.";
        $response['important3'] = "Date changes will incur penalty and fees (airline penalty plus any fare difference plus our processing service fee) and is based on availability of flight at the time of change.";
        $response['important4'] = "Date change after departure must be done by the airline directly (airline penalty plus any fare difference will apply and is based on availability of flight at the time of change).";
        $response['important5'] = "Routing change will incur penalty and fees (airline penalty plus any fare difference plus our processing service fee will apply and is based on availability of flight at the time of change).";
        $response['important6'] = "Contact our 24/7 customer service to make any changes.";
        $response['important7'] = "Prior to completing the booking, please review our terms & conditions";

        echo json_encode($response);exit;

    }

    public function itinerary_details_mobile()
    {

        $response['itinerary'] = "<ul class='fair-details'><li>Baggage fees and policies displayed are for reference ONLY. For any classes of service, and OTHER destinations not listed, please view airline policy directly on the airline website.</li>
<li>Baggage fees apply each way for select flights and are subject to change by the airlines at any time. Fees reflect non-rewards member travel in most cases.</li>
<li>If a flight is 'OPERATED BY' another carrier, or there is 'MORE THAN ONE CARRIER' on your booking, the more restrictive baggage policy MAY be in effect. Please contact the carrier(s) directly to clarify this, in order to avoid additional charges.</li>
<li>We're working hard to keep this chart updated, but fees/policies change frequently! Please contact your airline directly for the latest information.</li>
<li>Visit Airline website for additional information about seats, bags, and other restrictions.</li></ul>";

        $this->output_compressed_data($response);

    }

    public function review_details_mobile()
    {

        $response['review'] = "<ul><li>Review your trip details to make sure the dates and times are correct.</li>
<li>Check your spelling. Flight passenger names must match government-issued photo ID exactly.</li>
<li>Review the terms of your booking.</li>
<li>We understand that sometimes plans change. Please contact us to know the cancel or change fee. When the airline charges such fees in accordance with its own policies, the cost will be passed on to you.</li>
<li>Please note that our Service fees are in addition to any Airline refund penalty, exchange fees and/or fare difference. Please contact our 24/7 customer care center for further assistance.</li>
<li>Please check important information regarding airline liability limitations on respective airline website</li>
<li>Prices may not include baggage fees or other fees charged directly by the airline.</li>
<li>Fares are not guaranteed until ticketed.</li>
<li>Federal law forbids the carriage of hazardous materials aboard aircraft in your luggage or on your person. Please visit airline website for complete details.</li></ul>";

        $this->output_compressed_data($response);

    }

    public function website_terms_of_use()
    {

        $response['website_terms_of_use'] = "<div class='col-md-12 col-xs-12'><div class='lblbluebold16px'><h1>WEBSITE TERMS OF USE</h1></div><div class='lblfont12px'><p></p><p>Last Revised on&nbsp;April 8, 2018</p><p><strong>AGREEMENT BETWEEN CUSTOMER AND WIZFAIR LLC.</strong></p><p>Welcome to the TheWizFair.com website (the 'Website'). This Website is provided solely to assist customers in gathering travel information, determining the availability of travel—related goods and services, making legitimate reservations or otherwise transacting business with travel suppliers, and for no other purposes. The terms 'we', 'us', 'our', “TheWizFair” and 'TheWizFair, LLC.' refer to TheWizFair, LLC., a New Jersey Company, and its subsidiaries and corporate affiliates. “TheWizFair Partner” means any co-branded and/or linked website through which we provide links, content or service. The term 'you' refers to the customer visiting the Website and/or booking a reservation through us on this Website, or through our customer service agents.&nbsp;<br><br>This Website is offered to you conditioned upon your acceptance without modification of all the terms, conditions, and notices set forth below (collectively, the 'Terms of Use” or 'Agreement').<strong>&nbsp;Please read these Terms of Use carefully, as they contain important information about limitations of liability and resolution of disputes through arbitration rather than in court</strong>. You should also read our&nbsp;Privacy Policy, which also governs your use of the Website, and is incorporated by reference in this Agreement. By accessing or using this Website, booking any reservations for travel products or services on this Website, or contacting our call center agents, you agree that the Terms of Use then in force shall apply. If you do not agree to the Terms of Use, please do not use or book any reservations through this Website or our call center agents.</p><p><strong>USE OF THE WEBSITE</strong></p><p>As a condition of your use of this Website, you warrant that:</p><ol><li>you are at least 18 years of age;</li><li>you possess the legal authority to create a binding legal obligation;</li><li>you will use this Website in accordance with these Terms of Use;</li><li>you will only use this Website to make legitimate reservations for you or for another person for whom you are legally authorized to act;</li><li>you will inform such other persons about the Terms of Use that apply to the reservations you have made on their behalf, including all rules and restrictions applicable thereto;</li><li>all information supplied by you on this Website is true, accurate, current and complete; and</li><li>if you have an TheWizFair.com account, you will safeguard your account information and will supervise and be completely responsible for any use of your account by you and anyone other than you.</li></ol><p>We retain the right at our sole discretion to deny access to anyone to this Website and the services we offer, at any time and for any reason, including, but not limited to, for violation of these Terms of Use.&nbsp;<br><br><strong>DISPUTES</strong></p><p>TheWizFair is committed to customer satisfaction, so if you have a problem or dispute, we will try to resolve your concerns. But if we are unsuccessful, you may pursue claims as explained in this section.&nbsp;<br><br>You agree to give us an opportunity to resolve any disputes or claims relating in any way to the Website, any dealings with our customer service agents, any services or products provided, any representations made by us, or our Privacy Policy (“Claims”) by contacting&nbsp;TheWizFair Customer Support&nbsp;or 1-855-949-3247. If we are not able to resolve your Claims within 60 days, you may seek relief through arbitration or in small claims court, as set forth below.&nbsp;<br><br><strong>Any and all Claims will be resolved by binding arbitration, rather than in court</strong>, except you may assert Claims on an individual basis in small claims court if they qualify. This includes any Claims you assert against us, our subsidiaries, travel suppliers or any companies offering products or services through us (which are beneficiaries of this arbitration agreement). This also includes any Claims that arose before you accepted these Terms of Use, regardless of whether prior versions of the Terms of Use required arbitration.&nbsp;<br><br><strong>There is no judge or jury in arbitration, and court review of an arbitration award is limited. However, an arbitrator can award on an individual basis the same damages and relief as a court (including statutory damages, attorneys’ fees and costs), and must follow and enforce these Terms of Use as a court would.&nbsp;</strong><br><br>Arbitrations will be conducted by the American Arbitration Association (AAA) under its rules, including the AAA Consumer Rules. Payment of all filing, administration and arbitrator fees will be governed by the AAA's rules, except as provided in this section. If your total Claims seek less than $10,000, we will reimburse you for filing fees you pay to the AAA and will pay arbitrator’s fees. You may choose to have an arbitration conducted by telephone, based on written submissions, or in person in the state where you live or at another mutually agreed location.&nbsp;<br><br>To begin an arbitration proceeding, you must send a letter requesting arbitration and describing your Claims to “TheWizFair Legal: Arbitration Claim Manager,” at TheWizFair, LLC., 401A Ralston Dr, Mount Laurel, NJ, 08054. If we request arbitration against you, we will give you notice at the email address or street address you have provided. The AAA's rules and filing instructions are available at&nbsp;<a href='https://www.adr.org/'>www.adr.org</a>&nbsp;or by calling 1-800-778-7879.&nbsp;<br><br><strong>Any and all proceedings to resolve Claims will be conducted only on an individual basis and not in a class, consolidated or representative action.&nbsp;</strong>If for any reason a Claim proceeds in court rather than in arbitration&nbsp;<strong>we each waive any right to a jury trial</strong>. The Federal Arbitration Act and federal arbitration law apply to this agreement. An arbitration decision may be confirmed by any court with competent jurisdiction.&nbsp;<br><br><strong>PROHIBITED ACTIVITIES</strong></p><p>The content and information on this Website (including, but not limited to, price and availability of travel services) as well as the infrastructure used to provide such content and information, is proprietary to us or our suppliers and providers. While you may make limited copies of your travel itinerary (and related documents) for travel or service reservations booked through this Website, you agree not to otherwise modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell or re-sell any information, software, products, or services obtained from or through this Website. Additionally, you agree not to:&nbsp;<br>&nbsp;</p><ol><li>use this Website or its contents for any commercial purpose;</li><li>make any speculative, false, or fraudulent reservation or any reservation in anticipation of demand;</li><li>access, monitor or copy any content or information of this Website using any robot, spider, scraper or other automated means or any manual process for any purpose without our express written permission;</li><li>violate the restrictions in any robot exclusion headers on this Website or bypass or circumvent other measures employed to prevent or limit access to this Website;</li><li>take any action that imposes, or may impose, in our discretion, an unreasonable or disproportionately large load on our infrastructure;</li><li>deep—link to any portion of this Website (including, without limitation, the purchase path for any travel services) for any purpose without our express written permission; or</li><li>'frame', 'mirror' or otherwise incorporate any part of this Website into any other website without our prior written authorization.</li></ol><p>If your booking or account shows signs of fraud, abuse or suspicious activity, TheWizFair may cancel any travel or service reservations associated with your name, email address or account, and close any associated TheWizFair accounts. If you have conducted any fraudulent activity, TheWizFair reserves the right to take any necessary legal action and you may be liable for monetary losses to TheWizFair, including litigation costs and damages. To contest the cancellation of a booking or freezing or closure of an account, please contact TheWizFair Customer Service.&nbsp;<br><br><strong>PRIVACY POLICY</strong></p><p>TheWizFair LLC. believes in protecting your privacy. Please click here to review our current Privacy Policy, which also governs your use of the Website and, as stated above, is incorporated by reference, to understand our practices:&nbsp;Privacy Policy.</p><p><strong>SUPPLIER RULES AND RESTRICTIONS</strong></p><p><strong>Additional terms and conditions will apply to your reservation and purchase of travel—related goods and services that you select.&nbsp;</strong>Please read these additional terms and conditions carefully. If you have purchased an airfare, please ensure you read the full terms and conditions of carriage issued by the travel supplier, which can be found on the supplier’s website. You agree to abide by the terms and conditions of purchase imposed by any supplier with whom you elect to deal, including, but not limited to, payment of all amounts when due and compliance with the supplier's rules and restrictions regarding availability and use of fares, products, or services. Airfare is only guaranteed once the purchase has been completed and the tickets have been issued. Airlines and other travel suppliers may change their prices without notice. We reserve the right to cancel your booking if full payment is not received in a timely fashion.&nbsp;<br><br>TheWizFair may offer you the opportunity to book a reservation for a combination of two one—way tickets instead of a roundtrip ticket. Combined one-way tickets may provide a greater choice of flights, are often cheaper and can be combined on the same airline or on different airlines. Unlike roundtrip tickets, each one-way ticket is subject to its own rules, restrictions, and fees. If one of these flights is affected by an airline change (e.g. cancellation or rescheduling) that causes a Customer to make changes to the other flight, the Customer will be responsible for any fees incurred for making changes to the unaffected flight.&nbsp;<br><br>For certain low-cost carriers, the price of your flight has been converted from a different currency for convenience purposes to provide you an estimate of the amount of the purchase in your local currency. As a result, when you book, the amount charged to your credit card by the airline may be slightly different due to currency fluctuations. Your statement may also include a fee from your card issuer to process the transaction plus any applicable taxes for international purchases.&nbsp;<br><br>Federal law forbids the carriage of hazardous materials aboard aircraft in your luggage or on your person. A violation can result in five years' imprisonment and penalties of $250,000 or more (49 U.S.C. 5124). Hazardous materials include explosives, compressed gases, flammable liquids and solids, oxidizers, poisons, corrosives and radioactive materials. Examples: Paints, lighter fluid, fireworks, tear gases, oxygen bottles, and radio-pharmaceuticals. There are special exceptions for small quantities (up to 70 ounces total) of medicinal and toilet articles carried in your luggage and certain smoking materials carried on your person.&nbsp;<br><br>You acknowledge that some third-party providers offering certain services and/or activities may require you to sign their liability waiver prior to participating in the service and/or activity they offer. You understand that any violation of any such supplier's rules and restrictions may result in cancellation of your reservation(s), in your being denied access to the applicable travel product or services, in your forfeiting any monies paid for such reservation(s), and/or in our debiting your account for any costs we incur as a result of such violation.&nbsp;<br><br>TheWizFair is not liable for any costs incurred due to hotel relocation.&nbsp;<br><br><strong>Sort Order :</strong></p><p>Travelers have many options on to help them find the perfect hotel, flight, car rental, cruise or activity. The “sort” settings at the top of the page allows travelers to order search results to their preference, whether based on price, verified review score, or other criteria. The “filter” settings also allow travelers to include or exclude various options to suit their travel needs. If no options are selected, we will show a range of relevant options in the search results, based on the criteria outlined below:</p><ul><li>Lodging: Our default sort order reflects the relevance of properties to your search criteria, as we want to make sure you are able to quickly and easily find the offer that is right for you. We measure relevance by taking into account factors like a property’s location, its review scores, the popularity of the property (measured by how many travelers on our sites make bookings at that property), the quality of the content provided by the property, and the competitiveness of the property’s rates and availability, all relative to other properties meeting your chosen search criteria. The compensation which a property pays us for bookings made through our sites is also a factor for the relative ranking of properties with similar offers, based on the relevance factors described above. On our non-default sorts (e.g., by price or by star rating), properties with similar results will be ordered based on the factors above.</li><li>Flights: Our default sort order is based on lowest price. In cases where two flights have the same price, the shorter flight is listed first.</li><li>Car Rental: Our default sort order is primarily driven by price, but we may consider other relevant factors such as popularity, customer reviews, convenience of pickup location, and car type or category.</li><li>Activities: Our default sort order is manually curated by TheWizFair’s destination managers familiar with each market, taking into account such factors as price, popularity, distance from hotel options, and traveler feedback.</li><li>Vacation Packages: When combining several different travel products into a vacation package, we use the criteria outlined above to determine the sort order for each product.</li></ul><p>Additionally, we continually optimize our service to provide the best experience to travelers. Accordingly, we may test different default sort order algorithms from time to time.&nbsp;<br><br><strong>PREPAID HOTEL RESERVATIONS</strong></p><p>You acknowledge that the TheWizFair Companies pre-negotiate certain room rates with hotel suppliers to facilitate the booking of reservations. You also acknowledge that the TheWizFair Companies provide you services to facilitate such booking of reservations for a consideration (the 'facilitation fee'). The room rate displayed on the Website is a combination of the pre-negotiated room rate for rooms reserved on your behalf by the TheWizFair Companies and the facilitation fee retained by the TheWizFair Companies for their services. You authorize the TheWizFair Companies to book reservations for the total reservation price, which includes the room rate displayed on the Website, plus tax recovery charges, service fees, and where applicable, taxes on the TheWizFair Companies' services. You agree that your credit card will be charged by the TheWizFair Companies for the total reservation price. Upon submitting your reservation request you authorize the TheWizFair Companies, including Travelscape, LLC, to facilitate hotel reservations on your behalf, including making payment arrangements with hotel suppliers.&nbsp;<br><br>You acknowledge that except as provided below with respect to tax obligations on the amounts we retain for our services, the TheWizFair Companies do not collect taxes for remittance to applicable taxing authorities. The tax recovery charges on prepaid hotel transactions are a recovery of the estimated taxes (e.g. sales and use, occupancy, room tax, excise tax, value added tax, etc.) that the TheWizFair Companies pay to the hotel supplier for taxes due on the hotel's rental rate for the room. The hotel suppliers invoice the TheWizFair Companies for certain charges, including tax amounts. The hotel suppliers are responsible for remitting applicable taxes to the applicable taxing jurisdictions. None of the TheWizFair Companies act as co-vendors with the supplier with whom we book or reserve our customer's travel arrangements. Taxability and the appropriate tax rate vary greatly by location. The actual tax amounts paid by the TheWizFair Companies to the hotel suppliers may vary from the tax recovery charge amounts, depending upon the rates, taxability, etc. in effect at the time of the actual use of the hotel by our customers. We retain service fees as additional compensation in servicing your travel reservation. Service fees retained by the TheWizFair Companies for their services vary based on the amount and type of hotel reservation.&nbsp;<br><br>You may cancel or change your prepaid hotel reservation, but you will be charged the cancellation or change fee indicated in the rules and restrictions for the hotel reservation. If you do not cancel or change your reservation before the cancellation policy period applicable to the hotel you reserved, which varies by hotel (usually 24 to 72 hours) prior to your date of arrival, you will be subject to a charge equal to applicable nightly rates, tax recovery charges and service fees. In the event you do not show for the first night of the reservation and plan to check-in for subsequent nights in your reservation, you must confirm the reservation changes with us no later than the date of the first night of the reservation to prevent cancellation of your reservation.&nbsp;<br><br>You agree to pay any cancellation or change fees that you incur. In limited cases, some hotels do not permit changes to or cancellations of reservations after they are made, as indicated in the rules and restrictions for the hotel reservation. You agree to abide by the Terms of Use imposed with respect to your prepaid hotel reservations.&nbsp;<br><br>Sales, use and/or local hotel occupancy taxes are imposed on the amounts that we charge for our services (service fee and/or facilitation fee) in certain jurisdictions. The actual tax amounts on our services may vary depending on the rates in effect at the time of your hotel stay.&nbsp;<br><br>You may not book reservations for more than 8 rooms online for the same hotel/stay dates. If we determine that you have booked reservations for more than 8 rooms in total in separate reservations, we may cancel your reservations, and charge you a cancellation fee, if applicable. If you paid a non-refundable deposit, your deposit will be forfeited. If you wish to book reservations for 9 or more rooms, you must contact TheWizFair’s group travel specialists by phone at (855) 949-3247 or by filling out the group travel form online. One of our group travel specialists will research your request and contact you to complete your reservation. You may be asked to sign a written contract and/or pay a nonrefundable deposit.&nbsp;<br><br>Some hotel suppliers may require you to present a credit card or cash deposit upon check-in to cover additional expenses incurred during your stay. Such deposit is unrelated to any payment received by TheWizFair, LLC. for your hotel booking.&nbsp;<br><br><strong>PAY NOW OR PAY LATER DETAILS</strong></p><p>With certain hotels, you may be presented with the option to pay online now or pay later at the hotel. If you select the “Pay Online Now” option, TheWizFair will charge the amount to your credit card in US dollars immediately. If you select “Pay Later at the Hotel”, the hotel will charge your credit card in the local currency at the time of your stay. Please note that taxes and fees vary between the two payment options. Tax rates and foreign exchange rates could change in the time between booking and stay. TheWizFair coupons may only be applied to 'Pay Online Now' bookings.&nbsp;<br><br><strong>THEWIZFAIR UNPUBLISHED RATE RESERVATIONS</strong></p><p>When available, TheWizFair, LLC.'s subsidiary, Hotwire, LLC., may offer additional, discounted hotel booking options on the Website. These hotels, called TheWizFair Unpublished Rate hotels, are different from other hotels offered on the Website in several important ways. The name and exact address of the hotel are not shown until after payment has been made for the booking. All bookings are final and cannot be changed, refunded, exchanged, cancelled, or transferred to another party. Your credit card will be charged for the amount shown even if you do not use the booking. Room type will be determined by the hotel based on the number of guests provided at time of booking. All reservations are booked for stays in non-smoking rooms (subject to availability). Hotel room assignments are determined at check-in and upgrades are not available. The maximum number of TheWizFair Unpublished Rate rooms that can be booked at one time is six. All TheWizFair Unpublished Rate rooms will be booked under the same name, and the guest under whose name the reservation is made must be present at check-in. TheWizFair Unpublished Rate hotels are not eligible for hotel reward or club programs. Upon check-in, guests must present a valid ID and credit card in their name (the amount of available credit required will vary by hotel). Debit cards may not be accepted. For information about tax recovery charges, service fees, and taxes on our services, where applicable, please see 'Prepaid Hotel Reservations.'&nbsp;<br><br><strong>BANK AND CREDIT CARD FEES</strong></p><p>Your payment is processed in the United States, when we process your payment. When the travel supplier processes your payment, it will be processed in the country in which the travel supplier is based.<br><br>TheWizFair or the travel supplier may have to verify: (i) the validity of the payment card (through a charge of a nominal value that is either refunded within a few days or deducted from the final payment due to the travel supplier) and, (ii) the availability of funds on the payment card (to be confirmed by the bank issuing the your credit card).<br><br>Some banks and credit card companies impose fees for international or cross border transactions. For instance, if you are making a booking using a US-issued card with a non-US merchant, your bank may charge you a cross border or international transaction fee. Furthermore, booking international travel may be considered an international transaction by your bank or card company, since we may pass your card details to an international travel supplier to be charged. In addition, some banks and card companies impose fees for currency conversion. For instance, if you are making a booking in a currency other than the currency of your credit card, your credit card company may convert the booking amount to the currency of your credit card and charge you a conversion fee. The currency exchange rate and foreign transaction fee is determined solely by your bank on the day that they process the transaction. If you have any questions about these fees or the exchange rate applied to your booking, please contact your bank.&nbsp;<br><br><strong>CURRENCY CONVERTER</strong></p><p>If a currency convertor is available on the Website, the following terms and conditions apply: Currency rates are based on various publicly available sources and should be used as guidelines only. Rates are not verified as accurate, and actual rates may vary. Currency quotes are not updated every day. Check the date on the currency converter feature for the day that currency was last updated. The information supplied by this application is believed to be accurate, but the TheWizFair Companies, the TheWizFair Partners, and/or our respective suppliers do not warrant or guarantee such accuracy. When using this information for any financial purpose, we advise you to consult a qualified professional to verify the accuracy of the currency rates. We do not authorize the use of this information for any purpose other than your personal use and you are expressly prohibited from the resale, redistribution, and use of this information for commercial purposes.&nbsp;&nbsp;</p><p><strong>THEWIZFAIR REWARDS</strong></p><p>The TheWizFair Rewards program (“Program”) is offered to the Website’s customers at the sole discretion of TheWizFair, LLC., and is open only to individuals who have reached the legal age of majority in their respective province/territory of residence, have a valid email address, and have enrolled in the Program.&nbsp; &nbsp;Please click here to see the Terms and Conditions of the Program.</p><p><strong>PAY WITH POINTS</strong></p><p>TheWizFair allows points from selected loyalty programs (“Points Programs”) to be redeemed for certain travel-related goods and services on the Website. If you use or register to use any points on the Website these terms and conditions apply. To be eligible to use points on the Website, you must have an TheWizFair.com account, you must have an eligible account with the provider of the Points Program that is active and in good standing, and your purchase must exceed minimum transaction value for the use of your points (if any).&nbsp;<br><br><strong>LINKING YOUR ACCOUNT</strong>: For Points Programs other than TheWizFair Rewards (Your TheWizFair Rewards account is automatically linked to your TheWizFair.com account), your Points Program account (“Account”) will be linked to your TheWizFair.com account during the registration process on the Website. TheWizFair reserves the right, in its sole discretion, to deny registration, de-link your Account, or terminate your ability to use points on TheWizFair.com for any reason. During and after registration, the Points Program provider may provide TheWizFair with information about your Account, including your points balance.&nbsp;<br><br><strong>REDEEMING POINTS</strong>: There is no fee to redeem your points at TheWizFair.com. Points may only be redeemed for eligible purchases of TheWizFair travel-related goods and services that TheWizFair will determine in its discretion. Your Points Program provider will deduct from your Account balance the amount of points you elect to use toward your eligible purchase. You may not combine points from one Points Program provider with any other loyalty points from another provider to complete a single transaction. However, if you do not have enough points to complete an eligible purchase, you may use an eligible credit card associated with your Account to complete the transaction. Eligible cards for such transactions may be limited to cards associated with the Points Program used in the transaction. TheWizFair may, in its discretion, allow you to combine points with eligible coupons.&nbsp;<br><br><strong>ADDITIONAL TERMS</strong>: Purchases made with points are subject to the point to dollar conversion rate set by the Points Program provider. The Points Program provider has the right to change the point to dollar conversion rates at any time. Also, some Points Program providers may set a minimum amount of points that may be redeemed in any transaction. Additional terms and conditions may apply and are available at the applicable Points Program provider’s website.&nbsp;<br><br><strong>INTERNATIONAL TRAVEL</strong></p><p>You are responsible for ensuring that you meet foreign entry requirements and that your travel documents, such as passports and visas (transit, business, tourist, and otherwise), are in order and any other foreign entry requirements are met. TheWizFair has no special knowledge regarding foreign entry requirements or travel documents. We urge customers to review travel prohibitions, warnings, announcements, and advisories issued by the relevant governments prior to booking travel to international destinations.&nbsp;<br><br>Passport and Visa: You must consult the relevant Embassy or Consulate for this information. Requirements may change and you should check for up-to-date information before booking and departure. We accept no liability if you are refused entry onto a flight or into any country due to your failure to carry the correct and adequate passport, visa, or other travel documents required by any airline, authority, or country, including countries you may just be transiting through. This includes all stops made by the aircraft, even if you do not leave the aircraft or airport.&nbsp;<br><br>Health: Recommended inoculations for travel may change and you should consult your doctor for current recommendations before you depart. It is your responsibility to ensure that you meet all health entry requirements, obtain the recommended inoculations, take all recommended medication, and follow all medical advice in relation to your trip.&nbsp;<br><br>Disinsection: Although not common, most countries reserve the right to disinsect aircraft if there is a perceived threat to public health, agriculture or environment. The World Health Organization and the International Civil Aviation Organization have approved the following disinsection procedures: (1) spray the aircraft cabin with an aerosolized insecticide while passengers are on board or (2) treat the aircraft's interior surfaces with a residual insecticide while passengers are not on board. For more information, see:&nbsp;<a href='http://ostpxweb.dot.gov/policy/safetyenergyenv/disinsection.htm' target='_blank'>http://ostpxweb.dot.gov/policy/safetyenergyenv/disinsection.htm</a>&nbsp;<br><br>BY OFFERING RESERVATIONS FOR TRAVEL PRODUCTS IN PARTICULAR INTERNATIONAL DESTINATIONS, THEWIZFAIR DOES NOT REPRESENT OR WARRANT THAT TRAVEL TO SUCH AREAS IS ADVISABLE OR WITHOUT RISK, AND IS NOT LIABLE FOR DAMAGES OR LOSSES THAT MAY RESULT FROM TRAVEL TO SUCH DESTINATIONS.&nbsp;<br><br><strong>LIABILITY DISCLAIMER</strong></p><p><strong>The Information, Software, Products and Services published on this Website may include inaccuracies or errors, including pricing errors. In particular, the TheWizFair Companies and TheWizFair Partners do not guarantee the accuracy of, and disclaim all liability for any errors or other inaccuracies relating to the information and description of the hotel, air, cruise, car and other travel products and services displayed on this Website (including, without limitation, the pricing, photographs, list of hotel amenities, general product descriptions, etc.). In addition, TheWizFair, LLC. expressly reserves the right to correct any pricing errors on our Website and/or reservations made under an incorrect price. In such event, if available, we will offer you the opportunity to keep your reservation at the correct price or we will cancel your reservation without penalty.&nbsp;</strong><br><br><strong>Hotel ratings displayed on this Website are intended as only general guidelines, and the TheWizFair Companies and TheWizFair Partners do not guarantee the accuracy of the ratings. The TheWizFair Companies, the TheWizFair Partners and their respective suppliers make no guarantees about the availability of specific products and services. The TheWizFair Companies, the TheWizFair Partners and their respective suppliers may make improvements and/or changes on the Website at any time.&nbsp;</strong><br><br><strong>The TheWizFair Companies, the TheWizFair Partners and their respective suppliers make no representations about the suitability of the information, software, products and services contained on this Website for any purpose, and the inclusion or offering of any products or services on this Website does not constitute any endorsement or recommendation of such products or services by the TheWizFair Companies or the TheWizFair Partners. All such information, software products, and services are provided “as is” without warranty of any kind. The TheWizFair Companies, the TheWizFair Partners and their respective suppliers disclaim all warranties and conditions that this Website, its servers or any email sent from the TheWizFair Companies, the TheWizFair Partners and/or their respective suppliers are free of viruses or other harmful components. The TheWizFair Companies, the TheWizFair Partners and their respective suppliers hereby disclaim all warranties and conditions with regard to this information, software, products and services, including all implied warranties and conditions of merchantability, fitness for a particular response, title and non-infringement.&nbsp;</strong><br><br><strong>The carriers, hotels and other suppliers providing travel or other services on this Website are independent contractors and not agents or employees of the TheWizFair Companies or the TheWizFair Partners. The TheWizFair Companies and the TheWizFair Partners are not liable for the acts, errors, omissions, representations, warranties, breaches or negligence of any such suppliers or for any personal injuries, death, property damage, or other damages or expenses resulting there from. The TheWizFair Companies and the TheWizFair Partners have no liability and will make no refund in the event of any delay, cancellation, overbooking, strike, force majeure or other causes beyond their direct control, and they have no responsibility for any additional expenses, omissions, delays, re-routing or acts of any government or authority.&nbsp;</strong><br><br><strong>In no event shall the TheWizFair Companies, the TheWizFair Partners and/or their respective suppliers be liable for any direct, indirect, punitive, incidental, special or consequential damages arising out of, or in any way connected with, your access to, display of or use of this Website or with the delay or inability to access, display or use this Website (including, but not limited to, your reliance upon opinions appearing on this Website; any computer viruses, information, software, linked sites, products and services obtaining through this Website; or otherwise arising out of the access to, display of or use of this Website) whether based on a theory of negligence, contract, tort, strict liability, consumer protection statutes, or otherwise, and even if the TheWizFair Companies, the TheWizFair Partners and/or their respective suppliers have been advised of the possibility of such damages.&nbsp;</strong><br><br>If, despite the limitation above, the TheWizFair Companies, the TheWizFair Partners or their respective suppliers are found liable for any loss or damage which arises out of or in any way connected with any of the occurrences described above, then the liability of the TheWizFair Companies, the TheWizFair Partners and/or their respective suppliers will in no event exceed, in the aggregate, the greater of (a) the service fees you paid to TheWizFair, LLC. in connection with such transaction(s) on this Website, or (b) One-Hundred Dollars (US$100.00) or the equivalent in local currency.&nbsp;<br><br>The limitation of liability reflects the allocation of risk between the parties. The limitations specified in this section will survive and apply even if any limited remedy specified in these Terms of Use is found to have failed of its essential purpose. The limitations of liability provided in these Terms of Use inure to the benefit of the TheWizFair Companies, the TheWizFair Partners, and/or their respective suppliers.&nbsp;<br><br><strong>INDEMNIFICATION</strong></p><p>You agree to defend and indemnify the TheWizFair Companies, the TheWizFair Partners, and/or their respective suppliers and any of their officers, directors, employees and agents from and against any claims, causes of action, demands, recoveries, losses, damages, fines, penalties or other costs or expenses of any kind or nature including but not limited to reasonable legal and accounting fees, brought by third parties as a result of:&nbsp;</p><ol><li>your breach of these Terms of Use or the documents referenced herein;</li><li>your violation of any law or the rights of a third party; or</li><li>your use of this Website.</li></ol><p><strong>LINKS TO THIRD-PARTY SITES</strong></p><p>This Website may contain hyperlinks to websites operated by parties other than TheWizFair, LLC. Such hyperlinks are provided for your reference only. We do not control such websites and are not responsible for their contents or the privacy or other practices of such websites. Further, it is up to you to take precautions to ensure that whatever links you select or software you download (whether from this Website or other websites) is free of such items as viruses, worms, trojan horses, defects and other items of a destructive nature. Our inclusion of hyperlinks to such websites does not imply any endorsement of the material on such websites or any association with their operators.&nbsp;<br><br><strong>SOFTWARE AVAILABLE ON THIS WEBSITE</strong></p><p>Any software that we make available to download from this Website ('Software') or through your mobile application store, including the TheWizFair mobile application (the 'Mobile Application') is the copyrighted work of the TheWizFair Companies and/or our respective suppliers. Your use of such Software is governed by the terms of the end user license agreement, if any, which accompanies, or is included with, the Software ('License Agreement'). You may not install or use any Software that is accompanied by or includes a License Agreement unless you first agree to the License Agreement terms. For any Software made available for download on this Website not accompanied by a License Agreement, we hereby grant to you, the user, a limited, personal, non-exclusive, nontransferable license to download, install and use the Software and/or the Mobile Application for viewing and otherwise using this Website and/or accessing the content and information available within the Mobile Application (including, without limitation, price and availability of travel services) in accordance with these Terms of Use and for no other purpose.&nbsp;<br><br>Please note that all Software, including, without limitation, all HTML code and Active X controls contained on this Website, is owned by the TheWizFair Companies, TheWizFair Partners, and/or our respective suppliers, and is protected by copyright laws and international treaty provisions. Any reproduction or redistribution of the Software is expressly prohibited, and may result in severe civil and criminal penalties. Violators will be prosecuted to the maximum extent possible&nbsp;<br><br>WITHOUT LIMITING THE FOREGOING, COPYING OR REPRODUCTION OF THE SOFTWARE TO ANY OTHER SERVER OR LOCATION FOR FURTHER REPRODUCTION OR REDISTRIBUTION IS EXPRESSLY PROHIBITED. THE SOFTWARE IS WARRANTED, IF AT ALL, ONLY ACCORDING TO THE TERMS OF THE LICENSE AGREEMENT.&nbsp;<br><br>Your mobile device must be connected to the internet for the Mobile Application to function correctly. You are responsible for making all arrangements necessary for your device to have internet connectivity and are responsible for all sums your service provider may charge you arising out of the Mobile Application transmitting and receiving data (including but not limited to data roaming charges). As further described in our Privacy Policy, the Mobile Application will automatically transfer a small amount of data as part of its normal operation, including how you use the Mobile Application, which Content you access, and technical errors or problems which the Application may encounter while being used. By using the Mobile Application, you acknowledge, agree and consent to the automatic collection of this information.&nbsp;<br><br><strong>REVIEWS, COMMENTS, PHOTOS AND OTHER SUBMISSIONS</strong></p><p>We appreciate hearing from you. Please be aware that by submitting content to this Website by electronic mail, postings on this Website or otherwise, including any hotel reviews, photos, videos, questions, comments, suggestions, ideas or the like contained in any submissions (collectively, 'Submissions'), you grant TheWizFair and the affiliated, co-branded and/or linked website partners through whom we provide service (collectively, the 'TheWizFair Partners'), a nonexclusive, royalty-free, perpetual, transferable, irrevocable and fully sub-licensable right to (a) use, reproduce, modify, adapt, translate, distribute, publish, create derivative works from and publicly display and perform such Submissions throughout the world in any media, now known or hereafter devised; and (b) use the name that you submit in connection with such Submission. You acknowledge that the TheWizFair Companies may choose to provide attribution of your comments or reviews (for example, listing your name and hometown on a hotel review that you submit) at our discretion, and that such submissions may be shared with our supplier partners. You further grant the TheWizFair Companies the right to pursue at law any person or entity that violates your or the TheWizFair Companies' rights in the Submissions by a breach of these Terms of Use. You acknowledge and agree that Submissions are non-confidential and non-proprietary. You expressly waive any and all ‘moral rights’ (including rights of attribution or integrity) that may subsist in your Submissions and agree that you have no objection to the publication, use, modification, deletion or exploitation of your Submissions by us, the TheWizFair Partners or any of our partners or licensees. We take no responsibility and assume no liability for any Submissions posted or submitted by you. We have no obligation to post your comments; we reserve the right in our absolute discretion to determine which comments are published on the Website. If you do not agree to these Terms of Use, please do not provide us with any Submissions.&nbsp;<br><br>You are fully responsible for the content of your Submissions, (specifically including, but not limited to, reviews posted to this Website). You are prohibited from posting or transmitting to or from this Website: (i) any unlawful, threatening, libelous, defamatory, obscene, pornographic, or other material or content that would violate rights of publicity and/or privacy or that would violate any law; (ii) any commercial material or content (including, but not limited to, solicitation of funds, advertising, or marketing of any good or services); and (iii) any material or content that infringes, misappropriates or violates any copyright, trademark, patent right or other proprietary right of any third party. You shall be solely liable for any damages resulting from any violation of the foregoing restrictions, or any other harm resulting from your posting of content to this Website. You acknowledge that TheWizFair may exercise its rights (e.g. use, publish, delete) to any content you submit without notice to you. If you submit more than one review for the same hotel, only your most recent submission is eligible for use.&nbsp;<br><br>From time to time we may offer customers incentives to leave hotel reviews (e.g. discount coupon/entry into prize draws etc.)As it is important to us that hotel reviews are impartial and honest, these incentives will be available to customers regardless of whether the hotel review is positive or negative.&nbsp;<br><br><br>TheWizFair claims no ownership, affiliation with, or endorsement of any photos that are submitted by end users through our sites.&nbsp;<br><br>TheWizFair’s policies with respect to claims by third parties that the content of the Website, including the content of any Submissions, infringes the copyrights owned by said third party can be found in the&nbsp;Claims of Copyright Infringement&nbsp;section below.&nbsp;<br><br>The Google® Translate tool is made available on this Website to enable you to translate content such as user-generated hotel reviews. The Google® Translate tool uses an automated process to translate text and this may result in inaccuracies. Your use of the Google® Translate tool is entirely at your own risk. TheWizFair, LLC. does not warrant or make any promises, assurances or guarantees as to the accuracy or completeness of the translations provided by Google® Translate.&nbsp;</p><p><strong>COPYRIGHT AND TRADEMARK NOTICES</strong></p><p>All contents of this Website are ©2019 TheWizFair, LLC. All rights reserved. TheWizFair.us, TheWizFair.com, TheWizFair.in, the TheWizFair logo, the TheWizFair Affiliate Network logo, and the Airplane logo are either registered trademarks or trademarks of TheWizFair, LLC. in the U.S. and/or other countries. Other logos and product and company names mentioned herein may be the trademarks of their respective owners. TheWizFair, LLC. is not responsible for content on websites operated by parties other than TheWizFair, LLC.&nbsp;<br><br>If you are aware of an infringement of our brand, please let us know by emailing us at&nbsp;Support@TheWizFair.com.<br><br>&nbsp;</p><p><strong>ACCOUNT TERMINATION</strong></p><p>In accordance with the Digital Millennium Copyright Act ('DMCA') and other applicable law, TheWizFair has adopted a policy of terminating, in appropriate circumstances and at TheWizFair's sole discretion, subscribers or account holders who are deemed to be repeat infringers. TheWizFair may also at its sole discretion limit access to the Website and/or terminate the accounts of any users who infringe any intellectual property rights of others, whether or not there is any repeat infringement. If you believe that an account holder or subscriber is a repeat infringer, please provide information sufficient for us to verify that the account holder or subscriber is a repeat infringer when filing your notice.&nbsp;<br><br>&nbsp;</p><p><strong>NOTICE OF INFRINGING MATERIAL</strong></p><p>If you believe in good faith that materials hosted by us infringe your copyright, you (or your agent) may send us a written notice that includes the following information. Please note that we will not process your complaint if it is not properly filled out or is incomplete. Any misrepresentations in your notice regarding whether content or activity is infringing may expose you to liability for damages.&nbsp;<br>&nbsp;</p><ol><li>A clear identification of the copyrighted work you claim was infringed.</li><li>A clear identification of the material you claim is infringing on the Website, such as a link to the infringing material.</li><li>Your address, email address and telephone number.</li><li>A statement that you have a 'good faith belief that the material that is claimed as copyright infringement is not authorized by the copyright owner, its agent, or the law.'</li><li>A statement that 'the information in the notification is accurate, and under penalty of perjury, the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.'</li><li>A signature by the person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li></ol><p><a name='copyright'></a>You may send us your notice by email to support@TheWizFair.com<br>&nbsp;</p><p><strong>COUNTER NOTICES</strong></p><p>If material you have posted has been taken down, you may file a counter-notification by regular mail that sets forth the items specified below. You may want to seek legal counsel prior to doing so. Please include the following details:&nbsp;<br>&nbsp;</p><ol><li>Identification of the specific content that was removed or disabled and the location that content appeared on TheWizFair’s Website. Please provide the URL address if possible.</li><li>Your name, mailing address, telephone number and email address.</li><li>A statement that you consent to the jurisdiction of Federal District Court for the judicial district in which your address is located, or if your address is outside of the United States, for any judicial district in which TheWizFair, LLC. may be found, and that you will accept service of process from the party who reported your content, or that party’s agent.</li><li>The following statement: 'I swear, under penalty of perjury, that I have a good faith belief that the content identified above was removed or disabled as a result of a mistake or misidentification.”</li></ol><p><br>Sign the paper.&nbsp;<br>Send the written communication to the following address:&nbsp;<br><br>TheWizFair, LLC.<br>Attn: IP/Trademark Legal Dept., DMCA Complaints<br>401A Ralston Dr<br>Mount Laurel NJ 08054&nbsp;<br><br>OR you may email it to: support@thewizfair.com, Attn: IP/Trademark Legal Dept., DMCA Complaints For any additional questions regarding the DMCA process for TheWizFair, please contact us at (855) 949-3247.&nbsp;<br><br>&nbsp;</p><p><strong>PATENT NOTICES</strong></p><p>One or more patents owned by the TheWizFair Companies may apply to this Website and to the features and services accessible via the Website. Portions of this Website operate under license of one or more patents. Other patents pending.&nbsp;<br><br><strong>MAP TERMS</strong></p><p>Your use of mapping available on this Website is governed by the Microsoft Terms of Use and Microsoft Privacy Statement and the Google Terms of Use and Google Privacy Statement. Microsoft and Google reserve the right to change their Terms of Use and Privacy Statements at any time, at their sole discretion. Please click here for additional information:&nbsp;<br><br><a href='http://privacy.microsoft.com/en-us/default.mspx' target='_blank'>http://privacy.microsoft.com/en-us/default.mspx</a><br><a href='http://www.microsoft.com/maps/assets/docs/terms.aspx' target='_blank'>http://www.microsoft.com/maps/assets/docs/terms.aspx</a><br><a href='http://www.google.com/privacy/privacy-policy.html' target='_blank'>http://www.google.com/privacy/privacy-policy.html</a><br><a href='http://www.google.com/enterprise/earthmaps/legal/us/maps_AUP.html' target='_blank'>http://www.google.com/enterprise/earthmaps/legal/us/maps_AUP.html</a><br><a href='http://www.maps.google.com/help/legalnotices_maps.html' target='_blank'>http://www.maps.google.com/help/legalnotices_maps.html</a><br><a href='http://maps.google.com/help/terms_maps.html' target='_blank'>http://maps.google.com/help/terms_maps.html</a><br><br>OpenStreetMap geo data used in mapping is (c) OpenStreetMap contributors and available under the&nbsp;<a href='https://opendatacommons.org/licenses/odbl/'>Open Database License (ODbL)</a><br><br>&nbsp;</p><p><strong>GENERAL</strong></p><p>These Terms of Use are governed by the Federal Arbitration Act, federal arbitration law, and for reservations made by U.S. residents, the laws of the state in which your billing address is located, without regard to principles of conflicts of laws. Use of this Website is unauthorized in any jurisdiction that does not give effect to all provisions of this Agreement, including, without limitation, this paragraph.&nbsp;&nbsp;<br><br>You agree that no joint venture, partnership, or employment relationship exists between you and the TheWizFair Companies as a result of this Agreement or use of this Website.&nbsp;<br><br>Our performance of this Agreement is subject to existing laws and legal process, and nothing contained in this Agreement limits our right to comply with law enforcement or other governmental or legal requests or requirements relating to your use of this Website or information provided to or gathered by us with respect to such use.&nbsp;<br><br>If any part of this Agreement is found to be invalid, illegal or unenforceable, the validity, legality and enforceability of the remaining provisions will not in any way be affected or impaired. Our failure or delay in enforcing any provision of this Agreement at any time does not waive our right to enforce the same or any other provision(s) hereof in the future.&nbsp;<br><br>This Agreement (and any other terms and conditions or policies referenced herein) constitutes the entire agreement between you and TheWizFair, LLC. with respect to this Website, our services and your dealings and relationships with us, and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral, or written, between you and us. A printed version of this Agreement and of any notice given in electronic form shall be admissible in judicial, arbitration or any other administrative proceedings to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form.&nbsp;<br><br>Fictitious names of companies, products, people, characters, and/or data mentioned on this Website are not intended to represent any real individual, company, product, or event.&nbsp;<br><br>Any rights not expressly granted herein are reserved.&nbsp;<br><br>&nbsp;</p><p><strong>REGISTRATIONS</strong></p><p>Seller of Travel: WizFair LLC. is registered in the state of New Jersey.&nbsp;<br>&nbsp;</p><p><br><strong>SERVICE HELP</strong></p><p>For quick answers to your questions or ways to contact us, visit our Customer Support Center. Or, you can write to us at:&nbsp;<br>&nbsp;</p><p>Attn: Customer Service&nbsp;</p><p>TheWizFair, LLC.&nbsp;</p><p>401A Ralston Dr</p><p>Mount Laurel, NJ, 08054</p><p>&nbsp;</p><p></p></div></div>";

        $this->output_compressed_data($response);

    }

    public function payment_security()
    {

        $response['payment_security'] = "<div class='col-md-12 col-xs-12'><div class='lblbluebold16px'><h1>PAYMENT SECURITY</h1></div><div class='lblfont12px'><p></p><p>It is of paramount importance to us that all the transactions you conduct on our site are carried out in a safe and secure environment to your satisfaction. To achieve this, thewizfair.com and wizfair.us are protected by Secured Socket Layer technology. Your credit card details are transmitted in an encrypted form over the internet, so that no one else can read them. Your card details are shared with our service providers to the extent deemed to be necessary. If you have any additional queries or concerns please email us and we will reply to your queries with 24 hours.</p><p></p></div></div>";

        $this->output_compressed_data($response);

    }

    public function best_vacation_package()
    {

        $response['best_vacation_package'] = "<div class='col-md-12 col-xs-12'><div class='lblbluebold16px'><h1>Best Vacation Package</h1></div><div class='lblfont12px'><p></p><h2>ASIA HOLIDAYS PACKAGES</h2><!--  <p class='color-grey'>Curabitur nunc erat, consequat in erat ut, congue bibendum nulla. Suspendisse id tor.</p> --><h3>Dubai</h3><!-- <h4>from <b>$860</b></h4>   --><p><img alt='' src='http://wizfairvacation.com/img/packages/asia/dubai_p.jpg'> <strong>254 Review</strong></p><p><img alt='' src='img/calendar_icon.png'> <strong>3D/2N</strong></p><p><a href='dubai_package_list.php'><img alt='' src='img/flag_icon.png'>view more</a>hongkong_p</p><h3>Bali</h3><p><img alt='' src='img/people_icon.png'> <strong>222 Review</strong></p><p><img alt='' src='img/calendar_icon.png'> <strong>5D/4N</strong></p><p><a href='bali_package_list.php'><img alt='' src='img/flag_icon.png'>view more</a></p><h3>Hongkong &amp; Macau</h3><p>from <strong>$600</strong></p><p><img alt='' src='img/people_icon.png'> <strong>187 Review</strong></p><p><img alt='' src='img/calendar_icon.png'> <strong>7D/6N</strong></p><p><a href='hongkong-Macau-package_list.php'><img alt='' src='img/flag_icon.png'>view more</a></p><h3>THAILAND</h3><p><img alt='' src='img/people_icon.png'> <strong>275 Review</strong></p><p><img alt='' src='img/calendar_icon.png'> <strong>7D/6N</strong></p><p><a href='thailand-package_list.php'><img alt='' src='img/flag_icon.png'>view more</a></p><h3>SINGAPORE</h3><p><img alt='' src='img/people_icon.png'> <strong>215 Review</strong></p><p><img alt='' src='img/calendar_icon.png'> <strong>11D/10N</strong></p><p><a href='singapore-package_list.php'><img alt='' src='img/flag_icon.png'>view more</a></p><h3>MALAYSIA</h3><p><img alt='' src='img/people_icon.png'> <strong>217 Review</strong></p><p><img alt='' src='img/calendar_icon.png'> <strong>13D/12N </strong></p><p><a href='malaysia-package_list.php'><img alt='' src='img/flag_icon.png'>view more</a></p><h3>Vietnam</h3><p><img alt='' src='img/people_icon.png'> <strong>217 Review</strong></p><p><img alt='' src='img/calendar_icon.png'> <strong>13D/12N </strong></p><p><a href='vietnam-package_list.php'><img alt='' src='img/flag_icon.png'>view more</a></p><h3>INDIA</h3><p><img alt='' src='img/people_icon.png'> <strong>217 Review</strong></p><p><img alt='' src='img/calendar_icon.png'> <strong>13D/12N </strong></p><p><a href='india-package_list.php'><img alt='' src='img/flag_icon.png'>view more</a></p><p></p></div></div>";

        $this->output_compressed_data($response);

    }

    public function terms_condition()
    {

        $response['terms_condition'] = "<div class='col-md-12 col-xs-12'><div class='lblbluebold16px'><h1>Terms &amp; Condition</h1></div><div class='lblfont12px'><p></p><p><strong>AGREEMENT BETWEEN USER AND WIZFAIR</strong></p><p>APPLICABILITY OF THE AGREEMENT:</p><p>This agreement ('user agreement') incorporates the terms and conditions for Wizfair LLC and its affiliate Companies ('Wizfair') to provide services to the person (s) ('the User') intending to purchase or inquiring for any products and/ or services of WIZFAIR by using WIZFAIR's websites or using any other customer interface channels of WIZFAIR which includes its sales persons, offices, call centers, advertisements, information campaigns etc.</p><p>&nbsp;</p><p>Both User and WIZFAIR are individually referred as 'party' to the agreement and collective referred to as 'parties'.</p><p>&nbsp;</p><p>&nbsp;<strong>USER'S RESPONSIBILITY OF COGNIZANCE OF THIS AGREEMENT</strong></p><p>The Users availing services from WIZFAIR shall be deemed to have read, understood and expressly accepted the terms and conditions of this agreement, which shall govern the desired transaction or provision of such services by WIZFAIR for all purposes, and shall be binding on the User. All rights and liabilities of the User and/or WIZFAIR with respect to any services to be provided by WIZFAIR shall be restricted to the scope of this agreement.</p><p>&nbsp;</p><p>WIZFAIR reserves the right, in its sole discretion, to terminate the access to any or all WIZFAIR websites or its other sales channels and the related services or any portion thereof at any time, without notice, for general maintenance or any reason what so ever.</p><p>&nbsp;</p><p>In addition to this Agreement, there are certain terms of service (TOS) specific to the services rendered/ products provided by WIZFAIR like the air tickets, MICE, bus, rail, holiday packages etc. Such TOS will be provided/ updated by WIZFAIR which shall be deemed to be a part of this Agreement and in the event of a conflict between such TOS and this Agreement, the terms of this Agreement shall prevail. The User shall be required to read and accept the relevant TOS for the service/ product availed by the User.</p><p>&nbsp;</p><p>Additionally, the Service Provider itself may provide terms and guidelines that govern particular features, offers or the operating rules and policies applicable to each Service (for example, flights, hotel reservations, packages, etc.). The User shall be responsible for ensuring compliance with the terms and guidelines or operating rules and policies of the Service Provider with whom the User elects to deal, including terms and conditions set forth in a Service Providers' fare rules, contract of carriage or other rules.</p><p>&nbsp;</p><p>WIZFAIR's Services are offered to the User conditioned on acceptance without modification of all the terms, conditions and notices contained in this Agreement and the TOS, as may be applicable from time to time. For the removal of doubts, it is clarified that availing of the Services by the User constitutes an acknowledgement and acceptance by the User of this Agreement and the TOS. If the User does not agree with any part of such terms, conditions and notices, the User must not avail WIZFAIR's Services.</p><p>&nbsp;</p><p>In the event that any of the terms, conditions, and notices contained herein conflict with the Additional Terms or other terms and guidelines contained within any other WIZFAIR document, then these terms shall control.</p><p>&nbsp;</p><p><strong>THIRD PARTY ACCOUNT INFORMATION</strong></p><p>By using the Account Access service in WIZFAIR's websites, the User authorizes WIZFAIR and its agents to access third party sites, including that of Banks and other payment gateways, designated by them or on their behalf for retrieving requested information</p><p>&nbsp;</p><p>While registering, the User will choose a password and is responsible for maintaining the confidentiality of the password and the account.</p><p>&nbsp;</p><p>The User is fully responsible for all activities that occur while using their password or account. It is the duty of the User to notify WIZFAIR immediately in writing of any unauthorized use of their password or account or any other breach of security. WIZFAIR will not be liable for any loss that may be incurred by the User as a result of unauthorized use of his password or account, either with or without his knowledge. The User shall not use anyone else's password at any time</p><p>&nbsp;</p><p><strong>&nbsp;FEES PAYMENT</strong></p><p>WIZFAIR reserves the right to charge listing fees for certain listings, as well as transaction fees based on certain completed transactions using the services. WIZFAIR further reserves the right to alter any and all fees from time to time, without notice.</p><p>&nbsp;</p><p>The User shall be completely responsible for all charges, fees, duties, taxes, and assessments arising out of the use of the services.</p><p>&nbsp;</p><p>In case, there is a short charging by WIZFAIR for listing, services or transaction fee or any other fee or service because of any technical or other reason, it reserves the right to deduct/charge/claim the balance subsequent to the transaction at its own discretion.</p><p>&nbsp;</p><p>In the rare possibilities of the reservation not getting confirmed for any reason whatsoever, we will process the refund and intimate you of the same. Wizfair is not under any obligation to make another booking in lieu of or to compensate/ replace the unconfirmed one. All subsequent further bookings will be treated as new transactions with no reference to the earlier unconfirmed reservation.</p><p>&nbsp;</p><p>The User shall request Wizfair for any refunds against the unutilized or 'no show' air or hotel booking for any reasons within 90 days from the date of departure for the air ticket and/or the date of check in for the hotel booking. Any applicable refunds would accordingly be processed as per the defined policies of Airlines, hotels and Wizfair as the case may be. No refund would be payable for any requests made after the expiry of 90 days as above and all unclaimed amounts for such unutilized or ‘no show’ air or hotel booking shall be deemed to have been forfeited.</p><p>&nbsp;</p><p><strong>CONFIDENTIALITY</strong></p><p>Any information which is specifically mentioned by WIZFAIR as Confidential shall be maintained confidentially by the user and shall not be disclosed unless as required by law or to serve the purpose of this agreement and the obligations of both the parties therein.</p><p>&nbsp;</p><p><strong>USAGE OF THE MOBILE NUMBER OF THE USER BY WIZFAIR</strong></p><p>WIZFAIR may send booking confirmation, itinerary information, cancellation, payment confirmation, refund status, schedule change or any such other information relevant for the transaction, via SMS or by voice call on the contact number given by the User at the time of booking; WIZFAIR may also contact the User by voice call, SMS or email in case the User couldn’t or hasn’t concluded the booking, for any reason what so ever, to know the preference of the User for concluding the booking and also to help the User for the same. The User hereby unconditionally consents that such communications via SMS and/ or voice call by WIZFAIR is (a) upon the request and authorization of the User, (b) ‘transactional’ and not an ‘unsolicited commercial communication’. The User will indemnify WIZFAIR against all types of losses and damages incurred by WIZFAIR or any other authority due to any erroneous compliant raised by the User on WIZFAIR with respect to the intimations mentioned above or due to a wrong number or email id being provided by the User for any reason whatsoever.</p><p>&nbsp;</p><p><strong>&nbsp;ONUS OF THE USER</strong></p><p>WIZFAIR is responsible only for the transactions that are done by the User through WIZFAIR. WIZFAIR will not be responsible for screening, censoring or otherwise controlling transactions, including whether the transaction is legal and valid as per the laws of the land of the User.</p><p>&nbsp;</p><p>The User warrants that they will abide by all such additional procedures and guidelines, as modified from time to time, in connection with the use of the services. The User further warrants that they will comply with all applicable laws and regulations regarding use of the services with respect to the jurisdiction concerned for each transaction.</p><p>&nbsp;</p><p>The User represent and confirm that the User is of legal age to enter into a binding contract and is not a person barred from availing the Services under the laws of India or other applicable law.</p><p>&nbsp;</p><p><strong>ADVERTISERS ON WIZFAIR OR LINKED WEBSITES</strong></p><p>WIZFAIR is not responsible for any errors, omissions or representations on any of its pages or on any links or on any of the linked website pages. WIZFAIR does not endorse any advertiser on its web pages in any manner. The Users are requested to verify the accuracy of all information on their own before undertaking any reliance on such information.</p><p>&nbsp;</p><p>The linked sites are not under the control of WIZFAIR and WIZFAIR is not responsible for the contents of any linked site or any link contained in a linked site, or any changes or updates to such sites. WIZFAIR is providing these links to the Users only as a convenience and the inclusion of any link does not imply endorsement of the site by WIZFAIR.</p><p>&nbsp;</p><p><strong>INSURANCE</strong></p><p>Unless explicitly provided by WIZFAIR in any specific service or deliverable, obtaining sufficient insurance coverage is the obligation/option of the user and WIZFAIR doesn't accept any claims arising out of such scenarios.</p><p>&nbsp;</p><p>Insurance, if any provided as a part of the service/ product by WIZFAIR shall be as per the terms and conditions of the insuring company. The User shall contact the insurance company directly for any claims or disputes and WIZFAIR shall not provide any express or implied undertakings for acceptance of the claims by the insurance company.</p><p>&nbsp;</p><p><strong>FORCE MAJURE CIRCUMSTANCES</strong></p><p>The user agrees that there can be exceptional circumstances where the service operators like the airlines, hotels, the respective transportation providers or concerns may be unable to honor the confirmed bookings due to various reasons like climatic conditions, labor unrest, insolvency, business exigencies, government decisions, operational and technical issues, route and flight cancellations etc. If WIZFAIR is informed in advance of such situations where dishonor of bookings may happen, it will make its best efforts to provide similar alternative to its customers or refund the booking amount after reasonable service charges, if supported and refunded by that respective service operators. The user agrees that WIZFAIR being an agent for facilitating the booking services shall not be responsible for any such circumstances and the customers have to contact that service provider directly for any further resolutions and refunds.</p><p>&nbsp;</p><p>The User agrees that in situations due to any technical or other failure in WIZFAIR, services committed earlier may not be provided or may involve substantial modification. In such cases, WIZFAIR shall refund the entire amount received from the customer for availing such services minus the applicable cancellation, refund or other charges, which shall completely discharge any and all liabilities of WIZFAIR against such non-provision of services or deficiencies. Additional liabilities, if any, shall be borne by the User.</p><p>&nbsp;</p><p>WIZFAIR shall not be liable for delays or inabilities in performance or nonperformance in whole or in part of its obligations due to any causes that are not due to its acts or omissions and are beyond its reasonable control, such as acts of God, fire, strikes, embargo, acts of government, acts of terrorism or other similar causes, problems at airlines, rails, buses, hotels or transporters end. In such event, the user affected will be promptly given notice as the situation permits.</p><p>&nbsp;</p><p>Without prejudice to whatever is stated above, the maximum liability on part of WIZFAIR arising under any circumstances, in respect of any services offered on the site, shall be limited to the refund of total amount received from the customer for availing the services less any cancellation, refund or others charges, as may be applicable. In no case the liability shall include any loss, damage or additional expense whatsoever beyond the amount charged by WIZFAIR for its services.</p><p>&nbsp;</p><p>In no event shall WIZFAIR and/or its suppliers be liable for any direct, indirect, punitive, incidental, special, consequential damages or any damages whatsoever including, without limitation, damages for loss of use, data or profits, arising out of or in any way connected with the use or performance of the WIZFAIR website(s) or any other channel . Neither shall WIZFAIR be responsible for the delay or inability to use the WIZFAIR websites or related services, the provision of or failure to provide services, or for any information, software, products, services and related graphics obtained through the WIZFAIR website(s), or otherwise arising out of the use of the WIZFAIR website(s), whether based on contract, tort, negligence, strict liability or otherwise.</p><p>&nbsp;</p><p>WIZFAIR is not responsible for any errors, omissions or representations on any of its pages or on any links or on any of the linked website pages.</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>SAFETY OF DATA DOWNLOADED</strong></p><p>The User understands and agrees that any material and/or data downloaded or otherwise obtained through the use of the Service is done entirely at their own discretion and risk and they will be solely responsible for any damage to their computer systems or loss of data that results from the download of such material and/or data.</p><p>&nbsp;</p><p>Nevertheless, WIZFAIR will always make its best endeavors to ensure that the content on its websites or other information channels are free of any virus or such other malwares.</p><p>&nbsp;</p><p><strong>FEEDBACK FROM CUSTOMER AND SOLICITATION:</strong></p><p>The User is aware that Wizfair provides various services like hotel bookings, car rentals, holidays and would like to learn about them, to enhance his / her travel experience. The User hereby specifically authorizes Wizfair to contact the User with offers on various services offered by it through direct mailers, e-mailers, telephone calls, short messaging services (SMS) or any other medium, from time to time. In case that the customer chooses not to be contacted, he /she shall write to Wizfair for specific exclusion at support@thewizfair.com or provide his / her preferences to the respective service provider. The customers are advised to read and understand the privacy policy of WIZFAIR on its website in accordance of which WIZFAIR contacts, solicits the user or shares the user's information.</p><p>&nbsp;</p><p><strong>PROPRIETARY RIGHTS</strong></p><p>WIZFAIR may provide the User with contents such as sound, photographs, graphics, video or other material contained in sponsor advertisements or information. This material may be protected by copyrights, trademarks, or other intellectual property rights and laws.</p><p>&nbsp;</p><p>The User may use this material only as expressly authorized by WIZFAIR and shall not copy, transmit or create derivative works of such material without express authorization.</p><p>&nbsp;</p><p>The User acknowledges and agrees that he/she shall not upload post, reproduce, or distribute any content on or through the Services that is protected by copyright or other proprietary right of a third party, without obtaining the written permission of the owner of such right.</p><p>&nbsp;</p><p>Any copyrighted or other proprietary content distributed with the consent of the owner must contain the appropriate copyright or other proprietary rights notice. The unauthorized submission or distribution of copyrighted or other proprietary content is illegal and could subject the User to personal liability or criminal prosecution.</p><p>&nbsp;</p><p><strong>VISA OBLIGATIONS OF THE USER</strong></p><p>The travel bookings done by WIZFAIR are subject to the applicable requirements of Visa which are to be obtained by the individual traveller. WIZFAIR is not responsible for any issues, including inability to travel, arising out of such Visa requirements and is also not liable to refund for the untraveled bookings due to any such reason.</p><p>&nbsp;</p><p><strong>PERSONAL AND NON-COMMERCIAL USE LIMITATION</strong></p><p>Unless otherwise specified, the WIZFAIR services are for the User's personal and non - commercial use. The User may not modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell any information, software, products or services obtained from the WIZFAIR website(s) without the express written approval from WIZFAIR.</p><p>&nbsp;</p><p><strong>INDEMNIFICATION</strong></p><p>The User agrees to indemnify, defend and hold harmless WIZFAIR and/or its affiliates, their websites and their respective lawful successors and assigns from and against any and all losses, liabilities, claims, damages, costs and expenses (including reasonable legal fees and disbursements in connection therewith and interest chargeable thereon) asserted against or incurred by WIZFAIR and/or its affiliates, partner websites and their respective lawful successors and assigns that arise out of, result from, or may be payable by virtue of, any breach or non-performance of any representation, warranty, covenant or agreement made or obligation to be performed by the User pursuant to this agreement.</p><p>&nbsp;</p><p>The user shall be solely and exclusively liable for any breach of any country specific rules and regulations or general code of conduct and WIZFAIR cannot be held responsible for the same.</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>RIGHT TO REFUSE</strong></p><p>WIZFAIR at its sole discretion reserves the right to not to accept any customer order without assigning any reason thereof. Any contract to provide any service by WIZFAIR is not complete until full money towards the service is received from the customer and accepted by WIZFAIR.</p><p>&nbsp;</p><p>Without prejudice to the other remedies available to WIZFAIR under this agreement, the TOS or under applicable law, WIZFAIR may limit the user's activity, or end the user's listing, warn other users of the user's actions, immediately temporarily/indefinitely suspend or terminate the user's registration, and/or refuse to provide the user with access to the website if:</p><p>&nbsp;</p><p>The user is in breach of this agreement, the TOS and/or the documents it incorporates by reference;</p><p>WIZFAIR is unable to verify or authenticate any information provided by the user; or</p><p>WIZFAIR believes that the user's actions may infringe on any third party rights or breach any applicable law or otherwise result in any liability for the user, other users of the website and/or WIZFAIR.</p><p>WIZFAIR may at any time in its sole discretion reinstate suspended users. Once the user have been indefinitely suspended the user shall not register or attempt to register with WIZFAIR or use the website in any manner whatsoever until such time that the user is reinstated by WIZFAIR.</p><p>&nbsp;</p><p>Notwithstanding the foregoing, if the user breaches this agreement, the TOS or the documents it incorporates by reference, WIZFAIR reserves the right to recover any amounts due and owing by the user to WIZFAIR and/or the service provider and to take strict legal action as WIZFAIR deems necessary.</p><p>&nbsp;&nbsp;</p><p><strong>RIGHT TO CANCELLATION BY WIZFAIR IN CASE OF INVALID INFROMATION FROM USER</strong></p><p>The User expressly undertakes to provide to WIZFAIR only correct and valid information while requesting for any services under this agreement, and not to make any misrepresentation of facts at all. Any default on part of the User would vitiate this agreement and shall disentitle the User from availing the services from WIZFAIR.</p><p>&nbsp;</p><p>In case WIZFAIR discovers or has reasons to believe at any time during or after receiving a request for services from the User that the request for services is either unauthorized or the information provided by the User or any of them is not correct or that any fact has been misrepresented by him, WIZFAIR in its sole discretion shall have the unrestricted right to take any steps against the User(s), including cancellation of the bookings, etc. without any prior intimation to the User. In such an event, WIZFAIR shall not be responsible or liable for any loss or damage that may be caused to the User or any of them as a consequence of such cancellation of booking or services.</p><p>&nbsp;</p><p>The User unequivocally indemnifies WIZFAIR of any such claim or liability and shall not hold WIZFAIR responsible for any loss or damage arising out of measures taken by WIZFAIR for safeguarding its own interest and that of its genuine customers. This would also include WIZFAIR denying/cancelling any bookings on account of suspected fraud transactions.</p><p>&nbsp;</p><p><strong>INTERPRETATION NUMBER AND GENDER</strong></p><p>The terms and conditions herein shall apply equally to both the singular and plural form of the terms defined. Whenever the context may require, any pronoun shall include the corresponding masculine, feminine and neuter form. The words 'include', 'includes' and 'including' shall be deemed to be followed by the phrase 'without limitation'. Unless the context otherwise requires, the terms 'herein', 'hereof', 'hereto', 'hereunder' and words of similar import refer to this agreement as a whole.</p><p>&nbsp;&nbsp;</p><p><strong>SEVERABILITY</strong></p><p>If any provision of this agreement is determined to be invalid or unenforceable in whole or in part, such invalidity or unenforceability shall attach only to such provision or part of such provision and the remaining part of such provision and all other provisions of this Agreement shall continue to be in full force and effect.</p><p>&nbsp;</p><p><strong>HEADINGS</strong></p><p>The headings and subheadings herein are included for convenience and identification only and are not intended to describe, interpret, define or limit the scope, extent or intent of this agreement, terms and conditions, notices, or the right to use this website by the User contained herein or any other section or pages of WIZFAIR Websites or its partner websites or any provision hereof in any manner whatsoever.</p><p>&nbsp;</p><p>In the event that any of the terms, conditions, and notices contained herein conflict with the Additional Terms or other terms and guidelines contained within any particular WIZFAIR website, then these terms shall control.</p><p>&nbsp;</p><p><strong>RELATIONSHIP</strong></p><p>None of the provisions of any agreement, terms and conditions, notices, or the right to use this website by the User contained herein or any other section or pages of WIZFAIR Websites or its partner websites, shall be deemed to constitute a partnership between the User and WIZFAIR and no party shall have any authority to bind or shall be deemed to be the agent of the other in any way.</p><p>&nbsp;</p><p><strong>UPDATION OF THE INFORMATION BY WIZFAIR</strong></p><p>User acknowledges that WIZFAIR provides services with reasonable diligence and care. It endeavors its best to ensure that User does not face any inconvenience. However, at some times, the information, software, products, and services included in or available through the WIZFAIR websites or other sales channels and ad materials may include inaccuracies or typographical errors which will be immediately corrected as soon as WIZFAIR notices them. Changes are/may be periodically made/added to the information provided such. WIZFAIR may make improvements and/or changes in the WIZFAIR websites at any time without any notice to the User. Any advice received except through an authorized representative of WIZFAIR via the WIZFAIR websites should not be relied upon for any decisions.</p><p>&nbsp;</p><p><strong>MODIFICATION OF THESE TERMS OF USE</strong></p><p>WIZFAIR reserves the right to change the terms, conditions, and notices under which the WIZFAIR websites are offered, including but not limited to the charges. The User is responsible for regularly reviewing these terms and conditions.</p><p>&nbsp;&nbsp;</p><p><strong>JURISDICTION</strong></p><p>WIZFAIR hereby expressly disclaims any implied warranties imputed by the laws of any jurisdiction or country other than those where it is operating its offices. WIZFAIR considers itself and intends to be subject to the jurisdiction only of the courts of NCR of Delhi, India and New Jersey, USA.</p><p>&nbsp;&nbsp;</p><p><strong>RESPONSIBILITIES OF USER VIS-À-VIS THE AGREEMENT</strong></p><p>The User expressly agrees that use of the services is at their sole risk. To the extent WIZFAIR acts only as a booking agent on behalf of third party service providers, it shall not have any liability whatsoever for any aspect of the standards of services provided by the service providers. In no circumstances shall WIZFAIR be liable for the services provided by the service provider. The services are provided on an 'as is' and 'as available' basis. WIZFAIR may change the features or functionality of the services at any time, in their sole discretion, without notice. WIZFAIR expressly disclaims all warranties of any kind, whether express or implied, including, but not limited to the implied warranties of merchantability, fitness for a particular purpose and non-infringement. No advice or information, whether oral or written, which the User obtains from WIZFAIR or through the services shall create any warranty not expressly made herein or in the terms and conditions of the services. If the User does not agree with any of the terms above, they are advised not to read the material on any of the WIZFAIR pages or otherwise use any of the contents, pages, information or any other material provided by WIZFAIR. The sole and exclusive remedy of the User in case of disagreement, in whole or in part, of the user agreement, is to discontinue using the services after notifying WIZFAIR in writing.</p><p>&nbsp;</p><p></p></div></div>";

        $this->output_compressed_data($response);

    }

    public function agency_registration()
    {

        $response['agency_registration'] = "<div class='col-md-12 col-xs-12'><div class='lblbluebold16px'><h1>Agency Registration</h1></div><div class='lblfont12px'><p></p><h2>Introducing an Online Travel System that's about You! WizFair, best in class online travel platform.</h2><p>&nbsp;</p><p>Whether you are an established travel agency or looking to launch one, we have the Perfect Solutions to Augment your Business.</p><p>&nbsp;</p><h3>WHAT DO I GET WITH WIZFAIR?</h3><p>In a nutshell, you get what you need to be successful… Technology, marketing, back-office, training, high commissions, and as much or as little hand holding as you need. All our solutions are time tested and field proven so that you don’t have to waste your time “reinventing the wheel.”</p><p>&nbsp;</p><h3>WHAT MAKES WIZFAIR DIFFERENT?</h3><ul><li>Unparalleled support</li><li>Cutting edge technology</li><li>Best of class marketing</li><li>Highest commissions in the industry</li><li>And much, much more!</li></ul><p>&nbsp;</p><h3>FEATURES &amp; BENEFITS INCLUDED</h3><ul><li>Travel Search Access</li><li>Customer Relationship Management Technology</li><li>Advanced Tools and Support&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Access to All cheapest Flights tickets, Hotels and more&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Ability to book and profit on air, car and hotel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Highest Commissions on All Bookings&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Access to Over 600 Airlines &amp; 1000's of Vendors&nbsp;&nbsp;</li><li>Multiple Real-time Online Booking System&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Commissions paid with regular invoice!&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Advanced Consumer Websites with live booking engines&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Information resources with Layover Plans, Layouts &amp; Photos, property information with detailed descriptions, and much more!&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Multiple Rate Code Displays&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Unbeatable Back Office Support&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Ability to Manage &amp; Profit from bookings&nbsp;&nbsp;</li><li>Ticket Fulfillment &amp; Delivery&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Training - More than any other host! Earn rewards just for learning!&nbsp;&nbsp;&nbsp;</li><li>Procedural, Marketing, Technology and Sales coaching and mentoring&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Detailed Sales &amp; Commission Reporting&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Branded Customizable Electronic Invoicing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Access to Specials Not Available to the Public&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Access to 1000s of Group and Contract Rates&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li></ul><p>&nbsp;</p><h3>PRICING &amp; COMMISSIONS</h3><ul><li>Agent Enrollment Fee: NIL</li><li>Monthly Fee Including: Full CRM access, Learning Management Systems, available co-op assistance and amazing portal with live booking functionality. INR 5500 per month. Please contact us for membership options details.</li><li>You can upgrade, downgrade or cancel at anytime. It's entirely up to you.</li><li>Annual Agent Renewal Fee: NIL</li><li>Commission (depends on productivity)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 80-95%</li></ul><p>&nbsp;</p><h3>BENEFITS OF JOINING WIZFAIR</h3><p>Why work with WizFair? Why not do it myself?</p><p>WizFair is the fastest growing name in the travel industry. With our experience, we truly have the experience and the clout to make your business grow.</p><p>&nbsp;</p><h3>LESS TIME AND EFFORT</h3><p>Start-up costs, including forms, technology, memberships, and accreditations, could be thousands of rupees. Just the time to learn and develop the contacts would take you many months. We have everything you need so that you can start selling and making money from day one.</p><p>&nbsp;</p><h3>MORE COMMISSIONS</h3><p>Alone, you would be working on much lower commission level. When you partner with us, you get the highest commissions in the industry.</p><p>&nbsp;</p><h3>EASE OF DOING BUSINESS</h3><p>Become an expert, know what you’re doing. With us, you will do business faster, do more deals, and you will do higher volume. You get knowledge and personal back-up. YOU are on a TEAM!</p><p>&nbsp;</p><h3>NO NEED FOR SECRETARIES AND BACK OFFICE STAFF</h3><p>We do it for you. Our professional support staff is second to none. We take care of all the invoicing, paperwork, accounting, etc., all in the name of making you more profitable selling travel.</p><p>&nbsp;</p><h3>ACCESS TO SPECIAL DISCOUNTED RATES ON MAJOR AIRLINES AND VACATION VENDORS</h3><p>By virtue of our size and sales volume, the airlines give WizFair special rates such that their sales force can pass these on to consumers. These rates in most cases can’t be beat by anyone in the industry and thus, the consumer is more likely to buy from a WizFair associate than other agencies just on the cost factor alone.</p><p>&nbsp;</p><h3>GREAT SUPPORT</h3><p>Communication is the single largest failing point of other agencies programs. Not WizFair, though. We utilize cutting edge information processing technology to distribute information to you via the internet. All of our agents always have their dedicated Sales Manager to assist them with unusual situations and to help answer any questions, either by phone or e-mail or Chat. We Want You To Sell and We’re Here to Help.</p><p>&nbsp;</p><h3>PREFERRED CONNECTIONS WITH VACATION VENDORS OF ALL KINDS</h3><p>Our relationship with vendors gives us many advantages over other agencies. When we need specific accommodations, last minute changes, or any other special requests, the vendors are more than willing to spend the time with us and get it done whereas if it were another agency, they might just say, “It’s simply just too late to make any changes.” In fact, we even have special phone numbers (set aside solely for top producers) that we are assigned for just such cases.</p><p>&nbsp;</p><h2>SIGNUP NOW!</h2><h3>Registration Process</h3><ol><li>Click on the registration link. <a href='https://thewizfair.com/agent/index.php/user/agentRegister'>https://thewizfair.com/agent/index.php/user/agentRegister</a></li><li>Complete all the sections- Personal Info, Company Details, Login Info (create username and password)</li><li>Accept terms and conditions and click register</li></ol><p>&nbsp;</p><h2>Contact Us</h2><p><strong>Website: <a href='http://www.thewizfair.com'>www.thewizfair.com</a></strong></p><p><strong>Email: <a href='mailto:contact@thewizfair.com'>contact@thewizfair.com</a></strong></p><p><strong>India: 1800 1234 93247</strong></p><p><strong>USA: 855 949 3247</strong></p><p><strong>Fax: 855 949 3247</strong></p><p></p></div></div>";

        $this->output_compressed_data($response);

    }

    public function edit_pax()
    {
        $params     = $this->input->post();
        $new_params = [];

        //debug($params);exit;
        if (count($params)) {
            if ($params['email']) {
                $new_params['email'] = $params['email'];
                unset($params["email"]);
            }
            if ($params['phone']) {
                $new_params['phone'] = $params['phone'];
                unset($params["phone"]);
            }

            if ($params['flight_booking_details_origin']) {
                $new_id = $params['flight_booking_details_origin'];
                unset($params["flight_booking_details_origin"]);
            }

            $id            = $params["origin"];
            $app_reference = $params["app_reference"];
            if (!$params['is_domestic']) {
                $passport_issuing_country           = $GLOBALS['CI']->db_cache_api->get_country_list(array('k' => 'origin', 'v' => 'name'), array('origin' => $params['passenger_passport_issuing_country']));
                $params['passport_issuing_country'] = $passport_issuing_country[$params['passenger_passport_issuing_country']];
                unset($params["passenger_passport_issuing_country"]);
            }
            unset($params["is_domestic"]);
            unset($params["app_reference"]);
            unset($params["origin"]);
            $response = $this->flight_model->update_pax_details($params, $id);

            if (count($new_params)) {
                $this->flight_model->update_booking_details($new_params, $new_id);
            }

            //echo $this->db->last_query();exit;
            echo json_encode($response);
            exit();
            //redirect("flight/review_passengers/" . $app_reference);
        }
    }

    public function test_notification($value='')
    {
        $title = 'Flight Booking';
        $description = 'Dear User, your flight has confirmed.';
        $user_id = '1355';

        $device_details= $this->user_model->getDevices($user_id,$type='1',$title,$description);
        $this->load->library('fcm');
        $this->fcm->set_title($title);
        $this->fcm->set_message($description);

        if ($device_details['ios']) {
            //echo "<pre>";print_r($device_details['ios']);
            $this->fcm->set_device_type('ios');
            $device_ids = $device_details['ios'];
            $this->fcm->add_multiple_recipients($device_ids);
            $fcm_result = $this->fcm->send();
            //echo "<pre>";print_r($fcm_result);exit();
        }
        if ($device_details['android']) {
            //$device_details['android'] = array("d9IoRc3N-Qo:APA91bEcZWcMIH8R0t8v3E6SebglIEPpFTAaagqUgAlO_tWJJxQoc0j_9gRnMTdj55TVGXz0tvrUru7LQinaoT1aSi7eNVi2YFzCYDCGqviO15SOCxwUF7Hw3ngpWxMt2sS--jOSkmU-");
            $this->fcm->set_device_type('android');
            $device_ids = $device_details['android'];
            $this->fcm->add_multiple_recipients($device_ids);
            $fcm_result = $this->fcm->send();
            //echo "<pre>";print_r($fcm_result);exit();
        }

    }

}
