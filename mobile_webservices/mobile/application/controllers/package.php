<?php
ob_start();if (!defined('BASEPATH'))exit('No direct script access allowed');
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

error_reporting(0);

class Package extends CI_Controller {

		public function __construct()
	{
		parent::__construct();
		    $current_url = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
        $current_url = $this->config->site_url().$this->uri->uri_string(). $current_url;
        $this->url =  array(
            'continue' => $current_url,
        );
		    $current_url 	= $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
        $current_url 	= WEB_URL.$this->uri->uri_string(). $current_url;
		    $url 			= array('continue' => $current_url);
        $this->perPage 	= 10;
        $this->session->set_userdata($url);               
        $this->load->model('Home_Model');
        $this->load->model('Flight_Model');   
        $this->load->model('cart_model');
        $this->load->model('booking_model');  
        $this->load->model('email_model');  
        $this->load->library('xml_to_array');
        $this->load->model('Hotel_Model');
        $this->load->library('encrypt');
        $this->load->library('session');
        $this->load->library('flight/amedus_xml_to_array');
        $this->load->library('Ajax_pagination'); 
        $this->load->helper('flight/amedus_helper');  
	}

	public function index(){ 
        $request 						= $this->input->get();
        $request['ispackage'] 			= 'test';
        //print_r($request); exit;
        $data['req'] 		= json_decode(json_encode($request));
        $data['request'] 	= base64_encode(json_encode($request));
        $data['hotel_request'] 	= $request['hotel_request'];
        if($request['destination'] != $request['origin']){
			  //print_r($data); exit;
		$this->load->view(PROJECT_THEME.'/package/package_result', $data);
		}else{
			redirect('home');
		}
	}

	public function search(){
    $request = $search_request['data']= $this->input->get();     
    print_r($request);die;
		$ptdata = $_REQUEST;
		$type = "type=round";
		$adult = array_sum($request['adult']);
		$child = array_sum($request['child']);  		
		$fr = explode(' ', $request['to']);
		$request['city'] = $fr[0];
		$request['hotelpackage'] = 'test';
		$query 	= $type.'&origin='.substr(chop(substr($request['from'], -5), ')'), -3).'&destination='.substr(chop(substr($request['to'], -5), ')'), -3).'&depart_date='.$request['depature_from'].'&return_date='.$request['return_from'].'&ADT='.$adult.'&CHD='.$child.'&INF='.$request['infant'].'&class='.$request['class'].'&type11='.$request['hotelpackage'];
		$request_data = base64_encode(json_encode($request));
		unset($_SESSION['flight_id1']);
    $data['flight']=base64_encode(json_encode($query));
    $data['hotel']=$request_data;
    $search_data=$this->custom_db->insert_record('bundle_deal_history',$data);
    redirect(WEB_URL.'package/?'.$query.'&hotel_request='.$request_data.'&id='.$search_data['insert_id']); 
		exit;
		}

    public function getlowestprice_package($arivalDate)
    {
		
		$requestset 						= 	($_REQUEST['request']);        
      //  $data['request_data']				= 	$request;
             		
		$request1 							= 	json_decode(base64_decode($requestset), 1);
		$request1['hotel_checkin'] 			= $arivalDate;	   
        $data['request_data']				= 	base64_encode(json_encode($request1));
        
       // print_r($data); exit;
       //echo"<pre/>";print_r($_SESSION);exit();
		$sessionId = $_SESSION['session_id_amadeus'];
		if(isset($_SESSION['flight_id1']) && ($_SESSION['flight_id1']!="")){
		$flight_id1=$_SESSION['flight_id1'];
		$data['flight_result'] = $this->Flight_Model->getlowestprice_flight_byid($flight_id1);
		}
		else
		{
		$data['flight_result'] = $this->Flight_Model->getlast_lowset_flight();		
		}
				
		 $data['hotel_owest'] 	= $this->Hotel_Model->getlast_lowest_hotel($sessionId);
		 $loadpage 				= 	$this->load->view(PROJECT_THEME.'/package/package_hotel',$data,true);
		 		//echo"<pre/>";print_r($data); exit;
		   	   print json_encode(array(
						'hotel_search_result' 	=> $loadpage
						));
		 
		 //print_r($data);exit;
	
	}
	
	 public function getlowestprice_hotel_byid($id)
    {	
		$data['request_data'] 	= 	($_REQUEST['requestdata']);        
		
		//echo"<pre/>";print_r($_SESSION);exit();
		$flight_id1=$_SESSION['flight_id1'];
		$data['flight_result'] = $this->Flight_Model->getlowestprice_flight_byid($flight_id1);		
		 $data['hotel_owest'] 	= $this->Hotel_Model->getlowestprice_package_byid($id);
		 $loadpage 				= 	$this->load->view(PROJECT_THEME.'/package/package_hotel',$data,true);
		 		//echo"<pre/>";print_r($data); exit;
		   	   print json_encode(array(
						'hotel_search_result' 	=> $loadpage
						));
		 
		 //print_r($data);exit;
	
	}
	 public function getlowestprice_flight_byid($id,$request)
    {	
		exit();
		 //print_r($request); exit;
		//$request 								= 	($_REQUEST['request']);        
        $data['request_data']						= 	$request;
        $data['request_data1']								= 	json_decode(base64_decode($request), 1);
        $data['hotel_query']=$hotel_request  =$data['request_data1']['hotel_request'];
        $hotel_query =json_decode(base64_decode($hotel_request), 1);
        //print_r($data); exit;
		$sessionId = $_SESSION['session_id_amadeus'];
		$data['flight_result'] = $this->Flight_Model->getlowestprice_flight_byid($id);						
		$data['hotel_owest'] 	= $this->Hotel_Model->getlast_lowest_hotel($sessionId);
		 $_SESSION['flight_id1']=$id;
		//print_r($data['hotel_owest']); exit;
		
		$search_date=$data['hotel_owest'][0]->search_date;
		$explode_search_date=explode("-",$search_date);
		$Arivaldate_count=count($data['flight_result'][0]['FlightDetails'][0]['ArrivalDate']);
		$zz=$Arivaldate_count-1;
		$arraival_date_value=$data['flight_result'][0]['FlightDetails'][0]['ArrivalDate'][$zz];
	 	$formate_search_date=$explode_search_date[0]."-".$explode_search_date[1]."-".$explode_search_date[2];
		//print_r($explode_search_date);
		
		//$this->load->view(PROJECT_THEME.'/hotel/ajax_hotel_result',$data1);
		$loadpage 				= 	$this->load->view(PROJECT_THEME.'/package/package_hotel',$data,true);

		if($arraival_date_value == $formate_search_date)
		{
							   	   print json_encode(array(
						'hotel_search_result' 	=> $loadpage,
						'ArivalDate' 	=> ''
												));

		}
		else{			
			$hotel_query['hotel_checkin'] = $arraival_date_value;
					   	   
					   	   print json_encode(array(
						'hotel_search_result' 	=> $loadpage,
						'ArivalDate' 	=> $arraival_date_value
						));
		}
		

		 //print_r($data);exit;
	
	}
	
	
	
	
	

    public function package_details($flight_id,$hotelcode, $sessionid, $requestset)
    {	
		$flight_id = json_decode(base64_decode(base64_decode($flight_id))); 
		$hotel_code = json_decode(base64_decode(base64_decode($hotelcode)));		
		$data['dt'] = $this->Hotel_Model->get_hotel_list($hotel_code,$sessionid);	
		$data['hotel_code']=$hotel_code;
		$data['flight_id']=$flight_id;
		$data['request_data']=$requestset;		
    //echo"<pre/>";print_r($data);  exit;    
		$this->load->view(PROJECT_THEME.'/package/package_details', $data);  		
		
	}
	
public function description_service(){
	
        $request 											= 	($_REQUEST['request']); 
        $flight_request                 					=   ($_REQUEST['flight_request']);        
        $request_data										=   ($_REQUEST['request_data']);  
      //echo "<pre/>" ;print_r($request_data);  exit;      
        $requestset 										= 	json_decode(base64_decode($request), 1); 
        $sessionid											=   $_SESSION['session_id_amadeus'];
		    $data['flight_result'] 								= 	$this->Flight_Model->getlowestprice_flight_byid($flight_request);	

       if($_SESSION['hotel_code']==$requestset){	 
		  $data['photeldesc'] 									= 	$this->Hotel_Model->get_Hotel_info($requestset,$sessionid);
		  $data['photeldesc'][0]['Hotel_info_description']	= 	$data['photeldesc'][0]['Hotel_content_all']; 
		  $data['photeldesc'][0]['GuestRoomInfo_images'] 	= 	$data['photeldesc'][0]['Hotel_images_all'];
		   
	   }
	   else{     
        $datas            									= 	$this->Hotel_Model->get_hotel_description($requestset);
        $data['photeldesc'][0]	            				=   $datas['photeldesc'];
        $data['photeldesc'][0]['Hotel_facilities_info']		=	$data['photeldesc'][0]['GuestRoomInfo_feacilities'];
	   }
	    $data['rdt']										=   $this->Hotel_Model->get_hotel_room_list($requestset,$sessionid);
        $data['dt'] 										=   $this->Hotel_Model->get_hotel_list($requestset,$sessionid);	
        $data['request_data']								= 	$request_data;
  
        $dataroom											= 	json_decode(base64_decode($request_data), 1);
        
        $data['RoomsCount']									= 	$dataroom['rooms'];
        $data['roomdata']									= 	$dataroom;
        $ajax_page 											= 	$this->load->view(PROJECT_THEME.'/package/description_page',$data,true);
  	   
  	  //echo "<pre/>";print_r($data);  exit;
  	   print json_encode(array(
						'hotel_flight_description_result' 	=> $ajax_page,
						));
	 }
	 
	
	
	

    public function pre_booking(){

       //echo '<pre>'; print_r($_POST); exit();
         $flight = $this->input->post('temp_d');
        $flight = json_decode(base64_decode($flight)); 

        $request = $this->input->post('temp_r');

        $request = json_decode(base64_decode($request));
       
        $data['country']    =  $this->Flight_Model->get_country();
        $api = $this->input->post('chk_do');  //echo '<pre>'; print_r($flight); print_r($request); print_r($api);  exit();
          
        if ($api =='Flight-CRS') {
      //$seq2_ = $SequenceNumbers + 1;
          $request = base64_decode($request);
        $tete = json_decode($request);
      
      $data['flight_id'] = $flight->flight_id;

      //$data['module_crs'] = 'Flight-CRS';
      $pricing = $this->Flight_Model->getCRSPrice($flight->flight_id)->result();
      $segments = $this->Flight_Model->getCRSSegments($flight->flight_id)->result();
      
      $totalprice = $pricing[0]->adult_price + $pricing[0]->adult_tax;
        $TotalPrice = $totalprice;
        $data['totalarrr'] = $totalprice;
        
      $BasePrice = $pricing[0]->adult_price; //print_r($BasePrice); exit();
      $Taxes = $pricing[0]->adult_tax;
      
      $TotalPrice_Curr ='USD'; //print_r($TotalPrice_Curr); exit();
      $AirItinerary_xml = '';
      $AirPricingSolution_xml ='';
      //print_r($request->type); exit();
      $onward_from_city = json_decode($segments[0]->onward_from_city); //print_r(reset($onward_from_city)); exit();
      $to_city = json_decode($segments[0]->to_city);
      $return_from_city = json_decode($segments[0]->return_from_city);
      $return_to_city = json_decode($segments[0]->return_to_city);
      $onward_flight_code = json_decode($segments[0]->onward_flight_code);
      $return_flight_code = json_decode($segments[0]->return_flight_code);
      $onward_departure_time = json_decode($segments[0]->onward_departure_time);
      $onward_return_time = json_decode($segments[0]->onward_return_time);
      //$departuredate_onward = json_decode($segments[0]->onward_departure_date);
      //$last_segment_date = end($departuredate_onward);
        /*if($request->type == 'oneway'){
          
          $first_seg = reset($onward_from_city);
          $last_seg = end($to_city);
          $Carrier = reset($onward_flight_code);
        }else */
        //print_r($tete->type); exit();
        if ($tete->type == 'round') {
          $first_seg = reset($onward_from_city);   
          $last_seg = end($to_city);
          $Carrier = reset($onward_flight_code); 
          $first_seg_r = reset($return_from_city);
          $last_seg_r = end($return_to_city);
          $Carrier_r = reset($return_flight_code);
        }
        
        $fromCityName =  $this->Flight_Model->get_airport_cityname($first_seg);

        $toCityName =  $this->Flight_Model->get_airport_cityname($last_seg);
        //die;
        $AirImage = site_url().'assets/images/airline_logo/'.$Carrier.'.gif';
        $data['AirImage_r'] = $Carrier_r;
        $data['airlines'] = $Carrier;  //print_r($Carrier); exit();
      
        $ArrivalDateTime = reset($onward_return_time);

        $DepartureDateTime = end($onward_departure_time);
         //echo '<pre>dep'; print_r($first_seg->DepartureTime); exit();
        $seconds = $ArrivalDateTime - $DepartureDateTime;
      $days = floor($seconds / 86400);
        $hours = floor(($seconds - ($days * 86400)) / 3600);
        $minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
        // $seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
        if($days==0){
            $dur=$hours."h ".$minutes."m";  
        }else{
            $dur=$days."d ".$hours."h ".$minutes."m";
        }
        $data['duration'] = $dur;
         list($date, $time) = explode('T', $last_seg_r->ArrivalTime);
        $time = preg_replace("/[.]/", " ", $time);
        $data['timesarr_r'] = $time;
       list($time) = explode(" ", $time);
        $ArrivalDateTime_r = $date." ".$time; //Exploding T and adding space
        $ArrivalDateTime_r = $data['artt_r'] = strtotime($ArrivalDateTime_r);
        list($date, $time) = explode('T', $first_seg_r->DepartureTime);
        $time = preg_replace("/[.]/", " ", $time);
        $data['timesdep_r'] = $time;
        list($time) = explode(" ", $time);
        $DepartureDateTime_r = $date." ".$time; //Exploding T and adding space
        $DepartureDateTime_r = $data['dpta_r'] = strtotime($DepartureDateTime_r);
         //echo '<pre>dep'; print_r($first_seg->DepartureTime); exit();
        $seconds_r = $ArrivalDateTime_r - $DepartureDateTime_r;
      $days_r = floor($seconds_r / 86400);
        $hours_r = floor(($seconds_r - ($days_r * 86400)) / 3600);
        $minutes_r = floor(($seconds_r - ($days_r * 86400) - ($hours_r * 3600))/60);
        // $seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
        if($days_r==0){
            $dur_r=$hours_r."h ".$minutes_r."m";  
        }else{
            $dur_r=$days_r."d ".$hours_r."h ".$minutes_r."m";
        }
       $data['duration_r'] = $dur_r;
        $cart_flight = array(
        'request' => $this->input->post('temp_r'),
        'response' => $this->input->post('temp_d'),
        'Origin' => $first_seg,
        'Destination' => $last_seg,
        'fromCityName' => $fromCityName,
        'toCityName' => $toCityName,
        'DepartureTime' => $DepartureDateTime,
        'ArrivalTime' => $ArrivalDateTime,
        'duration' => '',
        'AirImage' => $AirImage,
        'AirPriceRes' => base64_encode(json_encode($tete)),
        'TotalPrice' => $TotalPrice,
        'SITE_CURR' => $_SESSION['currency'],
        //'MyMarkup' => $Markup,
        //'AdminMarkup' => $AdminMarkup,
        //'API_CURR' => $_SESSION['currency'],
        'API_CURR' => BASE_CURRENCY,
        'BasePrice' => $BasePrice,
        'TaxPrice' => $Taxes,
        'AirItinerary_xml' => $AirItinerary_xml, 
        'AirPricingSolution_xml' => $AirPricingSolution_xml,
        'seq_number' => '',
        'SecuritySession' => '', 
        'api' => 'Flight-CRS',
        'TIMESTAMP' => date('Y-m-d H:i:s')
      );
     
      $booking_cart_id = $this->Flight_Model->insert_cart_flight($cart_flight);
      $data['booking'] = $this->Flight_Model->get_cart_flight($booking_cart_id);
      
          $session_id = $this->session->userdata('session_id');

          $cart_global = array(
              'parent_cart_id' => 0,
              'ref_id' => $booking_cart_id,
              'module' => 'Flight-CRS',
              //'user_type' => $user_type,
              //'user_id' => $user_id,
              'session_id' => $session_id,
              'site_curr' => BASE_CURRENCY,
              'total' => $TotalPrice,
              'ip' =>  $this->input->ip_address(),
              'timestamp' => date('Y-m-d H:i:s')
          );

          $cart_global_id = $this->Cart_Model->insert_cart_global($cart_global);
          $data['cart_global_id'] = $cart_global_id;
          //echo 'cart_global_id'; print_r($cart_global_id); exit();
          $data['status'] = 1;
          
         $data['cart_data'] = $this->Cart_Model->getCartDataCrs($cart_global_id)->result(); //old
         
         $cart_data = $data['cart_data'];
          //echo 'hiiiiiiiiii<pre>'; print_r($data['cart_data']); exit();
          //$data['cart_data'] = $this->Cart_Model->get_cart_data($cart_global_id); //new 
          //print_r($data['cart_data']); exit();
          //echo 'cart<pre>'; print_r($cart_data); exit();
          $data['count'] = count($data['cart_data']);
          //print_r($session_id); exit();
          if(count($data['cart_data']) > 0){
              $data['isCart'] = true;
          }else{
              $data['isCart'] = false;
          }
    }
        if($api == 'AM')
        {
        $AirPriceReq_Res = Air_SellFromRecommendation($flight, $request);
        $AirPriceRes = $AirPriceReq_Res['Air_SellFromRecommendationRes'];
        
        $sresult = $this->amedus_xml_to_array->xml2ary($AirPriceRes);

        

       $SecSession     = $sresult['soapenv:Envelope']['soapenv:Header']['awsse:Session']; 
       //echo '<pre>'; print_r($SecSession); exit();
       $SessionId      = $SecSession['awsse:SessionId']['_v'];
       $SequenceNumbers = $SecSession['awsse:SequenceNumber']['_v'];
       $SecurityToken  = $SecSession['awsse:SecurityToken']['_v']; //print_r($SecurityToken); exit();
       $SecuritySession['SessionId']=$SessionId;
       $SecuritySession['Sequence']=$SequenceNumbers;
       $SecuritySession['SecurityToken']=$SecurityToken;
        
      // $error = ;

       if($sresult['soapenv:Envelope']['soapenv:Body']['Air_SellFromRecommendationReply']['errorAtMessageLevel'])
       {
        
        $data['errorno'] = 1;
        
       } 
       else
       {


       $sresult1 = $sresult['soapenv:Envelope']['soapenv:Body']['Air_SellFromRecommendationReply']['itineraryDetails'];
        
       if(isset($sresult1['segmentInformation']) && is_array($sresult1['segmentInformation'])) {
       $sresult3[0] = $sresult1;
       }else {
       $sresult3 = $sresult1;
       }
      
       foreach($sresult3 as $skey3 => $sval3) {
       if(isset($sval3['segmentInformation']['flightDetails']) && is_array($sval3['segmentInformation']['flightDetails'])) {
       $sresult2[$skey3][0] = $sval3['segmentInformation'];
       }else {
       $sresult2[$skey3] = $sval3['segmentInformation'];
       }
       }
  
       $skey = 0;
      foreach($sresult2 as $skey4 => $sval4) {
       foreach($sval4 as $skey5 => $sval5) {
      $status[$skey++] = $sval5['actionDetails']['statusCode']['_v'];
      }
       }
   
  $finalstatus = @implode("",@array_unique($status)); 
    //echo 'hi'; print_r($finalstatus); exit();
  if($finalstatus != "OK") {
            $error_status='';
            if($finalstatus == 'UNS')
            {
                $error_status = 'Unable to sell';
            }
            if($finalstatus == 'WL')
            {
                $error_status = 'Wait listed';
            }
            if($finalstatus == 'X')
            {
                $error_status = 'Cancelled after a successful sell';
            }
            if($finalstatus == 'RQ')
            {
                $error_status = 'Sell was not even attempted';
            }

            
            $xml_log = array(
                'Api' => 'AMADEUS',
                'XML_Type' => 'Flight',
                'XML_Request' => $AirPriceReq_Res['Air_SellFromRecommendationReq'],
                'XML_Response' => $AirPriceReq_Res['Air_SellFromRecommendationRes'],
                'Ip_address' => $this->input->ip_address(),
                'XML_Time' => date('Y-m-d H:i:s')
            ); 

            $this->xml_model->insert_xml_log($xml_log);
            $session_id = $this->session->userdata('session_id');
            $cart_data = $this->Cart_Model->getCartData($session_id)->result();

            $data['count'] = count($cart_data);
            $data['error'] = $error_status;
            if(count($cart_data) > 0){
                $data['isCart'] = true;
            }else{
                $data['isCart'] = false;
            }
           
        
  }
  else{
        $seq2_ = $SequenceNumbers + 1;
        
        //echo '<pre>'; print_r($flight); exit();
            $TotalPrice = $flight->TotalPrice;
            $data['totalarrr'] = $flight->TotalPrice;
            
            $BasePrice = $flight->BasePrice; //print_r($BasePrice); exit();
            $Taxes = $flight->Taxes;
            
            $TotalPrice_Curr =$flight->APICurrencyType; //print_r($TotalPrice_Curr); exit();
            $AirItinerary_xml = '';
            $AirPricingSolution_xml ='';
            //print_r($request); exit();
            if($request->type == 'round') {
                $first_seg = reset($flight->segments[0]);
                $last_seg = end($flight->segments[0]);
                $first_seg_r = reset($flight->segments[1]);
                $last_seg_r = end($flight->segments[1]);
            }
            
            $fromCityName =  $this->flight_model->get_airport_cityname($first_seg->Origin);

            $toCityName =  $this->flight_model->get_airport_cityname($last_seg->Destination);
            //die;
            $AirImage = site_url().'assets/images/airline_logo/'.$first_seg->Carrier.'.gif';
            $data['AirImage_r'] = $first_seg_r->Carrier;
            //print_r($AirImage_r); exit();
            //Exploding T from arrival time 
            
            list($date, $time) = explode('T', $last_seg->ArrivalTime);
            $time = preg_replace("/[.]/", " ", $time);
            $data['timesarr'] = $time;
             list($time) = explode(" ", $time);
            $ArrivalDateTime = $date." ".$time; //Exploding T and adding space
            $ArrivalDateTime = $data['artt'] = strtotime($ArrivalDateTime);

           //echo '<pre>'; print_r($timesarr); exit();
            //Exploding T from depature time  
            list($date, $time) = explode('T', $first_seg->DepartureTime);
            $time = preg_replace("/[.]/", " ", $time);
            $data['timesdep'] = $time;
            list($time) = explode(" ", $time);
            $DepartureDateTime = $date." ".$time; //Exploding T and adding space
            $DepartureDateTime = $data['dpta'] = strtotime($DepartureDateTime);
             //echo '<pre>dep'; print_r($first_seg->DepartureTime); exit();
            $seconds = $ArrivalDateTime - $DepartureDateTime;
            $days = floor($seconds / 86400);
            $hours = floor(($seconds - ($days * 86400)) / 3600);
            $minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
            // $seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
            if($days==0){
                $dur=$hours."h ".$minutes."m";  
            }else{
                $dur=$days."d ".$hours."h ".$minutes."m";
            }
            $data['duration'] = $dur;
             list($date, $time) = explode('T', $last_seg_r->ArrivalTime);
            $time = preg_replace("/[.]/", " ", $time);
            $data['timesarr_r'] = $time;
             list($time) = explode(" ", $time);
            $ArrivalDateTime_r = $date." ".$time; //Exploding T and adding space
            $ArrivalDateTime_r = $data['artt_r'] = strtotime($ArrivalDateTime_r);
            list($date, $time) = explode('T', $first_seg_r->DepartureTime);
            $time = preg_replace("/[.]/", " ", $time);
            $data['timesdep_r'] = $time;
            list($time) = explode(" ", $time);
            $DepartureDateTime_r = $date." ".$time; //Exploding T and adding space
            $DepartureDateTime_r = $data['dpta_r'] = strtotime($DepartureDateTime_r);
             //echo '<pre>dep'; print_r($first_seg->DepartureTime); exit();
            $seconds_r = $ArrivalDateTime_r - $DepartureDateTime_r;
            $days_r = floor($seconds_r / 86400);
            $hours_r = floor(($seconds_r - ($days_r * 86400)) / 3600);
            $minutes_r = floor(($seconds_r - ($days_r * 86400) - ($hours_r * 3600))/60);
            // $seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
            if($days_r==0){
                $dur_r=$hours_r."h ".$minutes_r."m";  
            }else{
                $dur_r=$days_r."d ".$hours_r."h ".$minutes_r."m";
            }
           $data['duration_r'] = $dur_r;
            $cart_flight = array(
                'request' => $this->input->post('temp_r'),
                'response' => $this->input->post('temp_d'),
                'Origin' => $first_seg->Origin,
                'Destination' => $first_seg->Destination,
                'fromCityName' => $fromCityName,
                'toCityName' => $toCityName,
                'DepartureTime' => $DepartureDateTime,
                'ArrivalTime' => $ArrivalDateTime,
                'duration' => $dur,
                'AirImage' => $AirImage,
                'AirPriceRes' => base64_encode(json_encode($request)),
                'TotalPrice' => $TotalPrice,
                'SITE_CURR' => $_SESSION['currency'],
                //'MyMarkup' => $Markup,
                //'AdminMarkup' => $AdminMarkup,
                //'API_CURR' => $_SESSION['currency'],
                'API_CURR' => BASE_CURRENCY,
                'BasePrice' => $BasePrice,
                'TaxPrice' => $Taxes,
                'AirItinerary_xml' => $AirItinerary_xml, 
                'AirPricingSolution_xml' => $AirPricingSolution_xml,
                'seq_number' => $seq2_,
                'SecuritySession' => json_encode($SecuritySession), 
                'api' => 'AMADEUS',
                'TIMESTAMP' => date('Y-m-d H:i:s')
            );
            $transferid = $this->input->post('transfer_ids');
            $visadetailsid = $this->input->post('visa_details_id');
            $booking_cart_id = $this->flight_model->insert_cart_flight($cart_flight);
            $data['booking'] = $this->flight_model->get_cart_flight($booking_cart_id); 
            $session_id = $this->session->userdata('session_id');

            $cart_global = array(
                'parent_cart_id' => 0,
                'ref_id' => $booking_cart_id,
                'module' => 'FLIGHT',
                //'user_type' => $user_type,
                //'user_id' => $user_id,
                'session_id' => $session_id,
                'site_curr' => BASE_CURRENCY,
                'total' => $TotalPrice,
                'transfer_ids' => $transferid,
                'visa_details_id' => $visadetailsid,
                'ip' =>  $this->input->ip_address(),
                'timestamp' => date('Y-m-d H:i:s')
            );
             
            $cart_global_id = $this->Cart_Model->insert_cart_global($cart_global);
            $data['cart_global_id'] = $cart_global_id;
            //echo 'cart_global_id'; print_r($cart_global_id); exit();
            $data['status'] = 1;
            //print_r($cart_global_id); exit;
           $data['cart_data'] = $this->Cart_Model->getCartData($session_id)->result(); //old
           $cart_data = $data['cart_data'];
            //echo 'hiiiiiiiiii<pre>'; print_r($data['cart_data']); exit();
            //$data['cart_data'] = $this->Cart_Model->get_cart_data($cart_global_id); //new 
            //print_r($data['cart_data']); exit();
            //echo 'cart<pre>'; print_r($cart_data); exit();
            $data['count'] = count($data['cart_data']);
            //print_r($session_id); exit();
            if(count($data['cart_data']) > 0){
                $data['isCart'] = true;
            }else{
                $data['isCart'] = false;
            }
        
  }
        } }
        else
        {
        }
    //print_r($cart_data); exit();
         if(!empty($cart_data)){

                $data['C_URL'] = site_url().'booking/'.$session_id.'/FLIGHT'; //print_r($data['C_URL']); exit();
                foreach ($cart_data as $key => $cartt) {
                    //print_r($cartt->module); exit();
                    if($cartt->module == 'FLIGHT'){
                        //print_r($cartt->cart_id);
                        $cart = $this->Cart_Model->getCartDataByModule($cartt->cart_id,$cartt->module)->row();
                        //echo '<pre>'; print_r($cart); exit();
                        $request = json_decode(base64_decode($cart->request));
                        //echo '<pre>'; print_r($request); exit();
                        if($request->type == 'round'){
                            $originCity = $this->flight_model->get_airport_cityname($request->origin);
                            $destinationCity = $this->flight_model->get_airport_cityname($request->destination);
                            $origin = $request->origin;
                            $destination = $request->destination;
                        
                        }
                        //echo '<pre>'; print_r($this->Account_Model->currency_convertor($cart->TotalPrice)); exit();
                        $data['cart'][] = array(
                            'RID' => $session_id.'cart'.$key,
                            'CID' => $cart->cart_id,
                            'REF_ID' => $cart->ref_id,
                            'TYPE' => 'flightcart',
                            'NAME' => $originCity.' ('.$origin.') - '.$destinationCity.' ('.$destination.')',
                            'URL' => site_url(),
                            'ADDRESS' => date('d M, Y H:i', $cart->DepartureTime).' - '.date('d M, Y H:i', $cart->ArrivalTime),
                            'TOTAL' => $cart->TotalPrice,
                            'IMAGE' => $cart->AirImage
                        );
                        //echo '<pre>'; print_r($data['cart']); exit();
                        
                        $GRAND_TOTAL[] = $cart->TotalPrice;
                    }
                }
                //$data['GRAND_TOTAL'] = $this->Account_Model->currency_convertor(array_sum($GRAND_TOTAL));
                $data['GRAND_TOTAL'] = array_sum($GRAND_TOTAL);
            }
    
            $data["module"] = "FLIGHT";
          
            if($data['errorno'] == 1){
               $this->load->view('flight/noavailability',$data);
            }else{
                //Hotel
                 $hotel['hotel_shop_id'] = $this->input->post('hotel_shop_id');
                 $hotel['process_id'] = $this->input->post('process_id');
                 $hotel['hotel_code'] = $this->input->post('hotel_code');
                 $hotel['hotel_name'] = $this->input->post('hotel_name');
                 $hotel['star'] = $this->input->post('star');
                 $hotel['room_type'] = $this->input->post('room_type');
                 $hotel['image'] = $this->input->post('image');
                 $hotel['city'] = $this->input->post('city');
                 $hotel['TotalPrice'] = $this->input->post('TotalPrice');
                 $hotel['transfer_ids'] = $this->input->post('transfer_ids');
                 $hotel['visa_details_id'] = $this->input->post('visa_details_id');

         //$data = $this->input->post();
        //echo "<pre>"; print_r($hotel); echo "<pre/>";die;
         $cart_id= $this->Hotel_Model->add_cart_details_hotelflight($hotel); //print_r($cart_id); exit();
         if ($cart_id > 0) {
            
            $count = $this->General_Model->getCartData($cart_id, 'Hotel')->num_rows();
            //echo "<pre>"; print_r($count); echo "<pre/>";die;
            if ($count > 0) {
                //$data['transfer_id'] = $transfer_id;
                $data['countries'] = $this->General_Model->load_countries()->result();
                //echo '<pre/>';print_r($data['countries']);exit;
                $data['country']    =  $this->Flight_Model->get_country();
                $data['book_temp_data'] = $book_temp_data = $this->General_Model->getCartDataByModule($cart_id, 'Hotel')->result();
                //echo "<pre>hi"; print_r($data['book_temp_data'][0]->transfer_ids); echo "<pre/>";die;
              // $trans =  $data['book_temp_data'][0]->transfer_ids;
               // $data['transfer_data'] = $this->General_Model->get_Transferdata($cart_id, $trans)->result();
                foreach ($book_temp_data as $key => $value) {
                    $cart_global_id[] = $value->module . ',' . $value->cart_id;
                }
                $data['cart_global'] = $cart_global_id;
                $data['cart_global_id'] = base64_encode(json_encode($cart_global_id));
                if ($this->session->userdata('b2c_details_id')) {
                    $data['user_type'] = $user_type = 3;
                    $data['user_id'] = $user_id = $this->session->userdata('b2c_details_id');
                    $data['userInfo'] = $this->Account_Model->getUserInfo($user_id)->row();
                    $data['email'] = $data['userInfo']->user_email;
                    $data['contact_no'] = $data['userInfo']->user_cell_phone;
                }
                $data["module"] = 'Hotel';
           //echo '<pre>'; print_r($data); exit();
             $this->load->view('package/package_prebooking',$data);
             } else {
                $this->load->view('errors/expiry');
            }
        } else {
            $this->load->view('errors/expiry');
        }
         }
    }
}	
