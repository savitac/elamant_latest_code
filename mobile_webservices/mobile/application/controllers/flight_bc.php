<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
/**
 *
 * @package Provab
 * @subpackage Flight
 * @author Arjun J<arjunjgowda260389@gmail.com>
 * @version V1
 */
class Flight extends CI_Controller {
	private $current_module;
	public function __construct() {
		parent::__construct ();
		// $this->output->enable_profiler(TRUE);
		$this->load->model ( 'flight_model' );
		$this->load->model ( 'user_model' ); // we need to load user model to access provab sms library
		$this->load->library ( 'provab_sms' ); // we need this provab_sms library to send sms.
		$this->load->library('social_network/facebook');//Facebook Library to enable share button
		$this->current_module = $this->config->item('current_module');
	}
	/**
	 * App Validation and reset of data
	 */

	function country_list(){
			//$page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_code ();
		$query ="select * from api_country_list where name != ''";
		$resutl['country_list'] =$GLOBALS['CI']->db->query($query)->result_array();
		$this->output_compressed_data($resutl);
}
	function pre_calendar_fare_search()
	{
		$params = $this->input->get();
		$safe_search_data = $this->flight_model->calendar_safe_search_data($params);
		//Need to check if its domestic travel
		$from_loc = $safe_search_data['from_loc'];
		$to_loc = $safe_search_data['to_loc'];
		$safe_search_data['is_domestic_one_way_flight'] = false;

		$safe_search_data['is_domestic_one_way_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);
		if ($safe_search_data['is_domestic_one_way_flight'] == false) {
			$page_params['from'] = '';
			$page_params['to'] = '';
		} else {
			$page_params['from'] = $safe_search_data['from'];
			$page_params['to'] = $safe_search_data['to'];
		}

		$page_params['depature'] = $safe_search_data['depature'];
		$page_params['carrier'] = $safe_search_data['carrier'];
		$page_params['adult'] = $safe_search_data['adult'];
		redirect(base_url().'index.php/flight/calendar_fare?'.http_build_query($page_params));
	}

	/**
	 * Airfare calendar
	 */
	function calendar_fare()
	{
		$params = $this->input->get();
		$active_booking_source = $this->flight_model->active_booking_source ();
		if (valid_array($active_booking_source) == true) {
			$safe_search_data = $this->flight_model->calendar_safe_search_data($params);
			$page_params = array (
					'flight_search_params' => $safe_search_data ,
					'active_booking_source' => $active_booking_source
			);
			$page_params ['from_currency'] = get_application_default_currency ();
			$page_params ['to_currency'] = get_application_currency_preference ();
			$this->template->view ( 'flight/calendar_fare_result', $page_params );
		}
	}
	/**
	 * Jaganaath
	 */
	function add_days_todate()
	{
		$get_data = $this->input->get();
		if(isset($get_data['search_id']) == true && intval($get_data['search_id']) > 0 && isset($get_data['new_date']) == true && empty($get_data['new_date']) == false) {
			$search_id = intval($get_data['search_id']);
			$new_date = trim($get_data['new_date']);
			$safe_search_data = $this->flight_model->get_safe_search_data ( $search_id );

			$day_diff = get_date_difference($safe_search_data['data']['depature'], $new_date);
			if(valid_array($safe_search_data) == true && $safe_search_data['status'] == true) {
				$safe_search_data = $safe_search_data['data'];
				$search_params = array();
				$search_params['trip_type'] = trim($safe_search_data['trip_type']);
				$search_params['from'] = trim($safe_search_data['from']);
				$search_params['to'] = trim($safe_search_data['to']);
				$search_params['depature'] = date('d-m-Y', strtotime($new_date));//Adding new Date
				if(isset($safe_search_data['return'])) {
					$search_params['return'] = add_days_to_date($day_diff, $safe_search_data['return']);//Check it
				}
				$search_params['adult'] = intval($safe_search_data['adult_config']);
				$search_params['child'] = intval($safe_search_data['child_config']);
				$search_params['infant'] = intval($safe_search_data['infant_config']);
				$search_params['search_flight'] = 'search';
				$search_params['v_class'] = trim($safe_search_data['v_class']);
				$search_params['carrier'] = $safe_search_data['carrier'];
				redirect(base_url().'index.php/general/pre_flight_search/?'.http_build_query($search_params));
			} else {
				$this->template->view ( 'general/popup_redirect');
			}
		} else {
			$this->template->view ( 'general/popup_redirect');
		}
	}
	/**
	 * Jaganath
	 * Search Request from Fare Calendar
	 */
	function pre_fare_search_result()
	{
		$get_data = $this->input->get();
		if(isset($get_data['from']) == true && empty($get_data['from']) == false &&
		isset($get_data['to']) == true && empty($get_data['to']) == false &&
		isset($get_data['depature']) == true && empty($get_data['depature']) == false) {
			$from = trim($get_data['from']);
			$to = trim($get_data['to']);
			$depature = trim($get_data['depature']);
			$from_loc_details = $this->custom_db->single_table_records('flight_airport_list', '*', array('airport_code' => $from));
			$to_loc_details = $this->custom_db->single_table_records('flight_airport_list', '*', array('airport_code' => $to));
			if($from_loc_details['status'] == true && $to_loc_details['status'] == true) {
				$depature = date('Y-m-d', strtotime($depature));
				$airport_code = trim($from_loc_details['data'][0]['airport_code']);
				$airport_city = trim($from_loc_details['data'][0]['airport_city']);
				$from = $airport_city.' ('.$airport_code.')';
				//To
				$airport_code = trim($to_loc_details['data'][0]['airport_code']);
				$airport_city = trim($to_loc_details['data'][0]['airport_city']);
				$to = $airport_city.' ('.$airport_code.')';

				//Forming Search Request
				$search_params = array();
				$search_params['trip_type'] = 'oneway';
				$search_params['from'] = $from;
				$search_params['to'] = $to;
				$search_params['depature'] = $depature;
				$search_params['adult'] = 1;
				$search_params['child'] = 0;
				$search_params['infant'] = 0;
				$search_params['search_flight'] = 'search';
				$search_params['v_class'] = 'All';
				$search_params['carrier'] = array('');
				redirect(base_url().'index.php/general/pre_flight_search/?'.http_build_query($search_params));
			} else {
				$this->template->view ( 'general/popup_redirect');
			}
		} else {
			$this->template->view ( 'general/popup_redirect');
		}
	}
	/**
	 * Search Result
	 * @param number $search_id
	 */
	function search($search_id)
	{
		$safe_search_data = $this->flight_model->get_safe_search_data ( $search_id );
		// Get all the FLIGHT bookings source which are active
		$active_booking_source = $this->flight_model->active_booking_source ();
		if (valid_array ( $active_booking_source ) == true and $safe_search_data ['status'] == true) {
			$safe_search_data ['data'] ['search_id'] = abs ( $search_id );
			$page_params = array (
					'flight_search_params' => $safe_search_data ['data'],
					'active_booking_source' => $active_booking_source 
			);
			$page_params ['from_currency'] = get_application_default_currency ();
			$page_params ['to_currency'] = get_application_currency_preference ();

			//Need to check if its domestic travel
			$from_loc = $safe_search_data['data']['from_loc'];
			$to_loc = $safe_search_data['data']['to_loc'];
			$page_params['is_domestic_one_way_flight'] = false;
			if ($safe_search_data['data']['trip_type'] == 'oneway') {
				$page_params['is_domestic_one_way_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);
			}
			$page_params['airline_list'] = $this->db_cache_api->get_airline_code_list();//Jaganath
			$this->template->view ( 'flight/search_result_page', $page_params );
		} else {
			if ($safe_search_data['status'] == true) {
				$this->template->view ( 'general/popup_redirect');
			} else {
				$this->template->view ( 'flight/exception');
			}
		}
	}
	/**
	 * Arjun J Gowda
	 * Passenger Details page for final bookings
	 * Here we need to run farequote/booking based on api
	 * View Page for booking
	 */
	function booking($search_id) {
		/*
		 * FIXME
		 * The length of Passenger first name and last name combine cannot more
		 * than 35 characters
		 */
		$pre_booking_params = $this->input->post ();
		$booking_type = $pre_booking_params ['booking_type'];

		load_flight_lib ( $pre_booking_params ['booking_source'] );
		$safe_search_data = $this->flight_lib->search_data ( $search_id );
		$safe_search_data ['data'] ['search_id'] = intval ( $search_id );
		$token = $this->flight_lib->unserialized_token ( $pre_booking_params ['token'], $pre_booking_params ['token_key'] );

		if ($token ['status'] == SUCCESS_STATUS) {
			$pre_booking_params ['token'] = $token ['data'] ['token'];
		}
		if (isset ( $pre_booking_params ['booking_source'] ) == true && $safe_search_data ['status'] == true) {
			//Jaganath - Check Travel is Domestic or International
			$from_loc = $safe_search_data['data']['from_loc'];
			$to_loc = $safe_search_data['data']['to_loc'];
			$safe_search_data['data']['is_domestic_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);

			$page_data ['active_payment_options'] = $this->module_model->get_active_payment_module_list ();
			$page_data ['search_data'] = $safe_search_data ['data'];
			$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => get_application_default_currency (),
					'to' => get_application_default_currency () 
			) );
			// We will load different page for different API providers... As we have dependency on API for Flight details
			$page_data ['search_data'] = $safe_search_data ['data'];
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			//Not to show cache data in browser
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			switch ($pre_booking_params ['booking_source']) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					switch ($booking_type) {
						case 'process_fare_quote' :
							// upate fare details
							$quote_update = $this->fare_quote_booking ( $pre_booking_params );
							if ($quote_update ['status'] == FAILURE_STATUS) {
								redirect ( base_url () . 'index.php/flight/exception?op=Remote IO error @ Session Expiry&notification=session' );
							} else {
								$pre_booking_params = $quote_update ['data'];
							}
							break;
						case 'process_booking' :
							break;
						default :
							redirect ( base_url () );
					}
					// Load View
					$page_data ['booking_source'] = $pre_booking_params ['booking_source'];
					$page_data ['pre_booking_params'] ['default_currency'] = get_application_default_currency ();
					$page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['currency_obj'] = $currency_obj;
					//Traveller Details
					$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
					//Extracting Segment Summary and Fare Details
					$updated_flight_details = $pre_booking_params['token'];
					$is_price_Changed = false;
					$flight_details = array();
					foreach($updated_flight_details as $k => $v) {
						if($is_price_Changed == false && $v['IsPriceChanged'] == true) {
							$is_price_Changed = true;
						}
						$temp_flight_details = $this->flight_lib->extract_flight_segment_fare_details($v['FlightDetails'], $currency_obj, $search_id, $this->current_module);
						unset($temp_flight_details[0]['BookingType']);//Not needed in Next page
						$flight_details[$k] = $temp_flight_details[0];
					}
					//Merge the Segment Details and Fare Details For Printing Purpose
					$flight_pre_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
					$pre_booking_params['token'] = $flight_details;
					$page_data ['pre_booking_params'] = $pre_booking_params;
					$page_data['pre_booking_summery'] = $flight_pre_booking_summary;
					$TotalPrice = $flight_pre_booking_summary['FareDetails'][$this->current_module.'_PriceDetails']['TotalFare'];
					$page_data['convenience_fees'] = $currency_obj->convenience_fees($TotalPrice, $search_id);
					$page_data['is_price_Changed'] = $is_price_Changed;
					$this->template->view ( 'flight/tbo/tbo_booking_page', $page_data );
					break;
			}
		} else {
			redirect(base_url());
		}
	}

	/**
	 * Fare Quote Booking
	 * This will be used for TBO LCC carrier
	 */
	private function fare_quote_booking($flight_booking_details) {
		// LCCSpecialReturn vs LCCNormalReturn
		//currency conversion
		$fare_quote_details = $this->flight_lib->fare_quote_details ( $flight_booking_details );
		//debug($fare_quote_details); exit('ok');
		if($fare_quote_details['status'] == SUCCESS_STATUS && valid_array($fare_quote_details) == true) {
			//Converting API currency data to preferred currency
			$currency_obj = new Currency(array('module_type' => 'flight','from' => get_api_data_currency(), 'to' => get_application_currency_preference()));
			$fare_quote_details = $this->flight_lib->farequote_data_in_preferred_currency($fare_quote_details, $currency_obj);
		}		
		return $fare_quote_details;
		//return $this->flight_lib->fare_quote_details ( $flight_booking_details );
		
	}
	/**
	 * Get Extra Services
	 */
	private function get_extra_services($flight_booking_details)
	{
		$extra_service_details =  $this->flight_lib->get_extra_services ( $flight_booking_details );
		return $extra_service_details;
	}

	/**
	 * Arjun J Gowda
	 * Secure Booking of FLIGHT
	 * Process booking no view page
	 */
	function pre_booking($search_id) 
	{
		$post_params = $this->input->post ();
		if (valid_array ( $post_params ) == false) {
			redirect ( base_url () );
		}
		//Setting Static Data - Jaganath
		$post_params['billing_city'] = 'Bangalore';
		$post_params['billing_zipcode'] = '560100';
		$post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';

		$temp_booking = $this->module_model->serialize_temp_booking_record ( $post_params, FLIGHT_BOOKING );
		$book_id = $temp_booking ['book_id'];
		$book_origin = $temp_booking ['temp_booking_origin'];
		// Make sure token and temp token matches
		$temp_token = unserialized_data ( $post_params ['token'],$post_params ['token_key']);
		if ($temp_token != false) {
			load_flight_lib ( $post_params ['booking_source'] );
			$amount = 0;
			$currency = '';
			if ($post_params ['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {
				$currency_obj = new Currency ( array (
						'module_type' => 'flight',
						'from' => get_application_default_currency (),
						'to' => get_application_default_currency () 
				) );
				$flight_details = $temp_token['token'];
				$flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
				$fare_details = $flight_booking_summary['FareDetails'][$this->current_module.'_PriceDetails'];
				$amount = $fare_details['TotalFare'];
				$currency = $fare_details['CurrencySymbol'];
			}
			/********* Convinence Fees Start ********/
			$convenience_fees = ceil($currency_obj->convenience_fees($amount, $search_id));
			/********* Convinence Fees End ********/
			 	
			/********* Promocode Start ********/
			$promocode_discount = 0;
			/********* Promocode End ********/

			$email= $post_params ['billing_email'];
			$phone = $post_params ['passenger_contact'];
			$verification_amount = ($amount+$convenience_fees-$promocode_discount);
			$firstname = $post_params ['first_name'] ['0'] . " " . $post_params ['last_name'] ['0'];
			$book_id = $book_id;
			$productinfo = META_AIRLINE_COURSE ;

			// check current balance before proceeding further
			$domain_balance_status = $this->domain_management_model->verify_current_balance ( $verification_amount, $currency );
			if ($domain_balance_status == true) {
				//Save the Booking Data
				$booking_data = $this->module_model->unserialize_temp_booking_record ( $book_id, $book_origin );
				$book_params = $booking_data['book_attributes'];
				$data = $this->flight_lib->save_booking($book_id, $book_params,$currency_obj, $this->current_module);
				switch ($post_params ['payment_method']) {
					case PAY_NOW :
						$this->load->model('transaction');
						$pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
						$this->transaction->create_payment_record($book_id, $amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount,$pg_currency_conversion_rate);
						redirect(base_url().'index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin);
						break;
					case PAY_AT_BANK :
						echo 'Under Construction - Remote IO Error';
						exit ();
						break;
				}
			} else {
				redirect ( base_url () . 'index.php/flight/exception?op=Amount Flight Booking&notification=insufficient_balance' );
			}
		}
		redirect ( base_url () . 'index.php/flight/exception?op=Remote IO error @ FLIGHT Booking&notification=validation' );
	}
		/*
		process booking in backend until show loader 
	*/
	function process_booking($book_id, $temp_book_origin){
		
		if($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0){

			$page_data ['form_url'] = base_url () . 'mobile/index.php/flight/secure_booking/'.$book_id.'/'.$temp_book_origin;
			$page_data ['form_method'] = 'POST';
			$page_data ['form_params'] ['book_id'] = $book_id;
			$page_data ['form_params'] ['temp_book_origin'] = $temp_book_origin;
			redirect (base_url () . 'mobile/index.php/flight/secure_booking/' . $book_id . '/' . $temp_book_origin);
			// $this->template->view('share/loader/booking_process_loader', $page_data);	

		}else{
			redirect(base_url().'index.php/flight/exception?op=Invalid request&notification=validation');
		}
		
	}
	/**
	 * Arjun J Gowda
	 * Do booking once payment is successfull - Payment Gateway
	 * and issue voucher
	 */
	function secure_booking() 
	{
		// error_reporting(0);

		
		$post_data = $this->input->post();
		//debug($post_data);exit;
		//if(valid_array($post_data) == true && isset($post_data['book_id']) == true && isset($post_data['temp_book_origin']) == true && empty($post_data['book_id']) == false && intval($post_data['temp_book_origin']) > 0){
		$book_id = $post_data['book_id'];
		$temp_book_origin = $post_data['temp_book_origin'];
		if(!empty($book_id) && !empty($temp_book_origin) && intval($temp_book_origin) > 0){
			//verify payment status and continue
			$book_id = trim($book_id);
			$temp_book_origin = intval($temp_book_origin);
			$this->load->model('transaction');
			
			$booking_status = $this->transaction->get_payment_status($book_id);
			// $booking_status['status'] = 'accepted';
			//debug($booking_status);exit;
			/*if($booking_status['status'] !== 'accepted'){
				redirect(base_url().'mobile/index.php/flight/exception?op=Payment Not Done&notification=validation');
			}*/
		} else{
			redirect(base_url().'mobile/index.php/flight/exception?op=InvalidBooking&notification=invalid');
		}

		//run booking request and do booking
		$temp_booking = $this->module_model->unserialize_temp_booking_record ( $book_id, $temp_book_origin );
		//debug($temp_booking);exit;
		//Delete the temp_booking record, after accessing
		
		$this->module_model->delete_temp_booking_record ($book_id, $temp_book_origin);
				
		load_flight_lib ( $temp_booking ['booking_source'] );
		if ($temp_booking ['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {

			$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => admin_base_currency (),
					'to' => admin_base_currency () 
			) );
			$flight_details = $temp_booking ['book_attributes'] ['token'] ['token'];
			$flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
			$fare_details = $flight_booking_summary['FareDetails'][$this->current_module.'_PriceDetails'];
			$currency = $fare_details['Currency'];
		}
		// debug($temp_booking);exit;
		// verify payment status and continue
		if ($temp_booking != false) {
			switch ($temp_booking ['booking_source']) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					try {
						$booking = $this->flight_lib->process_booking ( $book_id, $temp_booking ['book_attributes'] );
					}catch (Exception $e) {
						$booking ['status'] = BOOKING_ERROR;
					}
					// Save booking based on booking status and book id
					break;
			}
			// debug($booking);exit;
			if (in_array($booking ['status'], array(SUCCESS_STATUS, BOOKING_CONFIRMED, BOOKING_PENDING, BOOKING_FAILED, BOOKING_ERROR, BOOKING_HOLD, FAILURE_STATUS)) == true) {
				$currency_obj = new Currency ( array (
						'module_type' => 'flight',
						'from' => admin_base_currency (),
						'to' => admin_base_currency () 
				) );
				$booking ['data'] ['booking_params'] ['currency_obj'] = $currency_obj;
				//Update the booking Details
				$ticket_details = @$booking ['data'] ['ticket'];
				$ticket_details['master_booking_status'] = $booking ['status'];
			
				$data = $this->flight_lib->update_booking_details( $book_id, $booking ['data'] ['booking_params'], $ticket_details, $this->current_module);
				
				//Update Transaction Details
				$this->domain_management_model->update_transaction_details ( 'flight', $book_id, $data ['fare'], $data ['admin_markup'], $data ['agent_markup'], $data['convinence'], $data['discount'],$data['transaction_currency'], $data['currency_conversion_rate'] );
				// debug($booking);exit;
				
				if (in_array ( $data ['status'], array (
					'BOOKING_CONFIRMED',
					'BOOKING_PENDING',
					'BOOKING_HOLD' 
					) )) {
						// Sms config & Checkpoint
						/* if (active_sms_checkpoint ( 'booking' )) {
						$msg = "Dear " . $data ['name'] . " Thank you for Booking your ticket with us.Ticket Details will be sent to your email id";
						$msg = urlencode ( $msg );
						$sms_status = $this->provab_sms->send_msg ( $data ['phone'], $msg );
						// return $sms_status;
						} */
				//	echo  base_url () . 'mobile/index.php/voucher/flight/' . $book_id . '/' . $temp_booking ['booking_source'] . '/' . $data ['status'] . '/show_voucher' ; exit;
						redirect ( base_url () . 'mobile/index.php/voucher/flight/' . $book_id . '/' . $temp_booking ['booking_source'] . '/' . $data ['status'] . '/show_voucher' );
					} else {
						redirect ( base_url () . 'mobile/index.php/flight/exception?op=booking_exception&notification=' . $booking ['message'] );
					}
						
			} else {
				redirect ( base_url () . 'mobile/index.php/flight/exception?op=booking_exception&notification=' . $booking ['message'] );
			}
		}
	}
	/**
	 * Arjun J Gowda
	 * Do booking once payment is successfull - Payment Gateway
	 * and issue voucher
	 */
	function secure_booking_old() 
	{
		$book_id = $this->input->post('book_id');
		$temp_book_origin  = $this->input->post('temp_book_origin');
		//run booking request and do booking
		$temp_booking = $this->module_model->unserialize_temp_booking_record ( $book_id, $temp_book_origin );
		// FIXME Delete record from database
		load_flight_lib ( $temp_booking ['booking_source'] );
		if ($temp_booking ['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {
			$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => get_application_default_currency (),
					'to' => get_application_default_currency () 
			) );
			$flight_details = $temp_booking ['book_attributes'] ['token'] ['token'];
			$flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
			$fare_details = $flight_booking_summary['FareDetails'][$this->current_module.'_PriceDetails'];
			$total_booking_price = $fare_details['TotalFare'];
			$currency = $fare_details['Currency'];
		}
		// verify payment status and continue
		// Flight_Model::lock_tables();
		$domain_balance_status = $this->domain_management_model->verify_current_balance ( $total_booking_price, $currency );
		if ($domain_balance_status) {
			if ($temp_booking != false) {
				switch ($temp_booking ['booking_source']) {
					case PROVAB_FLIGHT_BOOKING_SOURCE :
						try {
							$booking = $this->flight_lib->process_booking ( $book_id, $temp_booking ['book_attributes'] );
							
						}catch (Exception $e) {
							$booking ['status'] = BOOKING_ERROR;
						}
						// Save booking based on booking status and book id
						break;
				}
				if (in_array($booking ['status'], array(SUCCESS_STATUS, BOOKING_CONFIRMED, BOOKING_PENDING, BOOKING_FAILED, BOOKING_ERROR)) == true) {
					$currency_obj = new Currency ( array (
							'module_type' => 'flight',
							'from' => get_application_default_currency (),
							'to' => get_application_default_currency () 
					) );
					$booking ['data'] ['booking_params'] ['currency_obj'] = $currency_obj;
					//Update the booking Details
					$icket_details = @$booking ['data'] ['ticket'];
					$icket_details['master_booking_status'] = $booking ['status'];
					$data = $this->flight_lib->update_booking_details( $book_id, $booking ['data'] ['booking_params'], $icket_details, @$booking ['data'] ['book'], $this->current_module);
					$this->domain_management_model->update_transaction_details ( 'flight', $book_id, $data ['fare'], $data ['admin_markup'], $data ['agent_markup'], $data['convinence'], $data['discount'] );
					if (in_array ( $data ['status'], array (
							'BOOKING_CONFIRMED',
							'BOOKING_PENDING' 
							) )) {
								// Sms config & Checkpoint
								if (active_sms_checkpoint ( 'booking' )) {
									$msg = "Dear " . $data ['name'] . " Thank you for Booking your ticket with us.Ticket Details will be sent to your email id";
									$this->provab_sms->send_msg ( $data ['phone'], $msg );
										// return $sms_status;
								}

								$app_reference   = $book_id;
								$booking_source  = $temp_booking ['booking_source'];
								$booking_status  = $data ['status'];
								$this->sendmail($app_reference,$booking_source,$booking_status);


								/*redirect ( base_url () . 'index.php/voucher/flight/' . $book_id . '/' . $temp_booking ['booking_source'] . '/' . $data ['status'] . '/show_voucher' );*/
								$resp['status'] = SUCCESS_STATUS;
								$resp['message'] = "Booking Confirmed";
								$resp['data'] =array('app_reference'=>$book_id,'booking_source'=>$temp_booking ['booking_source'],'booking_status'=>$data ['status']);
								echo json_encode($resp); exit;
							} else {
								// redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $booking ['msg'] );
								$resp['status'] = FAILURE_STATUS;
								$resp['message'] = "Booking Failed";
								$resp['data'] = array();
								echo json_encode($resp); exit;
							}
				} else {
					redirect ( base_url () . 'index.php/flight/exception?op=booking_exception&notification=' . $booking ['msg'] );
				}
			}
			// release table lock
			Flight_Model::release_locked_tables ();
		} else {
			// release table lock
			Flight_Model::release_locked_tables ();
			//echo base_url () . 'index.php/flight/exception?op=Remote IO error @ Insufficient&notification=validation';
			$response['status'] = FAILURE_STATUS;
			$response['message'] = "Insufficient Balance, Please contact admin";
			echo json_encode($response); exit;
			
		}
		// redirect(base_url().'index.php/flight/exception?op=Remote IO error @ FLIGHT Secure Booking&notification=validation');
	}

	/**
	 * Arjun J Gowda
	 * Process booking on hold - pay at bank
	 * Issue Ticket Later
	 */
	function booking_on_hold($book_id) {

	}
	/**
	 * Jaganath
	 */
	function pre_cancellation($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);
				$page_data['data'] = $assembled_booking_details['data'];
				$this->template->view('flight/pre_cancellation', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	function cancel_booking_mobile()
	{
		// error_reporting(E_ALL);
		$flight_data = $this->input->post('flight_cancel');
		$flight_data = json_decode($flight_data);
		$flight_data = json_decode(json_encode($flight_data), True);
		$app_reference = trim($flight_data['app_reference']);
		$booking_source = trim($flight_data['booking_source']);
		$transaction_origin = $flight_data['transaction_origin'][0];
		$passenger_data = $this->flight_model->get_passenger_info($app_reference);
		foreach($passenger_data as $key => $pass){
			$passenger_origin[$key] = $pass['origin'];
		}
		// debug($passenger_origin);exit;
		
			// debug($passenger_origin);exit;
			$booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
			// debug($booking_details);exit;
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib($booking_source);
				//Formatting the Data
				$this->load->library('booking_data_formatter');
				$booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, $this->current_module);
				$booking_details = $booking_details['data'];

				//Grouping the Passenger Ticket Ids
				$grouped_passenger_ticket_details = $this->flight_lib->group_cancellation_passenger_ticket_id($booking_details, $passenger_origin);
				$passenger_origin = $grouped_passenger_ticket_details['passenger_origin'];
				// debug($grouped_passenger_ticket_details);exit;
				$passenger_ticket_id = $grouped_passenger_ticket_details['passenger_ticket_id'];
				// debug($booking_details);
				// debug($passenger_origin);
				// debug($passenger_ticket_id);exit;
				$cancellation_details = $this->flight_lib->cancel_booking($booking_details, $passenger_origin, $passenger_ticket_id);
				//debug($cancellation_details);exit;
				if($cancellation_details['status'] == 1){
					$data['status'] = SUCCESS_STATUS ;
					$data['message'] = "Your Cancellation Request has been sent";
				}
				else{
					$data['status'] = FAILURE_STATUS;
					$data['message'] = "Some thing went wrong Please Try Again !!! ";
				}
				echo json_encode($data);
			}
		
	}
	/**
	 * Jaganath
	 * @param $app_reference
	 */
	function cancel_booking($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_flight_booking_data($master_booking_details, $this->current_module);
				$master_booking_details = $master_booking_details['data'];
				load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				$cancellation_details = $this->flight_lib->cancel_full_booking($master_booking_details);
				redirect('flight/cancellation_details/'.$app_reference.'/'.$booking_source);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	function cancellation_details($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $GLOBALS['CI']->flight_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$page_data = array();
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_flight_booking_data($master_booking_details, $this->current_module);
				$page_data['data'] = $master_booking_details['data'];
				$this->template->view('flight/cancellation_details', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}

	}
	/**
	 * Import Temp Booking data
	 */
	function import_temp_booking_data($book_id='FB03-125019-840625', $temp_book_origin=140)
	{
		$temp_booking = $this->module_model->unserialize_temp_booking_record ( $book_id, $temp_book_origin );
		if (valid_array($temp_booking) == true) {
			//debug($temp_booking);
			//echo 'transaction log tabel wrt app_reference';
			load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
			$currency_obj = new Currency ( array (
						'module_type' => 'flight',
						'from' => get_application_default_currency (),
						'to' => get_application_default_currency () 
				) );
			$book_params = $temp_booking['book_attributes'];
			$book_params['currency_obj'] = $currency_obj;
															
			$data = $this->flight_lib->save_booking($book_id, $book_params);
			//debug($data);
			$this->domain_management_model->update_transaction_details ( 'flight', $book_id, $data ['fare'], $data ['admin_markup'], $data ['agent_markup'], $data['convinence'], $data['discount'] );
			echo 'data saved';exit;
		} else {
			echo 'No data';exit;
		}
	}
	/**
	 * Arjun J Gowda
	 */
	function exception() {
		$module = META_AIRLINE_COURSE;
		$op = @$_GET ['op'];
		$notification = @$_GET ['notification'];
		$eid = $this->module_model->log_exception ( $module, $op, $notification );
		// set ip log session before redirection
		$this->session->set_flashdata ( array (
				'log_ip_info' => true 
		) );
		redirect ( base_url () . 'mobile/index.php/flight/event_logger/' . $eid );
	}
	function event_logger($eid = '') {
		$log_ip_info = $this->session->flashdata ( 'log_ip_info' );
		$this->template->view ( 'flight/exception', array (
				'log_ip_info' => $log_ip_info,
				'eid' => $eid 
		) );
	}
	function test_server()
	{
		$data = $this->custom_db->single_table_records('test', '*', array('origin' => 851));
		$response = json_decode($data['data'][0]['test'], true);
	}
	function booking_mobile()
	{
		// error_reporting(E_ALL);
		$post_params = $this->input->post ('farequote_data');
		// debug($post_params);exit;

		$var = json_decode($post_params);		

		$pre_booking_params_post = json_decode(json_encode($var), True);
		
		$pre_booking_params =  array();
		$pre_booking_params['is_domestic'] = $pre_booking_params_post['is_domestic'];
		$pre_booking_params['token'] = $pre_booking_params_post['token'];
		$pre_booking_params['token_key'] = $pre_booking_params_post['token_key'];
		$pre_booking_params['search_access_key'] = $pre_booking_params_post['search_access_key'];
		$pre_booking_params['is_lcc'] = '1';
		$pre_booking_params['promotional_plan_type'] = '0';
		$pre_booking_params['booking_type'] = 'process_fare_quote';
		$pre_booking_params['booking_source'] = 'PTBSID0000000002';

		$booking_type = $pre_booking_params ['booking_type'];
		$search_id = $pre_booking_params_post['search_id'];
		// echo 'herre'.$search_id;exit;
		load_flight_lib ( $pre_booking_params ['booking_source'] );
		$safe_search_data = $this->flight_lib->search_data ( $search_id );
		$safe_search_data ['data'] ['search_id'] = intval ( $search_id );
		$token = $this->flight_lib->unserialized_token ( $pre_booking_params ['token'], $pre_booking_params ['token_key'] );
		// debug($token);exit;
		if ($token ['status'] == SUCCESS_STATUS) {
			$pre_booking_params ['token'] = $token ['data'] ['token'];
		}
		if (isset ( $pre_booking_params ['booking_source'] ) == true && $safe_search_data ['status'] == true) {
			//Jaganath - Check Travel is Domestic or International
			$from_loc = $safe_search_data['data']['from_loc'];
			$to_loc = $safe_search_data['data']['to_loc'];
			$safe_search_data['data']['is_domestic_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);

			$page_data ['active_payment_options'] = $this->module_model->get_active_payment_module_list ();
			$page_data ['search_data'] = $safe_search_data ['data'];
			$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => get_application_default_currency (),
					'to' => get_application_default_currency () 
			) );
			// We will load different page for different API providers... As we have dependency on API for Flight details
			$page_data ['search_data'] = $safe_search_data ['data'];
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			//Not to show cache data in browser
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			switch ($pre_booking_params ['booking_source']) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					switch ($booking_type) {
						case 'process_fare_quote' : 
							// upate fare details
							//debug($pre_booking_params);exit;
							$quote_update = $this->fare_quote_booking ( $pre_booking_params );
							//debug($quote_update ); exit;
							if ($quote_update ['status'] == FAILURE_STATUS) {
								$data['status'] = FAILURE_STATUS;
								$data['message'] ="Unable to Process your request please try again";
								$this->output_compressed_data($data);
							} else {
								$pre_booking_params = $quote_update ['data'];
								$extra_services = $this->get_extra_services($pre_booking_params);
								if($extra_services['status'] == SUCCESS_STATUS){
									$page_data['extra_services'] = $extra_services['data'];
								} else {
									$page_data['extra_services'] = array();
								}
							}
							break;
						case 'process_booking' :
							break;
			
					}
				
					// Load View
					$page_data ['booking_source'] = $pre_booking_params ['booking_source'];
					$page_data ['pre_booking_params'] ['default_currency'] = get_application_default_currency ();
					
					$page_data ['currency_obj'] = $currency_obj;
					//Traveller Details
					$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
					//Extracting Segment Summary and Fare Details
					$updated_flight_details = $pre_booking_params['token'];
					$flight_details = array();
					//debug($updated_flight_details);exit;
					$empty_array = array();
					foreach($updated_flight_details as $k => $v) {
						$temp_flight_details = $this->flight_lib->extract_flight_segment_fare_details($v, $currency_obj, $search_id, $this->current_module,false,$empty_array,$empty_array,$empty_array,0,true);
						unset($temp_flight_details[0]['BookingType']);//Not needed in Next page
						$flight_details[$k] = $temp_flight_details[0];
					}
					//Merge the Segment Details and Fare Details For Printing Purpose
					
					$flight_pre_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);

					$pre_booking_params['token'] = $flight_details;
					$pre_booking_params['extra_services'] = $extra_services;
					$page_data ['pre_booking_params'] = $pre_booking_params;
					$page_data['pre_booking_summery'] = $flight_pre_booking_summary;
					$TotalPrice = $flight_pre_booking_summary['FareDetails'][$this->current_module.'_PriceDetails']['TotalFare'];
					$page_data['convenience_fees'] = $currency_obj->convenience_fees($TotalPrice, $search_id);
					
					/*if (is_logged_in_user() == true) {
					$this->load->model('recharge_model');
					$id = $GLOBALS['CI']->entity_user_id ;
					$magento_details = $this->recharge_model->get_magento_userid($id);
					$login_data['magento_user_id'] = $magento_details[0]['magento_user_id'] ;
					$login_data['REQUEST_METHOD'] = 'POST';
					$wallet_amount_details =  $this->magento_controller->get_wallet_amount($login_data);
					//echo "<pre>";print_r($wallet_amount_details); exit();
					$page_data['wallet_amount'] = $wallet_amount_details['data']['wallet_amount'];
					}*/
						//session expiry time calculation
					$page_data['session_expiry_details'] = $this->flight_lib->set_flight_search_session_expiry(true, $search_hash);
					
					$page_data['trip_type']		= $safe_search_data ['data'] ['trip_type'];
					$page_data['search_id']		= $search_id;
					$dynamic_params_url         = serialized_data($pre_booking_params);
					
					// $response['token']			= $dynamic_params_url;
					// $response['token_key']		= md5($dynamic_params_url);
					// echo $dynamic_params_url;exit;
				//	debug($dynamic_params_url); exit;
					// $response['status'] = true;
					$response['status'] = 1;
					$response['data'] = $page_data; 
					$response['data']['token'] = $dynamic_params_url;
					$response['data']['token_key'] = md5($dynamic_params_url);

					$this->CI = &get_instance();
					$this->CI->load->driver('cache');
					$cache_search = $this->CI->config->item('cache_flight_ssr_search');
					$search_hash =md5 ($pre_booking_params_post['search_id']);				
					
					if ($cache_search) {
							$cache_exp = $this->CI->config->item('cache_flight_search_ttl');
							$this->CI->cache->file->save($search_hash, $page_data, $cache_exp);
						}
					if ($cache_search) {
					$cache_contents = $this->CI->cache->file->get($search_hash);
						
						}
					$response['data']['search_hash_ssr'] =$search_hash;
					

					// debug($response);exit;
					//echo json_encode($response);

					$this->output_compressed_data($response);
					exit;	
			}
		} else {
			// $response['status'] = false;
			$response['status'] = 0;
			$response['msg'] = 'Failed in Farequote';
			//echo json_encode($response);
			$this->output_compressed_data($response);
		}
	}
	function booking_mobile_old()
	{
		// error_reporting(E_ALL);
		$post_params = $this->input->post ('farequote_data');
		//$post_params = '{"is_domestic":"1","token":["czoyMToiMTQ4NDY0ODM4MzY3MTUqXypPQjEwIjs="],"token_key":["c20b062b0f77cd42bbdf838aa64ed957"],"search_access_key":["MTQ4NDY0ODM4Mzk3MTg5NjY0NTk0MjA4OTUyMTE3MzgyMTQxMDIwNzc5NjUxODhfX18wUjBfX18yMDczODQ3Yy01MGU2LTQwYmQtYjUwZC1kZWU4ZmZlZjc2MmZfX19QVEJTSUQwMDAwMDAwMDAy"],"is_lcc":[""],"promotional_plan_type":[""],"booking_type":"process_fare_quote","booking_source":"PTBSID0000000002","search_id":"61"}';
		$var = json_decode($post_params);		
		$pre_booking_params_post = json_decode(json_encode($var), True);
		$pre_booking_params =  array();
		$pre_booking_params['is_domestic'] = $pre_booking_params_post['is_domestic'];
		$pre_booking_params['token'] = $pre_booking_params_post['token'];
		$pre_booking_params['token_key'] = $pre_booking_params_post['token_key'];
		$pre_booking_params['search_access_key'] = $pre_booking_params_post['search_access_key'];
		$pre_booking_params['is_lcc'] = '1';
		$pre_booking_params['promotional_plan_type'] = '0';
		$pre_booking_params['booking_type'] = 'process_fare_quote';
		$pre_booking_params['booking_source'] = 'PTBSID0000000002';

		$booking_type = $pre_booking_params ['booking_type'];
		$search_id = $pre_booking_params_post['search_id'];

		load_flight_lib ( $pre_booking_params ['booking_source'] );
		$safe_search_data = $this->flight_lib->search_data ( $search_id );
		$safe_search_data ['data'] ['search_id'] = intval ( $search_id );
		$token = $this->flight_lib->unserialized_token ( $pre_booking_params ['token'], $pre_booking_params ['token_key'] );

		if ($token ['status'] == SUCCESS_STATUS) {
			$pre_booking_params ['token'] = $token ['data'] ['token'];
		}
		if (isset ( $pre_booking_params ['booking_source'] ) == true && $safe_search_data ['status'] == true) {
			//Jaganath - Check Travel is Domestic or International
			$from_loc = $safe_search_data['data']['from_loc'];
			$to_loc = $safe_search_data['data']['to_loc'];
			$safe_search_data['data']['is_domestic_flight'] = $this->flight_model->is_domestic_flight($from_loc, $to_loc);

			$page_data ['active_payment_options'] = $this->module_model->get_active_payment_module_list ();
			$page_data ['search_data'] = $safe_search_data ['data'];
			$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => get_application_default_currency (),
					'to' => get_application_default_currency () 
			) );
			// We will load different page for different API providers... As we have dependency on API for Flight details
			$page_data ['search_data'] = $safe_search_data ['data'];
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			$page_data['pax_details'] = $this->user_model->get_current_user_details();

			//Not to show cache data in browser
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			switch ($pre_booking_params ['booking_source']) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					switch ($booking_type) {
						case 'process_fare_quote' :
							// upate fare details
							$quote_update = $this->fare_quote_booking ( $pre_booking_params );
							if ($quote_update ['status'] == FAILURE_STATUS) {
								$data['status'] = FAILURE_STATUS;
								$data['message'] ="Unable to Process your request please try again";
								echo json_encode($data); die;
							} else {
								$pre_booking_params = $quote_update ['data'];
							}
							break;
						case 'process_booking' :
							break;
			
					}
					// Load View
					$page_data ['booking_source'] = $pre_booking_params ['booking_source'];
					$page_data ['pre_booking_params'] ['default_currency'] = get_application_default_currency ();
					// $page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_code ();
					// $page_data ['country_list'] = $this->db_cache_api->get_iso_country_code ();
					$page_data ['currency_obj'] = $currency_obj;
					//Traveller Details
					$page_data['traveller_details'] = $this->user_model->get_user_traveller_details();
					//Extracting Segment Summary and Fare Details
					$updated_flight_details = $pre_booking_params['token'];
					$flight_details = array();
					foreach($updated_flight_details as $k => $v) {
						$temp_flight_details = $this->flight_lib->extract_flight_segment_fare_details($v['FlightDetails'], $currency_obj, $search_id, $this->current_module);
						unset($temp_flight_details[0]['BookingType']);//Not needed in Next page
						$flight_details[$k] = $temp_flight_details[0];
					}
					//Merge the Segment Details and Fare Details For Printing Purpose
					$flight_pre_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
					$pre_booking_params['token'] = $flight_details;
					$page_data ['pre_booking_params'] = $pre_booking_params;
					$page_data['pre_booking_summery'] = $flight_pre_booking_summary;
					$TotalPrice = $flight_pre_booking_summary['FareDetails'][$this->current_module.'_PriceDetails']['TotalFare'];
					$page_data['convenience_fees'] = $currency_obj->convenience_fees($TotalPrice, $search_id);
					
					if (is_logged_in_user() == true) {
					$this->load->model('recharge_model');
					$id = $GLOBALS['CI']->entity_user_id ;
					$magento_details = $this->recharge_model->get_magento_userid($id);
					$login_data['magento_user_id'] = $magento_details[0]['magento_user_id'] ;
					$login_data['REQUEST_METHOD'] = 'POST';
					$wallet_amount_details =  $this->magento_controller->get_wallet_amount($login_data);
					//echo "<pre>";print_r($wallet_amount_details); exit();
					$page_data['wallet_amount'] = $wallet_amount_details['data']['wallet_amount'];
					}

					$page_data['trip_type']		= $safe_search_data ['data'] ['trip_type'];
					$page_data['search_id']		= $search_id;
					$dynamic_params_url         = serialized_data($pre_booking_params);
					$response['token']			= $dynamic_params_url;
					$response['token_key']		= md5($dynamic_params_url);
					$response['status'] = true;
					$response['data'] = $page_data;
					echo json_encode($response);
					exit;	
			}
		} else {
			$response['status'] = false;
			$response['msg'] = 'Failed in Farequote';
			echo json_encode($response);
		}
	}

	function pre_booking_mobile($search_id='') 
	{    

	 	
		$passenger_data = $this->input->post('flight_book');
		$booking_step = @$_REQUEST['booking_step'];
		$search_ssr_hash =$_REQUEST['search_ssr_hash'];	

		
		//debug($passenger_data); exit();
		if(empty($passenger_data) && empty($key_token) && empty($token))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to process your request";
			echo json_encode($data);
			exit;
		}
		$passenger_data = json_decode($passenger_data,true);
		// debug($passenger_data['tageduser']);exit;
		
		//$passenger_token = json_decode($key_token);

		//$key_token = json_decode(json_encode($passenger_token), True);
		//debug($key_token);exit;
		//debug($api_token); die;
		
		$token = json_decode($this->input->post('token'));
		//	 debug($this->input->post('token'));exit;
		$passenger_data = json_decode(json_encode($passenger_data), True);
		$key_token  = $passenger_data['token_key'];
		$token = json_decode(json_encode($token), True);
		$token = $token['token_id'];
		// echo base64_decode($token);
	//	exit;
		
		// $passenger_type = array();
		$post_params1 = array();
		foreach ($passenger_data['Passengers'] as $key => $value) {

			$post_params1['passenger_type'][$key] = $value['passenger_type'];
			$post_params1['lead_passenger'][$key] = $value['lead_passenger'];
			$post_params1['lead_passenger'][$key] = $value['Gender'];
			$post_params1['passenger_nationality'][$key] = '92';
			$post_params1['gender'][$key] = $value['Gender'];
			$post_params1['name_title'][$key] = $value['Title'];
			$post_params1['first_name'][$key] = $value['FirstName'];
			$post_params1['last_name'][$key] = $value['LastName'];
			$post_params1['passenger_passport_number'][$key] = $value['PassportNumber'];
			$post_params1['date_of_birth'][$key] = $value['DateOfBirth'];
			$post_params1['passenger_passport_issuing_country'][$key] = $value['PassportIssueCountry'];
			$pass_exp = explode('-', $value['PassportExpiry']);
			$post_params1['passenger_passport_expiry_day'][$key] = $pass_exp['0'];
			$post_params1['passenger_passport_expiry_month'][$key] = $pass_exp['1'];
			$post_params1['passenger_passport_expiry_year'][$key] = $pass_exp['2'];

			// $CountryCode 				=	array($passenger_data['CountryCode']);
			// $CountryName 				=	array($passenger_data['CountryName']);
			// $ContactNo 					=	$passenger_data['ContactNo'];
			// $City 						=	$passenger_data['City'];
			// $PinCode 					=	$passenger_data['PinCode'];
			// $AddressLine1 				=	$passenger_data['AddressLine1'];
			// $Email						=	$passenger_data['Email'];
		}
		if(isset($passenger_data['payment_method'])){
			$payment_method = $passenger_data['payment_method'];
		}
		else{
			$payment_method = 'PNHB1';
		}
		$post_params1['total_amount_val'] = $passenger_data['total_amount_val'];
		$post_params1['convenience_fee'] = $passenger_data['convenience_fee'];
		$post_params1['currency_symbol'] = $passenger_data['currency_symbol'];
		$post_params1['currency'] = $passenger_data['currency'];
		$post_params1['billing_email'] = $passenger_data['Email'];
		$post_params1['billing_email'] = $passenger_data['Email'];
		$post_params1['country_code'] = $passenger_data['CountryCode'];
		$post_params1['passenger_contact'] = $passenger_data['ContactNo'];
		$post_params1['payment_method']  = $payment_method;
		$post_params1['customer_id'] = $passenger_data['customer_id'];
		$post_params1['op'] = 'book_room';
    	$post_params1['booking_source'] = PROVAB_FLIGHT_BOOKING_SOURCE;
    	$post_params1['promo_code_discount_val'] = '0.00';
    	$post_params1['promo_code'] = '';
    	$post_params1['search_id'] = $passenger_data['search_id'];
    	$post_params1['token'] = $token;
    	$post_params1['token_key'] = $key_token;
    	foreach($passenger_data['baggage_0'] as $key => $value){
    		// debug($value);exit;
    		$post_params1['baggage_0'][$key]  = @$value[$key];
    	}
    	foreach($passenger_data['meal_0'] as $key => $value){
    		// debug($value);exit;
    		$post_params1['meal_0'][$key]  = @$value[$key];
    	}
    	foreach($passenger_data['seat_0'] as $key => $value){
    		// debug($value);exit;
    		$post_params1['seat_0'][$key]  = @$value[$key];
    	}
    	// debug($post_params1);exit;
    	// $post_params1['meal_0'] = @$passenger_data['meal_0'];
    	// $post_params1['seat_0'] = @$passenger_data['seat_0'];
		// debug($post_params1);exit;
		
		// $PassportExpiry = explode('-', $PassportExpiry);
		
		// $post_params = array(
		// 	'search_id'								=>		$passenger_data['search_id'],
			
		// 	'token'									=>      $token,
		// 	'token_key'=>$key_token,
		// 	//'token_key'								=>		$passenger_data['ApiToken']['TokenId'],
		// 	'op'									=>		'book_room',
		// 	'booking_source'						=> 		PROVAB_FLIGHT_BOOKING_SOURCE,
		// 	'passenger_type'						=>		array('Adult'),
		// 	'lead_passenger'						=>		array('1'),
		// 	'gender'								=>		$Gender,
		// 	'passenger_nationality'					=>		array('92'),
		// 	'name_title'							=>		$Title,
		// 	'first_name'							=>		$FirstName,
		// 	'last_name'								=>		$LastName,
		// 	'date_of_birth'							=>		$DateOfBirth,
		// 	'passenger_passport_number'				=>		$PassportNumber,
		// 	'passenger_passport_issuing_country'	=>		$PassportIssueCountry,
		// 	'passenger_passport_expiry_day'			=>		array($PassportExpiry[0]),
		// 	'passenger_passport_expiry_month'		=>		array($PassportExpiry[1]),
		// 	'passenger_passport_expiry_year'		=>		array($PassportExpiry[2]),
		// 	'billing_email'							=>		$Email,
		// 	'passenger_contact'						=>		$ContactNo,
			
		// 	'pay_amount'							=>		$passenger_data['final_fare'],
		// 	'payment_method'						=>		($passenger_data['payment_method'])?$passenger_data['payment_method']:'PNHB1',
		// 	'customer_id'   					    =>      $passenger_data['customer_id']
		// 	);
		
		$post_params1['billing_city'] = 'Bangalore';
		$post_params1['billing_country'] = 'india';
		$post_params1['billing_zipcode'] = '560100';
		$post_params1['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';
		$post_params1['tc'] = 'on';


		if (valid_array ( $post_params1 ) == false) {
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "please enter the Valid data to book the ticket";
			$this->output_compressed_data($data);
			exit;
		}
		// debug($post_params1);exit;
		// Make sure token and temp token matches
		$valid_temp_token = unserialized_data ( $post_params1 ['token'] , $post_params1 ['token_key']);

		if ($valid_temp_token != false) {

			if($booking_step == 'additional_ssr'){
				$this->additional_ssr($search_ssr_hash);
			}


			load_flight_lib ( $post_params1 ['booking_source'] );
				$amount = 0;
				$currency = '';
			//Setting Static Data - Jaganath		
			// debug($post_params1);exit;
			// Make sure token and temp token matches
			$serealize_post = $post_params1;
			$temp_token1 = unserialized_data ( $post_params1 ['token']);
			$post_params1['token'] =  $temp_token1;
			$temp_token = $post_params1;
			// debug($temp_token);exit;
			if ($temp_token != false) {
				$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => get_application_currency_preference (),
					'to' => get_application_default_currency () 
				));
					// debug($post_params1);exit;
				// debug($post_params);exit;
				$post_params1['token']['token'] = $this->flight_lib->convert_token_to_application_currency($post_params1['token']['token'], $currency_obj, $this->current_module,'web');
				// debug($post_params1);exit;
				//Convert to Extra Services to application currency
				if(isset($post_params1['token']['extra_services']) == true){
				 	$post_params1['token']['extra_services'] = $extra_services = $this->flight_lib->convert_extra_services_to_application_currency($post_params1['token']['extra_services'], $currency_obj);
				}	
				// $temp_token1['extra_services'] = $extra_services;
				$token_se = serialized_data($post_params1['token']);
				$post_params1['token'] = $token_se;
				
				$temp_booking = $this->module_model->serialize_temp_booking_record ( $post_params1, FLIGHT_BOOKING );

				$book_id = $temp_booking ['book_id'];
				$book_origin = $temp_booking ['temp_booking_origin'];
				$user_id='';
				if(isset($passenger_data['tageduser']) && isset($passenger_data['customer_id']))
				{
					
					$tagged_users=$passenger_data['tageduser'];
					// debug($tagged_users);exit;
					$tagged_data=$this->db_cache_api->insert_tagged_user($book_id,$tagged_users,$passenger_data['customer_id']);
					$user_id=$passenger_data['customer_id'];
				}
				// debug($tagged_data);exit;
				if ($post_params1 ['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {
					$currency_obj = new Currency ( array (
							'module_type' => 'flight',
							'from' => get_application_default_currency (),
							'to' => get_application_default_currency () 
					) );
					// $temp_token = $temp_token1;
					// debug($post_params1);exit;
					$flight_details = unserialized_data ( $post_params1 ['token']);
					$flight_details = $flight_details['token'];
					// debug($flight_details);exit;
					$flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
					// debug($flight_booking_summary);exit;
					$fare_details = $flight_booking_summary['FareDetails'][$this->current_module.'_PriceDetails'];
					// debug($fare_details);exit;
					$amount = $fare_details['TotalFare'];
					$currency = $fare_details['CurrencySymbol'];
				}
				
				/********* Convinence Fees Start ********/
				$convenience_fees = ceil($currency_obj->convenience_fees($amount, $search_id));
				/********* Convinence Fees End ********/
				 	
				/********* Promocode Start ********/
				$promocode_discount = 0;
				/********* Promocode End ********/

				$email= $post_params1 ['billing_email'];
				$phone = $post_params1 ['passenger_contact'];
				$verification_amount = ($amount+$convenience_fees-$promocode_discount);
				$firstname = $post_params1 ['first_name'] ['0'] . " " . $post_params1 ['last_name'] ['0'];
				$book_id = $book_id;
				$productinfo = META_AIRLINE_COURSE ;

				// check current balance before proceeding further
				$domain_balance_status = $this->domain_management_model->verify_current_balance ( $verification_amount, $currency );

				if ($domain_balance_status == true) {
					//Save the Booking Data
					$booking_data = $this->module_model->unserialize_temp_booking_record ( $book_id, $book_origin );
					$booking_data['book_attributes']['token']['booking_source'] = $booking_data['book_attributes']['booking_source'];
					$booking_data['book_attributes']['search_id'] = $passenger_data['search_id']; 
					//debug($booking_data); die;
					$book_params = $booking_data['book_attributes'];
					$GLOBALS['CI']->entity_user_id = $passenger_data['customer_id'];
					$data = $this->flight_lib->save_booking($book_id, $book_params,$currency_obj, $this->current_module,$user_id);
					//Add Extra Service Price to Booking Amount
					$extra_services_total_price = $this->flight_model->get_extra_services_total_price($book_id);
					$amount = $amount+$extra_services_total_price;
					// echo 'herre'.$amount;exit;
					switch ($post_params1 ['payment_method']) {
						case PAY_NOW :
					
							$this->load->model('transaction');
					$pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
							$this->transaction->create_payment_record($book_id, $amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount,$pg_currency_conversion_rate);
							//$pre_booking_status['redirect'] = base_url().'index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;
							$pre_booking_status['data'] = array('app_reference'=>$book_id,'book_origin'=>$book_origin);
							$url=explode('/mobile_webservices', base_url());
							// debug($url);exit;
							$pre_booking_status['retun_url']   =$url[0].'/index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;
							$pre_booking_status['status']   = SUCCESS_STATUS;
							$this->output_compressed_data($pre_booking_status);
							exit();
						case PAY_AT_BANK :
							echo 'Under Construction - Remote IO Error';
							exit ();
							break;
					}
				} else {
					$data['status'] = FAILURE_STATUS;
					$data['message'] = "We can't process your request please contact our Customer Support";
					$this->output_compressed_data($data);
					exit;
				}
			}
			else{
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Invalid Request";
				$this->output_compressed_data($data);
				exit;
			}
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Invalid Token (Token Key Mismatch)";
			$this->output_compressed_data($data);
			exit;
		}
	}


	// Jun 7 2018,(generating web view for meal, seat, bag details)
//after entering pax name details, before payment gateway page, they have to call this api.
function additional_ssr($search_ssr_hash){
	$response['status'] = false;
	$response['msg'] = 'Failed in  additional SSr';
	//$search_ssr_hash =$_REQUEST['search_ssr_hash'];
	$this->CI = &get_instance();
	$this->CI->load->driver('cache');
	if(!empty($search_ssr_hash)){
	$passenger_data_json=$_REQUEST['flight_book'];	
		
	$passenger_data = json_decode($passenger_data_json);		
	$passenger_data = json_decode(json_encode($passenger_data), True);	
	$cache_search = $this->CI->config->item('cache_flight_ssr_search');			
	//fetching ssr from cache	
	if ($cache_search) {		
	$page_data = $this->CI->cache->file->get($search_ssr_hash);	
	
		if(!empty($page_data)){
			
			
			$page_data['passenger_data'] = base64_encode($passenger_data_json);
			$page_data['Email'] =$passenger_data['Email'];			
			$page_data['ContactNo'] =$passenger_data['ContactNo'];
			$page_data['FirstName'] =$passenger_data['Passengers'][0]['FirstName'];
			$page_data['LastName']=$passenger_data['Passengers'][0]['LastName'];
			//debug($page_data); die;
			load_flight_lib ( 'PTBSID0000000002' );		
			echo $this->template->isolated_view ( 'flight/tbo/tbo_booking_page', $page_data);
			exit;
			//echo $this->template->view ( 'flight/tbo/tbo_booking_page', $page_data);
			
		}else{
		$response['status'] = false;
		$response['msg'] = 'Page data is empty, Failed in  additional SSr';
		$this->output_compressed_data($response);
		die;
		}
	}

	}
	//$this->output_compressed_data($response);
}

// Jun 7 2018, after clicking continue,(after selecting meal, seat, bag details)
function process_additional_ssr($search_id){
	//error_reporting(E_ALL);
	$post_params =$_POST;
	if (valid_array ( $post_params ) == false) {
			redirect ( base_url () );
		
	}
	//debug($post_params); die;
	$post_params['billing_city'] = 'Bangalore';
	$post_params['billing_country'] = '92';
	$post_params['billing_zipcode'] = '560100';
	$post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';
	
	$valid_temp_token = unserialized_data ( $post_params ['token'] , $post_params ['token_key']);	
	if ($valid_temp_token != false) {
	load_flight_lib ( $post_params ['booking_source'] );			
	$amount = 0;
	$currency = '';
	$post_params['token'] = unserialized_data($post_params['token']);
	$currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => get_application_currency_preference (),
					'to' => get_application_default_currency () 
			));
			
	$post_params['token']['token'] =$flight_data = $this->flight_lib->convert_token_to_application_currency($post_params['token']['token'], $currency_obj, $this->current_module, 'web');
	if(isset($post_params['token']['extra_services']) == true){
			 	$post_params['token']['extra_services'] = $extra_SSR = $this->flight_lib->convert_extra_services_to_application_currency($post_params['token']['extra_services'], $currency_obj);
			 }
	
	 $post_params['token'] = serialized_data($post_params['token']);	
	 //fetchicn passenger information from app(from session)
	 //debug($this->session->all_userdata()); die;
	 $session_data =$GLOBALS['CI']->session->all_userdata();
	 $flight_ss_post_data = @$session_data['flight_ss_post_data'];
	 $post_passenger = json_decode(base64_decode($flight_ss_post_data), true);
	
	 $post_passenger['search_id'] = $post_params['search_id'];
	 $post_passenger['token'] = $post_params['token'];
	 $post_passenger['token_key'] = $post_params['token_key'];
	 $post_passenger['op'] = $post_params['op'];
	 $post_passenger['booking_source'] = $post_params['booking_source'];
	 $post_passenger['promo_code_discount_val'] = $post_params['promo_code_discount_val'];
	 $post_passenger['promo_code'] = $post_params['promo_code'];
	 $post_passenger['total_amount_val'] = $post_params['total_amount_val'];
	 $post_passenger['passenger_contact'] = ((!empty($post_params['passenger_contact'])) ? $post_params['passenger_contact']: '9489026310');
	 $post_passenger['billing_email'] = ((!empty($post_params['billing_email'])) ? $post_params['billing_email']: 'info@flytripnow.com');
	 $post_passenger['first_name'] = ((!empty($post_params['first_name'])) ? $post_params['first_name']: 'User');
	 $post_passenger['last_name'] = ((!empty($post_params['last_name'])) ? $post_params['last_name']: '');
	 
	$passenger_data =json_decode(base64_decode($post_params['passenger_data']), true);
	
	foreach ($passenger_data['Passengers'] as $key => $value) { 
			$post_passenger['passenger_type'][$key] = $value['passenger_type'];
			$post_passenger['lead_passenger'][$key] = $value['lead_passenger'];
			$post_passenger['lead_passenger'][$key] = $value['Gender'];
			$post_passenger['passenger_nationality'][$key] = '92';
			$post_passenger['gender'][$key] = $value['Gender'];
			$post_passenger['name_title'][$key] = $value['Title'];
			$post_passenger['first_name'][$key] = $value['FirstName'];
			$post_passenger['last_name'][$key] = $value['LastName'];
			$post_passenger['passenger_passport_number'][$key] = $value['PassportNumber'];
			$post_passenger['date_of_birth'][$key] = $value['DateOfBirth'];
			$post_passenger['passenger_passport_issuing_country'][$key] = $value['PassportIssueCountry'];
			$pass_exp = explode('-', $value['PassportExpiry']);
			$post_passenger['passenger_passport_expiry_day'][$key] = $pass_exp['0'];
			$post_passenger['passenger_passport_expiry_month'][$key] = $pass_exp['1'];
			$post_passenger['passenger_passport_expiry_year'][$key] = $pass_exp['2'];
		}
	
		// $flight_count = count($flight_data);
	 /* for($i=0; $i<$flight_count; $i++){
	  }*/	
		$bag_count =count($extra_SSR['data']['ExtraServiceDetails']['Baggage']) -1;
		$meal_count =count($extra_SSR['data']['ExtraServiceDetails']['Meals']) -1;
		$seat_count =count($extra_SSR['data']['ExtraServiceDetails']['Seat']) -1;

		 for($i=0; $i<=$bag_count; $i++){	 		
		if(isset($post_params["baggage_$i"])){
		 $post_passenger["baggage_$i"] = $post_params["baggage_$i"];	 
			}
	 	}
	  for($i=0; $i<=$meal_count; $i++){	 
		if(isset($post_params["meal_$i"])){
		 $post_passenger["meal_$i"] = $post_params["meal_$i"];	 
		}
	  }
	  
	   for($i=0; $i<=$meal_count; $i++){	 
		if(isset($post_params["meal_pref$i"])){
		 $post_passenger["meal_pref$i"] = $post_params["meal_pref$i"];	 
		}
	   }	
	   
	    for($i=0; $i<=$seat_count; $i++){	 		
		if(isset($post_params["seat_$i"])){
		 $post_passenger["seat_$i"] = $post_params["seat_$i"];	 
			}
		}
	 //  debug($_POST); debug($post_passenger); debug($meal_count); debug($seat_count); die;
	 $post_passenger['payment_method'] = $post_params['payment_method'];
	 $post_passenger['billing_city'] = $post_params['billing_city'];	
	 $post_passenger['billing_zipcode'] = $post_params['billing_zipcode'];
	 $post_passenger['billing_address_1'] = $post_params['billing_address_1'];
	
	 #error_reporting(E_ALL);
		if(valid_array($post_passenger)){
			//$post_params['passenger_passport_expiry_month'] = $this->flight_lib->reindex_passport_expiry_month($post_passenger['passenger_passport_expiry_month'], $search_id);
			$temp_booking = $this->module_model->serialize_temp_booking_record ($post_passenger, FLIGHT_BOOKING );	
					
			$book_id = $temp_booking ['book_id'];
			$book_origin = $temp_booking ['temp_booking_origin'];
			if ($post_passenger ['booking_source'] == PROVAB_FLIGHT_BOOKING_SOURCE) {
				$currency_obj = new Currency ( array (
						'module_type' => 'flight',
						'from' => admin_base_currency (),
						'to' => admin_base_currency () 
				) );
				$temp_token = unserialized_data ( $post_passenger ['token'] );
				$flight_details = $temp_token['token'];
				$flight_booking_summary = $this->flight_lib->merge_flight_segment_fare_details($flight_details);
				$fare_details = $flight_booking_summary['FareDetails'][$this->current_module.'_PriceDetails'];
				$amount = $fare_details['TotalFare'];
				$currency = $fare_details['CurrencySymbol'];
				}

				/********* Promocode Start ********/
				$promocode_discount = $post_passenger['promo_code_discount_val'];
				/********* Promocode End ********/
				
				$email= $post_passenger ['billing_email'];
				$phone = $post_passenger ['passenger_contact'];				
				$firstname = $post_passenger ['first_name']. " " . $post_passenger ['last_name'];
				$book_id = $book_id;
				$productinfo = META_AIRLINE_COURSE;				
				
				/********* Convinence Fees Start ********/
				$convenience_fees = ceil($currency_obj->convenience_fees($amount, $search_id));
				/********* Convinence Fees End ********/
				$currency = $fare_details['CurrencySymbol'];
				//Save the Booking Data
				$booking_data = $this->module_model->unserialize_temp_booking_record ( $book_id, $book_origin );				
				$book_params = $booking_data['book_attributes'];
				#	debug('ds');die;				
				$data = $this->flight_lib->save_booking($book_id, $book_params, $currency_obj, $this->current_module);
				
				//Add Extra Service Price to Booking Amount					
				$extra_services_total_price = $this->flight_model->get_extra_services_total_price($book_id);	
				if($extra_services_total_price >0){
					$amount += $extra_services_total_price;
				}	
				
				#debug($amount); die;
				$domain_balance_status = $this->domain_management_model->verify_current_balance ( $amount, $currency );
				if ($domain_balance_status == true) {
				
				switch ($post_passenger ['payment_method']) {
					case PAY_NOW :
						$this->load->model('transaction');
					$pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
							$this->transaction->create_payment_record($book_id, $amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount,$pg_currency_conversion_rate);
								//$pre_booking_status['redirect'] = base_url().'index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;
								$pre_booking_status['data'] = array('app_reference'=>$book_id,'book_origin'=>$book_origin, 'extra_services_total_price' =>$extra_services_total_price);
								//$pre_booking_status['retun_url']   = base_url().'/mobile/index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;
								$url=explode('/mobile_webservices', base_url());
							// debug($url);exit;
							$pre_booking_status['retun_url']   =$url[0].'/index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;
								// $pre_booking_status['retun_url']   = B_URL.'/mobile/index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;
								$pre_booking_status['status']   = SUCCESS_STATUS;
								echo json_encode($pre_booking_status);
								exit();
	
						break;
					case PAY_AT_BANK :
						echo 'Under Construction - Remote IO Error';
						exit ();
						break;
					}
				}else {
					$data['status'] = FAILURE_STATUS;
					$data['message'] = "We can't process your request please contact our Customer Support";
					$this->output_compressed_data($data);
					exit;
				}
				
				
			}else{
				
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Passenger post data is empty";
			$this->output_compressed_data($data);
			exit;
			}
			
		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Token data is invalid";
			$this->output_compressed_data($data);
			exit;
		}	
	
}


	function get_fare_details()
	{
		$response['status'] = FAILURE_STATUS;
		$response['data'] = '';	
		$params = $this->input->post('farerules');
		//$params = '{"data_access_key":"czoyMToiMTQ4NTgzODc1NzEzMTAqXypPQjEwIjs=","booking_source":"PTBSID0000000002","search_access_key":"MTQ4NTgzODc1NzIwNTE0MzEyNTQxODc1ODE3ODUxMTY3MzI5MzI0MDIxMDk4NDIwMTJfX18wUjBfX181NGQ2OTYzMC1kYjg4LTQzZjUtYTMwMS04MTBjNzRhNTVjZjNfX19QVEJTSUQwMDAwMDAwMDAy","provab_auth_key":""}';

		if(empty($params))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Invalid Request";
			echo json_encode($data);
			exit;
		}

		$tokens = json_decode($params);
		$params = json_decode(json_encode($tokens), True);

		load_flight_lib($params['booking_source']);
		$data_access_key = $params['data_access_key'];
		$params['data_access_key'] = unserialized_data($params['data_access_key']);
		if (empty($params['data_access_key']) == false) {
			switch($params['booking_source']) {
				case PROVAB_FLIGHT_BOOKING_SOURCE :
					$params['data_access_key'] = $this->flight_lib->read_token($data_access_key);
					$data = $this->flight_lib->get_fare_details($params['data_access_key'], $params['search_access_key']);
					
					if ($data['status'] == SUCCESS_STATUS) {
						$response['status']	= SUCCESS_STATUS;
						$response['data']	=$data['data'];
						$response['msg']	= 'Fare Details Available';
					}
			}
		}
		$this->output_compressed_data($response);
	}

	private function output_compressed_data($data)
	{
		while (ob_get_level() > 0) { ob_end_clean() ; }
		ob_start("ob_gzhandler");
		header('Content-type:application/json');
		echo json_encode($data);
		ob_end_flush();
		exit;
	}

	function test_mobile_sms()
	{
		$data ['name']  = "Jeeva";
		$data['phone'] = "8867161115";
		// Sms config & Checkpoint
		if (active_sms_checkpoint ( 'booking' )) {
			$msg = "Dear " . $data ['name'] . " Thank you for Booking your ticket with us.Ticket Details will be sent to your email id";
			$this->provab_sms->send_msg ( $data ['phone'], $msg );
				// return $sms_status;
		} 
	}

	function sendmail($app_reference,$booking_source,$booking_status)
	{
		$this->load->library('booking_data_formatter');
		$this->load->library('provab_mailer');
		$this->load->model('flight_model');
		if (empty($app_reference) == false) {
			$booking_details = $this->flight_model->get_booking_details($app_reference, $booking_source, $booking_status);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				load_flight_lib(PROVAB_FLIGHT_BOOKING_SOURCE);
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_flight_booking_data($booking_details, 'b2b');	
				$page_data['data'] = $assembled_booking_details['data'];
				//debug($page_data);exit;
				$email = $booking_details['data']['booking_details']['0']['email'];
				$this->load->library('provab_pdf');
				$create_pdf = new Provab_Pdf();
				$mail_template = $this->template->isolated_view('voucher/flight_pdf', $page_data);
				$pdf = $create_pdf->create_pdf($mail_template,'');
				$this->provab_mailer->send_mail($email, domain_name().' E-Ticket',$mail_template ,$pdf);
			}
		}
	}

}
