<?php
if (! defined ( 'BASEPATH' ))
exit ( 'No direct script access allowed' );
class Tours extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$current_url = $_SERVER ['QUERY_STRING'] ? '?' . $_SERVER ['QUERY_STRING'] : '';
		$current_url = $this->config->site_url () . $this->uri->uri_string () . $current_url;
		$url = array (
				'continue' => $current_url 
		);
		$this->session->set_userdata ( $url );
		$this->helpMenuLink = "";
		$this->load->model ( 'Help_Model' );
		$this->helpMenuLink = $this->Help_Model->fetchHelpLinks ();
		$this->load->model ( 'Package_Model' );
	}

	/**
	 * get all tours
	 */
	public function index() {
		$data ['packages'] = $this->Package_Model->getAllPackages ();
		$data ['countries'] = $this->Package_Model->getPackageCountries ();
		$data ['package_types'] = $this->Package_Model->getPackageTypes ();
		if (! empty ( $data ['packages'] )) {
			$this->template->view ( 'holiday/tours', $data );
		} else {
			redirect ();
		}
	}
	/**
	 * get the package details
	 */
	public function details($package_id) {
		$data ['package'] = $this->Package_Model->getPackage ( $package_id );
		$data ['package_itinerary'] = $this->Package_Model->getPackageItinerary ( $package_id );
		$data ['package_price_policy'] = $this->Package_Model->getPackagePricePolicy ( $package_id );
		$data ['package_cancel_policy'] = $this->Package_Model->getPackageCancelPolicy ( $package_id );
		$data ['package_traveller_photos'] = $this->Package_Model->getTravellerPhotos ( $package_id );
		if (! empty ( $data ['package'] )) {
			$this->template->view ( 'holiday/tours_detail', $data );
		} else {
			redirect ( "tours/index" );
		}
	}
	public function enquiry() {
		$package_id = $this->input->post ( 'package_id' );
		if ($package_id !='') {
			$data = $this->input->post ();
			$package = $this->Package_Model->getPackage ( $package_id );
			$data ['package_name'] = $package->package_name;
			$data ['package_duration'] = $package->duration;
			$data ['package_type'] = $package->package_type;
			$data ['with_or_without'] = $package->price_includes;
			$data ['package_description'] = $package->package_description;
			$data ['ip_address'] = $this->session->userdata ( 'ip_address' );
			$data ['status'] = '0';
			$data ['date'] = date ( 'Y-m-d' );
			$data ['domain_list_fk'] = get_domain_auth_id ();
				
			$result = $this->Package_Model->saveEnquiry ( $data );
			$data ['sucess'] = "Thank you for submitting your enquiry for this package, will get back to soon";
			redirect ( 'tours/details/' . $package_id );
		} else {
			redirect ();
		}
	}

	function temp_index($search_id)
	{
		$this->load->model('hotel_model');
		$safe_search_data = $this->hotel_model->get_safe_search_data($search_id);
		// Get all the hotels bookings source which are active
		$active_booking_source = $this->hotel_model->active_booking_source();
		if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
			$safe_search_data['data']['search_id'] = abs($search_id);
			$this->template->view('tours/search_result_page', array('hotel_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
		} else {
			$this->template->view ( 'general/popup_redirect');
		}
	}

	public function search() {
		$data = $this->input->get ();
		if (! empty ( $data )) {
			$country = $data ['country'];
			$packagetype = $data ['package_type'];
			if ($data ['duration']) {
				$duration = explode ( '-', $data ['duration'] );
				if (count ( $duration ) > 1) {
					$duration = "duration between " . $duration ['0'] . " AND " . $duration ['1'];
				} else {
					$duration = "duration >" . $duration ['0'];
				}
			} else {
				$duration = $data ['duration'];
			}
			if ($data ['budget']) {
				$budget = explode ( '-', $data ['budget'] );

				if (count ( $budget ) > 1) {
					$budget = "price between " . $budget ['0'] . " AND " . $budget ['1'];
				} else if ($budget [0]) {
					$budget = "price >" . $budget ['0'];
				}
			} else {
				$budget = $data ['budget'];
			}
			$domail_list_pk = get_domain_auth_id ();
			$data ['scountry'] = $country;
			$data ['spackage_type'] = $packagetype;
			$data ['sduration'] = $data ['duration'];
			$data ['sbudget'] = $data ['budget'];
			$data ['packages'] = $this->Package_Model->search ( $country, $packagetype, $duration, $budget, $domail_list_pk, $domail_list_pk );
			$data ['caption'] = $this->Package_Model->getPageCaption ( 'tours_packages' )->row ();
			$data ['countries'] = $this->Package_Model->getPackageCountries ();
			$data ['package_types'] = $this->Package_Model->getPackageTypes ();
			$this->template->view ( 'holiday/tours', $data );
		} else {
			redirect ( 'tours/all_tours' );
		}
	}
	function package_user_rating() {
		$rate_data = explode ( ',', $_POST ['rate'] );
		$pkg_id = $rate_data [0];
		$rating = $rate_data [1];

		$arr_data = array (
				'package_id' => $pkg_id,
				'rating' => $rating 
		);
		$res = $this->Package_Model->add_user_rating ( $arr_data );
	}
	public function all_tours() {
		$data ['caption'] = $this->Package_Model->getPageCaption ( 'tours_packages' )->row ();
		$data ['packages'] = $this->Package_Model->getAllPackages ();
		$data ['countries'] = $this->Package_Model->getPackageCountries ();
		$data ['package_types'] = $this->Package_Model->getPackageTypes ();
		if (! empty ( $data ['packages'] )) {
			$this->template->view ( 'holiday/tours', $data );
		} else {
			redirect ();
		}
	}

	// function getPackageCountry()
	// {
	// 	$term = $this->input->post('term'); //retrieve the search term that autocomplete sends
	// 	if(empty($term))
	// 	{
	// 		$data['status'] = FAILURE_STATUS;
	// 		$data['message'] = "Please enter the details to search";
	// 		echo json_encode($data);
	// 		exit;
	// 	}
	// 	$package_country = $this->Package_Model->getPackageCountry($term);
	// 	if(valid_array($package_country))
	// 	{
	// 		echo json_encode($package_country);
	// 		exit;
	// 	}else{
	// 		$data['message'] = "No Result Found";
	// 		echo json_encode($data);
	// 		exit;
	// 	}
	// }
	function getPackageCountry1()
	{
		$term = $this->input->post('term'); //retrieve the search term that autocomplete sends
		if(empty($term))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to search";
			echo json_encode($data);
			exit;
		}
		//$package_country = $this->Package_Model->getPackageCountry($term);
		$package_country = $this->Package_Model->getPackageCity($term);
		if(valid_array($package_country))
		{
			//debug($package_country);
			$data['status'] = SUCCESS_STATUS;
			foreach ($package_country as $key => $value) {
			//$data[$key]['status'] = SUCCESS_STATUS;
				# code...
				$data['package']['package_city'][]=$value['package_city'];
			
			//debug($data);
			
			}
			echo json_encode($data);
		exit;

		}else{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "No Result Found";
			echo json_encode($data);
			exit;
		}
	}
	
		function getPackageCountry()
	{
		$term = $this->input->post('term'); //retrieve the search term that autocomplete sends
		if(empty($term))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to search";
			echo json_encode($data);
			exit;
		}
		$package_country = $this->Package_Model->getPackageCountry($term);

		

		if(valid_array($package_country))
		{
			echo json_encode($package_country);
			exit;
		}else{
			$data['message'] = "No Result Found";
			echo json_encode($data);
			exit;
		}
	}

       function getAllCountry()
    {
    	$query = "select * from country";   
    	$country = $this->db->query($query)->result_array();
    	echo json_encode($country);
			exit;
    }
    
	function getPackageTypes()
	{
		$con_id = $this->input->post('country_id'); //retrieve the search term that autocomplete sends
		if(empty($con_id))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to search";
			echo json_encode($data);
			exit;
		}
		$package_type = $this->Package_Model->getPackageType($con_id);
		if(valid_array($package_type))
		{
			echo json_encode($package_type);
			exit;
		}else{
			$data['message'] = "No Result Found";
			echo json_encode($data);
			exit;
		}

	}

	function getPackageDurations()
	{
		// $con_id = $this->input->post('package_type_id'); //retrieve the search term that autocomplete sends
		// if(empty($con_id))
		// {
		// 	$data['status'] = FAILURE_STATUS;
		// 	$data['message'] = "Please enter the details to search";
		// 	echo json_encode($data);
		// 	exit;
		// }
		// $package_type = $this->Package_Model->getPackageDurations($con_id);
		// if(valid_array($package_type))
		// {
		// 	echo json_encode($package_type);
		// 	exit;
		// }else{
		// 	$data['message'] = "No Result Found";
		// 	echo json_encode($data);
		// 	exit;
		// }

		$data['days']=array('1-3', '4-7', '8-12', '12');
		echo json_encode($data);
	}

	function search_mobile()
	{
		error_reporting(0);
		$term = $this->input->post('package_search'); //retrieve the search term that autocomplete sends
		if(empty($term))
		{
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Please enter the details to search";
			echo json_encode($res);
			exit;
		}
		$package_search = json_decode($term);
		$data = json_decode(json_encode($package_search), True);
			// debug($data); exit();
			$country = $data ['city'];
			$packagetype = $data ['package_type'];
			$holiday_type=$data ['holiday_type'];
			if ($data ['duration']) {
				$duration = explode ( '-', $data ['duration'] );
				if (count ( $duration ) > 1) {
					$duration = "duration between " . $duration ['0'] . " AND " . $duration ['1'];
				} else {
					$duration = "duration >" . $duration ['0'];
				}
			} else {
				$duration = $data ['duration'];
			}

			$sort = $data['sort'];
			$domain_list_pk = get_domain_auth_id ();
			$data ['scountry'] = $country;
			$data ['spackage_type'] = $packagetype;
			$data ['sduration'] = $data ['duration'];
			$expirydate = $data['expirydate'];
			$expirydate=date("Y-m-d", strtotime($expirydate));
			$packages = $this->Package_Model->mobile_search( $country, $packagetype, $duration,$sort,$domain_list_pk,$expirydate,$holiday_type);
			if(valid_array($packages)){
				$res['status'] = SUCCESS_STATUS;
				$res['message'] = "Data Avaliable";
				$res['data'] ='';
				foreach ($packages as $pac_k => $pac_v) {
					$response['packages'] ='';
					$response['packages']['package_id']    			= $pac_v['package_id'];
					$response['packages']['package_code']  			= $pac_v['package_code'];
					$response['packages']['supplier_id']    		= $pac_v['supplier_id'];
					$response['packages']['tour_types'] 			= $pac_v['tour_types'];
					$response['packages']['package_name'] 			= $pac_v['package_name'];
					$response['packages']['package_tour_code'] 		= $pac_v['package_tour_code'];
					$response['packages']['duration']	 			= $pac_v['duration'];
					$response['packages']['package_description'] 	= $this->clear_description($pac_v['package_description']);
					$response['packages']['image']	 				= $GLOBALS['CI']->template->domain_upload_pckg_images($pac_v['image']);
					$response['packages']['package_country'] 		= $pac_v['package_country'];
					$response['packages']['package_state'] 			= $pac_v['package_state'];
					$response['packages']['package_city'] 			= $pac_v['package_city'];
					$response['packages']['package_location'] 		= $pac_v['package_location'];
					$response['packages']['package_type'] 			= $this->package_type_name($pac_v['package_type']);
					$response['packages']['price_includes'] 		= $pac_v['price_includes'];
					$response['packages']['deals'] 			        = $pac_v['deals'];
					$response['packages']['no_que'] 				= $pac_v['no_que'];
					$response['packages']['home_page'] 				= $pac_v['home_page'];
					$response['packages']['rating'] 				= $pac_v['rating'];
					$response['packages']['status'] 				= $pac_v['status'];
					$response['packages']['price'] 					= (float)$pac_v['price'];
					$response['packages']['display'] 				= $pac_v['display'];
					$response['packages']['domain_list_fk'] 		= $pac_v['domain_list_fk'];
					$response['packages']['top_destination'] 		= $pac_v['top_destination'];
					$response['packages']['package_id']	 			= $pac_v['package_id'];
					$response['packages']['no_of_days']	 			= $pac_v['no_of_days'];
					$response['packages']['no_of_nights']	 			= $pac_v['no_of_nights'];
					$res['data'][] = $response['packages'];
				}
				echo json_encode($res); 
				exit;
			}else{
				$res['status'] = FAILURE_STATUS;
				$res['message'] = "No Result Found Please search with Another Criteria";
				$res['data'] = "";
				echo json_encode($res); 
				exit;
			}
	}

	private function package_type_name($name)
	{
		$package_type = $this->Package_Model->getPackageTypeName($name);
		if(!empty($package_type))
		{
			return $package_type;
			
		}else{
			return "";
		}
	}

	private function clear_description($desc)
	{
		 $desc = strip_tags($desc);
		 return str_replace('\n', ' ', $desc);
	}

	function DetailMobile()
	{
		$package_id = $this->input->post('package_id');
		if(empty($package_id))
		{
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Please Please Select The Package to Get Details";
			echo json_encode($res);
			exit;
		}
		
		$packages = $this->Package_Model->getPackage($package_id);
		$data['package'] = $this->format_package($packages);
		$package_itinerary = $this->Package_Model->getPackageItinerary ( $package_id );
		$data ['package_itinerary'] = $this->format_package_itinerary($package_itinerary);
		//debug($package_itinerary);exit();
		//debug($data ['package_itinerary'] );exit();
		$package_price_policy = $this->Package_Model->getPackagePricePolicy ( $package_id );
		$data ['package_price_policy'] = $this->format_package_price_policy($package_price_policy);
		$data ['package_cancel_policy'] = $this->Package_Model->getPackageCancelPolicy( $package_id );
		$package_traveller_photos = $this->Package_Model->getTravellerPhotos( $package_id );
		$data ['package_traveller_photos'] = $this->format_package_traveller_images($package_traveller_photos);
		if (! empty ( $data ['package'] )) {
				$res['status'] = SUCCESS_STATUS;
				$res['message'] = "Data Avaliable";
				echo json_encode($data); 
				exit;
		} else {
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "No Details Found For This Package !!";
			echo json_encode($res);
			exit;
		}
	}

	private function format_package($packages)
	{
		if(!empty($packages)){
			$data['package_id'] 		= $packages->package_id;
			$data['package_code'] 		= $packages->package_code;
			$data['supplier_id'] 		= $packages->supplier_id;
			$data['tour_types'] 		= $packages->tour_types;
			$data['package_name'] 		= $packages->package_name;
			$data['package_tour_code'] 	= $packages->package_tour_code;
			$data['duration'] 			= $packages->duration;
			$data['package_description']= $this->clear_description($packages->package_description);
			$data['image'] 				= $GLOBALS['CI']->template->domain_upload_pckg_images($packages->image);
			$data['package_country'] 	= $packages->package_country;
			$data['package_state'] 		= $packages->package_state;
			$data['package_city'] 		= $packages->package_city;
			$data['package_location'] 	= $packages->package_location;
			$data['package_type'] 		= $packages->package_type;
			$data['price_includes']	= $packages->price_includes;
			$data['deals']				= $packages->deals;
			$data['no_que']			= $packages->no_que;
			$data['rating']			= $packages->rating;
			$data['status']			= $packages->status;
			$data['price']				= $packages->price;
			$data['display']			= $packages->display;
			$data['domain_list_fk']	= $packages->domain_list_fk;
			$data['top_destination']	= $packages->top_destination;
		}else{
			$data = array();
		}
		return $data;
	}

	private function format_package_itinerary($itinerary)
	{
		if(!empty($itinerary)){
			foreach ($itinerary as $pac_k => $pac_v) {
				$response['packages'] ='';
				$response['packages']['iti_id']    				= $pac_v['iti_id'];
				$response['packages']['package_id']  			= $pac_v['package_id'];
				$response['packages']['day']    				= $pac_v['day'];
				$response['packages']['package_city'] 			= $pac_v['package_city'];
				$response['packages']['place'] 					= $pac_v['place'];
				$response['packages']['itinerary_description'] 	= $this->clear_description($pac_v['itinerary_description']);
				$response['packages']['itinerary_image']	 	= $pac_v['itinerary_image'];
				$res[] = $response['packages'];
			}
		}else{
			$res = array();
		}
		return $res;
	}

	private function format_package_price_policy($privacy)
	{
		if(!empty($privacy)){
			$data['price_id'] 			= $privacy->price_id;
			$data['package_id'] 		= $privacy->package_id;
			$data['price_includes'] 	=  $this->clear_description($privacy->price_includes);
			$data['price_excludes'] 	= $this->clear_description($privacy->price_excludes);
		}else{
			$data = array();
		}
		return $data;
	}

	private function format_package_traveller_images($images)
	{

		if(!empty($images)){
			foreach ($images as $pac_k => $pac_v) {
				$res['img_id'] 			= $pac_v->img_id;
				$res['package_id'] 		= $pac_v->package_id;
				$res['traveller_image'] = $GLOBALS['CI']->template->domain_upload_pckg_images($pac_v->traveller_image);
				$res['user_id'] 		= $pac_v->user_id;
				$res['user_type'] 		= $pac_v->user_type;
				$res['status'] 			= $pac_v->status;
				$res['photo_description'] = $this->clear_description($pac_v->photo_description);
				$data[] = $res;
			}
		}else{
			$data = array();
		}
		return $data;
	}

	function enquiry_mobile()
	{
		$enquiry = $this->input->post ('enquiry');
		$package_enquiry = json_decode($enquiry);
		$package_enquiry = json_decode(json_encode($package_enquiry), True);
		
		$package_id = $package_enquiry['package_id'];
		if ($package_id !='') {
			$package = $this->Package_Model->getPackage ( $package_id );
			$data ['package_name'] = $package->package_name;
			$data ['package_duration'] = $package->duration;
			$data ['package_type'] = $package->package_type;
			$data ['with_or_without'] = $package->price_includes;
			$data ['package_description'] = $package->package_description;
			$data ['ip_address'] = $this->session->userdata ( 'ip_address' );
			$data ['date'] = date ( 'Y-m-d' );
			$data ['domain_list_fk'] = get_domain_auth_id ();
			$result = $this->Package_Model->saveEnquiry($data);
			$res['status'] = SUCCESS_STATUS;
			$res['message'] = "Thank you for submitting your enquiry for this package, will get back to soon";
			echo json_encode($res); 
			exit;
		} else {
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Some went Wrong Please Try Agin";
			$res['data'] = '';
			echo json_encode($res); 
			exit;
		}
	}



	function pre_booking_itinary() 
	{
	error_reporting(E_ALL);	
		$post_params = $this->input->post ();
		$decode_data=json_decode($post_params['passenger_data'],true);
		$post_params=$decode_data;
		// debug($post_params);exit;
		if (valid_array ( $post_params ) == false) {
			// redirect ( base_url () );
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to process your request";
			echo json_encode($data);
			exit;
		}
		//Setting Static Data - Jaganath
		$post_params['billing_city'] = 'Bangalore';
		$post_params['billing_zipcode'] = '560100';
		$post_params['billing_address_1'] = '2nd Floor, Venkatadri IT Park, HP Avenue,, Konnappana Agrahara, Electronic city';
		// Make sure token and temp token matches
		
	       $currency_obj = new Currency ( array (
					'module_type' => 'flight',
					'from' => get_application_currency_preference (),
					'to' => get_application_default_currency () 
			));
			$promo=0;
			$post_params['token'] = serialized_data($post_params);
			$post_params['promo_code'] = $promo;
			// debug($post_params);exit;
			$temp_booking = $this->module_model->serialize_temp_booking_record ($post_params, PACKAGE_BOOKING );
			$book_id = $temp_booking ['book_id'];
				$book_origin = $temp_booking ['temp_booking_origin'];

			if ($post_params ['booking_source'] == PACKAGE_BOOKING_SOURCE) {
				$currency_obj = new Currency ( array (
						'module_type' => 'tours',
						'from' => admin_base_currency (),
						'to' => admin_base_currency () 
				) );
				
				$amount = $post_params['total'];
				$currency = admin_base_currency();
			}
			
			$email= $post_params ['email'];
			$phone = $post_params ['phone'];
			$verification_amount = ($amount);
			$firstname = $post_params ['first_name'] . " " . $post_params ['last_name'];
			$book_id = $book_id;
			$productinfo = META_PACKAGE_COURSE;

			// check current balance before proceeding further
			$domain_balance_status = $this->domain_management_model->verify_current_balance ( $verification_amount, $currency );
			
			if ($domain_balance_status == true) {
				
				$booking_data = $this->module_model->unserialize_temp_booking_record ( $book_id, $book_origin );

				

				$book_params = $booking_data['book_attributes'];
				$pack_id=$post_params['pack_id'];
				$pax_count=($post_params['no_adults']+ $post_params['no_child']);
				//debug($currency_obj);
				$data = $this->save_booking($book_id, $book_params,$pack_id,$pax_count,$currency_obj, 'b2c');

				$convenience_fees=0;
				$promocode_discount=0;

				//debug($data); exit;
				switch ($post_params ['payment_method']) {
					case PAY_NOW :
						$this->load->model('transaction');
						$pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
						$this->transaction->create_payment_record($book_id, $amount, $firstname, $email, $phone, $productinfo, $convenience_fees, $promocode_discount, $pg_currency_conversion_rate);

						$pre_booking_status['data'] = array('app_reference'=>$book_id,'book_origin'=>$book_origin);
							$url=explode('/mobile_webservices', base_url());
							$pre_booking_status['data']['return_url']   = $url[0].'/index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;
							$pre_booking_status['status']   = SUCCESS_STATUS;
							$this->output_compressed_data($pre_booking_status);
							exit();

						echo "payment_gateway_exit";exit;
						// redirect(base_url().'index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin);
						//redirect ( base_url () . 'index.php/flight/secure_booking/' . $book_id . '/' . $book_origin );

						break;
					case PAY_AT_BANK :
						echo 'Under Construction - Remote IO Error';
						exit ();
						break;
				} 
			} else {
				echo "insufficient_balance";exit;
				// redirect ( base_url () . 'index.php/flight/exception?op=Amount Flight Booking&notification=insufficient_balance' );
			}
		}


		function save_booking($app_booking_id, $book_params,$pack_id,$pax_coun,$currency_obj, $module='b2c')
	  {
		//debug($book_params);
		
			
		$book_total_fare = array();
		$book_domain_markup = array();
		$book_level_one_markup = array();
		$master_transaction_status = 'BOOKING_INPROGRESS';
		$master_search_id = $book_params['pack_id'];

		$domain_origin = get_domain_auth_id();
		$app_reference = $app_booking_id;
		$booking_source = $book_params['token']['booking_source'];

		//PASSENGER DATA UPDATE
		$total_pax_count =($book_params['no_adults'] + $book_params['no_child']);
		$pax_count = $total_pax_count;

        //PREFERRED TRANSACTION CURRENCY AND CURRENCY CONVERSION RATE 
		$transaction_currency = get_application_currency_preference();
		$application_currency = admin_base_currency();
		$currency_conversion_rate = $currency_obj->transaction_currency_conversion_rate();

	    $token = $book_params['token'];
		$master_booking_source = array();
		$currency = $currency_obj->to_currency;
		$deduction_cur_obj	= clone $currency_obj;
		//Storing Flight Details - Every Segment can repeate also
		$token_index=1;
		$commissionable_fare =  $token['total'];
             $pnr = '';
			$book_id = '';
			$source = '';
			$ref_id = '';
			$transaction_status = 0;
			$GetBookingResult = array();
			$transaction_description = '';
			$getbooking_StatusCode = '';
			$getbooking_Description = '';
			$getbooking_Category = '';
			$WSTicket = array();
			$WSFareRule = array();
			//Saving Flight Transaction Details
			$tranaction_attributes = array();
			$pnr = '';
			$book_id = $pack_id;
			$source = 'Provab';
			$ref_id = '';
			$transaction_status = $master_transaction_status;
			$transaction_description = '';
			//Get Booking Details
			$getbooking_status_details = '';
			$getbooking_StatusCode = '';
			$getbooking_Description = '';
			$getbooking_Category = '';
			$tranaction_attributes['Fare'] = $token['total'];
			$sequence_number = $token_index;
			//Transaction Log Details
			$ticket_trans_status_group[] = $transaction_status;
			$book_total_fare[]	= $token['total'];
			
			//Need individual transaction price details
			//SAVE Transaction Details
			$transaction_insert_id = $this->Package_Model->save_package_booking_transaction_details(
			$app_reference, $transaction_status, $transaction_description, $pnr, $book_id, $source, $ref_id,
			json_encode($tranaction_attributes),$currency, $commissionable_fare);
			$transaction_insert_id = $transaction_insert_id['insert_id'];

			//Saving Passenger Details
			$i = 0;
			for ($i=0; $i<$total_pax_count; $i++)
			{
				$passenger_type = 'Adult';
				$is_lead =1;
				$first_name = $book_params['first_name'];
				
				$last_name = $book_params['last_name'];
				
				$gender = get_enum_list('gender', $book_params['gender'][$i]);
				$passenger_nationality = $book_params['country'];
				$status = $master_transaction_status;
				$passenger_attributes = $book_params; 
				$flight_booking_transaction_details_fk = $transaction_insert_id;//Adding Transaction Details Origin 
				//SAVE Pax Details
				$pax_insert_id =  $this->Package_Model->save_package_booking_passenger_details
                (
				  $app_reference,$passenger_type,$is_lead,$first_name,$last_name,$gender,$passenger_nationality,$status,
				json_encode($passenger_attributes), 

				$flight_booking_transaction_details_fk);

				//Save passenger ticket information		
				

			}//Adding Pax Details Ends
		    $date_of_travel=date('Y-m-d', strtotime($book_params['token']['date_of_travel']));
			//debug($date_of_travel);exit;

			

		//Save Master Booking Details
		$book_total_fare = $token['total'];
		$phone = $book_params['phone'];
		$alternate_number = '';
		$email = $book_params['email'];
		$payment_mode = $book_params['payment_method'];
		$created_by_id = intval(@$GLOBALS['CI']->entity_user_id);

		$passenger_country =$book_params['country'];
		
		$passenger_city = $book_params['billing_city'];

		$attributes = array('country' => $passenger_country, 'city' => $passenger_city, 'zipcode' => $book_params['billing_zipcode'], 'address' =>  $book_params['billing_address_1']);
		$flight_booking_status = $master_transaction_status;
		//SAVE Booking Details
		$this->Package_Model->save_package_booking_details(
		$domain_origin, $flight_booking_status, $app_reference, $booking_source, $phone, $alternate_number, $email,
		$payment_mode, json_encode($attributes), $created_by_id,
		 $transaction_currency, $currency_conversion_rate,$pack_id,$date_of_travel);

		/************** Update Convinence Fees And Other Details Start ******************/
		
		$response['fare'] = $book_total_fare;
		$response['status'] = $flight_booking_status;
		$response['status_description'] = $transaction_description;
		$response['name'] = $first_name;
		$response['phone'] = $phone; 
		//debug($response); exit;
			
		return $response;
	}

	private function output_compressed_data($data)
	{
		while (ob_get_level() > 0) { ob_end_clean() ; }
		ob_start("ob_gzhandler");
		header('Content-type:application/json');
		echo json_encode($data);
		ob_end_flush();
		exit;
	}




}
