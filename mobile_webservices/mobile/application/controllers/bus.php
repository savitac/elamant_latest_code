<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package    Provab
 * @subpackage Bus
 * @author     Arjun J<arjunjgowda260389@gmail.com>
 * @version    V1
 */

class Bus extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//we need to activate bus api which are active for current domain and load those libraries
		$this->index();
		$this->load->model('bus_model');
		$this->load->model('user_model');// we need to load user model to access provab sms library
		$this->load->library('provab_sms');// we need this provab_sms library to send sms.
		$this->load->library('social_network/facebook');//Facebook Library to enable share button
	}

	/**
	 * index page of application will be loaded here
	 */
	function index()
	{

	}

	/**
	 *  Arjun J Gowda
	 * Load bus Search Result
	 * @param number $search_id unique number which identifies search criteria given by user at the time of searching
	 */
	function search($search_id)
	{
		$safe_search_data = $this->bus_model->get_safe_search_data($search_id);
		// Get all the busses bookings source which are active
		$active_booking_source = $this->bus_model->active_booking_source();
		if ($safe_search_data['status'] == true and valid_array($active_booking_source) == true) {
			$safe_search_data['data']['search_id'] = abs($search_id);
			$this->template->view('bus/search_result_page', array('bus_search_params' => $safe_search_data['data'], 'active_booking_source' => $active_booking_source));
		} else {
			$this->template->view ( 'general/popup_redirect');
		}
	}

	/**
     *  Princess May 25 2018
     * Passenger Details page for final bookings
     * Here we need to run booking based on api
     */
   function booking($search_id) {
		#debug($_REQUEST); die;
   	error_reporting(0);
		if(!empty($_POST['booking'])){
		$booking_post = json_decode($this->input->post ("booking"), true);
		$pre_booking_params =$booking_post;
		$safe_search_data = $this->bus_model->get_safe_search_data ( $search_id );
		//debug($pre_booking_params); die;
		$safe_search_data ['data'] ['search_id'] = abs ( $search_id );
		$page_data ['active_payment_options'] = $this->module_model->get_active_payment_module_list ();

		if (isset ( $pre_booking_params ['booking_source'] ) == true) {
			$currency_obj = new Currency ( array (
					'module_type' => 'bus',
					'from' => get_application_default_currency (),
					'to' => get_application_default_currency () 
			) );

			// We will load different page for different API providers... As we have dependency on API for bus details page
			$page_data ['search_data'] = $safe_search_data ['data'];
			load_bus_lib ( $pre_booking_params ['booking_source'] );
			
			$this->load->model ( 'user_model' );
			$page_data ['pax_details'] = array ();
			
			
			header ( "Cache-Control: no-store, no-cache, must-revalidate, max-age=0" );
			header ( "Cache-Control: post-check=0, pre-check=0", false );
			header ( "Pragma: no-cache" );
			#debug($safe_search_data); die;
			if ($pre_booking_params ['booking_source'] == PROVAB_BUS_BOOKING_SOURCE && isset ( $pre_booking_params ['route_schedule_id'] ) == true and isset ( $pre_booking_params ['pickup_id'] ) == true and count ( $pre_booking_params ['seat'] ) > 0 ) {

					//princess modified on feb 21, 2018				
				//$pre_booking_params ['token'] = unserialized_data ( $pre_booking_params ['token'], $pre_booking_params ['token_key'] );
				//$pre_booking_params ['token'] = unserialize ( base64_decode ( $pre_booking_params ['token'] ) );			
				$pre_booking_params ['token'] = unserialized_data ( $pre_booking_params ['token'], $pre_booking_params ['token_key'] );
				
				$bus_details = $pre_booking_params ['token']; 
				
				if ($pre_booking_params ['token'] != false && valid_array ( $bus_details )) {
					// Index seat numbers
					$indexed_seats = $GLOBALS ['CI']->bus_lib->index_seat_number ( force_multple_data_format ( $bus_details ['Layout'] ['SeatDetails'] ['clsSeat'] ) );
					// Filter only selected seats
					$selected_seats = array ();
					$total_fare = 0;
					$markup_total_fare = 0;
					$domain_total_fare = 0;
					$domain_currency_obj = clone $currency_obj;
					$page_data ['domain_currency_obj'] = $domain_currency_obj;
					#debug($pre_booking_params['seat']); die;
					foreach ( $pre_booking_params ['seat'] as $ssk => $ssv ) {
						$cur_seat_attr = $indexed_seats [$ssv];
						
						$total_fare += $cur_seat_attr ['Fare'];
						
						// total currency to customer
						$temp_currency = $currency_obj->get_currency ( $cur_seat_attr ['Fare'],true, false, false );
						// currency to be deducted from domain
						$domain_currency = $domain_currency_obj->get_currency ( $cur_seat_attr ['Fare']);
						$cur_seat_attr ['Markup_Fare'] = $temp_currency ['default_value'];
						$markup_total_fare += $cur_seat_attr ['Markup_Fare'];
						$domain_total_fare += $domain_currency ['default_value'];
						
						$selected_seats [$ssv] = $cur_seat_attr;
					}
					
					$page_data ['default_currency'] = $default_currency= $temp_currency ['default_currency'];
					$page_data ['default_currency_symbol'] = $domain_currency_obj->get_currency_symbol ( $page_data ['default_currency'] );
					$page_data ['total_fare'] = $total_fare;
					$page_data ['markup_total_fare'] = $markup_total_fare;
					$page_data ['domain_total_fare'] = $domain_total_fare;
					
					$bus_details ['Layout'] ['SeatDetails'] ['clsSeat'] = $selected_seats;
					$bus_details ['Pickup'] ['clsPickup'] = $GLOBALS ['CI']->bus_lib->index_pickup_number ( force_multple_data_format ( @$bus_details ['Pickup'] ['clsPickup'] ) );
					$bus_details ['CancellationCharges'] ['clsCancellationCharge'] = force_multple_data_format ( $bus_details ['CancellationCharges'] ['clsCancellationCharge'] );
					
					// ----------- page data
					$page_data ['details'] = $bus_details;
					//debug($pre_booking_params); die;
					$page_data ['pre_booking_params'] = $pre_booking_params;
					$page_data ['pre_booking_params'] ['default_currency'] = get_application_default_currency ();
					$page_data ['iso_country_list'] = $this->db_cache_api->get_iso_country_list ();
					$page_data ['country_list'] = $this->db_cache_api->get_country_list ();
					$page_data ['currency_obj'] = $currency_obj;
					// Summarize Price
					// $page_data['price_summary'] = '';
					
					//token //princess feb 28, 18
					$bus_pickup = $bus_details ['Pickup'] ['clsPickup'];
					$pickup = $bus_pickup [$pre_booking_params ['pickup_id']];
					$pickup_string = ($pickup ['PickupName'] . ' - ' . get_time ( $pickup ['PickupTime'] ));
					$pickup_string .= ', Address : ' . $pickup ['Address'] . ', Landmark : ' . $pickup ['Landmark'] . ', Phone : ' . $pickup ['Phone'];				//debug($bus_details); die;
						$CUR_Route = ($bus_details ['Route']);
							$dynamic_params_url ['RouteScheduleId'] = $pre_booking_params ['route_schedule_id'];							
							$dynamic_params_url ['JourneyDate'] = $pre_booking_params ['JourneyDate'];
							$dynamic_params_url ['PickUpID'] = $pre_booking_params ['pickup_id'];
							
							$seat_attr ['markup_price_summary'] = $markup_total_fare;
							$seat_attr ['total_price_summary'] = $total_fare;
							$seat_attr ['domain_deduction_fare'] = $domain_total_fare;
							$seat_attr ['default_currency'] = $default_currency;
							
							$bus_seats = $bus_details ['Layout'] ['SeatDetails'] ['clsSeat'];
							$seat_count = count ( $pre_booking_params ['seat'] );
							for($i = 0; $i < $seat_count; $i ++) {
								$i_seat = $bus_seats [$pre_booking_params ['seat'] [$i]];
								
								$attr ['Fare'] = $i_seat ['Fare'];
								$attr ['Markup_Fare'] = $i_seat ['Markup_Fare'];
								$i_seat_title = array();
									if ($i_seat['IsAvailable'] != '0') {
										$attr['IsAcSeat']	= false;
										$i_seat_title[] = 'NON_AC';
									} else {
										$attr['IsAcSeat']	= true;
										$i_seat_title[] = 'AC';
									}
									if (intval($i_seat['decks']) == 'Lower') {
										$attr['decks'] = 'Lower';
										$i_seat_title[] = $i_seat['seat_no'].' Lower Deck';
									} else {
										$i_seat_title[] = $i_seat['seat_no'].' Upper Deck';
										$attr['decks'] = 'Upper';
									}
									$i_seat_title[] = ($i_seat['status'] != '2' ? '' : 'Reserved For Ladies');
									if ($i_seat['seat_type'] == '2') {
										$i_seat_type = 'sleeper-A.png';
										$attr['SeatType']	= 'sleeper';
									} else {
										$attr['SeatType']	= 'seat';
										$i_seat_type = 'seat-A.png';
									}
								$attr['seq_no'] = $i_seat['seq_no'];
								$i_seat_title[] = ' @ '.$domain_currency_obj->get_currency_symbol($pre_booking_params['default_currency']).' '.$i_seat['Markup_Fare'];
								$seat_attr['seats'][$i_seat['seat_no']] = $attr;
							}
							
							
							$dynamic_params_url['RouteScheduleId'] = $pre_booking_params['route_schedule_id'];
							$dynamic_params_url['JourneyDate'] = $pre_booking_params['journey_date'];
							$dynamic_params_url['PickUpID'] = $pre_booking_params['pickup_id'];
							$dynamic_params_url['DropID'] = $pre_booking_params['drop_id'];
							$dynamic_params_url['seat_attr'] = $seat_attr;
							$dynamic_params_url['DepartureTime'] = $CUR_Route['DepartureTime'];
							$dynamic_params_url['ArrivalTime'] = $CUR_Route['ArrivalTime'];
							$dynamic_params_url['departure_from'] = $CUR_Route['From'];
							$dynamic_params_url['arrival_to'] = $CUR_Route['To'];
							$dynamic_params_url['Form_id'] = $CUR_Route['Form_id']; 
							$dynamic_params_url['To_id'] = $CUR_Route['To_id'];
							$dynamic_params_url['boarding_from'] = $pickup_string;//
							$dynamic_params_url['dropping_to'] = $drop_string;//
							$dynamic_params_url['bus_type'] = $CUR_Route['BusLabel'];
							$dynamic_params_url['operator'] = $CUR_Route['CompanyName'];
							$dynamic_params_url['CommPCT'] = $CUR_Route['CommPCT'];
							$dynamic_params_url['CommAmount'] = $CUR_Route['CommAmount'];
							
					$dynamic_params_url = serialized_data ( $dynamic_params_url );
					$page_data['token_afterbooking'] =$dynamic_params_url;
					$page_data['token_key_afterbooking'] =md5($dynamic_params_url);
					//princess on feb 28, 18 ends-->
				
					$page_data ['pax_title_enum'] = get_enum_list ( 'title' );
					$gender_enum = get_enum_list ( 'gender' );
					unset ( $gender_enum [3] );
					$page_data ['gender_enum'] = $gender_enum;
					
					$page_data ['traveller_details'] = $this->user_model->get_user_traveller_details ();
					
					$response = array(
						'status' => SUCCESS_STATUS,
						'data' => $page_data
					);
					
					//$this->template->view ( 'bus/travelyaari/travelyaari_booking_page', $page_data );
				}else{
					$response = array(
						'status' => FAILURE_STATUS,
						'data' => "Bus token details not found!"
					);
				}

			} else {

				$response = array(
						'status' => FAILURE_STATUS,
						'data' => "Datas are missing!"
					);
			}
		} else {
			$response = array(
						'status' => FAILURE_STATUS,
						'data' => $page_data
					);
		}

		}else{
			$response = array(
						'status' => FAILURE_STATUS,
						'data' => "Pass Paramters inside booking key"
					);
		}
		#debug($response); die;
		echo json_encode($response);
	}

	/**
     *  Arjun J Gowda
     * Secure Booking of bus
     * 2879 single adult static booking request 2500
     * 261 double room static booking request 2308
     */
    function pre_booking($search_id = 2500, $static_search_result_id = 2879) {
        $post_params = $this->input->post();
        // debug($post_params);exit;
        //$this->custom_db->generate_static_response(json_encode($post_params));
        //Insert To temp_booking and proceed
        /* $post_params = $this->bus_model->get_static_response($static_search_result_id); */
        //Make sure token and temp token matches
        $valid_temp_token = unserialized_data($post_params['token'], $post_params['token_key']);
        $user_type = !empty($post_params['user_type']) ? $post_params['user_type'] : 'b2c';
        if ($valid_temp_token != false) {
            load_bus_lib($post_params['booking_source']);
            /*             * **Convert Display currency to Application default currency** */
            //After converting to default currency, storing in temp_booking
            $post_params['token'] = unserialized_data($post_params['token']);
            $currency_obj = new Currency(array(
                'module_type' => 'bus',
                'from' => get_application_currency_preference(),
                'to' => admin_base_currency()
            ));
            $post_params['token'] = $this->bus_lib->convert_token_to_application_currency($post_params['token'], $currency_obj, $this->current_module);
            $post_params['token'] = serialized_data($post_params['token']);

            $temp_token = unserialized_data($post_params['token']);
            // debug($temp_token);exit;
            if ($post_params['booking_source'] == PROVAB_BUS_BOOKING_SOURCE) {
                $amount = $temp_token['seat_attr']['markup_price_summary'];
                $currency = $temp_token['seat_attr']['default_currency'];
            }
            // debug($temp_token);exit;
            //check current balance before proceeding further
            $domain_balance_status = $this->domain_management_model->verify_current_balance($amount, $currency);

            if ($domain_balance_status == true) {

                //Block Seats
                //run block and then booking request
                $post_params['token'] = $temp_token;
                //debug($post_params);exit;
                $block_status = $this->bus_lib->block_seats($search_id, $post_params);
                // debug($block_status);exit;
                if ($block_status['status'] == SUCCESS_STATUS) {
                    $post_params['block_key'] = $block_status['data']['result']['HoldKey'];
                    $post_params['block_data'] = $block_status['data']['result']['Passenger'];
                    //update seat block details and continue
                    $post_params['token'] = serialized_data($post_params['token']);
                    $temp_booking = $this->module_model->serialize_temp_booking_record($post_params, BUS_BOOKING);
                    $book_id = $temp_booking['book_id'];
                    $book_origin = $temp_booking['temp_booking_origin'];

                    //details for PGI
                    $email = $post_params ['billing_email'];
                    $phone = $post_params ['passenger_contact'];
                    $pgi_amount = $amount;
                    $firstname = $post_params ['contact_name'] ['0'];
                    $productinfo = META_BUS_COURSE;

                    switch ($post_params['payment_method']) {
                        case PAY_NOW :
                            $this->load->model('transaction');
                            $pg_currency_conversion_rate = $currency_obj->payment_gateway_currency_conversion_rate();
                            $this->transaction->create_payment_record($book_id, $pgi_amount, $firstname, $email, $phone, $productinfo, 0, 0, $pg_currency_conversion_rate);
                            redirect(B_URL . 'index.php/payment_gateway/payment/' . $book_id . '/' . $book_origin);
                            break;
                        case PAY_AT_BANK : echo 'Under Construction - Remote IO Error';
                            exit;
                            break;
                    }
                } else {
                    redirect(base_url() . 'index.php/bus/exception?op=seat_block&notification=' . $block_status['msg']);
                }
            } else {
                redirect(base_url() . 'index.php/bus/exception?op=booking_balance&notification=insufficient balance');
            }
        }

        redirect(base_url() . 'index.php/bus/exception?op=validation_hack&notification=Remote IO error @ Bus Booking');
    }


    /*
        process booking in backend until show loader 
    */
    function process_booking($book_id, $temp_book_origin, $user_type, $user_id){
        
        if($book_id != '' && $temp_book_origin != '' && intval($temp_book_origin) > 0){

            $form_url = base_url().'/index.php/bus/secure_booking';
            $book_id = $book_id;
            $temp_book_origin = $temp_book_origin;
            if ($user_type == 'b2b') {
				redirect (base_url () . 'mobile/index.php/bus/secure_booking/' . $book_id . '/' . $temp_book_origin . '/' . $user_type . '/' . $user_id);
			} else {
				redirect ($form_url . $book_id . '/' . $temp_book_origin);
			}
        }else{
            
        }
        
    }
	/**
     *  Arjun J Gowda
     * Do booking once payment is successfull - Payment Gateway
     * and issue voucher
     * BB19-133522-532376/45
     */
    function secure_booking($book_id,$temp_book_origin, $user_type, $user_id) {
        //$post_data = $this->input->post();
        $post_data = array();
        $post_data['book_id'] = $book_id;
        $post_data['temp_book_origin'] = $temp_book_origin;

        if (valid_array($post_data) == true && isset($post_data['book_id']) == true && isset($post_data['temp_book_origin']) == true &&
                empty($post_data['book_id']) == false && intval($post_data['temp_book_origin']) > 0) {
            //verify payment status and continue
            $book_id = trim($post_data['book_id']);
            $temp_book_origin = intval($post_data['temp_book_origin']);
            $this->load->model('transaction');
            $booking_status = $this->transaction->get_payment_status($book_id);
            //$booking_status['status'] = 'accepted';
            // debug($booking_status);exit;
            if ($booking_status['status'] !== 'accepted') {
                redirect(base_url() . 'index.php/bus/exception?op=Payment%Not%Done&notification=validation');
            }
        } else {
            redirect(base_url() . 'index.php/bus/exception?op=InvalidBooking&notification=invalid');
        }
        //run booking request and do booking
        $temp_booking = $this->module_model->unserialize_temp_booking_record($book_id, $temp_book_origin);
        // debug($temp_booking);exit;
        //Delete the temp_booking record, after accessing
        // $this->module_model->delete_temp_booking_record ($book_id, $temp_book_origin);
        load_bus_lib($temp_booking['booking_source']);
        $amount = $temp_booking['book_attributes']['token']['seat_attr']['markup_price_summary'];
        $currency = $temp_booking['book_attributes']['token']['seat_attr']['default_currency'];
        //check current balance before proceeding further
        $domain_balance_status = $this->domain_management_model->verify_current_balance($amount, $currency);
        if ($domain_balance_status) {
            //lock table
            if ($temp_booking != false) {
                switch ($temp_booking['booking_source']) {
                    case PROVAB_BUS_BOOKING_SOURCE :
                        $booking = $this->bus_lib->process_booking($book_id, $temp_booking['book_attributes']);
                        break;
                }

                // debug($booking);exit;
                if ($booking['status'] == SUCCESS_STATUS) {
                    // debug($booking);
                    $get_bus_details = $this->bus_lib->get_booking_details($booking, $temp_booking['booking_source']);

                    $bookings['data']['result'] = $get_bus_details['data']['result']['GetBookingDetails'];
                    $bookings['data']['result']['ticket_details'] = $booking['data']['result']['ticket_details'];
                    $bookings['data']['temp_booking_cache'] = $temp_booking;
                    // debug($bookings);exit;
                    $currency_obj = new Currency(array('module_type' => 'bus', 'from' => admin_base_currency(), 'to' => admin_base_currency()));
                    $bookings['data']['currency_obj'] = $currency_obj;
                    //Save booking based on booking status and book id
                    $data = $this->bus_lib->save_booking($book_id, $bookings['data'], $user_type, $user_id);

                    if ($user_type == 'b2c') {
                    	$this->domain_management_model->update_transaction_details('bus', $book_id, $data['fare'], $data['domain_markup'], $data['level_one_markup'], @$data['convinence'], @$data['discount'], $data['transaction_currency'], $data['currency_conversion_rate']);
                    } else {
                    	$this->domain_management_model->update_transaction_details_b2b('bus', $book_id, $data['fare'], $data['domain_markup'], $data['level_one_markup'], @$data['convinence'], @$data['discount'], $data['transaction_currency'], $data['currency_conversion_rate'], $user_id);
                    }
                    //deduct balance and continue
                    //sms config ends here,

                    redirect(base_url() . 'index.php/voucher/bus/' . $book_id . '/' . $temp_booking['booking_source'] . '/BOOKING_CONFIRMED/show_voucher');
                } else {
                    redirect(base_url() . 'index.php/bus/exception?op=booking_exception&notification=' . $booking['msg']);
                }
            }
            //release table lock
        } else {
            redirect(base_url() . 'index.php/bus/exception?op=Remote%IO%error @ Insufficient&notification=validation');
        }
        redirect(base_url() . 'index.php/bus/exception?op=Remote%IO%error%@%bus%Secure Booking&notification=validation');
    }

	/**
	 *  Arjun J Gowda
	 *Process booking on hold - pay at bank
	 */
	function booking_on_hold($book_id)
	{

	}
	/**
	 * Jaganath
	 */
	function pre_cancellation($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$page_data = array();
			$booking_details = $this->bus_model->get_booking_details($app_reference, $booking_source);
			if ($booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				//Assemble Booking Data
				$assembled_booking_details = $this->booking_data_formatter->format_bus_booking_data($booking_details, 'b2c');
				$page_data['data'] = $assembled_booking_details['data'];
				$this->template->view('bus/pre_cancellation', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}
	}
	/*
	 * Jaganath
	 * Process the Booking Cancellation
	 * Full Booking Cancellation
	 *
	 */
	function cancel_booking($app_reference, $booking_source)
	{
		//echo 'Under Construction';exit;
		$app_reference = $this->input->post('book_id');
		$booking_source = $this->input->post('booking_source');
		if(empty($app_reference)&&empty($booking_source)){
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please Provide correct details";
			echo json_encode($data);
			exit;
		}
		if(empty($app_reference) == false) {
			$master_booking_details = $this->bus_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_bus_booking_data($master_booking_details, 'b2c');
				$master_booking_details = $master_booking_details['data']['booking_details'][0];
				// debug($master_booking_details);exit;
				$PNRNo = trim($master_booking_details['pnr']);
				$TicketNo = trim($master_booking_details['ticket']);
				$booking_details = array();
				$booking_details['PNRNo'] = $PNRNo;
				$booking_details['TicketNo'] = $TicketNo;
				$booking_details['app_reference'] = $app_reference;
				foreach ($master_booking_details['booking_customer_details'] as $bckey => $bcvalue) {
					
					$booking_details['SeatNos'][] = $bcvalue['seat_no'];
				}
				$booking_details['booking_source'] = $booking_source;
				load_bus_lib($booking_source);
				$cancellation_details = $this->bus_lib->cancel_full_booking($booking_details,$app_reference);//Invoke Cancellation Methods
				if($cancellation_details['status'] == true) {//IF Cancellation is Success
					/*$customer_ids = array();
					foreach($booking_customer_details as $cust_k => $cust_v) {
						$customer_ids[] = $cust_v['origin'];
					}*/
					$cancellation_details = $this->bus_lib->save_cancellation_data($app_reference, $cancellation_details);//Save Cancellation Data
				}
				if($cancellation_details['status'] == false) {					
					$data['message'] = $cancellation_details['msg'];
					$data['status']= FAILURE_STATUS;
				} else {
					$data['status']= SUCCESS_STATUS;
					$data['message'] = 'Your Cancellation Request has been sent';
				}
				echo json_encode($data);
				exit;
				// redirect('bus/cancellation_details/'.$app_reference.'/'.$booking_source);
			} else {
				$data['status'] = FAILURE_STATUS;
				$data['message'] = "Please Provide correct details";
				echo json_encode($data);
				exit;
				// redirect('security/log_event?event=Invalid Details');
			}
		} else {
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please Provide correct details";
			echo json_encode($data);
			exit;
			// redirect('security/log_event?event=Invalid Details');
		}
	}
	/**
	 * Jaganath
	 * Cancellation Details
	 * @param $app_reference
	 * @param $booking_source
	 */
	function cancellation_details($app_reference, $booking_source)
	{
		if (empty($app_reference) == false && empty($booking_source) == false) {
			$master_booking_details = $GLOBALS['CI']->bus_model->get_booking_details($app_reference, $booking_source);
			if ($master_booking_details['status'] == SUCCESS_STATUS) {
				$page_data = array();
				$this->load->library('booking_data_formatter');
				$master_booking_details = $this->booking_data_formatter->format_bus_booking_data($master_booking_details, 'b2c');
				$page_data['data'] = $master_booking_details['data'];
				$this->template->view('bus/cancellation_details', $page_data);
			} else {
				redirect('security/log_event?event=Invalid Details');
			}
		} else {
			redirect('security/log_event?event=Invalid Details');
		}

	}

	/**
	 * Arjun J Gowda
	 */
	function exception()
	{
		$module = META_BUS_COURSE;
		$op = @$_GET['op'];
		$notification = @$_GET['notification'];
		$eid = $this->module_model->log_exception($module, $op, $notification);
		//set ip log session before redirection
		$this->session->set_flashdata(array('log_ip_info' => true));
		redirect(base_url().'index.php/bus/event_logger/'.$eid);
	}

	function event_logger($eid='')
	{
		$log_ip_info = $this->session->flashdata('log_ip_info');
		$this->template->view('bus/exception', array('log_ip_info' => $log_ip_info, 'eid' => $eid));
	}

 	function bus_details_mobile() {

        // $get_data = '{"route_schedule_id": "614255067","journey_date": "2017-05-31","route_code": "41340~45792~298~299~2017-05-31","search_id": "555","ResultToken":"95fd37e70e033b29a70c2c80e92c6ff3","booking_source": "PTBSID0000000003"}';
        $get_data = $this->input->post('bus_details');
        if(empty($get_data))
        {
            $data['status'] = FAILURE_STATUS;
            $data['message'] = "Please enter the details to search";
            echo json_encode($data);
            exit;
        }


        //$search_data = $this->search_data($search_id);
        //Cache
     	/*   $cache_search = $this->CI->config->item('cache_bus_search');
        $search_hash = $this->search_hash;
        $header_info = $this->get_header();
  
        if ($cache_search) {
            $cache_contents = $this->CI->cache->file->get($search_hash);
        }
		*/
        $params = json_decode($get_data,true);
        if (empty($params['booking_source']) == false and empty($params['search_id']) == false and intval($params['search_id']) > 0) {
            load_bus_lib($params['booking_source']);
            switch ($params['booking_source']) {
                case PROVAB_BUS_BOOKING_SOURCE :
     				// debug($params); exit();
                    $details = $this->bus_lib->get_bus_details($params['route_schedule_id'], $params['journey_date'], $params['route_code'], $params['ResultToken'], $params['booking_source'], $params['search_id']);
            		// debug($details);exit;
                    if ($details['status'] == SUCCESS_STATUS) {
                        //Converting API currency data to preferred currency
                        $currency_obj = new Currency(array(
                            'module_type' => 'bus',
                            'from' => get_api_data_currency(),
                            'to' => get_application_currency_preference()
                        ));
                        $details = $this->bus_lib->seatdetails_in_preferred_currency($details, $details['data']['bus_booking_data'], $currency_obj);

                        //Display Bus Details
                        $currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_currency_preference(), 'to' => get_application_currency_preference()));
                        $response['stauts'] = SUCCESS_STATUS;
                        $page_data['search_id'] = $params['search_id'];
                        $page_data['ResultToken'] = $params['ResultToken'];
                        $page_data['details'] = $details['data']['result'];
                        $page_data['currency_obj'] = $currency_obj;
						//may 25 2018
                       	$page_data['token']=serialized_data($details);
						$page_data['token_key']=md5(serialized_data($details));
						
						
                        $response['data'] = $page_data;                  
                        $response['status'] = SUCCESS_STATUS;
                    } else {
                        $response['status'] =FAILURE_STATUS;
                        $response['message'] = "Unable To Fetch The Data";
                    }
                    break;
            }
        }
        $this->output_compressed_data($response);
    }

	function select_bus_seats_mobile()
	{
		//$get_data = '{"route_schedule_id":"614255067","route_code":"41340~45792~298~299~2017-05-31","journey_date":"2017-05-31T05:25:00","search_id":"555","booking_source":"PTBSID0000000003"}';
		$get_data = $this->input->post('bus_details');
		if(empty($get_data))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to search";
			echo json_encode($data);
			exit;
		}
		//$hotel_data = $hotel_data['hotel_data'];
		$bus_seats_para = json_decode($get_data);
		$params = json_decode(json_encode($bus_seats_para), True);
		
		if (empty($params['booking_source']) == false and empty($params['search_id']) == false and intval($params['search_id']) > 0) {
			load_bus_lib($params['booking_source']);
			switch ($params['booking_source']) {
				case PROVAB_BUS_BOOKING_SOURCE :
					$currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_default_currency(), 'to' => get_application_currency_preference()));
					$details = $this->bus_lib->get_bus_details($params['route_schedule_id'], $params['journey_date']);
					debug($details); die;
					if ($details['status'] == SUCCESS_STATUS) {
						$currency_obj = new Currency(array(
								'module_type' => 'bus',
								'from' => get_api_data_currency(), 
								'to' => get_application_currency_preference()
							));
						$details = $this->bus_lib->seatdetails_in_preferred_currency($details, $currency_obj);
						$final_fare = array();
						foreach ($details['data']['result']['Layout']['SeatDetails']['clsSeat'] as $bu_key => $bu_value) {
							$seat_fare = $bu_value;
							$search_price = $currency_obj->get_currency($bu_value['Fare']);
							$seat_fare['Fare'] = $search_price['default_value'];
							$final_fare[] = $seat_fare;
						}
						$details['data']['result']['Layout']['SeatDetails']['clsSeat'] =$final_fare; 
						$page_data['search_id'] = $params['search_id'];
						$page_data['details'] = $details['data']['result'];
						$response['status'] = SUCCESS_STATUS;
						$response['message'] = "Data Avalilable";
						$response['data'] = $details['data']['result'];
						$response['data']['search_id'] = $params['search_id'];
						$response['data']['token'] = $seat_no_summary = serialized_data($details['data']['result']);
						$response['data']['token_key'] = md5($seat_no_summary);
						$this->output_compressed_data($response);
					}else{
						$response['status'] =FAILURE_STATUS;
						$response['message'] = "Unable To Fetch The Data";
						$this->output_compressed_data($response);
					}
					break;
			}
		}else{
			$response['status'] = FAILURE_STATUS;
			$response['message'] = "Request fields are missing";
			$this->output_compressed_data($response);
		}
		$this->output_compressed_data($response);
	}

	function bus_booking_mobile()
	{
		$get_data = $this->input->post('confirm_seat');
		
		if(empty($get_data))
		{
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Please enter the details to search";
			$this->output_compressed_data($res);
			exit;
		}
		$bus_seats_para = json_decode($get_data);
		$pre_booking_params = json_decode(json_encode($bus_seats_para), True);
		
		if(valid_array($pre_booking_params) == false){
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Please enter the details to search";
			$this->output_compressed_data($res);
			exit;
		}else{
			$search_id = $pre_booking_params['search_id'];
		//$pre_booking_params = $this->input->post();
		$safe_search_data = $this->bus_model->get_safe_search_data($search_id);
		$safe_search_data['data']['search_id'] = abs($search_id);
		
		if (isset($pre_booking_params['booking_source']) == true) {
			$currency_obj = new Currency(array('module_type' => 'bus', 'from' => get_application_default_currency(), 'to' => get_application_default_currency()));
			//We will load different page for different API providers... As we have dependency on API for bus details page
			
			load_bus_lib($pre_booking_params['booking_source']);
			//Need to fill pax details by default if user has already logged in
			$this->load->model('user_model');
			

			//Not to show cache data in browser
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");

			if ($pre_booking_params['booking_source'] == PROVAB_BUS_BOOKING_SOURCE && isset($pre_booking_params['route_schedule_id']) == true and isset($pre_booking_params['pickup_id']) == true and
			count($pre_booking_params['seat']) > 0 and $safe_search_data['status'] == true)
			{
				$pre_booking_params['token'] = unserialized_data($pre_booking_params['token'], $pre_booking_params['token_key']);
				$bus_details = $pre_booking_params['token'];
				//$bus_details = $this->bus_lib->get_bus_details($pre_booking_params['route_schedule_id'], $pre_booking_params['journey_date']);
				if ($pre_booking_params['token'] != false && valid_array($bus_details)) {
					//Index seat numbers
					$indexed_seats = $GLOBALS ['CI']->bus_lib->index_seat_number ( force_multple_data_format ( $bus_details ['Layout'] ['SeatDetails'] ['clsSeat'] ) );
					//Filter only selected seats
					$selected_seats = array();
					$total_fare = 0;
					$markup_total_fare = 0;
					$domain_total_fare = 0;
					$domain_currency_obj = clone $currency_obj;
					
					foreach ($pre_booking_params['seat'] as $ssk => $ssv) {
						$cur_seat_attr = $indexed_seats[$ssv];

						$total_fare += $cur_seat_attr ['Fare'];

						// total currency to customer
						$temp_currency = $currency_obj->get_currency ( $cur_seat_attr ['Fare']);
						// currency to be deducted from domain
						$domain_currency = $domain_currency_obj->get_currency ( $cur_seat_attr ['Fare'], true, true, false);
						$cur_seat_attr ['Markup_Fare'] = $temp_currency ['default_value'];
						$markup_total_fare += $cur_seat_attr ['Markup_Fare'];
						$domain_total_fare += $domain_currency ['default_value'];

						$selected_seats[$ssv] = $cur_seat_attr;
					}
					$bus_details ['Layout'] ['SeatDetails'] ['clsSeat'] = $selected_seats;
					$bus_details ['Pickup'] ['clsPickup'] = $GLOBALS ['CI']->bus_lib->index_pickup_number ( force_multple_data_format ( @$bus_details ['Pickup'] ['clsPickup'] ) );
					$bus_details ['CancellationCharges'] ['clsCancellationCharge'] = force_multple_data_format ( $bus_details ['CancellationCharges'] ['clsCancellationCharge'] );
					
					$details = $bus_details;

					$CUR_Route = ($details ['Route']);
					$bus_seats = $details ['Layout'] ['SeatDetails'] ['clsSeat'];
					$bus_pickup = $details ['Pickup'] ['clsPickup'];

					$pickup = $bus_pickup[$pre_booking_params['pickup_id']]; 
					$pickup_string = ($pickup['PickupName'].' - '.get_time($pickup['PickupTime']));
					$pickup_string .= ', Address : '.$pickup['Address'].', Landmark : '.$pickup['Landmark'].', Phone : '.$pickup['Phone'];

					$seat_count = count ( $pre_booking_params ['seat'] );
					$seat_attr ['markup_price_summary'] = $markup_total_fare;
					$seat_attr ['total_price_summary'] = $total_fare;
					$seat_attr ['domain_deduction_fare'] = $domain_total_fare;
					$seat_attr ['default_currency'] = admin_base_currency();

					for($i=0; $i<$seat_count; $i++) {
		
						$i_seat = $bus_seats[$pre_booking_params['seat'][$i]];
						$attr['Fare']		= $i_seat['Fare'];
						$attr['API_Raw_Fare']		= $i_seat['API_Raw_Fare'];
						$attr['Markup_Fare']= $i_seat['Markup_Fare'];
						
						$i_seat_title = array();
						if ($i_seat['IsAC'] != 'false') {
							$attr['IsAcSeat']	= false;
							$i_seat_title[] = 'NON_AC';
						} else {
							$attr['IsAcSeat']	= true;
							$i_seat_title[] = 'AC';
						}
						if (intval($i_seat['Deck']) == 1) {
							$i_seat_title[] = $i_seat['SeatNo'].' Lower Deck';
						} else {
							$i_seat_title[] = $i_seat['SeatNo'].' Upper Deck';
						}
						$i_seat_title[] = ($i_seat['Gender'] != 'F' ? '' : 'Reserved For Ladies');
						if ($i_seat['IsSleeper'] == 'true') {
							$i_seat_type = 'sleeper-A.png';
							$attr['SeatType']	= 'sleeper';
						} else {
							$attr['SeatType']	= 'seat';
							$i_seat_type = 'seat-A.png';
						}
						$i_seat_title[] = ' @ '.$domain_currency_obj->get_currency_symbol($temp_currency['default_currency']).' '.$i_seat['Markup_Fare'];
						$seat_attr['seats'][$i_seat['SeatNo']] = $attr;
					}
					$dynamic_params_url['RouteScheduleId'] = $pre_booking_params['route_schedule_id'];
					$dynamic_params_url['JourneyDate'] = $pre_booking_params['journey_date'];
					$dynamic_params_url['PickUpID'] = $pre_booking_params['pickup_id'];
					$dynamic_params_url['seat_attr'] = $seat_attr;
					$dynamic_params_url['DepartureTime'] = $CUR_Route['DepartureTime'];
					$dynamic_params_url['ArrivalTime'] = $CUR_Route['ArrivalTime'];
					$dynamic_params_url['departure_from'] = $CUR_Route['FromCityName'];
					$dynamic_params_url['arrival_to'] = $CUR_Route['ToCityName'];
					$dynamic_params_url['boarding_from'] = $pickup_string;//
					$dynamic_params_url['dropping_to'] = '';//
					$dynamic_params_url['bus_type'] = $CUR_Route['BusLabel'];
					$dynamic_params_url['operator'] = $CUR_Route['CompanyName'];
					$dynamic_params_url['CommPCT'] = $CUR_Route['CommPCT'];
					$dynamic_params_url['CommAmount'] = $CUR_Route['CommAmount'];
					$dynamic_params_url = serialized_data($dynamic_params_url);

					$page_data['default_currency'] = $temp_currency['default_currency'];
					$page_data['default_currency_symbol'] = $domain_currency_obj->get_currency_symbol($page_data['default_currency']);
					$page_data['total_fare'] = $total_fare;
					$page_data['markup_total_fare'] = $markup_total_fare;
					$page_data['domain_total_fare'] = $domain_total_fare;
					$page_data['token'] = $dynamic_params_url;
					$page_data['token_key'] = md5($dynamic_params_url);
					

					
					$res['status'] = SUCCESS_STATUS;
					$res['message'] = "Data Avalilable";
					$res['data'] = $page_data;
					$this->output_compressed_data($res);

					
				}
			} else {
				$res['status'] = FAILURE_STATUS;
				$res['message'] = "Please Select The Seat Number";
				$res['data'] = array();
				$this->output_compressed_data($res);
			}
		} else {
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Booking Source Is Not Avalilable so please pass the booking source";
			$res['data'] = array();
			$this->output_compressed_data($res);
		}
	}
	}

	function pre_booking_mobile()
	{
		//$get_data  = '{"search_id":"555","gender":["1"],"pax_title":["1"],"contact_name":["test"],"age":["21"],"token":"YToxNDp7czoxNToiUm91dGVTY2hlZHVsZUlkIjtzOjk6IjYxNzkxNzg1MCI7czoxMToiSm91cm5leURhdGUiO3M6MTk6IjIwMTctMDUtMzFUMDA6MDA6MDAiO3M6ODoiUGlja1VwSUQiO3M6ODoiMjQ5ODk2NDkiO3M6OToic2VhdF9hdHRyIjthOjU6e3M6MjA6Im1hcmt1cF9wcmljZV9zdW1tYXJ5IjtkOjUzODtzOjE5OiJ0b3RhbF9wcmljZV9zdW1tYXJ5IjtpOjUzODtzOjIxOiJkb21haW5fZGVkdWN0aW9uX2ZhcmUiO2Q6NTM4O3M6MTY6ImRlZmF1bHRfY3VycmVuY3kiO3M6MzoiSU5SIjtzOjU6InNlYXRzIjthOjE6e2k6MTg7YTo0OntzOjQ6IkZhcmUiO3M6MzoiNTM4IjtzOjExOiJNYXJrdXBfRmFyZSI7czo2OiI1MzguMDAiO3M6ODoiSXNBY1NlYXQiO2I6MDtzOjg6IlNlYXRUeXBlIjtzOjQ6InNlYXQiO319fXM6MTM6IkRlcGFydHVyZVRpbWUiO3M6MTk6IjIwMTctMDUtMzFUMDc6MzA6MDAiO3M6MTE6IkFycml2YWxUaW1lIjtzOjE5OiIyMDE3LTA1LTMxVDE0OjAwOjAwIjtzOjE0OiJkZXBhcnR1cmVfZnJvbSI7czo5OiJiYW5nYWxvcmUiO3M6MTA6ImFycml2YWxfdG8iO3M6NzoiY2hlbm5haSI7czoxMzoiYm9hcmRpbmdfZnJvbSI7czoxNjc6IkthbGFzaXBhbHlhbSAtIDA4OjAwIEFNLCBBZGRyZXNzIDogIyAzMjEgVFNQIFJvYWQgT3BwIEJNQyBLYWxhc2lwYWx5YW0sIENhbGwgOiA5OTcyNTY4MDUyLzA4MCAyNjgwMTYxNi8xNywgTGFuZG1hcmsgOiBPcHAgQmFuZ2Fsb3JlIE1lZGljYWwgQ29sbGVnZSwgUGhvbmUgOiA5OTcyNTAwMzAwIjtzOjExOiJkcm9wcGluZ190byI7czowOiIiO3M6ODoiYnVzX3R5cGUiO3M6NDY6IjIrMiwgU2NhbmlhIE11bHRpLUF4bGUgU2VtaSBTbGVlcGVyLCBBQywgVmlkZW8iO3M6ODoib3BlcmF0b3IiO3M6MTE6IlNSUyBUcmF2ZWxzIjtzOjc6IkNvbW1QQ1QiO3M6MTM6IjcuNDM0OTQ0MjM3OTIiO3M6MTA6IkNvbW1BbW91bnQiO3M6MjoiNDAiO30=","token_key":"2d1fa278bb5438bcb79f452f84bc38c1","op":"book_bus","booking_source":"PTBSID0000000003","passenger_contact":"1111111111","alternate_contact":"","billing_email":"test@test.com","tc":"on","payment_method":"PNHB1"}';

      	//  $get_data = '{"ResultToken":"90e3ab4131d01ca0978d190480bac4e2","age":["21"],"alternate_contact":"","billing_email":"s@smail.com","booking_source":"PTBSID0000000003","contact_name":["cvv"],"gender":["Male"],"op":"book_bus","passenger_contact":"9844386672","pax_title":["Mr"],"payment_method":"PNHB1","search_id":"1679","tc":"on","token":"{\"RouteScheduleId\":\"110\",\"JourneyDate\":\"2018-03-30\",\"PickUpID\":\"776\",\"DropID\":\"136529\",\"DepartureTime\":\"2018-03-30 18:00:00\",\"ArrivalTime\":\"2018-03-31 06:00:00\",\"arrival_to\":\"Chennai\",\"Form_id\":\"4292\",\"To_id\":\"4562\",\"boarding_from\":\"Amar Hotel, Opp BMTC Bus Stand - 06:00 PM,Address : Amar Hotel, Opp BMTC Bus Stand, Majestic,Bnaglore, Landmark : Tankband, Phone :080-42112179\",\"dropping_to\":\"Ashok nagar - 06:00 AM\",\"bus_type\":\"Non A\\\/C, Seater\",\"operator\":\"GDS Demo Test\",\"CommAmount\":\"20\",\"CancPolicy\":[{\"Amt\":0,\"Pct\":100,\"Mins\":0},{\"Amt\":0,\"Pct\":50,\"Mins\":1440},{\"Amt\":0,\"Pct\":0,\"Mins\":2440}],\"seat_attr\":{\"markup_price_summary\":\"204\",\"total_price_summary\":\"204\",\"domain_deduction_fare\":\"204\",\"default_currency\":\"INR\",\"seats\":{\"5\":{\"Fare\":\"204\",\"Markup_Fare\":\"204\",\"IsAcSeat\":\"false\",\"SeatType\":\"1\",\"seq_no\":\"4\",\"decks\":\"Lower\"}}}}","payment_type":"PAYPAL"}';
		$get_data = $this->input->post('confirm_book');
		$wallet_bal = $this->input->post('wallet_bal');
		$user_type = $this->input->post('user_type');

		if(empty($get_data))
		{
			$data['status'] = FAILURE_STATUS;
			$data['message'] = "Please enter the details to search";
			$this->output_compressed_data($data);
			exit;
		}
		$bus_book_para = json_decode($get_data);
		$post_params = json_decode(json_encode($bus_book_para), True);

        $post_params['token'] = serialized_data($post_params['token']);
      
		if(valid_array($post_params) == false){
			$res['status'] = FAILURE_STATUS;
			$res['message'] = "Invalid Request ";
			$res['data'] = array();
			$this->output_compressed_data($res);
		}else{
			$search_id = $post_params['search_id'];
		$this->custom_db->generate_static_response(json_encode($post_params));

		//Make sure token and temp token matches
		$temp_token = json_decode(unserialized_data($post_params['token']),true);
		if ($temp_token != false) {
			$post_params['booking_source'] = strtoupper($post_params['booking_source']);
			// debug($post_params['booking_source']); exit(); 
			load_bus_lib($post_params['booking_source']);
			if ($post_params['booking_source'] == PROVAB_BUS_BOOKING_SOURCE) {
		// echo "sdfsdf"; exit();
				$amount		= $temp_token['seat_attr']['markup_price_summary'];
				$currency	= $temp_token['seat_attr']['default_currency'];
			}

			//check current balance before proceeding further
			$domain_balance_status = $this->domain_management_model->verify_current_balance($amount, $currency);
       // debug($domain_balance_status); exit;

			if ($domain_balance_status == true) {
				//Block Seats
				//run block and then booking request
				$post_params['token'] = $temp_token;
				$block_status = $this->bus_lib->block_seats($search_id, $post_params);

              	//  debug($block_status); exit;
				if ($block_status['status'] == SUCCESS_STATUS) {
					$post_params['block_key'] = $block_status['data']['result']['HoldKey'];
					$post_params['block_data'] = $block_status['data']['result']['Passenger'];
					$post_params['wallet_bal']=$wallet_bal;
					//update seat block details and continue
                    $post_params['token'] = serialized_data($temp_token);
					//$post_params['token'] = serialized_data($post_params['token']);
					$temp_booking = $this->module_model->serialize_temp_booking_record($post_params, BUS_BOOKING);

					$book_id = $temp_booking['book_id'];
					$book_origin = $temp_booking['temp_booking_origin'];
					if(isset($post_params['promo_code_discount_val']) && !empty($post_params['promo_code_discount_val']))
					{
						$amount=$amount-$post_params['promo_code_discount_val'];
					}
					//details for PGI
					$email = $post_params ['billing_email'];
					$phone = $post_params ['passenger_contact'];
					$GLOBALS['CI']->entity_user_id = $post_params ['customer_id'];

					$pgi_amount = $amount;
					$firstname = $post_params ['contact_name'] ['0'];
					$productinfo = META_BUS_COURSE;
					
					// $amount = $passenger_data['final_fare'];
					// debug($wallet_bal);exit;
					$wallet_bal=='off';
					if($wallet_bal=='on')
					{
						$wallet_amount=0;
						$wallet_data=$this->user_model->get_wallet_log($GLOBALS['CI']->entity_user_id);
						// debug($wallet_data);
						if(isset($wallet_data[0]) && !empty($wallet_data[0]))
						{

							$wallet_amount=$wallet_data[0]['closing_balance'];
							// debug($wallet_amount);
							if(($amount)>$wallet_amount)
							{
								// $this->flight_model->save_wallet_log($book_id,$amount,false,$wallet_amount);
								$amount=($amount)-$wallet_amount;
							}else if(($amount)<$wallet_amount)
							{
								// $this->flight_model->save_wallet_log($book_id,$amount,false,$wallet_amount);
								$amount=0;
							}else if(($amount)==$wallet_amount)
							{
								// $this->flight_model->save_wallet_log($book_id,$amount,false,$wallet_amount);
								$amount=0;
							}
						}
					}
					// debug($amount);exit;

					$this->load->model('transaction');
					// debug(base_url()); die;
					
					//$pgi_amount =1; //change this later -->
					$this->transaction->create_payment_record($book_id, $pgi_amount, $firstname, $email, $phone, $productinfo);
					//$payment_gateway_url = (base_url().'mobile/index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin.'/'.$post_params['payment_type']);
					

					$url=explode('/mobile_webservices', base_url());
					// debug($url); exit();

					/*if($amount>0)
					{*/
						if ($user_type == "b2b") {
							$domain_balance_status_b2b = $this->domain_management_model->verify_current_balance_b2b ( $amount, $currency, $post_params1['customer_id']);

							if ($domain_balance_status_b2b == false) {
								$pre_booking_status['status'] = FAILURE_STATUS;
								$pre_booking_status['message'] = "Insufficient Balance";
								$this->output_compressed_data($pre_booking_status);
								exit();
							}
							echo("arg1"); exit;
							$pre_booking_status['status'] = SUCCESS_STATUS;
							$this->transaction->update_payment_record_status($book_id, ACCEPTED, $response_params);
							$pre_booking_status['data']['return_url']   = base_url().'mobile/index.php/bus/process_booking/'.$book_id.'/'.$book_origin . '/' . $user_type . '/' . $GLOBALS['CI']->entity_user_id;
							$this->output_compressed_data($pre_booking_status);
							exit();
						} else {
							$payment_gateway_url = $url[0].'/index.php/payment_gateway/payment/'.$book_id.'/'.$book_origin;

							$pre_booking_status['data'] = array('app_reference'=>$book_id,'temp_book_origin'=>$book_origin,'return_url'=>$payment_gateway_url);
							$pre_booking_status['status']   = SUCCESS_STATUS;
							$this->output_compressed_data($pre_booking_status);
							exit();
						}
					/*}else{

						$pre_booking_status['status']   = SUCCESS_STATUS;
						$pre_booking_status['response_params'] ='wallet_amount_used';
						$this->transaction->update_payment_record_status($book_id, ACCEPTED, $response_params);
						$pre_booking_status['data']['return_url']   = $url[0].'/index.php/bus/process_booking/'.$book_id.'/'.$book_origin;
						$this->output_compressed_data($pre_booking_status);
						exit();
					}*/
							
					}
					else{
						$res['status'] = FAILURE_STATUS;
						$res['message'] = "Unable to process your request.";
						$res['data'] = array();
						echo json_encode($res); exit;
					}
				} else {
					$res['status'] = FAILURE_STATUS;
					$res['message'] = "You Don't have enough balance to book this ticket Please contact admin";
					$res['data'] = array();
					echo json_encode($res); exit;
				}
			} else {
				$res['status'] = FAILURE_STATUS;
				$res['message'] = "Invalid Request ";
				$res['data'] = array();
				echo json_encode($res); exit;
			}
		}
			
	}

	/**
	 * Compress and output data
	 * @param array $data
	 */
	private function output_compressed_data($data)
	{
		while (ob_get_level() > 0) { ob_end_clean() ; }
		ob_start("ob_gzhandler");
		header('Content-type:application/json');
		echo json_encode($data);
		ob_end_flush();
		exit;
	}
}
