<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['master_module_list']	= array(
META_AIRLINE_COURSE => 'flights',
META_TRANSFER_COURSE => 'transfershotelbed',
META_ACCOMODATION_COURSE => 'hotels',
META_BUS_COURSE => 'buses',
META_TRANSFERV1_COURSE=>'transfers',
// META_CAR_COURSE=>'car',
META_SIGHTSEEING_COURSE=>'activities',
META_PACKAGE_COURSE => 'holidays'

);
/******** Current Module ********/
$config['current_module'] = 'b2c';

$config['load_minified'] = false;

$config['verify_domain_balance'] = false;

/******** PAYMENT GATEWAY START ********/
//To enable/disable PG
$config['enable_payment_gateway'] = true;
$config['active_payment_gateway'] = 'AUTHORIZE';
$config['active_payment_system'] = 'live';
$config['payment_gateway_currency'] = 'USD';//INR//test/live
/******** PAYMENT GATEWAY END ********/


/******** BOOKING ENGINE START ********/
$config['flight_engine_system'] = 'test'; //test/live
$config['hotel_engine_system'] = 'test'; //test/live
$config['bus_engine_system'] = 'test'; //test/live
$config['transfer_engine_system'] ='test';
$config['external_service_system'] = 'test'; //test/live

$config['sight_seen_api_key'] = CURRENT_SIGHTSEEN_API_KEY;

$config['domain_key'] = CURRENT_DOMAIN_KEY;
$config['test_username'] = 'test245274'; // test229267
$config['test_password'] = 'test@245'; // test@229

/*$config['live_username'] = 'TMX390869';
$config['live_password'] = 'TMX@884390';

$config['test_username'] = 'TMX390869';
$config['test_password'] = 'TMX@884390';
*/


//Production URL
$config['flight_url'] = 'http://test.services.travelomatix.com/webservices/index.php/flight/service/';
$config['hotel_url'] = 'http://test.services.travelomatix.com/webservices/index.php/hotel_v3/service/';
$config['bus_url'] = 'http://test.services.travelomatix.com/webservices/index.php/bus/service/';
$config['sightseeing_url'] = 'http://test.services.travelomatix.com/webservices/index.php/sightseeing/service/';
$config['transferv1_url'] = 'http://test.services.travelomatix.com/webservices/index.php/transferv1/service/';

/*$config['flight_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/flight/service/';
$config['hotel_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/hotel_v3/service/';
$config['bus_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/bus/service/';
$config['sightseeing_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/sightseeing/service/';
$config['transferv1_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/transferv1/service/';*/

if ($config['external_service_system'] == 'live') {
	$config['external_service'] = 'http://prod.services.travelomatix.com/webservices/index.php/rest/';
} else {
	$config['external_service'] = 'http://test.services.travelomatix.com/webservices/index.php/rest/';
}
// $config['flight_url'] = 'http://test.services.travelomatix.com/webservices/index.php/flight/service/';
// $config['hotel_url'] = 'http://test.services.travelomatix.com/webservices/index.php/hotel_v3/service/';
// $config['bus_url'] = 'http://test.services.travelomatix.com/webservices/index.php/bus/service/';
// $config['sightseeing_url'] = 'http://test.services.travelomatix.com/webservices/index.php/sightseeing/service/';
// $config['transferv1_url'] = 'http://test.services.travelomatix.com/webservices/index.php/transferv1/service/';


// $config['live_username'] = 'TMX250875';
// $config['live_password'] = 'TMX@702250';
/******** BOOKING ENGINE END ********/


/**
 * 
 * Enable/Disable caching for search result
 */
$config['cache_hotel_search'] = true;
$config['cache_flight_search'] = true;
$config['cache_flight_ssr_search'] = true;
$config['cache_bus_search'] = true;


/**
 * Number of seconds results should be cached in the system
 */
$config['cache_hotel_search_ttl'] = 600;
$config['cache_flight_search_ttl'] = 600;
$config['cache_bus_search_ttl'] = 600;
$config['cache_sight_seen_search_ttl'] = 60;



/*$config['lazy_load_hotel_search'] = true;*/
$config['hotel_per_page_limit'] = 20;
$config['sight_seen_page_per_limit'] = 100;

/*
	search session expiry period in seconds
*/
$config['flight_search_session_expiry_period'] = 900;//600
$config['flight_search_session_expiry_alert_period'] = 600;//300
