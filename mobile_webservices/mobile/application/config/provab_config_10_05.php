<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['master_module_list']	= array(
META_AIRLINE_COURSE => 'flight',
META_TRANSFERS_COURSE => 'transfer',
META_ACCOMODATION_COURSE => 'hotel',
META_BUS_COURSE => 'bus',
META_PACKAGE_COURSE => 'package'
);
/******** Current Module ********/
$config['current_module'] = 'b2c';

$config['load_minified'] = false;

$config['verify_domain_balance'] = false;

/******** PAYMENT GATEWAY START ********/
//To enable/disable PG
$config['enable_payment_gateway'] = true;
$config['active_payment_gateway'] = 'PAYU';
$config['active_payment_system'] = 'test';//test/live
/******** PAYMENT GATEWAY END ********/


/******** BOOKING ENGINE START ********/
$config['flight_engine_system'] = 'live'; //test/live
$config['hotel_engine_system'] = 'live'; //test/live
$config['bus_engine_system'] = 'live'; //test/live
$config['external_service_system'] = 'live'; //test/live

$config['domain_key'] = CURRENT_DOMAIN_KEY;
$config['test_username'] = 'test';
$config['test_password'] = 'password';



//Production URL
$config['flight_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/flight/service/';
$config['hotel_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/hotel_v3/service/';
//$config['bus_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/bus_server/';

$config['bus_url'] = 'http://prod.services.travelomatix.com/webservices/index.php/bus/service/';

if ($config['external_service_system'] == 'live') {
	$config['external_service'] = 'http://prod.services.travelomatix.com/webservices/index.php/rest/';
} else {
	$config['external_service'] = 'http://test.services.travelomatix.com/webservices/index.php/rest/';
}

$config['live_username'] = 'accentric_2017';
$config['live_password'] = 'accentric_@2017';
/******** BOOKING ENGINE END ********/


/**
 * 
 * Enable/Disable caching for search result
 */
$config['cache_hotel_search'] = true;
$config['cache_flight_search'] = true;
$config['cache_bus_search'] = true;


/**
 * Number of seconds results should be cached in the system
 */
$config['cache_hotel_search_ttl'] = 300;
$config['cache_flight_search_ttl'] = 3000;
$config['cache_bus_search_ttl'] = 300;


/*$config['lazy_load_hotel_search'] = true;*/
$config['hotel_per_page_limit'] = 20;
/*
	search session expiry period in seconds
*/
$config['flight_search_session_expiry_period'] = 600;//600
$config['flight_search_session_expiry_alert_period'] = 300;//300
